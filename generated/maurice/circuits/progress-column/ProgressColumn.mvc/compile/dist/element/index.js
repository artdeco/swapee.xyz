/**
 * An abstract class of `xyz.swapee.wc.IProgressColumn` interface.
 * @extends {xyz.swapee.wc.AbstractProgressColumn}
 */
class AbstractProgressColumn extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IProgressColumn_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ProgressColumnPort}
 */
class ProgressColumnPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnController` interface.
 * @extends {xyz.swapee.wc.AbstractProgressColumnController}
 */
class AbstractProgressColumnController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IProgressColumn_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ProgressColumnElement}
 */
class ProgressColumnElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ProgressColumnBuffer}
 */
class ProgressColumnBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnComputer` interface.
 * @extends {xyz.swapee.wc.AbstractProgressColumnComputer}
 */
class AbstractProgressColumnComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ProgressColumnComputer}
 */
class ProgressColumnComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ProgressColumnController}
 */
class ProgressColumnController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractProgressColumn = AbstractProgressColumn
module.exports.ProgressColumnPort = ProgressColumnPort
module.exports.AbstractProgressColumnController = AbstractProgressColumnController
module.exports.ProgressColumnElement = ProgressColumnElement
module.exports.ProgressColumnBuffer = ProgressColumnBuffer
module.exports.AbstractProgressColumnComputer = AbstractProgressColumnComputer
module.exports.ProgressColumnComputer = ProgressColumnComputer
module.exports.ProgressColumnController = ProgressColumnController

Object.defineProperties(module.exports, {
 'AbstractProgressColumn': {get: () => require('./precompile/internal')[65269839711]},
 [65269839711]: {get: () => module.exports['AbstractProgressColumn']},
 'ProgressColumnPort': {get: () => require('./precompile/internal')[65269839713]},
 [65269839713]: {get: () => module.exports['ProgressColumnPort']},
 'AbstractProgressColumnController': {get: () => require('./precompile/internal')[65269839714]},
 [65269839714]: {get: () => module.exports['AbstractProgressColumnController']},
 'ProgressColumnElement': {get: () => require('./precompile/internal')[65269839718]},
 [65269839718]: {get: () => module.exports['ProgressColumnElement']},
 'ProgressColumnBuffer': {get: () => require('./precompile/internal')[652698397111]},
 [652698397111]: {get: () => module.exports['ProgressColumnBuffer']},
 'AbstractProgressColumnComputer': {get: () => require('./precompile/internal')[652698397130]},
 [652698397130]: {get: () => module.exports['AbstractProgressColumnComputer']},
 'ProgressColumnComputer': {get: () => require('./precompile/internal')[652698397131]},
 [652698397131]: {get: () => module.exports['ProgressColumnComputer']},
 'ProgressColumnController': {get: () => require('./precompile/internal')[652698397161]},
 [652698397161]: {get: () => module.exports['ProgressColumnController']},
})