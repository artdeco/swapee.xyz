/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} */
xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.IProgressColumnControllerARCaster filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnControllerAR} */
$xyz.swapee.wc.back.IProgressColumnControllerARCaster.prototype.asIProgressColumnControllerAR
/** @type {!xyz.swapee.wc.back.BoundProgressColumnControllerAR} */
$xyz.swapee.wc.back.IProgressColumnControllerARCaster.prototype.superProgressColumnControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnControllerARCaster}
 */
xyz.swapee.wc.back.IProgressColumnControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.IProgressColumnControllerAR filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 */
$xyz.swapee.wc.back.IProgressColumnControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnControllerAR}
 */
xyz.swapee.wc.back.IProgressColumnControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.ProgressColumnControllerAR filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.ProgressColumnControllerAR
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnControllerAR, ...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese)} */
xyz.swapee.wc.back.ProgressColumnControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.ProgressColumnControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.AbstractProgressColumnControllerAR filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
$xyz.swapee.wc.back.AbstractProgressColumnControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnControllerAR)} */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.ProgressColumnControllerARConstructor filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnControllerAR, ...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese)} */
xyz.swapee.wc.back.ProgressColumnControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.RecordIProgressColumnControllerAR filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.BoundIProgressColumnControllerAR filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 */
$xyz.swapee.wc.back.BoundIProgressColumnControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnControllerAR} */
xyz.swapee.wc.back.BoundIProgressColumnControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.BoundProgressColumnControllerAR filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnControllerAR} */
xyz.swapee.wc.back.BoundProgressColumnControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */