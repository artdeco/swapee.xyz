/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.IProgressColumnScreen.Initialese filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnScreen.Initialese} */
xyz.swapee.wc.back.IProgressColumnScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.IProgressColumnScreenCaster filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnScreen} */
$xyz.swapee.wc.back.IProgressColumnScreenCaster.prototype.asIProgressColumnScreen
/** @type {!xyz.swapee.wc.back.BoundProgressColumnScreen} */
$xyz.swapee.wc.back.IProgressColumnScreenCaster.prototype.superProgressColumnScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreenCaster}
 */
xyz.swapee.wc.back.IProgressColumnScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.IProgressColumnScreen filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenCaster}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenAT}
 */
$xyz.swapee.wc.back.IProgressColumnScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreen}
 */
xyz.swapee.wc.back.IProgressColumnScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.ProgressColumnScreen filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreen.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.ProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnScreen, ...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.ProgressColumnScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.AbstractProgressColumnScreen filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnScreen}
 */
$xyz.swapee.wc.back.AbstractProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnScreen)} */
xyz.swapee.wc.back.AbstractProgressColumnScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.ProgressColumnScreenConstructor filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnScreen, ...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.RecordIProgressColumnScreen filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.BoundIProgressColumnScreen filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreenAT}
 */
$xyz.swapee.wc.back.BoundIProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnScreen} */
xyz.swapee.wc.back.BoundIProgressColumnScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.BoundProgressColumnScreen filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnScreen} */
xyz.swapee.wc.back.BoundProgressColumnScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */