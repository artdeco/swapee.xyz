/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Initialese filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IProgressColumnElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Initialese} */
xyz.swapee.wc.IProgressColumnElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPortFields filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @interface */
$xyz.swapee.wc.IProgressColumnElementPortFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnElementPort.Inputs} */
$xyz.swapee.wc.IProgressColumnElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IProgressColumnElementPort.Inputs} */
$xyz.swapee.wc.IProgressColumnElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnElementPortFields}
 */
xyz.swapee.wc.IProgressColumnElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPortCaster filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @interface */
$xyz.swapee.wc.IProgressColumnElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnElementPort} */
$xyz.swapee.wc.IProgressColumnElementPortCaster.prototype.asIProgressColumnElementPort
/** @type {!xyz.swapee.wc.BoundProgressColumnElementPort} */
$xyz.swapee.wc.IProgressColumnElementPortCaster.prototype.superProgressColumnElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnElementPortCaster}
 */
xyz.swapee.wc.IProgressColumnElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IProgressColumnElementPort.Inputs>}
 */
$xyz.swapee.wc.IProgressColumnElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnElementPort}
 */
xyz.swapee.wc.IProgressColumnElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.ProgressColumnElementPort filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElementPort.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnElementPort.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.ProgressColumnElementPort
/** @type {function(new: xyz.swapee.wc.IProgressColumnElementPort, ...!xyz.swapee.wc.IProgressColumnElementPort.Initialese)} */
xyz.swapee.wc.ProgressColumnElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.ProgressColumnElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.AbstractProgressColumnElementPort filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElementPort.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnElementPort}
 */
$xyz.swapee.wc.AbstractProgressColumnElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnElementPort)} */
xyz.swapee.wc.AbstractProgressColumnElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnElementPort|typeof xyz.swapee.wc.ProgressColumnElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnElementPort|typeof xyz.swapee.wc.ProgressColumnElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnElementPort|typeof xyz.swapee.wc.ProgressColumnElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.ProgressColumnElementPortConstructor filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElementPort, ...!xyz.swapee.wc.IProgressColumnElementPort.Initialese)} */
xyz.swapee.wc.ProgressColumnElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.RecordIProgressColumnElementPort filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.BoundIProgressColumnElementPort filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnElementPortFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IProgressColumnElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundIProgressColumnElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnElementPort} */
xyz.swapee.wc.BoundIProgressColumnElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.BoundProgressColumnElementPort filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnElementPort} */
xyz.swapee.wc.BoundProgressColumnElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts.kycBlockOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts.kycBlockOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts.kycRequiredOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts.kycRequiredOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts.kycNotRequiredOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts.kycNotRequiredOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts.exchangeEmailOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts.exchangeEmailOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts.infoNoticeOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts.infoNoticeOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts.supportNoticeOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts.supportNoticeOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts.paymentStatusWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts.paymentStatusWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts.paymentStatusLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts.paymentStatusLaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts.transactionStatusWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts.transactionStatusWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts.transactionStatusLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts.transactionStatusLaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts.step2BotSepOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts.step2BotSepOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts.step1CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts.step1CoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts.step2CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts.step2CoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts.step3CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts.step3CoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts.step1LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts.step1LaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts.step2LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts.step2LaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts.step3LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts.step3LaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts.transactionIdWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts.transactionIdWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts.transactionIdLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts.transactionIdLaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts.step1ProgressCircleWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts.step1ProgressCircleWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts.step2ProgressCircleWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts.step2ProgressCircleWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts.step1WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts.step1WrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts.step2WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts.step2WrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts.step3WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts.step3WrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts.step1CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts.step1CircleOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts.step2CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts.step2CircleOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts.step3CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @typedef {!Object} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts.step3CircleOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts.prototype.kycBlockOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts.prototype.kycRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts.prototype.kycNotRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts.prototype.exchangeEmailOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts.prototype.infoNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts.prototype.supportNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts.prototype.paymentStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts.prototype.paymentStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts.prototype.transactionStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts.prototype.transactionStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts.prototype.step2BotSepOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts.prototype.step1CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts.prototype.step2CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts.prototype.step3CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts.prototype.step1LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts.prototype.step2LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts.prototype.step3LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts.prototype.transactionIdWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts.prototype.transactionIdLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts.prototype.step1ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts.prototype.step2ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts.prototype.step1WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts.prototype.step2WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts.prototype.step3WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts.prototype.step1CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts.prototype.step2CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts.prototype.step3CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts}
 */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IProgressColumnElementPort.Inputs}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts.prototype.kycBlockOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts.prototype.kycRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts.prototype.kycNotRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts.prototype.exchangeEmailOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts.prototype.infoNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts.prototype.supportNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts.prototype.paymentStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts.prototype.paymentStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts.prototype.transactionStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts.prototype.transactionStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts.prototype.step2BotSepOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts.prototype.step1CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts.prototype.step2CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts.prototype.step3CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts.prototype.step1LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts.prototype.step2LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts.prototype.step3LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts.prototype.transactionIdWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts.prototype.transactionIdLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts.prototype.step1ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts.prototype.step2ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts.prototype.step1WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts.prototype.step2WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts.prototype.step3WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts.prototype.step1CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts.prototype.step2CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts.prototype.step3CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts}
 */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs}
 */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts_Safe.prototype.kycBlockOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts_Safe.prototype.kycRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts_Safe.prototype.kycNotRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts_Safe.prototype.exchangeEmailOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts_Safe.prototype.infoNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts_Safe.prototype.supportNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts_Safe.prototype.paymentStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts_Safe.prototype.paymentStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts_Safe.prototype.transactionStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts_Safe.prototype.transactionStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts_Safe.prototype.step2BotSepOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts_Safe.prototype.step1CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts_Safe.prototype.step2CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts_Safe.prototype.step3CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts_Safe.prototype.step1LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts_Safe.prototype.step2LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts_Safe.prototype.step3LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts_Safe.prototype.transactionIdWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts_Safe.prototype.transactionIdLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts_Safe.prototype.step1ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts_Safe.prototype.step2ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts_Safe.prototype.step1WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts_Safe.prototype.step2WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts_Safe.prototype.step3WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts_Safe.prototype.step1CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts_Safe.prototype.step2CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts_Safe.prototype.step3CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts_Safe.prototype.kycBlockOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts_Safe.prototype.kycRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts_Safe.prototype.kycNotRequiredOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts_Safe.prototype.exchangeEmailOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts_Safe.prototype.infoNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts_Safe.prototype.supportNoticeOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts_Safe.prototype.paymentStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts_Safe.prototype.paymentStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts_Safe.prototype.transactionStatusWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts_Safe.prototype.transactionStatusLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts_Safe.prototype.step2BotSepOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts_Safe.prototype.step1CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts_Safe.prototype.step2CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts_Safe.prototype.step3CoOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts_Safe.prototype.step1LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts_Safe.prototype.step2LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts_Safe.prototype.step3LaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts_Safe.prototype.transactionIdWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts_Safe.prototype.transactionIdLaOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts_Safe.prototype.step1ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts_Safe.prototype.step2ProgressCircleWrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts_Safe.prototype.step1WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts_Safe.prototype.step2WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts_Safe.prototype.step3WrOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts_Safe.prototype.step1CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts_Safe.prototype.step2CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts_Safe filter:!ControllerPlugin~props 027536e32a7586006f905813d5f30069 */
/** @record */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts_Safe.prototype.step3CircleOpts
/** @typedef {$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts_Safe} */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
/* @typal-end */