/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/41-ProgressColumnClasses.xml} xyz.swapee.wc.ProgressColumnClasses filter:!ControllerPlugin~props 0b33a67f2f14608fcea73f64c0ab6978 */
/** @record */
$xyz.swapee.wc.ProgressColumnClasses = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleLoading
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleComplete
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleWaiting
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleFailed
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.StepLabelFailed
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.StepLabelWaiting
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ProgressColumnClasses}
 */
xyz.swapee.wc.ProgressColumnClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */