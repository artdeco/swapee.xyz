/** @const {?} */ $xyz.swapee.wc.IProgressColumnGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplay.Initialese}
 */
$xyz.swapee.wc.IProgressColumnGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnGPU.Initialese} */
xyz.swapee.wc.IProgressColumnGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IProgressColumnGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IProgressColumnGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnGPUFields}
 */
xyz.swapee.wc.IProgressColumnGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IProgressColumnGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnGPU} */
$xyz.swapee.wc.IProgressColumnGPUCaster.prototype.asIProgressColumnGPU
/** @type {!xyz.swapee.wc.BoundProgressColumnGPU} */
$xyz.swapee.wc.IProgressColumnGPUCaster.prototype.superProgressColumnGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnGPUCaster}
 */
xyz.swapee.wc.IProgressColumnGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!ProgressColumnMemory,>}
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplay}
 */
$xyz.swapee.wc.IProgressColumnGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnGPU}
 */
xyz.swapee.wc.IProgressColumnGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.ProgressColumnGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnGPU.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.ProgressColumnGPU
/** @type {function(new: xyz.swapee.wc.IProgressColumnGPU, ...!xyz.swapee.wc.IProgressColumnGPU.Initialese)} */
xyz.swapee.wc.ProgressColumnGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.ProgressColumnGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.AbstractProgressColumnGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnGPU}
 */
$xyz.swapee.wc.AbstractProgressColumnGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnGPU)} */
xyz.swapee.wc.AbstractProgressColumnGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.ProgressColumnGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnGPU, ...!xyz.swapee.wc.IProgressColumnGPU.Initialese)} */
xyz.swapee.wc.ProgressColumnGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.RecordIProgressColumnGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.BoundIProgressColumnGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnGPUFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!ProgressColumnMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnDisplay}
 */
$xyz.swapee.wc.BoundIProgressColumnGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnGPU} */
xyz.swapee.wc.BoundIProgressColumnGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.BoundProgressColumnGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnGPU} */
xyz.swapee.wc.BoundProgressColumnGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */