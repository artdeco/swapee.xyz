/** @const {?} */ $xyz.swapee.wc.front.IProgressColumnController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.IProgressColumnController.Initialese filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @record */
$xyz.swapee.wc.front.IProgressColumnController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IProgressColumnController.Initialese} */
xyz.swapee.wc.front.IProgressColumnController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.IProgressColumnControllerCaster filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @interface */
$xyz.swapee.wc.front.IProgressColumnControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIProgressColumnController} */
$xyz.swapee.wc.front.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/** @type {!xyz.swapee.wc.front.BoundProgressColumnController} */
$xyz.swapee.wc.front.IProgressColumnControllerCaster.prototype.superProgressColumnController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnControllerCaster}
 */
xyz.swapee.wc.front.IProgressColumnControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.IProgressColumnController filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerAT}
 */
$xyz.swapee.wc.front.IProgressColumnController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnController}
 */
xyz.swapee.wc.front.IProgressColumnController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.ProgressColumnController filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init
 * @implements {xyz.swapee.wc.front.IProgressColumnController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnController.Initialese>}
 */
$xyz.swapee.wc.front.ProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.ProgressColumnController
/** @type {function(new: xyz.swapee.wc.front.IProgressColumnController, ...!xyz.swapee.wc.front.IProgressColumnController.Initialese)} */
xyz.swapee.wc.front.ProgressColumnController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.ProgressColumnController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.AbstractProgressColumnController filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init
 * @extends {xyz.swapee.wc.front.ProgressColumnController}
 */
$xyz.swapee.wc.front.AbstractProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController
/** @type {function(new: xyz.swapee.wc.front.AbstractProgressColumnController)} */
xyz.swapee.wc.front.AbstractProgressColumnController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.ProgressColumnControllerConstructor filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnController, ...!xyz.swapee.wc.front.IProgressColumnController.Initialese)} */
xyz.swapee.wc.front.ProgressColumnControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.RecordIProgressColumnController filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIProgressColumnController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.BoundIProgressColumnController filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIProgressColumnController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnControllerAT}
 */
$xyz.swapee.wc.front.BoundIProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIProgressColumnController} */
xyz.swapee.wc.front.BoundIProgressColumnController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.BoundProgressColumnController filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundProgressColumnController} */
xyz.swapee.wc.front.BoundProgressColumnController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */