/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnScreenAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} */
xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.IProgressColumnScreenATCaster filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnScreenAT} */
$xyz.swapee.wc.back.IProgressColumnScreenATCaster.prototype.asIProgressColumnScreenAT
/** @type {!xyz.swapee.wc.back.BoundProgressColumnScreenAT} */
$xyz.swapee.wc.back.IProgressColumnScreenATCaster.prototype.superProgressColumnScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreenATCaster}
 */
xyz.swapee.wc.back.IProgressColumnScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.IProgressColumnScreenAT filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.IProgressColumnScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreenAT}
 */
xyz.swapee.wc.back.IProgressColumnScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.ProgressColumnScreenAT filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.ProgressColumnScreenAT
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnScreenAT, ...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.ProgressColumnScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.AbstractProgressColumnScreenAT filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
$xyz.swapee.wc.back.AbstractProgressColumnScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnScreenAT)} */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.ProgressColumnScreenATConstructor filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnScreenAT, ...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.RecordIProgressColumnScreenAT filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.BoundIProgressColumnScreenAT filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundIProgressColumnScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnScreenAT} */
xyz.swapee.wc.back.BoundIProgressColumnScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.BoundProgressColumnScreenAT filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnScreenAT} */
xyz.swapee.wc.back.BoundProgressColumnScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */