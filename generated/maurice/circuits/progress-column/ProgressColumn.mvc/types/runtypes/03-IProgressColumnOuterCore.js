/** @const {?} */ $xyz.swapee.wc.IProgressColumnOuterCore
/** @const {?} */ $xyz.swapee.wc.IProgressColumnOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPort
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IProgressColumnCore
/** @const {?} */ $xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Initialese filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Initialese} */
xyz.swapee.wc.IProgressColumnOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCoreFields filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @interface */
$xyz.swapee.wc.IProgressColumnOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnOuterCore.Model} */
$xyz.swapee.wc.IProgressColumnOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnOuterCoreFields}
 */
xyz.swapee.wc.IProgressColumnOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCoreCaster filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @interface */
$xyz.swapee.wc.IProgressColumnOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnOuterCore} */
$xyz.swapee.wc.IProgressColumnOuterCoreCaster.prototype.asIProgressColumnOuterCore
/** @type {!xyz.swapee.wc.BoundProgressColumnOuterCore} */
$xyz.swapee.wc.IProgressColumnOuterCoreCaster.prototype.superProgressColumnOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnOuterCoreCaster}
 */
xyz.swapee.wc.IProgressColumnOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreCaster}
 */
$xyz.swapee.wc.IProgressColumnOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnOuterCore}
 */
xyz.swapee.wc.IProgressColumnOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.ProgressColumnOuterCore filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnOuterCore.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.ProgressColumnOuterCore
/** @type {function(new: xyz.swapee.wc.IProgressColumnOuterCore)} */
xyz.swapee.wc.ProgressColumnOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.ProgressColumnOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.AbstractProgressColumnOuterCore filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ProgressColumnOuterCore}
 */
$xyz.swapee.wc.AbstractProgressColumnOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnOuterCore)} */
xyz.swapee.wc.AbstractProgressColumnOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.RecordIProgressColumnOuterCore filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.BoundIProgressColumnOuterCore filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIProgressColumnOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnOuterCore} */
xyz.swapee.wc.BoundIProgressColumnOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.BoundProgressColumnOuterCore filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnOuterCore} */
xyz.swapee.wc.BoundProgressColumnOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction.creatingTransaction filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction.creatingTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment.finishedOnPayment filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment.finishedOnPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed.paymentFailed filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed.paymentFailed

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted.paymentCompleted filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted.paymentCompleted

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId.transactionId filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId.transactionId

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus.paymentStatus filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus.paymentStatus

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1.loadingStep1 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1.loadingStep1

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2.loadingStep2 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2.loadingStep2

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3.loadingStep3 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3.loadingStep3

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status.status filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status.status

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1 = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2 = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3 = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status}
 */
$xyz.swapee.wc.IProgressColumnOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model} */
xyz.swapee.wc.IProgressColumnOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1 = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2 = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3 = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status}
 */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId} */
xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.Status filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.Status = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.Status} */
xyz.swapee.wc.IProgressColumnPort.Inputs.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionId filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3 filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.Status filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.Status = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.Status} */
xyz.swapee.wc.IProgressColumnCore.Model.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */