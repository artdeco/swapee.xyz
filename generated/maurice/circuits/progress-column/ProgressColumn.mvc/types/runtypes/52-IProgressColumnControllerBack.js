/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.IProgressColumnController.Initialese filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnController.Initialese} */
xyz.swapee.wc.back.IProgressColumnController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.IProgressColumnControllerCaster filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnController} */
$xyz.swapee.wc.back.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/** @type {!xyz.swapee.wc.back.BoundProgressColumnController} */
$xyz.swapee.wc.back.IProgressColumnControllerCaster.prototype.superProgressColumnController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnControllerCaster}
 */
xyz.swapee.wc.back.IProgressColumnControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.IProgressColumnController filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.back.IProgressColumnController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnController}
 */
xyz.swapee.wc.back.IProgressColumnController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.ProgressColumnController filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnController.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.ProgressColumnController
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnController, ...!xyz.swapee.wc.back.IProgressColumnController.Initialese)} */
xyz.swapee.wc.back.ProgressColumnController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.ProgressColumnController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.AbstractProgressColumnController filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnController}
 */
$xyz.swapee.wc.back.AbstractProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnController)} */
xyz.swapee.wc.back.AbstractProgressColumnController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.ProgressColumnControllerConstructor filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnController, ...!xyz.swapee.wc.back.IProgressColumnController.Initialese)} */
xyz.swapee.wc.back.ProgressColumnControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.RecordIProgressColumnController filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.BoundIProgressColumnController filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnController} */
xyz.swapee.wc.back.BoundIProgressColumnController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.BoundProgressColumnController filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnController} */
xyz.swapee.wc.back.BoundProgressColumnController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */