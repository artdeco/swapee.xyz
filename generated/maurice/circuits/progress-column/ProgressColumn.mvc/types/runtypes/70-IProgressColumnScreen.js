/** @const {?} */ $xyz.swapee.wc.IProgressColumnScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.IProgressColumnScreen.Initialese filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.IProgressColumnDisplay.Initialese}
 */
$xyz.swapee.wc.IProgressColumnScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnScreen.Initialese} */
xyz.swapee.wc.IProgressColumnScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.IProgressColumnScreenCaster filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/** @interface */
$xyz.swapee.wc.IProgressColumnScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnScreen} */
$xyz.swapee.wc.IProgressColumnScreenCaster.prototype.asIProgressColumnScreen
/** @type {!xyz.swapee.wc.BoundProgressColumnScreen} */
$xyz.swapee.wc.IProgressColumnScreenCaster.prototype.superProgressColumnScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnScreenCaster}
 */
xyz.swapee.wc.IProgressColumnScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.IProgressColumnScreen filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IProgressColumnController}
 * @extends {xyz.swapee.wc.IProgressColumnDisplay}
 */
$xyz.swapee.wc.IProgressColumnScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnScreen}
 */
xyz.swapee.wc.IProgressColumnScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.ProgressColumnScreen filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnScreen.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.ProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.IProgressColumnScreen, ...!xyz.swapee.wc.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.ProgressColumnScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.ProgressColumnScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.AbstractProgressColumnScreen filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnScreen}
 */
$xyz.swapee.wc.AbstractProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnScreen)} */
xyz.swapee.wc.AbstractProgressColumnScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.ProgressColumnScreenConstructor filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnScreen, ...!xyz.swapee.wc.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.ProgressColumnScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.RecordIProgressColumnScreen filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.BoundIProgressColumnScreen filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnController}
 * @extends {xyz.swapee.wc.BoundIProgressColumnDisplay}
 */
$xyz.swapee.wc.BoundIProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnScreen} */
xyz.swapee.wc.BoundIProgressColumnScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.BoundProgressColumnScreen filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnScreen} */
xyz.swapee.wc.BoundProgressColumnScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */