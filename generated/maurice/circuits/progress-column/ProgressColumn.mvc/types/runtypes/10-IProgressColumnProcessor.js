/** @const {?} */ $xyz.swapee.wc.IProgressColumnProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.IProgressColumnProcessor.Initialese filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnComputer.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.IProgressColumnProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnProcessor.Initialese} */
xyz.swapee.wc.IProgressColumnProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.IProgressColumnProcessorCaster filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/** @interface */
$xyz.swapee.wc.IProgressColumnProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnProcessor} */
$xyz.swapee.wc.IProgressColumnProcessorCaster.prototype.asIProgressColumnProcessor
/** @type {!xyz.swapee.wc.BoundProgressColumnProcessor} */
$xyz.swapee.wc.IProgressColumnProcessorCaster.prototype.superProgressColumnProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnProcessorCaster}
 */
xyz.swapee.wc.IProgressColumnProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.IProgressColumnProcessor filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnProcessorCaster}
 * @extends {xyz.swapee.wc.IProgressColumnComputer}
 * @extends {xyz.swapee.wc.IProgressColumnCore}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 */
$xyz.swapee.wc.IProgressColumnProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnProcessor}
 */
xyz.swapee.wc.IProgressColumnProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.ProgressColumnProcessor filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnProcessor.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.ProgressColumnProcessor
/** @type {function(new: xyz.swapee.wc.IProgressColumnProcessor, ...!xyz.swapee.wc.IProgressColumnProcessor.Initialese)} */
xyz.swapee.wc.ProgressColumnProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.ProgressColumnProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.AbstractProgressColumnProcessor filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnProcessor}
 */
$xyz.swapee.wc.AbstractProgressColumnProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnProcessor)} */
xyz.swapee.wc.AbstractProgressColumnProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.ProgressColumnProcessorConstructor filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnProcessor, ...!xyz.swapee.wc.IProgressColumnProcessor.Initialese)} */
xyz.swapee.wc.ProgressColumnProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.RecordIProgressColumnProcessor filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.BoundIProgressColumnProcessor filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 * @extends {xyz.swapee.wc.BoundIProgressColumnCore}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 */
$xyz.swapee.wc.BoundIProgressColumnProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnProcessor} */
xyz.swapee.wc.BoundIProgressColumnProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.BoundProgressColumnProcessor filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnProcessor} */
xyz.swapee.wc.BoundProgressColumnProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */