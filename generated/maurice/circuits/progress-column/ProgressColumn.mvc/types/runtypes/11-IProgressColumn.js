/** @const {?} */ $xyz.swapee.wc.IProgressColumn
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumnEnv filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @record */
$xyz.swapee.wc.ProgressColumnEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IProgressColumn} */
$xyz.swapee.wc.ProgressColumnEnv.prototype.progressColumn
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ProgressColumnEnv}
 */
xyz.swapee.wc.ProgressColumnEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn.Initialese filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnComputer.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.IProgressColumn.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumn.Initialese} */
xyz.swapee.wc.IProgressColumn.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumn
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumnFields filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @interface */
$xyz.swapee.wc.IProgressColumnFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumn.Pinout} */
$xyz.swapee.wc.IProgressColumnFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnFields}
 */
xyz.swapee.wc.IProgressColumnFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumnCaster filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @interface */
$xyz.swapee.wc.IProgressColumnCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumn} */
$xyz.swapee.wc.IProgressColumnCaster.prototype.asIProgressColumn
/** @type {!xyz.swapee.wc.BoundProgressColumn} */
$xyz.swapee.wc.IProgressColumnCaster.prototype.superProgressColumn
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCaster}
 */
xyz.swapee.wc.IProgressColumnCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCaster}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor}
 * @extends {xyz.swapee.wc.IProgressColumnComputer}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, null>}
 */
$xyz.swapee.wc.IProgressColumn = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumn}
 */
xyz.swapee.wc.IProgressColumn

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumn filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumn}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumn.Initialese>}
 */
$xyz.swapee.wc.ProgressColumn = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.ProgressColumn
/** @type {function(new: xyz.swapee.wc.IProgressColumn, ...!xyz.swapee.wc.IProgressColumn.Initialese)} */
xyz.swapee.wc.ProgressColumn.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.ProgressColumn.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.AbstractProgressColumn filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumn}
 */
$xyz.swapee.wc.AbstractProgressColumn = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumn)} */
xyz.swapee.wc.AbstractProgressColumn.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumn.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumnConstructor filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumn, ...!xyz.swapee.wc.IProgressColumn.Initialese)} */
xyz.swapee.wc.ProgressColumnConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn.MVCOptions filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @record */
$xyz.swapee.wc.IProgressColumn.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IProgressColumn.Pinout)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IProgressColumn.Pinout)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IProgressColumn.Pinout} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.ProgressColumnMemory)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.ProgressColumnClasses)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IProgressColumn.MVCOptions} */
xyz.swapee.wc.IProgressColumn.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumn
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.RecordIProgressColumn filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.BoundIProgressColumn filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumn}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnProcessor}
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, null>}
 */
$xyz.swapee.wc.BoundIProgressColumn = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumn} */
xyz.swapee.wc.BoundIProgressColumn

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.BoundProgressColumn filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumn}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumn = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumn} */
xyz.swapee.wc.BoundProgressColumn

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn.Pinout filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnController.Inputs}
 */
$xyz.swapee.wc.IProgressColumn.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumn.Pinout} */
xyz.swapee.wc.IProgressColumn.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumn
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumnBuffer filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.IProgressColumnBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnBuffer}
 */
xyz.swapee.wc.IProgressColumnBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumnBuffer filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnBuffer}
 */
$xyz.swapee.wc.ProgressColumnBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnBuffer}
 */
xyz.swapee.wc.ProgressColumnBuffer
/** @type {function(new: xyz.swapee.wc.IProgressColumnBuffer)} */
xyz.swapee.wc.ProgressColumnBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */