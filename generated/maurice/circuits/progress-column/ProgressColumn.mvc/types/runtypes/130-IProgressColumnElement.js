/** @const {?} */ $xyz.swapee.wc.IProgressColumnElement
/** @const {?} */ xyz.swapee.wc.IProgressColumnElement
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElement.Initialese filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.IProgressColumnElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnElement.Initialese} */
xyz.swapee.wc.IProgressColumnElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElementFields filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/** @interface */
$xyz.swapee.wc.IProgressColumnElementFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnElement.Inputs} */
$xyz.swapee.wc.IProgressColumnElementFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnElementFields}
 */
xyz.swapee.wc.IProgressColumnElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElementCaster filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/** @interface */
$xyz.swapee.wc.IProgressColumnElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnElement} */
$xyz.swapee.wc.IProgressColumnElementCaster.prototype.asIProgressColumnElement
/** @type {!xyz.swapee.wc.BoundProgressColumnElement} */
$xyz.swapee.wc.IProgressColumnElementCaster.prototype.superProgressColumnElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnElementCaster}
 */
xyz.swapee.wc.IProgressColumnElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElement filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs, null>}
 */
$xyz.swapee.wc.IProgressColumnElement = function() {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} model
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IProgressColumnElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model]
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.IProgressColumnElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnElement}
 */
xyz.swapee.wc.IProgressColumnElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.ProgressColumnElement filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElement.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnElement.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.ProgressColumnElement
/** @type {function(new: xyz.swapee.wc.IProgressColumnElement, ...!xyz.swapee.wc.IProgressColumnElement.Initialese)} */
xyz.swapee.wc.ProgressColumnElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.ProgressColumnElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.AbstractProgressColumnElement filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElement.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnElement}
 */
$xyz.swapee.wc.AbstractProgressColumnElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnElement)} */
xyz.swapee.wc.AbstractProgressColumnElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnElement|typeof xyz.swapee.wc.ProgressColumnElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnElement|typeof xyz.swapee.wc.ProgressColumnElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnElement|typeof xyz.swapee.wc.ProgressColumnElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.ProgressColumnElementConstructor filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElement, ...!xyz.swapee.wc.IProgressColumnElement.Initialese)} */
xyz.swapee.wc.ProgressColumnElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.RecordIProgressColumnElement filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/** @typedef {{ solder: xyz.swapee.wc.IProgressColumnElement.solder, render: xyz.swapee.wc.IProgressColumnElement.render, server: xyz.swapee.wc.IProgressColumnElement.server, inducer: xyz.swapee.wc.IProgressColumnElement.inducer }} */
xyz.swapee.wc.RecordIProgressColumnElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.BoundIProgressColumnElement filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnElementFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs, null>}
 */
$xyz.swapee.wc.BoundIProgressColumnElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnElement} */
xyz.swapee.wc.BoundIProgressColumnElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.BoundProgressColumnElement filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnElement} */
xyz.swapee.wc.BoundProgressColumnElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElement.solder filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} model
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IProgressColumnElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} model
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.IProgressColumnElement}
 */
$xyz.swapee.wc.IProgressColumnElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} model
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.IProgressColumnElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement.solder} */
xyz.swapee.wc.IProgressColumnElement.solder
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement._solder} */
xyz.swapee.wc.IProgressColumnElement._solder
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement.__solder} */
xyz.swapee.wc.IProgressColumnElement.__solder

// nss:xyz.swapee.wc.IProgressColumnElement,$xyz.swapee.wc.IProgressColumnElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElement.render filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.ProgressColumnMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IProgressColumnElement.render
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnElement, !xyz.swapee.wc.ProgressColumnMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IProgressColumnElement._render
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement.__render} */
xyz.swapee.wc.IProgressColumnElement.__render

// nss:xyz.swapee.wc.IProgressColumnElement,$xyz.swapee.wc.IProgressColumnElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElement.server filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.IProgressColumnElement}
 */
$xyz.swapee.wc.IProgressColumnElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.IProgressColumnElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement.server} */
xyz.swapee.wc.IProgressColumnElement.server
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement._server} */
xyz.swapee.wc.IProgressColumnElement._server
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement.__server} */
xyz.swapee.wc.IProgressColumnElement.__server

// nss:xyz.swapee.wc.IProgressColumnElement,$xyz.swapee.wc.IProgressColumnElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElement.inducer filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model]
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} [port]
 */
$xyz.swapee.wc.IProgressColumnElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model]
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} [port]
 * @this {xyz.swapee.wc.IProgressColumnElement}
 */
$xyz.swapee.wc.IProgressColumnElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model]
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.IProgressColumnElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement.inducer} */
xyz.swapee.wc.IProgressColumnElement.inducer
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement._inducer} */
xyz.swapee.wc.IProgressColumnElement._inducer
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnElement.__inducer} */
xyz.swapee.wc.IProgressColumnElement.__inducer

// nss:xyz.swapee.wc.IProgressColumnElement,$xyz.swapee.wc.IProgressColumnElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml} xyz.swapee.wc.IProgressColumnElement.Inputs filter:!ControllerPlugin~props c76b1f480eae7c752b4762311da393f6 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnPort.Inputs}
 * @extends {xyz.swapee.wc.IProgressColumnDisplay.Queries}
 * @extends {xyz.swapee.wc.IProgressColumnController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.IProgressColumnElementPort.Inputs}
 */
$xyz.swapee.wc.IProgressColumnElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnElement.Inputs} */
xyz.swapee.wc.IProgressColumnElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnElement
/* @typal-end */