/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplay.Initialese filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ProgressColumnClasses>}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.KycBlock
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.KycRequired
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.KycNotRequired
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.ExchangeEmail
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.InfoNotice
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.SupportNotice
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.PaymentStatusWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.PaymentStatusLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionStatusWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionStatusLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2BotSep
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1Co
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2Co
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3Co
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1La
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2La
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3La
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionIdWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionIdLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1ProgressCircleWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2ProgressCircleWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1Wr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2Wr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3Wr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1Circle
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2Circle
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3Circle
/** @typedef {$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese} */
xyz.swapee.wc.back.IProgressColumnDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplayFields filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycBlock
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycRequired
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycNotRequired
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.ExchangeEmail
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.InfoNotice
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.SupportNotice
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.PaymentStatusWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.PaymentStatusLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionStatusWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionStatusLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2BotSep
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Co
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Co
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Co
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1La
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2La
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3La
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionIdWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionIdLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1ProgressCircleWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2ProgressCircleWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Wr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Wr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Wr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Circle
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Circle
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Circle
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnDisplayFields}
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplayCaster filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnDisplay} */
$xyz.swapee.wc.back.IProgressColumnDisplayCaster.prototype.asIProgressColumnDisplay
/** @type {!xyz.swapee.wc.back.BoundProgressColumnDisplay} */
$xyz.swapee.wc.back.IProgressColumnDisplayCaster.prototype.superProgressColumnDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnDisplayCaster}
 */
xyz.swapee.wc.back.IProgressColumnDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplay filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.ProgressColumnClasses, null>}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnDisplay}
 */
xyz.swapee.wc.back.IProgressColumnDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.ProgressColumnDisplay filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IProgressColumnDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnDisplay.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.ProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnDisplay)} */
xyz.swapee.wc.back.ProgressColumnDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.ProgressColumnDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.AbstractProgressColumnDisplay filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.ProgressColumnDisplay}
 */
$xyz.swapee.wc.back.AbstractProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnDisplay)} */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.RecordIProgressColumnDisplay filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/** @typedef {{ paint: xyz.swapee.wc.back.IProgressColumnDisplay.paint }} */
xyz.swapee.wc.back.RecordIProgressColumnDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.BoundIProgressColumnDisplay filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.ProgressColumnClasses, null>}
 */
$xyz.swapee.wc.back.BoundIProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnDisplay} */
xyz.swapee.wc.back.BoundIProgressColumnDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.BoundProgressColumnDisplay filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnDisplay} */
xyz.swapee.wc.back.BoundProgressColumnDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplay.paint filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ProgressColumnMemory=, null=): void} */
xyz.swapee.wc.back.IProgressColumnDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IProgressColumnDisplay, !xyz.swapee.wc.ProgressColumnMemory=, null=): void} */
xyz.swapee.wc.back.IProgressColumnDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IProgressColumnDisplay.__paint} */
xyz.swapee.wc.back.IProgressColumnDisplay.__paint

// nss:xyz.swapee.wc.back.IProgressColumnDisplay,$xyz.swapee.wc.back.IProgressColumnDisplay,xyz.swapee.wc.back
/* @typal-end */