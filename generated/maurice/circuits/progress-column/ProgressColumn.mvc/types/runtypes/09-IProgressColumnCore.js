/** @const {?} */ $xyz.swapee.wc.IProgressColumnCore
/** @const {?} */ $xyz.swapee.wc.IProgressColumnCore.Model
/** @const {?} */ xyz.swapee.wc.IProgressColumnCore
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Initialese filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Initialese} */
xyz.swapee.wc.IProgressColumnCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCoreFields filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @interface */
$xyz.swapee.wc.IProgressColumnCoreFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnCore.Model} */
$xyz.swapee.wc.IProgressColumnCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCoreFields}
 */
xyz.swapee.wc.IProgressColumnCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCoreCaster filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @interface */
$xyz.swapee.wc.IProgressColumnCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnCore} */
$xyz.swapee.wc.IProgressColumnCoreCaster.prototype.asIProgressColumnCore
/** @type {!xyz.swapee.wc.BoundProgressColumnCore} */
$xyz.swapee.wc.IProgressColumnCoreCaster.prototype.superProgressColumnCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCoreCaster}
 */
xyz.swapee.wc.IProgressColumnCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCoreCaster}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore}
 */
$xyz.swapee.wc.IProgressColumnCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnCore.prototype.resetProgressColumnCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCore}
 */
xyz.swapee.wc.IProgressColumnCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.ProgressColumnCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnCore.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.ProgressColumnCore
/** @type {function(new: xyz.swapee.wc.IProgressColumnCore)} */
xyz.swapee.wc.ProgressColumnCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.ProgressColumnCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.AbstractProgressColumnCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ProgressColumnCore}
 */
$xyz.swapee.wc.AbstractProgressColumnCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnCore)} */
xyz.swapee.wc.AbstractProgressColumnCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.RecordIProgressColumnCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {{ resetCore: xyz.swapee.wc.IProgressColumnCore.resetCore, resetProgressColumnCore: xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore }} */
xyz.swapee.wc.RecordIProgressColumnCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.BoundIProgressColumnCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCoreFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCoreCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnOuterCore}
 */
$xyz.swapee.wc.BoundIProgressColumnCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnCore} */
xyz.swapee.wc.BoundIProgressColumnCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.BoundProgressColumnCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnCore} */
xyz.swapee.wc.BoundProgressColumnCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.resetCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnCore): void} */
xyz.swapee.wc.IProgressColumnCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnCore.__resetCore} */
xyz.swapee.wc.IProgressColumnCore.__resetCore

// nss:xyz.swapee.wc.IProgressColumnCore,$xyz.swapee.wc.IProgressColumnCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnCore): void} */
xyz.swapee.wc.IProgressColumnCore._resetProgressColumnCore
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore} */
xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore

// nss:xyz.swapee.wc.IProgressColumnCore,$xyz.swapee.wc.IProgressColumnCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction.processingTransaction filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction.processingTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus.transactionStatus filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus.transactionStatus

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted.transactionCompleted filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted.transactionCompleted

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed.transactionFailed filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed.transactionFailed

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction.prototype.processingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction} */
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus.prototype.transactionStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted.prototype.transactionCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed.prototype.transactionFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed}
 */
$xyz.swapee.wc.IProgressColumnCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model} */
xyz.swapee.wc.IProgressColumnCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe.prototype.processingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe.prototype.transactionStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe.prototype.transactionCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe.prototype.transactionFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */