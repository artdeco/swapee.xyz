/** @const {?} */ $xyz.swapee.wc.IProgressColumnPort
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPortInterface
/** @const {?} */ xyz.swapee.wc.IProgressColumnPort
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.Initialese filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IProgressColumnPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Initialese} */
xyz.swapee.wc.IProgressColumnPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortFields filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @interface */
$xyz.swapee.wc.IProgressColumnPortFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnPort.Inputs} */
$xyz.swapee.wc.IProgressColumnPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IProgressColumnPort.Inputs} */
$xyz.swapee.wc.IProgressColumnPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPortFields}
 */
xyz.swapee.wc.IProgressColumnPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortCaster filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @interface */
$xyz.swapee.wc.IProgressColumnPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnPort} */
$xyz.swapee.wc.IProgressColumnPortCaster.prototype.asIProgressColumnPort
/** @type {!xyz.swapee.wc.BoundProgressColumnPort} */
$xyz.swapee.wc.IProgressColumnPortCaster.prototype.superProgressColumnPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPortCaster}
 */
xyz.swapee.wc.IProgressColumnPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IProgressColumnPort.Inputs>}
 */
$xyz.swapee.wc.IProgressColumnPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnPort.prototype.resetProgressColumnPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPort}
 */
xyz.swapee.wc.IProgressColumnPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.ProgressColumnPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnPort.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.ProgressColumnPort
/** @type {function(new: xyz.swapee.wc.IProgressColumnPort, ...!xyz.swapee.wc.IProgressColumnPort.Initialese)} */
xyz.swapee.wc.ProgressColumnPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.ProgressColumnPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.AbstractProgressColumnPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnPort}
 */
$xyz.swapee.wc.AbstractProgressColumnPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnPort)} */
xyz.swapee.wc.AbstractProgressColumnPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.ProgressColumnPortConstructor filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnPort, ...!xyz.swapee.wc.IProgressColumnPort.Initialese)} */
xyz.swapee.wc.ProgressColumnPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.RecordIProgressColumnPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @typedef {{ resetPort: xyz.swapee.wc.IProgressColumnPort.resetPort, resetProgressColumnPort: xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort }} */
xyz.swapee.wc.RecordIProgressColumnPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.BoundIProgressColumnPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnPortFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IProgressColumnPort.Inputs>}
 */
$xyz.swapee.wc.BoundIProgressColumnPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnPort} */
xyz.swapee.wc.BoundIProgressColumnPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.BoundProgressColumnPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnPort} */
xyz.swapee.wc.BoundProgressColumnPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.resetPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnPort): void} */
xyz.swapee.wc.IProgressColumnPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnPort.__resetPort} */
xyz.swapee.wc.IProgressColumnPort.__resetPort

// nss:xyz.swapee.wc.IProgressColumnPort,$xyz.swapee.wc.IProgressColumnPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnPort): void} */
xyz.swapee.wc.IProgressColumnPort._resetProgressColumnPort
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort} */
xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort

// nss:xyz.swapee.wc.IProgressColumnPort,$xyz.swapee.wc.IProgressColumnPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.Inputs filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IProgressColumnPort.Inputs}
 */
xyz.swapee.wc.IProgressColumnPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IProgressColumnPort.WeakInputs}
 */
xyz.swapee.wc.IProgressColumnPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortInterface filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @interface */
$xyz.swapee.wc.IProgressColumnPortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPortInterface}
 */
xyz.swapee.wc.IProgressColumnPortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.ProgressColumnPortInterface filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnPortInterface}
 */
$xyz.swapee.wc.ProgressColumnPortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnPortInterface}
 */
xyz.swapee.wc.ProgressColumnPortInterface
/** @type {function(new: xyz.swapee.wc.IProgressColumnPortInterface)} */
xyz.swapee.wc.ProgressColumnPortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortInterface.Props filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @record */
$xyz.swapee.wc.IProgressColumnPortInterface.Props = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.creatingTransaction
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.finishedOnPayment
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.paymentFailed
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.paymentCompleted
/** @type {string} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.transactionId
/** @type {string} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.paymentStatus
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.loadingStep1
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.loadingStep2
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.loadingStep3
/** @type {string} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnPortInterface.Props} */
xyz.swapee.wc.IProgressColumnPortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPortInterface
/* @typal-end */