/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.core
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.creatingTransaction
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.finishedOnPayment
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.paymentFailed
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.paymentCompleted
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.transactionId
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.paymentStatus
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.status
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnMemoryPQs}
 */
xyz.swapee.wc.ProgressColumnMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a74ad
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.i9fc6
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a7265
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a2236
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.e97c0
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.g7113
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.b5666
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.jacb4
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnMemoryQPs}
 */
xyz.swapee.wc.ProgressColumnMemoryQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnMemoryQPs)} */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ProgressColumnMemoryPQs}
 */
$xyz.swapee.wc.ProgressColumnInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnInputsPQs}
 */
xyz.swapee.wc.ProgressColumnInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ProgressColumnMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.ProgressColumnInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnInputsQPs}
 */
xyz.swapee.wc.ProgressColumnInputsQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnInputsQPs)} */
xyz.swapee.wc.ProgressColumnInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnCachePQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnCachePQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.processingTransaction
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.transactionStatus
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.transactionCompleted
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.transactionFailed
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnCachePQs}
 */
xyz.swapee.wc.ProgressColumnCachePQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnCacheQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnCacheQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.h121e
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.b274c
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.de5a0
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.f17c7
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnCacheQPs}
 */
xyz.swapee.wc.ProgressColumnCacheQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnCacheQPs)} */
xyz.swapee.wc.ProgressColumnCacheQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.TransactionIdWr
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1Circle
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step2Circle
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step3Circle
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step2La
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.TransactionIdLa
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.PaymentFinalStatusWr
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.PaymentFinalStatusLa
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step3Wr
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step2BotSep
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1La
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1ProgressCircleWr
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnVdusPQs}
 */
xyz.swapee.wc.ProgressColumnVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e1
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e2
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e3
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e4
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e5
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e6
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e7
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e8
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e9
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e10
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e11
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e12
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e13
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnVdusQPs}
 */
xyz.swapee.wc.ProgressColumnVdusQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnVdusQPs)} */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnClassesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnClassesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleLoading
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleComplete
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleWaiting
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleFailed
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.StepLabelFailed
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnClassesPQs}
 */
xyz.swapee.wc.ProgressColumnClassesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnClassesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnClassesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.fe522
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.a5ad9
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.j912b
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.dc9d7
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.he898
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnClassesQPs}
 */
xyz.swapee.wc.ProgressColumnClassesQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnClassesQPs)} */
xyz.swapee.wc.ProgressColumnClassesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */