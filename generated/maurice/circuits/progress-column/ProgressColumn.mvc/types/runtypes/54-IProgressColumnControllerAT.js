/** @const {?} */ $xyz.swapee.wc.front.IProgressColumnControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} */
xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IProgressColumnControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.IProgressColumnControllerATCaster filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/** @interface */
$xyz.swapee.wc.front.IProgressColumnControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIProgressColumnControllerAT} */
$xyz.swapee.wc.front.IProgressColumnControllerATCaster.prototype.asIProgressColumnControllerAT
/** @type {!xyz.swapee.wc.front.BoundProgressColumnControllerAT} */
$xyz.swapee.wc.front.IProgressColumnControllerATCaster.prototype.superProgressColumnControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnControllerATCaster}
 */
xyz.swapee.wc.front.IProgressColumnControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.IProgressColumnControllerAT filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IProgressColumnControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnControllerAT}
 */
xyz.swapee.wc.front.IProgressColumnControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.ProgressColumnControllerAT filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IProgressColumnControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.ProgressColumnControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.ProgressColumnControllerAT
/** @type {function(new: xyz.swapee.wc.front.IProgressColumnControllerAT, ...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese)} */
xyz.swapee.wc.front.ProgressColumnControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.ProgressColumnControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.AbstractProgressColumnControllerAT filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
$xyz.swapee.wc.front.AbstractProgressColumnControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractProgressColumnControllerAT)} */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.ProgressColumnControllerATConstructor filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnControllerAT, ...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese)} */
xyz.swapee.wc.front.ProgressColumnControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.RecordIProgressColumnControllerAT filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIProgressColumnControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.BoundIProgressColumnControllerAT filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIProgressColumnControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIProgressColumnControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIProgressColumnControllerAT} */
xyz.swapee.wc.front.BoundIProgressColumnControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.BoundProgressColumnControllerAT filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundProgressColumnControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundProgressColumnControllerAT} */
xyz.swapee.wc.front.BoundProgressColumnControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */