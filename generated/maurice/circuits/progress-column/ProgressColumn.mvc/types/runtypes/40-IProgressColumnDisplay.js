/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.Initialese filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings>}
 */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese = function() {}
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.KycBlock
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.KycRequired
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.KycNotRequired
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.ExchangeEmail
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.InfoNotice
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.SupportNotice
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.PaymentStatusWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.PaymentStatusLa
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionStatusWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionStatusLa
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2BotSep
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1Co
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2Co
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3Co
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1La
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2La
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3La
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionIdWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionIdLa
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1ProgressCircleWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2ProgressCircleWr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1Wr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2Wr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3Wr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1Circle
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2Circle
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3Circle
/** @typedef {$xyz.swapee.wc.IProgressColumnDisplay.Initialese} */
xyz.swapee.wc.IProgressColumnDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplayFields filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @interface */
$xyz.swapee.wc.IProgressColumnDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnDisplay.Settings} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IProgressColumnDisplay.Queries} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.queries
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycBlock
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycRequired
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycNotRequired
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.ExchangeEmail
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.InfoNotice
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.SupportNotice
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.PaymentStatusWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.PaymentStatusLa
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionStatusWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionStatusLa
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2BotSep
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Co
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Co
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Co
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1La
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2La
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3La
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionIdWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionIdLa
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1ProgressCircleWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2ProgressCircleWr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Wr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Wr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Wr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Circle
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Circle
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Circle
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDisplayFields}
 */
xyz.swapee.wc.IProgressColumnDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplayCaster filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @interface */
$xyz.swapee.wc.IProgressColumnDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnDisplay} */
$xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.asIProgressColumnDisplay
/** @type {!xyz.swapee.wc.BoundIProgressColumnScreen} */
$xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.asIProgressColumnScreen
/** @type {!xyz.swapee.wc.BoundProgressColumnDisplay} */
$xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.superProgressColumnDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDisplayCaster}
 */
xyz.swapee.wc.IProgressColumnDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.ProgressColumnMemory, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, xyz.swapee.wc.IProgressColumnDisplay.Queries, null>}
 */
$xyz.swapee.wc.IProgressColumnDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDisplay}
 */
xyz.swapee.wc.IProgressColumnDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.ProgressColumnDisplay filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnDisplay.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.ProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.IProgressColumnDisplay, ...!xyz.swapee.wc.IProgressColumnDisplay.Initialese)} */
xyz.swapee.wc.ProgressColumnDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.ProgressColumnDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.AbstractProgressColumnDisplay filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnDisplay}
 */
$xyz.swapee.wc.AbstractProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnDisplay)} */
xyz.swapee.wc.AbstractProgressColumnDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.ProgressColumnDisplayConstructor filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnDisplay, ...!xyz.swapee.wc.IProgressColumnDisplay.Initialese)} */
xyz.swapee.wc.ProgressColumnDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.RecordIProgressColumnDisplay filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @typedef {{ paint: xyz.swapee.wc.IProgressColumnDisplay.paint }} */
xyz.swapee.wc.RecordIProgressColumnDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.BoundIProgressColumnDisplay filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnDisplayFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.ProgressColumnMemory, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, xyz.swapee.wc.IProgressColumnDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnDisplay} */
xyz.swapee.wc.BoundIProgressColumnDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.BoundProgressColumnDisplay filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnDisplay} */
xyz.swapee.wc.BoundProgressColumnDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.paint filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ProgressColumnMemory, null): void} */
xyz.swapee.wc.IProgressColumnDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnDisplay, !xyz.swapee.wc.ProgressColumnMemory, null): void} */
xyz.swapee.wc.IProgressColumnDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnDisplay.__paint} */
xyz.swapee.wc.IProgressColumnDisplay.__paint

// nss:xyz.swapee.wc.IProgressColumnDisplay,$xyz.swapee.wc.IProgressColumnDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.Queries filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @record */
$xyz.swapee.wc.IProgressColumnDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnDisplay.Queries} */
xyz.swapee.wc.IProgressColumnDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.Settings filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnDisplay.Queries}
 */
$xyz.swapee.wc.IProgressColumnDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnDisplay.Settings} */
xyz.swapee.wc.IProgressColumnDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDisplay
/* @typal-end */