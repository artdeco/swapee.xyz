/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/100-ProgressColumnMemory.xml} xyz.swapee.wc.ProgressColumnMemory filter:!ControllerPlugin~props 0c1f8beb29433d5699e2027f2c36594b */
/** @record */
$xyz.swapee.wc.ProgressColumnMemory = __$te_Mixin()
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.creatingTransaction
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.finishedOnPayment
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.paymentFailed
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.paymentCompleted
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionId
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.paymentStatus
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep1
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep2
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep3
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.status
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.processingTransaction
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionStatus
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionCompleted
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionFailed
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ProgressColumnMemory}
 */
xyz.swapee.wc.ProgressColumnMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */