/** @const {?} */ $xyz.swapee.wc.IProgressColumnComputer
/** @const {?} */ $xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/** @const {?} */ $xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/** @const {?} */ xyz.swapee.wc.IProgressColumnComputer
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.Initialese filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IProgressColumnComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.Initialese} */
xyz.swapee.wc.IProgressColumnComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputerCaster filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/** @interface */
$xyz.swapee.wc.IProgressColumnComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnComputer} */
$xyz.swapee.wc.IProgressColumnComputerCaster.prototype.asIProgressColumnComputer
/** @type {!xyz.swapee.wc.BoundProgressColumnComputer} */
$xyz.swapee.wc.IProgressColumnComputerCaster.prototype.superProgressColumnComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnComputerCaster}
 */
xyz.swapee.wc.IProgressColumnComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.IProgressColumnComputer = function() {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.prototype.adaptTransactionStatus = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.prototype.adaptProcessingTransaction = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.ProgressColumnMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnComputer}
 */
xyz.swapee.wc.IProgressColumnComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.ProgressColumnComputer filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnComputer.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.ProgressColumnComputer
/** @type {function(new: xyz.swapee.wc.IProgressColumnComputer, ...!xyz.swapee.wc.IProgressColumnComputer.Initialese)} */
xyz.swapee.wc.ProgressColumnComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.ProgressColumnComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.AbstractProgressColumnComputer filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnComputer}
 */
$xyz.swapee.wc.AbstractProgressColumnComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnComputer)} */
xyz.swapee.wc.AbstractProgressColumnComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.ProgressColumnComputerConstructor filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnComputer, ...!xyz.swapee.wc.IProgressColumnComputer.Initialese)} */
xyz.swapee.wc.ProgressColumnComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.RecordIProgressColumnComputer filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/** @typedef {{ adaptTransactionStatus: xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus, adaptProcessingTransaction: xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction, compute: xyz.swapee.wc.IProgressColumnComputer.compute }} */
xyz.swapee.wc.RecordIProgressColumnComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.BoundIProgressColumnComputer filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundIProgressColumnComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnComputer} */
xyz.swapee.wc.BoundIProgressColumnComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.BoundProgressColumnComputer filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnComputer} */
xyz.swapee.wc.BoundProgressColumnComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 * @this {xyz.swapee.wc.IProgressColumnComputer}
 */
$xyz.swapee.wc.IProgressColumnComputer._adaptTransactionStatus = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus} */
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer._adaptTransactionStatus} */
xyz.swapee.wc.IProgressColumnComputer._adaptTransactionStatus
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus} */
xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus

// nss:xyz.swapee.wc.IProgressColumnComputer,$xyz.swapee.wc.IProgressColumnComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 * @this {xyz.swapee.wc.IProgressColumnComputer}
 */
$xyz.swapee.wc.IProgressColumnComputer._adaptProcessingTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction} */
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer._adaptProcessingTransaction} */
xyz.swapee.wc.IProgressColumnComputer._adaptProcessingTransaction
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction} */
xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction

// nss:xyz.swapee.wc.IProgressColumnComputer,$xyz.swapee.wc.IProgressColumnComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.compute filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.ProgressColumnMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.ProgressColumnMemory): void} */
xyz.swapee.wc.IProgressColumnComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnComputer, xyz.swapee.wc.ProgressColumnMemory): void} */
xyz.swapee.wc.IProgressColumnComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.__compute} */
xyz.swapee.wc.IProgressColumnComputer.__compute

// nss:xyz.swapee.wc.IProgressColumnComputer,$xyz.swapee.wc.IProgressColumnComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} */
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return} */
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} */
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return} */
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/* @typal-end */