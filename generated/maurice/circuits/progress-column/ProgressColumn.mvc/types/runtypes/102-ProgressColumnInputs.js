/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/102-ProgressColumnInputs.xml} xyz.swapee.wc.front.ProgressColumnInputs filter:!ControllerPlugin~props 2419226a56c9a63508dfed6916ac6a1b */
/** @record */
$xyz.swapee.wc.front.ProgressColumnInputs = __$te_Mixin()
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.creatingTransaction
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.finishedOnPayment
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentFailed
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentCompleted
/** @type {string|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.transactionId
/** @type {string|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentStatus
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep1
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep2
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep3
/** @type {string|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.status
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.ProgressColumnInputs}
 */
xyz.swapee.wc.front.ProgressColumnInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */