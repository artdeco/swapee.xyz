/** @const {?} */ $xyz.swapee.wc.IProgressColumnController
/** @const {?} */ xyz.swapee.wc.IProgressColumnController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.Initialese filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IProgressColumnController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnController.Initialese} */
xyz.swapee.wc.IProgressColumnController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnControllerFields filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @interface */
$xyz.swapee.wc.IProgressColumnControllerFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnController.Inputs} */
$xyz.swapee.wc.IProgressColumnControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnControllerFields}
 */
xyz.swapee.wc.IProgressColumnControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnControllerCaster filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @interface */
$xyz.swapee.wc.IProgressColumnControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnController} */
$xyz.swapee.wc.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/** @type {!xyz.swapee.wc.BoundIProgressColumnProcessor} */
$xyz.swapee.wc.IProgressColumnControllerCaster.prototype.asIProgressColumnProcessor
/** @type {!xyz.swapee.wc.BoundProgressColumnController} */
$xyz.swapee.wc.IProgressColumnControllerCaster.prototype.superProgressColumnController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnControllerCaster}
 */
xyz.swapee.wc.IProgressColumnControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.IProgressColumnController = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnController.prototype.resetPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnController}
 */
xyz.swapee.wc.IProgressColumnController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.ProgressColumnController filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnController.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.ProgressColumnController
/** @type {function(new: xyz.swapee.wc.IProgressColumnController, ...!xyz.swapee.wc.IProgressColumnController.Initialese)} */
xyz.swapee.wc.ProgressColumnController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.ProgressColumnController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.AbstractProgressColumnController filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnController}
 */
$xyz.swapee.wc.AbstractProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnController)} */
xyz.swapee.wc.AbstractProgressColumnController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.ProgressColumnControllerConstructor filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnController, ...!xyz.swapee.wc.IProgressColumnController.Initialese)} */
xyz.swapee.wc.ProgressColumnControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.RecordIProgressColumnController filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @typedef {{ resetPort: xyz.swapee.wc.IProgressColumnController.resetPort }} */
xyz.swapee.wc.RecordIProgressColumnController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.BoundIProgressColumnController filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnControllerFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.BoundIProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnController} */
xyz.swapee.wc.BoundIProgressColumnController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.BoundProgressColumnController filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnController} */
xyz.swapee.wc.BoundProgressColumnController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.resetPort filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnController): void} */
xyz.swapee.wc.IProgressColumnController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnController.__resetPort} */
xyz.swapee.wc.IProgressColumnController.__resetPort

// nss:xyz.swapee.wc.IProgressColumnController,$xyz.swapee.wc.IProgressColumnController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.Inputs filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnPort.Inputs}
 */
$xyz.swapee.wc.IProgressColumnController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnController.Inputs} */
xyz.swapee.wc.IProgressColumnController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.WeakInputs filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnPort.WeakInputs}
 */
$xyz.swapee.wc.IProgressColumnController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnController.WeakInputs} */
xyz.swapee.wc.IProgressColumnController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnController
/* @typal-end */