/** @const {?} */ $xyz.swapee.wc.front.IProgressColumnScreenAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnScreen.Initialese}
 */
$xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} */
xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IProgressColumnScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.IProgressColumnScreenARCaster filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/** @interface */
$xyz.swapee.wc.front.IProgressColumnScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIProgressColumnScreenAR} */
$xyz.swapee.wc.front.IProgressColumnScreenARCaster.prototype.asIProgressColumnScreenAR
/** @type {!xyz.swapee.wc.front.BoundProgressColumnScreenAR} */
$xyz.swapee.wc.front.IProgressColumnScreenARCaster.prototype.superProgressColumnScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnScreenARCaster}
 */
xyz.swapee.wc.front.IProgressColumnScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.IProgressColumnScreenAR filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IProgressColumnScreen}
 */
$xyz.swapee.wc.front.IProgressColumnScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnScreenAR}
 */
xyz.swapee.wc.front.IProgressColumnScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.ProgressColumnScreenAR filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IProgressColumnScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.ProgressColumnScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.ProgressColumnScreenAR
/** @type {function(new: xyz.swapee.wc.front.IProgressColumnScreenAR, ...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese)} */
xyz.swapee.wc.front.ProgressColumnScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.ProgressColumnScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.AbstractProgressColumnScreenAR filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
$xyz.swapee.wc.front.AbstractProgressColumnScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractProgressColumnScreenAR)} */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.ProgressColumnScreenARConstructor filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnScreenAR, ...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese)} */
xyz.swapee.wc.front.ProgressColumnScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.RecordIProgressColumnScreenAR filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIProgressColumnScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.BoundIProgressColumnScreenAR filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIProgressColumnScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIProgressColumnScreen}
 */
$xyz.swapee.wc.front.BoundIProgressColumnScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIProgressColumnScreenAR} */
xyz.swapee.wc.front.BoundIProgressColumnScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.BoundProgressColumnScreenAR filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundProgressColumnScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundProgressColumnScreenAR} */
xyz.swapee.wc.front.BoundProgressColumnScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */