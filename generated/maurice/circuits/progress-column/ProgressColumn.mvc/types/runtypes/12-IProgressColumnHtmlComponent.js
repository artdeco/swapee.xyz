/** @const {?} */ $xyz.swapee.wc.IProgressColumnHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnController.Initialese}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreen.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumn.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnComputer.Initialese}
 */
$xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} */
xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.IProgressColumnHtmlComponentCaster filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/** @interface */
$xyz.swapee.wc.IProgressColumnHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnHtmlComponent} */
$xyz.swapee.wc.IProgressColumnHtmlComponentCaster.prototype.asIProgressColumnHtmlComponent
/** @type {!xyz.swapee.wc.BoundProgressColumnHtmlComponent} */
$xyz.swapee.wc.IProgressColumnHtmlComponentCaster.prototype.superProgressColumnHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnHtmlComponentCaster}
 */
xyz.swapee.wc.IProgressColumnHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.IProgressColumnHtmlComponent filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IProgressColumnController}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreen}
 * @extends {xyz.swapee.wc.IProgressColumn}
 * @extends {xyz.swapee.wc.IProgressColumnGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor}
 * @extends {xyz.swapee.wc.IProgressColumnComputer}
 */
$xyz.swapee.wc.IProgressColumnHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnHtmlComponent}
 */
xyz.swapee.wc.IProgressColumnHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.ProgressColumnHtmlComponent filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.ProgressColumnHtmlComponent
/** @type {function(new: xyz.swapee.wc.IProgressColumnHtmlComponent, ...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese)} */
xyz.swapee.wc.ProgressColumnHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.ProgressColumnHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.AbstractProgressColumnHtmlComponent filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
$xyz.swapee.wc.AbstractProgressColumnHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnHtmlComponent)} */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.ProgressColumnHtmlComponentConstructor filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnHtmlComponent, ...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese)} */
xyz.swapee.wc.ProgressColumnHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.RecordIProgressColumnHtmlComponent filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.BoundIProgressColumnHtmlComponent filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnController}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreen}
 * @extends {xyz.swapee.wc.BoundIProgressColumn}
 * @extends {xyz.swapee.wc.BoundIProgressColumnGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundIProgressColumnProcessor}
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 */
$xyz.swapee.wc.BoundIProgressColumnHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnHtmlComponent} */
xyz.swapee.wc.BoundIProgressColumnHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.BoundProgressColumnHtmlComponent filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnHtmlComponent} */
xyz.swapee.wc.BoundProgressColumnHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */