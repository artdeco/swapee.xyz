/** @const {?} */ $xyz.swapee.wc.IProgressColumnDesigner
/** @const {?} */ $xyz.swapee.wc.IProgressColumnDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IProgressColumnDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @interface */
$xyz.swapee.wc.IProgressColumnDesigner = function() {}
/**
 * @param {xyz.swapee.wc.ProgressColumnClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.ProgressColumnClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.ProgressColumnClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDesigner}
 */
xyz.swapee.wc.IProgressColumnDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.ProgressColumnDesigner filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnDesigner}
 */
$xyz.swapee.wc.ProgressColumnDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnDesigner}
 */
xyz.swapee.wc.ProgressColumnDesigner
/** @type {function(new: xyz.swapee.wc.IProgressColumnDesigner)} */
xyz.swapee.wc.ProgressColumnDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @record */
$xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IProgressColumnController} */
$xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh.prototype.ProgressColumn
/** @typedef {$xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh} */
xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @record */
$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IProgressColumnController} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh.prototype.ProgressColumn
/** @type {typeof xyz.swapee.wc.IProgressColumnController} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh} */
xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @record */
$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.ProgressColumnMemory} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool.prototype.ProgressColumn
/** @type {!xyz.swapee.wc.ProgressColumnMemory} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool} */
xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDesigner.relay
/* @typal-end */