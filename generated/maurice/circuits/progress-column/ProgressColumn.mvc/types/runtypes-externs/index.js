/** @const {?} */ $xyz.swapee.wc.IProgressColumnComputer
/** @const {?} */ $xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/** @const {?} */ $xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/** @const {?} */ $xyz.swapee.wc.IProgressColumnOuterCore
/** @const {?} */ $xyz.swapee.wc.IProgressColumnOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IProgressColumnCore
/** @const {?} */ $xyz.swapee.wc.IProgressColumnCore.Model
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPort
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IProgressColumnPortInterface
/** @const {?} */ $xyz.swapee.wc.IProgressColumnController
/** @const {?} */ $xyz.swapee.wc.IProgressColumnProcessor
/** @const {?} */ $xyz.swapee.wc.IProgressColumn
/** @const {?} */ $xyz.swapee.wc.IProgressColumnGPU
/** @const {?} */ $xyz.swapee.wc.IProgressColumnHtmlComponent
/** @const {?} */ $xyz.swapee.wc.IProgressColumnDesigner
/** @const {?} */ $xyz.swapee.wc.IProgressColumnDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IProgressColumnDesigner.relay
/** @const {?} */ $xyz.swapee.wc.front.IProgressColumnController
/** @const {?} */ $xyz.swapee.wc.front.IProgressColumnControllerAT
/** @const {?} */ $xyz.swapee.wc.front.IProgressColumnScreenAR
/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnController
/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnControllerAR
/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnScreenAT
/** @const {?} */ $xyz.swapee.wc.back.IProgressColumnScreen
/** @const {?} */ $xyz.swapee.wc.IProgressColumnScreen
/** @const {?} */ xyz.swapee.wc.IProgressColumnComputer
/** @const {?} */ xyz.swapee.wc.IProgressColumnPort
/** @const {?} */ xyz.swapee.wc.IProgressColumnCore
/** @const {?} */ xyz.swapee.wc.IProgressColumnController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IProgressColumnComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.Initialese} */
xyz.swapee.wc.IProgressColumnComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/** @interface */
$xyz.swapee.wc.IProgressColumnComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnComputer} */
$xyz.swapee.wc.IProgressColumnComputerCaster.prototype.asIProgressColumnComputer
/** @type {!xyz.swapee.wc.BoundProgressColumnComputer} */
$xyz.swapee.wc.IProgressColumnComputerCaster.prototype.superProgressColumnComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnComputerCaster}
 */
xyz.swapee.wc.IProgressColumnComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.IProgressColumnComputer = function() {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.prototype.adaptTransactionStatus = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.prototype.adaptProcessingTransaction = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.ProgressColumnMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnComputer}
 */
xyz.swapee.wc.IProgressColumnComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.ProgressColumnComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnComputer.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.ProgressColumnComputer
/** @type {function(new: xyz.swapee.wc.IProgressColumnComputer, ...!xyz.swapee.wc.IProgressColumnComputer.Initialese)} */
xyz.swapee.wc.ProgressColumnComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.ProgressColumnComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.AbstractProgressColumnComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnComputer}
 */
$xyz.swapee.wc.AbstractProgressColumnComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnComputer)} */
xyz.swapee.wc.AbstractProgressColumnComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.ProgressColumnComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnComputer, ...!xyz.swapee.wc.IProgressColumnComputer.Initialese)} */
xyz.swapee.wc.ProgressColumnComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.RecordIProgressColumnComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/** @typedef {{ adaptTransactionStatus: xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus, adaptProcessingTransaction: xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction, compute: xyz.swapee.wc.IProgressColumnComputer.compute }} */
xyz.swapee.wc.RecordIProgressColumnComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.BoundIProgressColumnComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundIProgressColumnComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnComputer} */
xyz.swapee.wc.BoundIProgressColumnComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.BoundProgressColumnComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnComputer} */
xyz.swapee.wc.BoundProgressColumnComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 * @this {xyz.swapee.wc.IProgressColumnComputer}
 */
$xyz.swapee.wc.IProgressColumnComputer._adaptTransactionStatus = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus} */
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer._adaptTransactionStatus} */
xyz.swapee.wc.IProgressColumnComputer._adaptTransactionStatus
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus} */
xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus

// nss:xyz.swapee.wc.IProgressColumnComputer,$xyz.swapee.wc.IProgressColumnComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 * @this {xyz.swapee.wc.IProgressColumnComputer}
 */
$xyz.swapee.wc.IProgressColumnComputer._adaptProcessingTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction} */
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer._adaptProcessingTransaction} */
xyz.swapee.wc.IProgressColumnComputer._adaptProcessingTransaction
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction} */
xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction

// nss:xyz.swapee.wc.IProgressColumnComputer,$xyz.swapee.wc.IProgressColumnComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.ProgressColumnMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.ProgressColumnMemory): void} */
xyz.swapee.wc.IProgressColumnComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnComputer, xyz.swapee.wc.ProgressColumnMemory): void} */
xyz.swapee.wc.IProgressColumnComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnComputer.__compute} */
xyz.swapee.wc.IProgressColumnComputer.__compute

// nss:xyz.swapee.wc.IProgressColumnComputer,$xyz.swapee.wc.IProgressColumnComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} */
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus.prototype.transactionStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return} */
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe.prototype.transactionStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} */
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction.prototype.processingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction} */
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d71faa9b93801bdc524d14a9f00ba3f5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction}
 */
$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return} */
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Initialese} */
xyz.swapee.wc.IProgressColumnOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @interface */
$xyz.swapee.wc.IProgressColumnOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnOuterCore.Model} */
$xyz.swapee.wc.IProgressColumnOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnOuterCoreFields}
 */
xyz.swapee.wc.IProgressColumnOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @interface */
$xyz.swapee.wc.IProgressColumnOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnOuterCore} */
$xyz.swapee.wc.IProgressColumnOuterCoreCaster.prototype.asIProgressColumnOuterCore
/** @type {!xyz.swapee.wc.BoundProgressColumnOuterCore} */
$xyz.swapee.wc.IProgressColumnOuterCoreCaster.prototype.superProgressColumnOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnOuterCoreCaster}
 */
xyz.swapee.wc.IProgressColumnOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreCaster}
 */
$xyz.swapee.wc.IProgressColumnOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnOuterCore}
 */
xyz.swapee.wc.IProgressColumnOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.ProgressColumnOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnOuterCore.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.ProgressColumnOuterCore
/** @type {function(new: xyz.swapee.wc.IProgressColumnOuterCore)} */
xyz.swapee.wc.ProgressColumnOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.ProgressColumnOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.AbstractProgressColumnOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ProgressColumnOuterCore}
 */
$xyz.swapee.wc.AbstractProgressColumnOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnOuterCore)} */
xyz.swapee.wc.AbstractProgressColumnOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.core
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.creatingTransaction
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.finishedOnPayment
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.paymentFailed
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.paymentCompleted
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.transactionId
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.paymentStatus
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.status
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnMemoryPQs}
 */
xyz.swapee.wc.ProgressColumnMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a74ad
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.i9fc6
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a7265
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a2236
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.e97c0
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.g7113
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.b5666
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.jacb4
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnMemoryQPs}
 */
xyz.swapee.wc.ProgressColumnMemoryQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnMemoryQPs)} */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.RecordIProgressColumnOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.BoundIProgressColumnOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIProgressColumnOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnOuterCore} */
xyz.swapee.wc.BoundIProgressColumnOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.BoundProgressColumnOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnOuterCore} */
xyz.swapee.wc.BoundProgressColumnOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction.creatingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction.creatingTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment.finishedOnPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment.finishedOnPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed.paymentFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed.paymentFailed

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted.paymentCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted.paymentCompleted

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId.transactionId exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId.transactionId

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus.paymentStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus.paymentStatus

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1.loadingStep1 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1.loadingStep1

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2.loadingStep2 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2.loadingStep2

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3.loadingStep3 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3.loadingStep3

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status.status exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status.status

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1 = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2 = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3 = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.Status} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status}
 */
$xyz.swapee.wc.IProgressColumnOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model} */
xyz.swapee.wc.IProgressColumnOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1 = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2 = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3 = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status}
 */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IProgressColumnPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Initialese} */
xyz.swapee.wc.IProgressColumnPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @interface */
$xyz.swapee.wc.IProgressColumnPortFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnPort.Inputs} */
$xyz.swapee.wc.IProgressColumnPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IProgressColumnPort.Inputs} */
$xyz.swapee.wc.IProgressColumnPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPortFields}
 */
xyz.swapee.wc.IProgressColumnPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @interface */
$xyz.swapee.wc.IProgressColumnPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnPort} */
$xyz.swapee.wc.IProgressColumnPortCaster.prototype.asIProgressColumnPort
/** @type {!xyz.swapee.wc.BoundProgressColumnPort} */
$xyz.swapee.wc.IProgressColumnPortCaster.prototype.superProgressColumnPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPortCaster}
 */
xyz.swapee.wc.IProgressColumnPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IProgressColumnPort.Inputs>}
 */
$xyz.swapee.wc.IProgressColumnPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnPort.prototype.resetProgressColumnPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPort}
 */
xyz.swapee.wc.IProgressColumnPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.ProgressColumnPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnPort.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.ProgressColumnPort
/** @type {function(new: xyz.swapee.wc.IProgressColumnPort, ...!xyz.swapee.wc.IProgressColumnPort.Initialese)} */
xyz.swapee.wc.ProgressColumnPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.ProgressColumnPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.AbstractProgressColumnPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnPort}
 */
$xyz.swapee.wc.AbstractProgressColumnPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnPort)} */
xyz.swapee.wc.AbstractProgressColumnPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.ProgressColumnPortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnPort, ...!xyz.swapee.wc.IProgressColumnPort.Initialese)} */
xyz.swapee.wc.ProgressColumnPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ProgressColumnMemoryPQs}
 */
$xyz.swapee.wc.ProgressColumnInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnInputsPQs}
 */
xyz.swapee.wc.ProgressColumnInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ProgressColumnMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.ProgressColumnInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnInputsQPs}
 */
xyz.swapee.wc.ProgressColumnInputsQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnInputsQPs)} */
xyz.swapee.wc.ProgressColumnInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.RecordIProgressColumnPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @typedef {{ resetPort: xyz.swapee.wc.IProgressColumnPort.resetPort, resetProgressColumnPort: xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort }} */
xyz.swapee.wc.RecordIProgressColumnPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.BoundIProgressColumnPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnPortFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IProgressColumnPort.Inputs>}
 */
$xyz.swapee.wc.BoundIProgressColumnPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnPort} */
xyz.swapee.wc.BoundIProgressColumnPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.BoundProgressColumnPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnPort} */
xyz.swapee.wc.BoundProgressColumnPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnPort): void} */
xyz.swapee.wc.IProgressColumnPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnPort.__resetPort} */
xyz.swapee.wc.IProgressColumnPort.__resetPort

// nss:xyz.swapee.wc.IProgressColumnPort,$xyz.swapee.wc.IProgressColumnPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnPort): void} */
xyz.swapee.wc.IProgressColumnPort._resetProgressColumnPort
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort} */
xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort

// nss:xyz.swapee.wc.IProgressColumnPort,$xyz.swapee.wc.IProgressColumnPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IProgressColumnPort.Inputs}
 */
xyz.swapee.wc.IProgressColumnPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IProgressColumnPort.WeakInputs}
 */
xyz.swapee.wc.IProgressColumnPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @interface */
$xyz.swapee.wc.IProgressColumnPortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnPortInterface}
 */
xyz.swapee.wc.IProgressColumnPortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.ProgressColumnPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnPortInterface}
 */
$xyz.swapee.wc.ProgressColumnPortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnPortInterface}
 */
xyz.swapee.wc.ProgressColumnPortInterface
/** @type {function(new: xyz.swapee.wc.IProgressColumnPortInterface)} */
xyz.swapee.wc.ProgressColumnPortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml} xyz.swapee.wc.IProgressColumnPortInterface.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4ec7822e1d7290cdd78c1a15430ba01f */
/** @record */
$xyz.swapee.wc.IProgressColumnPortInterface.Props = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.creatingTransaction
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.finishedOnPayment
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.paymentFailed
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.paymentCompleted
/** @type {string} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.transactionId
/** @type {string} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.paymentStatus
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.loadingStep1
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.loadingStep2
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.loadingStep3
/** @type {string} */
$xyz.swapee.wc.IProgressColumnPortInterface.Props.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnPortInterface.Props} */
xyz.swapee.wc.IProgressColumnPortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPortInterface
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Initialese} */
xyz.swapee.wc.IProgressColumnCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @interface */
$xyz.swapee.wc.IProgressColumnCoreFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnCore.Model} */
$xyz.swapee.wc.IProgressColumnCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCoreFields}
 */
xyz.swapee.wc.IProgressColumnCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @interface */
$xyz.swapee.wc.IProgressColumnCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnCore} */
$xyz.swapee.wc.IProgressColumnCoreCaster.prototype.asIProgressColumnCore
/** @type {!xyz.swapee.wc.BoundProgressColumnCore} */
$xyz.swapee.wc.IProgressColumnCoreCaster.prototype.superProgressColumnCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCoreCaster}
 */
xyz.swapee.wc.IProgressColumnCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCoreCaster}
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore}
 */
$xyz.swapee.wc.IProgressColumnCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnCore.prototype.resetProgressColumnCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCore}
 */
xyz.swapee.wc.IProgressColumnCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.ProgressColumnCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnCore.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.ProgressColumnCore
/** @type {function(new: xyz.swapee.wc.IProgressColumnCore)} */
xyz.swapee.wc.ProgressColumnCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.ProgressColumnCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.AbstractProgressColumnCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ProgressColumnCore}
 */
$xyz.swapee.wc.AbstractProgressColumnCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnCore)} */
xyz.swapee.wc.AbstractProgressColumnCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnCachePQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnCachePQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.processingTransaction
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.transactionStatus
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.transactionCompleted
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCachePQs.prototype.transactionFailed
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnCachePQs}
 */
xyz.swapee.wc.ProgressColumnCachePQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnCacheQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnCacheQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.h121e
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.b274c
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.de5a0
/** @type {string} */
$xyz.swapee.wc.ProgressColumnCacheQPs.prototype.f17c7
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnCacheQPs}
 */
xyz.swapee.wc.ProgressColumnCacheQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnCacheQPs)} */
xyz.swapee.wc.ProgressColumnCacheQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.RecordIProgressColumnCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {{ resetCore: xyz.swapee.wc.IProgressColumnCore.resetCore, resetProgressColumnCore: xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore }} */
xyz.swapee.wc.RecordIProgressColumnCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.BoundIProgressColumnCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnCoreFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCoreCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnOuterCore}
 */
$xyz.swapee.wc.BoundIProgressColumnCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnCore} */
xyz.swapee.wc.BoundIProgressColumnCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.BoundProgressColumnCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnCore} */
xyz.swapee.wc.BoundProgressColumnCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnCore): void} */
xyz.swapee.wc.IProgressColumnCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnCore.__resetCore} */
xyz.swapee.wc.IProgressColumnCore.__resetCore

// nss:xyz.swapee.wc.IProgressColumnCore,$xyz.swapee.wc.IProgressColumnCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnCore): void} */
xyz.swapee.wc.IProgressColumnCore._resetProgressColumnCore
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore} */
xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore

// nss:xyz.swapee.wc.IProgressColumnCore,$xyz.swapee.wc.IProgressColumnCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction.processingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction.processingTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus.transactionStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {string} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus.transactionStatus

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted.transactionCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted.transactionCompleted

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed.transactionFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed.transactionFailed

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted.prototype.transactionCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed.prototype.transactionFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted}
 * @extends {xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed}
 */
$xyz.swapee.wc.IProgressColumnCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model} */
xyz.swapee.wc.IProgressColumnCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IProgressColumnController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnController.Initialese} */
xyz.swapee.wc.IProgressColumnController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.IProgressColumnProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnComputer.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.IProgressColumnProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnProcessor.Initialese} */
xyz.swapee.wc.IProgressColumnProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.IProgressColumnProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/** @interface */
$xyz.swapee.wc.IProgressColumnProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnProcessor} */
$xyz.swapee.wc.IProgressColumnProcessorCaster.prototype.asIProgressColumnProcessor
/** @type {!xyz.swapee.wc.BoundProgressColumnProcessor} */
$xyz.swapee.wc.IProgressColumnProcessorCaster.prototype.superProgressColumnProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnProcessorCaster}
 */
xyz.swapee.wc.IProgressColumnProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @interface */
$xyz.swapee.wc.IProgressColumnControllerFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnController.Inputs} */
$xyz.swapee.wc.IProgressColumnControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnControllerFields}
 */
xyz.swapee.wc.IProgressColumnControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @interface */
$xyz.swapee.wc.IProgressColumnControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnController} */
$xyz.swapee.wc.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/** @type {!xyz.swapee.wc.BoundIProgressColumnProcessor} */
$xyz.swapee.wc.IProgressColumnControllerCaster.prototype.asIProgressColumnProcessor
/** @type {!xyz.swapee.wc.BoundProgressColumnController} */
$xyz.swapee.wc.IProgressColumnControllerCaster.prototype.superProgressColumnController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnControllerCaster}
 */
xyz.swapee.wc.IProgressColumnControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.IProgressColumnController = function() {}
/** @return {void} */
$xyz.swapee.wc.IProgressColumnController.prototype.resetPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnController}
 */
xyz.swapee.wc.IProgressColumnController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.IProgressColumnProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnProcessorCaster}
 * @extends {xyz.swapee.wc.IProgressColumnComputer}
 * @extends {xyz.swapee.wc.IProgressColumnCore}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 */
$xyz.swapee.wc.IProgressColumnProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnProcessor}
 */
xyz.swapee.wc.IProgressColumnProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.ProgressColumnProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnProcessor.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.ProgressColumnProcessor
/** @type {function(new: xyz.swapee.wc.IProgressColumnProcessor, ...!xyz.swapee.wc.IProgressColumnProcessor.Initialese)} */
xyz.swapee.wc.ProgressColumnProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.ProgressColumnProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.AbstractProgressColumnProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnProcessor}
 */
$xyz.swapee.wc.AbstractProgressColumnProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnProcessor)} */
xyz.swapee.wc.AbstractProgressColumnProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.ProgressColumnProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnProcessor, ...!xyz.swapee.wc.IProgressColumnProcessor.Initialese)} */
xyz.swapee.wc.ProgressColumnProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.RecordIProgressColumnProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.RecordIProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @typedef {{ resetPort: xyz.swapee.wc.IProgressColumnController.resetPort }} */
xyz.swapee.wc.RecordIProgressColumnController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.BoundIProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnControllerFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.ProgressColumnMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.BoundIProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnController} */
xyz.swapee.wc.BoundIProgressColumnController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.BoundIProgressColumnProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 * @extends {xyz.swapee.wc.BoundIProgressColumnCore}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 */
$xyz.swapee.wc.BoundIProgressColumnProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnProcessor} */
xyz.swapee.wc.BoundIProgressColumnProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml} xyz.swapee.wc.BoundProgressColumnProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8723c25e23481a44687ce599ad817242 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnProcessor} */
xyz.swapee.wc.BoundProgressColumnProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/100-ProgressColumnMemory.xml} xyz.swapee.wc.ProgressColumnMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0c1f8beb29433d5699e2027f2c36594b */
/** @record */
$xyz.swapee.wc.ProgressColumnMemory = __$te_Mixin()
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.creatingTransaction
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.finishedOnPayment
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.paymentFailed
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.paymentCompleted
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionId
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.paymentStatus
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep1
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep2
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep3
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.status
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.processingTransaction
/** @type {string} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionStatus
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionCompleted
/** @type {boolean} */
$xyz.swapee.wc.ProgressColumnMemory.prototype.transactionFailed
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ProgressColumnMemory}
 */
xyz.swapee.wc.ProgressColumnMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/102-ProgressColumnInputs.xml} xyz.swapee.wc.front.ProgressColumnInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2419226a56c9a63508dfed6916ac6a1b */
/** @record */
$xyz.swapee.wc.front.ProgressColumnInputs = __$te_Mixin()
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.creatingTransaction
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.finishedOnPayment
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentFailed
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentCompleted
/** @type {string|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.transactionId
/** @type {string|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentStatus
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep1
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep2
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep3
/** @type {string|undefined} */
$xyz.swapee.wc.front.ProgressColumnInputs.prototype.status
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.ProgressColumnInputs}
 */
xyz.swapee.wc.front.ProgressColumnInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumnEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @record */
$xyz.swapee.wc.ProgressColumnEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IProgressColumn} */
$xyz.swapee.wc.ProgressColumnEnv.prototype.progressColumn
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ProgressColumnEnv}
 */
xyz.swapee.wc.ProgressColumnEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnComputer.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.IProgressColumn.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumn.Initialese} */
xyz.swapee.wc.IProgressColumn.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumn
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumnFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @interface */
$xyz.swapee.wc.IProgressColumnFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumn.Pinout} */
$xyz.swapee.wc.IProgressColumnFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnFields}
 */
xyz.swapee.wc.IProgressColumnFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumnCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @interface */
$xyz.swapee.wc.IProgressColumnCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumn} */
$xyz.swapee.wc.IProgressColumnCaster.prototype.asIProgressColumn
/** @type {!xyz.swapee.wc.BoundProgressColumn} */
$xyz.swapee.wc.IProgressColumnCaster.prototype.superProgressColumn
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnCaster}
 */
xyz.swapee.wc.IProgressColumnCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCaster}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor}
 * @extends {xyz.swapee.wc.IProgressColumnComputer}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, null>}
 */
$xyz.swapee.wc.IProgressColumn = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumn}
 */
xyz.swapee.wc.IProgressColumn

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumn}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumn.Initialese>}
 */
$xyz.swapee.wc.ProgressColumn = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.ProgressColumn
/** @type {function(new: xyz.swapee.wc.IProgressColumn, ...!xyz.swapee.wc.IProgressColumn.Initialese)} */
xyz.swapee.wc.ProgressColumn.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.ProgressColumn.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.AbstractProgressColumn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumn}
 */
$xyz.swapee.wc.AbstractProgressColumn = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumn)} */
xyz.swapee.wc.AbstractProgressColumn.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumn.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumnConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumn, ...!xyz.swapee.wc.IProgressColumn.Initialese)} */
xyz.swapee.wc.ProgressColumnConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @record */
$xyz.swapee.wc.IProgressColumn.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IProgressColumn.Pinout)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IProgressColumn.Pinout)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IProgressColumn.Pinout} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.ProgressColumnMemory)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.ProgressColumnClasses)|undefined} */
$xyz.swapee.wc.IProgressColumn.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IProgressColumn.MVCOptions} */
xyz.swapee.wc.IProgressColumn.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumn
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.RecordIProgressColumn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.BoundIProgressColumn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumn}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnProcessor}
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, null>}
 */
$xyz.swapee.wc.BoundIProgressColumn = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumn} */
xyz.swapee.wc.BoundIProgressColumn

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.BoundProgressColumn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumn}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumn = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumn} */
xyz.swapee.wc.BoundProgressColumn

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnPort.Inputs}
 */
$xyz.swapee.wc.IProgressColumnController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnController.Inputs} */
xyz.swapee.wc.IProgressColumnController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumn.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnController.Inputs}
 */
$xyz.swapee.wc.IProgressColumn.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumn.Pinout} */
xyz.swapee.wc.IProgressColumn.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumn
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.IProgressColumnBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.IProgressColumnBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnBuffer}
 */
xyz.swapee.wc.IProgressColumnBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml} xyz.swapee.wc.ProgressColumnBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9816a6f589f743d7c32de1ab18938663 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnBuffer}
 */
$xyz.swapee.wc.ProgressColumnBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnBuffer}
 */
xyz.swapee.wc.ProgressColumnBuffer
/** @type {function(new: xyz.swapee.wc.IProgressColumnBuffer)} */
xyz.swapee.wc.ProgressColumnBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplay.Initialese}
 */
$xyz.swapee.wc.IProgressColumnGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnGPU.Initialese} */
xyz.swapee.wc.IProgressColumnGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnController.Initialese}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreen.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumn.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnComputer.Initialese}
 */
$xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} */
xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.IProgressColumnHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/** @interface */
$xyz.swapee.wc.IProgressColumnHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnHtmlComponent} */
$xyz.swapee.wc.IProgressColumnHtmlComponentCaster.prototype.asIProgressColumnHtmlComponent
/** @type {!xyz.swapee.wc.BoundProgressColumnHtmlComponent} */
$xyz.swapee.wc.IProgressColumnHtmlComponentCaster.prototype.superProgressColumnHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnHtmlComponentCaster}
 */
xyz.swapee.wc.IProgressColumnHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IProgressColumnGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IProgressColumnGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnGPUFields}
 */
xyz.swapee.wc.IProgressColumnGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IProgressColumnGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnGPU} */
$xyz.swapee.wc.IProgressColumnGPUCaster.prototype.asIProgressColumnGPU
/** @type {!xyz.swapee.wc.BoundProgressColumnGPU} */
$xyz.swapee.wc.IProgressColumnGPUCaster.prototype.superProgressColumnGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnGPUCaster}
 */
xyz.swapee.wc.IProgressColumnGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.IProgressColumnGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!ProgressColumnMemory,>}
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplay}
 */
$xyz.swapee.wc.IProgressColumnGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnGPU}
 */
xyz.swapee.wc.IProgressColumnGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.IProgressColumnHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IProgressColumnController}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreen}
 * @extends {xyz.swapee.wc.IProgressColumn}
 * @extends {xyz.swapee.wc.IProgressColumnGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.IProgressColumnProcessor}
 * @extends {xyz.swapee.wc.IProgressColumnComputer}
 */
$xyz.swapee.wc.IProgressColumnHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnHtmlComponent}
 */
xyz.swapee.wc.IProgressColumnHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.ProgressColumnHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.ProgressColumnHtmlComponent
/** @type {function(new: xyz.swapee.wc.IProgressColumnHtmlComponent, ...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese)} */
xyz.swapee.wc.ProgressColumnHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.ProgressColumnHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.AbstractProgressColumnHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
$xyz.swapee.wc.AbstractProgressColumnHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnHtmlComponent)} */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.ProgressColumnHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnHtmlComponent, ...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese)} */
xyz.swapee.wc.ProgressColumnHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.RecordIProgressColumnHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.RecordIProgressColumnGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.BoundIProgressColumnGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnGPUFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!ProgressColumnMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnDisplay}
 */
$xyz.swapee.wc.BoundIProgressColumnGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnGPU} */
xyz.swapee.wc.BoundIProgressColumnGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.BoundIProgressColumnHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnController}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreen}
 * @extends {xyz.swapee.wc.BoundIProgressColumn}
 * @extends {xyz.swapee.wc.BoundIProgressColumnGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundIProgressColumnProcessor}
 * @extends {xyz.swapee.wc.BoundIProgressColumnComputer}
 */
$xyz.swapee.wc.BoundIProgressColumnHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnHtmlComponent} */
xyz.swapee.wc.BoundIProgressColumnHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml} xyz.swapee.wc.BoundProgressColumnHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c2f421ff8781214d28ff29e33b8fa549 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnHtmlComponent} */
xyz.swapee.wc.BoundProgressColumnHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @interface */
$xyz.swapee.wc.IProgressColumnDesigner = function() {}
/**
 * @param {xyz.swapee.wc.ProgressColumnClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.ProgressColumnClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.ProgressColumnClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IProgressColumnDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDesigner}
 */
xyz.swapee.wc.IProgressColumnDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.ProgressColumnDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IProgressColumnDesigner}
 */
$xyz.swapee.wc.ProgressColumnDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnDesigner}
 */
xyz.swapee.wc.ProgressColumnDesigner
/** @type {function(new: xyz.swapee.wc.IProgressColumnDesigner)} */
xyz.swapee.wc.ProgressColumnDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @record */
$xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IProgressColumnController} */
$xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh.prototype.ProgressColumn
/** @typedef {$xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh} */
xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @record */
$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IProgressColumnController} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh.prototype.ProgressColumn
/** @type {typeof xyz.swapee.wc.IProgressColumnController} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh} */
xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml} xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3e3aa1a68c3a179a65a9908867fd8ff4 */
/** @record */
$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.ProgressColumnMemory} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool.prototype.ProgressColumn
/** @type {!xyz.swapee.wc.ProgressColumnMemory} */
$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool} */
xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings>}
 */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese = function() {}
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.KycBlock
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.KycRequired
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.KycNotRequired
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.ExchangeEmail
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.InfoNotice
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.SupportNotice
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.PaymentStatusWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.PaymentStatusLa
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionStatusWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionStatusLa
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2BotSep
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1Co
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2Co
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3Co
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1La
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2La
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3La
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionIdWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.TransactionIdLa
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1ProgressCircleWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2ProgressCircleWr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1Wr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2Wr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3Wr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step1Circle
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step2Circle
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IProgressColumnDisplay.Initialese.prototype.Step3Circle
/** @typedef {$xyz.swapee.wc.IProgressColumnDisplay.Initialese} */
xyz.swapee.wc.IProgressColumnDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @interface */
$xyz.swapee.wc.IProgressColumnDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IProgressColumnDisplay.Settings} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IProgressColumnDisplay.Queries} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.queries
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycBlock
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycRequired
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycNotRequired
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.ExchangeEmail
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.InfoNotice
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.SupportNotice
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.PaymentStatusWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.PaymentStatusLa
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionStatusWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionStatusLa
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2BotSep
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Co
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Co
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Co
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1La
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2La
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3La
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionIdWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionIdLa
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1ProgressCircleWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2ProgressCircleWr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Wr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Wr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Wr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Circle
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Circle
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Circle
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDisplayFields}
 */
xyz.swapee.wc.IProgressColumnDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @interface */
$xyz.swapee.wc.IProgressColumnDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnDisplay} */
$xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.asIProgressColumnDisplay
/** @type {!xyz.swapee.wc.BoundIProgressColumnScreen} */
$xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.asIProgressColumnScreen
/** @type {!xyz.swapee.wc.BoundProgressColumnDisplay} */
$xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.superProgressColumnDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDisplayCaster}
 */
xyz.swapee.wc.IProgressColumnDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IProgressColumnDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.ProgressColumnMemory, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, xyz.swapee.wc.IProgressColumnDisplay.Queries, null>}
 */
$xyz.swapee.wc.IProgressColumnDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnDisplay}
 */
xyz.swapee.wc.IProgressColumnDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.ProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnDisplay.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.ProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.IProgressColumnDisplay, ...!xyz.swapee.wc.IProgressColumnDisplay.Initialese)} */
xyz.swapee.wc.ProgressColumnDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.ProgressColumnDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.AbstractProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnDisplay}
 */
$xyz.swapee.wc.AbstractProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnDisplay)} */
xyz.swapee.wc.AbstractProgressColumnDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.ProgressColumnDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnDisplay, ...!xyz.swapee.wc.IProgressColumnDisplay.Initialese)} */
xyz.swapee.wc.ProgressColumnDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.ProgressColumnGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnGPU.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.ProgressColumnGPU
/** @type {function(new: xyz.swapee.wc.IProgressColumnGPU, ...!xyz.swapee.wc.IProgressColumnGPU.Initialese)} */
xyz.swapee.wc.ProgressColumnGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.ProgressColumnGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.AbstractProgressColumnGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnGPU}
 */
$xyz.swapee.wc.AbstractProgressColumnGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnGPU)} */
xyz.swapee.wc.AbstractProgressColumnGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.ProgressColumnGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnGPU, ...!xyz.swapee.wc.IProgressColumnGPU.Initialese)} */
xyz.swapee.wc.ProgressColumnGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml} xyz.swapee.wc.BoundProgressColumnGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnGPU} */
xyz.swapee.wc.BoundProgressColumnGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.RecordIProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @typedef {{ paint: xyz.swapee.wc.IProgressColumnDisplay.paint }} */
xyz.swapee.wc.RecordIProgressColumnDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.BoundIProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnDisplayFields}
 * @extends {xyz.swapee.wc.RecordIProgressColumnDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.ProgressColumnMemory, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, xyz.swapee.wc.IProgressColumnDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnDisplay} */
xyz.swapee.wc.BoundIProgressColumnDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.BoundProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnDisplay} */
xyz.swapee.wc.BoundProgressColumnDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ProgressColumnMemory, null): void} */
xyz.swapee.wc.IProgressColumnDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnDisplay, !xyz.swapee.wc.ProgressColumnMemory, null): void} */
xyz.swapee.wc.IProgressColumnDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnDisplay.__paint} */
xyz.swapee.wc.IProgressColumnDisplay.__paint

// nss:xyz.swapee.wc.IProgressColumnDisplay,$xyz.swapee.wc.IProgressColumnDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/** @record */
$xyz.swapee.wc.IProgressColumnDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnDisplay.Queries} */
xyz.swapee.wc.IProgressColumnDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml} xyz.swapee.wc.IProgressColumnDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnDisplay.Queries}
 */
$xyz.swapee.wc.IProgressColumnDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnDisplay.Settings} */
xyz.swapee.wc.IProgressColumnDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ProgressColumnClasses>}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.KycBlock
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.KycRequired
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.KycNotRequired
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.ExchangeEmail
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.InfoNotice
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.SupportNotice
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.PaymentStatusWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.PaymentStatusLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionStatusWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionStatusLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2BotSep
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1Co
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2Co
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3Co
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1La
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2La
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3La
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionIdWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.TransactionIdLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1ProgressCircleWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2ProgressCircleWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1Wr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2Wr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3Wr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step1Circle
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step2Circle
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese.prototype.Step3Circle
/** @typedef {$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese} */
xyz.swapee.wc.back.IProgressColumnDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycBlock
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycRequired
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycNotRequired
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.ExchangeEmail
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.InfoNotice
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.SupportNotice
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.PaymentStatusWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.PaymentStatusLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionStatusWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionStatusLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2BotSep
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Co
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Co
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Co
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1La
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2La
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3La
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionIdWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionIdLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1ProgressCircleWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2ProgressCircleWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Wr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Wr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Wr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Circle
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Circle
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Circle
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnDisplayFields}
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnDisplay} */
$xyz.swapee.wc.back.IProgressColumnDisplayCaster.prototype.asIProgressColumnDisplay
/** @type {!xyz.swapee.wc.back.BoundProgressColumnDisplay} */
$xyz.swapee.wc.back.IProgressColumnDisplayCaster.prototype.superProgressColumnDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnDisplayCaster}
 */
xyz.swapee.wc.back.IProgressColumnDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.ProgressColumnClasses, null>}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnDisplay}
 */
xyz.swapee.wc.back.IProgressColumnDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.ProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IProgressColumnDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnDisplay.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.ProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnDisplay)} */
xyz.swapee.wc.back.ProgressColumnDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.ProgressColumnDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.AbstractProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.ProgressColumnDisplay}
 */
$xyz.swapee.wc.back.AbstractProgressColumnDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnDisplay)} */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.TransactionIdWr
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1Circle
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step2Circle
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step3Circle
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step2La
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.TransactionIdLa
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.PaymentFinalStatusWr
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.PaymentFinalStatusLa
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step3Wr
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step2BotSep
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1La
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusPQs.prototype.Step1ProgressCircleWr
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnVdusPQs}
 */
xyz.swapee.wc.ProgressColumnVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e1
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e2
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e3
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e4
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e5
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e6
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e7
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e8
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e9
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e10
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e11
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e12
/** @type {string} */
$xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e13
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnVdusQPs}
 */
xyz.swapee.wc.ProgressColumnVdusQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnVdusQPs)} */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.RecordIProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/** @typedef {{ paint: xyz.swapee.wc.back.IProgressColumnDisplay.paint }} */
xyz.swapee.wc.back.RecordIProgressColumnDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.BoundIProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.ProgressColumnClasses, null>}
 */
$xyz.swapee.wc.back.BoundIProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnDisplay} */
xyz.swapee.wc.back.BoundIProgressColumnDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.BoundProgressColumnDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnDisplay} */
xyz.swapee.wc.back.BoundProgressColumnDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml} xyz.swapee.wc.back.IProgressColumnDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IProgressColumnDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ProgressColumnMemory=, null=): void} */
xyz.swapee.wc.back.IProgressColumnDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IProgressColumnDisplay, !xyz.swapee.wc.ProgressColumnMemory=, null=): void} */
xyz.swapee.wc.back.IProgressColumnDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IProgressColumnDisplay.__paint} */
xyz.swapee.wc.back.IProgressColumnDisplay.__paint

// nss:xyz.swapee.wc.back.IProgressColumnDisplay,$xyz.swapee.wc.back.IProgressColumnDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnClassesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ProgressColumnClassesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleLoading
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleComplete
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleWaiting
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.CircleFailed
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesPQs.prototype.StepLabelFailed
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ProgressColumnClassesPQs}
 */
xyz.swapee.wc.ProgressColumnClassesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml} xyz.swapee.wc.ProgressColumnClassesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ProgressColumnClassesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.fe522
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.a5ad9
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.j912b
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.dc9d7
/** @type {string} */
$xyz.swapee.wc.ProgressColumnClassesQPs.prototype.he898
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ProgressColumnClassesQPs}
 */
xyz.swapee.wc.ProgressColumnClassesQPs
/** @type {function(new: xyz.swapee.wc.ProgressColumnClassesQPs)} */
xyz.swapee.wc.ProgressColumnClassesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/41-ProgressColumnClasses.xml} xyz.swapee.wc.ProgressColumnClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0b33a67f2f14608fcea73f64c0ab6978 */
/** @record */
$xyz.swapee.wc.ProgressColumnClasses = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleLoading
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleComplete
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleWaiting
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.CircleFailed
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.StepLabelFailed
/** @type {string|undefined} */
$xyz.swapee.wc.ProgressColumnClasses.prototype.StepLabelWaiting
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ProgressColumnClasses}
 */
xyz.swapee.wc.ProgressColumnClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.ProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnController.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.ProgressColumnController
/** @type {function(new: xyz.swapee.wc.IProgressColumnController, ...!xyz.swapee.wc.IProgressColumnController.Initialese)} */
xyz.swapee.wc.ProgressColumnController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.ProgressColumnController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.AbstractProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnController}
 */
$xyz.swapee.wc.AbstractProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnController)} */
xyz.swapee.wc.AbstractProgressColumnController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.ProgressColumnControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnController, ...!xyz.swapee.wc.IProgressColumnController.Initialese)} */
xyz.swapee.wc.ProgressColumnControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.BoundProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnController} */
xyz.swapee.wc.BoundProgressColumnController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IProgressColumnController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IProgressColumnController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IProgressColumnController): void} */
xyz.swapee.wc.IProgressColumnController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IProgressColumnController.__resetPort} */
xyz.swapee.wc.IProgressColumnController.__resetPort

// nss:xyz.swapee.wc.IProgressColumnController,$xyz.swapee.wc.IProgressColumnController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml} xyz.swapee.wc.IProgressColumnController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7f70528ab5e712c9024b430319befe0 */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnPort.WeakInputs}
 */
$xyz.swapee.wc.IProgressColumnController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnController.WeakInputs} */
xyz.swapee.wc.IProgressColumnController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.IProgressColumnController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @record */
$xyz.swapee.wc.front.IProgressColumnController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IProgressColumnController.Initialese} */
xyz.swapee.wc.front.IProgressColumnController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.IProgressColumnControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @interface */
$xyz.swapee.wc.front.IProgressColumnControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIProgressColumnController} */
$xyz.swapee.wc.front.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/** @type {!xyz.swapee.wc.front.BoundProgressColumnController} */
$xyz.swapee.wc.front.IProgressColumnControllerCaster.prototype.superProgressColumnController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnControllerCaster}
 */
xyz.swapee.wc.front.IProgressColumnControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.IProgressColumnControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/** @interface */
$xyz.swapee.wc.front.IProgressColumnControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIProgressColumnControllerAT} */
$xyz.swapee.wc.front.IProgressColumnControllerATCaster.prototype.asIProgressColumnControllerAT
/** @type {!xyz.swapee.wc.front.BoundProgressColumnControllerAT} */
$xyz.swapee.wc.front.IProgressColumnControllerATCaster.prototype.superProgressColumnControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnControllerATCaster}
 */
xyz.swapee.wc.front.IProgressColumnControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.IProgressColumnControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IProgressColumnControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnControllerAT}
 */
xyz.swapee.wc.front.IProgressColumnControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.IProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerAT}
 */
$xyz.swapee.wc.front.IProgressColumnController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnController}
 */
xyz.swapee.wc.front.IProgressColumnController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.ProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init
 * @implements {xyz.swapee.wc.front.IProgressColumnController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnController.Initialese>}
 */
$xyz.swapee.wc.front.ProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.ProgressColumnController
/** @type {function(new: xyz.swapee.wc.front.IProgressColumnController, ...!xyz.swapee.wc.front.IProgressColumnController.Initialese)} */
xyz.swapee.wc.front.ProgressColumnController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.ProgressColumnController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.AbstractProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init
 * @extends {xyz.swapee.wc.front.ProgressColumnController}
 */
$xyz.swapee.wc.front.AbstractProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController
/** @type {function(new: xyz.swapee.wc.front.AbstractProgressColumnController)} */
xyz.swapee.wc.front.AbstractProgressColumnController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.ProgressColumnControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnController, ...!xyz.swapee.wc.front.IProgressColumnController.Initialese)} */
xyz.swapee.wc.front.ProgressColumnControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.RecordIProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIProgressColumnController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.RecordIProgressColumnControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIProgressColumnControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.BoundIProgressColumnControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIProgressColumnControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIProgressColumnControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIProgressColumnControllerAT} */
xyz.swapee.wc.front.BoundIProgressColumnControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.BoundIProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIProgressColumnController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnControllerAT}
 */
$xyz.swapee.wc.front.BoundIProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIProgressColumnController} */
xyz.swapee.wc.front.BoundIProgressColumnController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml} xyz.swapee.wc.front.BoundProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f2da925ed07f59774704a581276a9c79 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundProgressColumnController} */
xyz.swapee.wc.front.BoundProgressColumnController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.IProgressColumnController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnController.Initialese} */
xyz.swapee.wc.back.IProgressColumnController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.IProgressColumnControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnController} */
$xyz.swapee.wc.back.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/** @type {!xyz.swapee.wc.back.BoundProgressColumnController} */
$xyz.swapee.wc.back.IProgressColumnControllerCaster.prototype.superProgressColumnController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnControllerCaster}
 */
xyz.swapee.wc.back.IProgressColumnControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.IProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.back.IProgressColumnController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnController}
 */
xyz.swapee.wc.back.IProgressColumnController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.ProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnController.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.ProgressColumnController
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnController, ...!xyz.swapee.wc.back.IProgressColumnController.Initialese)} */
xyz.swapee.wc.back.ProgressColumnController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.ProgressColumnController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.AbstractProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnController}
 */
$xyz.swapee.wc.back.AbstractProgressColumnController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnController)} */
xyz.swapee.wc.back.AbstractProgressColumnController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.ProgressColumnControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnController, ...!xyz.swapee.wc.back.IProgressColumnController.Initialese)} */
xyz.swapee.wc.back.ProgressColumnControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.RecordIProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.BoundIProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerCaster}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IProgressColumnController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnController} */
xyz.swapee.wc.back.BoundIProgressColumnController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml} xyz.swapee.wc.back.BoundProgressColumnController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6ce0d61709720874c0c4a04e334fc00e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnController} */
xyz.swapee.wc.back.BoundProgressColumnController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnController.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} */
xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.IProgressColumnControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnControllerAR} */
$xyz.swapee.wc.back.IProgressColumnControllerARCaster.prototype.asIProgressColumnControllerAR
/** @type {!xyz.swapee.wc.back.BoundProgressColumnControllerAR} */
$xyz.swapee.wc.back.IProgressColumnControllerARCaster.prototype.superProgressColumnControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnControllerARCaster}
 */
xyz.swapee.wc.back.IProgressColumnControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.IProgressColumnControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IProgressColumnController}
 */
$xyz.swapee.wc.back.IProgressColumnControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnControllerAR}
 */
xyz.swapee.wc.back.IProgressColumnControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.ProgressColumnControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.ProgressColumnControllerAR
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnControllerAR, ...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese)} */
xyz.swapee.wc.back.ProgressColumnControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.ProgressColumnControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.AbstractProgressColumnControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
$xyz.swapee.wc.back.AbstractProgressColumnControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnControllerAR)} */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.ProgressColumnControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnControllerAR, ...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese)} */
xyz.swapee.wc.back.ProgressColumnControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.RecordIProgressColumnControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.BoundIProgressColumnControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIProgressColumnController}
 */
$xyz.swapee.wc.back.BoundIProgressColumnControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnControllerAR} */
xyz.swapee.wc.back.BoundIProgressColumnControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml} xyz.swapee.wc.back.BoundProgressColumnControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e45cbe72dd717830455ba7bd3b2b3ba5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnControllerAR} */
xyz.swapee.wc.back.BoundProgressColumnControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} */
xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IProgressColumnControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.ProgressColumnControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IProgressColumnControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.ProgressColumnControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.ProgressColumnControllerAT
/** @type {function(new: xyz.swapee.wc.front.IProgressColumnControllerAT, ...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese)} */
xyz.swapee.wc.front.ProgressColumnControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.ProgressColumnControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.AbstractProgressColumnControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
$xyz.swapee.wc.front.AbstractProgressColumnControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractProgressColumnControllerAT)} */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.ProgressColumnControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnControllerAT, ...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese)} */
xyz.swapee.wc.front.ProgressColumnControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml} xyz.swapee.wc.front.BoundProgressColumnControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f0607b6719617d570d52e32357a7b5d1 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundProgressColumnControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundProgressColumnControllerAT} */
xyz.swapee.wc.front.BoundProgressColumnControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.IProgressColumnScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.IProgressColumnDisplay.Initialese}
 */
$xyz.swapee.wc.IProgressColumnScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnScreen.Initialese} */
xyz.swapee.wc.IProgressColumnScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.IProgressColumnScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/** @interface */
$xyz.swapee.wc.IProgressColumnScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIProgressColumnScreen} */
$xyz.swapee.wc.IProgressColumnScreenCaster.prototype.asIProgressColumnScreen
/** @type {!xyz.swapee.wc.BoundProgressColumnScreen} */
$xyz.swapee.wc.IProgressColumnScreenCaster.prototype.superProgressColumnScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnScreenCaster}
 */
xyz.swapee.wc.IProgressColumnScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.IProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IProgressColumnController}
 * @extends {xyz.swapee.wc.IProgressColumnDisplay}
 */
$xyz.swapee.wc.IProgressColumnScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IProgressColumnScreen}
 */
xyz.swapee.wc.IProgressColumnScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.ProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init
 * @implements {xyz.swapee.wc.IProgressColumnScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnScreen.Initialese>}
 */
$xyz.swapee.wc.ProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.ProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.IProgressColumnScreen, ...!xyz.swapee.wc.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.ProgressColumnScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.ProgressColumnScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.AbstractProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init
 * @extends {xyz.swapee.wc.ProgressColumnScreen}
 */
$xyz.swapee.wc.AbstractProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.AbstractProgressColumnScreen)} */
xyz.swapee.wc.AbstractProgressColumnScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.continues
/**
 * @param {...((!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.ProgressColumnScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/** @typedef {function(new: xyz.swapee.wc.IProgressColumnScreen, ...!xyz.swapee.wc.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.ProgressColumnScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.RecordIProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIProgressColumnScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.BoundIProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIProgressColumnScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IProgressColumnScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnController}
 * @extends {xyz.swapee.wc.BoundIProgressColumnDisplay}
 */
$xyz.swapee.wc.BoundIProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundIProgressColumnScreen} */
xyz.swapee.wc.BoundIProgressColumnScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml} xyz.swapee.wc.BoundProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7566ef3d069eb30deabb42b51cba70d */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIProgressColumnScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundProgressColumnScreen} */
xyz.swapee.wc.BoundProgressColumnScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} */
xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.IProgressColumnScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese}
 */
$xyz.swapee.wc.back.IProgressColumnScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IProgressColumnScreen.Initialese} */
xyz.swapee.wc.back.IProgressColumnScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IProgressColumnScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.IProgressColumnScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnScreen} */
$xyz.swapee.wc.back.IProgressColumnScreenCaster.prototype.asIProgressColumnScreen
/** @type {!xyz.swapee.wc.back.BoundProgressColumnScreen} */
$xyz.swapee.wc.back.IProgressColumnScreenCaster.prototype.superProgressColumnScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreenCaster}
 */
xyz.swapee.wc.back.IProgressColumnScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.IProgressColumnScreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/** @interface */
$xyz.swapee.wc.back.IProgressColumnScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIProgressColumnScreenAT} */
$xyz.swapee.wc.back.IProgressColumnScreenATCaster.prototype.asIProgressColumnScreenAT
/** @type {!xyz.swapee.wc.back.BoundProgressColumnScreenAT} */
$xyz.swapee.wc.back.IProgressColumnScreenATCaster.prototype.superProgressColumnScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreenATCaster}
 */
xyz.swapee.wc.back.IProgressColumnScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.IProgressColumnScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.IProgressColumnScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreenAT}
 */
xyz.swapee.wc.back.IProgressColumnScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.IProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenCaster}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenAT}
 */
$xyz.swapee.wc.back.IProgressColumnScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IProgressColumnScreen}
 */
xyz.swapee.wc.back.IProgressColumnScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.ProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreen.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.ProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnScreen, ...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.ProgressColumnScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.AbstractProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnScreen}
 */
$xyz.swapee.wc.back.AbstractProgressColumnScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnScreen)} */
xyz.swapee.wc.back.AbstractProgressColumnScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.ProgressColumnScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnScreen, ...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.RecordIProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.RecordIProgressColumnScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIProgressColumnScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.BoundIProgressColumnScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundIProgressColumnScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnScreenAT} */
xyz.swapee.wc.back.BoundIProgressColumnScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.BoundIProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIProgressColumnScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IProgressColumnScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreenAT}
 */
$xyz.swapee.wc.back.BoundIProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIProgressColumnScreen} */
xyz.swapee.wc.back.BoundIProgressColumnScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml} xyz.swapee.wc.back.BoundProgressColumnScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c7c02329346ae2316b93a97c3a90c3f7 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnScreen} */
xyz.swapee.wc.back.BoundProgressColumnScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IProgressColumnScreen.Initialese}
 */
$xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} */
xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IProgressColumnScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.IProgressColumnScreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/** @interface */
$xyz.swapee.wc.front.IProgressColumnScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIProgressColumnScreenAR} */
$xyz.swapee.wc.front.IProgressColumnScreenARCaster.prototype.asIProgressColumnScreenAR
/** @type {!xyz.swapee.wc.front.BoundProgressColumnScreenAR} */
$xyz.swapee.wc.front.IProgressColumnScreenARCaster.prototype.superProgressColumnScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnScreenARCaster}
 */
xyz.swapee.wc.front.IProgressColumnScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.IProgressColumnScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IProgressColumnScreen}
 */
$xyz.swapee.wc.front.IProgressColumnScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IProgressColumnScreenAR}
 */
xyz.swapee.wc.front.IProgressColumnScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.ProgressColumnScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IProgressColumnScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.ProgressColumnScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.ProgressColumnScreenAR
/** @type {function(new: xyz.swapee.wc.front.IProgressColumnScreenAR, ...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese)} */
xyz.swapee.wc.front.ProgressColumnScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.ProgressColumnScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.AbstractProgressColumnScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
$xyz.swapee.wc.front.AbstractProgressColumnScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractProgressColumnScreenAR)} */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.ProgressColumnScreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnScreenAR, ...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese)} */
xyz.swapee.wc.front.ProgressColumnScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.RecordIProgressColumnScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIProgressColumnScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.BoundIProgressColumnScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIProgressColumnScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IProgressColumnScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIProgressColumnScreen}
 */
$xyz.swapee.wc.front.BoundIProgressColumnScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIProgressColumnScreenAR} */
xyz.swapee.wc.front.BoundIProgressColumnScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml} xyz.swapee.wc.front.BoundProgressColumnScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88615c5c8861c84b9589f579fed972d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIProgressColumnScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundProgressColumnScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundProgressColumnScreenAR} */
xyz.swapee.wc.front.BoundProgressColumnScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.ProgressColumnScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IProgressColumnScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.ProgressColumnScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.ProgressColumnScreenAT
/** @type {function(new: xyz.swapee.wc.back.IProgressColumnScreenAT, ...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.ProgressColumnScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.AbstractProgressColumnScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
$xyz.swapee.wc.back.AbstractProgressColumnScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractProgressColumnScreenAT)} */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.ProgressColumnScreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnScreenAT, ...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese)} */
xyz.swapee.wc.back.ProgressColumnScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml} xyz.swapee.wc.back.BoundProgressColumnScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94d9d7a2fe82060c43cba0bfe9d03005 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIProgressColumnScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundProgressColumnScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundProgressColumnScreenAT} */
xyz.swapee.wc.back.BoundProgressColumnScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe.prototype.creatingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe.prototype.finishedOnPayment
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe.prototype.paymentFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe.prototype.paymentCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe.prototype.transactionId
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe.prototype.paymentStatus
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe.prototype.loadingStep1
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe.prototype.loadingStep2
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe.prototype.loadingStep3
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/** @record */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe.prototype.status
/** @typedef {$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe} */
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId} */
xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.Status exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.Status = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.Status} */
xyz.swapee.wc.IProgressColumnPort.Inputs.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe} */
xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe}
 */
$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe} */
xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe.prototype.processingTransaction
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe.prototype.transactionCompleted
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f35d24e4e1ad7187ab4fe572fc78994f */
/** @record */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe.prototype.transactionFailed
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction} */
xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment} */
xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionId exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionId

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3 exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3 = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe} */
xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml} xyz.swapee.wc.IProgressColumnCore.Model.Status exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd487ede85e58495f8b406d448a76ac */
/**
 * @record
 * @extends {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status}
 */
$xyz.swapee.wc.IProgressColumnCore.Model.Status = function() {}
/** @typedef {$xyz.swapee.wc.IProgressColumnCore.Model.Status} */
xyz.swapee.wc.IProgressColumnCore.Model.Status

// nss:xyz.swapee.wc,$xyz.swapee.wc.IProgressColumnCore.Model
/* @typal-end */