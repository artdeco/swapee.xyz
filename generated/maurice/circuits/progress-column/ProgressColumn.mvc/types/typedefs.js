/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IProgressColumnComputer={}
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus={}
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction={}
xyz.swapee.wc.IProgressColumnOuterCore={}
xyz.swapee.wc.IProgressColumnOuterCore.Model={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3={}
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status={}
xyz.swapee.wc.IProgressColumnOuterCore.WeakModel={}
xyz.swapee.wc.IProgressColumnPort={}
xyz.swapee.wc.IProgressColumnPort.Inputs={}
xyz.swapee.wc.IProgressColumnPort.WeakInputs={}
xyz.swapee.wc.IProgressColumnCore={}
xyz.swapee.wc.IProgressColumnCore.Model={}
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction={}
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus={}
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted={}
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed={}
xyz.swapee.wc.IProgressColumnPortInterface={}
xyz.swapee.wc.IProgressColumnProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IProgressColumnController={}
xyz.swapee.wc.front.IProgressColumnControllerAT={}
xyz.swapee.wc.front.IProgressColumnScreenAR={}
xyz.swapee.wc.IProgressColumn={}
xyz.swapee.wc.IProgressColumnHtmlComponent={}
xyz.swapee.wc.IProgressColumnElement={}
xyz.swapee.wc.IProgressColumnElementPort={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts={}
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts={}
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs={}
xyz.swapee.wc.IProgressColumnDesigner={}
xyz.swapee.wc.IProgressColumnDesigner.communicator={}
xyz.swapee.wc.IProgressColumnDesigner.relay={}
xyz.swapee.wc.IProgressColumnDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IProgressColumnDisplay={}
xyz.swapee.wc.back.IProgressColumnController={}
xyz.swapee.wc.back.IProgressColumnControllerAR={}
xyz.swapee.wc.back.IProgressColumnScreen={}
xyz.swapee.wc.back.IProgressColumnScreenAT={}
xyz.swapee.wc.IProgressColumnController={}
xyz.swapee.wc.IProgressColumnScreen={}
xyz.swapee.wc.IProgressColumnGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/02-IProgressColumnComputer.xml}  d71faa9b93801bdc524d14a9f00ba3f5 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IProgressColumnComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnComputer)} xyz.swapee.wc.AbstractProgressColumnComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnComputer} xyz.swapee.wc.ProgressColumnComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnComputer` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnComputer
 */
xyz.swapee.wc.AbstractProgressColumnComputer = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnComputer.constructor&xyz.swapee.wc.ProgressColumnComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnComputer.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnComputer.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.AbstractProgressColumnComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnComputer.Initialese[]) => xyz.swapee.wc.IProgressColumnComputer} xyz.swapee.wc.ProgressColumnComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.ProgressColumnMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IProgressColumnComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IProgressColumnComputer
 */
xyz.swapee.wc.IProgressColumnComputer = class extends /** @type {xyz.swapee.wc.IProgressColumnComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus} */
xyz.swapee.wc.IProgressColumnComputer.prototype.adaptTransactionStatus = function() {}
/** @type {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction} */
xyz.swapee.wc.IProgressColumnComputer.prototype.adaptProcessingTransaction = function() {}
/** @type {xyz.swapee.wc.IProgressColumnComputer.compute} */
xyz.swapee.wc.IProgressColumnComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnComputer.Initialese>)} xyz.swapee.wc.ProgressColumnComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnComputer} xyz.swapee.wc.IProgressColumnComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IProgressColumnComputer_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnComputer
 * @implements {xyz.swapee.wc.IProgressColumnComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnComputer.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnComputer = class extends /** @type {xyz.swapee.wc.ProgressColumnComputer.constructor&xyz.swapee.wc.IProgressColumnComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnComputer}
 */
xyz.swapee.wc.ProgressColumnComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IProgressColumnComputer} */
xyz.swapee.wc.RecordIProgressColumnComputer

/** @typedef {xyz.swapee.wc.IProgressColumnComputer} xyz.swapee.wc.BoundIProgressColumnComputer */

/** @typedef {xyz.swapee.wc.ProgressColumnComputer} xyz.swapee.wc.BoundProgressColumnComputer */

/**
 * Contains getters to cast the _IProgressColumnComputer_ interface.
 * @interface xyz.swapee.wc.IProgressColumnComputerCaster
 */
xyz.swapee.wc.IProgressColumnComputerCaster = class { }
/**
 * Cast the _IProgressColumnComputer_ instance into the _BoundIProgressColumnComputer_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnComputer}
 */
xyz.swapee.wc.IProgressColumnComputerCaster.prototype.asIProgressColumnComputer
/**
 * Access the _ProgressColumnComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnComputer}
 */
xyz.swapee.wc.IProgressColumnComputerCaster.prototype.superProgressColumnComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form, changes: xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form) => (void|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return)} xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnComputer.__adaptTransactionStatus<!xyz.swapee.wc.IProgressColumnComputer>} xyz.swapee.wc.IProgressColumnComputer._adaptTransactionStatus */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus} */
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} form The form with inputs.
 * - `status` _string_ ⤴ *IProgressColumnOuterCore.Model.Status_Safe*
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form} changes The previous values of the form.
 * - `status` _string_ ⤴ *IProgressColumnOuterCore.Model.Status_Safe*
 * @return {void|xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return} The form with outputs.
 */
xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus} xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form, changes: xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form) => (void|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return)} xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnComputer.__adaptProcessingTransaction<!xyz.swapee.wc.IProgressColumnComputer>} xyz.swapee.wc.IProgressColumnComputer._adaptProcessingTransaction */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction} */
/**
 * @param {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} form The form with inputs.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user. ⤴ *IProgressColumnCore.Model.TransactionStatus_Safe*
 * Can be either:
 * > - _exchaning_
 * > - _sending_
 * > - _finished_
 * > - _failed_
 * @param {xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form} changes The previous values of the form.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user. ⤴ *IProgressColumnCore.Model.TransactionStatus_Safe*
 * Can be either:
 * > - _exchaning_
 * > - _sending_
 * > - _finished_
 * > - _failed_
 * @return {void|xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return} The form with outputs.
 */
xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction} xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.ProgressColumnMemory) => void} xyz.swapee.wc.IProgressColumnComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnComputer.__compute<!xyz.swapee.wc.IProgressColumnComputer>} xyz.swapee.wc.IProgressColumnComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.ProgressColumnMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IProgressColumnComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IProgressColumnComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/03-IProgressColumnOuterCore.xml}  8cd487ede85e58495f8b406d448a76ac */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IProgressColumnOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnOuterCore)} xyz.swapee.wc.AbstractProgressColumnOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnOuterCore} xyz.swapee.wc.ProgressColumnOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnOuterCore
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnOuterCore.constructor&xyz.swapee.wc.ProgressColumnOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnOuterCore.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.AbstractProgressColumnOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnOuterCoreCaster)} xyz.swapee.wc.IProgressColumnOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IProgressColumn_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IProgressColumnOuterCore
 */
xyz.swapee.wc.IProgressColumnOuterCore = class extends /** @type {xyz.swapee.wc.IProgressColumnOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IProgressColumnOuterCore.prototype.constructor = xyz.swapee.wc.IProgressColumnOuterCore

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnOuterCore.Initialese>)} xyz.swapee.wc.ProgressColumnOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnOuterCore} xyz.swapee.wc.IProgressColumnOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IProgressColumnOuterCore_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnOuterCore
 * @implements {xyz.swapee.wc.IProgressColumnOuterCore} The _IProgressColumn_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnOuterCore = class extends /** @type {xyz.swapee.wc.ProgressColumnOuterCore.constructor&xyz.swapee.wc.IProgressColumnOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ProgressColumnOuterCore.prototype.constructor = xyz.swapee.wc.ProgressColumnOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnOuterCore}
 */
xyz.swapee.wc.ProgressColumnOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnOuterCore.
 * @interface xyz.swapee.wc.IProgressColumnOuterCoreFields
 */
xyz.swapee.wc.IProgressColumnOuterCoreFields = class { }
/**
 * The _IProgressColumn_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IProgressColumnOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IProgressColumnOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore} */
xyz.swapee.wc.RecordIProgressColumnOuterCore

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore} xyz.swapee.wc.BoundIProgressColumnOuterCore */

/** @typedef {xyz.swapee.wc.ProgressColumnOuterCore} xyz.swapee.wc.BoundProgressColumnOuterCore */

/**
 *   The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created.
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction.creatingTransaction

/**
 *   Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful.
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment.finishedOnPayment

/**
 * The payment during step 2 did not succeed.
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed.paymentFailed

/**
 * The second, payment step, was successful. Moving on to the third step.
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted.paymentCompleted

/**
 * The ID of the created transaction.
 * @typedef {string}
 */
xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId.transactionId

/**
 * The status of the payment.
 * @typedef {string}
 */
xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus.paymentStatus

/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1.loadingStep1

/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2.loadingStep2

/** @typedef {boolean} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3.loadingStep3

/** @typedef {string} */
xyz.swapee.wc.IProgressColumnOuterCore.Model.Status.status

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction&xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment&xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed&xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted&xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId&xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus&xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1&xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2&xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3&xyz.swapee.wc.IProgressColumnOuterCore.Model.Status} xyz.swapee.wc.IProgressColumnOuterCore.Model The _IProgressColumn_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel The _IProgressColumn_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IProgressColumnOuterCore_ interface.
 * @interface xyz.swapee.wc.IProgressColumnOuterCoreCaster
 */
xyz.swapee.wc.IProgressColumnOuterCoreCaster = class { }
/**
 * Cast the _IProgressColumnOuterCore_ instance into the _BoundIProgressColumnOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnOuterCore}
 */
xyz.swapee.wc.IProgressColumnOuterCoreCaster.prototype.asIProgressColumnOuterCore
/**
 * Access the _ProgressColumnOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnOuterCore}
 */
xyz.swapee.wc.IProgressColumnOuterCoreCaster.prototype.superProgressColumnOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (optional overlay).
 * @prop {boolean} [creatingTransaction=false] The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (required overlay).
 * @prop {boolean} creatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (optional overlay).
 * @prop {boolean} [finishedOnPayment=false] Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (required overlay).
 * @prop {boolean} finishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed The payment during step 2 did not succeed (optional overlay).
 * @prop {boolean} [paymentFailed=false] The payment during step 2 did not succeed. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe The payment during step 2 did not succeed (required overlay).
 * @prop {boolean} paymentFailed The payment during step 2 did not succeed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted The second, payment step, was successful. Moving on to the third step (optional overlay).
 * @prop {boolean} [paymentCompleted=false] The second, payment step, was successful. Moving on to the third step. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe The second, payment step, was successful. Moving on to the third step (required overlay).
 * @prop {boolean} paymentCompleted The second, payment step, was successful. Moving on to the third step.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId The ID of the created transaction (optional overlay).
 * @prop {string} [transactionId=""] The ID of the created transaction. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe The ID of the created transaction (required overlay).
 * @prop {string} transactionId The ID of the created transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus The status of the payment (optional overlay).
 * @prop {'waiting'|'confirming'|'confirmed'} [paymentStatus=""] The status of the payment.
 * Can be either:
 * - _waiting_
 * - _confirming_
 * - _confirmed_
 *
 * Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe The status of the payment (required overlay).
 * @prop {'waiting'|'confirming'|'confirmed'} paymentStatus The status of the payment.
 * Can be either:
 * - _waiting_
 * - _confirming_
 * - _confirmed_
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1  (optional overlay).
 * @prop {boolean} [loadingStep1=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe  (required overlay).
 * @prop {boolean} loadingStep1
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2  (optional overlay).
 * @prop {boolean} [loadingStep2=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe  (required overlay).
 * @prop {boolean} loadingStep2
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3  (optional overlay).
 * @prop {boolean} [loadingStep3=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe  (required overlay).
 * @prop {boolean} loadingStep3
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status  (optional overlay).
 * @prop {string} [status=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe  (required overlay).
 * @prop {string} status
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (optional overlay).
 * @prop {*} [creatingTransaction=null] The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (required overlay).
 * @prop {*} creatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (optional overlay).
 * @prop {*} [finishedOnPayment=null] Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (required overlay).
 * @prop {*} finishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed The payment during step 2 did not succeed (optional overlay).
 * @prop {*} [paymentFailed=null] The payment during step 2 did not succeed. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe The payment during step 2 did not succeed (required overlay).
 * @prop {*} paymentFailed The payment during step 2 did not succeed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted The second, payment step, was successful. Moving on to the third step (optional overlay).
 * @prop {*} [paymentCompleted=null] The second, payment step, was successful. Moving on to the third step. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe The second, payment step, was successful. Moving on to the third step (required overlay).
 * @prop {*} paymentCompleted The second, payment step, was successful. Moving on to the third step.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId The ID of the created transaction (optional overlay).
 * @prop {*} [transactionId=null] The ID of the created transaction. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe The ID of the created transaction (required overlay).
 * @prop {*} transactionId The ID of the created transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus The status of the payment (optional overlay).
 * @prop {'waiting'|'confirming'|'confirmed'} [paymentStatus=null] The status of the payment.
 * Can be either:
 * - _waiting_
 * - _confirming_
 * - _confirmed_
 *
 * Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe The status of the payment (required overlay).
 * @prop {'waiting'|'confirming'|'confirmed'} paymentStatus The status of the payment.
 * Can be either:
 * - _waiting_
 * - _confirming_
 * - _confirmed_
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1  (optional overlay).
 * @prop {*} [loadingStep1=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe  (required overlay).
 * @prop {*} loadingStep1
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2  (optional overlay).
 * @prop {*} [loadingStep2=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe  (required overlay).
 * @prop {*} loadingStep2
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3  (optional overlay).
 * @prop {*} [loadingStep3=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe  (required overlay).
 * @prop {*} loadingStep3
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status  (optional overlay).
 * @prop {*} [status=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe  (required overlay).
 * @prop {*} status
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction} xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.CreatingTransaction_Safe The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (required overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment} xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.FinishedOnPayment_Safe Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (required overlay).
 */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed The payment during step 2 did not succeed (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentFailed_Safe The payment during step 2 did not succeed (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted The second, payment step, was successful. Moving on to the third step (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentCompleted_Safe The second, payment step, was successful. Moving on to the third step (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId} xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId The ID of the created transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.TransactionId_Safe The ID of the created transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus The status of the payment (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.PaymentStatus_Safe The status of the payment (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep1_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep2_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.LoadingStep3_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status} xyz.swapee.wc.IProgressColumnPort.Inputs.Status  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe} xyz.swapee.wc.IProgressColumnPort.Inputs.Status_Safe  (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction} xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.CreatingTransaction_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.CreatingTransaction_Safe The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (required overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment} xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.FinishedOnPayment_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.FinishedOnPayment_Safe Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (required overlay).
 */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed The payment during step 2 did not succeed (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentFailed_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentFailed_Safe The payment during step 2 did not succeed (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted The second, payment step, was successful. Moving on to the third step (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentCompleted_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentCompleted_Safe The second, payment step, was successful. Moving on to the third step (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId} xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId The ID of the created transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.TransactionId_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.TransactionId_Safe The ID of the created transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus The status of the payment (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.PaymentStatus_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.PaymentStatus_Safe The status of the payment (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep1_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep1_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep2_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep2_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.LoadingStep3_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.LoadingStep3_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status} xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.Status_Safe} xyz.swapee.wc.IProgressColumnPort.WeakInputs.Status_Safe  (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction} xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.CreatingTransaction_Safe} xyz.swapee.wc.IProgressColumnCore.Model.CreatingTransaction_Safe The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created (required overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment} xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.FinishedOnPayment_Safe} xyz.swapee.wc.IProgressColumnCore.Model.FinishedOnPayment_Safe Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful (required overlay).
 */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed} xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed The payment during step 2 did not succeed (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentFailed_Safe} xyz.swapee.wc.IProgressColumnCore.Model.PaymentFailed_Safe The payment during step 2 did not succeed (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted} xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted The second, payment step, was successful. Moving on to the third step (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentCompleted_Safe} xyz.swapee.wc.IProgressColumnCore.Model.PaymentCompleted_Safe The second, payment step, was successful. Moving on to the third step (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId} xyz.swapee.wc.IProgressColumnCore.Model.TransactionId The ID of the created transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.TransactionId_Safe} xyz.swapee.wc.IProgressColumnCore.Model.TransactionId_Safe The ID of the created transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus} xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus The status of the payment (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.PaymentStatus_Safe} xyz.swapee.wc.IProgressColumnCore.Model.PaymentStatus_Safe The status of the payment (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep1_Safe} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep1_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep2_Safe} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep2_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.LoadingStep3_Safe} xyz.swapee.wc.IProgressColumnCore.Model.LoadingStep3_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status} xyz.swapee.wc.IProgressColumnCore.Model.Status  (optional overlay). */

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model.Status_Safe} xyz.swapee.wc.IProgressColumnCore.Model.Status_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/04-IProgressColumnPort.xml}  4ec7822e1d7290cdd78c1a15430ba01f */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IProgressColumnPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnPort)} xyz.swapee.wc.AbstractProgressColumnPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnPort} xyz.swapee.wc.ProgressColumnPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnPort` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnPort
 */
xyz.swapee.wc.AbstractProgressColumnPort = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnPort.constructor&xyz.swapee.wc.ProgressColumnPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnPort.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnPort.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnPort|typeof xyz.swapee.wc.ProgressColumnPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.AbstractProgressColumnPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnPort.Initialese[]) => xyz.swapee.wc.IProgressColumnPort} xyz.swapee.wc.ProgressColumnPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnPortFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IProgressColumnPort.Inputs>)} xyz.swapee.wc.IProgressColumnPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IProgressColumn_, providing input
 * pins.
 * @interface xyz.swapee.wc.IProgressColumnPort
 */
xyz.swapee.wc.IProgressColumnPort = class extends /** @type {xyz.swapee.wc.IProgressColumnPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IProgressColumnPort.resetPort} */
xyz.swapee.wc.IProgressColumnPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort} */
xyz.swapee.wc.IProgressColumnPort.prototype.resetProgressColumnPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnPort&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnPort.Initialese>)} xyz.swapee.wc.ProgressColumnPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnPort} xyz.swapee.wc.IProgressColumnPort.typeof */
/**
 * A concrete class of _IProgressColumnPort_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnPort
 * @implements {xyz.swapee.wc.IProgressColumnPort} The port that serves as an interface to the _IProgressColumn_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnPort.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnPort = class extends /** @type {xyz.swapee.wc.ProgressColumnPort.constructor&xyz.swapee.wc.IProgressColumnPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnPort}
 */
xyz.swapee.wc.ProgressColumnPort.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnPort.
 * @interface xyz.swapee.wc.IProgressColumnPortFields
 */
xyz.swapee.wc.IProgressColumnPortFields = class { }
/**
 * The inputs to the _IProgressColumn_'s controller via its port.
 */
xyz.swapee.wc.IProgressColumnPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IProgressColumnPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IProgressColumnPortFields.prototype.props = /** @type {!xyz.swapee.wc.IProgressColumnPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnPort} */
xyz.swapee.wc.RecordIProgressColumnPort

/** @typedef {xyz.swapee.wc.IProgressColumnPort} xyz.swapee.wc.BoundIProgressColumnPort */

/** @typedef {xyz.swapee.wc.ProgressColumnPort} xyz.swapee.wc.BoundProgressColumnPort */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnOuterCore.WeakModel)} xyz.swapee.wc.IProgressColumnPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnOuterCore.WeakModel} xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IProgressColumn_'s controller via its port.
 * @record xyz.swapee.wc.IProgressColumnPort.Inputs
 */
xyz.swapee.wc.IProgressColumnPort.Inputs = class extends /** @type {xyz.swapee.wc.IProgressColumnPort.Inputs.constructor&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IProgressColumnPort.Inputs.prototype.constructor = xyz.swapee.wc.IProgressColumnPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnOuterCore.WeakModel)} xyz.swapee.wc.IProgressColumnPort.WeakInputs.constructor */
/**
 * The inputs to the _IProgressColumn_'s controller via its port.
 * @record xyz.swapee.wc.IProgressColumnPort.WeakInputs
 */
xyz.swapee.wc.IProgressColumnPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IProgressColumnPort.WeakInputs.constructor&xyz.swapee.wc.IProgressColumnOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IProgressColumnPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IProgressColumnPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IProgressColumnPortInterface
 */
xyz.swapee.wc.IProgressColumnPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IProgressColumnPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IProgressColumnPortInterface.prototype.constructor = xyz.swapee.wc.IProgressColumnPortInterface

/**
 * A concrete class of _IProgressColumnPortInterface_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnPortInterface
 * @implements {xyz.swapee.wc.IProgressColumnPortInterface} The port interface.
 */
xyz.swapee.wc.ProgressColumnPortInterface = class extends xyz.swapee.wc.IProgressColumnPortInterface { }
xyz.swapee.wc.ProgressColumnPortInterface.prototype.constructor = xyz.swapee.wc.ProgressColumnPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnPortInterface.Props
 * @prop {boolean} creatingTransaction The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created.
 * @prop {boolean} finishedOnPayment Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful.
 * @prop {boolean} paymentFailed The payment during step 2 did not succeed.
 * @prop {boolean} paymentCompleted The second, payment step, was successful. Moving on to the third step.
 * @prop {string} transactionId The ID of the created transaction.
 * @prop {'waiting'|'confirming'|'confirmed'} paymentStatus The status of the payment.
 * Can be either:
 * - _waiting_
 * - _confirming_
 * - _confirmed_
 * @prop {boolean} loadingStep1
 * @prop {boolean} loadingStep2
 * @prop {boolean} loadingStep3
 * @prop {string} status
 */

/**
 * Contains getters to cast the _IProgressColumnPort_ interface.
 * @interface xyz.swapee.wc.IProgressColumnPortCaster
 */
xyz.swapee.wc.IProgressColumnPortCaster = class { }
/**
 * Cast the _IProgressColumnPort_ instance into the _BoundIProgressColumnPort_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnPort}
 */
xyz.swapee.wc.IProgressColumnPortCaster.prototype.asIProgressColumnPort
/**
 * Access the _ProgressColumnPort_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnPort}
 */
xyz.swapee.wc.IProgressColumnPortCaster.prototype.superProgressColumnPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IProgressColumnPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnPort.__resetPort<!xyz.swapee.wc.IProgressColumnPort>} xyz.swapee.wc.IProgressColumnPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnPort.resetPort} */
/**
 * Resets the _IProgressColumn_ port.
 * @return {void}
 */
xyz.swapee.wc.IProgressColumnPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnPort.__resetProgressColumnPort<!xyz.swapee.wc.IProgressColumnPort>} xyz.swapee.wc.IProgressColumnPort._resetProgressColumnPort */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IProgressColumnPort.resetProgressColumnPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IProgressColumnPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/09-IProgressColumnCore.xml}  f35d24e4e1ad7187ab4fe572fc78994f */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IProgressColumnCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnCore)} xyz.swapee.wc.AbstractProgressColumnCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnCore} xyz.swapee.wc.ProgressColumnCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnCore` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnCore
 */
xyz.swapee.wc.AbstractProgressColumnCore = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnCore.constructor&xyz.swapee.wc.ProgressColumnCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnCore.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnCore.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnOuterCore|typeof xyz.swapee.wc.ProgressColumnOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.AbstractProgressColumnCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnCoreCaster&xyz.swapee.wc.IProgressColumnOuterCore)} xyz.swapee.wc.IProgressColumnCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnOuterCore} xyz.swapee.wc.IProgressColumnOuterCore.typeof */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IProgressColumnCore
 */
xyz.swapee.wc.IProgressColumnCore = class extends /** @type {xyz.swapee.wc.IProgressColumnCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IProgressColumnOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IProgressColumnCore.resetCore} */
xyz.swapee.wc.IProgressColumnCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore} */
xyz.swapee.wc.IProgressColumnCore.prototype.resetProgressColumnCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnCore&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnCore.Initialese>)} xyz.swapee.wc.ProgressColumnCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnCore} xyz.swapee.wc.IProgressColumnCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IProgressColumnCore_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnCore
 * @implements {xyz.swapee.wc.IProgressColumnCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnCore.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnCore = class extends /** @type {xyz.swapee.wc.ProgressColumnCore.constructor&xyz.swapee.wc.IProgressColumnCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ProgressColumnCore.prototype.constructor = xyz.swapee.wc.ProgressColumnCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnCore}
 */
xyz.swapee.wc.ProgressColumnCore.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnCore.
 * @interface xyz.swapee.wc.IProgressColumnCoreFields
 */
xyz.swapee.wc.IProgressColumnCoreFields = class { }
/**
 * The _IProgressColumn_'s memory.
 */
xyz.swapee.wc.IProgressColumnCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IProgressColumnCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IProgressColumnCoreFields.prototype.props = /** @type {xyz.swapee.wc.IProgressColumnCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnCore} */
xyz.swapee.wc.RecordIProgressColumnCore

/** @typedef {xyz.swapee.wc.IProgressColumnCore} xyz.swapee.wc.BoundIProgressColumnCore */

/** @typedef {xyz.swapee.wc.ProgressColumnCore} xyz.swapee.wc.BoundProgressColumnCore */

/**
 * The third step, where the moneys is exchanged and sent to the user.
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction.processingTransaction

/**
 * The third step, where the moneys is exchanged and sent to the user.
 * @typedef {string}
 */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus.transactionStatus

/**
 * Everything is done.
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted.transactionCompleted

/**
 * Transaction wasn't successful.
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed.transactionFailed

/** @typedef {xyz.swapee.wc.IProgressColumnOuterCore.Model&xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction&xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus&xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted&xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed} xyz.swapee.wc.IProgressColumnCore.Model The _IProgressColumn_'s memory. */

/**
 * Contains getters to cast the _IProgressColumnCore_ interface.
 * @interface xyz.swapee.wc.IProgressColumnCoreCaster
 */
xyz.swapee.wc.IProgressColumnCoreCaster = class { }
/**
 * Cast the _IProgressColumnCore_ instance into the _BoundIProgressColumnCore_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnCore}
 */
xyz.swapee.wc.IProgressColumnCoreCaster.prototype.asIProgressColumnCore
/**
 * Access the _ProgressColumnCore_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnCore}
 */
xyz.swapee.wc.IProgressColumnCoreCaster.prototype.superProgressColumnCore

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction The third step, where the moneys is exchanged and sent to the user (optional overlay).
 * @prop {boolean} [processingTransaction=false] The third step, where the moneys is exchanged and sent to the user. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.ProcessingTransaction_Safe The third step, where the moneys is exchanged and sent to the user (required overlay).
 * @prop {boolean} processingTransaction The third step, where the moneys is exchanged and sent to the user.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus The third step, where the moneys is exchanged and sent to the user (optional overlay).
 * @prop {'exchanging'|'sending'|'finished'|'failed'} [transactionStatus=""] The third step, where the moneys is exchanged and sent to the user.
 * Can be either:
 * - _exchanging_
 * - _sending_
 * - _finished_
 * - _failed_
 *
 * Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.TransactionStatus_Safe The third step, where the moneys is exchanged and sent to the user (required overlay).
 * @prop {'exchanging'|'sending'|'finished'|'failed'} transactionStatus The third step, where the moneys is exchanged and sent to the user.
 * Can be either:
 * - _exchanging_
 * - _sending_
 * - _finished_
 * - _failed_
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted Everything is done (optional overlay).
 * @prop {boolean} [transactionCompleted=false] Everything is done. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.TransactionCompleted_Safe Everything is done (required overlay).
 * @prop {boolean} transactionCompleted Everything is done.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed Transaction wasn't successful (optional overlay).
 * @prop {boolean} [transactionFailed=false] Transaction wasn't successful. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnCore.Model.TransactionFailed_Safe Transaction wasn't successful (required overlay).
 * @prop {boolean} transactionFailed Transaction wasn't successful.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IProgressColumnCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnCore.__resetCore<!xyz.swapee.wc.IProgressColumnCore>} xyz.swapee.wc.IProgressColumnCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnCore.resetCore} */
/**
 * Resets the _IProgressColumn_ core.
 * @return {void}
 */
xyz.swapee.wc.IProgressColumnCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnCore.__resetProgressColumnCore<!xyz.swapee.wc.IProgressColumnCore>} xyz.swapee.wc.IProgressColumnCore._resetProgressColumnCore */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IProgressColumnCore.resetProgressColumnCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IProgressColumnCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/10-IProgressColumnProcessor.xml}  8723c25e23481a44687ce599ad817242 */
/** @typedef {xyz.swapee.wc.IProgressColumnComputer.Initialese&xyz.swapee.wc.IProgressColumnController.Initialese} xyz.swapee.wc.IProgressColumnProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnProcessor)} xyz.swapee.wc.AbstractProgressColumnProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnProcessor} xyz.swapee.wc.ProgressColumnProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnProcessor
 */
xyz.swapee.wc.AbstractProgressColumnProcessor = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnProcessor.constructor&xyz.swapee.wc.ProgressColumnProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnProcessor.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnCore|typeof xyz.swapee.wc.ProgressColumnCore)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.AbstractProgressColumnProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnProcessor.Initialese[]) => xyz.swapee.wc.IProgressColumnProcessor} xyz.swapee.wc.ProgressColumnProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnProcessorCaster&xyz.swapee.wc.IProgressColumnComputer&xyz.swapee.wc.IProgressColumnCore&xyz.swapee.wc.IProgressColumnController)} xyz.swapee.wc.IProgressColumnProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnController} xyz.swapee.wc.IProgressColumnController.typeof */
/**
 * The processor to compute changes to the memory for the _IProgressColumn_.
 * @interface xyz.swapee.wc.IProgressColumnProcessor
 */
xyz.swapee.wc.IProgressColumnProcessor = class extends /** @type {xyz.swapee.wc.IProgressColumnProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IProgressColumnComputer.typeof&xyz.swapee.wc.IProgressColumnCore.typeof&xyz.swapee.wc.IProgressColumnController.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnProcessor.Initialese>)} xyz.swapee.wc.ProgressColumnProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnProcessor} xyz.swapee.wc.IProgressColumnProcessor.typeof */
/**
 * A concrete class of _IProgressColumnProcessor_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnProcessor
 * @implements {xyz.swapee.wc.IProgressColumnProcessor} The processor to compute changes to the memory for the _IProgressColumn_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnProcessor.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnProcessor = class extends /** @type {xyz.swapee.wc.ProgressColumnProcessor.constructor&xyz.swapee.wc.IProgressColumnProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnProcessor}
 */
xyz.swapee.wc.ProgressColumnProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IProgressColumnProcessor} */
xyz.swapee.wc.RecordIProgressColumnProcessor

/** @typedef {xyz.swapee.wc.IProgressColumnProcessor} xyz.swapee.wc.BoundIProgressColumnProcessor */

/** @typedef {xyz.swapee.wc.ProgressColumnProcessor} xyz.swapee.wc.BoundProgressColumnProcessor */

/**
 * Contains getters to cast the _IProgressColumnProcessor_ interface.
 * @interface xyz.swapee.wc.IProgressColumnProcessorCaster
 */
xyz.swapee.wc.IProgressColumnProcessorCaster = class { }
/**
 * Cast the _IProgressColumnProcessor_ instance into the _BoundIProgressColumnProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnProcessor}
 */
xyz.swapee.wc.IProgressColumnProcessorCaster.prototype.asIProgressColumnProcessor
/**
 * Access the _ProgressColumnProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnProcessor}
 */
xyz.swapee.wc.IProgressColumnProcessorCaster.prototype.superProgressColumnProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/100-ProgressColumnMemory.xml}  0c1f8beb29433d5699e2027f2c36594b */
/**
 * The memory of the _IProgressColumn_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.ProgressColumnMemory
 */
xyz.swapee.wc.ProgressColumnMemory = class { }
/**
 * The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.creatingTransaction = /** @type {boolean} */ (void 0)
/**
 * Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.finishedOnPayment = /** @type {boolean} */ (void 0)
/**
 * The payment during step 2 did not succeed. Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.paymentFailed = /** @type {boolean} */ (void 0)
/**
 * The second, payment step, was successful. Moving on to the third step. Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.paymentCompleted = /** @type {boolean} */ (void 0)
/**
 * The ID of the created transaction. Default empty string.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.transactionId = /** @type {string} */ (void 0)
/**
 * The status of the payment. Default empty string.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.paymentStatus = /** @type {string} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep1 = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep2 = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.loadingStep3 = /** @type {boolean} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.status = /** @type {string} */ (void 0)
/**
 * The third step, where the moneys is exchanged and sent to the user. Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.processingTransaction = /** @type {boolean} */ (void 0)
/**
 * The third step, where the moneys is exchanged and sent to the user. Default empty string.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.transactionStatus = /** @type {string} */ (void 0)
/**
 * Everything is done. Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.transactionCompleted = /** @type {boolean} */ (void 0)
/**
 * Transaction wasn't successful. Default `false`.
 */
xyz.swapee.wc.ProgressColumnMemory.prototype.transactionFailed = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/102-ProgressColumnInputs.xml}  2419226a56c9a63508dfed6916ac6a1b */
/**
 * The inputs of the _IProgressColumn_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.ProgressColumnInputs
 */
xyz.swapee.wc.front.ProgressColumnInputs = class { }
/**
 * The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.creatingTransaction = /** @type {boolean|undefined} */ (void 0)
/**
 * Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.finishedOnPayment = /** @type {boolean|undefined} */ (void 0)
/**
 * The payment during step 2 did not succeed. Default `false`.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentFailed = /** @type {boolean|undefined} */ (void 0)
/**
 * The second, payment step, was successful. Moving on to the third step. Default `false`.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentCompleted = /** @type {boolean|undefined} */ (void 0)
/**
 * The ID of the created transaction. Default empty string.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.transactionId = /** @type {string|undefined} */ (void 0)
/**
 * The status of the payment. Default empty string.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.paymentStatus = /** @type {string|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep1 = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep2 = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.loadingStep3 = /** @type {boolean|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.ProgressColumnInputs.prototype.status = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/11-IProgressColumn.xml}  9816a6f589f743d7c32de1ab18938663 */
/**
 * An atomic wrapper for the _IProgressColumn_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.ProgressColumnEnv
 */
xyz.swapee.wc.ProgressColumnEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.ProgressColumnEnv.prototype.progressColumn = /** @type {xyz.swapee.wc.IProgressColumn} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs>&xyz.swapee.wc.IProgressColumnProcessor.Initialese&xyz.swapee.wc.IProgressColumnComputer.Initialese&xyz.swapee.wc.IProgressColumnController.Initialese} xyz.swapee.wc.IProgressColumn.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumn)} xyz.swapee.wc.AbstractProgressColumn.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumn} xyz.swapee.wc.ProgressColumn.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumn` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumn
 */
xyz.swapee.wc.AbstractProgressColumn = class extends /** @type {xyz.swapee.wc.AbstractProgressColumn.constructor&xyz.swapee.wc.ProgressColumn.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumn.prototype.constructor = xyz.swapee.wc.AbstractProgressColumn
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumn.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumn} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumn.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.AbstractProgressColumn.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumn.Initialese[]) => xyz.swapee.wc.IProgressColumn} xyz.swapee.wc.ProgressColumnConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumn.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IProgressColumn.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IProgressColumn.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IProgressColumn.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.ProgressColumnMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.ProgressColumnClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnCaster&xyz.swapee.wc.IProgressColumnProcessor&xyz.swapee.wc.IProgressColumnComputer&xyz.swapee.wc.IProgressColumnController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, null>)} xyz.swapee.wc.IProgressColumn.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IProgressColumn
 */
xyz.swapee.wc.IProgressColumn = class extends /** @type {xyz.swapee.wc.IProgressColumn.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IProgressColumnProcessor.typeof&xyz.swapee.wc.IProgressColumnComputer.typeof&xyz.swapee.wc.IProgressColumnController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumn* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumn.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumn&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumn.Initialese>)} xyz.swapee.wc.ProgressColumn.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumn} xyz.swapee.wc.IProgressColumn.typeof */
/**
 * A concrete class of _IProgressColumn_ instances.
 * @constructor xyz.swapee.wc.ProgressColumn
 * @implements {xyz.swapee.wc.IProgressColumn} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumn.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumn = class extends /** @type {xyz.swapee.wc.ProgressColumn.constructor&xyz.swapee.wc.IProgressColumn.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumn* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumn* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumn.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumn.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumn}
 */
xyz.swapee.wc.ProgressColumn.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumn.
 * @interface xyz.swapee.wc.IProgressColumnFields
 */
xyz.swapee.wc.IProgressColumnFields = class { }
/**
 * The input pins of the _IProgressColumn_ port.
 */
xyz.swapee.wc.IProgressColumnFields.prototype.pinout = /** @type {!xyz.swapee.wc.IProgressColumn.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumn} */
xyz.swapee.wc.RecordIProgressColumn

/** @typedef {xyz.swapee.wc.IProgressColumn} xyz.swapee.wc.BoundIProgressColumn */

/** @typedef {xyz.swapee.wc.ProgressColumn} xyz.swapee.wc.BoundProgressColumn */

/** @typedef {xyz.swapee.wc.IProgressColumnController.Inputs} xyz.swapee.wc.IProgressColumn.Pinout The input pins of the _IProgressColumn_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>)} xyz.swapee.wc.IProgressColumnBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IProgressColumnBuffer
 */
xyz.swapee.wc.IProgressColumnBuffer = class extends /** @type {xyz.swapee.wc.IProgressColumnBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IProgressColumnBuffer.prototype.constructor = xyz.swapee.wc.IProgressColumnBuffer

/**
 * A concrete class of _IProgressColumnBuffer_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnBuffer
 * @implements {xyz.swapee.wc.IProgressColumnBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.ProgressColumnBuffer = class extends xyz.swapee.wc.IProgressColumnBuffer { }
xyz.swapee.wc.ProgressColumnBuffer.prototype.constructor = xyz.swapee.wc.ProgressColumnBuffer

/**
 * Contains getters to cast the _IProgressColumn_ interface.
 * @interface xyz.swapee.wc.IProgressColumnCaster
 */
xyz.swapee.wc.IProgressColumnCaster = class { }
/**
 * Cast the _IProgressColumn_ instance into the _BoundIProgressColumn_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumn}
 */
xyz.swapee.wc.IProgressColumnCaster.prototype.asIProgressColumn
/**
 * Access the _ProgressColumn_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumn}
 */
xyz.swapee.wc.IProgressColumnCaster.prototype.superProgressColumn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/110-ProgressColumnSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ProgressColumnMemoryPQs
 */
xyz.swapee.wc.ProgressColumnMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
    /**
     * `i9fc6`
     */
    this.creatingTransaction=/** @type {string} */ (void 0)
    /**
     * `a7265`
     */
    this.finishedOnPayment=/** @type {string} */ (void 0)
    /**
     * `a2236`
     */
    this.paymentFailed=/** @type {string} */ (void 0)
    /**
     * `e97c0`
     */
    this.paymentCompleted=/** @type {string} */ (void 0)
    /**
     * `g7113`
     */
    this.transactionId=/** @type {string} */ (void 0)
    /**
     * `b5666`
     */
    this.paymentStatus=/** @type {string} */ (void 0)
    /**
     * `jacb4`
     */
    this.status=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ProgressColumnMemoryPQs.prototype.constructor = xyz.swapee.wc.ProgressColumnMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ProgressColumnMemoryQPs
 * @dict
 */
xyz.swapee.wc.ProgressColumnMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)
/**
 * `creatingTransaction`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.i9fc6 = /** @type {string} */ (void 0)
/**
 * `finishedOnPayment`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a7265 = /** @type {string} */ (void 0)
/**
 * `paymentFailed`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.a2236 = /** @type {string} */ (void 0)
/**
 * `paymentCompleted`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.e97c0 = /** @type {string} */ (void 0)
/**
 * `transactionId`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.g7113 = /** @type {string} */ (void 0)
/**
 * `paymentStatus`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.b5666 = /** @type {string} */ (void 0)
/**
 * `status`
 */
xyz.swapee.wc.ProgressColumnMemoryQPs.prototype.jacb4 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnMemoryPQs)} xyz.swapee.wc.ProgressColumnInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnMemoryPQs} xyz.swapee.wc.ProgressColumnMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ProgressColumnInputsPQs
 */
xyz.swapee.wc.ProgressColumnInputsPQs = class extends /** @type {xyz.swapee.wc.ProgressColumnInputsPQs.constructor&xyz.swapee.wc.ProgressColumnMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ProgressColumnInputsPQs.prototype.constructor = xyz.swapee.wc.ProgressColumnInputsPQs

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnMemoryPQs)} xyz.swapee.wc.ProgressColumnInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ProgressColumnInputsQPs
 * @dict
 */
xyz.swapee.wc.ProgressColumnInputsQPs = class extends /** @type {xyz.swapee.wc.ProgressColumnInputsQPs.constructor&xyz.swapee.wc.ProgressColumnMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ProgressColumnInputsQPs.prototype.constructor = xyz.swapee.wc.ProgressColumnInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ProgressColumnCachePQs
 */
xyz.swapee.wc.ProgressColumnCachePQs = class {
  constructor() {
    /**
     * `h121e`
     */
    this.processingTransaction=/** @type {string} */ (void 0)
    /**
     * `b274c`
     */
    this.transactionStatus=/** @type {string} */ (void 0)
    /**
     * `de5a0`
     */
    this.transactionCompleted=/** @type {string} */ (void 0)
    /**
     * `f17c7`
     */
    this.transactionFailed=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ProgressColumnCachePQs.prototype.constructor = xyz.swapee.wc.ProgressColumnCachePQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ProgressColumnCacheQPs
 * @dict
 */
xyz.swapee.wc.ProgressColumnCacheQPs = class { }
/**
 * `processingTransaction`
 */
xyz.swapee.wc.ProgressColumnCacheQPs.prototype.h121e = /** @type {string} */ (void 0)
/**
 * `transactionStatus`
 */
xyz.swapee.wc.ProgressColumnCacheQPs.prototype.b274c = /** @type {string} */ (void 0)
/**
 * `transactionCompleted`
 */
xyz.swapee.wc.ProgressColumnCacheQPs.prototype.de5a0 = /** @type {string} */ (void 0)
/**
 * `transactionFailed`
 */
xyz.swapee.wc.ProgressColumnCacheQPs.prototype.f17c7 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ProgressColumnVdusPQs
 */
xyz.swapee.wc.ProgressColumnVdusPQs = class {
  constructor() {
    /**
     * `fa9e1`
     */
    this.TransactionIdWr=/** @type {string} */ (void 0)
    /**
     * `fa9e2`
     */
    this.Step1Circle=/** @type {string} */ (void 0)
    /**
     * `fa9e3`
     */
    this.Step2Circle=/** @type {string} */ (void 0)
    /**
     * `fa9e4`
     */
    this.Step3Circle=/** @type {string} */ (void 0)
    /**
     * `fa9e5`
     */
    this.Step2La=/** @type {string} */ (void 0)
    /**
     * `fa9e6`
     */
    this.TransactionIdLa=/** @type {string} */ (void 0)
    /**
     * `fa9e7`
     */
    this.PaymentFinalStatusWr=/** @type {string} */ (void 0)
    /**
     * `fa9e8`
     */
    this.PaymentFinalStatusLa=/** @type {string} */ (void 0)
    /**
     * `fa9e9`
     */
    this.Step3Wr=/** @type {string} */ (void 0)
    /**
     * `fa9e10`
     */
    this.Step2BotSep=/** @type {string} */ (void 0)
    /**
     * `fa9e11`
     */
    this.Step1=/** @type {string} */ (void 0)
    /**
     * `fa9e12`
     */
    this.Step1La=/** @type {string} */ (void 0)
    /**
     * `fa9e13`
     */
    this.Step1ProgressCircleWr=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ProgressColumnVdusPQs.prototype.constructor = xyz.swapee.wc.ProgressColumnVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ProgressColumnVdusQPs
 * @dict
 */
xyz.swapee.wc.ProgressColumnVdusQPs = class { }
/**
 * `TransactionIdWr`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e1 = /** @type {string} */ (void 0)
/**
 * `Step1Circle`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e2 = /** @type {string} */ (void 0)
/**
 * `Step2Circle`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e3 = /** @type {string} */ (void 0)
/**
 * `Step3Circle`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e4 = /** @type {string} */ (void 0)
/**
 * `Step2La`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e5 = /** @type {string} */ (void 0)
/**
 * `TransactionIdLa`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e6 = /** @type {string} */ (void 0)
/**
 * `PaymentFinalStatusWr`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e7 = /** @type {string} */ (void 0)
/**
 * `PaymentFinalStatusLa`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e8 = /** @type {string} */ (void 0)
/**
 * `Step3Wr`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e9 = /** @type {string} */ (void 0)
/**
 * `Step2BotSep`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e10 = /** @type {string} */ (void 0)
/**
 * `Step1`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e11 = /** @type {string} */ (void 0)
/**
 * `Step1La`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e12 = /** @type {string} */ (void 0)
/**
 * `Step1ProgressCircleWr`
 */
xyz.swapee.wc.ProgressColumnVdusQPs.prototype.fa9e13 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ProgressColumnClassesPQs
 */
xyz.swapee.wc.ProgressColumnClassesPQs = class {
  constructor() {
    /**
     * `fe522`
     */
    this.CircleLoading=/** @type {string} */ (void 0)
    /**
     * `a5ad9`
     */
    this.CircleComplete=/** @type {string} */ (void 0)
    /**
     * `j912b`
     */
    this.CircleWaiting=/** @type {string} */ (void 0)
    /**
     * `dc9d7`
     */
    this.CircleFailed=/** @type {string} */ (void 0)
    /**
     * `he898`
     */
    this.StepLabelFailed=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ProgressColumnClassesPQs.prototype.constructor = xyz.swapee.wc.ProgressColumnClassesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ProgressColumnClassesQPs
 * @dict
 */
xyz.swapee.wc.ProgressColumnClassesQPs = class { }
/**
 * `CircleLoading`
 */
xyz.swapee.wc.ProgressColumnClassesQPs.prototype.fe522 = /** @type {string} */ (void 0)
/**
 * `CircleComplete`
 */
xyz.swapee.wc.ProgressColumnClassesQPs.prototype.a5ad9 = /** @type {string} */ (void 0)
/**
 * `CircleWaiting`
 */
xyz.swapee.wc.ProgressColumnClassesQPs.prototype.j912b = /** @type {string} */ (void 0)
/**
 * `CircleFailed`
 */
xyz.swapee.wc.ProgressColumnClassesQPs.prototype.dc9d7 = /** @type {string} */ (void 0)
/**
 * `StepLabelFailed`
 */
xyz.swapee.wc.ProgressColumnClassesQPs.prototype.he898 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/12-IProgressColumnHtmlComponent.xml}  c2f421ff8781214d28ff29e33b8fa549 */
/** @typedef {xyz.swapee.wc.back.IProgressColumnController.Initialese&xyz.swapee.wc.back.IProgressColumnScreen.Initialese&xyz.swapee.wc.IProgressColumn.Initialese&xyz.swapee.wc.IProgressColumnGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IProgressColumnProcessor.Initialese&xyz.swapee.wc.IProgressColumnComputer.Initialese} xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnHtmlComponent)} xyz.swapee.wc.AbstractProgressColumnHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnHtmlComponent} xyz.swapee.wc.ProgressColumnHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnHtmlComponent
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnHtmlComponent.constructor&xyz.swapee.wc.ProgressColumnHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnHtmlComponent|typeof xyz.swapee.wc.ProgressColumnHtmlComponent)|(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.IProgressColumn|typeof xyz.swapee.wc.ProgressColumn)|(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IProgressColumnProcessor|typeof xyz.swapee.wc.ProgressColumnProcessor)|(!xyz.swapee.wc.IProgressColumnComputer|typeof xyz.swapee.wc.ProgressColumnComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.AbstractProgressColumnHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese[]) => xyz.swapee.wc.IProgressColumnHtmlComponent} xyz.swapee.wc.ProgressColumnHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnHtmlComponentCaster&xyz.swapee.wc.back.IProgressColumnController&xyz.swapee.wc.back.IProgressColumnScreen&xyz.swapee.wc.IProgressColumn&xyz.swapee.wc.IProgressColumnGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IProgressColumnProcessor&xyz.swapee.wc.IProgressColumnComputer)} xyz.swapee.wc.IProgressColumnHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IProgressColumnController} xyz.swapee.wc.back.IProgressColumnController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IProgressColumnScreen} xyz.swapee.wc.back.IProgressColumnScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnGPU} xyz.swapee.wc.IProgressColumnGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IProgressColumn_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IProgressColumnHtmlComponent
 */
xyz.swapee.wc.IProgressColumnHtmlComponent = class extends /** @type {xyz.swapee.wc.IProgressColumnHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IProgressColumnController.typeof&xyz.swapee.wc.back.IProgressColumnScreen.typeof&xyz.swapee.wc.IProgressColumn.typeof&xyz.swapee.wc.IProgressColumnGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IProgressColumnProcessor.typeof&xyz.swapee.wc.IProgressColumnComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese>)} xyz.swapee.wc.ProgressColumnHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnHtmlComponent} xyz.swapee.wc.IProgressColumnHtmlComponent.typeof */
/**
 * A concrete class of _IProgressColumnHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnHtmlComponent
 * @implements {xyz.swapee.wc.IProgressColumnHtmlComponent} The _IProgressColumn_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnHtmlComponent = class extends /** @type {xyz.swapee.wc.ProgressColumnHtmlComponent.constructor&xyz.swapee.wc.IProgressColumnHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
xyz.swapee.wc.ProgressColumnHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IProgressColumnHtmlComponent} */
xyz.swapee.wc.RecordIProgressColumnHtmlComponent

/** @typedef {xyz.swapee.wc.IProgressColumnHtmlComponent} xyz.swapee.wc.BoundIProgressColumnHtmlComponent */

/** @typedef {xyz.swapee.wc.ProgressColumnHtmlComponent} xyz.swapee.wc.BoundProgressColumnHtmlComponent */

/**
 * Contains getters to cast the _IProgressColumnHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IProgressColumnHtmlComponentCaster
 */
xyz.swapee.wc.IProgressColumnHtmlComponentCaster = class { }
/**
 * Cast the _IProgressColumnHtmlComponent_ instance into the _BoundIProgressColumnHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnHtmlComponent}
 */
xyz.swapee.wc.IProgressColumnHtmlComponentCaster.prototype.asIProgressColumnHtmlComponent
/**
 * Access the _ProgressColumnHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnHtmlComponent}
 */
xyz.swapee.wc.IProgressColumnHtmlComponentCaster.prototype.superProgressColumnHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/130-IProgressColumnElement.xml}  c76b1f480eae7c752b4762311da393f6 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IProgressColumnElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnElement)} xyz.swapee.wc.AbstractProgressColumnElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnElement} xyz.swapee.wc.ProgressColumnElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnElement` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnElement
 */
xyz.swapee.wc.AbstractProgressColumnElement = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnElement.constructor&xyz.swapee.wc.ProgressColumnElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnElement.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnElement.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnElement|typeof xyz.swapee.wc.ProgressColumnElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnElement|typeof xyz.swapee.wc.ProgressColumnElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnElement|typeof xyz.swapee.wc.ProgressColumnElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.AbstractProgressColumnElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnElement.Initialese[]) => xyz.swapee.wc.IProgressColumnElement} xyz.swapee.wc.ProgressColumnElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElementFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.IProgressColumnElement.Inputs, null>)} xyz.swapee.wc.IProgressColumnElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IProgressColumn_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IProgressColumnElement
 */
xyz.swapee.wc.IProgressColumnElement = class extends /** @type {xyz.swapee.wc.IProgressColumnElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IProgressColumnElement.solder} */
xyz.swapee.wc.IProgressColumnElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IProgressColumnElement.render} */
xyz.swapee.wc.IProgressColumnElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IProgressColumnElement.server} */
xyz.swapee.wc.IProgressColumnElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IProgressColumnElement.inducer} */
xyz.swapee.wc.IProgressColumnElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElement&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnElement.Initialese>)} xyz.swapee.wc.ProgressColumnElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElement} xyz.swapee.wc.IProgressColumnElement.typeof */
/**
 * A concrete class of _IProgressColumnElement_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnElement
 * @implements {xyz.swapee.wc.IProgressColumnElement} A component description.
 *
 * The _IProgressColumn_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnElement.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnElement = class extends /** @type {xyz.swapee.wc.ProgressColumnElement.constructor&xyz.swapee.wc.IProgressColumnElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnElement}
 */
xyz.swapee.wc.ProgressColumnElement.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnElement.
 * @interface xyz.swapee.wc.IProgressColumnElementFields
 */
xyz.swapee.wc.IProgressColumnElementFields = class { }
/**
 * The element-specific inputs to the _IProgressColumn_ component.
 */
xyz.swapee.wc.IProgressColumnElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IProgressColumnElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnElement} */
xyz.swapee.wc.RecordIProgressColumnElement

/** @typedef {xyz.swapee.wc.IProgressColumnElement} xyz.swapee.wc.BoundIProgressColumnElement */

/** @typedef {xyz.swapee.wc.ProgressColumnElement} xyz.swapee.wc.BoundProgressColumnElement */

/** @typedef {xyz.swapee.wc.IProgressColumnPort.Inputs&xyz.swapee.wc.IProgressColumnDisplay.Queries&xyz.swapee.wc.IProgressColumnController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IProgressColumnElementPort.Inputs} xyz.swapee.wc.IProgressColumnElement.Inputs The element-specific inputs to the _IProgressColumn_ component. */

/**
 * Contains getters to cast the _IProgressColumnElement_ interface.
 * @interface xyz.swapee.wc.IProgressColumnElementCaster
 */
xyz.swapee.wc.IProgressColumnElementCaster = class { }
/**
 * Cast the _IProgressColumnElement_ instance into the _BoundIProgressColumnElement_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnElement}
 */
xyz.swapee.wc.IProgressColumnElementCaster.prototype.asIProgressColumnElement
/**
 * Access the _ProgressColumnElement_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnElement}
 */
xyz.swapee.wc.IProgressColumnElementCaster.prototype.superProgressColumnElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ProgressColumnMemory, props: !xyz.swapee.wc.IProgressColumnElement.Inputs) => Object<string, *>} xyz.swapee.wc.IProgressColumnElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnElement.__solder<!xyz.swapee.wc.IProgressColumnElement>} xyz.swapee.wc.IProgressColumnElement._solder */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.ProgressColumnMemory} model The model.
 * - `core` _string_ The core property. Default empty string.
 * - `creatingTransaction` _boolean_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 * - `finishedOnPayment` _boolean_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 * - `paymentFailed` _boolean_ The payment during step 2 did not succeed. Default `false`.
 * - `paymentCompleted` _boolean_ The second, payment step, was successful. Moving on to the third step. Default `false`.
 * - `transactionId` _string_ The ID of the created transaction. Default empty string.
 * - `paymentStatus` _string_ The status of the payment.
 * Choose between:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default empty string.
 * - `status` _string_ Default empty string.
 * - `processingTransaction` _boolean_ The third step, where the moneys is exchanged and sent to the user. Default `false`.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user.
 * One of:
 * > - _exchaning_
 * > - _sending_
 * > - _finished_
 * > - _failed_Default empty string.
 * - `transactionCompleted` _boolean_ Everything is done. Default `false`.
 * - `transactionFailed` _boolean_ Transaction wasn't successful. Default `false`.
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} props The element props.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IProgressColumnOuterCore.WeakModel.Core* ⤴ *IProgressColumnOuterCore.WeakModel.Core* Default `null`.
 * - `[creatingTransaction=null]` _&#42;?_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. ⤴ *IProgressColumnOuterCore.WeakModel.CreatingTransaction* ⤴ *IProgressColumnOuterCore.WeakModel.CreatingTransaction* Default `null`.
 * - `[finishedOnPayment=null]` _&#42;?_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. ⤴ *IProgressColumnOuterCore.WeakModel.FinishedOnPayment* ⤴ *IProgressColumnOuterCore.WeakModel.FinishedOnPayment* Default `null`.
 * - `[paymentFailed=null]` _&#42;?_ The payment during step 2 did not succeed. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentFailed* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentFailed* Default `null`.
 * - `[paymentCompleted=null]` _&#42;?_ The second, payment step, was successful. Moving on to the third step. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentCompleted* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentCompleted* Default `null`.
 * - `[transactionId=null]` _&#42;?_ The ID of the created transaction. ⤴ *IProgressColumnOuterCore.WeakModel.TransactionId* ⤴ *IProgressColumnOuterCore.WeakModel.TransactionId* Default `null`.
 * - `[paymentStatus=null]` _&#42;?_ The status of the payment. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentStatus* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentStatus*
 * Choose between:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default `null`.
 * - `[status=null]` _&#42;?_ ⤴ *IProgressColumnOuterCore.WeakModel.Status* ⤴ *IProgressColumnOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IProgressColumnElementPort.Inputs.NoSolder* Default `false`.
 * - `[paymentFinalStatusWrOpts]` _!Object?_ The options to pass to the _PaymentFinalStatusWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.PaymentFinalStatusWrOpts* Default `{}`.
 * - `[paymentFinalStatusLaOpts]` _!Object?_ The options to pass to the _PaymentFinalStatusLa_ vdu. ⤴ *IProgressColumnElementPort.Inputs.PaymentFinalStatusLaOpts* Default `{}`.
 * - `[step2BotSepOpts]` _!Object?_ The options to pass to the _Step2BotSep_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2BotSepOpts* Default `{}`.
 * - `[step1Opts]` _!Object?_ The options to pass to the _Step1_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1Opts* Default `{}`.
 * - `[step1LaOpts]` _!Object?_ The options to pass to the _Step1La_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1LaOpts* Default `{}`.
 * - `[step2LaOpts]` _!Object?_ The options to pass to the _Step2La_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2LaOpts* Default `{}`.
 * - `[transactionIdWrOpts]` _!Object?_ The options to pass to the _TransactionIdWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.TransactionIdWrOpts* Default `{}`.
 * - `[transactionIdLaOpts]` _!Object?_ The options to pass to the _TransactionIdLa_ vdu. ⤴ *IProgressColumnElementPort.Inputs.TransactionIdLaOpts* Default `{}`.
 * - `[step1ProgressCircleWrOpts]` _!Object?_ The options to pass to the _Step1ProgressCircleWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts* Default `{}`.
 * - `[step3WrOpts]` _!Object?_ The options to pass to the _Step3Wr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step3WrOpts* Default `{}`.
 * - `[step1CircleOpts]` _!Object?_ The options to pass to the _Step1Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1CircleOpts* Default `{}`.
 * - `[step2CircleOpts]` _!Object?_ The options to pass to the _Step2Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2CircleOpts* Default `{}`.
 * - `[step3CircleOpts]` _!Object?_ The options to pass to the _Step3Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step3CircleOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IProgressColumnElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ProgressColumnMemory, instance?: !xyz.swapee.wc.IProgressColumnScreen&xyz.swapee.wc.IProgressColumnController) => !engineering.type.VNode} xyz.swapee.wc.IProgressColumnElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnElement.__render<!xyz.swapee.wc.IProgressColumnElement>} xyz.swapee.wc.IProgressColumnElement._render */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model] The model for the view.
 * - `core` _string_ The core property. Default empty string.
 * - `creatingTransaction` _boolean_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 * - `finishedOnPayment` _boolean_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 * - `paymentFailed` _boolean_ The payment during step 2 did not succeed. Default `false`.
 * - `paymentCompleted` _boolean_ The second, payment step, was successful. Moving on to the third step. Default `false`.
 * - `transactionId` _string_ The ID of the created transaction. Default empty string.
 * - `paymentStatus` _string_ The status of the payment.
 * Choose between:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default empty string.
 * - `status` _string_ Default empty string.
 * - `processingTransaction` _boolean_ The third step, where the moneys is exchanged and sent to the user. Default `false`.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user.
 * One of:
 * > - _exchaning_
 * > - _sending_
 * > - _finished_
 * > - _failed_Default empty string.
 * - `transactionCompleted` _boolean_ Everything is done. Default `false`.
 * - `transactionFailed` _boolean_ Transaction wasn't successful. Default `false`.
 * @param {!xyz.swapee.wc.IProgressColumnScreen&xyz.swapee.wc.IProgressColumnController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IProgressColumnElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ProgressColumnMemory, inputs: !xyz.swapee.wc.IProgressColumnElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IProgressColumnElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnElement.__server<!xyz.swapee.wc.IProgressColumnElement>} xyz.swapee.wc.IProgressColumnElement._server */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory The memory registers.
 * - `core` _string_ The core property. Default empty string.
 * - `creatingTransaction` _boolean_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 * - `finishedOnPayment` _boolean_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 * - `paymentFailed` _boolean_ The payment during step 2 did not succeed. Default `false`.
 * - `paymentCompleted` _boolean_ The second, payment step, was successful. Moving on to the third step. Default `false`.
 * - `transactionId` _string_ The ID of the created transaction. Default empty string.
 * - `paymentStatus` _string_ The status of the payment.
 * Choose between:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default empty string.
 * - `status` _string_ Default empty string.
 * - `processingTransaction` _boolean_ The third step, where the moneys is exchanged and sent to the user. Default `false`.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user.
 * One of:
 * > - _exchaning_
 * > - _sending_
 * > - _finished_
 * > - _failed_Default empty string.
 * - `transactionCompleted` _boolean_ Everything is done. Default `false`.
 * - `transactionFailed` _boolean_ Transaction wasn't successful. Default `false`.
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} inputs The inputs to the port.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IProgressColumnOuterCore.WeakModel.Core* ⤴ *IProgressColumnOuterCore.WeakModel.Core* Default `null`.
 * - `[creatingTransaction=null]` _&#42;?_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. ⤴ *IProgressColumnOuterCore.WeakModel.CreatingTransaction* ⤴ *IProgressColumnOuterCore.WeakModel.CreatingTransaction* Default `null`.
 * - `[finishedOnPayment=null]` _&#42;?_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. ⤴ *IProgressColumnOuterCore.WeakModel.FinishedOnPayment* ⤴ *IProgressColumnOuterCore.WeakModel.FinishedOnPayment* Default `null`.
 * - `[paymentFailed=null]` _&#42;?_ The payment during step 2 did not succeed. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentFailed* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentFailed* Default `null`.
 * - `[paymentCompleted=null]` _&#42;?_ The second, payment step, was successful. Moving on to the third step. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentCompleted* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentCompleted* Default `null`.
 * - `[transactionId=null]` _&#42;?_ The ID of the created transaction. ⤴ *IProgressColumnOuterCore.WeakModel.TransactionId* ⤴ *IProgressColumnOuterCore.WeakModel.TransactionId* Default `null`.
 * - `[paymentStatus=null]` _&#42;?_ The status of the payment. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentStatus* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentStatus*
 * Choose between:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default `null`.
 * - `[status=null]` _&#42;?_ ⤴ *IProgressColumnOuterCore.WeakModel.Status* ⤴ *IProgressColumnOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IProgressColumnElementPort.Inputs.NoSolder* Default `false`.
 * - `[paymentFinalStatusWrOpts]` _!Object?_ The options to pass to the _PaymentFinalStatusWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.PaymentFinalStatusWrOpts* Default `{}`.
 * - `[paymentFinalStatusLaOpts]` _!Object?_ The options to pass to the _PaymentFinalStatusLa_ vdu. ⤴ *IProgressColumnElementPort.Inputs.PaymentFinalStatusLaOpts* Default `{}`.
 * - `[step2BotSepOpts]` _!Object?_ The options to pass to the _Step2BotSep_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2BotSepOpts* Default `{}`.
 * - `[step1Opts]` _!Object?_ The options to pass to the _Step1_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1Opts* Default `{}`.
 * - `[step1LaOpts]` _!Object?_ The options to pass to the _Step1La_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1LaOpts* Default `{}`.
 * - `[step2LaOpts]` _!Object?_ The options to pass to the _Step2La_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2LaOpts* Default `{}`.
 * - `[transactionIdWrOpts]` _!Object?_ The options to pass to the _TransactionIdWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.TransactionIdWrOpts* Default `{}`.
 * - `[transactionIdLaOpts]` _!Object?_ The options to pass to the _TransactionIdLa_ vdu. ⤴ *IProgressColumnElementPort.Inputs.TransactionIdLaOpts* Default `{}`.
 * - `[step1ProgressCircleWrOpts]` _!Object?_ The options to pass to the _Step1ProgressCircleWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts* Default `{}`.
 * - `[step3WrOpts]` _!Object?_ The options to pass to the _Step3Wr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step3WrOpts* Default `{}`.
 * - `[step1CircleOpts]` _!Object?_ The options to pass to the _Step1Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1CircleOpts* Default `{}`.
 * - `[step2CircleOpts]` _!Object?_ The options to pass to the _Step2Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2CircleOpts* Default `{}`.
 * - `[step3CircleOpts]` _!Object?_ The options to pass to the _Step3Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step3CircleOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IProgressColumnElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ProgressColumnMemory, port?: !xyz.swapee.wc.IProgressColumnElement.Inputs) => ?} xyz.swapee.wc.IProgressColumnElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnElement.__inducer<!xyz.swapee.wc.IProgressColumnElement>} xyz.swapee.wc.IProgressColumnElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [model] The model of the component into which to induce the state.
 * - `core` _string_ The core property. Default empty string.
 * - `creatingTransaction` _boolean_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 * - `finishedOnPayment` _boolean_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 * - `paymentFailed` _boolean_ The payment during step 2 did not succeed. Default `false`.
 * - `paymentCompleted` _boolean_ The second, payment step, was successful. Moving on to the third step. Default `false`.
 * - `transactionId` _string_ The ID of the created transaction. Default empty string.
 * - `paymentStatus` _string_ The status of the payment.
 * Choose between:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default empty string.
 * - `status` _string_ Default empty string.
 * - `processingTransaction` _boolean_ The third step, where the moneys is exchanged and sent to the user. Default `false`.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user.
 * One of:
 * > - _exchaning_
 * > - _sending_
 * > - _finished_
 * > - _failed_Default empty string.
 * - `transactionCompleted` _boolean_ Everything is done. Default `false`.
 * - `transactionFailed` _boolean_ Transaction wasn't successful. Default `false`.
 * @param {!xyz.swapee.wc.IProgressColumnElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IProgressColumnOuterCore.WeakModel.Core* ⤴ *IProgressColumnOuterCore.WeakModel.Core* Default `null`.
 * - `[creatingTransaction=null]` _&#42;?_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. ⤴ *IProgressColumnOuterCore.WeakModel.CreatingTransaction* ⤴ *IProgressColumnOuterCore.WeakModel.CreatingTransaction* Default `null`.
 * - `[finishedOnPayment=null]` _&#42;?_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. ⤴ *IProgressColumnOuterCore.WeakModel.FinishedOnPayment* ⤴ *IProgressColumnOuterCore.WeakModel.FinishedOnPayment* Default `null`.
 * - `[paymentFailed=null]` _&#42;?_ The payment during step 2 did not succeed. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentFailed* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentFailed* Default `null`.
 * - `[paymentCompleted=null]` _&#42;?_ The second, payment step, was successful. Moving on to the third step. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentCompleted* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentCompleted* Default `null`.
 * - `[transactionId=null]` _&#42;?_ The ID of the created transaction. ⤴ *IProgressColumnOuterCore.WeakModel.TransactionId* ⤴ *IProgressColumnOuterCore.WeakModel.TransactionId* Default `null`.
 * - `[paymentStatus=null]` _&#42;?_ The status of the payment. ⤴ *IProgressColumnOuterCore.WeakModel.PaymentStatus* ⤴ *IProgressColumnOuterCore.WeakModel.PaymentStatus*
 * Choose between:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default `null`.
 * - `[status=null]` _&#42;?_ ⤴ *IProgressColumnOuterCore.WeakModel.Status* ⤴ *IProgressColumnOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IProgressColumnElementPort.Inputs.NoSolder* Default `false`.
 * - `[paymentFinalStatusWrOpts]` _!Object?_ The options to pass to the _PaymentFinalStatusWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.PaymentFinalStatusWrOpts* Default `{}`.
 * - `[paymentFinalStatusLaOpts]` _!Object?_ The options to pass to the _PaymentFinalStatusLa_ vdu. ⤴ *IProgressColumnElementPort.Inputs.PaymentFinalStatusLaOpts* Default `{}`.
 * - `[step2BotSepOpts]` _!Object?_ The options to pass to the _Step2BotSep_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2BotSepOpts* Default `{}`.
 * - `[step1Opts]` _!Object?_ The options to pass to the _Step1_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1Opts* Default `{}`.
 * - `[step1LaOpts]` _!Object?_ The options to pass to the _Step1La_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1LaOpts* Default `{}`.
 * - `[step2LaOpts]` _!Object?_ The options to pass to the _Step2La_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2LaOpts* Default `{}`.
 * - `[transactionIdWrOpts]` _!Object?_ The options to pass to the _TransactionIdWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.TransactionIdWrOpts* Default `{}`.
 * - `[transactionIdLaOpts]` _!Object?_ The options to pass to the _TransactionIdLa_ vdu. ⤴ *IProgressColumnElementPort.Inputs.TransactionIdLaOpts* Default `{}`.
 * - `[step1ProgressCircleWrOpts]` _!Object?_ The options to pass to the _Step1ProgressCircleWr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts* Default `{}`.
 * - `[step3WrOpts]` _!Object?_ The options to pass to the _Step3Wr_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step3WrOpts* Default `{}`.
 * - `[step1CircleOpts]` _!Object?_ The options to pass to the _Step1Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step1CircleOpts* Default `{}`.
 * - `[step2CircleOpts]` _!Object?_ The options to pass to the _Step2Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step2CircleOpts* Default `{}`.
 * - `[step3CircleOpts]` _!Object?_ The options to pass to the _Step3Circle_ vdu. ⤴ *IProgressColumnElementPort.Inputs.Step3CircleOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IProgressColumnElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IProgressColumnElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/140-IProgressColumnElementPort.xml}  027536e32a7586006f905813d5f30069 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IProgressColumnElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnElementPort)} xyz.swapee.wc.AbstractProgressColumnElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnElementPort} xyz.swapee.wc.ProgressColumnElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnElementPort
 */
xyz.swapee.wc.AbstractProgressColumnElementPort = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnElementPort.constructor&xyz.swapee.wc.ProgressColumnElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnElementPort.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnElementPort|typeof xyz.swapee.wc.ProgressColumnElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnElementPort|typeof xyz.swapee.wc.ProgressColumnElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnElementPort|typeof xyz.swapee.wc.ProgressColumnElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.AbstractProgressColumnElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnElementPort.Initialese[]) => xyz.swapee.wc.IProgressColumnElementPort} xyz.swapee.wc.ProgressColumnElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IProgressColumnElementPort.Inputs>)} xyz.swapee.wc.IProgressColumnElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IProgressColumnElementPort
 */
xyz.swapee.wc.IProgressColumnElementPort = class extends /** @type {xyz.swapee.wc.IProgressColumnElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnElementPort.Initialese>)} xyz.swapee.wc.ProgressColumnElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort} xyz.swapee.wc.IProgressColumnElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IProgressColumnElementPort_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnElementPort
 * @implements {xyz.swapee.wc.IProgressColumnElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnElementPort.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnElementPort = class extends /** @type {xyz.swapee.wc.ProgressColumnElementPort.constructor&xyz.swapee.wc.IProgressColumnElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnElementPort}
 */
xyz.swapee.wc.ProgressColumnElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnElementPort.
 * @interface xyz.swapee.wc.IProgressColumnElementPortFields
 */
xyz.swapee.wc.IProgressColumnElementPortFields = class { }
/**
 * The inputs to the _IProgressColumnElement_'s controller via its element port.
 */
xyz.swapee.wc.IProgressColumnElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IProgressColumnElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IProgressColumnElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IProgressColumnElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnElementPort} */
xyz.swapee.wc.RecordIProgressColumnElementPort

/** @typedef {xyz.swapee.wc.IProgressColumnElementPort} xyz.swapee.wc.BoundIProgressColumnElementPort */

/** @typedef {xyz.swapee.wc.ProgressColumnElementPort} xyz.swapee.wc.BoundProgressColumnElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _KycBlock_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts.kycBlockOpts

/**
 * The options to pass to the _KycRequired_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts.kycRequiredOpts

/**
 * The options to pass to the _KycNotRequired_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts.kycNotRequiredOpts

/**
 * The options to pass to the _ExchangeEmail_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts.exchangeEmailOpts

/**
 * The options to pass to the _InfoNotice_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts.infoNoticeOpts

/**
 * The options to pass to the _SupportNotice_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts.supportNoticeOpts

/**
 * The options to pass to the _PaymentStatusWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts.paymentStatusWrOpts

/**
 * The options to pass to the _PaymentStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts.paymentStatusLaOpts

/**
 * The options to pass to the _TransactionStatusWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts.transactionStatusWrOpts

/**
 * The options to pass to the _TransactionStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts.transactionStatusLaOpts

/**
 * The options to pass to the _Step2BotSep_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts.step2BotSepOpts

/**
 * The options to pass to the _Step1Co_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts.step1CoOpts

/**
 * The options to pass to the _Step2Co_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts.step2CoOpts

/**
 * The options to pass to the _Step3Co_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts.step3CoOpts

/**
 * The options to pass to the _Step1La_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts.step1LaOpts

/**
 * The options to pass to the _Step2La_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts.step2LaOpts

/**
 * The options to pass to the _Step3La_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts.step3LaOpts

/**
 * The options to pass to the _TransactionIdWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts.transactionIdWrOpts

/**
 * The options to pass to the _TransactionIdLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts.transactionIdLaOpts

/**
 * The options to pass to the _Step1ProgressCircleWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts.step1ProgressCircleWrOpts

/**
 * The options to pass to the _Step2ProgressCircleWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts.step2ProgressCircleWrOpts

/**
 * The options to pass to the _Step1Wr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts.step1WrOpts

/**
 * The options to pass to the _Step2Wr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts.step2WrOpts

/**
 * The options to pass to the _Step3Wr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts.step3WrOpts

/**
 * The options to pass to the _Step1Circle_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts.step1CircleOpts

/**
 * The options to pass to the _Step2Circle_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts.step2CircleOpts

/**
 * The options to pass to the _Step3Circle_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts.step3CircleOpts

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder&xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts)} xyz.swapee.wc.IProgressColumnElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder} xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts.typeof */
/**
 * The inputs to the _IProgressColumnElement_'s controller via its element port.
 * @record xyz.swapee.wc.IProgressColumnElementPort.Inputs
 */
xyz.swapee.wc.IProgressColumnElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IProgressColumnElementPort.Inputs.constructor&xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IProgressColumnElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IProgressColumnElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts)} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts.typeof */
/**
 * The inputs to the _IProgressColumnElement_'s controller via its element port.
 * @record xyz.swapee.wc.IProgressColumnElementPort.WeakInputs
 */
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.constructor&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts.typeof&xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IProgressColumnElementPort.WeakInputs

/**
 * Contains getters to cast the _IProgressColumnElementPort_ interface.
 * @interface xyz.swapee.wc.IProgressColumnElementPortCaster
 */
xyz.swapee.wc.IProgressColumnElementPortCaster = class { }
/**
 * Cast the _IProgressColumnElementPort_ instance into the _BoundIProgressColumnElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnElementPort}
 */
xyz.swapee.wc.IProgressColumnElementPortCaster.prototype.asIProgressColumnElementPort
/**
 * Access the _ProgressColumnElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnElementPort}
 */
xyz.swapee.wc.IProgressColumnElementPortCaster.prototype.superProgressColumnElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts The options to pass to the _KycBlock_ vdu (optional overlay).
 * @prop {!Object} [kycBlockOpts] The options to pass to the _KycBlock_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycBlockOpts_Safe The options to pass to the _KycBlock_ vdu (required overlay).
 * @prop {!Object} kycBlockOpts The options to pass to the _KycBlock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts The options to pass to the _KycRequired_ vdu (optional overlay).
 * @prop {!Object} [kycRequiredOpts] The options to pass to the _KycRequired_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycRequiredOpts_Safe The options to pass to the _KycRequired_ vdu (required overlay).
 * @prop {!Object} kycRequiredOpts The options to pass to the _KycRequired_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts The options to pass to the _KycNotRequired_ vdu (optional overlay).
 * @prop {!Object} [kycNotRequiredOpts] The options to pass to the _KycNotRequired_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.KycNotRequiredOpts_Safe The options to pass to the _KycNotRequired_ vdu (required overlay).
 * @prop {!Object} kycNotRequiredOpts The options to pass to the _KycNotRequired_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts The options to pass to the _ExchangeEmail_ vdu (optional overlay).
 * @prop {!Object} [exchangeEmailOpts] The options to pass to the _ExchangeEmail_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.ExchangeEmailOpts_Safe The options to pass to the _ExchangeEmail_ vdu (required overlay).
 * @prop {!Object} exchangeEmailOpts The options to pass to the _ExchangeEmail_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts The options to pass to the _InfoNotice_ vdu (optional overlay).
 * @prop {!Object} [infoNoticeOpts] The options to pass to the _InfoNotice_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.InfoNoticeOpts_Safe The options to pass to the _InfoNotice_ vdu (required overlay).
 * @prop {!Object} infoNoticeOpts The options to pass to the _InfoNotice_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts The options to pass to the _SupportNotice_ vdu (optional overlay).
 * @prop {!Object} [supportNoticeOpts] The options to pass to the _SupportNotice_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.SupportNoticeOpts_Safe The options to pass to the _SupportNotice_ vdu (required overlay).
 * @prop {!Object} supportNoticeOpts The options to pass to the _SupportNotice_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts The options to pass to the _PaymentStatusWr_ vdu (optional overlay).
 * @prop {!Object} [paymentStatusWrOpts] The options to pass to the _PaymentStatusWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusWrOpts_Safe The options to pass to the _PaymentStatusWr_ vdu (required overlay).
 * @prop {!Object} paymentStatusWrOpts The options to pass to the _PaymentStatusWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts The options to pass to the _PaymentStatusLa_ vdu (optional overlay).
 * @prop {!Object} [paymentStatusLaOpts] The options to pass to the _PaymentStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.PaymentStatusLaOpts_Safe The options to pass to the _PaymentStatusLa_ vdu (required overlay).
 * @prop {!Object} paymentStatusLaOpts The options to pass to the _PaymentStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts The options to pass to the _TransactionStatusWr_ vdu (optional overlay).
 * @prop {!Object} [transactionStatusWrOpts] The options to pass to the _TransactionStatusWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusWrOpts_Safe The options to pass to the _TransactionStatusWr_ vdu (required overlay).
 * @prop {!Object} transactionStatusWrOpts The options to pass to the _TransactionStatusWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts The options to pass to the _TransactionStatusLa_ vdu (optional overlay).
 * @prop {!Object} [transactionStatusLaOpts] The options to pass to the _TransactionStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionStatusLaOpts_Safe The options to pass to the _TransactionStatusLa_ vdu (required overlay).
 * @prop {!Object} transactionStatusLaOpts The options to pass to the _TransactionStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts The options to pass to the _Step2BotSep_ vdu (optional overlay).
 * @prop {!Object} [step2BotSepOpts] The options to pass to the _Step2BotSep_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2BotSepOpts_Safe The options to pass to the _Step2BotSep_ vdu (required overlay).
 * @prop {!Object} step2BotSepOpts The options to pass to the _Step2BotSep_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts The options to pass to the _Step1Co_ vdu (optional overlay).
 * @prop {!Object} [step1CoOpts] The options to pass to the _Step1Co_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CoOpts_Safe The options to pass to the _Step1Co_ vdu (required overlay).
 * @prop {!Object} step1CoOpts The options to pass to the _Step1Co_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts The options to pass to the _Step2Co_ vdu (optional overlay).
 * @prop {!Object} [step2CoOpts] The options to pass to the _Step2Co_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CoOpts_Safe The options to pass to the _Step2Co_ vdu (required overlay).
 * @prop {!Object} step2CoOpts The options to pass to the _Step2Co_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts The options to pass to the _Step3Co_ vdu (optional overlay).
 * @prop {!Object} [step3CoOpts] The options to pass to the _Step3Co_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CoOpts_Safe The options to pass to the _Step3Co_ vdu (required overlay).
 * @prop {!Object} step3CoOpts The options to pass to the _Step3Co_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts The options to pass to the _Step1La_ vdu (optional overlay).
 * @prop {!Object} [step1LaOpts] The options to pass to the _Step1La_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1LaOpts_Safe The options to pass to the _Step1La_ vdu (required overlay).
 * @prop {!Object} step1LaOpts The options to pass to the _Step1La_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts The options to pass to the _Step2La_ vdu (optional overlay).
 * @prop {!Object} [step2LaOpts] The options to pass to the _Step2La_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2LaOpts_Safe The options to pass to the _Step2La_ vdu (required overlay).
 * @prop {!Object} step2LaOpts The options to pass to the _Step2La_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts The options to pass to the _Step3La_ vdu (optional overlay).
 * @prop {!Object} [step3LaOpts] The options to pass to the _Step3La_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3LaOpts_Safe The options to pass to the _Step3La_ vdu (required overlay).
 * @prop {!Object} step3LaOpts The options to pass to the _Step3La_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts The options to pass to the _TransactionIdWr_ vdu (optional overlay).
 * @prop {!Object} [transactionIdWrOpts] The options to pass to the _TransactionIdWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdWrOpts_Safe The options to pass to the _TransactionIdWr_ vdu (required overlay).
 * @prop {!Object} transactionIdWrOpts The options to pass to the _TransactionIdWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts The options to pass to the _TransactionIdLa_ vdu (optional overlay).
 * @prop {!Object} [transactionIdLaOpts] The options to pass to the _TransactionIdLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.TransactionIdLaOpts_Safe The options to pass to the _TransactionIdLa_ vdu (required overlay).
 * @prop {!Object} transactionIdLaOpts The options to pass to the _TransactionIdLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts The options to pass to the _Step1ProgressCircleWr_ vdu (optional overlay).
 * @prop {!Object} [step1ProgressCircleWrOpts] The options to pass to the _Step1ProgressCircleWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1ProgressCircleWrOpts_Safe The options to pass to the _Step1ProgressCircleWr_ vdu (required overlay).
 * @prop {!Object} step1ProgressCircleWrOpts The options to pass to the _Step1ProgressCircleWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts The options to pass to the _Step2ProgressCircleWr_ vdu (optional overlay).
 * @prop {!Object} [step2ProgressCircleWrOpts] The options to pass to the _Step2ProgressCircleWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2ProgressCircleWrOpts_Safe The options to pass to the _Step2ProgressCircleWr_ vdu (required overlay).
 * @prop {!Object} step2ProgressCircleWrOpts The options to pass to the _Step2ProgressCircleWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts The options to pass to the _Step1Wr_ vdu (optional overlay).
 * @prop {!Object} [step1WrOpts] The options to pass to the _Step1Wr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1WrOpts_Safe The options to pass to the _Step1Wr_ vdu (required overlay).
 * @prop {!Object} step1WrOpts The options to pass to the _Step1Wr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts The options to pass to the _Step2Wr_ vdu (optional overlay).
 * @prop {!Object} [step2WrOpts] The options to pass to the _Step2Wr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2WrOpts_Safe The options to pass to the _Step2Wr_ vdu (required overlay).
 * @prop {!Object} step2WrOpts The options to pass to the _Step2Wr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts The options to pass to the _Step3Wr_ vdu (optional overlay).
 * @prop {!Object} [step3WrOpts] The options to pass to the _Step3Wr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3WrOpts_Safe The options to pass to the _Step3Wr_ vdu (required overlay).
 * @prop {!Object} step3WrOpts The options to pass to the _Step3Wr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts The options to pass to the _Step1Circle_ vdu (optional overlay).
 * @prop {!Object} [step1CircleOpts] The options to pass to the _Step1Circle_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step1CircleOpts_Safe The options to pass to the _Step1Circle_ vdu (required overlay).
 * @prop {!Object} step1CircleOpts The options to pass to the _Step1Circle_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts The options to pass to the _Step2Circle_ vdu (optional overlay).
 * @prop {!Object} [step2CircleOpts] The options to pass to the _Step2Circle_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step2CircleOpts_Safe The options to pass to the _Step2Circle_ vdu (required overlay).
 * @prop {!Object} step2CircleOpts The options to pass to the _Step2Circle_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts The options to pass to the _Step3Circle_ vdu (optional overlay).
 * @prop {!Object} [step3CircleOpts] The options to pass to the _Step3Circle_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.Inputs.Step3CircleOpts_Safe The options to pass to the _Step3Circle_ vdu (required overlay).
 * @prop {!Object} step3CircleOpts The options to pass to the _Step3Circle_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts The options to pass to the _KycBlock_ vdu (optional overlay).
 * @prop {*} [kycBlockOpts=null] The options to pass to the _KycBlock_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycBlockOpts_Safe The options to pass to the _KycBlock_ vdu (required overlay).
 * @prop {*} kycBlockOpts The options to pass to the _KycBlock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts The options to pass to the _KycRequired_ vdu (optional overlay).
 * @prop {*} [kycRequiredOpts=null] The options to pass to the _KycRequired_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycRequiredOpts_Safe The options to pass to the _KycRequired_ vdu (required overlay).
 * @prop {*} kycRequiredOpts The options to pass to the _KycRequired_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts The options to pass to the _KycNotRequired_ vdu (optional overlay).
 * @prop {*} [kycNotRequiredOpts=null] The options to pass to the _KycNotRequired_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.KycNotRequiredOpts_Safe The options to pass to the _KycNotRequired_ vdu (required overlay).
 * @prop {*} kycNotRequiredOpts The options to pass to the _KycNotRequired_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts The options to pass to the _ExchangeEmail_ vdu (optional overlay).
 * @prop {*} [exchangeEmailOpts=null] The options to pass to the _ExchangeEmail_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.ExchangeEmailOpts_Safe The options to pass to the _ExchangeEmail_ vdu (required overlay).
 * @prop {*} exchangeEmailOpts The options to pass to the _ExchangeEmail_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts The options to pass to the _InfoNotice_ vdu (optional overlay).
 * @prop {*} [infoNoticeOpts=null] The options to pass to the _InfoNotice_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.InfoNoticeOpts_Safe The options to pass to the _InfoNotice_ vdu (required overlay).
 * @prop {*} infoNoticeOpts The options to pass to the _InfoNotice_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts The options to pass to the _SupportNotice_ vdu (optional overlay).
 * @prop {*} [supportNoticeOpts=null] The options to pass to the _SupportNotice_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.SupportNoticeOpts_Safe The options to pass to the _SupportNotice_ vdu (required overlay).
 * @prop {*} supportNoticeOpts The options to pass to the _SupportNotice_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts The options to pass to the _PaymentStatusWr_ vdu (optional overlay).
 * @prop {*} [paymentStatusWrOpts=null] The options to pass to the _PaymentStatusWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusWrOpts_Safe The options to pass to the _PaymentStatusWr_ vdu (required overlay).
 * @prop {*} paymentStatusWrOpts The options to pass to the _PaymentStatusWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts The options to pass to the _PaymentStatusLa_ vdu (optional overlay).
 * @prop {*} [paymentStatusLaOpts=null] The options to pass to the _PaymentStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.PaymentStatusLaOpts_Safe The options to pass to the _PaymentStatusLa_ vdu (required overlay).
 * @prop {*} paymentStatusLaOpts The options to pass to the _PaymentStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts The options to pass to the _TransactionStatusWr_ vdu (optional overlay).
 * @prop {*} [transactionStatusWrOpts=null] The options to pass to the _TransactionStatusWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusWrOpts_Safe The options to pass to the _TransactionStatusWr_ vdu (required overlay).
 * @prop {*} transactionStatusWrOpts The options to pass to the _TransactionStatusWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts The options to pass to the _TransactionStatusLa_ vdu (optional overlay).
 * @prop {*} [transactionStatusLaOpts=null] The options to pass to the _TransactionStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionStatusLaOpts_Safe The options to pass to the _TransactionStatusLa_ vdu (required overlay).
 * @prop {*} transactionStatusLaOpts The options to pass to the _TransactionStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts The options to pass to the _Step2BotSep_ vdu (optional overlay).
 * @prop {*} [step2BotSepOpts=null] The options to pass to the _Step2BotSep_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2BotSepOpts_Safe The options to pass to the _Step2BotSep_ vdu (required overlay).
 * @prop {*} step2BotSepOpts The options to pass to the _Step2BotSep_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts The options to pass to the _Step1Co_ vdu (optional overlay).
 * @prop {*} [step1CoOpts=null] The options to pass to the _Step1Co_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CoOpts_Safe The options to pass to the _Step1Co_ vdu (required overlay).
 * @prop {*} step1CoOpts The options to pass to the _Step1Co_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts The options to pass to the _Step2Co_ vdu (optional overlay).
 * @prop {*} [step2CoOpts=null] The options to pass to the _Step2Co_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CoOpts_Safe The options to pass to the _Step2Co_ vdu (required overlay).
 * @prop {*} step2CoOpts The options to pass to the _Step2Co_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts The options to pass to the _Step3Co_ vdu (optional overlay).
 * @prop {*} [step3CoOpts=null] The options to pass to the _Step3Co_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CoOpts_Safe The options to pass to the _Step3Co_ vdu (required overlay).
 * @prop {*} step3CoOpts The options to pass to the _Step3Co_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts The options to pass to the _Step1La_ vdu (optional overlay).
 * @prop {*} [step1LaOpts=null] The options to pass to the _Step1La_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1LaOpts_Safe The options to pass to the _Step1La_ vdu (required overlay).
 * @prop {*} step1LaOpts The options to pass to the _Step1La_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts The options to pass to the _Step2La_ vdu (optional overlay).
 * @prop {*} [step2LaOpts=null] The options to pass to the _Step2La_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2LaOpts_Safe The options to pass to the _Step2La_ vdu (required overlay).
 * @prop {*} step2LaOpts The options to pass to the _Step2La_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts The options to pass to the _Step3La_ vdu (optional overlay).
 * @prop {*} [step3LaOpts=null] The options to pass to the _Step3La_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3LaOpts_Safe The options to pass to the _Step3La_ vdu (required overlay).
 * @prop {*} step3LaOpts The options to pass to the _Step3La_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts The options to pass to the _TransactionIdWr_ vdu (optional overlay).
 * @prop {*} [transactionIdWrOpts=null] The options to pass to the _TransactionIdWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdWrOpts_Safe The options to pass to the _TransactionIdWr_ vdu (required overlay).
 * @prop {*} transactionIdWrOpts The options to pass to the _TransactionIdWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts The options to pass to the _TransactionIdLa_ vdu (optional overlay).
 * @prop {*} [transactionIdLaOpts=null] The options to pass to the _TransactionIdLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.TransactionIdLaOpts_Safe The options to pass to the _TransactionIdLa_ vdu (required overlay).
 * @prop {*} transactionIdLaOpts The options to pass to the _TransactionIdLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts The options to pass to the _Step1ProgressCircleWr_ vdu (optional overlay).
 * @prop {*} [step1ProgressCircleWrOpts=null] The options to pass to the _Step1ProgressCircleWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1ProgressCircleWrOpts_Safe The options to pass to the _Step1ProgressCircleWr_ vdu (required overlay).
 * @prop {*} step1ProgressCircleWrOpts The options to pass to the _Step1ProgressCircleWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts The options to pass to the _Step2ProgressCircleWr_ vdu (optional overlay).
 * @prop {*} [step2ProgressCircleWrOpts=null] The options to pass to the _Step2ProgressCircleWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2ProgressCircleWrOpts_Safe The options to pass to the _Step2ProgressCircleWr_ vdu (required overlay).
 * @prop {*} step2ProgressCircleWrOpts The options to pass to the _Step2ProgressCircleWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts The options to pass to the _Step1Wr_ vdu (optional overlay).
 * @prop {*} [step1WrOpts=null] The options to pass to the _Step1Wr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1WrOpts_Safe The options to pass to the _Step1Wr_ vdu (required overlay).
 * @prop {*} step1WrOpts The options to pass to the _Step1Wr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts The options to pass to the _Step2Wr_ vdu (optional overlay).
 * @prop {*} [step2WrOpts=null] The options to pass to the _Step2Wr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2WrOpts_Safe The options to pass to the _Step2Wr_ vdu (required overlay).
 * @prop {*} step2WrOpts The options to pass to the _Step2Wr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts The options to pass to the _Step3Wr_ vdu (optional overlay).
 * @prop {*} [step3WrOpts=null] The options to pass to the _Step3Wr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3WrOpts_Safe The options to pass to the _Step3Wr_ vdu (required overlay).
 * @prop {*} step3WrOpts The options to pass to the _Step3Wr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts The options to pass to the _Step1Circle_ vdu (optional overlay).
 * @prop {*} [step1CircleOpts=null] The options to pass to the _Step1Circle_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step1CircleOpts_Safe The options to pass to the _Step1Circle_ vdu (required overlay).
 * @prop {*} step1CircleOpts The options to pass to the _Step1Circle_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts The options to pass to the _Step2Circle_ vdu (optional overlay).
 * @prop {*} [step2CircleOpts=null] The options to pass to the _Step2Circle_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step2CircleOpts_Safe The options to pass to the _Step2Circle_ vdu (required overlay).
 * @prop {*} step2CircleOpts The options to pass to the _Step2Circle_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts The options to pass to the _Step3Circle_ vdu (optional overlay).
 * @prop {*} [step3CircleOpts=null] The options to pass to the _Step3Circle_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnElementPort.WeakInputs.Step3CircleOpts_Safe The options to pass to the _Step3Circle_ vdu (required overlay).
 * @prop {*} step3CircleOpts The options to pass to the _Step3Circle_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/170-IProgressColumnDesigner.xml}  3e3aa1a68c3a179a65a9908867fd8ff4 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IProgressColumnDesigner
 */
xyz.swapee.wc.IProgressColumnDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.ProgressColumnClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IProgressColumn />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ProgressColumnClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IProgressColumn />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ProgressColumnClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ProgressColumn` _typeof IProgressColumnController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ProgressColumn` _typeof IProgressColumnController_
   * - `This` _typeof IProgressColumnController_
   * @param {!xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ProgressColumn` _!ProgressColumnMemory_
   * - `This` _!ProgressColumnMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ProgressColumnClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ProgressColumnClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IProgressColumnDesigner.prototype.constructor = xyz.swapee.wc.IProgressColumnDesigner

/**
 * A concrete class of _IProgressColumnDesigner_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnDesigner
 * @implements {xyz.swapee.wc.IProgressColumnDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.ProgressColumnDesigner = class extends xyz.swapee.wc.IProgressColumnDesigner { }
xyz.swapee.wc.ProgressColumnDesigner.prototype.constructor = xyz.swapee.wc.ProgressColumnDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IProgressColumnController} ProgressColumn
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IProgressColumnController} ProgressColumn
 * @prop {typeof xyz.swapee.wc.IProgressColumnController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IProgressColumnDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.ProgressColumnMemory} ProgressColumn
 * @prop {!xyz.swapee.wc.ProgressColumnMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplay.xml}  5d9d209603d3368414c61b1a7d4c59e1 */
/**
 * @typedef {Object} $xyz.swapee.wc.IProgressColumnDisplay.Initialese
 * @prop {HTMLDivElement} [KycBlock]
 * @prop {HTMLDivElement} [KycRequired]
 * @prop {HTMLDivElement} [KycNotRequired]
 * @prop {HTMLDivElement} [ExchangeEmail]
 * @prop {HTMLDivElement} [InfoNotice]
 * @prop {HTMLDivElement} [SupportNotice]
 * @prop {HTMLDivElement} [PaymentStatusWr]
 * @prop {HTMLSpanElement} [PaymentStatusLa]
 * @prop {HTMLDivElement} [TransactionStatusWr]
 * @prop {HTMLSpanElement} [TransactionStatusLa]
 * @prop {HTMLSpanElement} [Step2BotSep]
 * @prop {HTMLDivElement} [Step1Co]
 * @prop {HTMLDivElement} [Step2Co]
 * @prop {HTMLDivElement} [Step3Co]
 * @prop {HTMLSpanElement} [Step1La]
 * @prop {HTMLSpanElement} [Step2La]
 * @prop {HTMLSpanElement} [Step3La]
 * @prop {HTMLSpanElement} [TransactionIdWr]
 * @prop {HTMLSpanElement} [TransactionIdLa]
 * @prop {HTMLSpanElement} [Step1ProgressCircleWr]
 * @prop {HTMLSpanElement} [Step2ProgressCircleWr]
 * @prop {HTMLDivElement} [Step1Wr]
 * @prop {HTMLDivElement} [Step2Wr]
 * @prop {HTMLDivElement} [Step3Wr]
 * @prop {HTMLSpanElement} [Step1Circle]
 * @prop {HTMLSpanElement} [Step2Circle]
 * @prop {HTMLSpanElement} [Step3Circle]
 */
/** @typedef {$xyz.swapee.wc.IProgressColumnDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings>} xyz.swapee.wc.IProgressColumnDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnDisplay)} xyz.swapee.wc.AbstractProgressColumnDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnDisplay} xyz.swapee.wc.ProgressColumnDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnDisplay
 */
xyz.swapee.wc.AbstractProgressColumnDisplay = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnDisplay.constructor&xyz.swapee.wc.ProgressColumnDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnDisplay.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.AbstractProgressColumnDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnDisplay.Initialese[]) => xyz.swapee.wc.IProgressColumnDisplay} xyz.swapee.wc.ProgressColumnDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.ProgressColumnMemory, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, xyz.swapee.wc.IProgressColumnDisplay.Queries, null>)} xyz.swapee.wc.IProgressColumnDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IProgressColumn_.
 * @interface xyz.swapee.wc.IProgressColumnDisplay
 */
xyz.swapee.wc.IProgressColumnDisplay = class extends /** @type {xyz.swapee.wc.IProgressColumnDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IProgressColumnDisplay.paint} */
xyz.swapee.wc.IProgressColumnDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnDisplay.Initialese>)} xyz.swapee.wc.ProgressColumnDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnDisplay} xyz.swapee.wc.IProgressColumnDisplay.typeof */
/**
 * A concrete class of _IProgressColumnDisplay_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnDisplay
 * @implements {xyz.swapee.wc.IProgressColumnDisplay} Display for presenting information from the _IProgressColumn_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnDisplay.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnDisplay = class extends /** @type {xyz.swapee.wc.ProgressColumnDisplay.constructor&xyz.swapee.wc.IProgressColumnDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnDisplay}
 */
xyz.swapee.wc.ProgressColumnDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnDisplay.
 * @interface xyz.swapee.wc.IProgressColumnDisplayFields
 */
xyz.swapee.wc.IProgressColumnDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IProgressColumnDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IProgressColumnDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycBlock = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycRequired = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.KycNotRequired = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.ExchangeEmail = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.InfoNotice = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.SupportNotice = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.PaymentStatusWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.PaymentStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionStatusWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2BotSep = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Co = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Co = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Co = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1La = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2La = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3La = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionIdWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.TransactionIdLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1ProgressCircleWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2ProgressCircleWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Wr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Wr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Wr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step1Circle = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step2Circle = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IProgressColumnDisplayFields.prototype.Step3Circle = /** @type {HTMLSpanElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnDisplay} */
xyz.swapee.wc.RecordIProgressColumnDisplay

/** @typedef {xyz.swapee.wc.IProgressColumnDisplay} xyz.swapee.wc.BoundIProgressColumnDisplay */

/** @typedef {xyz.swapee.wc.ProgressColumnDisplay} xyz.swapee.wc.BoundProgressColumnDisplay */

/** @typedef {xyz.swapee.wc.IProgressColumnDisplay.Queries} xyz.swapee.wc.IProgressColumnDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IProgressColumnDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IProgressColumnDisplay_ interface.
 * @interface xyz.swapee.wc.IProgressColumnDisplayCaster
 */
xyz.swapee.wc.IProgressColumnDisplayCaster = class { }
/**
 * Cast the _IProgressColumnDisplay_ instance into the _BoundIProgressColumnDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnDisplay}
 */
xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.asIProgressColumnDisplay
/**
 * Cast the _IProgressColumnDisplay_ instance into the _BoundIProgressColumnScreen_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnScreen}
 */
xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.asIProgressColumnScreen
/**
 * Access the _ProgressColumnDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnDisplay}
 */
xyz.swapee.wc.IProgressColumnDisplayCaster.prototype.superProgressColumnDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ProgressColumnMemory, land: null) => void} xyz.swapee.wc.IProgressColumnDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnDisplay.__paint<!xyz.swapee.wc.IProgressColumnDisplay>} xyz.swapee.wc.IProgressColumnDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ProgressColumnMemory} memory The display data.
 * - `creatingTransaction` _boolean_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 * - `finishedOnPayment` _boolean_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 * - `paymentFailed` _boolean_ The payment during step 2 did not succeed. Default `false`.
 * - `paymentCompleted` _boolean_ The second, payment step, was successful. Moving on to the third step. Default `false`.
 * - `transactionId` _string_ The ID of the created transaction. Default empty string.
 * - `paymentStatus` _string_ The status of the payment.
 * Can be either:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default empty string.
 * - `loadingStep1` _boolean_ Default `false`.
 * - `loadingStep2` _boolean_ Default `false`.
 * - `loadingStep3` _boolean_ Default `false`.
 * - `status` _string_ Default empty string.
 * - `processingTransaction` _boolean_ The third step, where the moneys is exchanged and sent to the user. Default `false`.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user.
 * Choose between:
 * > - _exchanging_
 * > - _sending_
 * > - _finished_
 * > - _failed_Default empty string.
 * - `transactionCompleted` _boolean_ Everything is done. Default `false`.
 * - `transactionFailed` _boolean_ Transaction wasn't successful. Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IProgressColumnDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IProgressColumnDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/40-IProgressColumnDisplayBack.xml}  7c02a562239abaeb77b5d0493a1d7b71 */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IProgressColumnDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [KycBlock]
 * @prop {!com.webcircuits.IHtmlTwin} [KycRequired]
 * @prop {!com.webcircuits.IHtmlTwin} [KycNotRequired]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeEmail]
 * @prop {!com.webcircuits.IHtmlTwin} [InfoNotice]
 * @prop {!com.webcircuits.IHtmlTwin} [SupportNotice]
 * @prop {!com.webcircuits.IHtmlTwin} [PaymentStatusWr]
 * @prop {!com.webcircuits.IHtmlTwin} [PaymentStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionStatusWr]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [Step2BotSep]
 * @prop {!com.webcircuits.IHtmlTwin} [Step1Co]
 * @prop {!com.webcircuits.IHtmlTwin} [Step2Co]
 * @prop {!com.webcircuits.IHtmlTwin} [Step3Co]
 * @prop {!com.webcircuits.IHtmlTwin} [Step1La]
 * @prop {!com.webcircuits.IHtmlTwin} [Step2La]
 * @prop {!com.webcircuits.IHtmlTwin} [Step3La]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionIdWr]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionIdLa]
 * @prop {!com.webcircuits.IHtmlTwin} [Step1ProgressCircleWr]
 * @prop {!com.webcircuits.IHtmlTwin} [Step2ProgressCircleWr]
 * @prop {!com.webcircuits.IHtmlTwin} [Step1Wr]
 * @prop {!com.webcircuits.IHtmlTwin} [Step2Wr]
 * @prop {!com.webcircuits.IHtmlTwin} [Step3Wr]
 * @prop {!com.webcircuits.IHtmlTwin} [Step1Circle]
 * @prop {!com.webcircuits.IHtmlTwin} [Step2Circle]
 * @prop {!com.webcircuits.IHtmlTwin} [Step3Circle]
 */
/** @typedef {$xyz.swapee.wc.back.IProgressColumnDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ProgressColumnClasses>} xyz.swapee.wc.back.IProgressColumnDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ProgressColumnDisplay)} xyz.swapee.wc.back.AbstractProgressColumnDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ProgressColumnDisplay} xyz.swapee.wc.back.ProgressColumnDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IProgressColumnDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractProgressColumnDisplay
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractProgressColumnDisplay.constructor&xyz.swapee.wc.back.ProgressColumnDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractProgressColumnDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractProgressColumnDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.AbstractProgressColumnDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IProgressColumnDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.ProgressColumnClasses, null>)} xyz.swapee.wc.back.IProgressColumnDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IProgressColumnDisplay
 */
xyz.swapee.wc.back.IProgressColumnDisplay = class extends /** @type {xyz.swapee.wc.back.IProgressColumnDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IProgressColumnDisplay.paint} */
xyz.swapee.wc.back.IProgressColumnDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnDisplay.Initialese>)} xyz.swapee.wc.back.ProgressColumnDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IProgressColumnDisplay} xyz.swapee.wc.back.IProgressColumnDisplay.typeof */
/**
 * A concrete class of _IProgressColumnDisplay_ instances.
 * @constructor xyz.swapee.wc.back.ProgressColumnDisplay
 * @implements {xyz.swapee.wc.back.IProgressColumnDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.ProgressColumnDisplay = class extends /** @type {xyz.swapee.wc.back.ProgressColumnDisplay.constructor&xyz.swapee.wc.back.IProgressColumnDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.ProgressColumnDisplay.prototype.constructor = xyz.swapee.wc.back.ProgressColumnDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnDisplay}
 */
xyz.swapee.wc.back.ProgressColumnDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnDisplay.
 * @interface xyz.swapee.wc.back.IProgressColumnDisplayFields
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycBlock = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycRequired = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.KycNotRequired = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.ExchangeEmail = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.InfoNotice = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.SupportNotice = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.PaymentStatusWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.PaymentStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionStatusWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2BotSep = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Co = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Co = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Co = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1La = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2La = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3La = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionIdWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.TransactionIdLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1ProgressCircleWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2ProgressCircleWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Wr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Wr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Wr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step1Circle = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step2Circle = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IProgressColumnDisplayFields.prototype.Step3Circle = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IProgressColumnDisplay} */
xyz.swapee.wc.back.RecordIProgressColumnDisplay

/** @typedef {xyz.swapee.wc.back.IProgressColumnDisplay} xyz.swapee.wc.back.BoundIProgressColumnDisplay */

/** @typedef {xyz.swapee.wc.back.ProgressColumnDisplay} xyz.swapee.wc.back.BoundProgressColumnDisplay */

/**
 * Contains getters to cast the _IProgressColumnDisplay_ interface.
 * @interface xyz.swapee.wc.back.IProgressColumnDisplayCaster
 */
xyz.swapee.wc.back.IProgressColumnDisplayCaster = class { }
/**
 * Cast the _IProgressColumnDisplay_ instance into the _BoundIProgressColumnDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIProgressColumnDisplay}
 */
xyz.swapee.wc.back.IProgressColumnDisplayCaster.prototype.asIProgressColumnDisplay
/**
 * Access the _ProgressColumnDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundProgressColumnDisplay}
 */
xyz.swapee.wc.back.IProgressColumnDisplayCaster.prototype.superProgressColumnDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.ProgressColumnMemory, land?: null) => void} xyz.swapee.wc.back.IProgressColumnDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IProgressColumnDisplay.__paint<!xyz.swapee.wc.back.IProgressColumnDisplay>} xyz.swapee.wc.back.IProgressColumnDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IProgressColumnDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ProgressColumnMemory} [memory] The display data.
 * - `creatingTransaction` _boolean_ The user has entered her address and accepted the terms'n'conditions, the
 * transaction is being created. Default `false`.
 * - `finishedOnPayment` _boolean_ Whether the payment wasn't completed but the transaction is finished (e.g.,
 * overdue or expired). The exchange is unsuccessful. Default `false`.
 * - `paymentFailed` _boolean_ The payment during step 2 did not succeed. Default `false`.
 * - `paymentCompleted` _boolean_ The second, payment step, was successful. Moving on to the third step. Default `false`.
 * - `transactionId` _string_ The ID of the created transaction. Default empty string.
 * - `paymentStatus` _string_ The status of the payment.
 * Can be either:
 * > - _waiting_
 * > - _confirming_
 * > - _confirmed_Default empty string.
 * - `loadingStep1` _boolean_ Default `false`.
 * - `loadingStep2` _boolean_ Default `false`.
 * - `loadingStep3` _boolean_ Default `false`.
 * - `status` _string_ Default empty string.
 * - `processingTransaction` _boolean_ The third step, where the moneys is exchanged and sent to the user. Default `false`.
 * - `transactionStatus` _string_ The third step, where the moneys is exchanged and sent to the user.
 * Choose between:
 * > - _exchanging_
 * > - _sending_
 * > - _finished_
 * > - _failed_Default empty string.
 * - `transactionCompleted` _boolean_ Everything is done. Default `false`.
 * - `transactionFailed` _boolean_ Transaction wasn't successful. Default `false`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IProgressColumnDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IProgressColumnDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/41-ProgressColumnClasses.xml}  0b33a67f2f14608fcea73f64c0ab6978 */
/**
 * The classes of the _IProgressColumnDisplay_.
 * @record xyz.swapee.wc.ProgressColumnClasses
 */
xyz.swapee.wc.ProgressColumnClasses = class { }
/**
 * Circles in steps which are being transitioned to next.
 */
xyz.swapee.wc.ProgressColumnClasses.prototype.CircleLoading = /** @type {string|undefined} */ (void 0)
/**
 * Circles in steps which have been completed.
 */
xyz.swapee.wc.ProgressColumnClasses.prototype.CircleComplete = /** @type {string|undefined} */ (void 0)
/**
 * Circles in future steps.
 */
xyz.swapee.wc.ProgressColumnClasses.prototype.CircleWaiting = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ProgressColumnClasses.prototype.CircleFailed = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ProgressColumnClasses.prototype.StepLabelFailed = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ProgressColumnClasses.prototype.StepLabelWaiting = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ProgressColumnClasses.prototype.props = /** @type {xyz.swapee.wc.ProgressColumnClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/50-IProgressColumnController.xml}  c7f70528ab5e712c9024b430319befe0 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>} xyz.swapee.wc.IProgressColumnController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnController)} xyz.swapee.wc.AbstractProgressColumnController.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnController} xyz.swapee.wc.ProgressColumnController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnController` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnController
 */
xyz.swapee.wc.AbstractProgressColumnController = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnController.constructor&xyz.swapee.wc.ProgressColumnController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnController.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnController.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.AbstractProgressColumnController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnController.Initialese[]) => xyz.swapee.wc.IProgressColumnController} xyz.swapee.wc.ProgressColumnControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IProgressColumnOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.IProgressColumnController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IProgressColumnController.Inputs, !xyz.swapee.wc.ProgressColumnMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IProgressColumnController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IProgressColumnController.Inputs>)} xyz.swapee.wc.IProgressColumnController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IProgressColumnController
 */
xyz.swapee.wc.IProgressColumnController = class extends /** @type {xyz.swapee.wc.IProgressColumnController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IProgressColumnController.resetPort} */
xyz.swapee.wc.IProgressColumnController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnController&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnController.Initialese>)} xyz.swapee.wc.ProgressColumnController.constructor */
/**
 * A concrete class of _IProgressColumnController_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnController
 * @implements {xyz.swapee.wc.IProgressColumnController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnController.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnController = class extends /** @type {xyz.swapee.wc.ProgressColumnController.constructor&xyz.swapee.wc.IProgressColumnController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnController}
 */
xyz.swapee.wc.ProgressColumnController.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnController.
 * @interface xyz.swapee.wc.IProgressColumnControllerFields
 */
xyz.swapee.wc.IProgressColumnControllerFields = class { }
/**
 * The inputs to the _IProgressColumn_'s controller.
 */
xyz.swapee.wc.IProgressColumnControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IProgressColumnController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IProgressColumnControllerFields.prototype.props = /** @type {xyz.swapee.wc.IProgressColumnController} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnController} */
xyz.swapee.wc.RecordIProgressColumnController

/** @typedef {xyz.swapee.wc.IProgressColumnController} xyz.swapee.wc.BoundIProgressColumnController */

/** @typedef {xyz.swapee.wc.ProgressColumnController} xyz.swapee.wc.BoundProgressColumnController */

/** @typedef {xyz.swapee.wc.IProgressColumnPort.Inputs} xyz.swapee.wc.IProgressColumnController.Inputs The inputs to the _IProgressColumn_'s controller. */

/** @typedef {xyz.swapee.wc.IProgressColumnPort.WeakInputs} xyz.swapee.wc.IProgressColumnController.WeakInputs The inputs to the _IProgressColumn_'s controller. */

/**
 * Contains getters to cast the _IProgressColumnController_ interface.
 * @interface xyz.swapee.wc.IProgressColumnControllerCaster
 */
xyz.swapee.wc.IProgressColumnControllerCaster = class { }
/**
 * Cast the _IProgressColumnController_ instance into the _BoundIProgressColumnController_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnController}
 */
xyz.swapee.wc.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/**
 * Cast the _IProgressColumnController_ instance into the _BoundIProgressColumnProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnProcessor}
 */
xyz.swapee.wc.IProgressColumnControllerCaster.prototype.asIProgressColumnProcessor
/**
 * Access the _ProgressColumnController_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnController}
 */
xyz.swapee.wc.IProgressColumnControllerCaster.prototype.superProgressColumnController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IProgressColumnController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IProgressColumnController.__resetPort<!xyz.swapee.wc.IProgressColumnController>} xyz.swapee.wc.IProgressColumnController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IProgressColumnController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IProgressColumnController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/51-IProgressColumnControllerFront.xml}  f2da925ed07f59774704a581276a9c79 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IProgressColumnController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ProgressColumnController)} xyz.swapee.wc.front.AbstractProgressColumnController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ProgressColumnController} xyz.swapee.wc.front.ProgressColumnController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IProgressColumnController` interface.
 * @constructor xyz.swapee.wc.front.AbstractProgressColumnController
 */
xyz.swapee.wc.front.AbstractProgressColumnController = class extends /** @type {xyz.swapee.wc.front.AbstractProgressColumnController.constructor&xyz.swapee.wc.front.ProgressColumnController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractProgressColumnController.prototype.constructor = xyz.swapee.wc.front.AbstractProgressColumnController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractProgressColumnController.class = /** @type {typeof xyz.swapee.wc.front.AbstractProgressColumnController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.AbstractProgressColumnController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IProgressColumnController.Initialese[]) => xyz.swapee.wc.front.IProgressColumnController} xyz.swapee.wc.front.ProgressColumnControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IProgressColumnControllerCaster&xyz.swapee.wc.front.IProgressColumnControllerAT)} xyz.swapee.wc.front.IProgressColumnController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IProgressColumnControllerAT} xyz.swapee.wc.front.IProgressColumnControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IProgressColumnController
 */
xyz.swapee.wc.front.IProgressColumnController = class extends /** @type {xyz.swapee.wc.front.IProgressColumnController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IProgressColumnControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IProgressColumnController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnController.Initialese>)} xyz.swapee.wc.front.ProgressColumnController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IProgressColumnController} xyz.swapee.wc.front.IProgressColumnController.typeof */
/**
 * A concrete class of _IProgressColumnController_ instances.
 * @constructor xyz.swapee.wc.front.ProgressColumnController
 * @implements {xyz.swapee.wc.front.IProgressColumnController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnController.Initialese>} ‎
 */
xyz.swapee.wc.front.ProgressColumnController = class extends /** @type {xyz.swapee.wc.front.ProgressColumnController.constructor&xyz.swapee.wc.front.IProgressColumnController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IProgressColumnController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ProgressColumnController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnController}
 */
xyz.swapee.wc.front.ProgressColumnController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IProgressColumnController} */
xyz.swapee.wc.front.RecordIProgressColumnController

/** @typedef {xyz.swapee.wc.front.IProgressColumnController} xyz.swapee.wc.front.BoundIProgressColumnController */

/** @typedef {xyz.swapee.wc.front.ProgressColumnController} xyz.swapee.wc.front.BoundProgressColumnController */

/**
 * Contains getters to cast the _IProgressColumnController_ interface.
 * @interface xyz.swapee.wc.front.IProgressColumnControllerCaster
 */
xyz.swapee.wc.front.IProgressColumnControllerCaster = class { }
/**
 * Cast the _IProgressColumnController_ instance into the _BoundIProgressColumnController_ type.
 * @type {!xyz.swapee.wc.front.BoundIProgressColumnController}
 */
xyz.swapee.wc.front.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/**
 * Access the _ProgressColumnController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundProgressColumnController}
 */
xyz.swapee.wc.front.IProgressColumnControllerCaster.prototype.superProgressColumnController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/52-IProgressColumnControllerBack.xml}  6ce0d61709720874c0c4a04e334fc00e */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IProgressColumnController.Inputs>&xyz.swapee.wc.IProgressColumnController.Initialese} xyz.swapee.wc.back.IProgressColumnController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ProgressColumnController)} xyz.swapee.wc.back.AbstractProgressColumnController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ProgressColumnController} xyz.swapee.wc.back.ProgressColumnController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IProgressColumnController` interface.
 * @constructor xyz.swapee.wc.back.AbstractProgressColumnController
 */
xyz.swapee.wc.back.AbstractProgressColumnController = class extends /** @type {xyz.swapee.wc.back.AbstractProgressColumnController.constructor&xyz.swapee.wc.back.ProgressColumnController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractProgressColumnController.prototype.constructor = xyz.swapee.wc.back.AbstractProgressColumnController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractProgressColumnController.class = /** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnController|typeof xyz.swapee.wc.back.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.AbstractProgressColumnController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IProgressColumnController.Initialese[]) => xyz.swapee.wc.back.IProgressColumnController} xyz.swapee.wc.back.ProgressColumnControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IProgressColumnControllerCaster&xyz.swapee.wc.IProgressColumnController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IProgressColumnController.Inputs>)} xyz.swapee.wc.back.IProgressColumnController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IProgressColumnController
 */
xyz.swapee.wc.back.IProgressColumnController = class extends /** @type {xyz.swapee.wc.back.IProgressColumnController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IProgressColumnController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IProgressColumnController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnController.Initialese>)} xyz.swapee.wc.back.ProgressColumnController.constructor */
/**
 * A concrete class of _IProgressColumnController_ instances.
 * @constructor xyz.swapee.wc.back.ProgressColumnController
 * @implements {xyz.swapee.wc.back.IProgressColumnController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnController.Initialese>} ‎
 */
xyz.swapee.wc.back.ProgressColumnController = class extends /** @type {xyz.swapee.wc.back.ProgressColumnController.constructor&xyz.swapee.wc.back.IProgressColumnController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ProgressColumnController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnController}
 */
xyz.swapee.wc.back.ProgressColumnController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IProgressColumnController} */
xyz.swapee.wc.back.RecordIProgressColumnController

/** @typedef {xyz.swapee.wc.back.IProgressColumnController} xyz.swapee.wc.back.BoundIProgressColumnController */

/** @typedef {xyz.swapee.wc.back.ProgressColumnController} xyz.swapee.wc.back.BoundProgressColumnController */

/**
 * Contains getters to cast the _IProgressColumnController_ interface.
 * @interface xyz.swapee.wc.back.IProgressColumnControllerCaster
 */
xyz.swapee.wc.back.IProgressColumnControllerCaster = class { }
/**
 * Cast the _IProgressColumnController_ instance into the _BoundIProgressColumnController_ type.
 * @type {!xyz.swapee.wc.back.BoundIProgressColumnController}
 */
xyz.swapee.wc.back.IProgressColumnControllerCaster.prototype.asIProgressColumnController
/**
 * Access the _ProgressColumnController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundProgressColumnController}
 */
xyz.swapee.wc.back.IProgressColumnControllerCaster.prototype.superProgressColumnController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/53-IProgressColumnControllerAR.xml}  e45cbe72dd717830455ba7bd3b2b3ba5 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IProgressColumnController.Initialese} xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ProgressColumnControllerAR)} xyz.swapee.wc.back.AbstractProgressColumnControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ProgressColumnControllerAR} xyz.swapee.wc.back.ProgressColumnControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IProgressColumnControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractProgressColumnControllerAR
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractProgressColumnControllerAR.constructor&xyz.swapee.wc.back.ProgressColumnControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractProgressColumnControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnControllerAR|typeof xyz.swapee.wc.back.ProgressColumnControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnController|typeof xyz.swapee.wc.ProgressColumnController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.AbstractProgressColumnControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese[]) => xyz.swapee.wc.back.IProgressColumnControllerAR} xyz.swapee.wc.back.ProgressColumnControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IProgressColumnControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IProgressColumnController)} xyz.swapee.wc.back.IProgressColumnControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IProgressColumnControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IProgressColumnControllerAR
 */
xyz.swapee.wc.back.IProgressColumnControllerAR = class extends /** @type {xyz.swapee.wc.back.IProgressColumnControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IProgressColumnController.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IProgressColumnControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese>)} xyz.swapee.wc.back.ProgressColumnControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IProgressColumnControllerAR} xyz.swapee.wc.back.IProgressColumnControllerAR.typeof */
/**
 * A concrete class of _IProgressColumnControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.ProgressColumnControllerAR
 * @implements {xyz.swapee.wc.back.IProgressColumnControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IProgressColumnControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.ProgressColumnControllerAR = class extends /** @type {xyz.swapee.wc.back.ProgressColumnControllerAR.constructor&xyz.swapee.wc.back.IProgressColumnControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ProgressColumnControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnControllerAR}
 */
xyz.swapee.wc.back.ProgressColumnControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IProgressColumnControllerAR} */
xyz.swapee.wc.back.RecordIProgressColumnControllerAR

/** @typedef {xyz.swapee.wc.back.IProgressColumnControllerAR} xyz.swapee.wc.back.BoundIProgressColumnControllerAR */

/** @typedef {xyz.swapee.wc.back.ProgressColumnControllerAR} xyz.swapee.wc.back.BoundProgressColumnControllerAR */

/**
 * Contains getters to cast the _IProgressColumnControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IProgressColumnControllerARCaster
 */
xyz.swapee.wc.back.IProgressColumnControllerARCaster = class { }
/**
 * Cast the _IProgressColumnControllerAR_ instance into the _BoundIProgressColumnControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIProgressColumnControllerAR}
 */
xyz.swapee.wc.back.IProgressColumnControllerARCaster.prototype.asIProgressColumnControllerAR
/**
 * Access the _ProgressColumnControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundProgressColumnControllerAR}
 */
xyz.swapee.wc.back.IProgressColumnControllerARCaster.prototype.superProgressColumnControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/54-IProgressColumnControllerAT.xml}  f0607b6719617d570d52e32357a7b5d1 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ProgressColumnControllerAT)} xyz.swapee.wc.front.AbstractProgressColumnControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ProgressColumnControllerAT} xyz.swapee.wc.front.ProgressColumnControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IProgressColumnControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractProgressColumnControllerAT
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractProgressColumnControllerAT.constructor&xyz.swapee.wc.front.ProgressColumnControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractProgressColumnControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractProgressColumnControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnControllerAT|typeof xyz.swapee.wc.front.ProgressColumnControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.AbstractProgressColumnControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese[]) => xyz.swapee.wc.front.IProgressColumnControllerAT} xyz.swapee.wc.front.ProgressColumnControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IProgressColumnControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IProgressColumnControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IProgressColumnControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IProgressColumnControllerAT
 */
xyz.swapee.wc.front.IProgressColumnControllerAT = class extends /** @type {xyz.swapee.wc.front.IProgressColumnControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IProgressColumnControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese>)} xyz.swapee.wc.front.ProgressColumnControllerAT.constructor */
/**
 * A concrete class of _IProgressColumnControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.ProgressColumnControllerAT
 * @implements {xyz.swapee.wc.front.IProgressColumnControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IProgressColumnControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.ProgressColumnControllerAT = class extends /** @type {xyz.swapee.wc.front.ProgressColumnControllerAT.constructor&xyz.swapee.wc.front.IProgressColumnControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IProgressColumnControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ProgressColumnControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnControllerAT}
 */
xyz.swapee.wc.front.ProgressColumnControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IProgressColumnControllerAT} */
xyz.swapee.wc.front.RecordIProgressColumnControllerAT

/** @typedef {xyz.swapee.wc.front.IProgressColumnControllerAT} xyz.swapee.wc.front.BoundIProgressColumnControllerAT */

/** @typedef {xyz.swapee.wc.front.ProgressColumnControllerAT} xyz.swapee.wc.front.BoundProgressColumnControllerAT */

/**
 * Contains getters to cast the _IProgressColumnControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IProgressColumnControllerATCaster
 */
xyz.swapee.wc.front.IProgressColumnControllerATCaster = class { }
/**
 * Cast the _IProgressColumnControllerAT_ instance into the _BoundIProgressColumnControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIProgressColumnControllerAT}
 */
xyz.swapee.wc.front.IProgressColumnControllerATCaster.prototype.asIProgressColumnControllerAT
/**
 * Access the _ProgressColumnControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundProgressColumnControllerAT}
 */
xyz.swapee.wc.front.IProgressColumnControllerATCaster.prototype.superProgressColumnControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreen.xml}  d7566ef3d069eb30deabb42b51cba70d */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null>&xyz.swapee.wc.IProgressColumnDisplay.Initialese} xyz.swapee.wc.IProgressColumnScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnScreen)} xyz.swapee.wc.AbstractProgressColumnScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnScreen} xyz.swapee.wc.ProgressColumnScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnScreen` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnScreen
 */
xyz.swapee.wc.AbstractProgressColumnScreen = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnScreen.constructor&xyz.swapee.wc.ProgressColumnScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnScreen.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnScreen.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IProgressColumnController|typeof xyz.swapee.wc.front.ProgressColumnController)|(!xyz.swapee.wc.IProgressColumnDisplay|typeof xyz.swapee.wc.ProgressColumnDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.AbstractProgressColumnScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnScreen.Initialese[]) => xyz.swapee.wc.IProgressColumnScreen} xyz.swapee.wc.ProgressColumnScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.ProgressColumnMemory, !xyz.swapee.wc.front.ProgressColumnInputs, !HTMLDivElement, !xyz.swapee.wc.IProgressColumnDisplay.Settings, !xyz.swapee.wc.IProgressColumnDisplay.Queries, null, null>&xyz.swapee.wc.front.IProgressColumnController&xyz.swapee.wc.IProgressColumnDisplay)} xyz.swapee.wc.IProgressColumnScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IProgressColumnScreen
 */
xyz.swapee.wc.IProgressColumnScreen = class extends /** @type {xyz.swapee.wc.IProgressColumnScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IProgressColumnController.typeof&xyz.swapee.wc.IProgressColumnDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnScreen.Initialese>)} xyz.swapee.wc.ProgressColumnScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IProgressColumnScreen} xyz.swapee.wc.IProgressColumnScreen.typeof */
/**
 * A concrete class of _IProgressColumnScreen_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnScreen
 * @implements {xyz.swapee.wc.IProgressColumnScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnScreen.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnScreen = class extends /** @type {xyz.swapee.wc.ProgressColumnScreen.constructor&xyz.swapee.wc.IProgressColumnScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnScreen}
 */
xyz.swapee.wc.ProgressColumnScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IProgressColumnScreen} */
xyz.swapee.wc.RecordIProgressColumnScreen

/** @typedef {xyz.swapee.wc.IProgressColumnScreen} xyz.swapee.wc.BoundIProgressColumnScreen */

/** @typedef {xyz.swapee.wc.ProgressColumnScreen} xyz.swapee.wc.BoundProgressColumnScreen */

/**
 * Contains getters to cast the _IProgressColumnScreen_ interface.
 * @interface xyz.swapee.wc.IProgressColumnScreenCaster
 */
xyz.swapee.wc.IProgressColumnScreenCaster = class { }
/**
 * Cast the _IProgressColumnScreen_ instance into the _BoundIProgressColumnScreen_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnScreen}
 */
xyz.swapee.wc.IProgressColumnScreenCaster.prototype.asIProgressColumnScreen
/**
 * Access the _ProgressColumnScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnScreen}
 */
xyz.swapee.wc.IProgressColumnScreenCaster.prototype.superProgressColumnScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/70-IProgressColumnScreenBack.xml}  c7c02329346ae2316b93a97c3a90c3f7 */
/** @typedef {xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} xyz.swapee.wc.back.IProgressColumnScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ProgressColumnScreen)} xyz.swapee.wc.back.AbstractProgressColumnScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ProgressColumnScreen} xyz.swapee.wc.back.ProgressColumnScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IProgressColumnScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractProgressColumnScreen
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen = class extends /** @type {xyz.swapee.wc.back.AbstractProgressColumnScreen.constructor&xyz.swapee.wc.back.ProgressColumnScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractProgressColumnScreen.prototype.constructor = xyz.swapee.wc.back.AbstractProgressColumnScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnScreen|typeof xyz.swapee.wc.back.ProgressColumnScreen)|(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IProgressColumnScreen.Initialese[]) => xyz.swapee.wc.back.IProgressColumnScreen} xyz.swapee.wc.back.ProgressColumnScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IProgressColumnScreenCaster&xyz.swapee.wc.back.IProgressColumnScreenAT)} xyz.swapee.wc.back.IProgressColumnScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IProgressColumnScreenAT} xyz.swapee.wc.back.IProgressColumnScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IProgressColumnScreen
 */
xyz.swapee.wc.back.IProgressColumnScreen = class extends /** @type {xyz.swapee.wc.back.IProgressColumnScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IProgressColumnScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IProgressColumnScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreen.Initialese>)} xyz.swapee.wc.back.ProgressColumnScreen.constructor */
/**
 * A concrete class of _IProgressColumnScreen_ instances.
 * @constructor xyz.swapee.wc.back.ProgressColumnScreen
 * @implements {xyz.swapee.wc.back.IProgressColumnScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.ProgressColumnScreen = class extends /** @type {xyz.swapee.wc.back.ProgressColumnScreen.constructor&xyz.swapee.wc.back.IProgressColumnScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ProgressColumnScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreen}
 */
xyz.swapee.wc.back.ProgressColumnScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IProgressColumnScreen} */
xyz.swapee.wc.back.RecordIProgressColumnScreen

/** @typedef {xyz.swapee.wc.back.IProgressColumnScreen} xyz.swapee.wc.back.BoundIProgressColumnScreen */

/** @typedef {xyz.swapee.wc.back.ProgressColumnScreen} xyz.swapee.wc.back.BoundProgressColumnScreen */

/**
 * Contains getters to cast the _IProgressColumnScreen_ interface.
 * @interface xyz.swapee.wc.back.IProgressColumnScreenCaster
 */
xyz.swapee.wc.back.IProgressColumnScreenCaster = class { }
/**
 * Cast the _IProgressColumnScreen_ instance into the _BoundIProgressColumnScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIProgressColumnScreen}
 */
xyz.swapee.wc.back.IProgressColumnScreenCaster.prototype.asIProgressColumnScreen
/**
 * Access the _ProgressColumnScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundProgressColumnScreen}
 */
xyz.swapee.wc.back.IProgressColumnScreenCaster.prototype.superProgressColumnScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/73-IProgressColumnScreenAR.xml}  88615c5c8861c84b9589f579fed972d4 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IProgressColumnScreen.Initialese} xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ProgressColumnScreenAR)} xyz.swapee.wc.front.AbstractProgressColumnScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ProgressColumnScreenAR} xyz.swapee.wc.front.ProgressColumnScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IProgressColumnScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractProgressColumnScreenAR
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractProgressColumnScreenAR.constructor&xyz.swapee.wc.front.ProgressColumnScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractProgressColumnScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractProgressColumnScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IProgressColumnScreenAR|typeof xyz.swapee.wc.front.ProgressColumnScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IProgressColumnScreen|typeof xyz.swapee.wc.ProgressColumnScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.AbstractProgressColumnScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese[]) => xyz.swapee.wc.front.IProgressColumnScreenAR} xyz.swapee.wc.front.ProgressColumnScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IProgressColumnScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IProgressColumnScreen)} xyz.swapee.wc.front.IProgressColumnScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IProgressColumnScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IProgressColumnScreenAR
 */
xyz.swapee.wc.front.IProgressColumnScreenAR = class extends /** @type {xyz.swapee.wc.front.IProgressColumnScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IProgressColumnScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IProgressColumnScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IProgressColumnScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese>)} xyz.swapee.wc.front.ProgressColumnScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IProgressColumnScreenAR} xyz.swapee.wc.front.IProgressColumnScreenAR.typeof */
/**
 * A concrete class of _IProgressColumnScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.ProgressColumnScreenAR
 * @implements {xyz.swapee.wc.front.IProgressColumnScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IProgressColumnScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.ProgressColumnScreenAR = class extends /** @type {xyz.swapee.wc.front.ProgressColumnScreenAR.constructor&xyz.swapee.wc.front.IProgressColumnScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IProgressColumnScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ProgressColumnScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ProgressColumnScreenAR}
 */
xyz.swapee.wc.front.ProgressColumnScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IProgressColumnScreenAR} */
xyz.swapee.wc.front.RecordIProgressColumnScreenAR

/** @typedef {xyz.swapee.wc.front.IProgressColumnScreenAR} xyz.swapee.wc.front.BoundIProgressColumnScreenAR */

/** @typedef {xyz.swapee.wc.front.ProgressColumnScreenAR} xyz.swapee.wc.front.BoundProgressColumnScreenAR */

/**
 * Contains getters to cast the _IProgressColumnScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IProgressColumnScreenARCaster
 */
xyz.swapee.wc.front.IProgressColumnScreenARCaster = class { }
/**
 * Cast the _IProgressColumnScreenAR_ instance into the _BoundIProgressColumnScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIProgressColumnScreenAR}
 */
xyz.swapee.wc.front.IProgressColumnScreenARCaster.prototype.asIProgressColumnScreenAR
/**
 * Access the _ProgressColumnScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundProgressColumnScreenAR}
 */
xyz.swapee.wc.front.IProgressColumnScreenARCaster.prototype.superProgressColumnScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/74-IProgressColumnScreenAT.xml}  94d9d7a2fe82060c43cba0bfe9d03005 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ProgressColumnScreenAT)} xyz.swapee.wc.back.AbstractProgressColumnScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ProgressColumnScreenAT} xyz.swapee.wc.back.ProgressColumnScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IProgressColumnScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractProgressColumnScreenAT
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractProgressColumnScreenAT.constructor&xyz.swapee.wc.back.ProgressColumnScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractProgressColumnScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IProgressColumnScreenAT|typeof xyz.swapee.wc.back.ProgressColumnScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.AbstractProgressColumnScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese[]) => xyz.swapee.wc.back.IProgressColumnScreenAT} xyz.swapee.wc.back.ProgressColumnScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IProgressColumnScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IProgressColumnScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IProgressColumnScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IProgressColumnScreenAT
 */
xyz.swapee.wc.back.IProgressColumnScreenAT = class extends /** @type {xyz.swapee.wc.back.IProgressColumnScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IProgressColumnScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IProgressColumnScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese>)} xyz.swapee.wc.back.ProgressColumnScreenAT.constructor */
/**
 * A concrete class of _IProgressColumnScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.ProgressColumnScreenAT
 * @implements {xyz.swapee.wc.back.IProgressColumnScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IProgressColumnScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.ProgressColumnScreenAT = class extends /** @type {xyz.swapee.wc.back.ProgressColumnScreenAT.constructor&xyz.swapee.wc.back.IProgressColumnScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IProgressColumnScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ProgressColumnScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ProgressColumnScreenAT}
 */
xyz.swapee.wc.back.ProgressColumnScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IProgressColumnScreenAT} */
xyz.swapee.wc.back.RecordIProgressColumnScreenAT

/** @typedef {xyz.swapee.wc.back.IProgressColumnScreenAT} xyz.swapee.wc.back.BoundIProgressColumnScreenAT */

/** @typedef {xyz.swapee.wc.back.ProgressColumnScreenAT} xyz.swapee.wc.back.BoundProgressColumnScreenAT */

/**
 * Contains getters to cast the _IProgressColumnScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IProgressColumnScreenATCaster
 */
xyz.swapee.wc.back.IProgressColumnScreenATCaster = class { }
/**
 * Cast the _IProgressColumnScreenAT_ instance into the _BoundIProgressColumnScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIProgressColumnScreenAT}
 */
xyz.swapee.wc.back.IProgressColumnScreenATCaster.prototype.asIProgressColumnScreenAT
/**
 * Access the _ProgressColumnScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundProgressColumnScreenAT}
 */
xyz.swapee.wc.back.IProgressColumnScreenATCaster.prototype.superProgressColumnScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/progress-column/ProgressColumn.mvc/design/80-IProgressColumnGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IProgressColumnDisplay.Initialese} xyz.swapee.wc.IProgressColumnGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ProgressColumnGPU)} xyz.swapee.wc.AbstractProgressColumnGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ProgressColumnGPU} xyz.swapee.wc.ProgressColumnGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnGPU` interface.
 * @constructor xyz.swapee.wc.AbstractProgressColumnGPU
 */
xyz.swapee.wc.AbstractProgressColumnGPU = class extends /** @type {xyz.swapee.wc.AbstractProgressColumnGPU.constructor&xyz.swapee.wc.ProgressColumnGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractProgressColumnGPU.prototype.constructor = xyz.swapee.wc.AbstractProgressColumnGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractProgressColumnGPU.class = /** @type {typeof xyz.swapee.wc.AbstractProgressColumnGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractProgressColumnGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IProgressColumnGPU|typeof xyz.swapee.wc.ProgressColumnGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IProgressColumnDisplay|typeof xyz.swapee.wc.back.ProgressColumnDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.AbstractProgressColumnGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IProgressColumnGPU.Initialese[]) => xyz.swapee.wc.IProgressColumnGPU} xyz.swapee.wc.ProgressColumnGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IProgressColumnGPUCaster&com.webcircuits.IBrowserView<.!ProgressColumnMemory,>&xyz.swapee.wc.back.IProgressColumnDisplay)} xyz.swapee.wc.IProgressColumnGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!ProgressColumnMemory,>} com.webcircuits.IBrowserView<.!ProgressColumnMemory,>.typeof */
/**
 * Handles the periphery of the _IProgressColumnDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IProgressColumnGPU
 */
xyz.swapee.wc.IProgressColumnGPU = class extends /** @type {xyz.swapee.wc.IProgressColumnGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!ProgressColumnMemory,>.typeof&xyz.swapee.wc.back.IProgressColumnDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IProgressColumnGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IProgressColumnGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IProgressColumnGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnGPU.Initialese>)} xyz.swapee.wc.ProgressColumnGPU.constructor */
/**
 * A concrete class of _IProgressColumnGPU_ instances.
 * @constructor xyz.swapee.wc.ProgressColumnGPU
 * @implements {xyz.swapee.wc.IProgressColumnGPU} Handles the periphery of the _IProgressColumnDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IProgressColumnGPU.Initialese>} ‎
 */
xyz.swapee.wc.ProgressColumnGPU = class extends /** @type {xyz.swapee.wc.ProgressColumnGPU.constructor&xyz.swapee.wc.IProgressColumnGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IProgressColumnGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IProgressColumnGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IProgressColumnGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ProgressColumnGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ProgressColumnGPU}
 */
xyz.swapee.wc.ProgressColumnGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IProgressColumnGPU.
 * @interface xyz.swapee.wc.IProgressColumnGPUFields
 */
xyz.swapee.wc.IProgressColumnGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IProgressColumnGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IProgressColumnGPU} */
xyz.swapee.wc.RecordIProgressColumnGPU

/** @typedef {xyz.swapee.wc.IProgressColumnGPU} xyz.swapee.wc.BoundIProgressColumnGPU */

/** @typedef {xyz.swapee.wc.ProgressColumnGPU} xyz.swapee.wc.BoundProgressColumnGPU */

/**
 * Contains getters to cast the _IProgressColumnGPU_ interface.
 * @interface xyz.swapee.wc.IProgressColumnGPUCaster
 */
xyz.swapee.wc.IProgressColumnGPUCaster = class { }
/**
 * Cast the _IProgressColumnGPU_ instance into the _BoundIProgressColumnGPU_ type.
 * @type {!xyz.swapee.wc.BoundIProgressColumnGPU}
 */
xyz.swapee.wc.IProgressColumnGPUCaster.prototype.asIProgressColumnGPU
/**
 * Access the _ProgressColumnGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundProgressColumnGPU}
 */
xyz.swapee.wc.IProgressColumnGPUCaster.prototype.superProgressColumnGPU

// nss:xyz.swapee.wc
/* @typal-end */