import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnScreenAT}
 */
function __AbstractProgressColumnScreenAT() {}
__AbstractProgressColumnScreenAT.prototype = /** @type {!_AbstractProgressColumnScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnScreenAT}
 */
class _AbstractProgressColumnScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IProgressColumnScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnScreenAT} ‎
 */
class AbstractProgressColumnScreenAT extends newAbstract(
 _AbstractProgressColumnScreenAT,'IProgressColumnScreenAT',null,{
  asIProgressColumnScreenAT:1,
  superProgressColumnScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnScreenAT} */
AbstractProgressColumnScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnScreenAT} */
function AbstractProgressColumnScreenATClass(){}

export default AbstractProgressColumnScreenAT


AbstractProgressColumnScreenAT[$implementations]=[
 __AbstractProgressColumnScreenAT,
 UartUniversal,
]