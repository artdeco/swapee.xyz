import AbstractProgressColumnGPU from '../AbstractProgressColumnGPU'
import AbstractProgressColumnScreenBack from '../AbstractProgressColumnScreenBack'
import {HtmlComponent,makeRevealConcealPaints,mvc} from '@webcircuits/webcircuits'
import {access} from '@mauriceguest/guest2'
import {ProgressColumnInputsQPs} from '../../pqs/ProgressColumnInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractProgressColumn from '../AbstractProgressColumn'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnHtmlComponent}
 */
function __AbstractProgressColumnHtmlComponent() {}
__AbstractProgressColumnHtmlComponent.prototype = /** @type {!_AbstractProgressColumnHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnHtmlComponent}
 */
class _AbstractProgressColumnHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ProgressColumnHtmlComponent} */ (res)
  }
}
/**
 * The _IProgressColumn_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractProgressColumnHtmlComponent} ‎
 */
export class AbstractProgressColumnHtmlComponent extends newAbstract(
 _AbstractProgressColumnHtmlComponent,'IProgressColumnHtmlComponent',null,{
  asIProgressColumnHtmlComponent:1,
  superProgressColumnHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnHtmlComponent} */
AbstractProgressColumnHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnHtmlComponent} */
function AbstractProgressColumnHtmlComponentClass(){}


AbstractProgressColumnHtmlComponent[$implementations]=[
 __AbstractProgressColumnHtmlComponent,
 HtmlComponent,
 AbstractProgressColumn,
 AbstractProgressColumnGPU,
 AbstractProgressColumnScreenBack,
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  inputsQPs:ProgressColumnInputsQPs,
 }),

/** @type {!AbstractProgressColumnHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/function paintTransactionIdLa() {
   this.TransactionIdLa.setText(this.model.transactionId)
  },/**@this {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/function paintPaymentStatusLa() {
   this.PaymentStatusLa.setText(this.model.paymentStatus)
  },/**@this {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/function paintTransactionStatusLa() {
   this.TransactionStatusLa.setText(this.model.transactionStatus)
  }
 ] }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleLoading_on_Step1Circle({loadingStep1:loadingStep1}){
   const{
    asIProgressColumnGPU:{
     Step1Circle:Step1Circle,
    },
    classes:{CircleLoading:CircleLoading},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(loadingStep1) addClass(Step1Circle,CircleLoading)
   else removeClass(Step1Circle,CircleLoading)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleComplete_on_Step1Circle({transactionId:transactionId}){
   const{
    asIProgressColumnGPU:{
     Step1Circle:Step1Circle,
    },
    classes:{CircleComplete:CircleComplete},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(!!transactionId) addClass(Step1Circle,CircleComplete)
   else removeClass(Step1Circle,CircleComplete)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleLoading_on_Step2Circle({loadingStep2:loadingStep2}){
   const{
    asIProgressColumnGPU:{
     Step2Circle:Step2Circle,
    },
    classes:{CircleLoading:CircleLoading},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(loadingStep2) addClass(Step2Circle,CircleLoading)
   else removeClass(Step2Circle,CircleLoading)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleWaiting_on_Step2Circle({transactionId:transactionId}){
   const{
    asIProgressColumnGPU:{
     Step2Circle:Step2Circle,
    },
    classes:{CircleWaiting:CircleWaiting},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(!transactionId) addClass(Step2Circle,CircleWaiting)
   else removeClass(Step2Circle,CircleWaiting)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleFailed_on_Step2Circle({paymentFailed:paymentFailed}){
   const{
    asIProgressColumnGPU:{
     Step2Circle:Step2Circle,
    },
    classes:{CircleFailed:CircleFailed},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(paymentFailed) addClass(Step2Circle,CircleFailed)
   else removeClass(Step2Circle,CircleFailed)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleComplete_on_Step2Circle({paymentCompleted:paymentCompleted}){
   const{
    asIProgressColumnGPU:{
     Step2Circle:Step2Circle,
    },
    classes:{CircleComplete:CircleComplete},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(paymentCompleted) addClass(Step2Circle,CircleComplete)
   else removeClass(Step2Circle,CircleComplete)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleLoading_on_Step3Circle({loadingStep3:loadingStep3}){
   const{
    asIProgressColumnGPU:{
     Step3Circle:Step3Circle,
    },
    classes:{CircleLoading:CircleLoading},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(loadingStep3) addClass(Step3Circle,CircleLoading)
   else removeClass(Step3Circle,CircleLoading)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleWaiting_on_Step3Circle({transactionStatus:transactionStatus}){
   const{
    asIProgressColumnGPU:{
     Step3Circle:Step3Circle,
    },
    classes:{CircleWaiting:CircleWaiting},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(!transactionStatus) addClass(Step3Circle,CircleWaiting)
   else removeClass(Step3Circle,CircleWaiting)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleFailed_on_Step3Circle({status:status}){
   const{
    asIProgressColumnGPU:{
     Step3Circle:Step3Circle,
    },
    classes:{CircleFailed:CircleFailed},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(status=='failed') addClass(Step3Circle,CircleFailed)
   else removeClass(Step3Circle,CircleFailed)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_CircleComplete_on_Step3Circle({status:status}){
   const{
    asIProgressColumnGPU:{
     Step3Circle:Step3Circle,
    },
    classes:{CircleComplete:CircleComplete},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(status=='finished') addClass(Step3Circle,CircleComplete)
   else removeClass(Step3Circle,CircleComplete)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_StepLabelFailed_on_Step2La({paymentFailed:paymentFailed}){
   const{
    asIProgressColumnGPU:{
     Step2La:Step2La,
    },
    classes:{StepLabelFailed:StepLabelFailed},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(paymentFailed) addClass(Step2La,StepLabelFailed)
   else removeClass(Step2La,StepLabelFailed)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_StepLabelWaiting_on_Step2La({transactionId:transactionId}){
   const{
    asIProgressColumnGPU:{
     Step2La:Step2La,
    },
    classes:{StepLabelWaiting:StepLabelWaiting},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(!transactionId) addClass(Step2La,StepLabelWaiting)
   else removeClass(Step2La,StepLabelWaiting)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_StepLabelFailed_on_Step3La({status:status}){
   const{
    asIProgressColumnGPU:{
     Step3La:Step3La,
    },
    classes:{StepLabelFailed:StepLabelFailed},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(status=='failed') addClass(Step3La,StepLabelFailed)
   else removeClass(Step3La,StepLabelFailed)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:function paint_StepLabelWaiting_on_Step3La({transactionStatus:transactionStatus}){
   const{
    asIProgressColumnGPU:{
     Step3La:Step3La,
    },
    classes:{StepLabelWaiting:StepLabelWaiting},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(!transactionStatus) addClass(Step3La,StepLabelWaiting)
   else removeClass(Step3La,StepLabelWaiting)
  },
 }),
 AbstractProgressColumnHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnHtmlComponent}*/({
  paint:makeRevealConcealPaints({
   TransactionIdWr:{transactionId:1},
   PaymentStatusWr:{paymentStatus:1},
   TransactionStatusWr:{transactionStatus:1},
   Step3Wr:{finishedOnPayment:0},
   Step2BotSep:{finishedOnPayment:0},
  }),
 }),
]