import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnDisplay}
 */
function __AbstractProgressColumnDisplay() {}
__AbstractProgressColumnDisplay.prototype = /** @type {!_AbstractProgressColumnDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnDisplay}
 */
class _AbstractProgressColumnDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnDisplay} ‎
 */
class AbstractProgressColumnDisplay extends newAbstract(
 _AbstractProgressColumnDisplay,'IProgressColumnDisplay',null,{
  asIProgressColumnDisplay:1,
  superProgressColumnDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnDisplay} */
AbstractProgressColumnDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnDisplay} */
function AbstractProgressColumnDisplayClass(){}

export default AbstractProgressColumnDisplay


AbstractProgressColumnDisplay[$implementations]=[
 __AbstractProgressColumnDisplay,
 GraphicsDriverBack,
 AbstractProgressColumnDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IProgressColumnDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IProgressColumnDisplay}*/({
    TransactionIdWr:twinMock,
    Step1Circle:twinMock,
    Step2Circle:twinMock,
    Step3Circle:twinMock,
    Step2La:twinMock,
    Step3La:twinMock,
    TransactionIdLa:twinMock,
    PaymentStatusWr:twinMock,
    PaymentStatusLa:twinMock,
    TransactionStatusWr:twinMock,
    TransactionStatusLa:twinMock,
    Step3Wr:twinMock,
    Step2BotSep:twinMock,
    KycBlock:twinMock,
    KycRequired:twinMock,
    KycNotRequired:twinMock,
    ExchangeEmail:twinMock,
    InfoNotice:twinMock,
    SupportNotice:twinMock,
    Step1Co:twinMock,
    Step2Co:twinMock,
    Step3Co:twinMock,
    Step1La:twinMock,
    Step1ProgressCircleWr:twinMock,
    Step2ProgressCircleWr:twinMock,
    Step1Wr:twinMock,
    Step2Wr:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.ProgressColumnDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IProgressColumnDisplay.Initialese}*/({
   KycBlock:1,
   KycRequired:1,
   KycNotRequired:1,
   ExchangeEmail:1,
   InfoNotice:1,
   SupportNotice:1,
   PaymentStatusWr:1,
   PaymentStatusLa:1,
   TransactionStatusWr:1,
   TransactionStatusLa:1,
   Step2BotSep:1,
   Step1Co:1,
   Step2Co:1,
   Step3Co:1,
   Step1La:1,
   Step2La:1,
   Step3La:1,
   TransactionIdWr:1,
   TransactionIdLa:1,
   Step1ProgressCircleWr:1,
   Step2ProgressCircleWr:1,
   Step1Wr:1,
   Step2Wr:1,
   Step3Wr:1,
   Step1Circle:1,
   Step2Circle:1,
   Step3Circle:1,
  }),
  initializer({
   KycBlock:_KycBlock,
   KycRequired:_KycRequired,
   KycNotRequired:_KycNotRequired,
   ExchangeEmail:_ExchangeEmail,
   InfoNotice:_InfoNotice,
   SupportNotice:_SupportNotice,
   PaymentStatusWr:_PaymentStatusWr,
   PaymentStatusLa:_PaymentStatusLa,
   TransactionStatusWr:_TransactionStatusWr,
   TransactionStatusLa:_TransactionStatusLa,
   Step2BotSep:_Step2BotSep,
   Step1Co:_Step1Co,
   Step2Co:_Step2Co,
   Step3Co:_Step3Co,
   Step1La:_Step1La,
   Step2La:_Step2La,
   Step3La:_Step3La,
   TransactionIdWr:_TransactionIdWr,
   TransactionIdLa:_TransactionIdLa,
   Step1ProgressCircleWr:_Step1ProgressCircleWr,
   Step2ProgressCircleWr:_Step2ProgressCircleWr,
   Step1Wr:_Step1Wr,
   Step2Wr:_Step2Wr,
   Step3Wr:_Step3Wr,
   Step1Circle:_Step1Circle,
   Step2Circle:_Step2Circle,
   Step3Circle:_Step3Circle,
  }) {
   if(_KycBlock!==undefined) this.KycBlock=_KycBlock
   if(_KycRequired!==undefined) this.KycRequired=_KycRequired
   if(_KycNotRequired!==undefined) this.KycNotRequired=_KycNotRequired
   if(_ExchangeEmail!==undefined) this.ExchangeEmail=_ExchangeEmail
   if(_InfoNotice!==undefined) this.InfoNotice=_InfoNotice
   if(_SupportNotice!==undefined) this.SupportNotice=_SupportNotice
   if(_PaymentStatusWr!==undefined) this.PaymentStatusWr=_PaymentStatusWr
   if(_PaymentStatusLa!==undefined) this.PaymentStatusLa=_PaymentStatusLa
   if(_TransactionStatusWr!==undefined) this.TransactionStatusWr=_TransactionStatusWr
   if(_TransactionStatusLa!==undefined) this.TransactionStatusLa=_TransactionStatusLa
   if(_Step2BotSep!==undefined) this.Step2BotSep=_Step2BotSep
   if(_Step1Co!==undefined) this.Step1Co=_Step1Co
   if(_Step2Co!==undefined) this.Step2Co=_Step2Co
   if(_Step3Co!==undefined) this.Step3Co=_Step3Co
   if(_Step1La!==undefined) this.Step1La=_Step1La
   if(_Step2La!==undefined) this.Step2La=_Step2La
   if(_Step3La!==undefined) this.Step3La=_Step3La
   if(_TransactionIdWr!==undefined) this.TransactionIdWr=_TransactionIdWr
   if(_TransactionIdLa!==undefined) this.TransactionIdLa=_TransactionIdLa
   if(_Step1ProgressCircleWr!==undefined) this.Step1ProgressCircleWr=_Step1ProgressCircleWr
   if(_Step2ProgressCircleWr!==undefined) this.Step2ProgressCircleWr=_Step2ProgressCircleWr
   if(_Step1Wr!==undefined) this.Step1Wr=_Step1Wr
   if(_Step2Wr!==undefined) this.Step2Wr=_Step2Wr
   if(_Step3Wr!==undefined) this.Step3Wr=_Step3Wr
   if(_Step1Circle!==undefined) this.Step1Circle=_Step1Circle
   if(_Step2Circle!==undefined) this.Step2Circle=_Step2Circle
   if(_Step3Circle!==undefined) this.Step3Circle=_Step3Circle
  },
 }),
]