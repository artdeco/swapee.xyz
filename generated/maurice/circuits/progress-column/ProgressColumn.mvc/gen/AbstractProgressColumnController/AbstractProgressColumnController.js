import ProgressColumnBuffer from '../ProgressColumnBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ProgressColumnPortConnector} from '../ProgressColumnPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnController}
 */
function __AbstractProgressColumnController() {}
__AbstractProgressColumnController.prototype = /** @type {!_AbstractProgressColumnController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnController}
 */
class _AbstractProgressColumnController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractProgressColumnController} ‎
 */
export class AbstractProgressColumnController extends newAbstract(
 _AbstractProgressColumnController,'IProgressColumnController',null,{
  asIProgressColumnController:1,
  superProgressColumnController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnController} */
AbstractProgressColumnController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnController} */
function AbstractProgressColumnControllerClass(){}


AbstractProgressColumnController[$implementations]=[
 AbstractProgressColumnControllerClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IProgressColumnPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractProgressColumnController,
 ProgressColumnBuffer,
 IntegratedController,
 /**@type {!AbstractProgressColumnController}*/(ProgressColumnPortConnector),
]


export default AbstractProgressColumnController