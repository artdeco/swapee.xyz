import {makeBuffers} from '@webcircuits/webcircuits'

export const ProgressColumnBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  creatingTransaction:Boolean,
  finishedOnPayment:Boolean,
  paymentFailed:Boolean,
  paymentCompleted:Boolean,
  transactionId:String,
  paymentStatus:String,
  loadingStep1:Boolean,
  loadingStep2:Boolean,
  loadingStep3:Boolean,
  status:String,
 }),
})

export default ProgressColumnBuffer