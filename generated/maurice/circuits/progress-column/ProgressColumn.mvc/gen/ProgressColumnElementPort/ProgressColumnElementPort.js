import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ProgressColumnElementPort}
 */
function __ProgressColumnElementPort() {}
__ProgressColumnElementPort.prototype = /** @type {!_ProgressColumnElementPort} */ ({ })
/** @this {xyz.swapee.wc.ProgressColumnElementPort} */ function ProgressColumnElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IProgressColumnElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    kycBlockOpts: {},
    kycRequiredOpts: {},
    kycNotRequiredOpts: {},
    exchangeEmailOpts: {},
    infoNoticeOpts: {},
    supportNoticeOpts: {},
    paymentStatusWrOpts: {},
    paymentStatusLaOpts: {},
    transactionStatusWrOpts: {},
    transactionStatusLaOpts: {},
    step2BotSepOpts: {},
    step1CoOpts: {},
    step2CoOpts: {},
    step3CoOpts: {},
    step1LaOpts: {},
    step2LaOpts: {},
    step3LaOpts: {},
    transactionIdWrOpts: {},
    transactionIdLaOpts: {},
    step1ProgressCircleWrOpts: {},
    step2ProgressCircleWrOpts: {},
    step1WrOpts: {},
    step2WrOpts: {},
    step3WrOpts: {},
    step1CircleOpts: {},
    step2CircleOpts: {},
    step3CircleOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnElementPort}
 */
class _ProgressColumnElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractProgressColumnElementPort} ‎
 */
class ProgressColumnElementPort extends newAbstract(
 _ProgressColumnElementPort,'IProgressColumnElementPort',ProgressColumnElementPortConstructor,{
  asIProgressColumnElementPort:1,
  superProgressColumnElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnElementPort} */
ProgressColumnElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnElementPort} */
function ProgressColumnElementPortClass(){}

export default ProgressColumnElementPort


ProgressColumnElementPort[$implementations]=[
 __ProgressColumnElementPort,
 ProgressColumnElementPortClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'kyc-block-opts':undefined,
    'kyc-required-opts':undefined,
    'kyc-not-required-opts':undefined,
    'exchange-email-opts':undefined,
    'info-notice-opts':undefined,
    'support-notice-opts':undefined,
    'payment-status-wr-opts':undefined,
    'payment-status-la-opts':undefined,
    'transaction-status-wr-opts':undefined,
    'transaction-status-la-opts':undefined,
    'step2-bot-sep-opts':undefined,
    'step1-co-opts':undefined,
    'step2-co-opts':undefined,
    'step3-co-opts':undefined,
    'step1-la-opts':undefined,
    'step2-la-opts':undefined,
    'step3-la-opts':undefined,
    'transaction-id-wr-opts':undefined,
    'transaction-id-la-opts':undefined,
    'step1-progress-circle-wr-opts':undefined,
    'step2-progress-circle-wr-opts':undefined,
    'step1-wr-opts':undefined,
    'step2-wr-opts':undefined,
    'step3-wr-opts':undefined,
    'step1-circle-opts':undefined,
    'step2-circle-opts':undefined,
    'step3-circle-opts':undefined,
    'creating-transaction':undefined,
    'finished-on-payment':undefined,
    'payment-failed':undefined,
    'payment-completed':undefined,
    'transaction-id':undefined,
    'payment-status':undefined,
    'loading-step1':undefined,
    'loading-step2':undefined,
    'loading-step3':undefined,
   })
  },
 }),
]