import AbstractProgressColumnScreenAT from '../AbstractProgressColumnScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnScreenBack}
 */
function __AbstractProgressColumnScreenBack() {}
__AbstractProgressColumnScreenBack.prototype = /** @type {!_AbstractProgressColumnScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnScreen}
 */
class _AbstractProgressColumnScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnScreen} ‎
 */
class AbstractProgressColumnScreenBack extends newAbstract(
 _AbstractProgressColumnScreenBack,'IProgressColumnScreen',null,{
  asIProgressColumnScreen:1,
  superProgressColumnScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnScreen} */
AbstractProgressColumnScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnScreen} */
function AbstractProgressColumnScreenBackClass(){}

export default AbstractProgressColumnScreenBack


AbstractProgressColumnScreenBack[$implementations]=[
 __AbstractProgressColumnScreenBack,
 AbstractProgressColumnScreenAT,
]