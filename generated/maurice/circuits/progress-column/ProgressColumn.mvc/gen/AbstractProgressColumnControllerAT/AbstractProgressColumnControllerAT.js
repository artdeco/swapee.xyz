import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnControllerAT}
 */
function __AbstractProgressColumnControllerAT() {}
__AbstractProgressColumnControllerAT.prototype = /** @type {!_AbstractProgressColumnControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractProgressColumnControllerAT}
 */
class _AbstractProgressColumnControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IProgressColumnControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractProgressColumnControllerAT} ‎
 */
class AbstractProgressColumnControllerAT extends newAbstract(
 _AbstractProgressColumnControllerAT,'IProgressColumnControllerAT',null,{
  asIProgressColumnControllerAT:1,
  superProgressColumnControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractProgressColumnControllerAT} */
AbstractProgressColumnControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractProgressColumnControllerAT} */
function AbstractProgressColumnControllerATClass(){}

export default AbstractProgressColumnControllerAT


AbstractProgressColumnControllerAT[$implementations]=[
 __AbstractProgressColumnControllerAT,
 UartUniversal,
 AbstractProgressColumnControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IProgressColumnControllerAT}*/({
  get asIProgressColumnController(){
   return this
  },
 }),
]