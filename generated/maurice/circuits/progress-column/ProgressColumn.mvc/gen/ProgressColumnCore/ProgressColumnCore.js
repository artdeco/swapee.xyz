import {mountPins} from '@webcircuits/webcircuits'
import {ProgressColumnMemoryPQs} from '../../pqs/ProgressColumnMemoryPQs'
import {ProgressColumnCachePQs} from '../../pqs/ProgressColumnCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ProgressColumnCore}
 */
function __ProgressColumnCore() {}
__ProgressColumnCore.prototype = /** @type {!_ProgressColumnCore} */ ({ })
/** @this {xyz.swapee.wc.ProgressColumnCore} */ function ProgressColumnCoreConstructor() {
  /**@type {!xyz.swapee.wc.IProgressColumnCore.Model}*/
  this.model={
    processingTransaction: false,
    transactionStatus: '',
    transactionCompleted: false,
    transactionFailed: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnCore}
 */
class _ProgressColumnCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractProgressColumnCore} ‎
 */
class ProgressColumnCore extends newAbstract(
 _ProgressColumnCore,'IProgressColumnCore',ProgressColumnCoreConstructor,{
  asIProgressColumnCore:1,
  superProgressColumnCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnCore} */
ProgressColumnCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnCore} */
function ProgressColumnCoreClass(){}

export default ProgressColumnCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ProgressColumnOuterCore}
 */
function __ProgressColumnOuterCore() {}
__ProgressColumnOuterCore.prototype = /** @type {!_ProgressColumnOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ProgressColumnOuterCore} */
export function ProgressColumnOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IProgressColumnOuterCore.Model}*/
  this.model={
    creatingTransaction: false,
    finishedOnPayment: false,
    paymentFailed: false,
    paymentCompleted: false,
    transactionId: '',
    paymentStatus: '',
    loadingStep1: false,
    loadingStep2: false,
    loadingStep3: false,
    status: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnOuterCore}
 */
class _ProgressColumnOuterCore { }
/**
 * The _IProgressColumn_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractProgressColumnOuterCore} ‎
 */
export class ProgressColumnOuterCore extends newAbstract(
 _ProgressColumnOuterCore,'IProgressColumnOuterCore',ProgressColumnOuterCoreConstructor,{
  asIProgressColumnOuterCore:1,
  superProgressColumnOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnOuterCore} */
ProgressColumnOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnOuterCore} */
function ProgressColumnOuterCoreClass(){}


ProgressColumnOuterCore[$implementations]=[
 __ProgressColumnOuterCore,
 ProgressColumnOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnOuterCore}*/({
  constructor(){
   mountPins(this.model,'',ProgressColumnMemoryPQs)
   mountPins(this.model,'',ProgressColumnCachePQs)
  },
 }),
]

ProgressColumnCore[$implementations]=[
 ProgressColumnCoreClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnCore}*/({
  resetCore(){
   this.resetProgressColumnCore()
  },
  resetProgressColumnCore(){
   ProgressColumnCoreConstructor.call(
    /**@type {xyz.swapee.wc.ProgressColumnCore}*/(this),
   )
   ProgressColumnOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ProgressColumnOuterCore}*/(
     /**@type {!xyz.swapee.wc.IProgressColumnOuterCore}*/(this)),
   )
  },
 }),
 __ProgressColumnCore,
 ProgressColumnOuterCore,
]

export {ProgressColumnCore}