import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnDisplay}
 */
function __AbstractProgressColumnDisplay() {}
__AbstractProgressColumnDisplay.prototype = /** @type {!_AbstractProgressColumnDisplay} */ ({ })
/** @this {xyz.swapee.wc.ProgressColumnDisplay} */ function ProgressColumnDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.KycBlock=null
  /** @type {HTMLDivElement} */ this.KycRequired=null
  /** @type {HTMLDivElement} */ this.KycNotRequired=null
  /** @type {HTMLDivElement} */ this.ExchangeEmail=null
  /** @type {HTMLDivElement} */ this.InfoNotice=null
  /** @type {HTMLDivElement} */ this.SupportNotice=null
  /** @type {HTMLDivElement} */ this.PaymentStatusWr=null
  /** @type {HTMLSpanElement} */ this.PaymentStatusLa=null
  /** @type {HTMLDivElement} */ this.TransactionStatusWr=null
  /** @type {HTMLSpanElement} */ this.TransactionStatusLa=null
  /** @type {HTMLSpanElement} */ this.Step2BotSep=null
  /** @type {HTMLDivElement} */ this.Step1Co=null
  /** @type {HTMLDivElement} */ this.Step2Co=null
  /** @type {HTMLDivElement} */ this.Step3Co=null
  /** @type {HTMLSpanElement} */ this.Step1La=null
  /** @type {HTMLSpanElement} */ this.Step2La=null
  /** @type {HTMLSpanElement} */ this.Step3La=null
  /** @type {HTMLSpanElement} */ this.TransactionIdWr=null
  /** @type {HTMLSpanElement} */ this.TransactionIdLa=null
  /** @type {HTMLSpanElement} */ this.Step1ProgressCircleWr=null
  /** @type {HTMLSpanElement} */ this.Step2ProgressCircleWr=null
  /** @type {HTMLDivElement} */ this.Step1Wr=null
  /** @type {HTMLDivElement} */ this.Step2Wr=null
  /** @type {HTMLDivElement} */ this.Step3Wr=null
  /** @type {HTMLSpanElement} */ this.Step1Circle=null
  /** @type {HTMLSpanElement} */ this.Step2Circle=null
  /** @type {HTMLSpanElement} */ this.Step3Circle=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnDisplay}
 */
class _AbstractProgressColumnDisplay { }
/**
 * Display for presenting information from the _IProgressColumn_.
 * @extends {xyz.swapee.wc.AbstractProgressColumnDisplay} ‎
 */
class AbstractProgressColumnDisplay extends newAbstract(
 _AbstractProgressColumnDisplay,'IProgressColumnDisplay',ProgressColumnDisplayConstructor,{
  asIProgressColumnDisplay:1,
  superProgressColumnDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnDisplay} */
AbstractProgressColumnDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnDisplay} */
function AbstractProgressColumnDisplayClass(){}

export default AbstractProgressColumnDisplay


AbstractProgressColumnDisplay[$implementations]=[
 __AbstractProgressColumnDisplay,
 Display,
 AbstractProgressColumnDisplayClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIProgressColumnScreen:{vdusPQs:{
    TransactionIdWr:TransactionIdWr,
    Step1Circle:Step1Circle,
    Step2Circle:Step2Circle,
    Step3Circle:Step3Circle,
    Step2La:Step2La,
    Step3La:Step3La,
    TransactionIdLa:TransactionIdLa,
    PaymentStatusWr:PaymentStatusWr,
    PaymentStatusLa:PaymentStatusLa,
    TransactionStatusWr:TransactionStatusWr,
    TransactionStatusLa:TransactionStatusLa,
    Step3Wr:Step3Wr,
    Step2BotSep:Step2BotSep,
    KycBlock:KycBlock,
    KycRequired:KycRequired,
    KycNotRequired:KycNotRequired,
    ExchangeEmail:ExchangeEmail,
    InfoNotice:InfoNotice,
    SupportNotice:SupportNotice,
    Step1Co:Step1Co,
    Step2Co:Step2Co,
    Step3Co:Step3Co,
    Step1La:Step1La,
    Step1ProgressCircleWr:Step1ProgressCircleWr,
    Step2ProgressCircleWr:Step2ProgressCircleWr,
    Step1Wr:Step1Wr,
    Step2Wr:Step2Wr,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    TransactionIdWr:/**@type {HTMLSpanElement}*/(children[TransactionIdWr]),
    Step1Circle:/**@type {HTMLSpanElement}*/(children[Step1Circle]),
    Step2Circle:/**@type {HTMLSpanElement}*/(children[Step2Circle]),
    Step3Circle:/**@type {HTMLSpanElement}*/(children[Step3Circle]),
    Step2La:/**@type {HTMLSpanElement}*/(children[Step2La]),
    Step3La:/**@type {HTMLSpanElement}*/(children[Step3La]),
    TransactionIdLa:/**@type {HTMLSpanElement}*/(children[TransactionIdLa]),
    PaymentStatusWr:/**@type {HTMLDivElement}*/(children[PaymentStatusWr]),
    PaymentStatusLa:/**@type {HTMLSpanElement}*/(children[PaymentStatusLa]),
    TransactionStatusWr:/**@type {HTMLDivElement}*/(children[TransactionStatusWr]),
    TransactionStatusLa:/**@type {HTMLSpanElement}*/(children[TransactionStatusLa]),
    Step3Wr:/**@type {HTMLDivElement}*/(children[Step3Wr]),
    Step2BotSep:/**@type {HTMLSpanElement}*/(children[Step2BotSep]),
    KycBlock:/**@type {HTMLDivElement}*/(children[KycBlock]),
    KycRequired:/**@type {HTMLDivElement}*/(children[KycRequired]),
    KycNotRequired:/**@type {HTMLDivElement}*/(children[KycNotRequired]),
    ExchangeEmail:/**@type {HTMLDivElement}*/(children[ExchangeEmail]),
    InfoNotice:/**@type {HTMLDivElement}*/(children[InfoNotice]),
    SupportNotice:/**@type {HTMLDivElement}*/(children[SupportNotice]),
    Step1Co:/**@type {HTMLDivElement}*/(children[Step1Co]),
    Step2Co:/**@type {HTMLDivElement}*/(children[Step2Co]),
    Step3Co:/**@type {HTMLDivElement}*/(children[Step3Co]),
    Step1La:/**@type {HTMLSpanElement}*/(children[Step1La]),
    Step1ProgressCircleWr:/**@type {HTMLSpanElement}*/(children[Step1ProgressCircleWr]),
    Step2ProgressCircleWr:/**@type {HTMLSpanElement}*/(children[Step2ProgressCircleWr]),
    Step1Wr:/**@type {HTMLDivElement}*/(children[Step1Wr]),
    Step2Wr:/**@type {HTMLDivElement}*/(children[Step2Wr]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.ProgressColumnDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IProgressColumnDisplay.Initialese}*/({
   KycBlock:1,
   KycRequired:1,
   KycNotRequired:1,
   ExchangeEmail:1,
   InfoNotice:1,
   SupportNotice:1,
   PaymentStatusWr:1,
   PaymentStatusLa:1,
   TransactionStatusWr:1,
   TransactionStatusLa:1,
   Step2BotSep:1,
   Step1Co:1,
   Step2Co:1,
   Step3Co:1,
   Step1La:1,
   Step2La:1,
   Step3La:1,
   TransactionIdWr:1,
   TransactionIdLa:1,
   Step1ProgressCircleWr:1,
   Step2ProgressCircleWr:1,
   Step1Wr:1,
   Step2Wr:1,
   Step3Wr:1,
   Step1Circle:1,
   Step2Circle:1,
   Step3Circle:1,
  }),
  initializer({
   KycBlock:_KycBlock,
   KycRequired:_KycRequired,
   KycNotRequired:_KycNotRequired,
   ExchangeEmail:_ExchangeEmail,
   InfoNotice:_InfoNotice,
   SupportNotice:_SupportNotice,
   PaymentStatusWr:_PaymentStatusWr,
   PaymentStatusLa:_PaymentStatusLa,
   TransactionStatusWr:_TransactionStatusWr,
   TransactionStatusLa:_TransactionStatusLa,
   Step2BotSep:_Step2BotSep,
   Step1Co:_Step1Co,
   Step2Co:_Step2Co,
   Step3Co:_Step3Co,
   Step1La:_Step1La,
   Step2La:_Step2La,
   Step3La:_Step3La,
   TransactionIdWr:_TransactionIdWr,
   TransactionIdLa:_TransactionIdLa,
   Step1ProgressCircleWr:_Step1ProgressCircleWr,
   Step2ProgressCircleWr:_Step2ProgressCircleWr,
   Step1Wr:_Step1Wr,
   Step2Wr:_Step2Wr,
   Step3Wr:_Step3Wr,
   Step1Circle:_Step1Circle,
   Step2Circle:_Step2Circle,
   Step3Circle:_Step3Circle,
  }) {
   if(_KycBlock!==undefined) this.KycBlock=_KycBlock
   if(_KycRequired!==undefined) this.KycRequired=_KycRequired
   if(_KycNotRequired!==undefined) this.KycNotRequired=_KycNotRequired
   if(_ExchangeEmail!==undefined) this.ExchangeEmail=_ExchangeEmail
   if(_InfoNotice!==undefined) this.InfoNotice=_InfoNotice
   if(_SupportNotice!==undefined) this.SupportNotice=_SupportNotice
   if(_PaymentStatusWr!==undefined) this.PaymentStatusWr=_PaymentStatusWr
   if(_PaymentStatusLa!==undefined) this.PaymentStatusLa=_PaymentStatusLa
   if(_TransactionStatusWr!==undefined) this.TransactionStatusWr=_TransactionStatusWr
   if(_TransactionStatusLa!==undefined) this.TransactionStatusLa=_TransactionStatusLa
   if(_Step2BotSep!==undefined) this.Step2BotSep=_Step2BotSep
   if(_Step1Co!==undefined) this.Step1Co=_Step1Co
   if(_Step2Co!==undefined) this.Step2Co=_Step2Co
   if(_Step3Co!==undefined) this.Step3Co=_Step3Co
   if(_Step1La!==undefined) this.Step1La=_Step1La
   if(_Step2La!==undefined) this.Step2La=_Step2La
   if(_Step3La!==undefined) this.Step3La=_Step3La
   if(_TransactionIdWr!==undefined) this.TransactionIdWr=_TransactionIdWr
   if(_TransactionIdLa!==undefined) this.TransactionIdLa=_TransactionIdLa
   if(_Step1ProgressCircleWr!==undefined) this.Step1ProgressCircleWr=_Step1ProgressCircleWr
   if(_Step2ProgressCircleWr!==undefined) this.Step2ProgressCircleWr=_Step2ProgressCircleWr
   if(_Step1Wr!==undefined) this.Step1Wr=_Step1Wr
   if(_Step2Wr!==undefined) this.Step2Wr=_Step2Wr
   if(_Step3Wr!==undefined) this.Step3Wr=_Step3Wr
   if(_Step1Circle!==undefined) this.Step1Circle=_Step1Circle
   if(_Step2Circle!==undefined) this.Step2Circle=_Step2Circle
   if(_Step3Circle!==undefined) this.Step3Circle=_Step3Circle
  },
 }),
]