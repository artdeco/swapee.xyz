import AbstractProgressColumnControllerAR from '../AbstractProgressColumnControllerAR'
import {AbstractProgressColumnController} from '../AbstractProgressColumnController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnControllerBack}
 */
function __AbstractProgressColumnControllerBack() {}
__AbstractProgressColumnControllerBack.prototype = /** @type {!_AbstractProgressColumnControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnController}
 */
class _AbstractProgressColumnControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnController} ‎
 */
class AbstractProgressColumnControllerBack extends newAbstract(
 _AbstractProgressColumnControllerBack,'IProgressColumnController',null,{
  asIProgressColumnController:1,
  superProgressColumnController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnController} */
AbstractProgressColumnControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnController} */
function AbstractProgressColumnControllerBackClass(){}

export default AbstractProgressColumnControllerBack


AbstractProgressColumnControllerBack[$implementations]=[
 __AbstractProgressColumnControllerBack,
 AbstractProgressColumnController,
 AbstractProgressColumnControllerAR,
 DriverBack,
]