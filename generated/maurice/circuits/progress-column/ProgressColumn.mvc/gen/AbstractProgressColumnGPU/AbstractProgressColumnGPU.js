import AbstractProgressColumnDisplay from '../AbstractProgressColumnDisplayBack'
import ProgressColumnClassesPQs from '../../pqs/ProgressColumnClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {ProgressColumnClassesQPs} from '../../pqs/ProgressColumnClassesQPs'
import {ProgressColumnVdusPQs} from '../../pqs/ProgressColumnVdusPQs'
import {ProgressColumnVdusQPs} from '../../pqs/ProgressColumnVdusQPs'
import {ProgressColumnMemoryPQs} from '../../pqs/ProgressColumnMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnGPU}
 */
function __AbstractProgressColumnGPU() {}
__AbstractProgressColumnGPU.prototype = /** @type {!_AbstractProgressColumnGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnGPU}
 */
class _AbstractProgressColumnGPU { }
/**
 * Handles the periphery of the _IProgressColumnDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractProgressColumnGPU} ‎
 */
class AbstractProgressColumnGPU extends newAbstract(
 _AbstractProgressColumnGPU,'IProgressColumnGPU',null,{
  asIProgressColumnGPU:1,
  superProgressColumnGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnGPU} */
AbstractProgressColumnGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnGPU} */
function AbstractProgressColumnGPUClass(){}

export default AbstractProgressColumnGPU


AbstractProgressColumnGPU[$implementations]=[
 __AbstractProgressColumnGPU,
 AbstractProgressColumnGPUClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnGPU}*/({
  classesQPs:ProgressColumnClassesQPs,
  vdusPQs:ProgressColumnVdusPQs,
  vdusQPs:ProgressColumnVdusQPs,
  memoryPQs:ProgressColumnMemoryPQs,
 }),
 AbstractProgressColumnDisplay,
 BrowserView,
 AbstractProgressColumnGPUClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnGPU}*/({
  allocator(){
   pressFit(this.classes,'',ProgressColumnClassesPQs)
  },
 }),
]