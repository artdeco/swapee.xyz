import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnScreenAR}
 */
function __AbstractProgressColumnScreenAR() {}
__AbstractProgressColumnScreenAR.prototype = /** @type {!_AbstractProgressColumnScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractProgressColumnScreenAR}
 */
class _AbstractProgressColumnScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IProgressColumnScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractProgressColumnScreenAR} ‎
 */
class AbstractProgressColumnScreenAR extends newAbstract(
 _AbstractProgressColumnScreenAR,'IProgressColumnScreenAR',null,{
  asIProgressColumnScreenAR:1,
  superProgressColumnScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractProgressColumnScreenAR} */
AbstractProgressColumnScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractProgressColumnScreenAR} */
function AbstractProgressColumnScreenARClass(){}

export default AbstractProgressColumnScreenAR


AbstractProgressColumnScreenAR[$implementations]=[
 __AbstractProgressColumnScreenAR,
 AR,
 AbstractProgressColumnScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IProgressColumnScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractProgressColumnScreenAR}