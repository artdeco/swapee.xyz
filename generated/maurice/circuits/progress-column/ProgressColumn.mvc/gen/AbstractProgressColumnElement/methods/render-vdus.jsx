export default function ProgressColumnRenderVdus(){
 return (<div $id="ProgressColumn">
  <vdu $id="TransactionIdWr" />
  <vdu $id="Step1Circle" />
  <vdu $id="Step2Circle" />
  <vdu $id="Step3Circle" />
  <vdu $id="Step2La" />
  <vdu $id="Step3La" />
  <vdu $id="TransactionIdLa" />
  <vdu $id="PaymentStatusWr" />
  <vdu $id="PaymentStatusLa" />
  <vdu $id="TransactionStatusWr" />
  <vdu $id="TransactionStatusLa" />
  <vdu $id="Step3Wr" />
  <vdu $id="Step2BotSep" />
  <vdu $id="KycBlock" />
  <vdu $id="KycRequired" />
  <vdu $id="KycNotRequired" />
  <vdu $id="ExchangeEmail" />
  <vdu $id="InfoNotice" />
  <vdu $id="SupportNotice" />
  <vdu $id="Step1Co" />
  <vdu $id="Step2Co" />
  <vdu $id="Step3Co" />
  <vdu $id="Step1La" />
  <vdu $id="Step1ProgressCircleWr" />
  <vdu $id="Step2ProgressCircleWr" />
  <vdu $id="Step1Wr" />
  <vdu $id="Step2Wr" />
 </div>)
}