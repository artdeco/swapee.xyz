
import AbstractProgressColumn from '../AbstractProgressColumn'

/** @abstract {xyz.swapee.wc.IProgressColumnElement} */
export default class AbstractProgressColumnElement { }



AbstractProgressColumnElement[$implementations]=[AbstractProgressColumn,
 /** @type {!AbstractProgressColumnElement} */ ({
  rootId:'ProgressColumn',
  __$id:6526983971,
  fqn:'xyz.swapee.wc.IProgressColumn',
  maurice_element_v3:true,
 }),
]