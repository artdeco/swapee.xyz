import ProgressColumnRenderVdus from './methods/render-vdus'
import ProgressColumnElementPort from '../ProgressColumnElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {ProgressColumnInputsPQs} from '../../pqs/ProgressColumnInputsPQs'
import {ProgressColumnCachePQs} from '../../pqs/ProgressColumnCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractProgressColumn from '../AbstractProgressColumn'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnElement}
 */
function __AbstractProgressColumnElement() {}
__AbstractProgressColumnElement.prototype = /** @type {!_AbstractProgressColumnElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnElement}
 */
class _AbstractProgressColumnElement { }
/**
 * A component description.
 *
 * The _IProgressColumn_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractProgressColumnElement} ‎
 */
class AbstractProgressColumnElement extends newAbstract(
 _AbstractProgressColumnElement,'IProgressColumnElement',null,{
  asIProgressColumnElement:1,
  superProgressColumnElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnElement} */
AbstractProgressColumnElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnElement} */
function AbstractProgressColumnElementClass(){}

export default AbstractProgressColumnElement


AbstractProgressColumnElement[$implementations]=[
 __AbstractProgressColumnElement,
 ElementBase,
 AbstractProgressColumnElementClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':creating-transaction':creatingTransactionColAttr,
   ':finished-on-payment':finishedOnPaymentColAttr,
   ':payment-failed':paymentFailedColAttr,
   ':payment-completed':paymentCompletedColAttr,
   ':transaction-id':transactionIdColAttr,
   ':payment-status':paymentStatusColAttr,
   ':loading-step1':loadingStep1ColAttr,
   ':loading-step2':loadingStep2ColAttr,
   ':loading-step3':loadingStep3ColAttr,
   ':status':statusColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'creating-transaction':creatingTransactionAttr,
    'finished-on-payment':finishedOnPaymentAttr,
    'payment-failed':paymentFailedAttr,
    'payment-completed':paymentCompletedAttr,
    'transaction-id':transactionIdAttr,
    'payment-status':paymentStatusAttr,
    'loading-step1':loadingStep1Attr,
    'loading-step2':loadingStep2Attr,
    'loading-step3':loadingStep3Attr,
    'status':statusAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(creatingTransactionAttr===undefined?{'creating-transaction':creatingTransactionColAttr}:{}),
    ...(finishedOnPaymentAttr===undefined?{'finished-on-payment':finishedOnPaymentColAttr}:{}),
    ...(paymentFailedAttr===undefined?{'payment-failed':paymentFailedColAttr}:{}),
    ...(paymentCompletedAttr===undefined?{'payment-completed':paymentCompletedColAttr}:{}),
    ...(transactionIdAttr===undefined?{'transaction-id':transactionIdColAttr}:{}),
    ...(paymentStatusAttr===undefined?{'payment-status':paymentStatusColAttr}:{}),
    ...(loadingStep1Attr===undefined?{'loading-step1':loadingStep1ColAttr}:{}),
    ...(loadingStep2Attr===undefined?{'loading-step2':loadingStep2ColAttr}:{}),
    ...(loadingStep3Attr===undefined?{'loading-step3':loadingStep3ColAttr}:{}),
    ...(statusAttr===undefined?{'status':statusColAttr}:{}),
   }
  },
 }),
 AbstractProgressColumnElementClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'creating-transaction':creatingTransactionAttr,
   'finished-on-payment':finishedOnPaymentAttr,
   'payment-failed':paymentFailedAttr,
   'payment-completed':paymentCompletedAttr,
   'transaction-id':transactionIdAttr,
   'payment-status':paymentStatusAttr,
   'loading-step1':loadingStep1Attr,
   'loading-step2':loadingStep2Attr,
   'loading-step3':loadingStep3Attr,
   'status':statusAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    creatingTransaction:creatingTransactionAttr,
    finishedOnPayment:finishedOnPaymentAttr,
    paymentFailed:paymentFailedAttr,
    paymentCompleted:paymentCompletedAttr,
    transactionId:transactionIdAttr,
    paymentStatus:paymentStatusAttr,
    loadingStep1:loadingStep1Attr,
    loadingStep2:loadingStep2Attr,
    loadingStep3:loadingStep3Attr,
    status:statusAttr,
   }
  },
 }),
 AbstractProgressColumnElementClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractProgressColumnElementClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnElement}*/({
  render:ProgressColumnRenderVdus,
 }),
 AbstractProgressColumnElementClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnElement}*/({
  classes:{
   'CircleLoading': '5e522',
   'CircleComplete': 'a5ad9',
   'CircleWaiting': '9912b',
   'CircleFailed': 'dc9d7',
   'StepLabelFailed': '7e898',
   'StepLabelWaiting': 'e4d45',
  },
  inputsPQs:ProgressColumnInputsPQs,
  cachePQs:ProgressColumnCachePQs,
  vdus:{
   'TransactionIdWr': 'fa9e1',
   'Step1Circle': 'fa9e2',
   'Step2Circle': 'fa9e3',
   'Step3Circle': 'fa9e4',
   'Step2La': 'fa9e5',
   'TransactionIdLa': 'fa9e6',
   'Step3Wr': 'fa9e9',
   'Step2BotSep': 'fa9e10',
   'Step1La': 'fa9e12',
   'Step1ProgressCircleWr': 'fa9e13',
   'Step3La': 'fa9e14',
   'PaymentStatusWr': 'fa9e15',
   'PaymentStatusLa': 'fa9e16',
   'TransactionStatusWr': 'fa9e17',
   'TransactionStatusLa': 'fa9e18',
   'Step1Wr': 'fa9e19',
   'Step2Wr': 'fa9e20',
   'InfoNotice': 'fa9e21',
   'SupportNotice': 'fa9e22',
   'Step1Co': 'fa9e23',
   'Step2Co': 'fa9e24',
   'Step3Co': 'fa9e25',
   'Step2ProgressCircleWr': 'fa9e26',
   'KycRequired': 'fa9e27',
   'KycNotRequired': 'fa9e28',
   'ExchangeEmail': 'fa9e29',
   'KycBlock': 'fa9e30',
  },
 }),
 IntegratedComponent,
 AbstractProgressColumnElementClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','creatingTransaction','finishedOnPayment','paymentFailed','paymentCompleted','transactionId','paymentStatus','loadingStep1','loadingStep2','loadingStep3','status','no-solder',':no-solder','creating-transaction',':creating-transaction','finished-on-payment',':finished-on-payment','payment-failed',':payment-failed','payment-completed',':payment-completed','transaction-id',':transaction-id','payment-status',':payment-status','loading-step1',':loading-step1','loading-step2',':loading-step2','loading-step3',':loading-step3',':status','fe646','ef1d4','6c5cd','35399','c8e3d','2598d','6d28e','87e7e','c8d31','c6c14','f378b','4fba9','87196','c3763','08522','fbf32','3cc1f','41e54','77c32','8a859','07d58','10349','e5771','f81a7','70de9','7bc63','41102','f4b68','89fc6','a7265','02236','497c0','67113','b5666','da35f','277b1','d7265','9acb4','children']),
   })
  },
  get Port(){
   return ProgressColumnElementPort
  },
 }),
]



AbstractProgressColumnElement[$implementations]=[AbstractProgressColumn,
 /** @type {!AbstractProgressColumnElement} */ ({
  rootId:'ProgressColumn',
  __$id:6526983971,
  fqn:'xyz.swapee.wc.IProgressColumn',
  maurice_element_v3:true,
 }),
]