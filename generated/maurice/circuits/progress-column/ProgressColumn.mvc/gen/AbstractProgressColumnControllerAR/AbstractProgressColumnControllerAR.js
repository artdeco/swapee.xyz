import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnControllerAR}
 */
function __AbstractProgressColumnControllerAR() {}
__AbstractProgressColumnControllerAR.prototype = /** @type {!_AbstractProgressColumnControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnControllerAR}
 */
class _AbstractProgressColumnControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IProgressColumnControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractProgressColumnControllerAR} ‎
 */
class AbstractProgressColumnControllerAR extends newAbstract(
 _AbstractProgressColumnControllerAR,'IProgressColumnControllerAR',null,{
  asIProgressColumnControllerAR:1,
  superProgressColumnControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnControllerAR} */
AbstractProgressColumnControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractProgressColumnControllerAR} */
function AbstractProgressColumnControllerARClass(){}

export default AbstractProgressColumnControllerAR


AbstractProgressColumnControllerAR[$implementations]=[
 __AbstractProgressColumnControllerAR,
 AR,
 AbstractProgressColumnControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IProgressColumnControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]