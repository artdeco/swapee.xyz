import AbstractProgressColumnProcessor from '../AbstractProgressColumnProcessor'
import {ProgressColumnCore} from '../ProgressColumnCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractProgressColumnComputer} from '../AbstractProgressColumnComputer'
import {AbstractProgressColumnController} from '../AbstractProgressColumnController'
import {regulateProgressColumnCache} from './methods/regulateProgressColumnCache'
import {ProgressColumnCacheQPs} from '../../pqs/ProgressColumnCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumn}
 */
function __AbstractProgressColumn() {}
__AbstractProgressColumn.prototype = /** @type {!_AbstractProgressColumn} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumn}
 */
class _AbstractProgressColumn { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractProgressColumn} ‎
 */
class AbstractProgressColumn extends newAbstract(
 _AbstractProgressColumn,'IProgressColumn',null,{
  asIProgressColumn:1,
  superProgressColumn:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumn} */
AbstractProgressColumn.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumn} */
function AbstractProgressColumnClass(){}

export default AbstractProgressColumn


AbstractProgressColumn[$implementations]=[
 __AbstractProgressColumn,
 ProgressColumnCore,
 AbstractProgressColumnProcessor,
 IntegratedComponent,
 AbstractProgressColumnComputer,
 AbstractProgressColumnController,
 AbstractProgressColumnClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumn}*/({
  regulateState:regulateProgressColumnCache,
  stateQPs:ProgressColumnCacheQPs,
 }),
]


export {AbstractProgressColumn}