import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateProgressColumnCache=makeBuffers({
 processingTransaction:Boolean,
 transactionStatus:String,
 transactionCompleted:Boolean,
 transactionFailed:Boolean,
},{silent:true})