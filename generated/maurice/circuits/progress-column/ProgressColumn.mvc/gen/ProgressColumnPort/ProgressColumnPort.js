import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {ProgressColumnInputsPQs} from '../../pqs/ProgressColumnInputsPQs'
import {ProgressColumnOuterCoreConstructor} from '../ProgressColumnCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ProgressColumnPort}
 */
function __ProgressColumnPort() {}
__ProgressColumnPort.prototype = /** @type {!_ProgressColumnPort} */ ({ })
/** @this {xyz.swapee.wc.ProgressColumnPort} */ function ProgressColumnPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ProgressColumnOuterCore} */ ({model:null})
  ProgressColumnOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnPort}
 */
class _ProgressColumnPort { }
/**
 * The port that serves as an interface to the _IProgressColumn_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractProgressColumnPort} ‎
 */
export class ProgressColumnPort extends newAbstract(
 _ProgressColumnPort,'IProgressColumnPort',ProgressColumnPortConstructor,{
  asIProgressColumnPort:1,
  superProgressColumnPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnPort} */
ProgressColumnPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnPort} */
function ProgressColumnPortClass(){}

export const ProgressColumnPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IProgressColumn.Pinout>}*/({
 get Port() { return ProgressColumnPort },
})

ProgressColumnPort[$implementations]=[
 ProgressColumnPortClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnPort}*/({
  resetPort(){
   this.resetProgressColumnPort()
  },
  resetProgressColumnPort(){
   ProgressColumnPortConstructor.call(this)
  },
 }),
 __ProgressColumnPort,
 Parametric,
 ProgressColumnPortClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnPort}*/({
  constructor(){
   mountPins(this.inputs,'',ProgressColumnInputsPQs)
  },
 }),
]


export default ProgressColumnPort