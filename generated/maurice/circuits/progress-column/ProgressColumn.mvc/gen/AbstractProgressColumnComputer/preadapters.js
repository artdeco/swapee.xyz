
/**@this {xyz.swapee.wc.IProgressColumnComputer}*/
export function preadaptTransactionStatus(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IProgressColumnComputer.adaptTransactionStatus.Form}*/
 const _inputs={
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptTransactionStatus(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IProgressColumnComputer}*/
export function preadaptProcessingTransaction(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IProgressColumnComputer.adaptProcessingTransaction.Form}*/
 const _inputs={
  transactionStatus:inputs.transactionStatus,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptProcessingTransaction(__inputs,__changes)
 return RET
}