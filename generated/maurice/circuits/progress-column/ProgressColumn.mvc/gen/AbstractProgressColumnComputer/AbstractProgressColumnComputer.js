import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnComputer}
 */
function __AbstractProgressColumnComputer() {}
__AbstractProgressColumnComputer.prototype = /** @type {!_AbstractProgressColumnComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnComputer}
 */
class _AbstractProgressColumnComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractProgressColumnComputer} ‎
 */
export class AbstractProgressColumnComputer extends newAbstract(
 _AbstractProgressColumnComputer,'IProgressColumnComputer',null,{
  asIProgressColumnComputer:1,
  superProgressColumnComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnComputer} */
AbstractProgressColumnComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnComputer} */
function AbstractProgressColumnComputerClass(){}


AbstractProgressColumnComputer[$implementations]=[
 __AbstractProgressColumnComputer,
 Adapter,
]


export default AbstractProgressColumnComputer