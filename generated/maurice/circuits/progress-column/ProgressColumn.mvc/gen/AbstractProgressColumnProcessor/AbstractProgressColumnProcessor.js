import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnProcessor}
 */
function __AbstractProgressColumnProcessor() {}
__AbstractProgressColumnProcessor.prototype = /** @type {!_AbstractProgressColumnProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnProcessor}
 */
class _AbstractProgressColumnProcessor { }
/**
 * The processor to compute changes to the memory for the _IProgressColumn_.
 * @extends {xyz.swapee.wc.AbstractProgressColumnProcessor} ‎
 */
class AbstractProgressColumnProcessor extends newAbstract(
 _AbstractProgressColumnProcessor,'IProgressColumnProcessor',null,{
  asIProgressColumnProcessor:1,
  superProgressColumnProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnProcessor} */
AbstractProgressColumnProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnProcessor} */
function AbstractProgressColumnProcessorClass(){}

export default AbstractProgressColumnProcessor


AbstractProgressColumnProcessor[$implementations]=[
 __AbstractProgressColumnProcessor,
]