import ProgressColumnClassesPQs from '../../pqs/ProgressColumnClassesPQs'
import AbstractProgressColumnScreenAR from '../AbstractProgressColumnScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {ProgressColumnInputsPQs} from '../../pqs/ProgressColumnInputsPQs'
import {ProgressColumnMemoryQPs} from '../../pqs/ProgressColumnMemoryQPs'
import {ProgressColumnCacheQPs} from '../../pqs/ProgressColumnCacheQPs'
import {ProgressColumnVdusPQs} from '../../pqs/ProgressColumnVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractProgressColumnScreen}
 */
function __AbstractProgressColumnScreen() {}
__AbstractProgressColumnScreen.prototype = /** @type {!_AbstractProgressColumnScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractProgressColumnScreen}
 */
class _AbstractProgressColumnScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractProgressColumnScreen} ‎
 */
class AbstractProgressColumnScreen extends newAbstract(
 _AbstractProgressColumnScreen,'IProgressColumnScreen',null,{
  asIProgressColumnScreen:1,
  superProgressColumnScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractProgressColumnScreen} */
AbstractProgressColumnScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnScreen} */
function AbstractProgressColumnScreenClass(){}

export default AbstractProgressColumnScreen


AbstractProgressColumnScreen[$implementations]=[
 __AbstractProgressColumnScreen,
 AbstractProgressColumnScreenClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnScreen}*/({
  deduceInputs(){
   const{asIProgressColumnDisplay:{
    TransactionIdLa:TransactionIdLa,
    PaymentStatusLa:PaymentStatusLa,
    TransactionStatusLa:TransactionStatusLa,
   }}=this
   return{
    transactionId:TransactionIdLa?.innerText,
    paymentStatus:PaymentStatusLa?.innerText,
    transactionStatus:TransactionStatusLa?.innerText,
   }
  },
 }),
 AbstractProgressColumnScreenClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnScreen}*/({
  inputsPQs:ProgressColumnInputsPQs,
  classesPQs:ProgressColumnClassesPQs,
  memoryQPs:ProgressColumnMemoryQPs,
  cacheQPs:ProgressColumnCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractProgressColumnScreenAR,
 AbstractProgressColumnScreenClass.prototype=/**@type {!xyz.swapee.wc.IProgressColumnScreen}*/({
  vdusPQs:ProgressColumnVdusPQs,
 }),
]