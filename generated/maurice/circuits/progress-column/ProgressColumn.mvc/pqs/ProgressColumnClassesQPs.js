import ProgressColumnClassesPQs from './ProgressColumnClassesPQs'
export const ProgressColumnClassesQPs=/**@type {!xyz.swapee.wc.ProgressColumnClassesQPs}*/(Object.keys(ProgressColumnClassesPQs)
 .reduce((a,k)=>{a[ProgressColumnClassesPQs[k]]=k;return a},{}))