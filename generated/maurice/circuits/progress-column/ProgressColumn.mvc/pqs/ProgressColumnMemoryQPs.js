import {ProgressColumnMemoryPQs} from './ProgressColumnMemoryPQs'
export const ProgressColumnMemoryQPs=/**@type {!xyz.swapee.wc.ProgressColumnMemoryQPs}*/(Object.keys(ProgressColumnMemoryPQs)
 .reduce((a,k)=>{a[ProgressColumnMemoryPQs[k]]=k;return a},{}))