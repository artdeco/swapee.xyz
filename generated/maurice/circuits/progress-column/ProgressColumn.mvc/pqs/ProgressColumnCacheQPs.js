import {ProgressColumnCachePQs} from './ProgressColumnCachePQs'
export const ProgressColumnCacheQPs=/**@type {!xyz.swapee.wc.ProgressColumnCacheQPs}*/(Object.keys(ProgressColumnCachePQs)
 .reduce((a,k)=>{a[ProgressColumnCachePQs[k]]=k;return a},{}))