export const ProgressColumnVdusPQs=/**@type {!xyz.swapee.wc.ProgressColumnVdusPQs}*/({
 TransactionIdWr:'fa9e1',
 Step1Circle:'fa9e2',
 Step2Circle:'fa9e3',
 Step3Circle:'fa9e4',
 Step2La:'fa9e5',
 TransactionIdLa:'fa9e6',
 Step3Wr:'fa9e9',
 Step2BotSep:'fa9e10',
 Step1La:'fa9e12',
 Step1ProgressCircleWr:'fa9e13',
 Step3La:'fa9e14',
 PaymentStatusWr:'fa9e15',
 PaymentStatusLa:'fa9e16',
 TransactionStatusWr:'fa9e17',
 TransactionStatusLa:'fa9e18',
 Step1Wr:'fa9e19',
 Step2Wr:'fa9e20',
 InfoNotice:'fa9e21',
 SupportNotice:'fa9e22',
 Step1Co:'fa9e23',
 Step2Co:'fa9e24',
 Step3Co:'fa9e25',
 Step2ProgressCircleWr:'fa9e26',
 KycRequired:'fa9e27',
 KycNotRequired:'fa9e28',
 ExchangeEmail:'fa9e29',
 KycBlock:'fa9e30',
})