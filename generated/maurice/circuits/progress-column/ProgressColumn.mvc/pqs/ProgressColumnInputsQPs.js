import {ProgressColumnInputsPQs} from './ProgressColumnInputsPQs'
export const ProgressColumnInputsQPs=/**@type {!xyz.swapee.wc.ProgressColumnInputsQPs}*/(Object.keys(ProgressColumnInputsPQs)
 .reduce((a,k)=>{a[ProgressColumnInputsPQs[k]]=k;return a},{}))