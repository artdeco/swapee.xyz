import {ProgressColumnMemoryPQs} from './ProgressColumnMemoryPQs'
export const ProgressColumnInputsPQs=/**@type {!xyz.swapee.wc.ProgressColumnInputsQPs}*/({
 ...ProgressColumnMemoryPQs,
})