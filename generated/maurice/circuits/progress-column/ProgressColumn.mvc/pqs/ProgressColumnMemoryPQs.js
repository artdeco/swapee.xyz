export const ProgressColumnMemoryPQs=/**@type {!xyz.swapee.wc.ProgressColumnMemoryPQs}*/({
 core:'a74ad',
 creatingTransaction:'89fc6',
 finishedOnPayment:'a7265',
 paymentFailed:'02236',
 paymentCompleted:'497c0',
 transactionId:'67113',
 paymentStatus:'b5666',
 status:'9acb4',
 loadingStep1:'da35f',
 loadingStep2:'277b1',
 loadingStep3:'d7265',
})