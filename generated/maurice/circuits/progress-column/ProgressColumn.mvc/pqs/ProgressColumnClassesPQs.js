const ProgressColumnClassesPQs=/**@type {!xyz.swapee.wc.ProgressColumnClassesPQs}*/({
 CircleLoading:'5e522',
 CircleComplete:'a5ad9',
 CircleWaiting:'9912b',
 CircleFailed:'dc9d7',
 StepLabelFailed:'7e898',
 StepLabelWaiting:'e4d45',
})
export default ProgressColumnClassesPQs