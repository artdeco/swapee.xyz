import {ProgressColumnVdusPQs} from './ProgressColumnVdusPQs'
export const ProgressColumnVdusQPs=/**@type {!xyz.swapee.wc.ProgressColumnVdusQPs}*/(Object.keys(ProgressColumnVdusPQs)
 .reduce((a,k)=>{a[ProgressColumnVdusPQs[k]]=k;return a},{}))