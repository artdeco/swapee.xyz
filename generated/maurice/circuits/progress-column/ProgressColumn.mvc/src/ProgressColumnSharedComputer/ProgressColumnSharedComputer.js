import adaptProcessingTransaction from './methods/adapt-processing-transaction'
import adaptTransactionStatus from './methods/adapt-transaction-status'
import {preadaptProcessingTransaction,preadaptTransactionStatus} from '../../gen/AbstractProgressColumnComputer/preadapters'
import AbstractProgressColumnComputer from '../../gen/AbstractProgressColumnComputer'

const ProgressColumnSharedComputer=AbstractProgressColumnComputer.__trait(
 /** @type {!xyz.swapee.wc.IProgressColumnComputer} */ ({
  adaptProcessingTransaction:adaptProcessingTransaction,
  adaptTransactionStatus:adaptTransactionStatus,
  adapt:[preadaptProcessingTransaction,preadaptTransactionStatus],
 }),
)
export default ProgressColumnSharedComputer