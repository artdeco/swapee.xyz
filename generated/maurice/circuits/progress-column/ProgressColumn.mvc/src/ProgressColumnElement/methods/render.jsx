/** @type {xyz.swapee.wc.IProgressColumnElement._render} */
export default function ProgressColumnRender({
 loadingStep1,loadingStep2,loadingStep3,
 transactionId:transactionId,
 paymentFailed:paymentFailed,paymentStatus:paymentStatus,
 finishedOnPayment:finishedOnPayment,paymentCompleted:paymentCompleted,
 transactionStatus:transactionStatus,
 // processingTransaction:processingTransaction,
 status,
},{}) {
 return (<div $id="ProgressColumn">
  <p $id="TransactionIdWr" $reveal={transactionId} />
  <span $id="Step1Circle"
   CircleLoading={loadingStep1}
   CircleComplete={!!transactionId}
  />
  <span $id="Step2Circle"
   CircleLoading={loadingStep2}
   CircleWaiting={!transactionId}
   CircleFailed={paymentFailed}
   CircleComplete={paymentCompleted}
  />
  <span $id="Step3Circle"
   CircleLoading={loadingStep3}
   CircleWaiting={!transactionStatus}
   CircleFailed={status=='failed'}
   CircleComplete={status=='finished'}
  />

  <span $id="Step2La" StepLabelFailed={paymentFailed} StepLabelWaiting={!transactionId} />
  <span $id="Step3La" StepLabelFailed={status=='failed'} StepLabelWaiting={!transactionStatus} />

  <p $id="TransactionIdLa">{transactionId} </p>

  <div $id="PaymentStatusWr" $reveal={paymentStatus}>
   <span $id="PaymentStatusLa">{paymentStatus}</span>
  </div>

  <div $id="TransactionStatusWr" $reveal={transactionStatus}>
   <span $id="TransactionStatusLa">{transactionStatus}</span>
  </div>

  <div $id="Step3Wr" $conceal={finishedOnPayment} />
  <div $id="Step2BotSep" $conceal={finishedOnPayment} />
 </div>)
}