import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import ProgressColumnServerController from '../ProgressColumnServerController'
import ProgressColumnServerComputer from '../ProgressColumnServerComputer'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractProgressColumnElement from '../../gen/AbstractProgressColumnElement'

/** @extends {xyz.swapee.wc.ProgressColumnElement} */
export default class ProgressColumnElement extends AbstractProgressColumnElement.implements(
 ProgressColumnServerController,
 ProgressColumnServerComputer,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IProgressColumnElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IProgressColumnElement}*/({
   classesMap:        true,
   rootSelector:     `.ProgressColumn`,
   stylesheet:       'html/styles/ProgressColumn.css',
   blockName:        'html/ProgressColumnBlock.html',
  }),
){}

// thank you for using web circuits
