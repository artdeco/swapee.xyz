import ProgressColumnSharedComputer from '../ProgressColumnSharedComputer'
import AbstractProgressColumnComputer from '../../gen/AbstractProgressColumnComputer'

/** @extends {xyz.swapee.wc.ProgressColumnComputer} */
export default class ProgressColumnHtmlComputer extends AbstractProgressColumnComputer.implements(
 ProgressColumnSharedComputer,
){}