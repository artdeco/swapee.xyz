import ProgressColumnHtmlController from '../ProgressColumnHtmlController'
import ProgressColumnHtmlComputer from '../ProgressColumnHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractProgressColumnHtmlComponent} from '../../gen/AbstractProgressColumnHtmlComponent'

/** @extends {xyz.swapee.wc.ProgressColumnHtmlComponent} */
export default class extends AbstractProgressColumnHtmlComponent.implements(
 ProgressColumnHtmlController,
 ProgressColumnHtmlComputer,
 IntegratedComponentInitialiser,
){}