import AbstractProgressColumnControllerAT from '../../gen/AbstractProgressColumnControllerAT'
import ProgressColumnDisplay from '../ProgressColumnDisplay'
import AbstractProgressColumnScreen from '../../gen/AbstractProgressColumnScreen'

/** @extends {xyz.swapee.wc.ProgressColumnScreen} */
export default class extends AbstractProgressColumnScreen.implements(
 AbstractProgressColumnControllerAT,
 ProgressColumnDisplay,
 /**@type {!xyz.swapee.wc.IProgressColumnScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IProgressColumnScreen} */ ({
  __$id:6526983971,
 }),
){}