import ProgressColumnSharedComputer from '../ProgressColumnSharedComputer'
import AbstractProgressColumnComputer from '../../gen/AbstractProgressColumnComputer'

/** @extends {xyz.swapee.wc.ProgressColumnComputer} */
export default class ProgressColumnServerComputer extends AbstractProgressColumnComputer.implements(
 ProgressColumnSharedComputer,
){}