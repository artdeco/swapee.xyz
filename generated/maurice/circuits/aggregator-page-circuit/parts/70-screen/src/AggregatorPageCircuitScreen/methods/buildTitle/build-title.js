import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IAggregatorPageCircuitScreen._buildTitle} */
export default function buildTitle(state,baseTitle){
 const{
  amountFrom:amount,
  cryptoIn:from,
  cryptoOut:to,
 }=state
 if(!from||!to) return baseTitle
 return['Exchange',amount,from,'to',to,'|',baseTitle].join(' ')
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvYWdncmVnYXRvci1wYWdlLWNpcmN1aXQvYWdncmVnYXRvci1wYWdlLWNpcmN1aXQud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQW9GRyxTQUFTLFVBQVUsQ0FBQyxLQUFLLENBQUM7Q0FDekI7RUFDQyxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLFlBQVk7R0FDWDtDQUNGLEVBQUUsQ0FBQyxZQUFZLE9BQU87Q0FDdEIsaUJBQWlCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHO0FBQzdELENBQUYifQ==