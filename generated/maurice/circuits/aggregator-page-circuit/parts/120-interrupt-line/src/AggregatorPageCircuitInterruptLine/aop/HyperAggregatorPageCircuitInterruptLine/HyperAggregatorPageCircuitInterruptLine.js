import AbstractHyperAggregatorPageCircuitInterruptLine from '../../../../gen/AbstractAggregatorPageCircuitInterruptLine/hyper/AbstractHyperAggregatorPageCircuitInterruptLine'
import AggregatorPageCircuitInterruptLine from '../../AggregatorPageCircuitInterruptLine'
import AggregatorPageCircuitInterruptLineGeneralAspects from '../AggregatorPageCircuitInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperAggregatorPageCircuitInterruptLine} */
export default class extends AbstractHyperAggregatorPageCircuitInterruptLine
 .consults(
  AggregatorPageCircuitInterruptLineGeneralAspects,
 )
 .implements(
  AggregatorPageCircuitInterruptLine,
 )
{}