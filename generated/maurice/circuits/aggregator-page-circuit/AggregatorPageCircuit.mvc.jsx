/** @extends {xyz.swapee.wc.AbstractAggregatorPageCircuit} */
export default class AbstractAggregatorPageCircuit extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitComputer} */
export class AbstractAggregatorPageCircuitComputer extends (<computer>
   <adapter name="adaptIntent">
    <xyz.swapee.wc.IExchangeIntentCore currencyFrom="real" currencyTo amountFrom fixed float any />
    <outputs>
     <xyz.swapee.wc.IAggregatorPageCircuitCore type cryptoIn cryptoOut amountFrom />
    </outputs>
    From the intent, reads the data.
   </adapter>
   <adapter name="adaptType">
    <xyz.swapee.wc.IAggregatorPageCircuitCore type />
    <outputs>
     <xyz.swapee.wc.IAggregatorPageCircuitCore fixedRate floatRate anyRate />
    </outputs>
   </adapter>
   <adapter name="adaptInterlocks">
    <xyz.swapee.wc.IAggregatorPageCircuitCore fixedRate floatRate anyRate />
    <outputs>
     <xyz.swapee.wc.IAggregatorPageCircuitCore fixedRate floatRate anyRate />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitController} */
export class AbstractAggregatorPageCircuitController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitPort} */
export class AggregatorPageCircuitPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitView} */
export class AbstractAggregatorPageCircuitView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitElement} */
export class AbstractAggregatorPageCircuitElement extends (<element v3 html mv>
 <block src="./AggregatorPageCircuit.mvc/src/AggregatorPageCircuitElement/methods/render.jsx" />
 <inducer src="./AggregatorPageCircuit.mvc/src/AggregatorPageCircuitElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent} */
export class AbstractAggregatorPageCircuitHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
  </connectors>

</html-ic>) { }
// </class-end>