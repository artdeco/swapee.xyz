import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitProcessor}
 */
function __AbstractAggregatorPageCircuitProcessor() {}
__AbstractAggregatorPageCircuitProcessor.prototype = /** @type {!_AbstractAggregatorPageCircuitProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor}
 */
class _AbstractAggregatorPageCircuitProcessor { }
/**
 * The processor to compute changes to the memory for the _IAggregatorPageCircuit_.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor} ‎
 */
class AbstractAggregatorPageCircuitProcessor extends newAbstract(
 _AbstractAggregatorPageCircuitProcessor,34948615608,null,{
  asIAggregatorPageCircuitProcessor:1,
  superAggregatorPageCircuitProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor} */
AbstractAggregatorPageCircuitProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor} */
function AbstractAggregatorPageCircuitProcessorClass(){}

export default AbstractAggregatorPageCircuitProcessor


AbstractAggregatorPageCircuitProcessor[$implementations]=[
 __AbstractAggregatorPageCircuitProcessor,
 AbstractAggregatorPageCircuitProcessorClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitProcessor}*/({
  flipFixedRate(){
   const{
    asIAggregatorPageCircuitComputer:{
     model:{fixedRate:fixedRate},
     setInfo:setInfo,
    },
   }=this
   setInfo({fixedRate:!fixedRate})
  },
  flipFloatRate(){
   const{
    asIAggregatorPageCircuitComputer:{
     model:{floatRate:floatRate},
     setInfo:setInfo,
    },
   }=this
   setInfo({floatRate:!floatRate})
  },
  flipAnyRate(){
   const{
    asIAggregatorPageCircuitComputer:{
     model:{anyRate:anyRate},
     setInfo:setInfo,
    },
   }=this
   setInfo({anyRate:!anyRate})
  },
 }),
]