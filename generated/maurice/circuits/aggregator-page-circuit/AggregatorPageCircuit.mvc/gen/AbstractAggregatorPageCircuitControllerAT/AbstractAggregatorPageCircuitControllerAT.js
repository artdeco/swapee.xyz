import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitControllerAT}
 */
function __AbstractAggregatorPageCircuitControllerAT() {}
__AbstractAggregatorPageCircuitControllerAT.prototype = /** @type {!_AbstractAggregatorPageCircuitControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT}
 */
class _AbstractAggregatorPageCircuitControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IAggregatorPageCircuitControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT} ‎
 */
class AbstractAggregatorPageCircuitControllerAT extends newAbstract(
 _AbstractAggregatorPageCircuitControllerAT,349486156025,null,{
  asIAggregatorPageCircuitControllerAT:1,
  superAggregatorPageCircuitControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT} */
AbstractAggregatorPageCircuitControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT} */
function AbstractAggregatorPageCircuitControllerATClass(){}

export default AbstractAggregatorPageCircuitControllerAT


AbstractAggregatorPageCircuitControllerAT[$implementations]=[
 __AbstractAggregatorPageCircuitControllerAT,
 UartUniversal,
 AbstractAggregatorPageCircuitControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT}*/({
  get asIAggregatorPageCircuitController(){
   return this
  },
  setAmountFrom(){
   this.uart.t("inv",{mid:'2d96f',args:[...arguments]})
  },
  unsetAmountFrom(){
   this.uart.t("inv",{mid:'88eeb'})
  },
  setCryptoIn(){
   this.uart.t("inv",{mid:'6b12f',args:[...arguments]})
  },
  unsetCryptoIn(){
   this.uart.t("inv",{mid:'c2477'})
  },
  setCryptoOut(){
   this.uart.t("inv",{mid:'b82e8',args:[...arguments]})
  },
  unsetCryptoOut(){
   this.uart.t("inv",{mid:'63499'})
  },
  flipFixedRate(){
   this.uart.t("inv",{mid:'af2f0'})
  },
  setFixedRate(){
   this.uart.t("inv",{mid:'a2efb'})
  },
  unsetFixedRate(){
   this.uart.t("inv",{mid:'7e36f'})
  },
  flipFloatRate(){
   this.uart.t("inv",{mid:'d889d'})
  },
  setFloatRate(){
   this.uart.t("inv",{mid:'dd684'})
  },
  unsetFloatRate(){
   this.uart.t("inv",{mid:'6ed3e'})
  },
  flipAnyRate(){
   this.uart.t("inv",{mid:'35894'})
  },
  setAnyRate(){
   this.uart.t("inv",{mid:'fe483'})
  },
  unsetAnyRate(){
   this.uart.t("inv",{mid:'21cb9'})
  },
 }),
]