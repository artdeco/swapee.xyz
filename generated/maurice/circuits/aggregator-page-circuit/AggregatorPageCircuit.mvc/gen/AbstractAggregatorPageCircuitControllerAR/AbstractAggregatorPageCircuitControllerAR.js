import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitControllerAR}
 */
function __AbstractAggregatorPageCircuitControllerAR() {}
__AbstractAggregatorPageCircuitControllerAR.prototype = /** @type {!_AbstractAggregatorPageCircuitControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR}
 */
class _AbstractAggregatorPageCircuitControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IAggregatorPageCircuitControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR} ‎
 */
class AbstractAggregatorPageCircuitControllerAR extends newAbstract(
 _AbstractAggregatorPageCircuitControllerAR,349486156024,null,{
  asIAggregatorPageCircuitControllerAR:1,
  superAggregatorPageCircuitControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR} */
AbstractAggregatorPageCircuitControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR} */
function AbstractAggregatorPageCircuitControllerARClass(){}

export default AbstractAggregatorPageCircuitControllerAR


AbstractAggregatorPageCircuitControllerAR[$implementations]=[
 __AbstractAggregatorPageCircuitControllerAR,
 AR,
 AbstractAggregatorPageCircuitControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR}*/({
  allocator(){
   this.methods={
    setAmountFrom:'2d96f',
    unsetAmountFrom:'88eeb',
    setCryptoIn:'6b12f',
    unsetCryptoIn:'c2477',
    setCryptoOut:'b82e8',
    unsetCryptoOut:'63499',
    flipFixedRate:'af2f0',
    setFixedRate:'a2efb',
    unsetFixedRate:'7e36f',
    flipFloatRate:'d889d',
    setFloatRate:'dd684',
    unsetFloatRate:'6ed3e',
    flipAnyRate:'35894',
    setAnyRate:'fe483',
    unsetAnyRate:'21cb9',
   }
  },
 }),
]