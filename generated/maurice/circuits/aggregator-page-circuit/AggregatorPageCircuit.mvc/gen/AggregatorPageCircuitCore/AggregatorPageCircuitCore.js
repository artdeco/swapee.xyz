import {mountPins} from '@type.engineering/seers'
import {AggregatorPageCircuitMemoryPQs} from '../../pqs/AggregatorPageCircuitMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AggregatorPageCircuitCore}
 */
function __AggregatorPageCircuitCore() {}
__AggregatorPageCircuitCore.prototype = /** @type {!_AggregatorPageCircuitCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitCore}
 */
class _AggregatorPageCircuitCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitCore} ‎
 */
class AggregatorPageCircuitCore extends newAbstract(
 _AggregatorPageCircuitCore,34948615607,null,{
  asIAggregatorPageCircuitCore:1,
  superAggregatorPageCircuitCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitCore} */
AggregatorPageCircuitCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitCore} */
function AggregatorPageCircuitCoreClass(){}

export default AggregatorPageCircuitCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AggregatorPageCircuitOuterCore}
 */
function __AggregatorPageCircuitOuterCore() {}
__AggregatorPageCircuitOuterCore.prototype = /** @type {!_AggregatorPageCircuitOuterCore} */ ({ })
/** @this {xyz.swapee.wc.AggregatorPageCircuitOuterCore} */
export function AggregatorPageCircuitOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model}*/
  this.model={
    type: 'any',
    amountFrom: '0.1',
    cryptoIn: 'BTC',
    cryptoOut: 'ETH',
    fixedRate: false,
    floatRate: false,
    anyRate: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore}
 */
class _AggregatorPageCircuitOuterCore { }
/**
 * The _IAggregatorPageCircuit_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore} ‎
 */
export class AggregatorPageCircuitOuterCore extends newAbstract(
 _AggregatorPageCircuitOuterCore,34948615603,AggregatorPageCircuitOuterCoreConstructor,{
  asIAggregatorPageCircuitOuterCore:1,
  superAggregatorPageCircuitOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore} */
AggregatorPageCircuitOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore} */
function AggregatorPageCircuitOuterCoreClass(){}


AggregatorPageCircuitOuterCore[$implementations]=[
 __AggregatorPageCircuitOuterCore,
 AggregatorPageCircuitOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitOuterCore}*/({
  constructor(){
   mountPins(this.model,AggregatorPageCircuitMemoryPQs)

  },
 }),
]

AggregatorPageCircuitCore[$implementations]=[
 AggregatorPageCircuitCoreClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitCore}*/({
  resetCore(){
   this.resetAggregatorPageCircuitCore()
  },
  resetAggregatorPageCircuitCore(){
   AggregatorPageCircuitOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.AggregatorPageCircuitOuterCore}*/(
     /**@type {!xyz.swapee.wc.IAggregatorPageCircuitOuterCore}*/(this)),
   )
  },
 }),
 __AggregatorPageCircuitCore,
 AggregatorPageCircuitOuterCore,
]

export {AggregatorPageCircuitCore}