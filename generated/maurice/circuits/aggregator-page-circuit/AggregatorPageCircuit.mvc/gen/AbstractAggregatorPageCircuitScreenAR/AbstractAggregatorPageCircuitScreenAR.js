import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitScreenAR}
 */
function __AbstractAggregatorPageCircuitScreenAR() {}
__AbstractAggregatorPageCircuitScreenAR.prototype = /** @type {!_AbstractAggregatorPageCircuitScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR}
 */
class _AbstractAggregatorPageCircuitScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IAggregatorPageCircuitScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR} ‎
 */
class AbstractAggregatorPageCircuitScreenAR extends newAbstract(
 _AbstractAggregatorPageCircuitScreenAR,349486156028,null,{
  asIAggregatorPageCircuitScreenAR:1,
  superAggregatorPageCircuitScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR} */
AbstractAggregatorPageCircuitScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR} */
function AbstractAggregatorPageCircuitScreenARClass(){}

export default AbstractAggregatorPageCircuitScreenAR


AbstractAggregatorPageCircuitScreenAR[$implementations]=[
 __AbstractAggregatorPageCircuitScreenAR,
 AR,
 AbstractAggregatorPageCircuitScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractAggregatorPageCircuitScreenAR}