import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {AggregatorPageCircuitInputsPQs} from '../../pqs/AggregatorPageCircuitInputsPQs'
import {AggregatorPageCircuitOuterCoreConstructor} from '../AggregatorPageCircuitCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AggregatorPageCircuitPort}
 */
function __AggregatorPageCircuitPort() {}
__AggregatorPageCircuitPort.prototype = /** @type {!_AggregatorPageCircuitPort} */ ({ })
/** @this {xyz.swapee.wc.AggregatorPageCircuitPort} */ function AggregatorPageCircuitPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.AggregatorPageCircuitOuterCore} */ ({model:null})
  AggregatorPageCircuitOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitPort}
 */
class _AggregatorPageCircuitPort { }
/**
 * The port that serves as an interface to the _IAggregatorPageCircuit_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitPort} ‎
 */
export class AggregatorPageCircuitPort extends newAbstract(
 _AggregatorPageCircuitPort,34948615605,AggregatorPageCircuitPortConstructor,{
  asIAggregatorPageCircuitPort:1,
  superAggregatorPageCircuitPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitPort} */
AggregatorPageCircuitPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitPort} */
function AggregatorPageCircuitPortClass(){}

export const AggregatorPageCircuitPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IAggregatorPageCircuit.Pinout>}*/({
 get Port() { return AggregatorPageCircuitPort },
})

AggregatorPageCircuitPort[$implementations]=[
 AggregatorPageCircuitPortClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitPort}*/({
  resetPort(){
   this.resetAggregatorPageCircuitPort()
  },
  resetAggregatorPageCircuitPort(){
   AggregatorPageCircuitPortConstructor.call(this)
  },
 }),
 __AggregatorPageCircuitPort,
 Parametric,
 AggregatorPageCircuitPortClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitPort}*/({
  constructor(){
   mountPins(this.inputs,AggregatorPageCircuitInputsPQs)
  },
 }),
]


export default AggregatorPageCircuitPort