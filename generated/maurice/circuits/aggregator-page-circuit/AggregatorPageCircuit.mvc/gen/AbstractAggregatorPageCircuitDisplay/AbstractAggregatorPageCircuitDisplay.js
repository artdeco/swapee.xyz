import {Display} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitDisplay}
 */
function __AbstractAggregatorPageCircuitDisplay() {}
__AbstractAggregatorPageCircuitDisplay.prototype = /** @type {!_AbstractAggregatorPageCircuitDisplay} */ ({ })
/** @this {xyz.swapee.wc.AggregatorPageCircuitDisplay} */ function AggregatorPageCircuitDisplayConstructor() {
  /** @type {HTMLElement} */ this.ExchangeIntent=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay}
 */
class _AbstractAggregatorPageCircuitDisplay { }
/**
 * Display for presenting information from the _IAggregatorPageCircuit_.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay} ‎
 */
class AbstractAggregatorPageCircuitDisplay extends newAbstract(
 _AbstractAggregatorPageCircuitDisplay,349486156017,AggregatorPageCircuitDisplayConstructor,{
  asIAggregatorPageCircuitDisplay:1,
  superAggregatorPageCircuitDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay} */
AbstractAggregatorPageCircuitDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay} */
function AbstractAggregatorPageCircuitDisplayClass(){}

export default AbstractAggregatorPageCircuitDisplay


AbstractAggregatorPageCircuitDisplay[$implementations]=[
 __AbstractAggregatorPageCircuitDisplay,
 Display,
 AbstractAggregatorPageCircuitDisplayClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{exchangeIntentScopeSel:exchangeIntentScopeSel}}=this
    this.scan({
     exchangeIntentSel:exchangeIntentScopeSel,
    })
   })
  },
  scan:function vduScan({exchangeIntentSel:exchangeIntentSelScope}){
   const{element:element,queries:{exchangeIntentSel}}=this

   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   Object.assign(this,{
    ExchangeIntent:ExchangeIntent,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.AggregatorPageCircuitDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese}*/({
   ExchangeIntent:1,
  }),
  initializer({
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]