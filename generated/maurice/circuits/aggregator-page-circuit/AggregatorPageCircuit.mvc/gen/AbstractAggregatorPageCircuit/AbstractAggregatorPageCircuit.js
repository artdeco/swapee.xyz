import AbstractAggregatorPageCircuitProcessor from '../AbstractAggregatorPageCircuitProcessor'
import {AggregatorPageCircuitCore} from '../AggregatorPageCircuitCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractAggregatorPageCircuitComputer} from '../AbstractAggregatorPageCircuitComputer'
import {AbstractAggregatorPageCircuitController} from '../AbstractAggregatorPageCircuitController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuit}
 */
function __AbstractAggregatorPageCircuit() {}
__AbstractAggregatorPageCircuit.prototype = /** @type {!_AbstractAggregatorPageCircuit} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuit}
 */
class _AbstractAggregatorPageCircuit { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuit} ‎
 */
class AbstractAggregatorPageCircuit extends newAbstract(
 _AbstractAggregatorPageCircuit,34948615609,null,{
  asIAggregatorPageCircuit:1,
  superAggregatorPageCircuit:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuit} */
AbstractAggregatorPageCircuit.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuit} */
function AbstractAggregatorPageCircuitClass(){}

export default AbstractAggregatorPageCircuit


AbstractAggregatorPageCircuit[$implementations]=[
 __AbstractAggregatorPageCircuit,
 AggregatorPageCircuitCore,
 AbstractAggregatorPageCircuitProcessor,
 IntegratedComponent,
 AbstractAggregatorPageCircuitComputer,
 AbstractAggregatorPageCircuitController,
]


export {AbstractAggregatorPageCircuit}