import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AggregatorPageCircuitElementPort}
 */
function __AggregatorPageCircuitElementPort() {}
__AggregatorPageCircuitElementPort.prototype = /** @type {!_AggregatorPageCircuitElementPort} */ ({ })
/** @this {xyz.swapee.wc.AggregatorPageCircuitElementPort} */ function AggregatorPageCircuitElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    exchangeIntentOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort}
 */
class _AggregatorPageCircuitElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort} ‎
 */
class AggregatorPageCircuitElementPort extends newAbstract(
 _AggregatorPageCircuitElementPort,349486156014,AggregatorPageCircuitElementPortConstructor,{
  asIAggregatorPageCircuitElementPort:1,
  superAggregatorPageCircuitElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort} */
AggregatorPageCircuitElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort} */
function AggregatorPageCircuitElementPortClass(){}

export default AggregatorPageCircuitElementPort


AggregatorPageCircuitElementPort[$implementations]=[
 __AggregatorPageCircuitElementPort,
 AggregatorPageCircuitElementPortClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'exchange-intent-opts':undefined,
    'amount-from':undefined,
    'crypto-in':undefined,
    'crypto-out':undefined,
    'fixed-rate':undefined,
    'float-rate':undefined,
    'any-rate':undefined,
   })
  },
 }),
]