import AggregatorPageCircuitElementPort from '../AggregatorPageCircuitElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {AggregatorPageCircuitInputsPQs} from '../../pqs/AggregatorPageCircuitInputsPQs'
import {AggregatorPageCircuitQueriesPQs} from '../../pqs/AggregatorPageCircuitQueriesPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractAggregatorPageCircuit from '../AbstractAggregatorPageCircuit'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitElement}
 */
function __AbstractAggregatorPageCircuitElement() {}
__AbstractAggregatorPageCircuitElement.prototype = /** @type {!_AbstractAggregatorPageCircuitElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitElement}
 */
class _AbstractAggregatorPageCircuitElement { }
/**
 * A component description.
 *
 * The _IAggregatorPageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitElement} ‎
 */
class AbstractAggregatorPageCircuitElement extends newAbstract(
 _AbstractAggregatorPageCircuitElement,349486156013,null,{
  asIAggregatorPageCircuitElement:1,
  superAggregatorPageCircuitElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElement} */
AbstractAggregatorPageCircuitElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElement} */
function AbstractAggregatorPageCircuitElementClass(){}

export default AbstractAggregatorPageCircuitElement


AbstractAggregatorPageCircuitElement[$implementations]=[
 __AbstractAggregatorPageCircuitElement,
 ElementBase,
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':type':typeColAttr,
   ':amount-from':amountFromColAttr,
   ':crypto-in':cryptoInColAttr,
   ':crypto-out':cryptoOutColAttr,
   ':fixed-rate':fixedRateColAttr,
   ':float-rate':floatRateColAttr,
   ':any-rate':anyRateColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'type':typeAttr,
    'amount-from':amountFromAttr,
    'crypto-in':cryptoInAttr,
    'crypto-out':cryptoOutAttr,
    'fixed-rate':fixedRateAttr,
    'float-rate':floatRateAttr,
    'any-rate':anyRateAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(typeAttr===undefined?{'type':typeColAttr}:{}),
    ...(amountFromAttr===undefined?{'amount-from':amountFromColAttr}:{}),
    ...(cryptoInAttr===undefined?{'crypto-in':cryptoInColAttr}:{}),
    ...(cryptoOutAttr===undefined?{'crypto-out':cryptoOutColAttr}:{}),
    ...(fixedRateAttr===undefined?{'fixed-rate':fixedRateColAttr}:{}),
    ...(floatRateAttr===undefined?{'float-rate':floatRateColAttr}:{}),
    ...(anyRateAttr===undefined?{'any-rate':anyRateColAttr}:{}),
   }
  },
 }),
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'type':typeAttr,
   'amount-from':amountFromAttr,
   'crypto-in':cryptoInAttr,
   'crypto-out':cryptoOutAttr,
   'fixed-rate':fixedRateAttr,
   'float-rate':floatRateAttr,
   'any-rate':anyRateAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    type:typeAttr,
    amountFrom:amountFromAttr,
    cryptoIn:cryptoInAttr,
    cryptoOut:cryptoOutAttr,
    fixedRate:fixedRateAttr,
    floatRate:floatRateAttr,
    anyRate:anyRateAttr,
   }
  },
 }),
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  inputsPQs:AggregatorPageCircuitInputsPQs,
  queriesPQs:AggregatorPageCircuitQueriesPQs,
  vdus:{
   'ExchangeIntent': 'acdb1',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','type','amountFrom','cryptoIn','cryptoOut','fixedRate','floatRate','anyRate','query:exchange-intent','no-solder',':no-solder',':type','amount-from',':amount-from','crypto-in',':crypto-in','crypto-out',':crypto-out','fixed-rate',':fixed-rate','float-rate',':float-rate','any-rate',':any-rate','fe646','c3b6b','599dc','748e6','4212d','c60b5','7d0bd','a8df4','60e57','children']),
   })
  },
  get Port(){
   return AggregatorPageCircuitElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:exchange-intent':exchangeIntentSel}){
   const _ret={}
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   return _ret
  },
 }),
 Landed,
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
   }
  },
 }),
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractAggregatorPageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
  solder:(_,{
   exchangeIntentSel:exchangeIntentSel,
  })=>{
   return{
    exchangeIntentSel:exchangeIntentSel,
   }
  },
 }),
]



AbstractAggregatorPageCircuitElement[$implementations]=[AbstractAggregatorPageCircuit,
 /** @type {!AbstractAggregatorPageCircuitElement} */ ({
  rootId:'AggregatorPageCircuit',
  __$id:3494861560,
  fqn:'xyz.swapee.wc.IAggregatorPageCircuit',
  maurice_element_v3:true,
 }),
]