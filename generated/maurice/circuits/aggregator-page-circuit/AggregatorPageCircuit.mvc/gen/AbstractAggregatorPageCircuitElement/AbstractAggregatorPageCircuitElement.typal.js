
import AbstractAggregatorPageCircuit from '../AbstractAggregatorPageCircuit'

/** @abstract {xyz.swapee.wc.IAggregatorPageCircuitElement} */
export default class AbstractAggregatorPageCircuitElement { }



AbstractAggregatorPageCircuitElement[$implementations]=[AbstractAggregatorPageCircuit,
 /** @type {!AbstractAggregatorPageCircuitElement} */ ({
  rootId:'AggregatorPageCircuit',
  __$id:3494861560,
  fqn:'xyz.swapee.wc.IAggregatorPageCircuit',
  maurice_element_v3:true,
 }),
]