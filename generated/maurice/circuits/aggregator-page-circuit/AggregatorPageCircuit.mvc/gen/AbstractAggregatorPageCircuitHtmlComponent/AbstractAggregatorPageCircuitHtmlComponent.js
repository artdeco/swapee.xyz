import AbstractAggregatorPageCircuitGPU from '../AbstractAggregatorPageCircuitGPU'
import AbstractAggregatorPageCircuitScreenBack from '../AbstractAggregatorPageCircuitScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {AggregatorPageCircuitInputsQPs} from '../../pqs/AggregatorPageCircuitInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractAggregatorPageCircuit from '../AbstractAggregatorPageCircuit'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitHtmlComponent}
 */
function __AbstractAggregatorPageCircuitHtmlComponent() {}
__AbstractAggregatorPageCircuitHtmlComponent.prototype = /** @type {!_AbstractAggregatorPageCircuitHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent}
 */
class _AbstractAggregatorPageCircuitHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.AggregatorPageCircuitHtmlComponent} */ (res)
  }
}
/**
 * The _IAggregatorPageCircuit_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent} ‎
 */
export class AbstractAggregatorPageCircuitHtmlComponent extends newAbstract(
 _AbstractAggregatorPageCircuitHtmlComponent,349486156012,null,{
  asIAggregatorPageCircuitHtmlComponent:1,
  superAggregatorPageCircuitHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent} */
AbstractAggregatorPageCircuitHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent} */
function AbstractAggregatorPageCircuitHtmlComponentClass(){}


AbstractAggregatorPageCircuitHtmlComponent[$implementations]=[
 __AbstractAggregatorPageCircuitHtmlComponent,
 HtmlComponent,
 AbstractAggregatorPageCircuit,
 AbstractAggregatorPageCircuitGPU,
 AbstractAggregatorPageCircuitScreenBack,
 Landed,
 AbstractAggregatorPageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
   }
  },
 }),
 AbstractAggregatorPageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent}*/({
  inputsQPs:AggregatorPageCircuitInputsQPs,
 }),
 AbstractAggregatorPageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIAggregatorPageCircuitGPU:{
     ExchangeIntent:ExchangeIntent,
    },
   }=this
   complete(7833868048,{ExchangeIntent:ExchangeIntent},{
    cryptoIn:'96c88', // -> currencyFrom
    cryptoOut:'c23cd', // -> currencyTo
    amountFrom:'748e6', // -> amountFrom
    fixedRate:'cec31', // -> fixed
    floatRate:'546ad', // -> float
    anyRate:'100b8', // -> any
    get'b2fda'(){ // -> ready
     return true
    },
   })
  },
 }),
]