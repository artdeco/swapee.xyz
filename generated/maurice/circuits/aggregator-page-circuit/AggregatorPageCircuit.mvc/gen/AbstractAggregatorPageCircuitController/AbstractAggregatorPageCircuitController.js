import AggregatorPageCircuitBuffer from '../AggregatorPageCircuitBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {AggregatorPageCircuitPortConnector} from '../AggregatorPageCircuitPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitController}
 */
function __AbstractAggregatorPageCircuitController() {}
__AbstractAggregatorPageCircuitController.prototype = /** @type {!_AbstractAggregatorPageCircuitController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitController}
 */
class _AbstractAggregatorPageCircuitController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitController} ‎
 */
export class AbstractAggregatorPageCircuitController extends newAbstract(
 _AbstractAggregatorPageCircuitController,349486156021,null,{
  asIAggregatorPageCircuitController:1,
  superAggregatorPageCircuitController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitController} */
AbstractAggregatorPageCircuitController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitController} */
function AbstractAggregatorPageCircuitControllerClass(){}


AbstractAggregatorPageCircuitController[$implementations]=[
 AbstractAggregatorPageCircuitControllerClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IAggregatorPageCircuitPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractAggregatorPageCircuitController,
 AggregatorPageCircuitBuffer,
 IntegratedController,
 /**@type {!AbstractAggregatorPageCircuitController}*/(AggregatorPageCircuitPortConnector),
 AbstractAggregatorPageCircuitControllerClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitController}*/({
  setAmountFrom(val){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({amountFrom:val})
  },
  setCryptoIn(val){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoIn:val})
  },
  setCryptoOut(val){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoOut:val})
  },
  setFixedRate(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({fixedRate:true})
  },
  setFloatRate(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({floatRate:true})
  },
  setAnyRate(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({anyRate:true})
  },
  unsetAmountFrom(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({amountFrom:''})
  },
  unsetCryptoIn(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoIn:''})
  },
  unsetCryptoOut(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoOut:''})
  },
  unsetFixedRate(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({fixedRate:false})
  },
  unsetFloatRate(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({floatRate:false})
  },
  unsetAnyRate(){
   const{asIAggregatorPageCircuitController:{setInputs:setInputs}}=this
   setInputs({anyRate:false})
  },
 }),
]


export default AbstractAggregatorPageCircuitController