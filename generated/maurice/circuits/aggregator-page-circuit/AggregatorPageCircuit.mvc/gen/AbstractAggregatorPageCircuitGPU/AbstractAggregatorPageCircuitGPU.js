import AbstractAggregatorPageCircuitDisplay from '../AbstractAggregatorPageCircuitDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {AggregatorPageCircuitVdusPQs} from '../../pqs/AggregatorPageCircuitVdusPQs'
import {AggregatorPageCircuitVdusQPs} from '../../pqs/AggregatorPageCircuitVdusQPs'
import {AggregatorPageCircuitMemoryPQs} from '../../pqs/AggregatorPageCircuitMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitGPU}
 */
function __AbstractAggregatorPageCircuitGPU() {}
__AbstractAggregatorPageCircuitGPU.prototype = /** @type {!_AbstractAggregatorPageCircuitGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitGPU}
 */
class _AbstractAggregatorPageCircuitGPU { }
/**
 * Handles the periphery of the _IAggregatorPageCircuitDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitGPU} ‎
 */
class AbstractAggregatorPageCircuitGPU extends newAbstract(
 _AbstractAggregatorPageCircuitGPU,349486156016,null,{
  asIAggregatorPageCircuitGPU:1,
  superAggregatorPageCircuitGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitGPU} */
AbstractAggregatorPageCircuitGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitGPU} */
function AbstractAggregatorPageCircuitGPUClass(){}

export default AbstractAggregatorPageCircuitGPU


AbstractAggregatorPageCircuitGPU[$implementations]=[
 __AbstractAggregatorPageCircuitGPU,
 AbstractAggregatorPageCircuitGPUClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitGPU}*/({
  vdusPQs:AggregatorPageCircuitVdusPQs,
  vdusQPs:AggregatorPageCircuitVdusQPs,
  memoryPQs:AggregatorPageCircuitMemoryPQs,
 }),
 AbstractAggregatorPageCircuitDisplay,
 BrowserView,
]