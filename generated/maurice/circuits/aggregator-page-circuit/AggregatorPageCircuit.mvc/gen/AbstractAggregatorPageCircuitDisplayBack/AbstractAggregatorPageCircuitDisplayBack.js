import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitDisplay}
 */
function __AbstractAggregatorPageCircuitDisplay() {}
__AbstractAggregatorPageCircuitDisplay.prototype = /** @type {!_AbstractAggregatorPageCircuitDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay}
 */
class _AbstractAggregatorPageCircuitDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay} ‎
 */
class AbstractAggregatorPageCircuitDisplay extends newAbstract(
 _AbstractAggregatorPageCircuitDisplay,349486156020,null,{
  asIAggregatorPageCircuitDisplay:1,
  superAggregatorPageCircuitDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay} */
AbstractAggregatorPageCircuitDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay} */
function AbstractAggregatorPageCircuitDisplayClass(){}

export default AbstractAggregatorPageCircuitDisplay


AbstractAggregatorPageCircuitDisplay[$implementations]=[
 __AbstractAggregatorPageCircuitDisplay,
 GraphicsDriverBack,
 AbstractAggregatorPageCircuitDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay}*/({
    ExchangeIntent:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.AggregatorPageCircuitDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.Initialese}*/({
   ExchangeIntent:1,
  }),
  initializer({
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]