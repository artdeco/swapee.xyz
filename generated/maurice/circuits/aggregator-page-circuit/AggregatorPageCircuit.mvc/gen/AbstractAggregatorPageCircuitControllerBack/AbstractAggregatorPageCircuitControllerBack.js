import AbstractAggregatorPageCircuitControllerAR from '../AbstractAggregatorPageCircuitControllerAR'
import {AbstractAggregatorPageCircuitController} from '../AbstractAggregatorPageCircuitController'
import {DriverBack,NavigatorBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitControllerBack}
 */
function __AbstractAggregatorPageCircuitControllerBack() {}
__AbstractAggregatorPageCircuitControllerBack.prototype = /** @type {!_AbstractAggregatorPageCircuitControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitController}
 */
class _AbstractAggregatorPageCircuitControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitController} ‎
 */
class AbstractAggregatorPageCircuitControllerBack extends newAbstract(
 _AbstractAggregatorPageCircuitControllerBack,349486156023,null,{
  asIAggregatorPageCircuitController:1,
  superAggregatorPageCircuitController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitController} */
AbstractAggregatorPageCircuitControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitController} */
function AbstractAggregatorPageCircuitControllerBackClass(){}

export default AbstractAggregatorPageCircuitControllerBack


AbstractAggregatorPageCircuitControllerBack[$implementations]=[
 __AbstractAggregatorPageCircuitControllerBack,
 AbstractAggregatorPageCircuitController,
 AbstractAggregatorPageCircuitControllerAR,
 DriverBack,
 NavigatorBack,
]