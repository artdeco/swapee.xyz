import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitScreenAT}
 */
function __AbstractAggregatorPageCircuitScreenAT() {}
__AbstractAggregatorPageCircuitScreenAT.prototype = /** @type {!_AbstractAggregatorPageCircuitScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT}
 */
class _AbstractAggregatorPageCircuitScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IAggregatorPageCircuitScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT} ‎
 */
class AbstractAggregatorPageCircuitScreenAT extends newAbstract(
 _AbstractAggregatorPageCircuitScreenAT,349486156029,null,{
  asIAggregatorPageCircuitScreenAT:1,
  superAggregatorPageCircuitScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT} */
AbstractAggregatorPageCircuitScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT} */
function AbstractAggregatorPageCircuitScreenATClass(){}

export default AbstractAggregatorPageCircuitScreenAT


AbstractAggregatorPageCircuitScreenAT[$implementations]=[
 __AbstractAggregatorPageCircuitScreenAT,
 UartUniversal,
]