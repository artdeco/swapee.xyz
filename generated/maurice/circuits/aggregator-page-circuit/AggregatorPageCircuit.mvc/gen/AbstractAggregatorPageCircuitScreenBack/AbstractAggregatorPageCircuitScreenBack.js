import AbstractAggregatorPageCircuitScreenAT from '../AbstractAggregatorPageCircuitScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitScreenBack}
 */
function __AbstractAggregatorPageCircuitScreenBack() {}
__AbstractAggregatorPageCircuitScreenBack.prototype = /** @type {!_AbstractAggregatorPageCircuitScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen}
 */
class _AbstractAggregatorPageCircuitScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen} ‎
 */
class AbstractAggregatorPageCircuitScreenBack extends newAbstract(
 _AbstractAggregatorPageCircuitScreenBack,349486156027,null,{
  asIAggregatorPageCircuitScreen:1,
  superAggregatorPageCircuitScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen} */
AbstractAggregatorPageCircuitScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen} */
function AbstractAggregatorPageCircuitScreenBackClass(){}

export default AbstractAggregatorPageCircuitScreenBack


AbstractAggregatorPageCircuitScreenBack[$implementations]=[
 __AbstractAggregatorPageCircuitScreenBack,
 AbstractAggregatorPageCircuitScreenAT,
]