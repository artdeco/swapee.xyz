import AbstractAggregatorPageCircuitScreenAR from '../AbstractAggregatorPageCircuitScreenAR'
import {CoreCache,Screen,AddressBar} from '@webcircuits/front'
import {AggregatorPageCircuitInputsPQs} from '../../pqs/AggregatorPageCircuitInputsPQs'
import {AggregatorPageCircuitQueriesPQs} from '../../pqs/AggregatorPageCircuitQueriesPQs'
import {AggregatorPageCircuitMemoryQPs} from '../../pqs/AggregatorPageCircuitMemoryQPs'
import {AggregatorPageCircuitVdusPQs} from '../../pqs/AggregatorPageCircuitVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitScreen}
 */
function __AbstractAggregatorPageCircuitScreen() {}
__AbstractAggregatorPageCircuitScreen.prototype = /** @type {!_AbstractAggregatorPageCircuitScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitScreen}
 */
class _AbstractAggregatorPageCircuitScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitScreen} ‎
 */
class AbstractAggregatorPageCircuitScreen extends newAbstract(
 _AbstractAggregatorPageCircuitScreen,349486156026,null,{
  asIAggregatorPageCircuitScreen:1,
  superAggregatorPageCircuitScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitScreen} */
AbstractAggregatorPageCircuitScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitScreen} */
function AbstractAggregatorPageCircuitScreenClass(){}

export default AbstractAggregatorPageCircuitScreen


AbstractAggregatorPageCircuitScreen[$implementations]=[
 __AbstractAggregatorPageCircuitScreen,
 AbstractAggregatorPageCircuitScreenClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitScreen}*/({
  inputsPQs:AggregatorPageCircuitInputsPQs,
  queriesPQs:AggregatorPageCircuitQueriesPQs,
  memoryQPs:AggregatorPageCircuitMemoryQPs,
 }),
 Screen,
 AbstractAggregatorPageCircuitScreenAR,
 AddressBar,
 AbstractAggregatorPageCircuitScreenClass.prototype=/**@type {!xyz.swapee.wc.IAggregatorPageCircuitScreen}*/({
  vdusPQs:AggregatorPageCircuitVdusPQs,
 }),
]