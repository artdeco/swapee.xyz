import {makeBuffers} from '@webcircuits/webcircuits'

export const AggregatorPageCircuitBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  type:String,
  amountFrom:String,
  cryptoIn:String,
  cryptoOut:String,
  fixedRate:Boolean,
  floatRate:Boolean,
  anyRate:Boolean,
 }),
})

export default AggregatorPageCircuitBuffer