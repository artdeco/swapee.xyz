
/**@this {xyz.swapee.wc.IAggregatorPageCircuitComputer}*/
export function preadaptIntent(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Form}*/
 const _inputs={
  currencyFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['96c88']:void 0,
  currencyTo:this.land.ExchangeIntent?this.land.ExchangeIntent.model['c23cd']:void 0,
  amountFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['748e6']:void 0,
  fixed:this.land.ExchangeIntent?this.land.ExchangeIntent.model['cec31']:void 0,
  float:this.land.ExchangeIntent?this.land.ExchangeIntent.model['546ad']:void 0,
  any:this.land.ExchangeIntent?this.land.ExchangeIntent.model['100b8']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.currencyFrom)) return
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.amountFrom=changes['748e6']
 changes.fixed=changes['cec31']
 changes.float=changes['546ad']
 changes.any=changes['100b8']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptIntent(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IAggregatorPageCircuitComputer}*/
export function preadaptType(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Form}*/
 const _inputs={
  type:inputs.type,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptType(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IAggregatorPageCircuitComputer}*/
export function preadaptInterlocks(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Form}*/
 const _inputs={
  fixedRate:inputs.fixedRate,
  floatRate:inputs.floatRate,
  anyRate:inputs.anyRate,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptInterlocks(__inputs,__changes)
 return RET
}