import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractAggregatorPageCircuitComputer}
 */
function __AbstractAggregatorPageCircuitComputer() {}
__AbstractAggregatorPageCircuitComputer.prototype = /** @type {!_AbstractAggregatorPageCircuitComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitComputer}
 */
class _AbstractAggregatorPageCircuitComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitComputer} ‎
 */
export class AbstractAggregatorPageCircuitComputer extends newAbstract(
 _AbstractAggregatorPageCircuitComputer,34948615601,null,{
  asIAggregatorPageCircuitComputer:1,
  superAggregatorPageCircuitComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitComputer} */
AbstractAggregatorPageCircuitComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitComputer} */
function AbstractAggregatorPageCircuitComputerClass(){}


AbstractAggregatorPageCircuitComputer[$implementations]=[
 __AbstractAggregatorPageCircuitComputer,
 Adapter,
]


export default AbstractAggregatorPageCircuitComputer