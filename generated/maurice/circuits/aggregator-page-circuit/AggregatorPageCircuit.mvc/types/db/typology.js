/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IAggregatorPageCircuitComputer': {
  'id': 34948615601,
  'symbols': {},
  'methods': {
   'adaptIntent': 1,
   'adaptType': 2,
   'compute': 3,
   'adaptInterlocks': 4
  }
 },
 'xyz.swapee.wc.AggregatorPageCircuitMemoryPQs': {
  'id': 34948615602,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitOuterCore': {
  'id': 34948615603,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.AggregatorPageCircuitInputsPQs': {
  'id': 34948615604,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitPort': {
  'id': 34948615605,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetAggregatorPageCircuitPort': 2
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitPortInterface': {
  'id': 34948615606,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitCore': {
  'id': 34948615607,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetAggregatorPageCircuitCore': 2
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitProcessor': {
  'id': 34948615608,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuit': {
  'id': 34948615609,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitBuffer': {
  'id': 349486156010,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil': {
  'id': 349486156011,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent': {
  'id': 349486156012,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitElement': {
  'id': 349486156013,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildExchangeIntent': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitElementPort': {
  'id': 349486156014,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitDesigner': {
  'id': 349486156015,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitGPU': {
  'id': 349486156016,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitDisplay': {
  'id': 349486156017,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.AggregatorPageCircuitQueriesPQs': {
  'id': 349486156018,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.AggregatorPageCircuitVdusPQs': {
  'id': 349486156019,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitDisplay': {
  'id': 349486156020,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitController': {
  'id': 349486156021,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setAmountFrom': 2,
   'unsetAmountFrom': 3,
   'setCryptoIn': 4,
   'unsetCryptoIn': 5,
   'setCryptoOut': 6,
   'unsetCryptoOut': 7,
   'flipFixedRate': 8,
   'setFixedRate': 9,
   'unsetFixedRate': 10,
   'flipFloatRate': 11,
   'setFloatRate': 12,
   'unsetFloatRate': 13,
   'flipAnyRate': 14,
   'setAnyRate': 15,
   'unsetAnyRate': 16
  }
 },
 'xyz.swapee.wc.front.IAggregatorPageCircuitController': {
  'id': 349486156022,
  'symbols': {},
  'methods': {
   'setAmountFrom': 1,
   'unsetAmountFrom': 2,
   'setCryptoIn': 3,
   'unsetCryptoIn': 4,
   'setCryptoOut': 5,
   'unsetCryptoOut': 6,
   'flipFixedRate': 7,
   'setFixedRate': 8,
   'unsetFixedRate': 9,
   'flipFloatRate': 10,
   'setFloatRate': 11,
   'unsetFloatRate': 12,
   'flipAnyRate': 13,
   'setAnyRate': 14,
   'unsetAnyRate': 15
  }
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitController': {
  'id': 349486156023,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR': {
  'id': 349486156024,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT': {
  'id': 349486156025,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitScreen': {
  'id': 349486156026,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitScreen': {
  'id': 349486156027,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR': {
  'id': 349486156028,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT': {
  'id': 349486156029,
  'symbols': {},
  'methods': {}
 }
})