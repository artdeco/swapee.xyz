/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IAggregatorPageCircuitComputer={}
xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent={}
xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType={}
xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks={}
xyz.swapee.wc.IAggregatorPageCircuitComputer.compute={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.Type={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AmountFrom={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoIn={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoOut={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FixedRate={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FloatRate={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AnyRate={}
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel={}
xyz.swapee.wc.IAggregatorPageCircuitPort={}
xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs={}
xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs={}
xyz.swapee.wc.IAggregatorPageCircuitCore={}
xyz.swapee.wc.IAggregatorPageCircuitCore.Model={}
xyz.swapee.wc.IAggregatorPageCircuitPortInterface={}
xyz.swapee.wc.IAggregatorPageCircuitProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IAggregatorPageCircuitController={}
xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT={}
xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR={}
xyz.swapee.wc.IAggregatorPageCircuit={}
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil={}
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent={}
xyz.swapee.wc.IAggregatorPageCircuitElement={}
xyz.swapee.wc.IAggregatorPageCircuitElement.build={}
xyz.swapee.wc.IAggregatorPageCircuitElement.short={}
xyz.swapee.wc.IAggregatorPageCircuitElementPort={}
xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs={}
xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts={}
xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs={}
xyz.swapee.wc.IAggregatorPageCircuitDesigner={}
xyz.swapee.wc.IAggregatorPageCircuitDesigner.communicator={}
xyz.swapee.wc.IAggregatorPageCircuitDesigner.relay={}
xyz.swapee.wc.IAggregatorPageCircuitDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IAggregatorPageCircuitDisplay={}
xyz.swapee.wc.back.IAggregatorPageCircuitController={}
xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR={}
xyz.swapee.wc.back.IAggregatorPageCircuitScreen={}
xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT={}
xyz.swapee.wc.IAggregatorPageCircuitController={}
xyz.swapee.wc.IAggregatorPageCircuitScreen={}
xyz.swapee.wc.IAggregatorPageCircuitGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/02-IAggregatorPageCircuitComputer.xml}  824b1d6d69ddfcae898baf2b53e2dd4f */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitComputer)} xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitComputer} xyz.swapee.wc.AggregatorPageCircuitComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitComputer` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitComputer
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.constructor&xyz.swapee.wc.AggregatorPageCircuitComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitComputer}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitComputer}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitComputer}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitComputer}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitComputer} xyz.swapee.wc.AggregatorPageCircuitComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.AggregatorPageCircuitMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.AggregatorPageCircuitLand>)} xyz.swapee.wc.IAggregatorPageCircuitComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitComputer
 */
xyz.swapee.wc.IAggregatorPageCircuitComputer = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent} */
xyz.swapee.wc.IAggregatorPageCircuitComputer.prototype.adaptIntent = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType} */
xyz.swapee.wc.IAggregatorPageCircuitComputer.prototype.adaptType = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks} */
xyz.swapee.wc.IAggregatorPageCircuitComputer.prototype.adaptInterlocks = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer.compute} */
xyz.swapee.wc.IAggregatorPageCircuitComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitComputer} xyz.swapee.wc.IAggregatorPageCircuitComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitComputer_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitComputer
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitComputer = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitComputer.constructor&xyz.swapee.wc.IAggregatorPageCircuitComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitComputer}
 */
xyz.swapee.wc.AggregatorPageCircuitComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitComputer} */
xyz.swapee.wc.RecordIAggregatorPageCircuitComputer

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitComputer} xyz.swapee.wc.BoundIAggregatorPageCircuitComputer */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitComputer} xyz.swapee.wc.BoundAggregatorPageCircuitComputer */

/**
 * Contains getters to cast the _IAggregatorPageCircuitComputer_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitComputerCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitComputerCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitComputer_ instance into the _BoundIAggregatorPageCircuitComputer_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitComputer}
 */
xyz.swapee.wc.IAggregatorPageCircuitComputerCaster.prototype.asIAggregatorPageCircuitComputer
/**
 * Access the _AggregatorPageCircuitComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitComputer}
 */
xyz.swapee.wc.IAggregatorPageCircuitComputerCaster.prototype.superAggregatorPageCircuitComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Form, changes: xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Form) => (void|xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Return)} xyz.swapee.wc.IAggregatorPageCircuitComputer.__adaptIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitComputer.__adaptIntent<!xyz.swapee.wc.IAggregatorPageCircuitComputer>} xyz.swapee.wc.IAggregatorPageCircuitComputer._adaptIntent */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent} */
/**
 * From the intent, reads the data.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Form} form The form with inputs.
 * @param {xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Return} The form with outputs.
 */
xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.Any_Safe} xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore.Model.Type&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.CryptoIn&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.CryptoOut&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AmountFrom} xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptIntent.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Form, changes: xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Form) => (void|xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Return)} xyz.swapee.wc.IAggregatorPageCircuitComputer.__adaptType
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitComputer.__adaptType<!xyz.swapee.wc.IAggregatorPageCircuitComputer>} xyz.swapee.wc.IAggregatorPageCircuitComputer._adaptType */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType} */
/**
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Form} form The form with inputs.
 * - `type` _string_ ⤴ *IAggregatorPageCircuitOuterCore.Model.Type_Safe*
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_
 * @param {xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Form} changes The previous values of the form.
 * - `type` _string_ ⤴ *IAggregatorPageCircuitOuterCore.Model.Type_Safe*
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_
 * @return {void|xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Return} The form with outputs.
 */
xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore.Model.Type_Safe} xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FixedRate&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FloatRate&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AnyRate} xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptType.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Form, changes: xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Form) => (void|xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Return)} xyz.swapee.wc.IAggregatorPageCircuitComputer.__adaptInterlocks
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitComputer.__adaptInterlocks<!xyz.swapee.wc.IAggregatorPageCircuitComputer>} xyz.swapee.wc.IAggregatorPageCircuitComputer._adaptInterlocks */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks} */
/**
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Form} form The form with inputs.
 * - `fixedRate` _boolean_ ⤴ *IAggregatorPageCircuitOuterCore.Model.FixedRate_Safe*
 * - `floatRate` _boolean_ ⤴ *IAggregatorPageCircuitOuterCore.Model.FloatRate_Safe*
 * - `anyRate` _boolean_ ⤴ *IAggregatorPageCircuitOuterCore.Model.AnyRate_Safe*
 * @param {xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Form} changes The previous values of the form.
 * - `fixedRate` _boolean_ ⤴ *IAggregatorPageCircuitOuterCore.Model.FixedRate_Safe*
 * - `floatRate` _boolean_ ⤴ *IAggregatorPageCircuitOuterCore.Model.FloatRate_Safe*
 * - `anyRate` _boolean_ ⤴ *IAggregatorPageCircuitOuterCore.Model.AnyRate_Safe*
 * @return {void|xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Return} The form with outputs.
 */
xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FixedRate_Safe&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FloatRate_Safe&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AnyRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FixedRate&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FloatRate&xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AnyRate} xyz.swapee.wc.IAggregatorPageCircuitComputer.adaptInterlocks.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.AggregatorPageCircuitMemory, land: !xyz.swapee.wc.IAggregatorPageCircuitComputer.compute.Land) => void} xyz.swapee.wc.IAggregatorPageCircuitComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitComputer.__compute<!xyz.swapee.wc.IAggregatorPageCircuitComputer>} xyz.swapee.wc.IAggregatorPageCircuitComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.AggregatorPageCircuitMemory} mem The memory.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitComputer.compute.Land} land The land.
 * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IAggregatorPageCircuitComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/03-IAggregatorPageCircuitOuterCore.xml}  fc69040f9c7af2a2ac9d002d31ceafbf */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitOuterCore)} xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore} xyz.swapee.wc.AggregatorPageCircuitOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.constructor&xyz.swapee.wc.AggregatorPageCircuitOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitOuterCore|typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitOuterCore|typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitOuterCore|typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitOuterCoreCaster)} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IAggregatorPageCircuit_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitOuterCore
 */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitOuterCore

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitOuterCore} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitOuterCore_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitOuterCore
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitOuterCore} The _IAggregatorPageCircuit_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitOuterCore = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitOuterCore.constructor&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.AggregatorPageCircuitOuterCore.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore}
 */
xyz.swapee.wc.AggregatorPageCircuitOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitOuterCore.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitOuterCoreFields
 */
xyz.swapee.wc.IAggregatorPageCircuitOuterCoreFields = class { }
/**
 * The _IAggregatorPageCircuit_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IAggregatorPageCircuitOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore} */
xyz.swapee.wc.RecordIAggregatorPageCircuitOuterCore

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore} xyz.swapee.wc.BoundIAggregatorPageCircuitOuterCore */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitOuterCore} xyz.swapee.wc.BoundAggregatorPageCircuitOuterCore */

/** @typedef {string} */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.Type.type

/** @typedef {string} */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AmountFrom.amountFrom

/** @typedef {string} */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoIn.cryptoIn

/** @typedef {string} */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoOut.cryptoOut

/** @typedef {boolean} */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FixedRate.fixedRate

/** @typedef {boolean} */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FloatRate.floatRate

/** @typedef {boolean} */
xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AnyRate.anyRate

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.Type&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AmountFrom&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoIn&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoOut&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FixedRate&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FloatRate&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AnyRate} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model The _IAggregatorPageCircuit_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.Type&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FixedRate&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FloatRate&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AnyRate} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel The _IAggregatorPageCircuit_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IAggregatorPageCircuitOuterCore_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitOuterCoreCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitOuterCoreCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitOuterCore_ instance into the _BoundIAggregatorPageCircuitOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitOuterCore}
 */
xyz.swapee.wc.IAggregatorPageCircuitOuterCoreCaster.prototype.asIAggregatorPageCircuitOuterCore
/**
 * Access the _AggregatorPageCircuitOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitOuterCore}
 */
xyz.swapee.wc.IAggregatorPageCircuitOuterCoreCaster.prototype.superAggregatorPageCircuitOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.Type  (optional overlay).
 * @prop {'fixed'|'float'|'any'} [type="any"]
 * Can be either:
 * - _fixed_
 * - _float_
 * - _any_
 *
 * Default `any`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.Type_Safe  (required overlay).
 * @prop {'fixed'|'float'|'any'} type
 * Can be either:
 * - _fixed_
 * - _float_
 * - _any_
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AmountFrom  (optional overlay).
 * @prop {string} [amountFrom="0.1"] Default `0.1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AmountFrom_Safe  (required overlay).
 * @prop {string} amountFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoIn  (optional overlay).
 * @prop {string} [cryptoIn="BTC"] Default `BTC`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoIn_Safe  (required overlay).
 * @prop {string} cryptoIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoOut  (optional overlay).
 * @prop {string} [cryptoOut="ETH"] Default `ETH`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoOut_Safe  (required overlay).
 * @prop {string} cryptoOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FixedRate  (optional overlay).
 * @prop {boolean} [fixedRate=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FixedRate_Safe  (required overlay).
 * @prop {boolean} fixedRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FloatRate  (optional overlay).
 * @prop {boolean} [floatRate=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FloatRate_Safe  (required overlay).
 * @prop {boolean} floatRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AnyRate  (optional overlay).
 * @prop {boolean} [anyRate=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AnyRate_Safe  (required overlay).
 * @prop {boolean} anyRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.Type  (optional overlay).
 * @prop {'fixed'|'float'|'any'} [type="any"]
 * Can be either:
 * - _fixed_
 * - _float_
 * - _any_
 *
 * Default `any`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.Type_Safe  (required overlay).
 * @prop {'fixed'|'float'|'any'} type
 * Can be either:
 * - _fixed_
 * - _float_
 * - _any_
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom  (optional overlay).
 * @prop {*} [amountFrom="0.1"] Default `0.1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom_Safe  (required overlay).
 * @prop {*} amountFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn  (optional overlay).
 * @prop {*} [cryptoIn="BTC"] Default `BTC`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn_Safe  (required overlay).
 * @prop {*} cryptoIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut  (optional overlay).
 * @prop {*} [cryptoOut="ETH"] Default `ETH`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut_Safe  (required overlay).
 * @prop {*} cryptoOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FixedRate  (optional overlay).
 * @prop {*} [fixedRate=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FixedRate_Safe  (required overlay).
 * @prop {*} fixedRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FloatRate  (optional overlay).
 * @prop {*} [floatRate=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FloatRate_Safe  (required overlay).
 * @prop {*} floatRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AnyRate  (optional overlay).
 * @prop {*} [anyRate=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AnyRate_Safe  (required overlay).
 * @prop {*} anyRate
 */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.Type} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.Type  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.Type_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.Type_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.CryptoIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.CryptoIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.CryptoOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.CryptoOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FixedRate} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.FixedRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FixedRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.FixedRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FloatRate} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.FloatRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FloatRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.FloatRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AnyRate} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.AnyRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AnyRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.AnyRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.Type} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.Type  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.Type_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.Type_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.CryptoIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.CryptoIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.CryptoOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.CryptoOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FixedRate} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.FixedRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FixedRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.FixedRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FloatRate} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.FloatRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.FloatRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.FloatRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AnyRate} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.AnyRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.AnyRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.AnyRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.Type} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.Type  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.Type_Safe} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.Type_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AmountFrom} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AmountFrom_Safe} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoIn} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.CryptoIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoIn_Safe} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.CryptoIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoOut} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.CryptoOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.CryptoOut_Safe} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.CryptoOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FixedRate} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FixedRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FixedRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FixedRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FloatRate} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FloatRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.FloatRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.FloatRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AnyRate} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AnyRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model.AnyRate_Safe} xyz.swapee.wc.IAggregatorPageCircuitCore.Model.AnyRate_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/04-IAggregatorPageCircuitPort.xml}  6db4819466219196d946ae8dd3a965f1 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IAggregatorPageCircuitPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitPort)} xyz.swapee.wc.AbstractAggregatorPageCircuitPort.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitPort} xyz.swapee.wc.AggregatorPageCircuitPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitPort` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitPort
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitPort = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitPort.constructor&xyz.swapee.wc.AggregatorPageCircuitPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitPort.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitPort.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitPort|typeof xyz.swapee.wc.AggregatorPageCircuitPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitPort|typeof xyz.swapee.wc.AggregatorPageCircuitPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitPort|typeof xyz.swapee.wc.AggregatorPageCircuitPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitPort.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitPort} xyz.swapee.wc.AggregatorPageCircuitPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitPortFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs>)} xyz.swapee.wc.IAggregatorPageCircuitPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IAggregatorPageCircuit_, providing input
 * pins.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitPort
 */
xyz.swapee.wc.IAggregatorPageCircuitPort = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitPort.resetPort} */
xyz.swapee.wc.IAggregatorPageCircuitPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitPort.resetAggregatorPageCircuitPort} */
xyz.swapee.wc.IAggregatorPageCircuitPort.prototype.resetAggregatorPageCircuitPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitPort&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitPort.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitPort} xyz.swapee.wc.IAggregatorPageCircuitPort.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitPort_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitPort
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitPort} The port that serves as an interface to the _IAggregatorPageCircuit_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitPort.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitPort = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitPort.constructor&xyz.swapee.wc.IAggregatorPageCircuitPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitPort}
 */
xyz.swapee.wc.AggregatorPageCircuitPort.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitPort.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitPortFields
 */
xyz.swapee.wc.IAggregatorPageCircuitPortFields = class { }
/**
 * The inputs to the _IAggregatorPageCircuit_'s controller via its port.
 */
xyz.swapee.wc.IAggregatorPageCircuitPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IAggregatorPageCircuitPortFields.prototype.props = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitPort} */
xyz.swapee.wc.RecordIAggregatorPageCircuitPort

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitPort} xyz.swapee.wc.BoundIAggregatorPageCircuitPort */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitPort} xyz.swapee.wc.BoundAggregatorPageCircuitPort */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel)} xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel} xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IAggregatorPageCircuit_'s controller via its port.
 * @record xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs
 */
xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.constructor&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel)} xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.constructor */
/**
 * The inputs to the _IAggregatorPageCircuit_'s controller via its port.
 * @record xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs
 */
xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.constructor&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitPortInterface
 */
xyz.swapee.wc.IAggregatorPageCircuitPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IAggregatorPageCircuitPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IAggregatorPageCircuitPortInterface.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitPortInterface

/**
 * A concrete class of _IAggregatorPageCircuitPortInterface_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitPortInterface
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitPortInterface} The port interface.
 */
xyz.swapee.wc.AggregatorPageCircuitPortInterface = class extends xyz.swapee.wc.IAggregatorPageCircuitPortInterface { }
xyz.swapee.wc.AggregatorPageCircuitPortInterface.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitPortInterface.Props
 * @prop {boolean} fixedRate
 * @prop {boolean} floatRate
 * @prop {boolean} anyRate
 * @prop {'fixed'|'float'|'any'} [type="any"]
 * Can be either:
 * - _fixed_
 * - _float_
 * - _any_
 *
 * Default `any`.
 * @prop {string} [amountFrom="0.1"] Default `0.1`.
 * @prop {string} [cryptoIn="BTC"] Default `BTC`.
 * @prop {string} [cryptoOut="ETH"] Default `ETH`.
 */

/**
 * Contains getters to cast the _IAggregatorPageCircuitPort_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitPortCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitPortCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitPort_ instance into the _BoundIAggregatorPageCircuitPort_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitPort}
 */
xyz.swapee.wc.IAggregatorPageCircuitPortCaster.prototype.asIAggregatorPageCircuitPort
/**
 * Access the _AggregatorPageCircuitPort_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitPort}
 */
xyz.swapee.wc.IAggregatorPageCircuitPortCaster.prototype.superAggregatorPageCircuitPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitPort.__resetPort<!xyz.swapee.wc.IAggregatorPageCircuitPort>} xyz.swapee.wc.IAggregatorPageCircuitPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitPort.resetPort} */
/**
 * Resets the _IAggregatorPageCircuit_ port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitPort.__resetAggregatorPageCircuitPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitPort.__resetAggregatorPageCircuitPort<!xyz.swapee.wc.IAggregatorPageCircuitPort>} xyz.swapee.wc.IAggregatorPageCircuitPort._resetAggregatorPageCircuitPort */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitPort.resetAggregatorPageCircuitPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitPort.resetAggregatorPageCircuitPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IAggregatorPageCircuitPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/09-IAggregatorPageCircuitCore.xml}  1350bf9acc12ff555895c79b6fa7e725 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IAggregatorPageCircuitCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitCore)} xyz.swapee.wc.AbstractAggregatorPageCircuitCore.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitCore} xyz.swapee.wc.AggregatorPageCircuitCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitCore` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitCore
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitCore = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitCore.constructor&xyz.swapee.wc.AggregatorPageCircuitCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitCore.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitCore.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitCore|typeof xyz.swapee.wc.AggregatorPageCircuitCore)|(!xyz.swapee.wc.IAggregatorPageCircuitOuterCore|typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitCore|typeof xyz.swapee.wc.AggregatorPageCircuitCore)|(!xyz.swapee.wc.IAggregatorPageCircuitOuterCore|typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitCore|typeof xyz.swapee.wc.AggregatorPageCircuitCore)|(!xyz.swapee.wc.IAggregatorPageCircuitOuterCore|typeof xyz.swapee.wc.AggregatorPageCircuitOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitCore}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitCoreCaster&xyz.swapee.wc.IAggregatorPageCircuitOuterCore)} xyz.swapee.wc.IAggregatorPageCircuitCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitCore
 */
xyz.swapee.wc.IAggregatorPageCircuitCore = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IAggregatorPageCircuitOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IAggregatorPageCircuitCore.resetCore} */
xyz.swapee.wc.IAggregatorPageCircuitCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitCore.resetAggregatorPageCircuitCore} */
xyz.swapee.wc.IAggregatorPageCircuitCore.prototype.resetAggregatorPageCircuitCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitCore&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitCore.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitCore} xyz.swapee.wc.IAggregatorPageCircuitCore.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitCore_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitCore
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitCore.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitCore = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitCore.constructor&xyz.swapee.wc.IAggregatorPageCircuitCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.AggregatorPageCircuitCore.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitCore}
 */
xyz.swapee.wc.AggregatorPageCircuitCore.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitCore.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitCoreFields
 */
xyz.swapee.wc.IAggregatorPageCircuitCoreFields = class { }
/**
 * The _IAggregatorPageCircuit_'s memory.
 */
xyz.swapee.wc.IAggregatorPageCircuitCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IAggregatorPageCircuitCoreFields.prototype.props = /** @type {xyz.swapee.wc.IAggregatorPageCircuitCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore} */
xyz.swapee.wc.RecordIAggregatorPageCircuitCore

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore} xyz.swapee.wc.BoundIAggregatorPageCircuitCore */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitCore} xyz.swapee.wc.BoundAggregatorPageCircuitCore */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model} xyz.swapee.wc.IAggregatorPageCircuitCore.Model The _IAggregatorPageCircuit_'s memory. */

/**
 * Contains getters to cast the _IAggregatorPageCircuitCore_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitCoreCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitCoreCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitCore_ instance into the _BoundIAggregatorPageCircuitCore_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitCore}
 */
xyz.swapee.wc.IAggregatorPageCircuitCoreCaster.prototype.asIAggregatorPageCircuitCore
/**
 * Access the _AggregatorPageCircuitCore_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitCore}
 */
xyz.swapee.wc.IAggregatorPageCircuitCoreCaster.prototype.superAggregatorPageCircuitCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore.__resetCore<!xyz.swapee.wc.IAggregatorPageCircuitCore>} xyz.swapee.wc.IAggregatorPageCircuitCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitCore.resetCore} */
/**
 * Resets the _IAggregatorPageCircuit_ core.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitCore.__resetAggregatorPageCircuitCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitCore.__resetAggregatorPageCircuitCore<!xyz.swapee.wc.IAggregatorPageCircuitCore>} xyz.swapee.wc.IAggregatorPageCircuitCore._resetAggregatorPageCircuitCore */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitCore.resetAggregatorPageCircuitCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitCore.resetAggregatorPageCircuitCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IAggregatorPageCircuitCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/10-IAggregatorPageCircuitProcessor.xml}  9cf233560b18c69375a59b90d1c58b7b */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese&xyz.swapee.wc.IAggregatorPageCircuitController.Initialese} xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitProcessor)} xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitProcessor} xyz.swapee.wc.AggregatorPageCircuitProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.constructor&xyz.swapee.wc.AggregatorPageCircuitProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!xyz.swapee.wc.IAggregatorPageCircuitCore|typeof xyz.swapee.wc.AggregatorPageCircuitCore)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!xyz.swapee.wc.IAggregatorPageCircuitCore|typeof xyz.swapee.wc.AggregatorPageCircuitCore)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!xyz.swapee.wc.IAggregatorPageCircuitCore|typeof xyz.swapee.wc.AggregatorPageCircuitCore)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitProcessor} xyz.swapee.wc.AggregatorPageCircuitProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitProcessorCaster&xyz.swapee.wc.IAggregatorPageCircuitComputer&xyz.swapee.wc.IAggregatorPageCircuitCore&xyz.swapee.wc.IAggregatorPageCircuitController)} xyz.swapee.wc.IAggregatorPageCircuitProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController} xyz.swapee.wc.IAggregatorPageCircuitController.typeof */
/**
 * The processor to compute changes to the memory for the _IAggregatorPageCircuit_.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitProcessor
 */
xyz.swapee.wc.IAggregatorPageCircuitProcessor = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IAggregatorPageCircuitComputer.typeof&xyz.swapee.wc.IAggregatorPageCircuitCore.typeof&xyz.swapee.wc.IAggregatorPageCircuitController.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitProcessor} xyz.swapee.wc.IAggregatorPageCircuitProcessor.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitProcessor_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitProcessor
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitProcessor} The processor to compute changes to the memory for the _IAggregatorPageCircuit_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitProcessor = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitProcessor.constructor&xyz.swapee.wc.IAggregatorPageCircuitProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.AggregatorPageCircuitProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitProcessor} */
xyz.swapee.wc.RecordIAggregatorPageCircuitProcessor

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitProcessor} xyz.swapee.wc.BoundIAggregatorPageCircuitProcessor */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitProcessor} xyz.swapee.wc.BoundAggregatorPageCircuitProcessor */

/**
 * Contains getters to cast the _IAggregatorPageCircuitProcessor_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitProcessorCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitProcessorCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitProcessor_ instance into the _BoundIAggregatorPageCircuitProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.IAggregatorPageCircuitProcessorCaster.prototype.asIAggregatorPageCircuitProcessor
/**
 * Access the _AggregatorPageCircuitProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.IAggregatorPageCircuitProcessorCaster.prototype.superAggregatorPageCircuitProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/100-AggregatorPageCircuitMemory.xml}  9fa795f3c7571e158ff3fe1c87fab868 */
/**
 * The memory of the _IAggregatorPageCircuit_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.AggregatorPageCircuitMemory
 */
xyz.swapee.wc.AggregatorPageCircuitMemory = class { }
/**
 * Default empty string.
 */
xyz.swapee.wc.AggregatorPageCircuitMemory.prototype.type = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.AggregatorPageCircuitMemory.prototype.amountFrom = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.AggregatorPageCircuitMemory.prototype.cryptoIn = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.AggregatorPageCircuitMemory.prototype.cryptoOut = /** @type {string} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.AggregatorPageCircuitMemory.prototype.fixedRate = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.AggregatorPageCircuitMemory.prototype.floatRate = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.AggregatorPageCircuitMemory.prototype.anyRate = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/102-AggregatorPageCircuitInputs.xml}  6bf7048503076db1f8f8022efe1d487c */
/**
 * The inputs of the _IAggregatorPageCircuit_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.AggregatorPageCircuitInputs
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs = class { }
/**
 * Default `any`.
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs.prototype.type = /** @type {string|undefined} */ (void 0)
/**
 * Default `0.1`.
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs.prototype.amountFrom = /** @type {string|undefined} */ (void 0)
/**
 * Default `BTC`.
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs.prototype.cryptoIn = /** @type {string|undefined} */ (void 0)
/**
 * Default `ETH`.
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs.prototype.cryptoOut = /** @type {string|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs.prototype.fixedRate = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs.prototype.floatRate = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.AggregatorPageCircuitInputs.prototype.anyRate = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/11-IAggregatorPageCircuit.xml}  9033e9706c4c19101d7be9cec245eaa2 */
/**
 * An atomic wrapper for the _IAggregatorPageCircuit_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.AggregatorPageCircuitEnv
 */
xyz.swapee.wc.AggregatorPageCircuitEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.AggregatorPageCircuitEnv.prototype.aggregatorPageCircuit = /** @type {xyz.swapee.wc.IAggregatorPageCircuit} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.IAggregatorPageCircuitController.Inputs>&xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese&xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese&xyz.swapee.wc.IAggregatorPageCircuitController.Initialese} xyz.swapee.wc.IAggregatorPageCircuit.Initialese */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuit)} xyz.swapee.wc.AbstractAggregatorPageCircuit.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuit} xyz.swapee.wc.AggregatorPageCircuit.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuit` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuit
 */
xyz.swapee.wc.AbstractAggregatorPageCircuit = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuit.constructor&xyz.swapee.wc.AggregatorPageCircuit.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuit.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuit
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuit.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuit} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuit|typeof xyz.swapee.wc.AggregatorPageCircuit)|(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuit}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuit.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuit}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuit.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuit}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuit.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuit|typeof xyz.swapee.wc.AggregatorPageCircuit)|(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuit}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuit.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuit|typeof xyz.swapee.wc.AggregatorPageCircuit)|(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuit}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuit.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuit.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuit} xyz.swapee.wc.AggregatorPageCircuitConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuit.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IAggregatorPageCircuit.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IAggregatorPageCircuit.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IAggregatorPageCircuit.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.AggregatorPageCircuitMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.AggregatorPageCircuitClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitCaster&xyz.swapee.wc.IAggregatorPageCircuitProcessor&xyz.swapee.wc.IAggregatorPageCircuitComputer&xyz.swapee.wc.IAggregatorPageCircuitController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.IAggregatorPageCircuitController.Inputs, !xyz.swapee.wc.AggregatorPageCircuitLand>)} xyz.swapee.wc.IAggregatorPageCircuit.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IAggregatorPageCircuit
 */
xyz.swapee.wc.IAggregatorPageCircuit = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuit.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IAggregatorPageCircuitProcessor.typeof&xyz.swapee.wc.IAggregatorPageCircuitComputer.typeof&xyz.swapee.wc.IAggregatorPageCircuitController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuit* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuit.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuit.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuit&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuit.Initialese>)} xyz.swapee.wc.AggregatorPageCircuit.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuit} xyz.swapee.wc.IAggregatorPageCircuit.typeof */
/**
 * A concrete class of _IAggregatorPageCircuit_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuit
 * @implements {xyz.swapee.wc.IAggregatorPageCircuit} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuit.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuit = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuit.constructor&xyz.swapee.wc.IAggregatorPageCircuit.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuit* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuit.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuit* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuit.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuit.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuit}
 */
xyz.swapee.wc.AggregatorPageCircuit.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuit.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitFields
 */
xyz.swapee.wc.IAggregatorPageCircuitFields = class { }
/**
 * The input pins of the _IAggregatorPageCircuit_ port.
 */
xyz.swapee.wc.IAggregatorPageCircuitFields.prototype.pinout = /** @type {!xyz.swapee.wc.IAggregatorPageCircuit.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuit} */
xyz.swapee.wc.RecordIAggregatorPageCircuit

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuit} xyz.swapee.wc.BoundIAggregatorPageCircuit */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuit} xyz.swapee.wc.BoundAggregatorPageCircuit */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.Inputs} xyz.swapee.wc.IAggregatorPageCircuit.Pinout The input pins of the _IAggregatorPageCircuit_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs>)} xyz.swapee.wc.IAggregatorPageCircuitBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitBuffer
 */
xyz.swapee.wc.IAggregatorPageCircuitBuffer = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IAggregatorPageCircuitBuffer.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitBuffer

/**
 * A concrete class of _IAggregatorPageCircuitBuffer_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitBuffer
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.AggregatorPageCircuitBuffer = class extends xyz.swapee.wc.IAggregatorPageCircuitBuffer { }
xyz.swapee.wc.AggregatorPageCircuitBuffer.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitBuffer

/**
 * Contains getters to cast the _IAggregatorPageCircuit_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitCaster = class { }
/**
 * Cast the _IAggregatorPageCircuit_ instance into the _BoundIAggregatorPageCircuit_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuit}
 */
xyz.swapee.wc.IAggregatorPageCircuitCaster.prototype.asIAggregatorPageCircuit
/**
 * Access the _AggregatorPageCircuit_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuit}
 */
xyz.swapee.wc.IAggregatorPageCircuitCaster.prototype.superAggregatorPageCircuit

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/110-AggregatorPageCircuitSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.AggregatorPageCircuitMemoryPQs
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryPQs = class {
  constructor() {
    /**
     * `f99dc`
     */
    this.type=/** @type {string} */ (void 0)
    /**
     * `h48e6`
     */
    this.amountFrom=/** @type {string} */ (void 0)
    /**
     * `e212d`
     */
    this.cryptoIn=/** @type {string} */ (void 0)
    /**
     * `c60b5`
     */
    this.cryptoOut=/** @type {string} */ (void 0)
    /**
     * `hd0bd`
     */
    this.fixedRate=/** @type {string} */ (void 0)
    /**
     * `a8df4`
     */
    this.floatRate=/** @type {string} */ (void 0)
    /**
     * `g0e57`
     */
    this.anyRate=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.AggregatorPageCircuitMemoryPQs.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitMemoryQPs
 * @dict
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs = class { }
/**
 * `type`
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs.prototype.f99dc = /** @type {string} */ (void 0)
/**
 * `amountFrom`
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs.prototype.h48e6 = /** @type {string} */ (void 0)
/**
 * `cryptoIn`
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs.prototype.e212d = /** @type {string} */ (void 0)
/**
 * `cryptoOut`
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs.prototype.c60b5 = /** @type {string} */ (void 0)
/**
 * `fixedRate`
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs.prototype.hd0bd = /** @type {string} */ (void 0)
/**
 * `floatRate`
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs.prototype.a8df4 = /** @type {string} */ (void 0)
/**
 * `anyRate`
 */
xyz.swapee.wc.AggregatorPageCircuitMemoryQPs.prototype.g0e57 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitMemoryPQs)} xyz.swapee.wc.AggregatorPageCircuitInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitMemoryPQs} xyz.swapee.wc.AggregatorPageCircuitMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.AggregatorPageCircuitInputsPQs
 */
xyz.swapee.wc.AggregatorPageCircuitInputsPQs = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitInputsPQs.constructor&xyz.swapee.wc.AggregatorPageCircuitMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.AggregatorPageCircuitInputsPQs.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitInputsPQs

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitMemoryPQs)} xyz.swapee.wc.AggregatorPageCircuitInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitInputsQPs
 * @dict
 */
xyz.swapee.wc.AggregatorPageCircuitInputsQPs = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitInputsQPs.constructor&xyz.swapee.wc.AggregatorPageCircuitMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.AggregatorPageCircuitInputsQPs.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.AggregatorPageCircuitQueriesPQs
 */
xyz.swapee.wc.AggregatorPageCircuitQueriesPQs = class {
  constructor() {
    /**
     * `b3da4`
     */
    this.exchangeIntentSel=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.AggregatorPageCircuitQueriesPQs.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitQueriesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitQueriesQPs
 * @dict
 */
xyz.swapee.wc.AggregatorPageCircuitQueriesQPs = class { }
/**
 * `exchangeIntentSel`
 */
xyz.swapee.wc.AggregatorPageCircuitQueriesQPs.prototype.b3da4 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.AggregatorPageCircuitVdusPQs
 */
xyz.swapee.wc.AggregatorPageCircuitVdusPQs = class {
  constructor() {
    /**
     * `acdb1`
     */
    this.ExchangeIntent=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.AggregatorPageCircuitVdusPQs.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitVdusQPs
 * @dict
 */
xyz.swapee.wc.AggregatorPageCircuitVdusQPs = class { }
/**
 * `ExchangeIntent`
 */
xyz.swapee.wc.AggregatorPageCircuitVdusQPs.prototype.acdb1 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/12-IAggregatorPageCircuitHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtilFields)} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.router} */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IAggregatorPageCircuitHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitHtmlComponentUtil
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitHtmlComponentUtil = class extends xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil { }
xyz.swapee.wc.AggregatorPageCircuitHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitHtmlComponentUtil

/**
 * Fields of the IAggregatorPageCircuitHtmlComponentUtil.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtilFields
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil} */
xyz.swapee.wc.RecordIAggregatorPageCircuitHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil} xyz.swapee.wc.BoundIAggregatorPageCircuitHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitHtmlComponentUtil} xyz.swapee.wc.BoundAggregatorPageCircuitHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPort} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IAggregatorPageCircuitPort} AggregatorPageCircuit The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.AggregatorPageCircuitMemory} AggregatorPageCircuit
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} ExchangeIntent
 * @prop {!xyz.swapee.wc.IAggregatorPageCircuit.Pinout} AggregatorPageCircuit
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.__router<!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil>} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `ExchangeIntent` _typeof IExchangeIntentPort_
 * - `AggregatorPageCircuit` _typeof IAggregatorPageCircuitPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `ExchangeIntent` _!ExchangeIntentMemory_
 * - `AggregatorPageCircuit` _!AggregatorPageCircuitMemory_
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `ExchangeIntent` _!IExchangeIntent.Pinout_
 * - `AggregatorPageCircuit` _!IAggregatorPageCircuit.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/12-IAggregatorPageCircuitHtmlComponent.xml}  7fd7ba1ed02a90fed1177fdfd4e577f3 */
/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese&xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese&xyz.swapee.wc.IAggregatorPageCircuit.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IAggregatorPageCircuitProcessor.Initialese&xyz.swapee.wc.IAggregatorPageCircuitComputer.Initialese} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitHtmlComponent)} xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent} xyz.swapee.wc.AggregatorPageCircuitHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.constructor&xyz.swapee.wc.AggregatorPageCircuitHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent|typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent)|(!xyz.swapee.wc.back.IAggregatorPageCircuitController|typeof xyz.swapee.wc.back.AggregatorPageCircuitController)|(!xyz.swapee.wc.back.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen)|(!xyz.swapee.wc.IAggregatorPageCircuit|typeof xyz.swapee.wc.AggregatorPageCircuit)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IAggregatorPageCircuitGPU|typeof xyz.swapee.wc.AggregatorPageCircuitGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent|typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent)|(!xyz.swapee.wc.back.IAggregatorPageCircuitController|typeof xyz.swapee.wc.back.AggregatorPageCircuitController)|(!xyz.swapee.wc.back.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen)|(!xyz.swapee.wc.IAggregatorPageCircuit|typeof xyz.swapee.wc.AggregatorPageCircuit)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IAggregatorPageCircuitGPU|typeof xyz.swapee.wc.AggregatorPageCircuitGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent|typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent)|(!xyz.swapee.wc.back.IAggregatorPageCircuitController|typeof xyz.swapee.wc.back.AggregatorPageCircuitController)|(!xyz.swapee.wc.back.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen)|(!xyz.swapee.wc.IAggregatorPageCircuit|typeof xyz.swapee.wc.AggregatorPageCircuit)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IAggregatorPageCircuitGPU|typeof xyz.swapee.wc.AggregatorPageCircuitGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IAggregatorPageCircuitProcessor|typeof xyz.swapee.wc.AggregatorPageCircuitProcessor)|(!xyz.swapee.wc.IAggregatorPageCircuitComputer|typeof xyz.swapee.wc.AggregatorPageCircuitComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent} xyz.swapee.wc.AggregatorPageCircuitHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentCaster&xyz.swapee.wc.back.IAggregatorPageCircuitController&xyz.swapee.wc.back.IAggregatorPageCircuitScreen&xyz.swapee.wc.IAggregatorPageCircuit&com.webcircuits.ILanded<!xyz.swapee.wc.AggregatorPageCircuitLand>&xyz.swapee.wc.IAggregatorPageCircuitGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.IAggregatorPageCircuitController.Inputs, !HTMLDivElement, !xyz.swapee.wc.AggregatorPageCircuitLand>&xyz.swapee.wc.IAggregatorPageCircuitProcessor&xyz.swapee.wc.IAggregatorPageCircuitComputer)} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IAggregatorPageCircuitController} xyz.swapee.wc.back.IAggregatorPageCircuitController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IAggregatorPageCircuitScreen} xyz.swapee.wc.back.IAggregatorPageCircuitScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitGPU} xyz.swapee.wc.IAggregatorPageCircuitGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IAggregatorPageCircuit_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IAggregatorPageCircuitController.typeof&xyz.swapee.wc.back.IAggregatorPageCircuitScreen.typeof&xyz.swapee.wc.IAggregatorPageCircuit.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IAggregatorPageCircuitGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IAggregatorPageCircuitProcessor.typeof&xyz.swapee.wc.IAggregatorPageCircuitComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent} xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitHtmlComponent
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent} The _IAggregatorPageCircuit_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitHtmlComponent = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitHtmlComponent.constructor&xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitHtmlComponent}
 */
xyz.swapee.wc.AggregatorPageCircuitHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent} */
xyz.swapee.wc.RecordIAggregatorPageCircuitHtmlComponent

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent} xyz.swapee.wc.BoundIAggregatorPageCircuitHtmlComponent */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitHtmlComponent} xyz.swapee.wc.BoundAggregatorPageCircuitHtmlComponent */

/**
 * Contains getters to cast the _IAggregatorPageCircuitHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitHtmlComponent_ instance into the _BoundIAggregatorPageCircuitHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitHtmlComponent}
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentCaster.prototype.asIAggregatorPageCircuitHtmlComponent
/**
 * Access the _AggregatorPageCircuitHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitHtmlComponent}
 */
xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentCaster.prototype.superAggregatorPageCircuitHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/130-IAggregatorPageCircuitElement.xml}  2a21be54a3e3f36b26ba5b0efdb65456 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.AggregatorPageCircuitLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.IExchangeIntent>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IAggregatorPageCircuitElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitElement)} xyz.swapee.wc.AbstractAggregatorPageCircuitElement.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitElement} xyz.swapee.wc.AggregatorPageCircuitElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitElement` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitElement
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElement = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitElement.constructor&xyz.swapee.wc.AggregatorPageCircuitElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitElement.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElement.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitElement|typeof xyz.swapee.wc.AggregatorPageCircuitElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElement}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElement}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitElement|typeof xyz.swapee.wc.AggregatorPageCircuitElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElement}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitElement|typeof xyz.swapee.wc.AggregatorPageCircuitElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElement}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitElement.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitElement} xyz.swapee.wc.AggregatorPageCircuitElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitElementFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs, !xyz.swapee.wc.AggregatorPageCircuitLand>&com.webcircuits.ILanded<!xyz.swapee.wc.AggregatorPageCircuitLand>&guest.maurice.IMilleu<!xyz.swapee.wc.IExchangeIntent>)} xyz.swapee.wc.IAggregatorPageCircuitElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IAggregatorPageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitElement
 */
xyz.swapee.wc.IAggregatorPageCircuitElement = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.solder} */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.render} */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.build} */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.buildExchangeIntent} */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.buildExchangeIntent = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.short} */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.server} */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement.inducer} */
xyz.swapee.wc.IAggregatorPageCircuitElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitElement&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitElement.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement} xyz.swapee.wc.IAggregatorPageCircuitElement.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitElement_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitElement
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitElement} A component description.
 *
 * The _IAggregatorPageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitElement.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitElement = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitElement.constructor&xyz.swapee.wc.IAggregatorPageCircuitElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElement}
 */
xyz.swapee.wc.AggregatorPageCircuitElement.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitElement.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitElementFields
 */
xyz.swapee.wc.IAggregatorPageCircuitElementFields = class { }
/**
 * The element-specific inputs to the _IAggregatorPageCircuit_ component.
 */
xyz.swapee.wc.IAggregatorPageCircuitElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IAggregatorPageCircuitElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement} */
xyz.swapee.wc.RecordIAggregatorPageCircuitElement

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement} xyz.swapee.wc.BoundIAggregatorPageCircuitElement */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitElement} xyz.swapee.wc.BoundAggregatorPageCircuitElement */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs&xyz.swapee.wc.IAggregatorPageCircuitDisplay.Queries&xyz.swapee.wc.IAggregatorPageCircuitController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs} xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs The element-specific inputs to the _IAggregatorPageCircuit_ component. */

/**
 * Contains getters to cast the _IAggregatorPageCircuitElement_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitElementCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitElementCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitElement_ instance into the _BoundIAggregatorPageCircuitElement_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitElement}
 */
xyz.swapee.wc.IAggregatorPageCircuitElementCaster.prototype.asIAggregatorPageCircuitElement
/**
 * Access the _AggregatorPageCircuitElement_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitElement}
 */
xyz.swapee.wc.IAggregatorPageCircuitElementCaster.prototype.superAggregatorPageCircuitElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.AggregatorPageCircuitMemory, props: !xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs) => Object<string, *>} xyz.swapee.wc.IAggregatorPageCircuitElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement.__solder<!xyz.swapee.wc.IAggregatorPageCircuitElement>} xyz.swapee.wc.IAggregatorPageCircuitElement._solder */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitMemory} model The model.
 * - `type` _string_
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatRate` _boolean_ Default `false`.
 * - `anyRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs} props The element props.
 * - `[type="any"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.Type* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.Type*
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default `any`.
 * - `[amountFrom="0.1"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom* Default `0.1`.
 * - `[cryptoIn="BTC"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn* Default `BTC`.
 * - `[cryptoOut="ETH"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut* Default `ETH`.
 * - `[fixedRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FixedRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FixedRate* Default `null`.
 * - `[floatRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FloatRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FloatRate* Default `null`.
 * - `[anyRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AnyRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AnyRate* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IAggregatorPageCircuitDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IAggregatorPageCircuitElementPort.Inputs.NoSolder* Default `false`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.AggregatorPageCircuitMemory, instance?: !xyz.swapee.wc.IAggregatorPageCircuitScreen&xyz.swapee.wc.IAggregatorPageCircuitController) => !engineering.type.VNode} xyz.swapee.wc.IAggregatorPageCircuitElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement.__render<!xyz.swapee.wc.IAggregatorPageCircuitElement>} xyz.swapee.wc.IAggregatorPageCircuitElement._render */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitMemory} [model] The model for the view.
 * - `type` _string_
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatRate` _boolean_ Default `false`.
 * - `anyRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitScreen&xyz.swapee.wc.IAggregatorPageCircuitController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IAggregatorPageCircuitElement.build.Cores, instances: !xyz.swapee.wc.IAggregatorPageCircuitElement.build.Instances) => ?} xyz.swapee.wc.IAggregatorPageCircuitElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement.__build<!xyz.swapee.wc.IAggregatorPageCircuitElement>} xyz.swapee.wc.IAggregatorPageCircuitElement._build */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitElement.build.Cores} cores The models of components on the land.
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntentCore.Model_
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntent_
 * @return {?}
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.IExchangeIntent} ExchangeIntent
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeIntentCore.Model, instance: !xyz.swapee.wc.IExchangeIntent) => ?} xyz.swapee.wc.IAggregatorPageCircuitElement.__buildExchangeIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement.__buildExchangeIntent<!xyz.swapee.wc.IAggregatorPageCircuitElement>} xyz.swapee.wc.IAggregatorPageCircuitElement._buildExchangeIntent */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement.buildExchangeIntent} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeIntent_ component.
 * @param {!xyz.swapee.wc.IExchangeIntentCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeIntent} instance
 * @return {?}
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.buildExchangeIntent = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.AggregatorPageCircuitMemory, ports: !xyz.swapee.wc.IAggregatorPageCircuitElement.short.Ports, cores: !xyz.swapee.wc.IAggregatorPageCircuitElement.short.Cores) => ?} xyz.swapee.wc.IAggregatorPageCircuitElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement.__short<!xyz.swapee.wc.IAggregatorPageCircuitElement>} xyz.swapee.wc.IAggregatorPageCircuitElement._short */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitMemory} model The model from which to feed properties to peer's ports.
 * - `type` _string_
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatRate` _boolean_ Default `false`.
 * - `anyRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitElement.short.Ports} ports The ports of the peers.
 * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentPortInterface_
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitElement.short.Cores} cores The cores of the peers.
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntentCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPortInterface} ExchangeIntent
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.AggregatorPageCircuitMemory, inputs: !xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IAggregatorPageCircuitElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement.__server<!xyz.swapee.wc.IAggregatorPageCircuitElement>} xyz.swapee.wc.IAggregatorPageCircuitElement._server */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitMemory} memory The memory registers.
 * - `type` _string_
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatRate` _boolean_ Default `false`.
 * - `anyRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs} inputs The inputs to the port.
 * - `[type="any"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.Type* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.Type*
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default `any`.
 * - `[amountFrom="0.1"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom* Default `0.1`.
 * - `[cryptoIn="BTC"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn* Default `BTC`.
 * - `[cryptoOut="ETH"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut* Default `ETH`.
 * - `[fixedRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FixedRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FixedRate* Default `null`.
 * - `[floatRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FloatRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FloatRate* Default `null`.
 * - `[anyRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AnyRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AnyRate* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IAggregatorPageCircuitDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IAggregatorPageCircuitElementPort.Inputs.NoSolder* Default `false`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.AggregatorPageCircuitMemory, port?: !xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs) => ?} xyz.swapee.wc.IAggregatorPageCircuitElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElement.__inducer<!xyz.swapee.wc.IAggregatorPageCircuitElement>} xyz.swapee.wc.IAggregatorPageCircuitElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitMemory} [model] The model of the component into which to induce the state.
 * - `type` _string_
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatRate` _boolean_ Default `false`.
 * - `anyRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IAggregatorPageCircuitElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[type="any"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.Type* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.Type*
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default `any`.
 * - `[amountFrom="0.1"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AmountFrom* Default `0.1`.
 * - `[cryptoIn="BTC"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoIn* Default `BTC`.
 * - `[cryptoOut="ETH"]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.CryptoOut* Default `ETH`.
 * - `[fixedRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FixedRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FixedRate* Default `null`.
 * - `[floatRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FloatRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.FloatRate* Default `null`.
 * - `[anyRate=null]` _&#42;?_ ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AnyRate* ⤴ *IAggregatorPageCircuitOuterCore.WeakModel.AnyRate* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IAggregatorPageCircuitDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IAggregatorPageCircuitElementPort.Inputs.NoSolder* Default `false`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IAggregatorPageCircuitElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IAggregatorPageCircuitElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/140-IAggregatorPageCircuitElementPort.xml}  f2332326d142a665bb68622e43d5cf0c */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitElementPort)} xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitElementPort} xyz.swapee.wc.AggregatorPageCircuitElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.constructor&xyz.swapee.wc.AggregatorPageCircuitElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitElementPort|typeof xyz.swapee.wc.AggregatorPageCircuitElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElementPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitElementPort|typeof xyz.swapee.wc.AggregatorPageCircuitElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElementPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitElementPort|typeof xyz.swapee.wc.AggregatorPageCircuitElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElementPort}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitElementPort.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitElementPort} xyz.swapee.wc.AggregatorPageCircuitElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs>)} xyz.swapee.wc.IAggregatorPageCircuitElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitElementPort
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPort = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElementPort} xyz.swapee.wc.IAggregatorPageCircuitElementPort.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitElementPort_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitElementPort
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitElementPort = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitElementPort.constructor&xyz.swapee.wc.IAggregatorPageCircuitElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitElementPort}
 */
xyz.swapee.wc.AggregatorPageCircuitElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitElementPort.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitElementPortFields
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPortFields = class { }
/**
 * The inputs to the _IAggregatorPageCircuitElement_'s controller via its element port.
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElementPort} */
xyz.swapee.wc.RecordIAggregatorPageCircuitElementPort

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitElementPort} xyz.swapee.wc.BoundIAggregatorPageCircuitElementPort */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitElementPort} xyz.swapee.wc.BoundAggregatorPageCircuitElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _ExchangeIntent_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder&xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts)} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts.typeof */
/**
 * The inputs to the _IAggregatorPageCircuitElement_'s controller via its element port.
 * @record xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.constructor&xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.ExchangeIntentOpts)} xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.ExchangeIntentOpts} xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.ExchangeIntentOpts.typeof */
/**
 * The inputs to the _IAggregatorPageCircuitElement_'s controller via its element port.
 * @record xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.constructor&xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.ExchangeIntentOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs

/**
 * Contains getters to cast the _IAggregatorPageCircuitElementPort_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitElementPortCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPortCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitElementPort_ instance into the _BoundIAggregatorPageCircuitElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitElementPort}
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPortCaster.prototype.asIAggregatorPageCircuitElementPort
/**
 * Access the _AggregatorPageCircuitElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitElementPort}
 */
xyz.swapee.wc.IAggregatorPageCircuitElementPortCaster.prototype.superAggregatorPageCircuitElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {!Object} [exchangeIntentOpts] The options to pass to the _ExchangeIntent_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.Inputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {!Object} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {*} [exchangeIntentOpts=null] The options to pass to the _ExchangeIntent_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitElementPort.WeakInputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {*} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/170-IAggregatorPageCircuitDesigner.xml}  7f4bd3bb7ba7bc08c7dbce35caad471b */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitDesigner
 */
xyz.swapee.wc.IAggregatorPageCircuitDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.AggregatorPageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IAggregatorPageCircuit />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.AggregatorPageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IAggregatorPageCircuit />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.AggregatorPageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IAggregatorPageCircuitDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `AggregatorPageCircuit` _typeof IAggregatorPageCircuitController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IAggregatorPageCircuitDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `AggregatorPageCircuit` _typeof IAggregatorPageCircuitController_
   * - `This` _typeof IAggregatorPageCircuitController_
   * @param {!xyz.swapee.wc.IAggregatorPageCircuitDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_
   * - `AggregatorPageCircuit` _!AggregatorPageCircuitMemory_
   * - `This` _!AggregatorPageCircuitMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.AggregatorPageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.AggregatorPageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IAggregatorPageCircuitDesigner.prototype.constructor = xyz.swapee.wc.IAggregatorPageCircuitDesigner

/**
 * A concrete class of _IAggregatorPageCircuitDesigner_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitDesigner
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.AggregatorPageCircuitDesigner = class extends xyz.swapee.wc.IAggregatorPageCircuitDesigner { }
xyz.swapee.wc.AggregatorPageCircuitDesigner.prototype.constructor = xyz.swapee.wc.AggregatorPageCircuitDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IAggregatorPageCircuitController} AggregatorPageCircuit
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IAggregatorPageCircuitController} AggregatorPageCircuit
 * @prop {typeof xyz.swapee.wc.IAggregatorPageCircuitController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.AggregatorPageCircuitMemory} AggregatorPageCircuit
 * @prop {!xyz.swapee.wc.AggregatorPageCircuitMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/200-AggregatorPageCircuitLand.xml}  dca27d5b33b6cecf7e2977daf9db541d */
/**
 * The surrounding of the _IAggregatorPageCircuit_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.AggregatorPageCircuitLand
 */
xyz.swapee.wc.AggregatorPageCircuitLand = class { }
/**
 *
 */
xyz.swapee.wc.AggregatorPageCircuitLand.prototype.ExchangeIntent = /** @type {xyz.swapee.wc.IExchangeIntent} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/40-IAggregatorPageCircuitDisplay.xml}  8ba32e148f054b70f4d1fc36f812f550 */
/**
 * @typedef {Object} $xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese
 * @prop {HTMLElement} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 */
/** @typedef {$xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IAggregatorPageCircuitDisplay.Settings>} xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitDisplay)} xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitDisplay} xyz.swapee.wc.AggregatorPageCircuitDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.constructor&xyz.swapee.wc.AggregatorPageCircuitDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.AggregatorPageCircuitDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.AggregatorPageCircuitDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.AggregatorPageCircuitDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitDisplay} xyz.swapee.wc.AggregatorPageCircuitDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.AggregatorPageCircuitMemory, !HTMLDivElement, !xyz.swapee.wc.IAggregatorPageCircuitDisplay.Settings, xyz.swapee.wc.IAggregatorPageCircuitDisplay.Queries, null>)} xyz.swapee.wc.IAggregatorPageCircuitDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IAggregatorPageCircuit_.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitDisplay
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplay = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitDisplay.paint} */
xyz.swapee.wc.IAggregatorPageCircuitDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitDisplay} xyz.swapee.wc.IAggregatorPageCircuitDisplay.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitDisplay_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitDisplay
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitDisplay} Display for presenting information from the _IAggregatorPageCircuit_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitDisplay = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitDisplay.constructor&xyz.swapee.wc.IAggregatorPageCircuitDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.AggregatorPageCircuitDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitDisplay.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitDisplayFields
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitDisplay.Queries} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer. Default `null`.
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayFields.prototype.ExchangeIntent = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitDisplay} */
xyz.swapee.wc.RecordIAggregatorPageCircuitDisplay

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitDisplay} xyz.swapee.wc.BoundIAggregatorPageCircuitDisplay */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitDisplay} xyz.swapee.wc.BoundAggregatorPageCircuitDisplay */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitDisplay.Queries} xyz.swapee.wc.IAggregatorPageCircuitDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IAggregatorPageCircuitDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [exchangeIntentSel=""] The query to discover the _ExchangeIntent_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IAggregatorPageCircuitDisplay_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitDisplayCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitDisplay_ instance into the _BoundIAggregatorPageCircuitDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayCaster.prototype.asIAggregatorPageCircuitDisplay
/**
 * Cast the _IAggregatorPageCircuitDisplay_ instance into the _BoundIAggregatorPageCircuitScreen_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitScreen}
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayCaster.prototype.asIAggregatorPageCircuitScreen
/**
 * Access the _AggregatorPageCircuitDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplayCaster.prototype.superAggregatorPageCircuitDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.AggregatorPageCircuitMemory, land: null) => void} xyz.swapee.wc.IAggregatorPageCircuitDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitDisplay.__paint<!xyz.swapee.wc.IAggregatorPageCircuitDisplay>} xyz.swapee.wc.IAggregatorPageCircuitDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitMemory} memory The display data.
 * - `type` _string_
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatRate` _boolean_ Default `false`.
 * - `anyRate` _boolean_ Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IAggregatorPageCircuitDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/40-IAggregatorPageCircuitDisplayBack.xml}  7c02704eda00aa05f9c45bfd297bd4aa */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.AggregatorPageCircuitClasses>} xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.AggregatorPageCircuitDisplay)} xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay} xyz.swapee.wc.back.AggregatorPageCircuitDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IAggregatorPageCircuitDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.constructor&xyz.swapee.wc.back.AggregatorPageCircuitDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IAggregatorPageCircuitDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IAggregatorPageCircuitDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.AggregatorPageCircuitClasses, !xyz.swapee.wc.AggregatorPageCircuitLand>)} xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitDisplay
 */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplay = class extends /** @type {xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.paint} */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IAggregatorPageCircuitDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.Initialese>)} xyz.swapee.wc.back.AggregatorPageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IAggregatorPageCircuitDisplay} xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitDisplay_ instances.
 * @constructor xyz.swapee.wc.back.AggregatorPageCircuitDisplay
 * @implements {xyz.swapee.wc.back.IAggregatorPageCircuitDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.AggregatorPageCircuitDisplay = class extends /** @type {xyz.swapee.wc.back.AggregatorPageCircuitDisplay.constructor&xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.AggregatorPageCircuitDisplay.prototype.constructor = xyz.swapee.wc.back.AggregatorPageCircuitDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.back.AggregatorPageCircuitDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitDisplay.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitDisplayFields
 */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplayFields = class { }
/**
 * The via for the _ExchangeIntent_ peer.
 */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplayFields.prototype.ExchangeIntent = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitDisplay} */
xyz.swapee.wc.back.RecordIAggregatorPageCircuitDisplay

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitDisplay} xyz.swapee.wc.back.BoundIAggregatorPageCircuitDisplay */

/** @typedef {xyz.swapee.wc.back.AggregatorPageCircuitDisplay} xyz.swapee.wc.back.BoundAggregatorPageCircuitDisplay */

/**
 * Contains getters to cast the _IAggregatorPageCircuitDisplay_ interface.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitDisplayCaster
 */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplayCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitDisplay_ instance into the _BoundIAggregatorPageCircuitDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIAggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplayCaster.prototype.asIAggregatorPageCircuitDisplay
/**
 * Access the _AggregatorPageCircuitDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundAggregatorPageCircuitDisplay}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplayCaster.prototype.superAggregatorPageCircuitDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.AggregatorPageCircuitMemory, land?: !xyz.swapee.wc.AggregatorPageCircuitLand) => void} xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.__paint<!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay>} xyz.swapee.wc.back.IAggregatorPageCircuitDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitMemory} [memory] The display data.
 * - `type` _string_
 * Can be either:
 * > - _fixed_
 * > - _float_
 * > - _any_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatRate` _boolean_ Default `false`.
 * - `anyRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.AggregatorPageCircuitLand} [land] The land data.
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntent_
 * @return {void}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IAggregatorPageCircuitDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/41-AggregatorPageCircuitClasses.xml}  4f0891c040cbd9412999ad84e74c0f45 */
/**
 * The classes of the _IAggregatorPageCircuitDisplay_.
 * @record xyz.swapee.wc.AggregatorPageCircuitClasses
 */
xyz.swapee.wc.AggregatorPageCircuitClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.AggregatorPageCircuitClasses.prototype.props = /** @type {xyz.swapee.wc.AggregatorPageCircuitClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/50-IAggregatorPageCircuitController.xml}  21942c2bd2840f23cdf5d33d92230fbd */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs, !xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel>} xyz.swapee.wc.IAggregatorPageCircuitController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitController)} xyz.swapee.wc.AbstractAggregatorPageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitController} xyz.swapee.wc.AggregatorPageCircuitController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitController` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitController
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitController.constructor&xyz.swapee.wc.AggregatorPageCircuitController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitController.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitController.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitController}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitController}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitController}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitController}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitController.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitController} xyz.swapee.wc.AggregatorPageCircuitControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs, !xyz.swapee.wc.IAggregatorPageCircuitOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IAggregatorPageCircuitOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs, !xyz.swapee.wc.IAggregatorPageCircuitController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs, !xyz.swapee.wc.AggregatorPageCircuitMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs>)} xyz.swapee.wc.IAggregatorPageCircuitController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitController
 */
xyz.swapee.wc.IAggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.resetPort} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.setAmountFrom} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.setAmountFrom = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.unsetAmountFrom} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.unsetAmountFrom = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.setCryptoIn} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.setCryptoIn = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.unsetCryptoIn} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.unsetCryptoIn = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.setCryptoOut} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.setCryptoOut = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.unsetCryptoOut} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.unsetCryptoOut = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.flipFixedRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.flipFixedRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.setFixedRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.setFixedRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.unsetFixedRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.unsetFixedRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.flipFloatRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.flipFloatRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.setFloatRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.setFloatRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.unsetFloatRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.unsetFloatRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.flipAnyRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.flipAnyRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.setAnyRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.setAnyRate = function() {}
/** @type {xyz.swapee.wc.IAggregatorPageCircuitController.unsetAnyRate} */
xyz.swapee.wc.IAggregatorPageCircuitController.prototype.unsetAnyRate = function() {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitController&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitController.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitController.constructor */
/**
 * A concrete class of _IAggregatorPageCircuitController_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitController
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitController.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitController.constructor&xyz.swapee.wc.IAggregatorPageCircuitController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitController}
 */
xyz.swapee.wc.AggregatorPageCircuitController.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitController.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitControllerFields
 */
xyz.swapee.wc.IAggregatorPageCircuitControllerFields = class { }
/**
 * The inputs to the _IAggregatorPageCircuit_'s controller.
 */
xyz.swapee.wc.IAggregatorPageCircuitControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IAggregatorPageCircuitControllerFields.prototype.props = /** @type {xyz.swapee.wc.IAggregatorPageCircuitController} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController} */
xyz.swapee.wc.RecordIAggregatorPageCircuitController

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController} xyz.swapee.wc.BoundIAggregatorPageCircuitController */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitController} xyz.swapee.wc.BoundAggregatorPageCircuitController */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs} xyz.swapee.wc.IAggregatorPageCircuitController.Inputs The inputs to the _IAggregatorPageCircuit_'s controller. */

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitPort.WeakInputs} xyz.swapee.wc.IAggregatorPageCircuitController.WeakInputs The inputs to the _IAggregatorPageCircuit_'s controller. */

/**
 * Contains getters to cast the _IAggregatorPageCircuitController_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitControllerCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitControllerCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitController_ instance into the _BoundIAggregatorPageCircuitController_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitController}
 */
xyz.swapee.wc.IAggregatorPageCircuitControllerCaster.prototype.asIAggregatorPageCircuitController
/**
 * Cast the _IAggregatorPageCircuitController_ instance into the _BoundIAggregatorPageCircuitProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitProcessor}
 */
xyz.swapee.wc.IAggregatorPageCircuitControllerCaster.prototype.asIAggregatorPageCircuitProcessor
/**
 * Access the _AggregatorPageCircuitController_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitController}
 */
xyz.swapee.wc.IAggregatorPageCircuitControllerCaster.prototype.superAggregatorPageCircuitController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__resetPort<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.resetPort = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__setAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__setAmountFrom<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._setAmountFrom */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.setAmountFrom} */
/**
 * Sets the `amountFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.setAmountFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__unsetAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__unsetAmountFrom<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._unsetAmountFrom */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.unsetAmountFrom} */
/**
 * Clears the `amountFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.unsetAmountFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__setCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__setCryptoIn<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._setCryptoIn */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.setCryptoIn} */
/**
 * Sets the `cryptoIn` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.setCryptoIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__unsetCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__unsetCryptoIn<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._unsetCryptoIn */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.unsetCryptoIn} */
/**
 * Clears the `cryptoIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.unsetCryptoIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__setCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__setCryptoOut<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._setCryptoOut */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.setCryptoOut} */
/**
 * Sets the `cryptoOut` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.setCryptoOut = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__unsetCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__unsetCryptoOut<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._unsetCryptoOut */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.unsetCryptoOut} */
/**
 * Clears the `cryptoOut` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.unsetCryptoOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__flipFixedRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__flipFixedRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._flipFixedRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.flipFixedRate} */
/**
 * Flips between the positive and negative values of the `fixedRate`.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.flipFixedRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__setFixedRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__setFixedRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._setFixedRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.setFixedRate} */
/**
 * Sets the `fixedRate` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.setFixedRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__unsetFixedRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__unsetFixedRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._unsetFixedRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.unsetFixedRate} */
/**
 * Clears the `fixedRate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.unsetFixedRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__flipFloatRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__flipFloatRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._flipFloatRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.flipFloatRate} */
/**
 * Flips between the positive and negative values of the `floatRate`.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.flipFloatRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__setFloatRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__setFloatRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._setFloatRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.setFloatRate} */
/**
 * Sets the `floatRate` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.setFloatRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__unsetFloatRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__unsetFloatRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._unsetFloatRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.unsetFloatRate} */
/**
 * Clears the `floatRate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.unsetFloatRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__flipAnyRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__flipAnyRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._flipAnyRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.flipAnyRate} */
/**
 * Flips between the positive and negative values of the `anyRate`.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.flipAnyRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__setAnyRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__setAnyRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._setAnyRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.setAnyRate} */
/**
 * Sets the `anyRate` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.setAnyRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IAggregatorPageCircuitController.__unsetAnyRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitController.__unsetAnyRate<!xyz.swapee.wc.IAggregatorPageCircuitController>} xyz.swapee.wc.IAggregatorPageCircuitController._unsetAnyRate */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitController.unsetAnyRate} */
/**
 * Clears the `anyRate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IAggregatorPageCircuitController.unsetAnyRate = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IAggregatorPageCircuitController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/51-IAggregatorPageCircuitControllerFront.xml}  774cad64383381c80e7e5dda9e7074c0 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IAggregatorPageCircuitController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.AggregatorPageCircuitController)} xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.AggregatorPageCircuitController} xyz.swapee.wc.front.AggregatorPageCircuitController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IAggregatorPageCircuitController` interface.
 * @constructor xyz.swapee.wc.front.AbstractAggregatorPageCircuitController
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.constructor&xyz.swapee.wc.front.AggregatorPageCircuitController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.prototype.constructor = xyz.swapee.wc.front.AbstractAggregatorPageCircuitController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.class = /** @type {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitController|typeof xyz.swapee.wc.front.AggregatorPageCircuitController)|(!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT|typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitController}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitController}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitController|typeof xyz.swapee.wc.front.AggregatorPageCircuitController)|(!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT|typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitController}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitController|typeof xyz.swapee.wc.front.AggregatorPageCircuitController)|(!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT|typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitController}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IAggregatorPageCircuitController.Initialese[]) => xyz.swapee.wc.front.IAggregatorPageCircuitController} xyz.swapee.wc.front.AggregatorPageCircuitControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IAggregatorPageCircuitControllerCaster&xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT)} xyz.swapee.wc.front.IAggregatorPageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT} xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IAggregatorPageCircuitController
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.setAmountFrom} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.setAmountFrom = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetAmountFrom} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.unsetAmountFrom = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.setCryptoIn} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.setCryptoIn = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetCryptoIn} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.unsetCryptoIn = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.setCryptoOut} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.setCryptoOut = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetCryptoOut} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.unsetCryptoOut = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.flipFixedRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.flipFixedRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.setFixedRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.setFixedRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetFixedRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.unsetFixedRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.flipFloatRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.flipFloatRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.setFloatRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.setFloatRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetFloatRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.unsetFloatRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.flipAnyRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.flipAnyRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.setAnyRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.setAnyRate = function() {}
/** @type {xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetAnyRate} */
xyz.swapee.wc.front.IAggregatorPageCircuitController.prototype.unsetAnyRate = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IAggregatorPageCircuitController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IAggregatorPageCircuitController.Initialese>)} xyz.swapee.wc.front.AggregatorPageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController} xyz.swapee.wc.front.IAggregatorPageCircuitController.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitController_ instances.
 * @constructor xyz.swapee.wc.front.AggregatorPageCircuitController
 * @implements {xyz.swapee.wc.front.IAggregatorPageCircuitController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IAggregatorPageCircuitController.Initialese>} ‎
 */
xyz.swapee.wc.front.AggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.front.AggregatorPageCircuitController.constructor&xyz.swapee.wc.front.IAggregatorPageCircuitController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.AggregatorPageCircuitController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitController}
 */
xyz.swapee.wc.front.AggregatorPageCircuitController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController} */
xyz.swapee.wc.front.RecordIAggregatorPageCircuitController

/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController} xyz.swapee.wc.front.BoundIAggregatorPageCircuitController */

/** @typedef {xyz.swapee.wc.front.AggregatorPageCircuitController} xyz.swapee.wc.front.BoundAggregatorPageCircuitController */

/**
 * Contains getters to cast the _IAggregatorPageCircuitController_ interface.
 * @interface xyz.swapee.wc.front.IAggregatorPageCircuitControllerCaster
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitController_ instance into the _BoundIAggregatorPageCircuitController_ type.
 * @type {!xyz.swapee.wc.front.BoundIAggregatorPageCircuitController}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerCaster.prototype.asIAggregatorPageCircuitController
/**
 * Access the _AggregatorPageCircuitController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundAggregatorPageCircuitController}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerCaster.prototype.superAggregatorPageCircuitController

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__setAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__setAmountFrom<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._setAmountFrom */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.setAmountFrom} */
/**
 * Sets the `amountFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.setAmountFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetAmountFrom<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._unsetAmountFrom */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetAmountFrom} */
/**
 * Clears the `amountFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetAmountFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__setCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__setCryptoIn<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._setCryptoIn */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.setCryptoIn} */
/**
 * Sets the `cryptoIn` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.setCryptoIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetCryptoIn<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._unsetCryptoIn */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetCryptoIn} */
/**
 * Clears the `cryptoIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetCryptoIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__setCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__setCryptoOut<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._setCryptoOut */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.setCryptoOut} */
/**
 * Sets the `cryptoOut` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.setCryptoOut = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetCryptoOut<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._unsetCryptoOut */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetCryptoOut} */
/**
 * Clears the `cryptoOut` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetCryptoOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__flipFixedRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__flipFixedRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._flipFixedRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.flipFixedRate} */
/**
 * Flips between the positive and negative values of the `fixedRate`.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.flipFixedRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__setFixedRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__setFixedRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._setFixedRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.setFixedRate} */
/**
 * Sets the `fixedRate` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.setFixedRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetFixedRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetFixedRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._unsetFixedRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetFixedRate} */
/**
 * Clears the `fixedRate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetFixedRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__flipFloatRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__flipFloatRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._flipFloatRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.flipFloatRate} */
/**
 * Flips between the positive and negative values of the `floatRate`.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.flipFloatRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__setFloatRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__setFloatRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._setFloatRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.setFloatRate} */
/**
 * Sets the `floatRate` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.setFloatRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetFloatRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetFloatRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._unsetFloatRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetFloatRate} */
/**
 * Clears the `floatRate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetFloatRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__flipAnyRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__flipAnyRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._flipAnyRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.flipAnyRate} */
/**
 * Flips between the positive and negative values of the `anyRate`.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.flipAnyRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__setAnyRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__setAnyRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._setAnyRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.setAnyRate} */
/**
 * Sets the `anyRate` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.setAnyRate = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetAnyRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitController.__unsetAnyRate<!xyz.swapee.wc.front.IAggregatorPageCircuitController>} xyz.swapee.wc.front.IAggregatorPageCircuitController._unsetAnyRate */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetAnyRate} */
/**
 * Clears the `anyRate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitController.unsetAnyRate = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IAggregatorPageCircuitController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/52-IAggregatorPageCircuitControllerBack.xml}  f2ee05ad842c00cca409ccc2d96a42e6 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs>&xyz.swapee.wc.IAggregatorPageCircuitController.Initialese} xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.AggregatorPageCircuitController)} xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.AggregatorPageCircuitController} xyz.swapee.wc.back.AggregatorPageCircuitController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IAggregatorPageCircuitController` interface.
 * @constructor xyz.swapee.wc.back.AbstractAggregatorPageCircuitController
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.constructor&xyz.swapee.wc.back.AggregatorPageCircuitController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.prototype.constructor = xyz.swapee.wc.back.AbstractAggregatorPageCircuitController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.class = /** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitController|typeof xyz.swapee.wc.back.AggregatorPageCircuitController)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!com.webcircuits.INavigatorBack|typeof com.webcircuits.NavigatorBack)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitController}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitController}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitController|typeof xyz.swapee.wc.back.AggregatorPageCircuitController)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!com.webcircuits.INavigatorBack|typeof com.webcircuits.NavigatorBack)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitController}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitController|typeof xyz.swapee.wc.back.AggregatorPageCircuitController)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)|(!com.webcircuits.INavigatorBack|typeof com.webcircuits.NavigatorBack)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitController}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese[]) => xyz.swapee.wc.back.IAggregatorPageCircuitController} xyz.swapee.wc.back.AggregatorPageCircuitControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IAggregatorPageCircuitControllerCaster&xyz.swapee.wc.IAggregatorPageCircuitController&com.webcircuits.INavigatorBack<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs, !xyz.swapee.wc.AggregatorPageCircuitMemory>&com.webcircuits.IDriverBack<!xyz.swapee.wc.IAggregatorPageCircuitController.Inputs>)} xyz.swapee.wc.back.IAggregatorPageCircuitController.constructor */
/** @typedef {typeof com.webcircuits.INavigatorBack} com.webcircuits.INavigatorBack.typeof */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitController
 */
xyz.swapee.wc.back.IAggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.back.IAggregatorPageCircuitController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IAggregatorPageCircuitController.typeof&com.webcircuits.INavigatorBack.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IAggregatorPageCircuitController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IAggregatorPageCircuitController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese>)} xyz.swapee.wc.back.AggregatorPageCircuitController.constructor */
/**
 * A concrete class of _IAggregatorPageCircuitController_ instances.
 * @constructor xyz.swapee.wc.back.AggregatorPageCircuitController
 * @implements {xyz.swapee.wc.back.IAggregatorPageCircuitController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese>} ‎
 */
xyz.swapee.wc.back.AggregatorPageCircuitController = class extends /** @type {xyz.swapee.wc.back.AggregatorPageCircuitController.constructor&xyz.swapee.wc.back.IAggregatorPageCircuitController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.AggregatorPageCircuitController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitController}
 */
xyz.swapee.wc.back.AggregatorPageCircuitController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitController} */
xyz.swapee.wc.back.RecordIAggregatorPageCircuitController

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitController} xyz.swapee.wc.back.BoundIAggregatorPageCircuitController */

/** @typedef {xyz.swapee.wc.back.AggregatorPageCircuitController} xyz.swapee.wc.back.BoundAggregatorPageCircuitController */

/**
 * Contains getters to cast the _IAggregatorPageCircuitController_ interface.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitControllerCaster
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitController_ instance into the _BoundIAggregatorPageCircuitController_ type.
 * @type {!xyz.swapee.wc.back.BoundIAggregatorPageCircuitController}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerCaster.prototype.asIAggregatorPageCircuitController
/**
 * Access the _AggregatorPageCircuitController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundAggregatorPageCircuitController}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerCaster.prototype.superAggregatorPageCircuitController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/53-IAggregatorPageCircuitControllerAR.xml}  1681561bf0a4ee7ab12d213507d68d96 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IAggregatorPageCircuitController.Initialese} xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.AggregatorPageCircuitControllerAR)} xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR} xyz.swapee.wc.back.AggregatorPageCircuitControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.constructor&xyz.swapee.wc.back.AggregatorPageCircuitControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR|typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR|typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR|typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IAggregatorPageCircuitController|typeof xyz.swapee.wc.AggregatorPageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.Initialese[]) => xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR} xyz.swapee.wc.back.AggregatorPageCircuitControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IAggregatorPageCircuitControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IAggregatorPageCircuitController)} xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IAggregatorPageCircuitControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR = class extends /** @type {xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IAggregatorPageCircuitController.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.Initialese>)} xyz.swapee.wc.back.AggregatorPageCircuitControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR} xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.AggregatorPageCircuitControllerAR
 * @implements {xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IAggregatorPageCircuitControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.AggregatorPageCircuitControllerAR = class extends /** @type {xyz.swapee.wc.back.AggregatorPageCircuitControllerAR.constructor&xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.AggregatorPageCircuitControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitControllerAR}
 */
xyz.swapee.wc.back.AggregatorPageCircuitControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR} */
xyz.swapee.wc.back.RecordIAggregatorPageCircuitControllerAR

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR} xyz.swapee.wc.back.BoundIAggregatorPageCircuitControllerAR */

/** @typedef {xyz.swapee.wc.back.AggregatorPageCircuitControllerAR} xyz.swapee.wc.back.BoundAggregatorPageCircuitControllerAR */

/**
 * Contains getters to cast the _IAggregatorPageCircuitControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitControllerARCaster
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerARCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitControllerAR_ instance into the _BoundIAggregatorPageCircuitControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIAggregatorPageCircuitControllerAR}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerARCaster.prototype.asIAggregatorPageCircuitControllerAR
/**
 * Access the _AggregatorPageCircuitControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundAggregatorPageCircuitControllerAR}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitControllerARCaster.prototype.superAggregatorPageCircuitControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/54-IAggregatorPageCircuitControllerAT.xml}  4227129c27b9c932f650f8064e3a79ba */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.AggregatorPageCircuitControllerAT)} xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT} xyz.swapee.wc.front.AggregatorPageCircuitControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.constructor&xyz.swapee.wc.front.AggregatorPageCircuitControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT|typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT|typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT|typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.Initialese[]) => xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT} xyz.swapee.wc.front.AggregatorPageCircuitControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IAggregatorPageCircuitControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IAggregatorPageCircuitControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT = class extends /** @type {xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.Initialese>)} xyz.swapee.wc.front.AggregatorPageCircuitControllerAT.constructor */
/**
 * A concrete class of _IAggregatorPageCircuitControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.AggregatorPageCircuitControllerAT
 * @implements {xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IAggregatorPageCircuitControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.AggregatorPageCircuitControllerAT = class extends /** @type {xyz.swapee.wc.front.AggregatorPageCircuitControllerAT.constructor&xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.AggregatorPageCircuitControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitControllerAT}
 */
xyz.swapee.wc.front.AggregatorPageCircuitControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT} */
xyz.swapee.wc.front.RecordIAggregatorPageCircuitControllerAT

/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT} xyz.swapee.wc.front.BoundIAggregatorPageCircuitControllerAT */

/** @typedef {xyz.swapee.wc.front.AggregatorPageCircuitControllerAT} xyz.swapee.wc.front.BoundAggregatorPageCircuitControllerAT */

/**
 * Contains getters to cast the _IAggregatorPageCircuitControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IAggregatorPageCircuitControllerATCaster
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerATCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitControllerAT_ instance into the _BoundIAggregatorPageCircuitControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIAggregatorPageCircuitControllerAT}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerATCaster.prototype.asIAggregatorPageCircuitControllerAT
/**
 * Access the _AggregatorPageCircuitControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundAggregatorPageCircuitControllerAT}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitControllerATCaster.prototype.superAggregatorPageCircuitControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/70-IAggregatorPageCircuitScreen.xml}  89a6f2f2aea53c002bc77004a6d05c04 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.front.AggregatorPageCircuitInputs, !HTMLDivElement, !xyz.swapee.wc.IAggregatorPageCircuitDisplay.Settings, !xyz.swapee.wc.IAggregatorPageCircuitDisplay.Queries, null>&xyz.swapee.wc.IAggregatorPageCircuitDisplay.Initialese} xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitScreen)} xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitScreen} xyz.swapee.wc.AggregatorPageCircuitScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitScreen` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitScreen
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.constructor&xyz.swapee.wc.AggregatorPageCircuitScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.AggregatorPageCircuitScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IAggregatorPageCircuitController|typeof xyz.swapee.wc.front.AggregatorPageCircuitController)|(!xyz.swapee.wc.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.AggregatorPageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitScreen}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.AggregatorPageCircuitScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IAggregatorPageCircuitController|typeof xyz.swapee.wc.front.AggregatorPageCircuitController)|(!xyz.swapee.wc.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.AggregatorPageCircuitDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.AggregatorPageCircuitScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IAggregatorPageCircuitController|typeof xyz.swapee.wc.front.AggregatorPageCircuitController)|(!xyz.swapee.wc.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.AggregatorPageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitScreen} xyz.swapee.wc.AggregatorPageCircuitScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.AggregatorPageCircuitMemory, !xyz.swapee.wc.front.AggregatorPageCircuitInputs, !HTMLDivElement, !xyz.swapee.wc.IAggregatorPageCircuitDisplay.Settings, !xyz.swapee.wc.IAggregatorPageCircuitDisplay.Queries, null, null>&xyz.swapee.wc.front.IAggregatorPageCircuitController&xyz.swapee.wc.IAggregatorPageCircuitDisplay)} xyz.swapee.wc.IAggregatorPageCircuitScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitScreen
 */
xyz.swapee.wc.IAggregatorPageCircuitScreen = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IAggregatorPageCircuitController.typeof&xyz.swapee.wc.IAggregatorPageCircuitDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IAggregatorPageCircuitScreen} xyz.swapee.wc.IAggregatorPageCircuitScreen.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitScreen_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitScreen
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitScreen = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitScreen.constructor&xyz.swapee.wc.IAggregatorPageCircuitScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.AggregatorPageCircuitScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitScreen} */
xyz.swapee.wc.RecordIAggregatorPageCircuitScreen

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitScreen} xyz.swapee.wc.BoundIAggregatorPageCircuitScreen */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitScreen} xyz.swapee.wc.BoundAggregatorPageCircuitScreen */

/**
 * Contains getters to cast the _IAggregatorPageCircuitScreen_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitScreenCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitScreenCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitScreen_ instance into the _BoundIAggregatorPageCircuitScreen_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitScreen}
 */
xyz.swapee.wc.IAggregatorPageCircuitScreenCaster.prototype.asIAggregatorPageCircuitScreen
/**
 * Access the _AggregatorPageCircuitScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitScreen}
 */
xyz.swapee.wc.IAggregatorPageCircuitScreenCaster.prototype.superAggregatorPageCircuitScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/70-IAggregatorPageCircuitScreenBack.xml}  295c4dad5e5ad14a2a841e3f46b69a4e */
/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese} xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.AggregatorPageCircuitScreen)} xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen} xyz.swapee.wc.back.AggregatorPageCircuitScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IAggregatorPageCircuitScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen = class extends /** @type {xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.constructor&xyz.swapee.wc.back.AggregatorPageCircuitScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.prototype.constructor = xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen)|(!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen)|(!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen)|(!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese[]) => xyz.swapee.wc.back.IAggregatorPageCircuitScreen} xyz.swapee.wc.back.AggregatorPageCircuitScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IAggregatorPageCircuitScreenCaster&xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT)} xyz.swapee.wc.back.IAggregatorPageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT} xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitScreen
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreen = class extends /** @type {xyz.swapee.wc.back.IAggregatorPageCircuitScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IAggregatorPageCircuitScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese>)} xyz.swapee.wc.back.AggregatorPageCircuitScreen.constructor */
/**
 * A concrete class of _IAggregatorPageCircuitScreen_ instances.
 * @constructor xyz.swapee.wc.back.AggregatorPageCircuitScreen
 * @implements {xyz.swapee.wc.back.IAggregatorPageCircuitScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.AggregatorPageCircuitScreen = class extends /** @type {xyz.swapee.wc.back.AggregatorPageCircuitScreen.constructor&xyz.swapee.wc.back.IAggregatorPageCircuitScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.AggregatorPageCircuitScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreen}
 */
xyz.swapee.wc.back.AggregatorPageCircuitScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitScreen} */
xyz.swapee.wc.back.RecordIAggregatorPageCircuitScreen

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitScreen} xyz.swapee.wc.back.BoundIAggregatorPageCircuitScreen */

/** @typedef {xyz.swapee.wc.back.AggregatorPageCircuitScreen} xyz.swapee.wc.back.BoundAggregatorPageCircuitScreen */

/**
 * Contains getters to cast the _IAggregatorPageCircuitScreen_ interface.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitScreenCaster
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitScreen_ instance into the _BoundIAggregatorPageCircuitScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIAggregatorPageCircuitScreen}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenCaster.prototype.asIAggregatorPageCircuitScreen
/**
 * Access the _AggregatorPageCircuitScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundAggregatorPageCircuitScreen}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenCaster.prototype.superAggregatorPageCircuitScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/73-IAggregatorPageCircuitScreenAR.xml}  d9b1a965904b1b84a3a059346267378c */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IAggregatorPageCircuitScreen.Initialese} xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.AggregatorPageCircuitScreenAR)} xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR} xyz.swapee.wc.front.AggregatorPageCircuitScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.constructor&xyz.swapee.wc.front.AggregatorPageCircuitScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR|typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.AggregatorPageCircuitScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR|typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.AggregatorPageCircuitScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR|typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IAggregatorPageCircuitScreen|typeof xyz.swapee.wc.AggregatorPageCircuitScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractAggregatorPageCircuitScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.Initialese[]) => xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR} xyz.swapee.wc.front.AggregatorPageCircuitScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IAggregatorPageCircuitScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IAggregatorPageCircuitScreen)} xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IAggregatorPageCircuitScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR
 */
xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR = class extends /** @type {xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IAggregatorPageCircuitScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.Initialese>)} xyz.swapee.wc.front.AggregatorPageCircuitScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR} xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.typeof */
/**
 * A concrete class of _IAggregatorPageCircuitScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.AggregatorPageCircuitScreenAR
 * @implements {xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IAggregatorPageCircuitScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.AggregatorPageCircuitScreenAR = class extends /** @type {xyz.swapee.wc.front.AggregatorPageCircuitScreenAR.constructor&xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.AggregatorPageCircuitScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.AggregatorPageCircuitScreenAR}
 */
xyz.swapee.wc.front.AggregatorPageCircuitScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR} */
xyz.swapee.wc.front.RecordIAggregatorPageCircuitScreenAR

/** @typedef {xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR} xyz.swapee.wc.front.BoundIAggregatorPageCircuitScreenAR */

/** @typedef {xyz.swapee.wc.front.AggregatorPageCircuitScreenAR} xyz.swapee.wc.front.BoundAggregatorPageCircuitScreenAR */

/**
 * Contains getters to cast the _IAggregatorPageCircuitScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IAggregatorPageCircuitScreenARCaster
 */
xyz.swapee.wc.front.IAggregatorPageCircuitScreenARCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitScreenAR_ instance into the _BoundIAggregatorPageCircuitScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIAggregatorPageCircuitScreenAR}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitScreenARCaster.prototype.asIAggregatorPageCircuitScreenAR
/**
 * Access the _AggregatorPageCircuitScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundAggregatorPageCircuitScreenAR}
 */
xyz.swapee.wc.front.IAggregatorPageCircuitScreenARCaster.prototype.superAggregatorPageCircuitScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/74-IAggregatorPageCircuitScreenAT.xml}  a454aadfbe9441c1cfd735c031faabb2 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.AggregatorPageCircuitScreenAT)} xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT} xyz.swapee.wc.back.AggregatorPageCircuitScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.constructor&xyz.swapee.wc.back.AggregatorPageCircuitScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT|typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractAggregatorPageCircuitScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese[]) => xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT} xyz.swapee.wc.back.AggregatorPageCircuitScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IAggregatorPageCircuitScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IAggregatorPageCircuitScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT = class extends /** @type {xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese>)} xyz.swapee.wc.back.AggregatorPageCircuitScreenAT.constructor */
/**
 * A concrete class of _IAggregatorPageCircuitScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.AggregatorPageCircuitScreenAT
 * @implements {xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IAggregatorPageCircuitScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.AggregatorPageCircuitScreenAT = class extends /** @type {xyz.swapee.wc.back.AggregatorPageCircuitScreenAT.constructor&xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.AggregatorPageCircuitScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.AggregatorPageCircuitScreenAT}
 */
xyz.swapee.wc.back.AggregatorPageCircuitScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT} */
xyz.swapee.wc.back.RecordIAggregatorPageCircuitScreenAT

/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT} xyz.swapee.wc.back.BoundIAggregatorPageCircuitScreenAT */

/** @typedef {xyz.swapee.wc.back.AggregatorPageCircuitScreenAT} xyz.swapee.wc.back.BoundAggregatorPageCircuitScreenAT */

/**
 * Contains getters to cast the _IAggregatorPageCircuitScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IAggregatorPageCircuitScreenATCaster
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenATCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitScreenAT_ instance into the _BoundIAggregatorPageCircuitScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIAggregatorPageCircuitScreenAT}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenATCaster.prototype.asIAggregatorPageCircuitScreenAT
/**
 * Access the _AggregatorPageCircuitScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundAggregatorPageCircuitScreenAT}
 */
xyz.swapee.wc.back.IAggregatorPageCircuitScreenATCaster.prototype.superAggregatorPageCircuitScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/design/80-IAggregatorPageCircuitGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.Initialese} xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.AggregatorPageCircuitGPU)} xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.AggregatorPageCircuitGPU} xyz.swapee.wc.AggregatorPageCircuitGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitGPU` interface.
 * @constructor xyz.swapee.wc.AbstractAggregatorPageCircuitGPU
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU = class extends /** @type {xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.constructor&xyz.swapee.wc.AggregatorPageCircuitGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.prototype.constructor = xyz.swapee.wc.AbstractAggregatorPageCircuitGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.class = /** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitGPU|typeof xyz.swapee.wc.AggregatorPageCircuitGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitGPU}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitGPU}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitGPU|typeof xyz.swapee.wc.AggregatorPageCircuitGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitGPU}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IAggregatorPageCircuitGPU|typeof xyz.swapee.wc.AggregatorPageCircuitGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IAggregatorPageCircuitDisplay|typeof xyz.swapee.wc.back.AggregatorPageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitGPU}
 */
xyz.swapee.wc.AbstractAggregatorPageCircuitGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese[]) => xyz.swapee.wc.IAggregatorPageCircuitGPU} xyz.swapee.wc.AggregatorPageCircuitGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IAggregatorPageCircuitGPUCaster&com.webcircuits.IBrowserView<.!AggregatorPageCircuitMemory,.!AggregatorPageCircuitLand>&xyz.swapee.wc.back.IAggregatorPageCircuitDisplay)} xyz.swapee.wc.IAggregatorPageCircuitGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!AggregatorPageCircuitMemory,.!AggregatorPageCircuitLand>} com.webcircuits.IBrowserView<.!AggregatorPageCircuitMemory,.!AggregatorPageCircuitLand>.typeof */
/**
 * Handles the periphery of the _IAggregatorPageCircuitDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitGPU
 */
xyz.swapee.wc.IAggregatorPageCircuitGPU = class extends /** @type {xyz.swapee.wc.IAggregatorPageCircuitGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!AggregatorPageCircuitMemory,.!AggregatorPageCircuitLand>.typeof&xyz.swapee.wc.back.IAggregatorPageCircuitDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IAggregatorPageCircuitGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IAggregatorPageCircuitGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IAggregatorPageCircuitGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese>)} xyz.swapee.wc.AggregatorPageCircuitGPU.constructor */
/**
 * A concrete class of _IAggregatorPageCircuitGPU_ instances.
 * @constructor xyz.swapee.wc.AggregatorPageCircuitGPU
 * @implements {xyz.swapee.wc.IAggregatorPageCircuitGPU} Handles the periphery of the _IAggregatorPageCircuitDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese>} ‎
 */
xyz.swapee.wc.AggregatorPageCircuitGPU = class extends /** @type {xyz.swapee.wc.AggregatorPageCircuitGPU.constructor&xyz.swapee.wc.IAggregatorPageCircuitGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IAggregatorPageCircuitGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IAggregatorPageCircuitGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IAggregatorPageCircuitGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.AggregatorPageCircuitGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.AggregatorPageCircuitGPU}
 */
xyz.swapee.wc.AggregatorPageCircuitGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IAggregatorPageCircuitGPU.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitGPUFields
 */
xyz.swapee.wc.IAggregatorPageCircuitGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IAggregatorPageCircuitGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitGPU} */
xyz.swapee.wc.RecordIAggregatorPageCircuitGPU

/** @typedef {xyz.swapee.wc.IAggregatorPageCircuitGPU} xyz.swapee.wc.BoundIAggregatorPageCircuitGPU */

/** @typedef {xyz.swapee.wc.AggregatorPageCircuitGPU} xyz.swapee.wc.BoundAggregatorPageCircuitGPU */

/**
 * Contains getters to cast the _IAggregatorPageCircuitGPU_ interface.
 * @interface xyz.swapee.wc.IAggregatorPageCircuitGPUCaster
 */
xyz.swapee.wc.IAggregatorPageCircuitGPUCaster = class { }
/**
 * Cast the _IAggregatorPageCircuitGPU_ instance into the _BoundIAggregatorPageCircuitGPU_ type.
 * @type {!xyz.swapee.wc.BoundIAggregatorPageCircuitGPU}
 */
xyz.swapee.wc.IAggregatorPageCircuitGPUCaster.prototype.asIAggregatorPageCircuitGPU
/**
 * Access the _AggregatorPageCircuitGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundAggregatorPageCircuitGPU}
 */
xyz.swapee.wc.IAggregatorPageCircuitGPUCaster.prototype.superAggregatorPageCircuitGPU

// nss:xyz.swapee.wc
/* @typal-end */