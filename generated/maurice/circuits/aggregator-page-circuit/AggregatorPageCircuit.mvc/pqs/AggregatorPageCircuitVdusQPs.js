import {AggregatorPageCircuitVdusPQs} from './AggregatorPageCircuitVdusPQs'
export const AggregatorPageCircuitVdusQPs=/**@type {!xyz.swapee.wc.AggregatorPageCircuitVdusQPs}*/(Object.keys(AggregatorPageCircuitVdusPQs)
 .reduce((a,k)=>{a[AggregatorPageCircuitVdusPQs[k]]=k;return a},{}))