import {AggregatorPageCircuitInputsPQs} from './AggregatorPageCircuitInputsPQs'
export const AggregatorPageCircuitInputsQPs=/**@type {!xyz.swapee.wc.AggregatorPageCircuitInputsQPs}*/(Object.keys(AggregatorPageCircuitInputsPQs)
 .reduce((a,k)=>{a[AggregatorPageCircuitInputsPQs[k]]=k;return a},{}))