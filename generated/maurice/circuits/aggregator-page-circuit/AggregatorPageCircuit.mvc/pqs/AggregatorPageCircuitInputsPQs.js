import {AggregatorPageCircuitMemoryPQs} from './AggregatorPageCircuitMemoryPQs'
export const AggregatorPageCircuitInputsPQs=/**@type {!xyz.swapee.wc.AggregatorPageCircuitInputsQPs}*/({
 ...AggregatorPageCircuitMemoryPQs,
})