import {AggregatorPageCircuitQueriesPQs} from './AggregatorPageCircuitQueriesPQs'
export const AggregatorPageCircuitQueriesQPs=/**@type {!xyz.swapee.wc.AggregatorPageCircuitQueriesQPs}*/(Object.keys(AggregatorPageCircuitQueriesPQs)
 .reduce((a,k)=>{a[AggregatorPageCircuitQueriesPQs[k]]=k;return a},{}))