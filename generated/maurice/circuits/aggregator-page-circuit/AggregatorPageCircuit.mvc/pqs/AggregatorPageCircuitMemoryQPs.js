import {AggregatorPageCircuitMemoryPQs} from './AggregatorPageCircuitMemoryPQs'
export const AggregatorPageCircuitMemoryQPs=/**@type {!xyz.swapee.wc.AggregatorPageCircuitMemoryQPs}*/(Object.keys(AggregatorPageCircuitMemoryPQs)
 .reduce((a,k)=>{a[AggregatorPageCircuitMemoryPQs[k]]=k;return a},{}))