export const AggregatorPageCircuitMemoryPQs=/**@type {!xyz.swapee.wc.AggregatorPageCircuitMemoryPQs}*/({
 type:'599dc',
 amountFrom:'748e6',
 cryptoIn:'4212d',
 cryptoOut:'c60b5',
 fixedRate:'7d0bd',
 floatRate:'a8df4',
 anyRate:'60e57',
})