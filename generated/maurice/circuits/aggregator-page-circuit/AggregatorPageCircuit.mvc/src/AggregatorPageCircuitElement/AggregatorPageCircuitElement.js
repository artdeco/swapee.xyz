import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import AggregatorPageCircuitServerController from '../AggregatorPageCircuitServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractAggregatorPageCircuitElement from '../../gen/AbstractAggregatorPageCircuitElement'

/** @extends {xyz.swapee.wc.AggregatorPageCircuitElement} */
export default class AggregatorPageCircuitElement extends AbstractAggregatorPageCircuitElement.implements(
 AggregatorPageCircuitServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IAggregatorPageCircuitElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IAggregatorPageCircuitElement}*/({
   classesMap:   true,
   rootSelector: '.AggregatorPageCircuit',
   stylesheet:   'html/styles/AggregatorPageCircuit.css',
   blockName:    'html/AggregatorPageCircuitBlock.html',
  }),
  /** @type {xyz.swapee.wc.IAggregatorPageCircuitDesigner} */({
  }),
){}

// thank you for using web circuits
