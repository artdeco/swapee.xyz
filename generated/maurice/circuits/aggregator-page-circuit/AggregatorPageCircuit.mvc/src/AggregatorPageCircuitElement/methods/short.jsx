/** @type {xyz.swapee.wc.IAggregatorPageCircuitElement._short} */
export default function short({
 amountFrom:amountFrom,cryptoOut:cryptoOut,cryptoIn:cryptoIn,
 fixedRate:fixedRate,floatRate:floatRate,anyRate:anyRate,
},{
 ExchangeIntent:ExchangeIntent,
}){
 return (<>
  <ExchangeIntent currencyFrom={cryptoIn} currencyTo={cryptoOut}
   amountFrom={amountFrom} fixed={fixedRate} float={floatRate} any={anyRate}
   ready={true} />
 </>)
}