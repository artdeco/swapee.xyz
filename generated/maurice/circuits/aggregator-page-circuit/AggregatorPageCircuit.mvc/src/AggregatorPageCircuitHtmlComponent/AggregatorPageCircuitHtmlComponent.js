import {urlInputs} from '../../../../../../../maurice/circuits/aggregator-page-circuit/url-inputs'
import AggregatorPageCircuitHtmlController from '../AggregatorPageCircuitHtmlController'
import AggregatorPageCircuitHtmlComputer from '../AggregatorPageCircuitHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractAggregatorPageCircuitHtmlComponent} from '../../gen/AbstractAggregatorPageCircuitHtmlComponent'

/** @extends {xyz.swapee.wc.AggregatorPageCircuitHtmlComponent} */
export default class extends AbstractAggregatorPageCircuitHtmlComponent.implements(
 AggregatorPageCircuitHtmlController,
 AggregatorPageCircuitHtmlComputer,
 IntegratedComponentInitialiser,
/**@type {!xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent}*/({
   urlInputs:urlInputs,
  }),
){}