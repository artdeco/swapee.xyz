import adaptInterlocks from './methods/adapt-interlocks'
import adaptType from './methods/adapt-type'
import adaptIntent from './methods/adapt-intent'
import {preadaptInterlocks,preadaptType,preadaptIntent} from '../../gen/AbstractAggregatorPageCircuitComputer/preadapters'
import AbstractAggregatorPageCircuitComputer from '../../gen/AbstractAggregatorPageCircuitComputer'

/** @extends {xyz.swapee.wc.AggregatorPageCircuitComputer} */
export default class AggregatorPageCircuitHtmlComputer extends AbstractAggregatorPageCircuitComputer.implements(
 /** @type {!xyz.swapee.wc.IAggregatorPageCircuitComputer} */ ({
  adaptInterlocks:adaptInterlocks,
  adaptType:adaptType,
  adaptIntent:adaptIntent,
  adapt:[preadaptInterlocks,preadaptType,preadaptIntent],
 }),
){}