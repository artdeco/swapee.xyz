/** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer._adaptInterlocks} */
export default function adaptInterlocks({
 fixedRate:fixedRate,floatRate:floatRate,anyRate:anyRate,
},{fixedRate:fixedRatePrev,floatRate:floatRatePrev,anyRate:anyRatePrev}) {
 if(fixedRate&&fixedRatePrev!==undefined){
  return{floatRate:false,anyRate:false}
 }
 if(floatRate&&floatRatePrev!==undefined){
  return{fixedRate:false,anyRate:false}
 }
 if(anyRate&&anyRatePrev!==undefined){
  return{fixedRate:false,floatRate:false}
 }
}