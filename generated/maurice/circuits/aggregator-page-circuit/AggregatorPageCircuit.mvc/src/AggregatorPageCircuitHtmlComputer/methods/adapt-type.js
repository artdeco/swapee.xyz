/** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer._adaptType} */
export default function adaptType({type:type}) {
 if(type=='fixed') {
  return{fixedRate:true}
 }
 if(type=='float'){
  return{floatRate:true}
 }
 if(type=='any'){
  return{anyRate:true}
 }
}