/** @type {xyz.swapee.wc.IAggregatorPageCircuitComputer._adaptIntent} */
export default function adaptIntent({
 any:any,float:float,fixed:fixed,
 currencyFrom:currencyFrom,currencyTo:currencyTo,amountFrom:amountFrom,
}){
 let type
 if(fixed) type='fixed'
 else if(float) type='float'
 else if(any) type='any'
 return{
  cryptoIn:currencyFrom,cryptoOut:currencyTo,
  type:type,amountFrom:amountFrom,
 }
}