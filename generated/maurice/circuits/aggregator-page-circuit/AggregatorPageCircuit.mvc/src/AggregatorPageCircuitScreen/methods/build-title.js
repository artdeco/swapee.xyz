/** @type {xyz.swapee.wc.IAggregatorPageCircuitScreen._buildTitle} */
export default function buildTitle(state,baseTitle){
 const{
  amountFrom:amount,
  cryptoIn:from,
  cryptoOut:to,
 }=state
 if(!from||!to) return baseTitle
 return['Exchange',amount,from,'to',to,'|',baseTitle].join(' ')
}