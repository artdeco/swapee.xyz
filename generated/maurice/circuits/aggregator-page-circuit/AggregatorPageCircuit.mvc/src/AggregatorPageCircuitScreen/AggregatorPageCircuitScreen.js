import {urlInputs} from '../../../../../../../maurice/circuits/aggregator-page-circuit/url-inputs'
import buildTitle from './methods/build-title'
import AbstractAggregatorPageCircuitControllerAT from '../../gen/AbstractAggregatorPageCircuitControllerAT'
import AggregatorPageCircuitDisplay from '../AggregatorPageCircuitDisplay'
import AbstractAggregatorPageCircuitScreen from '../../gen/AbstractAggregatorPageCircuitScreen'

/** @extends {xyz.swapee.wc.AggregatorPageCircuitScreen} */
export default class extends AbstractAggregatorPageCircuitScreen.implements(
 AbstractAggregatorPageCircuitControllerAT,
 AggregatorPageCircuitDisplay,
 /**@type {!xyz.swapee.wc.IAggregatorPageCircuitScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IAggregatorPageCircuitScreen} */ ({
  buildTitle:buildTitle,
  __$id:3494861560,
 }),
/**@type {!xyz.swapee.wc.IAggregatorPageCircuitScreen}*/({
   baseTitle:document.title,
   urlInputs:urlInputs,
  }),
){}