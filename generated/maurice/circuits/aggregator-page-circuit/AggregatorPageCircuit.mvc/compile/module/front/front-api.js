import { AggregatorPageCircuitDisplay, AggregatorPageCircuitScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.AggregatorPageCircuitDisplay} */
export { AggregatorPageCircuitDisplay }
/** @lazy @api {xyz.swapee.wc.AggregatorPageCircuitScreen} */
export { AggregatorPageCircuitScreen }