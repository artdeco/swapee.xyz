/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuit` interface.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuit}
 */
class AbstractAggregatorPageCircuit extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IAggregatorPageCircuit_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitPort}
 */
class AggregatorPageCircuitPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitController` interface.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitController}
 */
class AbstractAggregatorPageCircuitController extends (class {/* lazy-loaded */}) {}
/**
 * The _IAggregatorPageCircuit_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitHtmlComponent}
 */
class AggregatorPageCircuitHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitBuffer}
 */
class AggregatorPageCircuitBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitComputer` interface.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitComputer}
 */
class AbstractAggregatorPageCircuitComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitComputer}
 */
class AggregatorPageCircuitComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AggregatorPageCircuitController}
 */
class AggregatorPageCircuitController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractAggregatorPageCircuit = AbstractAggregatorPageCircuit
module.exports.AggregatorPageCircuitPort = AggregatorPageCircuitPort
module.exports.AbstractAggregatorPageCircuitController = AbstractAggregatorPageCircuitController
module.exports.AggregatorPageCircuitHtmlComponent = AggregatorPageCircuitHtmlComponent
module.exports.AggregatorPageCircuitBuffer = AggregatorPageCircuitBuffer
module.exports.AbstractAggregatorPageCircuitComputer = AbstractAggregatorPageCircuitComputer
module.exports.AggregatorPageCircuitComputer = AggregatorPageCircuitComputer
module.exports.AggregatorPageCircuitController = AggregatorPageCircuitController