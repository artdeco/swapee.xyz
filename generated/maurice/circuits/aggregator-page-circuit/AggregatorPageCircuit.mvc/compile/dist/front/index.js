/**
 * Display for presenting information from the _IAggregatorPageCircuit_.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitDisplay}
 */
class AggregatorPageCircuitDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitScreen}
 */
class AggregatorPageCircuitScreen extends (class {/* lazy-loaded */}) {}

module.exports.AggregatorPageCircuitDisplay = AggregatorPageCircuitDisplay
module.exports.AggregatorPageCircuitScreen = AggregatorPageCircuitScreen