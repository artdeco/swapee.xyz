var e="depack-remove-start",f=e;try{if(e)throw Error();Object.setPrototypeOf(f,f);f.J=new WeakMap;f.map=new Map;f.set=new Set;Object.getOwnPropertySymbols({});Object.getOwnPropertyDescriptors({});e.includes("");[].keys();Object.values({});Object.assign({},{})}catch(a){}e="depack-remove-end";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const g=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const h=g["37270038985"],k=g["372700389810"],l=g["372700389811"];function m(a,b,d,c){return g["372700389812"](a,b,d,c,!1,void 0)};/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const n=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();/*

@LICENSE @webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const p=n["61893096584"],q=n["61893096586"],r=n["618930965811"],t=n["618930965812"],u=n["618930965818"];function v(){}v.prototype={};function w(){this.g=null}class x{}class y extends m(x,349486156017,w,{s:1,G:2}){}y[l]=[v,p,function(){}.prototype={constructor(){h(this,()=>{const {queries:{A:a}}=this;this.scan({h:a})})},scan:function({h:a}){const {element:b,queries:{h:d}}=this;let c;a?c=b.closest(a):c=document;Object.assign(this,{g:d?c.querySelector(d):void 0})}},{[k]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var z=class extends y.implements(){};const A={i:"amount",j:"from",l:"to",type:"type"};function B(a,b){const d=a.j,c=a.l;return d&&c?["Exchange",a.i,d,"to",c,"|",b].join(" "):b};function C(){}C.prototype={};class D{}class E extends m(D,349486156025,null,{o:1,F:2}){}E[l]=[C,r,function(){}.prototype={}];function F(){}F.prototype={};class G{}class H extends m(G,349486156028,null,{v:1,I:2}){}H[l]=[F,t,function(){}.prototype={allocator(){this.methods={}}}];const I={type:"599dc",i:"748e6",j:"4212d",l:"c60b5",C:"7d0bd",D:"a8df4",m:"60e57"};const J={...I};const K=Object.keys(I).reduce((a,b)=>{a[I[b]]=b;return a},{});function L(){}L.prototype={};class M{}class N extends m(M,349486156026,null,{u:1,H:2}){}function O(){}N[l]=[L,O.prototype={inputsPQs:J,queriesPQs:{h:"13da4"},memoryQPs:K},q,H,u,O.prototype={vdusPQs:{g:"acdb1"}}];var P=class extends N.implements(E,z,{get queries(){return this.settings}},{buildTitle:B,__$id:3494861560},{baseTitle:document.title,urlInputs:A}){};module.exports["349486156041"]=z;module.exports["349486156071"]=P;

//# sourceMappingURL=internal.js.map