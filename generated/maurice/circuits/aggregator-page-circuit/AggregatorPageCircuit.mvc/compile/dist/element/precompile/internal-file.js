/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function e(a,b,f,g){return c["372700389812"](a,b,f,g,!1,void 0)};function k(){}k.prototype={};class l{}class m extends e(l,34948615608,null,{K:1,Z:2}){}m[d]=[k,{}];


const n=function(){return require(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];const p={type:"599dc",i:"748e6",l:"4212d",m:"c60b5",o:"7d0bd",s:"a8df4",j:"60e57"};function q(){}q.prototype={};class r{}class w extends e(r,34948615607,null,{F:1,U:2}){}function x(){}x.prototype={};function y(){this.model={type:"any",i:"0.1",l:"BTC",m:"ETH",o:!1,s:!1,j:!1}}class z{}class A extends e(z,34948615603,y,{I:1,X:2}){}A[d]=[x,{constructor(){n(this.model,p)}}];w[d]=[{},q,A];

const B=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const C=B.IntegratedComponentInitialiser,D=B.IntegratedComponent,aa=B["95173443851"],ba=B["95173443852"];
const E=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=E["615055805212"],da=E["615055805218"],ea=E["615055805235"];function F(){}F.prototype={};class fa{}class G extends e(fa,34948615601,null,{A:1,R:2}){}G[d]=[F,da];const H={regulate:ca({type:String,i:String,l:String,m:String,o:Boolean,s:Boolean,j:Boolean})};
const I=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ha=I.IntegratedController,ia=I.Parametric;const J={...p};function K(){}K.prototype={};function L(){const a={model:null};y.call(a);this.inputs=a.model}class ja{}class M extends e(ja,34948615605,L,{J:1,Y:2}){}function N(){}M[d]=[N.prototype={resetPort(){L.call(this)}},K,ia,N.prototype={constructor(){n(this.inputs,J)}}];function O(){}O.prototype={};class ka{}class P extends e(ka,349486156021,null,{D:1,T:2}){}function Q(){}P[d]=[Q.prototype={resetPort(){this.port.resetPort()}},O,H,ha,{get Port(){return M}},Q.prototype={}];function R(){}R.prototype={};class la{}class S extends e(la,34948615609,null,{v:1,P:2}){}S[d]=[R,w,m,D,G,P];function ma(){return{}};const na=require(eval('"@type.engineering/web-computing"')).h;function oa(){return na("div",{$id:"AggregatorPageCircuit"})};const pa=require(eval('"@type.engineering/web-computing"')).h;function qa(){return pa("div",{$id:"AggregatorPageCircuit"})};var T=class extends P.implements(){};require("https");require("http");const ra=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}ra("aqt");require("fs");require("child_process");
const U=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const sa=U.ElementBase,ta=U.HTMLBlocker;function V(){}V.prototype={};function ua(){this.inputs={noSolder:!1,M:{}}}class va{}class W extends e(va,349486156014,ua,{H:1,W:2}){}W[d]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"exchange-intent-opts":void 0,"amount-from":void 0,"crypto-in":void 0,"crypto-out":void 0,"fixed-rate":void 0,"float-rate":void 0,"any-rate":void 0})}}];function X(){}X.prototype={};class wa{}class Y extends e(wa,349486156013,null,{G:1,V:2}){}function Z(){}
Y[d]=[X,sa,Z.prototype={calibrate:function({":no-solder":a,":type":b,":amount-from":f,":crypto-in":g,":crypto-out":h,":fixed-rate":t,":float-rate":u,":any-rate":v}){const {attributes:{"no-solder":xa,type:ya,"amount-from":za,"crypto-in":Aa,"crypto-out":Ba,"fixed-rate":Ca,"float-rate":Da,"any-rate":Ea}}=this;return{...(void 0===xa?{"no-solder":a}:{}),...(void 0===ya?{type:b}:{}),...(void 0===za?{"amount-from":f}:{}),...(void 0===Aa?{"crypto-in":g}:{}),...(void 0===Ba?{"crypto-out":h}:{}),...(void 0===
Ca?{"fixed-rate":t}:{}),...(void 0===Da?{"float-rate":u}:{}),...(void 0===Ea?{"any-rate":v}:{})}}},Z.prototype={calibrate:({"no-solder":a,type:b,"amount-from":f,"crypto-in":g,"crypto-out":h,"fixed-rate":t,"float-rate":u,"any-rate":v})=>({noSolder:a,type:b,i:f,l:g,m:h,o:t,s:u,j:v})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={inputsPQs:J,queriesPQs:{g:"13da4"},vdus:{ExchangeIntent:"acdb1"}},D,ba,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder type amountFrom cryptoIn cryptoOut fixedRate floatRate anyRate query:exchange-intent no-solder :no-solder :type amount-from :amount-from crypto-in :crypto-in crypto-out :crypto-out fixed-rate :fixed-rate float-rate :float-rate any-rate :any-rate fe646 c3b6b 599dc 748e6 4212d c60b5 7d0bd a8df4 60e57 children".split(" "))})},
get Port(){return W},calibrate:function({"query:exchange-intent":a}){const b={};a&&(b.g=a);return b}},ea,Z.prototype={constructor(){this.land={u:null}}},Z.prototype={calibrate:async function({g:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:f},asIElement:{fqn:g}}=this,h=await b(a);if(!h)return console.warn("\u2757\ufe0f exchangeIntentSel %s must be present on the page for %s to work",a,g),{};f.u=h}},Z.prototype={solder:(a,{g:b})=>({g:b})}];
Y[d]=[S,{rootId:"AggregatorPageCircuit",__$id:3494861560,fqn:"xyz.swapee.wc.IAggregatorPageCircuit",maurice_element_v3:!0}];class Fa extends Y.implements(T,aa,ta,C,{solder:ma,server:oa,render:qa},{classesMap:!0,rootSelector:".AggregatorPageCircuit",stylesheet:"html/styles/AggregatorPageCircuit.css",blockName:"html/AggregatorPageCircuitBlock.html"},{}){};module.exports["34948615600"]=S;module.exports["34948615601"]=S;module.exports["34948615603"]=M;module.exports["34948615604"]=P;module.exports["34948615608"]=Fa;module.exports["349486156011"]=H;module.exports["349486156030"]=G;module.exports["349486156061"]=T;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['3494861560']=module.exports