import Module from './element'

/**@extends {xyz.swapee.wc.AbstractAggregatorPageCircuit}*/
export class AbstractAggregatorPageCircuit extends Module['34948615601'] {}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuit} */
AbstractAggregatorPageCircuit.class=function(){}
/** @type {typeof xyz.swapee.wc.AggregatorPageCircuitPort} */
export const AggregatorPageCircuitPort=Module['34948615603']
/**@extends {xyz.swapee.wc.AbstractAggregatorPageCircuitController}*/
export class AbstractAggregatorPageCircuitController extends Module['34948615604'] {}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitController} */
AbstractAggregatorPageCircuitController.class=function(){}
/** @type {typeof xyz.swapee.wc.AggregatorPageCircuitElement} */
export const AggregatorPageCircuitElement=Module['34948615608']
/** @type {typeof xyz.swapee.wc.AggregatorPageCircuitBuffer} */
export const AggregatorPageCircuitBuffer=Module['349486156011']
/**@extends {xyz.swapee.wc.AbstractAggregatorPageCircuitComputer}*/
export class AbstractAggregatorPageCircuitComputer extends Module['349486156030'] {}
/** @type {typeof xyz.swapee.wc.AbstractAggregatorPageCircuitComputer} */
AbstractAggregatorPageCircuitComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AggregatorPageCircuitController} */
export const AggregatorPageCircuitController=Module['349486156061']