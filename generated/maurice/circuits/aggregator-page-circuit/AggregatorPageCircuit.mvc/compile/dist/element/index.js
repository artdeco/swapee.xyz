/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuit` interface.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuit}
 */
class AbstractAggregatorPageCircuit extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IAggregatorPageCircuit_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitPort}
 */
class AggregatorPageCircuitPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitController` interface.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitController}
 */
class AbstractAggregatorPageCircuitController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IAggregatorPageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitElement}
 */
class AggregatorPageCircuitElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitBuffer}
 */
class AggregatorPageCircuitBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IAggregatorPageCircuitComputer` interface.
 * @extends {xyz.swapee.wc.AbstractAggregatorPageCircuitComputer}
 */
class AbstractAggregatorPageCircuitComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AggregatorPageCircuitController}
 */
class AggregatorPageCircuitController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractAggregatorPageCircuit = AbstractAggregatorPageCircuit
module.exports.AggregatorPageCircuitPort = AggregatorPageCircuitPort
module.exports.AbstractAggregatorPageCircuitController = AbstractAggregatorPageCircuitController
module.exports.AggregatorPageCircuitElement = AggregatorPageCircuitElement
module.exports.AggregatorPageCircuitBuffer = AggregatorPageCircuitBuffer
module.exports.AbstractAggregatorPageCircuitComputer = AbstractAggregatorPageCircuitComputer
module.exports.AggregatorPageCircuitController = AggregatorPageCircuitController

Object.defineProperties(module.exports, {
 'AbstractAggregatorPageCircuit': {get: () => require('./precompile/internal')[34948615601]},
 [34948615601]: {get: () => module.exports['AbstractAggregatorPageCircuit']},
 'AggregatorPageCircuitPort': {get: () => require('./precompile/internal')[34948615603]},
 [34948615603]: {get: () => module.exports['AggregatorPageCircuitPort']},
 'AbstractAggregatorPageCircuitController': {get: () => require('./precompile/internal')[34948615604]},
 [34948615604]: {get: () => module.exports['AbstractAggregatorPageCircuitController']},
 'AggregatorPageCircuitElement': {get: () => require('./precompile/internal')[34948615608]},
 [34948615608]: {get: () => module.exports['AggregatorPageCircuitElement']},
 'AggregatorPageCircuitBuffer': {get: () => require('./precompile/internal')[349486156011]},
 [349486156011]: {get: () => module.exports['AggregatorPageCircuitBuffer']},
 'AbstractAggregatorPageCircuitComputer': {get: () => require('./precompile/internal')[349486156030]},
 [349486156030]: {get: () => module.exports['AbstractAggregatorPageCircuitComputer']},
 'AggregatorPageCircuitController': {get: () => require('./precompile/internal')[349486156061]},
 [349486156061]: {get: () => module.exports['AggregatorPageCircuitController']},
})