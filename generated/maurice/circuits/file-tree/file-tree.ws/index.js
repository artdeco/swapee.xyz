const d=new Date
let _FileTreePage,_fileTreeArcsIds,_fileTreeMethodsIds
// _getFileTree

// const PROD=true
const PROD=false

;({
 FileTreePage:_FileTreePage,
 // getFileTree:_getFileTree,
 fileTreeArcsIds:_fileTreeArcsIds,
 fileTreeMethodsIds:_fileTreeMethodsIds,
}=require(PROD?'xyz/swapee/rc/file-tree/prod':'./file-tree.page'))

const dd=new Date
console.log(`${PROD?`📦`:`📝`} Loaded %s in %sms`,'file-tree',dd.getTime()-d.getTime())

import 'xyz/swapee/rc/file-tree'

export const FileTreePage=/**@type {typeof xyz.swapee.rc.FileTreePage}*/(_FileTreePage)
export const fileTreeArcsIds=/**@type {typeof xyz.swapee.rc.fileTreeArcsIds}*/(_fileTreeArcsIds)
export const fileTreeMethodsIds=/**@type {typeof xyz.swapee.rc.fileTreeMethodsIds}*/(_fileTreeMethodsIds)
// export const getFileTree=/**@type {xyz.swapee.rc.getFileTree}*/(_getFileTree)