/** @extends {xyz.swapee.wc.AbstractFileTree} */
export default class AbstractFileTree extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractFileTreeComputer} */
export class AbstractFileTreeComputer extends (<computer>
   <adapter name="adaptCurrentFilePath">
    <xyz.swapee.wc.IFileEditorCore path />
    <outputs>
     <xyz.swapee.wc.IFileTreeCore currentFilePath />
    </outputs>
   </adapter>
   <adapter name="adaptFilesRotor">
    <xyz.swapee.wc.IFileTreeCore files currentFilePath />
    <outputs>
     <xyz.swapee.wc.IFileTreeCore filesRotor />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractFileTreeController} */
export class AbstractFileTreeController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractFileTreePort} */
export class FileTreePort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractFileTreeView} */
export class AbstractFileTreeView extends (<view>
  <classes>
   <string opt name="Heading" />
   <string opt name="FileOpen" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractFileTreeElement} */
export class AbstractFileTreeElement extends (<element v3 html mv>
 <block src="./FileTree.mvc/src/FileTreeElement/methods/render_.jsx" />
 <inducer src="./FileTree.mvc/src/FileTreeElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractFileTreeHtmlComponent} */
export class AbstractFileTreeHtmlComponent extends (<html-ic>
  <records>
    <record name="IFileTree.FolderStator">
    <string name="name">The name of the folder.</string>
    <number name="count">The total number of all items.</number>
    <number name="fileCount">The number of files.</number>
    <number name="folderCount">The number of folders.</number>
    The folder.
    <is-stator dyn="Folder" component-name="IFileTree" />

    The stator of the _FolderDynamo_.
  </record>
  <record name="IFileTree.FileStator">
    <string name="path">The path to the file.</string>
    <string name="name">The name of the folder.</string>
    <bool name="isOpen">Whether the file is open in the editor.</bool>
    <bool name="isHidden">Whether the file is hidden.</bool>
    <string name="lang">The language.</string>
    The file.
    <is-stator dyn="File" component-name="IFileTree" />

    The stator of the _FileDynamo_.
  </record>
  <record name="IFileTree.FolderBrushes">
    <string name="name">The name of the folder.</string>
    <number name="count">The total number of all items.</number>
    <number name="fileCount">The number of files.</number>
    <number name="folderCount">The number of folders.</number>
    The folder.
    The brushes inside the rotor of the _FolderDynamo_.
  </record>
  <record name="IFileTree.FileBrushes">
    <string name="path">The path to the file.</string>
    <string name="name">The name of the folder.</string>
    <bool name="isOpen">Whether the file is open in the editor.</bool>
    <bool name="isHidden">Whether the file is hidden.</bool>
    <string name="lang">The language.</string>
    The file.
    The brushes inside the rotor of the _FileDynamo_.
  </record>
  </records>

  <connectors>

   <xyz.swapee.wc.IFileEditor via="FileEditor" />
  </connectors>

</html-ic>) { }
// </class-end>