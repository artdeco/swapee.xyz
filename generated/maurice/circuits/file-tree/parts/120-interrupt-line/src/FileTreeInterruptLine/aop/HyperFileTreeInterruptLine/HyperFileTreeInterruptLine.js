import AbstractHyperFileTreeInterruptLine from '../../../../gen/AbstractFileTreeInterruptLine/hyper/AbstractHyperFileTreeInterruptLine'
import FileTreeInterruptLine from '../../FileTreeInterruptLine'
import FileTreeInterruptLineGeneralAspects from '../FileTreeInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperFileTreeInterruptLine} */
export default class extends AbstractHyperFileTreeInterruptLine
 .consults(
  FileTreeInterruptLineGeneralAspects,
 )
 .implements(
  FileTreeInterruptLine,
 )
{}