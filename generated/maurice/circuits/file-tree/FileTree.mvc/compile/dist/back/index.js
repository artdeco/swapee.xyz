/**
 * An abstract class of `xyz.swapee.wc.IFileTree` interface.
 * @extends {xyz.swapee.wc.AbstractFileTree}
 */
class AbstractFileTree extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IFileTree_, providing input
 * pins.
 * @extends {xyz.swapee.wc.FileTreePort}
 */
class FileTreePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeController` interface.
 * @extends {xyz.swapee.wc.AbstractFileTreeController}
 */
class AbstractFileTreeController extends (class {/* lazy-loaded */}) {}
/**
 * The _IFileTree_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.FileTreeHtmlComponent}
 */
class FileTreeHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.FileTreeBuffer}
 */
class FileTreeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * Responsible for generation of component instances to place inside containers.
 * @extends {xyz.swapee.wc.FileTreeGenerator}
 */
class FileTreeGenerator extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractFileTreeComputer}
 */
class AbstractFileTreeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.FileTreeComputer}
 */
class FileTreeComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _IFileTree_.
 * @extends {xyz.swapee.wc.FileTreeProcessor}
 */
class FileTreeProcessor extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.FileTreeController}
 */
class FileTreeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractFileTree = AbstractFileTree
module.exports.FileTreePort = FileTreePort
module.exports.AbstractFileTreeController = AbstractFileTreeController
module.exports.FileTreeHtmlComponent = FileTreeHtmlComponent
module.exports.FileTreeBuffer = FileTreeBuffer
module.exports.FileTreeGenerator = FileTreeGenerator
module.exports.AbstractFileTreeComputer = AbstractFileTreeComputer
module.exports.FileTreeComputer = FileTreeComputer
module.exports.FileTreeProcessor = FileTreeProcessor
module.exports.FileTreeController = FileTreeController