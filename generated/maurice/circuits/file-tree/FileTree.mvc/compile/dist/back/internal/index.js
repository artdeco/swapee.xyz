import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractFileTree}*/
export class AbstractFileTree extends Module['34906682501'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileTree} */
AbstractFileTree.class=function(){}
/** @type {typeof xyz.swapee.wc.FileTreePort} */
export const FileTreePort=Module['34906682503']
/**@extends {xyz.swapee.wc.AbstractFileTreeController}*/
export class AbstractFileTreeController extends Module['34906682504'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeController} */
AbstractFileTreeController.class=function(){}
/** @type {typeof xyz.swapee.wc.FileTreeHtmlComponent} */
export const FileTreeHtmlComponent=Module['349066825010']
/** @type {typeof xyz.swapee.wc.FileTreeBuffer} */
export const FileTreeBuffer=Module['349066825011']
/** @type {typeof xyz.swapee.wc.FileTreeGenerator} */
export const FileTreeGenerator=Module['349066825018']
/**@extends {xyz.swapee.wc.AbstractFileTreeComputer}*/
export class AbstractFileTreeComputer extends Module['349066825030'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeComputer} */
AbstractFileTreeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.FileTreeComputer} */
export const FileTreeComputer=Module['349066825031']
/** @type {typeof xyz.swapee.wc.FileTreeProcessor} */
export const FileTreeProcessor=Module['349066825051']
/** @type {typeof xyz.swapee.wc.back.FileTreeController} */
export const FileTreeController=Module['349066825061']