/**
 * An abstract class of `xyz.swapee.wc.IFileTree` interface.
 * @extends {xyz.swapee.wc.AbstractFileTree}
 */
class AbstractFileTree extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IFileTree_, providing input
 * pins.
 * @extends {xyz.swapee.wc.FileTreePort}
 */
class FileTreePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeController` interface.
 * @extends {xyz.swapee.wc.AbstractFileTreeController}
 */
class AbstractFileTreeController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IFileTree_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.FileTreeElement}
 */
class FileTreeElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.FileTreeBuffer}
 */
class FileTreeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractFileTreeComputer}
 */
class AbstractFileTreeComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _IFileTree_.
 * @extends {xyz.swapee.wc.FileTreeProcessor}
 */
class FileTreeProcessor extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.FileTreeController}
 */
class FileTreeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractFileTree = AbstractFileTree
module.exports.FileTreePort = FileTreePort
module.exports.AbstractFileTreeController = AbstractFileTreeController
module.exports.FileTreeElement = FileTreeElement
module.exports.FileTreeBuffer = FileTreeBuffer
module.exports.AbstractFileTreeComputer = AbstractFileTreeComputer
module.exports.FileTreeProcessor = FileTreeProcessor
module.exports.FileTreeController = FileTreeController

Object.defineProperties(module.exports, {
 'AbstractFileTree': {get: () => require('./precompile/internal')[34906682501]},
 [34906682501]: {get: () => module.exports['AbstractFileTree']},
 'FileTreePort': {get: () => require('./precompile/internal')[34906682503]},
 [34906682503]: {get: () => module.exports['FileTreePort']},
 'AbstractFileTreeController': {get: () => require('./precompile/internal')[34906682504]},
 [34906682504]: {get: () => module.exports['AbstractFileTreeController']},
 'FileTreeElement': {get: () => require('./precompile/internal')[34906682508]},
 [34906682508]: {get: () => module.exports['FileTreeElement']},
 'FileTreeBuffer': {get: () => require('./precompile/internal')[349066825011]},
 [349066825011]: {get: () => module.exports['FileTreeBuffer']},
 'AbstractFileTreeComputer': {get: () => require('./precompile/internal')[349066825030]},
 [349066825030]: {get: () => module.exports['AbstractFileTreeComputer']},
 'FileTreeProcessor': {get: () => require('./precompile/internal')[349066825051]},
 [349066825051]: {get: () => module.exports['FileTreeProcessor']},
 'FileTreeController': {get: () => require('./precompile/internal')[349066825061]},
 [349066825061]: {get: () => module.exports['FileTreeController']},
})