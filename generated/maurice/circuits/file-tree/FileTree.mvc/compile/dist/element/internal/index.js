import Module from './element'

/**@extends {xyz.swapee.wc.AbstractFileTree}*/
export class AbstractFileTree extends Module['34906682501'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileTree} */
AbstractFileTree.class=function(){}
/** @type {typeof xyz.swapee.wc.FileTreePort} */
export const FileTreePort=Module['34906682503']
/**@extends {xyz.swapee.wc.AbstractFileTreeController}*/
export class AbstractFileTreeController extends Module['34906682504'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeController} */
AbstractFileTreeController.class=function(){}
/** @type {typeof xyz.swapee.wc.FileTreeElement} */
export const FileTreeElement=Module['34906682508']
/** @type {typeof xyz.swapee.wc.FileTreeBuffer} */
export const FileTreeBuffer=Module['349066825011']
/**@extends {xyz.swapee.wc.AbstractFileTreeComputer}*/
export class AbstractFileTreeComputer extends Module['349066825030'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeComputer} */
AbstractFileTreeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.FileTreeProcessor} */
export const FileTreeProcessor=Module['349066825051']
/** @type {typeof xyz.swapee.wc.FileTreeController} */
export const FileTreeController=Module['349066825061']