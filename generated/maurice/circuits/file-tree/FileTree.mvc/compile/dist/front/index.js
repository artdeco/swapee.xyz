/**
 * Display for presenting information from the _IFileTree_.
 * @extends {xyz.swapee.wc.FileTreeDisplay}
 */
class FileTreeDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.FileTreeScreen}
 */
class FileTreeScreen extends (class {/* lazy-loaded */}) {}

module.exports.FileTreeDisplay = FileTreeDisplay
module.exports.FileTreeScreen = FileTreeScreen