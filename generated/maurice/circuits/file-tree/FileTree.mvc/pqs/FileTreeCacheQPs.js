import {FileTreeCachePQs} from './FileTreeCachePQs'
export const FileTreeCacheQPs=/**@type {!xyz.swapee.wc.FileTreeCacheQPs}*/(Object.keys(FileTreeCachePQs)
 .reduce((a,k)=>{a[FileTreeCachePQs[k]]=k;return a},{}))