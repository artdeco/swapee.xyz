import {FileTreeInputsPQs} from './FileTreeInputsPQs'
export const FileTreeInputsQPs=/**@type {!xyz.swapee.wc.FileTreeInputsQPs}*/(Object.keys(FileTreeInputsPQs)
 .reduce((a,k)=>{a[FileTreeInputsPQs[k]]=k;return a},{}))