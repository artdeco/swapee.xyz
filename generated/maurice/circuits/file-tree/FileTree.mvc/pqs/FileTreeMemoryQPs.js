import {FileTreeMemoryPQs} from './FileTreeMemoryPQs'
export const FileTreeMemoryQPs=/**@type {!xyz.swapee.wc.FileTreeMemoryQPs}*/(Object.keys(FileTreeMemoryPQs)
 .reduce((a,k)=>{a[FileTreeMemoryPQs[k]]=k;return a},{}))