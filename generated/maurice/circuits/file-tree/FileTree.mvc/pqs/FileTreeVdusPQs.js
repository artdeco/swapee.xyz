export const FileTreeVdusPQs=/**@type {!xyz.swapee.wc.FileTreeVdusPQs}*/({
 FolderTemplate:'bf8d1',
 FileTemplate:'bf8d2',
 FolderLa:'bf8d3',
 ExpandedIc:'bf8d4',
 CollapsedIc:'bf8d5',
 Folders:'bf8d6',
 Files:'bf8d7',
 FolderWr:'bf8d8',
 ContentWr:'bf8d9',
 FileEditor:'bf8d10',
 LoIn:'bf8d11',
})