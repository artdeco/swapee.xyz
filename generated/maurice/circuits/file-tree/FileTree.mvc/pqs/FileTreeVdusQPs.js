import {FileTreeVdusPQs} from './FileTreeVdusPQs'
export const FileTreeVdusQPs=/**@type {!xyz.swapee.wc.FileTreeVdusQPs}*/(Object.keys(FileTreeVdusPQs)
 .reduce((a,k)=>{a[FileTreeVdusPQs[k]]=k;return a},{}))