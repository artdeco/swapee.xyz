import {FileTreeQueriesPQs} from './FileTreeQueriesPQs'
export const FileTreeQueriesQPs=/**@type {!xyz.swapee.wc.FileTreeQueriesQPs}*/(Object.keys(FileTreeQueriesPQs)
 .reduce((a,k)=>{a[FileTreeQueriesPQs[k]]=k;return a},{}))