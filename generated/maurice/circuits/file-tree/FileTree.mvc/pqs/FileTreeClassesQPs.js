import FileTreeClassesPQs from './FileTreeClassesPQs'
export const FileTreeClassesQPs=/**@type {!xyz.swapee.wc.FileTreeClassesQPs}*/(Object.keys(FileTreeClassesPQs)
 .reduce((a,k)=>{a[FileTreeClassesPQs[k]]=k;return a},{}))