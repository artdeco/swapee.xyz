import AbstractFileTreeProcessor from '../AbstractFileTreeProcessor'
import {FileTreeCore} from '../FileTreeCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractFileTreeComputer} from '../AbstractFileTreeComputer'
import {AbstractFileTreeController} from '../AbstractFileTreeController'
import {regulateFileTreeCache} from './methods/regulateFileTreeCache'
import {FileTreeCacheQPs} from '../../pqs/FileTreeCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTree}
 */
function __AbstractFileTree() {}
__AbstractFileTree.prototype = /** @type {!_AbstractFileTree} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTree}
 */
class _AbstractFileTree { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractFileTree} ‎
 */
class AbstractFileTree extends newAbstract(
 _AbstractFileTree,349066825010,null,{
  asIFileTree:1,
  superFileTree:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTree} */
AbstractFileTree.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTree} */
function AbstractFileTreeClass(){}

export default AbstractFileTree


AbstractFileTree[$implementations]=[
 __AbstractFileTree,
 FileTreeCore,
 AbstractFileTreeProcessor,
 IntegratedComponent,
 AbstractFileTreeComputer,
 AbstractFileTreeController,
 AbstractFileTreeClass.prototype=/**@type {!xyz.swapee.wc.IFileTree}*/({
  regulateState:regulateFileTreeCache,
  stateQPs:FileTreeCacheQPs,
 }),
]


export {AbstractFileTree}