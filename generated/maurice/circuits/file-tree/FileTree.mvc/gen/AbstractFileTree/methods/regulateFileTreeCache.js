import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateFileTreeCache=makeBuffers({
 folders:Array,
 files:Array,
 loadingFiles:Boolean,
 hasMoreFiles:Boolean,
 loadFilesError:5,
},{silent:true})