import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeControllerAT}
 */
function __AbstractFileTreeControllerAT() {}
__AbstractFileTreeControllerAT.prototype = /** @type {!_AbstractFileTreeControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractFileTreeControllerAT}
 */
class _AbstractFileTreeControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IFileTreeControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractFileTreeControllerAT} ‎
 */
class AbstractFileTreeControllerAT extends newAbstract(
 _AbstractFileTreeControllerAT,349066825029,null,{
  asIFileTreeControllerAT:1,
  superFileTreeControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractFileTreeControllerAT} */
AbstractFileTreeControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractFileTreeControllerAT} */
function AbstractFileTreeControllerATClass(){}

export default AbstractFileTreeControllerAT


AbstractFileTreeControllerAT[$implementations]=[
 __AbstractFileTreeControllerAT,
 UartUniversal,
 AbstractFileTreeControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IFileTreeControllerAT}*/({
  get asIFileTreeController(){
   return this
  },
  openFile(){
   this.uart.t("inv",{mid:'65109',args:[...arguments]})
  },
  onOpenFile(){
   this.uart.t("inv",{mid:'d119c',args:[...arguments]})
  },
  flipExpanded(){
   this.uart.t("inv",{mid:'cca8b'})
  },
  setExpanded(){
   this.uart.t("inv",{mid:'0716a'})
  },
  unsetExpanded(){
   this.uart.t("inv",{mid:'2151b'})
  },
  loadFiles(){
   this.uart.t("inv",{mid:'af182'})
  },
 }),
]