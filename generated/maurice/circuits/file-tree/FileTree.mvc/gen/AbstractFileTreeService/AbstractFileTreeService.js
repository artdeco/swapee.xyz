import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeService}
 */
function __AbstractFileTreeService() {}
__AbstractFileTreeService.prototype = /** @type {!_AbstractFileTreeService} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeService}
 */
class _AbstractFileTreeService { }
/**
 * A service for the IFileTree.
 * @extends {xyz.swapee.wc.AbstractFileTreeService} ‎
 */
class AbstractFileTreeService extends newAbstract(
 _AbstractFileTreeService,349066825020,null,{
  asIFileTreeService:1,
  superFileTreeService:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeService} */
AbstractFileTreeService.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeService} */
function AbstractFileTreeServiceClass(){}

export default AbstractFileTreeService


AbstractFileTreeService[$implementations]=[
 __AbstractFileTreeService,
]

export {AbstractFileTreeService}