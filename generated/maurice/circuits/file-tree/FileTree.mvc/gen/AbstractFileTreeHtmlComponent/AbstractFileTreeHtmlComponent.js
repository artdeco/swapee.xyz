import AbstractFileTreeGPU from '../AbstractFileTreeGPU'
import AbstractFileTreeScreenBack from '../AbstractFileTreeScreenBack'
import AbstractFileTreeRadio from '../AbstractFileTreeRadio'
import {HtmlComponent,Landed,rotate,makeRevealConcealPaints,mvc} from '@webcircuits/webcircuits'
import {access} from '@mauriceguest/guest2'
import {FileTreeInputsQPs} from '../../pqs/FileTreeInputsQPs'
import { newAbstract, $implementations, create, schedule } from '@type.engineering/type-engineer'
import AbstractFileTree from '../AbstractFileTree'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeHtmlComponent}
 */
function __AbstractFileTreeHtmlComponent() {}
__AbstractFileTreeHtmlComponent.prototype = /** @type {!_AbstractFileTreeHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeHtmlComponent}
 */
class _AbstractFileTreeHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.FileTreeHtmlComponent} */ (res)
  }
}
/**
 * The _IFileTree_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractFileTreeHtmlComponent} ‎
 */
export class AbstractFileTreeHtmlComponent extends newAbstract(
 _AbstractFileTreeHtmlComponent,349066825012,null,{
  asIFileTreeHtmlComponent:1,
  superFileTreeHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeHtmlComponent} */
AbstractFileTreeHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeHtmlComponent} */
function AbstractFileTreeHtmlComponentClass(){}


AbstractFileTreeHtmlComponent[$implementations]=[
 __AbstractFileTreeHtmlComponent,
 HtmlComponent,
 AbstractFileTree,
 AbstractFileTreeGPU,
 AbstractFileTreeScreenBack,
 AbstractFileTreeRadio,
 Landed,
 AbstractFileTreeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeHtmlComponent}*/({
  constructor(){
   this.land={
    FileEditor:null,
   }
  },
 }),
 AbstractFileTreeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeHtmlComponent}*/({
  deduceProps(el){
   const{
    asIBrowserView:{weHaveClass:weHaveClass},
    classes:{
     Heading:Heading,
    },
   }=this

   const heading=weHaveClass(Heading)
   return{
    heading:heading,
   }
  },
 }),
 AbstractFileTreeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeHtmlComponent}*/({
  inputsQPs:FileTreeInputsQPs,
 }),

/** @type {!xyz.swapee.wc.IFileTreeHtmlComponent} */ ({paint:[
  /**@this {!xyz.swapee.wc.IFileTreeHtmlComponent}*/
  function paintFolderDynamo({foldersRotor:foldersRotor}){
   const{
    asIFileTreeGPU:{
     Folders:Folders,
     makeFolderElement:makeFolderElement,
    },
    asIGraphicsDriverBack:{makeVdu:makeVdu},
    asIFileTree:{
     stateFolder:stateFolder,brushFolder:brushFolder,initFolder:initFolder,
    },
   }=this
   rotate(Folders,foldersRotor,{
    create:stateFolder,
    make:()=>{
     const twin=makeVdu()
     makeFolderElement(twin.id)
     return twin
    },
    brush:brushFolder,
    init:initFolder,
    strategy:'reuse',
   })
  },/**@this {!xyz.swapee.wc.IFileTreeHtmlComponent}*/
  function paintFileDynamo({filesRotor:filesRotor}){
   const{
    asIFileTreeGPU:{
     Files:Files,
     makeFileElement:makeFileElement,
    },
    asIGraphicsDriverBack:{makeVdu:makeVdu},
    asIFileTree:{
     stateFile:stateFile,brushFile:brushFile,initFile:initFile,
    },
   }=this
   rotate(Files,filesRotor,{
    create:stateFile,
    make:()=>{
     const twin=makeVdu()
     makeFileElement(twin.id)
     return twin
    },
    brush:brushFile,
    init:initFile,
    strategy:'reuse',
   })
  },
 ] }),

/** @type {!AbstractFileTreeHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IFileTreeHtmlComponent}*/function paintFolderLa() {
     this.FolderLa.setText(this.model.folder)
   }
 ] }),
 AbstractFileTreeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeHtmlComponent}*/({
  paint:function paint_Heading_on_element({heading:heading}){
   const{
    asIFileTreeGPU:{
     element:element,
    },
    classes:{Heading:Heading},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(heading) addClass(element,Heading)
   else removeClass(element,Heading)
  },
 }),
 AbstractFileTreeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeHtmlComponent}*/({
  paint:makeRevealConcealPaints({
   LoIn:{loadingFiles:1},
   ExpandedIc:{expanded:1},
   CollapsedIc:{expanded:0},
   ContentWr:{expanded:1},
  }),
 }),
 AbstractFileTreeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIFileTreeGPU:{
     FileEditor:FileEditor,
    },
   }=this
   complete(9961973136,{FileEditor:FileEditor})
  },
 }),
 AbstractFileTreeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeHtmlComponent}*/({
  paint(_,{FileEditor:FileEditor}){
   const land={FileEditor:FileEditor}
   if(!land.FileEditor) return
   this.onOpenFile=FileEditor['833e7'] // onOpenFile=openFile
  },
 }),
]