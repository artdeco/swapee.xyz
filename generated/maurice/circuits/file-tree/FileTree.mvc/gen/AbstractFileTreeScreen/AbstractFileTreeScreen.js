import FileTreeClassesPQs from '../../pqs/FileTreeClassesPQs'
import AbstractFileTreeScreenAR from '../AbstractFileTreeScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {FileTreeInputsPQs} from '../../pqs/FileTreeInputsPQs'
import {FileTreeQueriesPQs} from '../../pqs/FileTreeQueriesPQs'
import {FileTreeMemoryQPs} from '../../pqs/FileTreeMemoryQPs'
import {FileTreeCacheQPs} from '../../pqs/FileTreeCacheQPs'
import {FileTreeVdusPQs} from '../../pqs/FileTreeVdusPQs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeScreen}
 */
function __AbstractFileTreeScreen() {}
__AbstractFileTreeScreen.prototype = /** @type {!_AbstractFileTreeScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeScreen}
 */
class _AbstractFileTreeScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractFileTreeScreen} ‎
 */
class AbstractFileTreeScreen extends newAbstract(
 _AbstractFileTreeScreen,349066825030,null,{
  asIFileTreeScreen:1,
  superFileTreeScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeScreen} */
AbstractFileTreeScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeScreen} */
function AbstractFileTreeScreenClass(){}

export default AbstractFileTreeScreen


AbstractFileTreeScreen[$implementations]=[
 AbstractFileTreeScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeScreen}*/({
  makeFolderElement(vid){
   const{
    FolderTemplate:FolderTemplate,
    asIScreen:{makeElement:makeElement},
   }=this
   const el=/**@type {!HTMLDivElement}*/(makeElement(FolderTemplate,vid))
   return el
  },
  makeFileElement(vid){
   const{
    FileTemplate:FileTemplate,
    asIScreen:{makeElement:makeElement},
   }=this
   const el=/**@type {!HTMLDivElement}*/(makeElement(FileTemplate,vid))
   return el
  },
 }),
 __AbstractFileTreeScreen,
 AbstractFileTreeScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeScreen}*/({
  inputsPQs:FileTreeInputsPQs,
  classesPQs:FileTreeClassesPQs,
  queriesPQs:FileTreeQueriesPQs,
  memoryQPs:FileTreeMemoryQPs,
  cacheQPs:FileTreeCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractFileTreeScreenAR,
 AbstractFileTreeScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeScreen}*/({
  vdusPQs:FileTreeVdusPQs,
 }),
 AbstractFileTreeScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeScreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const FolderWr=this.FolderWr
    if(FolderWr){
     FolderWr.addEventListener('click',(ev)=>{
      this.flipExpanded()
     })
    }
   })
  },
 }),
 AbstractFileTreeScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeScreen}*/({
  deduceInputs(){
   const{asIFileTreeDisplay:{
    FolderLa:FolderLa,
   }}=this
   return{
    folder:FolderLa?.innerText,
   }
  },
 }),
]