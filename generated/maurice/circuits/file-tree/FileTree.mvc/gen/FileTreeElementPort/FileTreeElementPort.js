import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreeElementPort}
 */
function __FileTreeElementPort() {}
__FileTreeElementPort.prototype = /** @type {!_FileTreeElementPort} */ ({ })
/** @this {xyz.swapee.wc.FileTreeElementPort} */ function FileTreeElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IFileTreeElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    loInOpts: {},
    contentWrOpts: {},
    expandedIcOpts: {},
    collapsedIcOpts: {},
    folderWrOpts: {},
    folderLaOpts: {},
    foldersOpts: {},
    filesOpts: {},
    fileEditorOpts: {},
    folderTemplateOpts: {},
    fileTemplateOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeElementPort}
 */
class _FileTreeElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractFileTreeElementPort} ‎
 */
class FileTreeElementPort extends newAbstract(
 _FileTreeElementPort,349066825014,FileTreeElementPortConstructor,{
  asIFileTreeElementPort:1,
  superFileTreeElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeElementPort} */
FileTreeElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeElementPort} */
function FileTreeElementPortClass(){}

export default FileTreeElementPort


FileTreeElementPort[$implementations]=[
 __FileTreeElementPort,
 FileTreeElementPortClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'lo-in-opts':undefined,
    'content-wr-opts':undefined,
    'expanded-ic-opts':undefined,
    'collapsed-ic-opts':undefined,
    'folder-wr-opts':undefined,
    'folder-la-opts':undefined,
    'folders-opts':undefined,
    'files-opts':undefined,
    'file-editor-opts':undefined,
    'folder-template-opts':undefined,
    'file-template-opts':undefined,
    'show-hidden':undefined,
    'current-file-path':undefined,
    'folders-rotor':undefined,
    'files-rotor':undefined,
   })
  },
 }),
]