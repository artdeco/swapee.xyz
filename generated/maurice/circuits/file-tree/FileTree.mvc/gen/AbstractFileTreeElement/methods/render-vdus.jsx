export default function FileTreeRenderVdus(){
 return (<div $id="FileTree">
  <vdu $template="FolderTemplate" />
  <vdu $template="FileTemplate" />
  <vdu $id="FolderWr" />
  <vdu $id="FolderLa" />
  <vdu $id="LoIn" />
  <vdu $id="ExpandedIc" />
  <vdu $id="CollapsedIc" />
  <vdu $id="ContentWr" />
  <vdu $id="Folders" />
  <vdu $id="Files" />
 </div>)
}