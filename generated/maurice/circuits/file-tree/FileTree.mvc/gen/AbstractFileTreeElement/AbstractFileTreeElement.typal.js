
import AbstractFileTree from '../AbstractFileTree'

/** @abstract {xyz.swapee.wc.IFileTreeElement} */
export default class AbstractFileTreeElement { }



AbstractFileTreeElement[$implementations]=[AbstractFileTree,
 /** @type {!AbstractFileTreeElement} */ ({
  rootId:'FileTree',
  __$id:3490668250,
  fqn:'xyz.swapee.wc.IFileTree',
  maurice_element_v3:true,
 }),
]