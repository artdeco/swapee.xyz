import FileTreeRenderVdus from './methods/render-vdus'
import FileTreeElementPort from '../FileTreeElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {FileTreeInputsPQs} from '../../pqs/FileTreeInputsPQs'
import {FileTreeQueriesPQs} from '../../pqs/FileTreeQueriesPQs'
import {FileTreeCachePQs} from '../../pqs/FileTreeCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractFileTree from '../AbstractFileTree'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeElement}
 */
function __AbstractFileTreeElement() {}
__AbstractFileTreeElement.prototype = /** @type {!_AbstractFileTreeElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeElement}
 */
class _AbstractFileTreeElement { }
/**
 * A component description.
 *
 * The _IFileTree_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractFileTreeElement} ‎
 */
class AbstractFileTreeElement extends newAbstract(
 _AbstractFileTreeElement,349066825013,null,{
  asIFileTreeElement:1,
  superFileTreeElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeElement} */
AbstractFileTreeElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeElement} */
function AbstractFileTreeElementClass(){}

export default AbstractFileTreeElement


AbstractFileTreeElement[$implementations]=[
 __AbstractFileTreeElement,
 ElementBase,
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':host':hostColAttr,
   ':heading':headingColAttr,
   ':root':rootColAttr,
   ':folder':folderColAttr,
   ':show-hidden':showHiddenColAttr,
   ':current-file-path':currentFilePathColAttr,
   ':expanded':expandedColAttr,
   ':folders-rotor':foldersRotorColAttr,
   ':files-rotor':filesRotorColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'host':hostAttr,
    'heading':headingAttr,
    'root':rootAttr,
    'folder':folderAttr,
    'show-hidden':showHiddenAttr,
    'current-file-path':currentFilePathAttr,
    'expanded':expandedAttr,
    'folders-rotor':foldersRotorAttr,
    'files-rotor':filesRotorAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(hostAttr===undefined?{'host':hostColAttr}:{}),
    ...(headingAttr===undefined?{'heading':headingColAttr}:{}),
    ...(rootAttr===undefined?{'root':rootColAttr}:{}),
    ...(folderAttr===undefined?{'folder':folderColAttr}:{}),
    ...(showHiddenAttr===undefined?{'show-hidden':showHiddenColAttr}:{}),
    ...(currentFilePathAttr===undefined?{'current-file-path':currentFilePathColAttr}:{}),
    ...(expandedAttr===undefined?{'expanded':expandedColAttr}:{}),
    ...(foldersRotorAttr===undefined?{'folders-rotor':foldersRotorColAttr}:{}),
    ...(filesRotorAttr===undefined?{'files-rotor':filesRotorColAttr}:{}),
   }
  },
 }),
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'host':hostAttr,
   'heading':headingAttr,
   'root':rootAttr,
   'folder':folderAttr,
   'show-hidden':showHiddenAttr,
   'current-file-path':currentFilePathAttr,
   'expanded':expandedAttr,
   'folders-rotor':foldersRotorAttr,
   'files-rotor':filesRotorAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    host:hostAttr,
    heading:headingAttr,
    root:rootAttr,
    folder:folderAttr,
    showHidden:showHiddenAttr,
    currentFilePath:currentFilePathAttr,
    expanded:expandedAttr,
    foldersRotor:foldersRotorAttr,
    filesRotor:filesRotorAttr,
   }
  },
 }),
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  render:FileTreeRenderVdus,
 }),
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  classes:{
   'Heading': '01013',
   'FileOpen': '499c4',
  },
  inputsPQs:FileTreeInputsPQs,
  queriesPQs:FileTreeQueriesPQs,
  cachePQs:FileTreeCachePQs,
  vdus:{
   'FolderTemplate': 'bf8d1',
   'FileTemplate': 'bf8d2',
   'FolderLa': 'bf8d3',
   'ExpandedIc': 'bf8d4',
   'CollapsedIc': 'bf8d5',
   'Folders': 'bf8d6',
   'Files': 'bf8d7',
   'FolderWr': 'bf8d8',
   'ContentWr': 'bf8d9',
   'FileEditor': 'bf8d10',
   'LoIn': 'bf8d11',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','host','heading','root','folder','showHidden','currentFilePath','expanded','foldersRotor','filesRotor','query:file-editor','no-solder',':no-solder',':host',':heading',':root',':folder','show-hidden',':show-hidden','current-file-path',':current-file-path',':expanded','folders-rotor',':folders-rotor','files-rotor',':files-rotor','fe646','04d01','ca9e1','aef35','a3806','c5c15','ec0b0','073d8','d0bc0','3d88e','0b15a','68bb8','67b3d','f74f5','63a9f','85114','beb5c','c9620','1a613','2d0d7','c87fd','children']),
   })
  },
  get Port(){
   return FileTreeElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:file-editor':fileEditorSel}){
   const _ret={}
   if(fileEditorSel) _ret.fileEditorSel=fileEditorSel
   return _ret
  },
 }),
 Landed,
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  constructor(){
   this.land={
    FileEditor:null,
   }
  },
 }),
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  calibrate:async function awaitOnFileEditor({fileEditorSel:fileEditorSel}){
   if(!fileEditorSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const FileEditor=await milleu(fileEditorSel)
   if(!FileEditor) {
    console.warn('❗️ fileEditorSel %s must be present on the page for %s to work',fileEditorSel,fqn)
    return{}
   }
   land.FileEditor=FileEditor
  },
 }),
 AbstractFileTreeElementClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
  solder:(_,{
   fileEditorSel:fileEditorSel,
  })=>{
   return{
    fileEditorSel:fileEditorSel,
   }
  },
 }),
]



AbstractFileTreeElement[$implementations]=[AbstractFileTree,
 /** @type {!AbstractFileTreeElement} */ ({
  rootId:'FileTree',
  __$id:3490668250,
  fqn:'xyz.swapee.wc.IFileTree',
  maurice_element_v3:true,
 }),
]