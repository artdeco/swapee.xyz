import {mountPins} from '@webcircuits/webcircuits'
import {FileTreeMemoryPQs} from '../../pqs/FileTreeMemoryPQs'
import {FileTreeCachePQs} from '../../pqs/FileTreeCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreeCore}
 */
function __FileTreeCore() {}
__FileTreeCore.prototype = /** @type {!_FileTreeCore} */ ({ })
/** @this {xyz.swapee.wc.FileTreeCore} */ function FileTreeCoreConstructor() {
  /**@type {!xyz.swapee.wc.IFileTreeCore.Model}*/
  this.model={
    folders: [],
    files: [],
    loadingFiles: false,
    hasMoreFiles: null,
    loadFilesError: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeCore}
 */
class _FileTreeCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractFileTreeCore} ‎
 */
class FileTreeCore extends newAbstract(
 _FileTreeCore,34906682508,FileTreeCoreConstructor,{
  asIFileTreeCore:1,
  superFileTreeCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeCore} */
FileTreeCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeCore} */
function FileTreeCoreClass(){}

export default FileTreeCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreeOuterCore}
 */
function __FileTreeOuterCore() {}
__FileTreeOuterCore.prototype = /** @type {!_FileTreeOuterCore} */ ({ })
/** @this {xyz.swapee.wc.FileTreeOuterCore} */
export function FileTreeOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IFileTreeOuterCore.Model}*/
  this.model={
    host: '',
    heading: false,
    root: '',
    folder: '.',
    showHidden: false,
    currentFilePath: '',
    expanded: true,
    foldersRotor: null,
    filesRotor: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeOuterCore}
 */
class _FileTreeOuterCore { }
/**
 * The _IFileTree_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractFileTreeOuterCore} ‎
 */
export class FileTreeOuterCore extends newAbstract(
 _FileTreeOuterCore,34906682503,FileTreeOuterCoreConstructor,{
  asIFileTreeOuterCore:1,
  superFileTreeOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeOuterCore} */
FileTreeOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeOuterCore} */
function FileTreeOuterCoreClass(){}


FileTreeOuterCore[$implementations]=[
 __FileTreeOuterCore,
 FileTreeOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeOuterCore}*/({
  constructor(){
   mountPins(this.model,'',FileTreeMemoryPQs)
   mountPins(this.model,'',FileTreeCachePQs)
  },
 }),
]

FileTreeCore[$implementations]=[
 FileTreeCoreClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeCore}*/({
  resetCore(){
   this.resetFileTreeCore()
  },
  resetFileTreeCore(){
   FileTreeCoreConstructor.call(
    /**@type {xyz.swapee.wc.FileTreeCore}*/(this),
   )
   FileTreeOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.FileTreeOuterCore}*/(
     /**@type {!xyz.swapee.wc.IFileTreeOuterCore}*/(this)),
   )
  },
 }),
 __FileTreeCore,
 FileTreeOuterCore,
]

export {FileTreeCore}