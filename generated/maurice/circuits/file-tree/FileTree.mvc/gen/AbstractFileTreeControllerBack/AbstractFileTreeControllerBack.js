import AbstractFileTreeControllerAR from '../AbstractFileTreeControllerAR'
import {AbstractFileTreeController} from '../AbstractFileTreeController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeControllerBack}
 */
function __AbstractFileTreeControllerBack() {}
__AbstractFileTreeControllerBack.prototype = /** @type {!_AbstractFileTreeControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileTreeController}
 */
class _AbstractFileTreeControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractFileTreeController} ‎
 */
class AbstractFileTreeControllerBack extends newAbstract(
 _AbstractFileTreeControllerBack,349066825027,null,{
  asIFileTreeController:1,
  superFileTreeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeController} */
AbstractFileTreeControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeController} */
function AbstractFileTreeControllerBackClass(){}

export default AbstractFileTreeControllerBack


AbstractFileTreeControllerBack[$implementations]=[
 __AbstractFileTreeControllerBack,
 AbstractFileTreeController,
 AbstractFileTreeControllerAR,
 DriverBack,
]