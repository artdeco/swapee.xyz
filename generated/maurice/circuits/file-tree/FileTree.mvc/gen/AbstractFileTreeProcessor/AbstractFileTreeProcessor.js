import {preadaptLoadFiles} from '../AbstractFileTreeRadio/preadapters'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeProcessor}
 */
function __AbstractFileTreeProcessor() {}
__AbstractFileTreeProcessor.prototype = /** @type {!_AbstractFileTreeProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeProcessor}
 */
class _AbstractFileTreeProcessor { }
/**
 * The processor to compute changes to the memory for the _IFileTree_.
 * @extends {xyz.swapee.wc.AbstractFileTreeProcessor} ‎
 */
class AbstractFileTreeProcessor extends newAbstract(
 _AbstractFileTreeProcessor,34906682509,null,{
  asIFileTreeProcessor:1,
  superFileTreeProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeProcessor} */
AbstractFileTreeProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeProcessor} */
function AbstractFileTreeProcessorClass(){}

export default AbstractFileTreeProcessor


AbstractFileTreeProcessor[$implementations]=[
 __AbstractFileTreeProcessor,
 AbstractFileTreeProcessorClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeProcessor}*/({
  loadFiles(){
   return preadaptLoadFiles.call(this,this.model).then(this.setInfo)
  },
 }),
 AbstractFileTreeProcessorClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeProcessor}*/({
  flipExpanded(){
   const{
    asIFileTreeComputer:{
     model:{expanded:expanded},
     setInfo:setInfo,
    },
   }=this
   setInfo({expanded:!expanded})
  },
 }),
]