import FileTreeBuffer from '../FileTreeBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {FileTreePortConnector} from '../FileTreePort'
import {defineEmitters} from '@mauriceguest/guest2'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeController}
 */
function __AbstractFileTreeController() {}
__AbstractFileTreeController.prototype = /** @type {!_AbstractFileTreeController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeController}
 */
class _AbstractFileTreeController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractFileTreeController} ‎
 */
export class AbstractFileTreeController extends newAbstract(
 _AbstractFileTreeController,349066825025,null,{
  asIFileTreeController:1,
  superFileTreeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeController} */
AbstractFileTreeController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeController} */
function AbstractFileTreeControllerClass(){}


AbstractFileTreeController[$implementations]=[
 AbstractFileTreeControllerClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IFileTreePort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractFileTreeController,
 AbstractFileTreeControllerClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeController}*/({
  openFile:precombined,
 }),
 FileTreeBuffer,
 IntegratedController,
 /**@type {!AbstractFileTreeController}*/(FileTreePortConnector),
 AbstractFileTreeControllerClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeController}*/({
  constructor(){
   defineEmitters(this,{
    onOpenFile:true,
   })
  },
 }),
 AbstractFileTreeControllerClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeController}*/({
  setExpanded(){
   const{asIFileTreeController:{setInputs:setInputs}}=this
   setInputs({expanded:true})
  },
  unsetExpanded(){
   const{asIFileTreeController:{setInputs:setInputs}}=this
   setInputs({expanded:false})
  },
 }),
 AbstractFileTreeControllerClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeController}*/({
  openFile(path,name,lang){
   const{asIFileTreeController:{onOpenFile:onOpenFile}}=this
   onOpenFile(path,name,lang)
  },
 }),
]


export default AbstractFileTreeController