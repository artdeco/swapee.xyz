import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeScreenAR}
 */
function __AbstractFileTreeScreenAR() {}
__AbstractFileTreeScreenAR.prototype = /** @type {!_AbstractFileTreeScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractFileTreeScreenAR}
 */
class _AbstractFileTreeScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IFileTreeScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractFileTreeScreenAR} ‎
 */
class AbstractFileTreeScreenAR extends newAbstract(
 _AbstractFileTreeScreenAR,349066825032,null,{
  asIFileTreeScreenAR:1,
  superFileTreeScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractFileTreeScreenAR} */
AbstractFileTreeScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractFileTreeScreenAR} */
function AbstractFileTreeScreenARClass(){}

export default AbstractFileTreeScreenAR


AbstractFileTreeScreenAR[$implementations]=[
 __AbstractFileTreeScreenAR,
 AR,
 AbstractFileTreeScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IFileTreeScreenAR}*/({
  allocator(){
   this.methods={
    makeFolderElement:'28d6b',
    makeFileElement:'069c7',
   }
  },
 }),
]
export {AbstractFileTreeScreenAR}