import AbstractFileTreeScreenAT from '../AbstractFileTreeScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeScreenBack}
 */
function __AbstractFileTreeScreenBack() {}
__AbstractFileTreeScreenBack.prototype = /** @type {!_AbstractFileTreeScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileTreeScreen}
 */
class _AbstractFileTreeScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractFileTreeScreen} ‎
 */
class AbstractFileTreeScreenBack extends newAbstract(
 _AbstractFileTreeScreenBack,349066825031,null,{
  asIFileTreeScreen:1,
  superFileTreeScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeScreen} */
AbstractFileTreeScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeScreen} */
function AbstractFileTreeScreenBackClass(){}

export default AbstractFileTreeScreenBack


AbstractFileTreeScreenBack[$implementations]=[
 __AbstractFileTreeScreenBack,
 AbstractFileTreeScreenAT,
]