import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeComputer}
 */
function __AbstractFileTreeComputer() {}
__AbstractFileTreeComputer.prototype = /** @type {!_AbstractFileTreeComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeComputer}
 */
class _AbstractFileTreeComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractFileTreeComputer} ‎
 */
export class AbstractFileTreeComputer extends newAbstract(
 _AbstractFileTreeComputer,34906682501,null,{
  asIFileTreeComputer:1,
  superFileTreeComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeComputer} */
AbstractFileTreeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeComputer} */
function AbstractFileTreeComputerClass(){}


AbstractFileTreeComputer[$implementations]=[
 __AbstractFileTreeComputer,
 Adapter,
]


export default AbstractFileTreeComputer