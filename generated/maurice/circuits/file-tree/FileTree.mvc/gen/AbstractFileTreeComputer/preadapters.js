
/**@this {xyz.swapee.wc.IFileTreeComputer}*/
export function preadaptFiles(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileTreeComputer.adaptFiles.Form}*/
 const _inputs={
  root:inputs.root,
  folder:inputs.folder,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptFiles(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IFileTreeComputer}*/
export function preadaptCurrentFilePath(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Form}*/
 const _inputs={
  path:this.land.FileEditor?this.land.FileEditor.model['d6fe1']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCurrentFilePath(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IFileTreeComputer}*/
export function preadaptFilesRotor(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Form}*/
 const _inputs={
  files:inputs.files,
  currentFilePath:inputs.currentFilePath,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptFilesRotor(__inputs,__changes)
 return RET
}