import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {FileTreeInputsPQs} from '../../pqs/FileTreeInputsPQs'
import {FileTreeOuterCoreConstructor} from '../FileTreeCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreePort}
 */
function __FileTreePort() {}
__FileTreePort.prototype = /** @type {!_FileTreePort} */ ({ })
/** @this {xyz.swapee.wc.FileTreePort} */ function FileTreePortConstructor() {
  const self=/** @type {!xyz.swapee.wc.FileTreeOuterCore} */ ({model:null})
  FileTreeOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreePort}
 */
class _FileTreePort { }
/**
 * The port that serves as an interface to the _IFileTree_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractFileTreePort} ‎
 */
export class FileTreePort extends newAbstract(
 _FileTreePort,34906682505,FileTreePortConstructor,{
  asIFileTreePort:1,
  superFileTreePort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreePort} */
FileTreePort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreePort} */
function FileTreePortClass(){}

export const FileTreePortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IFileTree.Pinout>}*/({
 get Port() { return FileTreePort },
})

FileTreePort[$implementations]=[
 FileTreePortClass.prototype=/**@type {!xyz.swapee.wc.IFileTreePort}*/({
  resetPort(){
   this.resetFileTreePort()
  },
  resetFileTreePort(){
   FileTreePortConstructor.call(this)
  },
 }),
 __FileTreePort,
 Parametric,
 FileTreePortClass.prototype=/**@type {!xyz.swapee.wc.IFileTreePort}*/({
  constructor(){
   mountPins(this.inputs,'',FileTreeInputsPQs)
  },
 }),
]


export default FileTreePort