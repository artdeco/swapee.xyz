import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeDisplay}
 */
function __AbstractFileTreeDisplay() {}
__AbstractFileTreeDisplay.prototype = /** @type {!_AbstractFileTreeDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileTreeDisplay}
 */
class _AbstractFileTreeDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractFileTreeDisplay} ‎
 */
class AbstractFileTreeDisplay extends newAbstract(
 _AbstractFileTreeDisplay,349066825024,null,{
  asIFileTreeDisplay:1,
  superFileTreeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeDisplay} */
AbstractFileTreeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeDisplay} */
function AbstractFileTreeDisplayClass(){}

export default AbstractFileTreeDisplay


AbstractFileTreeDisplay[$implementations]=[
 __AbstractFileTreeDisplay,
 GraphicsDriverBack,
 AbstractFileTreeDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IFileTreeDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IFileTreeDisplay}*/({
    FolderTemplate:twinMock,
    FileTemplate:twinMock,
    FolderWr:twinMock,
    FolderLa:twinMock,
    LoIn:twinMock,
    ExpandedIc:twinMock,
    CollapsedIc:twinMock,
    ContentWr:twinMock,
    Folders:twinMock,
    Files:twinMock,
    FileEditor:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.FileTreeDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IFileTreeDisplay.Initialese}*/({
   LoIn:1,
   ContentWr:1,
   ExpandedIc:1,
   CollapsedIc:1,
   FolderWr:1,
   FolderLa:1,
   Folders:1,
   Files:1,
   FileEditor:1,
   FolderTemplate:1,
   FileTemplate:1,
  }),
  initializer({
   LoIn:_LoIn,
   ContentWr:_ContentWr,
   ExpandedIc:_ExpandedIc,
   CollapsedIc:_CollapsedIc,
   FolderWr:_FolderWr,
   FolderLa:_FolderLa,
   Folders:_Folders,
   Files:_Files,
   FileEditor:_FileEditor,
   FolderTemplate:_FolderTemplate,
   FileTemplate:_FileTemplate,
  }) {
   if(_LoIn!==undefined) this.LoIn=_LoIn
   if(_ContentWr!==undefined) this.ContentWr=_ContentWr
   if(_ExpandedIc!==undefined) this.ExpandedIc=_ExpandedIc
   if(_CollapsedIc!==undefined) this.CollapsedIc=_CollapsedIc
   if(_FolderWr!==undefined) this.FolderWr=_FolderWr
   if(_FolderLa!==undefined) this.FolderLa=_FolderLa
   if(_Folders!==undefined) this.Folders=_Folders
   if(_Files!==undefined) this.Files=_Files
   if(_FileEditor!==undefined) this.FileEditor=_FileEditor
   if(_FolderTemplate!==undefined) this.FolderTemplate=_FolderTemplate
   if(_FileTemplate!==undefined) this.FileTemplate=_FileTemplate
  },
 }),
]