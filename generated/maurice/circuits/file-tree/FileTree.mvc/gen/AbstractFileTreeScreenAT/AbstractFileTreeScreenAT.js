import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeScreenAT}
 */
function __AbstractFileTreeScreenAT() {}
__AbstractFileTreeScreenAT.prototype = /** @type {!_AbstractFileTreeScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileTreeScreenAT}
 */
class _AbstractFileTreeScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IFileTreeScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractFileTreeScreenAT} ‎
 */
class AbstractFileTreeScreenAT extends newAbstract(
 _AbstractFileTreeScreenAT,349066825033,null,{
  asIFileTreeScreenAT:1,
  superFileTreeScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeScreenAT} */
AbstractFileTreeScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeScreenAT} */
function AbstractFileTreeScreenATClass(){}

export default AbstractFileTreeScreenAT


AbstractFileTreeScreenAT[$implementations]=[
 __AbstractFileTreeScreenAT,
 UartUniversal,
 AbstractFileTreeScreenATClass.prototype=/**@type {!xyz.swapee.wc.back.IFileTreeScreenAT}*/({
  makeFolderElement(){
   const{asIFileTreeScreenAT:{uart:uart}}=this
   uart.t('inv',{mid:'28d6b',args:[...arguments]})
  },
  makeFileElement(){
   const{asIFileTreeScreenAT:{uart:uart}}=this
   uart.t('inv',{mid:'069c7',args:[...arguments]})
  },
 }),
]