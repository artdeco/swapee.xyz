import AbstractFileTreeDisplay from '../AbstractFileTreeDisplayBack'
import FileTreeClassesPQs from '../../pqs/FileTreeClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {FileTreeClassesQPs} from '../../pqs/FileTreeClassesQPs'
import {FileTreeVdusPQs} from '../../pqs/FileTreeVdusPQs'
import {FileTreeVdusQPs} from '../../pqs/FileTreeVdusQPs'
import {FileTreeMemoryPQs} from '../../pqs/FileTreeMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeGPU}
 */
function __AbstractFileTreeGPU() {}
__AbstractFileTreeGPU.prototype = /** @type {!_AbstractFileTreeGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeGPU}
 */
class _AbstractFileTreeGPU { }
/**
 * Handles the periphery of the _IFileTreeDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractFileTreeGPU} ‎
 */
class AbstractFileTreeGPU extends newAbstract(
 _AbstractFileTreeGPU,349066825021,null,{
  asIFileTreeGPU:1,
  superFileTreeGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeGPU} */
AbstractFileTreeGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeGPU} */
function AbstractFileTreeGPUClass(){}

export default AbstractFileTreeGPU


AbstractFileTreeGPU[$implementations]=[
 __AbstractFileTreeGPU,
 AbstractFileTreeGPUClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeGPU}*/({
  classesQPs:FileTreeClassesQPs,
  vdusPQs:FileTreeVdusPQs,
  vdusQPs:FileTreeVdusQPs,
  memoryPQs:FileTreeMemoryPQs,
 }),
 AbstractFileTreeDisplay,
 BrowserView,
 AbstractFileTreeGPUClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeGPU}*/({
  allocator(){
   pressFit(this.classes,'',FileTreeClassesPQs)
  },
 }),
]