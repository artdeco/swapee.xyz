import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeControllerAR}
 */
function __AbstractFileTreeControllerAR() {}
__AbstractFileTreeControllerAR.prototype = /** @type {!_AbstractFileTreeControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileTreeControllerAR}
 */
class _AbstractFileTreeControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IFileTreeControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractFileTreeControllerAR} ‎
 */
class AbstractFileTreeControllerAR extends newAbstract(
 _AbstractFileTreeControllerAR,349066825028,null,{
  asIFileTreeControllerAR:1,
  superFileTreeControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeControllerAR} */
AbstractFileTreeControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileTreeControllerAR} */
function AbstractFileTreeControllerARClass(){}

export default AbstractFileTreeControllerAR


AbstractFileTreeControllerAR[$implementations]=[
 __AbstractFileTreeControllerAR,
 AR,
 AbstractFileTreeControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IFileTreeControllerAR}*/({
  allocator(){
   this.methods={
    openFile:'65109',
    onOpenFile:'d119c',
    flipExpanded:'cca8b',
    setExpanded:'0716a',
    unsetExpanded:'2151b',
    loadFiles:'af182',
   }
  },
 }),
]