import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeDisplay}
 */
function __AbstractFileTreeDisplay() {}
__AbstractFileTreeDisplay.prototype = /** @type {!_AbstractFileTreeDisplay} */ ({ })
/** @this {xyz.swapee.wc.FileTreeDisplay} */ function FileTreeDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.LoIn=null
  /** @type {HTMLDivElement} */ this.ContentWr=null
  /** @type {HTMLDivElement} */ this.ExpandedIc=null
  /** @type {HTMLDivElement} */ this.CollapsedIc=null
  /** @type {HTMLDivElement} */ this.FolderWr=null
  /** @type {HTMLSpanElement} */ this.FolderLa=null
  /** @type {HTMLDivElement} */ this.Folders=null
  /** @type {HTMLDivElement} */ this.Files=null
  /** @type {HTMLElement} */ this.FileEditor=null
  /** @type {HTMLDivElement} */ this.FolderTemplate=null
  /** @type {HTMLDivElement} */ this.FileTemplate=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeDisplay}
 */
class _AbstractFileTreeDisplay { }
/**
 * Display for presenting information from the _IFileTree_.
 * @extends {xyz.swapee.wc.AbstractFileTreeDisplay} ‎
 */
class AbstractFileTreeDisplay extends newAbstract(
 _AbstractFileTreeDisplay,349066825022,FileTreeDisplayConstructor,{
  asIFileTreeDisplay:1,
  superFileTreeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeDisplay} */
AbstractFileTreeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeDisplay} */
function AbstractFileTreeDisplayClass(){}

export default AbstractFileTreeDisplay


AbstractFileTreeDisplay[$implementations]=[
 __AbstractFileTreeDisplay,
 Display,
 AbstractFileTreeDisplayClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{fileEditorScopeSel:fileEditorScopeSel}}=this
    this.scan({
     fileEditorSel:fileEditorScopeSel,
    })
   })
  },
  scan:function vduScan({fileEditorSel:fileEditorSelScope}){
   const{element:element,asIFileTreeScreen:{vdusPQs:{
    FolderTemplate:FolderTemplate,
    FileTemplate:FileTemplate,
    FolderWr:FolderWr,
    FolderLa:FolderLa,
    LoIn:LoIn,
    ExpandedIc:ExpandedIc,
    CollapsedIc:CollapsedIc,
    ContentWr:ContentWr,
    Folders:Folders,
    Files:Files,
   }},queries:{fileEditorSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const FileEditor=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _FileEditor=fileEditorSel?root.querySelector(fileEditorSel):void 0
    return _FileEditor
   })(fileEditorSelScope)
   Object.assign(this,{
    FolderTemplate:/**@type {HTMLDivElement}*/(children[FolderTemplate]),
    FileTemplate:/**@type {HTMLDivElement}*/(children[FileTemplate]),
    FolderWr:/**@type {HTMLDivElement}*/(children[FolderWr]),
    FolderLa:/**@type {HTMLSpanElement}*/(children[FolderLa]),
    LoIn:/**@type {HTMLSpanElement}*/(children[LoIn]),
    ExpandedIc:/**@type {HTMLDivElement}*/(children[ExpandedIc]),
    CollapsedIc:/**@type {HTMLDivElement}*/(children[CollapsedIc]),
    ContentWr:/**@type {HTMLDivElement}*/(children[ContentWr]),
    Folders:/**@type {HTMLDivElement}*/(children[Folders]),
    Files:/**@type {HTMLDivElement}*/(children[Files]),
    FileEditor:FileEditor,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.FileTreeDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IFileTreeDisplay.Initialese}*/({
   LoIn:1,
   ContentWr:1,
   ExpandedIc:1,
   CollapsedIc:1,
   FolderWr:1,
   FolderLa:1,
   Folders:1,
   Files:1,
   FileEditor:1,
   FolderTemplate:1,
   FileTemplate:1,
  }),
  initializer({
   LoIn:_LoIn,
   ContentWr:_ContentWr,
   ExpandedIc:_ExpandedIc,
   CollapsedIc:_CollapsedIc,
   FolderWr:_FolderWr,
   FolderLa:_FolderLa,
   Folders:_Folders,
   Files:_Files,
   FileEditor:_FileEditor,
   FolderTemplate:_FolderTemplate,
   FileTemplate:_FileTemplate,
  }) {
   if(_LoIn!==undefined) this.LoIn=_LoIn
   if(_ContentWr!==undefined) this.ContentWr=_ContentWr
   if(_ExpandedIc!==undefined) this.ExpandedIc=_ExpandedIc
   if(_CollapsedIc!==undefined) this.CollapsedIc=_CollapsedIc
   if(_FolderWr!==undefined) this.FolderWr=_FolderWr
   if(_FolderLa!==undefined) this.FolderLa=_FolderLa
   if(_Folders!==undefined) this.Folders=_Folders
   if(_Files!==undefined) this.Files=_Files
   if(_FileEditor!==undefined) this.FileEditor=_FileEditor
   if(_FolderTemplate!==undefined) this.FolderTemplate=_FolderTemplate
   if(_FileTemplate!==undefined) this.FileTemplate=_FileTemplate
  },
 }),
]