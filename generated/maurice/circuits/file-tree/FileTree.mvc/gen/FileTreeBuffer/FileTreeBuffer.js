import {makeBuffers} from '@webcircuits/webcircuits'

export const FileTreeBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  host:String,
  heading:Boolean,
  root:String,
  folder:String,
  showHidden:Boolean,
  currentFilePath:String,
  expanded:Boolean,
  foldersRotor:Array,
  filesRotor:Array,
 }),
})

export default FileTreeBuffer