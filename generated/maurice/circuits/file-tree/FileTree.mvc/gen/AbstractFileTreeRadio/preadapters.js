
/**@this {xyz.swapee.wc.IFileTreeRadio}*/
export function preadaptLoadFiles(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles.Form}*/
 const _inputs={
  host:inputs.host,
  root:inputs.root,
  folder:inputs.folder,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadFiles(__inputs,__changes)
 return RET
}