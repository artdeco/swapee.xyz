import {preadaptLoadFiles} from './preadapters'
import {makeLoaders} from '@webcircuits/webcircuits'
import {StatefulLoader} from '@mauriceguest/guest2'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeRadio}
 */
function __AbstractFileTreeRadio() {}
__AbstractFileTreeRadio.prototype = /** @type {!_AbstractFileTreeRadio} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeRadio}
 */
class _AbstractFileTreeRadio { }
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @extends {xyz.swapee.wc.AbstractFileTreeRadio} ‎
 */
class AbstractFileTreeRadio extends newAbstract(
 _AbstractFileTreeRadio,349066825018,null,{
  asIFileTreeRadio:1,
  superFileTreeRadio:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeRadio} */
AbstractFileTreeRadio.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeRadio} */
function AbstractFileTreeRadioClass(){}

export default AbstractFileTreeRadio


AbstractFileTreeRadio[$implementations]=[
 __AbstractFileTreeRadio,
 makeLoaders(3490668250,{
  adaptLoadFiles:[
   '6453b',
   {
    root:'63a9f',
    folder:'85114',
   },
   {
    files:'45b96',
    folders:'7cb31',
   },
   {
    loadingFiles:1,
    loadFilesError:2,
   },
  ],
 }),
 StatefulLoader,
 {adapt:[preadaptLoadFiles]},
]