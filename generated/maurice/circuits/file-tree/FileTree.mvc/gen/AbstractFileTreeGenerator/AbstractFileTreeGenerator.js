import {Stator} from '@webcircuits/webcircuits'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreeGenerator}
 */
function __AbstractFileTreeGenerator() {}
__AbstractFileTreeGenerator.prototype = /** @type {!_AbstractFileTreeGenerator} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileTreeGenerator}
 */
class _AbstractFileTreeGenerator { }
/**
 * Responsible for generation of component instances to place inside containers.
 * @extends {xyz.swapee.wc.AbstractFileTreeGenerator} ‎
 */
class AbstractFileTreeGenerator extends newAbstract(
 _AbstractFileTreeGenerator,349066825017,null,{
  asIFileTreeGenerator:1,
  superFileTreeGenerator:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileTreeGenerator} */
AbstractFileTreeGenerator.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileTreeGenerator} */
function AbstractFileTreeGeneratorClass(){}

export default AbstractFileTreeGenerator


AbstractFileTreeGenerator[$implementations]=[
 __AbstractFileTreeGenerator,
 AbstractFileTreeGeneratorClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeGenerator}*/({
  brushFolder:precombined,
  paintFolder:precombined,
  assignFolder:precombined,
  initFolder:precombined,
  brushFile:precombined,
  paintFile:precombined,
  assignFile:precombined,
  initFile:precombined,
 }),
 AbstractFileTreeGeneratorClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeGenerator}*/({
  paintFile({name:name},element){
   element.select('[data-id=FileName]').setText(name)
  },
 }),
 AbstractFileTreeGeneratorClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeGenerator}*/({
  paintFile({isOpen:isOpen,name:name,path:path,lang:lang},element){
   const{
    asIFileTreeHtmlComponent:{
     classes:{FileOpen:FileOpen},
     asIBrowserView:{addClass:addClass,removeClass:removeClass},
    },
   }=this
   if(isOpen) addClass(element,FileOpen)
   else removeClass(element,FileOpen)
  },
 }),
 AbstractFileTreeGeneratorClass.prototype=/**@type {!xyz.swapee.wc.IFileTreeGenerator}*/({
  paintFolder({name:name},element){
   element.select('[data-id=FolderName]').setText(name)
  },
  brushFolder(brushes,stator){
   const{asIFileTreeGenerator:{paintFolder:paintFolder}}=this
   stator.setState(brushes)
   paintFolder(stator.state,stator.element)
  },
  initFolder(stator,el){
   const{asIFileTreeGenerator:{assignFolder:assignFolder},land:land}=this
   assignFolder(stator.state,el,land)
  },
  assignFile(state,el){
   el.listen('click',()=>{
    const{
     asIFileTreeHtmlComponent:{
      asIFileTreeController:{openFile:openFile},
     },
    }=this
    openFile(state.path,state.name,state.lang)
   })
  },
  brushFile(brushes,stator){
   const{asIFileTreeGenerator:{paintFile:paintFile}}=this
   stator.setState(brushes)
   paintFile(stator.state,stator.element)
  },
  initFile(stator,el){
   const{asIFileTreeGenerator:{assignFile:assignFile},land:land}=this
   assignFile(stator.state,el,land)
  },
  stateFolder(el){
   const stator=new Stator({
    element:el,
    state:{
     name:void 0,count:void 0,fileCount:void 0,folderCount:void 0,
    },
    dynamo:/**@type {!engineering.type.seers.ISeen}*/(this),
   })

   return stator
  },
  stateFile(el){
   const stator=new Stator({
    element:el,
    state:{
     path:void 0,name:void 0,isOpen:void 0,isHidden:void 0,lang:void 0,
    },
    dynamo:/**@type {!engineering.type.seers.ISeen}*/(this),
   })

   return stator
  },
 }),
]

export {AbstractFileTreeGenerator}