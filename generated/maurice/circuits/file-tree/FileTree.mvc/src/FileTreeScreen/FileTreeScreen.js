import AbstractFileTreeControllerAT from '../../gen/AbstractFileTreeControllerAT'
import FileTreeDisplay from '../FileTreeDisplay'
import AbstractFileTreeScreen from '../../gen/AbstractFileTreeScreen'

/** @extends {xyz.swapee.wc.FileTreeScreen} */
export default class extends AbstractFileTreeScreen.implements(
 AbstractFileTreeControllerAT,
 FileTreeDisplay,
 /**@type {!xyz.swapee.wc.IFileTreeScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IFileTreeScreen} */ ({
  __$id:3490668250,
 }),
/**@type {!xyz.swapee.wc.IFileTreeScreen}*/({
   // deduceInputs(el) {},
  }),
){}