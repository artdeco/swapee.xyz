/** @type {xyz.swapee.wc.IFileTreeComputer._adaptFilesRotor} */
export default function adaptFilesRotor({files:files,currentFilePath:currentFilePath}) {
 return{
  filesRotor:files.map(({name:name,path:path})=>{
   const[,lang='']=/\.([^.]+)$/.exec(name)||[]
   return {
    name,path,
    isHidden:name.startsWith('.'),
    lang:lang,
    isOpen:currentFilePath==path,
   }}),
 }
}