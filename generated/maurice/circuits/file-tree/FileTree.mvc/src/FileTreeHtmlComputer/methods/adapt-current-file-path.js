/** @type {xyz.swapee.wc.IFileTreeComputer._adaptCurrentFilePath} */
export default function adaptCurrentFilePath({path:path}) {
 return {
  currentFilePath:path,
 }
}