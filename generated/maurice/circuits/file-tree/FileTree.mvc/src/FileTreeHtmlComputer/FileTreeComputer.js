import adaptCurrentFilePath from './methods/adapt-current-file-path'
import adaptFilesRotor from './methods/adapt-files-rotor'
import {preadaptCurrentFilePath,preadaptFilesRotor} from '../../gen/AbstractFileTreeComputer/preadapters'
import AbstractFileTreeComputer from '../../gen/AbstractFileTreeComputer'

/** @extends {xyz.swapee.wc.FileTreeComputer} */
export default class FileTreeHtmlComputer extends AbstractFileTreeComputer.implements(
 /** @type {!xyz.swapee.wc.IFileTreeComputer} */ ({
  adaptCurrentFilePath:adaptCurrentFilePath,
  adaptFilesRotor:adaptFilesRotor,
  adapt:[preadaptCurrentFilePath,preadaptFilesRotor],
 }),
){}