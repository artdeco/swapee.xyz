import openFile from './methods/open-file'
import AbstractFileTreeProcessor from '../../gen/AbstractFileTreeProcessor'

const FileTreeSharedProcessor=AbstractFileTreeProcessor.__trait(
 /** @type {!xyz.swapee.wc.IFileTreeProcessor} */ ({
  openFile:openFile,
 }),
)
export default FileTreeSharedProcessor