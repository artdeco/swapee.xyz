import FileTreeHtmlController from '../FileTreeHtmlController'
import FileTreeHtmlComputer from '../FileTreeHtmlComputer'
import FileTreeHtmlProcessor from '../FileTreeHtmlProcessor'
import FileTreeGenerator from '../FileTreeGenerator'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractFileTreeHtmlComponent} from '../../gen/AbstractFileTreeHtmlComponent'

/** @extends {xyz.swapee.wc.FileTreeHtmlComponent} */
export default class extends AbstractFileTreeHtmlComponent.implements(
 FileTreeHtmlController,
 FileTreeHtmlComputer,
 FileTreeHtmlProcessor,
 FileTreeGenerator,
 IntegratedComponentInitialiser,
){}