import '@type.engineering/type-engineer'
import {readdir, lstat} from 'fs/promises'
import {join} from 'path'

/**@type {xyz.swapee.wc.IFileTreeService._filterFiles} */
export default async function filterFiles({root:root,folder:folder}) {
 const path=join(root,folder)
 const res=await readdir(path)
 /** @type {Array<xyz.swapee.wc.IFileTree.FolderBrushes>} */
 const folders=[]
 /** @type {Array<xyz.swapee.wc.IFileTree.FileBrushes>} */
 const files=[]
 for(const item of res) {
  const p=join(path,item)
  const s=await lstat(p)
  if(s.isFile()) {
   files.push({
    name:item,path:p,
   })
  }
  if(s.isDirectory()) folders.push({
   name:item,path:p,
  })
 }
 return{folders:folders,files:files}
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZmlsZS10cmVlL2ZpbGUtdHJlZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBc05HLE1BQU0sU0FBUyxXQUFXLEVBQUUsU0FBUyxDQUFDLGFBQWEsQ0FBQztDQUNuRCxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0NBQ3JCLE1BQU0sR0FBRyxDQUFDLE1BQU0sT0FBTyxDQUFDOztDQUV4QixNQUFNLE9BQU8sQ0FBQzs7Q0FFZCxNQUFNLEtBQUssQ0FBQztDQUNaLEdBQUcsQ0FBQyxNQUFNLEtBQUssR0FBRztFQUNqQixNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0VBQ2xCLE1BQU0sQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDO0VBQ3BCLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO0dBQ1gsS0FBSyxDQUFDLElBQUk7SUFDVCxTQUFTLENBQUMsTUFBTTtBQUNwQixJQUFJO0FBQ0o7RUFDRSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxJQUFJO0dBQy9CLFNBQVMsQ0FBQyxNQUFNO0FBQ25CLEdBQUc7QUFDSDtDQUNDLE9BQU8sZUFBZSxDQUFDLFdBQVc7QUFDbkMsQ0FBRiJ9