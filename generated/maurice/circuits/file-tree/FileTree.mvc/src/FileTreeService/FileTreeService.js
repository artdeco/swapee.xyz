import AbstractFileTreeService from '../../gen/AbstractFileTreeService'
import filterFiles from './methods/filterFiles/filter-files'

/**@extends {xyz.swapee.wc.FileTreeService} */
export default class extends AbstractFileTreeService.implements(
 AbstractFileTreeService.class.prototype=/**@type {!xyz.swapee.wc.FileTreeService}*/({
  filterFiles:filterFiles,
 }),
){}