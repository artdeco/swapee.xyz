
/**@type {xyz.swapee.wc.IFileTreeDesigner._relay} */
export default function relay({FileTree,This,FileEditor}) {
 return h(Fragment,{},
  h(This,{ onOpenFile:FileEditor.openFile })
 )
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZmlsZS10cmVlL2ZpbGUtdHJlZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBNk9HLFNBQVMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO0NBQ3hDLE9BQU8sWUFBQztFQUNQLFNBQU0sV0FBWSxVQUFVLENBQUMsUUFBUztDQUN2QztBQUNELENBQUYifQ==