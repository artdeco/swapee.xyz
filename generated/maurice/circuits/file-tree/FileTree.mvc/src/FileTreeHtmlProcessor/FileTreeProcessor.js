import FileTreeSharedProcessor from '../FileTreeSharedProcessor'
import AbstractFileTreeProcessor from '../../gen/AbstractFileTreeProcessor'

/** @extends {xyz.swapee.wc.FileTreeProcessor} */
export default class extends AbstractFileTreeProcessor.implements(
 FileTreeSharedProcessor,
){}