import {FolderDynamo} from '../../../../../../../../maurice/circuits/file-tree/dyns/folder-dynamo'
import {FileDynamo} from '../../../../../../../../maurice/circuits/file-tree/dyns/file-dynamo'
import {landProxy} from '@mauriceguest/guest2'

/** @type {xyz.swapee.wc.IFileTreeElement._render} */
export default function FileTreeRender({
 foldersRotor,filesRotor,folder,expanded,heading,
 loadingFiles:loadingFiles,
},{makeFolderElement,makeFileElement,flipExpanded}) { // todo: flip method in addition to setCollapsed/expanded
 return (<div $id="FileTree" Heading={heading}>
  <div $id="FolderWr" onClick={flipExpanded}>
   <span $id="FolderLa">{folder}</span>
  </div>
  <span $id="FolderLa">{folder}</span>
  <span $id="LoIn" $reveal={loadingFiles}/>
  <span $id="ExpandedIc" $reveal={expanded}/>
  <span $id="CollapsedIc" $conceal={expanded}/>
  <span $id="ContentWr" $reveal={expanded}/>
  <div $id="Folders" $noc $rotor={foldersRotor} $dyn="Folder">
   {foldersRotor&&foldersRotor.map(state=>{
    return FolderDynamo.call(null,state,/**@type {!xyz.swapee.wc.FileTreeLand}*/(landProxy))
   })}
  </div>
  <div $id="Files" $noc $rotor={filesRotor} $dyn="File">
   {filesRotor&&filesRotor.map(state=>{
    return FileDynamo.call(null,state,/**@type {!xyz.swapee.wc.FileTreeLand}*/(landProxy))
   })}
  </div>
 </div>)
}