import {FolderDynamo} from '../../../../../../../../maurice/circuits/file-tree/dyns/folder-dynamo'
import {FileDynamo} from '../../../../../../../../maurice/circuits/file-tree/dyns/file-dynamo'

/** @type {xyz.swapee.wc.IFileTreeElement._render_} */
export default function FileTreeRender({
 foldersRotor,filesRotor,folder,expanded,heading,
 loadingFiles:loadingFiles,
},{makeFolderElement,makeFileElement,flipExpanded}) { // todo: flip method in addition to setCollapsed/expanded
 return (<div $id="FileTree" Heading={heading}>
  <div $id="FolderWr" onClick={flipExpanded}>
   <span $id="FolderLa">{folder}</span>
  </div>
  <span $id="FolderLa">{folder}</span>
  <span $id="LoIn" $reveal={loadingFiles}/>
  <span $id="ExpandedIc" $reveal={expanded}/>
  <span $id="CollapsedIc" $conceal={expanded}/>
  <span $id="ContentWr" $reveal={expanded}/>
  <FolderDynamo $id="Folders" $rotor={foldersRotor} $make={makeFolderElement}  />
  <FileDynamo $id="Files" $rotor={filesRotor} $make={makeFileElement}  />

  <div $template="FolderTemplate" />,
  <div $template="FileTemplate" />
 </div>)
}