/** @type {xyz.swapee.wc.IFileTreeElement._solder} */
export default function solder({root:root,folder:folder,heading:heading}) {
 return{
  heading:heading,
  root,folder,
  'host':this.REMOTE_HOST,
 }
}