import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import FileTreeServerController from '../FileTreeServerController'
import FileTreeServerProcessor from '../FileTreeServerProcessor'
import FileTreeService from '../../src/FileTreeService'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractFileTreeElement from '../../gen/AbstractFileTreeElement'

/** @extends {xyz.swapee.wc.FileTreeElement} */
export default class FileTreeElement extends AbstractFileTreeElement.implements(
 FileTreeServerController,
 FileTreeServerProcessor,
 FileTreeService,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IFileTreeElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IFileTreeElement}*/({
   classesMap: true,
   rootSelector:     `.FileTree`,
   stylesheet:       'html/styles/FileTree.css',
   blockName:        'html/FileTreeBlock.html',
  }),
){}

// thank you for using web circuits
