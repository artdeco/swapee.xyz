/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IFileTreeComputer={}
xyz.swapee.wc.IFileTreeComputer.adaptFiles={}
xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath={}
xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor={}
xyz.swapee.wc.IFileTreeComputer.compute={}
xyz.swapee.wc.IFileTreeOuterCore={}
xyz.swapee.wc.IFileTreeOuterCore.Model={}
xyz.swapee.wc.IFileTreeOuterCore.Model.Host={}
xyz.swapee.wc.IFileTreeOuterCore.Model.Heading={}
xyz.swapee.wc.IFileTreeOuterCore.Model.Root={}
xyz.swapee.wc.IFileTreeOuterCore.Model.Folder={}
xyz.swapee.wc.IFileTreeOuterCore.Model.ShowHidden={}
xyz.swapee.wc.IFileTreeOuterCore.Model.CurrentFilePath={}
xyz.swapee.wc.IFileTreeOuterCore.Model.Expanded={}
xyz.swapee.wc.IFileTreeOuterCore.Model.FoldersRotor={}
xyz.swapee.wc.IFileTreeOuterCore.Model.FilesRotor={}
xyz.swapee.wc.IFileTreeOuterCore.WeakModel={}
xyz.swapee.wc.IFileTreePort={}
xyz.swapee.wc.IFileTreePort.Inputs={}
xyz.swapee.wc.IFileTreePort.WeakInputs={}
xyz.swapee.wc.IFileTreeCore={}
xyz.swapee.wc.IFileTreeCore.Model={}
xyz.swapee.wc.IFileTreeCore.Model.Folders={}
xyz.swapee.wc.IFileTreeCore.Model.Files={}
xyz.swapee.wc.IFileTreeCore.Model.LoadingFiles={}
xyz.swapee.wc.IFileTreeCore.Model.HasMoreFiles={}
xyz.swapee.wc.IFileTreeCore.Model.LoadFilesError={}
xyz.swapee.wc.IFileTreePortInterface={}
xyz.swapee.wc.IFileTreeProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IFileTreeController={}
xyz.swapee.wc.front.IFileTreeControllerAT={}
xyz.swapee.wc.front.IFileTreeScreenAR={}
xyz.swapee.wc.IFileTree={}
xyz.swapee.wc.IFileTreeHtmlComponentUtil={}
xyz.swapee.wc.IFileTreeHtmlComponent={}
xyz.swapee.wc.IFileTreeElement={}
xyz.swapee.wc.IFileTreeElement.build={}
xyz.swapee.wc.IFileTreeElement.short={}
xyz.swapee.wc.IFileTreeElementPort={}
xyz.swapee.wc.IFileTreeElementPort.Inputs={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts={}
xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts={}
xyz.swapee.wc.IFileTreeElementPort.WeakInputs={}
xyz.swapee.wc.IFileTreeGenerator={}
xyz.swapee.wc.IFileTreeRadio={}
xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles={}
xyz.swapee.wc.IFileTreeDesigner={}
xyz.swapee.wc.IFileTreeDesigner.communicator={}
xyz.swapee.wc.IFileTreeDesigner.relay={}
xyz.swapee.wc.IFileTreeService={}
xyz.swapee.wc.IFileTreeService.filterFiles={}
xyz.swapee.wc.IFileTreeDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IFileTreeDisplay={}
xyz.swapee.wc.back.IFileTreeController={}
xyz.swapee.wc.back.IFileTreeControllerAR={}
xyz.swapee.wc.back.IFileTreeScreen={}
xyz.swapee.wc.back.IFileTreeScreenAT={}
xyz.swapee.wc.IFileTreeController={}
xyz.swapee.wc.IFileTreeScreen={}
xyz.swapee.wc.IFileTreeGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/02-IFileTreeComputer.xml}  56570ddcffa18dd568c8c42f61b1b5f5 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IFileTreeComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeComputer)} xyz.swapee.wc.AbstractFileTreeComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeComputer} xyz.swapee.wc.FileTreeComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeComputer` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeComputer
 */
xyz.swapee.wc.AbstractFileTreeComputer = class extends /** @type {xyz.swapee.wc.AbstractFileTreeComputer.constructor&xyz.swapee.wc.FileTreeComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeComputer.prototype.constructor = xyz.swapee.wc.AbstractFileTreeComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeComputer.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeComputer}
 */
xyz.swapee.wc.AbstractFileTreeComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeComputer}
 */
xyz.swapee.wc.AbstractFileTreeComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeComputer}
 */
xyz.swapee.wc.AbstractFileTreeComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeComputer}
 */
xyz.swapee.wc.AbstractFileTreeComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeComputer.Initialese[]) => xyz.swapee.wc.IFileTreeComputer} xyz.swapee.wc.FileTreeComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileTreeComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.FileTreeMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.FileTreeLand>)} xyz.swapee.wc.IFileTreeComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IFileTreeComputer
 */
xyz.swapee.wc.IFileTreeComputer = class extends /** @type {xyz.swapee.wc.IFileTreeComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreeComputer.adaptFiles} */
xyz.swapee.wc.IFileTreeComputer.prototype.adaptFiles = function() {}
/** @type {xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath} */
xyz.swapee.wc.IFileTreeComputer.prototype.adaptCurrentFilePath = function() {}
/** @type {xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor} */
xyz.swapee.wc.IFileTreeComputer.prototype.adaptFilesRotor = function() {}
/** @type {xyz.swapee.wc.IFileTreeComputer.compute} */
xyz.swapee.wc.IFileTreeComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeComputer.Initialese>)} xyz.swapee.wc.FileTreeComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeComputer} xyz.swapee.wc.IFileTreeComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileTreeComputer_ instances.
 * @constructor xyz.swapee.wc.FileTreeComputer
 * @implements {xyz.swapee.wc.IFileTreeComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeComputer.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeComputer = class extends /** @type {xyz.swapee.wc.FileTreeComputer.constructor&xyz.swapee.wc.IFileTreeComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeComputer}
 */
xyz.swapee.wc.FileTreeComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileTreeComputer} */
xyz.swapee.wc.RecordIFileTreeComputer

/** @typedef {xyz.swapee.wc.IFileTreeComputer} xyz.swapee.wc.BoundIFileTreeComputer */

/** @typedef {xyz.swapee.wc.FileTreeComputer} xyz.swapee.wc.BoundFileTreeComputer */

/**
 * Contains getters to cast the _IFileTreeComputer_ interface.
 * @interface xyz.swapee.wc.IFileTreeComputerCaster
 */
xyz.swapee.wc.IFileTreeComputerCaster = class { }
/**
 * Cast the _IFileTreeComputer_ instance into the _BoundIFileTreeComputer_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeComputer}
 */
xyz.swapee.wc.IFileTreeComputerCaster.prototype.asIFileTreeComputer
/**
 * Access the _FileTreeComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeComputer}
 */
xyz.swapee.wc.IFileTreeComputerCaster.prototype.superFileTreeComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileTreeComputer.adaptFiles.Form, changes: xyz.swapee.wc.IFileTreeComputer.adaptFiles.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileTreeComputer.adaptFiles.Return|void)>)} xyz.swapee.wc.IFileTreeComputer.__adaptFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeComputer.__adaptFiles<!xyz.swapee.wc.IFileTreeComputer>} xyz.swapee.wc.IFileTreeComputer._adaptFiles */
/** @typedef {typeof xyz.swapee.wc.IFileTreeComputer.adaptFiles} */
/**
 * Returns files and folders in the directory.
 * @param {!xyz.swapee.wc.IFileTreeComputer.adaptFiles.Form} form The form with inputs.
 * - `root` _string_ The root folder on the disk to read from. ⤴ *IFileTreeOuterCore.Model.Root_Safe*
 * - `folder` _string_ The name of the folder inside the root presented to the user. ⤴ *IFileTreeOuterCore.Model.Folder_Safe*
 * @param {xyz.swapee.wc.IFileTreeComputer.adaptFiles.Form} changes The previous values of the form.
 * - `root` _string_ The root folder on the disk to read from. ⤴ *IFileTreeOuterCore.Model.Root_Safe*
 * - `folder` _string_ The name of the folder inside the root presented to the user. ⤴ *IFileTreeOuterCore.Model.Folder_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileTreeComputer.adaptFiles.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IFileTreeComputer.adaptFiles = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.Root_Safe&xyz.swapee.wc.IFileTreeCore.Model.Folder_Safe} xyz.swapee.wc.IFileTreeComputer.adaptFiles.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.Files&xyz.swapee.wc.IFileTreeCore.Model.Folders} xyz.swapee.wc.IFileTreeComputer.adaptFiles.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Form, changes: xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Form) => (void|xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Return)} xyz.swapee.wc.IFileTreeComputer.__adaptCurrentFilePath
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeComputer.__adaptCurrentFilePath<!xyz.swapee.wc.IFileTreeComputer>} xyz.swapee.wc.IFileTreeComputer._adaptCurrentFilePath */
/** @typedef {typeof xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath} */
/**
 * @param {!xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Form} form The form with inputs.
 * @param {xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Return} The form with outputs.
 */
xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Path_Safe} xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.CurrentFilePath} xyz.swapee.wc.IFileTreeComputer.adaptCurrentFilePath.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Form, changes: xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Form) => (void|xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Return)} xyz.swapee.wc.IFileTreeComputer.__adaptFilesRotor
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeComputer.__adaptFilesRotor<!xyz.swapee.wc.IFileTreeComputer>} xyz.swapee.wc.IFileTreeComputer._adaptFilesRotor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor} */
/**
 * @param {!xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Form} form The form with inputs.
 * - `files` _!Array&lt;{ name: string, path: string }&gt;_ The discovered files. ⤴ *IFileTreeCore.Model.Files_Safe*
 * - `currentFilePath` _string_ The file that is currently open. ⤴ *IFileTreeOuterCore.Model.CurrentFilePath_Safe*
 * @param {xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Form} changes The previous values of the form.
 * - `files` _!Array&lt;{ name: string, path: string }&gt;_ The discovered files. ⤴ *IFileTreeCore.Model.Files_Safe*
 * - `currentFilePath` _string_ The file that is currently open. ⤴ *IFileTreeOuterCore.Model.CurrentFilePath_Safe*
 * @return {void|xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Return} The form with outputs.
 */
xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.Files_Safe&xyz.swapee.wc.IFileTreeCore.Model.CurrentFilePath_Safe} xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.FilesRotor} xyz.swapee.wc.IFileTreeComputer.adaptFilesRotor.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.FileTreeMemory, land: !xyz.swapee.wc.IFileTreeComputer.compute.Land) => void} xyz.swapee.wc.IFileTreeComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeComputer.__compute<!xyz.swapee.wc.IFileTreeComputer>} xyz.swapee.wc.IFileTreeComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IFileTreeComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.FileTreeMemory} mem The memory.
 * @param {!xyz.swapee.wc.IFileTreeComputer.compute.Land} land The land.
 * - `FileEditor` _!xyz.swapee.wc.FileEditorMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IFileTreeComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.FileEditorMemory} FileEditor `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/03-IFileTreeOuterCore.xml}  bf1d02f3fecd63df136fadbeeb0ed6be */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileTreeOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeOuterCore)} xyz.swapee.wc.AbstractFileTreeOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeOuterCore} xyz.swapee.wc.FileTreeOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeOuterCore
 */
xyz.swapee.wc.AbstractFileTreeOuterCore = class extends /** @type {xyz.swapee.wc.AbstractFileTreeOuterCore.constructor&xyz.swapee.wc.FileTreeOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeOuterCore.prototype.constructor = xyz.swapee.wc.AbstractFileTreeOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileTreeOuterCore|typeof xyz.swapee.wc.FileTreeOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeOuterCore}
 */
xyz.swapee.wc.AbstractFileTreeOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeOuterCore}
 */
xyz.swapee.wc.AbstractFileTreeOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileTreeOuterCore|typeof xyz.swapee.wc.FileTreeOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeOuterCore}
 */
xyz.swapee.wc.AbstractFileTreeOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileTreeOuterCore|typeof xyz.swapee.wc.FileTreeOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeOuterCore}
 */
xyz.swapee.wc.AbstractFileTreeOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeOuterCoreCaster)} xyz.swapee.wc.IFileTreeOuterCore.constructor */
/**
 * The _IFileTree_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IFileTreeOuterCore
 */
xyz.swapee.wc.IFileTreeOuterCore = class extends /** @type {xyz.swapee.wc.IFileTreeOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IFileTreeOuterCore.prototype.constructor = xyz.swapee.wc.IFileTreeOuterCore

/** @typedef {function(new: xyz.swapee.wc.IFileTreeOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeOuterCore.Initialese>)} xyz.swapee.wc.FileTreeOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeOuterCore} xyz.swapee.wc.IFileTreeOuterCore.typeof */
/**
 * A concrete class of _IFileTreeOuterCore_ instances.
 * @constructor xyz.swapee.wc.FileTreeOuterCore
 * @implements {xyz.swapee.wc.IFileTreeOuterCore} The _IFileTree_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeOuterCore = class extends /** @type {xyz.swapee.wc.FileTreeOuterCore.constructor&xyz.swapee.wc.IFileTreeOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileTreeOuterCore.prototype.constructor = xyz.swapee.wc.FileTreeOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeOuterCore}
 */
xyz.swapee.wc.FileTreeOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeOuterCore.
 * @interface xyz.swapee.wc.IFileTreeOuterCoreFields
 */
xyz.swapee.wc.IFileTreeOuterCoreFields = class { }
/**
 * The _IFileTree_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IFileTreeOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IFileTreeOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore} */
xyz.swapee.wc.RecordIFileTreeOuterCore

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore} xyz.swapee.wc.BoundIFileTreeOuterCore */

/** @typedef {xyz.swapee.wc.FileTreeOuterCore} xyz.swapee.wc.BoundFileTreeOuterCore */

/**
 * The host property.
 * @typedef {string}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.Host.host

/**
 * Whether the folder acts as heading.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.Heading.heading

/**
 * The root folder on the disk to read from.
 * @typedef {string}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.Root.root

/**
 * The name of the folder inside the root presented to the user.
 * @typedef {string}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.Folder.folder

/**
 * Whether to report hidden files (those that start with `.`).
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.ShowHidden.showHidden

/**
 * The file that is currently open.
 * @typedef {string}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.CurrentFilePath.currentFilePath

/**
 * Whether folder is currently expanded.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.Expanded.expanded

/**
 * An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator.
 * @typedef {Array<!xyz.swapee.wc.IFileTree.FolderBrushes>}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.FoldersRotor.foldersRotor

/**
 * An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator.
 * @typedef {Array<!xyz.swapee.wc.IFileTree.FileBrushes>}
 */
xyz.swapee.wc.IFileTreeOuterCore.Model.FilesRotor.filesRotor

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Host&xyz.swapee.wc.IFileTreeOuterCore.Model.Heading&xyz.swapee.wc.IFileTreeOuterCore.Model.Root&xyz.swapee.wc.IFileTreeOuterCore.Model.Folder&xyz.swapee.wc.IFileTreeOuterCore.Model.ShowHidden&xyz.swapee.wc.IFileTreeOuterCore.Model.CurrentFilePath&xyz.swapee.wc.IFileTreeOuterCore.Model.Expanded&xyz.swapee.wc.IFileTreeOuterCore.Model.FoldersRotor&xyz.swapee.wc.IFileTreeOuterCore.Model.FilesRotor} xyz.swapee.wc.IFileTreeOuterCore.Model The _IFileTree_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Host&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Heading&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Root&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Folder&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.ShowHidden&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.CurrentFilePath&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Expanded&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FoldersRotor&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FilesRotor} xyz.swapee.wc.IFileTreeOuterCore.WeakModel The _IFileTree_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IFileTreeOuterCore_ interface.
 * @interface xyz.swapee.wc.IFileTreeOuterCoreCaster
 */
xyz.swapee.wc.IFileTreeOuterCoreCaster = class { }
/**
 * Cast the _IFileTreeOuterCore_ instance into the _BoundIFileTreeOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeOuterCore}
 */
xyz.swapee.wc.IFileTreeOuterCoreCaster.prototype.asIFileTreeOuterCore
/**
 * Access the _FileTreeOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeOuterCore}
 */
xyz.swapee.wc.IFileTreeOuterCoreCaster.prototype.superFileTreeOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Host The host property (optional overlay).
 * @prop {string} [host=""] The host property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Host_Safe The host property (required overlay).
 * @prop {string} host The host property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Heading Whether the folder acts as heading (optional overlay).
 * @prop {boolean} [heading=false] Whether the folder acts as heading. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Heading_Safe Whether the folder acts as heading (required overlay).
 * @prop {boolean} heading Whether the folder acts as heading.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Root The root folder on the disk to read from (optional overlay).
 * @prop {string} [root=""] The root folder on the disk to read from. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Root_Safe The root folder on the disk to read from (required overlay).
 * @prop {string} root The root folder on the disk to read from.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Folder The name of the folder inside the root presented to the user (optional overlay).
 * @prop {string} [folder="."] The name of the folder inside the root presented to the user. Default `.`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Folder_Safe The name of the folder inside the root presented to the user (required overlay).
 * @prop {string} folder The name of the folder inside the root presented to the user.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.ShowHidden Whether to report hidden files (those that start with `.`) (optional overlay).
 * @prop {boolean} [showHidden=false] Whether to report hidden files (those that start with `.`). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.ShowHidden_Safe Whether to report hidden files (those that start with `.`) (required overlay).
 * @prop {boolean} showHidden Whether to report hidden files (those that start with `.`).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.CurrentFilePath The file that is currently open (optional overlay).
 * @prop {string} [currentFilePath=""] The file that is currently open. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.CurrentFilePath_Safe The file that is currently open (required overlay).
 * @prop {string} currentFilePath The file that is currently open.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Expanded Whether folder is currently expanded (optional overlay).
 * @prop {boolean} [expanded=true] Whether folder is currently expanded. Default `true`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.Expanded_Safe Whether folder is currently expanded (required overlay).
 * @prop {boolean} expanded Whether folder is currently expanded.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.FoldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (optional overlay).
 * @prop {Array<!xyz.swapee.wc.IFileTree.FolderBrushes>} [foldersRotor=null] An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.FoldersRotor_Safe An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (required overlay).
 * @prop {Array<!xyz.swapee.wc.IFileTree.FolderBrushes>} foldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.FilesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (optional overlay).
 * @prop {Array<!xyz.swapee.wc.IFileTree.FileBrushes>} [filesRotor=null] An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.Model.FilesRotor_Safe An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (required overlay).
 * @prop {Array<!xyz.swapee.wc.IFileTree.FileBrushes>} filesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Host The host property (optional overlay).
 * @prop {*} [host=null] The host property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Host_Safe The host property (required overlay).
 * @prop {*} host The host property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Heading Whether the folder acts as heading (optional overlay).
 * @prop {*} [heading=null] Whether the folder acts as heading. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Heading_Safe Whether the folder acts as heading (required overlay).
 * @prop {*} heading Whether the folder acts as heading.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Root The root folder on the disk to read from (optional overlay).
 * @prop {*} [root=null] The root folder on the disk to read from. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Root_Safe The root folder on the disk to read from (required overlay).
 * @prop {*} root The root folder on the disk to read from.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Folder The name of the folder inside the root presented to the user (optional overlay).
 * @prop {*} [folder="."] The name of the folder inside the root presented to the user. Default `.`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Folder_Safe The name of the folder inside the root presented to the user (required overlay).
 * @prop {*} folder The name of the folder inside the root presented to the user.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.ShowHidden Whether to report hidden files (those that start with `.`) (optional overlay).
 * @prop {*} [showHidden=null] Whether to report hidden files (those that start with `.`). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.ShowHidden_Safe Whether to report hidden files (those that start with `.`) (required overlay).
 * @prop {*} showHidden Whether to report hidden files (those that start with `.`).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.CurrentFilePath The file that is currently open (optional overlay).
 * @prop {*} [currentFilePath=null] The file that is currently open. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.CurrentFilePath_Safe The file that is currently open (required overlay).
 * @prop {*} currentFilePath The file that is currently open.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Expanded Whether folder is currently expanded (optional overlay).
 * @prop {*} [expanded=true] Whether folder is currently expanded. Default `true`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Expanded_Safe Whether folder is currently expanded (required overlay).
 * @prop {*} expanded Whether folder is currently expanded.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FoldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (optional overlay).
 * @prop {*} [foldersRotor=null] An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FoldersRotor_Safe An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (required overlay).
 * @prop {*} foldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FilesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (optional overlay).
 * @prop {*} [filesRotor=null] An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FilesRotor_Safe An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (required overlay).
 * @prop {*} filesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator.
 */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Host} xyz.swapee.wc.IFileTreePort.Inputs.Host The host property (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.IFileTreePort.Inputs.Host_Safe The host property (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Heading} xyz.swapee.wc.IFileTreePort.Inputs.Heading Whether the folder acts as heading (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Heading_Safe} xyz.swapee.wc.IFileTreePort.Inputs.Heading_Safe Whether the folder acts as heading (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Root} xyz.swapee.wc.IFileTreePort.Inputs.Root The root folder on the disk to read from (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Root_Safe} xyz.swapee.wc.IFileTreePort.Inputs.Root_Safe The root folder on the disk to read from (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Folder} xyz.swapee.wc.IFileTreePort.Inputs.Folder The name of the folder inside the root presented to the user (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Folder_Safe} xyz.swapee.wc.IFileTreePort.Inputs.Folder_Safe The name of the folder inside the root presented to the user (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.ShowHidden} xyz.swapee.wc.IFileTreePort.Inputs.ShowHidden Whether to report hidden files (those that start with `.`) (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.ShowHidden_Safe} xyz.swapee.wc.IFileTreePort.Inputs.ShowHidden_Safe Whether to report hidden files (those that start with `.`) (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.CurrentFilePath} xyz.swapee.wc.IFileTreePort.Inputs.CurrentFilePath The file that is currently open (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.CurrentFilePath_Safe} xyz.swapee.wc.IFileTreePort.Inputs.CurrentFilePath_Safe The file that is currently open (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Expanded} xyz.swapee.wc.IFileTreePort.Inputs.Expanded Whether folder is currently expanded (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Expanded_Safe} xyz.swapee.wc.IFileTreePort.Inputs.Expanded_Safe Whether folder is currently expanded (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FoldersRotor} xyz.swapee.wc.IFileTreePort.Inputs.FoldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FoldersRotor_Safe} xyz.swapee.wc.IFileTreePort.Inputs.FoldersRotor_Safe An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FilesRotor} xyz.swapee.wc.IFileTreePort.Inputs.FilesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FilesRotor_Safe} xyz.swapee.wc.IFileTreePort.Inputs.FilesRotor_Safe An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Host} xyz.swapee.wc.IFileTreePort.WeakInputs.Host The host property (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.Host_Safe The host property (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Heading} xyz.swapee.wc.IFileTreePort.WeakInputs.Heading Whether the folder acts as heading (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Heading_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.Heading_Safe Whether the folder acts as heading (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Root} xyz.swapee.wc.IFileTreePort.WeakInputs.Root The root folder on the disk to read from (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Root_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.Root_Safe The root folder on the disk to read from (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Folder} xyz.swapee.wc.IFileTreePort.WeakInputs.Folder The name of the folder inside the root presented to the user (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Folder_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.Folder_Safe The name of the folder inside the root presented to the user (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.ShowHidden} xyz.swapee.wc.IFileTreePort.WeakInputs.ShowHidden Whether to report hidden files (those that start with `.`) (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.ShowHidden_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.ShowHidden_Safe Whether to report hidden files (those that start with `.`) (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.CurrentFilePath} xyz.swapee.wc.IFileTreePort.WeakInputs.CurrentFilePath The file that is currently open (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.CurrentFilePath_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.CurrentFilePath_Safe The file that is currently open (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Expanded} xyz.swapee.wc.IFileTreePort.WeakInputs.Expanded Whether folder is currently expanded (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.Expanded_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.Expanded_Safe Whether folder is currently expanded (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FoldersRotor} xyz.swapee.wc.IFileTreePort.WeakInputs.FoldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FoldersRotor_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.FoldersRotor_Safe An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FilesRotor} xyz.swapee.wc.IFileTreePort.WeakInputs.FilesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.WeakModel.FilesRotor_Safe} xyz.swapee.wc.IFileTreePort.WeakInputs.FilesRotor_Safe An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Host} xyz.swapee.wc.IFileTreeCore.Model.Host The host property (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Host_Safe} xyz.swapee.wc.IFileTreeCore.Model.Host_Safe The host property (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Heading} xyz.swapee.wc.IFileTreeCore.Model.Heading Whether the folder acts as heading (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Heading_Safe} xyz.swapee.wc.IFileTreeCore.Model.Heading_Safe Whether the folder acts as heading (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Root} xyz.swapee.wc.IFileTreeCore.Model.Root The root folder on the disk to read from (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Root_Safe} xyz.swapee.wc.IFileTreeCore.Model.Root_Safe The root folder on the disk to read from (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Folder} xyz.swapee.wc.IFileTreeCore.Model.Folder The name of the folder inside the root presented to the user (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Folder_Safe} xyz.swapee.wc.IFileTreeCore.Model.Folder_Safe The name of the folder inside the root presented to the user (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.ShowHidden} xyz.swapee.wc.IFileTreeCore.Model.ShowHidden Whether to report hidden files (those that start with `.`) (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.ShowHidden_Safe} xyz.swapee.wc.IFileTreeCore.Model.ShowHidden_Safe Whether to report hidden files (those that start with `.`) (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.CurrentFilePath} xyz.swapee.wc.IFileTreeCore.Model.CurrentFilePath The file that is currently open (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.CurrentFilePath_Safe} xyz.swapee.wc.IFileTreeCore.Model.CurrentFilePath_Safe The file that is currently open (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Expanded} xyz.swapee.wc.IFileTreeCore.Model.Expanded Whether folder is currently expanded (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.Expanded_Safe} xyz.swapee.wc.IFileTreeCore.Model.Expanded_Safe Whether folder is currently expanded (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.FoldersRotor} xyz.swapee.wc.IFileTreeCore.Model.FoldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.FoldersRotor_Safe} xyz.swapee.wc.IFileTreeCore.Model.FoldersRotor_Safe An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator (required overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.FilesRotor} xyz.swapee.wc.IFileTreeCore.Model.FilesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model.FilesRotor_Safe} xyz.swapee.wc.IFileTreeCore.Model.FilesRotor_Safe An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/04-IFileTreePort.xml}  3bd79513ed6e8b1902d29968b8e01ac5 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IFileTreePort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreePort)} xyz.swapee.wc.AbstractFileTreePort.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreePort} xyz.swapee.wc.FileTreePort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreePort` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreePort
 */
xyz.swapee.wc.AbstractFileTreePort = class extends /** @type {xyz.swapee.wc.AbstractFileTreePort.constructor&xyz.swapee.wc.FileTreePort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreePort.prototype.constructor = xyz.swapee.wc.AbstractFileTreePort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreePort.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreePort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreePort|typeof xyz.swapee.wc.FileTreePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreePort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreePort}
 */
xyz.swapee.wc.AbstractFileTreePort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreePort}
 */
xyz.swapee.wc.AbstractFileTreePort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreePort|typeof xyz.swapee.wc.FileTreePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreePort}
 */
xyz.swapee.wc.AbstractFileTreePort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreePort|typeof xyz.swapee.wc.FileTreePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreePort}
 */
xyz.swapee.wc.AbstractFileTreePort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreePort.Initialese[]) => xyz.swapee.wc.IFileTreePort} xyz.swapee.wc.FileTreePortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileTreePortFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreePortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IFileTreePort.Inputs>)} xyz.swapee.wc.IFileTreePort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IFileTree_, providing input
 * pins.
 * @interface xyz.swapee.wc.IFileTreePort
 */
xyz.swapee.wc.IFileTreePort = class extends /** @type {xyz.swapee.wc.IFileTreePort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreePort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreePort.resetPort} */
xyz.swapee.wc.IFileTreePort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IFileTreePort.resetFileTreePort} */
xyz.swapee.wc.IFileTreePort.prototype.resetFileTreePort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreePort&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreePort.Initialese>)} xyz.swapee.wc.FileTreePort.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreePort} xyz.swapee.wc.IFileTreePort.typeof */
/**
 * A concrete class of _IFileTreePort_ instances.
 * @constructor xyz.swapee.wc.FileTreePort
 * @implements {xyz.swapee.wc.IFileTreePort} The port that serves as an interface to the _IFileTree_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreePort.Initialese>} ‎
 */
xyz.swapee.wc.FileTreePort = class extends /** @type {xyz.swapee.wc.FileTreePort.constructor&xyz.swapee.wc.IFileTreePort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreePort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreePort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreePort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreePort}
 */
xyz.swapee.wc.FileTreePort.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreePort.
 * @interface xyz.swapee.wc.IFileTreePortFields
 */
xyz.swapee.wc.IFileTreePortFields = class { }
/**
 * The inputs to the _IFileTree_'s controller via its port.
 */
xyz.swapee.wc.IFileTreePortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileTreePort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IFileTreePortFields.prototype.props = /** @type {!xyz.swapee.wc.IFileTreePort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreePort} */
xyz.swapee.wc.RecordIFileTreePort

/** @typedef {xyz.swapee.wc.IFileTreePort} xyz.swapee.wc.BoundIFileTreePort */

/** @typedef {xyz.swapee.wc.FileTreePort} xyz.swapee.wc.BoundFileTreePort */

/** @typedef {function(new: xyz.swapee.wc.IFileTreeOuterCore.WeakModel)} xyz.swapee.wc.IFileTreePort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeOuterCore.WeakModel} xyz.swapee.wc.IFileTreeOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IFileTree_'s controller via its port.
 * @record xyz.swapee.wc.IFileTreePort.Inputs
 */
xyz.swapee.wc.IFileTreePort.Inputs = class extends /** @type {xyz.swapee.wc.IFileTreePort.Inputs.constructor&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IFileTreePort.Inputs.prototype.constructor = xyz.swapee.wc.IFileTreePort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IFileTreeOuterCore.WeakModel)} xyz.swapee.wc.IFileTreePort.WeakInputs.constructor */
/**
 * The inputs to the _IFileTree_'s controller via its port.
 * @record xyz.swapee.wc.IFileTreePort.WeakInputs
 */
xyz.swapee.wc.IFileTreePort.WeakInputs = class extends /** @type {xyz.swapee.wc.IFileTreePort.WeakInputs.constructor&xyz.swapee.wc.IFileTreeOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IFileTreePort.WeakInputs.prototype.constructor = xyz.swapee.wc.IFileTreePort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IFileTreePortInterface
 */
xyz.swapee.wc.IFileTreePortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IFileTreePortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IFileTreePortInterface.prototype.constructor = xyz.swapee.wc.IFileTreePortInterface

/**
 * A concrete class of _IFileTreePortInterface_ instances.
 * @constructor xyz.swapee.wc.FileTreePortInterface
 * @implements {xyz.swapee.wc.IFileTreePortInterface} The port interface.
 */
xyz.swapee.wc.FileTreePortInterface = class extends xyz.swapee.wc.IFileTreePortInterface { }
xyz.swapee.wc.FileTreePortInterface.prototype.constructor = xyz.swapee.wc.FileTreePortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreePortInterface.Props
 * @prop {string} host The host property.
 * @prop {boolean} heading Whether the folder acts as heading.
 * @prop {string} root The root folder on the disk to read from.
 * @prop {string} folder The name of the folder inside the root presented to the user. Default `.`.
 * @prop {boolean} showHidden Whether to report hidden files (those that start with `.`).
 * @prop {string} currentFilePath The file that is currently open.
 * @prop {Array<!xyz.swapee.wc.IFileTree.FolderBrushes>} foldersRotor An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator.
 * @prop {Array<!xyz.swapee.wc.IFileTree.FileBrushes>} filesRotor An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator.
 * @prop {boolean} [expanded=true] Whether folder is currently expanded. Default `true`.
 */

/**
 * Contains getters to cast the _IFileTreePort_ interface.
 * @interface xyz.swapee.wc.IFileTreePortCaster
 */
xyz.swapee.wc.IFileTreePortCaster = class { }
/**
 * Cast the _IFileTreePort_ instance into the _BoundIFileTreePort_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreePort}
 */
xyz.swapee.wc.IFileTreePortCaster.prototype.asIFileTreePort
/**
 * Access the _FileTreePort_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreePort}
 */
xyz.swapee.wc.IFileTreePortCaster.prototype.superFileTreePort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreePort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreePort.__resetPort<!xyz.swapee.wc.IFileTreePort>} xyz.swapee.wc.IFileTreePort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IFileTreePort.resetPort} */
/**
 * Resets the _IFileTree_ port.
 * @return {void}
 */
xyz.swapee.wc.IFileTreePort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreePort.__resetFileTreePort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreePort.__resetFileTreePort<!xyz.swapee.wc.IFileTreePort>} xyz.swapee.wc.IFileTreePort._resetFileTreePort */
/** @typedef {typeof xyz.swapee.wc.IFileTreePort.resetFileTreePort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IFileTreePort.resetFileTreePort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreePort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/09-IFileTreeCore.xml}  4ea969dcc4d31e199186e969b6969145 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileTreeCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeCore)} xyz.swapee.wc.AbstractFileTreeCore.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeCore} xyz.swapee.wc.FileTreeCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeCore` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeCore
 */
xyz.swapee.wc.AbstractFileTreeCore = class extends /** @type {xyz.swapee.wc.AbstractFileTreeCore.constructor&xyz.swapee.wc.FileTreeCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeCore.prototype.constructor = xyz.swapee.wc.AbstractFileTreeCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeCore.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeCore|typeof xyz.swapee.wc.FileTreeCore)|(!xyz.swapee.wc.IFileTreeOuterCore|typeof xyz.swapee.wc.FileTreeOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeCore}
 */
xyz.swapee.wc.AbstractFileTreeCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeCore}
 */
xyz.swapee.wc.AbstractFileTreeCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeCore|typeof xyz.swapee.wc.FileTreeCore)|(!xyz.swapee.wc.IFileTreeOuterCore|typeof xyz.swapee.wc.FileTreeOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeCore}
 */
xyz.swapee.wc.AbstractFileTreeCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeCore|typeof xyz.swapee.wc.FileTreeCore)|(!xyz.swapee.wc.IFileTreeOuterCore|typeof xyz.swapee.wc.FileTreeOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeCore}
 */
xyz.swapee.wc.AbstractFileTreeCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeCoreCaster&xyz.swapee.wc.IFileTreeOuterCore)} xyz.swapee.wc.IFileTreeCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IFileTreeCore
 */
xyz.swapee.wc.IFileTreeCore = class extends /** @type {xyz.swapee.wc.IFileTreeCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileTreeOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IFileTreeCore.resetCore} */
xyz.swapee.wc.IFileTreeCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IFileTreeCore.resetFileTreeCore} */
xyz.swapee.wc.IFileTreeCore.prototype.resetFileTreeCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeCore&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeCore.Initialese>)} xyz.swapee.wc.FileTreeCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeCore} xyz.swapee.wc.IFileTreeCore.typeof */
/**
 * A concrete class of _IFileTreeCore_ instances.
 * @constructor xyz.swapee.wc.FileTreeCore
 * @implements {xyz.swapee.wc.IFileTreeCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeCore.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeCore = class extends /** @type {xyz.swapee.wc.FileTreeCore.constructor&xyz.swapee.wc.IFileTreeCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileTreeCore.prototype.constructor = xyz.swapee.wc.FileTreeCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeCore}
 */
xyz.swapee.wc.FileTreeCore.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeCore.
 * @interface xyz.swapee.wc.IFileTreeCoreFields
 */
xyz.swapee.wc.IFileTreeCoreFields = class { }
/**
 * The _IFileTree_'s memory.
 */
xyz.swapee.wc.IFileTreeCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IFileTreeCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IFileTreeCoreFields.prototype.props = /** @type {xyz.swapee.wc.IFileTreeCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeCore} */
xyz.swapee.wc.RecordIFileTreeCore

/** @typedef {xyz.swapee.wc.IFileTreeCore} xyz.swapee.wc.BoundIFileTreeCore */

/** @typedef {xyz.swapee.wc.FileTreeCore} xyz.swapee.wc.BoundFileTreeCore */

/**
 * The discovered folders.
 * @typedef {!Array<{ name: string, path: string }>}
 */
xyz.swapee.wc.IFileTreeCore.Model.Folders.folders

/**
 * The discovered files.
 * @typedef {!Array<{ name: string, path: string }>}
 */
xyz.swapee.wc.IFileTreeCore.Model.Files.files

/**
 * Whether the items are being loaded from the remote.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileTreeCore.Model.LoadingFiles.loadingFiles

/**
 * Whether there are more items to load.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileTreeCore.Model.HasMoreFiles.hasMoreFiles

/**
 * An error during loading of items from the remote.
 * @typedef {Error}
 */
xyz.swapee.wc.IFileTreeCore.Model.LoadFilesError.loadFilesError

/** @typedef {xyz.swapee.wc.IFileTreeOuterCore.Model&xyz.swapee.wc.IFileTreeCore.Model.Folders&xyz.swapee.wc.IFileTreeCore.Model.Files&xyz.swapee.wc.IFileTreeCore.Model.LoadingFiles&xyz.swapee.wc.IFileTreeCore.Model.HasMoreFiles&xyz.swapee.wc.IFileTreeCore.Model.LoadFilesError} xyz.swapee.wc.IFileTreeCore.Model The _IFileTree_'s memory. */

/**
 * Contains getters to cast the _IFileTreeCore_ interface.
 * @interface xyz.swapee.wc.IFileTreeCoreCaster
 */
xyz.swapee.wc.IFileTreeCoreCaster = class { }
/**
 * Cast the _IFileTreeCore_ instance into the _BoundIFileTreeCore_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeCore}
 */
xyz.swapee.wc.IFileTreeCoreCaster.prototype.asIFileTreeCore
/**
 * Access the _FileTreeCore_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeCore}
 */
xyz.swapee.wc.IFileTreeCoreCaster.prototype.superFileTreeCore

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.Folders The discovered folders (optional overlay).
 * @prop {!Array<{ name: string, path: string }>} [folders] The discovered folders. Default `[]`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.Folders_Safe The discovered folders (required overlay).
 * @prop {!Array<{ name: string, path: string }>} folders The discovered folders.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.Files The discovered files (optional overlay).
 * @prop {!Array<{ name: string, path: string }>} [files] The discovered files. Default `[]`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.Files_Safe The discovered files (required overlay).
 * @prop {!Array<{ name: string, path: string }>} files The discovered files.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.LoadingFiles Whether the items are being loaded from the remote (optional overlay).
 * @prop {boolean} [loadingFiles=false] Whether the items are being loaded from the remote. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.LoadingFiles_Safe Whether the items are being loaded from the remote (required overlay).
 * @prop {boolean} loadingFiles Whether the items are being loaded from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.HasMoreFiles Whether there are more items to load (optional overlay).
 * @prop {?boolean} [hasMoreFiles=null] Whether there are more items to load. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.HasMoreFiles_Safe Whether there are more items to load (required overlay).
 * @prop {?boolean} hasMoreFiles Whether there are more items to load.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.LoadFilesError An error during loading of items from the remote (optional overlay).
 * @prop {?Error} [loadFilesError=null] An error during loading of items from the remote. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeCore.Model.LoadFilesError_Safe An error during loading of items from the remote (required overlay).
 * @prop {?Error} loadFilesError An error during loading of items from the remote.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreeCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeCore.__resetCore<!xyz.swapee.wc.IFileTreeCore>} xyz.swapee.wc.IFileTreeCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IFileTreeCore.resetCore} */
/**
 * Resets the _IFileTree_ core.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreeCore.__resetFileTreeCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeCore.__resetFileTreeCore<!xyz.swapee.wc.IFileTreeCore>} xyz.swapee.wc.IFileTreeCore._resetFileTreeCore */
/** @typedef {typeof xyz.swapee.wc.IFileTreeCore.resetFileTreeCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeCore.resetFileTreeCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/10-IFileTreeProcessor.xml}  4364cc64130ab254ac4b0d1bda38db92 */
/** @typedef {xyz.swapee.wc.IFileTreeComputer.Initialese&xyz.swapee.wc.IFileTreeController.Initialese} xyz.swapee.wc.IFileTreeProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeProcessor)} xyz.swapee.wc.AbstractFileTreeProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeProcessor} xyz.swapee.wc.FileTreeProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeProcessor
 */
xyz.swapee.wc.AbstractFileTreeProcessor = class extends /** @type {xyz.swapee.wc.AbstractFileTreeProcessor.constructor&xyz.swapee.wc.FileTreeProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeProcessor.prototype.constructor = xyz.swapee.wc.AbstractFileTreeProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeCore|typeof xyz.swapee.wc.FileTreeCore)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeProcessor}
 */
xyz.swapee.wc.AbstractFileTreeProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeProcessor}
 */
xyz.swapee.wc.AbstractFileTreeProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeCore|typeof xyz.swapee.wc.FileTreeCore)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeProcessor}
 */
xyz.swapee.wc.AbstractFileTreeProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeCore|typeof xyz.swapee.wc.FileTreeCore)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeProcessor}
 */
xyz.swapee.wc.AbstractFileTreeProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeProcessor.Initialese[]) => xyz.swapee.wc.IFileTreeProcessor} xyz.swapee.wc.FileTreeProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileTreeProcessorCaster&xyz.swapee.wc.IFileTreeComputer&xyz.swapee.wc.IFileTreeCore&xyz.swapee.wc.IFileTreeController)} xyz.swapee.wc.IFileTreeProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController} xyz.swapee.wc.IFileTreeController.typeof */
/**
 * The processor to compute changes to the memory for the _IFileTree_.
 * @interface xyz.swapee.wc.IFileTreeProcessor
 */
xyz.swapee.wc.IFileTreeProcessor = class extends /** @type {xyz.swapee.wc.IFileTreeProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileTreeComputer.typeof&xyz.swapee.wc.IFileTreeCore.typeof&xyz.swapee.wc.IFileTreeController.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeProcessor.Initialese>)} xyz.swapee.wc.FileTreeProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeProcessor} xyz.swapee.wc.IFileTreeProcessor.typeof */
/**
 * A concrete class of _IFileTreeProcessor_ instances.
 * @constructor xyz.swapee.wc.FileTreeProcessor
 * @implements {xyz.swapee.wc.IFileTreeProcessor} The processor to compute changes to the memory for the _IFileTree_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeProcessor.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeProcessor = class extends /** @type {xyz.swapee.wc.FileTreeProcessor.constructor&xyz.swapee.wc.IFileTreeProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeProcessor}
 */
xyz.swapee.wc.FileTreeProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileTreeProcessor} */
xyz.swapee.wc.RecordIFileTreeProcessor

/** @typedef {xyz.swapee.wc.IFileTreeProcessor} xyz.swapee.wc.BoundIFileTreeProcessor */

/** @typedef {xyz.swapee.wc.FileTreeProcessor} xyz.swapee.wc.BoundFileTreeProcessor */

/**
 * Contains getters to cast the _IFileTreeProcessor_ interface.
 * @interface xyz.swapee.wc.IFileTreeProcessorCaster
 */
xyz.swapee.wc.IFileTreeProcessorCaster = class { }
/**
 * Cast the _IFileTreeProcessor_ instance into the _BoundIFileTreeProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeProcessor}
 */
xyz.swapee.wc.IFileTreeProcessorCaster.prototype.asIFileTreeProcessor
/**
 * Access the _FileTreeProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeProcessor}
 */
xyz.swapee.wc.IFileTreeProcessorCaster.prototype.superFileTreeProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/100-FileTreeMemory.xml}  4ce2c2c8ca88a2a291d5a61957ecba85 */
/**
 * The memory of the _IFileTree_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.FileTreeMemory
 */
xyz.swapee.wc.FileTreeMemory = class { }
/**
 * The host property. Default empty string.
 */
xyz.swapee.wc.FileTreeMemory.prototype.host = /** @type {string} */ (void 0)
/**
 * Whether the folder acts as heading. Default `false`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.heading = /** @type {boolean} */ (void 0)
/**
 * The root folder on the disk to read from. Default empty string.
 */
xyz.swapee.wc.FileTreeMemory.prototype.root = /** @type {string} */ (void 0)
/**
 * The name of the folder inside the root presented to the user. Default `.`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.folder = /** @type {string} */ (void 0)
/**
 * Whether to report hidden files (those that start with `.`). Default `false`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.showHidden = /** @type {boolean} */ (void 0)
/**
 * The file that is currently open. Default empty string.
 */
xyz.swapee.wc.FileTreeMemory.prototype.currentFilePath = /** @type {string} */ (void 0)
/**
 * Whether folder is currently expanded. Default `false`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.expanded = /** @type {boolean} */ (void 0)
/**
 * An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.foldersRotor = /** @type {Array<!xyz.swapee.wc.IFileTree.FolderBrushes>} */ (void 0)
/**
 * An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.filesRotor = /** @type {Array<!xyz.swapee.wc.IFileTree.FileBrushes>} */ (void 0)
/**
 * The discovered folders. Default `[]`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.folders = /** @type {!Array<{ name: string, path: string }>} */ (void 0)
/**
 * The discovered files. Default `[]`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.files = /** @type {!Array<{ name: string, path: string }>} */ (void 0)
/**
 * Whether the items are being loaded from the remote. Default `false`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.loadingFiles = /** @type {boolean} */ (void 0)
/**
 * Whether there are more items to load. Default `null`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.hasMoreFiles = /** @type {?boolean} */ (void 0)
/**
 * An error during loading of items from the remote. Default `null`.
 */
xyz.swapee.wc.FileTreeMemory.prototype.loadFilesError = /** @type {?Error} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/102-FileTreeInputs.xml}  b3c0e2e4e661872fbf1d214964da806f */
/**
 * The inputs of the _IFileTree_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.FileTreeInputs
 */
xyz.swapee.wc.front.FileTreeInputs = class { }
/**
 * The host property. Default empty string.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.host = /** @type {string|undefined} */ (void 0)
/**
 * Whether the folder acts as heading. Default `false`.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.heading = /** @type {boolean|undefined} */ (void 0)
/**
 * The root folder on the disk to read from. Default empty string.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.root = /** @type {string|undefined} */ (void 0)
/**
 * The name of the folder inside the root presented to the user. Default `.`.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.folder = /** @type {string|undefined} */ (void 0)
/**
 * Whether to report hidden files (those that start with `.`). Default `false`.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.showHidden = /** @type {boolean|undefined} */ (void 0)
/**
 * The file that is currently open. Default empty string.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.currentFilePath = /** @type {string|undefined} */ (void 0)
/**
 * Whether folder is currently expanded. Default `true`.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.expanded = /** @type {boolean|undefined} */ (void 0)
/**
 * An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.foldersRotor = /** @type {(Array<!xyz.swapee.wc.IFileTree.FolderBrushes>)|undefined} */ (void 0)
/**
 * An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 */
xyz.swapee.wc.front.FileTreeInputs.prototype.filesRotor = /** @type {(Array<!xyz.swapee.wc.IFileTree.FileBrushes>)|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/11-IFileTree.xml}  38785a4dd77c318570eb848c4e6f1178 */
/**
 * An atomic wrapper for the _IFileTree_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.FileTreeEnv
 */
xyz.swapee.wc.FileTreeEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.FileTreeEnv.prototype.fileTree = /** @type {xyz.swapee.wc.IFileTree} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.IFileTreeController.Inputs>&xyz.swapee.wc.IFileTreeProcessor.Initialese&xyz.swapee.wc.IFileTreeComputer.Initialese&xyz.swapee.wc.IFileTreeController.Initialese} xyz.swapee.wc.IFileTree.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileTree)} xyz.swapee.wc.AbstractFileTree.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTree} xyz.swapee.wc.FileTree.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTree` interface.
 * @constructor xyz.swapee.wc.AbstractFileTree
 */
xyz.swapee.wc.AbstractFileTree = class extends /** @type {xyz.swapee.wc.AbstractFileTree.constructor&xyz.swapee.wc.FileTree.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTree.prototype.constructor = xyz.swapee.wc.AbstractFileTree
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTree.class = /** @type {typeof xyz.swapee.wc.AbstractFileTree} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTree|typeof xyz.swapee.wc.FileTree)|(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTree}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTree.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTree}
 */
xyz.swapee.wc.AbstractFileTree.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTree}
 */
xyz.swapee.wc.AbstractFileTree.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTree|typeof xyz.swapee.wc.FileTree)|(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTree}
 */
xyz.swapee.wc.AbstractFileTree.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTree|typeof xyz.swapee.wc.FileTree)|(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTree}
 */
xyz.swapee.wc.AbstractFileTree.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTree.Initialese[]) => xyz.swapee.wc.IFileTree} xyz.swapee.wc.FileTreeConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTree.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IFileTree.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IFileTree.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IFileTree.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.FileTreeMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.FileTreeClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IFileTreeFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeCaster&xyz.swapee.wc.IFileTreeProcessor&xyz.swapee.wc.IFileTreeComputer&xyz.swapee.wc.IFileTreeController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.IFileTreeController.Inputs, !xyz.swapee.wc.FileTreeLand>)} xyz.swapee.wc.IFileTree.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeProcessor} xyz.swapee.wc.IFileTreeProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController} xyz.swapee.wc.IFileTreeController.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IFileTree
 */
xyz.swapee.wc.IFileTree = class extends /** @type {xyz.swapee.wc.IFileTree.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileTreeProcessor.typeof&xyz.swapee.wc.IFileTreeComputer.typeof&xyz.swapee.wc.IFileTreeController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTree* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTree.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTree.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileTree&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTree.Initialese>)} xyz.swapee.wc.FileTree.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTree} xyz.swapee.wc.IFileTree.typeof */
/**
 * A concrete class of _IFileTree_ instances.
 * @constructor xyz.swapee.wc.FileTree
 * @implements {xyz.swapee.wc.IFileTree} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTree.Initialese>} ‎
 */
xyz.swapee.wc.FileTree = class extends /** @type {xyz.swapee.wc.FileTree.constructor&xyz.swapee.wc.IFileTree.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTree* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTree.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTree* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTree.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTree.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTree}
 */
xyz.swapee.wc.FileTree.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTree.
 * @interface xyz.swapee.wc.IFileTreeFields
 */
xyz.swapee.wc.IFileTreeFields = class { }
/**
 * The input pins of the _IFileTree_ port.
 */
xyz.swapee.wc.IFileTreeFields.prototype.pinout = /** @type {!xyz.swapee.wc.IFileTree.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTree} */
xyz.swapee.wc.RecordIFileTree

/** @typedef {xyz.swapee.wc.IFileTree} xyz.swapee.wc.BoundIFileTree */

/** @typedef {xyz.swapee.wc.FileTree} xyz.swapee.wc.BoundFileTree */

/** @typedef {xyz.swapee.wc.IFileTreeController.Inputs} xyz.swapee.wc.IFileTree.Pinout The input pins of the _IFileTree_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IFileTreeController.Inputs>)} xyz.swapee.wc.IFileTreeBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IFileTreeBuffer
 */
xyz.swapee.wc.IFileTreeBuffer = class extends /** @type {xyz.swapee.wc.IFileTreeBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IFileTreeBuffer.prototype.constructor = xyz.swapee.wc.IFileTreeBuffer

/**
 * A concrete class of _IFileTreeBuffer_ instances.
 * @constructor xyz.swapee.wc.FileTreeBuffer
 * @implements {xyz.swapee.wc.IFileTreeBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.FileTreeBuffer = class extends xyz.swapee.wc.IFileTreeBuffer { }
xyz.swapee.wc.FileTreeBuffer.prototype.constructor = xyz.swapee.wc.FileTreeBuffer

/**
 * Contains getters to cast the _IFileTree_ interface.
 * @interface xyz.swapee.wc.IFileTreeCaster
 */
xyz.swapee.wc.IFileTreeCaster = class { }
/**
 * Cast the _IFileTree_ instance into the _BoundIFileTree_ type.
 * @type {!xyz.swapee.wc.BoundIFileTree}
 */
xyz.swapee.wc.IFileTreeCaster.prototype.asIFileTree
/**
 * Access the _FileTree_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTree}
 */
xyz.swapee.wc.IFileTreeCaster.prototype.superFileTree

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/110-FileTreeSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileTreeMemoryPQs
 */
xyz.swapee.wc.FileTreeMemoryPQs = class {
  constructor() {
    /**
     * `g3a9f`
     */
    this.root=/** @type {string} */ (void 0)
    /**
     * `i5114`
     */
    this.folder=/** @type {string} */ (void 0)
    /**
     * `beb5c`
     */
    this.showHidden=/** @type {string} */ (void 0)
    /**
     * `ba613`
     */
    this.expanded=/** @type {string} */ (void 0)
    /**
     * `cd0d7`
     */
    this.foldersRotor=/** @type {string} */ (void 0)
    /**
     * `c87fd`
     */
    this.filesRotor=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.FileTreeMemoryPQs.prototype.constructor = xyz.swapee.wc.FileTreeMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileTreeMemoryQPs
 * @dict
 */
xyz.swapee.wc.FileTreeMemoryQPs = class { }
/**
 * `root`
 */
xyz.swapee.wc.FileTreeMemoryQPs.prototype.g3a9f = /** @type {string} */ (void 0)
/**
 * `folder`
 */
xyz.swapee.wc.FileTreeMemoryQPs.prototype.i5114 = /** @type {string} */ (void 0)
/**
 * `showHidden`
 */
xyz.swapee.wc.FileTreeMemoryQPs.prototype.beb5c = /** @type {string} */ (void 0)
/**
 * `expanded`
 */
xyz.swapee.wc.FileTreeMemoryQPs.prototype.ba613 = /** @type {string} */ (void 0)
/**
 * `foldersRotor`
 */
xyz.swapee.wc.FileTreeMemoryQPs.prototype.cd0d7 = /** @type {string} */ (void 0)
/**
 * `filesRotor`
 */
xyz.swapee.wc.FileTreeMemoryQPs.prototype.c87fd = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.FileTreeMemoryPQs)} xyz.swapee.wc.FileTreeInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeMemoryPQs} xyz.swapee.wc.FileTreeMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileTreeInputsPQs
 */
xyz.swapee.wc.FileTreeInputsPQs = class extends /** @type {xyz.swapee.wc.FileTreeInputsPQs.constructor&xyz.swapee.wc.FileTreeMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.FileTreeInputsPQs.prototype.constructor = xyz.swapee.wc.FileTreeInputsPQs

/** @typedef {function(new: xyz.swapee.wc.FileTreeMemoryPQs)} xyz.swapee.wc.FileTreeInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileTreeInputsQPs
 * @dict
 */
xyz.swapee.wc.FileTreeInputsQPs = class extends /** @type {xyz.swapee.wc.FileTreeInputsQPs.constructor&xyz.swapee.wc.FileTreeMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.FileTreeInputsQPs.prototype.constructor = xyz.swapee.wc.FileTreeInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileTreeCachePQs
 */
xyz.swapee.wc.FileTreeCachePQs = class {
  constructor() {
    /**
     * `hcb31`
     */
    this.folders=/** @type {string} */ (void 0)
    /**
     * `e5b96`
     */
    this.files=/** @type {string} */ (void 0)
    /**
     * `ga4bb`
     */
    this.loadingFiles=/** @type {string} */ (void 0)
    /**
     * `h5c89`
     */
    this.hasMoreFiles=/** @type {string} */ (void 0)
    /**
     * `efd6e`
     */
    this.loadFilesError=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.FileTreeCachePQs.prototype.constructor = xyz.swapee.wc.FileTreeCachePQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileTreeCacheQPs
 * @dict
 */
xyz.swapee.wc.FileTreeCacheQPs = class { }
/**
 * `folders`
 */
xyz.swapee.wc.FileTreeCacheQPs.prototype.hcb31 = /** @type {string} */ (void 0)
/**
 * `files`
 */
xyz.swapee.wc.FileTreeCacheQPs.prototype.e5b96 = /** @type {string} */ (void 0)
/**
 * `loadingFiles`
 */
xyz.swapee.wc.FileTreeCacheQPs.prototype.ga4bb = /** @type {string} */ (void 0)
/**
 * `hasMoreFiles`
 */
xyz.swapee.wc.FileTreeCacheQPs.prototype.h5c89 = /** @type {string} */ (void 0)
/**
 * `loadFilesError`
 */
xyz.swapee.wc.FileTreeCacheQPs.prototype.efd6e = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileTreeVdusPQs
 */
xyz.swapee.wc.FileTreeVdusPQs = class {
  constructor() {
    /**
     * `bf8d1`
     */
    this.FolderTemplate=/** @type {string} */ (void 0)
    /**
     * `bf8d2`
     */
    this.FileTemplate=/** @type {string} */ (void 0)
    /**
     * `bf8d3`
     */
    this.FolderLa=/** @type {string} */ (void 0)
    /**
     * `bf8d4`
     */
    this.ExpandedIc=/** @type {string} */ (void 0)
    /**
     * `bf8d5`
     */
    this.CollapsedIc=/** @type {string} */ (void 0)
    /**
     * `bf8d6`
     */
    this.Folders=/** @type {string} */ (void 0)
    /**
     * `bf8d7`
     */
    this.Files=/** @type {string} */ (void 0)
    /**
     * `bf8d8`
     */
    this.FolderWr=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.FileTreeVdusPQs.prototype.constructor = xyz.swapee.wc.FileTreeVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileTreeVdusQPs
 * @dict
 */
xyz.swapee.wc.FileTreeVdusQPs = class { }
/**
 * `FolderTemplate`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d1 = /** @type {string} */ (void 0)
/**
 * `FileTemplate`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d2 = /** @type {string} */ (void 0)
/**
 * `FolderLa`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d3 = /** @type {string} */ (void 0)
/**
 * `ExpandedIc`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d4 = /** @type {string} */ (void 0)
/**
 * `CollapsedIc`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d5 = /** @type {string} */ (void 0)
/**
 * `Folders`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d6 = /** @type {string} */ (void 0)
/**
 * `Files`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d7 = /** @type {string} */ (void 0)
/**
 * `FolderWr`
 */
xyz.swapee.wc.FileTreeVdusQPs.prototype.bf8d8 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/12-IFileTreeHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IFileTreeHtmlComponentUtilFields)} xyz.swapee.wc.IFileTreeHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IFileTreeHtmlComponentUtil */
xyz.swapee.wc.IFileTreeHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IFileTreeHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IFileTreeHtmlComponentUtil.router} */
xyz.swapee.wc.IFileTreeHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IFileTreeHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.FileTreeHtmlComponentUtil
 * @implements {xyz.swapee.wc.IFileTreeHtmlComponentUtil} ‎
 */
xyz.swapee.wc.FileTreeHtmlComponentUtil = class extends xyz.swapee.wc.IFileTreeHtmlComponentUtil { }
xyz.swapee.wc.FileTreeHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.FileTreeHtmlComponentUtil

/**
 * Fields of the IFileTreeHtmlComponentUtil.
 * @interface xyz.swapee.wc.IFileTreeHtmlComponentUtilFields
 */
xyz.swapee.wc.IFileTreeHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IFileTreeHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IFileTreeHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeHtmlComponentUtil} */
xyz.swapee.wc.RecordIFileTreeHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IFileTreeHtmlComponentUtil} xyz.swapee.wc.BoundIFileTreeHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.FileTreeHtmlComponentUtil} xyz.swapee.wc.BoundFileTreeHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.IFileEditorPort} FileEditor
 * @prop {typeof xyz.swapee.wc.IFileTreePort} FileTree The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.FileEditorMemory} FileEditor
 * @prop {!xyz.swapee.wc.FileTreeMemory} FileTree
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.IFileEditor.Pinout} FileEditor
 * @prop {!xyz.swapee.wc.IFileTree.Pinout} FileTree
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IFileTreeHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeHtmlComponentUtil.__router<!xyz.swapee.wc.IFileTreeHtmlComponentUtil>} xyz.swapee.wc.IFileTreeHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IFileTreeHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `FileEditor` _typeof IFileEditorPort_
 * - `FileTree` _typeof IFileTreePort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `FileEditor` _!FileEditorMemory_
 * - `FileTree` _!FileTreeMemory_
 * @param {!xyz.swapee.wc.IFileTreeHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `FileEditor` _!IFileEditor.Pinout_
 * - `FileTree` _!IFileTree.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IFileTreeHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/12-IFileTreeHtmlComponent.xml}  f15684b4eafd78b710b575f2ee21152d */
/** @typedef {xyz.swapee.wc.back.IFileTreeController.Initialese&xyz.swapee.wc.back.IFileTreeScreen.Initialese&xyz.swapee.wc.IFileTree.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IFileTreeGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IFileTreeProcessor.Initialese&xyz.swapee.wc.IFileTreeComputer.Initialese&xyz.swapee.wc.IFileTreeGenerator.Initialese} xyz.swapee.wc.IFileTreeHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeHtmlComponent)} xyz.swapee.wc.AbstractFileTreeHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeHtmlComponent} xyz.swapee.wc.FileTreeHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeHtmlComponent
 */
xyz.swapee.wc.AbstractFileTreeHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractFileTreeHtmlComponent.constructor&xyz.swapee.wc.FileTreeHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractFileTreeHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeHtmlComponent|typeof xyz.swapee.wc.FileTreeHtmlComponent)|(!xyz.swapee.wc.back.IFileTreeController|typeof xyz.swapee.wc.back.FileTreeController)|(!xyz.swapee.wc.back.IFileTreeScreen|typeof xyz.swapee.wc.back.FileTreeScreen)|(!xyz.swapee.wc.IFileTree|typeof xyz.swapee.wc.FileTree)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IFileTreeGPU|typeof xyz.swapee.wc.FileTreeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeGenerator|typeof xyz.swapee.wc.FileTreeGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeHtmlComponent}
 */
xyz.swapee.wc.AbstractFileTreeHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeHtmlComponent}
 */
xyz.swapee.wc.AbstractFileTreeHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeHtmlComponent|typeof xyz.swapee.wc.FileTreeHtmlComponent)|(!xyz.swapee.wc.back.IFileTreeController|typeof xyz.swapee.wc.back.FileTreeController)|(!xyz.swapee.wc.back.IFileTreeScreen|typeof xyz.swapee.wc.back.FileTreeScreen)|(!xyz.swapee.wc.IFileTree|typeof xyz.swapee.wc.FileTree)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IFileTreeGPU|typeof xyz.swapee.wc.FileTreeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeGenerator|typeof xyz.swapee.wc.FileTreeGenerator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeHtmlComponent}
 */
xyz.swapee.wc.AbstractFileTreeHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeHtmlComponent|typeof xyz.swapee.wc.FileTreeHtmlComponent)|(!xyz.swapee.wc.back.IFileTreeController|typeof xyz.swapee.wc.back.FileTreeController)|(!xyz.swapee.wc.back.IFileTreeScreen|typeof xyz.swapee.wc.back.FileTreeScreen)|(!xyz.swapee.wc.IFileTree|typeof xyz.swapee.wc.FileTree)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IFileTreeGPU|typeof xyz.swapee.wc.FileTreeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IFileTreeProcessor|typeof xyz.swapee.wc.FileTreeProcessor)|(!xyz.swapee.wc.IFileTreeComputer|typeof xyz.swapee.wc.FileTreeComputer)|(!xyz.swapee.wc.IFileTreeGenerator|typeof xyz.swapee.wc.FileTreeGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeHtmlComponent}
 */
xyz.swapee.wc.AbstractFileTreeHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeHtmlComponent.Initialese[]) => xyz.swapee.wc.IFileTreeHtmlComponent} xyz.swapee.wc.FileTreeHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileTreeHtmlComponentCaster&xyz.swapee.wc.back.IFileTreeController&xyz.swapee.wc.back.IFileTreeScreen&xyz.swapee.wc.IFileTree&com.webcircuits.ILanded<!xyz.swapee.wc.FileTreeLand>&xyz.swapee.wc.IFileTreeGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.IFileTreeController.Inputs, !HTMLDivElement, !xyz.swapee.wc.FileTreeLand>&xyz.swapee.wc.IFileTreeProcessor&xyz.swapee.wc.IFileTreeComputer&xyz.swapee.wc.IFileTreeGenerator)} xyz.swapee.wc.IFileTreeHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileTreeController} xyz.swapee.wc.back.IFileTreeController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IFileTreeScreen} xyz.swapee.wc.back.IFileTreeScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGPU} xyz.swapee.wc.IFileTreeGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator} xyz.swapee.wc.IFileTreeGenerator.typeof */
/**
 * The _IFileTree_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IFileTreeHtmlComponent
 */
xyz.swapee.wc.IFileTreeHtmlComponent = class extends /** @type {xyz.swapee.wc.IFileTreeHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IFileTreeController.typeof&xyz.swapee.wc.back.IFileTreeScreen.typeof&xyz.swapee.wc.IFileTree.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IFileTreeGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IFileTreeProcessor.typeof&xyz.swapee.wc.IFileTreeComputer.typeof&xyz.swapee.wc.IFileTreeGenerator.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeHtmlComponent.Initialese>)} xyz.swapee.wc.FileTreeHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeHtmlComponent} xyz.swapee.wc.IFileTreeHtmlComponent.typeof */
/**
 * A concrete class of _IFileTreeHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.FileTreeHtmlComponent
 * @implements {xyz.swapee.wc.IFileTreeHtmlComponent} The _IFileTree_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeHtmlComponent = class extends /** @type {xyz.swapee.wc.FileTreeHtmlComponent.constructor&xyz.swapee.wc.IFileTreeHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeHtmlComponent}
 */
xyz.swapee.wc.FileTreeHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileTreeHtmlComponent} */
xyz.swapee.wc.RecordIFileTreeHtmlComponent

/** @typedef {xyz.swapee.wc.IFileTreeHtmlComponent} xyz.swapee.wc.BoundIFileTreeHtmlComponent */

/** @typedef {xyz.swapee.wc.FileTreeHtmlComponent} xyz.swapee.wc.BoundFileTreeHtmlComponent */

/**
 * Contains getters to cast the _IFileTreeHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IFileTreeHtmlComponentCaster
 */
xyz.swapee.wc.IFileTreeHtmlComponentCaster = class { }
/**
 * Cast the _IFileTreeHtmlComponent_ instance into the _BoundIFileTreeHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeHtmlComponent}
 */
xyz.swapee.wc.IFileTreeHtmlComponentCaster.prototype.asIFileTreeHtmlComponent
/**
 * Access the _FileTreeHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeHtmlComponent}
 */
xyz.swapee.wc.IFileTreeHtmlComponentCaster.prototype.superFileTreeHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/130-IFileTreeElement.xml}  9f221944b33502b70125deb419ef5933 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.FileTreeLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.IFileEditor>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.IFileTreeElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IFileTreeElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeElement)} xyz.swapee.wc.AbstractFileTreeElement.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeElement} xyz.swapee.wc.FileTreeElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeElement` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeElement
 */
xyz.swapee.wc.AbstractFileTreeElement = class extends /** @type {xyz.swapee.wc.AbstractFileTreeElement.constructor&xyz.swapee.wc.FileTreeElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeElement.prototype.constructor = xyz.swapee.wc.AbstractFileTreeElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeElement.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeElement|typeof xyz.swapee.wc.FileTreeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeElement}
 */
xyz.swapee.wc.AbstractFileTreeElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElement}
 */
xyz.swapee.wc.AbstractFileTreeElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeElement|typeof xyz.swapee.wc.FileTreeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElement}
 */
xyz.swapee.wc.AbstractFileTreeElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeElement|typeof xyz.swapee.wc.FileTreeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElement}
 */
xyz.swapee.wc.AbstractFileTreeElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeElement.Initialese[]) => xyz.swapee.wc.IFileTreeElement} xyz.swapee.wc.FileTreeElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileTreeElementFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.IFileTreeElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.IFileTreeElement.Inputs, !xyz.swapee.wc.FileTreeLand>&com.webcircuits.ILanded<!xyz.swapee.wc.FileTreeLand>&guest.maurice.IMilleu<!xyz.swapee.wc.IFileEditor>)} xyz.swapee.wc.IFileTreeElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IFileTree_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IFileTreeElement
 */
xyz.swapee.wc.IFileTreeElement = class extends /** @type {xyz.swapee.wc.IFileTreeElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreeElement.solder} */
xyz.swapee.wc.IFileTreeElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IFileTreeElement.render} */
xyz.swapee.wc.IFileTreeElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IFileTreeElement.build} */
xyz.swapee.wc.IFileTreeElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IFileTreeElement.buildFileEditor} */
xyz.swapee.wc.IFileTreeElement.prototype.buildFileEditor = function() {}
/** @type {xyz.swapee.wc.IFileTreeElement.short} */
xyz.swapee.wc.IFileTreeElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IFileTreeElement.server} */
xyz.swapee.wc.IFileTreeElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IFileTreeElement.inducer} */
xyz.swapee.wc.IFileTreeElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeElement&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeElement.Initialese>)} xyz.swapee.wc.FileTreeElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement} xyz.swapee.wc.IFileTreeElement.typeof */
/**
 * A concrete class of _IFileTreeElement_ instances.
 * @constructor xyz.swapee.wc.FileTreeElement
 * @implements {xyz.swapee.wc.IFileTreeElement} A component description.
 *
 * The _IFileTree_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeElement.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeElement = class extends /** @type {xyz.swapee.wc.FileTreeElement.constructor&xyz.swapee.wc.IFileTreeElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeElement}
 */
xyz.swapee.wc.FileTreeElement.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeElement.
 * @interface xyz.swapee.wc.IFileTreeElementFields
 */
xyz.swapee.wc.IFileTreeElementFields = class { }
/**
 * The element-specific inputs to the _IFileTree_ component.
 */
xyz.swapee.wc.IFileTreeElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileTreeElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IFileTreeElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeElement} */
xyz.swapee.wc.RecordIFileTreeElement

/** @typedef {xyz.swapee.wc.IFileTreeElement} xyz.swapee.wc.BoundIFileTreeElement */

/** @typedef {xyz.swapee.wc.FileTreeElement} xyz.swapee.wc.BoundFileTreeElement */

/** @typedef {xyz.swapee.wc.IFileTreePort.Inputs&xyz.swapee.wc.IFileTreeDisplay.Queries&xyz.swapee.wc.IFileTreeController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IFileTreeElementPort.Inputs} xyz.swapee.wc.IFileTreeElement.Inputs The element-specific inputs to the _IFileTree_ component. */

/**
 * Contains getters to cast the _IFileTreeElement_ interface.
 * @interface xyz.swapee.wc.IFileTreeElementCaster
 */
xyz.swapee.wc.IFileTreeElementCaster = class { }
/**
 * Cast the _IFileTreeElement_ instance into the _BoundIFileTreeElement_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeElement}
 */
xyz.swapee.wc.IFileTreeElementCaster.prototype.asIFileTreeElement
/**
 * Access the _FileTreeElement_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeElement}
 */
xyz.swapee.wc.IFileTreeElementCaster.prototype.superFileTreeElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.FileTreeMemory, props: !xyz.swapee.wc.IFileTreeElement.Inputs) => Object<string, *>} xyz.swapee.wc.IFileTreeElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeElement.__solder<!xyz.swapee.wc.IFileTreeElement>} xyz.swapee.wc.IFileTreeElement._solder */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.FileTreeMemory} model The model.
 * - `host` _string_ The host property. Default empty string.
 * - `root` _string_ The root folder on the disk to read from. Default empty string.
 * - `folder` _string_ The name of the folder inside the root presented to the user. Default `.`.
 * - `showHidden` _boolean_ Whether to report hidden files (those that start with `.`). Default `false`.
 * - `expanded` _boolean_ Whether folder is currently expanded. Default `false`.
 * - `foldersRotor` _Array&lt;!IFileTree.FolderBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 * - `filesRotor` _Array&lt;!IFileTree.FileBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 * - `folders` _!Array&lt;string&gt;_ The discovered folders. Default `[]`.
 * - `files` _!Array&lt;string&gt;_ The discovered files. Default `[]`.
 * - `loadingFiles` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreFiles` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadFilesError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileTreeElement.Inputs} props The element props.
 * - `[host=null]` _&#42;?_ The host property. ⤴ *IFileTreeOuterCore.WeakModel.Host* ⤴ *IFileTreeOuterCore.WeakModel.Host* Default `null`.
 * - `[root=null]` _&#42;?_ The root folder on the disk to read from. ⤴ *IFileTreeOuterCore.WeakModel.Root* ⤴ *IFileTreeOuterCore.WeakModel.Root* Default `null`.
 * - `[folder="."]` _&#42;?_ The name of the folder inside the root presented to the user. ⤴ *IFileTreeOuterCore.WeakModel.Folder* ⤴ *IFileTreeOuterCore.WeakModel.Folder* Default `.`.
 * - `[showHidden=null]` _&#42;?_ Whether to report hidden files (those that start with `.`). ⤴ *IFileTreeOuterCore.WeakModel.ShowHidden* ⤴ *IFileTreeOuterCore.WeakModel.ShowHidden* Default `null`.
 * - `[expanded=true]` _&#42;?_ Whether folder is currently expanded. ⤴ *IFileTreeOuterCore.WeakModel.Expanded* ⤴ *IFileTreeOuterCore.WeakModel.Expanded* Default `true`.
 * - `[foldersRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. ⤴ *IFileTreeOuterCore.WeakModel.FoldersRotor* ⤴ *IFileTreeOuterCore.WeakModel.FoldersRotor* Default `null`.
 * - `[filesRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. ⤴ *IFileTreeOuterCore.WeakModel.FilesRotor* ⤴ *IFileTreeOuterCore.WeakModel.FilesRotor* Default `null`.
 * - `[fileEditorSel=""]` _string?_ The query to discover the _FileEditor_ VDU. ⤴ *IFileTreeDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IFileTreeElementPort.Inputs.NoSolder* Default `false`.
 * - `[contentWrOpts]` _!Object?_ The options to pass to the _ContentWr_ vdu. ⤴ *IFileTreeElementPort.Inputs.ContentWrOpts* Default `{}`.
 * - `[expandedIcOpts]` _!Object?_ The options to pass to the _ExpandedIc_ vdu. ⤴ *IFileTreeElementPort.Inputs.ExpandedIcOpts* Default `{}`.
 * - `[collapsedIcOpts]` _!Object?_ The options to pass to the _CollapsedIc_ vdu. ⤴ *IFileTreeElementPort.Inputs.CollapsedIcOpts* Default `{}`.
 * - `[folderWrOpts]` _!Object?_ The options to pass to the _FolderWr_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderWrOpts* Default `{}`.
 * - `[folderLaOpts]` _!Object?_ The options to pass to the _FolderLa_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderLaOpts* Default `{}`.
 * - `[foldersOpts]` _!Object?_ The options to pass to the _Folders_ vdu. ⤴ *IFileTreeElementPort.Inputs.FoldersOpts* Default `{}`.
 * - `[filesOpts]` _!Object?_ The options to pass to the _Files_ vdu. ⤴ *IFileTreeElementPort.Inputs.FilesOpts* Default `{}`.
 * - `[fileEditorOpts]` _!Object?_ The options to pass to the _FileEditor_ vdu. ⤴ *IFileTreeElementPort.Inputs.FileEditorOpts* Default `{}`.
 * - `[folderTemplateOpts]` _!Object?_ The options to pass to the _FolderTemplate_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderTemplateOpts* Default `{}`.
 * - `[fileTemplateOpts]` _!Object?_ The options to pass to the _FileTemplate_ vdu. ⤴ *IFileTreeElementPort.Inputs.FileTemplateOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IFileTreeElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.FileTreeMemory, instance?: !xyz.swapee.wc.IFileTreeScreen&xyz.swapee.wc.IFileTreeController) => !engineering.type.VNode} xyz.swapee.wc.IFileTreeElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeElement.__render<!xyz.swapee.wc.IFileTreeElement>} xyz.swapee.wc.IFileTreeElement._render */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.FileTreeMemory} [model] The model for the view.
 * - `host` _string_ The host property. Default empty string.
 * - `root` _string_ The root folder on the disk to read from. Default empty string.
 * - `folder` _string_ The name of the folder inside the root presented to the user. Default `.`.
 * - `showHidden` _boolean_ Whether to report hidden files (those that start with `.`). Default `false`.
 * - `expanded` _boolean_ Whether folder is currently expanded. Default `false`.
 * - `foldersRotor` _Array&lt;!IFileTree.FolderBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 * - `filesRotor` _Array&lt;!IFileTree.FileBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 * - `folders` _!Array&lt;string&gt;_ The discovered folders. Default `[]`.
 * - `files` _!Array&lt;string&gt;_ The discovered files. Default `[]`.
 * - `loadingFiles` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreFiles` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadFilesError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileTreeScreen&xyz.swapee.wc.IFileTreeController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IFileTreeElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IFileTreeElement.build.Cores, instances: !xyz.swapee.wc.IFileTreeElement.build.Instances) => ?} xyz.swapee.wc.IFileTreeElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeElement.__build<!xyz.swapee.wc.IFileTreeElement>} xyz.swapee.wc.IFileTreeElement._build */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IFileTreeElement.build.Cores} cores The models of components on the land.
 * - `FileEditor` _!xyz.swapee.wc.IFileEditorCore.Model_
 * @param {!xyz.swapee.wc.IFileTreeElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `FileEditor` _!xyz.swapee.wc.IFileEditor_
 * @return {?}
 */
xyz.swapee.wc.IFileTreeElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.IFileEditorCore.Model} FileEditor
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.IFileEditor} FileEditor
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IFileEditorCore.Model, instance: !xyz.swapee.wc.IFileEditor) => ?} xyz.swapee.wc.IFileTreeElement.__buildFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeElement.__buildFileEditor<!xyz.swapee.wc.IFileTreeElement>} xyz.swapee.wc.IFileTreeElement._buildFileEditor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement.buildFileEditor} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IFileEditor_ component.
 * @param {!xyz.swapee.wc.IFileEditorCore.Model} model
 * - `[content=null]` _?string?_ The read content of the file. ⤴ *IFileEditorOuterCore.Model.Content* Default `null`.
 * - `[path=""]` _string?_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path* Default empty string.
 * - `[filename=""]` _string?_ The name of the file. ⤴ *IFileEditorOuterCore.Model.Filename* Default empty string.
 * - `[hasChanges=false]` _boolean?_ Whether the file has changes. ⤴ *IFileEditorOuterCore.Model.HasChanges* Default `false`.
 * - `[readFile=false]` _boolean?_ . ⤴ *IFileEditorOuterCore.Model.ReadFile* Default `false`.
 * - `[saveFile=false]` _boolean?_ . ⤴ *IFileEditorOuterCore.Model.SaveFile* Default `false`.
 * - `[loadingReadFile=false]` _boolean?_ Whether the items are being loaded from the remote. ⤴ *IFileEditorCore.Model.LoadingReadFile* Default `false`.
 * - `[hasMoreReadFile=null]` _?boolean?_ Whether there are more items to load. ⤴ *IFileEditorCore.Model.HasMoreReadFile* Default `null`.
 * - `[loadReadFileError=null]` _?Error?_ An error during loading of items from the remote. ⤴ *IFileEditorCore.Model.LoadReadFileError* Default `null`.
 * - `[loadingSaveFile=false]` _boolean?_ Whether the items are being loaded from the remote. ⤴ *IFileEditorCore.Model.LoadingSaveFile* Default `false`.
 * - `[hasMoreSaveFile=null]` _?boolean?_ Whether there are more items to load. ⤴ *IFileEditorCore.Model.HasMoreSaveFile* Default `null`.
 * - `[loadSaveFileError=null]` _?Error?_ An error during loading of items from the remote. ⤴ *IFileEditorCore.Model.LoadSaveFileError* Default `null`.
 * @param {!xyz.swapee.wc.IFileEditor} instance
 * @return {?}
 */
xyz.swapee.wc.IFileTreeElement.buildFileEditor = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.FileTreeMemory, ports: !xyz.swapee.wc.IFileTreeElement.short.Ports, cores: !xyz.swapee.wc.IFileTreeElement.short.Cores) => ?} xyz.swapee.wc.IFileTreeElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeElement.__short<!xyz.swapee.wc.IFileTreeElement>} xyz.swapee.wc.IFileTreeElement._short */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.FileTreeMemory} model The model from which to feed properties to peer's ports.
 * - `host` _string_ The host property. Default empty string.
 * - `root` _string_ The root folder on the disk to read from. Default empty string.
 * - `folder` _string_ The name of the folder inside the root presented to the user. Default `.`.
 * - `showHidden` _boolean_ Whether to report hidden files (those that start with `.`). Default `false`.
 * - `expanded` _boolean_ Whether folder is currently expanded. Default `false`.
 * - `foldersRotor` _Array&lt;!IFileTree.FolderBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 * - `filesRotor` _Array&lt;!IFileTree.FileBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 * - `folders` _!Array&lt;string&gt;_ The discovered folders. Default `[]`.
 * - `files` _!Array&lt;string&gt;_ The discovered files. Default `[]`.
 * - `loadingFiles` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreFiles` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadFilesError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileTreeElement.short.Ports} ports The ports of the peers.
 * - `FileEditor` _typeof xyz.swapee.wc.IFileEditorPortInterface_
 * @param {!xyz.swapee.wc.IFileTreeElement.short.Cores} cores The cores of the peers.
 * - `FileEditor` _xyz.swapee.wc.IFileEditorCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IFileTreeElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.IFileEditorPortInterface} FileEditor
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.IFileEditorCore.Model} FileEditor
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.FileTreeMemory, inputs: !xyz.swapee.wc.IFileTreeElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IFileTreeElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeElement.__server<!xyz.swapee.wc.IFileTreeElement>} xyz.swapee.wc.IFileTreeElement._server */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.FileTreeMemory} memory The memory registers.
 * - `host` _string_ The host property. Default empty string.
 * - `root` _string_ The root folder on the disk to read from. Default empty string.
 * - `folder` _string_ The name of the folder inside the root presented to the user. Default `.`.
 * - `showHidden` _boolean_ Whether to report hidden files (those that start with `.`). Default `false`.
 * - `expanded` _boolean_ Whether folder is currently expanded. Default `false`.
 * - `foldersRotor` _Array&lt;!IFileTree.FolderBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 * - `filesRotor` _Array&lt;!IFileTree.FileBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 * - `folders` _!Array&lt;string&gt;_ The discovered folders. Default `[]`.
 * - `files` _!Array&lt;string&gt;_ The discovered files. Default `[]`.
 * - `loadingFiles` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreFiles` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadFilesError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileTreeElement.Inputs} inputs The inputs to the port.
 * - `[host=null]` _&#42;?_ The host property. ⤴ *IFileTreeOuterCore.WeakModel.Host* ⤴ *IFileTreeOuterCore.WeakModel.Host* Default `null`.
 * - `[root=null]` _&#42;?_ The root folder on the disk to read from. ⤴ *IFileTreeOuterCore.WeakModel.Root* ⤴ *IFileTreeOuterCore.WeakModel.Root* Default `null`.
 * - `[folder="."]` _&#42;?_ The name of the folder inside the root presented to the user. ⤴ *IFileTreeOuterCore.WeakModel.Folder* ⤴ *IFileTreeOuterCore.WeakModel.Folder* Default `.`.
 * - `[showHidden=null]` _&#42;?_ Whether to report hidden files (those that start with `.`). ⤴ *IFileTreeOuterCore.WeakModel.ShowHidden* ⤴ *IFileTreeOuterCore.WeakModel.ShowHidden* Default `null`.
 * - `[expanded=true]` _&#42;?_ Whether folder is currently expanded. ⤴ *IFileTreeOuterCore.WeakModel.Expanded* ⤴ *IFileTreeOuterCore.WeakModel.Expanded* Default `true`.
 * - `[foldersRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. ⤴ *IFileTreeOuterCore.WeakModel.FoldersRotor* ⤴ *IFileTreeOuterCore.WeakModel.FoldersRotor* Default `null`.
 * - `[filesRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. ⤴ *IFileTreeOuterCore.WeakModel.FilesRotor* ⤴ *IFileTreeOuterCore.WeakModel.FilesRotor* Default `null`.
 * - `[fileEditorSel=""]` _string?_ The query to discover the _FileEditor_ VDU. ⤴ *IFileTreeDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IFileTreeElementPort.Inputs.NoSolder* Default `false`.
 * - `[contentWrOpts]` _!Object?_ The options to pass to the _ContentWr_ vdu. ⤴ *IFileTreeElementPort.Inputs.ContentWrOpts* Default `{}`.
 * - `[expandedIcOpts]` _!Object?_ The options to pass to the _ExpandedIc_ vdu. ⤴ *IFileTreeElementPort.Inputs.ExpandedIcOpts* Default `{}`.
 * - `[collapsedIcOpts]` _!Object?_ The options to pass to the _CollapsedIc_ vdu. ⤴ *IFileTreeElementPort.Inputs.CollapsedIcOpts* Default `{}`.
 * - `[folderWrOpts]` _!Object?_ The options to pass to the _FolderWr_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderWrOpts* Default `{}`.
 * - `[folderLaOpts]` _!Object?_ The options to pass to the _FolderLa_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderLaOpts* Default `{}`.
 * - `[foldersOpts]` _!Object?_ The options to pass to the _Folders_ vdu. ⤴ *IFileTreeElementPort.Inputs.FoldersOpts* Default `{}`.
 * - `[filesOpts]` _!Object?_ The options to pass to the _Files_ vdu. ⤴ *IFileTreeElementPort.Inputs.FilesOpts* Default `{}`.
 * - `[fileEditorOpts]` _!Object?_ The options to pass to the _FileEditor_ vdu. ⤴ *IFileTreeElementPort.Inputs.FileEditorOpts* Default `{}`.
 * - `[folderTemplateOpts]` _!Object?_ The options to pass to the _FolderTemplate_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderTemplateOpts* Default `{}`.
 * - `[fileTemplateOpts]` _!Object?_ The options to pass to the _FileTemplate_ vdu. ⤴ *IFileTreeElementPort.Inputs.FileTemplateOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IFileTreeElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.FileTreeMemory, port?: !xyz.swapee.wc.IFileTreeElement.Inputs) => ?} xyz.swapee.wc.IFileTreeElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeElement.__inducer<!xyz.swapee.wc.IFileTreeElement>} xyz.swapee.wc.IFileTreeElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.FileTreeMemory} [model] The model of the component into which to induce the state.
 * - `host` _string_ The host property. Default empty string.
 * - `root` _string_ The root folder on the disk to read from. Default empty string.
 * - `folder` _string_ The name of the folder inside the root presented to the user. Default `.`.
 * - `showHidden` _boolean_ Whether to report hidden files (those that start with `.`). Default `false`.
 * - `expanded` _boolean_ Whether folder is currently expanded. Default `false`.
 * - `foldersRotor` _Array&lt;!IFileTree.FolderBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 * - `filesRotor` _Array&lt;!IFileTree.FileBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 * - `folders` _!Array&lt;string&gt;_ The discovered folders. Default `[]`.
 * - `files` _!Array&lt;string&gt;_ The discovered files. Default `[]`.
 * - `loadingFiles` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreFiles` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadFilesError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileTreeElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[host=null]` _&#42;?_ The host property. ⤴ *IFileTreeOuterCore.WeakModel.Host* ⤴ *IFileTreeOuterCore.WeakModel.Host* Default `null`.
 * - `[root=null]` _&#42;?_ The root folder on the disk to read from. ⤴ *IFileTreeOuterCore.WeakModel.Root* ⤴ *IFileTreeOuterCore.WeakModel.Root* Default `null`.
 * - `[folder="."]` _&#42;?_ The name of the folder inside the root presented to the user. ⤴ *IFileTreeOuterCore.WeakModel.Folder* ⤴ *IFileTreeOuterCore.WeakModel.Folder* Default `.`.
 * - `[showHidden=null]` _&#42;?_ Whether to report hidden files (those that start with `.`). ⤴ *IFileTreeOuterCore.WeakModel.ShowHidden* ⤴ *IFileTreeOuterCore.WeakModel.ShowHidden* Default `null`.
 * - `[expanded=true]` _&#42;?_ Whether folder is currently expanded. ⤴ *IFileTreeOuterCore.WeakModel.Expanded* ⤴ *IFileTreeOuterCore.WeakModel.Expanded* Default `true`.
 * - `[foldersRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. ⤴ *IFileTreeOuterCore.WeakModel.FoldersRotor* ⤴ *IFileTreeOuterCore.WeakModel.FoldersRotor* Default `null`.
 * - `[filesRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. ⤴ *IFileTreeOuterCore.WeakModel.FilesRotor* ⤴ *IFileTreeOuterCore.WeakModel.FilesRotor* Default `null`.
 * - `[fileEditorSel=""]` _string?_ The query to discover the _FileEditor_ VDU. ⤴ *IFileTreeDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IFileTreeElementPort.Inputs.NoSolder* Default `false`.
 * - `[contentWrOpts]` _!Object?_ The options to pass to the _ContentWr_ vdu. ⤴ *IFileTreeElementPort.Inputs.ContentWrOpts* Default `{}`.
 * - `[expandedIcOpts]` _!Object?_ The options to pass to the _ExpandedIc_ vdu. ⤴ *IFileTreeElementPort.Inputs.ExpandedIcOpts* Default `{}`.
 * - `[collapsedIcOpts]` _!Object?_ The options to pass to the _CollapsedIc_ vdu. ⤴ *IFileTreeElementPort.Inputs.CollapsedIcOpts* Default `{}`.
 * - `[folderWrOpts]` _!Object?_ The options to pass to the _FolderWr_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderWrOpts* Default `{}`.
 * - `[folderLaOpts]` _!Object?_ The options to pass to the _FolderLa_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderLaOpts* Default `{}`.
 * - `[foldersOpts]` _!Object?_ The options to pass to the _Folders_ vdu. ⤴ *IFileTreeElementPort.Inputs.FoldersOpts* Default `{}`.
 * - `[filesOpts]` _!Object?_ The options to pass to the _Files_ vdu. ⤴ *IFileTreeElementPort.Inputs.FilesOpts* Default `{}`.
 * - `[fileEditorOpts]` _!Object?_ The options to pass to the _FileEditor_ vdu. ⤴ *IFileTreeElementPort.Inputs.FileEditorOpts* Default `{}`.
 * - `[folderTemplateOpts]` _!Object?_ The options to pass to the _FolderTemplate_ vdu. ⤴ *IFileTreeElementPort.Inputs.FolderTemplateOpts* Default `{}`.
 * - `[fileTemplateOpts]` _!Object?_ The options to pass to the _FileTemplate_ vdu. ⤴ *IFileTreeElementPort.Inputs.FileTemplateOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IFileTreeElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/14-Records.xml}  f52653d086bb007566a617470138e384 */
/**
 * @typedef {Object} xyz.swapee.wc.IFileTree.FolderStator The stator of the _FolderDynamo_.
 * @prop {string} name The name of the folder.
 * @prop {number} count The total number of all items.
 * @prop {number} fileCount The number of files.
 * @prop {number} folderCount The number of folders.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTree.FileStator The stator of the _FileDynamo_.
 * @prop {string} path The path to the file.
 * @prop {string} name The name of the folder.
 * @prop {boolean} isOpen Whether the file is open in the editor.
 * @prop {boolean} isHidden Whether the file is hidden.
 * @prop {string} lang The language.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTree.FolderBrushes The folder.
 * The brushes inside the rotor of the _FolderDynamo_.
 * @prop {string} name The name of the folder.
 * @prop {number} count The total number of all items.
 * @prop {number} fileCount The number of files.
 * @prop {number} folderCount The number of folders.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTree.FileBrushes The file.
 * The brushes inside the rotor of the _FileDynamo_.
 * @prop {string} path The path to the file.
 * @prop {string} name The name of the folder.
 * @prop {boolean} isOpen Whether the file is open in the editor.
 * @prop {boolean} isHidden Whether the file is hidden.
 * @prop {string} lang The language.
 */

/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/140-IFileTreeElementPort.xml}  2228ecd811318a3e6095b3f7cfa6cee6 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IFileTreeElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeElementPort)} xyz.swapee.wc.AbstractFileTreeElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeElementPort} xyz.swapee.wc.FileTreeElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeElementPort
 */
xyz.swapee.wc.AbstractFileTreeElementPort = class extends /** @type {xyz.swapee.wc.AbstractFileTreeElementPort.constructor&xyz.swapee.wc.FileTreeElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeElementPort.prototype.constructor = xyz.swapee.wc.AbstractFileTreeElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeElementPort|typeof xyz.swapee.wc.FileTreeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeElementPort}
 */
xyz.swapee.wc.AbstractFileTreeElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElementPort}
 */
xyz.swapee.wc.AbstractFileTreeElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeElementPort|typeof xyz.swapee.wc.FileTreeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElementPort}
 */
xyz.swapee.wc.AbstractFileTreeElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeElementPort|typeof xyz.swapee.wc.FileTreeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeElementPort}
 */
xyz.swapee.wc.AbstractFileTreeElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeElementPort.Initialese[]) => xyz.swapee.wc.IFileTreeElementPort} xyz.swapee.wc.FileTreeElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileTreeElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IFileTreeElementPort.Inputs>)} xyz.swapee.wc.IFileTreeElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IFileTreeElementPort
 */
xyz.swapee.wc.IFileTreeElementPort = class extends /** @type {xyz.swapee.wc.IFileTreeElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeElementPort.Initialese>)} xyz.swapee.wc.FileTreeElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort} xyz.swapee.wc.IFileTreeElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileTreeElementPort_ instances.
 * @constructor xyz.swapee.wc.FileTreeElementPort
 * @implements {xyz.swapee.wc.IFileTreeElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeElementPort.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeElementPort = class extends /** @type {xyz.swapee.wc.FileTreeElementPort.constructor&xyz.swapee.wc.IFileTreeElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeElementPort}
 */
xyz.swapee.wc.FileTreeElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeElementPort.
 * @interface xyz.swapee.wc.IFileTreeElementPortFields
 */
xyz.swapee.wc.IFileTreeElementPortFields = class { }
/**
 * The inputs to the _IFileTreeElement_'s controller via its element port.
 */
xyz.swapee.wc.IFileTreeElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileTreeElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IFileTreeElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IFileTreeElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeElementPort} */
xyz.swapee.wc.RecordIFileTreeElementPort

/** @typedef {xyz.swapee.wc.IFileTreeElementPort} xyz.swapee.wc.BoundIFileTreeElementPort */

/** @typedef {xyz.swapee.wc.FileTreeElementPort} xyz.swapee.wc.BoundFileTreeElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _LoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts.loInOpts

/**
 * The options to pass to the _ContentWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts.contentWrOpts

/**
 * The options to pass to the _ExpandedIc_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts.expandedIcOpts

/**
 * The options to pass to the _CollapsedIc_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts.collapsedIcOpts

/**
 * The options to pass to the _FolderWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts.folderWrOpts

/**
 * The options to pass to the _FolderLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts.folderLaOpts

/**
 * The options to pass to the _Folders_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts.foldersOpts

/**
 * The options to pass to the _Files_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts.filesOpts

/**
 * The options to pass to the _FileEditor_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts.fileEditorOpts

/**
 * The options to pass to the _FolderTemplate_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts.folderTemplateOpts

/**
 * The options to pass to the _FileTemplate_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts.fileTemplateOpts

/** @typedef {function(new: xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder&xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts&xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts)} xyz.swapee.wc.IFileTreeElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder} xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts} xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts.typeof */
/**
 * The inputs to the _IFileTreeElement_'s controller via its element port.
 * @record xyz.swapee.wc.IFileTreeElementPort.Inputs
 */
xyz.swapee.wc.IFileTreeElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IFileTreeElementPort.Inputs.constructor&xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IFileTreeElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IFileTreeElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IFileTreeElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.LoInOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ContentWrOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ExpandedIcOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.CollapsedIcOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderWrOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderLaOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FoldersOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FilesOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileEditorOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderTemplateOpts&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileTemplateOpts)} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.LoInOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.LoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ContentWrOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ContentWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ExpandedIcOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ExpandedIcOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.CollapsedIcOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.CollapsedIcOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderWrOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderLaOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FoldersOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FoldersOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FilesOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FilesOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileEditorOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileEditorOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderTemplateOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderTemplateOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileTemplateOpts} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileTemplateOpts.typeof */
/**
 * The inputs to the _IFileTreeElement_'s controller via its element port.
 * @record xyz.swapee.wc.IFileTreeElementPort.WeakInputs
 */
xyz.swapee.wc.IFileTreeElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IFileTreeElementPort.WeakInputs.constructor&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.LoInOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ContentWrOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ExpandedIcOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.CollapsedIcOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderWrOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderLaOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FoldersOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FilesOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileEditorOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderTemplateOpts.typeof&xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileTemplateOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IFileTreeElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IFileTreeElementPort.WeakInputs

/**
 * Contains getters to cast the _IFileTreeElementPort_ interface.
 * @interface xyz.swapee.wc.IFileTreeElementPortCaster
 */
xyz.swapee.wc.IFileTreeElementPortCaster = class { }
/**
 * Cast the _IFileTreeElementPort_ instance into the _BoundIFileTreeElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeElementPort}
 */
xyz.swapee.wc.IFileTreeElementPortCaster.prototype.asIFileTreeElementPort
/**
 * Access the _FileTreeElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeElementPort}
 */
xyz.swapee.wc.IFileTreeElementPortCaster.prototype.superFileTreeElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts The options to pass to the _LoIn_ vdu (optional overlay).
 * @prop {!Object} [loInOpts] The options to pass to the _LoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.LoInOpts_Safe The options to pass to the _LoIn_ vdu (required overlay).
 * @prop {!Object} loInOpts The options to pass to the _LoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts The options to pass to the _ContentWr_ vdu (optional overlay).
 * @prop {!Object} [contentWrOpts] The options to pass to the _ContentWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.ContentWrOpts_Safe The options to pass to the _ContentWr_ vdu (required overlay).
 * @prop {!Object} contentWrOpts The options to pass to the _ContentWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts The options to pass to the _ExpandedIc_ vdu (optional overlay).
 * @prop {!Object} [expandedIcOpts] The options to pass to the _ExpandedIc_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.ExpandedIcOpts_Safe The options to pass to the _ExpandedIc_ vdu (required overlay).
 * @prop {!Object} expandedIcOpts The options to pass to the _ExpandedIc_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts The options to pass to the _CollapsedIc_ vdu (optional overlay).
 * @prop {!Object} [collapsedIcOpts] The options to pass to the _CollapsedIc_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.CollapsedIcOpts_Safe The options to pass to the _CollapsedIc_ vdu (required overlay).
 * @prop {!Object} collapsedIcOpts The options to pass to the _CollapsedIc_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts The options to pass to the _FolderWr_ vdu (optional overlay).
 * @prop {!Object} [folderWrOpts] The options to pass to the _FolderWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderWrOpts_Safe The options to pass to the _FolderWr_ vdu (required overlay).
 * @prop {!Object} folderWrOpts The options to pass to the _FolderWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts The options to pass to the _FolderLa_ vdu (optional overlay).
 * @prop {!Object} [folderLaOpts] The options to pass to the _FolderLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderLaOpts_Safe The options to pass to the _FolderLa_ vdu (required overlay).
 * @prop {!Object} folderLaOpts The options to pass to the _FolderLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts The options to pass to the _Folders_ vdu (optional overlay).
 * @prop {!Object} [foldersOpts] The options to pass to the _Folders_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FoldersOpts_Safe The options to pass to the _Folders_ vdu (required overlay).
 * @prop {!Object} foldersOpts The options to pass to the _Folders_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts The options to pass to the _Files_ vdu (optional overlay).
 * @prop {!Object} [filesOpts] The options to pass to the _Files_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FilesOpts_Safe The options to pass to the _Files_ vdu (required overlay).
 * @prop {!Object} filesOpts The options to pass to the _Files_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts The options to pass to the _FileEditor_ vdu (optional overlay).
 * @prop {!Object} [fileEditorOpts] The options to pass to the _FileEditor_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FileEditorOpts_Safe The options to pass to the _FileEditor_ vdu (required overlay).
 * @prop {!Object} fileEditorOpts The options to pass to the _FileEditor_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts The options to pass to the _FolderTemplate_ vdu (optional overlay).
 * @prop {!Object} [folderTemplateOpts] The options to pass to the _FolderTemplate_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FolderTemplateOpts_Safe The options to pass to the _FolderTemplate_ vdu (required overlay).
 * @prop {!Object} folderTemplateOpts The options to pass to the _FolderTemplate_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts The options to pass to the _FileTemplate_ vdu (optional overlay).
 * @prop {!Object} [fileTemplateOpts] The options to pass to the _FileTemplate_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.Inputs.FileTemplateOpts_Safe The options to pass to the _FileTemplate_ vdu (required overlay).
 * @prop {!Object} fileTemplateOpts The options to pass to the _FileTemplate_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.LoInOpts The options to pass to the _LoIn_ vdu (optional overlay).
 * @prop {*} [loInOpts=null] The options to pass to the _LoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.LoInOpts_Safe The options to pass to the _LoIn_ vdu (required overlay).
 * @prop {*} loInOpts The options to pass to the _LoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ContentWrOpts The options to pass to the _ContentWr_ vdu (optional overlay).
 * @prop {*} [contentWrOpts=null] The options to pass to the _ContentWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ContentWrOpts_Safe The options to pass to the _ContentWr_ vdu (required overlay).
 * @prop {*} contentWrOpts The options to pass to the _ContentWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ExpandedIcOpts The options to pass to the _ExpandedIc_ vdu (optional overlay).
 * @prop {*} [expandedIcOpts=null] The options to pass to the _ExpandedIc_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.ExpandedIcOpts_Safe The options to pass to the _ExpandedIc_ vdu (required overlay).
 * @prop {*} expandedIcOpts The options to pass to the _ExpandedIc_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.CollapsedIcOpts The options to pass to the _CollapsedIc_ vdu (optional overlay).
 * @prop {*} [collapsedIcOpts=null] The options to pass to the _CollapsedIc_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.CollapsedIcOpts_Safe The options to pass to the _CollapsedIc_ vdu (required overlay).
 * @prop {*} collapsedIcOpts The options to pass to the _CollapsedIc_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderWrOpts The options to pass to the _FolderWr_ vdu (optional overlay).
 * @prop {*} [folderWrOpts=null] The options to pass to the _FolderWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderWrOpts_Safe The options to pass to the _FolderWr_ vdu (required overlay).
 * @prop {*} folderWrOpts The options to pass to the _FolderWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderLaOpts The options to pass to the _FolderLa_ vdu (optional overlay).
 * @prop {*} [folderLaOpts=null] The options to pass to the _FolderLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderLaOpts_Safe The options to pass to the _FolderLa_ vdu (required overlay).
 * @prop {*} folderLaOpts The options to pass to the _FolderLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FoldersOpts The options to pass to the _Folders_ vdu (optional overlay).
 * @prop {*} [foldersOpts=null] The options to pass to the _Folders_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FoldersOpts_Safe The options to pass to the _Folders_ vdu (required overlay).
 * @prop {*} foldersOpts The options to pass to the _Folders_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FilesOpts The options to pass to the _Files_ vdu (optional overlay).
 * @prop {*} [filesOpts=null] The options to pass to the _Files_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FilesOpts_Safe The options to pass to the _Files_ vdu (required overlay).
 * @prop {*} filesOpts The options to pass to the _Files_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileEditorOpts The options to pass to the _FileEditor_ vdu (optional overlay).
 * @prop {*} [fileEditorOpts=null] The options to pass to the _FileEditor_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileEditorOpts_Safe The options to pass to the _FileEditor_ vdu (required overlay).
 * @prop {*} fileEditorOpts The options to pass to the _FileEditor_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderTemplateOpts The options to pass to the _FolderTemplate_ vdu (optional overlay).
 * @prop {*} [folderTemplateOpts=null] The options to pass to the _FolderTemplate_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FolderTemplateOpts_Safe The options to pass to the _FolderTemplate_ vdu (required overlay).
 * @prop {*} folderTemplateOpts The options to pass to the _FolderTemplate_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileTemplateOpts The options to pass to the _FileTemplate_ vdu (optional overlay).
 * @prop {*} [fileTemplateOpts=null] The options to pass to the _FileTemplate_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeElementPort.WeakInputs.FileTemplateOpts_Safe The options to pass to the _FileTemplate_ vdu (required overlay).
 * @prop {*} fileTemplateOpts The options to pass to the _FileTemplate_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/150-IFileTreeGenerator.xml}  13681db7826fdd95f782b24c11678347 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileTreeGenerator.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeGenerator)} xyz.swapee.wc.AbstractFileTreeGenerator.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeGenerator} xyz.swapee.wc.FileTreeGenerator.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeGenerator` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeGenerator
 */
xyz.swapee.wc.AbstractFileTreeGenerator = class extends /** @type {xyz.swapee.wc.AbstractFileTreeGenerator.constructor&xyz.swapee.wc.FileTreeGenerator.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeGenerator.prototype.constructor = xyz.swapee.wc.AbstractFileTreeGenerator
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeGenerator.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeGenerator} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeGenerator|typeof xyz.swapee.wc.FileTreeGenerator)|!xyz.swapee.wc.IFileTreeGeneratorHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGenerator}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeGenerator.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeGenerator}
 */
xyz.swapee.wc.AbstractFileTreeGenerator.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGenerator}
 */
xyz.swapee.wc.AbstractFileTreeGenerator.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeGenerator|typeof xyz.swapee.wc.FileTreeGenerator)|!xyz.swapee.wc.IFileTreeGeneratorHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGenerator}
 */
xyz.swapee.wc.AbstractFileTreeGenerator.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeGenerator|typeof xyz.swapee.wc.FileTreeGenerator)|!xyz.swapee.wc.IFileTreeGeneratorHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGenerator}
 */
xyz.swapee.wc.AbstractFileTreeGenerator.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeGenerator.Initialese[]) => xyz.swapee.wc.IFileTreeGenerator} xyz.swapee.wc.FileTreeGeneratorConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IFileTreeGeneratorHyperslice
 */
xyz.swapee.wc.IFileTreeGeneratorHyperslice = class {
  constructor() {
    /**
     * Assigns properties to the stator from the brush or brushes.
     */
    this.brushFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator._brushFolder|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._brushFolder>} */ (void 0)
    /**
     * Allows to execute paint methods reactively when the state changes.
     */
    this.paintFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator._paintFolder|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._paintFolder>} */ (void 0)
    /**
     * Is executed when the stator is created for the element and allows to assign
     * interrupt listeners bound to the stator.
     */
    this.assignFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator._assignFolder|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._assignFolder>} */ (void 0)
    /**
     * Creates a component via a capacitor and establishes circuits between its port
     * and the stator.
     */
    this.initFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator._initFolder|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._initFolder>} */ (void 0)
    /**
     * Assigns properties to the stator from the brush or brushes.
     */
    this.brushFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator._brushFile|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._brushFile>} */ (void 0)
    /**
     * Allows to execute paint methods reactively when the state changes.
     */
    this.paintFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator._paintFile|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._paintFile>} */ (void 0)
    /**
     * Is executed when the stator is created for the element and allows to assign
     * interrupt listeners bound to the stator.
     */
    this.assignFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator._assignFile|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._assignFile>} */ (void 0)
    /**
     * Creates a component via a capacitor and establishes circuits between its port
     * and the stator.
     */
    this.initFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator._initFile|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator._initFile>} */ (void 0)
  }
}
xyz.swapee.wc.IFileTreeGeneratorHyperslice.prototype.constructor = xyz.swapee.wc.IFileTreeGeneratorHyperslice

/**
 * A concrete class of _IFileTreeGeneratorHyperslice_ instances.
 * @constructor xyz.swapee.wc.FileTreeGeneratorHyperslice
 * @implements {xyz.swapee.wc.IFileTreeGeneratorHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.FileTreeGeneratorHyperslice = class extends xyz.swapee.wc.IFileTreeGeneratorHyperslice { }
xyz.swapee.wc.FileTreeGeneratorHyperslice.prototype.constructor = xyz.swapee.wc.FileTreeGeneratorHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IFileTreeGeneratorBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IFileTreeGeneratorBindingHyperslice = class {
  constructor() {
    /**
     * Assigns properties to the stator from the brush or brushes.
     */
    this.brushFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__brushFolder<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__brushFolder<THIS>>} */ (void 0)
    /**
     * Allows to execute paint methods reactively when the state changes.
     */
    this.paintFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__paintFolder<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__paintFolder<THIS>>} */ (void 0)
    /**
     * Is executed when the stator is created for the element and allows to assign
     * interrupt listeners bound to the stator.
     */
    this.assignFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__assignFolder<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__assignFolder<THIS>>} */ (void 0)
    /**
     * Creates a component via a capacitor and establishes circuits between its port
     * and the stator.
     */
    this.initFolder=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__initFolder<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__initFolder<THIS>>} */ (void 0)
    /**
     * Assigns properties to the stator from the brush or brushes.
     */
    this.brushFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__brushFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__brushFile<THIS>>} */ (void 0)
    /**
     * Allows to execute paint methods reactively when the state changes.
     */
    this.paintFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__paintFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__paintFile<THIS>>} */ (void 0)
    /**
     * Is executed when the stator is created for the element and allows to assign
     * interrupt listeners bound to the stator.
     */
    this.assignFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__assignFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__assignFile<THIS>>} */ (void 0)
    /**
     * Creates a component via a capacitor and establishes circuits between its port
     * and the stator.
     */
    this.initFile=/** @type {!xyz.swapee.wc.IFileTreeGenerator.__initFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeGenerator.__initFile<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IFileTreeGeneratorBindingHyperslice.prototype.constructor = xyz.swapee.wc.IFileTreeGeneratorBindingHyperslice

/**
 * A concrete class of _IFileTreeGeneratorBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.FileTreeGeneratorBindingHyperslice
 * @implements {xyz.swapee.wc.IFileTreeGeneratorBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IFileTreeGeneratorBindingHyperslice<THIS>}
 */
xyz.swapee.wc.FileTreeGeneratorBindingHyperslice = class extends xyz.swapee.wc.IFileTreeGeneratorBindingHyperslice { }
xyz.swapee.wc.FileTreeGeneratorBindingHyperslice.prototype.constructor = xyz.swapee.wc.FileTreeGeneratorBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.IFileTreeGeneratorFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeGeneratorCaster)} xyz.swapee.wc.IFileTreeGenerator.constructor */
/**
 * Responsible for generation of component instances to place inside containers.
 * @interface xyz.swapee.wc.IFileTreeGenerator
 */
xyz.swapee.wc.IFileTreeGenerator = class extends /** @type {xyz.swapee.wc.IFileTreeGenerator.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeGenerator* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeGenerator.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeGenerator.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.brushFolder} */
xyz.swapee.wc.IFileTreeGenerator.prototype.brushFolder = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.paintFolder} */
xyz.swapee.wc.IFileTreeGenerator.prototype.paintFolder = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.assignFolder} */
xyz.swapee.wc.IFileTreeGenerator.prototype.assignFolder = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.stateFolder} */
xyz.swapee.wc.IFileTreeGenerator.prototype.stateFolder = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.initFolder} */
xyz.swapee.wc.IFileTreeGenerator.prototype.initFolder = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.FolderDynamo} */
xyz.swapee.wc.IFileTreeGenerator.prototype.FolderDynamo = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.brushFile} */
xyz.swapee.wc.IFileTreeGenerator.prototype.brushFile = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.paintFile} */
xyz.swapee.wc.IFileTreeGenerator.prototype.paintFile = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.assignFile} */
xyz.swapee.wc.IFileTreeGenerator.prototype.assignFile = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.stateFile} */
xyz.swapee.wc.IFileTreeGenerator.prototype.stateFile = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.initFile} */
xyz.swapee.wc.IFileTreeGenerator.prototype.initFile = function() {}
/** @type {xyz.swapee.wc.IFileTreeGenerator.FileDynamo} */
xyz.swapee.wc.IFileTreeGenerator.prototype.FileDynamo = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeGenerator&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeGenerator.Initialese>)} xyz.swapee.wc.FileTreeGenerator.constructor */
/**
 * A concrete class of _IFileTreeGenerator_ instances.
 * @constructor xyz.swapee.wc.FileTreeGenerator
 * @implements {xyz.swapee.wc.IFileTreeGenerator} Responsible for generation of component instances to place inside containers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeGenerator.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeGenerator = class extends /** @type {xyz.swapee.wc.FileTreeGenerator.constructor&xyz.swapee.wc.IFileTreeGenerator.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeGenerator* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeGenerator.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeGenerator* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeGenerator.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeGenerator.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeGenerator}
 */
xyz.swapee.wc.FileTreeGenerator.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeGenerator.
 * @interface xyz.swapee.wc.IFileTreeGeneratorFields
 */
xyz.swapee.wc.IFileTreeGeneratorFields = class { }
/**
 * The land accessible to the generee.
 */
xyz.swapee.wc.IFileTreeGeneratorFields.prototype.land = /** @type {!xyz.swapee.wc.FileTreeLand} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeGenerator} */
xyz.swapee.wc.RecordIFileTreeGenerator

/** @typedef {xyz.swapee.wc.IFileTreeGenerator} xyz.swapee.wc.BoundIFileTreeGenerator */

/** @typedef {xyz.swapee.wc.FileTreeGenerator} xyz.swapee.wc.BoundFileTreeGenerator */

/**
 * Contains getters to cast the _IFileTreeGenerator_ interface.
 * @interface xyz.swapee.wc.IFileTreeGeneratorCaster
 */
xyz.swapee.wc.IFileTreeGeneratorCaster = class { }
/**
 * Cast the _IFileTreeGenerator_ instance into the _BoundIFileTreeGenerator_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeGenerator}
 */
xyz.swapee.wc.IFileTreeGeneratorCaster.prototype.asIFileTreeGenerator
/**
 * Cast the _IFileTreeGenerator_ instance into the _BoundIFileTreeHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeHtmlComponent}
 */
xyz.swapee.wc.IFileTreeGeneratorCaster.prototype.asIFileTreeHtmlComponent
/**
 * Access the _FileTreeGenerator_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeGenerator}
 */
xyz.swapee.wc.IFileTreeGeneratorCaster.prototype.superFileTreeGenerator

/**
 * @typedef {(this: THIS, brushes: !xyz.swapee.wc.IFileTree.FolderBrushes, stator: !com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FolderStator>) => void} xyz.swapee.wc.IFileTreeGenerator.__brushFolder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__brushFolder<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._brushFolder */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.brushFolder} */
/**
 * Assigns properties to the stator from the brush or brushes. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileTree.FolderBrushes} brushes The brushes.
 * @param {!com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FolderStator>} stator The stator.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.brushFolder = function(brushes, stator) {}

/**
 * @typedef {(this: THIS, state: !xyz.swapee.wc.IFileTree.FolderStator, element: !com.webcircuits.IHtmlTwin) => void} xyz.swapee.wc.IFileTreeGenerator.__paintFolder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__paintFolder<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._paintFolder */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.paintFolder} */
/**
 * Allows to execute paint methods reactively when the state changes. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileTree.FolderStator} state The state of the stator.
 * @param {!com.webcircuits.IHtmlTwin} element The element.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.paintFolder = function(state, element) {}

/**
 * @typedef {(this: THIS, state: !xyz.swapee.wc.IFileTree.FolderStator, element: !com.webcircuits.IHtmlTwin, land: !xyz.swapee.wc.FileTreeLand) => void} xyz.swapee.wc.IFileTreeGenerator.__assignFolder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__assignFolder<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._assignFolder */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.assignFolder} */
/**
 * Is executed when the stator is created for the element and allows to assign
 * interrupt listeners bound to the stator. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileTree.FolderStator} state The state of the stator.
 * @param {!com.webcircuits.IHtmlTwin} element The element.
 * @param {!xyz.swapee.wc.FileTreeLand} land The land.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.assignFolder = function(state, element, land) {}

/**
 * @typedef {(this: THIS, el: !com.webcircuits.IHtmlTwin) => !com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FolderStator>} xyz.swapee.wc.IFileTreeGenerator.__stateFolder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__stateFolder<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._stateFolder */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.stateFolder} */
/**
 * Builds a stator for an element.
 * @param {!com.webcircuits.IHtmlTwin} el The element against which to build the stator.
 * @return {!com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FolderStator>}
 */
xyz.swapee.wc.IFileTreeGenerator.stateFolder = function(el) {}

/**
 * @typedef {(this: THIS, stator: !com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FolderStator>, el: !com.webcircuits.IHtmlTwin) => void} xyz.swapee.wc.IFileTreeGenerator.__initFolder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__initFolder<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._initFolder */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.initFolder} */
/**
 * Creates a component via a capacitor and establishes circuits between its port
 * and the stator. `🔗 $combine`
 * @param {!com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FolderStator>} stator The stator.
 * @param {!com.webcircuits.IHtmlTwin} el The element that will underlie the component.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.initFolder = function(stator, el) {}

/**
 * @typedef {(this: THIS, stator: !xyz.swapee.wc.IFileTree.FolderStator, land: !xyz.swapee.wc.FileTreeLand&{FileTree:!xyz.swapee.wc.IFileTreeController}) => !Object} xyz.swapee.wc.IFileTreeGenerator.__FolderDynamo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__FolderDynamo<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._FolderDynamo */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.FolderDynamo} */
/**
 * The method that links the stator to the component.
 * @param {!xyz.swapee.wc.IFileTree.FolderStator} stator The stator.
 * @param {!xyz.swapee.wc.FileTreeLand&{FileTree:!xyz.swapee.wc.IFileTreeController}} land The land.
 * @return {!Object}
 */
xyz.swapee.wc.IFileTreeGenerator.FolderDynamo = function(stator, land) {}

/**
 * @typedef {(this: THIS, brushes: !xyz.swapee.wc.IFileTree.FileBrushes, stator: !com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FileStator>) => void} xyz.swapee.wc.IFileTreeGenerator.__brushFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__brushFile<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._brushFile */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.brushFile} */
/**
 * Assigns properties to the stator from the brush or brushes. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileTree.FileBrushes} brushes The brushes.
 * @param {!com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FileStator>} stator The stator.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.brushFile = function(brushes, stator) {}

/**
 * @typedef {(this: THIS, state: !xyz.swapee.wc.IFileTree.FileStator, element: !com.webcircuits.IHtmlTwin) => void} xyz.swapee.wc.IFileTreeGenerator.__paintFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__paintFile<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._paintFile */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.paintFile} */
/**
 * Allows to execute paint methods reactively when the state changes. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileTree.FileStator} state The state of the stator.
 * @param {!com.webcircuits.IHtmlTwin} element The element.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.paintFile = function(state, element) {}

/**
 * @typedef {(this: THIS, state: !xyz.swapee.wc.IFileTree.FileStator, element: !com.webcircuits.IHtmlTwin, land: !xyz.swapee.wc.FileTreeLand) => void} xyz.swapee.wc.IFileTreeGenerator.__assignFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__assignFile<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._assignFile */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.assignFile} */
/**
 * Is executed when the stator is created for the element and allows to assign
 * interrupt listeners bound to the stator. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileTree.FileStator} state The state of the stator.
 * @param {!com.webcircuits.IHtmlTwin} element The element.
 * @param {!xyz.swapee.wc.FileTreeLand} land The land.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.assignFile = function(state, element, land) {}

/**
 * @typedef {(this: THIS, el: !com.webcircuits.IHtmlTwin) => !com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FileStator>} xyz.swapee.wc.IFileTreeGenerator.__stateFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__stateFile<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._stateFile */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.stateFile} */
/**
 * Builds a stator for an element.
 * @param {!com.webcircuits.IHtmlTwin} el The element against which to build the stator.
 * @return {!com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FileStator>}
 */
xyz.swapee.wc.IFileTreeGenerator.stateFile = function(el) {}

/**
 * @typedef {(this: THIS, stator: !com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FileStator>, el: !com.webcircuits.IHtmlTwin) => void} xyz.swapee.wc.IFileTreeGenerator.__initFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__initFile<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._initFile */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.initFile} */
/**
 * Creates a component via a capacitor and establishes circuits between its port
 * and the stator. `🔗 $combine`
 * @param {!com.webcircuits.IStator<!xyz.swapee.wc.IFileTree.FileStator>} stator The stator.
 * @param {!com.webcircuits.IHtmlTwin} el The element that will underlie the component.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeGenerator.initFile = function(stator, el) {}

/**
 * @typedef {(this: THIS, stator: !xyz.swapee.wc.IFileTree.FileStator, land: !xyz.swapee.wc.FileTreeLand&{FileTree:!xyz.swapee.wc.IFileTreeController}) => !Object} xyz.swapee.wc.IFileTreeGenerator.__FileDynamo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGenerator.__FileDynamo<!xyz.swapee.wc.IFileTreeGenerator>} xyz.swapee.wc.IFileTreeGenerator._FileDynamo */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGenerator.FileDynamo} */
/**
 * The method that links the stator to the component.
 * @param {!xyz.swapee.wc.IFileTree.FileStator} stator The stator.
 * @param {!xyz.swapee.wc.FileTreeLand&{FileTree:!xyz.swapee.wc.IFileTreeController}} land The land.
 * @return {!Object}
 */
xyz.swapee.wc.IFileTreeGenerator.FileDynamo = function(stator, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeGenerator
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/160-IFileTreeRadio.xml}  b62fb71a4da76fb1a25f9c13755214b7 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileTreeRadio.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeRadio)} xyz.swapee.wc.AbstractFileTreeRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeRadio} xyz.swapee.wc.FileTreeRadio.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeRadio` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeRadio
 */
xyz.swapee.wc.AbstractFileTreeRadio = class extends /** @type {xyz.swapee.wc.AbstractFileTreeRadio.constructor&xyz.swapee.wc.FileTreeRadio.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeRadio.prototype.constructor = xyz.swapee.wc.AbstractFileTreeRadio
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeRadio.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeRadio} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileTreeRadio|typeof xyz.swapee.wc.FileTreeRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeRadio.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeRadio}
 */
xyz.swapee.wc.AbstractFileTreeRadio.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeRadio}
 */
xyz.swapee.wc.AbstractFileTreeRadio.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileTreeRadio|typeof xyz.swapee.wc.FileTreeRadio} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeRadio}
 */
xyz.swapee.wc.AbstractFileTreeRadio.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileTreeRadio|typeof xyz.swapee.wc.FileTreeRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeRadio}
 */
xyz.swapee.wc.AbstractFileTreeRadio.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileTreeRadioCaster)} xyz.swapee.wc.IFileTreeRadio.constructor */
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @interface xyz.swapee.wc.IFileTreeRadio
 */
xyz.swapee.wc.IFileTreeRadio = class extends /** @type {xyz.swapee.wc.IFileTreeRadio.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles} */
xyz.swapee.wc.IFileTreeRadio.prototype.adaptLoadFiles = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeRadio&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeRadio.Initialese>)} xyz.swapee.wc.FileTreeRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeRadio} xyz.swapee.wc.IFileTreeRadio.typeof */
/**
 * A concrete class of _IFileTreeRadio_ instances.
 * @constructor xyz.swapee.wc.FileTreeRadio
 * @implements {xyz.swapee.wc.IFileTreeRadio} Transmits data _to-_ and _fro-_ off-chip system.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeRadio.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeRadio = class extends /** @type {xyz.swapee.wc.FileTreeRadio.constructor&xyz.swapee.wc.IFileTreeRadio.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileTreeRadio.prototype.constructor = xyz.swapee.wc.FileTreeRadio
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeRadio}
 */
xyz.swapee.wc.FileTreeRadio.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileTreeRadio} */
xyz.swapee.wc.RecordIFileTreeRadio

/** @typedef {xyz.swapee.wc.IFileTreeRadio} xyz.swapee.wc.BoundIFileTreeRadio */

/** @typedef {xyz.swapee.wc.FileTreeRadio} xyz.swapee.wc.BoundFileTreeRadio */

/**
 * Contains getters to cast the _IFileTreeRadio_ interface.
 * @interface xyz.swapee.wc.IFileTreeRadioCaster
 */
xyz.swapee.wc.IFileTreeRadioCaster = class { }
/**
 * Cast the _IFileTreeRadio_ instance into the _BoundIFileTreeRadio_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeRadio}
 */
xyz.swapee.wc.IFileTreeRadioCaster.prototype.asIFileTreeRadio
/**
 * Cast the _IFileTreeRadio_ instance into the _BoundIFileTreeComputer_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeComputer}
 */
xyz.swapee.wc.IFileTreeRadioCaster.prototype.asIFileTreeComputer
/**
 * Access the _FileTreeRadio_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeRadio}
 */
xyz.swapee.wc.IFileTreeRadioCaster.prototype.superFileTreeRadio

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles.Form, changes: IFileTreeComputer.adaptLoadFiles.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles.Return|void)>)} xyz.swapee.wc.IFileTreeRadio.__adaptLoadFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeRadio.__adaptLoadFiles<!xyz.swapee.wc.IFileTreeRadio>} xyz.swapee.wc.IFileTreeRadio._adaptLoadFiles */
/** @typedef {typeof xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles} */
/**
 * Returns files and folders in the directory.
 * @param {!xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles.Form} form The form with inputs.
 * - `root` _string_ The root folder on the disk to read from. ⤴ *IFileTreeOuterCore.Model.Root_Safe*
 * - `folder` _string_ The name of the folder inside the root presented to the user. ⤴ *IFileTreeOuterCore.Model.Folder_Safe*
 * @param {IFileTreeComputer.adaptLoadFiles.Form} changes The previous values of the form.
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.Root_Safe&xyz.swapee.wc.IFileTreeCore.Model.Folder_Safe} xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.Files&xyz.swapee.wc.IFileTreeCore.Model.Folders} xyz.swapee.wc.IFileTreeRadio.adaptLoadFiles.Return The form with outputs. */

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeRadio
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/170-IFileTreeDesigner.xml}  9da9f21bd9593efe640d37b1b2f9d96a */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IFileTreeDesigner
 */
xyz.swapee.wc.IFileTreeDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.FileTreeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IFileTree />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.FileTreeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IFileTree />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.FileTreeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IFileTreeDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `FileEditor` _typeof xyz.swapee.wc.IFileEditorController_
   * - `FileTree` _typeof IFileTreeController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IFileTreeDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `FileEditor` _typeof xyz.swapee.wc.IFileEditorController_
   * - `FileTree` _typeof IFileTreeController_
   * - `This` _typeof IFileTreeController_
   * @param {!xyz.swapee.wc.IFileTreeDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `FileEditor` _!xyz.swapee.wc.FileEditorMemory_
   * - `FileTree` _!FileTreeMemory_
   * - `This` _!FileTreeMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.FileTreeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.FileTreeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IFileTreeDesigner.prototype.constructor = xyz.swapee.wc.IFileTreeDesigner

/**
 * A concrete class of _IFileTreeDesigner_ instances.
 * @constructor xyz.swapee.wc.FileTreeDesigner
 * @implements {xyz.swapee.wc.IFileTreeDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.FileTreeDesigner = class extends xyz.swapee.wc.IFileTreeDesigner { }
xyz.swapee.wc.FileTreeDesigner.prototype.constructor = xyz.swapee.wc.FileTreeDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IFileEditorController} FileEditor
 * @prop {typeof xyz.swapee.wc.IFileTreeController} FileTree
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IFileEditorController} FileEditor
 * @prop {typeof xyz.swapee.wc.IFileTreeController} FileTree
 * @prop {typeof xyz.swapee.wc.IFileTreeController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.FileEditorMemory} FileEditor
 * @prop {!xyz.swapee.wc.FileTreeMemory} FileTree
 * @prop {!xyz.swapee.wc.FileTreeMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/190-IFileTreeService.xml}  0420b6ffa55fb4af6d201dbb40725ca4 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileTreeService.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeService)} xyz.swapee.wc.AbstractFileTreeService.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeService} xyz.swapee.wc.FileTreeService.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeService` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeService
 */
xyz.swapee.wc.AbstractFileTreeService = class extends /** @type {xyz.swapee.wc.AbstractFileTreeService.constructor&xyz.swapee.wc.FileTreeService.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeService.prototype.constructor = xyz.swapee.wc.AbstractFileTreeService
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeService.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeService} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileTreeService|typeof xyz.swapee.wc.FileTreeService} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeService.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeService}
 */
xyz.swapee.wc.AbstractFileTreeService.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeService}
 */
xyz.swapee.wc.AbstractFileTreeService.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileTreeService|typeof xyz.swapee.wc.FileTreeService} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeService}
 */
xyz.swapee.wc.AbstractFileTreeService.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileTreeService|typeof xyz.swapee.wc.FileTreeService} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeService}
 */
xyz.swapee.wc.AbstractFileTreeService.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileTreeServiceCaster)} xyz.swapee.wc.IFileTreeService.constructor */
/**
 * A service for the IFileTree.
 * @interface xyz.swapee.wc.IFileTreeService
 */
xyz.swapee.wc.IFileTreeService = class extends /** @type {xyz.swapee.wc.IFileTreeService.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IFileTreeService.filterFiles} */
xyz.swapee.wc.IFileTreeService.prototype.filterFiles = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeService&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeService.Initialese>)} xyz.swapee.wc.FileTreeService.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeService} xyz.swapee.wc.IFileTreeService.typeof */
/**
 * A concrete class of _IFileTreeService_ instances.
 * @constructor xyz.swapee.wc.FileTreeService
 * @implements {xyz.swapee.wc.IFileTreeService} A service for the IFileTree.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeService.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeService = class extends /** @type {xyz.swapee.wc.FileTreeService.constructor&xyz.swapee.wc.IFileTreeService.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileTreeService.prototype.constructor = xyz.swapee.wc.FileTreeService
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeService}
 */
xyz.swapee.wc.FileTreeService.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileTreeService} */
xyz.swapee.wc.RecordIFileTreeService

/** @typedef {xyz.swapee.wc.IFileTreeService} xyz.swapee.wc.BoundIFileTreeService */

/** @typedef {xyz.swapee.wc.FileTreeService} xyz.swapee.wc.BoundFileTreeService */

/**
 * Contains getters to cast the _IFileTreeService_ interface.
 * @interface xyz.swapee.wc.IFileTreeServiceCaster
 */
xyz.swapee.wc.IFileTreeServiceCaster = class { }
/**
 * Cast the _IFileTreeService_ instance into the _BoundIFileTreeService_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeService}
 */
xyz.swapee.wc.IFileTreeServiceCaster.prototype.asIFileTreeService
/**
 * Access the _FileTreeService_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeService}
 */
xyz.swapee.wc.IFileTreeServiceCaster.prototype.superFileTreeService

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileTreeService.filterFiles.Form) => !Promise<xyz.swapee.wc.IFileTreeService.filterFiles.Return>} xyz.swapee.wc.IFileTreeService.__filterFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeService.__filterFiles<!xyz.swapee.wc.IFileTreeService>} xyz.swapee.wc.IFileTreeService._filterFiles */
/** @typedef {typeof xyz.swapee.wc.IFileTreeService.filterFiles} */
/**
 * Returns files and folders in the directory.
 * @param {!xyz.swapee.wc.IFileTreeService.filterFiles.Form} form The form.
 * - `root` _string_ The root folder on the disk to read from. ⤴ *IFileTreeOuterCore.Model.Root_Safe*
 * - `folder` _string_ The name of the folder inside the root presented to the user. ⤴ *IFileTreeOuterCore.Model.Folder_Safe*
 * @return {!Promise<xyz.swapee.wc.IFileTreeService.filterFiles.Return>}
 */
xyz.swapee.wc.IFileTreeService.filterFiles = function(form) {}

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.Root_Safe&xyz.swapee.wc.IFileTreeCore.Model.Folder_Safe} xyz.swapee.wc.IFileTreeService.filterFiles.Form The form. */

/** @typedef {xyz.swapee.wc.IFileTreeCore.Model.Files&xyz.swapee.wc.IFileTreeCore.Model.Folders} xyz.swapee.wc.IFileTreeService.filterFiles.Return */

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeService
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/200-FileTreeLand.xml}  e385864725ca98e83b0db06a3109445d */
/**
 * The surrounding of the _IFileTree_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.FileTreeLand
 */
xyz.swapee.wc.FileTreeLand = class { }
/**
 *
 */
xyz.swapee.wc.FileTreeLand.prototype.FileEditor = /** @type {xyz.swapee.wc.IFileEditor} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/40-IFileTreeDisplay.xml}  b24905e524a191d33400fff021cb783d */
/**
 * @typedef {Object} $xyz.swapee.wc.IFileTreeDisplay.Initialese
 * @prop {HTMLSpanElement} [LoIn]
 * @prop {HTMLDivElement} [ContentWr]
 * @prop {HTMLDivElement} [ExpandedIc]
 * @prop {HTMLDivElement} [CollapsedIc]
 * @prop {HTMLDivElement} [FolderWr]
 * @prop {HTMLSpanElement} [FolderLa]
 * @prop {HTMLDivElement} [Folders]
 * @prop {HTMLDivElement} [Files]
 * @prop {HTMLElement} [FileEditor] The via for the _FileEditor_ peer.
 * @prop {HTMLDivElement} [FolderTemplate] The template VDU for _Folder_.
 * @prop {HTMLDivElement} [FileTemplate] The template VDU for _File_.
 */
/** @typedef {$xyz.swapee.wc.IFileTreeDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IFileTreeDisplay.Settings>} xyz.swapee.wc.IFileTreeDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileTreeDisplay)} xyz.swapee.wc.AbstractFileTreeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeDisplay} xyz.swapee.wc.FileTreeDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeDisplay
 */
xyz.swapee.wc.AbstractFileTreeDisplay = class extends /** @type {xyz.swapee.wc.AbstractFileTreeDisplay.constructor&xyz.swapee.wc.FileTreeDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeDisplay.prototype.constructor = xyz.swapee.wc.AbstractFileTreeDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeDisplay|typeof xyz.swapee.wc.FileTreeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeDisplay}
 */
xyz.swapee.wc.AbstractFileTreeDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeDisplay}
 */
xyz.swapee.wc.AbstractFileTreeDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeDisplay|typeof xyz.swapee.wc.FileTreeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeDisplay}
 */
xyz.swapee.wc.AbstractFileTreeDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeDisplay|typeof xyz.swapee.wc.FileTreeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeDisplay}
 */
xyz.swapee.wc.AbstractFileTreeDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeDisplay.Initialese[]) => xyz.swapee.wc.IFileTreeDisplay} xyz.swapee.wc.FileTreeDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileTreeDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.FileTreeMemory, !HTMLDivElement, !xyz.swapee.wc.IFileTreeDisplay.Settings, xyz.swapee.wc.IFileTreeDisplay.Queries, null>)} xyz.swapee.wc.IFileTreeDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IFileTree_.
 * @interface xyz.swapee.wc.IFileTreeDisplay
 */
xyz.swapee.wc.IFileTreeDisplay = class extends /** @type {xyz.swapee.wc.IFileTreeDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreeDisplay.paint} */
xyz.swapee.wc.IFileTreeDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeDisplay.Initialese>)} xyz.swapee.wc.FileTreeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeDisplay} xyz.swapee.wc.IFileTreeDisplay.typeof */
/**
 * A concrete class of _IFileTreeDisplay_ instances.
 * @constructor xyz.swapee.wc.FileTreeDisplay
 * @implements {xyz.swapee.wc.IFileTreeDisplay} Display for presenting information from the _IFileTree_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeDisplay.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeDisplay = class extends /** @type {xyz.swapee.wc.FileTreeDisplay.constructor&xyz.swapee.wc.IFileTreeDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeDisplay}
 */
xyz.swapee.wc.FileTreeDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeDisplay.
 * @interface xyz.swapee.wc.IFileTreeDisplayFields
 */
xyz.swapee.wc.IFileTreeDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IFileTreeDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IFileTreeDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.LoIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.ContentWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.ExpandedIc = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.CollapsedIc = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.FolderWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.FolderLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.Folders = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.Files = /** @type {HTMLDivElement} */ (void 0)
/**
 * The via for the _FileEditor_ peer. Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.FileEditor = /** @type {HTMLElement} */ (void 0)
/**
 * The template VDU for _Folder_. Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.FolderTemplate = /** @type {HTMLDivElement} */ (void 0)
/**
 * The template VDU for _File_. Default `null`.
 */
xyz.swapee.wc.IFileTreeDisplayFields.prototype.FileTemplate = /** @type {HTMLDivElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeDisplay} */
xyz.swapee.wc.RecordIFileTreeDisplay

/** @typedef {xyz.swapee.wc.IFileTreeDisplay} xyz.swapee.wc.BoundIFileTreeDisplay */

/** @typedef {xyz.swapee.wc.FileTreeDisplay} xyz.swapee.wc.BoundFileTreeDisplay */

/** @typedef {xyz.swapee.wc.IFileTreeDisplay.Queries} xyz.swapee.wc.IFileTreeDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IFileTreeDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [fileEditorSel=""] The query to discover the _FileEditor_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IFileTreeDisplay_ interface.
 * @interface xyz.swapee.wc.IFileTreeDisplayCaster
 */
xyz.swapee.wc.IFileTreeDisplayCaster = class { }
/**
 * Cast the _IFileTreeDisplay_ instance into the _BoundIFileTreeDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeDisplay}
 */
xyz.swapee.wc.IFileTreeDisplayCaster.prototype.asIFileTreeDisplay
/**
 * Cast the _IFileTreeDisplay_ instance into the _BoundIFileTreeScreen_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeScreen}
 */
xyz.swapee.wc.IFileTreeDisplayCaster.prototype.asIFileTreeScreen
/**
 * Access the _FileTreeDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeDisplay}
 */
xyz.swapee.wc.IFileTreeDisplayCaster.prototype.superFileTreeDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.FileTreeMemory, land: null) => void} xyz.swapee.wc.IFileTreeDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeDisplay.__paint<!xyz.swapee.wc.IFileTreeDisplay>} xyz.swapee.wc.IFileTreeDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IFileTreeDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.FileTreeMemory} memory The display data.
 * - `host` _string_ The host property. Default empty string.
 * - `heading` _boolean_ Whether the folder acts as heading. Default `false`.
 * - `root` _string_ The root folder on the disk to read from. Default empty string.
 * - `folder` _string_ The name of the folder inside the root presented to the user. Default `.`.
 * - `showHidden` _boolean_ Whether to report hidden files (those that start with `.`). Default `false`.
 * - `expanded` _boolean_ Whether folder is currently expanded. Default `false`.
 * - `foldersRotor` _Array&lt;!IFileTree.FolderBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 * - `filesRotor` _Array&lt;!IFileTree.FileBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 * - `folders` _!Array&lt;string&gt;_ The discovered folders. Default `[]`.
 * - `files` _!Array&lt;string&gt;_ The discovered files. Default `[]`.
 * - `loadingFiles` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreFiles` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadFilesError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/40-IFileTreeDisplayBack.xml}  d64357dce7b14b3161dac2759f9ea9b6 */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IFileTreeDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [LoIn]
 * @prop {!com.webcircuits.IHtmlTwin} [ContentWr]
 * @prop {!com.webcircuits.IHtmlTwin} [ExpandedIc]
 * @prop {!com.webcircuits.IHtmlTwin} [CollapsedIc]
 * @prop {!com.webcircuits.IHtmlTwin} [FolderWr]
 * @prop {!com.webcircuits.IHtmlTwin} [FolderLa]
 * @prop {!com.webcircuits.IHtmlTwin} [Folders]
 * @prop {!com.webcircuits.IHtmlTwin} [Files]
 * @prop {!com.webcircuits.IHtmlTwin} [FileEditor] The via for the _FileEditor_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [FolderTemplate] The template VDU for _Folder_.
 * @prop {!com.webcircuits.IHtmlTwin} [FileTemplate] The template VDU for _File_.
 */
/** @typedef {$xyz.swapee.wc.back.IFileTreeDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.FileTreeClasses>} xyz.swapee.wc.back.IFileTreeDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.FileTreeDisplay)} xyz.swapee.wc.back.AbstractFileTreeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileTreeDisplay} xyz.swapee.wc.back.FileTreeDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileTreeDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileTreeDisplay
 */
xyz.swapee.wc.back.AbstractFileTreeDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractFileTreeDisplay.constructor&xyz.swapee.wc.back.FileTreeDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileTreeDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractFileTreeDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileTreeDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileTreeDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileTreeDisplay|typeof xyz.swapee.wc.back.FileTreeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileTreeDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileTreeDisplay}
 */
xyz.swapee.wc.back.AbstractFileTreeDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeDisplay}
 */
xyz.swapee.wc.back.AbstractFileTreeDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileTreeDisplay|typeof xyz.swapee.wc.back.FileTreeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeDisplay}
 */
xyz.swapee.wc.back.AbstractFileTreeDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileTreeDisplay|typeof xyz.swapee.wc.back.FileTreeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeDisplay}
 */
xyz.swapee.wc.back.AbstractFileTreeDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileTreeDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IFileTreeDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.FileTreeClasses, !xyz.swapee.wc.FileTreeLand>)} xyz.swapee.wc.back.IFileTreeDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IFileTreeDisplay
 */
xyz.swapee.wc.back.IFileTreeDisplay = class extends /** @type {xyz.swapee.wc.back.IFileTreeDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IFileTreeDisplay.paint} */
xyz.swapee.wc.back.IFileTreeDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileTreeDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeDisplay.Initialese>)} xyz.swapee.wc.back.FileTreeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileTreeDisplay} xyz.swapee.wc.back.IFileTreeDisplay.typeof */
/**
 * A concrete class of _IFileTreeDisplay_ instances.
 * @constructor xyz.swapee.wc.back.FileTreeDisplay
 * @implements {xyz.swapee.wc.back.IFileTreeDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.FileTreeDisplay = class extends /** @type {xyz.swapee.wc.back.FileTreeDisplay.constructor&xyz.swapee.wc.back.IFileTreeDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.FileTreeDisplay.prototype.constructor = xyz.swapee.wc.back.FileTreeDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileTreeDisplay}
 */
xyz.swapee.wc.back.FileTreeDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeDisplay.
 * @interface xyz.swapee.wc.back.IFileTreeDisplayFields
 */
xyz.swapee.wc.back.IFileTreeDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.LoIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.ContentWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.ExpandedIc = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.CollapsedIc = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.FolderWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.FolderLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.Folders = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.Files = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _FileEditor_ peer.
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.FileEditor = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The template VDU for _Folder_.
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.FolderTemplate = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The template VDU for _File_.
 */
xyz.swapee.wc.back.IFileTreeDisplayFields.prototype.FileTemplate = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IFileTreeDisplay} */
xyz.swapee.wc.back.RecordIFileTreeDisplay

/** @typedef {xyz.swapee.wc.back.IFileTreeDisplay} xyz.swapee.wc.back.BoundIFileTreeDisplay */

/** @typedef {xyz.swapee.wc.back.FileTreeDisplay} xyz.swapee.wc.back.BoundFileTreeDisplay */

/**
 * Contains getters to cast the _IFileTreeDisplay_ interface.
 * @interface xyz.swapee.wc.back.IFileTreeDisplayCaster
 */
xyz.swapee.wc.back.IFileTreeDisplayCaster = class { }
/**
 * Cast the _IFileTreeDisplay_ instance into the _BoundIFileTreeDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileTreeDisplay}
 */
xyz.swapee.wc.back.IFileTreeDisplayCaster.prototype.asIFileTreeDisplay
/**
 * Access the _FileTreeDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileTreeDisplay}
 */
xyz.swapee.wc.back.IFileTreeDisplayCaster.prototype.superFileTreeDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.FileTreeMemory, land?: !xyz.swapee.wc.FileTreeLand) => void} xyz.swapee.wc.back.IFileTreeDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IFileTreeDisplay.__paint<!xyz.swapee.wc.back.IFileTreeDisplay>} xyz.swapee.wc.back.IFileTreeDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IFileTreeDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.FileTreeMemory} [memory] The display data.
 * - `host` _string_ The host property. Default empty string.
 * - `heading` _boolean_ Whether the folder acts as heading. Default `false`.
 * - `root` _string_ The root folder on the disk to read from. Default empty string.
 * - `folder` _string_ The name of the folder inside the root presented to the user. Default `.`.
 * - `showHidden` _boolean_ Whether to report hidden files (those that start with `.`). Default `false`.
 * - `expanded` _boolean_ Whether folder is currently expanded. Default `false`.
 * - `foldersRotor` _Array&lt;!IFileTree.FolderBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFolder` method to transfer to the stator. Default `null`.
 * - `filesRotor` _Array&lt;!IFileTree.FileBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushFile` method to transfer to the stator. Default `null`.
 * - `folders` _!Array&lt;string&gt;_ The discovered folders. Default `[]`.
 * - `files` _!Array&lt;string&gt;_ The discovered files. Default `[]`.
 * - `loadingFiles` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreFiles` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadFilesError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.FileTreeLand} [land] The land data.
 * - `FileEditor` _xyz.swapee.wc.IFileEditor_
 * @return {void}
 */
xyz.swapee.wc.back.IFileTreeDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IFileTreeDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/41-FileTreeClasses.xml}  e46e592cc16b27d40cde1aa884e1e276 */
/**
 * The classes of the _IFileTreeDisplay_.
 * @record xyz.swapee.wc.FileTreeClasses
 */
xyz.swapee.wc.FileTreeClasses = class { }
/**
 *
 */
xyz.swapee.wc.FileTreeClasses.prototype.Heading = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.FileTreeClasses.prototype.FileOpen = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.FileTreeClasses.prototype.props = /** @type {xyz.swapee.wc.FileTreeClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/50-IFileTreeController.xml}  588968dc912bf21098cb131bc0f001ad */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IFileTreeController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IFileTreeController.Inputs, !xyz.swapee.wc.IFileTreeOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IFileTreeOuterCore.WeakModel>} xyz.swapee.wc.IFileTreeController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileTreeController)} xyz.swapee.wc.AbstractFileTreeController.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeController} xyz.swapee.wc.FileTreeController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeController` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeController
 */
xyz.swapee.wc.AbstractFileTreeController = class extends /** @type {xyz.swapee.wc.AbstractFileTreeController.constructor&xyz.swapee.wc.FileTreeController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeController.prototype.constructor = xyz.swapee.wc.AbstractFileTreeController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeController.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IFileTreeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeController}
 */
xyz.swapee.wc.AbstractFileTreeController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeController}
 */
xyz.swapee.wc.AbstractFileTreeController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IFileTreeControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeController}
 */
xyz.swapee.wc.AbstractFileTreeController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IFileTreeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeController}
 */
xyz.swapee.wc.AbstractFileTreeController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeController.Initialese[]) => xyz.swapee.wc.IFileTreeController} xyz.swapee.wc.FileTreeControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IFileTreeControllerHyperslice
 */
xyz.swapee.wc.IFileTreeControllerHyperslice = class {
  constructor() {
    /**
     * Calls to open the file.
     */
    this.openFile=/** @type {!xyz.swapee.wc.IFileTreeController._openFile|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeController._openFile>} */ (void 0)
  }
}
xyz.swapee.wc.IFileTreeControllerHyperslice.prototype.constructor = xyz.swapee.wc.IFileTreeControllerHyperslice

/**
 * A concrete class of _IFileTreeControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.FileTreeControllerHyperslice
 * @implements {xyz.swapee.wc.IFileTreeControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.FileTreeControllerHyperslice = class extends xyz.swapee.wc.IFileTreeControllerHyperslice { }
xyz.swapee.wc.FileTreeControllerHyperslice.prototype.constructor = xyz.swapee.wc.FileTreeControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IFileTreeControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IFileTreeControllerBindingHyperslice = class {
  constructor() {
    /**
     * Calls to open the file.
     */
    this.openFile=/** @type {!xyz.swapee.wc.IFileTreeController.__openFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileTreeController.__openFile<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IFileTreeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.IFileTreeControllerBindingHyperslice

/**
 * A concrete class of _IFileTreeControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.FileTreeControllerBindingHyperslice
 * @implements {xyz.swapee.wc.IFileTreeControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IFileTreeControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.FileTreeControllerBindingHyperslice = class extends xyz.swapee.wc.IFileTreeControllerBindingHyperslice { }
xyz.swapee.wc.FileTreeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.FileTreeControllerBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.IFileTreeControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IFileTreeController.Inputs, !xyz.swapee.wc.IFileTreeOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IFileTreeOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IFileTreeController.Inputs, !xyz.swapee.wc.IFileTreeController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IFileTreeController.Inputs, !xyz.swapee.wc.FileTreeMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IFileTreeController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IFileTreeController.Inputs>)} xyz.swapee.wc.IFileTreeController.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IFileTreeController
 */
xyz.swapee.wc.IFileTreeController = class extends /** @type {xyz.swapee.wc.IFileTreeController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreeController.resetPort} */
xyz.swapee.wc.IFileTreeController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IFileTreeController.openFile} */
xyz.swapee.wc.IFileTreeController.prototype.openFile = function() {}
/** @type {xyz.swapee.wc.IFileTreeController.onOpenFile} */
xyz.swapee.wc.IFileTreeController.prototype.onOpenFile = function() {}
/** @type {xyz.swapee.wc.IFileTreeController.flipExpanded} */
xyz.swapee.wc.IFileTreeController.prototype.flipExpanded = function() {}
/** @type {xyz.swapee.wc.IFileTreeController.setExpanded} */
xyz.swapee.wc.IFileTreeController.prototype.setExpanded = function() {}
/** @type {xyz.swapee.wc.IFileTreeController.unsetExpanded} */
xyz.swapee.wc.IFileTreeController.prototype.unsetExpanded = function() {}
/** @type {xyz.swapee.wc.IFileTreeController.loadFiles} */
xyz.swapee.wc.IFileTreeController.prototype.loadFiles = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeController&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeController.Initialese>)} xyz.swapee.wc.FileTreeController.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController} xyz.swapee.wc.IFileTreeController.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileTreeController_ instances.
 * @constructor xyz.swapee.wc.FileTreeController
 * @implements {xyz.swapee.wc.IFileTreeController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeController.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeController = class extends /** @type {xyz.swapee.wc.FileTreeController.constructor&xyz.swapee.wc.IFileTreeController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeController}
 */
xyz.swapee.wc.FileTreeController.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeController.
 * @interface xyz.swapee.wc.IFileTreeControllerFields
 */
xyz.swapee.wc.IFileTreeControllerFields = class { }
/**
 * The inputs to the _IFileTree_'s controller.
 */
xyz.swapee.wc.IFileTreeControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileTreeController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IFileTreeControllerFields.prototype.props = /** @type {xyz.swapee.wc.IFileTreeController} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeController} */
xyz.swapee.wc.RecordIFileTreeController

/** @typedef {xyz.swapee.wc.IFileTreeController} xyz.swapee.wc.BoundIFileTreeController */

/** @typedef {xyz.swapee.wc.FileTreeController} xyz.swapee.wc.BoundFileTreeController */

/** @typedef {xyz.swapee.wc.IFileTreePort.Inputs} xyz.swapee.wc.IFileTreeController.Inputs The inputs to the _IFileTree_'s controller. */

/** @typedef {xyz.swapee.wc.IFileTreePort.WeakInputs} xyz.swapee.wc.IFileTreeController.WeakInputs The inputs to the _IFileTree_'s controller. */

/**
 * Contains getters to cast the _IFileTreeController_ interface.
 * @interface xyz.swapee.wc.IFileTreeControllerCaster
 */
xyz.swapee.wc.IFileTreeControllerCaster = class { }
/**
 * Cast the _IFileTreeController_ instance into the _BoundIFileTreeController_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeController}
 */
xyz.swapee.wc.IFileTreeControllerCaster.prototype.asIFileTreeController
/**
 * Cast the _IFileTreeController_ instance into the _BoundIFileTreeProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeProcessor}
 */
xyz.swapee.wc.IFileTreeControllerCaster.prototype.asIFileTreeProcessor
/**
 * Access the _FileTreeController_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeController}
 */
xyz.swapee.wc.IFileTreeControllerCaster.prototype.superFileTreeController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreeController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeController.__resetPort<!xyz.swapee.wc.IFileTreeController>} xyz.swapee.wc.IFileTreeController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeController.resetPort = function() {}

/**
 * @typedef {(this: THIS, path: string, name: string, lang: string) => void} xyz.swapee.wc.IFileTreeController.__openFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeController.__openFile<!xyz.swapee.wc.IFileTreeController>} xyz.swapee.wc.IFileTreeController._openFile */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController.openFile} */
/**
 * Calls to open the file. `🔗 $combine`
 * @param {string} path The path on the FS.
 * @param {string} name The name of the file.
 * @param {string} lang The language.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeController.openFile = function(path, name, lang) {}

/**
 * @typedef {(this: THIS, path: string, name: string, lang: string) => void} xyz.swapee.wc.IFileTreeController.__onOpenFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeController.__onOpenFile<!xyz.swapee.wc.IFileTreeController>} xyz.swapee.wc.IFileTreeController._onOpenFile */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController.onOpenFile} */
/**
 * Calls to open the file.
 *
 * Executed when `openFile` is called. Can be used in relays.
 * @param {string} path The path on the FS.
 * @param {string} name The name of the file.
 * @param {string} lang The language.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeController.onOpenFile = function(path, name, lang) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreeController.__flipExpanded
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeController.__flipExpanded<!xyz.swapee.wc.IFileTreeController>} xyz.swapee.wc.IFileTreeController._flipExpanded */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController.flipExpanded} */
/**
 * Flips between the positive and negative values of the `expanded`.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeController.flipExpanded = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreeController.__setExpanded
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeController.__setExpanded<!xyz.swapee.wc.IFileTreeController>} xyz.swapee.wc.IFileTreeController._setExpanded */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController.setExpanded} */
/**
 * Sets the `expanded` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeController.setExpanded = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreeController.__unsetExpanded
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeController.__unsetExpanded<!xyz.swapee.wc.IFileTreeController>} xyz.swapee.wc.IFileTreeController._unsetExpanded */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController.unsetExpanded} */
/**
 * Clears the `expanded` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeController.unsetExpanded = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileTreeController.__loadFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeController.__loadFiles<!xyz.swapee.wc.IFileTreeController>} xyz.swapee.wc.IFileTreeController._loadFiles */
/** @typedef {typeof xyz.swapee.wc.IFileTreeController.loadFiles} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.IFileTreeController.loadFiles = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/51-IFileTreeControllerFront.xml}  0831bb28fe376e5ca6fa61c2e5b2c652 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IFileTreeController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.FileTreeController)} xyz.swapee.wc.front.AbstractFileTreeController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.FileTreeController} xyz.swapee.wc.front.FileTreeController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IFileTreeController` interface.
 * @constructor xyz.swapee.wc.front.AbstractFileTreeController
 */
xyz.swapee.wc.front.AbstractFileTreeController = class extends /** @type {xyz.swapee.wc.front.AbstractFileTreeController.constructor&xyz.swapee.wc.front.FileTreeController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractFileTreeController.prototype.constructor = xyz.swapee.wc.front.AbstractFileTreeController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractFileTreeController.class = /** @type {typeof xyz.swapee.wc.front.AbstractFileTreeController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IFileTreeController|typeof xyz.swapee.wc.front.FileTreeController)|(!xyz.swapee.wc.front.IFileTreeControllerAT|typeof xyz.swapee.wc.front.FileTreeControllerAT)|!xyz.swapee.wc.front.IFileTreeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractFileTreeController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractFileTreeController}
 */
xyz.swapee.wc.front.AbstractFileTreeController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeController}
 */
xyz.swapee.wc.front.AbstractFileTreeController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IFileTreeController|typeof xyz.swapee.wc.front.FileTreeController)|(!xyz.swapee.wc.front.IFileTreeControllerAT|typeof xyz.swapee.wc.front.FileTreeControllerAT)|!xyz.swapee.wc.front.IFileTreeControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeController}
 */
xyz.swapee.wc.front.AbstractFileTreeController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IFileTreeController|typeof xyz.swapee.wc.front.FileTreeController)|(!xyz.swapee.wc.front.IFileTreeControllerAT|typeof xyz.swapee.wc.front.FileTreeControllerAT)|!xyz.swapee.wc.front.IFileTreeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeController}
 */
xyz.swapee.wc.front.AbstractFileTreeController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IFileTreeController.Initialese[]) => xyz.swapee.wc.front.IFileTreeController} xyz.swapee.wc.front.FileTreeControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.IFileTreeControllerHyperslice
 */
xyz.swapee.wc.front.IFileTreeControllerHyperslice = class {
  constructor() {
    /**
     * Calls to open the file.
     */
    this.openFile=/** @type {!xyz.swapee.wc.front.IFileTreeController._openFile|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IFileTreeController._openFile>} */ (void 0)
  }
}
xyz.swapee.wc.front.IFileTreeControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.IFileTreeControllerHyperslice

/**
 * A concrete class of _IFileTreeControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.FileTreeControllerHyperslice
 * @implements {xyz.swapee.wc.front.IFileTreeControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.front.FileTreeControllerHyperslice = class extends xyz.swapee.wc.front.IFileTreeControllerHyperslice { }
xyz.swapee.wc.front.FileTreeControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.FileTreeControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.IFileTreeControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.front.IFileTreeControllerBindingHyperslice = class {
  constructor() {
    /**
     * Calls to open the file.
     */
    this.openFile=/** @type {!xyz.swapee.wc.front.IFileTreeController.__openFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IFileTreeController.__openFile<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.front.IFileTreeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.IFileTreeControllerBindingHyperslice

/**
 * A concrete class of _IFileTreeControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.FileTreeControllerBindingHyperslice
 * @implements {xyz.swapee.wc.front.IFileTreeControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.front.IFileTreeControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.front.FileTreeControllerBindingHyperslice = class extends xyz.swapee.wc.front.IFileTreeControllerBindingHyperslice { }
xyz.swapee.wc.front.FileTreeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.FileTreeControllerBindingHyperslice

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IFileTreeControllerCaster&xyz.swapee.wc.front.IFileTreeControllerAT)} xyz.swapee.wc.front.IFileTreeController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeControllerAT} xyz.swapee.wc.front.IFileTreeControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IFileTreeController
 */
xyz.swapee.wc.front.IFileTreeController = class extends /** @type {xyz.swapee.wc.front.IFileTreeController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IFileTreeControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileTreeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IFileTreeController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IFileTreeController.openFile} */
xyz.swapee.wc.front.IFileTreeController.prototype.openFile = function() {}
/** @type {xyz.swapee.wc.front.IFileTreeController.onOpenFile} */
xyz.swapee.wc.front.IFileTreeController.prototype.onOpenFile = function() {}
/** @type {xyz.swapee.wc.front.IFileTreeController.flipExpanded} */
xyz.swapee.wc.front.IFileTreeController.prototype.flipExpanded = function() {}
/** @type {xyz.swapee.wc.front.IFileTreeController.setExpanded} */
xyz.swapee.wc.front.IFileTreeController.prototype.setExpanded = function() {}
/** @type {xyz.swapee.wc.front.IFileTreeController.unsetExpanded} */
xyz.swapee.wc.front.IFileTreeController.prototype.unsetExpanded = function() {}
/** @type {xyz.swapee.wc.front.IFileTreeController.loadFiles} */
xyz.swapee.wc.front.IFileTreeController.prototype.loadFiles = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IFileTreeController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileTreeController.Initialese>)} xyz.swapee.wc.front.FileTreeController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeController} xyz.swapee.wc.front.IFileTreeController.typeof */
/**
 * A concrete class of _IFileTreeController_ instances.
 * @constructor xyz.swapee.wc.front.FileTreeController
 * @implements {xyz.swapee.wc.front.IFileTreeController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileTreeController.Initialese>} ‎
 */
xyz.swapee.wc.front.FileTreeController = class extends /** @type {xyz.swapee.wc.front.FileTreeController.constructor&xyz.swapee.wc.front.IFileTreeController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IFileTreeController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileTreeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.FileTreeController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.FileTreeController}
 */
xyz.swapee.wc.front.FileTreeController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IFileTreeController} */
xyz.swapee.wc.front.RecordIFileTreeController

/** @typedef {xyz.swapee.wc.front.IFileTreeController} xyz.swapee.wc.front.BoundIFileTreeController */

/** @typedef {xyz.swapee.wc.front.FileTreeController} xyz.swapee.wc.front.BoundFileTreeController */

/**
 * Contains getters to cast the _IFileTreeController_ interface.
 * @interface xyz.swapee.wc.front.IFileTreeControllerCaster
 */
xyz.swapee.wc.front.IFileTreeControllerCaster = class { }
/**
 * Cast the _IFileTreeController_ instance into the _BoundIFileTreeController_ type.
 * @type {!xyz.swapee.wc.front.BoundIFileTreeController}
 */
xyz.swapee.wc.front.IFileTreeControllerCaster.prototype.asIFileTreeController
/**
 * Access the _FileTreeController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundFileTreeController}
 */
xyz.swapee.wc.front.IFileTreeControllerCaster.prototype.superFileTreeController

/**
 * @typedef {(this: THIS, path: string, name: string, lang: string) => void} xyz.swapee.wc.front.IFileTreeController.__openFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileTreeController.__openFile<!xyz.swapee.wc.front.IFileTreeController>} xyz.swapee.wc.front.IFileTreeController._openFile */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeController.openFile} */
/**
 * Calls to open the file. `🔗 $combine` `🔗 $combine`
 * @param {string} path The path on the FS.
 * @param {string} name The name of the file.
 * @param {string} lang The language.
 * @return {void}
 */
xyz.swapee.wc.front.IFileTreeController.openFile = function(path, name, lang) {}

/**
 * @typedef {(this: THIS, path: string, name: string, lang: string) => void} xyz.swapee.wc.front.IFileTreeController.__onOpenFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileTreeController.__onOpenFile<!xyz.swapee.wc.front.IFileTreeController>} xyz.swapee.wc.front.IFileTreeController._onOpenFile */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeController.onOpenFile} */
/**
 * Calls to open the file.
 *
 * Executed when `openFile` is called. Can be used in relays.
 * @param {string} path The path on the FS.
 * @param {string} name The name of the file.
 * @param {string} lang The language.
 * @return {void}
 */
xyz.swapee.wc.front.IFileTreeController.onOpenFile = function(path, name, lang) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileTreeController.__flipExpanded
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileTreeController.__flipExpanded<!xyz.swapee.wc.front.IFileTreeController>} xyz.swapee.wc.front.IFileTreeController._flipExpanded */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeController.flipExpanded} */
/**
 * Flips between the positive and negative values of the `expanded`.
 * @return {void}
 */
xyz.swapee.wc.front.IFileTreeController.flipExpanded = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileTreeController.__setExpanded
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileTreeController.__setExpanded<!xyz.swapee.wc.front.IFileTreeController>} xyz.swapee.wc.front.IFileTreeController._setExpanded */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeController.setExpanded} */
/**
 * Sets the `expanded` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IFileTreeController.setExpanded = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileTreeController.__unsetExpanded
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileTreeController.__unsetExpanded<!xyz.swapee.wc.front.IFileTreeController>} xyz.swapee.wc.front.IFileTreeController._unsetExpanded */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeController.unsetExpanded} */
/**
 * Clears the `expanded` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IFileTreeController.unsetExpanded = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileTreeController.__loadFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileTreeController.__loadFiles<!xyz.swapee.wc.front.IFileTreeController>} xyz.swapee.wc.front.IFileTreeController._loadFiles */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeController.loadFiles} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.front.IFileTreeController.loadFiles = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IFileTreeController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/52-IFileTreeControllerBack.xml}  6e8abc06b057375aae07943315280541 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IFileTreeController.Inputs>&xyz.swapee.wc.IFileTreeController.Initialese} xyz.swapee.wc.back.IFileTreeController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.FileTreeController)} xyz.swapee.wc.back.AbstractFileTreeController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileTreeController} xyz.swapee.wc.back.FileTreeController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileTreeController` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileTreeController
 */
xyz.swapee.wc.back.AbstractFileTreeController = class extends /** @type {xyz.swapee.wc.back.AbstractFileTreeController.constructor&xyz.swapee.wc.back.FileTreeController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileTreeController.prototype.constructor = xyz.swapee.wc.back.AbstractFileTreeController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileTreeController.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileTreeController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileTreeController|typeof xyz.swapee.wc.back.FileTreeController)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileTreeController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileTreeController}
 */
xyz.swapee.wc.back.AbstractFileTreeController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeController}
 */
xyz.swapee.wc.back.AbstractFileTreeController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileTreeController|typeof xyz.swapee.wc.back.FileTreeController)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeController}
 */
xyz.swapee.wc.back.AbstractFileTreeController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileTreeController|typeof xyz.swapee.wc.back.FileTreeController)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeController}
 */
xyz.swapee.wc.back.AbstractFileTreeController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileTreeController.Initialese[]) => xyz.swapee.wc.back.IFileTreeController} xyz.swapee.wc.back.FileTreeControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileTreeControllerCaster&xyz.swapee.wc.IFileTreeController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IFileTreeController.Inputs>)} xyz.swapee.wc.back.IFileTreeController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IFileTreeController
 */
xyz.swapee.wc.back.IFileTreeController = class extends /** @type {xyz.swapee.wc.back.IFileTreeController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileTreeController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileTreeController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileTreeController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeController.Initialese>)} xyz.swapee.wc.back.FileTreeController.constructor */
/**
 * A concrete class of _IFileTreeController_ instances.
 * @constructor xyz.swapee.wc.back.FileTreeController
 * @implements {xyz.swapee.wc.back.IFileTreeController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeController.Initialese>} ‎
 */
xyz.swapee.wc.back.FileTreeController = class extends /** @type {xyz.swapee.wc.back.FileTreeController.constructor&xyz.swapee.wc.back.IFileTreeController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileTreeController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileTreeController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileTreeController}
 */
xyz.swapee.wc.back.FileTreeController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileTreeController} */
xyz.swapee.wc.back.RecordIFileTreeController

/** @typedef {xyz.swapee.wc.back.IFileTreeController} xyz.swapee.wc.back.BoundIFileTreeController */

/** @typedef {xyz.swapee.wc.back.FileTreeController} xyz.swapee.wc.back.BoundFileTreeController */

/**
 * Contains getters to cast the _IFileTreeController_ interface.
 * @interface xyz.swapee.wc.back.IFileTreeControllerCaster
 */
xyz.swapee.wc.back.IFileTreeControllerCaster = class { }
/**
 * Cast the _IFileTreeController_ instance into the _BoundIFileTreeController_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileTreeController}
 */
xyz.swapee.wc.back.IFileTreeControllerCaster.prototype.asIFileTreeController
/**
 * Access the _FileTreeController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileTreeController}
 */
xyz.swapee.wc.back.IFileTreeControllerCaster.prototype.superFileTreeController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/53-IFileTreeControllerAR.xml}  f54544acb402dd52eba9a73518173db2 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IFileTreeController.Initialese} xyz.swapee.wc.back.IFileTreeControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.FileTreeControllerAR)} xyz.swapee.wc.back.AbstractFileTreeControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileTreeControllerAR} xyz.swapee.wc.back.FileTreeControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileTreeControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileTreeControllerAR
 */
xyz.swapee.wc.back.AbstractFileTreeControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractFileTreeControllerAR.constructor&xyz.swapee.wc.back.FileTreeControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileTreeControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractFileTreeControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileTreeControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileTreeControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileTreeControllerAR|typeof xyz.swapee.wc.back.FileTreeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileTreeControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileTreeControllerAR}
 */
xyz.swapee.wc.back.AbstractFileTreeControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeControllerAR}
 */
xyz.swapee.wc.back.AbstractFileTreeControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileTreeControllerAR|typeof xyz.swapee.wc.back.FileTreeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeControllerAR}
 */
xyz.swapee.wc.back.AbstractFileTreeControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileTreeControllerAR|typeof xyz.swapee.wc.back.FileTreeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileTreeController|typeof xyz.swapee.wc.FileTreeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeControllerAR}
 */
xyz.swapee.wc.back.AbstractFileTreeControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileTreeControllerAR.Initialese[]) => xyz.swapee.wc.back.IFileTreeControllerAR} xyz.swapee.wc.back.FileTreeControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileTreeControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IFileTreeController)} xyz.swapee.wc.back.IFileTreeControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IFileTreeControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IFileTreeControllerAR
 */
xyz.swapee.wc.back.IFileTreeControllerAR = class extends /** @type {xyz.swapee.wc.back.IFileTreeControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IFileTreeController.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileTreeControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileTreeControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeControllerAR.Initialese>)} xyz.swapee.wc.back.FileTreeControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileTreeControllerAR} xyz.swapee.wc.back.IFileTreeControllerAR.typeof */
/**
 * A concrete class of _IFileTreeControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.FileTreeControllerAR
 * @implements {xyz.swapee.wc.back.IFileTreeControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IFileTreeControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.FileTreeControllerAR = class extends /** @type {xyz.swapee.wc.back.FileTreeControllerAR.constructor&xyz.swapee.wc.back.IFileTreeControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileTreeControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileTreeControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileTreeControllerAR}
 */
xyz.swapee.wc.back.FileTreeControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileTreeControllerAR} */
xyz.swapee.wc.back.RecordIFileTreeControllerAR

/** @typedef {xyz.swapee.wc.back.IFileTreeControllerAR} xyz.swapee.wc.back.BoundIFileTreeControllerAR */

/** @typedef {xyz.swapee.wc.back.FileTreeControllerAR} xyz.swapee.wc.back.BoundFileTreeControllerAR */

/**
 * Contains getters to cast the _IFileTreeControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IFileTreeControllerARCaster
 */
xyz.swapee.wc.back.IFileTreeControllerARCaster = class { }
/**
 * Cast the _IFileTreeControllerAR_ instance into the _BoundIFileTreeControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileTreeControllerAR}
 */
xyz.swapee.wc.back.IFileTreeControllerARCaster.prototype.asIFileTreeControllerAR
/**
 * Access the _FileTreeControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileTreeControllerAR}
 */
xyz.swapee.wc.back.IFileTreeControllerARCaster.prototype.superFileTreeControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/54-IFileTreeControllerAT.xml}  dcc79fcd247a4ef425f1dc3ef94cde95 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IFileTreeControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.FileTreeControllerAT)} xyz.swapee.wc.front.AbstractFileTreeControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.FileTreeControllerAT} xyz.swapee.wc.front.FileTreeControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IFileTreeControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractFileTreeControllerAT
 */
xyz.swapee.wc.front.AbstractFileTreeControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractFileTreeControllerAT.constructor&xyz.swapee.wc.front.FileTreeControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractFileTreeControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractFileTreeControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractFileTreeControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractFileTreeControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IFileTreeControllerAT|typeof xyz.swapee.wc.front.FileTreeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractFileTreeControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractFileTreeControllerAT}
 */
xyz.swapee.wc.front.AbstractFileTreeControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeControllerAT}
 */
xyz.swapee.wc.front.AbstractFileTreeControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IFileTreeControllerAT|typeof xyz.swapee.wc.front.FileTreeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeControllerAT}
 */
xyz.swapee.wc.front.AbstractFileTreeControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IFileTreeControllerAT|typeof xyz.swapee.wc.front.FileTreeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeControllerAT}
 */
xyz.swapee.wc.front.AbstractFileTreeControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IFileTreeControllerAT.Initialese[]) => xyz.swapee.wc.front.IFileTreeControllerAT} xyz.swapee.wc.front.FileTreeControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IFileTreeControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IFileTreeControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IFileTreeControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IFileTreeControllerAT
 */
xyz.swapee.wc.front.IFileTreeControllerAT = class extends /** @type {xyz.swapee.wc.front.IFileTreeControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileTreeControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IFileTreeControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IFileTreeControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileTreeControllerAT.Initialese>)} xyz.swapee.wc.front.FileTreeControllerAT.constructor */
/**
 * A concrete class of _IFileTreeControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.FileTreeControllerAT
 * @implements {xyz.swapee.wc.front.IFileTreeControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IFileTreeControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileTreeControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.FileTreeControllerAT = class extends /** @type {xyz.swapee.wc.front.FileTreeControllerAT.constructor&xyz.swapee.wc.front.IFileTreeControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IFileTreeControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileTreeControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.FileTreeControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.FileTreeControllerAT}
 */
xyz.swapee.wc.front.FileTreeControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IFileTreeControllerAT} */
xyz.swapee.wc.front.RecordIFileTreeControllerAT

/** @typedef {xyz.swapee.wc.front.IFileTreeControllerAT} xyz.swapee.wc.front.BoundIFileTreeControllerAT */

/** @typedef {xyz.swapee.wc.front.FileTreeControllerAT} xyz.swapee.wc.front.BoundFileTreeControllerAT */

/**
 * Contains getters to cast the _IFileTreeControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IFileTreeControllerATCaster
 */
xyz.swapee.wc.front.IFileTreeControllerATCaster = class { }
/**
 * Cast the _IFileTreeControllerAT_ instance into the _BoundIFileTreeControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIFileTreeControllerAT}
 */
xyz.swapee.wc.front.IFileTreeControllerATCaster.prototype.asIFileTreeControllerAT
/**
 * Access the _FileTreeControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundFileTreeControllerAT}
 */
xyz.swapee.wc.front.IFileTreeControllerATCaster.prototype.superFileTreeControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/70-IFileTreeScreen.xml}  d4174f10b0a6f4de6111bd576f267756 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.front.FileTreeInputs, !HTMLDivElement, !xyz.swapee.wc.IFileTreeDisplay.Settings, !xyz.swapee.wc.IFileTreeDisplay.Queries, null>&xyz.swapee.wc.IFileTreeDisplay.Initialese} xyz.swapee.wc.IFileTreeScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileTreeScreen)} xyz.swapee.wc.AbstractFileTreeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeScreen} xyz.swapee.wc.FileTreeScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeScreen` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeScreen
 */
xyz.swapee.wc.AbstractFileTreeScreen = class extends /** @type {xyz.swapee.wc.AbstractFileTreeScreen.constructor&xyz.swapee.wc.FileTreeScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeScreen.prototype.constructor = xyz.swapee.wc.AbstractFileTreeScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeScreen.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeScreen|typeof xyz.swapee.wc.FileTreeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IFileTreeController|typeof xyz.swapee.wc.front.FileTreeController)|(!xyz.swapee.wc.IFileTreeDisplay|typeof xyz.swapee.wc.FileTreeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeScreen}
 */
xyz.swapee.wc.AbstractFileTreeScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeScreen}
 */
xyz.swapee.wc.AbstractFileTreeScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeScreen|typeof xyz.swapee.wc.FileTreeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IFileTreeController|typeof xyz.swapee.wc.front.FileTreeController)|(!xyz.swapee.wc.IFileTreeDisplay|typeof xyz.swapee.wc.FileTreeDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeScreen}
 */
xyz.swapee.wc.AbstractFileTreeScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeScreen|typeof xyz.swapee.wc.FileTreeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IFileTreeController|typeof xyz.swapee.wc.front.FileTreeController)|(!xyz.swapee.wc.IFileTreeDisplay|typeof xyz.swapee.wc.FileTreeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeScreen}
 */
xyz.swapee.wc.AbstractFileTreeScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeScreen.Initialese[]) => xyz.swapee.wc.IFileTreeScreen} xyz.swapee.wc.FileTreeScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileTreeScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.FileTreeMemory, !xyz.swapee.wc.front.FileTreeInputs, !HTMLDivElement, !xyz.swapee.wc.IFileTreeDisplay.Settings, !xyz.swapee.wc.IFileTreeDisplay.Queries, null, null>&xyz.swapee.wc.front.IFileTreeController&xyz.swapee.wc.IFileTreeDisplay)} xyz.swapee.wc.IFileTreeScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IFileTreeScreen
 */
xyz.swapee.wc.IFileTreeScreen = class extends /** @type {xyz.swapee.wc.IFileTreeScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IFileTreeController.typeof&xyz.swapee.wc.IFileTreeDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeScreen.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreeScreen.makeFolderElement} */
xyz.swapee.wc.IFileTreeScreen.prototype.makeFolderElement = function() {}
/** @type {xyz.swapee.wc.IFileTreeScreen.makeFileElement} */
xyz.swapee.wc.IFileTreeScreen.prototype.makeFileElement = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeScreen.Initialese>)} xyz.swapee.wc.FileTreeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileTreeScreen} xyz.swapee.wc.IFileTreeScreen.typeof */
/**
 * A concrete class of _IFileTreeScreen_ instances.
 * @constructor xyz.swapee.wc.FileTreeScreen
 * @implements {xyz.swapee.wc.IFileTreeScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeScreen.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeScreen = class extends /** @type {xyz.swapee.wc.FileTreeScreen.constructor&xyz.swapee.wc.IFileTreeScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeScreen}
 */
xyz.swapee.wc.FileTreeScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileTreeScreen} */
xyz.swapee.wc.RecordIFileTreeScreen

/** @typedef {xyz.swapee.wc.IFileTreeScreen} xyz.swapee.wc.BoundIFileTreeScreen */

/** @typedef {xyz.swapee.wc.FileTreeScreen} xyz.swapee.wc.BoundFileTreeScreen */

/**
 * Contains getters to cast the _IFileTreeScreen_ interface.
 * @interface xyz.swapee.wc.IFileTreeScreenCaster
 */
xyz.swapee.wc.IFileTreeScreenCaster = class { }
/**
 * Cast the _IFileTreeScreen_ instance into the _BoundIFileTreeScreen_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeScreen}
 */
xyz.swapee.wc.IFileTreeScreenCaster.prototype.asIFileTreeScreen
/**
 * Access the _FileTreeScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeScreen}
 */
xyz.swapee.wc.IFileTreeScreenCaster.prototype.superFileTreeScreen

/**
 * @typedef {(this: THIS, tempVid: number) => !HTMLDivElement} xyz.swapee.wc.IFileTreeScreen.__makeFolderElement
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeScreen.__makeFolderElement<!xyz.swapee.wc.IFileTreeScreen>} xyz.swapee.wc.IFileTreeScreen._makeFolderElement */
/** @typedef {typeof xyz.swapee.wc.IFileTreeScreen.makeFolderElement} */
/**
 * Creates a new VDU from the _FolderTemplate_.
 * @param {number} tempVid The VDU id recorded on the back.
 * @return {!HTMLDivElement}
 */
xyz.swapee.wc.IFileTreeScreen.makeFolderElement = function(tempVid) {}

/**
 * @typedef {(this: THIS, tempVid: number) => !HTMLDivElement} xyz.swapee.wc.IFileTreeScreen.__makeFileElement
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeScreen.__makeFileElement<!xyz.swapee.wc.IFileTreeScreen>} xyz.swapee.wc.IFileTreeScreen._makeFileElement */
/** @typedef {typeof xyz.swapee.wc.IFileTreeScreen.makeFileElement} */
/**
 * Creates a new VDU from the _FileTemplate_.
 * @param {number} tempVid The VDU id recorded on the back.
 * @return {!HTMLDivElement}
 */
xyz.swapee.wc.IFileTreeScreen.makeFileElement = function(tempVid) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeScreen
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/70-IFileTreeScreenBack.xml}  834c67047311ad6f13fc493fd591f80d */
/** @typedef {xyz.swapee.wc.back.IFileTreeScreenAT.Initialese} xyz.swapee.wc.back.IFileTreeScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.FileTreeScreen)} xyz.swapee.wc.back.AbstractFileTreeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileTreeScreen} xyz.swapee.wc.back.FileTreeScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileTreeScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileTreeScreen
 */
xyz.swapee.wc.back.AbstractFileTreeScreen = class extends /** @type {xyz.swapee.wc.back.AbstractFileTreeScreen.constructor&xyz.swapee.wc.back.FileTreeScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileTreeScreen.prototype.constructor = xyz.swapee.wc.back.AbstractFileTreeScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileTreeScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileTreeScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileTreeScreen|typeof xyz.swapee.wc.back.FileTreeScreen)|(!xyz.swapee.wc.back.IFileTreeScreenAT|typeof xyz.swapee.wc.back.FileTreeScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileTreeScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileTreeScreen}
 */
xyz.swapee.wc.back.AbstractFileTreeScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreen}
 */
xyz.swapee.wc.back.AbstractFileTreeScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileTreeScreen|typeof xyz.swapee.wc.back.FileTreeScreen)|(!xyz.swapee.wc.back.IFileTreeScreenAT|typeof xyz.swapee.wc.back.FileTreeScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreen}
 */
xyz.swapee.wc.back.AbstractFileTreeScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileTreeScreen|typeof xyz.swapee.wc.back.FileTreeScreen)|(!xyz.swapee.wc.back.IFileTreeScreenAT|typeof xyz.swapee.wc.back.FileTreeScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreen}
 */
xyz.swapee.wc.back.AbstractFileTreeScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileTreeScreen.Initialese[]) => xyz.swapee.wc.back.IFileTreeScreen} xyz.swapee.wc.back.FileTreeScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileTreeScreenCaster&xyz.swapee.wc.back.IFileTreeScreenAT)} xyz.swapee.wc.back.IFileTreeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileTreeScreenAT} xyz.swapee.wc.back.IFileTreeScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IFileTreeScreen
 */
xyz.swapee.wc.back.IFileTreeScreen = class extends /** @type {xyz.swapee.wc.back.IFileTreeScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IFileTreeScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileTreeScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileTreeScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeScreen.Initialese>)} xyz.swapee.wc.back.FileTreeScreen.constructor */
/**
 * A concrete class of _IFileTreeScreen_ instances.
 * @constructor xyz.swapee.wc.back.FileTreeScreen
 * @implements {xyz.swapee.wc.back.IFileTreeScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.FileTreeScreen = class extends /** @type {xyz.swapee.wc.back.FileTreeScreen.constructor&xyz.swapee.wc.back.IFileTreeScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileTreeScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileTreeScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileTreeScreen}
 */
xyz.swapee.wc.back.FileTreeScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileTreeScreen} */
xyz.swapee.wc.back.RecordIFileTreeScreen

/** @typedef {xyz.swapee.wc.back.IFileTreeScreen} xyz.swapee.wc.back.BoundIFileTreeScreen */

/** @typedef {xyz.swapee.wc.back.FileTreeScreen} xyz.swapee.wc.back.BoundFileTreeScreen */

/**
 * Contains getters to cast the _IFileTreeScreen_ interface.
 * @interface xyz.swapee.wc.back.IFileTreeScreenCaster
 */
xyz.swapee.wc.back.IFileTreeScreenCaster = class { }
/**
 * Cast the _IFileTreeScreen_ instance into the _BoundIFileTreeScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileTreeScreen}
 */
xyz.swapee.wc.back.IFileTreeScreenCaster.prototype.asIFileTreeScreen
/**
 * Access the _FileTreeScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileTreeScreen}
 */
xyz.swapee.wc.back.IFileTreeScreenCaster.prototype.superFileTreeScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/73-IFileTreeScreenAR.xml}  7aac54248f7aadcb51c52a9ac0689067 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IFileTreeScreen.Initialese} xyz.swapee.wc.front.IFileTreeScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.FileTreeScreenAR)} xyz.swapee.wc.front.AbstractFileTreeScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.FileTreeScreenAR} xyz.swapee.wc.front.FileTreeScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IFileTreeScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractFileTreeScreenAR
 */
xyz.swapee.wc.front.AbstractFileTreeScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractFileTreeScreenAR.constructor&xyz.swapee.wc.front.FileTreeScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractFileTreeScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractFileTreeScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractFileTreeScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractFileTreeScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IFileTreeScreenAR|typeof xyz.swapee.wc.front.FileTreeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileTreeScreen|typeof xyz.swapee.wc.FileTreeScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractFileTreeScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractFileTreeScreenAR}
 */
xyz.swapee.wc.front.AbstractFileTreeScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeScreenAR}
 */
xyz.swapee.wc.front.AbstractFileTreeScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IFileTreeScreenAR|typeof xyz.swapee.wc.front.FileTreeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileTreeScreen|typeof xyz.swapee.wc.FileTreeScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeScreenAR}
 */
xyz.swapee.wc.front.AbstractFileTreeScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IFileTreeScreenAR|typeof xyz.swapee.wc.front.FileTreeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileTreeScreen|typeof xyz.swapee.wc.FileTreeScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileTreeScreenAR}
 */
xyz.swapee.wc.front.AbstractFileTreeScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IFileTreeScreenAR.Initialese[]) => xyz.swapee.wc.front.IFileTreeScreenAR} xyz.swapee.wc.front.FileTreeScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IFileTreeScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IFileTreeScreen)} xyz.swapee.wc.front.IFileTreeScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IFileTreeScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IFileTreeScreenAR
 */
xyz.swapee.wc.front.IFileTreeScreenAR = class extends /** @type {xyz.swapee.wc.front.IFileTreeScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IFileTreeScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileTreeScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IFileTreeScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IFileTreeScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileTreeScreenAR.Initialese>)} xyz.swapee.wc.front.FileTreeScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IFileTreeScreenAR} xyz.swapee.wc.front.IFileTreeScreenAR.typeof */
/**
 * A concrete class of _IFileTreeScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.FileTreeScreenAR
 * @implements {xyz.swapee.wc.front.IFileTreeScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IFileTreeScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileTreeScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.FileTreeScreenAR = class extends /** @type {xyz.swapee.wc.front.FileTreeScreenAR.constructor&xyz.swapee.wc.front.IFileTreeScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IFileTreeScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileTreeScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.FileTreeScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.FileTreeScreenAR}
 */
xyz.swapee.wc.front.FileTreeScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IFileTreeScreenAR} */
xyz.swapee.wc.front.RecordIFileTreeScreenAR

/** @typedef {xyz.swapee.wc.front.IFileTreeScreenAR} xyz.swapee.wc.front.BoundIFileTreeScreenAR */

/** @typedef {xyz.swapee.wc.front.FileTreeScreenAR} xyz.swapee.wc.front.BoundFileTreeScreenAR */

/**
 * Contains getters to cast the _IFileTreeScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IFileTreeScreenARCaster
 */
xyz.swapee.wc.front.IFileTreeScreenARCaster = class { }
/**
 * Cast the _IFileTreeScreenAR_ instance into the _BoundIFileTreeScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIFileTreeScreenAR}
 */
xyz.swapee.wc.front.IFileTreeScreenARCaster.prototype.asIFileTreeScreenAR
/**
 * Access the _FileTreeScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundFileTreeScreenAR}
 */
xyz.swapee.wc.front.IFileTreeScreenARCaster.prototype.superFileTreeScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/74-IFileTreeScreenAT.xml}  74496ce656d376dfdffeb135ccfd7566 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IFileTreeScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.FileTreeScreenAT)} xyz.swapee.wc.back.AbstractFileTreeScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileTreeScreenAT} xyz.swapee.wc.back.FileTreeScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileTreeScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileTreeScreenAT
 */
xyz.swapee.wc.back.AbstractFileTreeScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractFileTreeScreenAT.constructor&xyz.swapee.wc.back.FileTreeScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileTreeScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractFileTreeScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileTreeScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileTreeScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileTreeScreenAT|typeof xyz.swapee.wc.back.FileTreeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileTreeScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileTreeScreenAT}
 */
xyz.swapee.wc.back.AbstractFileTreeScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreenAT}
 */
xyz.swapee.wc.back.AbstractFileTreeScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileTreeScreenAT|typeof xyz.swapee.wc.back.FileTreeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreenAT}
 */
xyz.swapee.wc.back.AbstractFileTreeScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileTreeScreenAT|typeof xyz.swapee.wc.back.FileTreeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileTreeScreenAT}
 */
xyz.swapee.wc.back.AbstractFileTreeScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileTreeScreenAT.Initialese[]) => xyz.swapee.wc.back.IFileTreeScreenAT} xyz.swapee.wc.back.FileTreeScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileTreeScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IFileTreeScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IFileTreeScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IFileTreeScreenAT
 */
xyz.swapee.wc.back.IFileTreeScreenAT = class extends /** @type {xyz.swapee.wc.back.IFileTreeScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileTreeScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileTreeScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeScreenAT.Initialese>)} xyz.swapee.wc.back.FileTreeScreenAT.constructor */
/**
 * A concrete class of _IFileTreeScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.FileTreeScreenAT
 * @implements {xyz.swapee.wc.back.IFileTreeScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IFileTreeScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileTreeScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.FileTreeScreenAT = class extends /** @type {xyz.swapee.wc.back.FileTreeScreenAT.constructor&xyz.swapee.wc.back.IFileTreeScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileTreeScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileTreeScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileTreeScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileTreeScreenAT}
 */
xyz.swapee.wc.back.FileTreeScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileTreeScreenAT} */
xyz.swapee.wc.back.RecordIFileTreeScreenAT

/** @typedef {xyz.swapee.wc.back.IFileTreeScreenAT} xyz.swapee.wc.back.BoundIFileTreeScreenAT */

/** @typedef {xyz.swapee.wc.back.FileTreeScreenAT} xyz.swapee.wc.back.BoundFileTreeScreenAT */

/**
 * Contains getters to cast the _IFileTreeScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IFileTreeScreenATCaster
 */
xyz.swapee.wc.back.IFileTreeScreenATCaster = class { }
/**
 * Cast the _IFileTreeScreenAT_ instance into the _BoundIFileTreeScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileTreeScreenAT}
 */
xyz.swapee.wc.back.IFileTreeScreenATCaster.prototype.asIFileTreeScreenAT
/**
 * Access the _FileTreeScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileTreeScreenAT}
 */
xyz.swapee.wc.back.IFileTreeScreenATCaster.prototype.superFileTreeScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-tree/FileTree.mvc/design/80-IFileTreeGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IFileTreeDisplay.Initialese} xyz.swapee.wc.IFileTreeGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileTreeGPU)} xyz.swapee.wc.AbstractFileTreeGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.FileTreeGPU} xyz.swapee.wc.FileTreeGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileTreeGPU` interface.
 * @constructor xyz.swapee.wc.AbstractFileTreeGPU
 */
xyz.swapee.wc.AbstractFileTreeGPU = class extends /** @type {xyz.swapee.wc.AbstractFileTreeGPU.constructor&xyz.swapee.wc.FileTreeGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileTreeGPU.prototype.constructor = xyz.swapee.wc.AbstractFileTreeGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileTreeGPU.class = /** @type {typeof xyz.swapee.wc.AbstractFileTreeGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileTreeGPU|typeof xyz.swapee.wc.FileTreeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IFileTreeDisplay|typeof xyz.swapee.wc.back.FileTreeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileTreeGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileTreeGPU}
 */
xyz.swapee.wc.AbstractFileTreeGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGPU}
 */
xyz.swapee.wc.AbstractFileTreeGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileTreeGPU|typeof xyz.swapee.wc.FileTreeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IFileTreeDisplay|typeof xyz.swapee.wc.back.FileTreeDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGPU}
 */
xyz.swapee.wc.AbstractFileTreeGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileTreeGPU|typeof xyz.swapee.wc.FileTreeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IFileTreeDisplay|typeof xyz.swapee.wc.back.FileTreeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileTreeGPU}
 */
xyz.swapee.wc.AbstractFileTreeGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileTreeGPU.Initialese[]) => xyz.swapee.wc.IFileTreeGPU} xyz.swapee.wc.FileTreeGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileTreeGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IFileTreeGPUCaster&com.webcircuits.IBrowserView<.!FileTreeMemory,>&xyz.swapee.wc.back.IFileTreeDisplay)} xyz.swapee.wc.IFileTreeGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!FileTreeMemory,>} com.webcircuits.IBrowserView<.!FileTreeMemory,>.typeof */
/**
 * Handles the periphery of the _IFileTreeDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IFileTreeGPU
 */
xyz.swapee.wc.IFileTreeGPU = class extends /** @type {xyz.swapee.wc.IFileTreeGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!FileTreeMemory,>.typeof&xyz.swapee.wc.back.IFileTreeDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileTreeGPU.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileTreeGPU.makeFolderElement} */
xyz.swapee.wc.IFileTreeGPU.prototype.makeFolderElement = function() {}
/** @type {xyz.swapee.wc.IFileTreeGPU.makeFileElement} */
xyz.swapee.wc.IFileTreeGPU.prototype.makeFileElement = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileTreeGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeGPU.Initialese>)} xyz.swapee.wc.FileTreeGPU.constructor */
/**
 * A concrete class of _IFileTreeGPU_ instances.
 * @constructor xyz.swapee.wc.FileTreeGPU
 * @implements {xyz.swapee.wc.IFileTreeGPU} Handles the periphery of the _IFileTreeDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileTreeGPU.Initialese>} ‎
 */
xyz.swapee.wc.FileTreeGPU = class extends /** @type {xyz.swapee.wc.FileTreeGPU.constructor&xyz.swapee.wc.IFileTreeGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileTreeGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileTreeGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileTreeGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileTreeGPU}
 */
xyz.swapee.wc.FileTreeGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreeGPU.
 * @interface xyz.swapee.wc.IFileTreeGPUFields
 */
xyz.swapee.wc.IFileTreeGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IFileTreeGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileTreeGPU} */
xyz.swapee.wc.RecordIFileTreeGPU

/** @typedef {xyz.swapee.wc.IFileTreeGPU} xyz.swapee.wc.BoundIFileTreeGPU */

/** @typedef {xyz.swapee.wc.FileTreeGPU} xyz.swapee.wc.BoundFileTreeGPU */

/**
 * Contains getters to cast the _IFileTreeGPU_ interface.
 * @interface xyz.swapee.wc.IFileTreeGPUCaster
 */
xyz.swapee.wc.IFileTreeGPUCaster = class { }
/**
 * Cast the _IFileTreeGPU_ instance into the _BoundIFileTreeGPU_ type.
 * @type {!xyz.swapee.wc.BoundIFileTreeGPU}
 */
xyz.swapee.wc.IFileTreeGPUCaster.prototype.asIFileTreeGPU
/**
 * Access the _FileTreeGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundFileTreeGPU}
 */
xyz.swapee.wc.IFileTreeGPUCaster.prototype.superFileTreeGPU

/**
 * @typedef {(this: THIS, vid: number) => !HTMLDivElement} xyz.swapee.wc.IFileTreeGPU.__makeFolderElement
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGPU.__makeFolderElement<!xyz.swapee.wc.IFileTreeGPU>} xyz.swapee.wc.IFileTreeGPU._makeFolderElement */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGPU.makeFolderElement} */
/**
 * Creates a new element from the _FolderTemplate_ template.
 * @param {number} vid The VDU id.
 * @return {!HTMLDivElement} A new element created from the template.
 */
xyz.swapee.wc.IFileTreeGPU.makeFolderElement = function(vid) {}

/**
 * @typedef {(this: THIS, vid: number) => !HTMLDivElement} xyz.swapee.wc.IFileTreeGPU.__makeFileElement
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileTreeGPU.__makeFileElement<!xyz.swapee.wc.IFileTreeGPU>} xyz.swapee.wc.IFileTreeGPU._makeFileElement */
/** @typedef {typeof xyz.swapee.wc.IFileTreeGPU.makeFileElement} */
/**
 * Creates a new element from the _FileTemplate_ template.
 * @param {number} vid The VDU id.
 * @return {!HTMLDivElement} A new element created from the template.
 */
xyz.swapee.wc.IFileTreeGPU.makeFileElement = function(vid) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileTreeGPU
/* @typal-end */