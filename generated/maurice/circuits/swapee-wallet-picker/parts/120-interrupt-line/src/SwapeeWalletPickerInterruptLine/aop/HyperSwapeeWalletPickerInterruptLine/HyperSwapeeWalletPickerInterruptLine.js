import AbstractHyperSwapeeWalletPickerInterruptLine from '../../../../gen/AbstractSwapeeWalletPickerInterruptLine/hyper/AbstractHyperSwapeeWalletPickerInterruptLine'
import SwapeeWalletPickerInterruptLine from '../../SwapeeWalletPickerInterruptLine'
import SwapeeWalletPickerInterruptLineGeneralAspects from '../SwapeeWalletPickerInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperSwapeeWalletPickerInterruptLine} */
export default class extends AbstractHyperSwapeeWalletPickerInterruptLine
 .consults(
  SwapeeWalletPickerInterruptLineGeneralAspects,
 )
 .implements(
  SwapeeWalletPickerInterruptLine,
 )
{}