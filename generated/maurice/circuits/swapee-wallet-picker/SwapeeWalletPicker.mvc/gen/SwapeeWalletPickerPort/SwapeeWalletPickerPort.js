import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {SwapeeWalletPickerInputsPQs} from '../../pqs/SwapeeWalletPickerInputsPQs'
import {SwapeeWalletPickerOuterCoreConstructor} from '../SwapeeWalletPickerCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeWalletPickerPort}
 */
function __SwapeeWalletPickerPort() {}
__SwapeeWalletPickerPort.prototype = /** @type {!_SwapeeWalletPickerPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeWalletPickerPort} */ function SwapeeWalletPickerPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.SwapeeWalletPickerOuterCore} */ ({model:null})
  SwapeeWalletPickerOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerPort}
 */
class _SwapeeWalletPickerPort { }
/**
 * The port that serves as an interface to the _ISwapeeWalletPicker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerPort} ‎
 */
export class SwapeeWalletPickerPort extends newAbstract(
 _SwapeeWalletPickerPort,51412537865,SwapeeWalletPickerPortConstructor,{
  asISwapeeWalletPickerPort:1,
  superSwapeeWalletPickerPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerPort} */
SwapeeWalletPickerPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerPort} */
function SwapeeWalletPickerPortClass(){}

export const SwapeeWalletPickerPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ISwapeeWalletPicker.Pinout>}*/({
 get Port() { return SwapeeWalletPickerPort },
})

SwapeeWalletPickerPort[$implementations]=[
 SwapeeWalletPickerPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerPort}*/({
  resetPort(){
   this.resetSwapeeWalletPickerPort()
  },
  resetSwapeeWalletPickerPort(){
   SwapeeWalletPickerPortConstructor.call(this)
  },
 }),
 __SwapeeWalletPickerPort,
 Parametric,
 SwapeeWalletPickerPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerPort}*/({
  constructor(){
   mountPins(this.inputs,SwapeeWalletPickerInputsPQs)
  },
 }),
]


export default SwapeeWalletPickerPort