import AbstractSwapeeWalletPickerDisplay from '../AbstractSwapeeWalletPickerDisplayBack'
import SwapeeWalletPickerClassesPQs from '../../pqs/SwapeeWalletPickerClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {SwapeeWalletPickerClassesQPs} from '../../pqs/SwapeeWalletPickerClassesQPs'
import {SwapeeWalletPickerVdusPQs} from '../../pqs/SwapeeWalletPickerVdusPQs'
import {SwapeeWalletPickerVdusQPs} from '../../pqs/SwapeeWalletPickerVdusQPs'
import {SwapeeWalletPickerMemoryPQs} from '../../pqs/SwapeeWalletPickerMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerGPU}
 */
function __AbstractSwapeeWalletPickerGPU() {}
__AbstractSwapeeWalletPickerGPU.prototype = /** @type {!_AbstractSwapeeWalletPickerGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerGPU}
 */
class _AbstractSwapeeWalletPickerGPU { }
/**
 * Handles the periphery of the _ISwapeeWalletPickerDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerGPU} ‎
 */
class AbstractSwapeeWalletPickerGPU extends newAbstract(
 _AbstractSwapeeWalletPickerGPU,514125378618,null,{
  asISwapeeWalletPickerGPU:1,
  superSwapeeWalletPickerGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGPU} */
AbstractSwapeeWalletPickerGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGPU} */
function AbstractSwapeeWalletPickerGPUClass(){}

export default AbstractSwapeeWalletPickerGPU


AbstractSwapeeWalletPickerGPU[$implementations]=[
 __AbstractSwapeeWalletPickerGPU,
 AbstractSwapeeWalletPickerGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerGPU}*/({
  classesQPs:SwapeeWalletPickerClassesQPs,
  vdusPQs:SwapeeWalletPickerVdusPQs,
  vdusQPs:SwapeeWalletPickerVdusQPs,
  memoryPQs:SwapeeWalletPickerMemoryPQs,
 }),
 AbstractSwapeeWalletPickerDisplay,
 BrowserView,
 AbstractSwapeeWalletPickerGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerGPU}*/({
  allocator(){
   pressFit(this.classes,'',SwapeeWalletPickerClassesPQs)
  },
 }),
]