import {Stator} from '@webcircuits/webcircuits'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerGenerator}
 */
function __AbstractSwapeeWalletPickerGenerator() {}
__AbstractSwapeeWalletPickerGenerator.prototype = /** @type {!_AbstractSwapeeWalletPickerGenerator} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator}
 */
class _AbstractSwapeeWalletPickerGenerator { }
/**
 * Responsible for generation of component instances to place inside containers.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator} ‎
 */
class AbstractSwapeeWalletPickerGenerator extends newAbstract(
 _AbstractSwapeeWalletPickerGenerator,514125378616,null,{
  asISwapeeWalletPickerGenerator:1,
  superSwapeeWalletPickerGenerator:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator} */
AbstractSwapeeWalletPickerGenerator.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator} */
function AbstractSwapeeWalletPickerGeneratorClass(){}

export default AbstractSwapeeWalletPickerGenerator


AbstractSwapeeWalletPickerGenerator[$implementations]=[
 __AbstractSwapeeWalletPickerGenerator,
 AbstractSwapeeWalletPickerGeneratorClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator}*/({
  brushWallet:precombined,
  paintWallet:precombined,
  assignWallet:precombined,
  initWallet:precombined,
 }),
 AbstractSwapeeWalletPickerGeneratorClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator}*/({
  paintWallet({address:address},element){
   element.select('[data-id=WalletAddress]').setText(address)
  },
 }),
 AbstractSwapeeWalletPickerGeneratorClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator}*/({
  paintWallet({address:address},element){
   const{
    asISwapeeWalletPickerHtmlComponent:{
     asIBrowserView:{data:data},
    },
   }=this
   data(element,'address',address)
  },
 }),
 AbstractSwapeeWalletPickerGeneratorClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator}*/({
  brushWallet(brushes,stator){
   const{asISwapeeWalletPickerGenerator:{paintWallet:paintWallet}}=this
   stator.setState(brushes)
   paintWallet(stator.state,stator.element)
  },
  initWallet(stator,el){
   const{asISwapeeWalletPickerGenerator:{assignWallet:assignWallet},land:land}=this
   assignWallet(stator.state,el,land)
  },
  stateWallet(el){
   const stator=new Stator({
    element:el,
    state:{
     address:void 0,
    },
    dynamo:/**@type {!engineering.type.seers.ISeen}*/(this),
   })

   return stator
  },
 }),
]

export {AbstractSwapeeWalletPickerGenerator}