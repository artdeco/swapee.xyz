import AbstractSwapeeWalletPickerProcessor from '../AbstractSwapeeWalletPickerProcessor'
import {AbstractSwapeeWalletPickerCPU} from '../AbstractSwapeeWalletPickerCPU'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractSwapeeWalletPickerComputer} from '../AbstractSwapeeWalletPickerComputer'
import {AbstractSwapeeWalletPickerController} from '../AbstractSwapeeWalletPickerController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPicker}
 */
function __AbstractSwapeeWalletPicker() {}
__AbstractSwapeeWalletPicker.prototype = /** @type {!_AbstractSwapeeWalletPicker} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPicker}
 */
class _AbstractSwapeeWalletPicker { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPicker} ‎
 */
class AbstractSwapeeWalletPicker extends newAbstract(
 _AbstractSwapeeWalletPicker,51412537869,null,{
  asISwapeeWalletPicker:1,
  superSwapeeWalletPicker:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPicker} */
AbstractSwapeeWalletPicker.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPicker} */
function AbstractSwapeeWalletPickerClass(){}

export default AbstractSwapeeWalletPicker


AbstractSwapeeWalletPicker[$implementations]=[
 __AbstractSwapeeWalletPicker,
 AbstractSwapeeWalletPickerCPU,
 AbstractSwapeeWalletPickerProcessor,
 IntegratedComponent,
 AbstractSwapeeWalletPickerComputer,
 AbstractSwapeeWalletPickerController,
]


export {AbstractSwapeeWalletPicker}