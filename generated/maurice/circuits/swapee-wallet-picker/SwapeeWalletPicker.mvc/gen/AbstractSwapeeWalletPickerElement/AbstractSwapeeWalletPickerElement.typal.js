
import AbstractSwapeeWalletPicker from '../AbstractSwapeeWalletPicker'

/** @abstract {xyz.swapee.wc.ISwapeeWalletPickerElement} */
export default class AbstractSwapeeWalletPickerElement { }



AbstractSwapeeWalletPickerElement[$implementations]=[AbstractSwapeeWalletPicker,
 /** @type {!AbstractSwapeeWalletPickerElement} */ ({
  rootId:'SwapeeWalletPicker',
  __$id:5141253786,
  fqn:'xyz.swapee.wc.ISwapeeWalletPicker',
  maurice_element_v3:true,
 }),
]