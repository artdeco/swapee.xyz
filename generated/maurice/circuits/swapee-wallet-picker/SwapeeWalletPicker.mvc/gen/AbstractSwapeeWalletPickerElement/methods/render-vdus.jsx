export default function SwapeeWalletPickerRenderVdus(){
 return (<div $id="SwapeeWalletPicker">
  <vdu $template="WalletTemplate" />
  <vdu $id="Wallets" />
  <vdu $id="CreateInvoiceBu" />
  <vdu $id="InvoiceLoIn" />
  <vdu $id="SwapeeAccountWr" />
  <vdu $id="GetWalletBu" />
  <vdu $id="SwapeeLoginBu" />
 </div>)
}