import SwapeeWalletPickerRenderVdus from './methods/render-vdus'
import SwapeeWalletPickerElementPort from '../SwapeeWalletPickerElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {SwapeeWalletPickerInputsPQs} from '../../pqs/SwapeeWalletPickerInputsPQs'
import {SwapeeWalletPickerQueriesPQs} from '../../pqs/SwapeeWalletPickerQueriesPQs'
import { newAbstract, $implementations, create } from '@type.engineering/type-engineer'
import AbstractSwapeeWalletPicker from '../AbstractSwapeeWalletPicker'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerElement}
 */
function __AbstractSwapeeWalletPickerElement() {}
__AbstractSwapeeWalletPickerElement.prototype = /** @type {!_AbstractSwapeeWalletPickerElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerElement}
 */
class _AbstractSwapeeWalletPickerElement { }
/**
 * A component description.
 *
 * The _ISwapeeWalletPicker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerElement} ‎
 */
class AbstractSwapeeWalletPickerElement extends newAbstract(
 _AbstractSwapeeWalletPickerElement,514125378612,null,{
  asISwapeeWalletPickerElement:1,
  superSwapeeWalletPickerElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElement} */
AbstractSwapeeWalletPickerElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElement} */
function AbstractSwapeeWalletPickerElementClass(){}

export default AbstractSwapeeWalletPickerElement


AbstractSwapeeWalletPickerElement[$implementations]=[
 __AbstractSwapeeWalletPickerElement,
 ElementBase,
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':is-signed-in':isSignedInColAttr,
   ':wallets':walletsColAttr,
   ':invoice':invoiceColAttr,
   ':load-create-invoice':loadCreateInvoiceColAttr,
   ':wallets-rotor':walletsRotorColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'is-signed-in':isSignedInAttr,
    'wallets':walletsAttr,
    'invoice':invoiceAttr,
    'load-create-invoice':loadCreateInvoiceAttr,
    'wallets-rotor':walletsRotorAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(isSignedInAttr===undefined?{'is-signed-in':isSignedInColAttr}:{}),
    ...(walletsAttr===undefined?{'wallets':walletsColAttr}:{}),
    ...(invoiceAttr===undefined?{'invoice':invoiceColAttr}:{}),
    ...(loadCreateInvoiceAttr===undefined?{'load-create-invoice':loadCreateInvoiceColAttr}:{}),
    ...(walletsRotorAttr===undefined?{'wallets-rotor':walletsRotorColAttr}:{}),
   }
  },
 }),
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'is-signed-in':isSignedInAttr,
   'wallets':walletsAttr,
   'invoice':invoiceAttr,
   'load-create-invoice':loadCreateInvoiceAttr,
   'wallets-rotor':walletsRotorAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    isSignedIn:isSignedInAttr,
    wallets:walletsAttr,
    invoice:invoiceAttr,
    loadCreateInvoice:loadCreateInvoiceAttr,
    walletsRotor:walletsRotorAttr,
   }
  },
 }),
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 Landed,
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.ISwapeeWalletPickerElement}*/({land:{
    SwapeeMe:null,
   }}))
  },
  render:function renderSwapeeMe(){
   const{
    asILanded:{
     land:{
      SwapeeMe:SwapeeMe,
     },
    },
    asISwapeeWalletPickerElement:{
     buildSwapeeMe:buildSwapeeMe,
    },
   }=this
   if(!SwapeeMe) return
   const{model:SwapeeMeModel}=SwapeeMe
   const{
    '94a08':token,
   }=SwapeeMeModel
   const res=buildSwapeeMe({
    token:token,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  render:SwapeeWalletPickerRenderVdus,
 }),
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  classes:{
   'Class': '9bd81',
  },
  inputsPQs:SwapeeWalletPickerInputsPQs,
  queriesPQs:SwapeeWalletPickerQueriesPQs,
  vdus:{
   'WalletTemplate': 'a04f1',
   'Wallets': 'a04f2',
   'SwapeeAccountWr': 'a04f3',
   'GetWalletBu': 'a04f4',
   'SwapeeLoginBu': 'a04f5',
   'SwapeeMe': 'a04f6',
   'CreateInvoiceBu': 'a04f7',
   'InvoiceLoIn': 'a04f8',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','isSignedIn','wallets','invoice','loadCreateInvoice','walletsRotor','query:swapee-me','no-solder',':no-solder','is-signed-in',':is-signed-in',':wallets',':invoice','load-create-invoice',':load-create-invoice','wallets-rotor',':wallets-rotor','fe646','046c0','60ed0','7721a','b0f6c','1f611','98555','4ccfe','58b0a','c60d7','90b43','e5f96','824a4','e825f','children']),
   })
  },
  get Port(){
   return SwapeeWalletPickerElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:swapee-me':swapeeMeSel}){
   const _ret={}
   if(swapeeMeSel) _ret.swapeeMeSel=swapeeMeSel
   return _ret
  },
 }),
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  constructor(){
   this.land={
    SwapeeMe:null,
   }
  },
 }),
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  calibrate:async function awaitOnSwapeeMe({swapeeMeSel:swapeeMeSel}){
   if(!swapeeMeSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const SwapeeMe=await milleu(swapeeMeSel)
   if(!SwapeeMe) {
    console.warn('❗️ swapeeMeSel %s must be present on the page for %s to work',swapeeMeSel,fqn)
    return{}
   }
   land.SwapeeMe=SwapeeMe
  },
 }),
 AbstractSwapeeWalletPickerElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
  solder:(_,{
   swapeeMeSel:swapeeMeSel,
  })=>{
   return{
    swapeeMeSel:swapeeMeSel,
   }
  },
 }),
]



AbstractSwapeeWalletPickerElement[$implementations]=[AbstractSwapeeWalletPicker,
 /** @type {!AbstractSwapeeWalletPickerElement} */ ({
  rootId:'SwapeeWalletPicker',
  __$id:5141253786,
  fqn:'xyz.swapee.wc.ISwapeeWalletPicker',
  maurice_element_v3:true,
 }),
]