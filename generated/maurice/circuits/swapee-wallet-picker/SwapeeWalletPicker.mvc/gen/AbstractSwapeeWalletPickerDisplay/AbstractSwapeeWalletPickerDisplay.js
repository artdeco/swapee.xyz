import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerDisplay}
 */
function __AbstractSwapeeWalletPickerDisplay() {}
__AbstractSwapeeWalletPickerDisplay.prototype = /** @type {!_AbstractSwapeeWalletPickerDisplay} */ ({ })
/** @this {xyz.swapee.wc.SwapeeWalletPickerDisplay} */ function SwapeeWalletPickerDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.Wallets=null
  /** @type {HTMLDivElement} */ this.SwapeeAccountWr=null
  /** @type {HTMLButtonElement} */ this.GetWalletBu=null
  /** @type {HTMLButtonElement} */ this.CreateInvoiceBu=null
  /** @type {HTMLButtonElement} */ this.SwapeeLoginBu=null
  /** @type {HTMLDivElement} */ this.InvoiceLoIn=null
  /** @type {HTMLElement} */ this.SwapeeMe=null
  /** @type {HTMLDivElement} */ this.WalletTemplate=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay}
 */
class _AbstractSwapeeWalletPickerDisplay { }
/**
 * Display for presenting information from the _ISwapeeWalletPicker_.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay} ‎
 */
class AbstractSwapeeWalletPickerDisplay extends newAbstract(
 _AbstractSwapeeWalletPickerDisplay,514125378619,SwapeeWalletPickerDisplayConstructor,{
  asISwapeeWalletPickerDisplay:1,
  superSwapeeWalletPickerDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay} */
AbstractSwapeeWalletPickerDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay} */
function AbstractSwapeeWalletPickerDisplayClass(){}

export default AbstractSwapeeWalletPickerDisplay


AbstractSwapeeWalletPickerDisplay[$implementations]=[
 __AbstractSwapeeWalletPickerDisplay,
 Display,
 AbstractSwapeeWalletPickerDisplayClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{swapeeMeScopeSel:swapeeMeScopeSel}}=this
    this.scan({
     swapeeMeSel:swapeeMeScopeSel,
    })
   })
  },
  scan:function vduScan({swapeeMeSel:swapeeMeSelScope}){
   const{element:element,asISwapeeWalletPickerScreen:{vdusPQs:{
    WalletTemplate:WalletTemplate,
    Wallets:Wallets,
    CreateInvoiceBu:CreateInvoiceBu,
    InvoiceLoIn:InvoiceLoIn,
    SwapeeAccountWr:SwapeeAccountWr,
    GetWalletBu:GetWalletBu,
    SwapeeLoginBu:SwapeeLoginBu,
   }},queries:{swapeeMeSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const SwapeeMe=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _SwapeeMe=swapeeMeSel?root.querySelector(swapeeMeSel):void 0
    return _SwapeeMe
   })(swapeeMeSelScope)
   Object.assign(this,{
    WalletTemplate:/**@type {HTMLDivElement}*/(children[WalletTemplate]),
    Wallets:/**@type {HTMLDivElement}*/(children[Wallets]),
    CreateInvoiceBu:/**@type {HTMLButtonElement}*/(children[CreateInvoiceBu]),
    InvoiceLoIn:/**@type {HTMLDivElement}*/(children[InvoiceLoIn]),
    SwapeeAccountWr:/**@type {HTMLDivElement}*/(children[SwapeeAccountWr]),
    GetWalletBu:/**@type {HTMLButtonElement}*/(children[GetWalletBu]),
    SwapeeLoginBu:/**@type {HTMLButtonElement}*/(children[SwapeeLoginBu]),
    SwapeeMe:SwapeeMe,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.SwapeeWalletPickerDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese}*/({
   Wallets:1,
   SwapeeAccountWr:1,
   GetWalletBu:1,
   CreateInvoiceBu:1,
   SwapeeLoginBu:1,
   InvoiceLoIn:1,
   SwapeeMe:1,
   WalletTemplate:1,
  }),
  initializer({
   Wallets:_Wallets,
   SwapeeAccountWr:_SwapeeAccountWr,
   GetWalletBu:_GetWalletBu,
   CreateInvoiceBu:_CreateInvoiceBu,
   SwapeeLoginBu:_SwapeeLoginBu,
   InvoiceLoIn:_InvoiceLoIn,
   SwapeeMe:_SwapeeMe,
   WalletTemplate:_WalletTemplate,
  }) {
   if(_Wallets!==undefined) this.Wallets=_Wallets
   if(_SwapeeAccountWr!==undefined) this.SwapeeAccountWr=_SwapeeAccountWr
   if(_GetWalletBu!==undefined) this.GetWalletBu=_GetWalletBu
   if(_CreateInvoiceBu!==undefined) this.CreateInvoiceBu=_CreateInvoiceBu
   if(_SwapeeLoginBu!==undefined) this.SwapeeLoginBu=_SwapeeLoginBu
   if(_InvoiceLoIn!==undefined) this.InvoiceLoIn=_InvoiceLoIn
   if(_SwapeeMe!==undefined) this.SwapeeMe=_SwapeeMe
   if(_WalletTemplate!==undefined) this.WalletTemplate=_WalletTemplate
  },
 }),
]