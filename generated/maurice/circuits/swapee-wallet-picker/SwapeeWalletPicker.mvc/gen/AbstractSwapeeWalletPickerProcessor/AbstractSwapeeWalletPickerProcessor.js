import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerProcessor}
 */
function __AbstractSwapeeWalletPickerProcessor() {}
__AbstractSwapeeWalletPickerProcessor.prototype = /** @type {!_AbstractSwapeeWalletPickerProcessor} */ ({
  /** @return {void} */
  pulseLoadCreateInvoice() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     loadCreateInvoice:true,
    })
  },
})
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor}
 */
class _AbstractSwapeeWalletPickerProcessor { }
/**
 * The processor to compute changes to the memory for the _ISwapeeWalletPicker_.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor} ‎
 */
class AbstractSwapeeWalletPickerProcessor extends newAbstract(
 _AbstractSwapeeWalletPickerProcessor,51412537868,null,{
  asISwapeeWalletPickerProcessor:1,
  superSwapeeWalletPickerProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor} */
AbstractSwapeeWalletPickerProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor} */
function AbstractSwapeeWalletPickerProcessorClass(){}

export default AbstractSwapeeWalletPickerProcessor


AbstractSwapeeWalletPickerProcessor[$implementations]=[
 __AbstractSwapeeWalletPickerProcessor,
]