import AbstractSwapeeWalletPickerGPU from '../AbstractSwapeeWalletPickerGPU'
import AbstractSwapeeWalletPickerScreenBack from '../AbstractSwapeeWalletPickerScreenBack'
import {HtmlComponent,Landed,rotate,makeRevealConcealPaints,mvc} from '@webcircuits/webcircuits'
import {SwapeeWalletPickerInputsQPs} from '../../pqs/SwapeeWalletPickerInputsQPs'
import { newAbstract, $implementations, create, schedule } from '@type.engineering/type-engineer'
import AbstractSwapeeWalletPicker from '../AbstractSwapeeWalletPicker'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerHtmlComponent}
 */
function __AbstractSwapeeWalletPickerHtmlComponent() {}
__AbstractSwapeeWalletPickerHtmlComponent.prototype = /** @type {!_AbstractSwapeeWalletPickerHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent}
 */
class _AbstractSwapeeWalletPickerHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.SwapeeWalletPickerHtmlComponent} */ (res)
  }
}
/**
 * The _ISwapeeWalletPicker_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent} ‎
 */
export class AbstractSwapeeWalletPickerHtmlComponent extends newAbstract(
 _AbstractSwapeeWalletPickerHtmlComponent,514125378611,null,{
  asISwapeeWalletPickerHtmlComponent:1,
  superSwapeeWalletPickerHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent} */
AbstractSwapeeWalletPickerHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent} */
function AbstractSwapeeWalletPickerHtmlComponentClass(){}


AbstractSwapeeWalletPickerHtmlComponent[$implementations]=[
 __AbstractSwapeeWalletPickerHtmlComponent,
 HtmlComponent,
 AbstractSwapeeWalletPicker,
 AbstractSwapeeWalletPickerGPU,
 AbstractSwapeeWalletPickerScreenBack,
 Landed,
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  constructor(){
   this.land={
    SwapeeMe:null,
   }
  },
 }),
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  inputsQPs:SwapeeWalletPickerInputsQPs,
 }),

/** @type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent} */ ({paint:
  /**@this {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/
  function paintWalletDynamo({walletsRotor:walletsRotor}){
   const{
    asISwapeeWalletPickerGPU:{
     Wallets:Wallets,
     makeWalletElement:makeWalletElement,
    },
    asIGraphicsDriverBack:{makeVdu:makeVdu},
    asISwapeeWalletPicker:{
     stateWallet:stateWallet,brushWallet:brushWallet,initWallet:initWallet,
    },
   }=this
   rotate(Wallets,walletsRotor,{
    create:stateWallet,
    make:()=>{
     const twin=makeVdu()
     makeWalletElement(twin.id)
     return twin
    },
    brush:brushWallet,
    init:initWallet,
    strategy:'reuse',
   })
  },
  }),
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  paint:function paint_disabled_on_CreateInvoiceBu({loading:loading}){
   const{asISwapeeWalletPickerGPU:{CreateInvoiceBu:CreateInvoiceBu},asIBrowserView:{attr:attr}}=this
   attr(CreateInvoiceBu,'disabled',(loading)||null)
  },
 }),
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  paint:makeRevealConcealPaints({
   InvoiceLoIn:{loading:1},
  }),
 }),
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  paint:function interrupt_click_on_SwapeeLoginBu(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   const{asISwapeeWalletPickerGPU:{SwapeeLoginBu:SwapeeLoginBu}}=this
   SwapeeLoginBu.listen('click',(ev)=>{
    SwapeeMe['2cef1']()
   })
  },
 }),
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  paint:function $conceal_SwapeeAccountWr(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '94a08':token,
   }=SwapeeMe.model
   const{
    asISwapeeWalletPickerGPU:{SwapeeAccountWr:SwapeeAccountWr},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(SwapeeAccountWr,token)
  },
 }),
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  paint:function $reveal_CreateInvoiceBu(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '94a08':token,
   }=SwapeeMe.model
   const{
    asISwapeeWalletPickerGPU:{CreateInvoiceBu:CreateInvoiceBu},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(CreateInvoiceBu,token)
  },
 }),
 AbstractSwapeeWalletPickerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asISwapeeWalletPickerGPU:{
     SwapeeMe:SwapeeMe,
    },
   }=this
   complete(6200449439,{SwapeeMe:SwapeeMe})
  },
 }),
]