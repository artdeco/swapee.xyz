import AbstractSwapeeWalletPickerControllerAR from '../AbstractSwapeeWalletPickerControllerAR'
import {AbstractSwapeeWalletPickerController} from '../AbstractSwapeeWalletPickerController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerControllerBack}
 */
function __AbstractSwapeeWalletPickerControllerBack() {}
__AbstractSwapeeWalletPickerControllerBack.prototype = /** @type {!_AbstractSwapeeWalletPickerControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerController}
 */
class _AbstractSwapeeWalletPickerControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerController} ‎
 */
class AbstractSwapeeWalletPickerControllerBack extends newAbstract(
 _AbstractSwapeeWalletPickerControllerBack,514125378624,null,{
  asISwapeeWalletPickerController:1,
  superSwapeeWalletPickerController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerController} */
AbstractSwapeeWalletPickerControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerController} */
function AbstractSwapeeWalletPickerControllerBackClass(){}

export default AbstractSwapeeWalletPickerControllerBack


AbstractSwapeeWalletPickerControllerBack[$implementations]=[
 __AbstractSwapeeWalletPickerControllerBack,
 AbstractSwapeeWalletPickerController,
 AbstractSwapeeWalletPickerControllerAR,
 DriverBack,
]