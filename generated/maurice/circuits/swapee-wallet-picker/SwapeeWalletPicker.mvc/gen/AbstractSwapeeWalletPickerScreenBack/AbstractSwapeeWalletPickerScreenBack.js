import AbstractSwapeeWalletPickerScreenAT from '../AbstractSwapeeWalletPickerScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerScreenBack}
 */
function __AbstractSwapeeWalletPickerScreenBack() {}
__AbstractSwapeeWalletPickerScreenBack.prototype = /** @type {!_AbstractSwapeeWalletPickerScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen}
 */
class _AbstractSwapeeWalletPickerScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen} ‎
 */
class AbstractSwapeeWalletPickerScreenBack extends newAbstract(
 _AbstractSwapeeWalletPickerScreenBack,514125378628,null,{
  asISwapeeWalletPickerScreen:1,
  superSwapeeWalletPickerScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen} */
AbstractSwapeeWalletPickerScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen} */
function AbstractSwapeeWalletPickerScreenBackClass(){}

export default AbstractSwapeeWalletPickerScreenBack


AbstractSwapeeWalletPickerScreenBack[$implementations]=[
 __AbstractSwapeeWalletPickerScreenBack,
 AbstractSwapeeWalletPickerScreenAT,
]