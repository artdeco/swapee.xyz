import {makeBuffers} from '@webcircuits/webcircuits'

export const SwapeeWalletPickerBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  isSignedIn:Boolean,
  wallets:[String],
  invoice:String,
  loadCreateInvoice:Boolean,
  walletsRotor:Array,
 }),
})

export default SwapeeWalletPickerBuffer