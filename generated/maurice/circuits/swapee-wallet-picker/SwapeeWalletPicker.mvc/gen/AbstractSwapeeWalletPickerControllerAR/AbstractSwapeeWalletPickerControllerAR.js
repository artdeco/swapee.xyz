import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerControllerAR}
 */
function __AbstractSwapeeWalletPickerControllerAR() {}
__AbstractSwapeeWalletPickerControllerAR.prototype = /** @type {!_AbstractSwapeeWalletPickerControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR}
 */
class _AbstractSwapeeWalletPickerControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeWalletPickerControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR} ‎
 */
class AbstractSwapeeWalletPickerControllerAR extends newAbstract(
 _AbstractSwapeeWalletPickerControllerAR,514125378625,null,{
  asISwapeeWalletPickerControllerAR:1,
  superSwapeeWalletPickerControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR} */
AbstractSwapeeWalletPickerControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR} */
function AbstractSwapeeWalletPickerControllerARClass(){}

export default AbstractSwapeeWalletPickerControllerAR


AbstractSwapeeWalletPickerControllerAR[$implementations]=[
 __AbstractSwapeeWalletPickerControllerAR,
 AR,
 AbstractSwapeeWalletPickerControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR}*/({
  allocator(){
   this.methods={
    pulseLoadCreateInvoice:'83537',
   }
  },
 }),
]