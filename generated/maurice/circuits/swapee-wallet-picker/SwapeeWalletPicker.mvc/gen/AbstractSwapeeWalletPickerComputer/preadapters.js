
/**@this {xyz.swapee.wc.ISwapeeWalletPickerComputer}*/
export function preadaptWalletsRotor(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Form}*/
 const _inputs={
  wallets:inputs.wallets,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptWalletsRotor(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ISwapeeWalletPickerComputer}*/
export function preadaptLoadBitcoinAddress(inputs,changes,mapForm) {
 const{SwapeeMe:SwapeeMe}=this.land
 if(!SwapeeMe) return
 /**@type {!xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Form}*/
 const _inputs={
  loadCreateInvoice:inputs.loadCreateInvoice,
  swapeeHost:SwapeeMe.model['ab70c'],
  token:SwapeeMe.model['94a08'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.loadCreateInvoice)||(!__inputs.swapeeHost)||(!__inputs.token)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadBitcoinAddress(__inputs,__changes)
 return RET
}