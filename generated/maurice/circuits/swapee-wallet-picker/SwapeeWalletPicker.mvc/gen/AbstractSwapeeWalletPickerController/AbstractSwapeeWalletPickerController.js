import SwapeeWalletPickerBuffer from '../SwapeeWalletPickerBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {SwapeeWalletPickerPortConnector} from '../SwapeeWalletPickerPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerController}
 */
function __AbstractSwapeeWalletPickerController() {}
__AbstractSwapeeWalletPickerController.prototype = /** @type {!_AbstractSwapeeWalletPickerController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerController}
 */
class _AbstractSwapeeWalletPickerController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerController} ‎
 */
export class AbstractSwapeeWalletPickerController extends newAbstract(
 _AbstractSwapeeWalletPickerController,514125378622,null,{
  asISwapeeWalletPickerController:1,
  superSwapeeWalletPickerController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerController} */
AbstractSwapeeWalletPickerController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerController} */
function AbstractSwapeeWalletPickerControllerClass(){}


AbstractSwapeeWalletPickerController[$implementations]=[
 AbstractSwapeeWalletPickerControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.ISwapeeWalletPickerPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractSwapeeWalletPickerController,
 /**@type {!xyz.swapee.wc.ISwapeeWalletPickerController}*/({
  calibrate: /**@this {!xyz.swapee.wc.ISwapeeWalletPickerController}*/ function calibrateLoadCreateInvoice({loadCreateInvoice:loadCreateInvoice}){
   if(!loadCreateInvoice) return
   setTimeout(()=>{
    const{asISwapeeWalletPickerController:{setInputs:setInputs}}=this
    setInputs({
     loadCreateInvoice:false,
    })
   },1)
  },
 }),
 SwapeeWalletPickerBuffer,
 IntegratedController,
 /**@type {!AbstractSwapeeWalletPickerController}*/(SwapeeWalletPickerPortConnector),
]


export default AbstractSwapeeWalletPickerController