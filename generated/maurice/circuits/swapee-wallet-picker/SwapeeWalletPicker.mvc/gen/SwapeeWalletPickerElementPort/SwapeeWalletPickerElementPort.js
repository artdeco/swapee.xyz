import { create, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeWalletPickerElementPort}
 */
function __SwapeeWalletPickerElementPort() {}
__SwapeeWalletPickerElementPort.prototype = /** @type {!_SwapeeWalletPickerElementPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeWalletPickerElementPort} */ function SwapeeWalletPickerElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    walletsOpts: {},
    swapeeAccountWrOpts: {},
    getWalletBuOpts: {},
    createInvoiceBuOpts: {},
    swapeeLoginBuOpts: {},
    invoiceLoInOpts: {},
    swapeeMeOpts: {},
    walletTemplateOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort}
 */
class _SwapeeWalletPickerElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort} ‎
 */
class SwapeeWalletPickerElementPort extends newAbstract(
 _SwapeeWalletPickerElementPort,514125378613,SwapeeWalletPickerElementPortConstructor,{
  asISwapeeWalletPickerElementPort:1,
  superSwapeeWalletPickerElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort} */
SwapeeWalletPickerElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort} */
function SwapeeWalletPickerElementPortClass(){}

export default SwapeeWalletPickerElementPort


SwapeeWalletPickerElementPort[$implementations]=[
 __SwapeeWalletPickerElementPort,
 SwapeeWalletPickerElementPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'wallets-opts':undefined,
    'swapee-account-wr-opts':undefined,
    'get-wallet-bu-opts':undefined,
    'create-invoice-bu-opts':undefined,
    'swapee-login-bu-opts':undefined,
    'invoice-lo-in-opts':undefined,
    'swapee-me-opts':undefined,
    'wallet-template-opts':undefined,
    'is-signed-in':undefined,
    'load-create-invoice':undefined,
    'wallets-rotor':undefined,
   })
  },
 }),
]