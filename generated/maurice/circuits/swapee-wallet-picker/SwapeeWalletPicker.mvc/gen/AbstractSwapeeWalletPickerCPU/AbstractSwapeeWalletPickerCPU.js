import {StatefulLoadable} from '@mauriceguest/guest2'
import {SwapeeWalletPickerCore} from '../SwapeeWalletPickerCore'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerCPU}
 */
function __AbstractSwapeeWalletPickerCPU() {}
__AbstractSwapeeWalletPickerCPU.prototype = /** @type {!_AbstractSwapeeWalletPickerCPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU}
 */
class _AbstractSwapeeWalletPickerCPU { }
/**
 * The _ISwapeeWalletPicker_'s CPU that encapsulates the component's native atomic
 * core, as well as additional embedded cores.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU} ‎
 */
export class AbstractSwapeeWalletPickerCPU extends newAbstract(
 _AbstractSwapeeWalletPickerCPU,514125378634,null,{
  asISwapeeWalletPickerCPU:1,
  superSwapeeWalletPickerCPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCPU} */
AbstractSwapeeWalletPickerCPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCPU} */
function AbstractSwapeeWalletPickerCPUClass(){}


AbstractSwapeeWalletPickerCPU[$implementations]=[
 __AbstractSwapeeWalletPickerCPU,
 AbstractSwapeeWalletPickerCPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerCPU}*/({
  resetCore:precombined,
 }),
 StatefulLoadable,
 SwapeeWalletPickerCore,
]


export default AbstractSwapeeWalletPickerCPU