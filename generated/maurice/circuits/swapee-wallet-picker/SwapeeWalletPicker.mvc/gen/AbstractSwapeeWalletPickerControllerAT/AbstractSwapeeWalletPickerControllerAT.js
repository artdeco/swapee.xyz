import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerControllerAT}
 */
function __AbstractSwapeeWalletPickerControllerAT() {}
__AbstractSwapeeWalletPickerControllerAT.prototype = /** @type {!_AbstractSwapeeWalletPickerControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT}
 */
class _AbstractSwapeeWalletPickerControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeWalletPickerControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT} ‎
 */
class AbstractSwapeeWalletPickerControllerAT extends newAbstract(
 _AbstractSwapeeWalletPickerControllerAT,514125378626,null,{
  asISwapeeWalletPickerControllerAT:1,
  superSwapeeWalletPickerControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT} */
AbstractSwapeeWalletPickerControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT} */
function AbstractSwapeeWalletPickerControllerATClass(){}

export default AbstractSwapeeWalletPickerControllerAT


AbstractSwapeeWalletPickerControllerAT[$implementations]=[
 __AbstractSwapeeWalletPickerControllerAT,
 UartUniversal,
 AbstractSwapeeWalletPickerControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT}*/({
  get asISwapeeWalletPickerController(){
   return this
  },
  pulseLoadCreateInvoice(){
   this.uart.t("inv",{mid:'83537'})
  },
 }),
]