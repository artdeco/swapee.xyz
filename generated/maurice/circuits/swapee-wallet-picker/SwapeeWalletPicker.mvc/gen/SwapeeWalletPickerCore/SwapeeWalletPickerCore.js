import {mountPins} from '@type.engineering/seers'
import {SwapeeWalletPickerMemoryPQs} from '../../pqs/SwapeeWalletPickerMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeWalletPickerCore}
 */
function __SwapeeWalletPickerCore() {}
__SwapeeWalletPickerCore.prototype = /** @type {!_SwapeeWalletPickerCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCore}
 */
class _SwapeeWalletPickerCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCore} ‎
 */
class SwapeeWalletPickerCore extends newAbstract(
 _SwapeeWalletPickerCore,51412537867,null,{
  asISwapeeWalletPickerCore:1,
  superSwapeeWalletPickerCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCore} */
SwapeeWalletPickerCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCore} */
function SwapeeWalletPickerCoreClass(){}

export default SwapeeWalletPickerCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeWalletPickerOuterCore}
 */
function __SwapeeWalletPickerOuterCore() {}
__SwapeeWalletPickerOuterCore.prototype = /** @type {!_SwapeeWalletPickerOuterCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeWalletPickerOuterCore} */
export function SwapeeWalletPickerOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model}*/
  this.model={
    isSignedIn: false,
    wallets: [],
    invoice: '',
    loadCreateInvoice: false,
    walletsRotor: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore}
 */
class _SwapeeWalletPickerOuterCore { }
/**
 * The _ISwapeeWalletPicker_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore} ‎
 */
export class SwapeeWalletPickerOuterCore extends newAbstract(
 _SwapeeWalletPickerOuterCore,51412537863,SwapeeWalletPickerOuterCoreConstructor,{
  asISwapeeWalletPickerOuterCore:1,
  superSwapeeWalletPickerOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore} */
SwapeeWalletPickerOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore} */
function SwapeeWalletPickerOuterCoreClass(){}


SwapeeWalletPickerOuterCore[$implementations]=[
 __SwapeeWalletPickerOuterCore,
 SwapeeWalletPickerOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerOuterCore}*/({
  constructor(){
   mountPins(this.model,SwapeeWalletPickerMemoryPQs)

  },
 }),
]

SwapeeWalletPickerCore[$implementations]=[
 SwapeeWalletPickerCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerCore}*/({
  resetCore(){
   this.resetSwapeeWalletPickerCore()
  },
  resetSwapeeWalletPickerCore(){
   SwapeeWalletPickerOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.SwapeeWalletPickerOuterCore}*/(
     /**@type {!xyz.swapee.wc.ISwapeeWalletPickerOuterCore}*/(this)),
   )
  },
 }),
 __SwapeeWalletPickerCore,
 SwapeeWalletPickerOuterCore,
]

export {SwapeeWalletPickerCore}