import SwapeeWalletPickerClassesPQs from '../../pqs/SwapeeWalletPickerClassesPQs'
import AbstractSwapeeWalletPickerScreenAR from '../AbstractSwapeeWalletPickerScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {SwapeeWalletPickerInputsPQs} from '../../pqs/SwapeeWalletPickerInputsPQs'
import {SwapeeWalletPickerQueriesPQs} from '../../pqs/SwapeeWalletPickerQueriesPQs'
import {SwapeeWalletPickerMemoryQPs} from '../../pqs/SwapeeWalletPickerMemoryQPs'
import {SwapeeWalletPickerVdusPQs} from '../../pqs/SwapeeWalletPickerVdusPQs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerScreen}
 */
function __AbstractSwapeeWalletPickerScreen() {}
__AbstractSwapeeWalletPickerScreen.prototype = /** @type {!_AbstractSwapeeWalletPickerScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerScreen}
 */
class _AbstractSwapeeWalletPickerScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerScreen} ‎
 */
class AbstractSwapeeWalletPickerScreen extends newAbstract(
 _AbstractSwapeeWalletPickerScreen,514125378627,null,{
  asISwapeeWalletPickerScreen:1,
  superSwapeeWalletPickerScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerScreen} */
AbstractSwapeeWalletPickerScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerScreen} */
function AbstractSwapeeWalletPickerScreenClass(){}

export default AbstractSwapeeWalletPickerScreen


AbstractSwapeeWalletPickerScreen[$implementations]=[
 AbstractSwapeeWalletPickerScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerScreen}*/({
  makeWalletElement(vid){
   const{
    WalletTemplate:WalletTemplate,
    asIScreen:{makeElement:makeElement},
   }=this
   const el=/**@type {!HTMLDivElement}*/(makeElement(WalletTemplate,vid))
   return el
  },
 }),
 __AbstractSwapeeWalletPickerScreen,
 AbstractSwapeeWalletPickerScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerScreen}*/({
  inputsPQs:SwapeeWalletPickerInputsPQs,
  classesPQs:SwapeeWalletPickerClassesPQs,
  queriesPQs:SwapeeWalletPickerQueriesPQs,
  memoryQPs:SwapeeWalletPickerMemoryQPs,
 }),
 Screen,
 AbstractSwapeeWalletPickerScreenAR,
 AbstractSwapeeWalletPickerScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerScreen}*/({
  vdusPQs:SwapeeWalletPickerVdusPQs,
 }),
 AbstractSwapeeWalletPickerScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerScreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const CreateInvoiceBu=this.CreateInvoiceBu
    if(CreateInvoiceBu){
     CreateInvoiceBu.addEventListener('click',(ev)=>{
     ev.preventDefault()
      this.pulseLoadCreateInvoice()
     return false
     })
    }
   })
  },
 }),
]