import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerScreenAT}
 */
function __AbstractSwapeeWalletPickerScreenAT() {}
__AbstractSwapeeWalletPickerScreenAT.prototype = /** @type {!_AbstractSwapeeWalletPickerScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT}
 */
class _AbstractSwapeeWalletPickerScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeWalletPickerScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT} ‎
 */
class AbstractSwapeeWalletPickerScreenAT extends newAbstract(
 _AbstractSwapeeWalletPickerScreenAT,514125378630,null,{
  asISwapeeWalletPickerScreenAT:1,
  superSwapeeWalletPickerScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT} */
AbstractSwapeeWalletPickerScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT} */
function AbstractSwapeeWalletPickerScreenATClass(){}

export default AbstractSwapeeWalletPickerScreenAT


AbstractSwapeeWalletPickerScreenAT[$implementations]=[
 __AbstractSwapeeWalletPickerScreenAT,
 UartUniversal,
 AbstractSwapeeWalletPickerScreenATClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT}*/({
  makeWalletElement(){
   const{asISwapeeWalletPickerScreenAT:{uart:uart}}=this
   uart.t('inv',{mid:'86307',args:[...arguments]})
  },
 }),
]