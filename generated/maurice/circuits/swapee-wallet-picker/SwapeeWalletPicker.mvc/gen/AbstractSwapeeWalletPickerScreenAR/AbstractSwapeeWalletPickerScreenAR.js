import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerScreenAR}
 */
function __AbstractSwapeeWalletPickerScreenAR() {}
__AbstractSwapeeWalletPickerScreenAR.prototype = /** @type {!_AbstractSwapeeWalletPickerScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR}
 */
class _AbstractSwapeeWalletPickerScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeWalletPickerScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR} ‎
 */
class AbstractSwapeeWalletPickerScreenAR extends newAbstract(
 _AbstractSwapeeWalletPickerScreenAR,514125378629,null,{
  asISwapeeWalletPickerScreenAR:1,
  superSwapeeWalletPickerScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR} */
AbstractSwapeeWalletPickerScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR} */
function AbstractSwapeeWalletPickerScreenARClass(){}

export default AbstractSwapeeWalletPickerScreenAR


AbstractSwapeeWalletPickerScreenAR[$implementations]=[
 __AbstractSwapeeWalletPickerScreenAR,
 AR,
 AbstractSwapeeWalletPickerScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR}*/({
  allocator(){
   this.methods={
    makeWalletElement:'86307',
   }
  },
 }),
]
export {AbstractSwapeeWalletPickerScreenAR}