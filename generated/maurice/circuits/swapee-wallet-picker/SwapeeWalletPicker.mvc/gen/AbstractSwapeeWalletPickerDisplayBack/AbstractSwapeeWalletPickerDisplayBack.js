import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeWalletPickerDisplay}
 */
function __AbstractSwapeeWalletPickerDisplay() {}
__AbstractSwapeeWalletPickerDisplay.prototype = /** @type {!_AbstractSwapeeWalletPickerDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay}
 */
class _AbstractSwapeeWalletPickerDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay} ‎
 */
class AbstractSwapeeWalletPickerDisplay extends newAbstract(
 _AbstractSwapeeWalletPickerDisplay,514125378621,null,{
  asISwapeeWalletPickerDisplay:1,
  superSwapeeWalletPickerDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay} */
AbstractSwapeeWalletPickerDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay} */
function AbstractSwapeeWalletPickerDisplayClass(){}

export default AbstractSwapeeWalletPickerDisplay


AbstractSwapeeWalletPickerDisplay[$implementations]=[
 __AbstractSwapeeWalletPickerDisplay,
 GraphicsDriverBack,
 AbstractSwapeeWalletPickerDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay}*/({
    WalletTemplate:twinMock,
    Wallets:twinMock,
    CreateInvoiceBu:twinMock,
    InvoiceLoIn:twinMock,
    SwapeeAccountWr:twinMock,
    GetWalletBu:twinMock,
    SwapeeLoginBu:twinMock,
    SwapeeMe:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.SwapeeWalletPickerDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.Initialese}*/({
   Wallets:1,
   SwapeeAccountWr:1,
   GetWalletBu:1,
   CreateInvoiceBu:1,
   SwapeeLoginBu:1,
   InvoiceLoIn:1,
   SwapeeMe:1,
   WalletTemplate:1,
  }),
  initializer({
   Wallets:_Wallets,
   SwapeeAccountWr:_SwapeeAccountWr,
   GetWalletBu:_GetWalletBu,
   CreateInvoiceBu:_CreateInvoiceBu,
   SwapeeLoginBu:_SwapeeLoginBu,
   InvoiceLoIn:_InvoiceLoIn,
   SwapeeMe:_SwapeeMe,
   WalletTemplate:_WalletTemplate,
  }) {
   if(_Wallets!==undefined) this.Wallets=_Wallets
   if(_SwapeeAccountWr!==undefined) this.SwapeeAccountWr=_SwapeeAccountWr
   if(_GetWalletBu!==undefined) this.GetWalletBu=_GetWalletBu
   if(_CreateInvoiceBu!==undefined) this.CreateInvoiceBu=_CreateInvoiceBu
   if(_SwapeeLoginBu!==undefined) this.SwapeeLoginBu=_SwapeeLoginBu
   if(_InvoiceLoIn!==undefined) this.InvoiceLoIn=_InvoiceLoIn
   if(_SwapeeMe!==undefined) this.SwapeeMe=_SwapeeMe
   if(_WalletTemplate!==undefined) this.WalletTemplate=_WalletTemplate
  },
 }),
]