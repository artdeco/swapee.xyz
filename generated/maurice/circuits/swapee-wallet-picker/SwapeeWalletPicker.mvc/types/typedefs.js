/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ISwapeeWalletPickerComputer={}
xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor={}
xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress={}
xyz.swapee.wc.ISwapeeWalletPickerComputer.compute={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.IsSignedIn={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Wallets={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Invoice={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.LoadCreateInvoice={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.WalletsRotor={}
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel={}
xyz.swapee.wc.ISwapeeWalletPickerPort={}
xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs={}
xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs={}
xyz.swapee.wc.ISwapeeWalletPickerCore={}
xyz.swapee.wc.ISwapeeWalletPickerCore.Model={}
xyz.swapee.wc.ISwapeeWalletPickerPortInterface={}
xyz.swapee.wc.ISwapeeWalletPickerCPU={}
xyz.swapee.wc.ISwapeeWalletPickerProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ISwapeeWalletPickerController={}
xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT={}
xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR={}
xyz.swapee.wc.ISwapeeWalletPicker={}
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil={}
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent={}
xyz.swapee.wc.ISwapeeWalletPickerElement={}
xyz.swapee.wc.ISwapeeWalletPickerElement.build={}
xyz.swapee.wc.ISwapeeWalletPickerElement.short={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts={}
xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs={}
xyz.swapee.wc.ISwapeeWalletPickerGenerator={}
xyz.swapee.wc.ISwapeeWalletPickerDesigner={}
xyz.swapee.wc.ISwapeeWalletPickerDesigner.communicator={}
xyz.swapee.wc.ISwapeeWalletPickerDesigner.relay={}
xyz.swapee.wc.ISwapeeWalletPickerDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ISwapeeWalletPickerDisplay={}
xyz.swapee.wc.back.ISwapeeWalletPickerController={}
xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR={}
xyz.swapee.wc.back.ISwapeeWalletPickerScreen={}
xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT={}
xyz.swapee.wc.ISwapeeWalletPickerController={}
xyz.swapee.wc.ISwapeeWalletPickerScreen={}
xyz.swapee.wc.ISwapeeWalletPickerGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/02-ISwapeeWalletPickerComputer.xml}  c0c30d98aa6a0df2681ac7d06b14896a */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerComputer)} xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerComputer} xyz.swapee.wc.SwapeeWalletPickerComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerComputer` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerComputer
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.constructor&xyz.swapee.wc.SwapeeWalletPickerComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerComputer}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerComputer}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerComputer}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerComputer}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerComputer} xyz.swapee.wc.SwapeeWalletPickerComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.SwapeeWalletPickerMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeWalletPickerLand>)} xyz.swapee.wc.ISwapeeWalletPickerComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerComputer
 */
xyz.swapee.wc.ISwapeeWalletPickerComputer = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor} */
xyz.swapee.wc.ISwapeeWalletPickerComputer.prototype.adaptWalletsRotor = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress} */
xyz.swapee.wc.ISwapeeWalletPickerComputer.prototype.adaptLoadBitcoinAddress = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerComputer.compute} */
xyz.swapee.wc.ISwapeeWalletPickerComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerComputer} xyz.swapee.wc.ISwapeeWalletPickerComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerComputer_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerComputer
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerComputer = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerComputer.constructor&xyz.swapee.wc.ISwapeeWalletPickerComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerComputer}
 */
xyz.swapee.wc.SwapeeWalletPickerComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerComputer} */
xyz.swapee.wc.RecordISwapeeWalletPickerComputer

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerComputer} xyz.swapee.wc.BoundISwapeeWalletPickerComputer */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerComputer} xyz.swapee.wc.BoundSwapeeWalletPickerComputer */

/**
 * Contains getters to cast the _ISwapeeWalletPickerComputer_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerComputerCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerComputerCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerComputer_ instance into the _BoundISwapeeWalletPickerComputer_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerComputer}
 */
xyz.swapee.wc.ISwapeeWalletPickerComputerCaster.prototype.asISwapeeWalletPickerComputer
/**
 * Access the _SwapeeWalletPickerComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerComputer}
 */
xyz.swapee.wc.ISwapeeWalletPickerComputerCaster.prototype.superSwapeeWalletPickerComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Form, changes: xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Form) => (void|xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Return)} xyz.swapee.wc.ISwapeeWalletPickerComputer.__adaptWalletsRotor
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerComputer.__adaptWalletsRotor<!xyz.swapee.wc.ISwapeeWalletPickerComputer>} xyz.swapee.wc.ISwapeeWalletPickerComputer._adaptWalletsRotor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor} */
/**
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Form} form The form with inputs.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. ⤴ *ISwapeeWalletPickerOuterCore.Model.Wallets_Safe*
 * @param {xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Form} changes The previous values of the form.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. ⤴ *ISwapeeWalletPickerOuterCore.Model.Wallets_Safe*
 * @return {void|xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Return} The form with outputs.
 */
xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore.Model.Wallets_Safe} xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore.Model.WalletsRotor} xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptWalletsRotor.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Form, changes: xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Return|void)>)} xyz.swapee.wc.ISwapeeWalletPickerComputer.__adaptLoadBitcoinAddress
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerComputer.__adaptLoadBitcoinAddress<!xyz.swapee.wc.ISwapeeWalletPickerComputer>} xyz.swapee.wc.ISwapeeWalletPickerComputer._adaptLoadBitcoinAddress */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress} */
/**
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Form} form The form with inputs.
 * - `swapeeHost` _string_ The Swapee.me host. ⤴ *ISwapeeMeOuterCore.Model.SwapeeHost_Safe*
 * - `token` _string_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Token_Safe*
 * @param {xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Form} changes The previous values of the form.
 * - `swapeeHost` _string_ The Swapee.me host. ⤴ *ISwapeeMeOuterCore.Model.SwapeeHost_Safe*
 * - `token` _string_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Token_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore.Model.LoadCreateInvoice_Safe&xyz.swapee.wc.ISwapeeMeCore.Model.SwapeeHost_Safe&xyz.swapee.wc.ISwapeeMeCore.Model.Token_Safe} xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore.Model.Invoice} xyz.swapee.wc.ISwapeeWalletPickerComputer.adaptLoadBitcoinAddress.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.SwapeeWalletPickerMemory, land: !xyz.swapee.wc.ISwapeeWalletPickerComputer.compute.Land) => void} xyz.swapee.wc.ISwapeeWalletPickerComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerComputer.__compute<!xyz.swapee.wc.ISwapeeWalletPickerComputer>} xyz.swapee.wc.ISwapeeWalletPickerComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.SwapeeWalletPickerMemory} mem The memory.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerComputer.compute.Land} land The land.
 * - `SwapeeMe` _!xyz.swapee.wc.SwapeeMeMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/03-ISwapeeWalletPickerOuterCore.xml}  7cf75e5c6ff41239164ca022283f4606 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerOuterCore)} xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore} xyz.swapee.wc.SwapeeWalletPickerOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.constructor&xyz.swapee.wc.SwapeeWalletPickerOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerOuterCore|typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerOuterCore|typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerOuterCore|typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerOuterCoreCaster)} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _ISwapeeWalletPicker_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerOuterCore
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCore = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerOuterCore

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerOuterCore} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerOuterCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerOuterCore
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerOuterCore} The _ISwapeeWalletPicker_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerOuterCore = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerOuterCore.constructor&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeWalletPickerOuterCore.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore}
 */
xyz.swapee.wc.SwapeeWalletPickerOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerOuterCore.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerOuterCoreFields
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCoreFields = class { }
/**
 * The _ISwapeeWalletPicker_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore} */
xyz.swapee.wc.RecordISwapeeWalletPickerOuterCore

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore} xyz.swapee.wc.BoundISwapeeWalletPickerOuterCore */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerOuterCore} xyz.swapee.wc.BoundSwapeeWalletPickerOuterCore */

/**
 * Whether the person is signed in.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.IsSignedIn.isSignedIn

/**
 * The available wallets.
 * @typedef {!Array<string>}
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Wallets.wallets

/**
 * The _BitCoin_ address.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Invoice.invoice

/**
 * Starts creating an invoice.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.LoadCreateInvoice.loadCreateInvoice

/**
 * An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator.
 * @typedef {Array<!xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes>}
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.WalletsRotor.walletsRotor

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.IsSignedIn&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Wallets&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Invoice&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.LoadCreateInvoice&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.WalletsRotor} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model The _ISwapeeWalletPicker_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Wallets&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Invoice&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.LoadCreateInvoice&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel The _ISwapeeWalletPicker_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ISwapeeWalletPickerOuterCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerOuterCoreCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCoreCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerOuterCore_ instance into the _BoundISwapeeWalletPickerOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerOuterCore}
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCoreCaster.prototype.asISwapeeWalletPickerOuterCore
/**
 * Access the _SwapeeWalletPickerOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerOuterCore}
 */
xyz.swapee.wc.ISwapeeWalletPickerOuterCoreCaster.prototype.superSwapeeWalletPickerOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.IsSignedIn Whether the person is signed in (optional overlay).
 * @prop {boolean} [isSignedIn=false] Whether the person is signed in. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.IsSignedIn_Safe Whether the person is signed in (required overlay).
 * @prop {boolean} isSignedIn Whether the person is signed in.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Wallets The available wallets (optional overlay).
 * @prop {!Array<string>} [wallets] The available wallets. Default `[]`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Wallets_Safe The available wallets (required overlay).
 * @prop {!Array<string>} wallets The available wallets.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Invoice The _BitCoin_ address (optional overlay).
 * @prop {string} [invoice=""] The _BitCoin_ address. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Invoice_Safe The _BitCoin_ address (required overlay).
 * @prop {string} invoice The _BitCoin_ address.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.LoadCreateInvoice Starts creating an invoice (optional overlay).
 * @prop {boolean} [loadCreateInvoice=false] Starts creating an invoice. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.LoadCreateInvoice_Safe Starts creating an invoice (required overlay).
 * @prop {boolean} loadCreateInvoice Starts creating an invoice.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.WalletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (optional overlay).
 * @prop {Array<!xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes>} [walletsRotor=null] An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.WalletsRotor_Safe An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (required overlay).
 * @prop {Array<!xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes>} walletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn Whether the person is signed in (optional overlay).
 * @prop {*} [isSignedIn=null] Whether the person is signed in. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn_Safe Whether the person is signed in (required overlay).
 * @prop {*} isSignedIn Whether the person is signed in.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Wallets The available wallets (optional overlay).
 * @prop {*} [wallets=null] The available wallets. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Wallets_Safe The available wallets (required overlay).
 * @prop {*} wallets The available wallets.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Invoice The _BitCoin_ address (optional overlay).
 * @prop {*} [invoice=null] The _BitCoin_ address. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Invoice_Safe The _BitCoin_ address (required overlay).
 * @prop {*} invoice The _BitCoin_ address.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.LoadCreateInvoice Starts creating an invoice (optional overlay).
 * @prop {*} [loadCreateInvoice=null] Starts creating an invoice. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.LoadCreateInvoice_Safe Starts creating an invoice (required overlay).
 * @prop {*} loadCreateInvoice Starts creating an invoice.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (optional overlay).
 * @prop {*} [walletsRotor=null] An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor_Safe An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (required overlay).
 * @prop {*} walletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator.
 */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.IsSignedIn Whether the person is signed in (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.IsSignedIn_Safe Whether the person is signed in (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Wallets} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.Wallets The available wallets (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Wallets_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.Wallets_Safe The available wallets (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Invoice} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.Invoice The _BitCoin_ address (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Invoice_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.Invoice_Safe The _BitCoin_ address (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.LoadCreateInvoice} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.LoadCreateInvoice Starts creating an invoice (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.LoadCreateInvoice_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.LoadCreateInvoice_Safe Starts creating an invoice (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.WalletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.WalletsRotor_Safe An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.IsSignedIn Whether the person is signed in (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.IsSignedIn_Safe Whether the person is signed in (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Wallets} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.Wallets The available wallets (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Wallets_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.Wallets_Safe The available wallets (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Invoice} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.Invoice The _BitCoin_ address (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.Invoice_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.Invoice_Safe The _BitCoin_ address (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.LoadCreateInvoice} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.LoadCreateInvoice Starts creating an invoice (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.LoadCreateInvoice_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.LoadCreateInvoice_Safe Starts creating an invoice (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.WalletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor_Safe} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.WalletsRotor_Safe An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.IsSignedIn} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.IsSignedIn Whether the person is signed in (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.IsSignedIn_Safe} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.IsSignedIn_Safe Whether the person is signed in (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Wallets} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.Wallets The available wallets (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Wallets_Safe} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.Wallets_Safe The available wallets (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Invoice} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.Invoice The _BitCoin_ address (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.Invoice_Safe} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.Invoice_Safe The _BitCoin_ address (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.LoadCreateInvoice} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.LoadCreateInvoice Starts creating an invoice (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.LoadCreateInvoice_Safe} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.LoadCreateInvoice_Safe Starts creating an invoice (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.WalletsRotor} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.WalletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model.WalletsRotor_Safe} xyz.swapee.wc.ISwapeeWalletPickerCore.Model.WalletsRotor_Safe An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/04-ISwapeeWalletPickerPort.xml}  d360351379de5d34bddd9f860c18b10e */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeWalletPickerPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerPort)} xyz.swapee.wc.AbstractSwapeeWalletPickerPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerPort} xyz.swapee.wc.SwapeeWalletPickerPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerPort
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerPort.constructor&xyz.swapee.wc.SwapeeWalletPickerPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerPort|typeof xyz.swapee.wc.SwapeeWalletPickerPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerPort|typeof xyz.swapee.wc.SwapeeWalletPickerPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerPort|typeof xyz.swapee.wc.SwapeeWalletPickerPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerPort.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerPort} xyz.swapee.wc.SwapeeWalletPickerPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs>)} xyz.swapee.wc.ISwapeeWalletPickerPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ISwapeeWalletPicker_, providing input
 * pins.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerPort
 */
xyz.swapee.wc.ISwapeeWalletPickerPort = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerPort.resetPort} */
xyz.swapee.wc.ISwapeeWalletPickerPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerPort.resetSwapeeWalletPickerPort} */
xyz.swapee.wc.ISwapeeWalletPickerPort.prototype.resetSwapeeWalletPickerPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerPort.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerPort} xyz.swapee.wc.ISwapeeWalletPickerPort.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerPort
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerPort} The port that serves as an interface to the _ISwapeeWalletPicker_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerPort = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerPort.constructor&xyz.swapee.wc.ISwapeeWalletPickerPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerPort}
 */
xyz.swapee.wc.SwapeeWalletPickerPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerPort.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerPortFields
 */
xyz.swapee.wc.ISwapeeWalletPickerPortFields = class { }
/**
 * The inputs to the _ISwapeeWalletPicker_'s controller via its port.
 */
xyz.swapee.wc.ISwapeeWalletPickerPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeWalletPickerPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerPort} */
xyz.swapee.wc.RecordISwapeeWalletPickerPort

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerPort} xyz.swapee.wc.BoundISwapeeWalletPickerPort */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerPort} xyz.swapee.wc.BoundSwapeeWalletPickerPort */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel} xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ISwapeeWalletPicker_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs
 */
xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.constructor&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.constructor */
/**
 * The inputs to the _ISwapeeWalletPicker_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerPortInterface
 */
xyz.swapee.wc.ISwapeeWalletPickerPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeWalletPickerPortInterface.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerPortInterface

/**
 * A concrete class of _ISwapeeWalletPickerPortInterface_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerPortInterface
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerPortInterface} The port interface.
 */
xyz.swapee.wc.SwapeeWalletPickerPortInterface = class extends xyz.swapee.wc.ISwapeeWalletPickerPortInterface { }
xyz.swapee.wc.SwapeeWalletPickerPortInterface.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerPortInterface.Props
 * @prop {boolean} isSignedIn Whether the person is signed in.
 * @prop {!Array<string>} wallets The available wallets.
 * @prop {string} invoice The _BitCoin_ address.
 * @prop {boolean} loadCreateInvoice Starts creating an invoice.
 * @prop {Array<!xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes>} walletsRotor An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator.
 */

/**
 * Contains getters to cast the _ISwapeeWalletPickerPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerPortCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerPortCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerPort_ instance into the _BoundISwapeeWalletPickerPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerPort}
 */
xyz.swapee.wc.ISwapeeWalletPickerPortCaster.prototype.asISwapeeWalletPickerPort
/**
 * Access the _SwapeeWalletPickerPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerPort}
 */
xyz.swapee.wc.ISwapeeWalletPickerPortCaster.prototype.superSwapeeWalletPickerPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerPort.__resetPort<!xyz.swapee.wc.ISwapeeWalletPickerPort>} xyz.swapee.wc.ISwapeeWalletPickerPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerPort.resetPort} */
/**
 * Resets the _ISwapeeWalletPicker_ port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerPort.__resetSwapeeWalletPickerPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerPort.__resetSwapeeWalletPickerPort<!xyz.swapee.wc.ISwapeeWalletPickerPort>} xyz.swapee.wc.ISwapeeWalletPickerPort._resetSwapeeWalletPickerPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerPort.resetSwapeeWalletPickerPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerPort.resetSwapeeWalletPickerPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/08-ISwapeeWalletPickerCPU.xml}  0189fb3606ba8ffdc00c7afaaa2dc396 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeWalletPickerCPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerCPU)} xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerCPU} xyz.swapee.wc.SwapeeWalletPickerCPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerCPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerCPU
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.constructor&xyz.swapee.wc.SwapeeWalletPickerCPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerCPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerCPU|typeof xyz.swapee.wc.SwapeeWalletPickerCPU)|(!guest.maurice.IStatefulLoadable|typeof guest.maurice.StatefulLoadable)|(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|!xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerCPU|typeof xyz.swapee.wc.SwapeeWalletPickerCPU)|(!guest.maurice.IStatefulLoadable|typeof guest.maurice.StatefulLoadable)|(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|!xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerCPU|typeof xyz.swapee.wc.SwapeeWalletPickerCPU)|(!guest.maurice.IStatefulLoadable|typeof guest.maurice.StatefulLoadable)|(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|!xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerCPU.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerCPU} xyz.swapee.wc.SwapeeWalletPickerCPUConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice = class {
  constructor() {
    /**
     * Resets all cores.
     */
    this.resetCore=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerCPU._resetCore|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerCPU._resetCore>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice

/**
 * A concrete class of _ISwapeeWalletPickerCPUHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerCPUHyperslice
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.SwapeeWalletPickerCPUHyperslice = class extends xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice { }
xyz.swapee.wc.SwapeeWalletPickerCPUHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerCPUHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice = class {
  constructor() {
    /**
     * Resets all cores.
     */
    this.resetCore=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerCPU.__resetCore<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerCPU.__resetCore<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice

/**
 * A concrete class of _ISwapeeWalletPickerCPUBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerCPUBindingHyperslice
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice<THIS>}
 */
xyz.swapee.wc.SwapeeWalletPickerCPUBindingHyperslice = class extends xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice { }
xyz.swapee.wc.SwapeeWalletPickerCPUBindingHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerCPUBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerCPUFields&guest.maurice.IStatefulLoadable&xyz.swapee.wc.ISwapeeWalletPickerCore&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerCPUCaster)} xyz.swapee.wc.ISwapeeWalletPickerCPU.constructor */
/** @typedef {typeof guest.maurice.IStatefulLoadable} guest.maurice.IStatefulLoadable.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerCore} xyz.swapee.wc.ISwapeeWalletPickerCore.typeof */
/**
 * The _ISwapeeWalletPicker_'s CPU that encapsulates the component's native atomic
 * core, as well as additional embedded cores.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCPU
 */
xyz.swapee.wc.ISwapeeWalletPickerCPU = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerCPU.constructor&guest.maurice.IStatefulLoadable.typeof&xyz.swapee.wc.ISwapeeWalletPickerCore.typeof&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerCPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerCPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerCPU.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerCPU.resetCore} */
xyz.swapee.wc.ISwapeeWalletPickerCPU.prototype.resetCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerCPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerCPU.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerCPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerCPU} xyz.swapee.wc.ISwapeeWalletPickerCPU.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerCPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerCPU
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerCPU} The _ISwapeeWalletPicker_'s CPU that encapsulates the component's native atomic
 * core, as well as additional embedded cores.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerCPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerCPU = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerCPU.constructor&xyz.swapee.wc.ISwapeeWalletPickerCPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerCPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerCPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerCPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerCPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerCPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCPU}
 */
xyz.swapee.wc.SwapeeWalletPickerCPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerCPU.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCPUFields
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUFields = class { }
/**
 * The model comprises of 2 cores.
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerCPU.Model} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerCPU.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCPU} */
xyz.swapee.wc.RecordISwapeeWalletPickerCPU

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCPU} xyz.swapee.wc.BoundISwapeeWalletPickerCPU */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerCPU} xyz.swapee.wc.BoundSwapeeWalletPickerCPU */

/** @typedef {guest.maurice.IStatefulLoadable.State&xyz.swapee.wc.ISwapeeWalletPickerCore.Model} xyz.swapee.wc.ISwapeeWalletPickerCPU.Model The model comprises of 2 cores. */

/**
 * Contains getters to cast the _ISwapeeWalletPickerCPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCPUCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerCPU_ instance into the _BoundISwapeeWalletPickerCPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerCPU}
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUCaster.prototype.asISwapeeWalletPickerCPU
/**
 * Access the _SwapeeWalletPickerCPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerCPU}
 */
xyz.swapee.wc.ISwapeeWalletPickerCPUCaster.prototype.superSwapeeWalletPickerCPU

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerCPU.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCPU.__resetCore<!xyz.swapee.wc.ISwapeeWalletPickerCPU>} xyz.swapee.wc.ISwapeeWalletPickerCPU._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerCPU.resetCore} */
/**
 * Resets all cores. `🔗 $combine`
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerCPU.resetCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerCPU
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/09-ISwapeeWalletPickerCore.xml}  7ce064d6f8e6919ec1ca27f2bc6593cb */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeWalletPickerCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerCore)} xyz.swapee.wc.AbstractSwapeeWalletPickerCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerCore} xyz.swapee.wc.SwapeeWalletPickerCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerCore
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerCore.constructor&xyz.swapee.wc.SwapeeWalletPickerCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|(!xyz.swapee.wc.ISwapeeWalletPickerOuterCore|typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|(!xyz.swapee.wc.ISwapeeWalletPickerOuterCore|typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|(!xyz.swapee.wc.ISwapeeWalletPickerOuterCore|typeof xyz.swapee.wc.SwapeeWalletPickerOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCore}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerCoreCaster&xyz.swapee.wc.ISwapeeWalletPickerOuterCore)} xyz.swapee.wc.ISwapeeWalletPickerCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCore
 */
xyz.swapee.wc.ISwapeeWalletPickerCore = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeWalletPickerOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeWalletPickerCore.resetCore} */
xyz.swapee.wc.ISwapeeWalletPickerCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerCore.resetSwapeeWalletPickerCore} */
xyz.swapee.wc.ISwapeeWalletPickerCore.prototype.resetSwapeeWalletPickerCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerCore.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerCore} xyz.swapee.wc.ISwapeeWalletPickerCore.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerCore
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerCore = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerCore.constructor&xyz.swapee.wc.ISwapeeWalletPickerCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeWalletPickerCore.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerCore}
 */
xyz.swapee.wc.SwapeeWalletPickerCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerCore.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCoreFields
 */
xyz.swapee.wc.ISwapeeWalletPickerCoreFields = class { }
/**
 * The _ISwapeeWalletPicker_'s memory.
 */
xyz.swapee.wc.ISwapeeWalletPickerCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ISwapeeWalletPickerCoreFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeWalletPickerCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore} */
xyz.swapee.wc.RecordISwapeeWalletPickerCore

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore} xyz.swapee.wc.BoundISwapeeWalletPickerCore */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerCore} xyz.swapee.wc.BoundSwapeeWalletPickerCore */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model} xyz.swapee.wc.ISwapeeWalletPickerCore.Model The _ISwapeeWalletPicker_'s memory. */

/**
 * Contains getters to cast the _ISwapeeWalletPickerCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCoreCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerCoreCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerCore_ instance into the _BoundISwapeeWalletPickerCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerCore}
 */
xyz.swapee.wc.ISwapeeWalletPickerCoreCaster.prototype.asISwapeeWalletPickerCore
/**
 * Access the _SwapeeWalletPickerCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerCore}
 */
xyz.swapee.wc.ISwapeeWalletPickerCoreCaster.prototype.superSwapeeWalletPickerCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore.__resetCore<!xyz.swapee.wc.ISwapeeWalletPickerCore>} xyz.swapee.wc.ISwapeeWalletPickerCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerCore.resetCore} */
/**
 * Resets the _ISwapeeWalletPicker_ core.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerCore.__resetSwapeeWalletPickerCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerCore.__resetSwapeeWalletPickerCore<!xyz.swapee.wc.ISwapeeWalletPickerCore>} xyz.swapee.wc.ISwapeeWalletPickerCore._resetSwapeeWalletPickerCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerCore.resetSwapeeWalletPickerCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerCore.resetSwapeeWalletPickerCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/10-ISwapeeWalletPickerProcessor.xml}  c79a8c71040dc4c26a4ce26dd6e2eaac */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese&xyz.swapee.wc.ISwapeeWalletPickerController.Initialese} xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerProcessor)} xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerProcessor} xyz.swapee.wc.SwapeeWalletPickerProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.constructor&xyz.swapee.wc.SwapeeWalletPickerProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerCore|typeof xyz.swapee.wc.SwapeeWalletPickerCore)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerProcessor} xyz.swapee.wc.SwapeeWalletPickerProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerProcessorCaster&xyz.swapee.wc.ISwapeeWalletPickerComputer&xyz.swapee.wc.ISwapeeWalletPickerCore&xyz.swapee.wc.ISwapeeWalletPickerController)} xyz.swapee.wc.ISwapeeWalletPickerProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerComputer} xyz.swapee.wc.ISwapeeWalletPickerComputer.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerCore} xyz.swapee.wc.ISwapeeWalletPickerCore.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerController} xyz.swapee.wc.ISwapeeWalletPickerController.typeof */
/**
 * The processor to compute changes to the memory for the _ISwapeeWalletPicker_.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerProcessor
 */
xyz.swapee.wc.ISwapeeWalletPickerProcessor = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeWalletPickerComputer.typeof&xyz.swapee.wc.ISwapeeWalletPickerCore.typeof&xyz.swapee.wc.ISwapeeWalletPickerController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerProcessor.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerProcessor.pulseLoadCreateInvoice} */
xyz.swapee.wc.ISwapeeWalletPickerProcessor.prototype.pulseLoadCreateInvoice = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerProcessor} xyz.swapee.wc.ISwapeeWalletPickerProcessor.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerProcessor_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerProcessor
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerProcessor} The processor to compute changes to the memory for the _ISwapeeWalletPicker_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerProcessor = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerProcessor.constructor&xyz.swapee.wc.ISwapeeWalletPickerProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.SwapeeWalletPickerProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerProcessor} */
xyz.swapee.wc.RecordISwapeeWalletPickerProcessor

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerProcessor} xyz.swapee.wc.BoundISwapeeWalletPickerProcessor */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerProcessor} xyz.swapee.wc.BoundSwapeeWalletPickerProcessor */

/**
 * Contains getters to cast the _ISwapeeWalletPickerProcessor_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerProcessorCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerProcessorCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerProcessor_ instance into the _BoundISwapeeWalletPickerProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.ISwapeeWalletPickerProcessorCaster.prototype.asISwapeeWalletPickerProcessor
/**
 * Access the _SwapeeWalletPickerProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.ISwapeeWalletPickerProcessorCaster.prototype.superSwapeeWalletPickerProcessor

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerProcessor.__pulseLoadCreateInvoice
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerProcessor.__pulseLoadCreateInvoice<!xyz.swapee.wc.ISwapeeWalletPickerProcessor>} xyz.swapee.wc.ISwapeeWalletPickerProcessor._pulseLoadCreateInvoice */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerProcessor.pulseLoadCreateInvoice} */
/**
 * A method called to set the `loadCreateInvoice` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerProcessor.pulseLoadCreateInvoice = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerProcessor
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/100-SwapeeWalletPickerMemory.xml}  56db08c061a9b9bae54e6555b6b3d25f */
/**
 * The memory of the _ISwapeeWalletPicker_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.SwapeeWalletPickerMemory
 */
xyz.swapee.wc.SwapeeWalletPickerMemory = class { }
/**
 * Whether the person is signed in. Default `false`.
 */
xyz.swapee.wc.SwapeeWalletPickerMemory.prototype.isSignedIn = /** @type {boolean} */ (void 0)
/**
 * The available wallets. Default `[]`.
 */
xyz.swapee.wc.SwapeeWalletPickerMemory.prototype.wallets = /** @type {!Array<string>} */ (void 0)
/**
 * The _BitCoin_ address. Default empty string.
 */
xyz.swapee.wc.SwapeeWalletPickerMemory.prototype.invoice = /** @type {string} */ (void 0)
/**
 * Starts creating an invoice. Default `false`.
 */
xyz.swapee.wc.SwapeeWalletPickerMemory.prototype.loadCreateInvoice = /** @type {boolean} */ (void 0)
/**
 * An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 */
xyz.swapee.wc.SwapeeWalletPickerMemory.prototype.walletsRotor = /** @type {Array<!xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes>} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/102-SwapeeWalletPickerInputs.xml}  e0c52adeb3bf9d1b4ae41cf20873db8c */
/**
 * The inputs of the _ISwapeeWalletPicker_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.SwapeeWalletPickerInputs
 */
xyz.swapee.wc.front.SwapeeWalletPickerInputs = class { }
/**
 * Whether the person is signed in. Default `false`.
 */
xyz.swapee.wc.front.SwapeeWalletPickerInputs.prototype.isSignedIn = /** @type {boolean|undefined} */ (void 0)
/**
 * The available wallets. Default `[]`.
 */
xyz.swapee.wc.front.SwapeeWalletPickerInputs.prototype.wallets = /** @type {(!Array<string>)|undefined} */ (void 0)
/**
 * The _BitCoin_ address. Default empty string.
 */
xyz.swapee.wc.front.SwapeeWalletPickerInputs.prototype.invoice = /** @type {string|undefined} */ (void 0)
/**
 * Starts creating an invoice. Default `false`.
 */
xyz.swapee.wc.front.SwapeeWalletPickerInputs.prototype.loadCreateInvoice = /** @type {boolean|undefined} */ (void 0)
/**
 * An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 */
xyz.swapee.wc.front.SwapeeWalletPickerInputs.prototype.walletsRotor = /** @type {(Array<!xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes>)|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/11-ISwapeeWalletPicker.xml}  0fc05e7226710fda453c82b8f7c6043c */
/**
 * An atomic wrapper for the _ISwapeeWalletPicker_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.SwapeeWalletPickerEnv
 */
xyz.swapee.wc.SwapeeWalletPickerEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.SwapeeWalletPickerEnv.prototype.swapeeWalletPicker = /** @type {xyz.swapee.wc.ISwapeeWalletPicker} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.ISwapeeWalletPickerController.Inputs>&xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese&xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese&xyz.swapee.wc.ISwapeeWalletPickerController.Initialese} xyz.swapee.wc.ISwapeeWalletPicker.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPicker)} xyz.swapee.wc.AbstractSwapeeWalletPicker.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPicker} xyz.swapee.wc.SwapeeWalletPicker.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPicker` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPicker
 */
xyz.swapee.wc.AbstractSwapeeWalletPicker = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPicker.constructor&xyz.swapee.wc.SwapeeWalletPicker.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPicker.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPicker
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPicker.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPicker} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPicker|typeof xyz.swapee.wc.SwapeeWalletPicker)|(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPicker}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPicker.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPicker}
 */
xyz.swapee.wc.AbstractSwapeeWalletPicker.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPicker}
 */
xyz.swapee.wc.AbstractSwapeeWalletPicker.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPicker|typeof xyz.swapee.wc.SwapeeWalletPicker)|(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPicker}
 */
xyz.swapee.wc.AbstractSwapeeWalletPicker.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPicker|typeof xyz.swapee.wc.SwapeeWalletPicker)|(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPicker}
 */
xyz.swapee.wc.AbstractSwapeeWalletPicker.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPicker.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPicker} xyz.swapee.wc.SwapeeWalletPickerConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPicker.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ISwapeeWalletPicker.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ISwapeeWalletPicker.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ISwapeeWalletPicker.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.SwapeeWalletPickerMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.SwapeeWalletPickerClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerCaster&xyz.swapee.wc.ISwapeeWalletPickerProcessor&xyz.swapee.wc.ISwapeeWalletPickerComputer&xyz.swapee.wc.ISwapeeWalletPickerController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.ISwapeeWalletPickerController.Inputs, !xyz.swapee.wc.SwapeeWalletPickerLand>)} xyz.swapee.wc.ISwapeeWalletPicker.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerProcessor} xyz.swapee.wc.ISwapeeWalletPickerProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerController} xyz.swapee.wc.ISwapeeWalletPickerController.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ISwapeeWalletPicker
 */
xyz.swapee.wc.ISwapeeWalletPicker = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPicker.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeWalletPickerProcessor.typeof&xyz.swapee.wc.ISwapeeWalletPickerComputer.typeof&xyz.swapee.wc.ISwapeeWalletPickerController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPicker* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPicker.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPicker.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPicker&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPicker.Initialese>)} xyz.swapee.wc.SwapeeWalletPicker.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPicker} xyz.swapee.wc.ISwapeeWalletPicker.typeof */
/**
 * A concrete class of _ISwapeeWalletPicker_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPicker
 * @implements {xyz.swapee.wc.ISwapeeWalletPicker} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPicker.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPicker = class extends /** @type {xyz.swapee.wc.SwapeeWalletPicker.constructor&xyz.swapee.wc.ISwapeeWalletPicker.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPicker* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPicker.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPicker* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPicker.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPicker.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPicker}
 */
xyz.swapee.wc.SwapeeWalletPicker.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPicker.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerFields
 */
xyz.swapee.wc.ISwapeeWalletPickerFields = class { }
/**
 * The input pins of the _ISwapeeWalletPicker_ port.
 */
xyz.swapee.wc.ISwapeeWalletPickerFields.prototype.pinout = /** @type {!xyz.swapee.wc.ISwapeeWalletPicker.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPicker} */
xyz.swapee.wc.RecordISwapeeWalletPicker

/** @typedef {xyz.swapee.wc.ISwapeeWalletPicker} xyz.swapee.wc.BoundISwapeeWalletPicker */

/** @typedef {xyz.swapee.wc.SwapeeWalletPicker} xyz.swapee.wc.BoundSwapeeWalletPicker */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerController.Inputs} xyz.swapee.wc.ISwapeeWalletPicker.Pinout The input pins of the _ISwapeeWalletPicker_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs>)} xyz.swapee.wc.ISwapeeWalletPickerBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerBuffer
 */
xyz.swapee.wc.ISwapeeWalletPickerBuffer = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeWalletPickerBuffer.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerBuffer

/**
 * A concrete class of _ISwapeeWalletPickerBuffer_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerBuffer
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.SwapeeWalletPickerBuffer = class extends xyz.swapee.wc.ISwapeeWalletPickerBuffer { }
xyz.swapee.wc.SwapeeWalletPickerBuffer.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerBuffer

/**
 * Contains getters to cast the _ISwapeeWalletPicker_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerCaster = class { }
/**
 * Cast the _ISwapeeWalletPicker_ instance into the _BoundISwapeeWalletPicker_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPicker}
 */
xyz.swapee.wc.ISwapeeWalletPickerCaster.prototype.asISwapeeWalletPicker
/**
 * Access the _SwapeeWalletPicker_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPicker}
 */
xyz.swapee.wc.ISwapeeWalletPickerCaster.prototype.superSwapeeWalletPicker

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/110-SwapeeWalletPickerSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeWalletPickerMemoryPQs
 */
xyz.swapee.wc.SwapeeWalletPickerMemoryPQs = class {
  constructor() {
    /**
     * `c60d7`
     */
    this.isSignedIn=/** @type {string} */ (void 0)
    /**
     * `j0b43`
     */
    this.wallets=/** @type {string} */ (void 0)
    /**
     * `e825f`
     */
    this.walletsRotor=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeWalletPickerMemoryPQs.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerMemoryQPs
 * @dict
 */
xyz.swapee.wc.SwapeeWalletPickerMemoryQPs = class { }
/**
 * `isSignedIn`
 */
xyz.swapee.wc.SwapeeWalletPickerMemoryQPs.prototype.c60d7 = /** @type {string} */ (void 0)
/**
 * `wallets`
 */
xyz.swapee.wc.SwapeeWalletPickerMemoryQPs.prototype.j0b43 = /** @type {string} */ (void 0)
/**
 * `walletsRotor`
 */
xyz.swapee.wc.SwapeeWalletPickerMemoryQPs.prototype.e825f = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerMemoryPQs)} xyz.swapee.wc.SwapeeWalletPickerInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerMemoryPQs} xyz.swapee.wc.SwapeeWalletPickerMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeWalletPickerInputsPQs
 */
xyz.swapee.wc.SwapeeWalletPickerInputsPQs = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerInputsPQs.constructor&xyz.swapee.wc.SwapeeWalletPickerMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeWalletPickerInputsPQs.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerInputsPQs

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerMemoryPQs)} xyz.swapee.wc.SwapeeWalletPickerInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerInputsQPs
 * @dict
 */
xyz.swapee.wc.SwapeeWalletPickerInputsQPs = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerInputsQPs.constructor&xyz.swapee.wc.SwapeeWalletPickerMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeWalletPickerInputsQPs.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeWalletPickerVdusPQs
 */
xyz.swapee.wc.SwapeeWalletPickerVdusPQs = class {
  constructor() {
    /**
     * `a04f1`
     */
    this.WalletTemplate=/** @type {string} */ (void 0)
    /**
     * `a04f2`
     */
    this.Wallets=/** @type {string} */ (void 0)
    /**
     * `a04f3`
     */
    this.SwapeeAccountWr=/** @type {string} */ (void 0)
    /**
     * `a04f4`
     */
    this.GetWalletBu=/** @type {string} */ (void 0)
    /**
     * `a04f5`
     */
    this.SwapeeLoginBu=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeWalletPickerVdusPQs.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerVdusQPs
 * @dict
 */
xyz.swapee.wc.SwapeeWalletPickerVdusQPs = class { }
/**
 * `WalletTemplate`
 */
xyz.swapee.wc.SwapeeWalletPickerVdusQPs.prototype.a04f1 = /** @type {string} */ (void 0)
/**
 * `Wallets`
 */
xyz.swapee.wc.SwapeeWalletPickerVdusQPs.prototype.a04f2 = /** @type {string} */ (void 0)
/**
 * `SwapeeAccountWr`
 */
xyz.swapee.wc.SwapeeWalletPickerVdusQPs.prototype.a04f3 = /** @type {string} */ (void 0)
/**
 * `GetWalletBu`
 */
xyz.swapee.wc.SwapeeWalletPickerVdusQPs.prototype.a04f4 = /** @type {string} */ (void 0)
/**
 * `SwapeeLoginBu`
 */
xyz.swapee.wc.SwapeeWalletPickerVdusQPs.prototype.a04f5 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/12-ISwapeeWalletPickerHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtilFields)} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.router} */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _ISwapeeWalletPickerHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerHtmlComponentUtil
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerHtmlComponentUtil = class extends xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil { }
xyz.swapee.wc.SwapeeWalletPickerHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerHtmlComponentUtil

/**
 * Fields of the ISwapeeWalletPickerHtmlComponentUtil.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtilFields
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil} */
xyz.swapee.wc.RecordISwapeeWalletPickerHtmlComponentUtil

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil} xyz.swapee.wc.BoundISwapeeWalletPickerHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerHtmlComponentUtil} xyz.swapee.wc.BoundSwapeeWalletPickerHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.ISwapeeMePort} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeWalletPickerPort} SwapeeWalletPicker The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe
 * @prop {!xyz.swapee.wc.SwapeeWalletPickerMemory} SwapeeWalletPicker
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.ISwapeeMe.Pinout} SwapeeMe
 * @prop {!xyz.swapee.wc.ISwapeeWalletPicker.Pinout} SwapeeWalletPicker
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.__router<!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil>} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `SwapeeMe` _typeof ISwapeeMePort_
 * - `SwapeeWalletPicker` _typeof ISwapeeWalletPickerPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `SwapeeMe` _!SwapeeMeMemory_
 * - `SwapeeWalletPicker` _!SwapeeWalletPickerMemory_
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `SwapeeMe` _!ISwapeeMe.Pinout_
 * - `SwapeeWalletPicker` _!ISwapeeWalletPicker.Pinout_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/12-ISwapeeWalletPickerHtmlComponent.xml}  83b184feffc9e257031b0a4aa925b2eb */
/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese&xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese&xyz.swapee.wc.ISwapeeWalletPicker.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ISwapeeWalletPickerProcessor.Initialese&xyz.swapee.wc.ISwapeeWalletPickerComputer.Initialese&xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerHtmlComponent)} xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent} xyz.swapee.wc.SwapeeWalletPickerHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.constructor&xyz.swapee.wc.SwapeeWalletPickerHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent|typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeWalletPickerController|typeof xyz.swapee.wc.back.SwapeeWalletPickerController)|(!xyz.swapee.wc.back.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen)|(!xyz.swapee.wc.ISwapeeWalletPicker|typeof xyz.swapee.wc.SwapeeWalletPicker)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeWalletPickerGPU|typeof xyz.swapee.wc.SwapeeWalletPickerGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerGenerator|typeof xyz.swapee.wc.SwapeeWalletPickerGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent|typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeWalletPickerController|typeof xyz.swapee.wc.back.SwapeeWalletPickerController)|(!xyz.swapee.wc.back.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen)|(!xyz.swapee.wc.ISwapeeWalletPicker|typeof xyz.swapee.wc.SwapeeWalletPicker)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeWalletPickerGPU|typeof xyz.swapee.wc.SwapeeWalletPickerGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerGenerator|typeof xyz.swapee.wc.SwapeeWalletPickerGenerator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent|typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeWalletPickerController|typeof xyz.swapee.wc.back.SwapeeWalletPickerController)|(!xyz.swapee.wc.back.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen)|(!xyz.swapee.wc.ISwapeeWalletPicker|typeof xyz.swapee.wc.SwapeeWalletPicker)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeWalletPickerGPU|typeof xyz.swapee.wc.SwapeeWalletPickerGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeWalletPickerProcessor|typeof xyz.swapee.wc.SwapeeWalletPickerProcessor)|(!xyz.swapee.wc.ISwapeeWalletPickerComputer|typeof xyz.swapee.wc.SwapeeWalletPickerComputer)|(!xyz.swapee.wc.ISwapeeWalletPickerGenerator|typeof xyz.swapee.wc.SwapeeWalletPickerGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent} xyz.swapee.wc.SwapeeWalletPickerHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentCaster&xyz.swapee.wc.back.ISwapeeWalletPickerController&xyz.swapee.wc.back.ISwapeeWalletPickerScreen&xyz.swapee.wc.ISwapeeWalletPicker&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeWalletPickerLand>&xyz.swapee.wc.ISwapeeWalletPickerGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.ISwapeeWalletPickerController.Inputs, !HTMLDivElement, !xyz.swapee.wc.SwapeeWalletPickerLand>&xyz.swapee.wc.ISwapeeWalletPickerProcessor&xyz.swapee.wc.ISwapeeWalletPickerComputer&xyz.swapee.wc.ISwapeeWalletPickerGenerator)} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeWalletPickerController} xyz.swapee.wc.back.ISwapeeWalletPickerController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeWalletPickerScreen} xyz.swapee.wc.back.ISwapeeWalletPickerScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGPU} xyz.swapee.wc.ISwapeeWalletPickerGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGenerator} xyz.swapee.wc.ISwapeeWalletPickerGenerator.typeof */
/**
 * The _ISwapeeWalletPicker_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeWalletPickerController.typeof&xyz.swapee.wc.back.ISwapeeWalletPickerScreen.typeof&xyz.swapee.wc.ISwapeeWalletPicker.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.ISwapeeWalletPickerGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ISwapeeWalletPickerProcessor.typeof&xyz.swapee.wc.ISwapeeWalletPickerComputer.typeof&xyz.swapee.wc.ISwapeeWalletPickerGenerator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent} xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerHtmlComponent
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent} The _ISwapeeWalletPicker_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerHtmlComponent = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerHtmlComponent.constructor&xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.SwapeeWalletPickerHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent} */
xyz.swapee.wc.RecordISwapeeWalletPickerHtmlComponent

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent} xyz.swapee.wc.BoundISwapeeWalletPickerHtmlComponent */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerHtmlComponent} xyz.swapee.wc.BoundSwapeeWalletPickerHtmlComponent */

/**
 * Contains getters to cast the _ISwapeeWalletPickerHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerHtmlComponent_ instance into the _BoundISwapeeWalletPickerHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentCaster.prototype.asISwapeeWalletPickerHtmlComponent
/**
 * Access the _SwapeeWalletPickerHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentCaster.prototype.superSwapeeWalletPickerHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/130-ISwapeeWalletPickerElement.xml}  8569f142cfa8ca2d254a1918ceba7905 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.SwapeeWalletPickerLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.ISwapeeMe>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ISwapeeWalletPickerElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerElement)} xyz.swapee.wc.AbstractSwapeeWalletPickerElement.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerElement} xyz.swapee.wc.SwapeeWalletPickerElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerElement` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerElement
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElement = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerElement.constructor&xyz.swapee.wc.SwapeeWalletPickerElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerElement.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElement.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerElement|typeof xyz.swapee.wc.SwapeeWalletPickerElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElement}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElement}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerElement|typeof xyz.swapee.wc.SwapeeWalletPickerElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElement}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerElement|typeof xyz.swapee.wc.SwapeeWalletPickerElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElement}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerElement.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerElement} xyz.swapee.wc.SwapeeWalletPickerElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerElementFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs, !xyz.swapee.wc.SwapeeWalletPickerLand>&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeWalletPickerLand>&guest.maurice.IMilleu<!xyz.swapee.wc.ISwapeeMe>)} xyz.swapee.wc.ISwapeeWalletPickerElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _ISwapeeWalletPicker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerElement
 */
xyz.swapee.wc.ISwapeeWalletPickerElement = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.solder} */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.render} */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.build} */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.buildSwapeeMe} */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.buildSwapeeMe = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.short} */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.server} */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement.inducer} */
xyz.swapee.wc.ISwapeeWalletPickerElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerElement&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerElement.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement} xyz.swapee.wc.ISwapeeWalletPickerElement.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerElement_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerElement
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerElement} A component description.
 *
 * The _ISwapeeWalletPicker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerElement.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerElement = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerElement.constructor&xyz.swapee.wc.ISwapeeWalletPickerElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElement}
 */
xyz.swapee.wc.SwapeeWalletPickerElement.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerElement.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerElementFields
 */
xyz.swapee.wc.ISwapeeWalletPickerElementFields = class { }
/**
 * The element-specific inputs to the _ISwapeeWalletPicker_ component.
 */
xyz.swapee.wc.ISwapeeWalletPickerElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.ISwapeeWalletPickerElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement} */
xyz.swapee.wc.RecordISwapeeWalletPickerElement

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement} xyz.swapee.wc.BoundISwapeeWalletPickerElement */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerElement} xyz.swapee.wc.BoundSwapeeWalletPickerElement */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs&xyz.swapee.wc.ISwapeeWalletPickerDisplay.Queries&xyz.swapee.wc.ISwapeeWalletPickerController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs} xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs The element-specific inputs to the _ISwapeeWalletPicker_ component. */

/**
 * Contains getters to cast the _ISwapeeWalletPickerElement_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerElementCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerElementCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerElement_ instance into the _BoundISwapeeWalletPickerElement_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerElement}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementCaster.prototype.asISwapeeWalletPickerElement
/**
 * Access the _SwapeeWalletPickerElement_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerElement}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementCaster.prototype.superSwapeeWalletPickerElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeWalletPickerMemory, props: !xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs) => Object<string, *>} xyz.swapee.wc.ISwapeeWalletPickerElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement.__solder<!xyz.swapee.wc.ISwapeeWalletPickerElement>} xyz.swapee.wc.ISwapeeWalletPickerElement._solder */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerMemory} model The model.
 * - `isSignedIn` _boolean_ Whether the person is signed in. Default `false`.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. Default `[]`.
 * - `walletsRotor` _Array&lt;!ISwapeeWalletPicker.WalletBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs} props The element props.
 * - `[isSignedIn=null]` _&#42;?_ Whether the person is signed in. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn* Default `null`.
 * - `[wallets=null]` _&#42;?_ The available wallets. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.Wallets* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.Wallets* Default `null`.
 * - `[walletsRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeWalletPickerDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeWalletPickerElementPort.Inputs.NoSolder* Default `false`.
 * - `[walletsOpts]` _!Object?_ The options to pass to the _Wallets_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.WalletsOpts* Default `{}`.
 * - `[swapeeAccountWrOpts]` _!Object?_ The options to pass to the _SwapeeAccountWr_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts* Default `{}`.
 * - `[getWalletBuOpts]` _!Object?_ The options to pass to the _GetWalletBu_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts* Default `{}`.
 * - `[swapeeLoginBuOpts]` _!Object?_ The options to pass to the _SwapeeLoginBu_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * - `[walletTemplateOpts]` _!Object?_ The options to pass to the _WalletTemplate_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeWalletPickerMemory, instance?: !xyz.swapee.wc.ISwapeeWalletPickerScreen&xyz.swapee.wc.ISwapeeWalletPickerController) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeWalletPickerElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement.__render<!xyz.swapee.wc.ISwapeeWalletPickerElement>} xyz.swapee.wc.ISwapeeWalletPickerElement._render */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerMemory} [model] The model for the view.
 * - `isSignedIn` _boolean_ Whether the person is signed in. Default `false`.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. Default `[]`.
 * - `walletsRotor` _Array&lt;!ISwapeeWalletPicker.WalletBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerScreen&xyz.swapee.wc.ISwapeeWalletPickerController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.ISwapeeWalletPickerElement.build.Cores, instances: !xyz.swapee.wc.ISwapeeWalletPickerElement.build.Instances) => ?} xyz.swapee.wc.ISwapeeWalletPickerElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement.__build<!xyz.swapee.wc.ISwapeeWalletPickerElement>} xyz.swapee.wc.ISwapeeWalletPickerElement._build */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerElement.build.Cores} cores The models of components on the land.
 * - `SwapeeMe` _!xyz.swapee.wc.ISwapeeMeCore.Model_
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `SwapeeMe` _!xyz.swapee.wc.ISwapeeMe_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.ISwapeeMeCore.Model} SwapeeMe
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.ISwapeeMe} SwapeeMe
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ISwapeeMeCore.Model, instance: !xyz.swapee.wc.ISwapeeMe) => ?} xyz.swapee.wc.ISwapeeWalletPickerElement.__buildSwapeeMe
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement.__buildSwapeeMe<!xyz.swapee.wc.ISwapeeWalletPickerElement>} xyz.swapee.wc.ISwapeeWalletPickerElement._buildSwapeeMe */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement.buildSwapeeMe} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ISwapeeMe_ component.
 * @param {!xyz.swapee.wc.ISwapeeMeCore.Model} model
 * - `[token=""]` _string?_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Token* Default empty string.
 * - `[userid=""]` _string?_ The id of the user. ⤴ *ISwapeeMeOuterCore.Model.Userid* Default empty string.
 * - `[username=""]` _string?_ The username obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Username* Default empty string.
 * - `[displayName=""]` _string?_ The display name obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.DisplayName* Default empty string.
 * - `[profile=""]` _string?_ The profile id. ⤴ *ISwapeeMeOuterCore.Model.Profile* Default empty string.
 * - `[userpic=""]` _string?_ The picture of the user. ⤴ *ISwapeeMeCore.Model.Userpic* Default empty string.
 * @param {!xyz.swapee.wc.ISwapeeMe} instance
 * @return {?}
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.buildSwapeeMe = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeWalletPickerMemory, ports: !xyz.swapee.wc.ISwapeeWalletPickerElement.short.Ports, cores: !xyz.swapee.wc.ISwapeeWalletPickerElement.short.Cores) => ?} xyz.swapee.wc.ISwapeeWalletPickerElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement.__short<!xyz.swapee.wc.ISwapeeWalletPickerElement>} xyz.swapee.wc.ISwapeeWalletPickerElement._short */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerMemory} model The model from which to feed properties to peer's ports.
 * - `isSignedIn` _boolean_ Whether the person is signed in. Default `false`.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. Default `[]`.
 * - `walletsRotor` _Array&lt;!ISwapeeWalletPicker.WalletBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerElement.short.Ports} ports The ports of the peers.
 * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMePortInterface_
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerElement.short.Cores} cores The cores of the peers.
 * - `SwapeeMe` _xyz.swapee.wc.ISwapeeMeCore.Model_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMePortInterface} SwapeeMe
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.ISwapeeMeCore.Model} SwapeeMe
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeWalletPickerMemory, inputs: !xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeWalletPickerElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement.__server<!xyz.swapee.wc.ISwapeeWalletPickerElement>} xyz.swapee.wc.ISwapeeWalletPickerElement._server */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerMemory} memory The memory registers.
 * - `isSignedIn` _boolean_ Whether the person is signed in. Default `false`.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. Default `[]`.
 * - `walletsRotor` _Array&lt;!ISwapeeWalletPicker.WalletBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs} inputs The inputs to the port.
 * - `[isSignedIn=null]` _&#42;?_ Whether the person is signed in. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn* Default `null`.
 * - `[wallets=null]` _&#42;?_ The available wallets. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.Wallets* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.Wallets* Default `null`.
 * - `[walletsRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeWalletPickerDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeWalletPickerElementPort.Inputs.NoSolder* Default `false`.
 * - `[walletsOpts]` _!Object?_ The options to pass to the _Wallets_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.WalletsOpts* Default `{}`.
 * - `[swapeeAccountWrOpts]` _!Object?_ The options to pass to the _SwapeeAccountWr_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts* Default `{}`.
 * - `[getWalletBuOpts]` _!Object?_ The options to pass to the _GetWalletBu_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts* Default `{}`.
 * - `[swapeeLoginBuOpts]` _!Object?_ The options to pass to the _SwapeeLoginBu_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * - `[walletTemplateOpts]` _!Object?_ The options to pass to the _WalletTemplate_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeWalletPickerMemory, port?: !xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs) => ?} xyz.swapee.wc.ISwapeeWalletPickerElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElement.__inducer<!xyz.swapee.wc.ISwapeeWalletPickerElement>} xyz.swapee.wc.ISwapeeWalletPickerElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerMemory} [model] The model of the component into which to induce the state.
 * - `isSignedIn` _boolean_ Whether the person is signed in. Default `false`.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. Default `[]`.
 * - `walletsRotor` _Array&lt;!ISwapeeWalletPicker.WalletBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeWalletPickerElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[isSignedIn=null]` _&#42;?_ Whether the person is signed in. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.IsSignedIn* Default `null`.
 * - `[wallets=null]` _&#42;?_ The available wallets. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.Wallets* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.Wallets* Default `null`.
 * - `[walletsRotor=null]` _&#42;?_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor* ⤴ *ISwapeeWalletPickerOuterCore.WeakModel.WalletsRotor* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeWalletPickerDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeWalletPickerElementPort.Inputs.NoSolder* Default `false`.
 * - `[walletsOpts]` _!Object?_ The options to pass to the _Wallets_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.WalletsOpts* Default `{}`.
 * - `[swapeeAccountWrOpts]` _!Object?_ The options to pass to the _SwapeeAccountWr_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts* Default `{}`.
 * - `[getWalletBuOpts]` _!Object?_ The options to pass to the _GetWalletBu_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts* Default `{}`.
 * - `[swapeeLoginBuOpts]` _!Object?_ The options to pass to the _SwapeeLoginBu_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * - `[walletTemplateOpts]` _!Object?_ The options to pass to the _WalletTemplate_ vdu. ⤴ *ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeWalletPickerElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/14-Records.xml}  99a31e59698e45f3ad2e47fc975f0be5 */
/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPicker.WalletStator The stator of the _WalletDynamo_.
 * @prop {string} address
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes A single wallet available to the user.
 * The brushes inside the rotor of the _WalletDynamo_.
 * @prop {string} address
 */

/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/140-ISwapeeWalletPickerElementPort.xml}  586eb793fe59d43f8d5e7808be9ee857 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerElementPort)} xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerElementPort} xyz.swapee.wc.SwapeeWalletPickerElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.constructor&xyz.swapee.wc.SwapeeWalletPickerElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerElementPort|typeof xyz.swapee.wc.SwapeeWalletPickerElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElementPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerElementPort|typeof xyz.swapee.wc.SwapeeWalletPickerElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElementPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerElementPort|typeof xyz.swapee.wc.SwapeeWalletPickerElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElementPort}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerElementPort.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerElementPort} xyz.swapee.wc.SwapeeWalletPickerElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs>)} xyz.swapee.wc.ISwapeeWalletPickerElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerElementPort
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort} xyz.swapee.wc.ISwapeeWalletPickerElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerElementPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerElementPort
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerElementPort = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerElementPort.constructor&xyz.swapee.wc.ISwapeeWalletPickerElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerElementPort}
 */
xyz.swapee.wc.SwapeeWalletPickerElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerElementPort.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerElementPortFields
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPortFields = class { }
/**
 * The inputs to the _ISwapeeWalletPickerElement_'s controller via its element port.
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElementPort} */
xyz.swapee.wc.RecordISwapeeWalletPickerElementPort

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerElementPort} xyz.swapee.wc.BoundISwapeeWalletPickerElementPort */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerElementPort} xyz.swapee.wc.BoundSwapeeWalletPickerElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _Wallets_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts.walletsOpts

/**
 * The options to pass to the _SwapeeAccountWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts.swapeeAccountWrOpts

/**
 * The options to pass to the _GetWalletBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts.getWalletBuOpts

/**
 * The options to pass to the _CreateInvoiceBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts.createInvoiceBuOpts

/**
 * The options to pass to the _SwapeeLoginBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts.swapeeLoginBuOpts

/**
 * The options to pass to the _InvoiceLoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts.invoiceLoInOpts

/**
 * The options to pass to the _SwapeeMe_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts.swapeeMeOpts

/**
 * The options to pass to the _WalletTemplate_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts.walletTemplateOpts

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts)} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts.typeof */
/**
 * The inputs to the _ISwapeeWalletPickerElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.constructor&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.NoSolder&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletsOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeAccountWrOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.GetWalletBuOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.CreateInvoiceBuOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeLoginBuOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.InvoiceLoInOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeMeOpts&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletTemplateOpts)} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletsOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletsOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeAccountWrOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeAccountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.GetWalletBuOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.GetWalletBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.CreateInvoiceBuOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.CreateInvoiceBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeLoginBuOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeLoginBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.InvoiceLoInOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.InvoiceLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeMeOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeMeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletTemplateOpts} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletTemplateOpts.typeof */
/**
 * The inputs to the _ISwapeeWalletPickerElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletsOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeAccountWrOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.GetWalletBuOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.CreateInvoiceBuOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeLoginBuOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.InvoiceLoInOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeMeOpts.typeof&xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletTemplateOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs

/**
 * Contains getters to cast the _ISwapeeWalletPickerElementPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerElementPortCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPortCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerElementPort_ instance into the _BoundISwapeeWalletPickerElementPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerElementPort}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPortCaster.prototype.asISwapeeWalletPickerElementPort
/**
 * Access the _SwapeeWalletPickerElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerElementPort}
 */
xyz.swapee.wc.ISwapeeWalletPickerElementPortCaster.prototype.superSwapeeWalletPickerElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts The options to pass to the _Wallets_ vdu (optional overlay).
 * @prop {!Object} [walletsOpts] The options to pass to the _Wallets_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletsOpts_Safe The options to pass to the _Wallets_ vdu (required overlay).
 * @prop {!Object} walletsOpts The options to pass to the _Wallets_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts The options to pass to the _SwapeeAccountWr_ vdu (optional overlay).
 * @prop {!Object} [swapeeAccountWrOpts] The options to pass to the _SwapeeAccountWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeAccountWrOpts_Safe The options to pass to the _SwapeeAccountWr_ vdu (required overlay).
 * @prop {!Object} swapeeAccountWrOpts The options to pass to the _SwapeeAccountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts The options to pass to the _GetWalletBu_ vdu (optional overlay).
 * @prop {!Object} [getWalletBuOpts] The options to pass to the _GetWalletBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.GetWalletBuOpts_Safe The options to pass to the _GetWalletBu_ vdu (required overlay).
 * @prop {!Object} getWalletBuOpts The options to pass to the _GetWalletBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts The options to pass to the _CreateInvoiceBu_ vdu (optional overlay).
 * @prop {!Object} [createInvoiceBuOpts] The options to pass to the _CreateInvoiceBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.CreateInvoiceBuOpts_Safe The options to pass to the _CreateInvoiceBu_ vdu (required overlay).
 * @prop {!Object} createInvoiceBuOpts The options to pass to the _CreateInvoiceBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts The options to pass to the _SwapeeLoginBu_ vdu (optional overlay).
 * @prop {!Object} [swapeeLoginBuOpts] The options to pass to the _SwapeeLoginBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeLoginBuOpts_Safe The options to pass to the _SwapeeLoginBu_ vdu (required overlay).
 * @prop {!Object} swapeeLoginBuOpts The options to pass to the _SwapeeLoginBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts The options to pass to the _InvoiceLoIn_ vdu (optional overlay).
 * @prop {!Object} [invoiceLoInOpts] The options to pass to the _InvoiceLoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.InvoiceLoInOpts_Safe The options to pass to the _InvoiceLoIn_ vdu (required overlay).
 * @prop {!Object} invoiceLoInOpts The options to pass to the _InvoiceLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts The options to pass to the _SwapeeMe_ vdu (optional overlay).
 * @prop {!Object} [swapeeMeOpts] The options to pass to the _SwapeeMe_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.SwapeeMeOpts_Safe The options to pass to the _SwapeeMe_ vdu (required overlay).
 * @prop {!Object} swapeeMeOpts The options to pass to the _SwapeeMe_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts The options to pass to the _WalletTemplate_ vdu (optional overlay).
 * @prop {!Object} [walletTemplateOpts] The options to pass to the _WalletTemplate_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.Inputs.WalletTemplateOpts_Safe The options to pass to the _WalletTemplate_ vdu (required overlay).
 * @prop {!Object} walletTemplateOpts The options to pass to the _WalletTemplate_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletsOpts The options to pass to the _Wallets_ vdu (optional overlay).
 * @prop {*} [walletsOpts=null] The options to pass to the _Wallets_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletsOpts_Safe The options to pass to the _Wallets_ vdu (required overlay).
 * @prop {*} walletsOpts The options to pass to the _Wallets_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeAccountWrOpts The options to pass to the _SwapeeAccountWr_ vdu (optional overlay).
 * @prop {*} [swapeeAccountWrOpts=null] The options to pass to the _SwapeeAccountWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeAccountWrOpts_Safe The options to pass to the _SwapeeAccountWr_ vdu (required overlay).
 * @prop {*} swapeeAccountWrOpts The options to pass to the _SwapeeAccountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.GetWalletBuOpts The options to pass to the _GetWalletBu_ vdu (optional overlay).
 * @prop {*} [getWalletBuOpts=null] The options to pass to the _GetWalletBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.GetWalletBuOpts_Safe The options to pass to the _GetWalletBu_ vdu (required overlay).
 * @prop {*} getWalletBuOpts The options to pass to the _GetWalletBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.CreateInvoiceBuOpts The options to pass to the _CreateInvoiceBu_ vdu (optional overlay).
 * @prop {*} [createInvoiceBuOpts=null] The options to pass to the _CreateInvoiceBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.CreateInvoiceBuOpts_Safe The options to pass to the _CreateInvoiceBu_ vdu (required overlay).
 * @prop {*} createInvoiceBuOpts The options to pass to the _CreateInvoiceBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeLoginBuOpts The options to pass to the _SwapeeLoginBu_ vdu (optional overlay).
 * @prop {*} [swapeeLoginBuOpts=null] The options to pass to the _SwapeeLoginBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeLoginBuOpts_Safe The options to pass to the _SwapeeLoginBu_ vdu (required overlay).
 * @prop {*} swapeeLoginBuOpts The options to pass to the _SwapeeLoginBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.InvoiceLoInOpts The options to pass to the _InvoiceLoIn_ vdu (optional overlay).
 * @prop {*} [invoiceLoInOpts=null] The options to pass to the _InvoiceLoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.InvoiceLoInOpts_Safe The options to pass to the _InvoiceLoIn_ vdu (required overlay).
 * @prop {*} invoiceLoInOpts The options to pass to the _InvoiceLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeMeOpts The options to pass to the _SwapeeMe_ vdu (optional overlay).
 * @prop {*} [swapeeMeOpts=null] The options to pass to the _SwapeeMe_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.SwapeeMeOpts_Safe The options to pass to the _SwapeeMe_ vdu (required overlay).
 * @prop {*} swapeeMeOpts The options to pass to the _SwapeeMe_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletTemplateOpts The options to pass to the _WalletTemplate_ vdu (optional overlay).
 * @prop {*} [walletTemplateOpts=null] The options to pass to the _WalletTemplate_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerElementPort.WeakInputs.WalletTemplateOpts_Safe The options to pass to the _WalletTemplate_ vdu (required overlay).
 * @prop {*} walletTemplateOpts The options to pass to the _WalletTemplate_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/150-ISwapeeWalletPickerGenerator.xml}  72f4111fe934f4dc475c273a49c06457 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerGenerator)} xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerGenerator} xyz.swapee.wc.SwapeeWalletPickerGenerator.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerGenerator` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.constructor&xyz.swapee.wc.SwapeeWalletPickerGenerator.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerGenerator|typeof xyz.swapee.wc.SwapeeWalletPickerGenerator)|!xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGenerator}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGenerator}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerGenerator|typeof xyz.swapee.wc.SwapeeWalletPickerGenerator)|!xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGenerator}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerGenerator|typeof xyz.swapee.wc.SwapeeWalletPickerGenerator)|!xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGenerator}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGenerator.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerGenerator} xyz.swapee.wc.SwapeeWalletPickerGeneratorConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice = class {
  constructor() {
    /**
     * Assigns properties to the stator from the brush or brushes.
     */
    this.brushWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator._brushWallet|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator._brushWallet>} */ (void 0)
    /**
     * Allows to execute paint methods reactively when the state changes.
     */
    this.paintWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator._paintWallet|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator._paintWallet>} */ (void 0)
    /**
     * Is executed when the stator is created for the element and allows to assign
     * interrupt listeners bound to the stator.
     */
    this.assignWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator._assignWallet|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator._assignWallet>} */ (void 0)
    /**
     * Creates a component via a capacitor and establishes circuits between its port
     * and the stator.
     */
    this.initWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator._initWallet|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator._initWallet>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice

/**
 * A concrete class of _ISwapeeWalletPickerGeneratorHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerGeneratorHyperslice
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.SwapeeWalletPickerGeneratorHyperslice = class extends xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice { }
xyz.swapee.wc.SwapeeWalletPickerGeneratorHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerGeneratorHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice = class {
  constructor() {
    /**
     * Assigns properties to the stator from the brush or brushes.
     */
    this.brushWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__brushWallet<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__brushWallet<THIS>>} */ (void 0)
    /**
     * Allows to execute paint methods reactively when the state changes.
     */
    this.paintWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__paintWallet<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__paintWallet<THIS>>} */ (void 0)
    /**
     * Is executed when the stator is created for the element and allows to assign
     * interrupt listeners bound to the stator.
     */
    this.assignWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__assignWallet<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__assignWallet<THIS>>} */ (void 0)
    /**
     * Creates a component via a capacitor and establishes circuits between its port
     * and the stator.
     */
    this.initWallet=/** @type {!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__initWallet<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeWalletPickerGenerator.__initWallet<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice

/**
 * A concrete class of _ISwapeeWalletPickerGeneratorBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerGeneratorBindingHyperslice
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice<THIS>}
 */
xyz.swapee.wc.SwapeeWalletPickerGeneratorBindingHyperslice = class extends xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice { }
xyz.swapee.wc.SwapeeWalletPickerGeneratorBindingHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerGeneratorBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerGeneratorFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerGeneratorCaster)} xyz.swapee.wc.ISwapeeWalletPickerGenerator.constructor */
/**
 * Responsible for generation of component instances to place inside containers.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGenerator
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerGenerator* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.brushWallet} */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.prototype.brushWallet = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.paintWallet} */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.prototype.paintWallet = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.assignWallet} */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.prototype.assignWallet = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.stateWallet} */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.prototype.stateWallet = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.initWallet} */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.prototype.initWallet = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.WalletDynamo} */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.prototype.WalletDynamo = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerGenerator&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerGenerator.constructor */
/**
 * A concrete class of _ISwapeeWalletPickerGenerator_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerGenerator
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerGenerator} Responsible for generation of component instances to place inside containers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerGenerator = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerGenerator.constructor&xyz.swapee.wc.ISwapeeWalletPickerGenerator.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerGenerator* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerGenerator* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerGenerator.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerGenerator.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGenerator}
 */
xyz.swapee.wc.SwapeeWalletPickerGenerator.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerGenerator.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGeneratorFields
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorFields = class { }
/**
 * The land accessible to the generee.
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorFields.prototype.land = /** @type {!xyz.swapee.wc.SwapeeWalletPickerLand} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator} */
xyz.swapee.wc.RecordISwapeeWalletPickerGenerator

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator} xyz.swapee.wc.BoundISwapeeWalletPickerGenerator */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerGenerator} xyz.swapee.wc.BoundSwapeeWalletPickerGenerator */

/**
 * Contains getters to cast the _ISwapeeWalletPickerGenerator_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGeneratorCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerGenerator_ instance into the _BoundISwapeeWalletPickerGenerator_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerGenerator}
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorCaster.prototype.asISwapeeWalletPickerGenerator
/**
 * Cast the _ISwapeeWalletPickerGenerator_ instance into the _BoundISwapeeWalletPickerHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerHtmlComponent}
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorCaster.prototype.asISwapeeWalletPickerHtmlComponent
/**
 * Access the _SwapeeWalletPickerGenerator_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerGenerator}
 */
xyz.swapee.wc.ISwapeeWalletPickerGeneratorCaster.prototype.superSwapeeWalletPickerGenerator

/**
 * @typedef {(this: THIS, brushes: !xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes, stator: !com.webcircuits.IStator<!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator>) => void} xyz.swapee.wc.ISwapeeWalletPickerGenerator.__brushWallet
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator.__brushWallet<!xyz.swapee.wc.ISwapeeWalletPickerGenerator>} xyz.swapee.wc.ISwapeeWalletPickerGenerator._brushWallet */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGenerator.brushWallet} */
/**
 * Assigns properties to the stator from the brush or brushes. `🔗 $combine`
 * @param {!xyz.swapee.wc.ISwapeeWalletPicker.WalletBrushes} brushes The brushes.
 * @param {!com.webcircuits.IStator<!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator>} stator The stator.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.brushWallet = function(brushes, stator) {}

/**
 * @typedef {(this: THIS, state: !xyz.swapee.wc.ISwapeeWalletPicker.WalletStator, element: !com.webcircuits.IHtmlTwin) => void} xyz.swapee.wc.ISwapeeWalletPickerGenerator.__paintWallet
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator.__paintWallet<!xyz.swapee.wc.ISwapeeWalletPickerGenerator>} xyz.swapee.wc.ISwapeeWalletPickerGenerator._paintWallet */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGenerator.paintWallet} */
/**
 * Allows to execute paint methods reactively when the state changes. `🔗 $combine`
 * @param {!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator} state The state of the stator.
 * @param {!com.webcircuits.IHtmlTwin} element The element.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.paintWallet = function(state, element) {}

/**
 * @typedef {(this: THIS, state: !xyz.swapee.wc.ISwapeeWalletPicker.WalletStator, element: !com.webcircuits.IHtmlTwin, land: !xyz.swapee.wc.SwapeeWalletPickerLand) => void} xyz.swapee.wc.ISwapeeWalletPickerGenerator.__assignWallet
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator.__assignWallet<!xyz.swapee.wc.ISwapeeWalletPickerGenerator>} xyz.swapee.wc.ISwapeeWalletPickerGenerator._assignWallet */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGenerator.assignWallet} */
/**
 * Is executed when the stator is created for the element and allows to assign
 * interrupt listeners bound to the stator. `🔗 $combine`
 * @param {!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator} state The state of the stator.
 * @param {!com.webcircuits.IHtmlTwin} element The element.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerLand} land The land.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.assignWallet = function(state, element, land) {}

/**
 * @typedef {(this: THIS, el: !com.webcircuits.IHtmlTwin) => !com.webcircuits.IStator<!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator>} xyz.swapee.wc.ISwapeeWalletPickerGenerator.__stateWallet
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator.__stateWallet<!xyz.swapee.wc.ISwapeeWalletPickerGenerator>} xyz.swapee.wc.ISwapeeWalletPickerGenerator._stateWallet */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGenerator.stateWallet} */
/**
 * Builds a stator for an element.
 * @param {!com.webcircuits.IHtmlTwin} el The element against which to build the stator.
 * @return {!com.webcircuits.IStator<!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator>}
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.stateWallet = function(el) {}

/**
 * @typedef {(this: THIS, stator: !com.webcircuits.IStator<!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator>, el: !com.webcircuits.IHtmlTwin) => void} xyz.swapee.wc.ISwapeeWalletPickerGenerator.__initWallet
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator.__initWallet<!xyz.swapee.wc.ISwapeeWalletPickerGenerator>} xyz.swapee.wc.ISwapeeWalletPickerGenerator._initWallet */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGenerator.initWallet} */
/**
 * Creates a component via a capacitor and establishes circuits between its port
 * and the stator. `🔗 $combine`
 * @param {!com.webcircuits.IStator<!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator>} stator The stator.
 * @param {!com.webcircuits.IHtmlTwin} el The element that will underlie the component.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.initWallet = function(stator, el) {}

/**
 * @typedef {(this: THIS, stator: !xyz.swapee.wc.ISwapeeWalletPicker.WalletStator, land: !xyz.swapee.wc.SwapeeWalletPickerLand&{SwapeeWalletPicker:!xyz.swapee.wc.ISwapeeWalletPickerController}) => !Object} xyz.swapee.wc.ISwapeeWalletPickerGenerator.__WalletDynamo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGenerator.__WalletDynamo<!xyz.swapee.wc.ISwapeeWalletPickerGenerator>} xyz.swapee.wc.ISwapeeWalletPickerGenerator._WalletDynamo */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGenerator.WalletDynamo} */
/**
 * The method that links the stator to the component.
 * @param {!xyz.swapee.wc.ISwapeeWalletPicker.WalletStator} stator The stator.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerLand&{SwapeeWalletPicker:!xyz.swapee.wc.ISwapeeWalletPickerController}} land The land.
 * @return {!Object}
 */
xyz.swapee.wc.ISwapeeWalletPickerGenerator.WalletDynamo = function(stator, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerGenerator
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/170-ISwapeeWalletPickerDesigner.xml}  6e25b0aa39f850cc08dbfabe26577095 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerDesigner
 */
xyz.swapee.wc.ISwapeeWalletPickerDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.SwapeeWalletPickerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeWalletPicker />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeWalletPickerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeWalletPicker />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeWalletPickerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeWalletPickerDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMeController_
   * - `SwapeeWalletPicker` _typeof ISwapeeWalletPickerController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeWalletPickerDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMeController_
   * - `SwapeeWalletPicker` _typeof ISwapeeWalletPickerController_
   * - `This` _typeof ISwapeeWalletPickerController_
   * @param {!xyz.swapee.wc.ISwapeeWalletPickerDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `SwapeeMe` _!xyz.swapee.wc.SwapeeMeMemory_
   * - `SwapeeWalletPicker` _!SwapeeWalletPickerMemory_
   * - `This` _!SwapeeWalletPickerMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeWalletPickerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeWalletPickerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ISwapeeWalletPickerDesigner.prototype.constructor = xyz.swapee.wc.ISwapeeWalletPickerDesigner

/**
 * A concrete class of _ISwapeeWalletPickerDesigner_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerDesigner
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.SwapeeWalletPickerDesigner = class extends xyz.swapee.wc.ISwapeeWalletPickerDesigner { }
xyz.swapee.wc.SwapeeWalletPickerDesigner.prototype.constructor = xyz.swapee.wc.SwapeeWalletPickerDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeWalletPickerController} SwapeeWalletPicker
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeWalletPickerController} SwapeeWalletPicker
 * @prop {typeof xyz.swapee.wc.ISwapeeWalletPickerController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe
 * @prop {!xyz.swapee.wc.SwapeeWalletPickerMemory} SwapeeWalletPicker
 * @prop {!xyz.swapee.wc.SwapeeWalletPickerMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/200-SwapeeWalletPickerLand.xml}  a67f727991c12fea746decf73dc01f66 */
/**
 * The surrounding of the _ISwapeeWalletPicker_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.SwapeeWalletPickerLand
 */
xyz.swapee.wc.SwapeeWalletPickerLand = class { }
/**
 *
 */
xyz.swapee.wc.SwapeeWalletPickerLand.prototype.SwapeeMe = /** @type {xyz.swapee.wc.ISwapeeMe} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/40-ISwapeeWalletPickerDisplay.xml}  84a23f54d5e5a547dc8a165718f061c0 */
/**
 * @typedef {Object} $xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese
 * @prop {HTMLDivElement} [Wallets] The container for wallets.
 * @prop {HTMLDivElement} [SwapeeAccountWr] The wrapper for login/sign up buttons.
 * @prop {HTMLButtonElement} [GetWalletBu] The button to sign up with swapee.me and get a wallet.
 * @prop {HTMLButtonElement} [CreateInvoiceBu] Generates an invoice to get the _BitCoin_ address.
 * @prop {HTMLButtonElement} [SwapeeLoginBu] The button to login with swapee.me to list wallets.
 * @prop {HTMLDivElement} [InvoiceLoIn]
 * @prop {HTMLElement} [SwapeeMe] The via for the _SwapeeMe_ peer.
 * @prop {HTMLDivElement} [WalletTemplate] The template VDU for _Wallet_.
 */
/** @typedef {$xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ISwapeeWalletPickerDisplay.Settings>} xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerDisplay)} xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerDisplay} xyz.swapee.wc.SwapeeWalletPickerDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.constructor&xyz.swapee.wc.SwapeeWalletPickerDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.SwapeeWalletPickerDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.SwapeeWalletPickerDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.SwapeeWalletPickerDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerDisplay} xyz.swapee.wc.SwapeeWalletPickerDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.SwapeeWalletPickerMemory, !HTMLDivElement, !xyz.swapee.wc.ISwapeeWalletPickerDisplay.Settings, xyz.swapee.wc.ISwapeeWalletPickerDisplay.Queries, null>)} xyz.swapee.wc.ISwapeeWalletPickerDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ISwapeeWalletPicker_.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerDisplay
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplay = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerDisplay.paint} */
xyz.swapee.wc.ISwapeeWalletPickerDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerDisplay} xyz.swapee.wc.ISwapeeWalletPickerDisplay.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerDisplay_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerDisplay
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerDisplay} Display for presenting information from the _ISwapeeWalletPicker_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerDisplay = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerDisplay.constructor&xyz.swapee.wc.ISwapeeWalletPickerDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.SwapeeWalletPickerDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerDisplay.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerDisplayFields
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerDisplay.Queries} */ (void 0)
/**
 * The container for wallets. Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.Wallets = /** @type {HTMLDivElement} */ (void 0)
/**
 * The wrapper for login/sign up buttons. Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.SwapeeAccountWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * The button to sign up with swapee.me and get a wallet. Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.GetWalletBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Generates an invoice to get the _BitCoin_ address. Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.CreateInvoiceBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * The button to login with swapee.me to list wallets. Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.SwapeeLoginBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.InvoiceLoIn = /** @type {HTMLDivElement} */ (void 0)
/**
 * The via for the _SwapeeMe_ peer. Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.SwapeeMe = /** @type {HTMLElement} */ (void 0)
/**
 * The template VDU for _Wallet_. Default `null`.
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayFields.prototype.WalletTemplate = /** @type {HTMLDivElement} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerDisplay} */
xyz.swapee.wc.RecordISwapeeWalletPickerDisplay

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerDisplay} xyz.swapee.wc.BoundISwapeeWalletPickerDisplay */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerDisplay} xyz.swapee.wc.BoundSwapeeWalletPickerDisplay */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerDisplay.Queries} xyz.swapee.wc.ISwapeeWalletPickerDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeWalletPickerDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [swapeeMeSel=""] The query to discover the _SwapeeMe_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _ISwapeeWalletPickerDisplay_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerDisplayCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerDisplay_ instance into the _BoundISwapeeWalletPickerDisplay_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayCaster.prototype.asISwapeeWalletPickerDisplay
/**
 * Cast the _ISwapeeWalletPickerDisplay_ instance into the _BoundISwapeeWalletPickerScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerScreen}
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayCaster.prototype.asISwapeeWalletPickerScreen
/**
 * Access the _SwapeeWalletPickerDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplayCaster.prototype.superSwapeeWalletPickerDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeWalletPickerMemory, land: null) => void} xyz.swapee.wc.ISwapeeWalletPickerDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerDisplay.__paint<!xyz.swapee.wc.ISwapeeWalletPickerDisplay>} xyz.swapee.wc.ISwapeeWalletPickerDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerMemory} memory The display data.
 * - `isSignedIn` _boolean_ Whether the person is signed in. Default `false`.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. Default `[]`.
 * - `invoice` _string_ The _BitCoin_ address. Default empty string.
 * - `loadCreateInvoice` _boolean_ Starts creating an invoice. Default `false`.
 * - `walletsRotor` _Array&lt;!ISwapeeWalletPicker.WalletBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/40-ISwapeeWalletPickerDisplayBack.xml}  7776426425330ef41a31e97eff2a7d9d */
/**
 * @typedef {Object} $xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [Wallets] The container for wallets.
 * @prop {!com.webcircuits.IHtmlTwin} [SwapeeAccountWr] The wrapper for login/sign up buttons.
 * @prop {!com.webcircuits.IHtmlTwin} [GetWalletBu] The button to sign up with swapee.me and get a wallet.
 * @prop {!com.webcircuits.IHtmlTwin} [CreateInvoiceBu] Generates an invoice to get the _BitCoin_ address.
 * @prop {!com.webcircuits.IHtmlTwin} [SwapeeLoginBu] The button to login with swapee.me to list wallets.
 * @prop {!com.webcircuits.IHtmlTwin} [InvoiceLoIn]
 * @prop {!com.webcircuits.IHtmlTwin} [SwapeeMe] The via for the _SwapeeMe_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [WalletTemplate] The template VDU for _Wallet_.
 */
/** @typedef {$xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.SwapeeWalletPickerClasses>} xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeWalletPickerDisplay)} xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay} xyz.swapee.wc.back.SwapeeWalletPickerDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeWalletPickerDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.constructor&xyz.swapee.wc.back.SwapeeWalletPickerDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeWalletPickerDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.SwapeeWalletPickerClasses, !xyz.swapee.wc.SwapeeWalletPickerLand>)} xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerDisplay
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplay = class extends /** @type {xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.paint} */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeWalletPickerDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.Initialese>)} xyz.swapee.wc.back.SwapeeWalletPickerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeWalletPickerDisplay} xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerDisplay_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeWalletPickerDisplay
 * @implements {xyz.swapee.wc.back.ISwapeeWalletPickerDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeWalletPickerDisplay = class extends /** @type {xyz.swapee.wc.back.SwapeeWalletPickerDisplay.constructor&xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.SwapeeWalletPickerDisplay.prototype.constructor = xyz.swapee.wc.back.SwapeeWalletPickerDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.back.SwapeeWalletPickerDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerDisplay.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields = class { }
/**
 * The container for wallets.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.Wallets = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The wrapper for login/sign up buttons.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.SwapeeAccountWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The button to sign up with swapee.me and get a wallet.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.GetWalletBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * Generates an invoice to get the _BitCoin_ address.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.CreateInvoiceBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The button to login with swapee.me to list wallets.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.SwapeeLoginBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.InvoiceLoIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _SwapeeMe_ peer.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.SwapeeMe = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The template VDU for _Wallet_.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayFields.prototype.WalletTemplate = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerDisplay} */
xyz.swapee.wc.back.RecordISwapeeWalletPickerDisplay

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerDisplay} xyz.swapee.wc.back.BoundISwapeeWalletPickerDisplay */

/** @typedef {xyz.swapee.wc.back.SwapeeWalletPickerDisplay} xyz.swapee.wc.back.BoundSwapeeWalletPickerDisplay */

/**
 * Contains getters to cast the _ISwapeeWalletPickerDisplay_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerDisplayCaster
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerDisplay_ instance into the _BoundISwapeeWalletPickerDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayCaster.prototype.asISwapeeWalletPickerDisplay
/**
 * Access the _SwapeeWalletPickerDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeWalletPickerDisplay}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplayCaster.prototype.superSwapeeWalletPickerDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.SwapeeWalletPickerMemory, land?: !xyz.swapee.wc.SwapeeWalletPickerLand) => void} xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.__paint<!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay>} xyz.swapee.wc.back.ISwapeeWalletPickerDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerMemory} [memory] The display data.
 * - `isSignedIn` _boolean_ Whether the person is signed in. Default `false`.
 * - `wallets` _!Array&lt;string&gt;_ The available wallets. Default `[]`.
 * - `invoice` _string_ The _BitCoin_ address. Default empty string.
 * - `loadCreateInvoice` _boolean_ Starts creating an invoice. Default `false`.
 * - `walletsRotor` _Array&lt;!ISwapeeWalletPicker.WalletBrushes&gt;_ An iterable rotor with brushes that will be passed to the `brushWallet` method to transfer to the stator. Default `null`.
 * @param {!xyz.swapee.wc.SwapeeWalletPickerLand} [land] The land data.
 * - `SwapeeMe` _xyz.swapee.wc.ISwapeeMe_
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeWalletPickerDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/41-SwapeeWalletPickerClasses.xml}  4143c6b0f17befcbd5a5ebc6406ed37e */
/**
 * The classes of the _ISwapeeWalletPickerDisplay_.
 * @record xyz.swapee.wc.SwapeeWalletPickerClasses
 */
xyz.swapee.wc.SwapeeWalletPickerClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.SwapeeWalletPickerClasses.prototype.Class = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.SwapeeWalletPickerClasses.prototype.props = /** @type {xyz.swapee.wc.SwapeeWalletPickerClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/50-ISwapeeWalletPickerController.xml}  6bff2d4864488d8e018df055e6723dee */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs, !xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel>} xyz.swapee.wc.ISwapeeWalletPickerController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerController)} xyz.swapee.wc.AbstractSwapeeWalletPickerController.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerController} xyz.swapee.wc.SwapeeWalletPickerController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerController` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerController
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerController.constructor&xyz.swapee.wc.SwapeeWalletPickerController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerController.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerController.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerController}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerController}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerController}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerController}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerController.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerController} xyz.swapee.wc.SwapeeWalletPickerControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs, !xyz.swapee.wc.ISwapeeWalletPickerOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ISwapeeWalletPickerOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs, !xyz.swapee.wc.ISwapeeWalletPickerController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs, !xyz.swapee.wc.SwapeeWalletPickerMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs>)} xyz.swapee.wc.ISwapeeWalletPickerController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerController
 */
xyz.swapee.wc.ISwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerController.resetPort} */
xyz.swapee.wc.ISwapeeWalletPickerController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerController.pulseLoadCreateInvoice} */
xyz.swapee.wc.ISwapeeWalletPickerController.prototype.pulseLoadCreateInvoice = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerController&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerController.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerController.constructor */
/**
 * A concrete class of _ISwapeeWalletPickerController_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerController
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerController.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerController.constructor&xyz.swapee.wc.ISwapeeWalletPickerController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerController}
 */
xyz.swapee.wc.SwapeeWalletPickerController.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerController.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerControllerFields
 */
xyz.swapee.wc.ISwapeeWalletPickerControllerFields = class { }
/**
 * The inputs to the _ISwapeeWalletPicker_'s controller.
 */
xyz.swapee.wc.ISwapeeWalletPickerControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeWalletPickerControllerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeWalletPickerController} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerController} */
xyz.swapee.wc.RecordISwapeeWalletPickerController

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerController} xyz.swapee.wc.BoundISwapeeWalletPickerController */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerController} xyz.swapee.wc.BoundSwapeeWalletPickerController */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerPort.Inputs} xyz.swapee.wc.ISwapeeWalletPickerController.Inputs The inputs to the _ISwapeeWalletPicker_'s controller. */

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerPort.WeakInputs} xyz.swapee.wc.ISwapeeWalletPickerController.WeakInputs The inputs to the _ISwapeeWalletPicker_'s controller. */

/**
 * Contains getters to cast the _ISwapeeWalletPickerController_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerControllerCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerControllerCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerController_ instance into the _BoundISwapeeWalletPickerController_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerController}
 */
xyz.swapee.wc.ISwapeeWalletPickerControllerCaster.prototype.asISwapeeWalletPickerController
/**
 * Cast the _ISwapeeWalletPickerController_ instance into the _BoundISwapeeWalletPickerProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerProcessor}
 */
xyz.swapee.wc.ISwapeeWalletPickerControllerCaster.prototype.asISwapeeWalletPickerProcessor
/**
 * Access the _SwapeeWalletPickerController_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerController}
 */
xyz.swapee.wc.ISwapeeWalletPickerControllerCaster.prototype.superSwapeeWalletPickerController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerController.__resetPort<!xyz.swapee.wc.ISwapeeWalletPickerController>} xyz.swapee.wc.ISwapeeWalletPickerController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeWalletPickerController.__pulseLoadCreateInvoice
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerController.__pulseLoadCreateInvoice<!xyz.swapee.wc.ISwapeeWalletPickerController>} xyz.swapee.wc.ISwapeeWalletPickerController._pulseLoadCreateInvoice */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerController.pulseLoadCreateInvoice} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeWalletPickerController.pulseLoadCreateInvoice = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/51-ISwapeeWalletPickerControllerFront.xml}  7b77e54876e1b0f1ea34c6ed8a5a1a10 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ISwapeeWalletPickerController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeWalletPickerController)} xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeWalletPickerController} xyz.swapee.wc.front.SwapeeWalletPickerController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeWalletPickerController` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeWalletPickerController
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.constructor&xyz.swapee.wc.front.SwapeeWalletPickerController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeWalletPickerController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerController|typeof xyz.swapee.wc.front.SwapeeWalletPickerController)|(!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT|typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerController}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerController}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerController|typeof xyz.swapee.wc.front.SwapeeWalletPickerController)|(!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT|typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerController}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerController|typeof xyz.swapee.wc.front.SwapeeWalletPickerController)|(!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT|typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerController}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeWalletPickerController.Initialese[]) => xyz.swapee.wc.front.ISwapeeWalletPickerController} xyz.swapee.wc.front.SwapeeWalletPickerControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeWalletPickerControllerCaster&xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT)} xyz.swapee.wc.front.ISwapeeWalletPickerController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT} xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ISwapeeWalletPickerController
 */
xyz.swapee.wc.front.ISwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.front.ISwapeeWalletPickerController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeWalletPickerController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.ISwapeeWalletPickerController.pulseLoadCreateInvoice} */
xyz.swapee.wc.front.ISwapeeWalletPickerController.prototype.pulseLoadCreateInvoice = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeWalletPickerController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeWalletPickerController.Initialese>)} xyz.swapee.wc.front.SwapeeWalletPickerController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeWalletPickerController} xyz.swapee.wc.front.ISwapeeWalletPickerController.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerController_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeWalletPickerController
 * @implements {xyz.swapee.wc.front.ISwapeeWalletPickerController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeWalletPickerController.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.front.SwapeeWalletPickerController.constructor&xyz.swapee.wc.front.ISwapeeWalletPickerController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeWalletPickerController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerController}
 */
xyz.swapee.wc.front.SwapeeWalletPickerController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeWalletPickerController} */
xyz.swapee.wc.front.RecordISwapeeWalletPickerController

/** @typedef {xyz.swapee.wc.front.ISwapeeWalletPickerController} xyz.swapee.wc.front.BoundISwapeeWalletPickerController */

/** @typedef {xyz.swapee.wc.front.SwapeeWalletPickerController} xyz.swapee.wc.front.BoundSwapeeWalletPickerController */

/**
 * Contains getters to cast the _ISwapeeWalletPickerController_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeWalletPickerControllerCaster
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerController_ instance into the _BoundISwapeeWalletPickerController_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeWalletPickerController}
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerCaster.prototype.asISwapeeWalletPickerController
/**
 * Access the _SwapeeWalletPickerController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeWalletPickerController}
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerCaster.prototype.superSwapeeWalletPickerController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeWalletPickerController.__pulseLoadCreateInvoice
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeWalletPickerController.__pulseLoadCreateInvoice<!xyz.swapee.wc.front.ISwapeeWalletPickerController>} xyz.swapee.wc.front.ISwapeeWalletPickerController._pulseLoadCreateInvoice */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeWalletPickerController.pulseLoadCreateInvoice} */
/** @return {void} */
xyz.swapee.wc.front.ISwapeeWalletPickerController.pulseLoadCreateInvoice = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.ISwapeeWalletPickerController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/52-ISwapeeWalletPickerControllerBack.xml}  ee99f404770cc75629f1715089022d32 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs>&xyz.swapee.wc.ISwapeeWalletPickerController.Initialese} xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeWalletPickerController)} xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeWalletPickerController} xyz.swapee.wc.back.SwapeeWalletPickerController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeWalletPickerController` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeWalletPickerController
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.constructor&xyz.swapee.wc.back.SwapeeWalletPickerController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeWalletPickerController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerController|typeof xyz.swapee.wc.back.SwapeeWalletPickerController)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerController}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerController}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerController|typeof xyz.swapee.wc.back.SwapeeWalletPickerController)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerController}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerController|typeof xyz.swapee.wc.back.SwapeeWalletPickerController)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerController}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese[]) => xyz.swapee.wc.back.ISwapeeWalletPickerController} xyz.swapee.wc.back.SwapeeWalletPickerControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeWalletPickerControllerCaster&xyz.swapee.wc.ISwapeeWalletPickerController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ISwapeeWalletPickerController.Inputs>)} xyz.swapee.wc.back.ISwapeeWalletPickerController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerController
 */
xyz.swapee.wc.back.ISwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.back.ISwapeeWalletPickerController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeWalletPickerController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeWalletPickerController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese>)} xyz.swapee.wc.back.SwapeeWalletPickerController.constructor */
/**
 * A concrete class of _ISwapeeWalletPickerController_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeWalletPickerController
 * @implements {xyz.swapee.wc.back.ISwapeeWalletPickerController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeWalletPickerController = class extends /** @type {xyz.swapee.wc.back.SwapeeWalletPickerController.constructor&xyz.swapee.wc.back.ISwapeeWalletPickerController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeWalletPickerController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerController}
 */
xyz.swapee.wc.back.SwapeeWalletPickerController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerController} */
xyz.swapee.wc.back.RecordISwapeeWalletPickerController

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerController} xyz.swapee.wc.back.BoundISwapeeWalletPickerController */

/** @typedef {xyz.swapee.wc.back.SwapeeWalletPickerController} xyz.swapee.wc.back.BoundSwapeeWalletPickerController */

/**
 * Contains getters to cast the _ISwapeeWalletPickerController_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerControllerCaster
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerController_ instance into the _BoundISwapeeWalletPickerController_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeWalletPickerController}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerCaster.prototype.asISwapeeWalletPickerController
/**
 * Access the _SwapeeWalletPickerController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeWalletPickerController}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerCaster.prototype.superSwapeeWalletPickerController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/53-ISwapeeWalletPickerControllerAR.xml}  75b868a9a97519eacba0adc7fad297dd */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeWalletPickerController.Initialese} xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeWalletPickerControllerAR)} xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR} xyz.swapee.wc.back.SwapeeWalletPickerControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.constructor&xyz.swapee.wc.back.SwapeeWalletPickerControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR|typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR|typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR|typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeWalletPickerController|typeof xyz.swapee.wc.SwapeeWalletPickerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.Initialese[]) => xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR} xyz.swapee.wc.back.SwapeeWalletPickerControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeWalletPickerControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeWalletPickerController)} xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeWalletPickerControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR = class extends /** @type {xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeWalletPickerController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.Initialese>)} xyz.swapee.wc.back.SwapeeWalletPickerControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR} xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeWalletPickerControllerAR
 * @implements {xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeWalletPickerControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeWalletPickerControllerAR = class extends /** @type {xyz.swapee.wc.back.SwapeeWalletPickerControllerAR.constructor&xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeWalletPickerControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerControllerAR}
 */
xyz.swapee.wc.back.SwapeeWalletPickerControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR} */
xyz.swapee.wc.back.RecordISwapeeWalletPickerControllerAR

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR} xyz.swapee.wc.back.BoundISwapeeWalletPickerControllerAR */

/** @typedef {xyz.swapee.wc.back.SwapeeWalletPickerControllerAR} xyz.swapee.wc.back.BoundSwapeeWalletPickerControllerAR */

/**
 * Contains getters to cast the _ISwapeeWalletPickerControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerControllerARCaster
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerARCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerControllerAR_ instance into the _BoundISwapeeWalletPickerControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeWalletPickerControllerAR}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerARCaster.prototype.asISwapeeWalletPickerControllerAR
/**
 * Access the _SwapeeWalletPickerControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeWalletPickerControllerAR}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerControllerARCaster.prototype.superSwapeeWalletPickerControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/54-ISwapeeWalletPickerControllerAT.xml}  5a6b558b9fd6f005982e6652c854a596 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeWalletPickerControllerAT)} xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT} xyz.swapee.wc.front.SwapeeWalletPickerControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.constructor&xyz.swapee.wc.front.SwapeeWalletPickerControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT|typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT|typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT|typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.Initialese[]) => xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT} xyz.swapee.wc.front.SwapeeWalletPickerControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeWalletPickerControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeWalletPickerControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT = class extends /** @type {xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.Initialese>)} xyz.swapee.wc.front.SwapeeWalletPickerControllerAT.constructor */
/**
 * A concrete class of _ISwapeeWalletPickerControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeWalletPickerControllerAT
 * @implements {xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeWalletPickerControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeWalletPickerControllerAT = class extends /** @type {xyz.swapee.wc.front.SwapeeWalletPickerControllerAT.constructor&xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeWalletPickerControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerControllerAT}
 */
xyz.swapee.wc.front.SwapeeWalletPickerControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT} */
xyz.swapee.wc.front.RecordISwapeeWalletPickerControllerAT

/** @typedef {xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT} xyz.swapee.wc.front.BoundISwapeeWalletPickerControllerAT */

/** @typedef {xyz.swapee.wc.front.SwapeeWalletPickerControllerAT} xyz.swapee.wc.front.BoundSwapeeWalletPickerControllerAT */

/**
 * Contains getters to cast the _ISwapeeWalletPickerControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeWalletPickerControllerATCaster
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerATCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerControllerAT_ instance into the _BoundISwapeeWalletPickerControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeWalletPickerControllerAT}
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerATCaster.prototype.asISwapeeWalletPickerControllerAT
/**
 * Access the _SwapeeWalletPickerControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeWalletPickerControllerAT}
 */
xyz.swapee.wc.front.ISwapeeWalletPickerControllerATCaster.prototype.superSwapeeWalletPickerControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/70-ISwapeeWalletPickerScreen.xml}  dfc38645fb8eaaf0b09d2afec16e6e4d */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.front.SwapeeWalletPickerInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeWalletPickerDisplay.Settings, !xyz.swapee.wc.ISwapeeWalletPickerDisplay.Queries, null>&xyz.swapee.wc.ISwapeeWalletPickerDisplay.Initialese} xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerScreen)} xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerScreen} xyz.swapee.wc.SwapeeWalletPickerScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerScreen` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerScreen
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.constructor&xyz.swapee.wc.SwapeeWalletPickerScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.SwapeeWalletPickerScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeWalletPickerController|typeof xyz.swapee.wc.front.SwapeeWalletPickerController)|(!xyz.swapee.wc.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.SwapeeWalletPickerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerScreen}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.SwapeeWalletPickerScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeWalletPickerController|typeof xyz.swapee.wc.front.SwapeeWalletPickerController)|(!xyz.swapee.wc.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.SwapeeWalletPickerDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.SwapeeWalletPickerScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeWalletPickerController|typeof xyz.swapee.wc.front.SwapeeWalletPickerController)|(!xyz.swapee.wc.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.SwapeeWalletPickerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerScreen} xyz.swapee.wc.SwapeeWalletPickerScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.SwapeeWalletPickerMemory, !xyz.swapee.wc.front.SwapeeWalletPickerInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeWalletPickerDisplay.Settings, !xyz.swapee.wc.ISwapeeWalletPickerDisplay.Queries, null, null>&xyz.swapee.wc.front.ISwapeeWalletPickerController&xyz.swapee.wc.ISwapeeWalletPickerDisplay)} xyz.swapee.wc.ISwapeeWalletPickerScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerScreen
 */
xyz.swapee.wc.ISwapeeWalletPickerScreen = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ISwapeeWalletPickerController.typeof&xyz.swapee.wc.ISwapeeWalletPickerDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerScreen.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerScreen.makeWalletElement} */
xyz.swapee.wc.ISwapeeWalletPickerScreen.prototype.makeWalletElement = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerScreen} xyz.swapee.wc.ISwapeeWalletPickerScreen.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerScreen_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerScreen
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerScreen = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerScreen.constructor&xyz.swapee.wc.ISwapeeWalletPickerScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.SwapeeWalletPickerScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerScreen} */
xyz.swapee.wc.RecordISwapeeWalletPickerScreen

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerScreen} xyz.swapee.wc.BoundISwapeeWalletPickerScreen */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerScreen} xyz.swapee.wc.BoundSwapeeWalletPickerScreen */

/**
 * Contains getters to cast the _ISwapeeWalletPickerScreen_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerScreenCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerScreenCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerScreen_ instance into the _BoundISwapeeWalletPickerScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerScreen}
 */
xyz.swapee.wc.ISwapeeWalletPickerScreenCaster.prototype.asISwapeeWalletPickerScreen
/**
 * Access the _SwapeeWalletPickerScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerScreen}
 */
xyz.swapee.wc.ISwapeeWalletPickerScreenCaster.prototype.superSwapeeWalletPickerScreen

/**
 * @typedef {(this: THIS, tempVid: number) => !HTMLDivElement} xyz.swapee.wc.ISwapeeWalletPickerScreen.__makeWalletElement
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerScreen.__makeWalletElement<!xyz.swapee.wc.ISwapeeWalletPickerScreen>} xyz.swapee.wc.ISwapeeWalletPickerScreen._makeWalletElement */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerScreen.makeWalletElement} */
/**
 * Creates a new VDU from the _WalletTemplate_.
 * @param {number} tempVid The VDU id recorded on the back.
 * @return {!HTMLDivElement}
 */
xyz.swapee.wc.ISwapeeWalletPickerScreen.makeWalletElement = function(tempVid) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerScreen
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/70-ISwapeeWalletPickerScreenBack.xml}  64eaa35f10186c3a164d46a824bd72cf */
/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese} xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeWalletPickerScreen)} xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen} xyz.swapee.wc.back.SwapeeWalletPickerScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeWalletPickerScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.constructor&xyz.swapee.wc.back.SwapeeWalletPickerScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen)|(!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen)|(!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen)|(!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese[]) => xyz.swapee.wc.back.ISwapeeWalletPickerScreen} xyz.swapee.wc.back.SwapeeWalletPickerScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeWalletPickerScreenCaster&xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT)} xyz.swapee.wc.back.ISwapeeWalletPickerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT} xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerScreen
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreen = class extends /** @type {xyz.swapee.wc.back.ISwapeeWalletPickerScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeWalletPickerScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese>)} xyz.swapee.wc.back.SwapeeWalletPickerScreen.constructor */
/**
 * A concrete class of _ISwapeeWalletPickerScreen_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeWalletPickerScreen
 * @implements {xyz.swapee.wc.back.ISwapeeWalletPickerScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeWalletPickerScreen = class extends /** @type {xyz.swapee.wc.back.SwapeeWalletPickerScreen.constructor&xyz.swapee.wc.back.ISwapeeWalletPickerScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeWalletPickerScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreen}
 */
xyz.swapee.wc.back.SwapeeWalletPickerScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerScreen} */
xyz.swapee.wc.back.RecordISwapeeWalletPickerScreen

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerScreen} xyz.swapee.wc.back.BoundISwapeeWalletPickerScreen */

/** @typedef {xyz.swapee.wc.back.SwapeeWalletPickerScreen} xyz.swapee.wc.back.BoundSwapeeWalletPickerScreen */

/**
 * Contains getters to cast the _ISwapeeWalletPickerScreen_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerScreenCaster
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerScreen_ instance into the _BoundISwapeeWalletPickerScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeWalletPickerScreen}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenCaster.prototype.asISwapeeWalletPickerScreen
/**
 * Access the _SwapeeWalletPickerScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeWalletPickerScreen}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenCaster.prototype.superSwapeeWalletPickerScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/73-ISwapeeWalletPickerScreenAR.xml}  fe5000c59e476319faa73e9eb462e498 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeWalletPickerScreen.Initialese} xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeWalletPickerScreenAR)} xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR} xyz.swapee.wc.front.SwapeeWalletPickerScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.constructor&xyz.swapee.wc.front.SwapeeWalletPickerScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR|typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.SwapeeWalletPickerScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR|typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.SwapeeWalletPickerScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR|typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeWalletPickerScreen|typeof xyz.swapee.wc.SwapeeWalletPickerScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeWalletPickerScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.Initialese[]) => xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR} xyz.swapee.wc.front.SwapeeWalletPickerScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeWalletPickerScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeWalletPickerScreen)} xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeWalletPickerScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR
 */
xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR = class extends /** @type {xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeWalletPickerScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.Initialese>)} xyz.swapee.wc.front.SwapeeWalletPickerScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR} xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.typeof */
/**
 * A concrete class of _ISwapeeWalletPickerScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeWalletPickerScreenAR
 * @implements {xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeWalletPickerScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeWalletPickerScreenAR = class extends /** @type {xyz.swapee.wc.front.SwapeeWalletPickerScreenAR.constructor&xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeWalletPickerScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeWalletPickerScreenAR}
 */
xyz.swapee.wc.front.SwapeeWalletPickerScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR} */
xyz.swapee.wc.front.RecordISwapeeWalletPickerScreenAR

/** @typedef {xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR} xyz.swapee.wc.front.BoundISwapeeWalletPickerScreenAR */

/** @typedef {xyz.swapee.wc.front.SwapeeWalletPickerScreenAR} xyz.swapee.wc.front.BoundSwapeeWalletPickerScreenAR */

/**
 * Contains getters to cast the _ISwapeeWalletPickerScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeWalletPickerScreenARCaster
 */
xyz.swapee.wc.front.ISwapeeWalletPickerScreenARCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerScreenAR_ instance into the _BoundISwapeeWalletPickerScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeWalletPickerScreenAR}
 */
xyz.swapee.wc.front.ISwapeeWalletPickerScreenARCaster.prototype.asISwapeeWalletPickerScreenAR
/**
 * Access the _SwapeeWalletPickerScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeWalletPickerScreenAR}
 */
xyz.swapee.wc.front.ISwapeeWalletPickerScreenARCaster.prototype.superSwapeeWalletPickerScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/74-ISwapeeWalletPickerScreenAT.xml}  ae14d20db4af929921c89b1b7cf8efde */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeWalletPickerScreenAT)} xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT} xyz.swapee.wc.back.SwapeeWalletPickerScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.constructor&xyz.swapee.wc.back.SwapeeWalletPickerScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT|typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeWalletPickerScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese[]) => xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT} xyz.swapee.wc.back.SwapeeWalletPickerScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeWalletPickerScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeWalletPickerScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT = class extends /** @type {xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese>)} xyz.swapee.wc.back.SwapeeWalletPickerScreenAT.constructor */
/**
 * A concrete class of _ISwapeeWalletPickerScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeWalletPickerScreenAT
 * @implements {xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeWalletPickerScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeWalletPickerScreenAT = class extends /** @type {xyz.swapee.wc.back.SwapeeWalletPickerScreenAT.constructor&xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeWalletPickerScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeWalletPickerScreenAT}
 */
xyz.swapee.wc.back.SwapeeWalletPickerScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT} */
xyz.swapee.wc.back.RecordISwapeeWalletPickerScreenAT

/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT} xyz.swapee.wc.back.BoundISwapeeWalletPickerScreenAT */

/** @typedef {xyz.swapee.wc.back.SwapeeWalletPickerScreenAT} xyz.swapee.wc.back.BoundSwapeeWalletPickerScreenAT */

/**
 * Contains getters to cast the _ISwapeeWalletPickerScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeWalletPickerScreenATCaster
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenATCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerScreenAT_ instance into the _BoundISwapeeWalletPickerScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeWalletPickerScreenAT}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenATCaster.prototype.asISwapeeWalletPickerScreenAT
/**
 * Access the _SwapeeWalletPickerScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeWalletPickerScreenAT}
 */
xyz.swapee.wc.back.ISwapeeWalletPickerScreenATCaster.prototype.superSwapeeWalletPickerScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-wallet-picker/SwapeeWalletPicker.mvc/design/80-ISwapeeWalletPickerGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.Initialese} xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeWalletPickerGPU)} xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeWalletPickerGPU} xyz.swapee.wc.SwapeeWalletPickerGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerGPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeWalletPickerGPU
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.constructor&xyz.swapee.wc.SwapeeWalletPickerGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeWalletPickerGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerGPU|typeof xyz.swapee.wc.SwapeeWalletPickerGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerGPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerGPU|typeof xyz.swapee.wc.SwapeeWalletPickerGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeWalletPickerGPU|typeof xyz.swapee.wc.SwapeeWalletPickerGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeWalletPickerDisplay|typeof xyz.swapee.wc.back.SwapeeWalletPickerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGPU}
 */
xyz.swapee.wc.AbstractSwapeeWalletPickerGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese[]) => xyz.swapee.wc.ISwapeeWalletPickerGPU} xyz.swapee.wc.SwapeeWalletPickerGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeWalletPickerGPUCaster&com.webcircuits.IBrowserView<.!SwapeeWalletPickerMemory,>&xyz.swapee.wc.back.ISwapeeWalletPickerDisplay)} xyz.swapee.wc.ISwapeeWalletPickerGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!SwapeeWalletPickerMemory,>} com.webcircuits.IBrowserView<.!SwapeeWalletPickerMemory,>.typeof */
/**
 * Handles the periphery of the _ISwapeeWalletPickerDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGPU
 */
xyz.swapee.wc.ISwapeeWalletPickerGPU = class extends /** @type {xyz.swapee.wc.ISwapeeWalletPickerGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!SwapeeWalletPickerMemory,>.typeof&xyz.swapee.wc.back.ISwapeeWalletPickerDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeWalletPickerGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeWalletPickerGPU.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeWalletPickerGPU.makeWalletElement} */
xyz.swapee.wc.ISwapeeWalletPickerGPU.prototype.makeWalletElement = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeWalletPickerGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese>)} xyz.swapee.wc.SwapeeWalletPickerGPU.constructor */
/**
 * A concrete class of _ISwapeeWalletPickerGPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeWalletPickerGPU
 * @implements {xyz.swapee.wc.ISwapeeWalletPickerGPU} Handles the periphery of the _ISwapeeWalletPickerDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeWalletPickerGPU = class extends /** @type {xyz.swapee.wc.SwapeeWalletPickerGPU.constructor&xyz.swapee.wc.ISwapeeWalletPickerGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeWalletPickerGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeWalletPickerGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeWalletPickerGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeWalletPickerGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeWalletPickerGPU}
 */
xyz.swapee.wc.SwapeeWalletPickerGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeWalletPickerGPU.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGPUFields
 */
xyz.swapee.wc.ISwapeeWalletPickerGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ISwapeeWalletPickerGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGPU} */
xyz.swapee.wc.RecordISwapeeWalletPickerGPU

/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGPU} xyz.swapee.wc.BoundISwapeeWalletPickerGPU */

/** @typedef {xyz.swapee.wc.SwapeeWalletPickerGPU} xyz.swapee.wc.BoundSwapeeWalletPickerGPU */

/**
 * Contains getters to cast the _ISwapeeWalletPickerGPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeWalletPickerGPUCaster
 */
xyz.swapee.wc.ISwapeeWalletPickerGPUCaster = class { }
/**
 * Cast the _ISwapeeWalletPickerGPU_ instance into the _BoundISwapeeWalletPickerGPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeWalletPickerGPU}
 */
xyz.swapee.wc.ISwapeeWalletPickerGPUCaster.prototype.asISwapeeWalletPickerGPU
/**
 * Access the _SwapeeWalletPickerGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeWalletPickerGPU}
 */
xyz.swapee.wc.ISwapeeWalletPickerGPUCaster.prototype.superSwapeeWalletPickerGPU

/**
 * @typedef {(this: THIS, vid: number) => !HTMLDivElement} xyz.swapee.wc.ISwapeeWalletPickerGPU.__makeWalletElement
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeWalletPickerGPU.__makeWalletElement<!xyz.swapee.wc.ISwapeeWalletPickerGPU>} xyz.swapee.wc.ISwapeeWalletPickerGPU._makeWalletElement */
/** @typedef {typeof xyz.swapee.wc.ISwapeeWalletPickerGPU.makeWalletElement} */
/**
 * Creates a new element from the _WalletTemplate_ template.
 * @param {number} vid The VDU id.
 * @return {!HTMLDivElement} A new element created from the template.
 */
xyz.swapee.wc.ISwapeeWalletPickerGPU.makeWalletElement = function(vid) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeWalletPickerGPU
/* @typal-end */