import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {5141253786} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=d["372700389810"],e=d["372700389811"];function g(a,b,c,f){return d["372700389812"](a,b,c,f,!1,void 0)}const h=d.precombined;function k(){}k.prototype={X(){const {asIIntegratedController:{setInputs:a}}=this;a({h:!0})}};class ba{}class m extends g(ba,51412537868,null,{ha:1,wa:2}){}m[e]=[k];


const n=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];

const p=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ca=p.IntegratedController,da=p.Parametric;
const q=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ea=q["61505580523"],fa=q["615055805212"],ha=q["615055805218"],ia=q["615055805221"],ja=q["615055805223"],ka=q["615055805230"],la=q["615055805235"],ma=q["615055805237"],na=q["615055805238"];
const r=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const oa=r.StatefulLoader,pa=r.StatefulLoadable,qa=r.IntegratedComponentInitialiser,ra=r.IntegratedComponent,sa=r["38"];const t={K:"c60d7",u:"90b43",C:"e825f",D:"e5f96",h:"824a4"};function u(){}u.prototype={};class ta{}class y extends g(ta,51412537867,null,{da:1,oa:2}){}function z(){}z.prototype={};function A(){this.model={K:!1,u:[],D:"",h:!1,C:null}}class ua{}class B extends g(ua,51412537863,A,{fa:1,ta:2}){}B[e]=[z,{constructor(){n(this.model,t)}}];y[e]=[{},u,B];function C(){}C.prototype={};class va{}class D extends g(va,514125378634,null,{aa:1,ka:2}){}D[e]=[C,{},pa,y];function E(){}E.prototype={};class wa{}class F extends g(wa,51412537861,null,{ba:1,la:2}){}F[e]=[E,ha];const G={regulate:fa({K:Boolean,u:[String],D:String,h:Boolean,C:Array})};const H={...t};function I(){}I.prototype={};function J(){const a={model:null};A.call(a);this.inputs=a.model}class xa{}class K extends g(xa,51412537865,J,{ga:1,ua:2}){}function L(){}K[e]=[L.prototype={resetPort(){J.call(this)}},I,da,L.prototype={constructor(){n(this.inputs,H)}}];function M(){}M.prototype={};class ya{}class N extends g(ya,514125378622,null,{G:1,Z:2}){}N[e]=[{resetPort(){this.port.resetPort()}},M,{calibrate:function({h:a}){a&&setTimeout(()=>{const {G:{setInputs:b}}=this;b({h:!1})},1)}},G,ca,{get Port(){return K}}];function O(){}O.prototype={};class za{}class P extends g(za,51412537869,null,{R:1,ja:2}){}P[e]=[O,D,m,ra,F,N];
const Q=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const Aa=Q["12817393923"],Ba=Q["12817393924"],Ca=Q["12817393925"],Da=Q["12817393926"];function R(){}R.prototype={};class Ea{}class S extends g(Ea,514125378625,null,{ca:1,ma:2}){}S[e]=[R,Da,{allocator(){this.methods={X:"83537"}}}];function T(){}T.prototype={};class Fa{}class U extends g(Fa,514125378624,null,{G:1,Z:2}){}U[e]=[T,N,S,Aa];var V=class extends U.implements(){};function Ga(){Object.defineProperty(this,"processJSON",{value(a){return a}})};async function Ha({M:a,L:b}){const {asIStatefulLoader:{fetchJSON:c}}=this;a=await c(\`\${b}/invoice\`,{method:"POST",headers:{Authorization:\`Bearer \${a}\`,"Content-Type":"application/json"},body:JSON.stringify({invoice:{amount:0,type:"bitcoin"}})});if(!a)return{};({hash:a}=a);return{D:a}};function Ia({u:a}){return{C:a.map(b=>({address:b}))}};function Ja(a,b,c){a={u:a.u};a=c?c(a):a;b=c?c(b):b;return this.P(a,b)}function Ka(a,b,c){const f=this.land.g;if(f&&(a={h:a.h,L:f.model.ab70c,M:f.model["94a08"]},a=c?c(a):a,a.h&&a.L&&a.M))return b=c?c(b):b,this.O(a,b)};class W extends F.implements({__$constructor:Ga,O:Ha,P:Ia,adapt:[Ka,Ja]}){};function La(){}La.prototype={};class Ma{}class Na extends g(Ma,514125378616,null,{H:1,ra:2}){}function X(){}Na[e]=[La,X.prototype={I:h,F:h,V:h,J:h},X.prototype={F({address:a},b){b.select("[data-id=WalletAddress]").setText(a)}},X.prototype={F({address:a},b){const {T:{asIBrowserView:{data:c}}}=this;c(b,"address",a)}},X.prototype={I(a,b){const {H:{F:c}}=this;b.setState(a);c(b.state,b.element)},J(a,b){const {H:{V:c},land:f}=this;c(a.state,b,f)},Y(a){return new ma({element:a,state:{address:void 0},dynamo:this})}}];var Oa=class extends Na.implements(){};function Pa(){}Pa.prototype={};class Qa{}class Ra extends g(Qa,514125378621,null,{ea:1,pa:2}){}
Ra[e]=[Pa,Ba,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{A:a,s:a,i:a,l:a,m:a,v:a,o:a,g:a})}},{[aa]:{s:1,m:1,v:1,i:1,o:1,l:1,g:1,A:1},initializer({s:a,m:b,v:c,i:f,o:v,l:w,g:x,A:l}){void 0!==a&&(this.s=a);void 0!==b&&(this.m=b);void 0!==c&&(this.v=c);void 0!==f&&(this.i=f);void 0!==v&&(this.o=v);void 0!==w&&(this.l=w);void 0!==x&&(this.g=x);void 0!==l&&(this.A=l)}}];const Y={$:"9bd81"};const Sa=Object.keys(Y).reduce((a,b)=>{a[Y[b]]=b;return a},{});const Ta={A:"a04f1",s:"a04f2",m:"a04f3",v:"a04f4",o:"a04f5",g:"a04f6",i:"a04f7",l:"a04f8"};const Ua=Object.keys(Ta).reduce((a,b)=>{a[Ta[b]]=b;return a},{});function Va(){}Va.prototype={};class Wa{}class Xa extends g(Wa,514125378618,null,{j:1,qa:2}){}function Ya(){}Xa[e]=[Va,Ya.prototype={classesQPs:Sa,vdusQPs:Ua,memoryPQs:t},Ra,ea,Ya.prototype={allocator(){sa(this.classes,"",Y)}}];function Za(){}Za.prototype={};class $a{}class ab extends g($a,514125378630,null,{U:1,ya:2}){}ab[e]=[Za,Ca,{W(){const {U:{uart:a}}=this;a.t("inv",{mid:"86307",args:[...arguments]})}}];function bb(){}bb.prototype={};class cb{}class db extends g(cb,514125378628,null,{ia:1,xa:2}){}db[e]=[bb,ab];const eb=Object.keys(H).reduce((a,b)=>{a[H[b]]=b;return a},{});function fb(){}fb.prototype={};class gb{static mvc(a,b,c){return ja(this,a,b,null,c)}}class hb extends g(gb,514125378611,null,{T:1,sa:2}){}function Z(){}
hb[e]=[fb,ia,P,Xa,db,la,Z.prototype={constructor(){this.land={g:null}}},Z.prototype={inputsQPs:eb},{paint:function({C:a}){const {j:{s:b,W:c},asIGraphicsDriverBack:{makeVdu:f},R:{Y:v,I:w,J:x}}=this;ka(b,a,{create:v,make:()=>{const l=f();c(l.id);return l},brush:w,init:x,strategy:"reuse"})}},Z.prototype={paint:function({loading:a}){const {j:{i:b},asIBrowserView:{attr:c}}=this;c(b,"disabled",a||null)}},Z.prototype={paint:na({l:{loading:1}})},Z.prototype={paint:function(a,{g:b}){b&&({j:{o:a}}=this,a.listen("click",
()=>{b["2cef1"]()}))}},Z.prototype={paint:function(a,{g:b}){if(b){({"94a08":a}=b.model);var {j:{m:c},asIBrowserView:{conceal:f}}=this;f(c,a)}}},Z.prototype={paint:function(a,{g:b}){if(b){({"94a08":a}=b.model);var {j:{i:c},asIBrowserView:{reveal:f}}=this;f(c,a)}}},Z.prototype={scheduleTwo:function(){const {asIHtmlComponent:{complete:a},j:{g:b}}=this;a(6200449439,{g:b})}}];var ib=class extends hb.implements(V,W,Oa,qa,oa){};module.exports["51412537860"]=P;module.exports["51412537861"]=P;module.exports["51412537863"]=K;module.exports["51412537864"]=N;module.exports["51412537865"]=D;module.exports["514125378610"]=ib;module.exports["514125378611"]=G;module.exports["514125378618"]=Oa;module.exports["514125378630"]=F;module.exports["514125378631"]=W;module.exports["514125378661"]=V;
/*! @embed-object-end {5141253786} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule