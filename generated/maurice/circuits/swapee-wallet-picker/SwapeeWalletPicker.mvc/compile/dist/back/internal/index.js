import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPicker}*/
export class AbstractSwapeeWalletPicker extends Module['51412537861'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPicker} */
AbstractSwapeeWalletPicker.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerPort} */
export const SwapeeWalletPickerPort=Module['51412537863']
/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPickerController}*/
export class AbstractSwapeeWalletPickerController extends Module['51412537864'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerController} */
AbstractSwapeeWalletPickerController.class=function(){}
/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU}*/
export class AbstractSwapeeWalletPickerCPU extends Module['51412537865'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCPU} */
AbstractSwapeeWalletPickerCPU.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerHtmlComponent} */
export const SwapeeWalletPickerHtmlComponent=Module['514125378610']
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerBuffer} */
export const SwapeeWalletPickerBuffer=Module['514125378611']
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerGenerator} */
export const SwapeeWalletPickerGenerator=Module['514125378618']
/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPickerComputer}*/
export class AbstractSwapeeWalletPickerComputer extends Module['514125378630'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerComputer} */
AbstractSwapeeWalletPickerComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerComputer} */
export const SwapeeWalletPickerComputer=Module['514125378631']
/** @type {typeof xyz.swapee.wc.back.SwapeeWalletPickerController} */
export const SwapeeWalletPickerController=Module['514125378661']