/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPicker` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPicker}
 */
class AbstractSwapeeWalletPicker extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeWalletPicker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerPort}
 */
class SwapeeWalletPickerPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerController}
 */
class AbstractSwapeeWalletPickerController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerCPU` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU}
 */
class AbstractSwapeeWalletPickerCPU extends (class {/* lazy-loaded */}) {}
/**
 * The _ISwapeeWalletPicker_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerHtmlComponent}
 */
class SwapeeWalletPickerHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerBuffer}
 */
class SwapeeWalletPickerBuffer extends (class {/* lazy-loaded */}) {}
/**
 * Responsible for generation of component instances to place inside containers.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerGenerator}
 */
class SwapeeWalletPickerGenerator extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerComputer}
 */
class AbstractSwapeeWalletPickerComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerComputer}
 */
class SwapeeWalletPickerComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.SwapeeWalletPickerController}
 */
class SwapeeWalletPickerController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeWalletPicker = AbstractSwapeeWalletPicker
module.exports.SwapeeWalletPickerPort = SwapeeWalletPickerPort
module.exports.AbstractSwapeeWalletPickerController = AbstractSwapeeWalletPickerController
module.exports.AbstractSwapeeWalletPickerCPU = AbstractSwapeeWalletPickerCPU
module.exports.SwapeeWalletPickerHtmlComponent = SwapeeWalletPickerHtmlComponent
module.exports.SwapeeWalletPickerBuffer = SwapeeWalletPickerBuffer
module.exports.SwapeeWalletPickerGenerator = SwapeeWalletPickerGenerator
module.exports.AbstractSwapeeWalletPickerComputer = AbstractSwapeeWalletPickerComputer
module.exports.SwapeeWalletPickerComputer = SwapeeWalletPickerComputer
module.exports.SwapeeWalletPickerController = SwapeeWalletPickerController