import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {5141253786} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const d=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const e=d["372700389811"];function f(a,b,c,g){return d["372700389812"](a,b,c,g,!1,void 0)};function h(){}h.prototype={D(){const {asIIntegratedController:{setInputs:a}}=this;a({g:!0})}};class l{}class m extends f(l,"ISwapeeWalletPickerProcessor",null,{R:1,ga:2}){}m[e]=[h];

const n=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const p=n.StatefulLoadable,aa=n.IntegratedComponentInitialiser,q=n.IntegratedComponent,ba=n["95173443851"],ca=n["95173443852"];
const r=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const da=r["615055805212"],ea=r["615055805218"],t=r["615055805233"],fa=r["615055805235"];const u={o:"c60d7",s:"90b43",j:"e825f",m:"e5f96",g:"824a4"};function v(){}v.prototype={};class ha{}class w extends f(ha,"ISwapeeWalletPickerCore",null,{J:1,aa:2}){}function y(){}y.prototype={};function z(){this.model={o:!1,s:[],m:"",g:!1,j:null}}class ia{}class A extends f(ia,"ISwapeeWalletPickerOuterCore",z,{M:1,ea:2}){}A[e]=[y,{constructor(){t(this.model,"",u)}}];w[e]=[{},v,A];function B(){}B.prototype={};class ja{}class C extends f(ja,"ISwapeeWalletPickerCPU",null,{H:1,Y:2}){}C[e]=[B,{},p,w];function D(){}D.prototype={};class ka{}class E extends f(ka,"ISwapeeWalletPickerComputer",null,{I:1,Z:2}){}E[e]=[D,ea];const F={regulate:da({o:Boolean,s:[String],m:String,g:Boolean,j:Array})};
const G=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=G.IntegratedController,ma=G.Parametric;const H={...u};function I(){}I.prototype={};function na(){const a={model:null};z.call(a);this.inputs=a.model}class oa{}class J extends f(oa,"ISwapeeWalletPickerPort",na,{P:1,fa:2}){}function K(){}J[e]=[K.prototype={},I,ma,K.prototype={constructor(){t(this.inputs,"",H)}}];function L(){}L.prototype={};class pa{}class M extends f(pa,"ISwapeeWalletPickerController",null,{u:1,$:2}){}M[e]=[{},L,{calibrate:function({g:a}){a&&setTimeout(()=>{const {u:{setInputs:b}}=this;b({g:!1})},1)}},F,la,{get Port(){return J}}];function N(){}N.prototype={};class qa{}class O extends f(qa,"ISwapeeWalletPicker",null,{G:1,X:2}){}O[e]=[N,C,m,q,E,M];function ra(){return{}};require(eval('"@type.engineering/web-computing"'));const sa=require(eval('"@type.engineering/web-computing"')).h;function ta(){return sa("div",{$id:"SwapeeWalletPicker"})};require(eval('"@type.engineering/web-computing"'));const P=require(eval('"@type.engineering/web-computing"')).h;function ua({F:a},{W:b}){return P("div",{$id:"SwapeeWalletPicker"},P("button",{$id:"SwapeeLoginBu",onClick:b}),P("button",{$id:"SwapeeAccountWr",$conceal:a}),P("button",{$id:"CreateInvoiceBu",$reveal:a}))};require(eval('"@type.engineering/web-computing"'));const Q=require(eval('"@type.engineering/web-computing"')).h,va=({address:a})=>Q("div",{$id:"Wallet","data-address":a},Q("span",{$id:"WalletAddress"},a));require(eval('"@type.engineering/web-computing"'));const R=require(eval('"@type.engineering/web-computing"')).h;function wa({j:a,loading:b},{D:c}){return R("div",{$id:"SwapeeWalletPicker"},R("div",{$id:"Wallets",$noc:!0,$rotor:a,$dyn:"Wallet"},a&&a.map(g=>va.call(null,g))),R("button",{$id:"CreateInvoiceBu",disabled:b,onClick:c}),R("span",{$id:"InvoiceLoIn",$reveal:b}))};var S=class extends M.implements(){};require("https");require("http");const xa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}xa("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const ya=T.ElementBase,za=T.HTMLBlocker;require(eval('"@type.engineering/web-computing"'));const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function Aa(){this.inputs={noSolder:!1,la:{},ha:{},U:{},T:{},ia:{},V:{},ja:{},ka:{}}}class Ba{}class W extends f(Ba,"ISwapeeWalletPickerElementPort",Aa,{K:1,da:2}){}
W[e]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"wallets-opts":void 0,"swapee-account-wr-opts":void 0,"get-wallet-bu-opts":void 0,"create-invoice-bu-opts":void 0,"swapee-login-bu-opts":void 0,"invoice-lo-in-opts":void 0,"swapee-me-opts":void 0,"wallet-template-opts":void 0,"is-signed-in":void 0,"load-create-invoice":void 0,"wallets-rotor":void 0})}}];function X(){}X.prototype={};class Ca{}class Y extends f(Ca,"ISwapeeWalletPickerElement",null,{v:1,ba:2}){}function Z(){}
Y[e]=[X,ya,Z.prototype={calibrate:function({":no-solder":a,":is-signed-in":b,":wallets":c,":invoice":g,":load-create-invoice":k,":wallets-rotor":x}){const {attributes:{"no-solder":Da,"is-signed-in":Ea,wallets:Fa,invoice:Ga,"load-create-invoice":Ha,"wallets-rotor":Ia}}=this;return{...(void 0===Da?{"no-solder":a}:{}),...(void 0===Ea?{"is-signed-in":b}:{}),...(void 0===Fa?{wallets:c}:{}),...(void 0===Ga?{invoice:g}:{}),...(void 0===Ha?{"load-create-invoice":k}:{}),...(void 0===Ia?{"wallets-rotor":x}:
{})}}},Z.prototype={calibrate:({"no-solder":a,"is-signed-in":b,wallets:c,invoice:g,"load-create-invoice":k,"wallets-rotor":x})=>({noSolder:a,o:b,s:c,m:g,g:k,j:x})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},fa,Z.prototype={constructor(){Object.assign(this,{land:{l:null}})},render:function(){const {asILanded:{land:{l:a}},v:{A:b}}=this;if(a){var {"94a08":c}=a.model;c=b({F:c},{});c.attributes.$id=this.rootId;c.nodeName="div";return c}}},Z.prototype={render:function(){return U("div",{$id:"SwapeeWalletPicker"},
U("vdu",{$template:"WalletTemplate"}),U("vdu",{$id:"Wallets"}),U("vdu",{$id:"CreateInvoiceBu"}),U("vdu",{$id:"InvoiceLoIn"}),U("vdu",{$id:"SwapeeAccountWr"}),U("vdu",{$id:"GetWalletBu"}),U("vdu",{$id:"SwapeeLoginBu"}))}},Z.prototype={classes:{Class:"9bd81"},inputsPQs:H,queriesPQs:{i:"50b0c"},vdus:{WalletTemplate:"a04f1",Wallets:"a04f2",SwapeeAccountWr:"a04f3",GetWalletBu:"a04f4",SwapeeLoginBu:"a04f5",SwapeeMe:"a04f6",CreateInvoiceBu:"a04f7",InvoiceLoIn:"a04f8"}},q,ca,Z.prototype={constructor(){Object.assign(this,
{knownInputs:new Set("noSolder isSignedIn wallets invoice loadCreateInvoice walletsRotor query:swapee-me no-solder :no-solder is-signed-in :is-signed-in :wallets :invoice load-create-invoice :load-create-invoice wallets-rotor :wallets-rotor fe646 046c0 60ed0 7721a b0f6c 1f611 98555 4ccfe 58b0a c60d7 90b43 e5f96 824a4 e825f children".split(" "))})},get Port(){return W},calibrate:function({"query:swapee-me":a}){const b={};a&&(b.i=a);return b}},Z.prototype={constructor(){this.land={l:null}}},Z.prototype=
{calibrate:async function({i:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:c},asIElement:{fqn:g}}=this,k=await b(a);if(!k)return console.warn("\\u2757\\ufe0f swapeeMeSel %s must be present on the page for %s to work",a,g),{};c.l=k}},Z.prototype={solder:(a,{i:b})=>({i:b})}];Y[e]=[O,{rootId:"SwapeeWalletPicker",__$id:5141253786,fqn:"xyz.swapee.wc.ISwapeeWalletPicker",maurice_element_v3:!0}];class Ja extends Y.implements(S,ba,za,aa,{allocator:function(){}},{solder:ra,server:ta,A:ua,render:wa},{classesMap:!0,rootSelector:".SwapeeWalletPicker",stylesheet:"html/styles/SwapeeWalletPicker.css",blockName:"html/SwapeeWalletPickerBlock.html"}){};module.exports["51412537860"]=O;module.exports["51412537861"]=O;module.exports["51412537863"]=J;module.exports["51412537864"]=M;module.exports["51412537865"]=C;module.exports["51412537868"]=Ja;module.exports["514125378611"]=F;module.exports["514125378630"]=E;module.exports["514125378661"]=S;
/*! @embed-object-end {5141253786} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule