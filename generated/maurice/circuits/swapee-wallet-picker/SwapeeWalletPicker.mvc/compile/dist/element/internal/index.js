import Module from './element'

/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPicker}*/
export class AbstractSwapeeWalletPicker extends Module['51412537861'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPicker} */
AbstractSwapeeWalletPicker.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerPort} */
export const SwapeeWalletPickerPort=Module['51412537863']
/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPickerController}*/
export class AbstractSwapeeWalletPickerController extends Module['51412537864'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerController} */
AbstractSwapeeWalletPickerController.class=function(){}
/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU}*/
export class AbstractSwapeeWalletPickerCPU extends Module['51412537865'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerCPU} */
AbstractSwapeeWalletPickerCPU.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerElement} */
export const SwapeeWalletPickerElement=Module['51412537868']
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerBuffer} */
export const SwapeeWalletPickerBuffer=Module['514125378611']
/**@extends {xyz.swapee.wc.AbstractSwapeeWalletPickerComputer}*/
export class AbstractSwapeeWalletPickerComputer extends Module['514125378630'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPickerComputer} */
AbstractSwapeeWalletPickerComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeWalletPickerController} */
export const SwapeeWalletPickerController=Module['514125378661']