/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPicker` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPicker}
 */
class AbstractSwapeeWalletPicker extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeWalletPicker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerPort}
 */
class SwapeeWalletPickerPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerController}
 */
class AbstractSwapeeWalletPickerController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerCPU` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU}
 */
class AbstractSwapeeWalletPickerCPU extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ISwapeeWalletPicker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerElement}
 */
class SwapeeWalletPickerElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerBuffer}
 */
class SwapeeWalletPickerBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeWalletPickerComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerComputer}
 */
class AbstractSwapeeWalletPickerComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerController}
 */
class SwapeeWalletPickerController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeWalletPicker = AbstractSwapeeWalletPicker
module.exports.SwapeeWalletPickerPort = SwapeeWalletPickerPort
module.exports.AbstractSwapeeWalletPickerController = AbstractSwapeeWalletPickerController
module.exports.AbstractSwapeeWalletPickerCPU = AbstractSwapeeWalletPickerCPU
module.exports.SwapeeWalletPickerElement = SwapeeWalletPickerElement
module.exports.SwapeeWalletPickerBuffer = SwapeeWalletPickerBuffer
module.exports.AbstractSwapeeWalletPickerComputer = AbstractSwapeeWalletPickerComputer
module.exports.SwapeeWalletPickerController = SwapeeWalletPickerController

Object.defineProperties(module.exports, {
 'AbstractSwapeeWalletPicker': {get: () => require('./precompile/internal')[51412537861]},
 [51412537861]: {get: () => module.exports['AbstractSwapeeWalletPicker']},
 'SwapeeWalletPickerPort': {get: () => require('./precompile/internal')[51412537863]},
 [51412537863]: {get: () => module.exports['SwapeeWalletPickerPort']},
 'AbstractSwapeeWalletPickerController': {get: () => require('./precompile/internal')[51412537864]},
 [51412537864]: {get: () => module.exports['AbstractSwapeeWalletPickerController']},
 'AbstractSwapeeWalletPickerCPU': {get: () => require('./precompile/internal')[51412537865]},
 [51412537865]: {get: () => module.exports['AbstractSwapeeWalletPickerCPU']},
 'SwapeeWalletPickerElement': {get: () => require('./precompile/internal')[51412537868]},
 [51412537868]: {get: () => module.exports['SwapeeWalletPickerElement']},
 'SwapeeWalletPickerBuffer': {get: () => require('./precompile/internal')[514125378611]},
 [514125378611]: {get: () => module.exports['SwapeeWalletPickerBuffer']},
 'AbstractSwapeeWalletPickerComputer': {get: () => require('./precompile/internal')[514125378630]},
 [514125378630]: {get: () => module.exports['AbstractSwapeeWalletPickerComputer']},
 'SwapeeWalletPickerController': {get: () => require('./precompile/internal')[514125378661]},
 [514125378661]: {get: () => module.exports['SwapeeWalletPickerController']},
})