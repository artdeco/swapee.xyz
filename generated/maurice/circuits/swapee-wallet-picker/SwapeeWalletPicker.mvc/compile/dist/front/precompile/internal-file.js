/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const k=h["37270038985"],l=h["37270038986"],m=h["372700389810"],t=h["372700389811"];function v(a,b,c,e){return h["372700389812"](a,b,c,e,!1,void 0)};
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const x=w["61893096584"],z=w["61893096586"],A=w["618930965811"],B=w["618930965812"],C=w["618930965815"];function D(){}D.prototype={};function E(){this.h=this.s=this.j=this.m=this.g=this.i=this.l=this.o=null}class F{}class G extends v(F,514125378619,E,{D:1,K:2}){}
G[t]=[D,x,{constructor(){k(this,()=>{const {queries:{O:a}}=this;this.scan({u:a})})},scan:function({u:a}){const {element:b,v:{vdusPQs:{h:c,o:e,g:n,j:p,l:q,i:r,m:Q}},queries:{u:y}}=this,d=C(b);let u;a?u=b.closest(a):u=document;Object.assign(this,{h:d[c],o:d[e],g:d[n],j:d[p],l:d[q],i:d[r],m:d[Q],s:y?u.querySelector(y):void 0})}},{[m]:{o:1,l:1,i:1,g:1,m:1,j:1,s:1,h:1},initializer({o:a,l:b,i:c,g:e,m:n,j:p,s:q,h:r}){void 0!==a&&(this.o=a);void 0!==b&&(this.l=b);void 0!==c&&(this.i=
c);void 0!==e&&(this.g=e);void 0!==n&&(this.m=n);void 0!==p&&(this.j=p);void 0!==q&&(this.s=q);void 0!==r&&(this.h=r)}}];var H=class extends G.implements(){};function I(){};function J(){}J.prototype={};class K{}class L extends v(K,514125378626,null,{C:1,J:2}){}L[t]=[J,A,{}];function M(){}M.prototype={};class N{}class O extends v(N,514125378629,null,{F:1,M:2}){}O[t]=[M,B,{allocator(){this.methods={A:"86307"}}}];const P={H:"c60d7",P:"90b43",R:"e825f",G:"e5f96",I:"824a4"};const R={...P};const S=Object.keys(P).reduce((a,b)=>{a[P[b]]=b;return a},{});function T(){}T.prototype={};class U{}class V extends v(U,514125378627,null,{v:1,L:2}){}function W(){}V[t]=[W.prototype={A(a){const {h:b,asIScreen:{makeElement:c}}=this;return c(b,a)}},T,W.prototype={inputsPQs:R,queriesPQs:{u:"50b0c"},memoryQPs:S},z,O,W.prototype={vdusPQs:{h:"a04f1",o:"a04f2",l:"a04f3",i:"a04f4",m:"a04f5",s:"a04f6",g:"a04f7",j:"a04f8"}},W.prototype={constructor(){l(this,()=>{const a=this.g;a&&a.addEventListener("click",b=>{b.preventDefault();this.uart.t("inv",{mid:"83537"});return!1})})}}];var X=class extends V.implements(L,H,{get queries(){return this.settings}},{deduceInputs:I,__$id:5141253786}){};module.exports["514125378641"]=H;module.exports["514125378671"]=X;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['5141253786']=module.exports