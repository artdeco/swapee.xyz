/**
 * Display for presenting information from the _ISwapeeWalletPicker_.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerDisplay}
 */
class SwapeeWalletPickerDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.SwapeeWalletPickerScreen}
 */
class SwapeeWalletPickerScreen extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeWalletPickerDisplay = SwapeeWalletPickerDisplay
module.exports.SwapeeWalletPickerScreen = SwapeeWalletPickerScreen