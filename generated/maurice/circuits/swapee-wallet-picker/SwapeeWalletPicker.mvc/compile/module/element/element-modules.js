export default [
 1, // AbstractSwapeeWalletPicker
 3, // SwapeeWalletPickerPort
 4, // AbstractSwapeeWalletPickerController
 5, // AbstractSwapeeWalletPickerCPU
 8, // SwapeeWalletPickerElement
 11, // SwapeeWalletPickerBuffer
 30, // AbstractSwapeeWalletPickerComputer
 61, // SwapeeWalletPickerController
]