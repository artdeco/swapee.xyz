import { SwapeeWalletPickerDisplay, SwapeeWalletPickerScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.SwapeeWalletPickerDisplay} */
export { SwapeeWalletPickerDisplay }
/** @lazy @api {xyz.swapee.wc.SwapeeWalletPickerScreen} */
export { SwapeeWalletPickerScreen }