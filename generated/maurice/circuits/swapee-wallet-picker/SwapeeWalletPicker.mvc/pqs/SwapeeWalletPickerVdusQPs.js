import {SwapeeWalletPickerVdusPQs} from './SwapeeWalletPickerVdusPQs'
export const SwapeeWalletPickerVdusQPs=/**@type {!xyz.swapee.wc.SwapeeWalletPickerVdusQPs}*/(Object.keys(SwapeeWalletPickerVdusPQs)
 .reduce((a,k)=>{a[SwapeeWalletPickerVdusPQs[k]]=k;return a},{}))