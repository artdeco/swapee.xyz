export const SwapeeWalletPickerVdusPQs=/**@type {!xyz.swapee.wc.SwapeeWalletPickerVdusPQs}*/({
 WalletTemplate:'a04f1',
 Wallets:'a04f2',
 SwapeeAccountWr:'a04f3',
 GetWalletBu:'a04f4',
 SwapeeLoginBu:'a04f5',
 SwapeeMe:'a04f6',
 CreateInvoiceBu:'a04f7',
 InvoiceLoIn:'a04f8',
})