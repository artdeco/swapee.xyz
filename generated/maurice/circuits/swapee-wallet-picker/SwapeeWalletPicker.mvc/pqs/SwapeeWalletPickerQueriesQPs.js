import {SwapeeWalletPickerQueriesPQs} from './SwapeeWalletPickerQueriesPQs'
export const SwapeeWalletPickerQueriesQPs=/**@type {!xyz.swapee.wc.SwapeeWalletPickerQueriesQPs}*/(Object.keys(SwapeeWalletPickerQueriesPQs)
 .reduce((a,k)=>{a[SwapeeWalletPickerQueriesPQs[k]]=k;return a},{}))