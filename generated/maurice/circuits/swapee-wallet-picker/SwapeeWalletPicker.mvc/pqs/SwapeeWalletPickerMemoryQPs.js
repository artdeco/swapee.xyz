import {SwapeeWalletPickerMemoryPQs} from './SwapeeWalletPickerMemoryPQs'
export const SwapeeWalletPickerMemoryQPs=/**@type {!xyz.swapee.wc.SwapeeWalletPickerMemoryQPs}*/(Object.keys(SwapeeWalletPickerMemoryPQs)
 .reduce((a,k)=>{a[SwapeeWalletPickerMemoryPQs[k]]=k;return a},{}))