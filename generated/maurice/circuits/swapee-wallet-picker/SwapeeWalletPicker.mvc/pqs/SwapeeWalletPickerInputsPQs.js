import {SwapeeWalletPickerMemoryPQs} from './SwapeeWalletPickerMemoryPQs'
export const SwapeeWalletPickerInputsPQs=/**@type {!xyz.swapee.wc.SwapeeWalletPickerInputsQPs}*/({
 ...SwapeeWalletPickerMemoryPQs,
})