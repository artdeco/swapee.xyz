import SwapeeWalletPickerClassesPQs from './SwapeeWalletPickerClassesPQs'
export const SwapeeWalletPickerClassesQPs=/**@type {!xyz.swapee.wc.SwapeeWalletPickerClassesQPs}*/(Object.keys(SwapeeWalletPickerClassesPQs)
 .reduce((a,k)=>{a[SwapeeWalletPickerClassesPQs[k]]=k;return a},{}))