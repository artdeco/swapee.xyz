import {SwapeeWalletPickerInputsPQs} from './SwapeeWalletPickerInputsPQs'
export const SwapeeWalletPickerInputsQPs=/**@type {!xyz.swapee.wc.SwapeeWalletPickerInputsQPs}*/(Object.keys(SwapeeWalletPickerInputsPQs)
 .reduce((a,k)=>{a[SwapeeWalletPickerInputsPQs[k]]=k;return a},{}))