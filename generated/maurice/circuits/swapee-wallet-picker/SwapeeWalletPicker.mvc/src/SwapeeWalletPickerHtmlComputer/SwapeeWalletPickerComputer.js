import __$constructor from './methods/__$constructor'
import adaptLoadBitcoinAddress from './methods/adapt-load-bitcoin-address'
import adaptWalletsRotor from './methods/adapt-wallets-rotor'
import {preadaptLoadBitcoinAddress,preadaptWalletsRotor} from '../../gen/AbstractSwapeeWalletPickerComputer/preadapters'
import AbstractSwapeeWalletPickerComputer from '../../gen/AbstractSwapeeWalletPickerComputer'

/** @extends {xyz.swapee.wc.SwapeeWalletPickerComputer} */
export default class SwapeeWalletPickerHtmlComputer extends AbstractSwapeeWalletPickerComputer.implements(
 /** @type {!xyz.swapee.wc.ISwapeeWalletPickerComputer} */ ({
  __$constructor:__$constructor,
  adaptLoadBitcoinAddress:adaptLoadBitcoinAddress,
  adaptWalletsRotor:adaptWalletsRotor,
  adapt:[preadaptLoadBitcoinAddress,preadaptWalletsRotor],
 }),
){}