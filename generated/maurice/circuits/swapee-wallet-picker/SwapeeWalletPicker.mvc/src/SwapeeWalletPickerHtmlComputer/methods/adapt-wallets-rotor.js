/** @type {xyz.swapee.wc.ISwapeeWalletPickerComputer._adaptWalletsRotor} */
export default function adaptWalletsRotor({wallets:wallets}) {
 return{
  walletsRotor:wallets.map((address)=>({address:address})),
 }
}