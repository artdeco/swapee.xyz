/** @type {xyz.swapee.wc.ISwapeeWalletPickerComputer._adaptLoadBitcoinAddress} */
export default async function adaptLoadBitcoinAddress({token:token,swapeeHost:swapeeHost}) {
 const {
  asIStatefulLoader: {
   fetchJSON:fetchJSON, // this can be applied to the best rates too via onramper
  },
 }=/**@type {!xyz.swapee.wc.ISwapeeWalletPickerComputer}*/(this)
 const data=await fetchJSON(`${swapeeHost}/invoice`,{
  method:'POST',
  headers:{
   'Authorization':`Bearer ${token}`,
   "Content-Type": "application/json",
  },
  body:JSON.stringify({
   'invoice':{
    amount:0,
    type:'bitcoin',
   },
  }),
 })
 // debugger
 if(!data) return {}
 const{'hash':hash}=data

 return{invoice:hash}
}