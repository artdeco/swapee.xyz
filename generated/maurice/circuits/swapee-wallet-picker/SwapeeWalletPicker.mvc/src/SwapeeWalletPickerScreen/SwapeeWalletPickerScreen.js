import deduceInputs from './methods/deduce-inputs'
import AbstractSwapeeWalletPickerControllerAT from '../../gen/AbstractSwapeeWalletPickerControllerAT'
import SwapeeWalletPickerDisplay from '../SwapeeWalletPickerDisplay'
import AbstractSwapeeWalletPickerScreen from '../../gen/AbstractSwapeeWalletPickerScreen'

/** @extends {xyz.swapee.wc.SwapeeWalletPickerScreen} */
export default class extends AbstractSwapeeWalletPickerScreen.implements(
 AbstractSwapeeWalletPickerControllerAT,
 SwapeeWalletPickerDisplay,
 /**@type {!xyz.swapee.wc.ISwapeeWalletPickerScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ISwapeeWalletPickerScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:5141253786,
 }),
){}