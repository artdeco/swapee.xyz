/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement._buildSwapeeMe} */
export default function buildSwapeeMe({token},{signIn}) {
 return (<div $id="SwapeeWalletPicker">
  <button $id="SwapeeLoginBu" onClick={signIn} />
  <button $id="SwapeeAccountWr" $conceal={token} />
  <button $id="CreateInvoiceBu" $reveal={token} />
 </div>)
}