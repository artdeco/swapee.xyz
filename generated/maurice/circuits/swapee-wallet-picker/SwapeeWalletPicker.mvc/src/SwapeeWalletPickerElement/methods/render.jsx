import {WalletDynamo} from '../../../../../../../../maurice/circuits/swapee-wallet-picker/dyns/wallet-dynamo'
import {landProxy} from '@mauriceguest/guest2'

/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement._render} */
export default function SwapeeWalletPickerRender({walletsRotor:walletsRotor,loading:loading},{
 makeWalletElement:makeWalletElement,pulseLoadCreateInvoice,
}) {
 return (<div $id="SwapeeWalletPicker">
  <div $id="Wallets" $noc $rotor={walletsRotor} $dyn="Wallet">
   {walletsRotor&&walletsRotor.map(state=>{
    return WalletDynamo.call(null,state,/**@type {!xyz.swapee.wc.SwapeeWalletPickerLand}*/(landProxy))
   })}
  </div>
  <button $id="CreateInvoiceBu" disabled={loading} onClick={pulseLoadCreateInvoice} />
  <span $id="InvoiceLoIn" $reveal={loading} />
 </div>)
}