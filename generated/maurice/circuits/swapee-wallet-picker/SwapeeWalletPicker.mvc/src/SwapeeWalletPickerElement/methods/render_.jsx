import {WalletDynamo} from '../../../../../../../../maurice/circuits/swapee-wallet-picker/dyns/wallet-dynamo'

/** @type {xyz.swapee.wc.ISwapeeWalletPickerElement._render_} */
export default function SwapeeWalletPickerRender({walletsRotor:walletsRotor,loading:loading},{
 makeWalletElement:makeWalletElement,pulseLoadCreateInvoice,
}) {
 return (<div $id="SwapeeWalletPicker">
  <WalletDynamo $id="Wallets" $rotor={walletsRotor} $make={makeWalletElement} />
  <button $id="CreateInvoiceBu" disabled={loading} onClick={pulseLoadCreateInvoice} />
  <span $id="InvoiceLoIn" $reveal={loading} />

  <div $template="WalletTemplate" />
 </div>)
}