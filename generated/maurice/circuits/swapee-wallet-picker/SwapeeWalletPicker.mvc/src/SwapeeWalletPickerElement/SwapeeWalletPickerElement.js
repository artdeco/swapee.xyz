import solder from './methods/solder'
import server from './methods/server'
import buildSwapeeMe from './methods/build-swapee-me'
import render from './methods/render'
import SwapeeWalletPickerServerController from '../SwapeeWalletPickerServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractSwapeeWalletPickerElement from '../../gen/AbstractSwapeeWalletPickerElement'

/** @extends {xyz.swapee.wc.SwapeeWalletPickerElement} */
export default class SwapeeWalletPickerElement extends AbstractSwapeeWalletPickerElement.implements(
 SwapeeWalletPickerServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ISwapeeWalletPickerElement} */ ({
  solder:solder,
  server:server,
  buildSwapeeMe:buildSwapeeMe,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
   classesMap:       true,
   rootSelector:     `.SwapeeWalletPicker`,
   stylesheet:       'html/styles/SwapeeWalletPicker.css',
   blockName:        'html/SwapeeWalletPickerBlock.html',
  }),
){}

// thank you for using web circuits
