import {StatefulLoader} from '@mauriceguest/guest2'
import SwapeeWalletPickerHtmlController from '../SwapeeWalletPickerHtmlController'
import SwapeeWalletPickerHtmlComputer from '../SwapeeWalletPickerHtmlComputer'
import SwapeeWalletPickerGenerator from '../SwapeeWalletPickerGenerator'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractSwapeeWalletPickerHtmlComponent} from '../../gen/AbstractSwapeeWalletPickerHtmlComponent'

/** @extends {xyz.swapee.wc.SwapeeWalletPickerHtmlComponent} */
export default class extends AbstractSwapeeWalletPickerHtmlComponent.implements(
 SwapeeWalletPickerHtmlController,
 SwapeeWalletPickerHtmlComputer,
 SwapeeWalletPickerGenerator,
 IntegratedComponentInitialiser,

  StatefulLoader,
){}