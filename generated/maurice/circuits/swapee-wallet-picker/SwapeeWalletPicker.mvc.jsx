import {StatefulLoadable} from '@mauriceguest/guest2'

/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPicker} */
export default class AbstractSwapeeWalletPicker extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerComputer} */
export class AbstractSwapeeWalletPickerComputer extends (<computer>
   <adapter name="adaptWalletsRotor">
    <xyz.swapee.wc.ISwapeeWalletPickerCore wallets />
    <outputs>
     <xyz.swapee.wc.ISwapeeWalletPickerCore walletsRotor />
    </outputs>
   </adapter>

   <adapter name="adaptLoadBitcoinAddress" async>
    <xyz.swapee.wc.ISwapeeWalletPickerCore loadCreateInvoice="required" />
    <xyz.swapee.wc.ISwapeeMeCore swapeeHost="required" token="required" />
    <outputs>
     <xyz.swapee.wc.ISwapeeWalletPickerCore invoice  />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerCPU} */
export class AbstractSwapeeWalletPickerCPU extends (<cpu>


  <cores>
   <guest.maurice.IStatefulLoadable.State $={StatefulLoadable} />
  </cores>
</cpu>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerController} */
export class AbstractSwapeeWalletPickerController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerPort} */
export class SwapeeWalletPickerPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerView} */
export class AbstractSwapeeWalletPickerView extends (<view>
  <classes>
   <string opt name="Class">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerElement} */
export class AbstractSwapeeWalletPickerElement extends (<element v3 html mv>
 <block src="./SwapeeWalletPicker.mvc/src/SwapeeWalletPickerElement/methods/render_.jsx" />
 <inducer src="./SwapeeWalletPicker.mvc/src/SwapeeWalletPickerElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeWalletPickerHtmlComponent} */
export class AbstractSwapeeWalletPickerHtmlComponent extends (<html-ic>
  <records>
    <record name="ISwapeeWalletPicker.WalletStator">
    <string name="address" />
    A single wallet available to the user.
    <is-stator dyn="Wallet" component-name="ISwapeeWalletPicker" />

    The stator of the _WalletDynamo_.
  </record>
  <record name="ISwapeeWalletPicker.WalletBrushes">
    <string name="address" />
    A single wallet available to the user.
    The brushes inside the rotor of the _WalletDynamo_.
  </record>
  </records>

  <connectors>

   <xyz.swapee.wc.ISwapeeMe via="SwapeeMe" />
  </connectors>

</html-ic>) { }
// </class-end>