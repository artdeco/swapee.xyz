import AbstractHyperSwapeeLoadingInterruptLine from '../../../../gen/AbstractSwapeeLoadingInterruptLine/hyper/AbstractHyperSwapeeLoadingInterruptLine'
import SwapeeLoadingInterruptLine from '../../SwapeeLoadingInterruptLine'
import SwapeeLoadingInterruptLineGeneralAspects from '../SwapeeLoadingInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperSwapeeLoadingInterruptLine} */
export default class extends AbstractHyperSwapeeLoadingInterruptLine
 .consults(
  SwapeeLoadingInterruptLineGeneralAspects,
 )
 .implements(
  SwapeeLoadingInterruptLine,
 )
{}