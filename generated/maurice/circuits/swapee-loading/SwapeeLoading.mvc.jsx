/** @extends {xyz.swapee.wc.AbstractSwapeeLoading} */
export default class AbstractSwapeeLoading extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractSwapeeLoadingComputer} */
export class AbstractSwapeeLoadingComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeLoadingController} */
export class AbstractSwapeeLoadingController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeLoadingPort} */
export class SwapeeLoadingPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeLoadingView} */
export class AbstractSwapeeLoadingView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeLoadingElement} */
export class AbstractSwapeeLoadingElement extends (<element v3 html mv>
 <block src="./SwapeeLoading.mvc/src/SwapeeLoadingElement/methods/render.jsx" />
 <inducer src="./SwapeeLoading.mvc/src/SwapeeLoadingElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent} */
export class AbstractSwapeeLoadingHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>