import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import SwapeeLoadingServerController from '../SwapeeLoadingServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractSwapeeLoadingElement from '../../gen/AbstractSwapeeLoadingElement'

/** @extends {xyz.swapee.wc.SwapeeLoadingElement} */
export default class SwapeeLoadingElement extends AbstractSwapeeLoadingElement.implements(
 SwapeeLoadingServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /**@type {!xyz.swapee.wc.SwapeeLoadingElement}*/({
  allocator: function allocateSelectors(){
   const{
    attributes:{id:id},asIMaurice:{page:page},
    asISwapeeLoadingElement:{
    },
   }=this
   if(!page) return // buildees
  },
 }),
 /** @type {!xyz.swapee.wc.ISwapeeLoadingElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ISwapeeLoadingElement}*/({
   classesMap: true,
   rootSelector:     `.SwapeeLoading`,
   stylesheet:       'html/styles/SwapeeLoading.css',
   blockName:        'html/SwapeeLoadingBlock.html',
  }),
){}

// thank you for using web circuits
