import deduceInputs from './methods/deduce-inputs'
import AbstractSwapeeLoadingControllerAT from '../../gen/AbstractSwapeeLoadingControllerAT'
import SwapeeLoadingDisplay from '../SwapeeLoadingDisplay'
import AbstractSwapeeLoadingScreen from '../../gen/AbstractSwapeeLoadingScreen'

/** @extends {xyz.swapee.wc.SwapeeLoadingScreen} */
export default class extends AbstractSwapeeLoadingScreen.implements(
 AbstractSwapeeLoadingControllerAT,
 SwapeeLoadingDisplay,
 /**@type {!xyz.swapee.wc.ISwapeeLoadingScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ISwapeeLoadingScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:2828646422,
 }),
){}