import SwapeeLoadingHtmlController from '../SwapeeLoadingHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractSwapeeLoadingHtmlComponent} from '../../gen/AbstractSwapeeLoadingHtmlComponent'

/** @extends {xyz.swapee.wc.SwapeeLoadingHtmlComponent} */
export default class extends AbstractSwapeeLoadingHtmlComponent.implements(
 SwapeeLoadingHtmlController,
 IntegratedComponentInitialiser,
){}