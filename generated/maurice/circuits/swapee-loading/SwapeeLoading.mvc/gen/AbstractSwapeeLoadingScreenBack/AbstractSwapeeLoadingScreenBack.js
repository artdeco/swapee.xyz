import AbstractSwapeeLoadingScreenAT from '../AbstractSwapeeLoadingScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingScreenBack}
 */
function __AbstractSwapeeLoadingScreenBack() {}
__AbstractSwapeeLoadingScreenBack.prototype = /** @type {!_AbstractSwapeeLoadingScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingScreen}
 */
class _AbstractSwapeeLoadingScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingScreen} ‎
 */
class AbstractSwapeeLoadingScreenBack extends newAbstract(
 _AbstractSwapeeLoadingScreenBack,282864642223,null,{
  asISwapeeLoadingScreen:1,
  superSwapeeLoadingScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreen} */
AbstractSwapeeLoadingScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreen} */
function AbstractSwapeeLoadingScreenBackClass(){}

export default AbstractSwapeeLoadingScreenBack


AbstractSwapeeLoadingScreenBack[$implementations]=[
 __AbstractSwapeeLoadingScreenBack,
 AbstractSwapeeLoadingScreenAT,
]