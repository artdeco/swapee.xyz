import SwapeeLoadingBuffer from '../SwapeeLoadingBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {SwapeeLoadingPortConnector} from '../SwapeeLoadingPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingController}
 */
function __AbstractSwapeeLoadingController() {}
__AbstractSwapeeLoadingController.prototype = /** @type {!_AbstractSwapeeLoadingController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingController}
 */
class _AbstractSwapeeLoadingController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingController} ‎
 */
export class AbstractSwapeeLoadingController extends newAbstract(
 _AbstractSwapeeLoadingController,282864642217,null,{
  asISwapeeLoadingController:1,
  superSwapeeLoadingController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingController} */
AbstractSwapeeLoadingController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingController} */
function AbstractSwapeeLoadingControllerClass(){}


AbstractSwapeeLoadingController[$implementations]=[
 AbstractSwapeeLoadingControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.ISwapeeLoadingPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractSwapeeLoadingController,
 SwapeeLoadingBuffer,
 IntegratedController,
 /**@type {!AbstractSwapeeLoadingController}*/(SwapeeLoadingPortConnector),
]


export default AbstractSwapeeLoadingController