import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoadingElementPort}
 */
function __SwapeeLoadingElementPort() {}
__SwapeeLoadingElementPort.prototype = /** @type {!_SwapeeLoadingElementPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoadingElementPort} */ function SwapeeLoadingElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingElementPort}
 */
class _SwapeeLoadingElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingElementPort} ‎
 */
class SwapeeLoadingElementPort extends newAbstract(
 _SwapeeLoadingElementPort,282864642211,SwapeeLoadingElementPortConstructor,{
  asISwapeeLoadingElementPort:1,
  superSwapeeLoadingElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingElementPort} */
SwapeeLoadingElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingElementPort} */
function SwapeeLoadingElementPortClass(){}

export default SwapeeLoadingElementPort


SwapeeLoadingElementPort[$implementations]=[
 __SwapeeLoadingElementPort,
 SwapeeLoadingElementPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
   })
  },
 }),
]