import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingScreenAT}
 */
function __AbstractSwapeeLoadingScreenAT() {}
__AbstractSwapeeLoadingScreenAT.prototype = /** @type {!_AbstractSwapeeLoadingScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT}
 */
class _AbstractSwapeeLoadingScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeLoadingScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT} ‎
 */
class AbstractSwapeeLoadingScreenAT extends newAbstract(
 _AbstractSwapeeLoadingScreenAT,282864642225,null,{
  asISwapeeLoadingScreenAT:1,
  superSwapeeLoadingScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT} */
AbstractSwapeeLoadingScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT} */
function AbstractSwapeeLoadingScreenATClass(){}

export default AbstractSwapeeLoadingScreenAT


AbstractSwapeeLoadingScreenAT[$implementations]=[
 __AbstractSwapeeLoadingScreenAT,
 UartUniversal,
]