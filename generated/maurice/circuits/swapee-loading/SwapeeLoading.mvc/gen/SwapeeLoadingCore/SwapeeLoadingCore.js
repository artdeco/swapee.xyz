import {mountPins} from '@webcircuits/webcircuits'
import {SwapeeLoadingMemoryPQs} from '../../pqs/SwapeeLoadingMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoadingCore}
 */
function __SwapeeLoadingCore() {}
__SwapeeLoadingCore.prototype = /** @type {!_SwapeeLoadingCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingCore}
 */
class _SwapeeLoadingCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingCore} ‎
 */
class SwapeeLoadingCore extends newAbstract(
 _SwapeeLoadingCore,28286464225,null,{
  asISwapeeLoadingCore:1,
  superSwapeeLoadingCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingCore} */
SwapeeLoadingCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingCore} */
function SwapeeLoadingCoreClass(){}

export default SwapeeLoadingCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoadingOuterCore}
 */
function __SwapeeLoadingOuterCore() {}
__SwapeeLoadingOuterCore.prototype = /** @type {!_SwapeeLoadingOuterCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoadingOuterCore} */
export function SwapeeLoadingOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeLoadingOuterCore.Model}*/
  this.model={
    loading: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingOuterCore}
 */
class _SwapeeLoadingOuterCore { }
/**
 * The _ISwapeeLoading_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingOuterCore} ‎
 */
export class SwapeeLoadingOuterCore extends newAbstract(
 _SwapeeLoadingOuterCore,28286464222,SwapeeLoadingOuterCoreConstructor,{
  asISwapeeLoadingOuterCore:1,
  superSwapeeLoadingOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingOuterCore} */
SwapeeLoadingOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingOuterCore} */
function SwapeeLoadingOuterCoreClass(){}


SwapeeLoadingOuterCore[$implementations]=[
 __SwapeeLoadingOuterCore,
 SwapeeLoadingOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingOuterCore}*/({
  constructor(){
   mountPins(this.model,'',SwapeeLoadingMemoryPQs)

  },
 }),
]

SwapeeLoadingCore[$implementations]=[
 SwapeeLoadingCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingCore}*/({
  resetCore(){
   this.resetSwapeeLoadingCore()
  },
  resetSwapeeLoadingCore(){
   SwapeeLoadingOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.SwapeeLoadingOuterCore}*/(
     /**@type {!xyz.swapee.wc.ISwapeeLoadingOuterCore}*/(this)),
   )
  },
 }),
 __SwapeeLoadingCore,
 SwapeeLoadingOuterCore,
]

export {SwapeeLoadingCore}