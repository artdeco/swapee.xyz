import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingComputer}
 */
function __AbstractSwapeeLoadingComputer() {}
__AbstractSwapeeLoadingComputer.prototype = /** @type {!_AbstractSwapeeLoadingComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingComputer}
 */
class _AbstractSwapeeLoadingComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingComputer} ‎
 */
export class AbstractSwapeeLoadingComputer extends newAbstract(
 _AbstractSwapeeLoadingComputer,28286464221,null,{
  asISwapeeLoadingComputer:1,
  superSwapeeLoadingComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingComputer} */
AbstractSwapeeLoadingComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingComputer} */
function AbstractSwapeeLoadingComputerClass(){}


AbstractSwapeeLoadingComputer[$implementations]=[
 __AbstractSwapeeLoadingComputer,
 Adapter,
]


export default AbstractSwapeeLoadingComputer