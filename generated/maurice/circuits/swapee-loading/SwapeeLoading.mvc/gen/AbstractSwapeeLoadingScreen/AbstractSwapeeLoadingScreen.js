import AbstractSwapeeLoadingScreenAR from '../AbstractSwapeeLoadingScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {SwapeeLoadingInputsPQs} from '../../pqs/SwapeeLoadingInputsPQs'
import {SwapeeLoadingMemoryQPs} from '../../pqs/SwapeeLoadingMemoryQPs'
import {SwapeeLoadingVdusPQs} from '../../pqs/SwapeeLoadingVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingScreen}
 */
function __AbstractSwapeeLoadingScreen() {}
__AbstractSwapeeLoadingScreen.prototype = /** @type {!_AbstractSwapeeLoadingScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingScreen}
 */
class _AbstractSwapeeLoadingScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingScreen} ‎
 */
class AbstractSwapeeLoadingScreen extends newAbstract(
 _AbstractSwapeeLoadingScreen,282864642222,null,{
  asISwapeeLoadingScreen:1,
  superSwapeeLoadingScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingScreen} */
AbstractSwapeeLoadingScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingScreen} */
function AbstractSwapeeLoadingScreenClass(){}

export default AbstractSwapeeLoadingScreen


AbstractSwapeeLoadingScreen[$implementations]=[
 __AbstractSwapeeLoadingScreen,
 AbstractSwapeeLoadingScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingScreen}*/({
  inputsPQs:SwapeeLoadingInputsPQs,
  memoryQPs:SwapeeLoadingMemoryQPs,
 }),
 Screen,
 AbstractSwapeeLoadingScreenAR,
 AbstractSwapeeLoadingScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingScreen}*/({
  vdusPQs:SwapeeLoadingVdusPQs,
 }),
]