import AbstractSwapeeLoadingDisplay from '../AbstractSwapeeLoadingDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {SwapeeLoadingVdusPQs} from '../../pqs/SwapeeLoadingVdusPQs'
import {SwapeeLoadingVdusQPs} from '../../pqs/SwapeeLoadingVdusQPs'
import {SwapeeLoadingMemoryPQs} from '../../pqs/SwapeeLoadingMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingGPU}
 */
function __AbstractSwapeeLoadingGPU() {}
__AbstractSwapeeLoadingGPU.prototype = /** @type {!_AbstractSwapeeLoadingGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingGPU}
 */
class _AbstractSwapeeLoadingGPU { }
/**
 * Handles the periphery of the _ISwapeeLoadingDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingGPU} ‎
 */
class AbstractSwapeeLoadingGPU extends newAbstract(
 _AbstractSwapeeLoadingGPU,282864642213,null,{
  asISwapeeLoadingGPU:1,
  superSwapeeLoadingGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingGPU} */
AbstractSwapeeLoadingGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingGPU} */
function AbstractSwapeeLoadingGPUClass(){}

export default AbstractSwapeeLoadingGPU


AbstractSwapeeLoadingGPU[$implementations]=[
 __AbstractSwapeeLoadingGPU,
 AbstractSwapeeLoadingGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingGPU}*/({
  vdusPQs:SwapeeLoadingVdusPQs,
  vdusQPs:SwapeeLoadingVdusQPs,
  memoryPQs:SwapeeLoadingMemoryPQs,
 }),
 AbstractSwapeeLoadingDisplay,
 BrowserView,
]