import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingControllerAT}
 */
function __AbstractSwapeeLoadingControllerAT() {}
__AbstractSwapeeLoadingControllerAT.prototype = /** @type {!_AbstractSwapeeLoadingControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT}
 */
class _AbstractSwapeeLoadingControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeLoadingControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT} ‎
 */
class AbstractSwapeeLoadingControllerAT extends newAbstract(
 _AbstractSwapeeLoadingControllerAT,282864642221,null,{
  asISwapeeLoadingControllerAT:1,
  superSwapeeLoadingControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT} */
AbstractSwapeeLoadingControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT} */
function AbstractSwapeeLoadingControllerATClass(){}

export default AbstractSwapeeLoadingControllerAT


AbstractSwapeeLoadingControllerAT[$implementations]=[
 __AbstractSwapeeLoadingControllerAT,
 UartUniversal,
 AbstractSwapeeLoadingControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeLoadingControllerAT}*/({
  get asISwapeeLoadingController(){
   return this
  },
 }),
]