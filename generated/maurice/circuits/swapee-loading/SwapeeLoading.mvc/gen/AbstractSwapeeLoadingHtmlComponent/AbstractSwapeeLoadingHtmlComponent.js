import AbstractSwapeeLoadingGPU from '../AbstractSwapeeLoadingGPU'
import AbstractSwapeeLoadingScreenBack from '../AbstractSwapeeLoadingScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {SwapeeLoadingInputsQPs} from '../../pqs/SwapeeLoadingInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeLoading from '../AbstractSwapeeLoading'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingHtmlComponent}
 */
function __AbstractSwapeeLoadingHtmlComponent() {}
__AbstractSwapeeLoadingHtmlComponent.prototype = /** @type {!_AbstractSwapeeLoadingHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent}
 */
class _AbstractSwapeeLoadingHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.SwapeeLoadingHtmlComponent} */ (res)
  }
}
/**
 * The _ISwapeeLoading_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent} ‎
 */
export class AbstractSwapeeLoadingHtmlComponent extends newAbstract(
 _AbstractSwapeeLoadingHtmlComponent,28286464229,null,{
  asISwapeeLoadingHtmlComponent:1,
  superSwapeeLoadingHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent} */
AbstractSwapeeLoadingHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent} */
function AbstractSwapeeLoadingHtmlComponentClass(){}


AbstractSwapeeLoadingHtmlComponent[$implementations]=[
 __AbstractSwapeeLoadingHtmlComponent,
 HtmlComponent,
 AbstractSwapeeLoading,
 AbstractSwapeeLoadingGPU,
 AbstractSwapeeLoadingScreenBack,
 AbstractSwapeeLoadingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingHtmlComponent}*/({
  inputsQPs:SwapeeLoadingInputsQPs,
 }),
]