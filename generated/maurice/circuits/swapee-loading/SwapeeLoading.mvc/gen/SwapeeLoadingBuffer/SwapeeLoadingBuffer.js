import {makeBuffers} from '@webcircuits/webcircuits'

export const SwapeeLoadingBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  loading:Boolean,
 }),
})

export default SwapeeLoadingBuffer