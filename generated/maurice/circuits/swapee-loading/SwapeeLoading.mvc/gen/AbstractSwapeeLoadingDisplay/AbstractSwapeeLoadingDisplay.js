import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingDisplay}
 */
function __AbstractSwapeeLoadingDisplay() {}
__AbstractSwapeeLoadingDisplay.prototype = /** @type {!_AbstractSwapeeLoadingDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingDisplay}
 */
class _AbstractSwapeeLoadingDisplay { }
/**
 * Display for presenting information from the _ISwapeeLoading_.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingDisplay} ‎
 */
class AbstractSwapeeLoadingDisplay extends newAbstract(
 _AbstractSwapeeLoadingDisplay,282864642214,null,{
  asISwapeeLoadingDisplay:1,
  superSwapeeLoadingDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingDisplay} */
AbstractSwapeeLoadingDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingDisplay} */
function AbstractSwapeeLoadingDisplayClass(){}

export default AbstractSwapeeLoadingDisplay


AbstractSwapeeLoadingDisplay[$implementations]=[
 __AbstractSwapeeLoadingDisplay,
 Display,
]