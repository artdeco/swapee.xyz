import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {SwapeeLoadingInputsPQs} from '../../pqs/SwapeeLoadingInputsPQs'
import {SwapeeLoadingOuterCoreConstructor} from '../SwapeeLoadingCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoadingPort}
 */
function __SwapeeLoadingPort() {}
__SwapeeLoadingPort.prototype = /** @type {!_SwapeeLoadingPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoadingPort} */ function SwapeeLoadingPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.SwapeeLoadingOuterCore} */ ({model:null})
  SwapeeLoadingOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingPort}
 */
class _SwapeeLoadingPort { }
/**
 * The port that serves as an interface to the _ISwapeeLoading_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingPort} ‎
 */
export class SwapeeLoadingPort extends newAbstract(
 _SwapeeLoadingPort,28286464223,SwapeeLoadingPortConstructor,{
  asISwapeeLoadingPort:1,
  superSwapeeLoadingPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingPort} */
SwapeeLoadingPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingPort} */
function SwapeeLoadingPortClass(){}

export const SwapeeLoadingPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ISwapeeLoading.Pinout>}*/({
 get Port() { return SwapeeLoadingPort },
})

SwapeeLoadingPort[$implementations]=[
 SwapeeLoadingPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingPort}*/({
  resetPort(){
   this.resetSwapeeLoadingPort()
  },
  resetSwapeeLoadingPort(){
   SwapeeLoadingPortConstructor.call(this)
  },
 }),
 __SwapeeLoadingPort,
 Parametric,
 SwapeeLoadingPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingPort}*/({
  constructor(){
   mountPins(this.inputs,'',SwapeeLoadingInputsPQs)
  },
 }),
]


export default SwapeeLoadingPort