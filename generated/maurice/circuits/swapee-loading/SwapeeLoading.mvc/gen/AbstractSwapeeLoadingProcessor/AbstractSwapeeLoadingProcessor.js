import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingProcessor}
 */
function __AbstractSwapeeLoadingProcessor() {}
__AbstractSwapeeLoadingProcessor.prototype = /** @type {!_AbstractSwapeeLoadingProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingProcessor}
 */
class _AbstractSwapeeLoadingProcessor { }
/**
 * The processor to compute changes to the memory for the _ISwapeeLoading_.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingProcessor} ‎
 */
class AbstractSwapeeLoadingProcessor extends newAbstract(
 _AbstractSwapeeLoadingProcessor,28286464226,null,{
  asISwapeeLoadingProcessor:1,
  superSwapeeLoadingProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingProcessor} */
AbstractSwapeeLoadingProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingProcessor} */
function AbstractSwapeeLoadingProcessorClass(){}

export default AbstractSwapeeLoadingProcessor


AbstractSwapeeLoadingProcessor[$implementations]=[
 __AbstractSwapeeLoadingProcessor,
]