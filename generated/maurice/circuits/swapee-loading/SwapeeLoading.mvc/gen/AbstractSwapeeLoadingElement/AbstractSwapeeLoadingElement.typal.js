
import AbstractSwapeeLoading from '../AbstractSwapeeLoading'

/** @abstract {xyz.swapee.wc.ISwapeeLoadingElement} */
export default class AbstractSwapeeLoadingElement { }



AbstractSwapeeLoadingElement[$implementations]=[AbstractSwapeeLoading,
 /** @type {!AbstractSwapeeLoadingElement} */ ({
  rootId:'SwapeeLoading',
  __$id:2828646422,
  fqn:'xyz.swapee.wc.ISwapeeLoading',
  maurice_element_v3:true,
 }),
]