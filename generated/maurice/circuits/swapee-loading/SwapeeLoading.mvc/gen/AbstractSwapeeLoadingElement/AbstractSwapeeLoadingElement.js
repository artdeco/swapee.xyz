import SwapeeLoadingElementPort from '../SwapeeLoadingElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {SwapeeLoadingInputsPQs} from '../../pqs/SwapeeLoadingInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeLoading from '../AbstractSwapeeLoading'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingElement}
 */
function __AbstractSwapeeLoadingElement() {}
__AbstractSwapeeLoadingElement.prototype = /** @type {!_AbstractSwapeeLoadingElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingElement}
 */
class _AbstractSwapeeLoadingElement { }
/**
 * A component description.
 *
 * The _ISwapeeLoading_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingElement} ‎
 */
class AbstractSwapeeLoadingElement extends newAbstract(
 _AbstractSwapeeLoadingElement,282864642210,null,{
  asISwapeeLoadingElement:1,
  superSwapeeLoadingElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingElement} */
AbstractSwapeeLoadingElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingElement} */
function AbstractSwapeeLoadingElementClass(){}

export default AbstractSwapeeLoadingElement


AbstractSwapeeLoadingElement[$implementations]=[
 __AbstractSwapeeLoadingElement,
 ElementBase,
 AbstractSwapeeLoadingElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':loading':loadingColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'loading':loadingAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(loadingAttr===undefined?{'loading':loadingColAttr}:{}),
   }
  },
 }),
 AbstractSwapeeLoadingElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'loading':loadingAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    loading:loadingAttr,
   }
  },
 }),
 AbstractSwapeeLoadingElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractSwapeeLoadingElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingElement}*/({
  inputsPQs:SwapeeLoadingInputsPQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractSwapeeLoadingElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoadingElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','loading','no-solder',':no-solder',':loading','fe646','a14fa','children']),
   })
  },
  get Port(){
   return SwapeeLoadingElementPort
  },
 }),
]



AbstractSwapeeLoadingElement[$implementations]=[AbstractSwapeeLoading,
 /** @type {!AbstractSwapeeLoadingElement} */ ({
  rootId:'SwapeeLoading',
  __$id:2828646422,
  fqn:'xyz.swapee.wc.ISwapeeLoading',
  maurice_element_v3:true,
 }),
]