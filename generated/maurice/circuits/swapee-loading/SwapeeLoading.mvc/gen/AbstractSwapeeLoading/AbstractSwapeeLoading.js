import AbstractSwapeeLoadingProcessor from '../AbstractSwapeeLoadingProcessor'
import {SwapeeLoadingCore} from '../SwapeeLoadingCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractSwapeeLoadingComputer} from '../AbstractSwapeeLoadingComputer'
import {AbstractSwapeeLoadingController} from '../AbstractSwapeeLoadingController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoading}
 */
function __AbstractSwapeeLoading() {}
__AbstractSwapeeLoading.prototype = /** @type {!_AbstractSwapeeLoading} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoading}
 */
class _AbstractSwapeeLoading { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoading} ‎
 */
class AbstractSwapeeLoading extends newAbstract(
 _AbstractSwapeeLoading,28286464227,null,{
  asISwapeeLoading:1,
  superSwapeeLoading:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoading} */
AbstractSwapeeLoading.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoading} */
function AbstractSwapeeLoadingClass(){}

export default AbstractSwapeeLoading


AbstractSwapeeLoading[$implementations]=[
 __AbstractSwapeeLoading,
 SwapeeLoadingCore,
 AbstractSwapeeLoadingProcessor,
 IntegratedComponent,
 AbstractSwapeeLoadingComputer,
 AbstractSwapeeLoadingController,
]


export {AbstractSwapeeLoading}