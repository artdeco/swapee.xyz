import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingControllerAR}
 */
function __AbstractSwapeeLoadingControllerAR() {}
__AbstractSwapeeLoadingControllerAR.prototype = /** @type {!_AbstractSwapeeLoadingControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR}
 */
class _AbstractSwapeeLoadingControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeLoadingControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR} ‎
 */
class AbstractSwapeeLoadingControllerAR extends newAbstract(
 _AbstractSwapeeLoadingControllerAR,282864642220,null,{
  asISwapeeLoadingControllerAR:1,
  superSwapeeLoadingControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR} */
AbstractSwapeeLoadingControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR} */
function AbstractSwapeeLoadingControllerARClass(){}

export default AbstractSwapeeLoadingControllerAR


AbstractSwapeeLoadingControllerAR[$implementations]=[
 __AbstractSwapeeLoadingControllerAR,
 AR,
 AbstractSwapeeLoadingControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeLoadingControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]