import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingDisplay}
 */
function __AbstractSwapeeLoadingDisplay() {}
__AbstractSwapeeLoadingDisplay.prototype = /** @type {!_AbstractSwapeeLoadingDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay}
 */
class _AbstractSwapeeLoadingDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay} ‎
 */
class AbstractSwapeeLoadingDisplay extends newAbstract(
 _AbstractSwapeeLoadingDisplay,282864642216,null,{
  asISwapeeLoadingDisplay:1,
  superSwapeeLoadingDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay} */
AbstractSwapeeLoadingDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay} */
function AbstractSwapeeLoadingDisplayClass(){}

export default AbstractSwapeeLoadingDisplay


AbstractSwapeeLoadingDisplay[$implementations]=[
 __AbstractSwapeeLoadingDisplay,
 GraphicsDriverBack,
]