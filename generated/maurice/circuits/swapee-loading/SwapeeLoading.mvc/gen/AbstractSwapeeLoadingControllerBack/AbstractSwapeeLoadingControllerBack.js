import AbstractSwapeeLoadingControllerAR from '../AbstractSwapeeLoadingControllerAR'
import {AbstractSwapeeLoadingController} from '../AbstractSwapeeLoadingController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingControllerBack}
 */
function __AbstractSwapeeLoadingControllerBack() {}
__AbstractSwapeeLoadingControllerBack.prototype = /** @type {!_AbstractSwapeeLoadingControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingController}
 */
class _AbstractSwapeeLoadingControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoadingController} ‎
 */
class AbstractSwapeeLoadingControllerBack extends newAbstract(
 _AbstractSwapeeLoadingControllerBack,282864642219,null,{
  asISwapeeLoadingController:1,
  superSwapeeLoadingController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingController} */
AbstractSwapeeLoadingControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingController} */
function AbstractSwapeeLoadingControllerBackClass(){}

export default AbstractSwapeeLoadingControllerBack


AbstractSwapeeLoadingControllerBack[$implementations]=[
 __AbstractSwapeeLoadingControllerBack,
 AbstractSwapeeLoadingController,
 AbstractSwapeeLoadingControllerAR,
 DriverBack,
]