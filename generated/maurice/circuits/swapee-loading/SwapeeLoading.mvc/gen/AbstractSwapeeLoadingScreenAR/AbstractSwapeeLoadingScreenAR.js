import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoadingScreenAR}
 */
function __AbstractSwapeeLoadingScreenAR() {}
__AbstractSwapeeLoadingScreenAR.prototype = /** @type {!_AbstractSwapeeLoadingScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR}
 */
class _AbstractSwapeeLoadingScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeLoadingScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR} ‎
 */
class AbstractSwapeeLoadingScreenAR extends newAbstract(
 _AbstractSwapeeLoadingScreenAR,282864642224,null,{
  asISwapeeLoadingScreenAR:1,
  superSwapeeLoadingScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR} */
AbstractSwapeeLoadingScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR} */
function AbstractSwapeeLoadingScreenARClass(){}

export default AbstractSwapeeLoadingScreenAR


AbstractSwapeeLoadingScreenAR[$implementations]=[
 __AbstractSwapeeLoadingScreenAR,
 AR,
 AbstractSwapeeLoadingScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeLoadingScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractSwapeeLoadingScreenAR}