import {SwapeeLoadingVdusPQs} from './SwapeeLoadingVdusPQs'
export const SwapeeLoadingVdusQPs=/**@type {!xyz.swapee.wc.SwapeeLoadingVdusQPs}*/(Object.keys(SwapeeLoadingVdusPQs)
 .reduce((a,k)=>{a[SwapeeLoadingVdusPQs[k]]=k;return a},{}))