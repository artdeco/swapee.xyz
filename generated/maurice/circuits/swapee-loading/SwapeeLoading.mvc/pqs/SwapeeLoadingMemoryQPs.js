import {SwapeeLoadingMemoryPQs} from './SwapeeLoadingMemoryPQs'
export const SwapeeLoadingMemoryQPs=/**@type {!xyz.swapee.wc.SwapeeLoadingMemoryQPs}*/(Object.keys(SwapeeLoadingMemoryPQs)
 .reduce((a,k)=>{a[SwapeeLoadingMemoryPQs[k]]=k;return a},{}))