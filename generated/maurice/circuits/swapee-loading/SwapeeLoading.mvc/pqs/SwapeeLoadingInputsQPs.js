import {SwapeeLoadingInputsPQs} from './SwapeeLoadingInputsPQs'
export const SwapeeLoadingInputsQPs=/**@type {!xyz.swapee.wc.SwapeeLoadingInputsQPs}*/(Object.keys(SwapeeLoadingInputsPQs)
 .reduce((a,k)=>{a[SwapeeLoadingInputsPQs[k]]=k;return a},{}))