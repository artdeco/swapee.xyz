import {SwapeeLoadingMemoryPQs} from './SwapeeLoadingMemoryPQs'
export const SwapeeLoadingInputsPQs=/**@type {!xyz.swapee.wc.SwapeeLoadingInputsQPs}*/({
 ...SwapeeLoadingMemoryPQs,
})