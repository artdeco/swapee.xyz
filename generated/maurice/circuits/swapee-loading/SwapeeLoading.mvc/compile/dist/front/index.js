/**
 * Display for presenting information from the _ISwapeeLoading_.
 * @extends {xyz.swapee.wc.SwapeeLoadingDisplay}
 */
class SwapeeLoadingDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.SwapeeLoadingScreen}
 */
class SwapeeLoadingScreen extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeLoadingDisplay = SwapeeLoadingDisplay
module.exports.SwapeeLoadingScreen = SwapeeLoadingScreen