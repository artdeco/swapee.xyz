import Module from './browser'

/** @type {typeof xyz.swapee.wc.back.SwapeeLoadingController} */
export const SwapeeLoadingController=Module['282864642261']
/** @type {typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent} */
export const SwapeeLoadingHtmlComponent=Module['282864642210']
/** @type {typeof xyz.swapee.wc.SwapeeLoadingPort} */
export const SwapeeLoadingPort=Module['28286464223']
/**@extends {xyz.swapee.wc.AbstractSwapeeLoadingController}*/
export class AbstractSwapeeLoadingController extends Module['28286464224'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingController} */
AbstractSwapeeLoadingController.class=function(){}
/**@extends {xyz.swapee.wc.AbstractSwapeeLoadingComputer}*/
export class AbstractSwapeeLoadingComputer extends Module['282864642230'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingComputer} */
AbstractSwapeeLoadingComputer.class=function(){}
/**@extends {xyz.swapee.wc.AbstractSwapeeLoading}*/
export class AbstractSwapeeLoading extends Module['28286464221'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoading} */
AbstractSwapeeLoading.class=function(){}