/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.SwapeeLoadingController}
 */
class SwapeeLoadingController extends (class {/* lazy-loaded */}) {}
/**
 * The _ISwapeeLoading_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.SwapeeLoadingHtmlComponent}
 */
class SwapeeLoadingHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeLoading_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeLoadingPort}
 */
class SwapeeLoadingPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingController}
 */
class AbstractSwapeeLoadingController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingComputer}
 */
class AbstractSwapeeLoadingComputer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoading` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoading}
 */
class AbstractSwapeeLoading extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeLoadingController = SwapeeLoadingController
module.exports.SwapeeLoadingHtmlComponent = SwapeeLoadingHtmlComponent
module.exports.SwapeeLoadingPort = SwapeeLoadingPort
module.exports.AbstractSwapeeLoadingController = AbstractSwapeeLoadingController
module.exports.AbstractSwapeeLoadingComputer = AbstractSwapeeLoadingComputer
module.exports.AbstractSwapeeLoading = AbstractSwapeeLoading