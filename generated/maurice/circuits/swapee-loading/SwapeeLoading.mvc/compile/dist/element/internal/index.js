import Module from './element'

/** @type {typeof xyz.swapee.wc.SwapeeLoadingController} */
export const SwapeeLoadingController=Module['282864642261']
/** @type {typeof xyz.swapee.wc.SwapeeLoadingElement} */
export const SwapeeLoadingElement=Module['28286464228']
/** @type {typeof xyz.swapee.wc.SwapeeLoadingPort} */
export const SwapeeLoadingPort=Module['28286464223']
/**@extends {xyz.swapee.wc.AbstractSwapeeLoadingController}*/
export class AbstractSwapeeLoadingController extends Module['28286464224'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingController} */
AbstractSwapeeLoadingController.class=function(){}
/**@extends {xyz.swapee.wc.AbstractSwapeeLoadingComputer}*/
export class AbstractSwapeeLoadingComputer extends Module['282864642230'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingComputer} */
AbstractSwapeeLoadingComputer.class=function(){}
/**@extends {xyz.swapee.wc.AbstractSwapeeLoading}*/
export class AbstractSwapeeLoading extends Module['28286464221'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoading} */
AbstractSwapeeLoading.class=function(){}