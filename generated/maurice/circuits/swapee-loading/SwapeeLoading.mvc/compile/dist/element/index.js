/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.SwapeeLoadingController}
 */
class SwapeeLoadingController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ISwapeeLoading_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.SwapeeLoadingElement}
 */
class SwapeeLoadingElement extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeLoading_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeLoadingPort}
 */
class SwapeeLoadingPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingController}
 */
class AbstractSwapeeLoadingController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoadingComputer}
 */
class AbstractSwapeeLoadingComputer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoading` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoading}
 */
class AbstractSwapeeLoading extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeLoadingController = SwapeeLoadingController
module.exports.SwapeeLoadingElement = SwapeeLoadingElement
module.exports.SwapeeLoadingPort = SwapeeLoadingPort
module.exports.AbstractSwapeeLoadingController = AbstractSwapeeLoadingController
module.exports.AbstractSwapeeLoadingComputer = AbstractSwapeeLoadingComputer
module.exports.AbstractSwapeeLoading = AbstractSwapeeLoading

Object.defineProperties(module.exports, {
 'SwapeeLoadingController': {get: () => require('./precompile/internal')[282864642261]},
 [282864642261]: {get: () => module.exports['SwapeeLoadingController']},
 'SwapeeLoadingElement': {get: () => require('./precompile/internal')[28286464228]},
 [28286464228]: {get: () => module.exports['SwapeeLoadingElement']},
 'SwapeeLoadingPort': {get: () => require('./precompile/internal')[28286464223]},
 [28286464223]: {get: () => module.exports['SwapeeLoadingPort']},
 'AbstractSwapeeLoadingController': {get: () => require('./precompile/internal')[28286464224]},
 [28286464224]: {get: () => module.exports['AbstractSwapeeLoadingController']},
 'AbstractSwapeeLoadingComputer': {get: () => require('./precompile/internal')[282864642230]},
 [282864642230]: {get: () => module.exports['AbstractSwapeeLoadingComputer']},
 'AbstractSwapeeLoading': {get: () => require('./precompile/internal')[28286464221]},
 [28286464221]: {get: () => module.exports['AbstractSwapeeLoading']},
})