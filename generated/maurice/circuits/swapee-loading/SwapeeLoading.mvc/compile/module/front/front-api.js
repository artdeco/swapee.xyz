import { SwapeeLoadingDisplay, SwapeeLoadingScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.SwapeeLoadingDisplay} */
export { SwapeeLoadingDisplay }
/** @lazy @api {xyz.swapee.wc.SwapeeLoadingScreen} */
export { SwapeeLoadingScreen }