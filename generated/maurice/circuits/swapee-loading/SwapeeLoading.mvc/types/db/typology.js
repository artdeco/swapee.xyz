/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ISwapeeLoadingComputer': {
  'id': 28286464221,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.ISwapeeLoadingOuterCore': {
  'id': 28286464222,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingPort': {
  'id': 28286464223,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeLoadingPort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeLoadingPortInterface': {
  'id': 28286464224,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingCore': {
  'id': 28286464225,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeLoadingCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeLoadingProcessor': {
  'id': 28286464226,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoading': {
  'id': 28286464227,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingBuffer': {
  'id': 28286464228,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingHtmlComponent': {
  'id': 28286464229,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingElement': {
  'id': 282864642210,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ISwapeeLoadingElementPort': {
  'id': 282864642211,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingDesigner': {
  'id': 282864642212,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeLoadingGPU': {
  'id': 282864642213,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingDisplay': {
  'id': 282864642214,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeLoadingVdusPQs': {
  'id': 282864642215,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeLoadingDisplay': {
  'id': 282864642216,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ISwapeeLoadingController': {
  'id': 282864642217,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ISwapeeLoadingController': {
  'id': 282864642218,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeLoadingController': {
  'id': 282864642219,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeLoadingControllerAR': {
  'id': 282864642220,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeLoadingControllerAT': {
  'id': 282864642221,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoadingScreen': {
  'id': 282864642222,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeLoadingScreen': {
  'id': 282864642223,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeLoadingScreenAR': {
  'id': 282864642224,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeLoadingScreenAT': {
  'id': 282864642225,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeLoadingMemoryPQs': {
  'id': 282864642226,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.SwapeeLoadingInputsPQs': {
  'id': 282864642227,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})