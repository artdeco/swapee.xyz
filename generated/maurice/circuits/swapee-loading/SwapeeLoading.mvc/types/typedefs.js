/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ISwapeeLoadingComputer={}
xyz.swapee.wc.ISwapeeLoadingOuterCore={}
xyz.swapee.wc.ISwapeeLoadingOuterCore.Model={}
xyz.swapee.wc.ISwapeeLoadingOuterCore.Model.Loading={}
xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel={}
xyz.swapee.wc.ISwapeeLoadingPort={}
xyz.swapee.wc.ISwapeeLoadingPort.Inputs={}
xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs={}
xyz.swapee.wc.ISwapeeLoadingCore={}
xyz.swapee.wc.ISwapeeLoadingCore.Model={}
xyz.swapee.wc.ISwapeeLoadingPortInterface={}
xyz.swapee.wc.ISwapeeLoadingProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ISwapeeLoadingController={}
xyz.swapee.wc.front.ISwapeeLoadingControllerAT={}
xyz.swapee.wc.front.ISwapeeLoadingScreenAR={}
xyz.swapee.wc.ISwapeeLoading={}
xyz.swapee.wc.ISwapeeLoadingHtmlComponent={}
xyz.swapee.wc.ISwapeeLoadingElement={}
xyz.swapee.wc.ISwapeeLoadingElementPort={}
xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs={}
xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs={}
xyz.swapee.wc.ISwapeeLoadingDesigner={}
xyz.swapee.wc.ISwapeeLoadingDesigner.communicator={}
xyz.swapee.wc.ISwapeeLoadingDesigner.relay={}
xyz.swapee.wc.ISwapeeLoadingDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ISwapeeLoadingDisplay={}
xyz.swapee.wc.back.ISwapeeLoadingController={}
xyz.swapee.wc.back.ISwapeeLoadingControllerAR={}
xyz.swapee.wc.back.ISwapeeLoadingScreen={}
xyz.swapee.wc.back.ISwapeeLoadingScreenAT={}
xyz.swapee.wc.ISwapeeLoadingController={}
xyz.swapee.wc.ISwapeeLoadingScreen={}
xyz.swapee.wc.ISwapeeLoadingGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/02-ISwapeeLoadingComputer.xml}  e482fd443e37e11f6b7ff9fb319dfd37 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ISwapeeLoadingComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingComputer)} xyz.swapee.wc.AbstractSwapeeLoadingComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingComputer} xyz.swapee.wc.SwapeeLoadingComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingComputer` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingComputer
 */
xyz.swapee.wc.AbstractSwapeeLoadingComputer = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingComputer.constructor&xyz.swapee.wc.SwapeeLoadingComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingComputer.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingComputer.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoadingComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoadingComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoadingComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoadingComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingComputer.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingComputer} xyz.swapee.wc.SwapeeLoadingComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.SwapeeLoadingMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.ISwapeeLoadingComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ISwapeeLoadingComputer
 */
xyz.swapee.wc.ISwapeeLoadingComputer = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoadingComputer.compute} */
xyz.swapee.wc.ISwapeeLoadingComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingComputer.Initialese>)} xyz.swapee.wc.SwapeeLoadingComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingComputer} xyz.swapee.wc.ISwapeeLoadingComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeLoadingComputer_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingComputer
 * @implements {xyz.swapee.wc.ISwapeeLoadingComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingComputer.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingComputer = class extends /** @type {xyz.swapee.wc.SwapeeLoadingComputer.constructor&xyz.swapee.wc.ISwapeeLoadingComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingComputer}
 */
xyz.swapee.wc.SwapeeLoadingComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoadingComputer} */
xyz.swapee.wc.RecordISwapeeLoadingComputer

/** @typedef {xyz.swapee.wc.ISwapeeLoadingComputer} xyz.swapee.wc.BoundISwapeeLoadingComputer */

/** @typedef {xyz.swapee.wc.SwapeeLoadingComputer} xyz.swapee.wc.BoundSwapeeLoadingComputer */

/**
 * Contains getters to cast the _ISwapeeLoadingComputer_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingComputerCaster
 */
xyz.swapee.wc.ISwapeeLoadingComputerCaster = class { }
/**
 * Cast the _ISwapeeLoadingComputer_ instance into the _BoundISwapeeLoadingComputer_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingComputer}
 */
xyz.swapee.wc.ISwapeeLoadingComputerCaster.prototype.asISwapeeLoadingComputer
/**
 * Access the _SwapeeLoadingComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingComputer}
 */
xyz.swapee.wc.ISwapeeLoadingComputerCaster.prototype.superSwapeeLoadingComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.SwapeeLoadingMemory) => void} xyz.swapee.wc.ISwapeeLoadingComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingComputer.__compute<!xyz.swapee.wc.ISwapeeLoadingComputer>} xyz.swapee.wc.ISwapeeLoadingComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.SwapeeLoadingMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoadingComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoadingComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/03-ISwapeeLoadingOuterCore.xml}  84dd9ed8376b9b23a976b5dffb512762 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeLoadingOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingOuterCore)} xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingOuterCore} xyz.swapee.wc.SwapeeLoadingOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingOuterCore
 */
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.constructor&xyz.swapee.wc.SwapeeLoadingOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingOuterCore|typeof xyz.swapee.wc.SwapeeLoadingOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingOuterCore|typeof xyz.swapee.wc.SwapeeLoadingOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingOuterCore|typeof xyz.swapee.wc.SwapeeLoadingOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingOuterCoreCaster)} xyz.swapee.wc.ISwapeeLoadingOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _ISwapeeLoading_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ISwapeeLoadingOuterCore
 */
xyz.swapee.wc.ISwapeeLoadingOuterCore = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoadingOuterCore.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingOuterCore

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingOuterCore.Initialese>)} xyz.swapee.wc.SwapeeLoadingOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingOuterCore} xyz.swapee.wc.ISwapeeLoadingOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeLoadingOuterCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingOuterCore
 * @implements {xyz.swapee.wc.ISwapeeLoadingOuterCore} The _ISwapeeLoading_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingOuterCore = class extends /** @type {xyz.swapee.wc.SwapeeLoadingOuterCore.constructor&xyz.swapee.wc.ISwapeeLoadingOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeLoadingOuterCore.prototype.constructor = xyz.swapee.wc.SwapeeLoadingOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingOuterCore}
 */
xyz.swapee.wc.SwapeeLoadingOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingOuterCore.
 * @interface xyz.swapee.wc.ISwapeeLoadingOuterCoreFields
 */
xyz.swapee.wc.ISwapeeLoadingOuterCoreFields = class { }
/**
 * The _ISwapeeLoading_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ISwapeeLoadingOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeLoadingOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore} */
xyz.swapee.wc.RecordISwapeeLoadingOuterCore

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore} xyz.swapee.wc.BoundISwapeeLoadingOuterCore */

/** @typedef {xyz.swapee.wc.SwapeeLoadingOuterCore} xyz.swapee.wc.BoundSwapeeLoadingOuterCore */

/**
 * Whether currently loading.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeLoadingOuterCore.Model.Loading.loading

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.Model.Loading} xyz.swapee.wc.ISwapeeLoadingOuterCore.Model The _ISwapeeLoading_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.Loading} xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel The _ISwapeeLoading_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ISwapeeLoadingOuterCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingOuterCoreCaster
 */
xyz.swapee.wc.ISwapeeLoadingOuterCoreCaster = class { }
/**
 * Cast the _ISwapeeLoadingOuterCore_ instance into the _BoundISwapeeLoadingOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingOuterCore}
 */
xyz.swapee.wc.ISwapeeLoadingOuterCoreCaster.prototype.asISwapeeLoadingOuterCore
/**
 * Access the _SwapeeLoadingOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingOuterCore}
 */
xyz.swapee.wc.ISwapeeLoadingOuterCoreCaster.prototype.superSwapeeLoadingOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingOuterCore.Model.Loading Whether currently loading (optional overlay).
 * @prop {boolean} [loading=false] Whether currently loading. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingOuterCore.Model.Loading_Safe Whether currently loading (required overlay).
 * @prop {boolean} loading Whether currently loading.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.Loading Whether currently loading (optional overlay).
 * @prop {*} [loading=null] Whether currently loading. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.Loading_Safe Whether currently loading (required overlay).
 * @prop {*} loading Whether currently loading.
 */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.Loading} xyz.swapee.wc.ISwapeeLoadingPort.Inputs.Loading Whether currently loading (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.Loading_Safe} xyz.swapee.wc.ISwapeeLoadingPort.Inputs.Loading_Safe Whether currently loading (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.Loading} xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs.Loading Whether currently loading (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.Loading_Safe} xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs.Loading_Safe Whether currently loading (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.Model.Loading} xyz.swapee.wc.ISwapeeLoadingCore.Model.Loading Whether currently loading (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.Model.Loading_Safe} xyz.swapee.wc.ISwapeeLoadingCore.Model.Loading_Safe Whether currently loading (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/04-ISwapeeLoadingPort.xml}  fea4c74695d6ad69815f547215d7e98c */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeLoadingPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingPort)} xyz.swapee.wc.AbstractSwapeeLoadingPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingPort} xyz.swapee.wc.SwapeeLoadingPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingPort
 */
xyz.swapee.wc.AbstractSwapeeLoadingPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingPort.constructor&xyz.swapee.wc.SwapeeLoadingPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingPort|typeof xyz.swapee.wc.SwapeeLoadingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingPort|typeof xyz.swapee.wc.SwapeeLoadingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingPort|typeof xyz.swapee.wc.SwapeeLoadingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingPort.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingPort} xyz.swapee.wc.SwapeeLoadingPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeLoadingPort.Inputs>)} xyz.swapee.wc.ISwapeeLoadingPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ISwapeeLoading_, providing input
 * pins.
 * @interface xyz.swapee.wc.ISwapeeLoadingPort
 */
xyz.swapee.wc.ISwapeeLoadingPort = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoadingPort.resetPort} */
xyz.swapee.wc.ISwapeeLoadingPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoadingPort.resetSwapeeLoadingPort} */
xyz.swapee.wc.ISwapeeLoadingPort.prototype.resetSwapeeLoadingPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingPort.Initialese>)} xyz.swapee.wc.SwapeeLoadingPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingPort} xyz.swapee.wc.ISwapeeLoadingPort.typeof */
/**
 * A concrete class of _ISwapeeLoadingPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingPort
 * @implements {xyz.swapee.wc.ISwapeeLoadingPort} The port that serves as an interface to the _ISwapeeLoading_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingPort = class extends /** @type {xyz.swapee.wc.SwapeeLoadingPort.constructor&xyz.swapee.wc.ISwapeeLoadingPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingPort}
 */
xyz.swapee.wc.SwapeeLoadingPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingPort.
 * @interface xyz.swapee.wc.ISwapeeLoadingPortFields
 */
xyz.swapee.wc.ISwapeeLoadingPortFields = class { }
/**
 * The inputs to the _ISwapeeLoading_'s controller via its port.
 */
xyz.swapee.wc.ISwapeeLoadingPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoadingPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeLoadingPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeLoadingPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingPort} */
xyz.swapee.wc.RecordISwapeeLoadingPort

/** @typedef {xyz.swapee.wc.ISwapeeLoadingPort} xyz.swapee.wc.BoundISwapeeLoadingPort */

/** @typedef {xyz.swapee.wc.SwapeeLoadingPort} xyz.swapee.wc.BoundSwapeeLoadingPort */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeLoadingPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel} xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ISwapeeLoading_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeLoadingPort.Inputs
 */
xyz.swapee.wc.ISwapeeLoadingPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingPort.Inputs.constructor&xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoadingPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs.constructor */
/**
 * The inputs to the _ISwapeeLoading_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingPortInterface
 */
xyz.swapee.wc.ISwapeeLoadingPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ISwapeeLoadingPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeLoadingPortInterface.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingPortInterface

/**
 * A concrete class of _ISwapeeLoadingPortInterface_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingPortInterface
 * @implements {xyz.swapee.wc.ISwapeeLoadingPortInterface} The port interface.
 */
xyz.swapee.wc.SwapeeLoadingPortInterface = class extends xyz.swapee.wc.ISwapeeLoadingPortInterface { }
xyz.swapee.wc.SwapeeLoadingPortInterface.prototype.constructor = xyz.swapee.wc.SwapeeLoadingPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingPortInterface.Props
 * @prop {boolean} loading Whether currently loading.
 */

/**
 * Contains getters to cast the _ISwapeeLoadingPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingPortCaster
 */
xyz.swapee.wc.ISwapeeLoadingPortCaster = class { }
/**
 * Cast the _ISwapeeLoadingPort_ instance into the _BoundISwapeeLoadingPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingPort}
 */
xyz.swapee.wc.ISwapeeLoadingPortCaster.prototype.asISwapeeLoadingPort
/**
 * Access the _SwapeeLoadingPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingPort}
 */
xyz.swapee.wc.ISwapeeLoadingPortCaster.prototype.superSwapeeLoadingPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoadingPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingPort.__resetPort<!xyz.swapee.wc.ISwapeeLoadingPort>} xyz.swapee.wc.ISwapeeLoadingPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingPort.resetPort} */
/**
 * Resets the _ISwapeeLoading_ port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoadingPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoadingPort.__resetSwapeeLoadingPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingPort.__resetSwapeeLoadingPort<!xyz.swapee.wc.ISwapeeLoadingPort>} xyz.swapee.wc.ISwapeeLoadingPort._resetSwapeeLoadingPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingPort.resetSwapeeLoadingPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoadingPort.resetSwapeeLoadingPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoadingPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/09-ISwapeeLoadingCore.xml}  a931536af2187ed915214adac3dc5794 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeLoadingCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingCore)} xyz.swapee.wc.AbstractSwapeeLoadingCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingCore} xyz.swapee.wc.SwapeeLoadingCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingCore
 */
xyz.swapee.wc.AbstractSwapeeLoadingCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingCore.constructor&xyz.swapee.wc.SwapeeLoadingCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingCore|typeof xyz.swapee.wc.SwapeeLoadingCore)|(!xyz.swapee.wc.ISwapeeLoadingOuterCore|typeof xyz.swapee.wc.SwapeeLoadingOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingCore|typeof xyz.swapee.wc.SwapeeLoadingCore)|(!xyz.swapee.wc.ISwapeeLoadingOuterCore|typeof xyz.swapee.wc.SwapeeLoadingOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingCore|typeof xyz.swapee.wc.SwapeeLoadingCore)|(!xyz.swapee.wc.ISwapeeLoadingOuterCore|typeof xyz.swapee.wc.SwapeeLoadingOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingCore}
 */
xyz.swapee.wc.AbstractSwapeeLoadingCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingCoreCaster&xyz.swapee.wc.ISwapeeLoadingOuterCore)} xyz.swapee.wc.ISwapeeLoadingCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ISwapeeLoadingCore
 */
xyz.swapee.wc.ISwapeeLoadingCore = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoadingOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeLoadingCore.resetCore} */
xyz.swapee.wc.ISwapeeLoadingCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoadingCore.resetSwapeeLoadingCore} */
xyz.swapee.wc.ISwapeeLoadingCore.prototype.resetSwapeeLoadingCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingCore.Initialese>)} xyz.swapee.wc.SwapeeLoadingCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingCore} xyz.swapee.wc.ISwapeeLoadingCore.typeof */
/**
 * A concrete class of _ISwapeeLoadingCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingCore
 * @implements {xyz.swapee.wc.ISwapeeLoadingCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingCore = class extends /** @type {xyz.swapee.wc.SwapeeLoadingCore.constructor&xyz.swapee.wc.ISwapeeLoadingCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeLoadingCore.prototype.constructor = xyz.swapee.wc.SwapeeLoadingCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingCore}
 */
xyz.swapee.wc.SwapeeLoadingCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingCore.
 * @interface xyz.swapee.wc.ISwapeeLoadingCoreFields
 */
xyz.swapee.wc.ISwapeeLoadingCoreFields = class { }
/**
 * The _ISwapeeLoading_'s memory.
 */
xyz.swapee.wc.ISwapeeLoadingCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeLoadingCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ISwapeeLoadingCoreFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeLoadingCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingCore} */
xyz.swapee.wc.RecordISwapeeLoadingCore

/** @typedef {xyz.swapee.wc.ISwapeeLoadingCore} xyz.swapee.wc.BoundISwapeeLoadingCore */

/** @typedef {xyz.swapee.wc.SwapeeLoadingCore} xyz.swapee.wc.BoundSwapeeLoadingCore */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingOuterCore.Model} xyz.swapee.wc.ISwapeeLoadingCore.Model The _ISwapeeLoading_'s memory. */

/**
 * Contains getters to cast the _ISwapeeLoadingCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingCoreCaster
 */
xyz.swapee.wc.ISwapeeLoadingCoreCaster = class { }
/**
 * Cast the _ISwapeeLoadingCore_ instance into the _BoundISwapeeLoadingCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingCore}
 */
xyz.swapee.wc.ISwapeeLoadingCoreCaster.prototype.asISwapeeLoadingCore
/**
 * Access the _SwapeeLoadingCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingCore}
 */
xyz.swapee.wc.ISwapeeLoadingCoreCaster.prototype.superSwapeeLoadingCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoadingCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingCore.__resetCore<!xyz.swapee.wc.ISwapeeLoadingCore>} xyz.swapee.wc.ISwapeeLoadingCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingCore.resetCore} */
/**
 * Resets the _ISwapeeLoading_ core.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoadingCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoadingCore.__resetSwapeeLoadingCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingCore.__resetSwapeeLoadingCore<!xyz.swapee.wc.ISwapeeLoadingCore>} xyz.swapee.wc.ISwapeeLoadingCore._resetSwapeeLoadingCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingCore.resetSwapeeLoadingCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoadingCore.resetSwapeeLoadingCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoadingCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/10-ISwapeeLoadingProcessor.xml}  7414b38e880e42cad67c89758334f8f8 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingComputer.Initialese&xyz.swapee.wc.ISwapeeLoadingController.Initialese} xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingProcessor)} xyz.swapee.wc.AbstractSwapeeLoadingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingProcessor} xyz.swapee.wc.SwapeeLoadingProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingProcessor
 */
xyz.swapee.wc.AbstractSwapeeLoadingProcessor = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingProcessor.constructor&xyz.swapee.wc.SwapeeLoadingProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingProcessor.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!xyz.swapee.wc.ISwapeeLoadingCore|typeof xyz.swapee.wc.SwapeeLoadingCore)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoadingProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoadingProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!xyz.swapee.wc.ISwapeeLoadingCore|typeof xyz.swapee.wc.SwapeeLoadingCore)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoadingProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!xyz.swapee.wc.ISwapeeLoadingCore|typeof xyz.swapee.wc.SwapeeLoadingCore)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoadingProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingProcessor} xyz.swapee.wc.SwapeeLoadingProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingProcessorCaster&xyz.swapee.wc.ISwapeeLoadingComputer&xyz.swapee.wc.ISwapeeLoadingCore&xyz.swapee.wc.ISwapeeLoadingController)} xyz.swapee.wc.ISwapeeLoadingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingController} xyz.swapee.wc.ISwapeeLoadingController.typeof */
/**
 * The processor to compute changes to the memory for the _ISwapeeLoading_.
 * @interface xyz.swapee.wc.ISwapeeLoadingProcessor
 */
xyz.swapee.wc.ISwapeeLoadingProcessor = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoadingComputer.typeof&xyz.swapee.wc.ISwapeeLoadingCore.typeof&xyz.swapee.wc.ISwapeeLoadingController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese>)} xyz.swapee.wc.SwapeeLoadingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingProcessor} xyz.swapee.wc.ISwapeeLoadingProcessor.typeof */
/**
 * A concrete class of _ISwapeeLoadingProcessor_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingProcessor
 * @implements {xyz.swapee.wc.ISwapeeLoadingProcessor} The processor to compute changes to the memory for the _ISwapeeLoading_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingProcessor = class extends /** @type {xyz.swapee.wc.SwapeeLoadingProcessor.constructor&xyz.swapee.wc.ISwapeeLoadingProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingProcessor}
 */
xyz.swapee.wc.SwapeeLoadingProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoadingProcessor} */
xyz.swapee.wc.RecordISwapeeLoadingProcessor

/** @typedef {xyz.swapee.wc.ISwapeeLoadingProcessor} xyz.swapee.wc.BoundISwapeeLoadingProcessor */

/** @typedef {xyz.swapee.wc.SwapeeLoadingProcessor} xyz.swapee.wc.BoundSwapeeLoadingProcessor */

/**
 * Contains getters to cast the _ISwapeeLoadingProcessor_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingProcessorCaster
 */
xyz.swapee.wc.ISwapeeLoadingProcessorCaster = class { }
/**
 * Cast the _ISwapeeLoadingProcessor_ instance into the _BoundISwapeeLoadingProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingProcessor}
 */
xyz.swapee.wc.ISwapeeLoadingProcessorCaster.prototype.asISwapeeLoadingProcessor
/**
 * Access the _SwapeeLoadingProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingProcessor}
 */
xyz.swapee.wc.ISwapeeLoadingProcessorCaster.prototype.superSwapeeLoadingProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/100-SwapeeLoadingMemory.xml}  0e6766a0bcf67374d5b9ce2828c53cf9 */
/**
 * The memory of the _ISwapeeLoading_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.SwapeeLoadingMemory
 */
xyz.swapee.wc.SwapeeLoadingMemory = class { }
/**
 * Whether currently loading. Default `false`.
 */
xyz.swapee.wc.SwapeeLoadingMemory.prototype.loading = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/102-SwapeeLoadingInputs.xml}  35b34672219775ffa3cad4d9d409862e */
/**
 * The inputs of the _ISwapeeLoading_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.SwapeeLoadingInputs
 */
xyz.swapee.wc.front.SwapeeLoadingInputs = class { }
/**
 * Whether currently loading. Default `false`.
 */
xyz.swapee.wc.front.SwapeeLoadingInputs.prototype.loading = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/11-ISwapeeLoading.xml}  8c40af274e0789d2a1da33ffc824b8e1 */
/**
 * An atomic wrapper for the _ISwapeeLoading_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.SwapeeLoadingEnv
 */
xyz.swapee.wc.SwapeeLoadingEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.SwapeeLoadingEnv.prototype.swapeeLoading = /** @type {xyz.swapee.wc.ISwapeeLoading} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.ISwapeeLoadingController.Inputs>&xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese&xyz.swapee.wc.ISwapeeLoadingComputer.Initialese&xyz.swapee.wc.ISwapeeLoadingController.Initialese} xyz.swapee.wc.ISwapeeLoading.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoading)} xyz.swapee.wc.AbstractSwapeeLoading.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoading} xyz.swapee.wc.SwapeeLoading.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoading` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoading
 */
xyz.swapee.wc.AbstractSwapeeLoading = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoading.constructor&xyz.swapee.wc.SwapeeLoading.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoading.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoading
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoading.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoading} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoading|typeof xyz.swapee.wc.SwapeeLoading)|(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoading}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoading.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoading}
 */
xyz.swapee.wc.AbstractSwapeeLoading.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoading}
 */
xyz.swapee.wc.AbstractSwapeeLoading.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoading|typeof xyz.swapee.wc.SwapeeLoading)|(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoading}
 */
xyz.swapee.wc.AbstractSwapeeLoading.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoading|typeof xyz.swapee.wc.SwapeeLoading)|(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoading}
 */
xyz.swapee.wc.AbstractSwapeeLoading.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoading.Initialese[]) => xyz.swapee.wc.ISwapeeLoading} xyz.swapee.wc.SwapeeLoadingConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoading.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ISwapeeLoading.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ISwapeeLoading.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ISwapeeLoading.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.SwapeeLoadingMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.SwapeeLoadingClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingCaster&xyz.swapee.wc.ISwapeeLoadingProcessor&xyz.swapee.wc.ISwapeeLoadingComputer&xyz.swapee.wc.ISwapeeLoadingController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.ISwapeeLoadingController.Inputs, null>)} xyz.swapee.wc.ISwapeeLoading.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ISwapeeLoading
 */
xyz.swapee.wc.ISwapeeLoading = class extends /** @type {xyz.swapee.wc.ISwapeeLoading.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoadingProcessor.typeof&xyz.swapee.wc.ISwapeeLoadingComputer.typeof&xyz.swapee.wc.ISwapeeLoadingController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoading* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoading.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoading.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoading&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoading.Initialese>)} xyz.swapee.wc.SwapeeLoading.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoading} xyz.swapee.wc.ISwapeeLoading.typeof */
/**
 * A concrete class of _ISwapeeLoading_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoading
 * @implements {xyz.swapee.wc.ISwapeeLoading} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoading.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoading = class extends /** @type {xyz.swapee.wc.SwapeeLoading.constructor&xyz.swapee.wc.ISwapeeLoading.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoading* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoading.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoading* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoading.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoading.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoading}
 */
xyz.swapee.wc.SwapeeLoading.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoading.
 * @interface xyz.swapee.wc.ISwapeeLoadingFields
 */
xyz.swapee.wc.ISwapeeLoadingFields = class { }
/**
 * The input pins of the _ISwapeeLoading_ port.
 */
xyz.swapee.wc.ISwapeeLoadingFields.prototype.pinout = /** @type {!xyz.swapee.wc.ISwapeeLoading.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoading} */
xyz.swapee.wc.RecordISwapeeLoading

/** @typedef {xyz.swapee.wc.ISwapeeLoading} xyz.swapee.wc.BoundISwapeeLoading */

/** @typedef {xyz.swapee.wc.SwapeeLoading} xyz.swapee.wc.BoundSwapeeLoading */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingController.Inputs} xyz.swapee.wc.ISwapeeLoading.Pinout The input pins of the _ISwapeeLoading_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeLoadingController.Inputs>)} xyz.swapee.wc.ISwapeeLoadingBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ISwapeeLoadingBuffer
 */
xyz.swapee.wc.ISwapeeLoadingBuffer = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoadingBuffer.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingBuffer

/**
 * A concrete class of _ISwapeeLoadingBuffer_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingBuffer
 * @implements {xyz.swapee.wc.ISwapeeLoadingBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.SwapeeLoadingBuffer = class extends xyz.swapee.wc.ISwapeeLoadingBuffer { }
xyz.swapee.wc.SwapeeLoadingBuffer.prototype.constructor = xyz.swapee.wc.SwapeeLoadingBuffer

/**
 * Contains getters to cast the _ISwapeeLoading_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingCaster
 */
xyz.swapee.wc.ISwapeeLoadingCaster = class { }
/**
 * Cast the _ISwapeeLoading_ instance into the _BoundISwapeeLoading_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoading}
 */
xyz.swapee.wc.ISwapeeLoadingCaster.prototype.asISwapeeLoading
/**
 * Access the _SwapeeLoading_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoading}
 */
xyz.swapee.wc.ISwapeeLoadingCaster.prototype.superSwapeeLoading

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/110-SwapeeLoadingSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeLoadingVdusPQs
 */
xyz.swapee.wc.SwapeeLoadingVdusPQs = class { }
xyz.swapee.wc.SwapeeLoadingVdusPQs.prototype.constructor = xyz.swapee.wc.SwapeeLoadingVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeLoadingVdusQPs
 * @dict
 */
xyz.swapee.wc.SwapeeLoadingVdusQPs = class { }
xyz.swapee.wc.SwapeeLoadingVdusQPs.prototype.constructor = xyz.swapee.wc.SwapeeLoadingVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/12-ISwapeeLoadingHtmlComponent.xml}  f26531c069a3b7ec102d8970518ce3af */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingController.Initialese&xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese&xyz.swapee.wc.ISwapeeLoading.Initialese&xyz.swapee.wc.ISwapeeLoadingGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ISwapeeLoadingProcessor.Initialese&xyz.swapee.wc.ISwapeeLoadingComputer.Initialese} xyz.swapee.wc.ISwapeeLoadingHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingHtmlComponent)} xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent} xyz.swapee.wc.SwapeeLoadingHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent
 */
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.constructor&xyz.swapee.wc.SwapeeLoadingHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingHtmlComponent|typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeLoadingController|typeof xyz.swapee.wc.back.SwapeeLoadingController)|(!xyz.swapee.wc.back.ISwapeeLoadingScreen|typeof xyz.swapee.wc.back.SwapeeLoadingScreen)|(!xyz.swapee.wc.ISwapeeLoading|typeof xyz.swapee.wc.SwapeeLoading)|(!xyz.swapee.wc.ISwapeeLoadingGPU|typeof xyz.swapee.wc.SwapeeLoadingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingHtmlComponent|typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeLoadingController|typeof xyz.swapee.wc.back.SwapeeLoadingController)|(!xyz.swapee.wc.back.ISwapeeLoadingScreen|typeof xyz.swapee.wc.back.SwapeeLoadingScreen)|(!xyz.swapee.wc.ISwapeeLoading|typeof xyz.swapee.wc.SwapeeLoading)|(!xyz.swapee.wc.ISwapeeLoadingGPU|typeof xyz.swapee.wc.SwapeeLoadingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingHtmlComponent|typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeLoadingController|typeof xyz.swapee.wc.back.SwapeeLoadingController)|(!xyz.swapee.wc.back.ISwapeeLoadingScreen|typeof xyz.swapee.wc.back.SwapeeLoadingScreen)|(!xyz.swapee.wc.ISwapeeLoading|typeof xyz.swapee.wc.SwapeeLoading)|(!xyz.swapee.wc.ISwapeeLoadingGPU|typeof xyz.swapee.wc.SwapeeLoadingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeLoadingProcessor|typeof xyz.swapee.wc.SwapeeLoadingProcessor)|(!xyz.swapee.wc.ISwapeeLoadingComputer|typeof xyz.swapee.wc.SwapeeLoadingComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoadingHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingHtmlComponent.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingHtmlComponent} xyz.swapee.wc.SwapeeLoadingHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingHtmlComponentCaster&xyz.swapee.wc.back.ISwapeeLoadingController&xyz.swapee.wc.back.ISwapeeLoadingScreen&xyz.swapee.wc.ISwapeeLoading&xyz.swapee.wc.ISwapeeLoadingGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.ISwapeeLoadingController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.ISwapeeLoadingProcessor&xyz.swapee.wc.ISwapeeLoadingComputer)} xyz.swapee.wc.ISwapeeLoadingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoadingController} xyz.swapee.wc.back.ISwapeeLoadingController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoadingScreen} xyz.swapee.wc.back.ISwapeeLoadingScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingGPU} xyz.swapee.wc.ISwapeeLoadingGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _ISwapeeLoading_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ISwapeeLoadingHtmlComponent
 */
xyz.swapee.wc.ISwapeeLoadingHtmlComponent = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeLoadingController.typeof&xyz.swapee.wc.back.ISwapeeLoadingScreen.typeof&xyz.swapee.wc.ISwapeeLoading.typeof&xyz.swapee.wc.ISwapeeLoadingGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ISwapeeLoadingProcessor.typeof&xyz.swapee.wc.ISwapeeLoadingComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingHtmlComponent.Initialese>)} xyz.swapee.wc.SwapeeLoadingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingHtmlComponent} xyz.swapee.wc.ISwapeeLoadingHtmlComponent.typeof */
/**
 * A concrete class of _ISwapeeLoadingHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingHtmlComponent
 * @implements {xyz.swapee.wc.ISwapeeLoadingHtmlComponent} The _ISwapeeLoading_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingHtmlComponent = class extends /** @type {xyz.swapee.wc.SwapeeLoadingHtmlComponent.constructor&xyz.swapee.wc.ISwapeeLoadingHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingHtmlComponent}
 */
xyz.swapee.wc.SwapeeLoadingHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoadingHtmlComponent} */
xyz.swapee.wc.RecordISwapeeLoadingHtmlComponent

/** @typedef {xyz.swapee.wc.ISwapeeLoadingHtmlComponent} xyz.swapee.wc.BoundISwapeeLoadingHtmlComponent */

/** @typedef {xyz.swapee.wc.SwapeeLoadingHtmlComponent} xyz.swapee.wc.BoundSwapeeLoadingHtmlComponent */

/**
 * Contains getters to cast the _ISwapeeLoadingHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingHtmlComponentCaster
 */
xyz.swapee.wc.ISwapeeLoadingHtmlComponentCaster = class { }
/**
 * Cast the _ISwapeeLoadingHtmlComponent_ instance into the _BoundISwapeeLoadingHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingHtmlComponent}
 */
xyz.swapee.wc.ISwapeeLoadingHtmlComponentCaster.prototype.asISwapeeLoadingHtmlComponent
/**
 * Access the _SwapeeLoadingHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingHtmlComponent}
 */
xyz.swapee.wc.ISwapeeLoadingHtmlComponentCaster.prototype.superSwapeeLoadingHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/130-ISwapeeLoadingElement.xml}  78b8b953677f5573d5020c3052a73498 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.ISwapeeLoadingElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ISwapeeLoadingElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingElement)} xyz.swapee.wc.AbstractSwapeeLoadingElement.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingElement} xyz.swapee.wc.SwapeeLoadingElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingElement` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingElement
 */
xyz.swapee.wc.AbstractSwapeeLoadingElement = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingElement.constructor&xyz.swapee.wc.SwapeeLoadingElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingElement.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingElement.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingElement|typeof xyz.swapee.wc.SwapeeLoadingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingElement}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElement}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingElement|typeof xyz.swapee.wc.SwapeeLoadingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElement}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingElement|typeof xyz.swapee.wc.SwapeeLoadingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElement}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingElement.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingElement} xyz.swapee.wc.SwapeeLoadingElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingElementFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.ISwapeeLoadingElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.ISwapeeLoadingElement.Inputs, null>)} xyz.swapee.wc.ISwapeeLoadingElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _ISwapeeLoading_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ISwapeeLoadingElement
 */
xyz.swapee.wc.ISwapeeLoadingElement = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoadingElement.solder} */
xyz.swapee.wc.ISwapeeLoadingElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoadingElement.render} */
xyz.swapee.wc.ISwapeeLoadingElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoadingElement.server} */
xyz.swapee.wc.ISwapeeLoadingElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoadingElement.inducer} */
xyz.swapee.wc.ISwapeeLoadingElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingElement&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingElement.Initialese>)} xyz.swapee.wc.SwapeeLoadingElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElement} xyz.swapee.wc.ISwapeeLoadingElement.typeof */
/**
 * A concrete class of _ISwapeeLoadingElement_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingElement
 * @implements {xyz.swapee.wc.ISwapeeLoadingElement} A component description.
 *
 * The _ISwapeeLoading_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingElement.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingElement = class extends /** @type {xyz.swapee.wc.SwapeeLoadingElement.constructor&xyz.swapee.wc.ISwapeeLoadingElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElement}
 */
xyz.swapee.wc.SwapeeLoadingElement.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingElement.
 * @interface xyz.swapee.wc.ISwapeeLoadingElementFields
 */
xyz.swapee.wc.ISwapeeLoadingElementFields = class { }
/**
 * The element-specific inputs to the _ISwapeeLoading_ component.
 */
xyz.swapee.wc.ISwapeeLoadingElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoadingElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingElement} */
xyz.swapee.wc.RecordISwapeeLoadingElement

/** @typedef {xyz.swapee.wc.ISwapeeLoadingElement} xyz.swapee.wc.BoundISwapeeLoadingElement */

/** @typedef {xyz.swapee.wc.SwapeeLoadingElement} xyz.swapee.wc.BoundSwapeeLoadingElement */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingPort.Inputs&xyz.swapee.wc.ISwapeeLoadingDisplay.Queries&xyz.swapee.wc.ISwapeeLoadingController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs} xyz.swapee.wc.ISwapeeLoadingElement.Inputs The element-specific inputs to the _ISwapeeLoading_ component. */

/**
 * Contains getters to cast the _ISwapeeLoadingElement_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingElementCaster
 */
xyz.swapee.wc.ISwapeeLoadingElementCaster = class { }
/**
 * Cast the _ISwapeeLoadingElement_ instance into the _BoundISwapeeLoadingElement_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingElement}
 */
xyz.swapee.wc.ISwapeeLoadingElementCaster.prototype.asISwapeeLoadingElement
/**
 * Access the _SwapeeLoadingElement_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingElement}
 */
xyz.swapee.wc.ISwapeeLoadingElementCaster.prototype.superSwapeeLoadingElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeLoadingMemory, props: !xyz.swapee.wc.ISwapeeLoadingElement.Inputs) => Object<string, *>} xyz.swapee.wc.ISwapeeLoadingElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingElement.__solder<!xyz.swapee.wc.ISwapeeLoadingElement>} xyz.swapee.wc.ISwapeeLoadingElement._solder */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.SwapeeLoadingMemory} model The model.
 * @param {!xyz.swapee.wc.ISwapeeLoadingElement.Inputs} props The element props.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeLoadingElementPort.Inputs.NoSolder* Default `false`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ISwapeeLoadingElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeLoadingMemory, instance?: !xyz.swapee.wc.ISwapeeLoadingScreen&xyz.swapee.wc.ISwapeeLoadingController) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeLoadingElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingElement.__render<!xyz.swapee.wc.ISwapeeLoadingElement>} xyz.swapee.wc.ISwapeeLoadingElement._render */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.SwapeeLoadingMemory} [model] The model for the view.
 * @param {!xyz.swapee.wc.ISwapeeLoadingScreen&xyz.swapee.wc.ISwapeeLoadingController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeLoadingElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeLoadingMemory, inputs: !xyz.swapee.wc.ISwapeeLoadingElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeLoadingElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingElement.__server<!xyz.swapee.wc.ISwapeeLoadingElement>} xyz.swapee.wc.ISwapeeLoadingElement._server */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.SwapeeLoadingMemory} memory The memory registers.
 * @param {!xyz.swapee.wc.ISwapeeLoadingElement.Inputs} inputs The inputs to the port.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeLoadingElementPort.Inputs.NoSolder* Default `false`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeLoadingElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeLoadingMemory, port?: !xyz.swapee.wc.ISwapeeLoadingElement.Inputs) => ?} xyz.swapee.wc.ISwapeeLoadingElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingElement.__inducer<!xyz.swapee.wc.ISwapeeLoadingElement>} xyz.swapee.wc.ISwapeeLoadingElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.SwapeeLoadingMemory} [model] The model of the component into which to induce the state.
 * @param {!xyz.swapee.wc.ISwapeeLoadingElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeLoadingElementPort.Inputs.NoSolder* Default `false`.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeLoadingElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoadingElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/140-ISwapeeLoadingElementPort.xml}  b3728ef4ee99f98e1cdbb93b3202a6a1 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeLoadingElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingElementPort)} xyz.swapee.wc.AbstractSwapeeLoadingElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingElementPort} xyz.swapee.wc.SwapeeLoadingElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingElementPort
 */
xyz.swapee.wc.AbstractSwapeeLoadingElementPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingElementPort.constructor&xyz.swapee.wc.SwapeeLoadingElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingElementPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingElementPort|typeof xyz.swapee.wc.SwapeeLoadingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingElementPort|typeof xyz.swapee.wc.SwapeeLoadingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingElementPort|typeof xyz.swapee.wc.SwapeeLoadingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoadingElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingElementPort.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingElementPort} xyz.swapee.wc.SwapeeLoadingElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs>)} xyz.swapee.wc.ISwapeeLoadingElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ISwapeeLoadingElementPort
 */
xyz.swapee.wc.ISwapeeLoadingElementPort = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingElementPort.Initialese>)} xyz.swapee.wc.SwapeeLoadingElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElementPort} xyz.swapee.wc.ISwapeeLoadingElementPort.typeof */
/**
 * A concrete class of _ISwapeeLoadingElementPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingElementPort
 * @implements {xyz.swapee.wc.ISwapeeLoadingElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingElementPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingElementPort = class extends /** @type {xyz.swapee.wc.SwapeeLoadingElementPort.constructor&xyz.swapee.wc.ISwapeeLoadingElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingElementPort}
 */
xyz.swapee.wc.SwapeeLoadingElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingElementPort.
 * @interface xyz.swapee.wc.ISwapeeLoadingElementPortFields
 */
xyz.swapee.wc.ISwapeeLoadingElementPortFields = class { }
/**
 * The inputs to the _ISwapeeLoadingElement_'s controller via its element port.
 */
xyz.swapee.wc.ISwapeeLoadingElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeLoadingElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingElementPort} */
xyz.swapee.wc.RecordISwapeeLoadingElementPort

/** @typedef {xyz.swapee.wc.ISwapeeLoadingElementPort} xyz.swapee.wc.BoundISwapeeLoadingElementPort */

/** @typedef {xyz.swapee.wc.SwapeeLoadingElementPort} xyz.swapee.wc.BoundSwapeeLoadingElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder)} xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder} xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _ISwapeeLoadingElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs
 */
xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.constructor&xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _ISwapeeLoadingElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs

/**
 * Contains getters to cast the _ISwapeeLoadingElementPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingElementPortCaster
 */
xyz.swapee.wc.ISwapeeLoadingElementPortCaster = class { }
/**
 * Cast the _ISwapeeLoadingElementPort_ instance into the _BoundISwapeeLoadingElementPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingElementPort}
 */
xyz.swapee.wc.ISwapeeLoadingElementPortCaster.prototype.asISwapeeLoadingElementPort
/**
 * Access the _SwapeeLoadingElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingElementPort}
 */
xyz.swapee.wc.ISwapeeLoadingElementPortCaster.prototype.superSwapeeLoadingElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/170-ISwapeeLoadingDesigner.xml}  fdb7d18e12f20271e4175601d3186eb9 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ISwapeeLoadingDesigner
 */
xyz.swapee.wc.ISwapeeLoadingDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.SwapeeLoadingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeLoading />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeLoadingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeLoading />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeLoadingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeLoadingDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeLoading` _typeof ISwapeeLoadingController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeLoadingDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeLoading` _typeof ISwapeeLoadingController_
   * - `This` _typeof ISwapeeLoadingController_
   * @param {!xyz.swapee.wc.ISwapeeLoadingDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `SwapeeLoading` _!SwapeeLoadingMemory_
   * - `This` _!SwapeeLoadingMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeLoadingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeLoadingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ISwapeeLoadingDesigner.prototype.constructor = xyz.swapee.wc.ISwapeeLoadingDesigner

/**
 * A concrete class of _ISwapeeLoadingDesigner_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingDesigner
 * @implements {xyz.swapee.wc.ISwapeeLoadingDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.SwapeeLoadingDesigner = class extends xyz.swapee.wc.ISwapeeLoadingDesigner { }
xyz.swapee.wc.SwapeeLoadingDesigner.prototype.constructor = xyz.swapee.wc.SwapeeLoadingDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeLoadingController} SwapeeLoading
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeLoadingController} SwapeeLoading
 * @prop {typeof xyz.swapee.wc.ISwapeeLoadingController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoadingDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.SwapeeLoadingMemory} SwapeeLoading
 * @prop {!xyz.swapee.wc.SwapeeLoadingMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/40-ISwapeeLoadingDisplay.xml}  31b18a58d92f5833f53963b3e55e1e16 */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ISwapeeLoadingDisplay.Settings>} xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingDisplay)} xyz.swapee.wc.AbstractSwapeeLoadingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingDisplay} xyz.swapee.wc.SwapeeLoadingDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingDisplay
 */
xyz.swapee.wc.AbstractSwapeeLoadingDisplay = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingDisplay.constructor&xyz.swapee.wc.SwapeeLoadingDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingDisplay.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.SwapeeLoadingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoadingDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoadingDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.SwapeeLoadingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoadingDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.SwapeeLoadingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoadingDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingDisplay} xyz.swapee.wc.SwapeeLoadingDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.SwapeeLoadingMemory, !HTMLDivElement, !xyz.swapee.wc.ISwapeeLoadingDisplay.Settings, xyz.swapee.wc.ISwapeeLoadingDisplay.Queries, null>)} xyz.swapee.wc.ISwapeeLoadingDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ISwapeeLoading_.
 * @interface xyz.swapee.wc.ISwapeeLoadingDisplay
 */
xyz.swapee.wc.ISwapeeLoadingDisplay = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoadingDisplay.paint} */
xyz.swapee.wc.ISwapeeLoadingDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese>)} xyz.swapee.wc.SwapeeLoadingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingDisplay} xyz.swapee.wc.ISwapeeLoadingDisplay.typeof */
/**
 * A concrete class of _ISwapeeLoadingDisplay_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingDisplay
 * @implements {xyz.swapee.wc.ISwapeeLoadingDisplay} Display for presenting information from the _ISwapeeLoading_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingDisplay = class extends /** @type {xyz.swapee.wc.SwapeeLoadingDisplay.constructor&xyz.swapee.wc.ISwapeeLoadingDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.SwapeeLoadingDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingDisplay.
 * @interface xyz.swapee.wc.ISwapeeLoadingDisplayFields
 */
xyz.swapee.wc.ISwapeeLoadingDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ISwapeeLoadingDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ISwapeeLoadingDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ISwapeeLoadingDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ISwapeeLoadingDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingDisplay} */
xyz.swapee.wc.RecordISwapeeLoadingDisplay

/** @typedef {xyz.swapee.wc.ISwapeeLoadingDisplay} xyz.swapee.wc.BoundISwapeeLoadingDisplay */

/** @typedef {xyz.swapee.wc.SwapeeLoadingDisplay} xyz.swapee.wc.BoundSwapeeLoadingDisplay */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingDisplay.Queries} xyz.swapee.wc.ISwapeeLoadingDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeLoadingDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _ISwapeeLoadingDisplay_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingDisplayCaster
 */
xyz.swapee.wc.ISwapeeLoadingDisplayCaster = class { }
/**
 * Cast the _ISwapeeLoadingDisplay_ instance into the _BoundISwapeeLoadingDisplay_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingDisplay}
 */
xyz.swapee.wc.ISwapeeLoadingDisplayCaster.prototype.asISwapeeLoadingDisplay
/**
 * Cast the _ISwapeeLoadingDisplay_ instance into the _BoundISwapeeLoadingScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingScreen}
 */
xyz.swapee.wc.ISwapeeLoadingDisplayCaster.prototype.asISwapeeLoadingScreen
/**
 * Access the _SwapeeLoadingDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingDisplay}
 */
xyz.swapee.wc.ISwapeeLoadingDisplayCaster.prototype.superSwapeeLoadingDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeLoadingMemory, land: null) => void} xyz.swapee.wc.ISwapeeLoadingDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingDisplay.__paint<!xyz.swapee.wc.ISwapeeLoadingDisplay>} xyz.swapee.wc.ISwapeeLoadingDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeLoadingMemory} memory The display data.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoadingDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoadingDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/40-ISwapeeLoadingDisplayBack.xml}  dc497c0fd8c0820f4eec88ecd3974252 */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.SwapeeLoadingClasses>} xyz.swapee.wc.back.ISwapeeLoadingDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoadingDisplay)} xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoadingDisplay} xyz.swapee.wc.back.SwapeeLoadingDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoadingDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.constructor&xyz.swapee.wc.back.SwapeeLoadingDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.back.SwapeeLoadingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.back.SwapeeLoadingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.back.SwapeeLoadingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoadingDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.SwapeeLoadingClasses, null>)} xyz.swapee.wc.back.ISwapeeLoadingDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingDisplay
 */
xyz.swapee.wc.back.ISwapeeLoadingDisplay = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoadingDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ISwapeeLoadingDisplay.paint} */
xyz.swapee.wc.back.ISwapeeLoadingDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoadingDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingDisplay.Initialese>)} xyz.swapee.wc.back.SwapeeLoadingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoadingDisplay} xyz.swapee.wc.back.ISwapeeLoadingDisplay.typeof */
/**
 * A concrete class of _ISwapeeLoadingDisplay_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoadingDisplay
 * @implements {xyz.swapee.wc.back.ISwapeeLoadingDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoadingDisplay = class extends /** @type {xyz.swapee.wc.back.SwapeeLoadingDisplay.constructor&xyz.swapee.wc.back.ISwapeeLoadingDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.SwapeeLoadingDisplay.prototype.constructor = xyz.swapee.wc.back.SwapeeLoadingDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingDisplay}
 */
xyz.swapee.wc.back.SwapeeLoadingDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingDisplay} */
xyz.swapee.wc.back.RecordISwapeeLoadingDisplay

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingDisplay} xyz.swapee.wc.back.BoundISwapeeLoadingDisplay */

/** @typedef {xyz.swapee.wc.back.SwapeeLoadingDisplay} xyz.swapee.wc.back.BoundSwapeeLoadingDisplay */

/**
 * Contains getters to cast the _ISwapeeLoadingDisplay_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingDisplayCaster
 */
xyz.swapee.wc.back.ISwapeeLoadingDisplayCaster = class { }
/**
 * Cast the _ISwapeeLoadingDisplay_ instance into the _BoundISwapeeLoadingDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoadingDisplay}
 */
xyz.swapee.wc.back.ISwapeeLoadingDisplayCaster.prototype.asISwapeeLoadingDisplay
/**
 * Access the _SwapeeLoadingDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoadingDisplay}
 */
xyz.swapee.wc.back.ISwapeeLoadingDisplayCaster.prototype.superSwapeeLoadingDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.SwapeeLoadingMemory, land?: null) => void} xyz.swapee.wc.back.ISwapeeLoadingDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingDisplay.__paint<!xyz.swapee.wc.back.ISwapeeLoadingDisplay>} xyz.swapee.wc.back.ISwapeeLoadingDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoadingDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeLoadingMemory} [memory] The display data.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeLoadingDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeLoadingDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/41-SwapeeLoadingClasses.xml}  9b50ae4f196c576cb6892a40ba263e5e */
/**
 * The classes of the _ISwapeeLoadingDisplay_.
 * @record xyz.swapee.wc.SwapeeLoadingClasses
 */
xyz.swapee.wc.SwapeeLoadingClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.SwapeeLoadingClasses.prototype.props = /** @type {xyz.swapee.wc.SwapeeLoadingClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/50-ISwapeeLoadingController.xml}  03063e704ba1ffb3943be4e57a39d17b */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ISwapeeLoadingController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ISwapeeLoadingController.Inputs, !xyz.swapee.wc.ISwapeeLoadingOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel>} xyz.swapee.wc.ISwapeeLoadingController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingController)} xyz.swapee.wc.AbstractSwapeeLoadingController.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingController} xyz.swapee.wc.SwapeeLoadingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingController` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingController
 */
xyz.swapee.wc.AbstractSwapeeLoadingController = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingController.constructor&xyz.swapee.wc.SwapeeLoadingController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingController.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingController.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingController}
 */
xyz.swapee.wc.AbstractSwapeeLoadingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingController}
 */
xyz.swapee.wc.AbstractSwapeeLoadingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingController}
 */
xyz.swapee.wc.AbstractSwapeeLoadingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingController}
 */
xyz.swapee.wc.AbstractSwapeeLoadingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingController.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingController} xyz.swapee.wc.SwapeeLoadingControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ISwapeeLoadingController.Inputs, !xyz.swapee.wc.ISwapeeLoadingOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ISwapeeLoadingOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ISwapeeLoadingController.Inputs, !xyz.swapee.wc.ISwapeeLoadingController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ISwapeeLoadingController.Inputs, !xyz.swapee.wc.SwapeeLoadingMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeLoadingController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ISwapeeLoadingController.Inputs>)} xyz.swapee.wc.ISwapeeLoadingController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ISwapeeLoadingController
 */
xyz.swapee.wc.ISwapeeLoadingController = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoadingController.resetPort} */
xyz.swapee.wc.ISwapeeLoadingController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingController&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingController.Initialese>)} xyz.swapee.wc.SwapeeLoadingController.constructor */
/**
 * A concrete class of _ISwapeeLoadingController_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingController
 * @implements {xyz.swapee.wc.ISwapeeLoadingController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingController.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingController = class extends /** @type {xyz.swapee.wc.SwapeeLoadingController.constructor&xyz.swapee.wc.ISwapeeLoadingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingController}
 */
xyz.swapee.wc.SwapeeLoadingController.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingController.
 * @interface xyz.swapee.wc.ISwapeeLoadingControllerFields
 */
xyz.swapee.wc.ISwapeeLoadingControllerFields = class { }
/**
 * The inputs to the _ISwapeeLoading_'s controller.
 */
xyz.swapee.wc.ISwapeeLoadingControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoadingController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeLoadingControllerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeLoadingController} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingController} */
xyz.swapee.wc.RecordISwapeeLoadingController

/** @typedef {xyz.swapee.wc.ISwapeeLoadingController} xyz.swapee.wc.BoundISwapeeLoadingController */

/** @typedef {xyz.swapee.wc.SwapeeLoadingController} xyz.swapee.wc.BoundSwapeeLoadingController */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingPort.Inputs} xyz.swapee.wc.ISwapeeLoadingController.Inputs The inputs to the _ISwapeeLoading_'s controller. */

/** @typedef {xyz.swapee.wc.ISwapeeLoadingPort.WeakInputs} xyz.swapee.wc.ISwapeeLoadingController.WeakInputs The inputs to the _ISwapeeLoading_'s controller. */

/**
 * Contains getters to cast the _ISwapeeLoadingController_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingControllerCaster
 */
xyz.swapee.wc.ISwapeeLoadingControllerCaster = class { }
/**
 * Cast the _ISwapeeLoadingController_ instance into the _BoundISwapeeLoadingController_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingController}
 */
xyz.swapee.wc.ISwapeeLoadingControllerCaster.prototype.asISwapeeLoadingController
/**
 * Cast the _ISwapeeLoadingController_ instance into the _BoundISwapeeLoadingProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingProcessor}
 */
xyz.swapee.wc.ISwapeeLoadingControllerCaster.prototype.asISwapeeLoadingProcessor
/**
 * Access the _SwapeeLoadingController_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingController}
 */
xyz.swapee.wc.ISwapeeLoadingControllerCaster.prototype.superSwapeeLoadingController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoadingController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoadingController.__resetPort<!xyz.swapee.wc.ISwapeeLoadingController>} xyz.swapee.wc.ISwapeeLoadingController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoadingController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoadingController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/51-ISwapeeLoadingControllerFront.xml}  1b7cc373d5d4121e6069760102542a8b */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ISwapeeLoadingController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeLoadingController)} xyz.swapee.wc.front.AbstractSwapeeLoadingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeLoadingController} xyz.swapee.wc.front.SwapeeLoadingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeLoadingController` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeLoadingController
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingController = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeLoadingController.constructor&xyz.swapee.wc.front.SwapeeLoadingController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeLoadingController.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeLoadingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingController.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingController|typeof xyz.swapee.wc.front.SwapeeLoadingController)|(!xyz.swapee.wc.front.ISwapeeLoadingControllerAT|typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingController|typeof xyz.swapee.wc.front.SwapeeLoadingController)|(!xyz.swapee.wc.front.ISwapeeLoadingControllerAT|typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingController|typeof xyz.swapee.wc.front.SwapeeLoadingController)|(!xyz.swapee.wc.front.ISwapeeLoadingControllerAT|typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeLoadingController.Initialese[]) => xyz.swapee.wc.front.ISwapeeLoadingController} xyz.swapee.wc.front.SwapeeLoadingControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeLoadingControllerCaster&xyz.swapee.wc.front.ISwapeeLoadingControllerAT)} xyz.swapee.wc.front.ISwapeeLoadingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoadingControllerAT} xyz.swapee.wc.front.ISwapeeLoadingControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ISwapeeLoadingController
 */
xyz.swapee.wc.front.ISwapeeLoadingController = class extends /** @type {xyz.swapee.wc.front.ISwapeeLoadingController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ISwapeeLoadingControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoadingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeLoadingController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeLoadingController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoadingController.Initialese>)} xyz.swapee.wc.front.SwapeeLoadingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoadingController} xyz.swapee.wc.front.ISwapeeLoadingController.typeof */
/**
 * A concrete class of _ISwapeeLoadingController_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeLoadingController
 * @implements {xyz.swapee.wc.front.ISwapeeLoadingController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoadingController.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeLoadingController = class extends /** @type {xyz.swapee.wc.front.SwapeeLoadingController.constructor&xyz.swapee.wc.front.ISwapeeLoadingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeLoadingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoadingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeLoadingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingController}
 */
xyz.swapee.wc.front.SwapeeLoadingController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeLoadingController} */
xyz.swapee.wc.front.RecordISwapeeLoadingController

/** @typedef {xyz.swapee.wc.front.ISwapeeLoadingController} xyz.swapee.wc.front.BoundISwapeeLoadingController */

/** @typedef {xyz.swapee.wc.front.SwapeeLoadingController} xyz.swapee.wc.front.BoundSwapeeLoadingController */

/**
 * Contains getters to cast the _ISwapeeLoadingController_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeLoadingControllerCaster
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerCaster = class { }
/**
 * Cast the _ISwapeeLoadingController_ instance into the _BoundISwapeeLoadingController_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeLoadingController}
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerCaster.prototype.asISwapeeLoadingController
/**
 * Access the _SwapeeLoadingController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeLoadingController}
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerCaster.prototype.superSwapeeLoadingController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/52-ISwapeeLoadingControllerBack.xml}  2667e7285318c196459959fdd866501f */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ISwapeeLoadingController.Inputs>&xyz.swapee.wc.ISwapeeLoadingController.Initialese} xyz.swapee.wc.back.ISwapeeLoadingController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoadingController)} xyz.swapee.wc.back.AbstractSwapeeLoadingController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoadingController} xyz.swapee.wc.back.SwapeeLoadingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoadingController` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoadingController
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingController = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoadingController.constructor&xyz.swapee.wc.back.SwapeeLoadingController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoadingController.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoadingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingController.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingController|typeof xyz.swapee.wc.back.SwapeeLoadingController)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingController|typeof xyz.swapee.wc.back.SwapeeLoadingController)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingController|typeof xyz.swapee.wc.back.SwapeeLoadingController)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoadingController.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoadingController} xyz.swapee.wc.back.SwapeeLoadingControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoadingControllerCaster&xyz.swapee.wc.ISwapeeLoadingController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ISwapeeLoadingController.Inputs>)} xyz.swapee.wc.back.ISwapeeLoadingController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingController
 */
xyz.swapee.wc.back.ISwapeeLoadingController = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoadingController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoadingController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoadingController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoadingController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingController.Initialese>)} xyz.swapee.wc.back.SwapeeLoadingController.constructor */
/**
 * A concrete class of _ISwapeeLoadingController_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoadingController
 * @implements {xyz.swapee.wc.back.ISwapeeLoadingController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingController.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoadingController = class extends /** @type {xyz.swapee.wc.back.SwapeeLoadingController.constructor&xyz.swapee.wc.back.ISwapeeLoadingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoadingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoadingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingController}
 */
xyz.swapee.wc.back.SwapeeLoadingController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingController} */
xyz.swapee.wc.back.RecordISwapeeLoadingController

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingController} xyz.swapee.wc.back.BoundISwapeeLoadingController */

/** @typedef {xyz.swapee.wc.back.SwapeeLoadingController} xyz.swapee.wc.back.BoundSwapeeLoadingController */

/**
 * Contains getters to cast the _ISwapeeLoadingController_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingControllerCaster
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerCaster = class { }
/**
 * Cast the _ISwapeeLoadingController_ instance into the _BoundISwapeeLoadingController_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoadingController}
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerCaster.prototype.asISwapeeLoadingController
/**
 * Access the _SwapeeLoadingController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoadingController}
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerCaster.prototype.superSwapeeLoadingController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/53-ISwapeeLoadingControllerAR.xml}  4563d9a7f3ec0d9a2e0a8b6b3571e0ef */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeLoadingController.Initialese} xyz.swapee.wc.back.ISwapeeLoadingControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoadingControllerAR)} xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR} xyz.swapee.wc.back.SwapeeLoadingControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoadingControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.constructor&xyz.swapee.wc.back.SwapeeLoadingControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingControllerAR|typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingControllerAR|typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingControllerAR|typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoadingController|typeof xyz.swapee.wc.SwapeeLoadingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoadingControllerAR.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoadingControllerAR} xyz.swapee.wc.back.SwapeeLoadingControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoadingControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeLoadingController)} xyz.swapee.wc.back.ISwapeeLoadingControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeLoadingControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingControllerAR
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerAR = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoadingControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeLoadingController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoadingControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingControllerAR.Initialese>)} xyz.swapee.wc.back.SwapeeLoadingControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoadingControllerAR} xyz.swapee.wc.back.ISwapeeLoadingControllerAR.typeof */
/**
 * A concrete class of _ISwapeeLoadingControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoadingControllerAR
 * @implements {xyz.swapee.wc.back.ISwapeeLoadingControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeLoadingControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoadingControllerAR = class extends /** @type {xyz.swapee.wc.back.SwapeeLoadingControllerAR.constructor&xyz.swapee.wc.back.ISwapeeLoadingControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoadingControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoadingControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingControllerAR}
 */
xyz.swapee.wc.back.SwapeeLoadingControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingControllerAR} */
xyz.swapee.wc.back.RecordISwapeeLoadingControllerAR

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingControllerAR} xyz.swapee.wc.back.BoundISwapeeLoadingControllerAR */

/** @typedef {xyz.swapee.wc.back.SwapeeLoadingControllerAR} xyz.swapee.wc.back.BoundSwapeeLoadingControllerAR */

/**
 * Contains getters to cast the _ISwapeeLoadingControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingControllerARCaster
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerARCaster = class { }
/**
 * Cast the _ISwapeeLoadingControllerAR_ instance into the _BoundISwapeeLoadingControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoadingControllerAR}
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerARCaster.prototype.asISwapeeLoadingControllerAR
/**
 * Access the _SwapeeLoadingControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoadingControllerAR}
 */
xyz.swapee.wc.back.ISwapeeLoadingControllerARCaster.prototype.superSwapeeLoadingControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/54-ISwapeeLoadingControllerAT.xml}  8d81a7affb5421caf5479a567e6cbf53 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ISwapeeLoadingControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeLoadingControllerAT)} xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT} xyz.swapee.wc.front.SwapeeLoadingControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeLoadingControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.constructor&xyz.swapee.wc.front.SwapeeLoadingControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingControllerAT|typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingControllerAT|typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingControllerAT|typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeLoadingControllerAT.Initialese[]) => xyz.swapee.wc.front.ISwapeeLoadingControllerAT} xyz.swapee.wc.front.SwapeeLoadingControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeLoadingControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ISwapeeLoadingControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeLoadingControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ISwapeeLoadingControllerAT
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerAT = class extends /** @type {xyz.swapee.wc.front.ISwapeeLoadingControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoadingControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeLoadingControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoadingControllerAT.Initialese>)} xyz.swapee.wc.front.SwapeeLoadingControllerAT.constructor */
/**
 * A concrete class of _ISwapeeLoadingControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeLoadingControllerAT
 * @implements {xyz.swapee.wc.front.ISwapeeLoadingControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeLoadingControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoadingControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeLoadingControllerAT = class extends /** @type {xyz.swapee.wc.front.SwapeeLoadingControllerAT.constructor&xyz.swapee.wc.front.ISwapeeLoadingControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeLoadingControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoadingControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeLoadingControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingControllerAT}
 */
xyz.swapee.wc.front.SwapeeLoadingControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeLoadingControllerAT} */
xyz.swapee.wc.front.RecordISwapeeLoadingControllerAT

/** @typedef {xyz.swapee.wc.front.ISwapeeLoadingControllerAT} xyz.swapee.wc.front.BoundISwapeeLoadingControllerAT */

/** @typedef {xyz.swapee.wc.front.SwapeeLoadingControllerAT} xyz.swapee.wc.front.BoundSwapeeLoadingControllerAT */

/**
 * Contains getters to cast the _ISwapeeLoadingControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeLoadingControllerATCaster
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerATCaster = class { }
/**
 * Cast the _ISwapeeLoadingControllerAT_ instance into the _BoundISwapeeLoadingControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeLoadingControllerAT}
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerATCaster.prototype.asISwapeeLoadingControllerAT
/**
 * Access the _SwapeeLoadingControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeLoadingControllerAT}
 */
xyz.swapee.wc.front.ISwapeeLoadingControllerATCaster.prototype.superSwapeeLoadingControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/70-ISwapeeLoadingScreen.xml}  9ab73889e98c20198be04f7958f6bc5a */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.front.SwapeeLoadingInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeLoadingDisplay.Settings, !xyz.swapee.wc.ISwapeeLoadingDisplay.Queries, null>&xyz.swapee.wc.ISwapeeLoadingDisplay.Initialese} xyz.swapee.wc.ISwapeeLoadingScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingScreen)} xyz.swapee.wc.AbstractSwapeeLoadingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingScreen} xyz.swapee.wc.SwapeeLoadingScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingScreen` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingScreen
 */
xyz.swapee.wc.AbstractSwapeeLoadingScreen = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingScreen.constructor&xyz.swapee.wc.SwapeeLoadingScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingScreen.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingScreen.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingScreen|typeof xyz.swapee.wc.SwapeeLoadingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeLoadingController|typeof xyz.swapee.wc.front.SwapeeLoadingController)|(!xyz.swapee.wc.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.SwapeeLoadingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoadingScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoadingScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingScreen|typeof xyz.swapee.wc.SwapeeLoadingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeLoadingController|typeof xyz.swapee.wc.front.SwapeeLoadingController)|(!xyz.swapee.wc.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.SwapeeLoadingDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoadingScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingScreen|typeof xyz.swapee.wc.SwapeeLoadingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeLoadingController|typeof xyz.swapee.wc.front.SwapeeLoadingController)|(!xyz.swapee.wc.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.SwapeeLoadingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoadingScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingScreen.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingScreen} xyz.swapee.wc.SwapeeLoadingScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.SwapeeLoadingMemory, !xyz.swapee.wc.front.SwapeeLoadingInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeLoadingDisplay.Settings, !xyz.swapee.wc.ISwapeeLoadingDisplay.Queries, null, null>&xyz.swapee.wc.front.ISwapeeLoadingController&xyz.swapee.wc.ISwapeeLoadingDisplay)} xyz.swapee.wc.ISwapeeLoadingScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ISwapeeLoadingScreen
 */
xyz.swapee.wc.ISwapeeLoadingScreen = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ISwapeeLoadingController.typeof&xyz.swapee.wc.ISwapeeLoadingDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingScreen.Initialese>)} xyz.swapee.wc.SwapeeLoadingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoadingScreen} xyz.swapee.wc.ISwapeeLoadingScreen.typeof */
/**
 * A concrete class of _ISwapeeLoadingScreen_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingScreen
 * @implements {xyz.swapee.wc.ISwapeeLoadingScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingScreen.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingScreen = class extends /** @type {xyz.swapee.wc.SwapeeLoadingScreen.constructor&xyz.swapee.wc.ISwapeeLoadingScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingScreen}
 */
xyz.swapee.wc.SwapeeLoadingScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoadingScreen} */
xyz.swapee.wc.RecordISwapeeLoadingScreen

/** @typedef {xyz.swapee.wc.ISwapeeLoadingScreen} xyz.swapee.wc.BoundISwapeeLoadingScreen */

/** @typedef {xyz.swapee.wc.SwapeeLoadingScreen} xyz.swapee.wc.BoundSwapeeLoadingScreen */

/**
 * Contains getters to cast the _ISwapeeLoadingScreen_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingScreenCaster
 */
xyz.swapee.wc.ISwapeeLoadingScreenCaster = class { }
/**
 * Cast the _ISwapeeLoadingScreen_ instance into the _BoundISwapeeLoadingScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingScreen}
 */
xyz.swapee.wc.ISwapeeLoadingScreenCaster.prototype.asISwapeeLoadingScreen
/**
 * Access the _SwapeeLoadingScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingScreen}
 */
xyz.swapee.wc.ISwapeeLoadingScreenCaster.prototype.superSwapeeLoadingScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/70-ISwapeeLoadingScreenBack.xml}  b6eb1d6f5aeae5cde7020a28e5232cda */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese} xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoadingScreen)} xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoadingScreen} xyz.swapee.wc.back.SwapeeLoadingScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoadingScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoadingScreen
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.constructor&xyz.swapee.wc.back.SwapeeLoadingScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoadingScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingScreen|typeof xyz.swapee.wc.back.SwapeeLoadingScreen)|(!xyz.swapee.wc.back.ISwapeeLoadingScreenAT|typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingScreen|typeof xyz.swapee.wc.back.SwapeeLoadingScreen)|(!xyz.swapee.wc.back.ISwapeeLoadingScreenAT|typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingScreen|typeof xyz.swapee.wc.back.SwapeeLoadingScreen)|(!xyz.swapee.wc.back.ISwapeeLoadingScreenAT|typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoadingScreen} xyz.swapee.wc.back.SwapeeLoadingScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoadingScreenCaster&xyz.swapee.wc.back.ISwapeeLoadingScreenAT)} xyz.swapee.wc.back.ISwapeeLoadingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoadingScreenAT} xyz.swapee.wc.back.ISwapeeLoadingScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingScreen
 */
xyz.swapee.wc.back.ISwapeeLoadingScreen = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoadingScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeLoadingScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoadingScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoadingScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese>)} xyz.swapee.wc.back.SwapeeLoadingScreen.constructor */
/**
 * A concrete class of _ISwapeeLoadingScreen_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoadingScreen
 * @implements {xyz.swapee.wc.back.ISwapeeLoadingScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoadingScreen = class extends /** @type {xyz.swapee.wc.back.SwapeeLoadingScreen.constructor&xyz.swapee.wc.back.ISwapeeLoadingScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoadingScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreen}
 */
xyz.swapee.wc.back.SwapeeLoadingScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingScreen} */
xyz.swapee.wc.back.RecordISwapeeLoadingScreen

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingScreen} xyz.swapee.wc.back.BoundISwapeeLoadingScreen */

/** @typedef {xyz.swapee.wc.back.SwapeeLoadingScreen} xyz.swapee.wc.back.BoundSwapeeLoadingScreen */

/**
 * Contains getters to cast the _ISwapeeLoadingScreen_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingScreenCaster
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenCaster = class { }
/**
 * Cast the _ISwapeeLoadingScreen_ instance into the _BoundISwapeeLoadingScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoadingScreen}
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenCaster.prototype.asISwapeeLoadingScreen
/**
 * Access the _SwapeeLoadingScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoadingScreen}
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenCaster.prototype.superSwapeeLoadingScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/73-ISwapeeLoadingScreenAR.xml}  d6b6f66d5d73b44a4d86c25a4d5bb8a1 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeLoadingScreen.Initialese} xyz.swapee.wc.front.ISwapeeLoadingScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeLoadingScreenAR)} xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR} xyz.swapee.wc.front.SwapeeLoadingScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeLoadingScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.constructor&xyz.swapee.wc.front.SwapeeLoadingScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingScreenAR|typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoadingScreen|typeof xyz.swapee.wc.SwapeeLoadingScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingScreenAR|typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoadingScreen|typeof xyz.swapee.wc.SwapeeLoadingScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoadingScreenAR|typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoadingScreen|typeof xyz.swapee.wc.SwapeeLoadingScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoadingScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeLoadingScreenAR.Initialese[]) => xyz.swapee.wc.front.ISwapeeLoadingScreenAR} xyz.swapee.wc.front.SwapeeLoadingScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeLoadingScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeLoadingScreen)} xyz.swapee.wc.front.ISwapeeLoadingScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeLoadingScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ISwapeeLoadingScreenAR
 */
xyz.swapee.wc.front.ISwapeeLoadingScreenAR = class extends /** @type {xyz.swapee.wc.front.ISwapeeLoadingScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeLoadingScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoadingScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeLoadingScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeLoadingScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoadingScreenAR.Initialese>)} xyz.swapee.wc.front.SwapeeLoadingScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoadingScreenAR} xyz.swapee.wc.front.ISwapeeLoadingScreenAR.typeof */
/**
 * A concrete class of _ISwapeeLoadingScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeLoadingScreenAR
 * @implements {xyz.swapee.wc.front.ISwapeeLoadingScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeLoadingScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoadingScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeLoadingScreenAR = class extends /** @type {xyz.swapee.wc.front.SwapeeLoadingScreenAR.constructor&xyz.swapee.wc.front.ISwapeeLoadingScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeLoadingScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoadingScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeLoadingScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeLoadingScreenAR}
 */
xyz.swapee.wc.front.SwapeeLoadingScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeLoadingScreenAR} */
xyz.swapee.wc.front.RecordISwapeeLoadingScreenAR

/** @typedef {xyz.swapee.wc.front.ISwapeeLoadingScreenAR} xyz.swapee.wc.front.BoundISwapeeLoadingScreenAR */

/** @typedef {xyz.swapee.wc.front.SwapeeLoadingScreenAR} xyz.swapee.wc.front.BoundSwapeeLoadingScreenAR */

/**
 * Contains getters to cast the _ISwapeeLoadingScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeLoadingScreenARCaster
 */
xyz.swapee.wc.front.ISwapeeLoadingScreenARCaster = class { }
/**
 * Cast the _ISwapeeLoadingScreenAR_ instance into the _BoundISwapeeLoadingScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeLoadingScreenAR}
 */
xyz.swapee.wc.front.ISwapeeLoadingScreenARCaster.prototype.asISwapeeLoadingScreenAR
/**
 * Access the _SwapeeLoadingScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeLoadingScreenAR}
 */
xyz.swapee.wc.front.ISwapeeLoadingScreenARCaster.prototype.superSwapeeLoadingScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/74-ISwapeeLoadingScreenAT.xml}  5bac6867dd87e1108ed824c1ce4760e6 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoadingScreenAT)} xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT} xyz.swapee.wc.back.SwapeeLoadingScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoadingScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.constructor&xyz.swapee.wc.back.SwapeeLoadingScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingScreenAT|typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingScreenAT|typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoadingScreenAT|typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoadingScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoadingScreenAT} xyz.swapee.wc.back.SwapeeLoadingScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoadingScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ISwapeeLoadingScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeLoadingScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingScreenAT
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenAT = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoadingScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoadingScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese>)} xyz.swapee.wc.back.SwapeeLoadingScreenAT.constructor */
/**
 * A concrete class of _ISwapeeLoadingScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoadingScreenAT
 * @implements {xyz.swapee.wc.back.ISwapeeLoadingScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeLoadingScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoadingScreenAT = class extends /** @type {xyz.swapee.wc.back.SwapeeLoadingScreenAT.constructor&xyz.swapee.wc.back.ISwapeeLoadingScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoadingScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoadingScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoadingScreenAT}
 */
xyz.swapee.wc.back.SwapeeLoadingScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingScreenAT} */
xyz.swapee.wc.back.RecordISwapeeLoadingScreenAT

/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingScreenAT} xyz.swapee.wc.back.BoundISwapeeLoadingScreenAT */

/** @typedef {xyz.swapee.wc.back.SwapeeLoadingScreenAT} xyz.swapee.wc.back.BoundSwapeeLoadingScreenAT */

/**
 * Contains getters to cast the _ISwapeeLoadingScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoadingScreenATCaster
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenATCaster = class { }
/**
 * Cast the _ISwapeeLoadingScreenAT_ instance into the _BoundISwapeeLoadingScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoadingScreenAT}
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenATCaster.prototype.asISwapeeLoadingScreenAT
/**
 * Access the _SwapeeLoadingScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoadingScreenAT}
 */
xyz.swapee.wc.back.ISwapeeLoadingScreenATCaster.prototype.superSwapeeLoadingScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-loading/SwapeeLoading.mvc/design/80-ISwapeeLoadingGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoadingDisplay.Initialese} xyz.swapee.wc.ISwapeeLoadingGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoadingGPU)} xyz.swapee.wc.AbstractSwapeeLoadingGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoadingGPU} xyz.swapee.wc.SwapeeLoadingGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoadingGPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoadingGPU
 */
xyz.swapee.wc.AbstractSwapeeLoadingGPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoadingGPU.constructor&xyz.swapee.wc.SwapeeLoadingGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoadingGPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoadingGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoadingGPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoadingGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingGPU|typeof xyz.swapee.wc.SwapeeLoadingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.back.SwapeeLoadingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoadingGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoadingGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoadingGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoadingGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingGPU|typeof xyz.swapee.wc.SwapeeLoadingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.back.SwapeeLoadingDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoadingGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoadingGPU|typeof xyz.swapee.wc.SwapeeLoadingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeLoadingDisplay|typeof xyz.swapee.wc.back.SwapeeLoadingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoadingGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoadingGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoadingGPU.Initialese[]) => xyz.swapee.wc.ISwapeeLoadingGPU} xyz.swapee.wc.SwapeeLoadingGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoadingGPUCaster&com.webcircuits.IBrowserView<.!SwapeeLoadingMemory,>&xyz.swapee.wc.back.ISwapeeLoadingDisplay)} xyz.swapee.wc.ISwapeeLoadingGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!SwapeeLoadingMemory,>} com.webcircuits.IBrowserView<.!SwapeeLoadingMemory,>.typeof */
/**
 * Handles the periphery of the _ISwapeeLoadingDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ISwapeeLoadingGPU
 */
xyz.swapee.wc.ISwapeeLoadingGPU = class extends /** @type {xyz.swapee.wc.ISwapeeLoadingGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!SwapeeLoadingMemory,>.typeof&xyz.swapee.wc.back.ISwapeeLoadingDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoadingGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoadingGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoadingGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingGPU.Initialese>)} xyz.swapee.wc.SwapeeLoadingGPU.constructor */
/**
 * A concrete class of _ISwapeeLoadingGPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoadingGPU
 * @implements {xyz.swapee.wc.ISwapeeLoadingGPU} Handles the periphery of the _ISwapeeLoadingDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoadingGPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoadingGPU = class extends /** @type {xyz.swapee.wc.SwapeeLoadingGPU.constructor&xyz.swapee.wc.ISwapeeLoadingGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoadingGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoadingGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoadingGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoadingGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoadingGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoadingGPU}
 */
xyz.swapee.wc.SwapeeLoadingGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoadingGPU.
 * @interface xyz.swapee.wc.ISwapeeLoadingGPUFields
 */
xyz.swapee.wc.ISwapeeLoadingGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ISwapeeLoadingGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoadingGPU} */
xyz.swapee.wc.RecordISwapeeLoadingGPU

/** @typedef {xyz.swapee.wc.ISwapeeLoadingGPU} xyz.swapee.wc.BoundISwapeeLoadingGPU */

/** @typedef {xyz.swapee.wc.SwapeeLoadingGPU} xyz.swapee.wc.BoundSwapeeLoadingGPU */

/**
 * Contains getters to cast the _ISwapeeLoadingGPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoadingGPUCaster
 */
xyz.swapee.wc.ISwapeeLoadingGPUCaster = class { }
/**
 * Cast the _ISwapeeLoadingGPU_ instance into the _BoundISwapeeLoadingGPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoadingGPU}
 */
xyz.swapee.wc.ISwapeeLoadingGPUCaster.prototype.asISwapeeLoadingGPU
/**
 * Access the _SwapeeLoadingGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoadingGPU}
 */
xyz.swapee.wc.ISwapeeLoadingGPUCaster.prototype.superSwapeeLoadingGPU

// nss:xyz.swapee.wc
/* @typal-end */