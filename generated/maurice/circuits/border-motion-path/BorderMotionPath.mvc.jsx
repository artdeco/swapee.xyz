/** @extends {xyz.swapee.wc.AbstractBorderMotionPath} */
export default class AbstractBorderMotionPath extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractBorderMotionPathComputer} */
export class AbstractBorderMotionPathComputer extends (<computer>
   <adapter name="adaptPath">
    <com.webcircuits.ui.IClientRectCore width="real" height="real" />
    <xyz.swapee.wc.IBorderMotionPathCore borderRadius />
    <outputs>
     <xyz.swapee.wc.IBorderMotionPathCore path ready />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractBorderMotionPathController} */
export class AbstractBorderMotionPathController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractBorderMotionPathPort} */
export class BorderMotionPathPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractBorderMotionPathView} */
export class AbstractBorderMotionPathView extends (<view>
  <classes>
   <string opt name="Ready" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractBorderMotionPathElement} */
export class AbstractBorderMotionPathElement extends (<element v3 html mv>
 <block src="./BorderMotionPath.mvc/src/BorderMotionPathElement/methods/render.jsx" />
 <inducer src="./BorderMotionPath.mvc/src/BorderMotionPathElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent} */
export class AbstractBorderMotionPathHtmlComponent extends (<html-ic>
  <connectors>

   <com.webcircuits.ui.IClientRect via="ClientRect" />
  </connectors>

</html-ic>) { }
// </class-end>