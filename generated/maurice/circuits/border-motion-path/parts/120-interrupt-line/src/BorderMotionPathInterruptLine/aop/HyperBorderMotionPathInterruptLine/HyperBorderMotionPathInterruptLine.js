import AbstractHyperBorderMotionPathInterruptLine from '../../../../gen/AbstractBorderMotionPathInterruptLine/hyper/AbstractHyperBorderMotionPathInterruptLine'
import BorderMotionPathInterruptLine from '../../BorderMotionPathInterruptLine'
import BorderMotionPathInterruptLineGeneralAspects from '../BorderMotionPathInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperBorderMotionPathInterruptLine} */
export default class extends AbstractHyperBorderMotionPathInterruptLine
 .consults(
  BorderMotionPathInterruptLineGeneralAspects,
 )
 .implements(
  BorderMotionPathInterruptLine,
 )
{}