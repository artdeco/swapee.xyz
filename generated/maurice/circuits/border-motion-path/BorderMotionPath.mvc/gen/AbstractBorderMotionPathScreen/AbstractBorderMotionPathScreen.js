import BorderMotionPathClassesPQs from '../../pqs/BorderMotionPathClassesPQs'
import AbstractBorderMotionPathScreenAR from '../AbstractBorderMotionPathScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {BorderMotionPathInputsPQs} from '../../pqs/BorderMotionPathInputsPQs'
import {BorderMotionPathMemoryQPs} from '../../pqs/BorderMotionPathMemoryQPs'
import {BorderMotionPathVdusPQs} from '../../pqs/BorderMotionPathVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathScreen}
 */
function __AbstractBorderMotionPathScreen() {}
__AbstractBorderMotionPathScreen.prototype = /** @type {!_AbstractBorderMotionPathScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathScreen}
 */
class _AbstractBorderMotionPathScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathScreen} ‎
 */
class AbstractBorderMotionPathScreen extends newAbstract(
 _AbstractBorderMotionPathScreen,963146664724,null,{
  asIBorderMotionPathScreen:1,
  superBorderMotionPathScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathScreen} */
AbstractBorderMotionPathScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathScreen} */
function AbstractBorderMotionPathScreenClass(){}

export default AbstractBorderMotionPathScreen


AbstractBorderMotionPathScreen[$implementations]=[
 __AbstractBorderMotionPathScreen,
 AbstractBorderMotionPathScreenClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathScreen}*/({
  inputsPQs:BorderMotionPathInputsPQs,
  classesPQs:BorderMotionPathClassesPQs,
  memoryQPs:BorderMotionPathMemoryQPs,
 }),
 Screen,
 AbstractBorderMotionPathScreenAR,
 AbstractBorderMotionPathScreenClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathScreen}*/({
  vdusPQs:BorderMotionPathVdusPQs,
 }),
 AbstractBorderMotionPathScreenClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathScreen}*/({
  deduceInputs:(BorderMotionPath)=>{
   return {
    path:BorderMotionPath.style.getPropertyValue('--d6fe1'),
   }
  },
 }),
]