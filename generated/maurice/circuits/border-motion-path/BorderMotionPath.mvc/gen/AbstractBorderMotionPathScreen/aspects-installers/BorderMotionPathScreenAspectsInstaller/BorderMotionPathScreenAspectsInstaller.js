import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_BorderMotionPathScreenAspectsInstaller}
 */
function __BorderMotionPathScreenAspectsInstaller() {}
__BorderMotionPathScreenAspectsInstaller.prototype = /** @type {!_BorderMotionPathScreenAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathScreenAspectsInstaller}
 */
class _BorderMotionPathScreenAspectsInstaller { }

_BorderMotionPathScreenAspectsInstaller.prototype[$advice]=__BorderMotionPathScreenAspectsInstaller

/** @extends {xyz.swapee.wc.AbstractBorderMotionPathScreenAspectsInstaller} ‎ */
class BorderMotionPathScreenAspectsInstaller extends newAbstract(
 _BorderMotionPathScreenAspectsInstaller,963146664731,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathScreenAspectsInstaller} */
BorderMotionPathScreenAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathScreenAspectsInstaller} */
function BorderMotionPathScreenAspectsInstallerClass(){}

export default BorderMotionPathScreenAspectsInstaller


BorderMotionPathScreenAspectsInstaller[$implementations]=[
 BorderMotionPathScreenAspectsInstallerClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathScreenAspectsInstaller}*/({
  syncPath(){
   this.beforeSyncPath=1
   this.afterSyncPath=2
   this.aroundSyncPath=3
   this.afterSyncPathThrows=4
   this.afterSyncPathReturns=5
   this.afterSyncPathCancels=7
  },
 }),
 __BorderMotionPathScreenAspectsInstaller,
]