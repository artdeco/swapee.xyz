import AbstractBorderMotionPathDisplay from '../AbstractBorderMotionPathDisplayBack'
import BorderMotionPathClassesPQs from '../../pqs/BorderMotionPathClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {BorderMotionPathClassesQPs} from '../../pqs/BorderMotionPathClassesQPs'
import {BorderMotionPathVdusPQs} from '../../pqs/BorderMotionPathVdusPQs'
import {BorderMotionPathVdusQPs} from '../../pqs/BorderMotionPathVdusQPs'
import {BorderMotionPathMemoryPQs} from '../../pqs/BorderMotionPathMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathGPU}
 */
function __AbstractBorderMotionPathGPU() {}
__AbstractBorderMotionPathGPU.prototype = /** @type {!_AbstractBorderMotionPathGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathGPU}
 */
class _AbstractBorderMotionPathGPU { }
/**
 * Handles the periphery of the _IBorderMotionPathDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathGPU} ‎
 */
class AbstractBorderMotionPathGPU extends newAbstract(
 _AbstractBorderMotionPathGPU,963146664715,null,{
  asIBorderMotionPathGPU:1,
  superBorderMotionPathGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathGPU} */
AbstractBorderMotionPathGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathGPU} */
function AbstractBorderMotionPathGPUClass(){}

export default AbstractBorderMotionPathGPU


AbstractBorderMotionPathGPU[$implementations]=[
 __AbstractBorderMotionPathGPU,
 AbstractBorderMotionPathGPUClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathGPU}*/({
  classesQPs:BorderMotionPathClassesQPs,
  vdusPQs:BorderMotionPathVdusPQs,
  vdusQPs:BorderMotionPathVdusQPs,
  memoryPQs:BorderMotionPathMemoryPQs,
 }),
 AbstractBorderMotionPathDisplay,
 BrowserView,
 AbstractBorderMotionPathGPUClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathGPU}*/({
  allocator(){
   pressFit(this.classes,'',BorderMotionPathClassesPQs)
  },
 }),
]