import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_BorderMotionPathElementPort}
 */
function __BorderMotionPathElementPort() {}
__BorderMotionPathElementPort.prototype = /** @type {!_BorderMotionPathElementPort} */ ({ })
/** @this {xyz.swapee.wc.BorderMotionPathElementPort} */ function BorderMotionPathElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IBorderMotionPathElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    particlesWrOpts: {},
    pathOpts: {},
    clientRectOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathElementPort}
 */
class _BorderMotionPathElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathElementPort} ‎
 */
class BorderMotionPathElementPort extends newAbstract(
 _BorderMotionPathElementPort,963146664713,BorderMotionPathElementPortConstructor,{
  asIBorderMotionPathElementPort:1,
  superBorderMotionPathElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathElementPort} */
BorderMotionPathElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathElementPort} */
function BorderMotionPathElementPortClass(){}

export default BorderMotionPathElementPort


BorderMotionPathElementPort[$implementations]=[
 __BorderMotionPathElementPort,
 BorderMotionPathElementPortClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'particles-wr-opts':undefined,
    'path-opts':undefined,
    'client-rect-opts':undefined,
    'border-radius':undefined,
    'svg-path':undefined,
   })
  },
 }),
]