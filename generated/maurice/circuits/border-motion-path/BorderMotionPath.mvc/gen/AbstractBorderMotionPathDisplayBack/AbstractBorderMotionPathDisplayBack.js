import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathDisplay}
 */
function __AbstractBorderMotionPathDisplay() {}
__AbstractBorderMotionPathDisplay.prototype = /** @type {!_AbstractBorderMotionPathDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathDisplay}
 */
class _AbstractBorderMotionPathDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathDisplay} ‎
 */
class AbstractBorderMotionPathDisplay extends newAbstract(
 _AbstractBorderMotionPathDisplay,963146664718,null,{
  asIBorderMotionPathDisplay:1,
  superBorderMotionPathDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathDisplay} */
AbstractBorderMotionPathDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathDisplay} */
function AbstractBorderMotionPathDisplayClass(){}

export default AbstractBorderMotionPathDisplay


AbstractBorderMotionPathDisplay[$implementations]=[
 __AbstractBorderMotionPathDisplay,
 GraphicsDriverBack,
 AbstractBorderMotionPathDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IBorderMotionPathDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IBorderMotionPathDisplay}*/({
    Path:twinMock,
    ParticlesWr:twinMock,
    ClientRect:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.BorderMotionPathDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IBorderMotionPathDisplay.Initialese}*/({
   ParticlesWr:1,
   Path:1,
   ClientRect:1,
  }),
  initializer({
   ParticlesWr:_ParticlesWr,
   Path:_Path,
   ClientRect:_ClientRect,
  }) {
   if(_ParticlesWr!==undefined) this.ParticlesWr=_ParticlesWr
   if(_Path!==undefined) this.Path=_Path
   if(_ClientRect!==undefined) this.ClientRect=_ClientRect
  },
 }),
]