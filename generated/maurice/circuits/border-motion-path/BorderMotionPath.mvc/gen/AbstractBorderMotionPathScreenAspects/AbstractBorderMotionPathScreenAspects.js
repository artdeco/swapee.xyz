import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathScreenAspects}
 */
function __AbstractBorderMotionPathScreenAspects() {}
__AbstractBorderMotionPathScreenAspects.prototype = /** @type {!_AbstractBorderMotionPathScreenAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathScreenAspects}
 */
class _AbstractBorderMotionPathScreenAspects { }
/**
 * The aspects of the screen.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathScreenAspects} ‎
 */
class AbstractBorderMotionPathScreenAspects extends newAspects(
 _AbstractBorderMotionPathScreenAspects,963146664733,null,{
  asIBorderMotionPathScreenAspects:1,
  superBorderMotionPathScreenAspects:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathScreenAspects} */
AbstractBorderMotionPathScreenAspects.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathScreenAspects} */
function AbstractBorderMotionPathScreenAspectsClass(){}

export default AbstractBorderMotionPathScreenAspects


AbstractBorderMotionPathScreenAspects[$implementations]=[
 __AbstractBorderMotionPathScreenAspects,
]