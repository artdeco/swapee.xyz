import BorderMotionPathBuffer from '../BorderMotionPathBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {BorderMotionPathPortConnector} from '../BorderMotionPathPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathController}
 */
function __AbstractBorderMotionPathController() {}
__AbstractBorderMotionPathController.prototype = /** @type {!_AbstractBorderMotionPathController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathController}
 */
class _AbstractBorderMotionPathController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathController} ‎
 */
export class AbstractBorderMotionPathController extends newAbstract(
 _AbstractBorderMotionPathController,963146664719,null,{
  asIBorderMotionPathController:1,
  superBorderMotionPathController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathController} */
AbstractBorderMotionPathController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathController} */
function AbstractBorderMotionPathControllerClass(){}


AbstractBorderMotionPathController[$implementations]=[
 AbstractBorderMotionPathControllerClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IBorderMotionPathPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractBorderMotionPathController,
 BorderMotionPathBuffer,
 IntegratedController,
 /**@type {!AbstractBorderMotionPathController}*/(BorderMotionPathPortConnector),
]


export default AbstractBorderMotionPathController