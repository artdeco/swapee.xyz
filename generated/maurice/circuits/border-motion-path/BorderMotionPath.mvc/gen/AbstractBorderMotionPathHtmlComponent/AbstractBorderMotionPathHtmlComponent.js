import AbstractBorderMotionPathGPU from '../AbstractBorderMotionPathGPU'
import AbstractBorderMotionPathScreenBack from '../AbstractBorderMotionPathScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {BorderMotionPathInputsQPs} from '../../pqs/BorderMotionPathInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractBorderMotionPath from '../AbstractBorderMotionPath'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathHtmlComponent}
 */
function __AbstractBorderMotionPathHtmlComponent() {}
__AbstractBorderMotionPathHtmlComponent.prototype = /** @type {!_AbstractBorderMotionPathHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent}
 */
class _AbstractBorderMotionPathHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.BorderMotionPathHtmlComponent} */ (res)
  }
}
/**
 * The _IBorderMotionPath_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent} ‎
 */
export class AbstractBorderMotionPathHtmlComponent extends newAbstract(
 _AbstractBorderMotionPathHtmlComponent,963146664711,null,{
  asIBorderMotionPathHtmlComponent:1,
  superBorderMotionPathHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent} */
AbstractBorderMotionPathHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent} */
function AbstractBorderMotionPathHtmlComponentClass(){}


AbstractBorderMotionPathHtmlComponent[$implementations]=[
 __AbstractBorderMotionPathHtmlComponent,
 HtmlComponent,
 AbstractBorderMotionPath,
 AbstractBorderMotionPathGPU,
 AbstractBorderMotionPathScreenBack,
 Landed,
 AbstractBorderMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathHtmlComponent}*/({
  constructor(){
   this.land={
    ClientRect:null,
   }
  },
 }),
 AbstractBorderMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathHtmlComponent}*/({
  deduceProps(el){
   const{
    asIBrowserView:{weHaveClass:weHaveClass},
    classes:{
     Ready:Ready,
    },
   }=this

   return{
   }
  },
 }),
 AbstractBorderMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathHtmlComponent}*/({
  inputsQPs:BorderMotionPathInputsQPs,
 }),
 AbstractBorderMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathHtmlComponent}*/({
  paint:function paint_d_on_Path({svgPath:svgPath}){
   const{asIBorderMotionPathGPU:{Path:Path},asIBrowserView:{attr:attr}}=this
   attr(Path,'d',svgPath)
  },
 }),
 AbstractBorderMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathHtmlComponent}*/({
  paint:function paint_Ready_on_element({path:path}){
   const{
    asIBorderMotionPathGPU:{
     element:element,
    },
    classes:{Ready:Ready},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(!!path) addClass(element,Ready)
   else removeClass(element,Ready)
  },
 }),
 AbstractBorderMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathHtmlComponent}*/({
  paint:function paint_path_var_on_element({path:path}){
   const{asIBorderMotionPathGPU:{element:element},asIBrowserView:{style:style}}=this
   style(element,'--d6fe1',path)
  },
 }),
 AbstractBorderMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{build:build},
    asIBorderMotionPathGPU:{
     ClientRect:ClientRect,
    },
   }=this
   build(3715284221,{ClientRect:ClientRect})
  },
 }),
]