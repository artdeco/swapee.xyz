import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathComputer}
 */
function __AbstractBorderMotionPathComputer() {}
__AbstractBorderMotionPathComputer.prototype = /** @type {!_AbstractBorderMotionPathComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathComputer}
 */
class _AbstractBorderMotionPathComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathComputer} ‎
 */
export class AbstractBorderMotionPathComputer extends newAbstract(
 _AbstractBorderMotionPathComputer,96314666471,null,{
  asIBorderMotionPathComputer:1,
  superBorderMotionPathComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathComputer} */
AbstractBorderMotionPathComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathComputer} */
function AbstractBorderMotionPathComputerClass(){}


AbstractBorderMotionPathComputer[$implementations]=[
 __AbstractBorderMotionPathComputer,
 Adapter,
]


export default AbstractBorderMotionPathComputer