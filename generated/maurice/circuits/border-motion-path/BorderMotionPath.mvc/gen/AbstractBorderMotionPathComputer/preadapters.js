
/**@this {xyz.swapee.wc.IBorderMotionPathComputer}*/
export function preadaptPath(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Form}*/
 const _inputs={
  width:this.land.ClientRect?this.land.ClientRect.model['eaae2']:void 0,
  height:this.land.ClientRect?this.land.ClientRect.model['b435e']:void 0,
  borderRadius:inputs.borderRadius,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(([null,void 0].includes(__inputs.width))||([null,void 0].includes(__inputs.height))) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPath(__inputs,__changes)
 return RET
}