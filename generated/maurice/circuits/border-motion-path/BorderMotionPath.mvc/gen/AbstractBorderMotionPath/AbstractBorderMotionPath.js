import AbstractBorderMotionPathProcessor from '../AbstractBorderMotionPathProcessor'
import {BorderMotionPathCore} from '../BorderMotionPathCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractBorderMotionPathComputer} from '../AbstractBorderMotionPathComputer'
import {AbstractBorderMotionPathController} from '../AbstractBorderMotionPathController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPath}
 */
function __AbstractBorderMotionPath() {}
__AbstractBorderMotionPath.prototype = /** @type {!_AbstractBorderMotionPath} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPath}
 */
class _AbstractBorderMotionPath { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPath} ‎
 */
class AbstractBorderMotionPath extends newAbstract(
 _AbstractBorderMotionPath,96314666479,null,{
  asIBorderMotionPath:1,
  superBorderMotionPath:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPath} */
AbstractBorderMotionPath.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPath} */
function AbstractBorderMotionPathClass(){}

export default AbstractBorderMotionPath


AbstractBorderMotionPath[$implementations]=[
 __AbstractBorderMotionPath,
 BorderMotionPathCore,
 AbstractBorderMotionPathProcessor,
 IntegratedComponent,
 AbstractBorderMotionPathComputer,
 AbstractBorderMotionPathController,
]


export {AbstractBorderMotionPath}