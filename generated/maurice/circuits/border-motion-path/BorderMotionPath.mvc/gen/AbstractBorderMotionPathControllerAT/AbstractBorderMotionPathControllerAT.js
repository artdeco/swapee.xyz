import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathControllerAT}
 */
function __AbstractBorderMotionPathControllerAT() {}
__AbstractBorderMotionPathControllerAT.prototype = /** @type {!_AbstractBorderMotionPathControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT}
 */
class _AbstractBorderMotionPathControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IBorderMotionPathControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT} ‎
 */
class AbstractBorderMotionPathControllerAT extends newAbstract(
 _AbstractBorderMotionPathControllerAT,963146664723,null,{
  asIBorderMotionPathControllerAT:1,
  superBorderMotionPathControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT} */
AbstractBorderMotionPathControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT} */
function AbstractBorderMotionPathControllerATClass(){}

export default AbstractBorderMotionPathControllerAT


AbstractBorderMotionPathControllerAT[$implementations]=[
 __AbstractBorderMotionPathControllerAT,
 UartUniversal,
 AbstractBorderMotionPathControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IBorderMotionPathControllerAT}*/({
  get asIBorderMotionPathController(){
   return this
  },
 }),
]