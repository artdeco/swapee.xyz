import {mountPins} from '@webcircuits/webcircuits'
import {BorderMotionPathMemoryPQs} from '../../pqs/BorderMotionPathMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_BorderMotionPathCore}
 */
function __BorderMotionPathCore() {}
__BorderMotionPathCore.prototype = /** @type {!_BorderMotionPathCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathCore}
 */
class _BorderMotionPathCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathCore} ‎
 */
class BorderMotionPathCore extends newAbstract(
 _BorderMotionPathCore,96314666477,null,{
  asIBorderMotionPathCore:1,
  superBorderMotionPathCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathCore} */
BorderMotionPathCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathCore} */
function BorderMotionPathCoreClass(){}

export default BorderMotionPathCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_BorderMotionPathOuterCore}
 */
function __BorderMotionPathOuterCore() {}
__BorderMotionPathOuterCore.prototype = /** @type {!_BorderMotionPathOuterCore} */ ({ })
/** @this {xyz.swapee.wc.BorderMotionPathOuterCore} */
export function BorderMotionPathOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IBorderMotionPathOuterCore.Model}*/
  this.model={
    gap: 0.01,
    count: 12,
    borderRadius: 10,
    delay: 0,
    path: '',
    svgPath: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathOuterCore}
 */
class _BorderMotionPathOuterCore { }
/**
 * The _IBorderMotionPath_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathOuterCore} ‎
 */
export class BorderMotionPathOuterCore extends newAbstract(
 _BorderMotionPathOuterCore,96314666473,BorderMotionPathOuterCoreConstructor,{
  asIBorderMotionPathOuterCore:1,
  superBorderMotionPathOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathOuterCore} */
BorderMotionPathOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathOuterCore} */
function BorderMotionPathOuterCoreClass(){}


BorderMotionPathOuterCore[$implementations]=[
 __BorderMotionPathOuterCore,
 BorderMotionPathOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathOuterCore}*/({
  constructor(){
   mountPins(this.model,'',BorderMotionPathMemoryPQs)

  },
 }),
]

BorderMotionPathCore[$implementations]=[
 BorderMotionPathCoreClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathCore}*/({
  resetCore(){
   this.resetBorderMotionPathCore()
  },
  resetBorderMotionPathCore(){
   BorderMotionPathOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.BorderMotionPathOuterCore}*/(
     /**@type {!xyz.swapee.wc.IBorderMotionPathOuterCore}*/(this)),
   )
  },
 }),
 __BorderMotionPathCore,
 BorderMotionPathOuterCore,
]

export {BorderMotionPathCore}