import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathControllerAR}
 */
function __AbstractBorderMotionPathControllerAR() {}
__AbstractBorderMotionPathControllerAR.prototype = /** @type {!_AbstractBorderMotionPathControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR}
 */
class _AbstractBorderMotionPathControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IBorderMotionPathControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR} ‎
 */
class AbstractBorderMotionPathControllerAR extends newAbstract(
 _AbstractBorderMotionPathControllerAR,963146664722,null,{
  asIBorderMotionPathControllerAR:1,
  superBorderMotionPathControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR} */
AbstractBorderMotionPathControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR} */
function AbstractBorderMotionPathControllerARClass(){}

export default AbstractBorderMotionPathControllerAR


AbstractBorderMotionPathControllerAR[$implementations]=[
 __AbstractBorderMotionPathControllerAR,
 AR,
 AbstractBorderMotionPathControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IBorderMotionPathControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]