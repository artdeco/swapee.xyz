import BorderMotionPathScreenAspectsInstaller from '../AbstractBorderMotionPathScreen/aspects-installers/BorderMotionPathScreenAspectsInstaller'
import {SetInputs} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperBorderMotionPathScreen}
 */
function __AbstractHyperBorderMotionPathScreen() {}
__AbstractHyperBorderMotionPathScreen.prototype = /** @type {!_AbstractHyperBorderMotionPathScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractHyperBorderMotionPathScreen}
 */
class _AbstractHyperBorderMotionPathScreen { }
/**
 * The hyper screen with aspects.
 * @extends {xyz.swapee.wc.AbstractHyperBorderMotionPathScreen} ‎
 */
class AbstractHyperBorderMotionPathScreen extends newAbstract(
 _AbstractHyperBorderMotionPathScreen,963146664732,null,{
  asIHyperBorderMotionPathScreen:BorderMotionPathScreenAspectsInstaller,
  superHyperBorderMotionPathScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractHyperBorderMotionPathScreen} */
AbstractHyperBorderMotionPathScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractHyperBorderMotionPathScreen} */
function AbstractHyperBorderMotionPathScreenClass(){}

export default AbstractHyperBorderMotionPathScreen


AbstractHyperBorderMotionPathScreen[$implementations]=[
 __AbstractHyperBorderMotionPathScreen,
]
AbstractHyperBorderMotionPathScreen[$implementations]=[
 BorderMotionPathScreenAspectsInstaller,
 /**@type {!xyz.swapee.wc.IBorderMotionPathScreenAspects}*/(/**@type {!xyz.swapee.wc.IBorderMotionPathScreenAspectsPointcuts} */({
  afterSyncPathReturns: SetInputs,
 })),
]