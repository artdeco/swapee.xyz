import AbstractBorderMotionPathControllerAR from '../AbstractBorderMotionPathControllerAR'
import {AbstractBorderMotionPathController} from '../AbstractBorderMotionPathController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathControllerBack}
 */
function __AbstractBorderMotionPathControllerBack() {}
__AbstractBorderMotionPathControllerBack.prototype = /** @type {!_AbstractBorderMotionPathControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathController}
 */
class _AbstractBorderMotionPathControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathController} ‎
 */
class AbstractBorderMotionPathControllerBack extends newAbstract(
 _AbstractBorderMotionPathControllerBack,963146664721,null,{
  asIBorderMotionPathController:1,
  superBorderMotionPathController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathController} */
AbstractBorderMotionPathControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathController} */
function AbstractBorderMotionPathControllerBackClass(){}

export default AbstractBorderMotionPathControllerBack


AbstractBorderMotionPathControllerBack[$implementations]=[
 __AbstractBorderMotionPathControllerBack,
 AbstractBorderMotionPathController,
 AbstractBorderMotionPathControllerAR,
 DriverBack,
]