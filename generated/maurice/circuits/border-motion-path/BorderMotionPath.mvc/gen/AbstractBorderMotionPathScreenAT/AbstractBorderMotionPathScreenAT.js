import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathScreenAT}
 */
function __AbstractBorderMotionPathScreenAT() {}
__AbstractBorderMotionPathScreenAT.prototype = /** @type {!_AbstractBorderMotionPathScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT}
 */
class _AbstractBorderMotionPathScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IBorderMotionPathScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT} ‎
 */
class AbstractBorderMotionPathScreenAT extends newAbstract(
 _AbstractBorderMotionPathScreenAT,963146664727,null,{
  asIBorderMotionPathScreenAT:1,
  superBorderMotionPathScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT} */
AbstractBorderMotionPathScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT} */
function AbstractBorderMotionPathScreenATClass(){}

export default AbstractBorderMotionPathScreenAT


AbstractBorderMotionPathScreenAT[$implementations]=[
 __AbstractBorderMotionPathScreenAT,
 UartUniversal,
]