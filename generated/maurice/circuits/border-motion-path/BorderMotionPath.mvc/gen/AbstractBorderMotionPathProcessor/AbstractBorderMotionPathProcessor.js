import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathProcessor}
 */
function __AbstractBorderMotionPathProcessor() {}
__AbstractBorderMotionPathProcessor.prototype = /** @type {!_AbstractBorderMotionPathProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathProcessor}
 */
class _AbstractBorderMotionPathProcessor { }
/**
 * The processor to compute changes to the memory for the _IBorderMotionPath_.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathProcessor} ‎
 */
class AbstractBorderMotionPathProcessor extends newAbstract(
 _AbstractBorderMotionPathProcessor,96314666478,null,{
  asIBorderMotionPathProcessor:1,
  superBorderMotionPathProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathProcessor} */
AbstractBorderMotionPathProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathProcessor} */
function AbstractBorderMotionPathProcessorClass(){}

export default AbstractBorderMotionPathProcessor


AbstractBorderMotionPathProcessor[$implementations]=[
 __AbstractBorderMotionPathProcessor,
]