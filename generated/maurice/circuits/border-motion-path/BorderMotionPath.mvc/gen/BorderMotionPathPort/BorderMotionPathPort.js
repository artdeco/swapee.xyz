import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {BorderMotionPathInputsPQs} from '../../pqs/BorderMotionPathInputsPQs'
import {BorderMotionPathOuterCoreConstructor} from '../BorderMotionPathCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_BorderMotionPathPort}
 */
function __BorderMotionPathPort() {}
__BorderMotionPathPort.prototype = /** @type {!_BorderMotionPathPort} */ ({ })
/** @this {xyz.swapee.wc.BorderMotionPathPort} */ function BorderMotionPathPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.BorderMotionPathOuterCore} */ ({model:null})
  BorderMotionPathOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathPort}
 */
class _BorderMotionPathPort { }
/**
 * The port that serves as an interface to the _IBorderMotionPath_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathPort} ‎
 */
export class BorderMotionPathPort extends newAbstract(
 _BorderMotionPathPort,96314666475,BorderMotionPathPortConstructor,{
  asIBorderMotionPathPort:1,
  superBorderMotionPathPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathPort} */
BorderMotionPathPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathPort} */
function BorderMotionPathPortClass(){}

export const BorderMotionPathPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IBorderMotionPath.Pinout>}*/({
 get Port() { return BorderMotionPathPort },
})

BorderMotionPathPort[$implementations]=[
 BorderMotionPathPortClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathPort}*/({
  resetPort(){
   this.resetBorderMotionPathPort()
  },
  resetBorderMotionPathPort(){
   BorderMotionPathPortConstructor.call(this)
  },
 }),
 __BorderMotionPathPort,
 Parametric,
 BorderMotionPathPortClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathPort}*/({
  constructor(){
   mountPins(this.inputs,'',BorderMotionPathInputsPQs)
  },
 }),
]


export default BorderMotionPathPort