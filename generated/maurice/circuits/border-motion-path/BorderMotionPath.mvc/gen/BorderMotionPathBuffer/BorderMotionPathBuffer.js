import {makeBuffers} from '@webcircuits/webcircuits'

export const BorderMotionPathBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  gap:Number,
  count:Number,
  borderRadius:Number,
  delay:Number,
  path:String,
  svgPath:String,
 }),
})

export default BorderMotionPathBuffer