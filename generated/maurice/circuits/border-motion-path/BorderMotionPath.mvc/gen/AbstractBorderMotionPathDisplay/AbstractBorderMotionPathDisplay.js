import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathDisplay}
 */
function __AbstractBorderMotionPathDisplay() {}
__AbstractBorderMotionPathDisplay.prototype = /** @type {!_AbstractBorderMotionPathDisplay} */ ({ })
/** @this {xyz.swapee.wc.BorderMotionPathDisplay} */ function BorderMotionPathDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.ParticlesWr=null
  /** @type {HTMLDivElement} */ this.Path=null
  /** @type {HTMLElement} */ this.ClientRect=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathDisplay}
 */
class _AbstractBorderMotionPathDisplay { }
/**
 * Display for presenting information from the _IBorderMotionPath_.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathDisplay} ‎
 */
class AbstractBorderMotionPathDisplay extends newAbstract(
 _AbstractBorderMotionPathDisplay,963146664716,BorderMotionPathDisplayConstructor,{
  asIBorderMotionPathDisplay:1,
  superBorderMotionPathDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathDisplay} */
AbstractBorderMotionPathDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathDisplay} */
function AbstractBorderMotionPathDisplayClass(){}

export default AbstractBorderMotionPathDisplay


AbstractBorderMotionPathDisplay[$implementations]=[
 __AbstractBorderMotionPathDisplay,
 Display,
 AbstractBorderMotionPathDisplayClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIBorderMotionPathScreen:{vdusPQs:{
    Path:Path,
    ParticlesWr:ParticlesWr,
    ClientRect:ClientRect,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    Path:/**@type {HTMLDivElement}*/(children[Path]),
    ParticlesWr:/**@type {HTMLDivElement}*/(children[ParticlesWr]),
    ClientRect:/**@type {HTMLElement}*/(children[ClientRect]),
   })
  },
 }),
 AbstractBorderMotionPathDisplayClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathDisplay}*/({
  vars:{
   varPath:'d6fe1',
  },
 }),
 /** @type {!xyz.swapee.wc.BorderMotionPathDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IBorderMotionPathDisplay.Initialese}*/({
   ParticlesWr:1,
   Path:1,
   ClientRect:1,
  }),
  initializer({
   ParticlesWr:_ParticlesWr,
   Path:_Path,
   ClientRect:_ClientRect,
  }) {
   if(_ParticlesWr!==undefined) this.ParticlesWr=_ParticlesWr
   if(_Path!==undefined) this.Path=_Path
   if(_ClientRect!==undefined) this.ClientRect=_ClientRect
  },
 }),
]