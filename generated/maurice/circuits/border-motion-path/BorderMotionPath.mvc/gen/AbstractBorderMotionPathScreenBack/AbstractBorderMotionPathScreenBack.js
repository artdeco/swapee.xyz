import AbstractBorderMotionPathScreenAT from '../AbstractBorderMotionPathScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathScreenBack}
 */
function __AbstractBorderMotionPathScreenBack() {}
__AbstractBorderMotionPathScreenBack.prototype = /** @type {!_AbstractBorderMotionPathScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathScreen}
 */
class _AbstractBorderMotionPathScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractBorderMotionPathScreen} ‎
 */
class AbstractBorderMotionPathScreenBack extends newAbstract(
 _AbstractBorderMotionPathScreenBack,963146664725,null,{
  asIBorderMotionPathScreen:1,
  superBorderMotionPathScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreen} */
AbstractBorderMotionPathScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreen} */
function AbstractBorderMotionPathScreenBackClass(){}

export default AbstractBorderMotionPathScreenBack


AbstractBorderMotionPathScreenBack[$implementations]=[
 __AbstractBorderMotionPathScreenBack,
 AbstractBorderMotionPathScreenAT,
]