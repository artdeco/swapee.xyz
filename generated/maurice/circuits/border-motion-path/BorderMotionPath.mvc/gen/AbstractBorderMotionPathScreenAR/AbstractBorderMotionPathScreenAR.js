import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathScreenAR}
 */
function __AbstractBorderMotionPathScreenAR() {}
__AbstractBorderMotionPathScreenAR.prototype = /** @type {!_AbstractBorderMotionPathScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR}
 */
class _AbstractBorderMotionPathScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IBorderMotionPathScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR} ‎
 */
class AbstractBorderMotionPathScreenAR extends newAbstract(
 _AbstractBorderMotionPathScreenAR,963146664726,null,{
  asIBorderMotionPathScreenAR:1,
  superBorderMotionPathScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR} */
AbstractBorderMotionPathScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR} */
function AbstractBorderMotionPathScreenARClass(){}

export default AbstractBorderMotionPathScreenAR


AbstractBorderMotionPathScreenAR[$implementations]=[
 __AbstractBorderMotionPathScreenAR,
 AR,
 AbstractBorderMotionPathScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IBorderMotionPathScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractBorderMotionPathScreenAR}