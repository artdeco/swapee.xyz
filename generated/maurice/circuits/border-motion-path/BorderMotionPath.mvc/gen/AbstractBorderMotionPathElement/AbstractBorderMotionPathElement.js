import BorderMotionPathRenderVdus from './methods/render-vdus'
import BorderMotionPathElementPort from '../BorderMotionPathElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {BorderMotionPathInputsPQs} from '../../pqs/BorderMotionPathInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractBorderMotionPath from '../AbstractBorderMotionPath'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractBorderMotionPathElement}
 */
function __AbstractBorderMotionPathElement() {}
__AbstractBorderMotionPathElement.prototype = /** @type {!_AbstractBorderMotionPathElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathElement}
 */
class _AbstractBorderMotionPathElement { }
/**
 * A component description.
 *
 * The _IBorderMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathElement} ‎
 */
class AbstractBorderMotionPathElement extends newAbstract(
 _AbstractBorderMotionPathElement,963146664712,null,{
  asIBorderMotionPathElement:1,
  superBorderMotionPathElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathElement} */
AbstractBorderMotionPathElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathElement} */
function AbstractBorderMotionPathElementClass(){}

export default AbstractBorderMotionPathElement


AbstractBorderMotionPathElement[$implementations]=[
 __AbstractBorderMotionPathElement,
 ElementBase,
 AbstractBorderMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':gap':gapColAttr,
   ':count':countColAttr,
   ':border-radius':borderRadiusColAttr,
   ':delay':delayColAttr,
   ':path':pathColAttr,
   ':svg-path':svgPathColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'gap':gapAttr,
    'count':countAttr,
    'border-radius':borderRadiusAttr,
    'delay':delayAttr,
    'path':pathAttr,
    'svg-path':svgPathAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(gapAttr===undefined?{'gap':gapColAttr}:{}),
    ...(countAttr===undefined?{'count':countColAttr}:{}),
    ...(borderRadiusAttr===undefined?{'border-radius':borderRadiusColAttr}:{}),
    ...(delayAttr===undefined?{'delay':delayColAttr}:{}),
    ...(pathAttr===undefined?{'path':pathColAttr}:{}),
    ...(svgPathAttr===undefined?{'svg-path':svgPathColAttr}:{}),
   }
  },
 }),
 AbstractBorderMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'gap':gapAttr,
   'count':countAttr,
   'border-radius':borderRadiusAttr,
   'delay':delayAttr,
   'path':pathAttr,
   'svg-path':svgPathAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    gap:gapAttr,
    count:countAttr,
    borderRadius:borderRadiusAttr,
    delay:delayAttr,
    path:pathAttr,
    svgPath:svgPathAttr,
   }
  },
 }),
 AbstractBorderMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractBorderMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
  render:BorderMotionPathRenderVdus,
 }),
 AbstractBorderMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
  classes:{
   'Ready': 'e7d31',
  },
  customs:{
   'COLOR': '#a70dfe',
   'DIRECTION': 'normal',
   'PARTICLE_SIZE': 3,
  },
  inputsPQs:BorderMotionPathInputsPQs,
  vdus:{
   'ClientRect': 'd27d1',
   'ParticlesWr': 'd27d2',
   'Path': 'd27d3',
  },
  _vars_:{
   'path':'d6fe1',
  },
 }),
 IntegratedComponent,
 AbstractBorderMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','gap','count','borderRadius','delay','path','svgPath','no-solder',':no-solder',':gap',':count','border-radius',':border-radius',':delay',':path','svg-path',':svg-path','fe646','7cbd8','4ead1','37c50','df9bc','e2942','3f844','7243f','d6fe1','a1308','COLOR','DIRECTION','PARTICLE_SIZE','children']),
   })
  },
  get Port(){
   return BorderMotionPathElementPort
  },
 }),
 Landed,
 AbstractBorderMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
  constructor(){
   this.land={
    ClientRect:null,
   }
  },
 }),
]



AbstractBorderMotionPathElement[$implementations]=[AbstractBorderMotionPath,
 /** @type {!AbstractBorderMotionPathElement} */ ({
  rootId:'BorderMotionPath',
  __$id:9631466647,
  fqn:'xyz.swapee.wc.IBorderMotionPath',
  maurice_element_v3:true,
 }),
]