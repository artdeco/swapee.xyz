
import AbstractBorderMotionPath from '../AbstractBorderMotionPath'

/** @abstract {xyz.swapee.wc.IBorderMotionPathElement} */
export default class AbstractBorderMotionPathElement { }



AbstractBorderMotionPathElement[$implementations]=[AbstractBorderMotionPath,
 /** @type {!AbstractBorderMotionPathElement} */ ({
  rootId:'BorderMotionPath',
  __$id:9631466647,
  fqn:'xyz.swapee.wc.IBorderMotionPath',
  maurice_element_v3:true,
 }),
]