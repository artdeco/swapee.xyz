import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {9631466647} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=d["372700389810"],e=d["372700389811"];function f(a,b,c,p){return d["372700389812"](a,b,c,p,!1,void 0)};function g(){}g.prototype={};class ba{}class h extends f(ba,96314666478,null,{K:1,Z:2}){}h[e]=[g];
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=k["61505580523"],da=k["615055805212"],ea=k["615055805218"],fa=k["615055805221"],ha=k["615055805223"],l=k["615055805233"],ia=k["615055805235"];const m={l:"df9bc",count:"e2942",path:"d6fe1",borderRadius:"3f844",j:"a1308",ready:"b2fda",delay:"7243f"};function n(){}n.prototype={};class ja{}class q extends f(ja,96314666477,null,{F:1,T:2}){}function r(){}r.prototype={};function t(){this.model={l:.01,count:12,borderRadius:10,delay:0,path:"",j:""}}class ka{}class u extends f(ka,96314666473,t,{I:1,X:2}){}u[e]=[r,{constructor(){l(this.model,"",m)}}];q[e]=[{},n,u];

const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=v.IntegratedController,ma=v.Parametric;
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const na=w.IntegratedComponentInitialiser,oa=w.IntegratedComponent,pa=w["38"];function x(){}x.prototype={};class qa{}class y extends f(qa,96314666471,null,{C:1,P:2}){}y[e]=[x,ea];const z={regulate:da({l:Number,count:Number,borderRadius:Number,delay:Number,path:String,j:String})};const A={...m};function B(){}B.prototype={};function ra(){const a={model:null};t.call(a);this.inputs=a.model}class sa{}class C extends f(sa,96314666475,ra,{J:1,Y:2}){}function D(){}C[e]=[D.prototype={},B,ma,D.prototype={constructor(){l(this.inputs,"",A)}}];function E(){}E.prototype={};class ta{}class F extends f(ta,963146664719,null,{s:1,u:2}){}F[e]=[{},E,z,la,{get Port(){return C}}];function G(){}G.prototype={};class ua{}class H extends f(ua,96314666479,null,{A:1,O:2}){}H[e]=[G,q,h,oa,y,F];
const I=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const va=I["12817393923"],wa=I["12817393924"],xa=I["12817393925"],ya=I["12817393926"];function J(){}J.prototype={};class za{}class K extends f(za,963146664722,null,{D:1,R:2}){}K[e]=[J,ya,{allocator(){this.methods={}}}];function L(){}L.prototype={};class Aa{}class M extends f(Aa,963146664721,null,{s:1,u:2}){}M[e]=[L,F,K,va];var N=class extends M.implements(){};function Ba({width:a,height:b,borderRadius:c}){a=\`
M\${1},\${c}
L\${1},\${b-c}
A\${c},\${c} 0 0,0 \${c},\${b-1}
L\${a-c},\${b-1}
L\${a-1},\${b-c}
L\${a-1},\${c}
A\${c},\${c} 0 0,0 \${a-c},\${1}
L\${c},\${1} Z\`.replace(/\\n+/g," ");return{j:a,path:\`path('\${a}')\`}};function Ca(a,b,c){a={width:this.land.ClientRect?this.land.ClientRect.model.eaae2:void 0,height:this.land.ClientRect?this.land.ClientRect.model.b435e:void 0,borderRadius:a.borderRadius};a=c?c(a):a;if(![null,void 0].includes(a.width)&&![null,void 0].includes(a.height))return b=c?c(b):b,this.o(a,b)};class O extends y.implements({o:Ba,adapt:[Ca]}){};function P(){}P.prototype={};class Da{}class Q extends f(Da,963146664718,null,{G:1,U:2}){}Q[e]=[P,wa,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{g:a,h:a,ClientRect:a})}},{[aa]:{h:1,g:1,ClientRect:1},initializer({h:a,g:b,ClientRect:c}){void 0!==a&&(this.h=a);void 0!==b&&(this.g=b);void 0!==c&&(this.ClientRect=c)}}];const R={v:"5e932",m:"e7d31"};const Ea=Object.keys(R).reduce((a,b)=>{a[R[b]]=b;return a},{});const S={ClientRect:"d27d1",h:"d27d2",g:"d27d3"};const Fa=Object.keys(S).reduce((a,b)=>{a[S[b]]=b;return a},{});function T(){}T.prototype={};class Ga{}class U extends f(Ga,963146664715,null,{i:1,V:2}){}function V(){}U[e]=[T,V.prototype={classesQPs:Ea,vdusQPs:Fa,memoryPQs:m},Q,ca,V.prototype={allocator(){pa(this.classes,"",R)}}];function W(){}W.prototype={};class Ha{}class X extends f(Ha,963146664727,null,{M:1,aa:2}){}X[e]=[W,xa];function Y(){}Y.prototype={};class Ia{}class Ja extends f(Ia,963146664725,null,{L:1,$:2}){}Ja[e]=[Y,X];const Ka=Object.keys(A).reduce((a,b)=>{a[A[b]]=b;return a},{});function La(){}La.prototype={};class Ma{static mvc(a,b,c){return ha(this,a,b,null,c)}}class Na extends f(Ma,963146664711,null,{H:1,W:2}){}function Z(){}
Na[e]=[La,fa,H,U,Ja,ia,Z.prototype={constructor(){this.land={ClientRect:null}}},Z.prototype={deduceProps(){return{}}},Z.prototype={inputsQPs:Ka},Z.prototype={paint:function({j:a}){const {i:{g:b},asIBrowserView:{attr:c}}=this;c(b,"d",a)}},Z.prototype={paint:function({path:a}){const {i:{element:b},classes:{m:c},asIBrowserView:{addClass:p,removeClass:Oa}}=this;a?p(b,c):Oa(b,c)}},Z.prototype={paint:function({path:a}){const {i:{element:b},asIBrowserView:{style:c}}=this;c(b,"--d6fe1",a)}},Z.prototype={scheduleTwo:function(){const {asIHtmlComponent:{build:a},
i:{ClientRect:b}}=this;a(3715284221,{ClientRect:b})}}];var Pa=class extends Na.implements(N,O,na){};module.exports["96314666470"]=H;module.exports["96314666471"]=H;module.exports["96314666473"]=C;module.exports["96314666474"]=F;module.exports["963146664710"]=Pa;module.exports["963146664711"]=z;module.exports["963146664730"]=y;module.exports["963146664731"]=O;module.exports["963146664761"]=N;
/*! @embed-object-end {9631466647} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule