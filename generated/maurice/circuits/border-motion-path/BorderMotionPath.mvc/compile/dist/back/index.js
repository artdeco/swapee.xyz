/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPath` interface.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPath}
 */
class AbstractBorderMotionPath extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IBorderMotionPath_, providing input
 * pins.
 * @extends {xyz.swapee.wc.BorderMotionPathPort}
 */
class BorderMotionPathPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathController` interface.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathController}
 */
class AbstractBorderMotionPathController extends (class {/* lazy-loaded */}) {}
/**
 * The _IBorderMotionPath_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.BorderMotionPathHtmlComponent}
 */
class BorderMotionPathHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.BorderMotionPathBuffer}
 */
class BorderMotionPathBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathComputer` interface.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathComputer}
 */
class AbstractBorderMotionPathComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.BorderMotionPathComputer}
 */
class BorderMotionPathComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.BorderMotionPathController}
 */
class BorderMotionPathController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractBorderMotionPath = AbstractBorderMotionPath
module.exports.BorderMotionPathPort = BorderMotionPathPort
module.exports.AbstractBorderMotionPathController = AbstractBorderMotionPathController
module.exports.BorderMotionPathHtmlComponent = BorderMotionPathHtmlComponent
module.exports.BorderMotionPathBuffer = BorderMotionPathBuffer
module.exports.AbstractBorderMotionPathComputer = AbstractBorderMotionPathComputer
module.exports.BorderMotionPathComputer = BorderMotionPathComputer
module.exports.BorderMotionPathController = BorderMotionPathController