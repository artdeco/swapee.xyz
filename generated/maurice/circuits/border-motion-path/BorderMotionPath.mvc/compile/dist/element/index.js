/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPath` interface.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPath}
 */
class AbstractBorderMotionPath extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IBorderMotionPath_, providing input
 * pins.
 * @extends {xyz.swapee.wc.BorderMotionPathPort}
 */
class BorderMotionPathPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathController` interface.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathController}
 */
class AbstractBorderMotionPathController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IBorderMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.BorderMotionPathElement}
 */
class BorderMotionPathElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.BorderMotionPathBuffer}
 */
class BorderMotionPathBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathComputer` interface.
 * @extends {xyz.swapee.wc.AbstractBorderMotionPathComputer}
 */
class AbstractBorderMotionPathComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.BorderMotionPathController}
 */
class BorderMotionPathController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractBorderMotionPath = AbstractBorderMotionPath
module.exports.BorderMotionPathPort = BorderMotionPathPort
module.exports.AbstractBorderMotionPathController = AbstractBorderMotionPathController
module.exports.BorderMotionPathElement = BorderMotionPathElement
module.exports.BorderMotionPathBuffer = BorderMotionPathBuffer
module.exports.AbstractBorderMotionPathComputer = AbstractBorderMotionPathComputer
module.exports.BorderMotionPathController = BorderMotionPathController

Object.defineProperties(module.exports, {
 'AbstractBorderMotionPath': {get: () => require('./precompile/internal')[96314666471]},
 [96314666471]: {get: () => module.exports['AbstractBorderMotionPath']},
 'BorderMotionPathPort': {get: () => require('./precompile/internal')[96314666473]},
 [96314666473]: {get: () => module.exports['BorderMotionPathPort']},
 'AbstractBorderMotionPathController': {get: () => require('./precompile/internal')[96314666474]},
 [96314666474]: {get: () => module.exports['AbstractBorderMotionPathController']},
 'BorderMotionPathElement': {get: () => require('./precompile/internal')[96314666478]},
 [96314666478]: {get: () => module.exports['BorderMotionPathElement']},
 'BorderMotionPathBuffer': {get: () => require('./precompile/internal')[963146664711]},
 [963146664711]: {get: () => module.exports['BorderMotionPathBuffer']},
 'AbstractBorderMotionPathComputer': {get: () => require('./precompile/internal')[963146664730]},
 [963146664730]: {get: () => module.exports['AbstractBorderMotionPathComputer']},
 'BorderMotionPathController': {get: () => require('./precompile/internal')[963146664761]},
 [963146664761]: {get: () => module.exports['BorderMotionPathController']},
})