import Module from './element'

/**@extends {xyz.swapee.wc.AbstractBorderMotionPath}*/
export class AbstractBorderMotionPath extends Module['96314666471'] {}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPath} */
AbstractBorderMotionPath.class=function(){}
/** @type {typeof xyz.swapee.wc.BorderMotionPathPort} */
export const BorderMotionPathPort=Module['96314666473']
/**@extends {xyz.swapee.wc.AbstractBorderMotionPathController}*/
export class AbstractBorderMotionPathController extends Module['96314666474'] {}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathController} */
AbstractBorderMotionPathController.class=function(){}
/** @type {typeof xyz.swapee.wc.BorderMotionPathElement} */
export const BorderMotionPathElement=Module['96314666478']
/** @type {typeof xyz.swapee.wc.BorderMotionPathBuffer} */
export const BorderMotionPathBuffer=Module['963146664711']
/**@extends {xyz.swapee.wc.AbstractBorderMotionPathComputer}*/
export class AbstractBorderMotionPathComputer extends Module['963146664730'] {}
/** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathComputer} */
AbstractBorderMotionPathComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.BorderMotionPathController} */
export const BorderMotionPathController=Module['963146664761']