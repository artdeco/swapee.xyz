/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function g(a,b,e,f){return c["372700389812"](a,b,e,f,!1,void 0)};function k(){}k.prototype={};class l{}class m extends g(l,96314666478,null,{D:1,V:2}){}m[d]=[k];

const n=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const q=n["615055805212"],r=n["615055805218"],t=n["615055805233"],u=n["615055805235"];const v={g:"df9bc",count:"e2942",path:"d6fe1",borderRadius:"3f844",i:"a1308",ready:"b2fda",delay:"7243f"};function x(){}x.prototype={};class aa{}class y extends g(aa,96314666477,null,{o:1,M:2}){}function z(){}z.prototype={};function A(){this.model={g:.01,count:12,borderRadius:10,delay:0,path:"",i:""}}class ba{}class B extends g(ba,96314666473,A,{v:1,T:2}){}B[d]=[z,{constructor(){t(this.model,"",v)}}];y[d]=[{},x,B];
const C=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ca=C.IntegratedComponentInitialiser,D=C.IntegratedComponent,da=C["95173443851"];function E(){}E.prototype={};class ea{}class F extends g(ea,96314666471,null,{l:1,J:2}){}F[d]=[E,r];const G={regulate:q({g:Number,count:Number,borderRadius:Number,delay:Number,path:String,i:String})};
const H=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const fa=H.IntegratedController,ha=H.Parametric;const I={...v};function J(){}J.prototype={};function ia(){const a={model:null};A.call(a);this.inputs=a.model}class ja{}class K extends g(ja,96314666475,ia,{A:1,U:2}){}function L(){}K[d]=[L.prototype={},J,ha,L.prototype={constructor(){t(this.inputs,"",I)}}];function M(){}M.prototype={};class ka{}class N extends g(ka,963146664719,null,{m:1,K:2}){}N[d]=[{},M,G,fa,{get Port(){return K}}];function O(){}O.prototype={};class la{}class P extends g(la,96314666479,null,{j:1,I:2}){}P[d]=[O,y,m,D,F,N];function ma({borderRadius:a}){return{borderRadius:a}};const Q=require(eval('"@type.engineering/web-computing"')).h;function na({g:a,count:b,delay:e}){const f=1/b;return Q("div",{$id:"MotionPath"},Q("div",{$id:"ParticlesWr"},Array.from({length:b}).map((p,h)=>Q("div",{ParticleWr:!0,"animation-delay":(a*h-e).toFixed(3).replace(/\.?0+$/,"")+"s"},Q("div",{Particle:!0,opacity:(1-f*h).toFixed(1)})))))};const R=require(eval('"@type.engineering/web-computing"')).h;function oa({i:a,path:b}){return R("div",{$id:"BorderMotionPath","$-path":b,Ready:!!b},R("path",{$id:"Path",d:a}))};var S=class extends N.implements(){};require("https");require("http");const pa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}pa("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const qa=T.ElementBase,ra=T.HTMLBlocker;const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function sa(){this.inputs={noSolder:!1,G:{},H:{},F:{}}}class ta{}class W extends g(ta,963146664713,sa,{u:1,R:2}){}W[d]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"particles-wr-opts":void 0,"path-opts":void 0,"client-rect-opts":void 0,"border-radius":void 0,"svg-path":void 0})}}];function X(){}X.prototype={};class ua{}class Y extends g(ua,963146664712,null,{s:1,P:2}){}function Z(){}
Y[d]=[X,qa,Z.prototype={calibrate:function({":no-solder":a,":gap":b,":count":e,":border-radius":f,":delay":p,":path":h,":svg-path":w}){const {attributes:{"no-solder":va,gap:wa,count:xa,"border-radius":ya,delay:za,path:Aa,"svg-path":Ba}}=this;return{...(void 0===va?{"no-solder":a}:{}),...(void 0===wa?{gap:b}:{}),...(void 0===xa?{count:e}:{}),...(void 0===ya?{"border-radius":f}:{}),...(void 0===za?{delay:p}:{}),...(void 0===Aa?{path:h}:{}),...(void 0===Ba?{"svg-path":w}:{})}}},Z.prototype={calibrate:({"no-solder":a,
gap:b,count:e,"border-radius":f,delay:p,path:h,"svg-path":w})=>({noSolder:a,g:b,count:e,borderRadius:f,delay:p,path:h,i:w})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return U("div",{$id:"BorderMotionPath"},U("vdu",{$id:"Path"}),U("vdu",{$id:"ParticlesWr"}),U("vdu",{$id:"ClientRect"}))}},Z.prototype={classes:{Ready:"e7d31"},customs:{COLOR:"#a70dfe",DIRECTION:"normal",PARTICLE_SIZE:3},inputsPQs:I,vdus:{ClientRect:"d27d1",ParticlesWr:"d27d2",Path:"d27d3"},
_vars_:{path:"d6fe1"}},D,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder gap count borderRadius delay path svgPath no-solder :no-solder :gap :count border-radius :border-radius :delay :path svg-path :svg-path fe646 7cbd8 4ead1 37c50 df9bc e2942 3f844 7243f d6fe1 a1308 COLOR DIRECTION PARTICLE_SIZE children".split(" "))})},get Port(){return W}},u,Z.prototype={constructor(){this.land={ClientRect:null}}}];
Y[d]=[P,{rootId:"BorderMotionPath",__$id:9631466647,fqn:"xyz.swapee.wc.IBorderMotionPath",maurice_element_v3:!0}];class Ca extends Y.implements(S,da,ra,ca,{solder:ma,server:na,render:oa},{classesMap:!0,rootSelector:".BorderMotionPath",stylesheet:"html/styles/BorderMotionPath.css",blockName:"html/BorderMotionPathBlock.html"}){};module.exports["96314666470"]=P;module.exports["96314666471"]=P;module.exports["96314666473"]=K;module.exports["96314666474"]=N;module.exports["96314666478"]=Ca;module.exports["963146664711"]=G;module.exports["963146664730"]=F;module.exports["963146664761"]=S;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['9631466647']=module.exports