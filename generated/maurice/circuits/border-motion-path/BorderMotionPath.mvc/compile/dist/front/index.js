/**
 * Display for presenting information from the _IBorderMotionPath_.
 * @extends {xyz.swapee.wc.BorderMotionPathDisplay}
 */
class BorderMotionPathDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.BorderMotionPathScreen}
 */
class BorderMotionPathScreen extends (class {/* lazy-loaded */}) {}

module.exports.BorderMotionPathDisplay = BorderMotionPathDisplay
module.exports.BorderMotionPathScreen = BorderMotionPathScreen