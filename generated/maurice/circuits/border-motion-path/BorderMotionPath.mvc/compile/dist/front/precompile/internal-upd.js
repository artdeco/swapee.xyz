import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {9631466647} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const f=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const g=f["37270038985"],h=f["372700389810"],k=f["372700389811"];function n(a,b,c,l){return f["372700389812"](a,b,c,l,!1,void 0)};
const p=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const q=p["61893096584"],r=p["61893096586"],t=p["618930965811"],u=p["618930965812"],v=p["618930965815"];function w(){}w.prototype={};function x(){this.ClientRect=this.h=this.g=null}class y{}class z extends n(y,963146664716,x,{l:1,u:2}){}function A(){}
z[k]=[w,q,A.prototype={constructor(){g(this,()=>{this.scan({})})},scan:function(){const {element:a,i:{vdusPQs:{h:b,g:c,ClientRect:l}}}=this,m=v(a);Object.assign(this,{h:m[b],g:m[c],ClientRect:m[l]})}},A.prototype={},{[h]:{g:1,h:1,ClientRect:1},initializer({g:a,h:b,ClientRect:c}){void 0!==a&&(this.g=a);void 0!==b&&(this.h=b);void 0!==c&&(this.ClientRect=c)}}];var B=class extends z.implements(){};function C(){}C.prototype={};class D{}class E extends n(D,963146664723,null,{j:1,s:2}){}E[k]=[C,t,{}];function F(){}F.prototype={};class G{}class H extends n(G,963146664726,null,{m:1,A:2}){}H[k]=[F,u,{allocator(){this.methods={}}}];const I={o:"df9bc",count:"e2942",path:"d6fe1",borderRadius:"3f844",C:"a1308",ready:"b2fda",delay:"7243f"};const J={...I};const K=Object.keys(I).reduce((a,b)=>{a[I[b]]=b;return a},{});function L(){}L.prototype={};class M{}class N extends n(M,963146664724,null,{i:1,v:2}){}function O(){}N[k]=[L,O.prototype={inputsPQs:J,memoryQPs:K},r,H,O.prototype={vdusPQs:{ClientRect:"d27d1",g:"d27d2",h:"d27d3"}},O.prototype={deduceInputs:a=>({path:a.style.getPropertyValue("--d6fe1")})}];var P=class extends N.implements(E,B,{get queries(){return this.settings}},{__$id:9631466647}){};module.exports["963146664741"]=B;module.exports["963146664771"]=P;
/*! @embed-object-end {9631466647} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule