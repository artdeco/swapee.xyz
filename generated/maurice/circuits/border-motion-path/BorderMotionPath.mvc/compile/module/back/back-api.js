import { AbstractBorderMotionPath, BorderMotionPathPort,
 AbstractBorderMotionPathController, BorderMotionPathHtmlComponent,
 BorderMotionPathBuffer, AbstractBorderMotionPathComputer,
 BorderMotionPathComputer, BorderMotionPathController } from './back-exports'

/** @lazy @api {xyz.swapee.wc.AbstractBorderMotionPath} */
export { AbstractBorderMotionPath }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathPort} */
export { BorderMotionPathPort }
/** @lazy @api {xyz.swapee.wc.AbstractBorderMotionPathController} */
export { AbstractBorderMotionPathController }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathHtmlComponent} */
export { BorderMotionPathHtmlComponent }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathBuffer} */
export { BorderMotionPathBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractBorderMotionPathComputer} */
export { AbstractBorderMotionPathComputer }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathComputer} */
export { BorderMotionPathComputer }
/** @lazy @api {xyz.swapee.wc.back.BorderMotionPathController} */
export { BorderMotionPathController }