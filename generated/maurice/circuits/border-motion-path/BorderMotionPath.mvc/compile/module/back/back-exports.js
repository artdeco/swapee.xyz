import AbstractBorderMotionPath from '../../../gen/AbstractBorderMotionPath/AbstractBorderMotionPath'
module.exports['9631466647'+0]=AbstractBorderMotionPath
module.exports['9631466647'+1]=AbstractBorderMotionPath
export {AbstractBorderMotionPath}

import BorderMotionPathPort from '../../../gen/BorderMotionPathPort/BorderMotionPathPort'
module.exports['9631466647'+3]=BorderMotionPathPort
export {BorderMotionPathPort}

import AbstractBorderMotionPathController from '../../../gen/AbstractBorderMotionPathController/AbstractBorderMotionPathController'
module.exports['9631466647'+4]=AbstractBorderMotionPathController
export {AbstractBorderMotionPathController}

import BorderMotionPathHtmlComponent from '../../../src/BorderMotionPathHtmlComponent/BorderMotionPathHtmlComponent'
module.exports['9631466647'+10]=BorderMotionPathHtmlComponent
export {BorderMotionPathHtmlComponent}

import BorderMotionPathBuffer from '../../../gen/BorderMotionPathBuffer/BorderMotionPathBuffer'
module.exports['9631466647'+11]=BorderMotionPathBuffer
export {BorderMotionPathBuffer}

import AbstractBorderMotionPathComputer from '../../../gen/AbstractBorderMotionPathComputer/AbstractBorderMotionPathComputer'
module.exports['9631466647'+30]=AbstractBorderMotionPathComputer
export {AbstractBorderMotionPathComputer}

import BorderMotionPathComputer from '../../../src/BorderMotionPathHtmlComputer/BorderMotionPathComputer'
module.exports['9631466647'+31]=BorderMotionPathComputer
export {BorderMotionPathComputer}

import BorderMotionPathController from '../../../src/BorderMotionPathHtmlController/BorderMotionPathController'
module.exports['9631466647'+61]=BorderMotionPathController
export {BorderMotionPathController}