import { BorderMotionPathDisplay, BorderMotionPathScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.BorderMotionPathDisplay} */
export { BorderMotionPathDisplay }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathScreen} */
export { BorderMotionPathScreen }