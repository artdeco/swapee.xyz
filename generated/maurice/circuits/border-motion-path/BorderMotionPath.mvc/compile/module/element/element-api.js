import { AbstractBorderMotionPath, BorderMotionPathPort,
 AbstractBorderMotionPathController, BorderMotionPathElement,
 BorderMotionPathBuffer, AbstractBorderMotionPathComputer,
 BorderMotionPathController } from './element-exports'

/** @lazy @api {xyz.swapee.wc.AbstractBorderMotionPath} */
export { AbstractBorderMotionPath }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathPort} */
export { BorderMotionPathPort }
/** @lazy @api {xyz.swapee.wc.AbstractBorderMotionPathController} */
export { AbstractBorderMotionPathController }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathElement} */
export { BorderMotionPathElement }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathBuffer} */
export { BorderMotionPathBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractBorderMotionPathComputer} */
export { AbstractBorderMotionPathComputer }
/** @lazy @api {xyz.swapee.wc.BorderMotionPathController} */
export { BorderMotionPathController }