import {BorderMotionPathInputsPQs} from './BorderMotionPathInputsPQs'
export const BorderMotionPathInputsQPs=/**@type {!xyz.swapee.wc.BorderMotionPathInputsQPs}*/(Object.keys(BorderMotionPathInputsPQs)
 .reduce((a,k)=>{a[BorderMotionPathInputsPQs[k]]=k;return a},{}))