import {BorderMotionPathVdusPQs} from './BorderMotionPathVdusPQs'
export const BorderMotionPathVdusQPs=/**@type {!xyz.swapee.wc.BorderMotionPathVdusQPs}*/(Object.keys(BorderMotionPathVdusPQs)
 .reduce((a,k)=>{a[BorderMotionPathVdusPQs[k]]=k;return a},{}))