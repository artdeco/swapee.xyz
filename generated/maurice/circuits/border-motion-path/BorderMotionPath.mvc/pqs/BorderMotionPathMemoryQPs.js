import {BorderMotionPathMemoryPQs} from './BorderMotionPathMemoryPQs'
export const BorderMotionPathMemoryQPs=/**@type {!xyz.swapee.wc.BorderMotionPathMemoryQPs}*/(Object.keys(BorderMotionPathMemoryPQs)
 .reduce((a,k)=>{a[BorderMotionPathMemoryPQs[k]]=k;return a},{}))