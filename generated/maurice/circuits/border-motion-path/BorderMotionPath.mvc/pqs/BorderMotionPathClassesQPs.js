import BorderMotionPathClassesPQs from './BorderMotionPathClassesPQs'
export const BorderMotionPathClassesQPs=/**@type {!xyz.swapee.wc.BorderMotionPathClassesQPs}*/(Object.keys(BorderMotionPathClassesPQs)
 .reduce((a,k)=>{a[BorderMotionPathClassesPQs[k]]=k;return a},{}))