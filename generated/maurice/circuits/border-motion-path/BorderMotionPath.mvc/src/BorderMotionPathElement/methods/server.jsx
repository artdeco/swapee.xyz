/** @type {xyz.swapee.wc.IBorderMotionPathElement._server} */
export default function server({gap:gap,count:count,delay}) {
 const alphaStep=1/count

 return (<div $id="MotionPath">
  <div $id="ParticlesWr">
   {Array.from({length:count}).map((a,i)=>{
    return (<div ParticleWr animation-delay={((gap*i)-delay).toFixed(3).replace(/\.?0+$/,'')+'s'}>
     <div Particle opacity={(1-alphaStep*i).toFixed(1)} />
    </div>)
   })}
  </div>
 </div>)
}