/** @type {xyz.swapee.wc.IBorderMotionPathElement._render} */
export default function BorderMotionPathRender({svgPath,path},{}) {
 return (<div $id="BorderMotionPath" $-path={path} Ready={!!path}>
  <path $id="Path" d={svgPath} />
 </div>)
}