import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import BorderMotionPathServerController from '../BorderMotionPathServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractBorderMotionPathElement from '../../gen/AbstractBorderMotionPathElement'

/** @extends {xyz.swapee.wc.BorderMotionPathElement} */
export default class BorderMotionPathElement extends AbstractBorderMotionPathElement.implements(
 BorderMotionPathServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IBorderMotionPathElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IBorderMotionPathElement}*/({
   classesMap: true,
   rootSelector:     `.BorderMotionPath`,
   stylesheet:       'html/styles/BorderMotionPath.css',
   blockName:        'html/BorderMotionPathBlock.html',
  }),
){}

// thank you for using web circuits
