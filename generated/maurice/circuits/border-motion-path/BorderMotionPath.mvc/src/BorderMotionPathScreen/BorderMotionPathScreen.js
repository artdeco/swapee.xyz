import AbstractBorderMotionPathControllerAT from '../../gen/AbstractBorderMotionPathControllerAT'
import BorderMotionPathDisplay from '../BorderMotionPathDisplay'
import AbstractBorderMotionPathScreen from '../../gen/AbstractBorderMotionPathScreen'

/** @extends {xyz.swapee.wc.BorderMotionPathScreen} */
export default class extends AbstractBorderMotionPathScreen.implements(
 AbstractBorderMotionPathControllerAT,
 BorderMotionPathDisplay,
 /**@type {!xyz.swapee.wc.IBorderMotionPathScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IBorderMotionPathScreen} */ ({
  __$id:9631466647,
 }),
){}