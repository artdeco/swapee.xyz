import adaptPath from './methods/adapt-path'
import {preadaptPath} from '../../gen/AbstractBorderMotionPathComputer/preadapters'
import AbstractBorderMotionPathComputer from '../../gen/AbstractBorderMotionPathComputer'

/** @extends {xyz.swapee.wc.BorderMotionPathComputer} */
export default class BorderMotionPathHtmlComputer extends AbstractBorderMotionPathComputer.implements(
 /** @type {!xyz.swapee.wc.IBorderMotionPathComputer} */ ({
  adaptPath:adaptPath,
  adapt:[preadaptPath],
 }),
){}