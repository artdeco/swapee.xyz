/** @type {xyz.swapee.wc.IBorderMotionPathComputer._adaptPath} */
export default function adaptPath({width:width,height:height,borderRadius:borderRadius}) {
    // A${borderRadius},${borderRadius} 0 0,0 ${width},${height-borderRadius}
    const inset=1
    const svgPath=`
M${0+inset},${borderRadius}
L${0+inset},${height-borderRadius}
A${borderRadius},${borderRadius} 0 0,0 ${borderRadius},${height-inset}
L${width-borderRadius},${height-inset}
L${width-inset},${height-borderRadius}
L${width-inset},${borderRadius}
A${borderRadius},${borderRadius} 0 0,0 ${width-borderRadius},${0+inset}
L${borderRadius},${0+inset} Z`.replace(/\n+/g,' ')
    const path=`path('${svgPath}')`
    return{
     svgPath:svgPath,
     path:path,
    }
   }