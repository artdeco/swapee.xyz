import BorderMotionPathHtmlController from '../BorderMotionPathHtmlController'
import BorderMotionPathHtmlComputer from '../BorderMotionPathHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractBorderMotionPathHtmlComponent} from '../../gen/AbstractBorderMotionPathHtmlComponent'

/** @extends {xyz.swapee.wc.BorderMotionPathHtmlComponent} */
export default class extends AbstractBorderMotionPathHtmlComponent.implements(
 BorderMotionPathHtmlController,
 BorderMotionPathHtmlComputer,
 IntegratedComponentInitialiser,
){}