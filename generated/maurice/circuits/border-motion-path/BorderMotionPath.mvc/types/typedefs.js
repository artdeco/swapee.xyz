/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IBorderMotionPathComputer={}
xyz.swapee.wc.IBorderMotionPathComputer.adaptPath={}
xyz.swapee.wc.IBorderMotionPathComputer.compute={}
xyz.swapee.wc.IBorderMotionPathOuterCore={}
xyz.swapee.wc.IBorderMotionPathOuterCore.Model={}
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Gap={}
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Count={}
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.BorderRadius={}
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Delay={}
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Path={}
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.SvgPath={}
xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel={}
xyz.swapee.wc.IBorderMotionPathPort={}
xyz.swapee.wc.IBorderMotionPathPort.Inputs={}
xyz.swapee.wc.IBorderMotionPathPort.WeakInputs={}
xyz.swapee.wc.IBorderMotionPathCore={}
xyz.swapee.wc.IBorderMotionPathCore.Model={}
xyz.swapee.wc.IBorderMotionPathPortInterface={}
xyz.swapee.wc.IBorderMotionPathProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IBorderMotionPathController={}
xyz.swapee.wc.front.IBorderMotionPathControllerAT={}
xyz.swapee.wc.front.IBorderMotionPathScreenAR={}
xyz.swapee.wc.IBorderMotionPath={}
xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil={}
xyz.swapee.wc.IBorderMotionPathHtmlComponent={}
xyz.swapee.wc.IBorderMotionPathElement={}
xyz.swapee.wc.IBorderMotionPathElement.build={}
xyz.swapee.wc.IBorderMotionPathElement.short={}
xyz.swapee.wc.IBorderMotionPathElementPort={}
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs={}
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts={}
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts={}
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts={}
xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs={}
xyz.swapee.wc.IBorderMotionPathDesigner={}
xyz.swapee.wc.IBorderMotionPathDesigner.communicator={}
xyz.swapee.wc.IBorderMotionPathDesigner.relay={}
xyz.swapee.wc.IBorderMotionPathDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IBorderMotionPathDisplay={}
xyz.swapee.wc.back.IBorderMotionPathController={}
xyz.swapee.wc.back.IBorderMotionPathControllerAR={}
xyz.swapee.wc.back.IBorderMotionPathScreen={}
xyz.swapee.wc.back.IBorderMotionPathScreenAT={}
xyz.swapee.wc.IBorderMotionPathController={}
xyz.swapee.wc.IBorderMotionPathScreen={}
xyz.swapee.wc.IBorderMotionPathGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/02-IBorderMotionPathComputer.xml}  4b206e5e79f6c6a454c58a97e7cdcdf9 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IBorderMotionPathComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathComputer)} xyz.swapee.wc.AbstractBorderMotionPathComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathComputer} xyz.swapee.wc.BorderMotionPathComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathComputer` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathComputer
 */
xyz.swapee.wc.AbstractBorderMotionPathComputer = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathComputer.constructor&xyz.swapee.wc.BorderMotionPathComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathComputer.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathComputer.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathComputer}
 */
xyz.swapee.wc.AbstractBorderMotionPathComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathComputer}
 */
xyz.swapee.wc.AbstractBorderMotionPathComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathComputer}
 */
xyz.swapee.wc.AbstractBorderMotionPathComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathComputer}
 */
xyz.swapee.wc.AbstractBorderMotionPathComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathComputer.Initialese[]) => xyz.swapee.wc.IBorderMotionPathComputer} xyz.swapee.wc.BorderMotionPathComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.BorderMotionPathMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.BorderMotionPathLand>)} xyz.swapee.wc.IBorderMotionPathComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IBorderMotionPathComputer
 */
xyz.swapee.wc.IBorderMotionPathComputer = class extends /** @type {xyz.swapee.wc.IBorderMotionPathComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IBorderMotionPathComputer.adaptPath} */
xyz.swapee.wc.IBorderMotionPathComputer.prototype.adaptPath = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathComputer.compute} */
xyz.swapee.wc.IBorderMotionPathComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathComputer.Initialese>)} xyz.swapee.wc.BorderMotionPathComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathComputer} xyz.swapee.wc.IBorderMotionPathComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IBorderMotionPathComputer_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathComputer
 * @implements {xyz.swapee.wc.IBorderMotionPathComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathComputer.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathComputer = class extends /** @type {xyz.swapee.wc.BorderMotionPathComputer.constructor&xyz.swapee.wc.IBorderMotionPathComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathComputer}
 */
xyz.swapee.wc.BorderMotionPathComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IBorderMotionPathComputer} */
xyz.swapee.wc.RecordIBorderMotionPathComputer

/** @typedef {xyz.swapee.wc.IBorderMotionPathComputer} xyz.swapee.wc.BoundIBorderMotionPathComputer */

/** @typedef {xyz.swapee.wc.BorderMotionPathComputer} xyz.swapee.wc.BoundBorderMotionPathComputer */

/**
 * Contains getters to cast the _IBorderMotionPathComputer_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathComputerCaster
 */
xyz.swapee.wc.IBorderMotionPathComputerCaster = class { }
/**
 * Cast the _IBorderMotionPathComputer_ instance into the _BoundIBorderMotionPathComputer_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathComputer}
 */
xyz.swapee.wc.IBorderMotionPathComputerCaster.prototype.asIBorderMotionPathComputer
/**
 * Access the _BorderMotionPathComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathComputer}
 */
xyz.swapee.wc.IBorderMotionPathComputerCaster.prototype.superBorderMotionPathComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Form, changes: xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Form) => (void|xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Return)} xyz.swapee.wc.IBorderMotionPathComputer.__adaptPath
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathComputer.__adaptPath<!xyz.swapee.wc.IBorderMotionPathComputer>} xyz.swapee.wc.IBorderMotionPathComputer._adaptPath */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathComputer.adaptPath} */
/**
 * @param {!xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Form} form The form with inputs.
 * - `borderRadius` _number_ The border radius. ⤴ *IBorderMotionPathOuterCore.Model.BorderRadius_Safe*
 * @param {xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Form} changes The previous values of the form.
 * - `borderRadius` _number_ The border radius. ⤴ *IBorderMotionPathOuterCore.Model.BorderRadius_Safe*
 * @return {void|xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Return} The form with outputs.
 */
xyz.swapee.wc.IBorderMotionPathComputer.adaptPath = function(form, changes) {}

/** @typedef {com.webcircuits.ui.IClientRectCore.Model.Width_Safe&com.webcircuits.ui.IClientRectCore.Model.Height_Safe&xyz.swapee.wc.IBorderMotionPathCore.Model.BorderRadius_Safe} xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IBorderMotionPathCore.Model.Path&xyz.swapee.wc.IBorderMotionPathCore.Model.Ready} xyz.swapee.wc.IBorderMotionPathComputer.adaptPath.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.BorderMotionPathMemory, land: !xyz.swapee.wc.IBorderMotionPathComputer.compute.Land) => void} xyz.swapee.wc.IBorderMotionPathComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathComputer.__compute<!xyz.swapee.wc.IBorderMotionPathComputer>} xyz.swapee.wc.IBorderMotionPathComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.BorderMotionPathMemory} mem The memory.
 * @param {!xyz.swapee.wc.IBorderMotionPathComputer.compute.Land} land The land.
 * - `ClientRect` _!com.webcircuits.ui.ClientRectMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IBorderMotionPathComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathComputer.compute.Land The land.
 * @prop {!com.webcircuits.ui.ClientRectMemory} ClientRect `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IBorderMotionPathComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/03-IBorderMotionPathOuterCore.xml}  f73fe68fd3d95ef87abee313e2b887c2 */
/** @typedef {boolean} */
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Ready.ready

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Ready  (optional overlay).
 * @prop {boolean} [ready=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Ready_Safe  (required overlay).
 * @prop {boolean} ready
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Ready  (optional overlay).
 * @prop {*} [ready=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Ready_Safe  (required overlay).
 * @prop {*} ready
 */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Ready} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Ready  (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Ready_Safe} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Ready_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Ready} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Ready  (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Ready_Safe} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Ready_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Ready} xyz.swapee.wc.IBorderMotionPathCore.Model.Ready  (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Ready_Safe} xyz.swapee.wc.IBorderMotionPathCore.Model.Ready_Safe  (required overlay). */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IBorderMotionPathOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathOuterCore)} xyz.swapee.wc.AbstractBorderMotionPathOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathOuterCore} xyz.swapee.wc.BorderMotionPathOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathOuterCore
 */
xyz.swapee.wc.AbstractBorderMotionPathOuterCore = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathOuterCore.constructor&xyz.swapee.wc.BorderMotionPathOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathOuterCore.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IBorderMotionPathOuterCore|typeof xyz.swapee.wc.BorderMotionPathOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathOuterCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathOuterCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IBorderMotionPathOuterCore|typeof xyz.swapee.wc.BorderMotionPathOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathOuterCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IBorderMotionPathOuterCore|typeof xyz.swapee.wc.BorderMotionPathOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathOuterCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathOuterCoreCaster)} xyz.swapee.wc.IBorderMotionPathOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IBorderMotionPath_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IBorderMotionPathOuterCore
 */
xyz.swapee.wc.IBorderMotionPathOuterCore = class extends /** @type {xyz.swapee.wc.IBorderMotionPathOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IBorderMotionPathOuterCore.prototype.constructor = xyz.swapee.wc.IBorderMotionPathOuterCore

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathOuterCore.Initialese>)} xyz.swapee.wc.BorderMotionPathOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathOuterCore} xyz.swapee.wc.IBorderMotionPathOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IBorderMotionPathOuterCore_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathOuterCore
 * @implements {xyz.swapee.wc.IBorderMotionPathOuterCore} The _IBorderMotionPath_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathOuterCore = class extends /** @type {xyz.swapee.wc.BorderMotionPathOuterCore.constructor&xyz.swapee.wc.IBorderMotionPathOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.BorderMotionPathOuterCore.prototype.constructor = xyz.swapee.wc.BorderMotionPathOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathOuterCore}
 */
xyz.swapee.wc.BorderMotionPathOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathOuterCore.
 * @interface xyz.swapee.wc.IBorderMotionPathOuterCoreFields
 */
xyz.swapee.wc.IBorderMotionPathOuterCoreFields = class { }
/**
 * The _IBorderMotionPath_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IBorderMotionPathOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IBorderMotionPathOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore} */
xyz.swapee.wc.RecordIBorderMotionPathOuterCore

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore} xyz.swapee.wc.BoundIBorderMotionPathOuterCore */

/** @typedef {xyz.swapee.wc.BorderMotionPathOuterCore} xyz.swapee.wc.BoundBorderMotionPathOuterCore */

/**
 * The gap in terms of delay between moving particles.
 * @typedef {number}
 */
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Gap.gap

/**
 * How manu particles to add.
 * @typedef {number}
 */
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Count.count

/**
 * The border radius.
 * @typedef {number}
 */
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.BorderRadius.borderRadius

/**
 * The delay.
 * @typedef {number}
 */
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Delay.delay

/**
 * The path computed by JavaScript for the border.
 * @typedef {string}
 */
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Path.path

/**
 * The path computed by JavaScript for the border.
 * @typedef {string}
 */
xyz.swapee.wc.IBorderMotionPathOuterCore.Model.SvgPath.svgPath

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Gap&xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Count&xyz.swapee.wc.IBorderMotionPathOuterCore.Model.BorderRadius&xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Delay&xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Path&xyz.swapee.wc.IBorderMotionPathOuterCore.Model.SvgPath} xyz.swapee.wc.IBorderMotionPathOuterCore.Model The _IBorderMotionPath_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Gap&xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Count&xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.BorderRadius&xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Delay&xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Path&xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.SvgPath} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel The _IBorderMotionPath_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IBorderMotionPathOuterCore_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathOuterCoreCaster
 */
xyz.swapee.wc.IBorderMotionPathOuterCoreCaster = class { }
/**
 * Cast the _IBorderMotionPathOuterCore_ instance into the _BoundIBorderMotionPathOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathOuterCore}
 */
xyz.swapee.wc.IBorderMotionPathOuterCoreCaster.prototype.asIBorderMotionPathOuterCore
/**
 * Access the _BorderMotionPathOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathOuterCore}
 */
xyz.swapee.wc.IBorderMotionPathOuterCoreCaster.prototype.superBorderMotionPathOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Gap The gap in terms of delay between moving particles (optional overlay).
 * @prop {number} [gap=0.01] The gap in terms of delay between moving particles. Default `0.01`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Gap_Safe The gap in terms of delay between moving particles (required overlay).
 * @prop {number} gap The gap in terms of delay between moving particles.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Count How manu particles to add (optional overlay).
 * @prop {number} [count=12] How manu particles to add. Default `12`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Count_Safe How manu particles to add (required overlay).
 * @prop {number} count How manu particles to add.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.BorderRadius The border radius (optional overlay).
 * @prop {number} [borderRadius=10] The border radius. Default `10`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.BorderRadius_Safe The border radius (required overlay).
 * @prop {number} borderRadius The border radius.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Delay The delay (optional overlay).
 * @prop {number} [delay=0] The delay. Default `0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Delay_Safe The delay (required overlay).
 * @prop {number} delay The delay.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Path The path computed by JavaScript for the border (optional overlay).
 * @prop {string} [path=""] The path computed by JavaScript for the border. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Path_Safe The path computed by JavaScript for the border (required overlay).
 * @prop {string} path The path computed by JavaScript for the border.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.SvgPath The path computed by JavaScript for the border (optional overlay).
 * @prop {string} [svgPath=""] The path computed by JavaScript for the border. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.Model.SvgPath_Safe The path computed by JavaScript for the border (required overlay).
 * @prop {string} svgPath The path computed by JavaScript for the border.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Gap The gap in terms of delay between moving particles (optional overlay).
 * @prop {*} [gap="0.01"] The gap in terms of delay between moving particles. Default `0.01`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Gap_Safe The gap in terms of delay between moving particles (required overlay).
 * @prop {*} gap The gap in terms of delay between moving particles.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Count How manu particles to add (optional overlay).
 * @prop {*} [count=12] How manu particles to add. Default `12`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Count_Safe How manu particles to add (required overlay).
 * @prop {*} count How manu particles to add.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.BorderRadius The border radius (optional overlay).
 * @prop {*} [borderRadius=10] The border radius. Default `10`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.BorderRadius_Safe The border radius (required overlay).
 * @prop {*} borderRadius The border radius.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Delay The delay (optional overlay).
 * @prop {*} [delay=null] The delay. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Delay_Safe The delay (required overlay).
 * @prop {*} delay The delay.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Path The path computed by JavaScript for the border (optional overlay).
 * @prop {*} [path=null] The path computed by JavaScript for the border. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Path_Safe The path computed by JavaScript for the border (required overlay).
 * @prop {*} path The path computed by JavaScript for the border.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.SvgPath The path computed by JavaScript for the border (optional overlay).
 * @prop {*} [svgPath=null] The path computed by JavaScript for the border. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.SvgPath_Safe The path computed by JavaScript for the border (required overlay).
 * @prop {*} svgPath The path computed by JavaScript for the border.
 */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Gap} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Gap The gap in terms of delay between moving particles (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Gap_Safe} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Gap_Safe The gap in terms of delay between moving particles (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Count} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Count How manu particles to add (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Count_Safe} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Count_Safe How manu particles to add (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.BorderRadius} xyz.swapee.wc.IBorderMotionPathPort.Inputs.BorderRadius The border radius (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.BorderRadius_Safe} xyz.swapee.wc.IBorderMotionPathPort.Inputs.BorderRadius_Safe The border radius (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Delay} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Delay The delay (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Delay_Safe} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Delay_Safe The delay (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Path} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Path The path computed by JavaScript for the border (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Path_Safe} xyz.swapee.wc.IBorderMotionPathPort.Inputs.Path_Safe The path computed by JavaScript for the border (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.SvgPath} xyz.swapee.wc.IBorderMotionPathPort.Inputs.SvgPath The path computed by JavaScript for the border (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.SvgPath_Safe} xyz.swapee.wc.IBorderMotionPathPort.Inputs.SvgPath_Safe The path computed by JavaScript for the border (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Gap} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Gap The gap in terms of delay between moving particles (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Gap_Safe} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Gap_Safe The gap in terms of delay between moving particles (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Count} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Count How manu particles to add (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Count_Safe} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Count_Safe How manu particles to add (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.BorderRadius} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.BorderRadius The border radius (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.BorderRadius_Safe} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.BorderRadius_Safe The border radius (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Delay} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Delay The delay (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Delay_Safe} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Delay_Safe The delay (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Path} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Path The path computed by JavaScript for the border (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.Path_Safe} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.Path_Safe The path computed by JavaScript for the border (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.SvgPath} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.SvgPath The path computed by JavaScript for the border (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.SvgPath_Safe} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.SvgPath_Safe The path computed by JavaScript for the border (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Gap} xyz.swapee.wc.IBorderMotionPathCore.Model.Gap The gap in terms of delay between moving particles (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Gap_Safe} xyz.swapee.wc.IBorderMotionPathCore.Model.Gap_Safe The gap in terms of delay between moving particles (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Count} xyz.swapee.wc.IBorderMotionPathCore.Model.Count How manu particles to add (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Count_Safe} xyz.swapee.wc.IBorderMotionPathCore.Model.Count_Safe How manu particles to add (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.BorderRadius} xyz.swapee.wc.IBorderMotionPathCore.Model.BorderRadius The border radius (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.BorderRadius_Safe} xyz.swapee.wc.IBorderMotionPathCore.Model.BorderRadius_Safe The border radius (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Delay} xyz.swapee.wc.IBorderMotionPathCore.Model.Delay The delay (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Delay_Safe} xyz.swapee.wc.IBorderMotionPathCore.Model.Delay_Safe The delay (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Path} xyz.swapee.wc.IBorderMotionPathCore.Model.Path The path computed by JavaScript for the border (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.Path_Safe} xyz.swapee.wc.IBorderMotionPathCore.Model.Path_Safe The path computed by JavaScript for the border (required overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.SvgPath} xyz.swapee.wc.IBorderMotionPathCore.Model.SvgPath The path computed by JavaScript for the border (optional overlay). */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model.SvgPath_Safe} xyz.swapee.wc.IBorderMotionPathCore.Model.SvgPath_Safe The path computed by JavaScript for the border (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/04-IBorderMotionPathPort.xml}  c326ac5e84bacbab0ccd020b4b7c05dd */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IBorderMotionPathPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathPort)} xyz.swapee.wc.AbstractBorderMotionPathPort.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathPort} xyz.swapee.wc.BorderMotionPathPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathPort` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathPort
 */
xyz.swapee.wc.AbstractBorderMotionPathPort = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathPort.constructor&xyz.swapee.wc.BorderMotionPathPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathPort.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathPort.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathPort|typeof xyz.swapee.wc.BorderMotionPathPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathPort|typeof xyz.swapee.wc.BorderMotionPathPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathPort|typeof xyz.swapee.wc.BorderMotionPathPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathPort.Initialese[]) => xyz.swapee.wc.IBorderMotionPathPort} xyz.swapee.wc.BorderMotionPathPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathPortFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IBorderMotionPathPort.Inputs>)} xyz.swapee.wc.IBorderMotionPathPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IBorderMotionPath_, providing input
 * pins.
 * @interface xyz.swapee.wc.IBorderMotionPathPort
 */
xyz.swapee.wc.IBorderMotionPathPort = class extends /** @type {xyz.swapee.wc.IBorderMotionPathPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IBorderMotionPathPort.resetPort} */
xyz.swapee.wc.IBorderMotionPathPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathPort.resetBorderMotionPathPort} */
xyz.swapee.wc.IBorderMotionPathPort.prototype.resetBorderMotionPathPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathPort&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathPort.Initialese>)} xyz.swapee.wc.BorderMotionPathPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathPort} xyz.swapee.wc.IBorderMotionPathPort.typeof */
/**
 * A concrete class of _IBorderMotionPathPort_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathPort
 * @implements {xyz.swapee.wc.IBorderMotionPathPort} The port that serves as an interface to the _IBorderMotionPath_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathPort.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathPort = class extends /** @type {xyz.swapee.wc.BorderMotionPathPort.constructor&xyz.swapee.wc.IBorderMotionPathPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathPort}
 */
xyz.swapee.wc.BorderMotionPathPort.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathPort.
 * @interface xyz.swapee.wc.IBorderMotionPathPortFields
 */
xyz.swapee.wc.IBorderMotionPathPortFields = class { }
/**
 * The inputs to the _IBorderMotionPath_'s controller via its port.
 */
xyz.swapee.wc.IBorderMotionPathPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IBorderMotionPathPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IBorderMotionPathPortFields.prototype.props = /** @type {!xyz.swapee.wc.IBorderMotionPathPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathPort} */
xyz.swapee.wc.RecordIBorderMotionPathPort

/** @typedef {xyz.swapee.wc.IBorderMotionPathPort} xyz.swapee.wc.BoundIBorderMotionPathPort */

/** @typedef {xyz.swapee.wc.BorderMotionPathPort} xyz.swapee.wc.BoundBorderMotionPathPort */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel)} xyz.swapee.wc.IBorderMotionPathPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel} xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IBorderMotionPath_'s controller via its port.
 * @record xyz.swapee.wc.IBorderMotionPathPort.Inputs
 */
xyz.swapee.wc.IBorderMotionPathPort.Inputs = class extends /** @type {xyz.swapee.wc.IBorderMotionPathPort.Inputs.constructor&xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IBorderMotionPathPort.Inputs.prototype.constructor = xyz.swapee.wc.IBorderMotionPathPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel)} xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.constructor */
/**
 * The inputs to the _IBorderMotionPath_'s controller via its port.
 * @record xyz.swapee.wc.IBorderMotionPathPort.WeakInputs
 */
xyz.swapee.wc.IBorderMotionPathPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.constructor&xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IBorderMotionPathPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IBorderMotionPathPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IBorderMotionPathPortInterface
 */
xyz.swapee.wc.IBorderMotionPathPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IBorderMotionPathPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IBorderMotionPathPortInterface.prototype.constructor = xyz.swapee.wc.IBorderMotionPathPortInterface

/**
 * A concrete class of _IBorderMotionPathPortInterface_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathPortInterface
 * @implements {xyz.swapee.wc.IBorderMotionPathPortInterface} The port interface.
 */
xyz.swapee.wc.BorderMotionPathPortInterface = class extends xyz.swapee.wc.IBorderMotionPathPortInterface { }
xyz.swapee.wc.BorderMotionPathPortInterface.prototype.constructor = xyz.swapee.wc.BorderMotionPathPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathPortInterface.Props
 * @prop {number} gap The gap in terms of delay between moving particles. Default `0.01`.
 * @prop {number} count How manu particles to add. Default `12`.
 * @prop {number} borderRadius The border radius. Default `10`.
 * @prop {number} delay The delay.
 * @prop {string} path The path computed by JavaScript for the border.
 * @prop {string} svgPath The path computed by JavaScript for the border.
 */

/**
 * Contains getters to cast the _IBorderMotionPathPort_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathPortCaster
 */
xyz.swapee.wc.IBorderMotionPathPortCaster = class { }
/**
 * Cast the _IBorderMotionPathPort_ instance into the _BoundIBorderMotionPathPort_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathPort}
 */
xyz.swapee.wc.IBorderMotionPathPortCaster.prototype.asIBorderMotionPathPort
/**
 * Access the _BorderMotionPathPort_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathPort}
 */
xyz.swapee.wc.IBorderMotionPathPortCaster.prototype.superBorderMotionPathPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IBorderMotionPathPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathPort.__resetPort<!xyz.swapee.wc.IBorderMotionPathPort>} xyz.swapee.wc.IBorderMotionPathPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathPort.resetPort} */
/**
 * Resets the _IBorderMotionPath_ port.
 * @return {void}
 */
xyz.swapee.wc.IBorderMotionPathPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IBorderMotionPathPort.__resetBorderMotionPathPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathPort.__resetBorderMotionPathPort<!xyz.swapee.wc.IBorderMotionPathPort>} xyz.swapee.wc.IBorderMotionPathPort._resetBorderMotionPathPort */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathPort.resetBorderMotionPathPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IBorderMotionPathPort.resetBorderMotionPathPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IBorderMotionPathPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/09-IBorderMotionPathCore.xml}  e3e2d12a4b435b34225748e0894476cf */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IBorderMotionPathCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathCore)} xyz.swapee.wc.AbstractBorderMotionPathCore.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathCore} xyz.swapee.wc.BorderMotionPathCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathCore` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathCore
 */
xyz.swapee.wc.AbstractBorderMotionPathCore = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathCore.constructor&xyz.swapee.wc.BorderMotionPathCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathCore.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathCore.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathCore|typeof xyz.swapee.wc.BorderMotionPathCore)|(!xyz.swapee.wc.IBorderMotionPathOuterCore|typeof xyz.swapee.wc.BorderMotionPathOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathCore|typeof xyz.swapee.wc.BorderMotionPathCore)|(!xyz.swapee.wc.IBorderMotionPathOuterCore|typeof xyz.swapee.wc.BorderMotionPathOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathCore|typeof xyz.swapee.wc.BorderMotionPathCore)|(!xyz.swapee.wc.IBorderMotionPathOuterCore|typeof xyz.swapee.wc.BorderMotionPathOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathCore}
 */
xyz.swapee.wc.AbstractBorderMotionPathCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathCoreCaster&xyz.swapee.wc.IBorderMotionPathOuterCore)} xyz.swapee.wc.IBorderMotionPathCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IBorderMotionPathCore
 */
xyz.swapee.wc.IBorderMotionPathCore = class extends /** @type {xyz.swapee.wc.IBorderMotionPathCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IBorderMotionPathOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IBorderMotionPathCore.resetCore} */
xyz.swapee.wc.IBorderMotionPathCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathCore.resetBorderMotionPathCore} */
xyz.swapee.wc.IBorderMotionPathCore.prototype.resetBorderMotionPathCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathCore&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathCore.Initialese>)} xyz.swapee.wc.BorderMotionPathCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathCore} xyz.swapee.wc.IBorderMotionPathCore.typeof */
/**
 * A concrete class of _IBorderMotionPathCore_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathCore
 * @implements {xyz.swapee.wc.IBorderMotionPathCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathCore.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathCore = class extends /** @type {xyz.swapee.wc.BorderMotionPathCore.constructor&xyz.swapee.wc.IBorderMotionPathCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.BorderMotionPathCore.prototype.constructor = xyz.swapee.wc.BorderMotionPathCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathCore}
 */
xyz.swapee.wc.BorderMotionPathCore.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathCore.
 * @interface xyz.swapee.wc.IBorderMotionPathCoreFields
 */
xyz.swapee.wc.IBorderMotionPathCoreFields = class { }
/**
 * The _IBorderMotionPath_'s memory.
 */
xyz.swapee.wc.IBorderMotionPathCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IBorderMotionPathCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IBorderMotionPathCoreFields.prototype.props = /** @type {xyz.swapee.wc.IBorderMotionPathCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathCore} */
xyz.swapee.wc.RecordIBorderMotionPathCore

/** @typedef {xyz.swapee.wc.IBorderMotionPathCore} xyz.swapee.wc.BoundIBorderMotionPathCore */

/** @typedef {xyz.swapee.wc.BorderMotionPathCore} xyz.swapee.wc.BoundBorderMotionPathCore */

/** @typedef {xyz.swapee.wc.IBorderMotionPathOuterCore.Model} xyz.swapee.wc.IBorderMotionPathCore.Model The _IBorderMotionPath_'s memory. */

/**
 * Contains getters to cast the _IBorderMotionPathCore_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathCoreCaster
 */
xyz.swapee.wc.IBorderMotionPathCoreCaster = class { }
/**
 * Cast the _IBorderMotionPathCore_ instance into the _BoundIBorderMotionPathCore_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathCore}
 */
xyz.swapee.wc.IBorderMotionPathCoreCaster.prototype.asIBorderMotionPathCore
/**
 * Access the _BorderMotionPathCore_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathCore}
 */
xyz.swapee.wc.IBorderMotionPathCoreCaster.prototype.superBorderMotionPathCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IBorderMotionPathCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathCore.__resetCore<!xyz.swapee.wc.IBorderMotionPathCore>} xyz.swapee.wc.IBorderMotionPathCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathCore.resetCore} */
/**
 * Resets the _IBorderMotionPath_ core.
 * @return {void}
 */
xyz.swapee.wc.IBorderMotionPathCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IBorderMotionPathCore.__resetBorderMotionPathCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathCore.__resetBorderMotionPathCore<!xyz.swapee.wc.IBorderMotionPathCore>} xyz.swapee.wc.IBorderMotionPathCore._resetBorderMotionPathCore */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathCore.resetBorderMotionPathCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IBorderMotionPathCore.resetBorderMotionPathCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IBorderMotionPathCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/10-IBorderMotionPathProcessor.xml}  db776423db3bb1297dd1d6db854b2b25 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathComputer.Initialese&xyz.swapee.wc.IBorderMotionPathController.Initialese} xyz.swapee.wc.IBorderMotionPathProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathProcessor)} xyz.swapee.wc.AbstractBorderMotionPathProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathProcessor} xyz.swapee.wc.BorderMotionPathProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathProcessor
 */
xyz.swapee.wc.AbstractBorderMotionPathProcessor = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathProcessor.constructor&xyz.swapee.wc.BorderMotionPathProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathProcessor.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!xyz.swapee.wc.IBorderMotionPathCore|typeof xyz.swapee.wc.BorderMotionPathCore)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathProcessor}
 */
xyz.swapee.wc.AbstractBorderMotionPathProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathProcessor}
 */
xyz.swapee.wc.AbstractBorderMotionPathProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!xyz.swapee.wc.IBorderMotionPathCore|typeof xyz.swapee.wc.BorderMotionPathCore)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathProcessor}
 */
xyz.swapee.wc.AbstractBorderMotionPathProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!xyz.swapee.wc.IBorderMotionPathCore|typeof xyz.swapee.wc.BorderMotionPathCore)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathProcessor}
 */
xyz.swapee.wc.AbstractBorderMotionPathProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathProcessor.Initialese[]) => xyz.swapee.wc.IBorderMotionPathProcessor} xyz.swapee.wc.BorderMotionPathProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathProcessorCaster&xyz.swapee.wc.IBorderMotionPathComputer&xyz.swapee.wc.IBorderMotionPathCore&xyz.swapee.wc.IBorderMotionPathController)} xyz.swapee.wc.IBorderMotionPathProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathController} xyz.swapee.wc.IBorderMotionPathController.typeof */
/**
 * The processor to compute changes to the memory for the _IBorderMotionPath_.
 * @interface xyz.swapee.wc.IBorderMotionPathProcessor
 */
xyz.swapee.wc.IBorderMotionPathProcessor = class extends /** @type {xyz.swapee.wc.IBorderMotionPathProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IBorderMotionPathComputer.typeof&xyz.swapee.wc.IBorderMotionPathCore.typeof&xyz.swapee.wc.IBorderMotionPathController.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathProcessor.Initialese>)} xyz.swapee.wc.BorderMotionPathProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathProcessor} xyz.swapee.wc.IBorderMotionPathProcessor.typeof */
/**
 * A concrete class of _IBorderMotionPathProcessor_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathProcessor
 * @implements {xyz.swapee.wc.IBorderMotionPathProcessor} The processor to compute changes to the memory for the _IBorderMotionPath_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathProcessor.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathProcessor = class extends /** @type {xyz.swapee.wc.BorderMotionPathProcessor.constructor&xyz.swapee.wc.IBorderMotionPathProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathProcessor}
 */
xyz.swapee.wc.BorderMotionPathProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IBorderMotionPathProcessor} */
xyz.swapee.wc.RecordIBorderMotionPathProcessor

/** @typedef {xyz.swapee.wc.IBorderMotionPathProcessor} xyz.swapee.wc.BoundIBorderMotionPathProcessor */

/** @typedef {xyz.swapee.wc.BorderMotionPathProcessor} xyz.swapee.wc.BoundBorderMotionPathProcessor */

/**
 * Contains getters to cast the _IBorderMotionPathProcessor_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathProcessorCaster
 */
xyz.swapee.wc.IBorderMotionPathProcessorCaster = class { }
/**
 * Cast the _IBorderMotionPathProcessor_ instance into the _BoundIBorderMotionPathProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathProcessor}
 */
xyz.swapee.wc.IBorderMotionPathProcessorCaster.prototype.asIBorderMotionPathProcessor
/**
 * Access the _BorderMotionPathProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathProcessor}
 */
xyz.swapee.wc.IBorderMotionPathProcessorCaster.prototype.superBorderMotionPathProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/100-BorderMotionPathMemory.xml}  69cbf88865affcc53a3be78ef37b88c4 */
/**
 * The memory of the _IBorderMotionPath_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.BorderMotionPathMemory
 */
xyz.swapee.wc.BorderMotionPathMemory = class { }
/**
 * The gap in terms of delay between moving particles. Default `0.01`.
 */
xyz.swapee.wc.BorderMotionPathMemory.prototype.gap = /** @type {number} */ (void 0)
/**
 * How manu particles to add. Default `12`.
 */
xyz.swapee.wc.BorderMotionPathMemory.prototype.count = /** @type {number} */ (void 0)
/**
 * The border radius. Default `10`.
 */
xyz.swapee.wc.BorderMotionPathMemory.prototype.borderRadius = /** @type {number} */ (void 0)
/**
 * The delay. Default `0`.
 */
xyz.swapee.wc.BorderMotionPathMemory.prototype.delay = /** @type {number} */ (void 0)
/**
 * The path computed by JavaScript for the border. Default empty string.
 */
xyz.swapee.wc.BorderMotionPathMemory.prototype.path = /** @type {string} */ (void 0)
/**
 * The path computed by JavaScript for the border. Default empty string.
 */
xyz.swapee.wc.BorderMotionPathMemory.prototype.svgPath = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/102-BorderMotionPathInputs.xml}  e11b7f8a7163407788da4f5fe77ebe1d */
/**
 * The inputs of the _IBorderMotionPath_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.BorderMotionPathInputs
 */
xyz.swapee.wc.front.BorderMotionPathInputs = class { }
/**
 * The gap in terms of delay between moving particles. Default `0.01`.
 */
xyz.swapee.wc.front.BorderMotionPathInputs.prototype.gap = /** @type {number|undefined} */ (void 0)
/**
 * How manu particles to add. Default `12`.
 */
xyz.swapee.wc.front.BorderMotionPathInputs.prototype.count = /** @type {number|undefined} */ (void 0)
/**
 * The border radius. Default `10`.
 */
xyz.swapee.wc.front.BorderMotionPathInputs.prototype.borderRadius = /** @type {number|undefined} */ (void 0)
/**
 * The delay. Default `0`.
 */
xyz.swapee.wc.front.BorderMotionPathInputs.prototype.delay = /** @type {number|undefined} */ (void 0)
/**
 * The path computed by JavaScript for the border. Default empty string.
 */
xyz.swapee.wc.front.BorderMotionPathInputs.prototype.path = /** @type {string|undefined} */ (void 0)
/**
 * The path computed by JavaScript for the border. Default empty string.
 */
xyz.swapee.wc.front.BorderMotionPathInputs.prototype.svgPath = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/11-IBorderMotionPath.xml}  588c185412fa0888ac4580c358aab3b7 */
/**
 * An atomic wrapper for the _IBorderMotionPath_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.BorderMotionPathEnv
 */
xyz.swapee.wc.BorderMotionPathEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.BorderMotionPathEnv.prototype.borderMotionPath = /** @type {xyz.swapee.wc.IBorderMotionPath} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.IBorderMotionPathController.Inputs>&xyz.swapee.wc.IBorderMotionPathProcessor.Initialese&xyz.swapee.wc.IBorderMotionPathComputer.Initialese&xyz.swapee.wc.IBorderMotionPathController.Initialese} xyz.swapee.wc.IBorderMotionPath.Initialese */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPath)} xyz.swapee.wc.AbstractBorderMotionPath.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPath} xyz.swapee.wc.BorderMotionPath.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPath` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPath
 */
xyz.swapee.wc.AbstractBorderMotionPath = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPath.constructor&xyz.swapee.wc.BorderMotionPath.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPath.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPath
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPath.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPath} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPath|typeof xyz.swapee.wc.BorderMotionPath)|(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPath}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPath.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPath}
 */
xyz.swapee.wc.AbstractBorderMotionPath.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPath}
 */
xyz.swapee.wc.AbstractBorderMotionPath.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPath|typeof xyz.swapee.wc.BorderMotionPath)|(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPath}
 */
xyz.swapee.wc.AbstractBorderMotionPath.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPath|typeof xyz.swapee.wc.BorderMotionPath)|(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPath}
 */
xyz.swapee.wc.AbstractBorderMotionPath.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPath.Initialese[]) => xyz.swapee.wc.IBorderMotionPath} xyz.swapee.wc.BorderMotionPathConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPath.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IBorderMotionPath.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IBorderMotionPath.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IBorderMotionPath.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.BorderMotionPathMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.BorderMotionPathClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathCaster&xyz.swapee.wc.IBorderMotionPathProcessor&xyz.swapee.wc.IBorderMotionPathComputer&xyz.swapee.wc.IBorderMotionPathController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.IBorderMotionPathController.Inputs, !xyz.swapee.wc.BorderMotionPathLand>)} xyz.swapee.wc.IBorderMotionPath.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathProcessor} xyz.swapee.wc.IBorderMotionPathProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathController} xyz.swapee.wc.IBorderMotionPathController.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IBorderMotionPath
 */
xyz.swapee.wc.IBorderMotionPath = class extends /** @type {xyz.swapee.wc.IBorderMotionPath.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IBorderMotionPathProcessor.typeof&xyz.swapee.wc.IBorderMotionPathComputer.typeof&xyz.swapee.wc.IBorderMotionPathController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPath* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPath.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPath.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPath&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPath.Initialese>)} xyz.swapee.wc.BorderMotionPath.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPath} xyz.swapee.wc.IBorderMotionPath.typeof */
/**
 * A concrete class of _IBorderMotionPath_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPath
 * @implements {xyz.swapee.wc.IBorderMotionPath} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPath.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPath = class extends /** @type {xyz.swapee.wc.BorderMotionPath.constructor&xyz.swapee.wc.IBorderMotionPath.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPath* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPath.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPath* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPath.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPath.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPath}
 */
xyz.swapee.wc.BorderMotionPath.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPath.
 * @interface xyz.swapee.wc.IBorderMotionPathFields
 */
xyz.swapee.wc.IBorderMotionPathFields = class { }
/**
 * The input pins of the _IBorderMotionPath_ port.
 */
xyz.swapee.wc.IBorderMotionPathFields.prototype.pinout = /** @type {!xyz.swapee.wc.IBorderMotionPath.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPath} */
xyz.swapee.wc.RecordIBorderMotionPath

/** @typedef {xyz.swapee.wc.IBorderMotionPath} xyz.swapee.wc.BoundIBorderMotionPath */

/** @typedef {xyz.swapee.wc.BorderMotionPath} xyz.swapee.wc.BoundBorderMotionPath */

/** @typedef {xyz.swapee.wc.IBorderMotionPathController.Inputs} xyz.swapee.wc.IBorderMotionPath.Pinout The input pins of the _IBorderMotionPath_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IBorderMotionPathController.Inputs>)} xyz.swapee.wc.IBorderMotionPathBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IBorderMotionPathBuffer
 */
xyz.swapee.wc.IBorderMotionPathBuffer = class extends /** @type {xyz.swapee.wc.IBorderMotionPathBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IBorderMotionPathBuffer.prototype.constructor = xyz.swapee.wc.IBorderMotionPathBuffer

/**
 * A concrete class of _IBorderMotionPathBuffer_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathBuffer
 * @implements {xyz.swapee.wc.IBorderMotionPathBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.BorderMotionPathBuffer = class extends xyz.swapee.wc.IBorderMotionPathBuffer { }
xyz.swapee.wc.BorderMotionPathBuffer.prototype.constructor = xyz.swapee.wc.BorderMotionPathBuffer

/**
 * Contains getters to cast the _IBorderMotionPath_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathCaster
 */
xyz.swapee.wc.IBorderMotionPathCaster = class { }
/**
 * Cast the _IBorderMotionPath_ instance into the _BoundIBorderMotionPath_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPath}
 */
xyz.swapee.wc.IBorderMotionPathCaster.prototype.asIBorderMotionPath
/**
 * Access the _BorderMotionPath_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPath}
 */
xyz.swapee.wc.IBorderMotionPathCaster.prototype.superBorderMotionPath

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/110-BorderMotionPathSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.BorderMotionPathMemoryPQs
 */
xyz.swapee.wc.BorderMotionPathMemoryPQs = class {
  constructor() {
    /**
     * `df9bc`
     */
    this.gap=/** @type {string} */ (void 0)
    /**
     * `e2942`
     */
    this.count=/** @type {string} */ (void 0)
    /**
     * `d6fe1`
     */
    this.path=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.BorderMotionPathMemoryPQs.prototype.constructor = xyz.swapee.wc.BorderMotionPathMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.BorderMotionPathMemoryQPs
 * @dict
 */
xyz.swapee.wc.BorderMotionPathMemoryQPs = class { }
/**
 * `gap`
 */
xyz.swapee.wc.BorderMotionPathMemoryQPs.prototype.df9bc = /** @type {string} */ (void 0)
/**
 * `count`
 */
xyz.swapee.wc.BorderMotionPathMemoryQPs.prototype.e2942 = /** @type {string} */ (void 0)
/**
 * `path`
 */
xyz.swapee.wc.BorderMotionPathMemoryQPs.prototype.d6fe1 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathMemoryPQs)} xyz.swapee.wc.BorderMotionPathInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathMemoryPQs} xyz.swapee.wc.BorderMotionPathMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.BorderMotionPathInputsPQs
 */
xyz.swapee.wc.BorderMotionPathInputsPQs = class extends /** @type {xyz.swapee.wc.BorderMotionPathInputsPQs.constructor&xyz.swapee.wc.BorderMotionPathMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.BorderMotionPathInputsPQs.prototype.constructor = xyz.swapee.wc.BorderMotionPathInputsPQs

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathMemoryPQs)} xyz.swapee.wc.BorderMotionPathInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.BorderMotionPathInputsQPs
 * @dict
 */
xyz.swapee.wc.BorderMotionPathInputsQPs = class extends /** @type {xyz.swapee.wc.BorderMotionPathInputsQPs.constructor&xyz.swapee.wc.BorderMotionPathMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.BorderMotionPathInputsQPs.prototype.constructor = xyz.swapee.wc.BorderMotionPathInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.BorderMotionPathVdusPQs
 */
xyz.swapee.wc.BorderMotionPathVdusPQs = class { }
xyz.swapee.wc.BorderMotionPathVdusPQs.prototype.constructor = xyz.swapee.wc.BorderMotionPathVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.BorderMotionPathVdusQPs
 * @dict
 */
xyz.swapee.wc.BorderMotionPathVdusQPs = class { }
xyz.swapee.wc.BorderMotionPathVdusQPs.prototype.constructor = xyz.swapee.wc.BorderMotionPathVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/12-IBorderMotionPathHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathHtmlComponentUtilFields)} xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil */
xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.router} */
xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IBorderMotionPathHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathHtmlComponentUtil
 * @implements {xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil} ‎
 */
xyz.swapee.wc.BorderMotionPathHtmlComponentUtil = class extends xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil { }
xyz.swapee.wc.BorderMotionPathHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.BorderMotionPathHtmlComponentUtil

/**
 * Fields of the IBorderMotionPathHtmlComponentUtil.
 * @interface xyz.swapee.wc.IBorderMotionPathHtmlComponentUtilFields
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil} */
xyz.swapee.wc.RecordIBorderMotionPathHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil} xyz.swapee.wc.BoundIBorderMotionPathHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.BorderMotionPathHtmlComponentUtil} xyz.swapee.wc.BoundBorderMotionPathHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterNet
 * @prop {typeof com.webcircuits.ui.IClientRectPort} ClientRect
 * @prop {typeof xyz.swapee.wc.IBorderMotionPathPort} BorderMotionPath The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterCores
 * @prop {!com.webcircuits.ui.ClientRectMemory} ClientRect
 * @prop {!xyz.swapee.wc.BorderMotionPathMemory} BorderMotionPath
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterPorts
 * @prop {!com.webcircuits.ui.IClientRect.Pinout} ClientRect
 * @prop {!xyz.swapee.wc.IBorderMotionPath.Pinout} BorderMotionPath
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.__router<!xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil>} xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `ClientRect` _typeof com.webcircuits.ui.IClientRectPort_
 * - `BorderMotionPath` _typeof IBorderMotionPathPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `ClientRect` _!com.webcircuits.ui.ClientRectMemory_
 * - `BorderMotionPath` _!BorderMotionPathMemory_
 * @param {!xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `ClientRect` _!com.webcircuits.ui.IClientRect.Pinout_
 * - `BorderMotionPath` _!IBorderMotionPath.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/12-IBorderMotionPathHtmlComponent.xml}  1f68695d5b27bf0a3fa06207e365df7a */
/** @typedef {xyz.swapee.wc.back.IBorderMotionPathController.Initialese&xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese&xyz.swapee.wc.IBorderMotionPath.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IBorderMotionPathGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IBorderMotionPathProcessor.Initialese&xyz.swapee.wc.IBorderMotionPathComputer.Initialese} xyz.swapee.wc.IBorderMotionPathHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathHtmlComponent)} xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathHtmlComponent} xyz.swapee.wc.BorderMotionPathHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent
 */
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.constructor&xyz.swapee.wc.BorderMotionPathHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathHtmlComponent|typeof xyz.swapee.wc.BorderMotionPathHtmlComponent)|(!xyz.swapee.wc.back.IBorderMotionPathController|typeof xyz.swapee.wc.back.BorderMotionPathController)|(!xyz.swapee.wc.back.IBorderMotionPathScreen|typeof xyz.swapee.wc.back.BorderMotionPathScreen)|(!xyz.swapee.wc.IBorderMotionPath|typeof xyz.swapee.wc.BorderMotionPath)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IBorderMotionPathGPU|typeof xyz.swapee.wc.BorderMotionPathGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathHtmlComponent|typeof xyz.swapee.wc.BorderMotionPathHtmlComponent)|(!xyz.swapee.wc.back.IBorderMotionPathController|typeof xyz.swapee.wc.back.BorderMotionPathController)|(!xyz.swapee.wc.back.IBorderMotionPathScreen|typeof xyz.swapee.wc.back.BorderMotionPathScreen)|(!xyz.swapee.wc.IBorderMotionPath|typeof xyz.swapee.wc.BorderMotionPath)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IBorderMotionPathGPU|typeof xyz.swapee.wc.BorderMotionPathGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathHtmlComponent|typeof xyz.swapee.wc.BorderMotionPathHtmlComponent)|(!xyz.swapee.wc.back.IBorderMotionPathController|typeof xyz.swapee.wc.back.BorderMotionPathController)|(!xyz.swapee.wc.back.IBorderMotionPathScreen|typeof xyz.swapee.wc.back.BorderMotionPathScreen)|(!xyz.swapee.wc.IBorderMotionPath|typeof xyz.swapee.wc.BorderMotionPath)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IBorderMotionPathGPU|typeof xyz.swapee.wc.BorderMotionPathGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IBorderMotionPathProcessor|typeof xyz.swapee.wc.BorderMotionPathProcessor)|(!xyz.swapee.wc.IBorderMotionPathComputer|typeof xyz.swapee.wc.BorderMotionPathComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractBorderMotionPathHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathHtmlComponent.Initialese[]) => xyz.swapee.wc.IBorderMotionPathHtmlComponent} xyz.swapee.wc.BorderMotionPathHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathHtmlComponentCaster&xyz.swapee.wc.back.IBorderMotionPathController&xyz.swapee.wc.back.IBorderMotionPathScreen&xyz.swapee.wc.IBorderMotionPath&com.webcircuits.ILanded<!xyz.swapee.wc.BorderMotionPathLand>&xyz.swapee.wc.IBorderMotionPathGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.IBorderMotionPathController.Inputs, !HTMLDivElement, !xyz.swapee.wc.BorderMotionPathLand>&xyz.swapee.wc.IBorderMotionPathProcessor&xyz.swapee.wc.IBorderMotionPathComputer)} xyz.swapee.wc.IBorderMotionPathHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IBorderMotionPathController} xyz.swapee.wc.back.IBorderMotionPathController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IBorderMotionPathScreen} xyz.swapee.wc.back.IBorderMotionPathScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathGPU} xyz.swapee.wc.IBorderMotionPathGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IBorderMotionPath_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IBorderMotionPathHtmlComponent
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponent = class extends /** @type {xyz.swapee.wc.IBorderMotionPathHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IBorderMotionPathController.typeof&xyz.swapee.wc.back.IBorderMotionPathScreen.typeof&xyz.swapee.wc.IBorderMotionPath.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IBorderMotionPathGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IBorderMotionPathProcessor.typeof&xyz.swapee.wc.IBorderMotionPathComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathHtmlComponent.Initialese>)} xyz.swapee.wc.BorderMotionPathHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathHtmlComponent} xyz.swapee.wc.IBorderMotionPathHtmlComponent.typeof */
/**
 * A concrete class of _IBorderMotionPathHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathHtmlComponent
 * @implements {xyz.swapee.wc.IBorderMotionPathHtmlComponent} The _IBorderMotionPath_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathHtmlComponent = class extends /** @type {xyz.swapee.wc.BorderMotionPathHtmlComponent.constructor&xyz.swapee.wc.IBorderMotionPathHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathHtmlComponent}
 */
xyz.swapee.wc.BorderMotionPathHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IBorderMotionPathHtmlComponent} */
xyz.swapee.wc.RecordIBorderMotionPathHtmlComponent

/** @typedef {xyz.swapee.wc.IBorderMotionPathHtmlComponent} xyz.swapee.wc.BoundIBorderMotionPathHtmlComponent */

/** @typedef {xyz.swapee.wc.BorderMotionPathHtmlComponent} xyz.swapee.wc.BoundBorderMotionPathHtmlComponent */

/**
 * Contains getters to cast the _IBorderMotionPathHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathHtmlComponentCaster
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponentCaster = class { }
/**
 * Cast the _IBorderMotionPathHtmlComponent_ instance into the _BoundIBorderMotionPathHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathHtmlComponent}
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponentCaster.prototype.asIBorderMotionPathHtmlComponent
/**
 * Access the _BorderMotionPathHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathHtmlComponent}
 */
xyz.swapee.wc.IBorderMotionPathHtmlComponentCaster.prototype.superBorderMotionPathHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/130-IBorderMotionPathElement.xml}  46e44dc0dc1c84373075f9e59755afd3 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.BorderMotionPathLand>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.IBorderMotionPathElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IBorderMotionPathElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathElement)} xyz.swapee.wc.AbstractBorderMotionPathElement.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathElement} xyz.swapee.wc.BorderMotionPathElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathElement` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathElement
 */
xyz.swapee.wc.AbstractBorderMotionPathElement = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathElement.constructor&xyz.swapee.wc.BorderMotionPathElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathElement.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathElement.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathElement|typeof xyz.swapee.wc.BorderMotionPathElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathElement}
 */
xyz.swapee.wc.AbstractBorderMotionPathElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElement}
 */
xyz.swapee.wc.AbstractBorderMotionPathElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathElement|typeof xyz.swapee.wc.BorderMotionPathElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElement}
 */
xyz.swapee.wc.AbstractBorderMotionPathElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathElement|typeof xyz.swapee.wc.BorderMotionPathElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElement}
 */
xyz.swapee.wc.AbstractBorderMotionPathElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathElement.Initialese[]) => xyz.swapee.wc.IBorderMotionPathElement} xyz.swapee.wc.BorderMotionPathElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathElementFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.IBorderMotionPathElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.IBorderMotionPathElement.Inputs, !xyz.swapee.wc.BorderMotionPathLand>&com.webcircuits.ILanded<!xyz.swapee.wc.BorderMotionPathLand>)} xyz.swapee.wc.IBorderMotionPathElement.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * A component description.
 *
 * The _IBorderMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IBorderMotionPathElement
 */
xyz.swapee.wc.IBorderMotionPathElement = class extends /** @type {xyz.swapee.wc.IBorderMotionPathElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IBorderMotionPathElement.solder} */
xyz.swapee.wc.IBorderMotionPathElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathElement.render} */
xyz.swapee.wc.IBorderMotionPathElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathElement.build} */
xyz.swapee.wc.IBorderMotionPathElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathElement.buildClientRect} */
xyz.swapee.wc.IBorderMotionPathElement.prototype.buildClientRect = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathElement.short} */
xyz.swapee.wc.IBorderMotionPathElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathElement.server} */
xyz.swapee.wc.IBorderMotionPathElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IBorderMotionPathElement.inducer} */
xyz.swapee.wc.IBorderMotionPathElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathElement&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathElement.Initialese>)} xyz.swapee.wc.BorderMotionPathElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement} xyz.swapee.wc.IBorderMotionPathElement.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IBorderMotionPathElement_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathElement
 * @implements {xyz.swapee.wc.IBorderMotionPathElement} A component description.
 *
 * The _IBorderMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathElement.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathElement = class extends /** @type {xyz.swapee.wc.BorderMotionPathElement.constructor&xyz.swapee.wc.IBorderMotionPathElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathElement}
 */
xyz.swapee.wc.BorderMotionPathElement.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathElement.
 * @interface xyz.swapee.wc.IBorderMotionPathElementFields
 */
xyz.swapee.wc.IBorderMotionPathElementFields = class { }
/**
 * The available customisations of _IBorderMotionPath_.
 */
xyz.swapee.wc.IBorderMotionPathElementFields.prototype.customs = /** @type {!xyz.swapee.wc.IBorderMotionPathElement.Customs} */ (void 0)
/**
 * The element-specific inputs to the _IBorderMotionPath_ component.
 */
xyz.swapee.wc.IBorderMotionPathElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IBorderMotionPathElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IBorderMotionPathElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathElement} */
xyz.swapee.wc.RecordIBorderMotionPathElement

/** @typedef {xyz.swapee.wc.IBorderMotionPathElement} xyz.swapee.wc.BoundIBorderMotionPathElement */

/** @typedef {xyz.swapee.wc.BorderMotionPathElement} xyz.swapee.wc.BoundBorderMotionPathElement */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElement.Customs The available customisations of _IBorderMotionPath_.
 * @prop {string} [COLOR="#a70dfe"] Default `#a70dfe`.
 * @prop {string} [DIRECTION="normal"] Default `normal`.
 * @prop {number} [PARTICLE_SIZE=3] Default `3`.
 */

/** @typedef {xyz.swapee.wc.IBorderMotionPathPort.Inputs&xyz.swapee.wc.IBorderMotionPathDisplay.Queries&xyz.swapee.wc.IBorderMotionPathController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs} xyz.swapee.wc.IBorderMotionPathElement.Inputs The element-specific inputs to the _IBorderMotionPath_ component. */

/**
 * Contains getters to cast the _IBorderMotionPathElement_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathElementCaster
 */
xyz.swapee.wc.IBorderMotionPathElementCaster = class { }
/**
 * Cast the _IBorderMotionPathElement_ instance into the _BoundIBorderMotionPathElement_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathElement}
 */
xyz.swapee.wc.IBorderMotionPathElementCaster.prototype.asIBorderMotionPathElement
/**
 * Access the _BorderMotionPathElement_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathElement}
 */
xyz.swapee.wc.IBorderMotionPathElementCaster.prototype.superBorderMotionPathElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.BorderMotionPathMemory, props: !xyz.swapee.wc.IBorderMotionPathElement.Inputs) => Object<string, *>} xyz.swapee.wc.IBorderMotionPathElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathElement.__solder<!xyz.swapee.wc.IBorderMotionPathElement>} xyz.swapee.wc.IBorderMotionPathElement._solder */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.BorderMotionPathMemory} model The model.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0.01`.
 * - `count` _number_ How manu particles to add. Default `12`.
 * - `borderRadius` _number_ The border radius. Default `10`.
 * - `delay` _number_ The delay. Default `0`.
 * - `path` _string_ The path computed by JavaScript for the border. Default empty string.
 * - `svgPath` _string_ The path computed by JavaScript for the border. Default empty string.
 * @param {!xyz.swapee.wc.IBorderMotionPathElement.Inputs} props The element props.
 * - `[gap="0.01"]` _&#42;?_ The gap in terms of delay between moving particles. ⤴ *IBorderMotionPathOuterCore.WeakModel.Gap* ⤴ *IBorderMotionPathOuterCore.WeakModel.Gap* Default `0.01`.
 * - `[count=12]` _&#42;?_ How manu particles to add. ⤴ *IBorderMotionPathOuterCore.WeakModel.Count* ⤴ *IBorderMotionPathOuterCore.WeakModel.Count* Default `12`.
 * - `[borderRadius=10]` _&#42;?_ The border radius. ⤴ *IBorderMotionPathOuterCore.WeakModel.BorderRadius* ⤴ *IBorderMotionPathOuterCore.WeakModel.BorderRadius* Default `10`.
 * - `[delay=null]` _&#42;?_ The delay. ⤴ *IBorderMotionPathOuterCore.WeakModel.Delay* ⤴ *IBorderMotionPathOuterCore.WeakModel.Delay* Default `null`.
 * - `[path=null]` _&#42;?_ The path computed by JavaScript for the border. ⤴ *IBorderMotionPathOuterCore.WeakModel.Path* ⤴ *IBorderMotionPathOuterCore.WeakModel.Path* Default `null`.
 * - `[svgPath=null]` _&#42;?_ The path computed by JavaScript for the border. ⤴ *IBorderMotionPathOuterCore.WeakModel.SvgPath* ⤴ *IBorderMotionPathOuterCore.WeakModel.SvgPath* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IBorderMotionPathElementPort.Inputs.NoSolder* Default `false`.
 * - `[particlesWrOpts]` _!Object?_ The options to pass to the _ParticlesWr_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.ParticlesWrOpts* Default `{}`.
 * - `[pathOpts]` _!Object?_ The options to pass to the _Path_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.PathOpts* Default `{}`.
 * - `[clientRectOpts]` _!Object?_ The options to pass to the _ClientRect_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.ClientRectOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IBorderMotionPathElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.BorderMotionPathMemory, instance?: !xyz.swapee.wc.IBorderMotionPathScreen&xyz.swapee.wc.IBorderMotionPathController) => !engineering.type.VNode} xyz.swapee.wc.IBorderMotionPathElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathElement.__render<!xyz.swapee.wc.IBorderMotionPathElement>} xyz.swapee.wc.IBorderMotionPathElement._render */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.BorderMotionPathMemory} [model] The model for the view.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0.01`.
 * - `count` _number_ How manu particles to add. Default `12`.
 * - `borderRadius` _number_ The border radius. Default `10`.
 * - `delay` _number_ The delay. Default `0`.
 * - `path` _string_ The path computed by JavaScript for the border. Default empty string.
 * - `svgPath` _string_ The path computed by JavaScript for the border. Default empty string.
 * @param {!xyz.swapee.wc.IBorderMotionPathScreen&xyz.swapee.wc.IBorderMotionPathController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IBorderMotionPathElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IBorderMotionPathElement.build.Cores, instances: !xyz.swapee.wc.IBorderMotionPathElement.build.Instances) => ?} xyz.swapee.wc.IBorderMotionPathElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathElement.__build<!xyz.swapee.wc.IBorderMotionPathElement>} xyz.swapee.wc.IBorderMotionPathElement._build */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IBorderMotionPathElement.build.Cores} cores The models of components on the land.
 * - `ClientRect` _!com.webcircuits.ui.IClientRectCore.Model_
 * @param {!xyz.swapee.wc.IBorderMotionPathElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `ClientRect` _!com.webcircuits.ui.IClientRect_
 * @return {?}
 */
xyz.swapee.wc.IBorderMotionPathElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElement.build.Cores The models of components on the land.
 * @prop {!com.webcircuits.ui.IClientRectCore.Model} ClientRect
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!com.webcircuits.ui.IClientRect} ClientRect
 */

/**
 * @typedef {(this: THIS, model: !com.webcircuits.ui.IClientRectCore.Model, instance: !com.webcircuits.ui.IClientRect) => ?} xyz.swapee.wc.IBorderMotionPathElement.__buildClientRect
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathElement.__buildClientRect<!xyz.swapee.wc.IBorderMotionPathElement>} xyz.swapee.wc.IBorderMotionPathElement._buildClientRect */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement.buildClientRect} */
/**
 * Controls the VDUs using the model of the _com.webcircuits.ui.IClientRect_ component.
 * @param {!com.webcircuits.ui.IClientRectCore.Model} model
 * @param {!com.webcircuits.ui.IClientRect} instance
 * @return {?}
 */
xyz.swapee.wc.IBorderMotionPathElement.buildClientRect = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.BorderMotionPathMemory, ports: !xyz.swapee.wc.IBorderMotionPathElement.short.Ports, cores: !xyz.swapee.wc.IBorderMotionPathElement.short.Cores) => ?} xyz.swapee.wc.IBorderMotionPathElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathElement.__short<!xyz.swapee.wc.IBorderMotionPathElement>} xyz.swapee.wc.IBorderMotionPathElement._short */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.BorderMotionPathMemory} model The model from which to feed properties to peer's ports.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0.01`.
 * - `count` _number_ How manu particles to add. Default `12`.
 * - `borderRadius` _number_ The border radius. Default `10`.
 * - `delay` _number_ The delay. Default `0`.
 * - `path` _string_ The path computed by JavaScript for the border. Default empty string.
 * - `svgPath` _string_ The path computed by JavaScript for the border. Default empty string.
 * @param {!xyz.swapee.wc.IBorderMotionPathElement.short.Ports} ports The ports of the peers.
 * - `ClientRect` _typeof com.webcircuits.ui.IClientRectPortInterface_
 * @param {!xyz.swapee.wc.IBorderMotionPathElement.short.Cores} cores The cores of the peers.
 * - `ClientRect` _com.webcircuits.ui.IClientRectCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IBorderMotionPathElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElement.short.Ports The ports of the peers.
 * @prop {typeof com.webcircuits.ui.IClientRectPortInterface} ClientRect
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElement.short.Cores The cores of the peers.
 * @prop {com.webcircuits.ui.IClientRectCore.Model} ClientRect
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.BorderMotionPathMemory, inputs: !xyz.swapee.wc.IBorderMotionPathElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IBorderMotionPathElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathElement.__server<!xyz.swapee.wc.IBorderMotionPathElement>} xyz.swapee.wc.IBorderMotionPathElement._server */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.BorderMotionPathMemory} memory The memory registers.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0.01`.
 * - `count` _number_ How manu particles to add. Default `12`.
 * - `borderRadius` _number_ The border radius. Default `10`.
 * - `delay` _number_ The delay. Default `0`.
 * - `path` _string_ The path computed by JavaScript for the border. Default empty string.
 * - `svgPath` _string_ The path computed by JavaScript for the border. Default empty string.
 * @param {!xyz.swapee.wc.IBorderMotionPathElement.Inputs} inputs The inputs to the port.
 * - `[gap="0.01"]` _&#42;?_ The gap in terms of delay between moving particles. ⤴ *IBorderMotionPathOuterCore.WeakModel.Gap* ⤴ *IBorderMotionPathOuterCore.WeakModel.Gap* Default `0.01`.
 * - `[count=12]` _&#42;?_ How manu particles to add. ⤴ *IBorderMotionPathOuterCore.WeakModel.Count* ⤴ *IBorderMotionPathOuterCore.WeakModel.Count* Default `12`.
 * - `[borderRadius=10]` _&#42;?_ The border radius. ⤴ *IBorderMotionPathOuterCore.WeakModel.BorderRadius* ⤴ *IBorderMotionPathOuterCore.WeakModel.BorderRadius* Default `10`.
 * - `[delay=null]` _&#42;?_ The delay. ⤴ *IBorderMotionPathOuterCore.WeakModel.Delay* ⤴ *IBorderMotionPathOuterCore.WeakModel.Delay* Default `null`.
 * - `[path=null]` _&#42;?_ The path computed by JavaScript for the border. ⤴ *IBorderMotionPathOuterCore.WeakModel.Path* ⤴ *IBorderMotionPathOuterCore.WeakModel.Path* Default `null`.
 * - `[svgPath=null]` _&#42;?_ The path computed by JavaScript for the border. ⤴ *IBorderMotionPathOuterCore.WeakModel.SvgPath* ⤴ *IBorderMotionPathOuterCore.WeakModel.SvgPath* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IBorderMotionPathElementPort.Inputs.NoSolder* Default `false`.
 * - `[particlesWrOpts]` _!Object?_ The options to pass to the _ParticlesWr_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.ParticlesWrOpts* Default `{}`.
 * - `[pathOpts]` _!Object?_ The options to pass to the _Path_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.PathOpts* Default `{}`.
 * - `[clientRectOpts]` _!Object?_ The options to pass to the _ClientRect_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.ClientRectOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IBorderMotionPathElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.BorderMotionPathMemory, port?: !xyz.swapee.wc.IBorderMotionPathElement.Inputs) => ?} xyz.swapee.wc.IBorderMotionPathElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathElement.__inducer<!xyz.swapee.wc.IBorderMotionPathElement>} xyz.swapee.wc.IBorderMotionPathElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.BorderMotionPathMemory} [model] The model of the component into which to induce the state.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0.01`.
 * - `count` _number_ How manu particles to add. Default `12`.
 * - `borderRadius` _number_ The border radius. Default `10`.
 * - `delay` _number_ The delay. Default `0`.
 * - `path` _string_ The path computed by JavaScript for the border. Default empty string.
 * - `svgPath` _string_ The path computed by JavaScript for the border. Default empty string.
 * @param {!xyz.swapee.wc.IBorderMotionPathElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[gap="0.01"]` _&#42;?_ The gap in terms of delay between moving particles. ⤴ *IBorderMotionPathOuterCore.WeakModel.Gap* ⤴ *IBorderMotionPathOuterCore.WeakModel.Gap* Default `0.01`.
 * - `[count=12]` _&#42;?_ How manu particles to add. ⤴ *IBorderMotionPathOuterCore.WeakModel.Count* ⤴ *IBorderMotionPathOuterCore.WeakModel.Count* Default `12`.
 * - `[borderRadius=10]` _&#42;?_ The border radius. ⤴ *IBorderMotionPathOuterCore.WeakModel.BorderRadius* ⤴ *IBorderMotionPathOuterCore.WeakModel.BorderRadius* Default `10`.
 * - `[delay=null]` _&#42;?_ The delay. ⤴ *IBorderMotionPathOuterCore.WeakModel.Delay* ⤴ *IBorderMotionPathOuterCore.WeakModel.Delay* Default `null`.
 * - `[path=null]` _&#42;?_ The path computed by JavaScript for the border. ⤴ *IBorderMotionPathOuterCore.WeakModel.Path* ⤴ *IBorderMotionPathOuterCore.WeakModel.Path* Default `null`.
 * - `[svgPath=null]` _&#42;?_ The path computed by JavaScript for the border. ⤴ *IBorderMotionPathOuterCore.WeakModel.SvgPath* ⤴ *IBorderMotionPathOuterCore.WeakModel.SvgPath* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IBorderMotionPathElementPort.Inputs.NoSolder* Default `false`.
 * - `[particlesWrOpts]` _!Object?_ The options to pass to the _ParticlesWr_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.ParticlesWrOpts* Default `{}`.
 * - `[pathOpts]` _!Object?_ The options to pass to the _Path_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.PathOpts* Default `{}`.
 * - `[clientRectOpts]` _!Object?_ The options to pass to the _ClientRect_ vdu. ⤴ *IBorderMotionPathElementPort.Inputs.ClientRectOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IBorderMotionPathElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IBorderMotionPathElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/140-IBorderMotionPathElementPort.xml}  127d116ce8d25e674e44dd3678abc0d3 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IBorderMotionPathElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathElementPort)} xyz.swapee.wc.AbstractBorderMotionPathElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathElementPort} xyz.swapee.wc.BorderMotionPathElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathElementPort
 */
xyz.swapee.wc.AbstractBorderMotionPathElementPort = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathElementPort.constructor&xyz.swapee.wc.BorderMotionPathElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathElementPort.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathElementPort|typeof xyz.swapee.wc.BorderMotionPathElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathElementPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElementPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathElementPort|typeof xyz.swapee.wc.BorderMotionPathElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElementPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathElementPort|typeof xyz.swapee.wc.BorderMotionPathElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathElementPort}
 */
xyz.swapee.wc.AbstractBorderMotionPathElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathElementPort.Initialese[]) => xyz.swapee.wc.IBorderMotionPathElementPort} xyz.swapee.wc.BorderMotionPathElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IBorderMotionPathElementPort.Inputs>)} xyz.swapee.wc.IBorderMotionPathElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IBorderMotionPathElementPort
 */
xyz.swapee.wc.IBorderMotionPathElementPort = class extends /** @type {xyz.swapee.wc.IBorderMotionPathElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathElementPort.Initialese>)} xyz.swapee.wc.BorderMotionPathElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort} xyz.swapee.wc.IBorderMotionPathElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IBorderMotionPathElementPort_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathElementPort
 * @implements {xyz.swapee.wc.IBorderMotionPathElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathElementPort.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathElementPort = class extends /** @type {xyz.swapee.wc.BorderMotionPathElementPort.constructor&xyz.swapee.wc.IBorderMotionPathElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathElementPort}
 */
xyz.swapee.wc.BorderMotionPathElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathElementPort.
 * @interface xyz.swapee.wc.IBorderMotionPathElementPortFields
 */
xyz.swapee.wc.IBorderMotionPathElementPortFields = class { }
/**
 * The inputs to the _IBorderMotionPathElement_'s controller via its element port.
 */
xyz.swapee.wc.IBorderMotionPathElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IBorderMotionPathElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IBorderMotionPathElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IBorderMotionPathElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathElementPort} */
xyz.swapee.wc.RecordIBorderMotionPathElementPort

/** @typedef {xyz.swapee.wc.IBorderMotionPathElementPort} xyz.swapee.wc.BoundIBorderMotionPathElementPort */

/** @typedef {xyz.swapee.wc.BorderMotionPathElementPort} xyz.swapee.wc.BoundBorderMotionPathElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _ParticlesWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts.particlesWrOpts

/**
 * The options to pass to the _Path_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts.pathOpts

/**
 * The options to pass to the _ClientRect_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts.clientRectOpts

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts)} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts.typeof */
/**
 * The inputs to the _IBorderMotionPathElement_'s controller via its element port.
 * @record xyz.swapee.wc.IBorderMotionPathElementPort.Inputs
 */
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.constructor&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts.typeof&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts.typeof&xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IBorderMotionPathElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ParticlesWrOpts&xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.PathOpts&xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ClientRectOpts)} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ParticlesWrOpts} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ParticlesWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.PathOpts} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.PathOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ClientRectOpts} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ClientRectOpts.typeof */
/**
 * The inputs to the _IBorderMotionPathElement_'s controller via its element port.
 * @record xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs
 */
xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.constructor&xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ParticlesWrOpts.typeof&xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.PathOpts.typeof&xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ClientRectOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs

/**
 * Contains getters to cast the _IBorderMotionPathElementPort_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathElementPortCaster
 */
xyz.swapee.wc.IBorderMotionPathElementPortCaster = class { }
/**
 * Cast the _IBorderMotionPathElementPort_ instance into the _BoundIBorderMotionPathElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathElementPort}
 */
xyz.swapee.wc.IBorderMotionPathElementPortCaster.prototype.asIBorderMotionPathElementPort
/**
 * Access the _BorderMotionPathElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathElementPort}
 */
xyz.swapee.wc.IBorderMotionPathElementPortCaster.prototype.superBorderMotionPathElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts The options to pass to the _ParticlesWr_ vdu (optional overlay).
 * @prop {!Object} [particlesWrOpts] The options to pass to the _ParticlesWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ParticlesWrOpts_Safe The options to pass to the _ParticlesWr_ vdu (required overlay).
 * @prop {!Object} particlesWrOpts The options to pass to the _ParticlesWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts The options to pass to the _Path_ vdu (optional overlay).
 * @prop {!Object} [pathOpts] The options to pass to the _Path_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.PathOpts_Safe The options to pass to the _Path_ vdu (required overlay).
 * @prop {!Object} pathOpts The options to pass to the _Path_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts The options to pass to the _ClientRect_ vdu (optional overlay).
 * @prop {!Object} [clientRectOpts] The options to pass to the _ClientRect_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.Inputs.ClientRectOpts_Safe The options to pass to the _ClientRect_ vdu (required overlay).
 * @prop {!Object} clientRectOpts The options to pass to the _ClientRect_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ParticlesWrOpts The options to pass to the _ParticlesWr_ vdu (optional overlay).
 * @prop {*} [particlesWrOpts=null] The options to pass to the _ParticlesWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ParticlesWrOpts_Safe The options to pass to the _ParticlesWr_ vdu (required overlay).
 * @prop {*} particlesWrOpts The options to pass to the _ParticlesWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.PathOpts The options to pass to the _Path_ vdu (optional overlay).
 * @prop {*} [pathOpts=null] The options to pass to the _Path_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.PathOpts_Safe The options to pass to the _Path_ vdu (required overlay).
 * @prop {*} pathOpts The options to pass to the _Path_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ClientRectOpts The options to pass to the _ClientRect_ vdu (optional overlay).
 * @prop {*} [clientRectOpts=null] The options to pass to the _ClientRect_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathElementPort.WeakInputs.ClientRectOpts_Safe The options to pass to the _ClientRect_ vdu (required overlay).
 * @prop {*} clientRectOpts The options to pass to the _ClientRect_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/170-IBorderMotionPathDesigner.xml}  844247f8d8345b3eec978bdfe41e3d54 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IBorderMotionPathDesigner
 */
xyz.swapee.wc.IBorderMotionPathDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.BorderMotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IBorderMotionPath />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.BorderMotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IBorderMotionPath />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.BorderMotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IBorderMotionPathDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ClientRect` _typeof com.webcircuits.ui.IClientRectController_
   * - `BorderMotionPath` _typeof IBorderMotionPathController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IBorderMotionPathDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ClientRect` _typeof com.webcircuits.ui.IClientRectController_
   * - `BorderMotionPath` _typeof IBorderMotionPathController_
   * - `This` _typeof IBorderMotionPathController_
   * @param {!xyz.swapee.wc.IBorderMotionPathDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ClientRect` _!com.webcircuits.ui.ClientRectMemory_
   * - `BorderMotionPath` _!BorderMotionPathMemory_
   * - `This` _!BorderMotionPathMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.BorderMotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.BorderMotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IBorderMotionPathDesigner.prototype.constructor = xyz.swapee.wc.IBorderMotionPathDesigner

/**
 * A concrete class of _IBorderMotionPathDesigner_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathDesigner
 * @implements {xyz.swapee.wc.IBorderMotionPathDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.BorderMotionPathDesigner = class extends xyz.swapee.wc.IBorderMotionPathDesigner { }
xyz.swapee.wc.BorderMotionPathDesigner.prototype.constructor = xyz.swapee.wc.BorderMotionPathDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof com.webcircuits.ui.IClientRectController} ClientRect
 * @prop {typeof xyz.swapee.wc.IBorderMotionPathController} BorderMotionPath
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof com.webcircuits.ui.IClientRectController} ClientRect
 * @prop {typeof xyz.swapee.wc.IBorderMotionPathController} BorderMotionPath
 * @prop {typeof xyz.swapee.wc.IBorderMotionPathController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!com.webcircuits.ui.ClientRectMemory} ClientRect
 * @prop {!xyz.swapee.wc.BorderMotionPathMemory} BorderMotionPath
 * @prop {!xyz.swapee.wc.BorderMotionPathMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/200-BorderMotionPathLand.xml}  ef79767140f423b00578e3eeb1c40aef */
/**
 * The surrounding of the _IBorderMotionPath_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.BorderMotionPathLand
 */
xyz.swapee.wc.BorderMotionPathLand = class { }
/**
 *
 */
xyz.swapee.wc.BorderMotionPathLand.prototype.ClientRect = /** @type {com.webcircuits.ui.IClientRect} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/40-IBorderMotionPathDisplay.xml}  6af0875b2b63bc1b66d3c8b00c40bed7 */
/**
 * @typedef {Object} $xyz.swapee.wc.IBorderMotionPathDisplay.Initialese
 * @prop {HTMLDivElement} [ParticlesWr]
 * @prop {HTMLDivElement} [Path]
 * @prop {HTMLElement} [ClientRect] The via for the _ClientRect_ peer.
 */
/** @typedef {$xyz.swapee.wc.IBorderMotionPathDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IBorderMotionPathDisplay.Settings>} xyz.swapee.wc.IBorderMotionPathDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathDisplay)} xyz.swapee.wc.AbstractBorderMotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathDisplay} xyz.swapee.wc.BorderMotionPathDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathDisplay
 */
xyz.swapee.wc.AbstractBorderMotionPathDisplay = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathDisplay.constructor&xyz.swapee.wc.BorderMotionPathDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathDisplay.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathDisplay|typeof xyz.swapee.wc.BorderMotionPathDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathDisplay}
 */
xyz.swapee.wc.AbstractBorderMotionPathDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathDisplay}
 */
xyz.swapee.wc.AbstractBorderMotionPathDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathDisplay|typeof xyz.swapee.wc.BorderMotionPathDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathDisplay}
 */
xyz.swapee.wc.AbstractBorderMotionPathDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathDisplay|typeof xyz.swapee.wc.BorderMotionPathDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathDisplay}
 */
xyz.swapee.wc.AbstractBorderMotionPathDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathDisplay.Initialese[]) => xyz.swapee.wc.IBorderMotionPathDisplay} xyz.swapee.wc.BorderMotionPathDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.BorderMotionPathMemory, !HTMLDivElement, !xyz.swapee.wc.IBorderMotionPathDisplay.Settings, xyz.swapee.wc.IBorderMotionPathDisplay.Queries, null>)} xyz.swapee.wc.IBorderMotionPathDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IBorderMotionPath_.
 * @interface xyz.swapee.wc.IBorderMotionPathDisplay
 */
xyz.swapee.wc.IBorderMotionPathDisplay = class extends /** @type {xyz.swapee.wc.IBorderMotionPathDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IBorderMotionPathDisplay.paint} */
xyz.swapee.wc.IBorderMotionPathDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathDisplay.Initialese>)} xyz.swapee.wc.BorderMotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathDisplay} xyz.swapee.wc.IBorderMotionPathDisplay.typeof */
/**
 * A concrete class of _IBorderMotionPathDisplay_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathDisplay
 * @implements {xyz.swapee.wc.IBorderMotionPathDisplay} Display for presenting information from the _IBorderMotionPath_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathDisplay.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathDisplay = class extends /** @type {xyz.swapee.wc.BorderMotionPathDisplay.constructor&xyz.swapee.wc.IBorderMotionPathDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathDisplay}
 */
xyz.swapee.wc.BorderMotionPathDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathDisplay.
 * @interface xyz.swapee.wc.IBorderMotionPathDisplayFields
 */
xyz.swapee.wc.IBorderMotionPathDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IBorderMotionPathDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IBorderMotionPathDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IBorderMotionPathDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IBorderMotionPathDisplay.Queries} */ (void 0)
/**
 * The vars to be used by the display.
 */
xyz.swapee.wc.IBorderMotionPathDisplayFields.prototype.vars = /** @type {!xyz.swapee.wc.IBorderMotionPathDisplay.Vars} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IBorderMotionPathDisplayFields.prototype.ParticlesWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IBorderMotionPathDisplayFields.prototype.Path = /** @type {HTMLDivElement} */ (void 0)
/**
 * The via for the _ClientRect_ peer. Default `null`.
 */
xyz.swapee.wc.IBorderMotionPathDisplayFields.prototype.ClientRect = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathDisplay} */
xyz.swapee.wc.RecordIBorderMotionPathDisplay

/** @typedef {xyz.swapee.wc.IBorderMotionPathDisplay} xyz.swapee.wc.BoundIBorderMotionPathDisplay */

/** @typedef {xyz.swapee.wc.BorderMotionPathDisplay} xyz.swapee.wc.BoundBorderMotionPathDisplay */

/** @typedef {xyz.swapee.wc.IBorderMotionPathDisplay.Queries} xyz.swapee.wc.IBorderMotionPathDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IBorderMotionPathDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * @typedef {Object} xyz.swapee.wc.IBorderMotionPathDisplay.Vars The vars to be used by the display.
 * @prop {string} [varPath=""] The path computed by JavaScript for the border. Default empty string.
 */

/**
 * Contains getters to cast the _IBorderMotionPathDisplay_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathDisplayCaster
 */
xyz.swapee.wc.IBorderMotionPathDisplayCaster = class { }
/**
 * Cast the _IBorderMotionPathDisplay_ instance into the _BoundIBorderMotionPathDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathDisplay}
 */
xyz.swapee.wc.IBorderMotionPathDisplayCaster.prototype.asIBorderMotionPathDisplay
/**
 * Cast the _IBorderMotionPathDisplay_ instance into the _BoundIBorderMotionPathScreen_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathScreen}
 */
xyz.swapee.wc.IBorderMotionPathDisplayCaster.prototype.asIBorderMotionPathScreen
/**
 * Access the _BorderMotionPathDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathDisplay}
 */
xyz.swapee.wc.IBorderMotionPathDisplayCaster.prototype.superBorderMotionPathDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.BorderMotionPathMemory, land: null) => void} xyz.swapee.wc.IBorderMotionPathDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathDisplay.__paint<!xyz.swapee.wc.IBorderMotionPathDisplay>} xyz.swapee.wc.IBorderMotionPathDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.BorderMotionPathMemory} memory The display data.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * - `count` _number_ How manu particles to add. Default `0`.
 * - `borderRadius` _number_ The border radius. Default `0`.
 * - `path` _string_ The path computed by JavaScript for the border. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IBorderMotionPathDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IBorderMotionPathDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/40-IBorderMotionPathDisplayBack.xml}  5c70b07125e5e1ffa331920fe7fc6c35 */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IBorderMotionPathDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [ParticlesWr]
 * @prop {!com.webcircuits.IHtmlTwin} [Path]
 * @prop {!com.webcircuits.IHtmlTwin} [ClientRect] The via for the _ClientRect_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IBorderMotionPathDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.BorderMotionPathClasses>} xyz.swapee.wc.back.IBorderMotionPathDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.BorderMotionPathDisplay)} xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.BorderMotionPathDisplay} xyz.swapee.wc.back.BorderMotionPathDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IBorderMotionPathDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractBorderMotionPathDisplay
 */
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.constructor&xyz.swapee.wc.back.BorderMotionPathDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractBorderMotionPathDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathDisplay|typeof xyz.swapee.wc.back.BorderMotionPathDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractBorderMotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathDisplay|typeof xyz.swapee.wc.back.BorderMotionPathDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathDisplay|typeof xyz.swapee.wc.back.BorderMotionPathDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IBorderMotionPathDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IBorderMotionPathDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.BorderMotionPathClasses, !xyz.swapee.wc.BorderMotionPathLand>)} xyz.swapee.wc.back.IBorderMotionPathDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IBorderMotionPathDisplay
 */
xyz.swapee.wc.back.IBorderMotionPathDisplay = class extends /** @type {xyz.swapee.wc.back.IBorderMotionPathDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IBorderMotionPathDisplay.paint} */
xyz.swapee.wc.back.IBorderMotionPathDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IBorderMotionPathDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathDisplay.Initialese>)} xyz.swapee.wc.back.BorderMotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IBorderMotionPathDisplay} xyz.swapee.wc.back.IBorderMotionPathDisplay.typeof */
/**
 * A concrete class of _IBorderMotionPathDisplay_ instances.
 * @constructor xyz.swapee.wc.back.BorderMotionPathDisplay
 * @implements {xyz.swapee.wc.back.IBorderMotionPathDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.BorderMotionPathDisplay = class extends /** @type {xyz.swapee.wc.back.BorderMotionPathDisplay.constructor&xyz.swapee.wc.back.IBorderMotionPathDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.BorderMotionPathDisplay.prototype.constructor = xyz.swapee.wc.back.BorderMotionPathDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathDisplay}
 */
xyz.swapee.wc.back.BorderMotionPathDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathDisplay.
 * @interface xyz.swapee.wc.back.IBorderMotionPathDisplayFields
 */
xyz.swapee.wc.back.IBorderMotionPathDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IBorderMotionPathDisplayFields.prototype.ParticlesWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IBorderMotionPathDisplayFields.prototype.Path = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ClientRect_ peer.
 */
xyz.swapee.wc.back.IBorderMotionPathDisplayFields.prototype.ClientRect = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathDisplay} */
xyz.swapee.wc.back.RecordIBorderMotionPathDisplay

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathDisplay} xyz.swapee.wc.back.BoundIBorderMotionPathDisplay */

/** @typedef {xyz.swapee.wc.back.BorderMotionPathDisplay} xyz.swapee.wc.back.BoundBorderMotionPathDisplay */

/**
 * Contains getters to cast the _IBorderMotionPathDisplay_ interface.
 * @interface xyz.swapee.wc.back.IBorderMotionPathDisplayCaster
 */
xyz.swapee.wc.back.IBorderMotionPathDisplayCaster = class { }
/**
 * Cast the _IBorderMotionPathDisplay_ instance into the _BoundIBorderMotionPathDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIBorderMotionPathDisplay}
 */
xyz.swapee.wc.back.IBorderMotionPathDisplayCaster.prototype.asIBorderMotionPathDisplay
/**
 * Access the _BorderMotionPathDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundBorderMotionPathDisplay}
 */
xyz.swapee.wc.back.IBorderMotionPathDisplayCaster.prototype.superBorderMotionPathDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.BorderMotionPathMemory, land?: !xyz.swapee.wc.BorderMotionPathLand) => void} xyz.swapee.wc.back.IBorderMotionPathDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IBorderMotionPathDisplay.__paint<!xyz.swapee.wc.back.IBorderMotionPathDisplay>} xyz.swapee.wc.back.IBorderMotionPathDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IBorderMotionPathDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.BorderMotionPathMemory} [memory] The display data.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * - `count` _number_ How manu particles to add. Default `0`.
 * - `borderRadius` _number_ The border radius. Default `0`.
 * - `path` _string_ The path computed by JavaScript for the border. Default empty string.
 * @param {!xyz.swapee.wc.BorderMotionPathLand} [land] The land data.
 * - `ClientRect` _com.webcircuits.ui.IClientRect_
 * @return {void}
 */
xyz.swapee.wc.back.IBorderMotionPathDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IBorderMotionPathDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/41-BorderMotionPathClasses.xml}  67d18d8c5edddbb146ef1d398d09b0fe */
/**
 * The classes of the _IBorderMotionPathDisplay_.
 * @record xyz.swapee.wc.BorderMotionPathClasses
 */
xyz.swapee.wc.BorderMotionPathClasses = class { }
/**
 *
 */
xyz.swapee.wc.BorderMotionPathClasses.prototype.Ready = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.BorderMotionPathClasses.prototype.props = /** @type {xyz.swapee.wc.BorderMotionPathClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/50-IBorderMotionPathController.xml}  2753225462f29e3985da559ee95f522b */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IBorderMotionPathController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IBorderMotionPathController.Inputs, !xyz.swapee.wc.IBorderMotionPathOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel>} xyz.swapee.wc.IBorderMotionPathController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathController)} xyz.swapee.wc.AbstractBorderMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathController} xyz.swapee.wc.BorderMotionPathController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathController` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathController
 */
xyz.swapee.wc.AbstractBorderMotionPathController = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathController.constructor&xyz.swapee.wc.BorderMotionPathController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathController.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathController.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathController}
 */
xyz.swapee.wc.AbstractBorderMotionPathController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathController}
 */
xyz.swapee.wc.AbstractBorderMotionPathController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathController}
 */
xyz.swapee.wc.AbstractBorderMotionPathController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathController}
 */
xyz.swapee.wc.AbstractBorderMotionPathController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathController.Initialese[]) => xyz.swapee.wc.IBorderMotionPathController} xyz.swapee.wc.BorderMotionPathControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IBorderMotionPathController.Inputs, !xyz.swapee.wc.IBorderMotionPathOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IBorderMotionPathOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IBorderMotionPathController.Inputs, !xyz.swapee.wc.IBorderMotionPathController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IBorderMotionPathController.Inputs, !xyz.swapee.wc.BorderMotionPathMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IBorderMotionPathController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IBorderMotionPathController.Inputs>)} xyz.swapee.wc.IBorderMotionPathController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IBorderMotionPathController
 */
xyz.swapee.wc.IBorderMotionPathController = class extends /** @type {xyz.swapee.wc.IBorderMotionPathController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IBorderMotionPathController.resetPort} */
xyz.swapee.wc.IBorderMotionPathController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathController&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathController.Initialese>)} xyz.swapee.wc.BorderMotionPathController.constructor */
/**
 * A concrete class of _IBorderMotionPathController_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathController
 * @implements {xyz.swapee.wc.IBorderMotionPathController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathController.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathController = class extends /** @type {xyz.swapee.wc.BorderMotionPathController.constructor&xyz.swapee.wc.IBorderMotionPathController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathController}
 */
xyz.swapee.wc.BorderMotionPathController.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathController.
 * @interface xyz.swapee.wc.IBorderMotionPathControllerFields
 */
xyz.swapee.wc.IBorderMotionPathControllerFields = class { }
/**
 * The inputs to the _IBorderMotionPath_'s controller.
 */
xyz.swapee.wc.IBorderMotionPathControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IBorderMotionPathController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IBorderMotionPathControllerFields.prototype.props = /** @type {xyz.swapee.wc.IBorderMotionPathController} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathController} */
xyz.swapee.wc.RecordIBorderMotionPathController

/** @typedef {xyz.swapee.wc.IBorderMotionPathController} xyz.swapee.wc.BoundIBorderMotionPathController */

/** @typedef {xyz.swapee.wc.BorderMotionPathController} xyz.swapee.wc.BoundBorderMotionPathController */

/** @typedef {xyz.swapee.wc.IBorderMotionPathPort.Inputs} xyz.swapee.wc.IBorderMotionPathController.Inputs The inputs to the _IBorderMotionPath_'s controller. */

/** @typedef {xyz.swapee.wc.IBorderMotionPathPort.WeakInputs} xyz.swapee.wc.IBorderMotionPathController.WeakInputs The inputs to the _IBorderMotionPath_'s controller. */

/**
 * Contains getters to cast the _IBorderMotionPathController_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathControllerCaster
 */
xyz.swapee.wc.IBorderMotionPathControllerCaster = class { }
/**
 * Cast the _IBorderMotionPathController_ instance into the _BoundIBorderMotionPathController_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathController}
 */
xyz.swapee.wc.IBorderMotionPathControllerCaster.prototype.asIBorderMotionPathController
/**
 * Cast the _IBorderMotionPathController_ instance into the _BoundIBorderMotionPathProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathProcessor}
 */
xyz.swapee.wc.IBorderMotionPathControllerCaster.prototype.asIBorderMotionPathProcessor
/**
 * Access the _BorderMotionPathController_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathController}
 */
xyz.swapee.wc.IBorderMotionPathControllerCaster.prototype.superBorderMotionPathController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IBorderMotionPathController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IBorderMotionPathController.__resetPort<!xyz.swapee.wc.IBorderMotionPathController>} xyz.swapee.wc.IBorderMotionPathController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IBorderMotionPathController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IBorderMotionPathController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/51-IBorderMotionPathControllerFront.xml}  96832b73a2a6c84cfe8e25714a62d9fc */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IBorderMotionPathController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.BorderMotionPathController)} xyz.swapee.wc.front.AbstractBorderMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.BorderMotionPathController} xyz.swapee.wc.front.BorderMotionPathController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IBorderMotionPathController` interface.
 * @constructor xyz.swapee.wc.front.AbstractBorderMotionPathController
 */
xyz.swapee.wc.front.AbstractBorderMotionPathController = class extends /** @type {xyz.swapee.wc.front.AbstractBorderMotionPathController.constructor&xyz.swapee.wc.front.BorderMotionPathController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractBorderMotionPathController.prototype.constructor = xyz.swapee.wc.front.AbstractBorderMotionPathController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractBorderMotionPathController.class = /** @type {typeof xyz.swapee.wc.front.AbstractBorderMotionPathController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathController|typeof xyz.swapee.wc.front.BorderMotionPathController)|(!xyz.swapee.wc.front.IBorderMotionPathControllerAT|typeof xyz.swapee.wc.front.BorderMotionPathControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractBorderMotionPathController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractBorderMotionPathController}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathController}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathController|typeof xyz.swapee.wc.front.BorderMotionPathController)|(!xyz.swapee.wc.front.IBorderMotionPathControllerAT|typeof xyz.swapee.wc.front.BorderMotionPathControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathController}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathController|typeof xyz.swapee.wc.front.BorderMotionPathController)|(!xyz.swapee.wc.front.IBorderMotionPathControllerAT|typeof xyz.swapee.wc.front.BorderMotionPathControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathController}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IBorderMotionPathController.Initialese[]) => xyz.swapee.wc.front.IBorderMotionPathController} xyz.swapee.wc.front.BorderMotionPathControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IBorderMotionPathControllerCaster&xyz.swapee.wc.front.IBorderMotionPathControllerAT)} xyz.swapee.wc.front.IBorderMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IBorderMotionPathControllerAT} xyz.swapee.wc.front.IBorderMotionPathControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IBorderMotionPathController
 */
xyz.swapee.wc.front.IBorderMotionPathController = class extends /** @type {xyz.swapee.wc.front.IBorderMotionPathController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IBorderMotionPathControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IBorderMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IBorderMotionPathController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IBorderMotionPathController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IBorderMotionPathController.Initialese>)} xyz.swapee.wc.front.BorderMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IBorderMotionPathController} xyz.swapee.wc.front.IBorderMotionPathController.typeof */
/**
 * A concrete class of _IBorderMotionPathController_ instances.
 * @constructor xyz.swapee.wc.front.BorderMotionPathController
 * @implements {xyz.swapee.wc.front.IBorderMotionPathController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IBorderMotionPathController.Initialese>} ‎
 */
xyz.swapee.wc.front.BorderMotionPathController = class extends /** @type {xyz.swapee.wc.front.BorderMotionPathController.constructor&xyz.swapee.wc.front.IBorderMotionPathController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IBorderMotionPathController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IBorderMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.BorderMotionPathController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathController}
 */
xyz.swapee.wc.front.BorderMotionPathController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IBorderMotionPathController} */
xyz.swapee.wc.front.RecordIBorderMotionPathController

/** @typedef {xyz.swapee.wc.front.IBorderMotionPathController} xyz.swapee.wc.front.BoundIBorderMotionPathController */

/** @typedef {xyz.swapee.wc.front.BorderMotionPathController} xyz.swapee.wc.front.BoundBorderMotionPathController */

/**
 * Contains getters to cast the _IBorderMotionPathController_ interface.
 * @interface xyz.swapee.wc.front.IBorderMotionPathControllerCaster
 */
xyz.swapee.wc.front.IBorderMotionPathControllerCaster = class { }
/**
 * Cast the _IBorderMotionPathController_ instance into the _BoundIBorderMotionPathController_ type.
 * @type {!xyz.swapee.wc.front.BoundIBorderMotionPathController}
 */
xyz.swapee.wc.front.IBorderMotionPathControllerCaster.prototype.asIBorderMotionPathController
/**
 * Access the _BorderMotionPathController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundBorderMotionPathController}
 */
xyz.swapee.wc.front.IBorderMotionPathControllerCaster.prototype.superBorderMotionPathController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/52-IBorderMotionPathControllerBack.xml}  5bec10798682489abdbd3574bb089ad3 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IBorderMotionPathController.Inputs>&xyz.swapee.wc.IBorderMotionPathController.Initialese} xyz.swapee.wc.back.IBorderMotionPathController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.BorderMotionPathController)} xyz.swapee.wc.back.AbstractBorderMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.BorderMotionPathController} xyz.swapee.wc.back.BorderMotionPathController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IBorderMotionPathController` interface.
 * @constructor xyz.swapee.wc.back.AbstractBorderMotionPathController
 */
xyz.swapee.wc.back.AbstractBorderMotionPathController = class extends /** @type {xyz.swapee.wc.back.AbstractBorderMotionPathController.constructor&xyz.swapee.wc.back.BorderMotionPathController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractBorderMotionPathController.prototype.constructor = xyz.swapee.wc.back.AbstractBorderMotionPathController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractBorderMotionPathController.class = /** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathController|typeof xyz.swapee.wc.back.BorderMotionPathController)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractBorderMotionPathController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractBorderMotionPathController}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathController}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathController|typeof xyz.swapee.wc.back.BorderMotionPathController)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathController}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathController|typeof xyz.swapee.wc.back.BorderMotionPathController)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathController}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IBorderMotionPathController.Initialese[]) => xyz.swapee.wc.back.IBorderMotionPathController} xyz.swapee.wc.back.BorderMotionPathControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IBorderMotionPathControllerCaster&xyz.swapee.wc.IBorderMotionPathController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IBorderMotionPathController.Inputs>)} xyz.swapee.wc.back.IBorderMotionPathController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IBorderMotionPathController
 */
xyz.swapee.wc.back.IBorderMotionPathController = class extends /** @type {xyz.swapee.wc.back.IBorderMotionPathController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IBorderMotionPathController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IBorderMotionPathController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IBorderMotionPathController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathController.Initialese>)} xyz.swapee.wc.back.BorderMotionPathController.constructor */
/**
 * A concrete class of _IBorderMotionPathController_ instances.
 * @constructor xyz.swapee.wc.back.BorderMotionPathController
 * @implements {xyz.swapee.wc.back.IBorderMotionPathController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathController.Initialese>} ‎
 */
xyz.swapee.wc.back.BorderMotionPathController = class extends /** @type {xyz.swapee.wc.back.BorderMotionPathController.constructor&xyz.swapee.wc.back.IBorderMotionPathController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IBorderMotionPathController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.BorderMotionPathController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathController}
 */
xyz.swapee.wc.back.BorderMotionPathController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathController} */
xyz.swapee.wc.back.RecordIBorderMotionPathController

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathController} xyz.swapee.wc.back.BoundIBorderMotionPathController */

/** @typedef {xyz.swapee.wc.back.BorderMotionPathController} xyz.swapee.wc.back.BoundBorderMotionPathController */

/**
 * Contains getters to cast the _IBorderMotionPathController_ interface.
 * @interface xyz.swapee.wc.back.IBorderMotionPathControllerCaster
 */
xyz.swapee.wc.back.IBorderMotionPathControllerCaster = class { }
/**
 * Cast the _IBorderMotionPathController_ instance into the _BoundIBorderMotionPathController_ type.
 * @type {!xyz.swapee.wc.back.BoundIBorderMotionPathController}
 */
xyz.swapee.wc.back.IBorderMotionPathControllerCaster.prototype.asIBorderMotionPathController
/**
 * Access the _BorderMotionPathController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundBorderMotionPathController}
 */
xyz.swapee.wc.back.IBorderMotionPathControllerCaster.prototype.superBorderMotionPathController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/53-IBorderMotionPathControllerAR.xml}  7f8351f1dabc1fae90695b0a393000f9 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IBorderMotionPathController.Initialese} xyz.swapee.wc.back.IBorderMotionPathControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.BorderMotionPathControllerAR)} xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.BorderMotionPathControllerAR} xyz.swapee.wc.back.BorderMotionPathControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IBorderMotionPathControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR
 */
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.constructor&xyz.swapee.wc.back.BorderMotionPathControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathControllerAR|typeof xyz.swapee.wc.back.BorderMotionPathControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathControllerAR|typeof xyz.swapee.wc.back.BorderMotionPathControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathControllerAR|typeof xyz.swapee.wc.back.BorderMotionPathControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IBorderMotionPathController|typeof xyz.swapee.wc.BorderMotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IBorderMotionPathControllerAR.Initialese[]) => xyz.swapee.wc.back.IBorderMotionPathControllerAR} xyz.swapee.wc.back.BorderMotionPathControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IBorderMotionPathControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IBorderMotionPathController)} xyz.swapee.wc.back.IBorderMotionPathControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IBorderMotionPathControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IBorderMotionPathControllerAR
 */
xyz.swapee.wc.back.IBorderMotionPathControllerAR = class extends /** @type {xyz.swapee.wc.back.IBorderMotionPathControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IBorderMotionPathController.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IBorderMotionPathControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IBorderMotionPathControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathControllerAR.Initialese>)} xyz.swapee.wc.back.BorderMotionPathControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IBorderMotionPathControllerAR} xyz.swapee.wc.back.IBorderMotionPathControllerAR.typeof */
/**
 * A concrete class of _IBorderMotionPathControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.BorderMotionPathControllerAR
 * @implements {xyz.swapee.wc.back.IBorderMotionPathControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IBorderMotionPathControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.BorderMotionPathControllerAR = class extends /** @type {xyz.swapee.wc.back.BorderMotionPathControllerAR.constructor&xyz.swapee.wc.back.IBorderMotionPathControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IBorderMotionPathControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.BorderMotionPathControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathControllerAR}
 */
xyz.swapee.wc.back.BorderMotionPathControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathControllerAR} */
xyz.swapee.wc.back.RecordIBorderMotionPathControllerAR

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathControllerAR} xyz.swapee.wc.back.BoundIBorderMotionPathControllerAR */

/** @typedef {xyz.swapee.wc.back.BorderMotionPathControllerAR} xyz.swapee.wc.back.BoundBorderMotionPathControllerAR */

/**
 * Contains getters to cast the _IBorderMotionPathControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IBorderMotionPathControllerARCaster
 */
xyz.swapee.wc.back.IBorderMotionPathControllerARCaster = class { }
/**
 * Cast the _IBorderMotionPathControllerAR_ instance into the _BoundIBorderMotionPathControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIBorderMotionPathControllerAR}
 */
xyz.swapee.wc.back.IBorderMotionPathControllerARCaster.prototype.asIBorderMotionPathControllerAR
/**
 * Access the _BorderMotionPathControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundBorderMotionPathControllerAR}
 */
xyz.swapee.wc.back.IBorderMotionPathControllerARCaster.prototype.superBorderMotionPathControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/54-IBorderMotionPathControllerAT.xml}  fb1d551fd2bf9d92ad9291eddbb773a2 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IBorderMotionPathControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.BorderMotionPathControllerAT)} xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.BorderMotionPathControllerAT} xyz.swapee.wc.front.BorderMotionPathControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IBorderMotionPathControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT
 */
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.constructor&xyz.swapee.wc.front.BorderMotionPathControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathControllerAT|typeof xyz.swapee.wc.front.BorderMotionPathControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathControllerAT|typeof xyz.swapee.wc.front.BorderMotionPathControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathControllerAT|typeof xyz.swapee.wc.front.BorderMotionPathControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IBorderMotionPathControllerAT.Initialese[]) => xyz.swapee.wc.front.IBorderMotionPathControllerAT} xyz.swapee.wc.front.BorderMotionPathControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IBorderMotionPathControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IBorderMotionPathControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IBorderMotionPathControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IBorderMotionPathControllerAT
 */
xyz.swapee.wc.front.IBorderMotionPathControllerAT = class extends /** @type {xyz.swapee.wc.front.IBorderMotionPathControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IBorderMotionPathControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IBorderMotionPathControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IBorderMotionPathControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IBorderMotionPathControllerAT.Initialese>)} xyz.swapee.wc.front.BorderMotionPathControllerAT.constructor */
/**
 * A concrete class of _IBorderMotionPathControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.BorderMotionPathControllerAT
 * @implements {xyz.swapee.wc.front.IBorderMotionPathControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IBorderMotionPathControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IBorderMotionPathControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.BorderMotionPathControllerAT = class extends /** @type {xyz.swapee.wc.front.BorderMotionPathControllerAT.constructor&xyz.swapee.wc.front.IBorderMotionPathControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IBorderMotionPathControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IBorderMotionPathControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.BorderMotionPathControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathControllerAT}
 */
xyz.swapee.wc.front.BorderMotionPathControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IBorderMotionPathControllerAT} */
xyz.swapee.wc.front.RecordIBorderMotionPathControllerAT

/** @typedef {xyz.swapee.wc.front.IBorderMotionPathControllerAT} xyz.swapee.wc.front.BoundIBorderMotionPathControllerAT */

/** @typedef {xyz.swapee.wc.front.BorderMotionPathControllerAT} xyz.swapee.wc.front.BoundBorderMotionPathControllerAT */

/**
 * Contains getters to cast the _IBorderMotionPathControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IBorderMotionPathControllerATCaster
 */
xyz.swapee.wc.front.IBorderMotionPathControllerATCaster = class { }
/**
 * Cast the _IBorderMotionPathControllerAT_ instance into the _BoundIBorderMotionPathControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIBorderMotionPathControllerAT}
 */
xyz.swapee.wc.front.IBorderMotionPathControllerATCaster.prototype.asIBorderMotionPathControllerAT
/**
 * Access the _BorderMotionPathControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundBorderMotionPathControllerAT}
 */
xyz.swapee.wc.front.IBorderMotionPathControllerATCaster.prototype.superBorderMotionPathControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/70-IBorderMotionPathScreen.xml}  417cc0fb7bfaced0288c8b37f6a371a6 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.front.BorderMotionPathInputs, !HTMLDivElement, !xyz.swapee.wc.IBorderMotionPathDisplay.Settings, !xyz.swapee.wc.IBorderMotionPathDisplay.Queries, null>&xyz.swapee.wc.IBorderMotionPathDisplay.Initialese} xyz.swapee.wc.IBorderMotionPathScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathScreen)} xyz.swapee.wc.AbstractBorderMotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathScreen} xyz.swapee.wc.BorderMotionPathScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathScreen` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathScreen
 */
xyz.swapee.wc.AbstractBorderMotionPathScreen = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathScreen.constructor&xyz.swapee.wc.BorderMotionPathScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathScreen.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathScreen.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathScreen|typeof xyz.swapee.wc.BorderMotionPathScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IBorderMotionPathController|typeof xyz.swapee.wc.front.BorderMotionPathController)|(!xyz.swapee.wc.IBorderMotionPathDisplay|typeof xyz.swapee.wc.BorderMotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathScreen}
 */
xyz.swapee.wc.AbstractBorderMotionPathScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathScreen}
 */
xyz.swapee.wc.AbstractBorderMotionPathScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathScreen|typeof xyz.swapee.wc.BorderMotionPathScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IBorderMotionPathController|typeof xyz.swapee.wc.front.BorderMotionPathController)|(!xyz.swapee.wc.IBorderMotionPathDisplay|typeof xyz.swapee.wc.BorderMotionPathDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathScreen}
 */
xyz.swapee.wc.AbstractBorderMotionPathScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathScreen|typeof xyz.swapee.wc.BorderMotionPathScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IBorderMotionPathController|typeof xyz.swapee.wc.front.BorderMotionPathController)|(!xyz.swapee.wc.IBorderMotionPathDisplay|typeof xyz.swapee.wc.BorderMotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathScreen}
 */
xyz.swapee.wc.AbstractBorderMotionPathScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathScreen.Initialese[]) => xyz.swapee.wc.IBorderMotionPathScreen} xyz.swapee.wc.BorderMotionPathScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.BorderMotionPathMemory, !xyz.swapee.wc.front.BorderMotionPathInputs, !HTMLDivElement, !xyz.swapee.wc.IBorderMotionPathDisplay.Settings, !xyz.swapee.wc.IBorderMotionPathDisplay.Queries, null, null>&xyz.swapee.wc.front.IBorderMotionPathController&xyz.swapee.wc.IBorderMotionPathDisplay)} xyz.swapee.wc.IBorderMotionPathScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.front.IBorderMotionPathController} xyz.swapee.wc.front.IBorderMotionPathController.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IBorderMotionPathScreen
 */
xyz.swapee.wc.IBorderMotionPathScreen = class extends /** @type {xyz.swapee.wc.IBorderMotionPathScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IBorderMotionPathController.typeof&xyz.swapee.wc.IBorderMotionPathDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathScreen.Initialese>)} xyz.swapee.wc.BorderMotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IBorderMotionPathScreen} xyz.swapee.wc.IBorderMotionPathScreen.typeof */
/**
 * A concrete class of _IBorderMotionPathScreen_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathScreen
 * @implements {xyz.swapee.wc.IBorderMotionPathScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathScreen.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathScreen = class extends /** @type {xyz.swapee.wc.BorderMotionPathScreen.constructor&xyz.swapee.wc.IBorderMotionPathScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathScreen}
 */
xyz.swapee.wc.BorderMotionPathScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IBorderMotionPathScreen} */
xyz.swapee.wc.RecordIBorderMotionPathScreen

/** @typedef {xyz.swapee.wc.IBorderMotionPathScreen} xyz.swapee.wc.BoundIBorderMotionPathScreen */

/** @typedef {xyz.swapee.wc.BorderMotionPathScreen} xyz.swapee.wc.BoundBorderMotionPathScreen */

/**
 * Contains getters to cast the _IBorderMotionPathScreen_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathScreenCaster
 */
xyz.swapee.wc.IBorderMotionPathScreenCaster = class { }
/**
 * Cast the _IBorderMotionPathScreen_ instance into the _BoundIBorderMotionPathScreen_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathScreen}
 */
xyz.swapee.wc.IBorderMotionPathScreenCaster.prototype.asIBorderMotionPathScreen
/**
 * Access the _BorderMotionPathScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathScreen}
 */
xyz.swapee.wc.IBorderMotionPathScreenCaster.prototype.superBorderMotionPathScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/70-IBorderMotionPathScreenBack.xml}  5a9f39edd96cceb4e5ee07609a35958b */
/** @typedef {xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese} xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.BorderMotionPathScreen)} xyz.swapee.wc.back.AbstractBorderMotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.BorderMotionPathScreen} xyz.swapee.wc.back.BorderMotionPathScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IBorderMotionPathScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractBorderMotionPathScreen
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreen = class extends /** @type {xyz.swapee.wc.back.AbstractBorderMotionPathScreen.constructor&xyz.swapee.wc.back.BorderMotionPathScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractBorderMotionPathScreen.prototype.constructor = xyz.swapee.wc.back.AbstractBorderMotionPathScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathScreen|typeof xyz.swapee.wc.back.BorderMotionPathScreen)|(!xyz.swapee.wc.back.IBorderMotionPathScreenAT|typeof xyz.swapee.wc.back.BorderMotionPathScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreen}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreen}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathScreen|typeof xyz.swapee.wc.back.BorderMotionPathScreen)|(!xyz.swapee.wc.back.IBorderMotionPathScreenAT|typeof xyz.swapee.wc.back.BorderMotionPathScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreen}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathScreen|typeof xyz.swapee.wc.back.BorderMotionPathScreen)|(!xyz.swapee.wc.back.IBorderMotionPathScreenAT|typeof xyz.swapee.wc.back.BorderMotionPathScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreen}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese[]) => xyz.swapee.wc.back.IBorderMotionPathScreen} xyz.swapee.wc.back.BorderMotionPathScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IBorderMotionPathScreenCaster&xyz.swapee.wc.back.IBorderMotionPathScreenAT)} xyz.swapee.wc.back.IBorderMotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IBorderMotionPathScreenAT} xyz.swapee.wc.back.IBorderMotionPathScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IBorderMotionPathScreen
 */
xyz.swapee.wc.back.IBorderMotionPathScreen = class extends /** @type {xyz.swapee.wc.back.IBorderMotionPathScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IBorderMotionPathScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IBorderMotionPathScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IBorderMotionPathScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese>)} xyz.swapee.wc.back.BorderMotionPathScreen.constructor */
/**
 * A concrete class of _IBorderMotionPathScreen_ instances.
 * @constructor xyz.swapee.wc.back.BorderMotionPathScreen
 * @implements {xyz.swapee.wc.back.IBorderMotionPathScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.BorderMotionPathScreen = class extends /** @type {xyz.swapee.wc.back.BorderMotionPathScreen.constructor&xyz.swapee.wc.back.IBorderMotionPathScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.BorderMotionPathScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreen}
 */
xyz.swapee.wc.back.BorderMotionPathScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathScreen} */
xyz.swapee.wc.back.RecordIBorderMotionPathScreen

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathScreen} xyz.swapee.wc.back.BoundIBorderMotionPathScreen */

/** @typedef {xyz.swapee.wc.back.BorderMotionPathScreen} xyz.swapee.wc.back.BoundBorderMotionPathScreen */

/**
 * Contains getters to cast the _IBorderMotionPathScreen_ interface.
 * @interface xyz.swapee.wc.back.IBorderMotionPathScreenCaster
 */
xyz.swapee.wc.back.IBorderMotionPathScreenCaster = class { }
/**
 * Cast the _IBorderMotionPathScreen_ instance into the _BoundIBorderMotionPathScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIBorderMotionPathScreen}
 */
xyz.swapee.wc.back.IBorderMotionPathScreenCaster.prototype.asIBorderMotionPathScreen
/**
 * Access the _BorderMotionPathScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundBorderMotionPathScreen}
 */
xyz.swapee.wc.back.IBorderMotionPathScreenCaster.prototype.superBorderMotionPathScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/73-IBorderMotionPathScreenAR.xml}  49e738ab1db58a590adbd3fc5c18e85e */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IBorderMotionPathScreen.Initialese} xyz.swapee.wc.front.IBorderMotionPathScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.BorderMotionPathScreenAR)} xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.BorderMotionPathScreenAR} xyz.swapee.wc.front.BorderMotionPathScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IBorderMotionPathScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR
 */
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.constructor&xyz.swapee.wc.front.BorderMotionPathScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathScreenAR|typeof xyz.swapee.wc.front.BorderMotionPathScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IBorderMotionPathScreen|typeof xyz.swapee.wc.BorderMotionPathScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathScreenAR|typeof xyz.swapee.wc.front.BorderMotionPathScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IBorderMotionPathScreen|typeof xyz.swapee.wc.BorderMotionPathScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IBorderMotionPathScreenAR|typeof xyz.swapee.wc.front.BorderMotionPathScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IBorderMotionPathScreen|typeof xyz.swapee.wc.BorderMotionPathScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractBorderMotionPathScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IBorderMotionPathScreenAR.Initialese[]) => xyz.swapee.wc.front.IBorderMotionPathScreenAR} xyz.swapee.wc.front.BorderMotionPathScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IBorderMotionPathScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IBorderMotionPathScreen)} xyz.swapee.wc.front.IBorderMotionPathScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IBorderMotionPathScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IBorderMotionPathScreenAR
 */
xyz.swapee.wc.front.IBorderMotionPathScreenAR = class extends /** @type {xyz.swapee.wc.front.IBorderMotionPathScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IBorderMotionPathScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IBorderMotionPathScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IBorderMotionPathScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IBorderMotionPathScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IBorderMotionPathScreenAR.Initialese>)} xyz.swapee.wc.front.BorderMotionPathScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IBorderMotionPathScreenAR} xyz.swapee.wc.front.IBorderMotionPathScreenAR.typeof */
/**
 * A concrete class of _IBorderMotionPathScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.BorderMotionPathScreenAR
 * @implements {xyz.swapee.wc.front.IBorderMotionPathScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IBorderMotionPathScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IBorderMotionPathScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.BorderMotionPathScreenAR = class extends /** @type {xyz.swapee.wc.front.BorderMotionPathScreenAR.constructor&xyz.swapee.wc.front.IBorderMotionPathScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IBorderMotionPathScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IBorderMotionPathScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.BorderMotionPathScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.BorderMotionPathScreenAR}
 */
xyz.swapee.wc.front.BorderMotionPathScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IBorderMotionPathScreenAR} */
xyz.swapee.wc.front.RecordIBorderMotionPathScreenAR

/** @typedef {xyz.swapee.wc.front.IBorderMotionPathScreenAR} xyz.swapee.wc.front.BoundIBorderMotionPathScreenAR */

/** @typedef {xyz.swapee.wc.front.BorderMotionPathScreenAR} xyz.swapee.wc.front.BoundBorderMotionPathScreenAR */

/**
 * Contains getters to cast the _IBorderMotionPathScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IBorderMotionPathScreenARCaster
 */
xyz.swapee.wc.front.IBorderMotionPathScreenARCaster = class { }
/**
 * Cast the _IBorderMotionPathScreenAR_ instance into the _BoundIBorderMotionPathScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIBorderMotionPathScreenAR}
 */
xyz.swapee.wc.front.IBorderMotionPathScreenARCaster.prototype.asIBorderMotionPathScreenAR
/**
 * Access the _BorderMotionPathScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundBorderMotionPathScreenAR}
 */
xyz.swapee.wc.front.IBorderMotionPathScreenARCaster.prototype.superBorderMotionPathScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/74-IBorderMotionPathScreenAT.xml}  3021f48c1d3a4aab3e58a545a2f9c230 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.BorderMotionPathScreenAT)} xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.BorderMotionPathScreenAT} xyz.swapee.wc.back.BorderMotionPathScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IBorderMotionPathScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.constructor&xyz.swapee.wc.back.BorderMotionPathScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathScreenAT|typeof xyz.swapee.wc.back.BorderMotionPathScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathScreenAT|typeof xyz.swapee.wc.back.BorderMotionPathScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IBorderMotionPathScreenAT|typeof xyz.swapee.wc.back.BorderMotionPathScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractBorderMotionPathScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese[]) => xyz.swapee.wc.back.IBorderMotionPathScreenAT} xyz.swapee.wc.back.BorderMotionPathScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IBorderMotionPathScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IBorderMotionPathScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IBorderMotionPathScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IBorderMotionPathScreenAT
 */
xyz.swapee.wc.back.IBorderMotionPathScreenAT = class extends /** @type {xyz.swapee.wc.back.IBorderMotionPathScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IBorderMotionPathScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IBorderMotionPathScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese>)} xyz.swapee.wc.back.BorderMotionPathScreenAT.constructor */
/**
 * A concrete class of _IBorderMotionPathScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.BorderMotionPathScreenAT
 * @implements {xyz.swapee.wc.back.IBorderMotionPathScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IBorderMotionPathScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.BorderMotionPathScreenAT = class extends /** @type {xyz.swapee.wc.back.BorderMotionPathScreenAT.constructor&xyz.swapee.wc.back.IBorderMotionPathScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IBorderMotionPathScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.BorderMotionPathScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.BorderMotionPathScreenAT}
 */
xyz.swapee.wc.back.BorderMotionPathScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathScreenAT} */
xyz.swapee.wc.back.RecordIBorderMotionPathScreenAT

/** @typedef {xyz.swapee.wc.back.IBorderMotionPathScreenAT} xyz.swapee.wc.back.BoundIBorderMotionPathScreenAT */

/** @typedef {xyz.swapee.wc.back.BorderMotionPathScreenAT} xyz.swapee.wc.back.BoundBorderMotionPathScreenAT */

/**
 * Contains getters to cast the _IBorderMotionPathScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IBorderMotionPathScreenATCaster
 */
xyz.swapee.wc.back.IBorderMotionPathScreenATCaster = class { }
/**
 * Cast the _IBorderMotionPathScreenAT_ instance into the _BoundIBorderMotionPathScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIBorderMotionPathScreenAT}
 */
xyz.swapee.wc.back.IBorderMotionPathScreenATCaster.prototype.asIBorderMotionPathScreenAT
/**
 * Access the _BorderMotionPathScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundBorderMotionPathScreenAT}
 */
xyz.swapee.wc.back.IBorderMotionPathScreenATCaster.prototype.superBorderMotionPathScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/border-motion-path/BorderMotionPath.mvc/design/80-IBorderMotionPathGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IBorderMotionPathDisplay.Initialese} xyz.swapee.wc.IBorderMotionPathGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.BorderMotionPathGPU)} xyz.swapee.wc.AbstractBorderMotionPathGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.BorderMotionPathGPU} xyz.swapee.wc.BorderMotionPathGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IBorderMotionPathGPU` interface.
 * @constructor xyz.swapee.wc.AbstractBorderMotionPathGPU
 */
xyz.swapee.wc.AbstractBorderMotionPathGPU = class extends /** @type {xyz.swapee.wc.AbstractBorderMotionPathGPU.constructor&xyz.swapee.wc.BorderMotionPathGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractBorderMotionPathGPU.prototype.constructor = xyz.swapee.wc.AbstractBorderMotionPathGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractBorderMotionPathGPU.class = /** @type {typeof xyz.swapee.wc.AbstractBorderMotionPathGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathGPU|typeof xyz.swapee.wc.BorderMotionPathGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IBorderMotionPathDisplay|typeof xyz.swapee.wc.back.BorderMotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractBorderMotionPathGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractBorderMotionPathGPU}
 */
xyz.swapee.wc.AbstractBorderMotionPathGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathGPU}
 */
xyz.swapee.wc.AbstractBorderMotionPathGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathGPU|typeof xyz.swapee.wc.BorderMotionPathGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IBorderMotionPathDisplay|typeof xyz.swapee.wc.back.BorderMotionPathDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathGPU}
 */
xyz.swapee.wc.AbstractBorderMotionPathGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IBorderMotionPathGPU|typeof xyz.swapee.wc.BorderMotionPathGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IBorderMotionPathDisplay|typeof xyz.swapee.wc.back.BorderMotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.BorderMotionPathGPU}
 */
xyz.swapee.wc.AbstractBorderMotionPathGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IBorderMotionPathGPU.Initialese[]) => xyz.swapee.wc.IBorderMotionPathGPU} xyz.swapee.wc.BorderMotionPathGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IBorderMotionPathGPUCaster&com.webcircuits.IBrowserView<.!BorderMotionPathMemory,>&xyz.swapee.wc.back.IBorderMotionPathDisplay)} xyz.swapee.wc.IBorderMotionPathGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!BorderMotionPathMemory,>} com.webcircuits.IBrowserView<.!BorderMotionPathMemory,>.typeof */
/**
 * Handles the periphery of the _IBorderMotionPathDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IBorderMotionPathGPU
 */
xyz.swapee.wc.IBorderMotionPathGPU = class extends /** @type {xyz.swapee.wc.IBorderMotionPathGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!BorderMotionPathMemory,>.typeof&xyz.swapee.wc.back.IBorderMotionPathDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IBorderMotionPathGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IBorderMotionPathGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IBorderMotionPathGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathGPU.Initialese>)} xyz.swapee.wc.BorderMotionPathGPU.constructor */
/**
 * A concrete class of _IBorderMotionPathGPU_ instances.
 * @constructor xyz.swapee.wc.BorderMotionPathGPU
 * @implements {xyz.swapee.wc.IBorderMotionPathGPU} Handles the periphery of the _IBorderMotionPathDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IBorderMotionPathGPU.Initialese>} ‎
 */
xyz.swapee.wc.BorderMotionPathGPU = class extends /** @type {xyz.swapee.wc.BorderMotionPathGPU.constructor&xyz.swapee.wc.IBorderMotionPathGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IBorderMotionPathGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IBorderMotionPathGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IBorderMotionPathGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IBorderMotionPathGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.BorderMotionPathGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.BorderMotionPathGPU}
 */
xyz.swapee.wc.BorderMotionPathGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IBorderMotionPathGPU.
 * @interface xyz.swapee.wc.IBorderMotionPathGPUFields
 */
xyz.swapee.wc.IBorderMotionPathGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IBorderMotionPathGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IBorderMotionPathGPU} */
xyz.swapee.wc.RecordIBorderMotionPathGPU

/** @typedef {xyz.swapee.wc.IBorderMotionPathGPU} xyz.swapee.wc.BoundIBorderMotionPathGPU */

/** @typedef {xyz.swapee.wc.BorderMotionPathGPU} xyz.swapee.wc.BoundBorderMotionPathGPU */

/**
 * Contains getters to cast the _IBorderMotionPathGPU_ interface.
 * @interface xyz.swapee.wc.IBorderMotionPathGPUCaster
 */
xyz.swapee.wc.IBorderMotionPathGPUCaster = class { }
/**
 * Cast the _IBorderMotionPathGPU_ instance into the _BoundIBorderMotionPathGPU_ type.
 * @type {!xyz.swapee.wc.BoundIBorderMotionPathGPU}
 */
xyz.swapee.wc.IBorderMotionPathGPUCaster.prototype.asIBorderMotionPathGPU
/**
 * Access the _BorderMotionPathGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundBorderMotionPathGPU}
 */
xyz.swapee.wc.IBorderMotionPathGPUCaster.prototype.superBorderMotionPathGPU

// nss:xyz.swapee.wc
/* @typal-end */