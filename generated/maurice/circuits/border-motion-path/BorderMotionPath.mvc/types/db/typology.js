/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IBorderMotionPathComputer': {
  'id': 96314666471,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptPath': 2
  }
 },
 'xyz.swapee.wc.BorderMotionPathMemoryPQs': {
  'id': 96314666472,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IBorderMotionPathOuterCore': {
  'id': 96314666473,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.BorderMotionPathInputsPQs': {
  'id': 96314666474,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IBorderMotionPathPort': {
  'id': 96314666475,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetBorderMotionPathPort': 2
  }
 },
 'xyz.swapee.wc.IBorderMotionPathPortInterface': {
  'id': 96314666476,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathCore': {
  'id': 96314666477,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetBorderMotionPathCore': 2
  }
 },
 'xyz.swapee.wc.IBorderMotionPathProcessor': {
  'id': 96314666478,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPath': {
  'id': 96314666479,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathBuffer': {
  'id': 963146664710,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathHtmlComponent': {
  'id': 963146664711,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathElement': {
  'id': 963146664712,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4,
   'build': 5,
   'buildClientRect': 6,
   'short': 7
  }
 },
 'xyz.swapee.wc.IBorderMotionPathElementPort': {
  'id': 963146664713,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathDesigner': {
  'id': 963146664714,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IBorderMotionPathGPU': {
  'id': 963146664715,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathDisplay': {
  'id': 963146664716,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.BorderMotionPathVdusPQs': {
  'id': 963146664717,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IBorderMotionPathDisplay': {
  'id': 963146664718,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IBorderMotionPathController': {
  'id': 963146664719,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IBorderMotionPathController': {
  'id': 963146664720,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathController': {
  'id': 963146664721,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathControllerAR': {
  'id': 963146664722,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IBorderMotionPathControllerAT': {
  'id': 963146664723,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathScreen': {
  'id': 963146664724,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathScreen': {
  'id': 963146664725,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IBorderMotionPathScreenAR': {
  'id': 963146664726,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathScreenAT': {
  'id': 963146664727,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathScreenJoinpointModelHyperslice': {
  'id': 963146664728,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathScreenJoinpointModelBindingHyperslice': {
  'id': 963146664729,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathScreenJoinpointModel': {
  'id': 963146664730,
  'symbols': {},
  'methods': {
   'beforeSyncPath': 1,
   'afterSyncPath': 2,
   'afterSyncPathThrows': 3,
   'afterSyncPathReturns': 4,
   'afterSyncPathCancels': 5
  }
 },
 'xyz.swapee.wc.IBorderMotionPathScreenAspectsInstaller': {
  'id': 963146664731,
  'symbols': {},
  'methods': {
   'syncPath': 1
  }
 },
 'xyz.swapee.wc.IHyperBorderMotionPathScreen': {
  'id': 963146664732,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathScreenAspects': {
  'id': 963146664733,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil': {
  'id': 963146664734,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.BorderMotionPathClassesPQs': {
  'id': 963146664735,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})