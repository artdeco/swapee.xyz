import AbstractHyperSwapeeSocialButtonsInterruptLine from '../../../../gen/AbstractSwapeeSocialButtonsInterruptLine/hyper/AbstractHyperSwapeeSocialButtonsInterruptLine'
import SwapeeSocialButtonsInterruptLine from '../../SwapeeSocialButtonsInterruptLine'
import SwapeeSocialButtonsInterruptLineGeneralAspects from '../SwapeeSocialButtonsInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperSwapeeSocialButtonsInterruptLine} */
export default class extends AbstractHyperSwapeeSocialButtonsInterruptLine
 .consults(
  SwapeeSocialButtonsInterruptLineGeneralAspects,
 )
 .implements(
  SwapeeSocialButtonsInterruptLine,
 )
{}