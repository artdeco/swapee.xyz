import deduceInputs from './methods/deduce-inputs'
import AbstractSwapeeSocialButtonsControllerAT from '../../gen/AbstractSwapeeSocialButtonsControllerAT'
import SwapeeSocialButtonsDisplay from '../SwapeeSocialButtonsDisplay'
import AbstractSwapeeSocialButtonsScreen from '../../gen/AbstractSwapeeSocialButtonsScreen'

/** @extends {xyz.swapee.wc.SwapeeSocialButtonsScreen} */
export default class extends AbstractSwapeeSocialButtonsScreen.implements(
 AbstractSwapeeSocialButtonsControllerAT,
 SwapeeSocialButtonsDisplay,
 /**@type {!xyz.swapee.wc.ISwapeeSocialButtonsScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:9422576619,
 }),
){}