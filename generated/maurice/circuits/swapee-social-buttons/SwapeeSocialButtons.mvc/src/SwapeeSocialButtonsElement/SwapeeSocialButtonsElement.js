import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import SwapeeSocialButtonsServerController from '../SwapeeSocialButtonsServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractSwapeeSocialButtonsElement from '../../gen/AbstractSwapeeSocialButtonsElement'

/** @extends {xyz.swapee.wc.SwapeeSocialButtonsElement} */
export default class SwapeeSocialButtonsElement extends AbstractSwapeeSocialButtonsElement.implements(
 SwapeeSocialButtonsServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElement}*/({
   classesMap: true,
   rootSelector:     `.SwapeeSocialButtons`,
   stylesheet:       'html/styles/SwapeeSocialButtons.css',
   preserveClasses:['SocialButtons'],
   blockName:        'html/SwapeeSocialButtonsBlock.html',
  }),
){}

// thank you for using web circuits
