import SwapeeSocialButtonsHtmlController from '../SwapeeSocialButtonsHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractSwapeeSocialButtonsHtmlComponent} from '../../gen/AbstractSwapeeSocialButtonsHtmlComponent'

/** @extends {xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent} */
export default class extends AbstractSwapeeSocialButtonsHtmlComponent.implements(
 SwapeeSocialButtonsHtmlController,
 IntegratedComponentInitialiser,
){}