/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ISwapeeSocialButtonsComputer={}
xyz.swapee.wc.ISwapeeSocialButtonsOuterCore={}
xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model={}
xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model.Core={}
xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel={}
xyz.swapee.wc.ISwapeeSocialButtonsPort={}
xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs={}
xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs={}
xyz.swapee.wc.ISwapeeSocialButtonsCore={}
xyz.swapee.wc.ISwapeeSocialButtonsCore.Model={}
xyz.swapee.wc.ISwapeeSocialButtonsPortInterface={}
xyz.swapee.wc.ISwapeeSocialButtonsProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ISwapeeSocialButtonsController={}
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT={}
xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR={}
xyz.swapee.wc.ISwapeeSocialButtons={}
xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent={}
xyz.swapee.wc.ISwapeeSocialButtonsElement={}
xyz.swapee.wc.ISwapeeSocialButtonsElementPort={}
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs={}
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs={}
xyz.swapee.wc.ISwapeeSocialButtonsDesigner={}
xyz.swapee.wc.ISwapeeSocialButtonsDesigner.communicator={}
xyz.swapee.wc.ISwapeeSocialButtonsDesigner.relay={}
xyz.swapee.wc.ISwapeeSocialButtonsDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay={}
xyz.swapee.wc.back.ISwapeeSocialButtonsController={}
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR={}
xyz.swapee.wc.back.ISwapeeSocialButtonsScreen={}
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT={}
xyz.swapee.wc.ISwapeeSocialButtonsController={}
xyz.swapee.wc.ISwapeeSocialButtonsScreen={}
xyz.swapee.wc.ISwapeeSocialButtonsGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/02-ISwapeeSocialButtonsComputer.xml}  ba63c4f6568985438ccc8fd55c1a8caf */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsComputer)} xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsComputer} xyz.swapee.wc.SwapeeSocialButtonsComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsComputer` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.constructor&xyz.swapee.wc.SwapeeSocialButtonsComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsComputer}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsComputer}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsComputer}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsComputer} xyz.swapee.wc.SwapeeSocialButtonsComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.SwapeeSocialButtonsMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.ISwapeeSocialButtonsComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsComputer
 */
xyz.swapee.wc.ISwapeeSocialButtonsComputer = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsComputer.compute} */
xyz.swapee.wc.ISwapeeSocialButtonsComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsComputer} xyz.swapee.wc.ISwapeeSocialButtonsComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsComputer_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsComputer
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsComputer = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsComputer.constructor&xyz.swapee.wc.ISwapeeSocialButtonsComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsComputer}
 */
xyz.swapee.wc.SwapeeSocialButtonsComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsComputer} */
xyz.swapee.wc.RecordISwapeeSocialButtonsComputer

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsComputer} xyz.swapee.wc.BoundISwapeeSocialButtonsComputer */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsComputer} xyz.swapee.wc.BoundSwapeeSocialButtonsComputer */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsComputer_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsComputerCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsComputerCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsComputer_ instance into the _BoundISwapeeSocialButtonsComputer_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsComputer}
 */
xyz.swapee.wc.ISwapeeSocialButtonsComputerCaster.prototype.asISwapeeSocialButtonsComputer
/**
 * Access the _SwapeeSocialButtonsComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsComputer}
 */
xyz.swapee.wc.ISwapeeSocialButtonsComputerCaster.prototype.superSwapeeSocialButtonsComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.SwapeeSocialButtonsMemory) => void} xyz.swapee.wc.ISwapeeSocialButtonsComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsComputer.__compute<!xyz.swapee.wc.ISwapeeSocialButtonsComputer>} xyz.swapee.wc.ISwapeeSocialButtonsComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.SwapeeSocialButtonsMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeSocialButtonsComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeSocialButtonsComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/03-ISwapeeSocialButtonsOuterCore.xml}  ce06249f8d4e0df41d3b07273df3af93 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsOuterCore)} xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore} xyz.swapee.wc.SwapeeSocialButtonsOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.constructor&xyz.swapee.wc.SwapeeSocialButtonsOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore|typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore|typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore|typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreCaster)} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _ISwapeeSocialButtons_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsOuterCore
 */
xyz.swapee.wc.ISwapeeSocialButtonsOuterCore = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsOuterCore

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsOuterCore} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsOuterCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsOuterCore
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore} The _ISwapeeSocialButtons_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsOuterCore = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsOuterCore.constructor&xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeSocialButtonsOuterCore.prototype.constructor = xyz.swapee.wc.SwapeeSocialButtonsOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore}
 */
xyz.swapee.wc.SwapeeSocialButtonsOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsOuterCore.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreFields = class { }
/**
 * The _ISwapeeSocialButtons_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore} */
xyz.swapee.wc.RecordISwapeeSocialButtonsOuterCore

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore} xyz.swapee.wc.BoundISwapeeSocialButtonsOuterCore */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsOuterCore} xyz.swapee.wc.BoundSwapeeSocialButtonsOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model.Core.core

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model.Core} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model The _ISwapeeSocialButtons_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.Core} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel The _ISwapeeSocialButtons_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsOuterCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsOuterCore_ instance into the _BoundISwapeeSocialButtonsOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsOuterCore}
 */
xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreCaster.prototype.asISwapeeSocialButtonsOuterCore
/**
 * Access the _SwapeeSocialButtonsOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsOuterCore}
 */
xyz.swapee.wc.ISwapeeSocialButtonsOuterCoreCaster.prototype.superSwapeeSocialButtonsOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model.Core The core property (optional overlay).
 * @prop {string} [core=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model.Core_Safe The core property (required overlay).
 * @prop {string} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.Core The core property (optional overlay).
 * @prop {*} [core=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.Core_Safe The core property (required overlay).
 * @prop {*} core The core property.
 */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.Core} xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.Core} xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model.Core} xyz.swapee.wc.ISwapeeSocialButtonsCore.Model.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model.Core_Safe} xyz.swapee.wc.ISwapeeSocialButtonsCore.Model.Core_Safe The core property (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/04-ISwapeeSocialButtonsPort.xml}  d6ab1f60ada4f456e17bedb18886b435 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsPort)} xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsPort} xyz.swapee.wc.SwapeeSocialButtonsPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsPort
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.constructor&xyz.swapee.wc.SwapeeSocialButtonsPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsPort|typeof xyz.swapee.wc.SwapeeSocialButtonsPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsPort|typeof xyz.swapee.wc.SwapeeSocialButtonsPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsPort|typeof xyz.swapee.wc.SwapeeSocialButtonsPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsPort.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsPort} xyz.swapee.wc.SwapeeSocialButtonsPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs>)} xyz.swapee.wc.ISwapeeSocialButtonsPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ISwapeeSocialButtons_, providing input
 * pins.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsPort
 */
xyz.swapee.wc.ISwapeeSocialButtonsPort = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsPort.resetPort} */
xyz.swapee.wc.ISwapeeSocialButtonsPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsPort.resetSwapeeSocialButtonsPort} */
xyz.swapee.wc.ISwapeeSocialButtonsPort.prototype.resetSwapeeSocialButtonsPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsPort.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsPort} xyz.swapee.wc.ISwapeeSocialButtonsPort.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsPort
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsPort} The port that serves as an interface to the _ISwapeeSocialButtons_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsPort = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsPort.constructor&xyz.swapee.wc.ISwapeeSocialButtonsPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsPort}
 */
xyz.swapee.wc.SwapeeSocialButtonsPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsPort.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsPortFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsPortFields = class { }
/**
 * The inputs to the _ISwapeeSocialButtons_'s controller via its port.
 */
xyz.swapee.wc.ISwapeeSocialButtonsPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeSocialButtonsPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsPort} */
xyz.swapee.wc.RecordISwapeeSocialButtonsPort

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsPort} xyz.swapee.wc.BoundISwapeeSocialButtonsPort */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsPort} xyz.swapee.wc.BoundSwapeeSocialButtonsPort */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel} xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ISwapeeSocialButtons_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs
 */
xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs.constructor&xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs.constructor */
/**
 * The inputs to the _ISwapeeSocialButtons_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsPortInterface
 */
xyz.swapee.wc.ISwapeeSocialButtonsPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ISwapeeSocialButtonsPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeSocialButtonsPortInterface.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsPortInterface

/**
 * A concrete class of _ISwapeeSocialButtonsPortInterface_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsPortInterface
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsPortInterface} The port interface.
 */
xyz.swapee.wc.SwapeeSocialButtonsPortInterface = class extends xyz.swapee.wc.ISwapeeSocialButtonsPortInterface { }
xyz.swapee.wc.SwapeeSocialButtonsPortInterface.prototype.constructor = xyz.swapee.wc.SwapeeSocialButtonsPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsPortInterface.Props
 * @prop {string} core The core property.
 */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsPortCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsPortCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsPort_ instance into the _BoundISwapeeSocialButtonsPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsPort}
 */
xyz.swapee.wc.ISwapeeSocialButtonsPortCaster.prototype.asISwapeeSocialButtonsPort
/**
 * Access the _SwapeeSocialButtonsPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsPort}
 */
xyz.swapee.wc.ISwapeeSocialButtonsPortCaster.prototype.superSwapeeSocialButtonsPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeSocialButtonsPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsPort.__resetPort<!xyz.swapee.wc.ISwapeeSocialButtonsPort>} xyz.swapee.wc.ISwapeeSocialButtonsPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsPort.resetPort} */
/**
 * Resets the _ISwapeeSocialButtons_ port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeSocialButtonsPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeSocialButtonsPort.__resetSwapeeSocialButtonsPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsPort.__resetSwapeeSocialButtonsPort<!xyz.swapee.wc.ISwapeeSocialButtonsPort>} xyz.swapee.wc.ISwapeeSocialButtonsPort._resetSwapeeSocialButtonsPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsPort.resetSwapeeSocialButtonsPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeSocialButtonsPort.resetSwapeeSocialButtonsPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeSocialButtonsPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/09-ISwapeeSocialButtonsCore.xml}  3dc44e0a4a0ddeae510a379b9c63f3cf */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeSocialButtonsCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsCore)} xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsCore} xyz.swapee.wc.SwapeeSocialButtonsCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsCore
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.constructor&xyz.swapee.wc.SwapeeSocialButtonsCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsCore|typeof xyz.swapee.wc.SwapeeSocialButtonsCore)|(!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore|typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsCore|typeof xyz.swapee.wc.SwapeeSocialButtonsCore)|(!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore|typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsCore|typeof xyz.swapee.wc.SwapeeSocialButtonsCore)|(!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore|typeof xyz.swapee.wc.SwapeeSocialButtonsOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsCore}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsCoreCaster&xyz.swapee.wc.ISwapeeSocialButtonsOuterCore)} xyz.swapee.wc.ISwapeeSocialButtonsCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsCore
 */
xyz.swapee.wc.ISwapeeSocialButtonsCore = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsCore.resetCore} */
xyz.swapee.wc.ISwapeeSocialButtonsCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsCore.resetSwapeeSocialButtonsCore} */
xyz.swapee.wc.ISwapeeSocialButtonsCore.prototype.resetSwapeeSocialButtonsCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsCore.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsCore} xyz.swapee.wc.ISwapeeSocialButtonsCore.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsCore
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsCore = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsCore.constructor&xyz.swapee.wc.ISwapeeSocialButtonsCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeSocialButtonsCore.prototype.constructor = xyz.swapee.wc.SwapeeSocialButtonsCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsCore}
 */
xyz.swapee.wc.SwapeeSocialButtonsCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsCore.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsCoreFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsCoreFields = class { }
/**
 * The _ISwapeeSocialButtons_'s memory.
 */
xyz.swapee.wc.ISwapeeSocialButtonsCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ISwapeeSocialButtonsCoreFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeSocialButtonsCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsCore} */
xyz.swapee.wc.RecordISwapeeSocialButtonsCore

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsCore} xyz.swapee.wc.BoundISwapeeSocialButtonsCore */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsCore} xyz.swapee.wc.BoundSwapeeSocialButtonsCore */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model} xyz.swapee.wc.ISwapeeSocialButtonsCore.Model The _ISwapeeSocialButtons_'s memory. */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsCoreCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsCoreCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsCore_ instance into the _BoundISwapeeSocialButtonsCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsCore}
 */
xyz.swapee.wc.ISwapeeSocialButtonsCoreCaster.prototype.asISwapeeSocialButtonsCore
/**
 * Access the _SwapeeSocialButtonsCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsCore}
 */
xyz.swapee.wc.ISwapeeSocialButtonsCoreCaster.prototype.superSwapeeSocialButtonsCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeSocialButtonsCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsCore.__resetCore<!xyz.swapee.wc.ISwapeeSocialButtonsCore>} xyz.swapee.wc.ISwapeeSocialButtonsCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsCore.resetCore} */
/**
 * Resets the _ISwapeeSocialButtons_ core.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeSocialButtonsCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeSocialButtonsCore.__resetSwapeeSocialButtonsCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsCore.__resetSwapeeSocialButtonsCore<!xyz.swapee.wc.ISwapeeSocialButtonsCore>} xyz.swapee.wc.ISwapeeSocialButtonsCore._resetSwapeeSocialButtonsCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsCore.resetSwapeeSocialButtonsCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeSocialButtonsCore.resetSwapeeSocialButtonsCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeSocialButtonsCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/10-ISwapeeSocialButtonsProcessor.xml}  79ad6d854339cb59c550ad1a8e8d8f6f */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsProcessor)} xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor} xyz.swapee.wc.SwapeeSocialButtonsProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.constructor&xyz.swapee.wc.SwapeeSocialButtonsProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!xyz.swapee.wc.ISwapeeSocialButtonsCore|typeof xyz.swapee.wc.SwapeeSocialButtonsCore)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!xyz.swapee.wc.ISwapeeSocialButtonsCore|typeof xyz.swapee.wc.SwapeeSocialButtonsCore)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!xyz.swapee.wc.ISwapeeSocialButtonsCore|typeof xyz.swapee.wc.SwapeeSocialButtonsCore)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsProcessor} xyz.swapee.wc.SwapeeSocialButtonsProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsProcessorCaster&xyz.swapee.wc.ISwapeeSocialButtonsComputer&xyz.swapee.wc.ISwapeeSocialButtonsCore&xyz.swapee.wc.ISwapeeSocialButtonsController)} xyz.swapee.wc.ISwapeeSocialButtonsProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsController} xyz.swapee.wc.ISwapeeSocialButtonsController.typeof */
/**
 * The processor to compute changes to the memory for the _ISwapeeSocialButtons_.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsProcessor
 */
xyz.swapee.wc.ISwapeeSocialButtonsProcessor = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeSocialButtonsComputer.typeof&xyz.swapee.wc.ISwapeeSocialButtonsCore.typeof&xyz.swapee.wc.ISwapeeSocialButtonsController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsProcessor} xyz.swapee.wc.ISwapeeSocialButtonsProcessor.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsProcessor_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsProcessor
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsProcessor} The processor to compute changes to the memory for the _ISwapeeSocialButtons_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsProcessor = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsProcessor.constructor&xyz.swapee.wc.ISwapeeSocialButtonsProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.SwapeeSocialButtonsProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsProcessor} */
xyz.swapee.wc.RecordISwapeeSocialButtonsProcessor

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsProcessor} xyz.swapee.wc.BoundISwapeeSocialButtonsProcessor */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsProcessor} xyz.swapee.wc.BoundSwapeeSocialButtonsProcessor */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsProcessor_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsProcessorCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsProcessorCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsProcessor_ instance into the _BoundISwapeeSocialButtonsProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.ISwapeeSocialButtonsProcessorCaster.prototype.asISwapeeSocialButtonsProcessor
/**
 * Access the _SwapeeSocialButtonsProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.ISwapeeSocialButtonsProcessorCaster.prototype.superSwapeeSocialButtonsProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/100-SwapeeSocialButtonsMemory.xml}  477e1e2fad3eb66dcbd3a5c4522b692f */
/**
 * The memory of the _ISwapeeSocialButtons_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.SwapeeSocialButtonsMemory
 */
xyz.swapee.wc.SwapeeSocialButtonsMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.SwapeeSocialButtonsMemory.prototype.core = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/102-SwapeeSocialButtonsInputs.xml}  6e2dd17cb1a52001aadfd7ba0a2e2552 */
/**
 * The inputs of the _ISwapeeSocialButtons_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.SwapeeSocialButtonsInputs
 */
xyz.swapee.wc.front.SwapeeSocialButtonsInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.SwapeeSocialButtonsInputs.prototype.core = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/11-ISwapeeSocialButtons.xml}  f48671b81a49f08dbba54d4b96355e6c */
/**
 * An atomic wrapper for the _ISwapeeSocialButtons_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.SwapeeSocialButtonsEnv
 */
xyz.swapee.wc.SwapeeSocialButtonsEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.SwapeeSocialButtonsEnv.prototype.swapeeSocialButtons = /** @type {xyz.swapee.wc.ISwapeeSocialButtons} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs>&xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese} xyz.swapee.wc.ISwapeeSocialButtons.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtons)} xyz.swapee.wc.AbstractSwapeeSocialButtons.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtons} xyz.swapee.wc.SwapeeSocialButtons.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtons` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtons
 */
xyz.swapee.wc.AbstractSwapeeSocialButtons = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtons.constructor&xyz.swapee.wc.SwapeeSocialButtons.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtons.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtons
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtons.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtons} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtons|typeof xyz.swapee.wc.SwapeeSocialButtons)|(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtons}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtons.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtons}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtons.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtons}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtons.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtons|typeof xyz.swapee.wc.SwapeeSocialButtons)|(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtons}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtons.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtons|typeof xyz.swapee.wc.SwapeeSocialButtons)|(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtons}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtons.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtons.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtons} xyz.swapee.wc.SwapeeSocialButtonsConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtons.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ISwapeeSocialButtons.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ISwapeeSocialButtons.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ISwapeeSocialButtons.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.SwapeeSocialButtonsMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.SwapeeSocialButtonsClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsCaster&xyz.swapee.wc.ISwapeeSocialButtonsProcessor&xyz.swapee.wc.ISwapeeSocialButtonsComputer&xyz.swapee.wc.ISwapeeSocialButtonsController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs, null>)} xyz.swapee.wc.ISwapeeSocialButtons.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ISwapeeSocialButtons
 */
xyz.swapee.wc.ISwapeeSocialButtons = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtons.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeSocialButtonsProcessor.typeof&xyz.swapee.wc.ISwapeeSocialButtonsComputer.typeof&xyz.swapee.wc.ISwapeeSocialButtonsController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtons* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtons.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtons.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtons&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtons.Initialese>)} xyz.swapee.wc.SwapeeSocialButtons.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtons} xyz.swapee.wc.ISwapeeSocialButtons.typeof */
/**
 * A concrete class of _ISwapeeSocialButtons_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtons
 * @implements {xyz.swapee.wc.ISwapeeSocialButtons} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtons.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtons = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtons.constructor&xyz.swapee.wc.ISwapeeSocialButtons.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtons* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtons.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtons* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtons.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtons.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtons}
 */
xyz.swapee.wc.SwapeeSocialButtons.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtons.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsFields = class { }
/**
 * The input pins of the _ISwapeeSocialButtons_ port.
 */
xyz.swapee.wc.ISwapeeSocialButtonsFields.prototype.pinout = /** @type {!xyz.swapee.wc.ISwapeeSocialButtons.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtons} */
xyz.swapee.wc.RecordISwapeeSocialButtons

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtons} xyz.swapee.wc.BoundISwapeeSocialButtons */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtons} xyz.swapee.wc.BoundSwapeeSocialButtons */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs} xyz.swapee.wc.ISwapeeSocialButtons.Pinout The input pins of the _ISwapeeSocialButtons_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs>)} xyz.swapee.wc.ISwapeeSocialButtonsBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsBuffer
 */
xyz.swapee.wc.ISwapeeSocialButtonsBuffer = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeSocialButtonsBuffer.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsBuffer

/**
 * A concrete class of _ISwapeeSocialButtonsBuffer_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsBuffer
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.SwapeeSocialButtonsBuffer = class extends xyz.swapee.wc.ISwapeeSocialButtonsBuffer { }
xyz.swapee.wc.SwapeeSocialButtonsBuffer.prototype.constructor = xyz.swapee.wc.SwapeeSocialButtonsBuffer

/**
 * Contains getters to cast the _ISwapeeSocialButtons_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsCaster = class { }
/**
 * Cast the _ISwapeeSocialButtons_ instance into the _BoundISwapeeSocialButtons_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtons}
 */
xyz.swapee.wc.ISwapeeSocialButtonsCaster.prototype.asISwapeeSocialButtons
/**
 * Access the _SwapeeSocialButtons_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtons}
 */
xyz.swapee.wc.ISwapeeSocialButtonsCaster.prototype.superSwapeeSocialButtons

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/110-SwapeeSocialButtonsSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeSocialButtonsVdusPQs
 */
xyz.swapee.wc.SwapeeSocialButtonsVdusPQs = class { }
xyz.swapee.wc.SwapeeSocialButtonsVdusPQs.prototype.constructor = xyz.swapee.wc.SwapeeSocialButtonsVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsVdusQPs
 * @dict
 */
xyz.swapee.wc.SwapeeSocialButtonsVdusQPs = class { }
xyz.swapee.wc.SwapeeSocialButtonsVdusQPs.prototype.constructor = xyz.swapee.wc.SwapeeSocialButtonsVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/12-ISwapeeSocialButtonsHtmlComponent.xml}  a05df1505d4bd28f5df787198d8a9742 */
/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese&xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese&xyz.swapee.wc.ISwapeeSocialButtons.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsProcessor.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsComputer.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent)} xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent} xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.constructor&xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent|typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.back.SwapeeSocialButtonsController)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen)|(!xyz.swapee.wc.ISwapeeSocialButtons|typeof xyz.swapee.wc.SwapeeSocialButtons)|(!xyz.swapee.wc.ISwapeeSocialButtonsGPU|typeof xyz.swapee.wc.SwapeeSocialButtonsGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent|typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.back.SwapeeSocialButtonsController)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen)|(!xyz.swapee.wc.ISwapeeSocialButtons|typeof xyz.swapee.wc.SwapeeSocialButtons)|(!xyz.swapee.wc.ISwapeeSocialButtonsGPU|typeof xyz.swapee.wc.SwapeeSocialButtonsGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent|typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.back.SwapeeSocialButtonsController)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen)|(!xyz.swapee.wc.ISwapeeSocialButtons|typeof xyz.swapee.wc.SwapeeSocialButtons)|(!xyz.swapee.wc.ISwapeeSocialButtonsGPU|typeof xyz.swapee.wc.SwapeeSocialButtonsGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeSocialButtonsProcessor|typeof xyz.swapee.wc.SwapeeSocialButtonsProcessor)|(!xyz.swapee.wc.ISwapeeSocialButtonsComputer|typeof xyz.swapee.wc.SwapeeSocialButtonsComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent} xyz.swapee.wc.SwapeeSocialButtonsHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponentCaster&xyz.swapee.wc.back.ISwapeeSocialButtonsController&xyz.swapee.wc.back.ISwapeeSocialButtonsScreen&xyz.swapee.wc.ISwapeeSocialButtons&xyz.swapee.wc.ISwapeeSocialButtonsGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.ISwapeeSocialButtonsProcessor&xyz.swapee.wc.ISwapeeSocialButtonsComputer)} xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeSocialButtonsController} xyz.swapee.wc.back.ISwapeeSocialButtonsController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeSocialButtonsScreen} xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsGPU} xyz.swapee.wc.ISwapeeSocialButtonsGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _ISwapeeSocialButtons_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent
 */
xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeSocialButtonsController.typeof&xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.typeof&xyz.swapee.wc.ISwapeeSocialButtons.typeof&xyz.swapee.wc.ISwapeeSocialButtonsGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ISwapeeSocialButtonsProcessor.typeof&xyz.swapee.wc.ISwapeeSocialButtonsComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent} xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent} The _ISwapeeSocialButtons_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent.constructor&xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent}
 */
xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent} */
xyz.swapee.wc.RecordISwapeeSocialButtonsHtmlComponent

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent} xyz.swapee.wc.BoundISwapeeSocialButtonsHtmlComponent */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent} xyz.swapee.wc.BoundSwapeeSocialButtonsHtmlComponent */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponentCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponentCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsHtmlComponent_ instance into the _BoundISwapeeSocialButtonsHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsHtmlComponent}
 */
xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponentCaster.prototype.asISwapeeSocialButtonsHtmlComponent
/**
 * Access the _SwapeeSocialButtonsHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsHtmlComponent}
 */
xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponentCaster.prototype.superSwapeeSocialButtonsHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/130-ISwapeeSocialButtonsElement.xml}  3989f4d26846896281882d05a30f613c */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsElement)} xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsElement} xyz.swapee.wc.SwapeeSocialButtonsElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsElement` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsElement
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.constructor&xyz.swapee.wc.SwapeeSocialButtonsElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsElement|typeof xyz.swapee.wc.SwapeeSocialButtonsElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElement}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElement}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsElement|typeof xyz.swapee.wc.SwapeeSocialButtonsElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElement}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsElement|typeof xyz.swapee.wc.SwapeeSocialButtonsElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElement}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsElement.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsElement} xyz.swapee.wc.SwapeeSocialButtonsElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsElementFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs, null>)} xyz.swapee.wc.ISwapeeSocialButtonsElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _ISwapeeSocialButtons_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsElement
 */
xyz.swapee.wc.ISwapeeSocialButtonsElement = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsElement.solder} */
xyz.swapee.wc.ISwapeeSocialButtonsElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsElement.render} */
xyz.swapee.wc.ISwapeeSocialButtonsElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsElement.server} */
xyz.swapee.wc.ISwapeeSocialButtonsElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsElement.inducer} */
xyz.swapee.wc.ISwapeeSocialButtonsElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsElement&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsElement.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElement} xyz.swapee.wc.ISwapeeSocialButtonsElement.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsElement_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsElement
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsElement} A component description.
 *
 * The _ISwapeeSocialButtons_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsElement.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsElement = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsElement.constructor&xyz.swapee.wc.ISwapeeSocialButtonsElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElement}
 */
xyz.swapee.wc.SwapeeSocialButtonsElement.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsElement.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsElementFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementFields = class { }
/**
 * The element-specific inputs to the _ISwapeeSocialButtons_ component.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElement} */
xyz.swapee.wc.RecordISwapeeSocialButtonsElement

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElement} xyz.swapee.wc.BoundISwapeeSocialButtonsElement */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsElement} xyz.swapee.wc.BoundSwapeeSocialButtonsElement */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs&xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Queries&xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs} xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs The element-specific inputs to the _ISwapeeSocialButtons_ component. */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsElement_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsElementCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsElement_ instance into the _BoundISwapeeSocialButtonsElement_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsElement}
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementCaster.prototype.asISwapeeSocialButtonsElement
/**
 * Access the _SwapeeSocialButtonsElement_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsElement}
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementCaster.prototype.superSwapeeSocialButtonsElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeSocialButtonsMemory, props: !xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs) => Object<string, *>} xyz.swapee.wc.ISwapeeSocialButtonsElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElement.__solder<!xyz.swapee.wc.ISwapeeSocialButtonsElement>} xyz.swapee.wc.ISwapeeSocialButtonsElement._solder */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.SwapeeSocialButtonsMemory} model The model.
 * @param {!xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs} props The element props.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeSocialButtonsElementPort.Inputs.NoSolder* Default `false`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeSocialButtonsMemory, instance?: !xyz.swapee.wc.ISwapeeSocialButtonsScreen&xyz.swapee.wc.ISwapeeSocialButtonsController) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeSocialButtonsElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElement.__render<!xyz.swapee.wc.ISwapeeSocialButtonsElement>} xyz.swapee.wc.ISwapeeSocialButtonsElement._render */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.SwapeeSocialButtonsMemory} [model] The model for the view.
 * @param {!xyz.swapee.wc.ISwapeeSocialButtonsScreen&xyz.swapee.wc.ISwapeeSocialButtonsController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeSocialButtonsMemory, inputs: !xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeSocialButtonsElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElement.__server<!xyz.swapee.wc.ISwapeeSocialButtonsElement>} xyz.swapee.wc.ISwapeeSocialButtonsElement._server */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.SwapeeSocialButtonsMemory} memory The memory registers.
 * @param {!xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs} inputs The inputs to the port.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeSocialButtonsElementPort.Inputs.NoSolder* Default `false`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeSocialButtonsMemory, port?: !xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs) => ?} xyz.swapee.wc.ISwapeeSocialButtonsElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElement.__inducer<!xyz.swapee.wc.ISwapeeSocialButtonsElement>} xyz.swapee.wc.ISwapeeSocialButtonsElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.SwapeeSocialButtonsMemory} [model] The model of the component into which to induce the state.
 * @param {!xyz.swapee.wc.ISwapeeSocialButtonsElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeSocialButtonsElementPort.Inputs.NoSolder* Default `false`.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeSocialButtonsElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeSocialButtonsElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/140-ISwapeeSocialButtonsElementPort.xml}  e800ae67ffac58bf6482d8866d350dc7 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsElementPort)} xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort} xyz.swapee.wc.SwapeeSocialButtonsElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.constructor&xyz.swapee.wc.SwapeeSocialButtonsElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsElementPort|typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsElementPort|typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsElementPort|typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsElementPort} xyz.swapee.wc.SwapeeSocialButtonsElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs>)} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsElementPort
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPort = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElementPort} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsElementPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsElementPort
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsElementPort = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsElementPort.constructor&xyz.swapee.wc.ISwapeeSocialButtonsElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsElementPort}
 */
xyz.swapee.wc.SwapeeSocialButtonsElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsElementPort.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsElementPortFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPortFields = class { }
/**
 * The inputs to the _ISwapeeSocialButtonsElement_'s controller via its element port.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElementPort} */
xyz.swapee.wc.RecordISwapeeSocialButtonsElementPort

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsElementPort} xyz.swapee.wc.BoundISwapeeSocialButtonsElementPort */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsElementPort} xyz.swapee.wc.BoundSwapeeSocialButtonsElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder)} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _ISwapeeSocialButtonsElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.constructor&xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _ISwapeeSocialButtonsElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs

/**
 * Contains getters to cast the _ISwapeeSocialButtonsElementPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsElementPortCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPortCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsElementPort_ instance into the _BoundISwapeeSocialButtonsElementPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsElementPort}
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPortCaster.prototype.asISwapeeSocialButtonsElementPort
/**
 * Access the _SwapeeSocialButtonsElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsElementPort}
 */
xyz.swapee.wc.ISwapeeSocialButtonsElementPortCaster.prototype.superSwapeeSocialButtonsElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/170-ISwapeeSocialButtonsDesigner.xml}  044fd05c37b6d2222fc23afe9a910f1a */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsDesigner
 */
xyz.swapee.wc.ISwapeeSocialButtonsDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.SwapeeSocialButtonsClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeSocialButtons />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeSocialButtonsClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeSocialButtons />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeSocialButtonsClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeSocialButtonsDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeSocialButtons` _typeof ISwapeeSocialButtonsController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeSocialButtonsDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeSocialButtons` _typeof ISwapeeSocialButtonsController_
   * - `This` _typeof ISwapeeSocialButtonsController_
   * @param {!xyz.swapee.wc.ISwapeeSocialButtonsDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `SwapeeSocialButtons` _!SwapeeSocialButtonsMemory_
   * - `This` _!SwapeeSocialButtonsMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeSocialButtonsClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeSocialButtonsClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ISwapeeSocialButtonsDesigner.prototype.constructor = xyz.swapee.wc.ISwapeeSocialButtonsDesigner

/**
 * A concrete class of _ISwapeeSocialButtonsDesigner_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsDesigner
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.SwapeeSocialButtonsDesigner = class extends xyz.swapee.wc.ISwapeeSocialButtonsDesigner { }
xyz.swapee.wc.SwapeeSocialButtonsDesigner.prototype.constructor = xyz.swapee.wc.SwapeeSocialButtonsDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeSocialButtonsController} SwapeeSocialButtons
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeSocialButtonsController} SwapeeSocialButtons
 * @prop {typeof xyz.swapee.wc.ISwapeeSocialButtonsController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeSocialButtonsDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.SwapeeSocialButtonsMemory} SwapeeSocialButtons
 * @prop {!xyz.swapee.wc.SwapeeSocialButtonsMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/40-ISwapeeSocialButtonsDisplay.xml}  4653a673d4f94602be03b411216ef43c */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Settings>} xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsDisplay)} xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay} xyz.swapee.wc.SwapeeSocialButtonsDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.constructor&xyz.swapee.wc.SwapeeSocialButtonsDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsDisplay} xyz.swapee.wc.SwapeeSocialButtonsDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !HTMLDivElement, !xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Settings, xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Queries, null>)} xyz.swapee.wc.ISwapeeSocialButtonsDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ISwapeeSocialButtons_.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsDisplay
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplay = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsDisplay.paint} */
xyz.swapee.wc.ISwapeeSocialButtonsDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsDisplay} xyz.swapee.wc.ISwapeeSocialButtonsDisplay.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsDisplay_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsDisplay
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsDisplay} Display for presenting information from the _ISwapeeSocialButtons_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsDisplay = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsDisplay.constructor&xyz.swapee.wc.ISwapeeSocialButtonsDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.SwapeeSocialButtonsDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsDisplay.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsDisplayFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsDisplay} */
xyz.swapee.wc.RecordISwapeeSocialButtonsDisplay

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsDisplay} xyz.swapee.wc.BoundISwapeeSocialButtonsDisplay */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsDisplay} xyz.swapee.wc.BoundSwapeeSocialButtonsDisplay */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Queries} xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsDisplay_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsDisplayCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplayCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsDisplay_ instance into the _BoundISwapeeSocialButtonsDisplay_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplayCaster.prototype.asISwapeeSocialButtonsDisplay
/**
 * Cast the _ISwapeeSocialButtonsDisplay_ instance into the _BoundISwapeeSocialButtonsScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplayCaster.prototype.asISwapeeSocialButtonsScreen
/**
 * Access the _SwapeeSocialButtonsDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplayCaster.prototype.superSwapeeSocialButtonsDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeSocialButtonsMemory, land: null) => void} xyz.swapee.wc.ISwapeeSocialButtonsDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsDisplay.__paint<!xyz.swapee.wc.ISwapeeSocialButtonsDisplay>} xyz.swapee.wc.ISwapeeSocialButtonsDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeSocialButtonsMemory} memory The display data.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeSocialButtonsDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeSocialButtonsDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/40-ISwapeeSocialButtonsDisplayBack.xml}  4caea849c56da64880c6828607c4c50f */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.SwapeeSocialButtonsClasses>} xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeSocialButtonsDisplay)} xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay} xyz.swapee.wc.back.SwapeeSocialButtonsDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.constructor&xyz.swapee.wc.back.SwapeeSocialButtonsDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeSocialButtonsDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.SwapeeSocialButtonsClasses, null>)} xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay = class extends /** @type {xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.paint} */
xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.Initialese>)} xyz.swapee.wc.back.SwapeeSocialButtonsDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay} xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsDisplay_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeSocialButtonsDisplay
 * @implements {xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeSocialButtonsDisplay = class extends /** @type {xyz.swapee.wc.back.SwapeeSocialButtonsDisplay.constructor&xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.SwapeeSocialButtonsDisplay.prototype.constructor = xyz.swapee.wc.back.SwapeeSocialButtonsDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.back.SwapeeSocialButtonsDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay} */
xyz.swapee.wc.back.RecordISwapeeSocialButtonsDisplay

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay} xyz.swapee.wc.back.BoundISwapeeSocialButtonsDisplay */

/** @typedef {xyz.swapee.wc.back.SwapeeSocialButtonsDisplay} xyz.swapee.wc.back.BoundSwapeeSocialButtonsDisplay */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsDisplay_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsDisplayCaster
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsDisplayCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsDisplay_ instance into the _BoundISwapeeSocialButtonsDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsDisplayCaster.prototype.asISwapeeSocialButtonsDisplay
/**
 * Access the _SwapeeSocialButtonsDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeSocialButtonsDisplay}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsDisplayCaster.prototype.superSwapeeSocialButtonsDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.SwapeeSocialButtonsMemory, land?: null) => void} xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.__paint<!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay>} xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeSocialButtonsMemory} [memory] The display data.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/41-SwapeeSocialButtonsClasses.xml}  139d8272cb32c1861adfd04a727402e9 */
/**
 * The classes of the _ISwapeeSocialButtonsDisplay_.
 * @record xyz.swapee.wc.SwapeeSocialButtonsClasses
 */
xyz.swapee.wc.SwapeeSocialButtonsClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.SwapeeSocialButtonsClasses.prototype.props = /** @type {xyz.swapee.wc.SwapeeSocialButtonsClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/50-ISwapeeSocialButtonsController.xml}  b0d326836d7ec3cb1b3cb5a1efd7ebc6 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs, !xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel>} xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsController)} xyz.swapee.wc.AbstractSwapeeSocialButtonsController.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsController} xyz.swapee.wc.SwapeeSocialButtonsController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsController` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsController
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsController.constructor&xyz.swapee.wc.SwapeeSocialButtonsController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsController.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsController.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsController}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsController} xyz.swapee.wc.SwapeeSocialButtonsControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs, !xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs, !xyz.swapee.wc.ISwapeeSocialButtonsController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs, !xyz.swapee.wc.SwapeeSocialButtonsMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs>)} xyz.swapee.wc.ISwapeeSocialButtonsController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsController
 */
xyz.swapee.wc.ISwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeSocialButtonsController.resetPort} */
xyz.swapee.wc.ISwapeeSocialButtonsController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsController&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsController.constructor */
/**
 * A concrete class of _ISwapeeSocialButtonsController_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsController
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsController.constructor&xyz.swapee.wc.ISwapeeSocialButtonsController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.SwapeeSocialButtonsController.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsController.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsControllerFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsControllerFields = class { }
/**
 * The inputs to the _ISwapeeSocialButtons_'s controller.
 */
xyz.swapee.wc.ISwapeeSocialButtonsControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeSocialButtonsControllerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeSocialButtonsController} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsController} */
xyz.swapee.wc.RecordISwapeeSocialButtonsController

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsController} xyz.swapee.wc.BoundISwapeeSocialButtonsController */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsController} xyz.swapee.wc.BoundSwapeeSocialButtonsController */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsPort.Inputs} xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs The inputs to the _ISwapeeSocialButtons_'s controller. */

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsPort.WeakInputs} xyz.swapee.wc.ISwapeeSocialButtonsController.WeakInputs The inputs to the _ISwapeeSocialButtons_'s controller. */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsController_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsControllerCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsControllerCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsController_ instance into the _BoundISwapeeSocialButtonsController_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsController}
 */
xyz.swapee.wc.ISwapeeSocialButtonsControllerCaster.prototype.asISwapeeSocialButtonsController
/**
 * Cast the _ISwapeeSocialButtonsController_ instance into the _BoundISwapeeSocialButtonsProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsProcessor}
 */
xyz.swapee.wc.ISwapeeSocialButtonsControllerCaster.prototype.asISwapeeSocialButtonsProcessor
/**
 * Access the _SwapeeSocialButtonsController_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsController}
 */
xyz.swapee.wc.ISwapeeSocialButtonsControllerCaster.prototype.superSwapeeSocialButtonsController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeSocialButtonsController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsController.__resetPort<!xyz.swapee.wc.ISwapeeSocialButtonsController>} xyz.swapee.wc.ISwapeeSocialButtonsController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeSocialButtonsController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeSocialButtonsController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/51-ISwapeeSocialButtonsControllerFront.xml}  34255cf6cb7c8aa952509fc424ebe569 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ISwapeeSocialButtonsController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeSocialButtonsController)} xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeSocialButtonsController} xyz.swapee.wc.front.SwapeeSocialButtonsController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeSocialButtonsController` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.constructor&xyz.swapee.wc.front.SwapeeSocialButtonsController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.front.SwapeeSocialButtonsController)|(!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT|typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.front.SwapeeSocialButtonsController)|(!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT|typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.front.SwapeeSocialButtonsController)|(!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT|typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeSocialButtonsController.Initialese[]) => xyz.swapee.wc.front.ISwapeeSocialButtonsController} xyz.swapee.wc.front.SwapeeSocialButtonsControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeSocialButtonsControllerCaster&xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT)} xyz.swapee.wc.front.ISwapeeSocialButtonsController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT} xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ISwapeeSocialButtonsController
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.front.ISwapeeSocialButtonsController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeSocialButtonsController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeSocialButtonsController.Initialese>)} xyz.swapee.wc.front.SwapeeSocialButtonsController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeSocialButtonsController} xyz.swapee.wc.front.ISwapeeSocialButtonsController.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsController_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeSocialButtonsController
 * @implements {xyz.swapee.wc.front.ISwapeeSocialButtonsController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeSocialButtonsController.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.front.SwapeeSocialButtonsController.constructor&xyz.swapee.wc.front.ISwapeeSocialButtonsController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeSocialButtonsController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.front.SwapeeSocialButtonsController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeSocialButtonsController} */
xyz.swapee.wc.front.RecordISwapeeSocialButtonsController

/** @typedef {xyz.swapee.wc.front.ISwapeeSocialButtonsController} xyz.swapee.wc.front.BoundISwapeeSocialButtonsController */

/** @typedef {xyz.swapee.wc.front.SwapeeSocialButtonsController} xyz.swapee.wc.front.BoundSwapeeSocialButtonsController */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsController_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeSocialButtonsControllerCaster
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsController_ instance into the _BoundISwapeeSocialButtonsController_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeSocialButtonsController}
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerCaster.prototype.asISwapeeSocialButtonsController
/**
 * Access the _SwapeeSocialButtonsController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeSocialButtonsController}
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerCaster.prototype.superSwapeeSocialButtonsController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/52-ISwapeeSocialButtonsControllerBack.xml}  72413d8bbce10f353a3e5f8be1763234 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs>&xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese} xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeSocialButtonsController)} xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeSocialButtonsController} xyz.swapee.wc.back.SwapeeSocialButtonsController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeSocialButtonsController` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.constructor&xyz.swapee.wc.back.SwapeeSocialButtonsController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.back.SwapeeSocialButtonsController)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.back.SwapeeSocialButtonsController)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.back.SwapeeSocialButtonsController)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese[]) => xyz.swapee.wc.back.ISwapeeSocialButtonsController} xyz.swapee.wc.back.SwapeeSocialButtonsControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeSocialButtonsControllerCaster&xyz.swapee.wc.ISwapeeSocialButtonsController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ISwapeeSocialButtonsController.Inputs>)} xyz.swapee.wc.back.ISwapeeSocialButtonsController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsController
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.back.ISwapeeSocialButtonsController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeSocialButtonsController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeSocialButtonsController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese>)} xyz.swapee.wc.back.SwapeeSocialButtonsController.constructor */
/**
 * A concrete class of _ISwapeeSocialButtonsController_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeSocialButtonsController
 * @implements {xyz.swapee.wc.back.ISwapeeSocialButtonsController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeSocialButtonsController = class extends /** @type {xyz.swapee.wc.back.SwapeeSocialButtonsController.constructor&xyz.swapee.wc.back.ISwapeeSocialButtonsController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeSocialButtonsController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsController}
 */
xyz.swapee.wc.back.SwapeeSocialButtonsController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsController} */
xyz.swapee.wc.back.RecordISwapeeSocialButtonsController

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsController} xyz.swapee.wc.back.BoundISwapeeSocialButtonsController */

/** @typedef {xyz.swapee.wc.back.SwapeeSocialButtonsController} xyz.swapee.wc.back.BoundSwapeeSocialButtonsController */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsController_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsControllerCaster
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsController_ instance into the _BoundISwapeeSocialButtonsController_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeSocialButtonsController}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerCaster.prototype.asISwapeeSocialButtonsController
/**
 * Access the _SwapeeSocialButtonsController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeSocialButtonsController}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerCaster.prototype.superSwapeeSocialButtonsController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/53-ISwapeeSocialButtonsControllerAR.xml}  9e4e6116887141dbe010c00f6fbcd51c */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsController.Initialese} xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR)} xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR} xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.constructor&xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR|typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR|typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR|typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.SwapeeSocialButtonsController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.Initialese[]) => xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR} xyz.swapee.wc.back.SwapeeSocialButtonsControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeSocialButtonsControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeSocialButtonsController)} xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeSocialButtonsControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR = class extends /** @type {xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeSocialButtonsController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.Initialese>)} xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR} xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR
 * @implements {xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeSocialButtonsControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR = class extends /** @type {xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR.constructor&xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR}
 */
xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR} */
xyz.swapee.wc.back.RecordISwapeeSocialButtonsControllerAR

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR} xyz.swapee.wc.back.BoundISwapeeSocialButtonsControllerAR */

/** @typedef {xyz.swapee.wc.back.SwapeeSocialButtonsControllerAR} xyz.swapee.wc.back.BoundSwapeeSocialButtonsControllerAR */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsControllerARCaster
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerARCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsControllerAR_ instance into the _BoundISwapeeSocialButtonsControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeSocialButtonsControllerAR}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerARCaster.prototype.asISwapeeSocialButtonsControllerAR
/**
 * Access the _SwapeeSocialButtonsControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeSocialButtonsControllerAR}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsControllerARCaster.prototype.superSwapeeSocialButtonsControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/54-ISwapeeSocialButtonsControllerAT.xml}  98344bc8d5d781d1ddaa34bed8d96f15 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT)} xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT} xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.constructor&xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT|typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT|typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT|typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.Initialese[]) => xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT} xyz.swapee.wc.front.SwapeeSocialButtonsControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeSocialButtonsControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeSocialButtonsControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT = class extends /** @type {xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.Initialese>)} xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT.constructor */
/**
 * A concrete class of _ISwapeeSocialButtonsControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT
 * @implements {xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeSocialButtonsControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT = class extends /** @type {xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT.constructor&xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT}
 */
xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT} */
xyz.swapee.wc.front.RecordISwapeeSocialButtonsControllerAT

/** @typedef {xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT} xyz.swapee.wc.front.BoundISwapeeSocialButtonsControllerAT */

/** @typedef {xyz.swapee.wc.front.SwapeeSocialButtonsControllerAT} xyz.swapee.wc.front.BoundSwapeeSocialButtonsControllerAT */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeSocialButtonsControllerATCaster
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerATCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsControllerAT_ instance into the _BoundISwapeeSocialButtonsControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeSocialButtonsControllerAT}
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerATCaster.prototype.asISwapeeSocialButtonsControllerAT
/**
 * Access the _SwapeeSocialButtonsControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeSocialButtonsControllerAT}
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsControllerATCaster.prototype.superSwapeeSocialButtonsControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/70-ISwapeeSocialButtonsScreen.xml}  ae2be62fb2f6eacc83c80767709d0da2 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.front.SwapeeSocialButtonsInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Settings, !xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Queries, null>&xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsScreen)} xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsScreen} xyz.swapee.wc.SwapeeSocialButtonsScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsScreen` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.constructor&xyz.swapee.wc.SwapeeSocialButtonsScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.SwapeeSocialButtonsScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.front.SwapeeSocialButtonsController)|(!xyz.swapee.wc.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.SwapeeSocialButtonsScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.front.SwapeeSocialButtonsController)|(!xyz.swapee.wc.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.SwapeeSocialButtonsScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeSocialButtonsController|typeof xyz.swapee.wc.front.SwapeeSocialButtonsController)|(!xyz.swapee.wc.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.SwapeeSocialButtonsDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsScreen} xyz.swapee.wc.SwapeeSocialButtonsScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.SwapeeSocialButtonsMemory, !xyz.swapee.wc.front.SwapeeSocialButtonsInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Settings, !xyz.swapee.wc.ISwapeeSocialButtonsDisplay.Queries, null, null>&xyz.swapee.wc.front.ISwapeeSocialButtonsController&xyz.swapee.wc.ISwapeeSocialButtonsDisplay)} xyz.swapee.wc.ISwapeeSocialButtonsScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsScreen
 */
xyz.swapee.wc.ISwapeeSocialButtonsScreen = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ISwapeeSocialButtonsController.typeof&xyz.swapee.wc.ISwapeeSocialButtonsDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeSocialButtonsScreen} xyz.swapee.wc.ISwapeeSocialButtonsScreen.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsScreen_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsScreen
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsScreen = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsScreen.constructor&xyz.swapee.wc.ISwapeeSocialButtonsScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.SwapeeSocialButtonsScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsScreen} */
xyz.swapee.wc.RecordISwapeeSocialButtonsScreen

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsScreen} xyz.swapee.wc.BoundISwapeeSocialButtonsScreen */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsScreen} xyz.swapee.wc.BoundSwapeeSocialButtonsScreen */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsScreen_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsScreenCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsScreenCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsScreen_ instance into the _BoundISwapeeSocialButtonsScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.ISwapeeSocialButtonsScreenCaster.prototype.asISwapeeSocialButtonsScreen
/**
 * Access the _SwapeeSocialButtonsScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.ISwapeeSocialButtonsScreenCaster.prototype.superSwapeeSocialButtonsScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/70-ISwapeeSocialButtonsScreenBack.xml}  a270b06b5c43378a40f0320061c7fdae */
/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese} xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeSocialButtonsScreen)} xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen} xyz.swapee.wc.back.SwapeeSocialButtonsScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeSocialButtonsScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.constructor&xyz.swapee.wc.back.SwapeeSocialButtonsScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese[]) => xyz.swapee.wc.back.ISwapeeSocialButtonsScreen} xyz.swapee.wc.back.SwapeeSocialButtonsScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeSocialButtonsScreenCaster&xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT)} xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT} xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsScreen
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreen = class extends /** @type {xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeSocialButtonsScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese>)} xyz.swapee.wc.back.SwapeeSocialButtonsScreen.constructor */
/**
 * A concrete class of _ISwapeeSocialButtonsScreen_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeSocialButtonsScreen
 * @implements {xyz.swapee.wc.back.ISwapeeSocialButtonsScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeSocialButtonsScreen = class extends /** @type {xyz.swapee.wc.back.SwapeeSocialButtonsScreen.constructor&xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeSocialButtonsScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.back.SwapeeSocialButtonsScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsScreen} */
xyz.swapee.wc.back.RecordISwapeeSocialButtonsScreen

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsScreen} xyz.swapee.wc.back.BoundISwapeeSocialButtonsScreen */

/** @typedef {xyz.swapee.wc.back.SwapeeSocialButtonsScreen} xyz.swapee.wc.back.BoundSwapeeSocialButtonsScreen */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsScreen_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsScreenCaster
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsScreen_ instance into the _BoundISwapeeSocialButtonsScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenCaster.prototype.asISwapeeSocialButtonsScreen
/**
 * Access the _SwapeeSocialButtonsScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeSocialButtonsScreen}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenCaster.prototype.superSwapeeSocialButtonsScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/73-ISwapeeSocialButtonsScreenAR.xml}  847f8fc95db14a4659bf7cdd3dce35df */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeSocialButtonsScreen.Initialese} xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR)} xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR} xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.constructor&xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR|typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.SwapeeSocialButtonsScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR|typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.SwapeeSocialButtonsScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR|typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeSocialButtonsScreen|typeof xyz.swapee.wc.SwapeeSocialButtonsScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.Initialese[]) => xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR} xyz.swapee.wc.front.SwapeeSocialButtonsScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeSocialButtonsScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeSocialButtonsScreen)} xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeSocialButtonsScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR = class extends /** @type {xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeSocialButtonsScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.Initialese>)} xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR} xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.typeof */
/**
 * A concrete class of _ISwapeeSocialButtonsScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR
 * @implements {xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeSocialButtonsScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR = class extends /** @type {xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR.constructor&xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR}
 */
xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR} */
xyz.swapee.wc.front.RecordISwapeeSocialButtonsScreenAR

/** @typedef {xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR} xyz.swapee.wc.front.BoundISwapeeSocialButtonsScreenAR */

/** @typedef {xyz.swapee.wc.front.SwapeeSocialButtonsScreenAR} xyz.swapee.wc.front.BoundSwapeeSocialButtonsScreenAR */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeSocialButtonsScreenARCaster
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsScreenARCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsScreenAR_ instance into the _BoundISwapeeSocialButtonsScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeSocialButtonsScreenAR}
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsScreenARCaster.prototype.asISwapeeSocialButtonsScreenAR
/**
 * Access the _SwapeeSocialButtonsScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeSocialButtonsScreenAR}
 */
xyz.swapee.wc.front.ISwapeeSocialButtonsScreenARCaster.prototype.superSwapeeSocialButtonsScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/74-ISwapeeSocialButtonsScreenAT.xml}  0aa583bf77ba811cac277dd4dacab7ff */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT)} xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT} xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.constructor&xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT|typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese[]) => xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT} xyz.swapee.wc.back.SwapeeSocialButtonsScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeSocialButtonsScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeSocialButtonsScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT = class extends /** @type {xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese>)} xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT.constructor */
/**
 * A concrete class of _ISwapeeSocialButtonsScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT
 * @implements {xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeSocialButtonsScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT = class extends /** @type {xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT.constructor&xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT}
 */
xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT} */
xyz.swapee.wc.back.RecordISwapeeSocialButtonsScreenAT

/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT} xyz.swapee.wc.back.BoundISwapeeSocialButtonsScreenAT */

/** @typedef {xyz.swapee.wc.back.SwapeeSocialButtonsScreenAT} xyz.swapee.wc.back.BoundSwapeeSocialButtonsScreenAT */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeSocialButtonsScreenATCaster
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenATCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsScreenAT_ instance into the _BoundISwapeeSocialButtonsScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeSocialButtonsScreenAT}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenATCaster.prototype.asISwapeeSocialButtonsScreenAT
/**
 * Access the _SwapeeSocialButtonsScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeSocialButtonsScreenAT}
 */
xyz.swapee.wc.back.ISwapeeSocialButtonsScreenATCaster.prototype.superSwapeeSocialButtonsScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/design/80-ISwapeeSocialButtonsGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.Initialese} xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeSocialButtonsGPU)} xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeSocialButtonsGPU} xyz.swapee.wc.SwapeeSocialButtonsGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsGPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.constructor&xyz.swapee.wc.SwapeeSocialButtonsGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsGPU|typeof xyz.swapee.wc.SwapeeSocialButtonsGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsGPU}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsGPU|typeof xyz.swapee.wc.SwapeeSocialButtonsGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsGPU}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeSocialButtonsGPU|typeof xyz.swapee.wc.SwapeeSocialButtonsGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay|typeof xyz.swapee.wc.back.SwapeeSocialButtonsDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsGPU}
 */
xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese[]) => xyz.swapee.wc.ISwapeeSocialButtonsGPU} xyz.swapee.wc.SwapeeSocialButtonsGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeSocialButtonsGPUCaster&com.webcircuits.IBrowserView<.!SwapeeSocialButtonsMemory,>&xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay)} xyz.swapee.wc.ISwapeeSocialButtonsGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!SwapeeSocialButtonsMemory,>} com.webcircuits.IBrowserView<.!SwapeeSocialButtonsMemory,>.typeof */
/**
 * Handles the periphery of the _ISwapeeSocialButtonsDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsGPU
 */
xyz.swapee.wc.ISwapeeSocialButtonsGPU = class extends /** @type {xyz.swapee.wc.ISwapeeSocialButtonsGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!SwapeeSocialButtonsMemory,>.typeof&xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeSocialButtonsGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeSocialButtonsGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeSocialButtonsGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese>)} xyz.swapee.wc.SwapeeSocialButtonsGPU.constructor */
/**
 * A concrete class of _ISwapeeSocialButtonsGPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeSocialButtonsGPU
 * @implements {xyz.swapee.wc.ISwapeeSocialButtonsGPU} Handles the periphery of the _ISwapeeSocialButtonsDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeSocialButtonsGPU = class extends /** @type {xyz.swapee.wc.SwapeeSocialButtonsGPU.constructor&xyz.swapee.wc.ISwapeeSocialButtonsGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeSocialButtonsGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeSocialButtonsGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeSocialButtonsGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeSocialButtonsGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeSocialButtonsGPU}
 */
xyz.swapee.wc.SwapeeSocialButtonsGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeSocialButtonsGPU.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsGPUFields
 */
xyz.swapee.wc.ISwapeeSocialButtonsGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ISwapeeSocialButtonsGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsGPU} */
xyz.swapee.wc.RecordISwapeeSocialButtonsGPU

/** @typedef {xyz.swapee.wc.ISwapeeSocialButtonsGPU} xyz.swapee.wc.BoundISwapeeSocialButtonsGPU */

/** @typedef {xyz.swapee.wc.SwapeeSocialButtonsGPU} xyz.swapee.wc.BoundSwapeeSocialButtonsGPU */

/**
 * Contains getters to cast the _ISwapeeSocialButtonsGPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeSocialButtonsGPUCaster
 */
xyz.swapee.wc.ISwapeeSocialButtonsGPUCaster = class { }
/**
 * Cast the _ISwapeeSocialButtonsGPU_ instance into the _BoundISwapeeSocialButtonsGPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeSocialButtonsGPU}
 */
xyz.swapee.wc.ISwapeeSocialButtonsGPUCaster.prototype.asISwapeeSocialButtonsGPU
/**
 * Access the _SwapeeSocialButtonsGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeSocialButtonsGPU}
 */
xyz.swapee.wc.ISwapeeSocialButtonsGPUCaster.prototype.superSwapeeSocialButtonsGPU

// nss:xyz.swapee.wc
/* @typal-end */