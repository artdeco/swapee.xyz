import { SwapeeSocialButtonsDisplay, SwapeeSocialButtonsScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.SwapeeSocialButtonsDisplay} */
export { SwapeeSocialButtonsDisplay }
/** @lazy @api {xyz.swapee.wc.SwapeeSocialButtonsScreen} */
export { SwapeeSocialButtonsScreen }