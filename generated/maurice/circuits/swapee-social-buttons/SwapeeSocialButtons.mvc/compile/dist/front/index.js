/**
 * Display for presenting information from the _ISwapeeSocialButtons_.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsDisplay}
 */
class SwapeeSocialButtonsDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsScreen}
 */
class SwapeeSocialButtonsScreen extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeSocialButtonsDisplay = SwapeeSocialButtonsDisplay
module.exports.SwapeeSocialButtonsScreen = SwapeeSocialButtonsScreen