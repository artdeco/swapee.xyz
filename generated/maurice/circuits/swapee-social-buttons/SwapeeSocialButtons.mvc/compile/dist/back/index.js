/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtons` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtons}
 */
class AbstractSwapeeSocialButtons extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeSocialButtons_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsPort}
 */
class SwapeeSocialButtonsPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsController}
 */
class AbstractSwapeeSocialButtonsController extends (class {/* lazy-loaded */}) {}
/**
 * The _ISwapeeSocialButtons_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent}
 */
class SwapeeSocialButtonsHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsBuffer}
 */
class SwapeeSocialButtonsBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer}
 */
class AbstractSwapeeSocialButtonsComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.SwapeeSocialButtonsController}
 */
class SwapeeSocialButtonsController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeSocialButtons = AbstractSwapeeSocialButtons
module.exports.SwapeeSocialButtonsPort = SwapeeSocialButtonsPort
module.exports.AbstractSwapeeSocialButtonsController = AbstractSwapeeSocialButtonsController
module.exports.SwapeeSocialButtonsHtmlComponent = SwapeeSocialButtonsHtmlComponent
module.exports.SwapeeSocialButtonsBuffer = SwapeeSocialButtonsBuffer
module.exports.AbstractSwapeeSocialButtonsComputer = AbstractSwapeeSocialButtonsComputer
module.exports.SwapeeSocialButtonsController = SwapeeSocialButtonsController