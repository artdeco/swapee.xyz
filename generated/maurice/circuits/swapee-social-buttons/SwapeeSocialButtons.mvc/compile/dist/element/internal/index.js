import Module from './element'

/**@extends {xyz.swapee.wc.AbstractSwapeeSocialButtons}*/
export class AbstractSwapeeSocialButtons extends Module['94225766191'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtons} */
AbstractSwapeeSocialButtons.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeSocialButtonsPort} */
export const SwapeeSocialButtonsPort=Module['94225766193']
/**@extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsController}*/
export class AbstractSwapeeSocialButtonsController extends Module['94225766194'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsController} */
AbstractSwapeeSocialButtonsController.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeSocialButtonsElement} */
export const SwapeeSocialButtonsElement=Module['94225766198']
/** @type {typeof xyz.swapee.wc.SwapeeSocialButtonsBuffer} */
export const SwapeeSocialButtonsBuffer=Module['942257661911']
/**@extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer}*/
export class AbstractSwapeeSocialButtonsComputer extends Module['942257661930'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer} */
AbstractSwapeeSocialButtonsComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeSocialButtonsController} */
export const SwapeeSocialButtonsController=Module['942257661961']