/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtons` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtons}
 */
class AbstractSwapeeSocialButtons extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeSocialButtons_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsPort}
 */
class SwapeeSocialButtonsPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsController}
 */
class AbstractSwapeeSocialButtonsController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ISwapeeSocialButtons_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsElement}
 */
class SwapeeSocialButtonsElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsBuffer}
 */
class SwapeeSocialButtonsBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeSocialButtonsComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer}
 */
class AbstractSwapeeSocialButtonsComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.SwapeeSocialButtonsController}
 */
class SwapeeSocialButtonsController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeSocialButtons = AbstractSwapeeSocialButtons
module.exports.SwapeeSocialButtonsPort = SwapeeSocialButtonsPort
module.exports.AbstractSwapeeSocialButtonsController = AbstractSwapeeSocialButtonsController
module.exports.SwapeeSocialButtonsElement = SwapeeSocialButtonsElement
module.exports.SwapeeSocialButtonsBuffer = SwapeeSocialButtonsBuffer
module.exports.AbstractSwapeeSocialButtonsComputer = AbstractSwapeeSocialButtonsComputer
module.exports.SwapeeSocialButtonsController = SwapeeSocialButtonsController

Object.defineProperties(module.exports, {
 'AbstractSwapeeSocialButtons': {get: () => require('./precompile/internal')[94225766191]},
 [94225766191]: {get: () => module.exports['AbstractSwapeeSocialButtons']},
 'SwapeeSocialButtonsPort': {get: () => require('./precompile/internal')[94225766193]},
 [94225766193]: {get: () => module.exports['SwapeeSocialButtonsPort']},
 'AbstractSwapeeSocialButtonsController': {get: () => require('./precompile/internal')[94225766194]},
 [94225766194]: {get: () => module.exports['AbstractSwapeeSocialButtonsController']},
 'SwapeeSocialButtonsElement': {get: () => require('./precompile/internal')[94225766198]},
 [94225766198]: {get: () => module.exports['SwapeeSocialButtonsElement']},
 'SwapeeSocialButtonsBuffer': {get: () => require('./precompile/internal')[942257661911]},
 [942257661911]: {get: () => module.exports['SwapeeSocialButtonsBuffer']},
 'AbstractSwapeeSocialButtonsComputer': {get: () => require('./precompile/internal')[942257661930]},
 [942257661930]: {get: () => module.exports['AbstractSwapeeSocialButtonsComputer']},
 'SwapeeSocialButtonsController': {get: () => require('./precompile/internal')[942257661961]},
 [942257661961]: {get: () => module.exports['SwapeeSocialButtonsController']},
})