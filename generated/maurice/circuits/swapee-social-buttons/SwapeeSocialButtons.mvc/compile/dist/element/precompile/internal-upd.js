import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {9422576619} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const a=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const b=a["372700389811"];function e(c,d,h,k){return a["372700389812"](c,d,h,k,!1,void 0)};function f(){}f.prototype={};class g{}class l extends e(g,94225766196,null,{v:1,M:2}){}l[b]=[f];

const m=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const n=m["615055805212"],p=m["615055805218"],q=m["615055805233"];const r={core:"a74ad"};function t(){}t.prototype={};class u{}class v extends e(u,94225766195,null,{l:1,G:2}){}function w(){}w.prototype={};function x(){this.model={core:""}}class y{}class z extends e(y,94225766192,x,{s:1,J:2}){}z[b]=[w,{constructor(){q(this.model,"",r)}}];v[b]=[{},t,z];
const A=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const B=A.IntegratedComponentInitialiser,C=A.IntegratedComponent,D=A["95173443851"];function E(){}E.prototype={};class F{}class G extends e(F,94225766191,null,{i:1,D:2}){}G[b]=[E,p];const H={regulate:n({core:String})};
const I=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const J=I.IntegratedController,K=I.Parametric;const L={...r};function M(){}M.prototype={};function aa(){const c={model:null};x.call(c);this.inputs=c.model}class ba{}class N extends e(ba,94225766193,aa,{u:1,K:2}){}function O(){}N[b]=[O.prototype={},M,K,O.prototype={constructor(){q(this.inputs,"",L)}}];function P(){}P.prototype={};class ca{}class Q extends e(ca,942257661917,null,{j:1,F:2}){}Q[b]=[{},P,H,J,{get Port(){return N}}];function R(){}R.prototype={};class da{}class S extends e(da,94225766197,null,{g:1,A:2}){}S[b]=[R,v,l,C,G,Q];function ea(){return null};const fa=require(eval('"@type.engineering/web-computing"')).h;function ha(){return fa("div",{$id:"SwapeeSocialButtons"})};const ia=require(eval('"@type.engineering/web-computing"')).h;function ja(){return ia("div",{$id:"SwapeeSocialButtons"})};var T=class extends Q.implements(){};require("https");require("http");const ka=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(c){}ka("aqt");require("fs");require("child_process");
const U=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const la=U.ElementBase,ma=U.HTMLBlocker;function V(){}V.prototype={};function na(){this.inputs={noSolder:!1}}class oa{}class W extends e(oa,942257661911,na,{o:1,I:2}){}W[b]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0})}}];function X(){}X.prototype={};class pa{}class Y extends e(pa,942257661910,null,{m:1,H:2}){}function Z(){}
Y[b]=[X,la,Z.prototype={calibrate:function({":no-solder":c,":core":d}){const {attributes:{"no-solder":h,core:k}}=this;return{...(void 0===h?{"no-solder":c}:{}),...(void 0===k?{core:d}:{})}}},Z.prototype={calibrate:({"no-solder":c,core:d})=>({noSolder:c,core:d})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={inputsPQs:L,vdus:{}},C,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder core no-solder :no-solder :core fe646 a74ad children".split(" "))})},get Port(){return W}}];
Y[b]=[S,{rootId:"SwapeeSocialButtons",__$id:9422576619,fqn:"xyz.swapee.wc.ISwapeeSocialButtons",maurice_element_v3:!0}];class qa extends Y.implements(T,D,ma,B,{solder:ea,server:ha,render:ja},{classesMap:!0,rootSelector:".SwapeeSocialButtons",stylesheet:"html/styles/SwapeeSocialButtons.css",preserveClasses:["SocialButtons"],blockName:"html/SwapeeSocialButtonsBlock.html"}){};module.exports["94225766190"]=S;module.exports["94225766191"]=S;module.exports["94225766193"]=N;module.exports["94225766194"]=Q;module.exports["94225766198"]=qa;module.exports["942257661911"]=H;module.exports["942257661930"]=G;module.exports["942257661961"]=T;
/*! @embed-object-end {9422576619} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule