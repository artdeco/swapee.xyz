import {SwapeeSocialButtonsMemoryPQs} from './SwapeeSocialButtonsMemoryPQs'
export const SwapeeSocialButtonsMemoryQPs=/**@type {!xyz.swapee.wc.SwapeeSocialButtonsMemoryQPs}*/(Object.keys(SwapeeSocialButtonsMemoryPQs)
 .reduce((a,k)=>{a[SwapeeSocialButtonsMemoryPQs[k]]=k;return a},{}))