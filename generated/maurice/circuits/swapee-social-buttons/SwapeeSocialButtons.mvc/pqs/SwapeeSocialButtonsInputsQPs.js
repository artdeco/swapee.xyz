import {SwapeeSocialButtonsInputsPQs} from './SwapeeSocialButtonsInputsPQs'
export const SwapeeSocialButtonsInputsQPs=/**@type {!xyz.swapee.wc.SwapeeSocialButtonsInputsQPs}*/(Object.keys(SwapeeSocialButtonsInputsPQs)
 .reduce((a,k)=>{a[SwapeeSocialButtonsInputsPQs[k]]=k;return a},{}))