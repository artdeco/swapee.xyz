import {SwapeeSocialButtonsVdusPQs} from './SwapeeSocialButtonsVdusPQs'
export const SwapeeSocialButtonsVdusQPs=/**@type {!xyz.swapee.wc.SwapeeSocialButtonsVdusQPs}*/(Object.keys(SwapeeSocialButtonsVdusPQs)
 .reduce((a,k)=>{a[SwapeeSocialButtonsVdusPQs[k]]=k;return a},{}))