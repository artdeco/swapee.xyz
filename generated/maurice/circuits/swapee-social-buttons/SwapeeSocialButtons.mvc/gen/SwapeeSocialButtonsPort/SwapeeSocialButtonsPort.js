import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {SwapeeSocialButtonsInputsPQs} from '../../pqs/SwapeeSocialButtonsInputsPQs'
import {SwapeeSocialButtonsOuterCoreConstructor} from '../SwapeeSocialButtonsCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeSocialButtonsPort}
 */
function __SwapeeSocialButtonsPort() {}
__SwapeeSocialButtonsPort.prototype = /** @type {!_SwapeeSocialButtonsPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeSocialButtonsPort} */ function SwapeeSocialButtonsPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.SwapeeSocialButtonsOuterCore} */ ({model:null})
  SwapeeSocialButtonsOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsPort}
 */
class _SwapeeSocialButtonsPort { }
/**
 * The port that serves as an interface to the _ISwapeeSocialButtons_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsPort} ‎
 */
export class SwapeeSocialButtonsPort extends newAbstract(
 _SwapeeSocialButtonsPort,94225766193,SwapeeSocialButtonsPortConstructor,{
  asISwapeeSocialButtonsPort:1,
  superSwapeeSocialButtonsPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsPort} */
SwapeeSocialButtonsPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsPort} */
function SwapeeSocialButtonsPortClass(){}

export const SwapeeSocialButtonsPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ISwapeeSocialButtons.Pinout>}*/({
 get Port() { return SwapeeSocialButtonsPort },
})

SwapeeSocialButtonsPort[$implementations]=[
 SwapeeSocialButtonsPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsPort}*/({
  resetPort(){
   this.resetSwapeeSocialButtonsPort()
  },
  resetSwapeeSocialButtonsPort(){
   SwapeeSocialButtonsPortConstructor.call(this)
  },
 }),
 __SwapeeSocialButtonsPort,
 Parametric,
 SwapeeSocialButtonsPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsPort}*/({
  constructor(){
   mountPins(this.inputs,'',SwapeeSocialButtonsInputsPQs)
  },
 }),
]


export default SwapeeSocialButtonsPort