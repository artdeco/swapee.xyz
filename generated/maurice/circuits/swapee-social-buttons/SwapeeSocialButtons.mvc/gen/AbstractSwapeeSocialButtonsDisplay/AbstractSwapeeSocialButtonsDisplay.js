import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsDisplay}
 */
function __AbstractSwapeeSocialButtonsDisplay() {}
__AbstractSwapeeSocialButtonsDisplay.prototype = /** @type {!_AbstractSwapeeSocialButtonsDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay}
 */
class _AbstractSwapeeSocialButtonsDisplay { }
/**
 * Display for presenting information from the _ISwapeeSocialButtons_.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay} ‎
 */
class AbstractSwapeeSocialButtonsDisplay extends newAbstract(
 _AbstractSwapeeSocialButtonsDisplay,942257661914,null,{
  asISwapeeSocialButtonsDisplay:1,
  superSwapeeSocialButtonsDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay} */
AbstractSwapeeSocialButtonsDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsDisplay} */
function AbstractSwapeeSocialButtonsDisplayClass(){}

export default AbstractSwapeeSocialButtonsDisplay


AbstractSwapeeSocialButtonsDisplay[$implementations]=[
 __AbstractSwapeeSocialButtonsDisplay,
 Display,
]