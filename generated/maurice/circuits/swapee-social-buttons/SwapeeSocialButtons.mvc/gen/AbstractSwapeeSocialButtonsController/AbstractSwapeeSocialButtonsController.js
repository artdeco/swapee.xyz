import SwapeeSocialButtonsBuffer from '../SwapeeSocialButtonsBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {SwapeeSocialButtonsPortConnector} from '../SwapeeSocialButtonsPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsController}
 */
function __AbstractSwapeeSocialButtonsController() {}
__AbstractSwapeeSocialButtonsController.prototype = /** @type {!_AbstractSwapeeSocialButtonsController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsController}
 */
class _AbstractSwapeeSocialButtonsController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsController} ‎
 */
export class AbstractSwapeeSocialButtonsController extends newAbstract(
 _AbstractSwapeeSocialButtonsController,942257661917,null,{
  asISwapeeSocialButtonsController:1,
  superSwapeeSocialButtonsController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsController} */
AbstractSwapeeSocialButtonsController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsController} */
function AbstractSwapeeSocialButtonsControllerClass(){}


AbstractSwapeeSocialButtonsController[$implementations]=[
 AbstractSwapeeSocialButtonsControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.ISwapeeSocialButtonsPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractSwapeeSocialButtonsController,
 SwapeeSocialButtonsBuffer,
 IntegratedController,
 /**@type {!AbstractSwapeeSocialButtonsController}*/(SwapeeSocialButtonsPortConnector),
]


export default AbstractSwapeeSocialButtonsController