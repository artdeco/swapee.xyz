import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsScreenAT}
 */
function __AbstractSwapeeSocialButtonsScreenAT() {}
__AbstractSwapeeSocialButtonsScreenAT.prototype = /** @type {!_AbstractSwapeeSocialButtonsScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT}
 */
class _AbstractSwapeeSocialButtonsScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeSocialButtonsScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT} ‎
 */
class AbstractSwapeeSocialButtonsScreenAT extends newAbstract(
 _AbstractSwapeeSocialButtonsScreenAT,942257661925,null,{
  asISwapeeSocialButtonsScreenAT:1,
  superSwapeeSocialButtonsScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT} */
AbstractSwapeeSocialButtonsScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreenAT} */
function AbstractSwapeeSocialButtonsScreenATClass(){}

export default AbstractSwapeeSocialButtonsScreenAT


AbstractSwapeeSocialButtonsScreenAT[$implementations]=[
 __AbstractSwapeeSocialButtonsScreenAT,
 UartUniversal,
]