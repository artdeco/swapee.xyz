import AbstractSwapeeSocialButtonsDisplay from '../AbstractSwapeeSocialButtonsDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {SwapeeSocialButtonsVdusPQs} from '../../pqs/SwapeeSocialButtonsVdusPQs'
import {SwapeeSocialButtonsVdusQPs} from '../../pqs/SwapeeSocialButtonsVdusQPs'
import {SwapeeSocialButtonsMemoryPQs} from '../../pqs/SwapeeSocialButtonsMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsGPU}
 */
function __AbstractSwapeeSocialButtonsGPU() {}
__AbstractSwapeeSocialButtonsGPU.prototype = /** @type {!_AbstractSwapeeSocialButtonsGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU}
 */
class _AbstractSwapeeSocialButtonsGPU { }
/**
 * Handles the periphery of the _ISwapeeSocialButtonsDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU} ‎
 */
class AbstractSwapeeSocialButtonsGPU extends newAbstract(
 _AbstractSwapeeSocialButtonsGPU,942257661913,null,{
  asISwapeeSocialButtonsGPU:1,
  superSwapeeSocialButtonsGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU} */
AbstractSwapeeSocialButtonsGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsGPU} */
function AbstractSwapeeSocialButtonsGPUClass(){}

export default AbstractSwapeeSocialButtonsGPU


AbstractSwapeeSocialButtonsGPU[$implementations]=[
 __AbstractSwapeeSocialButtonsGPU,
 AbstractSwapeeSocialButtonsGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsGPU}*/({
  vdusPQs:SwapeeSocialButtonsVdusPQs,
  vdusQPs:SwapeeSocialButtonsVdusQPs,
  memoryPQs:SwapeeSocialButtonsMemoryPQs,
 }),
 AbstractSwapeeSocialButtonsDisplay,
 BrowserView,
]