import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsControllerAR}
 */
function __AbstractSwapeeSocialButtonsControllerAR() {}
__AbstractSwapeeSocialButtonsControllerAR.prototype = /** @type {!_AbstractSwapeeSocialButtonsControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR}
 */
class _AbstractSwapeeSocialButtonsControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeSocialButtonsControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR} ‎
 */
class AbstractSwapeeSocialButtonsControllerAR extends newAbstract(
 _AbstractSwapeeSocialButtonsControllerAR,942257661920,null,{
  asISwapeeSocialButtonsControllerAR:1,
  superSwapeeSocialButtonsControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR} */
AbstractSwapeeSocialButtonsControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsControllerAR} */
function AbstractSwapeeSocialButtonsControllerARClass(){}

export default AbstractSwapeeSocialButtonsControllerAR


AbstractSwapeeSocialButtonsControllerAR[$implementations]=[
 __AbstractSwapeeSocialButtonsControllerAR,
 AR,
 AbstractSwapeeSocialButtonsControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]