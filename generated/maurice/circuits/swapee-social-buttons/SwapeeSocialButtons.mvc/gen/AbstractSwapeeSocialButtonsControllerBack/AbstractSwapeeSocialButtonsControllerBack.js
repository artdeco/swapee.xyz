import AbstractSwapeeSocialButtonsControllerAR from '../AbstractSwapeeSocialButtonsControllerAR'
import {AbstractSwapeeSocialButtonsController} from '../AbstractSwapeeSocialButtonsController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsControllerBack}
 */
function __AbstractSwapeeSocialButtonsControllerBack() {}
__AbstractSwapeeSocialButtonsControllerBack.prototype = /** @type {!_AbstractSwapeeSocialButtonsControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController}
 */
class _AbstractSwapeeSocialButtonsControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController} ‎
 */
class AbstractSwapeeSocialButtonsControllerBack extends newAbstract(
 _AbstractSwapeeSocialButtonsControllerBack,942257661919,null,{
  asISwapeeSocialButtonsController:1,
  superSwapeeSocialButtonsController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController} */
AbstractSwapeeSocialButtonsControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsController} */
function AbstractSwapeeSocialButtonsControllerBackClass(){}

export default AbstractSwapeeSocialButtonsControllerBack


AbstractSwapeeSocialButtonsControllerBack[$implementations]=[
 __AbstractSwapeeSocialButtonsControllerBack,
 AbstractSwapeeSocialButtonsController,
 AbstractSwapeeSocialButtonsControllerAR,
 DriverBack,
]