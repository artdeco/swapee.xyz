import {mountPins} from '@webcircuits/webcircuits'
import {SwapeeSocialButtonsMemoryPQs} from '../../pqs/SwapeeSocialButtonsMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeSocialButtonsCore}
 */
function __SwapeeSocialButtonsCore() {}
__SwapeeSocialButtonsCore.prototype = /** @type {!_SwapeeSocialButtonsCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsCore}
 */
class _SwapeeSocialButtonsCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsCore} ‎
 */
class SwapeeSocialButtonsCore extends newAbstract(
 _SwapeeSocialButtonsCore,94225766195,null,{
  asISwapeeSocialButtonsCore:1,
  superSwapeeSocialButtonsCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsCore} */
SwapeeSocialButtonsCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsCore} */
function SwapeeSocialButtonsCoreClass(){}

export default SwapeeSocialButtonsCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeSocialButtonsOuterCore}
 */
function __SwapeeSocialButtonsOuterCore() {}
__SwapeeSocialButtonsOuterCore.prototype = /** @type {!_SwapeeSocialButtonsOuterCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeSocialButtonsOuterCore} */
export function SwapeeSocialButtonsOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore.Model}*/
  this.model={
    core: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore}
 */
class _SwapeeSocialButtonsOuterCore { }
/**
 * The _ISwapeeSocialButtons_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore} ‎
 */
export class SwapeeSocialButtonsOuterCore extends newAbstract(
 _SwapeeSocialButtonsOuterCore,94225766192,SwapeeSocialButtonsOuterCoreConstructor,{
  asISwapeeSocialButtonsOuterCore:1,
  superSwapeeSocialButtonsOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore} */
SwapeeSocialButtonsOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsOuterCore} */
function SwapeeSocialButtonsOuterCoreClass(){}


SwapeeSocialButtonsOuterCore[$implementations]=[
 __SwapeeSocialButtonsOuterCore,
 SwapeeSocialButtonsOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore}*/({
  constructor(){
   mountPins(this.model,'',SwapeeSocialButtonsMemoryPQs)

  },
 }),
]

SwapeeSocialButtonsCore[$implementations]=[
 SwapeeSocialButtonsCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsCore}*/({
  resetCore(){
   this.resetSwapeeSocialButtonsCore()
  },
  resetSwapeeSocialButtonsCore(){
   SwapeeSocialButtonsOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.SwapeeSocialButtonsOuterCore}*/(
     /**@type {!xyz.swapee.wc.ISwapeeSocialButtonsOuterCore}*/(this)),
   )
  },
 }),
 __SwapeeSocialButtonsCore,
 SwapeeSocialButtonsOuterCore,
]

export {SwapeeSocialButtonsCore}