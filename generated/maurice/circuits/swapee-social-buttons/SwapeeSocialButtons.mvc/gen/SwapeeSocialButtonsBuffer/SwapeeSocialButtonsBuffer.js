import {makeBuffers} from '@webcircuits/webcircuits'

export const SwapeeSocialButtonsBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  core:String,
 }),
})

export default SwapeeSocialButtonsBuffer