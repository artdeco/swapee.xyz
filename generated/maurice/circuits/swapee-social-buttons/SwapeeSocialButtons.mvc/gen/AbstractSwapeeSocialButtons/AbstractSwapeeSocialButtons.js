import AbstractSwapeeSocialButtonsProcessor from '../AbstractSwapeeSocialButtonsProcessor'
import {SwapeeSocialButtonsCore} from '../SwapeeSocialButtonsCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractSwapeeSocialButtonsComputer} from '../AbstractSwapeeSocialButtonsComputer'
import {AbstractSwapeeSocialButtonsController} from '../AbstractSwapeeSocialButtonsController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtons}
 */
function __AbstractSwapeeSocialButtons() {}
__AbstractSwapeeSocialButtons.prototype = /** @type {!_AbstractSwapeeSocialButtons} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtons}
 */
class _AbstractSwapeeSocialButtons { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtons} ‎
 */
class AbstractSwapeeSocialButtons extends newAbstract(
 _AbstractSwapeeSocialButtons,94225766197,null,{
  asISwapeeSocialButtons:1,
  superSwapeeSocialButtons:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtons} */
AbstractSwapeeSocialButtons.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtons} */
function AbstractSwapeeSocialButtonsClass(){}

export default AbstractSwapeeSocialButtons


AbstractSwapeeSocialButtons[$implementations]=[
 __AbstractSwapeeSocialButtons,
 SwapeeSocialButtonsCore,
 AbstractSwapeeSocialButtonsProcessor,
 IntegratedComponent,
 AbstractSwapeeSocialButtonsComputer,
 AbstractSwapeeSocialButtonsController,
]


export {AbstractSwapeeSocialButtons}