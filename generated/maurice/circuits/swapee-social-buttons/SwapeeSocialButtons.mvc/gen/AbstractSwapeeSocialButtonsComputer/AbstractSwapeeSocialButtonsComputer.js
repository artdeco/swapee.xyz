import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsComputer}
 */
function __AbstractSwapeeSocialButtonsComputer() {}
__AbstractSwapeeSocialButtonsComputer.prototype = /** @type {!_AbstractSwapeeSocialButtonsComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer}
 */
class _AbstractSwapeeSocialButtonsComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer} ‎
 */
export class AbstractSwapeeSocialButtonsComputer extends newAbstract(
 _AbstractSwapeeSocialButtonsComputer,94225766191,null,{
  asISwapeeSocialButtonsComputer:1,
  superSwapeeSocialButtonsComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer} */
AbstractSwapeeSocialButtonsComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer} */
function AbstractSwapeeSocialButtonsComputerClass(){}


AbstractSwapeeSocialButtonsComputer[$implementations]=[
 __AbstractSwapeeSocialButtonsComputer,
 Adapter,
]


export default AbstractSwapeeSocialButtonsComputer