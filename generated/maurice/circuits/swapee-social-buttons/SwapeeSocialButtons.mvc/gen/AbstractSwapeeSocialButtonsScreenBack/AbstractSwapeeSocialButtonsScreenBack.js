import AbstractSwapeeSocialButtonsScreenAT from '../AbstractSwapeeSocialButtonsScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsScreenBack}
 */
function __AbstractSwapeeSocialButtonsScreenBack() {}
__AbstractSwapeeSocialButtonsScreenBack.prototype = /** @type {!_AbstractSwapeeSocialButtonsScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen}
 */
class _AbstractSwapeeSocialButtonsScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen} ‎
 */
class AbstractSwapeeSocialButtonsScreenBack extends newAbstract(
 _AbstractSwapeeSocialButtonsScreenBack,942257661923,null,{
  asISwapeeSocialButtonsScreen:1,
  superSwapeeSocialButtonsScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen} */
AbstractSwapeeSocialButtonsScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsScreen} */
function AbstractSwapeeSocialButtonsScreenBackClass(){}

export default AbstractSwapeeSocialButtonsScreenBack


AbstractSwapeeSocialButtonsScreenBack[$implementations]=[
 __AbstractSwapeeSocialButtonsScreenBack,
 AbstractSwapeeSocialButtonsScreenAT,
]