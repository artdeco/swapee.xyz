import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeSocialButtonsElementPort}
 */
function __SwapeeSocialButtonsElementPort() {}
__SwapeeSocialButtonsElementPort.prototype = /** @type {!_SwapeeSocialButtonsElementPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeSocialButtonsElementPort} */ function SwapeeSocialButtonsElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort}
 */
class _SwapeeSocialButtonsElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort} ‎
 */
class SwapeeSocialButtonsElementPort extends newAbstract(
 _SwapeeSocialButtonsElementPort,942257661911,SwapeeSocialButtonsElementPortConstructor,{
  asISwapeeSocialButtonsElementPort:1,
  superSwapeeSocialButtonsElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort} */
SwapeeSocialButtonsElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElementPort} */
function SwapeeSocialButtonsElementPortClass(){}

export default SwapeeSocialButtonsElementPort


SwapeeSocialButtonsElementPort[$implementations]=[
 __SwapeeSocialButtonsElementPort,
 SwapeeSocialButtonsElementPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
   })
  },
 }),
]