import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsDisplay}
 */
function __AbstractSwapeeSocialButtonsDisplay() {}
__AbstractSwapeeSocialButtonsDisplay.prototype = /** @type {!_AbstractSwapeeSocialButtonsDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay}
 */
class _AbstractSwapeeSocialButtonsDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay} ‎
 */
class AbstractSwapeeSocialButtonsDisplay extends newAbstract(
 _AbstractSwapeeSocialButtonsDisplay,942257661916,null,{
  asISwapeeSocialButtonsDisplay:1,
  superSwapeeSocialButtonsDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay} */
AbstractSwapeeSocialButtonsDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeSocialButtonsDisplay} */
function AbstractSwapeeSocialButtonsDisplayClass(){}

export default AbstractSwapeeSocialButtonsDisplay


AbstractSwapeeSocialButtonsDisplay[$implementations]=[
 __AbstractSwapeeSocialButtonsDisplay,
 GraphicsDriverBack,
]