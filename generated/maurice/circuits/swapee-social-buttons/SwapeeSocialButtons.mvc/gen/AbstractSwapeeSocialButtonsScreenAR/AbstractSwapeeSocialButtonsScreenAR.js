import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsScreenAR}
 */
function __AbstractSwapeeSocialButtonsScreenAR() {}
__AbstractSwapeeSocialButtonsScreenAR.prototype = /** @type {!_AbstractSwapeeSocialButtonsScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR}
 */
class _AbstractSwapeeSocialButtonsScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeSocialButtonsScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR} ‎
 */
class AbstractSwapeeSocialButtonsScreenAR extends newAbstract(
 _AbstractSwapeeSocialButtonsScreenAR,942257661924,null,{
  asISwapeeSocialButtonsScreenAR:1,
  superSwapeeSocialButtonsScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR} */
AbstractSwapeeSocialButtonsScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsScreenAR} */
function AbstractSwapeeSocialButtonsScreenARClass(){}

export default AbstractSwapeeSocialButtonsScreenAR


AbstractSwapeeSocialButtonsScreenAR[$implementations]=[
 __AbstractSwapeeSocialButtonsScreenAR,
 AR,
 AbstractSwapeeSocialButtonsScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractSwapeeSocialButtonsScreenAR}