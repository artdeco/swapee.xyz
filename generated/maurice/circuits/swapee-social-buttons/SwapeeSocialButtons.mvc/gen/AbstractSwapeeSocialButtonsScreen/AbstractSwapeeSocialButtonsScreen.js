import AbstractSwapeeSocialButtonsScreenAR from '../AbstractSwapeeSocialButtonsScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {SwapeeSocialButtonsInputsPQs} from '../../pqs/SwapeeSocialButtonsInputsPQs'
import {SwapeeSocialButtonsMemoryQPs} from '../../pqs/SwapeeSocialButtonsMemoryQPs'
import {SwapeeSocialButtonsVdusPQs} from '../../pqs/SwapeeSocialButtonsVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsScreen}
 */
function __AbstractSwapeeSocialButtonsScreen() {}
__AbstractSwapeeSocialButtonsScreen.prototype = /** @type {!_AbstractSwapeeSocialButtonsScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen}
 */
class _AbstractSwapeeSocialButtonsScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen} ‎
 */
class AbstractSwapeeSocialButtonsScreen extends newAbstract(
 _AbstractSwapeeSocialButtonsScreen,942257661922,null,{
  asISwapeeSocialButtonsScreen:1,
  superSwapeeSocialButtonsScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen} */
AbstractSwapeeSocialButtonsScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsScreen} */
function AbstractSwapeeSocialButtonsScreenClass(){}

export default AbstractSwapeeSocialButtonsScreen


AbstractSwapeeSocialButtonsScreen[$implementations]=[
 __AbstractSwapeeSocialButtonsScreen,
 AbstractSwapeeSocialButtonsScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsScreen}*/({
  inputsPQs:SwapeeSocialButtonsInputsPQs,
  memoryQPs:SwapeeSocialButtonsMemoryQPs,
 }),
 Screen,
 AbstractSwapeeSocialButtonsScreenAR,
 AbstractSwapeeSocialButtonsScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsScreen}*/({
  vdusPQs:SwapeeSocialButtonsVdusPQs,
 }),
]