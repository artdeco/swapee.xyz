import AbstractSwapeeSocialButtonsGPU from '../AbstractSwapeeSocialButtonsGPU'
import AbstractSwapeeSocialButtonsScreenBack from '../AbstractSwapeeSocialButtonsScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {SwapeeSocialButtonsInputsQPs} from '../../pqs/SwapeeSocialButtonsInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeSocialButtons from '../AbstractSwapeeSocialButtons'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsHtmlComponent}
 */
function __AbstractSwapeeSocialButtonsHtmlComponent() {}
__AbstractSwapeeSocialButtonsHtmlComponent.prototype = /** @type {!_AbstractSwapeeSocialButtonsHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent}
 */
class _AbstractSwapeeSocialButtonsHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.SwapeeSocialButtonsHtmlComponent} */ (res)
  }
}
/**
 * The _ISwapeeSocialButtons_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent} ‎
 */
export class AbstractSwapeeSocialButtonsHtmlComponent extends newAbstract(
 _AbstractSwapeeSocialButtonsHtmlComponent,94225766199,null,{
  asISwapeeSocialButtonsHtmlComponent:1,
  superSwapeeSocialButtonsHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent} */
AbstractSwapeeSocialButtonsHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent} */
function AbstractSwapeeSocialButtonsHtmlComponentClass(){}


AbstractSwapeeSocialButtonsHtmlComponent[$implementations]=[
 __AbstractSwapeeSocialButtonsHtmlComponent,
 HtmlComponent,
 AbstractSwapeeSocialButtons,
 AbstractSwapeeSocialButtonsGPU,
 AbstractSwapeeSocialButtonsScreenBack,
 AbstractSwapeeSocialButtonsHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent}*/({
  inputsQPs:SwapeeSocialButtonsInputsQPs,
 }),
]