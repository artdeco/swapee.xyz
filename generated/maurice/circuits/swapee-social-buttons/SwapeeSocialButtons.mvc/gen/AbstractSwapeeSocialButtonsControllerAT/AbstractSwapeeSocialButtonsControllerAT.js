import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsControllerAT}
 */
function __AbstractSwapeeSocialButtonsControllerAT() {}
__AbstractSwapeeSocialButtonsControllerAT.prototype = /** @type {!_AbstractSwapeeSocialButtonsControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT}
 */
class _AbstractSwapeeSocialButtonsControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeSocialButtonsControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT} ‎
 */
class AbstractSwapeeSocialButtonsControllerAT extends newAbstract(
 _AbstractSwapeeSocialButtonsControllerAT,942257661921,null,{
  asISwapeeSocialButtonsControllerAT:1,
  superSwapeeSocialButtonsControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT} */
AbstractSwapeeSocialButtonsControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeSocialButtonsControllerAT} */
function AbstractSwapeeSocialButtonsControllerATClass(){}

export default AbstractSwapeeSocialButtonsControllerAT


AbstractSwapeeSocialButtonsControllerAT[$implementations]=[
 __AbstractSwapeeSocialButtonsControllerAT,
 UartUniversal,
 AbstractSwapeeSocialButtonsControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT}*/({
  get asISwapeeSocialButtonsController(){
   return this
  },
 }),
]