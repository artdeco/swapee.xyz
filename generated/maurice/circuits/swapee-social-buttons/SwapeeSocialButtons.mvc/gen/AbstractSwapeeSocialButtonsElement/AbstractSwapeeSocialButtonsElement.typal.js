
import AbstractSwapeeSocialButtons from '../AbstractSwapeeSocialButtons'

/** @abstract {xyz.swapee.wc.ISwapeeSocialButtonsElement} */
export default class AbstractSwapeeSocialButtonsElement { }



AbstractSwapeeSocialButtonsElement[$implementations]=[AbstractSwapeeSocialButtons,
 /** @type {!AbstractSwapeeSocialButtonsElement} */ ({
  rootId:'SwapeeSocialButtons',
  __$id:9422576619,
  fqn:'xyz.swapee.wc.ISwapeeSocialButtons',
  maurice_element_v3:true,
 }),
]