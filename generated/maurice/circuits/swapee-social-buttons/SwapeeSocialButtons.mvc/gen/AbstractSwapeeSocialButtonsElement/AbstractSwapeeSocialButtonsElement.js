import SwapeeSocialButtonsElementPort from '../SwapeeSocialButtonsElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {SwapeeSocialButtonsInputsPQs} from '../../pqs/SwapeeSocialButtonsInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeSocialButtons from '../AbstractSwapeeSocialButtons'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeSocialButtonsElement}
 */
function __AbstractSwapeeSocialButtonsElement() {}
__AbstractSwapeeSocialButtonsElement.prototype = /** @type {!_AbstractSwapeeSocialButtonsElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsElement}
 */
class _AbstractSwapeeSocialButtonsElement { }
/**
 * A component description.
 *
 * The _ISwapeeSocialButtons_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsElement} ‎
 */
class AbstractSwapeeSocialButtonsElement extends newAbstract(
 _AbstractSwapeeSocialButtonsElement,942257661910,null,{
  asISwapeeSocialButtonsElement:1,
  superSwapeeSocialButtonsElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElement} */
AbstractSwapeeSocialButtonsElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeSocialButtonsElement} */
function AbstractSwapeeSocialButtonsElementClass(){}

export default AbstractSwapeeSocialButtonsElement


AbstractSwapeeSocialButtonsElement[$implementations]=[
 __AbstractSwapeeSocialButtonsElement,
 ElementBase,
 AbstractSwapeeSocialButtonsElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':core':coreColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'core':coreAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(coreAttr===undefined?{'core':coreColAttr}:{}),
   }
  },
 }),
 AbstractSwapeeSocialButtonsElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'core':coreAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    core:coreAttr,
   }
  },
 }),
 AbstractSwapeeSocialButtonsElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractSwapeeSocialButtonsElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElement}*/({
  inputsPQs:SwapeeSocialButtonsInputsPQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractSwapeeSocialButtonsElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeSocialButtonsElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','core','no-solder',':no-solder',':core','fe646','a74ad','children']),
   })
  },
  get Port(){
   return SwapeeSocialButtonsElementPort
  },
 }),
]



AbstractSwapeeSocialButtonsElement[$implementations]=[AbstractSwapeeSocialButtons,
 /** @type {!AbstractSwapeeSocialButtonsElement} */ ({
  rootId:'SwapeeSocialButtons',
  __$id:9422576619,
  fqn:'xyz.swapee.wc.ISwapeeSocialButtons',
  maurice_element_v3:true,
 }),
]