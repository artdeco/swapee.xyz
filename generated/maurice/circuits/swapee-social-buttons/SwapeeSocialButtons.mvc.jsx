/** @extends {xyz.swapee.wc.AbstractSwapeeSocialButtons} */
export default class AbstractSwapeeSocialButtons extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsComputer} */
export class AbstractSwapeeSocialButtonsComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsController} */
export class AbstractSwapeeSocialButtonsController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsPort} */
export class SwapeeSocialButtonsPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsView} */
export class AbstractSwapeeSocialButtonsView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsElement} */
export class AbstractSwapeeSocialButtonsElement extends (<element v3 html mv>
 <block src="./SwapeeSocialButtons.mvc/src/SwapeeSocialButtonsElement/methods/render.jsx" />
 <inducer src="./SwapeeSocialButtons.mvc/src/SwapeeSocialButtonsElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeSocialButtonsHtmlComponent} */
export class AbstractSwapeeSocialButtonsHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>