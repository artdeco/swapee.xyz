/** @extends {xyz.swapee.wc.AbstractOffersTable} */
export default class AbstractOffersTable extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractOffersTableComputer} */
export class AbstractOffersTableComputer extends (<computer>
   <adapter name="adaptChangellyFixedLink">
    <xyz.swapee.wc.IExchangeIntentCore currencyFrom currencyTo amountFrom />
    <outputs>
     <xyz.swapee.wc.IOffersTableCore changellyFixedLink />
    </outputs>
   </adapter>
   <adapter name="adaptChangellyFloatLink">
    <xyz.swapee.wc.IExchangeIntentCore currencyFrom currencyTo amountFrom />
    <outputs>
     <xyz.swapee.wc.IOffersTableCore changellyFloatLink />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersTableController} */
export class AbstractOffersTableController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOffersTablePort} */
export class OffersTablePort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOffersTableView} */
export class AbstractOffersTableView extends (<view>
  <classes>
   <string opt name="ColHeading">The class.</string>
   <string opt name="Loading" />
   <string opt name="Hidden" />
   <string opt name="SelectedRateType" />
   <string opt name="BestOffer">Applied to the offer row which is the best offer.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersTableElement} */
export class AbstractOffersTableElement extends (<element v3 html mv>
 <block src="./OffersTable.mvc/src/OffersTableElement/methods/render.jsx" />
 <inducer src="./OffersTable.mvc/src/OffersTableElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersTableHtmlComponent} */
export class AbstractOffersTableHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
   <xyz.swapee.wc.IOffersAggregator via="OffersAggregator" />
   <com.webcircuits.ui.ICollapsar via="ProgressCollapsar" />
   <xyz.swapee.wc.ICryptoSelect via="CryptoSelectOut" />
  </connectors>

</html-ic>) { }
// </class-end>