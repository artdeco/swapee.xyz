/** @const {?} */ $xyz.swapee.wc.IOffersTableElementPort
/** @const {?} */ $xyz.swapee.wc.IOffersTableElementPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Initialese filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IOffersTableElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Initialese} */
xyz.swapee.wc.IOffersTableElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPortFields filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @interface */
$xyz.swapee.wc.IOffersTableElementPortFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTableElementPort.Inputs} */
$xyz.swapee.wc.IOffersTableElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOffersTableElementPort.Inputs} */
$xyz.swapee.wc.IOffersTableElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableElementPortFields}
 */
xyz.swapee.wc.IOffersTableElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPortCaster filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @interface */
$xyz.swapee.wc.IOffersTableElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableElementPort} */
$xyz.swapee.wc.IOffersTableElementPortCaster.prototype.asIOffersTableElementPort
/** @type {!xyz.swapee.wc.BoundOffersTableElementPort} */
$xyz.swapee.wc.IOffersTableElementPortCaster.prototype.superOffersTableElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableElementPortCaster}
 */
xyz.swapee.wc.IOffersTableElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersTableElementPort.Inputs>}
 */
$xyz.swapee.wc.IOffersTableElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableElementPort}
 */
xyz.swapee.wc.IOffersTableElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.OffersTableElementPort filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElementPort.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableElementPort.Initialese>}
 */
$xyz.swapee.wc.OffersTableElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.OffersTableElementPort
/** @type {function(new: xyz.swapee.wc.IOffersTableElementPort, ...!xyz.swapee.wc.IOffersTableElementPort.Initialese)} */
xyz.swapee.wc.OffersTableElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.OffersTableElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.AbstractOffersTableElementPort filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElementPort.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableElementPort}
 */
$xyz.swapee.wc.AbstractOffersTableElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableElementPort)} */
xyz.swapee.wc.AbstractOffersTableElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableElementPort|typeof xyz.swapee.wc.OffersTableElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableElementPort|typeof xyz.swapee.wc.OffersTableElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableElementPort|typeof xyz.swapee.wc.OffersTableElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.OffersTableElementPortConstructor filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableElementPort, ...!xyz.swapee.wc.IOffersTableElementPort.Initialese)} */
xyz.swapee.wc.OffersTableElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.RecordIOffersTableElementPort filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.BoundIOffersTableElementPort filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableElementPortFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOffersTableElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundIOffersTableElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableElementPort} */
xyz.swapee.wc.BoundIOffersTableElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.BoundOffersTableElementPort filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableElementPort} */
xyz.swapee.wc.BoundOffersTableElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts.coinImWrOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts.coinImWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts.changellyFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts.changellyFloatingOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts.changellyFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts.changellyFloatingOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts.changellyFloatLinkOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts.changellyFloatLinkOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts.changellyFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts.changellyFixedOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts.changellyFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts.changellyFixedOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts.changellyFixedLinkOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts.changellyFixedLinkOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts.letsExchangeFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts.letsExchangeFloatingOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts.letsExchangeFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts.letsExchangeFloatingOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts.letsExchangeFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts.letsExchangeFixedOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts.letsExchangeFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts.letsExchangeFixedOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts.changeNowFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts.changeNowFloatingOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts.changeNowFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts.changeNowFloatingOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts.changeNowFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts.changeNowFixedOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts.changeNowFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts.changeNowFixedOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts.offerCryptoOutOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts.offerCryptoOutOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts.resetInOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts.resetInOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts.resetNowBuOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts.resetNowBuOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts.exchangesLoadedOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts.exchangesLoadedOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts.exchangesTotalOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts.exchangesTotalOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts.progressCollapsarOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts.progressCollapsarOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts.cryptoSelectOutOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts.cryptoSelectOutOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts.offersAggregatorOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts.offersAggregatorOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts.prototype.changellyFloatLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts.prototype.changellyFixedLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts.prototype.letsExchangeFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts.prototype.letsExchangeFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts.prototype.letsExchangeFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts.prototype.letsExchangeFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts.prototype.changeNowFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts.prototype.changeNowFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts.prototype.changeNowFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts.prototype.changeNowFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts.prototype.offerCryptoOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts.prototype.resetInOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts.prototype.resetNowBuOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts.prototype.exchangesLoadedOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts.prototype.exchangesTotalOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts.prototype.progressCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts.prototype.cryptoSelectOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts.prototype.offersAggregatorOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts}
 */
$xyz.swapee.wc.IOffersTableElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersTableElementPort.Inputs}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts.prototype.changellyFloatLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts.prototype.changellyFixedLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts.prototype.letsExchangeFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts.prototype.letsExchangeFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts.prototype.letsExchangeFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts.prototype.letsExchangeFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts.prototype.changeNowFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts.prototype.changeNowFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts.prototype.changeNowFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts.prototype.changeNowFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts.prototype.offerCryptoOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts.prototype.resetInOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts.prototype.resetNowBuOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts.prototype.exchangesLoadedOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts.prototype.exchangesTotalOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts.prototype.progressCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts.prototype.cryptoSelectOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts.prototype.offersAggregatorOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts}
 */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs}
 */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts_Safe.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts_Safe.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts_Safe.prototype.changellyFloatLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts_Safe.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts_Safe.prototype.changellyFixedLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts_Safe.prototype.letsExchangeFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts_Safe.prototype.letsExchangeFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts_Safe.prototype.letsExchangeFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts_Safe.prototype.letsExchangeFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts_Safe.prototype.changeNowFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts_Safe.prototype.changeNowFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts_Safe.prototype.changeNowFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts_Safe.prototype.changeNowFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts_Safe.prototype.offerCryptoOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts_Safe.prototype.resetInOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts_Safe.prototype.resetNowBuOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts_Safe.prototype.exchangesLoadedOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts_Safe.prototype.exchangesTotalOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts_Safe.prototype.progressCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts_Safe.prototype.cryptoSelectOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts_Safe.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts_Safe.prototype.offersAggregatorOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts_Safe.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts_Safe.prototype.changellyFloatLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts_Safe.prototype.changellyFixedLinkOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts_Safe.prototype.letsExchangeFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts_Safe.prototype.letsExchangeFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts_Safe.prototype.letsExchangeFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts_Safe.prototype.letsExchangeFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts_Safe.prototype.changeNowFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts_Safe.prototype.changeNowFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts_Safe.prototype.changeNowFixedOfferOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts_Safe.prototype.changeNowFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts_Safe.prototype.offerCryptoOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts_Safe.prototype.resetInOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts_Safe.prototype.resetNowBuOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts_Safe.prototype.exchangesLoadedOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts_Safe.prototype.exchangesTotalOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts_Safe.prototype.progressCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts_Safe.prototype.cryptoSelectOutOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts_Safe.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts_Safe filter:!ControllerPlugin~props 433a42302241b3b8156c3835be1a8930 */
/** @record */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts_Safe.prototype.offersAggregatorOpts
/** @typedef {$xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts_Safe} */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElementPort.WeakInputs
/* @typal-end */