/** @const {?} */ $xyz.swapee.wc.back.IOffersTableController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.IOffersTableController.Initialese filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
$xyz.swapee.wc.back.IOffersTableController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOffersTableController.Initialese} */
xyz.swapee.wc.back.IOffersTableController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersTableController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.IOffersTableControllerCaster filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/** @interface */
$xyz.swapee.wc.back.IOffersTableControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersTableController} */
$xyz.swapee.wc.back.IOffersTableControllerCaster.prototype.asIOffersTableController
/** @type {!xyz.swapee.wc.back.BoundOffersTableController} */
$xyz.swapee.wc.back.IOffersTableControllerCaster.prototype.superOffersTableController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersTableControllerCaster}
 */
xyz.swapee.wc.back.IOffersTableControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.IOffersTableController filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.IOffersTableController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
$xyz.swapee.wc.back.IOffersTableController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersTableController}
 */
xyz.swapee.wc.back.IOffersTableController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.OffersTableController filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersTableController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableController.Initialese>}
 */
$xyz.swapee.wc.back.OffersTableController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.OffersTableController
/** @type {function(new: xyz.swapee.wc.back.IOffersTableController, ...!xyz.swapee.wc.back.IOffersTableController.Initialese)} */
xyz.swapee.wc.back.OffersTableController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.OffersTableController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.AbstractOffersTableController filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersTableController}
 */
$xyz.swapee.wc.back.AbstractOffersTableController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersTableController)} */
xyz.swapee.wc.back.AbstractOffersTableController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.OffersTableControllerConstructor filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableController, ...!xyz.swapee.wc.back.IOffersTableController.Initialese)} */
xyz.swapee.wc.back.OffersTableControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.RecordIOffersTableController filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersTableController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.BoundIOffersTableController filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersTableController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIOffersTableController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersTableController} */
xyz.swapee.wc.back.BoundIOffersTableController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.BoundOffersTableController filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersTableController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersTableController} */
xyz.swapee.wc.back.BoundOffersTableController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */