/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/200-OffersTableLand.xml} xyz.swapee.wc.OffersTableLand filter:!ControllerPlugin~props c809f8949abfadf614845ace79826c84 */
/** @record */
$xyz.swapee.wc.OffersTableLand = __$te_Mixin()
/** @type {!Object} */
$xyz.swapee.wc.OffersTableLand.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.OffersTableLand.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.OffersTableLand.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.OffersTableLand.prototype.CryptoSelectOut
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OffersTableLand}
 */
xyz.swapee.wc.OffersTableLand

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */