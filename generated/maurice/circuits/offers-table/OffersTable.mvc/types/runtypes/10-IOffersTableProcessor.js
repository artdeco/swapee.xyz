/** @const {?} */ $xyz.swapee.wc.IOffersTableProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.IOffersTableProcessor.Initialese filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
$xyz.swapee.wc.IOffersTableProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableProcessor.Initialese} */
xyz.swapee.wc.IOffersTableProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.IOffersTableProcessorCaster filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/** @interface */
$xyz.swapee.wc.IOffersTableProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableProcessor} */
$xyz.swapee.wc.IOffersTableProcessorCaster.prototype.asIOffersTableProcessor
/** @type {!xyz.swapee.wc.BoundOffersTableProcessor} */
$xyz.swapee.wc.IOffersTableProcessorCaster.prototype.superOffersTableProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableProcessorCaster}
 */
xyz.swapee.wc.IOffersTableProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.IOffersTableProcessor filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableProcessorCaster}
 * @extends {xyz.swapee.wc.IOffersTableComputer}
 * @extends {xyz.swapee.wc.IOffersTableCore}
 * @extends {xyz.swapee.wc.IOffersTableController}
 */
$xyz.swapee.wc.IOffersTableProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableProcessor}
 */
xyz.swapee.wc.IOffersTableProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.OffersTableProcessor filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableProcessor.Initialese>}
 */
$xyz.swapee.wc.OffersTableProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.OffersTableProcessor
/** @type {function(new: xyz.swapee.wc.IOffersTableProcessor, ...!xyz.swapee.wc.IOffersTableProcessor.Initialese)} */
xyz.swapee.wc.OffersTableProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.OffersTableProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.AbstractOffersTableProcessor filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableProcessor}
 */
$xyz.swapee.wc.AbstractOffersTableProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableProcessor)} */
xyz.swapee.wc.AbstractOffersTableProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.OffersTableProcessorConstructor filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableProcessor, ...!xyz.swapee.wc.IOffersTableProcessor.Initialese)} */
xyz.swapee.wc.OffersTableProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.RecordIOffersTableProcessor filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.BoundIOffersTableProcessor filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 * @extends {xyz.swapee.wc.BoundIOffersTableCore}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 */
$xyz.swapee.wc.BoundIOffersTableProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableProcessor} */
xyz.swapee.wc.BoundIOffersTableProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.BoundOffersTableProcessor filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableProcessor} */
xyz.swapee.wc.BoundOffersTableProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */