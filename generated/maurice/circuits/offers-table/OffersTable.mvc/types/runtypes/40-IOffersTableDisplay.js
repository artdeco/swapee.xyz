/** @const {?} */ $xyz.swapee.wc.IOffersTableDisplay
/** @const {?} */ $xyz.swapee.wc.IOffersTableDisplay.paintTableOrder
/** @const {?} */ xyz.swapee.wc.IOffersTableDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.Initialese filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings>}
 */
$xyz.swapee.wc.IOffersTableDisplay.Initialese = function() {}
/** @type {(!Array<!HTMLElement>)|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.CoinImWrs
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLAnchorElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFloatLink
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOfferAmount
/** @type {HTMLAnchorElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFixedLink
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOfferAmount
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOfferAmount
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOfferAmount
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOfferAmount
/** @type {(!Array<!HTMLSpanElement>)|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.OfferCryptoOuts
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ResetIn
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ResetNowBu
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ExchangesLoaded
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ExchangesTotal
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ProgressCollapsar
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.CryptoSelectOut
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ExchangeIntent
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.OffersAggregator
/** @typedef {$xyz.swapee.wc.IOffersTableDisplay.Initialese} */
xyz.swapee.wc.IOffersTableDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplayFields filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @interface */
$xyz.swapee.wc.IOffersTableDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTableDisplay.Settings} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IOffersTableDisplay.Queries} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.queries
/** @type {!Array<!HTMLElement>} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.CoinImWrs
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLAnchorElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatLink
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedOfferAmount
/** @type {HTMLAnchorElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedLink
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOfferAmount
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFixedOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFixedOfferAmount
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFloatingOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFloatingOfferAmount
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFixedOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFixedOfferAmount
/** @type {!Array<!HTMLSpanElement>} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.OfferCryptoOuts
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ResetIn
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ResetNowBu
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangesLoaded
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangesTotal
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ProgressCollapsar
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.CryptoSelectOut
/** @type {HTMLElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangeIntent
/** @type {HTMLElement} */
$xyz.swapee.wc.IOffersTableDisplayFields.prototype.OffersAggregator
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableDisplayFields}
 */
xyz.swapee.wc.IOffersTableDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplayCaster filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @interface */
$xyz.swapee.wc.IOffersTableDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableDisplay} */
$xyz.swapee.wc.IOffersTableDisplayCaster.prototype.asIOffersTableDisplay
/** @type {!xyz.swapee.wc.BoundIOffersTableScreen} */
$xyz.swapee.wc.IOffersTableDisplayCaster.prototype.asIOffersTableScreen
/** @type {!xyz.swapee.wc.BoundOffersTableDisplay} */
$xyz.swapee.wc.IOffersTableDisplayCaster.prototype.superOffersTableDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableDisplayCaster}
 */
xyz.swapee.wc.IOffersTableDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.OffersTableMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, xyz.swapee.wc.IOffersTableDisplay.Queries, null>}
 */
$xyz.swapee.wc.IOffersTableDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableDisplay.prototype.paint = function(memory, land) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} memory
 * @param {{ ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }} land
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableDisplay.prototype.paintTableOrder = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableDisplay}
 */
xyz.swapee.wc.IOffersTableDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.OffersTableDisplay filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableDisplay.Initialese>}
 */
$xyz.swapee.wc.OffersTableDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.OffersTableDisplay
/** @type {function(new: xyz.swapee.wc.IOffersTableDisplay, ...!xyz.swapee.wc.IOffersTableDisplay.Initialese)} */
xyz.swapee.wc.OffersTableDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.OffersTableDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.AbstractOffersTableDisplay filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableDisplay}
 */
$xyz.swapee.wc.AbstractOffersTableDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableDisplay)} */
xyz.swapee.wc.AbstractOffersTableDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.OffersTableDisplayConstructor filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableDisplay, ...!xyz.swapee.wc.IOffersTableDisplay.Initialese)} */
xyz.swapee.wc.OffersTableDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.RecordIOffersTableDisplay filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @typedef {{ paint: xyz.swapee.wc.IOffersTableDisplay.paint, paintTableOrder: xyz.swapee.wc.IOffersTableDisplay.paintTableOrder }} */
xyz.swapee.wc.RecordIOffersTableDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.BoundIOffersTableDisplay filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableDisplayFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.OffersTableMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, xyz.swapee.wc.IOffersTableDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIOffersTableDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableDisplay} */
xyz.swapee.wc.BoundIOffersTableDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.BoundOffersTableDisplay filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableDisplay} */
xyz.swapee.wc.BoundOffersTableDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.paint filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersTableMemory, null): void} */
xyz.swapee.wc.IOffersTableDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IOffersTableDisplay, !xyz.swapee.wc.OffersTableMemory, null): void} */
xyz.swapee.wc.IOffersTableDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IOffersTableDisplay.__paint} */
xyz.swapee.wc.IOffersTableDisplay.__paint

// nss:xyz.swapee.wc.IOffersTableDisplay,$xyz.swapee.wc.IOffersTableDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.paintTableOrder filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @param {!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} memory
 * @param {{ ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }} land
 */
$xyz.swapee.wc.IOffersTableDisplay.paintTableOrder = function(memory, land) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} memory
 * @param {{ ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }} land
 * @this {xyz.swapee.wc.IOffersTableDisplay}
 */
$xyz.swapee.wc.IOffersTableDisplay._paintTableOrder = function(memory, land) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} memory
 * @param {{ ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }} land
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder = function(memory, land) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableDisplay.paintTableOrder} */
xyz.swapee.wc.IOffersTableDisplay.paintTableOrder
/** @typedef {typeof $xyz.swapee.wc.IOffersTableDisplay._paintTableOrder} */
xyz.swapee.wc.IOffersTableDisplay._paintTableOrder
/** @typedef {typeof $xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder} */
xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder

// nss:xyz.swapee.wc.IOffersTableDisplay,$xyz.swapee.wc.IOffersTableDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.Queries filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @record */
$xyz.swapee.wc.IOffersTableDisplay.Queries = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.resetInSel
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.resetNowBuSel
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.exchangesLoadedSel
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.exchangesTotalSel
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.progressCollapsarSel
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.cryptoSelectOutSel
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.exchangeIntentSel
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.offersAggregatorSel
/** @typedef {$xyz.swapee.wc.IOffersTableDisplay.Queries} */
xyz.swapee.wc.IOffersTableDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.Settings filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableDisplay.Queries}
 */
$xyz.swapee.wc.IOffersTableDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableDisplay.Settings} */
xyz.swapee.wc.IOffersTableDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @record */
$xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} */
xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableDisplay.paintTableOrder
/* @typal-end */