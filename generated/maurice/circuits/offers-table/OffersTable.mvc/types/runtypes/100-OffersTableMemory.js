/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/100-OffersTableMemory.xml} xyz.swapee.wc.OffersTableMemory filter:!ControllerPlugin~props 304a20efb04f2f67bbf1424df82663ed */
/** @record */
$xyz.swapee.wc.OffersTableMemory = __$te_Mixin()
/** @type {number} */
$xyz.swapee.wc.OffersTableMemory.prototype.untilReset
/** @type {string} */
$xyz.swapee.wc.OffersTableMemory.prototype.changellyFixedLink
/** @type {string} */
$xyz.swapee.wc.OffersTableMemory.prototype.changellyFloatLink
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OffersTableMemory}
 */
xyz.swapee.wc.OffersTableMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */