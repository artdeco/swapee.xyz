/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersTableMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersTableMemoryPQs.prototype.untilReset
/** @type {string} */
$xyz.swapee.wc.OffersTableMemoryPQs.prototype.coinIm
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersTableMemoryPQs}
 */
xyz.swapee.wc.OffersTableMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersTableMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersTableMemoryQPs.prototype.fbbee
/** @type {string} */
$xyz.swapee.wc.OffersTableMemoryQPs.prototype.b1a5b
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableMemoryQPs}
 */
xyz.swapee.wc.OffersTableMemoryQPs
/** @type {function(new: xyz.swapee.wc.OffersTableMemoryQPs)} */
xyz.swapee.wc.OffersTableMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.OffersTableMemoryPQs}
 */
$xyz.swapee.wc.OffersTableInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersTableInputsPQs}
 */
xyz.swapee.wc.OffersTableInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.OffersTableInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableInputsQPs}
 */
xyz.swapee.wc.OffersTableInputsQPs
/** @type {function(new: xyz.swapee.wc.OffersTableInputsQPs)} */
xyz.swapee.wc.OffersTableInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableCachePQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersTableCachePQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersTableCachePQs.prototype.changellyFixedLink
/** @type {string} */
$xyz.swapee.wc.OffersTableCachePQs.prototype.changellyFloatLink
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersTableCachePQs}
 */
xyz.swapee.wc.OffersTableCachePQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableCacheQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersTableCacheQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersTableCacheQPs.prototype.ca106
/** @type {string} */
$xyz.swapee.wc.OffersTableCacheQPs.prototype.h1175
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableCacheQPs}
 */
xyz.swapee.wc.OffersTableCacheQPs
/** @type {function(new: xyz.swapee.wc.OffersTableCacheQPs)} */
xyz.swapee.wc.OffersTableCacheQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableQueriesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersTableQueriesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.cryptoSelectedImWrSel
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.resetInSel
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.resetNowBuSel
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.exchangesLoadedSel
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.exchangesTotalSel
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.progressCollapsarSel
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.exchangeIntentSel
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesPQs.prototype.offersAggregatorSel
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersTableQueriesPQs}
 */
xyz.swapee.wc.OffersTableQueriesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableQueriesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersTableQueriesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.dc6e2
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.ac854
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.be273
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.d5cac
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.f6836
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.b9642
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.b3da4
/** @type {string} */
$xyz.swapee.wc.OffersTableQueriesQPs.prototype.cde34
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableQueriesQPs}
 */
xyz.swapee.wc.OffersTableQueriesQPs
/** @type {function(new: xyz.swapee.wc.OffersTableQueriesQPs)} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersTableVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ResetIn
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ResetNowBu
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.CoinIms
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.CryptoSelectedImWr
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFloatingOfferAmount
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFixedOfferAmount
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFloatingOfferAmount
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFixedOfferAmount
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFloatingOfferAmount
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFixedOfferAmount
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.OfferCryptoOuts
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ExchangesLoaded
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ExchangesTotal
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ProgressCollapsar
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ExchangeIntent
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.OffersAggregator
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFloatingDealBroker
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusPQs.prototype.CryptoSelectOut
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersTableVdusPQs}
 */
xyz.swapee.wc.OffersTableVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersTableVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8741
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8742
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8743
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8744
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8745
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8746
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8747
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8748
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f8749
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87410
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87411
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87412
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87413
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87414
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87415
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87416
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87417
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87418
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87419
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87420
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87421
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87422
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87423
/** @type {string} */
$xyz.swapee.wc.OffersTableVdusQPs.prototype.f87424
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableVdusQPs}
 */
xyz.swapee.wc.OffersTableVdusQPs
/** @type {function(new: xyz.swapee.wc.OffersTableVdusQPs)} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableClassesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersTableClassesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesPQs.prototype.ColHeading
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesPQs.prototype.Loading
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesPQs.prototype.Hidden
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesPQs.prototype.SelectedRateType
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesPQs.prototype.BestOffer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersTableClassesPQs}
 */
xyz.swapee.wc.OffersTableClassesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableClassesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersTableClassesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesQPs.prototype.b6cc2
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesQPs.prototype.b6bfb
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesQPs.prototype.hacdf
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesQPs.prototype.aaa87
/** @type {string} */
$xyz.swapee.wc.OffersTableClassesQPs.prototype.e8977
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableClassesQPs}
 */
xyz.swapee.wc.OffersTableClassesQPs
/** @type {function(new: xyz.swapee.wc.OffersTableClassesQPs)} */
xyz.swapee.wc.OffersTableClassesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */