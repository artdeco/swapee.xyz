/** @const {?} */ $xyz.swapee.wc.IOffersTableOuterCore
/** @const {?} */ $xyz.swapee.wc.IOffersTableOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IOffersTableOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IOffersTablePort
/** @const {?} */ $xyz.swapee.wc.IOffersTablePort.Inputs
/** @const {?} */ $xyz.swapee.wc.IOffersTablePort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IOffersTableCore
/** @const {?} */ $xyz.swapee.wc.IOffersTableCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Initialese filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
$xyz.swapee.wc.IOffersTableOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableOuterCore.Initialese} */
xyz.swapee.wc.IOffersTableOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCoreFields filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @interface */
$xyz.swapee.wc.IOffersTableOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTableOuterCore.Model} */
$xyz.swapee.wc.IOffersTableOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableOuterCoreFields}
 */
xyz.swapee.wc.IOffersTableOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCoreCaster filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @interface */
$xyz.swapee.wc.IOffersTableOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableOuterCore} */
$xyz.swapee.wc.IOffersTableOuterCoreCaster.prototype.asIOffersTableOuterCore
/** @type {!xyz.swapee.wc.BoundOffersTableOuterCore} */
$xyz.swapee.wc.IOffersTableOuterCoreCaster.prototype.superOffersTableOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableOuterCoreCaster}
 */
xyz.swapee.wc.IOffersTableOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreCaster}
 */
$xyz.swapee.wc.IOffersTableOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableOuterCore}
 */
xyz.swapee.wc.IOffersTableOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.OffersTableOuterCore filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableOuterCore.Initialese>}
 */
$xyz.swapee.wc.OffersTableOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.OffersTableOuterCore
/** @type {function(new: xyz.swapee.wc.IOffersTableOuterCore)} */
xyz.swapee.wc.OffersTableOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.OffersTableOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.AbstractOffersTableOuterCore filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableOuterCore}
 */
$xyz.swapee.wc.AbstractOffersTableOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableOuterCore)} */
xyz.swapee.wc.AbstractOffersTableOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.RecordIOffersTableOuterCore filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.BoundIOffersTableOuterCore filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIOffersTableOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableOuterCore} */
xyz.swapee.wc.BoundIOffersTableOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.BoundOffersTableOuterCore filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableOuterCore} */
xyz.swapee.wc.BoundOffersTableOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset.untilReset filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @typedef {number} */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset.untilReset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
$xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset.prototype.untilReset
/** @typedef {$xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset} */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset}
 */
$xyz.swapee.wc.IOffersTableOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableOuterCore.Model} */
xyz.swapee.wc.IOffersTableOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
$xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset.prototype.untilReset
/** @typedef {$xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset} */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.WeakModel filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset}
 */
$xyz.swapee.wc.IOffersTableOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableOuterCore.WeakModel} */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
$xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe.prototype.untilReset
/** @typedef {$xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe} */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
$xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe.prototype.untilReset
/** @typedef {$xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe} */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset}
 */
$xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset} */
xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset_Safe filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe}
 */
$xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset_Safe} */
xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset}
 */
$xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset} */
xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset_Safe filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe}
 */
$xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset_Safe} */
xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableCore.Model.UntilReset filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset}
 */
$xyz.swapee.wc.IOffersTableCore.Model.UntilReset = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Model.UntilReset} */
xyz.swapee.wc.IOffersTableCore.Model.UntilReset

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableCore.Model.UntilReset_Safe filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe}
 */
$xyz.swapee.wc.IOffersTableCore.Model.UntilReset_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Model.UntilReset_Safe} */
xyz.swapee.wc.IOffersTableCore.Model.UntilReset_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore.Model
/* @typal-end */