/** @const {?} */ $xyz.swapee.wc.front.IOffersTableController
/** @const {?} */ xyz.swapee.wc.front.IOffersTableController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.Initialese filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @record */
$xyz.swapee.wc.front.IOffersTableController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOffersTableController.Initialese} */
xyz.swapee.wc.front.IOffersTableController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOffersTableController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableControllerCaster filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @interface */
$xyz.swapee.wc.front.IOffersTableControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOffersTableController} */
$xyz.swapee.wc.front.IOffersTableControllerCaster.prototype.asIOffersTableController
/** @type {!xyz.swapee.wc.front.BoundOffersTableController} */
$xyz.swapee.wc.front.IOffersTableControllerCaster.prototype.superOffersTableController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersTableControllerCaster}
 */
xyz.swapee.wc.front.IOffersTableControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerAT}
 */
$xyz.swapee.wc.front.IOffersTableController = function() {}
/** @return {?} */
$xyz.swapee.wc.front.IOffersTableController.prototype.resetTick = function() {}
/** @return {?} */
$xyz.swapee.wc.front.IOffersTableController.prototype.reset = function() {}
/** @return {?} */
$xyz.swapee.wc.front.IOffersTableController.prototype.onReset = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersTableController}
 */
xyz.swapee.wc.front.IOffersTableController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableController filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersTableController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableController.Initialese>}
 */
$xyz.swapee.wc.front.OffersTableController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.OffersTableController
/** @type {function(new: xyz.swapee.wc.front.IOffersTableController, ...!xyz.swapee.wc.front.IOffersTableController.Initialese)} */
xyz.swapee.wc.front.OffersTableController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.OffersTableController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.AbstractOffersTableController filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersTableController}
 */
$xyz.swapee.wc.front.AbstractOffersTableController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController
/** @type {function(new: xyz.swapee.wc.front.AbstractOffersTableController)} */
xyz.swapee.wc.front.AbstractOffersTableController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableControllerConstructor filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableController, ...!xyz.swapee.wc.front.IOffersTableController.Initialese)} */
xyz.swapee.wc.front.OffersTableControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableControllerHyperslice filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @interface */
$xyz.swapee.wc.front.IOffersTableControllerHyperslice = function() {}
/** @type {(!xyz.swapee.wc.front.IOffersTableController._reset|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersTableController._reset>)} */
$xyz.swapee.wc.front.IOffersTableControllerHyperslice.prototype.reset
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersTableControllerHyperslice}
 */
xyz.swapee.wc.front.IOffersTableControllerHyperslice

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableControllerHyperslice filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.front.IOffersTableControllerHyperslice}
 */
$xyz.swapee.wc.front.OffersTableControllerHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.front.OffersTableControllerHyperslice}
 */
xyz.swapee.wc.front.OffersTableControllerHyperslice
/** @type {function(new: xyz.swapee.wc.front.IOffersTableControllerHyperslice)} */
xyz.swapee.wc.front.OffersTableControllerHyperslice.prototype.constructor

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.front.IOffersTableController.__reset<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersTableController.__reset<THIS>>)} */
$xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice.prototype.reset
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableControllerBindingHyperslice filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.front.OffersTableControllerBindingHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @template THIS
 * @extends {$xyz.swapee.wc.front.OffersTableControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.front.OffersTableControllerBindingHyperslice
/** @type {function(new: xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice<THIS>)} */
xyz.swapee.wc.front.OffersTableControllerBindingHyperslice.prototype.constructor

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.RecordIOffersTableController filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @typedef {{ resetTick: xyz.swapee.wc.front.IOffersTableController.resetTick, reset: xyz.swapee.wc.front.IOffersTableController.reset, onReset: xyz.swapee.wc.front.IOffersTableController.onReset }} */
xyz.swapee.wc.front.RecordIOffersTableController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.BoundIOffersTableController filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersTableController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIOffersTableControllerAT}
 */
$xyz.swapee.wc.front.BoundIOffersTableController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOffersTableController} */
xyz.swapee.wc.front.BoundIOffersTableController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.BoundOffersTableController filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersTableController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOffersTableController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOffersTableController} */
xyz.swapee.wc.front.BoundOffersTableController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.resetTick filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.IOffersTableController.__resetTick = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.IOffersTableController.resetTick
/** @typedef {function(this: xyz.swapee.wc.front.IOffersTableController)} */
xyz.swapee.wc.front.IOffersTableController._resetTick
/** @typedef {typeof $xyz.swapee.wc.front.IOffersTableController.__resetTick} */
xyz.swapee.wc.front.IOffersTableController.__resetTick

// nss:xyz.swapee.wc.front.IOffersTableController,$xyz.swapee.wc.front.IOffersTableController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.reset filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.IOffersTableController.__reset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.IOffersTableController.reset
/** @typedef {function(this: xyz.swapee.wc.front.IOffersTableController)} */
xyz.swapee.wc.front.IOffersTableController._reset
/** @typedef {typeof $xyz.swapee.wc.front.IOffersTableController.__reset} */
xyz.swapee.wc.front.IOffersTableController.__reset

// nss:xyz.swapee.wc.front.IOffersTableController,$xyz.swapee.wc.front.IOffersTableController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.onReset filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.IOffersTableController.__onReset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.IOffersTableController.onReset
/** @typedef {function(this: xyz.swapee.wc.front.IOffersTableController)} */
xyz.swapee.wc.front.IOffersTableController._onReset
/** @typedef {typeof $xyz.swapee.wc.front.IOffersTableController.__onReset} */
xyz.swapee.wc.front.IOffersTableController.__onReset

// nss:xyz.swapee.wc.front.IOffersTableController,$xyz.swapee.wc.front.IOffersTableController,xyz.swapee.wc.front
/* @typal-end */