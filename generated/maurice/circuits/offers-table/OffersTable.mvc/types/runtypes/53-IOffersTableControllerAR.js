/** @const {?} */ $xyz.swapee.wc.back.IOffersTableControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.IOffersTableControllerAR.Initialese filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
$xyz.swapee.wc.back.IOffersTableControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} */
xyz.swapee.wc.back.IOffersTableControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersTableControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.IOffersTableControllerARCaster filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/** @interface */
$xyz.swapee.wc.back.IOffersTableControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersTableControllerAR} */
$xyz.swapee.wc.back.IOffersTableControllerARCaster.prototype.asIOffersTableControllerAR
/** @type {!xyz.swapee.wc.back.BoundOffersTableControllerAR} */
$xyz.swapee.wc.back.IOffersTableControllerARCaster.prototype.superOffersTableControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersTableControllerARCaster}
 */
xyz.swapee.wc.back.IOffersTableControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.IOffersTableControllerAR filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOffersTableController}
 */
$xyz.swapee.wc.back.IOffersTableControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersTableControllerAR}
 */
xyz.swapee.wc.back.IOffersTableControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.OffersTableControllerAR filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersTableControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.OffersTableControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.OffersTableControllerAR
/** @type {function(new: xyz.swapee.wc.back.IOffersTableControllerAR, ...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese)} */
xyz.swapee.wc.back.OffersTableControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.OffersTableControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.AbstractOffersTableControllerAR filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersTableControllerAR}
 */
$xyz.swapee.wc.back.AbstractOffersTableControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersTableControllerAR)} */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.OffersTableControllerARConstructor filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableControllerAR, ...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese)} */
xyz.swapee.wc.back.OffersTableControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.RecordIOffersTableControllerAR filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersTableControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.BoundIOffersTableControllerAR filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersTableControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 */
$xyz.swapee.wc.back.BoundIOffersTableControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersTableControllerAR} */
xyz.swapee.wc.back.BoundIOffersTableControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.BoundOffersTableControllerAR filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersTableControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersTableControllerAR} */
xyz.swapee.wc.back.BoundOffersTableControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */