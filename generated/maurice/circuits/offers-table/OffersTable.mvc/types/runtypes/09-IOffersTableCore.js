/** @const {?} */ $xyz.swapee.wc.IOffersTableCore
/** @const {?} */ $xyz.swapee.wc.IOffersTableCore.Model
/** @const {?} */ xyz.swapee.wc.IOffersTableCore
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Initialese filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
$xyz.swapee.wc.IOffersTableCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Initialese} */
xyz.swapee.wc.IOffersTableCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCoreFields filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @interface */
$xyz.swapee.wc.IOffersTableCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTableCore.Model} */
$xyz.swapee.wc.IOffersTableCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableCoreFields}
 */
xyz.swapee.wc.IOffersTableCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCoreCaster filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @interface */
$xyz.swapee.wc.IOffersTableCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableCore} */
$xyz.swapee.wc.IOffersTableCoreCaster.prototype.asIOffersTableCore
/** @type {!xyz.swapee.wc.BoundOffersTableCore} */
$xyz.swapee.wc.IOffersTableCoreCaster.prototype.superOffersTableCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableCoreCaster}
 */
xyz.swapee.wc.IOffersTableCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCoreCaster}
 * @extends {xyz.swapee.wc.IOffersTableOuterCore}
 */
$xyz.swapee.wc.IOffersTableCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersTableCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersTableCore.prototype.resetOffersTableCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableCore}
 */
xyz.swapee.wc.IOffersTableCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.OffersTableCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableCore.Initialese>}
 */
$xyz.swapee.wc.OffersTableCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.OffersTableCore
/** @type {function(new: xyz.swapee.wc.IOffersTableCore)} */
xyz.swapee.wc.OffersTableCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.OffersTableCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.AbstractOffersTableCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableCore}
 */
$xyz.swapee.wc.AbstractOffersTableCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableCore)} */
xyz.swapee.wc.AbstractOffersTableCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.RecordIOffersTableCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @typedef {{ resetCore: xyz.swapee.wc.IOffersTableCore.resetCore, resetOffersTableCore: xyz.swapee.wc.IOffersTableCore.resetOffersTableCore }} */
xyz.swapee.wc.RecordIOffersTableCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.BoundIOffersTableCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCoreCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableOuterCore}
 */
$xyz.swapee.wc.BoundIOffersTableCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableCore} */
xyz.swapee.wc.BoundIOffersTableCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.BoundOffersTableCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableCore} */
xyz.swapee.wc.BoundOffersTableCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.resetCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTableCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IOffersTableCore): void} */
xyz.swapee.wc.IOffersTableCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IOffersTableCore.__resetCore} */
xyz.swapee.wc.IOffersTableCore.__resetCore

// nss:xyz.swapee.wc.IOffersTableCore,$xyz.swapee.wc.IOffersTableCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.resetOffersTableCore filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTableCore.resetOffersTableCore
/** @typedef {function(this: xyz.swapee.wc.IOffersTableCore): void} */
xyz.swapee.wc.IOffersTableCore._resetOffersTableCore
/** @typedef {typeof $xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore} */
xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore

// nss:xyz.swapee.wc.IOffersTableCore,$xyz.swapee.wc.IOffersTableCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink.changellyFixedLink filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @typedef {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink.changellyFixedLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink.changellyFloatLink filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @typedef {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink.changellyFloatLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink.prototype.changellyFixedLink
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink.prototype.changellyFloatLink
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model}
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink}
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink}
 */
$xyz.swapee.wc.IOffersTableCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Model} */
xyz.swapee.wc.IOffersTableCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe.prototype.changellyFixedLink
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe.prototype.changellyFloatLink
/** @typedef {$xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableCore.Model
/* @typal-end */