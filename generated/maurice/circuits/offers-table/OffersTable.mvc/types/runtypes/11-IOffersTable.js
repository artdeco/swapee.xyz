/** @const {?} */ $xyz.swapee.wc.IOffersTable
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTableEnv filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @record */
$xyz.swapee.wc.OffersTableEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IOffersTable} */
$xyz.swapee.wc.OffersTableEnv.prototype.offersTable
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OffersTableEnv}
 */
xyz.swapee.wc.OffersTableEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable.Initialese filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersTableProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
$xyz.swapee.wc.IOffersTable.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTable.Initialese} */
xyz.swapee.wc.IOffersTable.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTable
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTableFields filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @interface */
$xyz.swapee.wc.IOffersTableFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTable.Pinout} */
$xyz.swapee.wc.IOffersTableFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableFields}
 */
xyz.swapee.wc.IOffersTableFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTableCaster filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @interface */
$xyz.swapee.wc.IOffersTableCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTable} */
$xyz.swapee.wc.IOffersTableCaster.prototype.asIOffersTable
/** @type {!xyz.swapee.wc.BoundOffersTable} */
$xyz.swapee.wc.IOffersTableCaster.prototype.superOffersTable
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableCaster}
 */
xyz.swapee.wc.IOffersTableCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCaster}
 * @extends {xyz.swapee.wc.IOffersTableProcessor}
 * @extends {xyz.swapee.wc.IOffersTableComputer}
 * @extends {xyz.swapee.wc.IOffersTableController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableLand>}
 */
$xyz.swapee.wc.IOffersTable = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTable}
 */
xyz.swapee.wc.IOffersTable

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTable filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTable}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTable.Initialese>}
 */
$xyz.swapee.wc.OffersTable = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.OffersTable
/** @type {function(new: xyz.swapee.wc.IOffersTable, ...!xyz.swapee.wc.IOffersTable.Initialese)} */
xyz.swapee.wc.OffersTable.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.OffersTable.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.AbstractOffersTable filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init
 * @extends {xyz.swapee.wc.OffersTable}
 */
$xyz.swapee.wc.AbstractOffersTable = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTable}
 */
xyz.swapee.wc.AbstractOffersTable
/** @type {function(new: xyz.swapee.wc.AbstractOffersTable)} */
xyz.swapee.wc.AbstractOffersTable.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTable}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTable.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTableConstructor filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTable, ...!xyz.swapee.wc.IOffersTable.Initialese)} */
xyz.swapee.wc.OffersTableConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable.MVCOptions filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @record */
$xyz.swapee.wc.IOffersTable.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IOffersTable.Pinout)|undefined} */
$xyz.swapee.wc.IOffersTable.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IOffersTable.Pinout)|undefined} */
$xyz.swapee.wc.IOffersTable.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IOffersTable.Pinout} */
$xyz.swapee.wc.IOffersTable.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.OffersTableMemory)|undefined} */
$xyz.swapee.wc.IOffersTable.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.OffersTableClasses)|undefined} */
$xyz.swapee.wc.IOffersTable.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IOffersTable.MVCOptions} */
xyz.swapee.wc.IOffersTable.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTable
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.RecordIOffersTable filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.BoundIOffersTable filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableFields}
 * @extends {xyz.swapee.wc.RecordIOffersTable}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableLand>}
 */
$xyz.swapee.wc.BoundIOffersTable = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTable} */
xyz.swapee.wc.BoundIOffersTable

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.BoundOffersTable filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTable}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTable = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTable} */
xyz.swapee.wc.BoundOffersTable

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable.Pinout filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableController.Inputs}
 */
$xyz.swapee.wc.IOffersTable.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTable.Pinout} */
xyz.swapee.wc.IOffersTable.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTable
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTableBuffer filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
$xyz.swapee.wc.IOffersTableBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableBuffer}
 */
xyz.swapee.wc.IOffersTableBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTableBuffer filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableBuffer}
 */
$xyz.swapee.wc.OffersTableBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableBuffer}
 */
xyz.swapee.wc.OffersTableBuffer
/** @type {function(new: xyz.swapee.wc.IOffersTableBuffer)} */
xyz.swapee.wc.OffersTableBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */