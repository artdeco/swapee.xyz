/** @const {?} */ $xyz.swapee.wc.IOffersTableDesigner
/** @const {?} */ $xyz.swapee.wc.IOffersTableDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IOffersTableDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @interface */
$xyz.swapee.wc.IOffersTableDesigner = function() {}
/**
 * @param {xyz.swapee.wc.OffersTableClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersTableDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.OffersTableClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersTableDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IOffersTableDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.OffersTableClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersTableDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableDesigner}
 */
xyz.swapee.wc.IOffersTableDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.OffersTableDesigner filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableDesigner}
 */
$xyz.swapee.wc.OffersTableDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableDesigner}
 */
xyz.swapee.wc.OffersTableDesigner
/** @type {function(new: xyz.swapee.wc.IOffersTableDesigner)} */
xyz.swapee.wc.OffersTableDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @record */
$xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.CryptoSelectOut
/** @type {typeof xyz.swapee.wc.IOffersTableController} */
$xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.OffersTable
/** @typedef {$xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh} */
xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner.relay.Mesh filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @record */
$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.CryptoSelectOut
/** @type {typeof xyz.swapee.wc.IOffersTableController} */
$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.OffersTable
/** @type {typeof xyz.swapee.wc.IOffersTableController} */
$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IOffersTableDesigner.relay.Mesh} */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner.relay.MemPool filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @record */
$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.CryptoSelectOut
/** @type {!xyz.swapee.wc.OffersTableMemory} */
$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.OffersTable
/** @type {!xyz.swapee.wc.OffersTableMemory} */
$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IOffersTableDesigner.relay.MemPool} */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableDesigner.relay
/* @typal-end */