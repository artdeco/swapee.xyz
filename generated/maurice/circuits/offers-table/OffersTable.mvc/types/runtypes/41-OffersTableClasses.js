/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/41-OffersTableClasses.xml} xyz.swapee.wc.OffersTableClasses filter:!ControllerPlugin~props dd863b71dc37de8f0b54845d922301e8 */
/** @record */
$xyz.swapee.wc.OffersTableClasses = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.OffersTableClasses.prototype.ColHeading
/** @type {string|undefined} */
$xyz.swapee.wc.OffersTableClasses.prototype.Loading
/** @type {string|undefined} */
$xyz.swapee.wc.OffersTableClasses.prototype.Hidden
/** @type {string|undefined} */
$xyz.swapee.wc.OffersTableClasses.prototype.SelectedRateType
/** @type {string|undefined} */
$xyz.swapee.wc.OffersTableClasses.prototype.BestOffer
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OffersTableClasses}
 */
xyz.swapee.wc.OffersTableClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */