/** @const {?} */ $xyz.swapee.wc.front.IOffersTableControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.IOffersTableControllerAT.Initialese filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IOffersTableControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} */
xyz.swapee.wc.front.IOffersTableControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOffersTableControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.IOffersTableControllerATCaster filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/** @interface */
$xyz.swapee.wc.front.IOffersTableControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOffersTableControllerAT} */
$xyz.swapee.wc.front.IOffersTableControllerATCaster.prototype.asIOffersTableControllerAT
/** @type {!xyz.swapee.wc.front.BoundOffersTableControllerAT} */
$xyz.swapee.wc.front.IOffersTableControllerATCaster.prototype.superOffersTableControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersTableControllerATCaster}
 */
xyz.swapee.wc.front.IOffersTableControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.IOffersTableControllerAT filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IOffersTableControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersTableControllerAT}
 */
xyz.swapee.wc.front.IOffersTableControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.OffersTableControllerAT filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersTableControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.OffersTableControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.OffersTableControllerAT
/** @type {function(new: xyz.swapee.wc.front.IOffersTableControllerAT, ...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese)} */
xyz.swapee.wc.front.OffersTableControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.OffersTableControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.AbstractOffersTableControllerAT filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersTableControllerAT}
 */
$xyz.swapee.wc.front.AbstractOffersTableControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractOffersTableControllerAT)} */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.OffersTableControllerATConstructor filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableControllerAT, ...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese)} */
xyz.swapee.wc.front.OffersTableControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.RecordIOffersTableControllerAT filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOffersTableControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.BoundIOffersTableControllerAT filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersTableControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIOffersTableControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOffersTableControllerAT} */
xyz.swapee.wc.front.BoundIOffersTableControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.BoundOffersTableControllerAT filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersTableControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOffersTableControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOffersTableControllerAT} */
xyz.swapee.wc.front.BoundOffersTableControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */