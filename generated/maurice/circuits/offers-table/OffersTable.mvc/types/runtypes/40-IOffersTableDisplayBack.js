/** @const {?} */ $xyz.swapee.wc.back.IOffersTableDisplay
/** @const {?} */ xyz.swapee.wc.back.IOffersTableDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplay.Initialese filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OffersTableClasses>}
 */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese = function() {}
/** @type {(!Array<!com.webcircuits.IHtmlTwin>)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.CoinImWrs
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFloatLink
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFixedLink
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOfferAmount
/** @type {(!Array<!com.webcircuits.IHtmlTwin>)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.OfferCryptoOuts
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ResetIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ResetNowBu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ExchangesLoaded
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ExchangesTotal
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ProgressCollapsar
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.CryptoSelectOut
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ExchangeIntent
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.OffersAggregator
/** @typedef {$xyz.swapee.wc.back.IOffersTableDisplay.Initialese} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersTableDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplayFields filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/** @interface */
$xyz.swapee.wc.back.IOffersTableDisplayFields = function() {}
/** @type {!Array<!com.webcircuits.IHtmlTwin>} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.CoinImWrs
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatLink
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedLink
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFixedOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFixedOfferAmount
/** @type {!Array<!com.webcircuits.IHtmlTwin>} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.OfferCryptoOuts
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ResetIn
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ResetNowBu
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangesLoaded
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangesTotal
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ProgressCollapsar
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.CryptoSelectOut
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangeIntent
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.OffersAggregator
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersTableDisplayFields}
 */
xyz.swapee.wc.back.IOffersTableDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplayCaster filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/** @interface */
$xyz.swapee.wc.back.IOffersTableDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersTableDisplay} */
$xyz.swapee.wc.back.IOffersTableDisplayCaster.prototype.asIOffersTableDisplay
/** @type {!xyz.swapee.wc.back.BoundOffersTableDisplay} */
$xyz.swapee.wc.back.IOffersTableDisplayCaster.prototype.superOffersTableDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersTableDisplayCaster}
 */
xyz.swapee.wc.back.IOffersTableDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplay filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.OffersTableClasses, !xyz.swapee.wc.OffersTableLand>}
 */
$xyz.swapee.wc.back.IOffersTableDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} [memory]
 * @param {!xyz.swapee.wc.OffersTableLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOffersTableDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersTableDisplay}
 */
xyz.swapee.wc.back.IOffersTableDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.OffersTableDisplay filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IOffersTableDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableDisplay.Initialese>}
 */
$xyz.swapee.wc.back.OffersTableDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.OffersTableDisplay
/** @type {function(new: xyz.swapee.wc.back.IOffersTableDisplay)} */
xyz.swapee.wc.back.OffersTableDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.OffersTableDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.AbstractOffersTableDisplay filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.OffersTableDisplay}
 */
$xyz.swapee.wc.back.AbstractOffersTableDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersTableDisplay)} */
xyz.swapee.wc.back.AbstractOffersTableDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.RecordIOffersTableDisplay filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/** @typedef {{ paint: xyz.swapee.wc.back.IOffersTableDisplay.paint }} */
xyz.swapee.wc.back.RecordIOffersTableDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.BoundIOffersTableDisplay filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIOffersTableDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.OffersTableClasses, !xyz.swapee.wc.OffersTableLand>}
 */
$xyz.swapee.wc.back.BoundIOffersTableDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersTableDisplay} */
xyz.swapee.wc.back.BoundIOffersTableDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.BoundOffersTableDisplay filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersTableDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersTableDisplay} */
xyz.swapee.wc.back.BoundOffersTableDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplay.paint filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} [memory]
 * @param {!xyz.swapee.wc.OffersTableLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOffersTableDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersTableMemory=, !xyz.swapee.wc.OffersTableLand=): void} */
xyz.swapee.wc.back.IOffersTableDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IOffersTableDisplay, !xyz.swapee.wc.OffersTableMemory=, !xyz.swapee.wc.OffersTableLand=): void} */
xyz.swapee.wc.back.IOffersTableDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IOffersTableDisplay.__paint} */
xyz.swapee.wc.back.IOffersTableDisplay.__paint

// nss:xyz.swapee.wc.back.IOffersTableDisplay,$xyz.swapee.wc.back.IOffersTableDisplay,xyz.swapee.wc.back
/* @typal-end */