/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/102-OffersTableInputs.xml} xyz.swapee.wc.front.OffersTableInputs filter:!ControllerPlugin~props ac2360e06a97afadce45a4977b1b51b9 */
/** @record */
$xyz.swapee.wc.front.OffersTableInputs = __$te_Mixin()
/** @type {number|undefined} */
$xyz.swapee.wc.front.OffersTableInputs.prototype.untilReset
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.OffersTableInputs}
 */
xyz.swapee.wc.front.OffersTableInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */