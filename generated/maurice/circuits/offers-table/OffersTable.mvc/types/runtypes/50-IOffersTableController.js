/** @const {?} */ $xyz.swapee.wc.IOffersTableController
/** @const {?} */ xyz.swapee.wc.IOffersTableController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.Initialese filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IOffersTableController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableController.Initialese} */
xyz.swapee.wc.IOffersTableController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerFields filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @interface */
$xyz.swapee.wc.IOffersTableControllerFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTableController.Inputs} */
$xyz.swapee.wc.IOffersTableControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableControllerFields}
 */
xyz.swapee.wc.IOffersTableControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerCaster filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @interface */
$xyz.swapee.wc.IOffersTableControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableController} */
$xyz.swapee.wc.IOffersTableControllerCaster.prototype.asIOffersTableController
/** @type {!xyz.swapee.wc.BoundIOffersTableProcessor} */
$xyz.swapee.wc.IOffersTableControllerCaster.prototype.asIOffersTableProcessor
/** @type {!xyz.swapee.wc.BoundOffersTableController} */
$xyz.swapee.wc.IOffersTableControllerCaster.prototype.superOffersTableController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableControllerCaster}
 */
xyz.swapee.wc.IOffersTableControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
$xyz.swapee.wc.IOffersTableController = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersTableController.prototype.resetPort = function() {}
/** @return {?} */
$xyz.swapee.wc.IOffersTableController.prototype.resetTick = function() {}
/** @return {?} */
$xyz.swapee.wc.IOffersTableController.prototype.reset = function() {}
/** @return {?} */
$xyz.swapee.wc.IOffersTableController.prototype.onReset = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableController}
 */
xyz.swapee.wc.IOffersTableController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableController filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableController.Initialese>}
 */
$xyz.swapee.wc.OffersTableController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.OffersTableController
/** @type {function(new: xyz.swapee.wc.IOffersTableController, ...!xyz.swapee.wc.IOffersTableController.Initialese)} */
xyz.swapee.wc.OffersTableController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.OffersTableController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.AbstractOffersTableController filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableController}
 */
$xyz.swapee.wc.AbstractOffersTableController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableController)} */
xyz.swapee.wc.AbstractOffersTableController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableControllerConstructor filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableController, ...!xyz.swapee.wc.IOffersTableController.Initialese)} */
xyz.swapee.wc.OffersTableControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerHyperslice filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @interface */
$xyz.swapee.wc.IOffersTableControllerHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IOffersTableController._reset|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableController._reset>)} */
$xyz.swapee.wc.IOffersTableControllerHyperslice.prototype.reset
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableControllerHyperslice}
 */
xyz.swapee.wc.IOffersTableControllerHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableControllerHyperslice filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableControllerHyperslice}
 */
$xyz.swapee.wc.OffersTableControllerHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableControllerHyperslice}
 */
xyz.swapee.wc.OffersTableControllerHyperslice
/** @type {function(new: xyz.swapee.wc.IOffersTableControllerHyperslice)} */
xyz.swapee.wc.OffersTableControllerHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerBindingHyperslice filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.IOffersTableControllerBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IOffersTableController.__reset<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableController.__reset<THIS>>)} */
$xyz.swapee.wc.IOffersTableControllerBindingHyperslice.prototype.reset
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.IOffersTableControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.IOffersTableControllerBindingHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableControllerBindingHyperslice filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableControllerBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.OffersTableControllerBindingHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @template THIS
 * @extends {$xyz.swapee.wc.OffersTableControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.OffersTableControllerBindingHyperslice
/** @type {function(new: xyz.swapee.wc.IOffersTableControllerBindingHyperslice<THIS>)} */
xyz.swapee.wc.OffersTableControllerBindingHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.RecordIOffersTableController filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersTableController.resetPort, resetTick: xyz.swapee.wc.IOffersTableController.resetTick, reset: xyz.swapee.wc.IOffersTableController.reset, onReset: xyz.swapee.wc.IOffersTableController.onReset }} */
xyz.swapee.wc.RecordIOffersTableController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.BoundIOffersTableController filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableControllerFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
$xyz.swapee.wc.BoundIOffersTableController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableController} */
xyz.swapee.wc.BoundIOffersTableController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.BoundOffersTableController filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableController = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableController} */
xyz.swapee.wc.BoundOffersTableController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.resetPort filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTableController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController): void} */
xyz.swapee.wc.IOffersTableController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOffersTableController.__resetPort} */
xyz.swapee.wc.IOffersTableController.__resetPort

// nss:xyz.swapee.wc.IOffersTableController,$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.resetTick filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.IOffersTableController.__resetTick = function() {}
/** @typedef {function()} */
xyz.swapee.wc.IOffersTableController.resetTick
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController)} */
xyz.swapee.wc.IOffersTableController._resetTick
/** @typedef {typeof $xyz.swapee.wc.IOffersTableController.__resetTick} */
xyz.swapee.wc.IOffersTableController.__resetTick

// nss:xyz.swapee.wc.IOffersTableController,$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.reset filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.IOffersTableController.__reset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.IOffersTableController.reset
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController)} */
xyz.swapee.wc.IOffersTableController._reset
/** @typedef {typeof $xyz.swapee.wc.IOffersTableController.__reset} */
xyz.swapee.wc.IOffersTableController.__reset

// nss:xyz.swapee.wc.IOffersTableController,$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.onReset filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.IOffersTableController.__onReset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.IOffersTableController.onReset
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController)} */
xyz.swapee.wc.IOffersTableController._onReset
/** @typedef {typeof $xyz.swapee.wc.IOffersTableController.__onReset} */
xyz.swapee.wc.IOffersTableController.__onReset

// nss:xyz.swapee.wc.IOffersTableController,$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.Inputs filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTablePort.Inputs}
 */
$xyz.swapee.wc.IOffersTableController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableController.Inputs} */
xyz.swapee.wc.IOffersTableController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.WeakInputs filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTablePort.WeakInputs}
 */
$xyz.swapee.wc.IOffersTableController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableController.WeakInputs} */
xyz.swapee.wc.IOffersTableController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableController
/* @typal-end */