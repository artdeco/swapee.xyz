/** @const {?} */ $xyz.swapee.wc.IOffersTablePort
/** @const {?} */ $xyz.swapee.wc.IOffersTablePortInterface
/** @const {?} */ xyz.swapee.wc.IOffersTablePort
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.Initialese filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IOffersTablePort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTablePort.Initialese} */
xyz.swapee.wc.IOffersTablePort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortFields filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @interface */
$xyz.swapee.wc.IOffersTablePortFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTablePort.Inputs} */
$xyz.swapee.wc.IOffersTablePortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOffersTablePort.Inputs} */
$xyz.swapee.wc.IOffersTablePortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTablePortFields}
 */
xyz.swapee.wc.IOffersTablePortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortCaster filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @interface */
$xyz.swapee.wc.IOffersTablePortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTablePort} */
$xyz.swapee.wc.IOffersTablePortCaster.prototype.asIOffersTablePort
/** @type {!xyz.swapee.wc.BoundOffersTablePort} */
$xyz.swapee.wc.IOffersTablePortCaster.prototype.superOffersTablePort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTablePortCaster}
 */
xyz.swapee.wc.IOffersTablePortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTablePortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTablePortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersTablePort.Inputs>}
 */
$xyz.swapee.wc.IOffersTablePort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersTablePort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersTablePort.prototype.resetOffersTablePort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTablePort}
 */
xyz.swapee.wc.IOffersTablePort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.OffersTablePort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTablePort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTablePort.Initialese>}
 */
$xyz.swapee.wc.OffersTablePort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.OffersTablePort
/** @type {function(new: xyz.swapee.wc.IOffersTablePort, ...!xyz.swapee.wc.IOffersTablePort.Initialese)} */
xyz.swapee.wc.OffersTablePort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.OffersTablePort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.AbstractOffersTablePort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init
 * @extends {xyz.swapee.wc.OffersTablePort}
 */
$xyz.swapee.wc.AbstractOffersTablePort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort
/** @type {function(new: xyz.swapee.wc.AbstractOffersTablePort)} */
xyz.swapee.wc.AbstractOffersTablePort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTablePort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.OffersTablePortConstructor filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTablePort, ...!xyz.swapee.wc.IOffersTablePort.Initialese)} */
xyz.swapee.wc.OffersTablePortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.RecordIOffersTablePort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersTablePort.resetPort, resetOffersTablePort: xyz.swapee.wc.IOffersTablePort.resetOffersTablePort }} */
xyz.swapee.wc.RecordIOffersTablePort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.BoundIOffersTablePort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTablePortFields}
 * @extends {xyz.swapee.wc.RecordIOffersTablePort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTablePortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOffersTablePort.Inputs>}
 */
$xyz.swapee.wc.BoundIOffersTablePort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTablePort} */
xyz.swapee.wc.BoundIOffersTablePort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.BoundOffersTablePort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTablePort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTablePort = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTablePort} */
xyz.swapee.wc.BoundOffersTablePort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.resetPort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersTablePort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTablePort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersTablePort): void} */
xyz.swapee.wc.IOffersTablePort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOffersTablePort.__resetPort} */
xyz.swapee.wc.IOffersTablePort.__resetPort

// nss:xyz.swapee.wc.IOffersTablePort,$xyz.swapee.wc.IOffersTablePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.resetOffersTablePort filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTablePort.resetOffersTablePort
/** @typedef {function(this: xyz.swapee.wc.IOffersTablePort): void} */
xyz.swapee.wc.IOffersTablePort._resetOffersTablePort
/** @typedef {typeof $xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort} */
xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort

// nss:xyz.swapee.wc.IOffersTablePort,$xyz.swapee.wc.IOffersTablePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.Inputs filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOffersTablePort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersTablePort.Inputs}
 */
xyz.swapee.wc.IOffersTablePort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.WeakInputs filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOffersTablePort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersTablePort.WeakInputs}
 */
xyz.swapee.wc.IOffersTablePort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortInterface filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @interface */
$xyz.swapee.wc.IOffersTablePortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTablePortInterface}
 */
xyz.swapee.wc.IOffersTablePortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.OffersTablePortInterface filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTablePortInterface}
 */
$xyz.swapee.wc.OffersTablePortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTablePortInterface}
 */
xyz.swapee.wc.OffersTablePortInterface
/** @type {function(new: xyz.swapee.wc.IOffersTablePortInterface)} */
xyz.swapee.wc.OffersTablePortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortInterface.Props filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @record */
$xyz.swapee.wc.IOffersTablePortInterface.Props = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.IOffersTablePortInterface.Props.prototype.untilReset
/** @typedef {$xyz.swapee.wc.IOffersTablePortInterface.Props} */
xyz.swapee.wc.IOffersTablePortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTablePortInterface
/* @typal-end */