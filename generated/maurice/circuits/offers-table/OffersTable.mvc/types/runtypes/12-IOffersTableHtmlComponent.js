/** @const {?} */ $xyz.swapee.wc.IOffersTableHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.IOffersTableHtmlComponent.Initialese filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersTableController.Initialese}
 * @extends {xyz.swapee.wc.back.IOffersTableScreen.Initialese}
 * @extends {xyz.swapee.wc.IOffersTable.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableComputer.Initialese}
 */
$xyz.swapee.wc.IOffersTableHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} */
xyz.swapee.wc.IOffersTableHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.IOffersTableHtmlComponentCaster filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/** @interface */
$xyz.swapee.wc.IOffersTableHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableHtmlComponent} */
$xyz.swapee.wc.IOffersTableHtmlComponentCaster.prototype.asIOffersTableHtmlComponent
/** @type {!xyz.swapee.wc.BoundOffersTableHtmlComponent} */
$xyz.swapee.wc.IOffersTableHtmlComponentCaster.prototype.superOffersTableHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableHtmlComponentCaster}
 */
xyz.swapee.wc.IOffersTableHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.IOffersTableHtmlComponent filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IOffersTableController}
 * @extends {xyz.swapee.wc.back.IOffersTableScreen}
 * @extends {xyz.swapee.wc.IOffersTable}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.IOffersTableGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.IOffersTableProcessor}
 * @extends {xyz.swapee.wc.IOffersTableComputer}
 */
$xyz.swapee.wc.IOffersTableHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableHtmlComponent}
 */
xyz.swapee.wc.IOffersTableHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.OffersTableHtmlComponent filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.OffersTableHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.OffersTableHtmlComponent
/** @type {function(new: xyz.swapee.wc.IOffersTableHtmlComponent, ...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese)} */
xyz.swapee.wc.OffersTableHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.OffersTableHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.AbstractOffersTableHtmlComponent filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableHtmlComponent}
 */
$xyz.swapee.wc.AbstractOffersTableHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableHtmlComponent)} */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.OffersTableHtmlComponentConstructor filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableHtmlComponent, ...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese)} */
xyz.swapee.wc.OffersTableHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.RecordIOffersTableHtmlComponent filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.BoundIOffersTableHtmlComponent filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIOffersTableController}
 * @extends {xyz.swapee.wc.back.BoundIOffersTableScreen}
 * @extends {xyz.swapee.wc.BoundIOffersTable}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.BoundIOffersTableGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.BoundIOffersTableProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 */
$xyz.swapee.wc.BoundIOffersTableHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableHtmlComponent} */
xyz.swapee.wc.BoundIOffersTableHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.BoundOffersTableHtmlComponent filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableHtmlComponent} */
xyz.swapee.wc.BoundOffersTableHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */