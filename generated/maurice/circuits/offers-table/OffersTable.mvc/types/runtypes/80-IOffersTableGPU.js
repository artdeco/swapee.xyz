/** @const {?} */ $xyz.swapee.wc.IOffersTableGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersTableDisplay.Initialese}
 */
$xyz.swapee.wc.IOffersTableGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableGPU.Initialese} */
xyz.swapee.wc.IOffersTableGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOffersTableGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IOffersTableGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableGPUFields}
 */
xyz.swapee.wc.IOffersTableGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOffersTableGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableGPU} */
$xyz.swapee.wc.IOffersTableGPUCaster.prototype.asIOffersTableGPU
/** @type {!xyz.swapee.wc.BoundOffersTableGPU} */
$xyz.swapee.wc.IOffersTableGPUCaster.prototype.superOffersTableGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableGPUCaster}
 */
xyz.swapee.wc.IOffersTableGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!OffersTableMemory,.!OffersTableLand>}
 * @extends {xyz.swapee.wc.back.IOffersTableDisplay}
 */
$xyz.swapee.wc.IOffersTableGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableGPU}
 */
xyz.swapee.wc.IOffersTableGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.OffersTableGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableGPU.Initialese>}
 */
$xyz.swapee.wc.OffersTableGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.OffersTableGPU
/** @type {function(new: xyz.swapee.wc.IOffersTableGPU, ...!xyz.swapee.wc.IOffersTableGPU.Initialese)} */
xyz.swapee.wc.OffersTableGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.OffersTableGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.AbstractOffersTableGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableGPU}
 */
$xyz.swapee.wc.AbstractOffersTableGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableGPU)} */
xyz.swapee.wc.AbstractOffersTableGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.OffersTableGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableGPU, ...!xyz.swapee.wc.IOffersTableGPU.Initialese)} */
xyz.swapee.wc.OffersTableGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.RecordIOffersTableGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.BoundIOffersTableGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableGPUFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!OffersTableMemory,.!OffersTableLand>}
 * @extends {xyz.swapee.wc.back.BoundIOffersTableDisplay}
 */
$xyz.swapee.wc.BoundIOffersTableGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableGPU} */
xyz.swapee.wc.BoundIOffersTableGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.BoundOffersTableGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableGPU} */
xyz.swapee.wc.BoundOffersTableGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */