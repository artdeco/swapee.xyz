/** @const {?} */ $xyz.swapee.wc.IOffersTableComputer
/** @const {?} */ $xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink
/** @const {?} */ $xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink
/** @const {?} */ $xyz.swapee.wc.IOffersTableComputer.compute
/** @const {?} */ xyz.swapee.wc.IOffersTableComputer
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.Initialese filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IOffersTableComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableComputer.Initialese} */
xyz.swapee.wc.IOffersTableComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputerCaster filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @interface */
$xyz.swapee.wc.IOffersTableComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableComputer} */
$xyz.swapee.wc.IOffersTableComputerCaster.prototype.asIOffersTableComputer
/** @type {!xyz.swapee.wc.BoundOffersTableComputer} */
$xyz.swapee.wc.IOffersTableComputerCaster.prototype.superOffersTableComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableComputerCaster}
 */
xyz.swapee.wc.IOffersTableComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.OffersTableMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>}
 */
$xyz.swapee.wc.IOffersTableComputer = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)}
 */
$xyz.swapee.wc.IOffersTableComputer.prototype.adaptChangellyFixedLink = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)}
 */
$xyz.swapee.wc.IOffersTableComputer.prototype.adaptChangellyFloatLink = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.OffersTableMemory} mem
 * @param {!xyz.swapee.wc.IOffersTableComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableComputer.prototype.compute = function(mem, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableComputer}
 */
xyz.swapee.wc.IOffersTableComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.OffersTableComputer filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableComputer.Initialese>}
 */
$xyz.swapee.wc.OffersTableComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.OffersTableComputer
/** @type {function(new: xyz.swapee.wc.IOffersTableComputer, ...!xyz.swapee.wc.IOffersTableComputer.Initialese)} */
xyz.swapee.wc.OffersTableComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.OffersTableComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.AbstractOffersTableComputer filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableComputer}
 */
$xyz.swapee.wc.AbstractOffersTableComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableComputer)} */
xyz.swapee.wc.AbstractOffersTableComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.OffersTableComputerConstructor filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableComputer, ...!xyz.swapee.wc.IOffersTableComputer.Initialese)} */
xyz.swapee.wc.OffersTableComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.RecordIOffersTableComputer filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @typedef {{ adaptChangellyFixedLink: xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink, adaptChangellyFloatLink: xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink, compute: xyz.swapee.wc.IOffersTableComputer.compute }} */
xyz.swapee.wc.RecordIOffersTableComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.BoundIOffersTableComputer filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.OffersTableMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersTableLand>}
 */
$xyz.swapee.wc.BoundIOffersTableComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableComputer} */
xyz.swapee.wc.BoundIOffersTableComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.BoundOffersTableComputer filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableComputer} */
xyz.swapee.wc.BoundOffersTableComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)}
 */
$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)}
 * @this {xyz.swapee.wc.IOffersTableComputer}
 */
$xyz.swapee.wc.IOffersTableComputer._adaptChangellyFixedLink = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer._adaptChangellyFixedLink} */
xyz.swapee.wc.IOffersTableComputer._adaptChangellyFixedLink
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink} */
xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink

// nss:xyz.swapee.wc.IOffersTableComputer,$xyz.swapee.wc.IOffersTableComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)}
 */
$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)}
 * @this {xyz.swapee.wc.IOffersTableComputer}
 */
$xyz.swapee.wc.IOffersTableComputer._adaptChangellyFloatLink = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer._adaptChangellyFloatLink} */
xyz.swapee.wc.IOffersTableComputer._adaptChangellyFloatLink
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink} */
xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink

// nss:xyz.swapee.wc.IOffersTableComputer,$xyz.swapee.wc.IOffersTableComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.compute filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @param {xyz.swapee.wc.OffersTableMemory} mem
 * @param {!xyz.swapee.wc.IOffersTableComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableComputer.compute = function(mem, land) {}
/**
 * @param {xyz.swapee.wc.OffersTableMemory} mem
 * @param {!xyz.swapee.wc.IOffersTableComputer.compute.Land} land
 * @return {void}
 * @this {xyz.swapee.wc.IOffersTableComputer}
 */
$xyz.swapee.wc.IOffersTableComputer._compute = function(mem, land) {}
/**
 * @template THIS
 * @param {xyz.swapee.wc.OffersTableMemory} mem
 * @param {!xyz.swapee.wc.IOffersTableComputer.compute.Land} land
 * @return {void}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableComputer.__compute = function(mem, land) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer.compute} */
xyz.swapee.wc.IOffersTableComputer.compute
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer._compute} */
xyz.swapee.wc.IOffersTableComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IOffersTableComputer.__compute} */
xyz.swapee.wc.IOffersTableComputer.__compute

// nss:xyz.swapee.wc.IOffersTableComputer,$xyz.swapee.wc.IOffersTableComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 */
$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink}
 */
$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 */
$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink}
 */
$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.compute.Land filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @record */
$xyz.swapee.wc.IOffersTableComputer.compute.Land = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.CryptoSelectOut
/** @typedef {$xyz.swapee.wc.IOffersTableComputer.compute.Land} */
xyz.swapee.wc.IOffersTableComputer.compute.Land

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableComputer.compute
/* @typal-end */