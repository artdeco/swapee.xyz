/** @const {?} */ $xyz.swapee.wc.IOffersTableElement
/** @const {?} */ $xyz.swapee.wc.IOffersTableElement.build
/** @const {?} */ $xyz.swapee.wc.IOffersTableElement.short
/** @const {?} */ xyz.swapee.wc.IOffersTableElement
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.Initialese filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @record
 * @extends {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.OffersTableLand>}
 * @extends {guest.maurice.IMilleu.Initialese<!(xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IOffersAggregator|com.webcircuits.ui.ICollapsar|xyz.swapee.wc.ICryptoSelect)>}
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.IOffersTableElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableElement.Initialese} */
xyz.swapee.wc.IOffersTableElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElementFields filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @interface */
$xyz.swapee.wc.IOffersTableElementFields = function() {}
/** @type {!xyz.swapee.wc.IOffersTableElement.Inputs} */
$xyz.swapee.wc.IOffersTableElementFields.prototype.inputs
/** @type {!Object<string, !Object<string, number>>} */
$xyz.swapee.wc.IOffersTableElementFields.prototype.buildees
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableElementFields}
 */
xyz.swapee.wc.IOffersTableElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElementCaster filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @interface */
$xyz.swapee.wc.IOffersTableElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableElement} */
$xyz.swapee.wc.IOffersTableElementCaster.prototype.asIOffersTableElement
/** @type {!xyz.swapee.wc.BoundOffersTableElement} */
$xyz.swapee.wc.IOffersTableElementCaster.prototype.superOffersTableElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableElementCaster}
 */
xyz.swapee.wc.IOffersTableElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs, !xyz.swapee.wc.OffersTableLand>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>}
 * @extends {guest.maurice.IMilleu<!(xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IOffersAggregator|com.webcircuits.ui.ICollapsar|xyz.swapee.wc.ICryptoSelect)>}
 */
$xyz.swapee.wc.IOffersTableElement = function() {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Instances} instances
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.build = function(cores, instances) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.buildExchangeIntent = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.buildOffersAggregator = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.buildProgressCollapsar = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.buildCryptoSelectOut = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Cores} cores
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} [model]
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableElement}
 */
xyz.swapee.wc.IOffersTableElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.OffersTableElement filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElement.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableElement.Initialese>}
 */
$xyz.swapee.wc.OffersTableElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.OffersTableElement
/** @type {function(new: xyz.swapee.wc.IOffersTableElement, ...!xyz.swapee.wc.IOffersTableElement.Initialese)} */
xyz.swapee.wc.OffersTableElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.OffersTableElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.AbstractOffersTableElement filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElement.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableElement}
 */
$xyz.swapee.wc.AbstractOffersTableElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableElement)} */
xyz.swapee.wc.AbstractOffersTableElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableElement|typeof xyz.swapee.wc.OffersTableElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableElement|typeof xyz.swapee.wc.OffersTableElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableElement|typeof xyz.swapee.wc.OffersTableElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.OffersTableElementConstructor filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableElement, ...!xyz.swapee.wc.IOffersTableElement.Initialese)} */
xyz.swapee.wc.OffersTableElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.RecordIOffersTableElement filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @typedef {{ solder: xyz.swapee.wc.IOffersTableElement.solder, render: xyz.swapee.wc.IOffersTableElement.render, build: xyz.swapee.wc.IOffersTableElement.build, buildExchangeIntent: xyz.swapee.wc.IOffersTableElement.buildExchangeIntent, buildOffersAggregator: xyz.swapee.wc.IOffersTableElement.buildOffersAggregator, buildProgressCollapsar: xyz.swapee.wc.IOffersTableElement.buildProgressCollapsar, buildCryptoSelectOut: xyz.swapee.wc.IOffersTableElement.buildCryptoSelectOut, short: xyz.swapee.wc.IOffersTableElement.short, server: xyz.swapee.wc.IOffersTableElement.server, inducer: xyz.swapee.wc.IOffersTableElement.inducer }} */
xyz.swapee.wc.RecordIOffersTableElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.BoundIOffersTableElement filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableElementFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs, !xyz.swapee.wc.OffersTableLand>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersTableLand>}
 * @extends {guest.maurice.BoundIMilleu<!(xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IOffersAggregator|com.webcircuits.ui.ICollapsar|xyz.swapee.wc.ICryptoSelect)>}
 */
$xyz.swapee.wc.BoundIOffersTableElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableElement} */
xyz.swapee.wc.BoundIOffersTableElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.BoundOffersTableElement filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableElement} */
xyz.swapee.wc.BoundOffersTableElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.solder filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IOffersTableElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.IOffersTableElement}
 */
$xyz.swapee.wc.IOffersTableElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.solder} */
xyz.swapee.wc.IOffersTableElement.solder
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement._solder} */
xyz.swapee.wc.IOffersTableElement._solder
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__solder} */
xyz.swapee.wc.IOffersTableElement.__solder

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.render filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersTableElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.OffersTableMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IOffersTableElement.render
/** @typedef {function(this: xyz.swapee.wc.IOffersTableElement, !xyz.swapee.wc.OffersTableMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IOffersTableElement._render
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__render} */
xyz.swapee.wc.IOffersTableElement.__render

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.build filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Instances} instances
 */
$xyz.swapee.wc.IOffersTableElement.build = function(cores, instances) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Instances} instances
 * @this {xyz.swapee.wc.IOffersTableElement}
 */
$xyz.swapee.wc.IOffersTableElement._build = function(cores, instances) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Instances} instances
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableElement.__build = function(cores, instances) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.build} */
xyz.swapee.wc.IOffersTableElement.build
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement._build} */
xyz.swapee.wc.IOffersTableElement._build
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__build} */
xyz.swapee.wc.IOffersTableElement.__build

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.buildExchangeIntent filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IOffersTableElement.__buildExchangeIntent = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IOffersTableElement.buildExchangeIntent
/** @typedef {function(this: xyz.swapee.wc.IOffersTableElement, !Object, !Object)} */
xyz.swapee.wc.IOffersTableElement._buildExchangeIntent
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__buildExchangeIntent} */
xyz.swapee.wc.IOffersTableElement.__buildExchangeIntent

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.buildOffersAggregator filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IOffersTableElement.__buildOffersAggregator = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IOffersTableElement.buildOffersAggregator
/** @typedef {function(this: xyz.swapee.wc.IOffersTableElement, !Object, !Object)} */
xyz.swapee.wc.IOffersTableElement._buildOffersAggregator
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__buildOffersAggregator} */
xyz.swapee.wc.IOffersTableElement.__buildOffersAggregator

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.buildProgressCollapsar filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IOffersTableElement.__buildProgressCollapsar = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IOffersTableElement.buildProgressCollapsar
/** @typedef {function(this: xyz.swapee.wc.IOffersTableElement, !Object, !Object)} */
xyz.swapee.wc.IOffersTableElement._buildProgressCollapsar
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__buildProgressCollapsar} */
xyz.swapee.wc.IOffersTableElement.__buildProgressCollapsar

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.buildCryptoSelectOut filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IOffersTableElement.__buildCryptoSelectOut = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IOffersTableElement.buildCryptoSelectOut
/** @typedef {function(this: xyz.swapee.wc.IOffersTableElement, !Object, !Object)} */
xyz.swapee.wc.IOffersTableElement._buildCryptoSelectOut
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__buildCryptoSelectOut} */
xyz.swapee.wc.IOffersTableElement.__buildCryptoSelectOut

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.short filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Cores} cores
 */
$xyz.swapee.wc.IOffersTableElement.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Cores} cores
 * @this {xyz.swapee.wc.IOffersTableElement}
 */
$xyz.swapee.wc.IOffersTableElement._short = function(model, ports, cores) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} model
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Cores} cores
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableElement.__short = function(model, ports, cores) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.short} */
xyz.swapee.wc.IOffersTableElement.short
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement._short} */
xyz.swapee.wc.IOffersTableElement._short
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__short} */
xyz.swapee.wc.IOffersTableElement.__short

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.server filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersTableElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.IOffersTableElement}
 */
$xyz.swapee.wc.IOffersTableElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.server} */
xyz.swapee.wc.IOffersTableElement.server
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement._server} */
xyz.swapee.wc.IOffersTableElement._server
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__server} */
xyz.swapee.wc.IOffersTableElement.__server

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.inducer filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} [model]
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} [port]
 */
$xyz.swapee.wc.IOffersTableElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} [model]
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} [port]
 * @this {xyz.swapee.wc.IOffersTableElement}
 */
$xyz.swapee.wc.IOffersTableElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} [model]
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersTableElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.inducer} */
xyz.swapee.wc.IOffersTableElement.inducer
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement._inducer} */
xyz.swapee.wc.IOffersTableElement._inducer
/** @typedef {typeof $xyz.swapee.wc.IOffersTableElement.__inducer} */
xyz.swapee.wc.IOffersTableElement.__inducer

// nss:xyz.swapee.wc.IOffersTableElement,$xyz.swapee.wc.IOffersTableElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.Inputs filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTablePort.Inputs}
 * @extends {xyz.swapee.wc.IOffersTableDisplay.Queries}
 * @extends {xyz.swapee.wc.IOffersTableController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.IOffersTableElementPort.Inputs}
 */
$xyz.swapee.wc.IOffersTableElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableElement.Inputs} */
xyz.swapee.wc.IOffersTableElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.build.Cores filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @record */
$xyz.swapee.wc.IOffersTableElement.build.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Cores.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Cores.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Cores.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Cores.prototype.CryptoSelectOut
/** @typedef {$xyz.swapee.wc.IOffersTableElement.build.Cores} */
xyz.swapee.wc.IOffersTableElement.build.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.build.Instances filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @record */
$xyz.swapee.wc.IOffersTableElement.build.Instances = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Instances.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Instances.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Instances.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.build.Instances.prototype.CryptoSelectOut
/** @typedef {$xyz.swapee.wc.IOffersTableElement.build.Instances} */
xyz.swapee.wc.IOffersTableElement.build.Instances

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.short.Ports filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @record */
$xyz.swapee.wc.IOffersTableElement.short.Ports = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Ports.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Ports.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Ports.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Ports.prototype.CryptoSelectOut
/** @typedef {$xyz.swapee.wc.IOffersTableElement.short.Ports} */
xyz.swapee.wc.IOffersTableElement.short.Ports

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElement.short
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml} xyz.swapee.wc.IOffersTableElement.short.Cores filter:!ControllerPlugin~props 8bf224495eb0e860ead39cc941c15c9d */
/** @record */
$xyz.swapee.wc.IOffersTableElement.short.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Cores.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Cores.prototype.OffersAggregator
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Cores.prototype.ProgressCollapsar
/** @type {!Object} */
$xyz.swapee.wc.IOffersTableElement.short.Cores.prototype.CryptoSelectOut
/** @typedef {$xyz.swapee.wc.IOffersTableElement.short.Cores} */
xyz.swapee.wc.IOffersTableElement.short.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableElement.short
/* @typal-end */