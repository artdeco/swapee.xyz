/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IOffersTableComputer': {
  'id': 11548161941,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptChangellyFixedLink': 2,
   'adaptChangellyFloatLink': 3
  }
 },
 'xyz.swapee.wc.OffersTableMemoryPQs': {
  'id': 11548161942,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTableOuterCore': {
  'id': 11548161943,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersTableInputsPQs': {
  'id': 11548161944,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTablePort': {
  'id': 11548161945,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersTablePort': 2
  }
 },
 'xyz.swapee.wc.IOffersTablePortInterface': {
  'id': 11548161946,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableCore': {
  'id': 11548161947,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersTableCore': 2
  }
 },
 'xyz.swapee.wc.IOffersTableProcessor': {
  'id': 11548161948,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTable': {
  'id': 11548161949,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableBuffer': {
  'id': 115481619410,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableHtmlComponentUtil': {
  'id': 115481619411,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IOffersTableHtmlComponent': {
  'id': 115481619412,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableElement': {
  'id': 115481619413,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildExchangeIntent': 4,
   'buildOffersAggregator': 5,
   'buildProgressCollapsar': 6,
   'buildCryptoSelectOut': 8,
   'short': 9,
   'server': 10,
   'inducer': 11
  }
 },
 'xyz.swapee.wc.IOffersTableElementPort': {
  'id': 115481619414,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableDesigner': {
  'id': 115481619415,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersTableGPU': {
  'id': 115481619416,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableDisplay': {
  'id': 115481619417,
  'symbols': {},
  'methods': {
   'paint': 1,
   'paintTableOrder': 3
  }
 },
 'xyz.swapee.wc.OffersTableQueriesPQs': {
  'id': 115481619418,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.OffersTableVdusPQs': {
  'id': 115481619419,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersTableDisplay': {
  'id': 115481619420,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersTableClassesPQs': {
  'id': 115481619421,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTableControllerHyperslice': {
  'id': 115481619422,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableControllerBindingHyperslice': {
  'id': 115481619423,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableController': {
  'id': 115481619424,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTick': 2,
   'reset': 3,
   'onReset': 4
  }
 },
 'xyz.swapee.wc.front.IOffersTableControllerHyperslice': {
  'id': 115481619425,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice': {
  'id': 115481619426,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableController': {
  'id': 115481619427,
  'symbols': {},
  'methods': {
   'resetTick': 1,
   'reset': 2,
   'onReset': 3
  }
 },
 'xyz.swapee.wc.back.IOffersTableController': {
  'id': 115481619428,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableControllerAR': {
  'id': 115481619429,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableControllerAT': {
  'id': 115481619430,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableScreen': {
  'id': 115481619431,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableScreen': {
  'id': 115481619432,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableScreenAR': {
  'id': 115481619433,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableScreenAT': {
  'id': 115481619434,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersTableCachePQs': {
  'id': 115481619435,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})