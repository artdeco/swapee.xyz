/**
 * @fileoverview
 * @externs
 */

xyz.swapee.wc.IOffersTableComputer={}
xyz.swapee.wc.IOffersTablePort={}
xyz.swapee.wc.IOffersTableCore={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IOffersTableController={}
xyz.swapee.wc.IOffersTableDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IOffersTableDisplay={}
xyz.swapee.wc.IOffersTableController={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.wc={}
$$xyz.swapee.wc.IOffersTableComputer={}
$$xyz.swapee.wc.IOffersTablePort={}
$$xyz.swapee.wc.IOffersTableCore={}
$$xyz.swapee.wc.IOffersTableDisplay={}
$$xyz.swapee.wc.back={}
$$xyz.swapee.wc.back.IOffersTableDisplay={}
$$xyz.swapee.wc.IOffersTableController={}
$$xyz.swapee.wc.front={}
$$xyz.swapee.wc.front.IOffersTableController={}
$$xyz.swapee.wc.IOffersTableHtmlComponentUtil={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
xyz.swapee.wc.IOffersTableComputer.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @interface */
xyz.swapee.wc.IOffersTableComputerCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableComputer} */
xyz.swapee.wc.IOffersTableComputerCaster.prototype.asIOffersTableComputer
/** @type {!xyz.swapee.wc.BoundOffersTableComputer} */
xyz.swapee.wc.IOffersTableComputerCaster.prototype.superOffersTableComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.OffersTableMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>}
 */
xyz.swapee.wc.IOffersTableComputer = function() {}
/** @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init */
xyz.swapee.wc.IOffersTableComputer.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)}
 */
xyz.swapee.wc.IOffersTableComputer.prototype.adaptChangellyFixedLink = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)}
 */
xyz.swapee.wc.IOffersTableComputer.prototype.adaptChangellyFloatLink = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.OffersTableMemory} mem
 * @param {!xyz.swapee.wc.IOffersTableComputer.compute.Land} land
 * @return {void}
 */
xyz.swapee.wc.IOffersTableComputer.prototype.compute = function(mem, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.OffersTableComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableComputer.Initialese>}
 */
xyz.swapee.wc.OffersTableComputer = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init */
xyz.swapee.wc.OffersTableComputer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.OffersTableComputer.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.AbstractOffersTableComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableComputer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.OffersTableComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableComputer, ...!xyz.swapee.wc.IOffersTableComputer.Initialese)} */
xyz.swapee.wc.OffersTableComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.RecordIOffersTableComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @typedef {{ adaptChangellyFixedLink: xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink, adaptChangellyFloatLink: xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink, compute: xyz.swapee.wc.IOffersTableComputer.compute }} */
xyz.swapee.wc.RecordIOffersTableComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.BoundIOffersTableComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.OffersTableMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersTableLand>}
 */
xyz.swapee.wc.BoundIOffersTableComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.BoundOffersTableComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)}
 */
$$xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form, xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form): (undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink
/** @typedef {function(this: xyz.swapee.wc.IOffersTableComputer, !xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form, xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form): (undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)} */
xyz.swapee.wc.IOffersTableComputer._adaptChangellyFixedLink
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink} */
xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink

// nss:xyz.swapee.wc.IOffersTableComputer,$$xyz.swapee.wc.IOffersTableComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} form
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)}
 */
$$xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form, xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form): (undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)} */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink
/** @typedef {function(this: xyz.swapee.wc.IOffersTableComputer, !xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form, xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form): (undefined|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)} */
xyz.swapee.wc.IOffersTableComputer._adaptChangellyFloatLink
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink} */
xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink

// nss:xyz.swapee.wc.IOffersTableComputer,$$xyz.swapee.wc.IOffersTableComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.OffersTableMemory} mem
 * @param {!xyz.swapee.wc.IOffersTableComputer.compute.Land} land
 * @return {void}
 */
$$xyz.swapee.wc.IOffersTableComputer.__compute = function(mem, land) {}
/** @typedef {function(xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableComputer.compute.Land): void} */
xyz.swapee.wc.IOffersTableComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IOffersTableComputer, xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableComputer.compute.Land): void} */
xyz.swapee.wc.IOffersTableComputer._compute
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableComputer.__compute} */
xyz.swapee.wc.IOffersTableComputer.__compute

// nss:xyz.swapee.wc.IOffersTableComputer,$$xyz.swapee.wc.IOffersTableComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink.prototype.changellyFixedLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink}
 */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink.prototype.changellyFloatLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink}
 */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml} xyz.swapee.wc.IOffersTableComputer.compute.Land exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ec30cab9599087bf7e441efe3e8b3fa4 */
/** @record */
xyz.swapee.wc.IOffersTableComputer.compute.Land = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.OffersAggregator
/** @type {!Object} */
xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.ProgressCollapsar
/** @type {!Object} */
xyz.swapee.wc.IOffersTableComputer.compute.Land.prototype.CryptoSelectOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
xyz.swapee.wc.IOffersTableOuterCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @interface */
xyz.swapee.wc.IOffersTableOuterCoreFields
/** @type {!xyz.swapee.wc.IOffersTableOuterCore.Model} */
xyz.swapee.wc.IOffersTableOuterCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @interface */
xyz.swapee.wc.IOffersTableOuterCoreCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableOuterCore} */
xyz.swapee.wc.IOffersTableOuterCoreCaster.prototype.asIOffersTableOuterCore
/** @type {!xyz.swapee.wc.BoundOffersTableOuterCore} */
xyz.swapee.wc.IOffersTableOuterCoreCaster.prototype.superOffersTableOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreCaster}
 */
xyz.swapee.wc.IOffersTableOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.OffersTableOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableOuterCore.Initialese>}
 */
xyz.swapee.wc.OffersTableOuterCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.OffersTableOuterCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.AbstractOffersTableOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore = function() {}
/**
 * @param {...(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersTableMemoryPQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableMemoryPQs.prototype.untilReset
/** @type {string} */
xyz.swapee.wc.OffersTableMemoryPQs.prototype.coinIm

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersTableMemoryQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableMemoryQPs.prototype.fbbee
/** @type {string} */
xyz.swapee.wc.OffersTableMemoryQPs.prototype.b1a5b

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.RecordIOffersTableOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.BoundIOffersTableOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableOuterCoreCaster}
 */
xyz.swapee.wc.BoundIOffersTableOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.BoundOffersTableOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset.untilReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @typedef {number} */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset.untilReset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset.prototype.untilReset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset}
 */
xyz.swapee.wc.IOffersTableOuterCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset.prototype.untilReset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset}
 */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
xyz.swapee.wc.IOffersTablePort.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @interface */
xyz.swapee.wc.IOffersTablePortFields
/** @type {!xyz.swapee.wc.IOffersTablePort.Inputs} */
xyz.swapee.wc.IOffersTablePortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOffersTablePort.Inputs} */
xyz.swapee.wc.IOffersTablePortFields.prototype.props

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @interface */
xyz.swapee.wc.IOffersTablePortCaster
/** @type {!xyz.swapee.wc.BoundIOffersTablePort} */
xyz.swapee.wc.IOffersTablePortCaster.prototype.asIOffersTablePort
/** @type {!xyz.swapee.wc.BoundOffersTablePort} */
xyz.swapee.wc.IOffersTablePortCaster.prototype.superOffersTablePort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTablePortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTablePortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersTablePort.Inputs>}
 */
xyz.swapee.wc.IOffersTablePort = function() {}
/** @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init */
xyz.swapee.wc.IOffersTablePort.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IOffersTablePort.prototype.resetPort = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersTablePort.prototype.resetOffersTablePort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.OffersTablePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTablePort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTablePort.Initialese>}
 */
xyz.swapee.wc.OffersTablePort = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init */
xyz.swapee.wc.OffersTablePort.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.OffersTablePort.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.AbstractOffersTablePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init
 * @extends {xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTablePort.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.OffersTablePortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTablePort, ...!xyz.swapee.wc.IOffersTablePort.Initialese)} */
xyz.swapee.wc.OffersTablePortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.OffersTableMemoryPQs}
 */
xyz.swapee.wc.OffersTableInputsPQs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableMemoryPQs}
 * @dict
 */
xyz.swapee.wc.OffersTableInputsQPs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.RecordIOffersTablePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersTablePort.resetPort, resetOffersTablePort: xyz.swapee.wc.IOffersTablePort.resetOffersTablePort }} */
xyz.swapee.wc.RecordIOffersTablePort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.BoundIOffersTablePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTablePortFields}
 * @extends {xyz.swapee.wc.RecordIOffersTablePort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTablePortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOffersTablePort.Inputs>}
 */
xyz.swapee.wc.BoundIOffersTablePort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.BoundOffersTablePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTablePort}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTablePort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersTablePort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTablePort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersTablePort): void} */
xyz.swapee.wc.IOffersTablePort._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IOffersTablePort.__resetPort} */
xyz.swapee.wc.IOffersTablePort.__resetPort

// nss:xyz.swapee.wc.IOffersTablePort,$$xyz.swapee.wc.IOffersTablePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.resetOffersTablePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTablePort.resetOffersTablePort
/** @typedef {function(this: xyz.swapee.wc.IOffersTablePort): void} */
xyz.swapee.wc.IOffersTablePort._resetOffersTablePort
/** @typedef {typeof $$xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort} */
xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort

// nss:xyz.swapee.wc.IOffersTablePort,$$xyz.swapee.wc.IOffersTablePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel}
 */
xyz.swapee.wc.IOffersTablePort.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel}
 */
xyz.swapee.wc.IOffersTablePort.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @interface */
xyz.swapee.wc.IOffersTablePortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.OffersTablePortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTablePortInterface}
 */
xyz.swapee.wc.OffersTablePortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml} xyz.swapee.wc.IOffersTablePortInterface.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b03a85ef4cd3de814403652285b3efc7 */
/** @record */
xyz.swapee.wc.IOffersTablePortInterface.Props = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.IOffersTablePortInterface.Props.prototype.untilReset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
xyz.swapee.wc.IOffersTableCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @interface */
xyz.swapee.wc.IOffersTableCoreFields
/** @type {!xyz.swapee.wc.IOffersTableCore.Model} */
xyz.swapee.wc.IOffersTableCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @interface */
xyz.swapee.wc.IOffersTableCoreCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableCore} */
xyz.swapee.wc.IOffersTableCoreCaster.prototype.asIOffersTableCore
/** @type {!xyz.swapee.wc.BoundOffersTableCore} */
xyz.swapee.wc.IOffersTableCoreCaster.prototype.superOffersTableCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCoreCaster}
 * @extends {xyz.swapee.wc.IOffersTableOuterCore}
 */
xyz.swapee.wc.IOffersTableCore = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersTableCore.prototype.resetCore = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersTableCore.prototype.resetOffersTableCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.OffersTableCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableCore.Initialese>}
 */
xyz.swapee.wc.OffersTableCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.OffersTableCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.AbstractOffersTableCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore = function() {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableCachePQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersTableCachePQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableCachePQs.prototype.changellyFixedLink
/** @type {string} */
xyz.swapee.wc.OffersTableCachePQs.prototype.changellyFloatLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableCacheQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersTableCacheQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableCacheQPs.prototype.ca106
/** @type {string} */
xyz.swapee.wc.OffersTableCacheQPs.prototype.h1175

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.RecordIOffersTableCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @typedef {{ resetCore: xyz.swapee.wc.IOffersTableCore.resetCore, resetOffersTableCore: xyz.swapee.wc.IOffersTableCore.resetOffersTableCore }} */
xyz.swapee.wc.RecordIOffersTableCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.BoundIOffersTableCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCoreCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableOuterCore}
 */
xyz.swapee.wc.BoundIOffersTableCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.BoundOffersTableCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersTableCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTableCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IOffersTableCore): void} */
xyz.swapee.wc.IOffersTableCore._resetCore
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableCore.__resetCore} */
xyz.swapee.wc.IOffersTableCore.__resetCore

// nss:xyz.swapee.wc.IOffersTableCore,$$xyz.swapee.wc.IOffersTableCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.resetOffersTableCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTableCore.resetOffersTableCore
/** @typedef {function(this: xyz.swapee.wc.IOffersTableCore): void} */
xyz.swapee.wc.IOffersTableCore._resetOffersTableCore
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore} */
xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore

// nss:xyz.swapee.wc.IOffersTableCore,$$xyz.swapee.wc.IOffersTableCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink.changellyFixedLink exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @typedef {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink.changellyFixedLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink.changellyFloatLink exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @typedef {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink.changellyFloatLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model}
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink}
 * @extends {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink}
 */
xyz.swapee.wc.IOffersTableCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>}
 */
xyz.swapee.wc.IOffersTableController.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.IOffersTableProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
xyz.swapee.wc.IOffersTableProcessor.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.IOffersTableProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/** @interface */
xyz.swapee.wc.IOffersTableProcessorCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableProcessor} */
xyz.swapee.wc.IOffersTableProcessorCaster.prototype.asIOffersTableProcessor
/** @type {!xyz.swapee.wc.BoundOffersTableProcessor} */
xyz.swapee.wc.IOffersTableProcessorCaster.prototype.superOffersTableProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @interface */
xyz.swapee.wc.IOffersTableControllerFields
/** @type {!xyz.swapee.wc.IOffersTableController.Inputs} */
xyz.swapee.wc.IOffersTableControllerFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @interface */
xyz.swapee.wc.IOffersTableControllerCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableController} */
xyz.swapee.wc.IOffersTableControllerCaster.prototype.asIOffersTableController
/** @type {!xyz.swapee.wc.BoundIOffersTableProcessor} */
xyz.swapee.wc.IOffersTableControllerCaster.prototype.asIOffersTableProcessor
/** @type {!xyz.swapee.wc.BoundOffersTableController} */
xyz.swapee.wc.IOffersTableControllerCaster.prototype.superOffersTableController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
xyz.swapee.wc.IOffersTableController = function() {}
/** @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init */
xyz.swapee.wc.IOffersTableController.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IOffersTableController.prototype.resetPort = function() {}
/** @return {?} */
xyz.swapee.wc.IOffersTableController.prototype.resetTick = function() {}
/** @return {?} */
xyz.swapee.wc.IOffersTableController.prototype.reset = function() {}
/** @return {?} */
xyz.swapee.wc.IOffersTableController.prototype.onReset = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.IOffersTableProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableProcessorCaster}
 * @extends {xyz.swapee.wc.IOffersTableComputer}
 * @extends {xyz.swapee.wc.IOffersTableCore}
 * @extends {xyz.swapee.wc.IOffersTableController}
 */
xyz.swapee.wc.IOffersTableProcessor = function() {}
/** @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init */
xyz.swapee.wc.IOffersTableProcessor.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.OffersTableProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableProcessor.Initialese>}
 */
xyz.swapee.wc.OffersTableProcessor = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init */
xyz.swapee.wc.OffersTableProcessor.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.OffersTableProcessor.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.AbstractOffersTableProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableProcessor.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.OffersTableProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableProcessor, ...!xyz.swapee.wc.IOffersTableProcessor.Initialese)} */
xyz.swapee.wc.OffersTableProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.RecordIOffersTableProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.RecordIOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersTableController.resetPort, resetTick: xyz.swapee.wc.IOffersTableController.resetTick, reset: xyz.swapee.wc.IOffersTableController.reset, onReset: xyz.swapee.wc.IOffersTableController.onReset }} */
xyz.swapee.wc.RecordIOffersTableController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.BoundIOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableControllerFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
xyz.swapee.wc.BoundIOffersTableController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.BoundIOffersTableProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 * @extends {xyz.swapee.wc.BoundIOffersTableCore}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 */
xyz.swapee.wc.BoundIOffersTableProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml} xyz.swapee.wc.BoundOffersTableProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d8d8f17b9b6aef6e9a783202a9e0ae82 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/100-OffersTableMemory.xml} xyz.swapee.wc.OffersTableMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 304a20efb04f2f67bbf1424df82663ed */
/** @record */
xyz.swapee.wc.OffersTableMemory = function() {}
/** @type {number} */
xyz.swapee.wc.OffersTableMemory.prototype.untilReset
/** @type {string} */
xyz.swapee.wc.OffersTableMemory.prototype.changellyFixedLink
/** @type {string} */
xyz.swapee.wc.OffersTableMemory.prototype.changellyFloatLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/102-OffersTableInputs.xml} xyz.swapee.wc.front.OffersTableInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ac2360e06a97afadce45a4977b1b51b9 */
/** @record */
xyz.swapee.wc.front.OffersTableInputs = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.front.OffersTableInputs.prototype.untilReset

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTableEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @record */
xyz.swapee.wc.OffersTableEnv = function() {}
/** @type {xyz.swapee.wc.IOffersTable} */
xyz.swapee.wc.OffersTableEnv.prototype.offersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersTableProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
xyz.swapee.wc.IOffersTable.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTableFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @interface */
xyz.swapee.wc.IOffersTableFields
/** @type {!xyz.swapee.wc.IOffersTable.Pinout} */
xyz.swapee.wc.IOffersTableFields.prototype.pinout

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTableCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @interface */
xyz.swapee.wc.IOffersTableCaster
/** @type {!xyz.swapee.wc.BoundIOffersTable} */
xyz.swapee.wc.IOffersTableCaster.prototype.asIOffersTable
/** @type {!xyz.swapee.wc.BoundOffersTable} */
xyz.swapee.wc.IOffersTableCaster.prototype.superOffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCaster}
 * @extends {xyz.swapee.wc.IOffersTableProcessor}
 * @extends {xyz.swapee.wc.IOffersTableComputer}
 * @extends {xyz.swapee.wc.IOffersTableController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableLand>}
 */
xyz.swapee.wc.IOffersTable = function() {}
/** @param {...!xyz.swapee.wc.IOffersTable.Initialese} init */
xyz.swapee.wc.IOffersTable.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTable exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTable}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTable.Initialese>}
 */
xyz.swapee.wc.OffersTable = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTable.Initialese} init */
xyz.swapee.wc.OffersTable.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.OffersTable.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.AbstractOffersTable exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init
 * @extends {xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTable}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTable.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTableConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTable, ...!xyz.swapee.wc.IOffersTable.Initialese)} */
xyz.swapee.wc.OffersTableConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @record */
xyz.swapee.wc.IOffersTable.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IOffersTable.Pinout)|undefined} */
xyz.swapee.wc.IOffersTable.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IOffersTable.Pinout)|undefined} */
xyz.swapee.wc.IOffersTable.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IOffersTable.Pinout} */
xyz.swapee.wc.IOffersTable.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.OffersTableMemory)|undefined} */
xyz.swapee.wc.IOffersTable.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.OffersTableClasses)|undefined} */
xyz.swapee.wc.IOffersTable.MVCOptions.prototype.classes

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.RecordIOffersTable exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.BoundIOffersTable exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableFields}
 * @extends {xyz.swapee.wc.RecordIOffersTable}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableLand>}
 */
xyz.swapee.wc.BoundIOffersTable = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.BoundOffersTable exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTable}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTable = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTablePort.Inputs}
 */
xyz.swapee.wc.IOffersTableController.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTable.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableController.Inputs}
 */
xyz.swapee.wc.IOffersTable.Pinout = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.IOffersTableBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
xyz.swapee.wc.IOffersTableBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml} xyz.swapee.wc.OffersTableBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableBuffer}
 */
xyz.swapee.wc.OffersTableBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersTableDisplay.Initialese}
 */
xyz.swapee.wc.IOffersTableGPU.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.IOffersTableHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersTableController.Initialese}
 * @extends {xyz.swapee.wc.back.IOffersTableScreen.Initialese}
 * @extends {xyz.swapee.wc.IOffersTable.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableComputer.Initialese}
 */
xyz.swapee.wc.IOffersTableHtmlComponent.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.IOffersTableHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/** @interface */
xyz.swapee.wc.IOffersTableHtmlComponentCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableHtmlComponent} */
xyz.swapee.wc.IOffersTableHtmlComponentCaster.prototype.asIOffersTableHtmlComponent
/** @type {!xyz.swapee.wc.BoundOffersTableHtmlComponent} */
xyz.swapee.wc.IOffersTableHtmlComponentCaster.prototype.superOffersTableHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IOffersTableGPUFields
/** @type {!Object<string, string>} */
xyz.swapee.wc.IOffersTableGPUFields.prototype.vdusPQs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IOffersTableGPUCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableGPU} */
xyz.swapee.wc.IOffersTableGPUCaster.prototype.asIOffersTableGPU
/** @type {!xyz.swapee.wc.BoundOffersTableGPU} */
xyz.swapee.wc.IOffersTableGPUCaster.prototype.superOffersTableGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.IOffersTableGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!OffersTableMemory,.!OffersTableLand>}
 * @extends {xyz.swapee.wc.back.IOffersTableDisplay}
 */
xyz.swapee.wc.IOffersTableGPU = function() {}
/** @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init */
xyz.swapee.wc.IOffersTableGPU.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.IOffersTableHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IOffersTableController}
 * @extends {xyz.swapee.wc.back.IOffersTableScreen}
 * @extends {xyz.swapee.wc.IOffersTable}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.IOffersTableGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.IOffersTableProcessor}
 * @extends {xyz.swapee.wc.IOffersTableComputer}
 */
xyz.swapee.wc.IOffersTableHtmlComponent = function() {}
/** @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init */
xyz.swapee.wc.IOffersTableHtmlComponent.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.OffersTableHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese>}
 */
xyz.swapee.wc.OffersTableHtmlComponent = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init */
xyz.swapee.wc.OffersTableHtmlComponent.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.OffersTableHtmlComponent.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.AbstractOffersTableHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.OffersTableHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableHtmlComponent, ...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese)} */
xyz.swapee.wc.OffersTableHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.IOffersTableHtmlComponentUtilFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @interface */
xyz.swapee.wc.IOffersTableHtmlComponentUtilFields
/** @type {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet} */
xyz.swapee.wc.IOffersTableHtmlComponentUtilFields.prototype.RouterNet
/** @type {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts} */
xyz.swapee.wc.IOffersTableHtmlComponentUtilFields.prototype.RouterPorts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.IOffersTableHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableHtmlComponentUtilFields}
 */
xyz.swapee.wc.IOffersTableHtmlComponentUtil = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts} [ports]
 * @return {?}
 */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.prototype.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.OffersTableHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableHtmlComponentUtil}
 */
xyz.swapee.wc.OffersTableHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.RecordIOffersTableHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @typedef {{ router: xyz.swapee.wc.IOffersTableHtmlComponentUtil.router }} */
xyz.swapee.wc.RecordIOffersTableHtmlComponentUtil

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.BoundIOffersTableHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableHtmlComponentUtilFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableHtmlComponentUtil}
 */
xyz.swapee.wc.BoundIOffersTableHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.BoundOffersTableHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableHtmlComponentUtil}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.IOffersTableHtmlComponentUtil.router exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts} [ports]
 */
$$xyz.swapee.wc.IOffersTableHtmlComponentUtil.__router = function(net, cores, ports) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.router
/** @typedef {function(this: xyz.swapee.wc.IOffersTableHtmlComponentUtil, !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil._router
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableHtmlComponentUtil.__router} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.__router

// nss:xyz.swapee.wc.IOffersTableHtmlComponentUtil,$$xyz.swapee.wc.IOffersTableHtmlComponentUtil,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet = function() {}
/** @type {typeof xyz.swapee.wc.IExchangeIntentPort} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet.prototype.ExchangeIntent
/** @type {typeof xyz.swapee.wc.IOffersAggregatorPort} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet.prototype.OffersAggregator
/** @type {typeof com.webcircuits.ui.ICollapsarPort} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet.prototype.ProgressCollapsar
/** @type {typeof xyz.swapee.wc.ICryptoSelectPort} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet.prototype.CryptoSelectOut
/** @type {typeof xyz.swapee.wc.IOffersTablePort} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet.prototype.OffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores = function() {}
/** @type {!xyz.swapee.wc.ExchangeIntentMemory} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.OffersAggregatorMemory} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores.prototype.OffersAggregator
/** @type {!com.webcircuits.ui.CollapsarMemory} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores.prototype.ProgressCollapsar
/** @type {!xyz.swapee.wc.CryptoSelectMemory} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores.prototype.CryptoSelectOut
/** @type {!xyz.swapee.wc.OffersTableMemory} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores.prototype.OffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml} xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts = function() {}
/** @type {!xyz.swapee.wc.IExchangeIntent.Pinout} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.IOffersAggregator.Pinout} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts.prototype.OffersAggregator
/** @type {!com.webcircuits.ui.ICollapsar.Pinout} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts.prototype.ProgressCollapsar
/** @type {!xyz.swapee.wc.ICryptoSelect.Pinout} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts.prototype.CryptoSelectOut
/** @type {!xyz.swapee.wc.IOffersTable.Pinout} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts.prototype.OffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.RecordIOffersTableHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.RecordIOffersTableGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.BoundIOffersTableGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableGPUFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!OffersTableMemory,.!OffersTableLand>}
 * @extends {xyz.swapee.wc.back.BoundIOffersTableDisplay}
 */
xyz.swapee.wc.BoundIOffersTableGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.BoundIOffersTableHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIOffersTableController}
 * @extends {xyz.swapee.wc.back.BoundIOffersTableScreen}
 * @extends {xyz.swapee.wc.BoundIOffersTable}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.BoundIOffersTableGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersTableLand>}
 * @extends {xyz.swapee.wc.BoundIOffersTableProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersTableComputer}
 */
xyz.swapee.wc.BoundIOffersTableHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml} xyz.swapee.wc.BoundOffersTableHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65cdde4cab8860325f1f333130b5f2a2 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @interface */
xyz.swapee.wc.IOffersTableDesigner = function() {}
/**
 * @param {xyz.swapee.wc.OffersTableClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IOffersTableDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.OffersTableClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IOffersTableDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh} mesh
 * @return {?}
 */
xyz.swapee.wc.IOffersTableDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IOffersTableDesigner.relay.MemPool} memPool
 * @return {?}
 */
xyz.swapee.wc.IOffersTableDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.OffersTableClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IOffersTableDesigner.prototype.lendClasses = function(classes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.OffersTableDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableDesigner}
 */
xyz.swapee.wc.OffersTableDesigner = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @record */
xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.OffersAggregator
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.ProgressCollapsar
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.CryptoSelectOut
/** @type {typeof xyz.swapee.wc.IOffersTableController} */
xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh.prototype.OffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @record */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.OffersAggregator
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.ProgressCollapsar
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.CryptoSelectOut
/** @type {typeof xyz.swapee.wc.IOffersTableController} */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.OffersTable
/** @type {typeof xyz.swapee.wc.IOffersTableController} */
xyz.swapee.wc.IOffersTableDesigner.relay.Mesh.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml} xyz.swapee.wc.IOffersTableDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 60eab01978215efd244238c0992912fe */
/** @record */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.OffersAggregator
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.ProgressCollapsar
/** @type {!Object} */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.CryptoSelectOut
/** @type {!xyz.swapee.wc.OffersTableMemory} */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.OffersTable
/** @type {!xyz.swapee.wc.OffersTableMemory} */
xyz.swapee.wc.IOffersTableDesigner.relay.MemPool.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/200-OffersTableLand.xml} xyz.swapee.wc.OffersTableLand exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c809f8949abfadf614845ace79826c84 */
/** @record */
xyz.swapee.wc.OffersTableLand = function() {}
/** @type {!Object} */
xyz.swapee.wc.OffersTableLand.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.OffersTableLand.prototype.OffersAggregator
/** @type {!Object} */
xyz.swapee.wc.OffersTableLand.prototype.ProgressCollapsar
/** @type {!Object} */
xyz.swapee.wc.OffersTableLand.prototype.CryptoSelectOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings>}
 */
xyz.swapee.wc.IOffersTableDisplay.Initialese = function() {}
/** @type {(!Array<!HTMLElement>)|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.CoinImWrs
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLAnchorElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFloatLink
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOfferAmount
/** @type {HTMLAnchorElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangellyFixedLink
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOfferAmount
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOfferAmount
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOfferAmount
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOfferAmount
/** @type {(!Array<!HTMLSpanElement>)|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.OfferCryptoOuts
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ResetIn
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ResetNowBu
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ExchangesLoaded
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ExchangesTotal
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ProgressCollapsar
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.CryptoSelectOut
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.ExchangeIntent
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Initialese.prototype.OffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @interface */
xyz.swapee.wc.IOffersTableDisplayFields
/** @type {!xyz.swapee.wc.IOffersTableDisplay.Settings} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IOffersTableDisplay.Queries} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.queries
/** @type {!Array<!HTMLElement>} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.CoinImWrs
/** @type {HTMLDivElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLAnchorElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatLink
/** @type {HTMLDivElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedOfferAmount
/** @type {HTMLAnchorElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedLink
/** @type {HTMLDivElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOfferAmount
/** @type {HTMLDivElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFixedOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFixedOfferAmount
/** @type {HTMLDivElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFloatingOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFloatingOfferAmount
/** @type {HTMLDivElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFixedOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFixedOfferAmount
/** @type {!Array<!HTMLSpanElement>} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.OfferCryptoOuts
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ResetIn
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ResetNowBu
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangesLoaded
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangesTotal
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ProgressCollapsar
/** @type {HTMLSpanElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.CryptoSelectOut
/** @type {HTMLElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangeIntent
/** @type {HTMLElement} */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.OffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @interface */
xyz.swapee.wc.IOffersTableDisplayCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableDisplay} */
xyz.swapee.wc.IOffersTableDisplayCaster.prototype.asIOffersTableDisplay
/** @type {!xyz.swapee.wc.BoundIOffersTableScreen} */
xyz.swapee.wc.IOffersTableDisplayCaster.prototype.asIOffersTableScreen
/** @type {!xyz.swapee.wc.BoundOffersTableDisplay} */
xyz.swapee.wc.IOffersTableDisplayCaster.prototype.superOffersTableDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersTableDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.OffersTableMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, xyz.swapee.wc.IOffersTableDisplay.Queries, null>}
 */
xyz.swapee.wc.IOffersTableDisplay = function() {}
/** @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init */
xyz.swapee.wc.IOffersTableDisplay.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {null} land
 * @return {void}
 */
xyz.swapee.wc.IOffersTableDisplay.prototype.paint = function(memory, land) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} memory
 * @param {{ ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }} land
 * @return {?}
 */
xyz.swapee.wc.IOffersTableDisplay.prototype.paintTableOrder = function(memory, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.OffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableDisplay.Initialese>}
 */
xyz.swapee.wc.OffersTableDisplay = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init */
xyz.swapee.wc.OffersTableDisplay.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.OffersTableDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.AbstractOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.OffersTableDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableDisplay, ...!xyz.swapee.wc.IOffersTableDisplay.Initialese)} */
xyz.swapee.wc.OffersTableDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.OffersTableGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableGPU.Initialese>}
 */
xyz.swapee.wc.OffersTableGPU = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init */
xyz.swapee.wc.OffersTableGPU.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.OffersTableGPU.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.AbstractOffersTableGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableGPU.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.OffersTableGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableGPU, ...!xyz.swapee.wc.IOffersTableGPU.Initialese)} */
xyz.swapee.wc.OffersTableGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml} xyz.swapee.wc.BoundOffersTableGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.RecordIOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @typedef {{ paint: xyz.swapee.wc.IOffersTableDisplay.paint, paintTableOrder: xyz.swapee.wc.IOffersTableDisplay.paintTableOrder }} */
xyz.swapee.wc.RecordIOffersTableDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.BoundIOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableDisplayFields}
 * @extends {xyz.swapee.wc.RecordIOffersTableDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.OffersTableMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, xyz.swapee.wc.IOffersTableDisplay.Queries, null>}
 */
xyz.swapee.wc.BoundIOffersTableDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.BoundOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} memory
 * @param {null} land
 * @return {void}
 */
$$xyz.swapee.wc.IOffersTableDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersTableMemory, null): void} */
xyz.swapee.wc.IOffersTableDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IOffersTableDisplay, !xyz.swapee.wc.OffersTableMemory, null): void} */
xyz.swapee.wc.IOffersTableDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableDisplay.__paint} */
xyz.swapee.wc.IOffersTableDisplay.__paint

// nss:xyz.swapee.wc.IOffersTableDisplay,$$xyz.swapee.wc.IOffersTableDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.paintTableOrder exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} memory
 * @param {{ ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }} land
 */
$$xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory, { ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } })} */
xyz.swapee.wc.IOffersTableDisplay.paintTableOrder
/** @typedef {function(this: xyz.swapee.wc.IOffersTableDisplay, !xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory, { ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } })} */
xyz.swapee.wc.IOffersTableDisplay._paintTableOrder
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder} */
xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder

// nss:xyz.swapee.wc.IOffersTableDisplay,$$xyz.swapee.wc.IOffersTableDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @record */
xyz.swapee.wc.IOffersTableDisplay.Queries = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.resetInSel
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.resetNowBuSel
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.exchangesLoadedSel
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.exchangesTotalSel
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.progressCollapsarSel
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.cryptoSelectOutSel
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.exchangeIntentSel
/** @type {string|undefined} */
xyz.swapee.wc.IOffersTableDisplay.Queries.prototype.offersAggregatorSel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableDisplay.Queries}
 */
xyz.swapee.wc.IOffersTableDisplay.Settings = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableQueriesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersTableQueriesPQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.cryptoSelectedImWrSel
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.resetInSel
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.resetNowBuSel
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.exchangesLoadedSel
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.exchangesTotalSel
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.progressCollapsarSel
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.exchangeIntentSel
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesPQs.prototype.offersAggregatorSel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableQueriesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersTableQueriesQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.dc6e2
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.ac854
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.be273
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.d5cac
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.f6836
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.b9642
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.b3da4
/** @type {string} */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.cde34

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml} xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8144168476af2d45f0214267fcf8f931 */
/** @record */
xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OffersTableClasses>}
 */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese = function() {}
/** @type {(!Array<!com.webcircuits.IHtmlTwin>)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.CoinImWrs
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFloatLink
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFixedOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangellyFixedLink
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.LetsExchangeFixedOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ChangeNowFixedOfferAmount
/** @type {(!Array<!com.webcircuits.IHtmlTwin>)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.OfferCryptoOuts
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ResetIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ResetNowBu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ExchangesLoaded
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ExchangesTotal
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ProgressCollapsar
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.CryptoSelectOut
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.ExchangeIntent
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersTableDisplay.Initialese.prototype.OffersAggregator

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/** @interface */
xyz.swapee.wc.back.IOffersTableDisplayFields
/** @type {!Array<!com.webcircuits.IHtmlTwin>} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.CoinImWrs
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatLink
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedLink
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFixedOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFixedOfferAmount
/** @type {!Array<!com.webcircuits.IHtmlTwin>} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.OfferCryptoOuts
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ResetIn
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ResetNowBu
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangesLoaded
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangesTotal
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ProgressCollapsar
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.CryptoSelectOut
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangeIntent
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.OffersAggregator

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/** @interface */
xyz.swapee.wc.back.IOffersTableDisplayCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersTableDisplay} */
xyz.swapee.wc.back.IOffersTableDisplayCaster.prototype.asIOffersTableDisplay
/** @type {!xyz.swapee.wc.back.BoundOffersTableDisplay} */
xyz.swapee.wc.back.IOffersTableDisplayCaster.prototype.superOffersTableDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.OffersTableClasses, !xyz.swapee.wc.OffersTableLand>}
 */
xyz.swapee.wc.back.IOffersTableDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OffersTableMemory} [memory]
 * @param {!xyz.swapee.wc.OffersTableLand} [land]
 * @return {void}
 */
xyz.swapee.wc.back.IOffersTableDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.OffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IOffersTableDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableDisplay.Initialese>}
 */
xyz.swapee.wc.back.OffersTableDisplay = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.OffersTableDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.AbstractOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay = function() {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersTableVdusPQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ResetIn
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ResetNowBu
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.CoinIms
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.CryptoSelectedImWr
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFloatingOfferAmount
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFixedOfferAmount
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFloatingOfferAmount
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.LetsExchangeFixedOfferAmount
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFloatingOfferAmount
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangeNowFixedOfferAmount
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.OfferCryptoOuts
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ExchangesLoaded
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ExchangesTotal
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ProgressCollapsar
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ExchangeIntent
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.OffersAggregator
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.ChangellyFloatingDealBroker
/** @type {string} */
xyz.swapee.wc.OffersTableVdusPQs.prototype.CryptoSelectOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersTableVdusQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8741
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8742
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8743
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8744
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8745
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8746
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8747
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8748
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8749
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87410
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87411
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87412
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87413
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87414
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87415
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87416
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87417
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87418
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87419
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87420
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87421
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87422
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87423
/** @type {string} */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87424

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.RecordIOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/** @typedef {{ paint: xyz.swapee.wc.back.IOffersTableDisplay.paint }} */
xyz.swapee.wc.back.RecordIOffersTableDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.BoundIOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIOffersTableDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.OffersTableClasses, !xyz.swapee.wc.OffersTableLand>}
 */
xyz.swapee.wc.back.BoundIOffersTableDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.BoundOffersTableDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersTableDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml} xyz.swapee.wc.back.IOffersTableDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersTableMemory} [memory]
 * @param {!xyz.swapee.wc.OffersTableLand} [land]
 * @return {void}
 */
$$xyz.swapee.wc.back.IOffersTableDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersTableMemory=, !xyz.swapee.wc.OffersTableLand=): void} */
xyz.swapee.wc.back.IOffersTableDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IOffersTableDisplay, !xyz.swapee.wc.OffersTableMemory=, !xyz.swapee.wc.OffersTableLand=): void} */
xyz.swapee.wc.back.IOffersTableDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.back.IOffersTableDisplay.__paint} */
xyz.swapee.wc.back.IOffersTableDisplay.__paint

// nss:xyz.swapee.wc.back.IOffersTableDisplay,$$xyz.swapee.wc.back.IOffersTableDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableClassesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersTableClassesPQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableClassesPQs.prototype.ColHeading
/** @type {string} */
xyz.swapee.wc.OffersTableClassesPQs.prototype.Loading
/** @type {string} */
xyz.swapee.wc.OffersTableClassesPQs.prototype.Hidden
/** @type {string} */
xyz.swapee.wc.OffersTableClassesPQs.prototype.SelectedRateType
/** @type {string} */
xyz.swapee.wc.OffersTableClassesPQs.prototype.BestOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml} xyz.swapee.wc.OffersTableClassesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersTableClassesQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersTableClassesQPs.prototype.b6cc2
/** @type {string} */
xyz.swapee.wc.OffersTableClassesQPs.prototype.b6bfb
/** @type {string} */
xyz.swapee.wc.OffersTableClassesQPs.prototype.hacdf
/** @type {string} */
xyz.swapee.wc.OffersTableClassesQPs.prototype.aaa87
/** @type {string} */
xyz.swapee.wc.OffersTableClassesQPs.prototype.e8977

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/41-OffersTableClasses.xml} xyz.swapee.wc.OffersTableClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props dd863b71dc37de8f0b54845d922301e8 */
/** @record */
xyz.swapee.wc.OffersTableClasses = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.OffersTableClasses.prototype.ColHeading
/** @type {string|undefined} */
xyz.swapee.wc.OffersTableClasses.prototype.Loading
/** @type {string|undefined} */
xyz.swapee.wc.OffersTableClasses.prototype.Hidden
/** @type {string|undefined} */
xyz.swapee.wc.OffersTableClasses.prototype.SelectedRateType
/** @type {string|undefined} */
xyz.swapee.wc.OffersTableClasses.prototype.BestOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableController.Initialese>}
 */
xyz.swapee.wc.OffersTableController = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init */
xyz.swapee.wc.OffersTableController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.OffersTableController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.AbstractOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableController, ...!xyz.swapee.wc.IOffersTableController.Initialese)} */
xyz.swapee.wc.OffersTableControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/** @interface */
xyz.swapee.wc.IOffersTableControllerHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IOffersTableController._reset|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableController._reset>)} */
xyz.swapee.wc.IOffersTableControllerHyperslice.prototype.reset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableControllerHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableControllerHyperslice}
 */
xyz.swapee.wc.OffersTableControllerHyperslice = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableControllerBindingHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.wc.IOffersTableControllerBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IOffersTableController.__reset<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableController.__reset<THIS>>)} */
xyz.swapee.wc.IOffersTableControllerBindingHyperslice.prototype.reset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.OffersTableControllerBindingHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableControllerBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.wc.OffersTableControllerBindingHyperslice = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.BoundOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersTableController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersTableController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController): void} */
xyz.swapee.wc.IOffersTableController._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableController.__resetPort} */
xyz.swapee.wc.IOffersTableController.__resetPort

// nss:xyz.swapee.wc.IOffersTableController,$$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.resetTick exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.IOffersTableController.__resetTick = function() {}
/** @typedef {function()} */
xyz.swapee.wc.IOffersTableController.resetTick
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController)} */
xyz.swapee.wc.IOffersTableController._resetTick
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableController.__resetTick} */
xyz.swapee.wc.IOffersTableController.__resetTick

// nss:xyz.swapee.wc.IOffersTableController,$$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.reset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.IOffersTableController.__reset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.IOffersTableController.reset
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController)} */
xyz.swapee.wc.IOffersTableController._reset
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableController.__reset} */
xyz.swapee.wc.IOffersTableController.__reset

// nss:xyz.swapee.wc.IOffersTableController,$$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.onReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.IOffersTableController.__onReset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.IOffersTableController.onReset
/** @typedef {function(this: xyz.swapee.wc.IOffersTableController)} */
xyz.swapee.wc.IOffersTableController._onReset
/** @typedef {typeof $$xyz.swapee.wc.IOffersTableController.__onReset} */
xyz.swapee.wc.IOffersTableController.__onReset

// nss:xyz.swapee.wc.IOffersTableController,$$xyz.swapee.wc.IOffersTableController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml} xyz.swapee.wc.IOffersTableController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d32ed6284e0a680c3bad109c94482a26 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTablePort.WeakInputs}
 */
xyz.swapee.wc.IOffersTableController.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @record */
xyz.swapee.wc.front.IOffersTableController.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @interface */
xyz.swapee.wc.front.IOffersTableControllerCaster
/** @type {!xyz.swapee.wc.front.BoundIOffersTableController} */
xyz.swapee.wc.front.IOffersTableControllerCaster.prototype.asIOffersTableController
/** @type {!xyz.swapee.wc.front.BoundOffersTableController} */
xyz.swapee.wc.front.IOffersTableControllerCaster.prototype.superOffersTableController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.IOffersTableControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/** @interface */
xyz.swapee.wc.front.IOffersTableControllerATCaster
/** @type {!xyz.swapee.wc.front.BoundIOffersTableControllerAT} */
xyz.swapee.wc.front.IOffersTableControllerATCaster.prototype.asIOffersTableControllerAT
/** @type {!xyz.swapee.wc.front.BoundOffersTableControllerAT} */
xyz.swapee.wc.front.IOffersTableControllerATCaster.prototype.superOffersTableControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.IOffersTableControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.front.IOffersTableControllerAT = function() {}
/** @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init */
xyz.swapee.wc.front.IOffersTableControllerAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerAT}
 */
xyz.swapee.wc.front.IOffersTableController = function() {}
/** @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init */
xyz.swapee.wc.front.IOffersTableController.prototype.constructor = function(...init) {}
/** @return {?} */
xyz.swapee.wc.front.IOffersTableController.prototype.resetTick = function() {}
/** @return {?} */
xyz.swapee.wc.front.IOffersTableController.prototype.reset = function() {}
/** @return {?} */
xyz.swapee.wc.front.IOffersTableController.prototype.onReset = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersTableController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableController.Initialese>}
 */
xyz.swapee.wc.front.OffersTableController = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init */
xyz.swapee.wc.front.OffersTableController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.OffersTableController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.AbstractOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice)} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableController, ...!xyz.swapee.wc.front.IOffersTableController.Initialese)} */
xyz.swapee.wc.front.OffersTableControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableControllerHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @interface */
xyz.swapee.wc.front.IOffersTableControllerHyperslice = function() {}
/** @type {(!xyz.swapee.wc.front.IOffersTableController._reset|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersTableController._reset>)} */
xyz.swapee.wc.front.IOffersTableControllerHyperslice.prototype.reset

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableControllerHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.front.IOffersTableControllerHyperslice}
 */
xyz.swapee.wc.front.OffersTableControllerHyperslice = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.front.IOffersTableController.__reset<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersTableController.__reset<THIS>>)} */
xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice.prototype.reset

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.OffersTableControllerBindingHyperslice exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.wc.front.OffersTableControllerBindingHyperslice = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.RecordIOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/** @typedef {{ resetTick: xyz.swapee.wc.front.IOffersTableController.resetTick, reset: xyz.swapee.wc.front.IOffersTableController.reset, onReset: xyz.swapee.wc.front.IOffersTableController.onReset }} */
xyz.swapee.wc.front.RecordIOffersTableController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.RecordIOffersTableControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOffersTableControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.BoundIOffersTableControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersTableControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.front.BoundIOffersTableControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.BoundIOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersTableController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIOffersTableControllerAT}
 */
xyz.swapee.wc.front.BoundIOffersTableController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.BoundOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersTableController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundOffersTableController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.resetTick exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.front.IOffersTableController.__resetTick = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.IOffersTableController.resetTick
/** @typedef {function(this: xyz.swapee.wc.front.IOffersTableController)} */
xyz.swapee.wc.front.IOffersTableController._resetTick
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersTableController.__resetTick} */
xyz.swapee.wc.front.IOffersTableController.__resetTick

// nss:xyz.swapee.wc.front.IOffersTableController,$$xyz.swapee.wc.front.IOffersTableController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.reset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.front.IOffersTableController.__reset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.IOffersTableController.reset
/** @typedef {function(this: xyz.swapee.wc.front.IOffersTableController)} */
xyz.swapee.wc.front.IOffersTableController._reset
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersTableController.__reset} */
xyz.swapee.wc.front.IOffersTableController.__reset

// nss:xyz.swapee.wc.front.IOffersTableController,$$xyz.swapee.wc.front.IOffersTableController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml} xyz.swapee.wc.front.IOffersTableController.onReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f3c5227a2699a2d38393fe0b6e2bde92 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.front.IOffersTableController.__onReset = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.IOffersTableController.onReset
/** @typedef {function(this: xyz.swapee.wc.front.IOffersTableController)} */
xyz.swapee.wc.front.IOffersTableController._onReset
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersTableController.__onReset} */
xyz.swapee.wc.front.IOffersTableController.__onReset

// nss:xyz.swapee.wc.front.IOffersTableController,$$xyz.swapee.wc.front.IOffersTableController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.IOffersTableController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
xyz.swapee.wc.back.IOffersTableController.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.IOffersTableControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/** @interface */
xyz.swapee.wc.back.IOffersTableControllerCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersTableController} */
xyz.swapee.wc.back.IOffersTableControllerCaster.prototype.asIOffersTableController
/** @type {!xyz.swapee.wc.back.BoundOffersTableController} */
xyz.swapee.wc.back.IOffersTableControllerCaster.prototype.superOffersTableController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.IOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.IOffersTableController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
xyz.swapee.wc.back.IOffersTableController = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init */
xyz.swapee.wc.back.IOffersTableController.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.OffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersTableController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableController.Initialese>}
 */
xyz.swapee.wc.back.OffersTableController = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init */
xyz.swapee.wc.back.OffersTableController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.OffersTableController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.AbstractOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.OffersTableControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableController, ...!xyz.swapee.wc.back.IOffersTableController.Initialese)} */
xyz.swapee.wc.back.OffersTableControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.RecordIOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersTableController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.BoundIOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersTableController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerCaster}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IOffersTableController.Inputs>}
 */
xyz.swapee.wc.back.BoundIOffersTableController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml} xyz.swapee.wc.back.BoundOffersTableController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8296dff8d5731ec958cd53d64248b967 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersTableController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.IOffersTableControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableController.Initialese}
 */
xyz.swapee.wc.back.IOffersTableControllerAR.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.IOffersTableControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/** @interface */
xyz.swapee.wc.back.IOffersTableControllerARCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersTableControllerAR} */
xyz.swapee.wc.back.IOffersTableControllerARCaster.prototype.asIOffersTableControllerAR
/** @type {!xyz.swapee.wc.back.BoundOffersTableControllerAR} */
xyz.swapee.wc.back.IOffersTableControllerARCaster.prototype.superOffersTableControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.IOffersTableControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOffersTableController}
 */
xyz.swapee.wc.back.IOffersTableControllerAR = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init */
xyz.swapee.wc.back.IOffersTableControllerAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.OffersTableControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersTableControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese>}
 */
xyz.swapee.wc.back.OffersTableControllerAR = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init */
xyz.swapee.wc.back.OffersTableControllerAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.OffersTableControllerAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.AbstractOffersTableControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.OffersTableControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableControllerAR, ...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese)} */
xyz.swapee.wc.back.OffersTableControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.RecordIOffersTableControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersTableControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.BoundIOffersTableControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersTableControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOffersTableController}
 */
xyz.swapee.wc.back.BoundIOffersTableControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml} xyz.swapee.wc.back.BoundOffersTableControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1623fb2bac9bea312e03af3f20b5bef */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersTableControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.IOffersTableControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.front.IOffersTableControllerAT.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.OffersTableControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersTableControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese>}
 */
xyz.swapee.wc.front.OffersTableControllerAT = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init */
xyz.swapee.wc.front.OffersTableControllerAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.OffersTableControllerAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.AbstractOffersTableControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.OffersTableControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableControllerAT, ...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese)} */
xyz.swapee.wc.front.OffersTableControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml} xyz.swapee.wc.front.BoundOffersTableControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a65c4249c246efc51bd7f02216a3700 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersTableControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundOffersTableControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.IOffersTableScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.front.OffersTableInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, !xyz.swapee.wc.IOffersTableDisplay.Queries, !xyz.swapee.wc.OffersTableClasses>}
 * @extends {xyz.swapee.wc.IOffersTableDisplay.Initialese}
 */
xyz.swapee.wc.IOffersTableScreen.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.IOffersTableScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/** @interface */
xyz.swapee.wc.IOffersTableScreenCaster
/** @type {!xyz.swapee.wc.BoundIOffersTableScreen} */
xyz.swapee.wc.IOffersTableScreenCaster.prototype.asIOffersTableScreen
/** @type {!xyz.swapee.wc.BoundOffersTableScreen} */
xyz.swapee.wc.IOffersTableScreenCaster.prototype.superOffersTableScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.IOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.front.OffersTableInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, !xyz.swapee.wc.IOffersTableDisplay.Queries, null, !xyz.swapee.wc.OffersTableClasses>}
 * @extends {xyz.swapee.wc.front.IOffersTableController}
 * @extends {xyz.swapee.wc.IOffersTableDisplay}
 */
xyz.swapee.wc.IOffersTableScreen = function() {}
/** @param {...!xyz.swapee.wc.IOffersTableScreen.Initialese} init */
xyz.swapee.wc.IOffersTableScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.OffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableScreen.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableScreen.Initialese>}
 */
xyz.swapee.wc.OffersTableScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersTableScreen.Initialese} init */
xyz.swapee.wc.OffersTableScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.OffersTableScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.AbstractOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableScreen.Initialese} init
 * @extends {xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.OffersTableScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableScreen, ...!xyz.swapee.wc.IOffersTableScreen.Initialese)} */
xyz.swapee.wc.OffersTableScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.RecordIOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersTableScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.BoundIOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.front.OffersTableInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, !xyz.swapee.wc.IOffersTableDisplay.Queries, null, !xyz.swapee.wc.OffersTableClasses>}
 * @extends {xyz.swapee.wc.front.BoundIOffersTableController}
 * @extends {xyz.swapee.wc.BoundIOffersTableDisplay}
 */
xyz.swapee.wc.BoundIOffersTableScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml} xyz.swapee.wc.BoundOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8421f50c143dcc151edfa185b8391171 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersTableScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.IOffersTableScreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.back.IOffersTableScreenAT.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.IOffersTableScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersTableScreenAT.Initialese}
 */
xyz.swapee.wc.back.IOffersTableScreen.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.IOffersTableScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/** @interface */
xyz.swapee.wc.back.IOffersTableScreenCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersTableScreen} */
xyz.swapee.wc.back.IOffersTableScreenCaster.prototype.asIOffersTableScreen
/** @type {!xyz.swapee.wc.back.BoundOffersTableScreen} */
xyz.swapee.wc.back.IOffersTableScreenCaster.prototype.superOffersTableScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.IOffersTableScreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/** @interface */
xyz.swapee.wc.back.IOffersTableScreenATCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersTableScreenAT} */
xyz.swapee.wc.back.IOffersTableScreenATCaster.prototype.asIOffersTableScreenAT
/** @type {!xyz.swapee.wc.back.BoundOffersTableScreenAT} */
xyz.swapee.wc.back.IOffersTableScreenATCaster.prototype.superOffersTableScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.IOffersTableScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.back.IOffersTableScreenAT = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} init */
xyz.swapee.wc.back.IOffersTableScreenAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.IOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableScreenCaster}
 * @extends {xyz.swapee.wc.back.IOffersTableScreenAT}
 */
xyz.swapee.wc.back.IOffersTableScreen = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersTableScreen.Initialese} init */
xyz.swapee.wc.back.IOffersTableScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.OffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersTableScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableScreen.Initialese>}
 */
xyz.swapee.wc.back.OffersTableScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersTableScreen.Initialese} init */
xyz.swapee.wc.back.OffersTableScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.OffersTableScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.AbstractOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.OffersTableScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableScreen, ...!xyz.swapee.wc.back.IOffersTableScreen.Initialese)} */
xyz.swapee.wc.back.OffersTableScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.RecordIOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersTableScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.RecordIOffersTableScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersTableScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.BoundIOffersTableScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersTableScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.back.BoundIOffersTableScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.BoundIOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersTableScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersTableScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIOffersTableScreenAT}
 */
xyz.swapee.wc.back.BoundIOffersTableScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml} xyz.swapee.wc.back.BoundOffersTableScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e7722121af84b76d55c212d5c38c0f1 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersTableScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.IOffersTableScreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOffersTableScreen.Initialese}
 */
xyz.swapee.wc.front.IOffersTableScreenAR.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.IOffersTableScreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/** @interface */
xyz.swapee.wc.front.IOffersTableScreenARCaster
/** @type {!xyz.swapee.wc.front.BoundIOffersTableScreenAR} */
xyz.swapee.wc.front.IOffersTableScreenARCaster.prototype.asIOffersTableScreenAR
/** @type {!xyz.swapee.wc.front.BoundOffersTableScreenAR} */
xyz.swapee.wc.front.IOffersTableScreenARCaster.prototype.superOffersTableScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.IOffersTableScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOffersTableScreen}
 */
xyz.swapee.wc.front.IOffersTableScreenAR = function() {}
/** @param {...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese} init */
xyz.swapee.wc.front.IOffersTableScreenAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.OffersTableScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersTableScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese>}
 */
xyz.swapee.wc.front.OffersTableScreenAR = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese} init */
xyz.swapee.wc.front.OffersTableScreenAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.OffersTableScreenAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.AbstractOffersTableScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableScreenAR|typeof xyz.swapee.wc.front.OffersTableScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableScreenAR|typeof xyz.swapee.wc.front.OffersTableScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersTableScreenAR|typeof xyz.swapee.wc.front.OffersTableScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.OffersTableScreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableScreenAR, ...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese)} */
xyz.swapee.wc.front.OffersTableScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.RecordIOffersTableScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOffersTableScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.BoundIOffersTableScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersTableScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersTableScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOffersTableScreen}
 */
xyz.swapee.wc.front.BoundIOffersTableScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml} xyz.swapee.wc.front.BoundOffersTableScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66abb7501297996b304e4b54c34b4e14 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersTableScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundOffersTableScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.OffersTableScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersTableScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese>}
 */
xyz.swapee.wc.back.OffersTableScreenAT = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} init */
xyz.swapee.wc.back.OffersTableScreenAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.OffersTableScreenAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.AbstractOffersTableScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.OffersTableScreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableScreenAT, ...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese)} */
xyz.swapee.wc.back.OffersTableScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml} xyz.swapee.wc.back.BoundOffersTableScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7f5bb34b512c051d213d57888e91d38 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersTableScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersTableScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe.prototype.untilReset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/** @record */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe.prototype.untilReset

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset}
 */
xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe}
 */
xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset}
 */
xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe}
 */
xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe.prototype.changellyFixedLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @record */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe.prototype.changellyFloatLink

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableCore.Model.UntilReset exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset}
 */
xyz.swapee.wc.IOffersTableCore.Model.UntilReset = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml} xyz.swapee.wc.IOffersTableCore.Model.UntilReset_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3ee3d2cf7e19df9ac131979745548307 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe}
 */
xyz.swapee.wc.IOffersTableCore.Model.UntilReset_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */