/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IOffersTableComputer={}
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink={}
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink={}
xyz.swapee.wc.IOffersTableComputer.compute={}
xyz.swapee.wc.IOffersTableOuterCore={}
xyz.swapee.wc.IOffersTableOuterCore.Model={}
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset={}
xyz.swapee.wc.IOffersTableOuterCore.WeakModel={}
xyz.swapee.wc.IOffersTablePort={}
xyz.swapee.wc.IOffersTablePort.Inputs={}
xyz.swapee.wc.IOffersTablePort.WeakInputs={}
xyz.swapee.wc.IOffersTableCore={}
xyz.swapee.wc.IOffersTableCore.Model={}
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink={}
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink={}
xyz.swapee.wc.IOffersTablePortInterface={}
xyz.swapee.wc.IOffersTableProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IOffersTableController={}
xyz.swapee.wc.front.IOffersTableControllerAT={}
xyz.swapee.wc.front.IOffersTableScreenAR={}
xyz.swapee.wc.IOffersTable={}
xyz.swapee.wc.IOffersTableHtmlComponentUtil={}
xyz.swapee.wc.IOffersTableHtmlComponent={}
xyz.swapee.wc.IOffersTableElement={}
xyz.swapee.wc.IOffersTableElement.build={}
xyz.swapee.wc.IOffersTableElement.short={}
xyz.swapee.wc.IOffersTableElementPort={}
xyz.swapee.wc.IOffersTableElementPort.Inputs={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts={}
xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts={}
xyz.swapee.wc.IOffersTableElementPort.WeakInputs={}
xyz.swapee.wc.IOffersTableDesigner={}
xyz.swapee.wc.IOffersTableDesigner.communicator={}
xyz.swapee.wc.IOffersTableDesigner.relay={}
xyz.swapee.wc.IOffersTableDisplay={}
xyz.swapee.wc.IOffersTableDisplay.paintTableOrder={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IOffersTableDisplay={}
xyz.swapee.wc.back.IOffersTableController={}
xyz.swapee.wc.back.IOffersTableControllerAR={}
xyz.swapee.wc.back.IOffersTableScreen={}
xyz.swapee.wc.back.IOffersTableScreenAT={}
xyz.swapee.wc.IOffersTableController={}
xyz.swapee.wc.IOffersTableScreen={}
xyz.swapee.wc.IOffersTableGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/02-IOffersTableComputer.xml}  ec30cab9599087bf7e441efe3e8b3fa4 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IOffersTableComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableComputer)} xyz.swapee.wc.AbstractOffersTableComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableComputer} xyz.swapee.wc.OffersTableComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableComputer` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableComputer
 */
xyz.swapee.wc.AbstractOffersTableComputer = class extends /** @type {xyz.swapee.wc.AbstractOffersTableComputer.constructor&xyz.swapee.wc.OffersTableComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableComputer.prototype.constructor = xyz.swapee.wc.AbstractOffersTableComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableComputer.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.AbstractOffersTableComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableComputer.Initialese[]) => xyz.swapee.wc.IOffersTableComputer} xyz.swapee.wc.OffersTableComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersTableComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.OffersTableMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>)} xyz.swapee.wc.IOffersTableComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IOffersTableComputer
 */
xyz.swapee.wc.IOffersTableComputer = class extends /** @type {xyz.swapee.wc.IOffersTableComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink} */
xyz.swapee.wc.IOffersTableComputer.prototype.adaptChangellyFixedLink = function() {}
/** @type {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink} */
xyz.swapee.wc.IOffersTableComputer.prototype.adaptChangellyFloatLink = function() {}
/** @type {xyz.swapee.wc.IOffersTableComputer.compute} */
xyz.swapee.wc.IOffersTableComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableComputer.Initialese>)} xyz.swapee.wc.OffersTableComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableComputer} xyz.swapee.wc.IOffersTableComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersTableComputer_ instances.
 * @constructor xyz.swapee.wc.OffersTableComputer
 * @implements {xyz.swapee.wc.IOffersTableComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableComputer.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableComputer = class extends /** @type {xyz.swapee.wc.OffersTableComputer.constructor&xyz.swapee.wc.IOffersTableComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableComputer}
 */
xyz.swapee.wc.OffersTableComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersTableComputer} */
xyz.swapee.wc.RecordIOffersTableComputer

/** @typedef {xyz.swapee.wc.IOffersTableComputer} xyz.swapee.wc.BoundIOffersTableComputer */

/** @typedef {xyz.swapee.wc.OffersTableComputer} xyz.swapee.wc.BoundOffersTableComputer */

/**
 * Contains getters to cast the _IOffersTableComputer_ interface.
 * @interface xyz.swapee.wc.IOffersTableComputerCaster
 */
xyz.swapee.wc.IOffersTableComputerCaster = class { }
/**
 * Cast the _IOffersTableComputer_ instance into the _BoundIOffersTableComputer_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableComputer}
 */
xyz.swapee.wc.IOffersTableComputerCaster.prototype.asIOffersTableComputer
/**
 * Access the _OffersTableComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableComputer}
 */
xyz.swapee.wc.IOffersTableComputerCaster.prototype.superOffersTableComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form, changes: xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form) => (void|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return)} xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFixedLink<!xyz.swapee.wc.IOffersTableComputer>} xyz.swapee.wc.IOffersTableComputer._adaptChangellyFixedLink */
/** @typedef {typeof xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink} */
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} form The form with inputs.
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return} The form with outputs.
 */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form, changes: xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form) => (void|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return)} xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableComputer.__adaptChangellyFloatLink<!xyz.swapee.wc.IOffersTableComputer>} xyz.swapee.wc.IOffersTableComputer._adaptChangellyFloatLink */
/** @typedef {typeof xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink} */
/**
 * @param {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} form The form with inputs.
 * @param {xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return} The form with outputs.
 */
xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink} xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.OffersTableMemory, land: !xyz.swapee.wc.IOffersTableComputer.compute.Land) => void} xyz.swapee.wc.IOffersTableComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableComputer.__compute<!xyz.swapee.wc.IOffersTableComputer>} xyz.swapee.wc.IOffersTableComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IOffersTableComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.OffersTableMemory} mem The memory.
 * @param {!xyz.swapee.wc.IOffersTableComputer.compute.Land} land The land.
 * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_ `.`
 * - `OffersAggregator` _!xyz.swapee.wc.OffersAggregatorMemory_ `.`
 * - `ProgressCollapsar` _!com.webcircuits.ui.CollapsarMemory_ `.`
 * - `CryptoSelectOut` _!xyz.swapee.wc.CryptoSelectMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IOffersTableComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent `.`
 * @prop {!xyz.swapee.wc.OffersAggregatorMemory} OffersAggregator `.`
 * @prop {!com.webcircuits.ui.CollapsarMemory} ProgressCollapsar `.`
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTableComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/03-IOffersTableOuterCore.xml}  3ee3d2cf7e19df9ac131979745548307 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersTableOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableOuterCore)} xyz.swapee.wc.AbstractOffersTableOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableOuterCore} xyz.swapee.wc.OffersTableOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableOuterCore
 */
xyz.swapee.wc.AbstractOffersTableOuterCore = class extends /** @type {xyz.swapee.wc.AbstractOffersTableOuterCore.constructor&xyz.swapee.wc.OffersTableOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableOuterCore.prototype.constructor = xyz.swapee.wc.AbstractOffersTableOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.AbstractOffersTableOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableOuterCoreCaster)} xyz.swapee.wc.IOffersTableOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IOffersTable_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IOffersTableOuterCore
 */
xyz.swapee.wc.IOffersTableOuterCore = class extends /** @type {xyz.swapee.wc.IOffersTableOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersTableOuterCore.prototype.constructor = xyz.swapee.wc.IOffersTableOuterCore

/** @typedef {function(new: xyz.swapee.wc.IOffersTableOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableOuterCore.Initialese>)} xyz.swapee.wc.OffersTableOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableOuterCore} xyz.swapee.wc.IOffersTableOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersTableOuterCore_ instances.
 * @constructor xyz.swapee.wc.OffersTableOuterCore
 * @implements {xyz.swapee.wc.IOffersTableOuterCore} The _IOffersTable_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableOuterCore = class extends /** @type {xyz.swapee.wc.OffersTableOuterCore.constructor&xyz.swapee.wc.IOffersTableOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OffersTableOuterCore.prototype.constructor = xyz.swapee.wc.OffersTableOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableOuterCore}
 */
xyz.swapee.wc.OffersTableOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableOuterCore.
 * @interface xyz.swapee.wc.IOffersTableOuterCoreFields
 */
xyz.swapee.wc.IOffersTableOuterCoreFields = class { }
/**
 * The _IOffersTable_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IOffersTableOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOffersTableOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore} */
xyz.swapee.wc.RecordIOffersTableOuterCore

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore} xyz.swapee.wc.BoundIOffersTableOuterCore */

/** @typedef {xyz.swapee.wc.OffersTableOuterCore} xyz.swapee.wc.BoundOffersTableOuterCore */

/**
 * How many seconds left until the offers are reset.
 * @typedef {number}
 */
xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset.untilReset

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset} xyz.swapee.wc.IOffersTableOuterCore.Model The _IOffersTable_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset} xyz.swapee.wc.IOffersTableOuterCore.WeakModel The _IOffersTable_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IOffersTableOuterCore_ interface.
 * @interface xyz.swapee.wc.IOffersTableOuterCoreCaster
 */
xyz.swapee.wc.IOffersTableOuterCoreCaster = class { }
/**
 * Cast the _IOffersTableOuterCore_ instance into the _BoundIOffersTableOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableOuterCore}
 */
xyz.swapee.wc.IOffersTableOuterCoreCaster.prototype.asIOffersTableOuterCore
/**
 * Access the _OffersTableOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableOuterCore}
 */
xyz.swapee.wc.IOffersTableOuterCoreCaster.prototype.superOffersTableOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset How many seconds left until the offers are reset (optional overlay).
 * @prop {number} [untilReset=90] How many seconds left until the offers are reset. Default `90`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe How many seconds left until the offers are reset (required overlay).
 * @prop {number} untilReset How many seconds left until the offers are reset.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset How many seconds left until the offers are reset (optional overlay).
 * @prop {*} [untilReset=90] How many seconds left until the offers are reset. Default `90`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe How many seconds left until the offers are reset (required overlay).
 * @prop {*} untilReset How many seconds left until the offers are reset.
 */

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset} xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset How many seconds left until the offers are reset (optional overlay). */

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe} xyz.swapee.wc.IOffersTablePort.Inputs.UntilReset_Safe How many seconds left until the offers are reset (required overlay). */

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset} xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset How many seconds left until the offers are reset (optional overlay). */

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.WeakModel.UntilReset_Safe} xyz.swapee.wc.IOffersTablePort.WeakInputs.UntilReset_Safe How many seconds left until the offers are reset (required overlay). */

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset} xyz.swapee.wc.IOffersTableCore.Model.UntilReset How many seconds left until the offers are reset (optional overlay). */

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.Model.UntilReset_Safe} xyz.swapee.wc.IOffersTableCore.Model.UntilReset_Safe How many seconds left until the offers are reset (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/04-IOffersTablePort.xml}  b03a85ef4cd3de814403652285b3efc7 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOffersTablePort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTablePort)} xyz.swapee.wc.AbstractOffersTablePort.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTablePort} xyz.swapee.wc.OffersTablePort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTablePort` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTablePort
 */
xyz.swapee.wc.AbstractOffersTablePort = class extends /** @type {xyz.swapee.wc.AbstractOffersTablePort.constructor&xyz.swapee.wc.OffersTablePort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTablePort.prototype.constructor = xyz.swapee.wc.AbstractOffersTablePort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTablePort.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTablePort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTablePort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTablePort|typeof xyz.swapee.wc.OffersTablePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.AbstractOffersTablePort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTablePort.Initialese[]) => xyz.swapee.wc.IOffersTablePort} xyz.swapee.wc.OffersTablePortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersTablePortFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTablePortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersTablePort.Inputs>)} xyz.swapee.wc.IOffersTablePort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IOffersTable_, providing input
 * pins.
 * @interface xyz.swapee.wc.IOffersTablePort
 */
xyz.swapee.wc.IOffersTablePort = class extends /** @type {xyz.swapee.wc.IOffersTablePort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTablePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTablePort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersTablePort.resetPort} */
xyz.swapee.wc.IOffersTablePort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IOffersTablePort.resetOffersTablePort} */
xyz.swapee.wc.IOffersTablePort.prototype.resetOffersTablePort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTablePort&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTablePort.Initialese>)} xyz.swapee.wc.OffersTablePort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTablePort} xyz.swapee.wc.IOffersTablePort.typeof */
/**
 * A concrete class of _IOffersTablePort_ instances.
 * @constructor xyz.swapee.wc.OffersTablePort
 * @implements {xyz.swapee.wc.IOffersTablePort} The port that serves as an interface to the _IOffersTable_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTablePort.Initialese>} ‎
 */
xyz.swapee.wc.OffersTablePort = class extends /** @type {xyz.swapee.wc.OffersTablePort.constructor&xyz.swapee.wc.IOffersTablePort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTablePort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTablePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTablePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTablePort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTablePort}
 */
xyz.swapee.wc.OffersTablePort.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTablePort.
 * @interface xyz.swapee.wc.IOffersTablePortFields
 */
xyz.swapee.wc.IOffersTablePortFields = class { }
/**
 * The inputs to the _IOffersTable_'s controller via its port.
 */
xyz.swapee.wc.IOffersTablePortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersTablePort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOffersTablePortFields.prototype.props = /** @type {!xyz.swapee.wc.IOffersTablePort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTablePort} */
xyz.swapee.wc.RecordIOffersTablePort

/** @typedef {xyz.swapee.wc.IOffersTablePort} xyz.swapee.wc.BoundIOffersTablePort */

/** @typedef {xyz.swapee.wc.OffersTablePort} xyz.swapee.wc.BoundOffersTablePort */

/** @typedef {function(new: xyz.swapee.wc.IOffersTableOuterCore.WeakModel)} xyz.swapee.wc.IOffersTablePort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableOuterCore.WeakModel} xyz.swapee.wc.IOffersTableOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IOffersTable_'s controller via its port.
 * @record xyz.swapee.wc.IOffersTablePort.Inputs
 */
xyz.swapee.wc.IOffersTablePort.Inputs = class extends /** @type {xyz.swapee.wc.IOffersTablePort.Inputs.constructor&xyz.swapee.wc.IOffersTableOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersTablePort.Inputs.prototype.constructor = xyz.swapee.wc.IOffersTablePort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOffersTableOuterCore.WeakModel)} xyz.swapee.wc.IOffersTablePort.WeakInputs.constructor */
/**
 * The inputs to the _IOffersTable_'s controller via its port.
 * @record xyz.swapee.wc.IOffersTablePort.WeakInputs
 */
xyz.swapee.wc.IOffersTablePort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOffersTablePort.WeakInputs.constructor&xyz.swapee.wc.IOffersTableOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersTablePort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOffersTablePort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IOffersTablePortInterface
 */
xyz.swapee.wc.IOffersTablePortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IOffersTablePortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IOffersTablePortInterface.prototype.constructor = xyz.swapee.wc.IOffersTablePortInterface

/**
 * A concrete class of _IOffersTablePortInterface_ instances.
 * @constructor xyz.swapee.wc.OffersTablePortInterface
 * @implements {xyz.swapee.wc.IOffersTablePortInterface} The port interface.
 */
xyz.swapee.wc.OffersTablePortInterface = class extends xyz.swapee.wc.IOffersTablePortInterface { }
xyz.swapee.wc.OffersTablePortInterface.prototype.constructor = xyz.swapee.wc.OffersTablePortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTablePortInterface.Props
 * @prop {number} [untilReset=90] How many seconds left until the offers are reset. Default `90`.
 */

/**
 * Contains getters to cast the _IOffersTablePort_ interface.
 * @interface xyz.swapee.wc.IOffersTablePortCaster
 */
xyz.swapee.wc.IOffersTablePortCaster = class { }
/**
 * Cast the _IOffersTablePort_ instance into the _BoundIOffersTablePort_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTablePort}
 */
xyz.swapee.wc.IOffersTablePortCaster.prototype.asIOffersTablePort
/**
 * Access the _OffersTablePort_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTablePort}
 */
xyz.swapee.wc.IOffersTablePortCaster.prototype.superOffersTablePort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersTablePort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTablePort.__resetPort<!xyz.swapee.wc.IOffersTablePort>} xyz.swapee.wc.IOffersTablePort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOffersTablePort.resetPort} */
/**
 * Resets the _IOffersTable_ port.
 * @return {void}
 */
xyz.swapee.wc.IOffersTablePort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTablePort.__resetOffersTablePort<!xyz.swapee.wc.IOffersTablePort>} xyz.swapee.wc.IOffersTablePort._resetOffersTablePort */
/** @typedef {typeof xyz.swapee.wc.IOffersTablePort.resetOffersTablePort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOffersTablePort.resetOffersTablePort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTablePort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/09-IOffersTableCore.xml}  9d3ba3302d5b2a0aade0c1feff0f3464 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersTableCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableCore)} xyz.swapee.wc.AbstractOffersTableCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableCore} xyz.swapee.wc.OffersTableCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableCore` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableCore
 */
xyz.swapee.wc.AbstractOffersTableCore = class extends /** @type {xyz.swapee.wc.AbstractOffersTableCore.constructor&xyz.swapee.wc.OffersTableCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableCore.prototype.constructor = xyz.swapee.wc.AbstractOffersTableCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableCore.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableOuterCore|typeof xyz.swapee.wc.OffersTableOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.AbstractOffersTableCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableCoreCaster&xyz.swapee.wc.IOffersTableOuterCore)} xyz.swapee.wc.IOffersTableCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableOuterCore} xyz.swapee.wc.IOffersTableOuterCore.typeof */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IOffersTableCore
 */
xyz.swapee.wc.IOffersTableCore = class extends /** @type {xyz.swapee.wc.IOffersTableCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersTableOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IOffersTableCore.resetCore} */
xyz.swapee.wc.IOffersTableCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IOffersTableCore.resetOffersTableCore} */
xyz.swapee.wc.IOffersTableCore.prototype.resetOffersTableCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableCore.Initialese>)} xyz.swapee.wc.OffersTableCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableCore} xyz.swapee.wc.IOffersTableCore.typeof */
/**
 * A concrete class of _IOffersTableCore_ instances.
 * @constructor xyz.swapee.wc.OffersTableCore
 * @implements {xyz.swapee.wc.IOffersTableCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableCore.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableCore = class extends /** @type {xyz.swapee.wc.OffersTableCore.constructor&xyz.swapee.wc.IOffersTableCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OffersTableCore.prototype.constructor = xyz.swapee.wc.OffersTableCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableCore}
 */
xyz.swapee.wc.OffersTableCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableCore.
 * @interface xyz.swapee.wc.IOffersTableCoreFields
 */
xyz.swapee.wc.IOffersTableCoreFields = class { }
/**
 * The _IOffersTable_'s memory.
 */
xyz.swapee.wc.IOffersTableCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOffersTableCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IOffersTableCoreFields.prototype.props = /** @type {xyz.swapee.wc.IOffersTableCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableCore} */
xyz.swapee.wc.RecordIOffersTableCore

/** @typedef {xyz.swapee.wc.IOffersTableCore} xyz.swapee.wc.BoundIOffersTableCore */

/** @typedef {xyz.swapee.wc.OffersTableCore} xyz.swapee.wc.BoundOffersTableCore */

/** @typedef {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink.changellyFixedLink

/** @typedef {string} */
xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink.changellyFloatLink

/** @typedef {xyz.swapee.wc.IOffersTableOuterCore.Model&xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink&xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink} xyz.swapee.wc.IOffersTableCore.Model The _IOffersTable_'s memory. */

/**
 * Contains getters to cast the _IOffersTableCore_ interface.
 * @interface xyz.swapee.wc.IOffersTableCoreCaster
 */
xyz.swapee.wc.IOffersTableCoreCaster = class { }
/**
 * Cast the _IOffersTableCore_ instance into the _BoundIOffersTableCore_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableCore}
 */
xyz.swapee.wc.IOffersTableCoreCaster.prototype.asIOffersTableCore
/**
 * Access the _OffersTableCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableCore}
 */
xyz.swapee.wc.IOffersTableCoreCaster.prototype.superOffersTableCore

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink  (optional overlay).
 * @prop {string} [changellyFixedLink=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFixedLink_Safe  (required overlay).
 * @prop {string} changellyFixedLink
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink  (optional overlay).
 * @prop {string} [changellyFloatLink=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableCore.Model.ChangellyFloatLink_Safe  (required overlay).
 * @prop {string} changellyFloatLink
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersTableCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableCore.__resetCore<!xyz.swapee.wc.IOffersTableCore>} xyz.swapee.wc.IOffersTableCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IOffersTableCore.resetCore} */
/**
 * Resets the _IOffersTable_ core.
 * @return {void}
 */
xyz.swapee.wc.IOffersTableCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableCore.__resetOffersTableCore<!xyz.swapee.wc.IOffersTableCore>} xyz.swapee.wc.IOffersTableCore._resetOffersTableCore */
/** @typedef {typeof xyz.swapee.wc.IOffersTableCore.resetOffersTableCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOffersTableCore.resetOffersTableCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTableCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/10-IOffersTableProcessor.xml}  d8d8f17b9b6aef6e9a783202a9e0ae82 */
/** @typedef {xyz.swapee.wc.IOffersTableComputer.Initialese&xyz.swapee.wc.IOffersTableController.Initialese} xyz.swapee.wc.IOffersTableProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableProcessor)} xyz.swapee.wc.AbstractOffersTableProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableProcessor} xyz.swapee.wc.OffersTableProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableProcessor
 */
xyz.swapee.wc.AbstractOffersTableProcessor = class extends /** @type {xyz.swapee.wc.AbstractOffersTableProcessor.constructor&xyz.swapee.wc.OffersTableProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableProcessor.prototype.constructor = xyz.swapee.wc.AbstractOffersTableProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableCore|typeof xyz.swapee.wc.OffersTableCore)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.AbstractOffersTableProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableProcessor.Initialese[]) => xyz.swapee.wc.IOffersTableProcessor} xyz.swapee.wc.OffersTableProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersTableProcessorCaster&xyz.swapee.wc.IOffersTableComputer&xyz.swapee.wc.IOffersTableCore&xyz.swapee.wc.IOffersTableController)} xyz.swapee.wc.IOffersTableProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableController} xyz.swapee.wc.IOffersTableController.typeof */
/**
 * The processor to compute changes to the memory for the _IOffersTable_.
 * @interface xyz.swapee.wc.IOffersTableProcessor
 */
xyz.swapee.wc.IOffersTableProcessor = class extends /** @type {xyz.swapee.wc.IOffersTableProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersTableComputer.typeof&xyz.swapee.wc.IOffersTableCore.typeof&xyz.swapee.wc.IOffersTableController.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableProcessor.Initialese>)} xyz.swapee.wc.OffersTableProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableProcessor} xyz.swapee.wc.IOffersTableProcessor.typeof */
/**
 * A concrete class of _IOffersTableProcessor_ instances.
 * @constructor xyz.swapee.wc.OffersTableProcessor
 * @implements {xyz.swapee.wc.IOffersTableProcessor} The processor to compute changes to the memory for the _IOffersTable_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableProcessor.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableProcessor = class extends /** @type {xyz.swapee.wc.OffersTableProcessor.constructor&xyz.swapee.wc.IOffersTableProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableProcessor}
 */
xyz.swapee.wc.OffersTableProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersTableProcessor} */
xyz.swapee.wc.RecordIOffersTableProcessor

/** @typedef {xyz.swapee.wc.IOffersTableProcessor} xyz.swapee.wc.BoundIOffersTableProcessor */

/** @typedef {xyz.swapee.wc.OffersTableProcessor} xyz.swapee.wc.BoundOffersTableProcessor */

/**
 * Contains getters to cast the _IOffersTableProcessor_ interface.
 * @interface xyz.swapee.wc.IOffersTableProcessorCaster
 */
xyz.swapee.wc.IOffersTableProcessorCaster = class { }
/**
 * Cast the _IOffersTableProcessor_ instance into the _BoundIOffersTableProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableProcessor}
 */
xyz.swapee.wc.IOffersTableProcessorCaster.prototype.asIOffersTableProcessor
/**
 * Access the _OffersTableProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableProcessor}
 */
xyz.swapee.wc.IOffersTableProcessorCaster.prototype.superOffersTableProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/100-OffersTableMemory.xml}  304a20efb04f2f67bbf1424df82663ed */
/**
 * The memory of the _IOffersTable_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.OffersTableMemory
 */
xyz.swapee.wc.OffersTableMemory = class { }
/**
 * How many seconds left until the offers are reset. Default `0`.
 */
xyz.swapee.wc.OffersTableMemory.prototype.untilReset = /** @type {number} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.OffersTableMemory.prototype.changellyFixedLink = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.OffersTableMemory.prototype.changellyFloatLink = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/102-OffersTableInputs.xml}  ac2360e06a97afadce45a4977b1b51b9 */
/**
 * The inputs of the _IOffersTable_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.OffersTableInputs
 */
xyz.swapee.wc.front.OffersTableInputs = class { }
/**
 * How many seconds left until the offers are reset. Default `90`.
 */
xyz.swapee.wc.front.OffersTableInputs.prototype.untilReset = /** @type {number|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/11-IOffersTable.xml}  b8bb1f7f28a3e0f614b093eff95f9086 */
/**
 * An atomic wrapper for the _IOffersTable_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.OffersTableEnv
 */
xyz.swapee.wc.OffersTableEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.OffersTableEnv.prototype.offersTable = /** @type {xyz.swapee.wc.IOffersTable} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs>&xyz.swapee.wc.IOffersTableProcessor.Initialese&xyz.swapee.wc.IOffersTableComputer.Initialese&xyz.swapee.wc.IOffersTableController.Initialese} xyz.swapee.wc.IOffersTable.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersTable)} xyz.swapee.wc.AbstractOffersTable.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTable} xyz.swapee.wc.OffersTable.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTable` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTable
 */
xyz.swapee.wc.AbstractOffersTable = class extends /** @type {xyz.swapee.wc.AbstractOffersTable.constructor&xyz.swapee.wc.OffersTable.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTable.prototype.constructor = xyz.swapee.wc.AbstractOffersTable
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTable.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTable} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTable}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTable.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.AbstractOffersTable.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTable.Initialese[]) => xyz.swapee.wc.IOffersTable} xyz.swapee.wc.OffersTableConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTable.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IOffersTable.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IOffersTable.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IOffersTable.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.OffersTableMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.OffersTableClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IOffersTableFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableCaster&xyz.swapee.wc.IOffersTableProcessor&xyz.swapee.wc.IOffersTableComputer&xyz.swapee.wc.IOffersTableController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableLand>)} xyz.swapee.wc.IOffersTable.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IOffersTable
 */
xyz.swapee.wc.IOffersTable = class extends /** @type {xyz.swapee.wc.IOffersTable.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersTableProcessor.typeof&xyz.swapee.wc.IOffersTableComputer.typeof&xyz.swapee.wc.IOffersTableController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTable* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTable.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTable&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTable.Initialese>)} xyz.swapee.wc.OffersTable.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTable} xyz.swapee.wc.IOffersTable.typeof */
/**
 * A concrete class of _IOffersTable_ instances.
 * @constructor xyz.swapee.wc.OffersTable
 * @implements {xyz.swapee.wc.IOffersTable} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTable.Initialese>} ‎
 */
xyz.swapee.wc.OffersTable = class extends /** @type {xyz.swapee.wc.OffersTable.constructor&xyz.swapee.wc.IOffersTable.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTable* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTable* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTable.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTable.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTable}
 */
xyz.swapee.wc.OffersTable.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTable.
 * @interface xyz.swapee.wc.IOffersTableFields
 */
xyz.swapee.wc.IOffersTableFields = class { }
/**
 * The input pins of the _IOffersTable_ port.
 */
xyz.swapee.wc.IOffersTableFields.prototype.pinout = /** @type {!xyz.swapee.wc.IOffersTable.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTable} */
xyz.swapee.wc.RecordIOffersTable

/** @typedef {xyz.swapee.wc.IOffersTable} xyz.swapee.wc.BoundIOffersTable */

/** @typedef {xyz.swapee.wc.OffersTable} xyz.swapee.wc.BoundOffersTable */

/** @typedef {xyz.swapee.wc.IOffersTableController.Inputs} xyz.swapee.wc.IOffersTable.Pinout The input pins of the _IOffersTable_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>)} xyz.swapee.wc.IOffersTableBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IOffersTableBuffer
 */
xyz.swapee.wc.IOffersTableBuffer = class extends /** @type {xyz.swapee.wc.IOffersTableBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersTableBuffer.prototype.constructor = xyz.swapee.wc.IOffersTableBuffer

/**
 * A concrete class of _IOffersTableBuffer_ instances.
 * @constructor xyz.swapee.wc.OffersTableBuffer
 * @implements {xyz.swapee.wc.IOffersTableBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.OffersTableBuffer = class extends xyz.swapee.wc.IOffersTableBuffer { }
xyz.swapee.wc.OffersTableBuffer.prototype.constructor = xyz.swapee.wc.OffersTableBuffer

/**
 * Contains getters to cast the _IOffersTable_ interface.
 * @interface xyz.swapee.wc.IOffersTableCaster
 */
xyz.swapee.wc.IOffersTableCaster = class { }
/**
 * Cast the _IOffersTable_ instance into the _BoundIOffersTable_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTable}
 */
xyz.swapee.wc.IOffersTableCaster.prototype.asIOffersTable
/**
 * Access the _OffersTable_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTable}
 */
xyz.swapee.wc.IOffersTableCaster.prototype.superOffersTable

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/110-OffersTableSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersTableMemoryPQs
 */
xyz.swapee.wc.OffersTableMemoryPQs = class {
  constructor() {
    /**
     * `fbbee`
     */
    this.untilReset=/** @type {string} */ (void 0)
    /**
     * `b1a5b`
     */
    this.coinIm=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OffersTableMemoryPQs.prototype.constructor = xyz.swapee.wc.OffersTableMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersTableMemoryQPs
 * @dict
 */
xyz.swapee.wc.OffersTableMemoryQPs = class { }
/**
 * `untilReset`
 */
xyz.swapee.wc.OffersTableMemoryQPs.prototype.fbbee = /** @type {string} */ (void 0)
/**
 * `coinIm`
 */
xyz.swapee.wc.OffersTableMemoryQPs.prototype.b1a5b = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.OffersTableMemoryPQs)} xyz.swapee.wc.OffersTableInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableMemoryPQs} xyz.swapee.wc.OffersTableMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersTableInputsPQs
 */
xyz.swapee.wc.OffersTableInputsPQs = class extends /** @type {xyz.swapee.wc.OffersTableInputsPQs.constructor&xyz.swapee.wc.OffersTableMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OffersTableInputsPQs.prototype.constructor = xyz.swapee.wc.OffersTableInputsPQs

/** @typedef {function(new: xyz.swapee.wc.OffersTableMemoryPQs)} xyz.swapee.wc.OffersTableInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersTableInputsQPs
 * @dict
 */
xyz.swapee.wc.OffersTableInputsQPs = class extends /** @type {xyz.swapee.wc.OffersTableInputsQPs.constructor&xyz.swapee.wc.OffersTableMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OffersTableInputsQPs.prototype.constructor = xyz.swapee.wc.OffersTableInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersTableQueriesPQs
 */
xyz.swapee.wc.OffersTableQueriesPQs = class {
  constructor() {
    /**
     * `dc6e2`
     */
    this.cryptoSelectedImWrSel=/** @type {string} */ (void 0)
    /**
     * `ac854`
     */
    this.resetInSel=/** @type {string} */ (void 0)
    /**
     * `be273`
     */
    this.resetNowBuSel=/** @type {string} */ (void 0)
    /**
     * `d5cac`
     */
    this.exchangesLoadedSel=/** @type {string} */ (void 0)
    /**
     * `f6836`
     */
    this.exchangesTotalSel=/** @type {string} */ (void 0)
    /**
     * `b9642`
     */
    this.progressCollapsarSel=/** @type {string} */ (void 0)
    /**
     * `b3da4`
     */
    this.exchangeIntentSel=/** @type {string} */ (void 0)
    /**
     * `cde34`
     */
    this.offersAggregatorSel=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OffersTableQueriesPQs.prototype.constructor = xyz.swapee.wc.OffersTableQueriesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersTableQueriesQPs
 * @dict
 */
xyz.swapee.wc.OffersTableQueriesQPs = class { }
/**
 * `cryptoSelectedImWrSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.dc6e2 = /** @type {string} */ (void 0)
/**
 * `resetInSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.ac854 = /** @type {string} */ (void 0)
/**
 * `resetNowBuSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.be273 = /** @type {string} */ (void 0)
/**
 * `exchangesLoadedSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.d5cac = /** @type {string} */ (void 0)
/**
 * `exchangesTotalSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.f6836 = /** @type {string} */ (void 0)
/**
 * `progressCollapsarSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.b9642 = /** @type {string} */ (void 0)
/**
 * `exchangeIntentSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.b3da4 = /** @type {string} */ (void 0)
/**
 * `offersAggregatorSel`
 */
xyz.swapee.wc.OffersTableQueriesQPs.prototype.cde34 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersTableVdusPQs
 */
xyz.swapee.wc.OffersTableVdusPQs = class {
  constructor() {
    /**
     * `f8741`
     */
    this.ResetIn=/** @type {string} */ (void 0)
    /**
     * `f8742`
     */
    this.ResetNowBu=/** @type {string} */ (void 0)
    /**
     * `f8743`
     */
    this.CoinIms=/** @type {string} */ (void 0)
    /**
     * `f8744`
     */
    this.CryptoSelectedImWr=/** @type {string} */ (void 0)
    /**
     * `f8745`
     */
    this.ChangellyFloatingOffer=/** @type {string} */ (void 0)
    /**
     * `f8746`
     */
    this.ChangellyFloatingOfferAmount=/** @type {string} */ (void 0)
    /**
     * `f8747`
     */
    this.ChangellyFixedOffer=/** @type {string} */ (void 0)
    /**
     * `f8748`
     */
    this.ChangellyFixedOfferAmount=/** @type {string} */ (void 0)
    /**
     * `f8749`
     */
    this.LetsExchangeFloatingOffer=/** @type {string} */ (void 0)
    /**
     * `f87410`
     */
    this.LetsExchangeFloatingOfferAmount=/** @type {string} */ (void 0)
    /**
     * `f87411`
     */
    this.LetsExchangeFixedOffer=/** @type {string} */ (void 0)
    /**
     * `f87412`
     */
    this.LetsExchangeFixedOfferAmount=/** @type {string} */ (void 0)
    /**
     * `f87413`
     */
    this.ChangeNowFloatingOffer=/** @type {string} */ (void 0)
    /**
     * `f87414`
     */
    this.ChangeNowFloatingOfferAmount=/** @type {string} */ (void 0)
    /**
     * `f87415`
     */
    this.ChangeNowFixedOffer=/** @type {string} */ (void 0)
    /**
     * `f87416`
     */
    this.ChangeNowFixedOfferAmount=/** @type {string} */ (void 0)
    /**
     * `f87417`
     */
    this.OfferCryptoOuts=/** @type {string} */ (void 0)
    /**
     * `f87418`
     */
    this.ExchangesLoaded=/** @type {string} */ (void 0)
    /**
     * `f87419`
     */
    this.ExchangesTotal=/** @type {string} */ (void 0)
    /**
     * `f87420`
     */
    this.ProgressCollapsar=/** @type {string} */ (void 0)
    /**
     * `f87421`
     */
    this.ExchangeIntent=/** @type {string} */ (void 0)
    /**
     * `f87422`
     */
    this.OffersAggregator=/** @type {string} */ (void 0)
    /**
     * `f87423`
     */
    this.ChangellyFloatingDealBroker=/** @type {string} */ (void 0)
    /**
     * `f87424`
     */
    this.CryptoSelectOut=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OffersTableVdusPQs.prototype.constructor = xyz.swapee.wc.OffersTableVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersTableVdusQPs
 * @dict
 */
xyz.swapee.wc.OffersTableVdusQPs = class { }
/**
 * `ResetIn`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8741 = /** @type {string} */ (void 0)
/**
 * `ResetNowBu`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8742 = /** @type {string} */ (void 0)
/**
 * `CoinIms`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8743 = /** @type {string} */ (void 0)
/**
 * `CryptoSelectedImWr`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8744 = /** @type {string} */ (void 0)
/**
 * `ChangellyFloatingOffer`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8745 = /** @type {string} */ (void 0)
/**
 * `ChangellyFloatingOfferAmount`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8746 = /** @type {string} */ (void 0)
/**
 * `ChangellyFixedOffer`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8747 = /** @type {string} */ (void 0)
/**
 * `ChangellyFixedOfferAmount`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8748 = /** @type {string} */ (void 0)
/**
 * `LetsExchangeFloatingOffer`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f8749 = /** @type {string} */ (void 0)
/**
 * `LetsExchangeFloatingOfferAmount`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87410 = /** @type {string} */ (void 0)
/**
 * `LetsExchangeFixedOffer`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87411 = /** @type {string} */ (void 0)
/**
 * `LetsExchangeFixedOfferAmount`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87412 = /** @type {string} */ (void 0)
/**
 * `ChangeNowFloatingOffer`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87413 = /** @type {string} */ (void 0)
/**
 * `ChangeNowFloatingOfferAmount`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87414 = /** @type {string} */ (void 0)
/**
 * `ChangeNowFixedOffer`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87415 = /** @type {string} */ (void 0)
/**
 * `ChangeNowFixedOfferAmount`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87416 = /** @type {string} */ (void 0)
/**
 * `OfferCryptoOuts`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87417 = /** @type {string} */ (void 0)
/**
 * `ExchangesLoaded`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87418 = /** @type {string} */ (void 0)
/**
 * `ExchangesTotal`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87419 = /** @type {string} */ (void 0)
/**
 * `ProgressCollapsar`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87420 = /** @type {string} */ (void 0)
/**
 * `ExchangeIntent`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87421 = /** @type {string} */ (void 0)
/**
 * `OffersAggregator`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87422 = /** @type {string} */ (void 0)
/**
 * `ChangellyFloatingDealBroker`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87423 = /** @type {string} */ (void 0)
/**
 * `CryptoSelectOut`
 */
xyz.swapee.wc.OffersTableVdusQPs.prototype.f87424 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersTableClassesPQs
 */
xyz.swapee.wc.OffersTableClassesPQs = class {
  constructor() {
    /**
     * `b6cc2`
     */
    this.ColHeading=/** @type {string} */ (void 0)
    /**
     * `b6bfb`
     */
    this.Loading=/** @type {string} */ (void 0)
    /**
     * `hacdf`
     */
    this.Hidden=/** @type {string} */ (void 0)
    /**
     * `aaa87`
     */
    this.SelectedRateType=/** @type {string} */ (void 0)
    /**
     * `e8977`
     */
    this.BestOffer=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OffersTableClassesPQs.prototype.constructor = xyz.swapee.wc.OffersTableClassesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersTableClassesQPs
 * @dict
 */
xyz.swapee.wc.OffersTableClassesQPs = class { }
/**
 * `ColHeading`
 */
xyz.swapee.wc.OffersTableClassesQPs.prototype.b6cc2 = /** @type {string} */ (void 0)
/**
 * `Loading`
 */
xyz.swapee.wc.OffersTableClassesQPs.prototype.b6bfb = /** @type {string} */ (void 0)
/**
 * `Hidden`
 */
xyz.swapee.wc.OffersTableClassesQPs.prototype.hacdf = /** @type {string} */ (void 0)
/**
 * `SelectedRateType`
 */
xyz.swapee.wc.OffersTableClassesQPs.prototype.aaa87 = /** @type {string} */ (void 0)
/**
 * `BestOffer`
 */
xyz.swapee.wc.OffersTableClassesQPs.prototype.e8977 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableHtmlComponentUtilFields)} xyz.swapee.wc.IOffersTableHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IOffersTableHtmlComponentUtil */
xyz.swapee.wc.IOffersTableHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IOffersTableHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IOffersTableHtmlComponentUtil.router} */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IOffersTableHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.OffersTableHtmlComponentUtil
 * @implements {xyz.swapee.wc.IOffersTableHtmlComponentUtil} ‎
 */
xyz.swapee.wc.OffersTableHtmlComponentUtil = class extends xyz.swapee.wc.IOffersTableHtmlComponentUtil { }
xyz.swapee.wc.OffersTableHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.OffersTableHtmlComponentUtil

/**
 * Fields of the IOffersTableHtmlComponentUtil.
 * @interface xyz.swapee.wc.IOffersTableHtmlComponentUtilFields
 */
xyz.swapee.wc.IOffersTableHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IOffersTableHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IOffersTableHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableHtmlComponentUtil} */
xyz.swapee.wc.RecordIOffersTableHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IOffersTableHtmlComponentUtil} xyz.swapee.wc.BoundIOffersTableHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.OffersTableHtmlComponentUtil} xyz.swapee.wc.BoundOffersTableHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPort} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorPort} OffersAggregator
 * @prop {typeof com.webcircuits.ui.ICollapsarPort} ProgressCollapsar
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPort} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IOffersTablePort} OffersTable The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.OffersAggregatorMemory} OffersAggregator
 * @prop {!com.webcircuits.ui.CollapsarMemory} ProgressCollapsar
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut
 * @prop {!xyz.swapee.wc.OffersTableMemory} OffersTable
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} ExchangeIntent
 * @prop {!xyz.swapee.wc.IOffersAggregator.Pinout} OffersAggregator
 * @prop {!com.webcircuits.ui.ICollapsar.Pinout} ProgressCollapsar
 * @prop {!xyz.swapee.wc.ICryptoSelect.Pinout} CryptoSelectOut
 * @prop {!xyz.swapee.wc.IOffersTable.Pinout} OffersTable
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IOffersTableHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableHtmlComponentUtil.__router<!xyz.swapee.wc.IOffersTableHtmlComponentUtil>} xyz.swapee.wc.IOffersTableHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IOffersTableHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `ExchangeIntent` _typeof IExchangeIntentPort_
 * - `OffersAggregator` _typeof IOffersAggregatorPort_
 * - `ProgressCollapsar` _typeof com.webcircuits.ui.ICollapsarPort_
 * - `CryptoSelectOut` _typeof ICryptoSelectPort_
 * - `OffersTable` _typeof IOffersTablePort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `ExchangeIntent` _!ExchangeIntentMemory_
 * - `OffersAggregator` _!OffersAggregatorMemory_
 * - `ProgressCollapsar` _!com.webcircuits.ui.CollapsarMemory_
 * - `CryptoSelectOut` _!CryptoSelectMemory_
 * - `OffersTable` _!OffersTableMemory_
 * @param {!xyz.swapee.wc.IOffersTableHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `ExchangeIntent` _!IExchangeIntent.Pinout_
 * - `OffersAggregator` _!IOffersAggregator.Pinout_
 * - `ProgressCollapsar` _!com.webcircuits.ui.ICollapsar.Pinout_
 * - `CryptoSelectOut` _!ICryptoSelect.Pinout_
 * - `OffersTable` _!IOffersTable.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IOffersTableHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTableHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/12-IOffersTableHtmlComponent.xml}  65cdde4cab8860325f1f333130b5f2a2 */
/** @typedef {xyz.swapee.wc.back.IOffersTableController.Initialese&xyz.swapee.wc.back.IOffersTableScreen.Initialese&xyz.swapee.wc.IOffersTable.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IOffersTableGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IOffersTableProcessor.Initialese&xyz.swapee.wc.IOffersTableComputer.Initialese} xyz.swapee.wc.IOffersTableHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableHtmlComponent)} xyz.swapee.wc.AbstractOffersTableHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableHtmlComponent} xyz.swapee.wc.OffersTableHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableHtmlComponent
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractOffersTableHtmlComponent.constructor&xyz.swapee.wc.OffersTableHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractOffersTableHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableHtmlComponent|typeof xyz.swapee.wc.OffersTableHtmlComponent)|(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.IOffersTable|typeof xyz.swapee.wc.OffersTable)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersTableProcessor|typeof xyz.swapee.wc.OffersTableProcessor)|(!xyz.swapee.wc.IOffersTableComputer|typeof xyz.swapee.wc.OffersTableComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersTableHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableHtmlComponent.Initialese[]) => xyz.swapee.wc.IOffersTableHtmlComponent} xyz.swapee.wc.OffersTableHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersTableHtmlComponentCaster&xyz.swapee.wc.back.IOffersTableController&xyz.swapee.wc.back.IOffersTableScreen&xyz.swapee.wc.IOffersTable&com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>&xyz.swapee.wc.IOffersTableGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersTableLand>&xyz.swapee.wc.IOffersTableProcessor&xyz.swapee.wc.IOffersTableComputer)} xyz.swapee.wc.IOffersTableHtmlComponent.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IOffersTableController} xyz.swapee.wc.back.IOffersTableController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IOffersTableScreen} xyz.swapee.wc.back.IOffersTableScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTable} xyz.swapee.wc.IOffersTable.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableGPU} xyz.swapee.wc.IOffersTableGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableProcessor} xyz.swapee.wc.IOffersTableProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableComputer} xyz.swapee.wc.IOffersTableComputer.typeof */
/**
 * The _IOffersTable_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IOffersTableHtmlComponent
 */
xyz.swapee.wc.IOffersTableHtmlComponent = class extends /** @type {xyz.swapee.wc.IOffersTableHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOffersTableController.typeof&xyz.swapee.wc.back.IOffersTableScreen.typeof&xyz.swapee.wc.IOffersTable.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IOffersTableGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IOffersTableProcessor.typeof&xyz.swapee.wc.IOffersTableComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese>)} xyz.swapee.wc.OffersTableHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableHtmlComponent} xyz.swapee.wc.IOffersTableHtmlComponent.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersTableHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.OffersTableHtmlComponent
 * @implements {xyz.swapee.wc.IOffersTableHtmlComponent} The _IOffersTable_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableHtmlComponent = class extends /** @type {xyz.swapee.wc.OffersTableHtmlComponent.constructor&xyz.swapee.wc.IOffersTableHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableHtmlComponent}
 */
xyz.swapee.wc.OffersTableHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersTableHtmlComponent} */
xyz.swapee.wc.RecordIOffersTableHtmlComponent

/** @typedef {xyz.swapee.wc.IOffersTableHtmlComponent} xyz.swapee.wc.BoundIOffersTableHtmlComponent */

/** @typedef {xyz.swapee.wc.OffersTableHtmlComponent} xyz.swapee.wc.BoundOffersTableHtmlComponent */

/**
 * Contains getters to cast the _IOffersTableHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IOffersTableHtmlComponentCaster
 */
xyz.swapee.wc.IOffersTableHtmlComponentCaster = class { }
/**
 * Cast the _IOffersTableHtmlComponent_ instance into the _BoundIOffersTableHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableHtmlComponent}
 */
xyz.swapee.wc.IOffersTableHtmlComponentCaster.prototype.asIOffersTableHtmlComponent
/**
 * Access the _OffersTableHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableHtmlComponent}
 */
xyz.swapee.wc.IOffersTableHtmlComponentCaster.prototype.superOffersTableHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/130-IOffersTableElement.xml}  8bf224495eb0e860ead39cc941c15c9d */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.OffersTableLand>&guest.maurice.IMilleu.Initialese<!(xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IOffersAggregator|com.webcircuits.ui.ICollapsar|xyz.swapee.wc.ICryptoSelect)>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IOffersTableElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableElement)} xyz.swapee.wc.AbstractOffersTableElement.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableElement} xyz.swapee.wc.OffersTableElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableElement` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableElement
 */
xyz.swapee.wc.AbstractOffersTableElement = class extends /** @type {xyz.swapee.wc.AbstractOffersTableElement.constructor&xyz.swapee.wc.OffersTableElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableElement.prototype.constructor = xyz.swapee.wc.AbstractOffersTableElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableElement.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableElement|typeof xyz.swapee.wc.OffersTableElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableElement|typeof xyz.swapee.wc.OffersTableElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableElement|typeof xyz.swapee.wc.OffersTableElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.AbstractOffersTableElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableElement.Initialese[]) => xyz.swapee.wc.IOffersTableElement} xyz.swapee.wc.OffersTableElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersTableElementFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.IOffersTableElement.Inputs, !xyz.swapee.wc.OffersTableLand>&com.webcircuits.ILanded<!xyz.swapee.wc.OffersTableLand>&guest.maurice.IMilleu<!(xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IOffersAggregator|com.webcircuits.ui.ICollapsar|xyz.swapee.wc.ICryptoSelect)>)} xyz.swapee.wc.IOffersTableElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IOffersTable_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IOffersTableElement
 */
xyz.swapee.wc.IOffersTableElement = class extends /** @type {xyz.swapee.wc.IOffersTableElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersTableElement.solder} */
xyz.swapee.wc.IOffersTableElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.render} */
xyz.swapee.wc.IOffersTableElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.build} */
xyz.swapee.wc.IOffersTableElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.buildExchangeIntent} */
xyz.swapee.wc.IOffersTableElement.prototype.buildExchangeIntent = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.buildOffersAggregator} */
xyz.swapee.wc.IOffersTableElement.prototype.buildOffersAggregator = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.buildProgressCollapsar} */
xyz.swapee.wc.IOffersTableElement.prototype.buildProgressCollapsar = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.buildCryptoSelectOut} */
xyz.swapee.wc.IOffersTableElement.prototype.buildCryptoSelectOut = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.short} */
xyz.swapee.wc.IOffersTableElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.server} */
xyz.swapee.wc.IOffersTableElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IOffersTableElement.inducer} */
xyz.swapee.wc.IOffersTableElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableElement&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableElement.Initialese>)} xyz.swapee.wc.OffersTableElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement} xyz.swapee.wc.IOffersTableElement.typeof */
/**
 * A concrete class of _IOffersTableElement_ instances.
 * @constructor xyz.swapee.wc.OffersTableElement
 * @implements {xyz.swapee.wc.IOffersTableElement} A component description.
 *
 * The _IOffersTable_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableElement.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableElement = class extends /** @type {xyz.swapee.wc.OffersTableElement.constructor&xyz.swapee.wc.IOffersTableElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableElement}
 */
xyz.swapee.wc.OffersTableElement.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableElement.
 * @interface xyz.swapee.wc.IOffersTableElementFields
 */
xyz.swapee.wc.IOffersTableElementFields = class { }
/**
 * The element-specific inputs to the _IOffersTable_ component.
 */
xyz.swapee.wc.IOffersTableElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersTableElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IOffersTableElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableElement} */
xyz.swapee.wc.RecordIOffersTableElement

/** @typedef {xyz.swapee.wc.IOffersTableElement} xyz.swapee.wc.BoundIOffersTableElement */

/** @typedef {xyz.swapee.wc.OffersTableElement} xyz.swapee.wc.BoundOffersTableElement */

/** @typedef {xyz.swapee.wc.IOffersTablePort.Inputs&xyz.swapee.wc.IOffersTableDisplay.Queries&xyz.swapee.wc.IOffersTableController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IOffersTableElementPort.Inputs} xyz.swapee.wc.IOffersTableElement.Inputs The element-specific inputs to the _IOffersTable_ component. */

/**
 * Contains getters to cast the _IOffersTableElement_ interface.
 * @interface xyz.swapee.wc.IOffersTableElementCaster
 */
xyz.swapee.wc.IOffersTableElementCaster = class { }
/**
 * Cast the _IOffersTableElement_ instance into the _BoundIOffersTableElement_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableElement}
 */
xyz.swapee.wc.IOffersTableElementCaster.prototype.asIOffersTableElement
/**
 * Access the _OffersTableElement_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableElement}
 */
xyz.swapee.wc.IOffersTableElementCaster.prototype.superOffersTableElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IDealBrokerCore.Model, instance: !xyz.swapee.wc.IDealBroker) => ?} xyz.swapee.wc.IOffersTableElement.__buildChangellyFloatingDealBroker
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__buildChangellyFloatingDealBroker<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._buildChangellyFloatingDealBroker */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.buildChangellyFloatingDealBroker} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IDealBroker_ component.
 * @param {!xyz.swapee.wc.IDealBrokerCore.Model} model
 * @param {!xyz.swapee.wc.IDealBroker} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.buildChangellyFloatingDealBroker = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.OffersTableMemory, props: !xyz.swapee.wc.IOffersTableElement.Inputs) => Object<string, *>} xyz.swapee.wc.IOffersTableElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__solder<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._solder */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.OffersTableMemory} model The model.
 * - `untilReset` _number_ How many seconds left until the offers are reset. Default `0`.
 * - `coinIm` _string_ When out coin is selected, its image is stored here. Default empty string.
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} props The element props.
 * - `[untilReset=90]` _&#42;?_ How many seconds left until the offers are reset. ⤴ *IOffersTableOuterCore.WeakModel.UntilReset* ⤴ *IOffersTableOuterCore.WeakModel.UntilReset* Default `90`.
 * - `[coinIm=null]` _&#42;?_ When out coin is selected, its image is stored here. ⤴ *IOffersTableOuterCore.WeakModel.CoinIm* ⤴ *IOffersTableOuterCore.WeakModel.CoinIm* Default `null`.
 * - `[cryptoSelectedImWrSel=""]` _string?_ The query to discover the _CryptoSelectedImWr_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[resetInSel=""]` _string?_ The query to discover the _ResetIn_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[resetNowBuSel=""]` _string?_ The query to discover the _ResetNowBu_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangesLoadedSel=""]` _string?_ The query to discover the _ExchangesLoaded_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangesTotalSel=""]` _string?_ The query to discover the _ExchangesTotal_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[progressCollapsarSel=""]` _string?_ The query to discover the _ProgressCollapsar_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[cryptoSelectOutSel=""]` _string?_ The query to discover the _CryptoSelectOut_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[offersAggregatorSel=""]` _string?_ The query to discover the _OffersAggregator_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOffersTableElementPort.Inputs.NoSolder* Default `false`.
 * - `[cryptoSelectedImWrOpts]` _!Object?_ The options to pass to the _CryptoSelectedImWr_ vdu. ⤴ *IOffersTableElementPort.Inputs.CryptoSelectedImWrOpts* Default `{}`.
 * - `[coinImOpts]` _!Object?_ The options to pass to the _CoinIm_ vdu. ⤴ *IOffersTableElementPort.Inputs.CoinImOpts* Default `{}`.
 * - `[changellyFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts* Default `{}`.
 * - `[changellyFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts* Default `{}`.
 * - `[changellyFixedOfferOpts]` _!Object?_ The options to pass to the _ChangellyFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts* Default `{}`.
 * - `[changellyFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts* Default `{}`.
 * - `[letsExchangeFloatingOfferOpts]` _!Object?_ The options to pass to the _LetsExchangeFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts* Default `{}`.
 * - `[letsExchangeFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts* Default `{}`.
 * - `[letsExchangeFixedOfferOpts]` _!Object?_ The options to pass to the _LetsExchangeFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts* Default `{}`.
 * - `[letsExchangeFixedOfferAmountOpts]` _!Object?_ The options to pass to the _LetsExchangeFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts* Default `{}`.
 * - `[changeNowFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangeNowFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts* Default `{}`.
 * - `[changeNowFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangeNowFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts* Default `{}`.
 * - `[changeNowFixedOfferOpts]` _!Object?_ The options to pass to the _ChangeNowFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts* Default `{}`.
 * - `[changeNowFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangeNowFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts* Default `{}`.
 * - `[offerCryptoOutOpts]` _!Object?_ The options to pass to the _OfferCryptoOut_ vdu. ⤴ *IOffersTableElementPort.Inputs.OfferCryptoOutOpts* Default `{}`.
 * - `[resetInOpts]` _!Object?_ The options to pass to the _ResetIn_ vdu. ⤴ *IOffersTableElementPort.Inputs.ResetInOpts* Default `{}`.
 * - `[resetNowBuOpts]` _!Object?_ The options to pass to the _ResetNowBu_ vdu. ⤴ *IOffersTableElementPort.Inputs.ResetNowBuOpts* Default `{}`.
 * - `[exchangesLoadedOpts]` _!Object?_ The options to pass to the _ExchangesLoaded_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangesLoadedOpts* Default `{}`.
 * - `[exchangesTotalOpts]` _!Object?_ The options to pass to the _ExchangesTotal_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangesTotalOpts* Default `{}`.
 * - `[progressCollapsarOpts]` _!Object?_ The options to pass to the _ProgressCollapsar_ vdu. ⤴ *IOffersTableElementPort.Inputs.ProgressCollapsarOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IOffersTableElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[offersAggregatorOpts]` _!Object?_ The options to pass to the _OffersAggregator_ vdu. ⤴ *IOffersTableElementPort.Inputs.OffersAggregatorOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IOffersTableElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OffersTableMemory, instance?: !xyz.swapee.wc.IOffersTableScreen&xyz.swapee.wc.IOffersTableController) => !engineering.type.VNode} xyz.swapee.wc.IOffersTableElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__render<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._render */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.OffersTableMemory} [model] The model for the view.
 * - `untilReset` _number_ How many seconds left until the offers are reset. Default `0`.
 * - `coinIm` _string_ When out coin is selected, its image is stored here. Default empty string.
 * @param {!xyz.swapee.wc.IOffersTableScreen&xyz.swapee.wc.IOffersTableController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOffersTableElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IOffersTableElement.build.Cores, instances: !xyz.swapee.wc.IOffersTableElement.build.Instances) => ?} xyz.swapee.wc.IOffersTableElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__build<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._build */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Cores} cores The models of components on the land.
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntentCore.Model_
 * - `OffersAggregator` _!xyz.swapee.wc.IOffersAggregatorCore.Model_
 * - `ProgressCollapsar` _!com.webcircuits.ui.ICollapsarCore.Model_
 * - `CryptoSelectOut` _!xyz.swapee.wc.ICryptoSelectCore.Model_
 * @param {!xyz.swapee.wc.IOffersTableElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntent_
 * - `OffersAggregator` _!xyz.swapee.wc.IOffersAggregator_
 * - `ProgressCollapsar` _!com.webcircuits.ui.ICollapsar_
 * - `CryptoSelectOut` _!xyz.swapee.wc.ICryptoSelect_
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 * @prop {!xyz.swapee.wc.IOffersAggregatorCore.Model} OffersAggregator
 * @prop {!com.webcircuits.ui.ICollapsarCore.Model} ProgressCollapsar
 * @prop {!xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.IExchangeIntent} ExchangeIntent
 * @prop {!xyz.swapee.wc.IOffersAggregator} OffersAggregator
 * @prop {!com.webcircuits.ui.ICollapsar} ProgressCollapsar
 * @prop {!xyz.swapee.wc.ICryptoSelect} CryptoSelectOut
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeIntentCore.Model, instance: !xyz.swapee.wc.IExchangeIntent) => ?} xyz.swapee.wc.IOffersTableElement.__buildExchangeIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__buildExchangeIntent<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._buildExchangeIntent */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.buildExchangeIntent} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeIntent_ component.
 * @param {!xyz.swapee.wc.IExchangeIntentCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeIntent} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.buildExchangeIntent = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IOffersAggregatorCore.Model, instance: !xyz.swapee.wc.IOffersAggregator) => ?} xyz.swapee.wc.IOffersTableElement.__buildOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__buildOffersAggregator<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._buildOffersAggregator */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.buildOffersAggregator} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IOffersAggregator_ component.
 * @param {!xyz.swapee.wc.IOffersAggregatorCore.Model} model
 * @param {!xyz.swapee.wc.IOffersAggregator} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.buildOffersAggregator = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !com.webcircuits.ui.ICollapsarCore.Model, instance: !com.webcircuits.ui.ICollapsar) => ?} xyz.swapee.wc.IOffersTableElement.__buildProgressCollapsar
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__buildProgressCollapsar<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._buildProgressCollapsar */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.buildProgressCollapsar} */
/**
 * Controls the VDUs using the model of the _com.webcircuits.ui.ICollapsar_ component.
 * @param {!com.webcircuits.ui.ICollapsarCore.Model} model
 * @param {!com.webcircuits.ui.ICollapsar} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.buildProgressCollapsar = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ICryptoSelectCore.Model, instance: !xyz.swapee.wc.ICryptoSelect) => ?} xyz.swapee.wc.IOffersTableElement.__buildCryptoSelectOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__buildCryptoSelectOut<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._buildCryptoSelectOut */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.buildCryptoSelectOut} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ICryptoSelect_ component.
 * @param {!xyz.swapee.wc.ICryptoSelectCore.Model} model
 * @param {!xyz.swapee.wc.ICryptoSelect} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.buildCryptoSelectOut = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.OffersTableMemory, ports: !xyz.swapee.wc.IOffersTableElement.short.Ports, cores: !xyz.swapee.wc.IOffersTableElement.short.Cores) => ?} xyz.swapee.wc.IOffersTableElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__short<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._short */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.OffersTableMemory} model The model from which to feed properties to peer's ports.
 * - `untilReset` _number_ How many seconds left until the offers are reset. Default `0`.
 * - `coinIm` _string_ When out coin is selected, its image is stored here. Default empty string.
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Ports} ports The ports of the peers.
 * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentPortInterface_
 * - `OffersAggregator` _typeof xyz.swapee.wc.IOffersAggregatorPortInterface_
 * - `ProgressCollapsar` _typeof com.webcircuits.ui.ICollapsarPortInterface_
 * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectPortInterface_
 * @param {!xyz.swapee.wc.IOffersTableElement.short.Cores} cores The cores of the peers.
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntentCore.Model_
 * - `OffersAggregator` _xyz.swapee.wc.IOffersAggregatorCore.Model_
 * - `ProgressCollapsar` _com.webcircuits.ui.ICollapsarCore.Model_
 * - `CryptoSelectOut` _xyz.swapee.wc.ICryptoSelectCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPortInterface} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorPortInterface} OffersAggregator
 * @prop {typeof com.webcircuits.ui.ICollapsarPortInterface} ProgressCollapsar
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPortInterface} CryptoSelectOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 * @prop {xyz.swapee.wc.IOffersAggregatorCore.Model} OffersAggregator
 * @prop {com.webcircuits.ui.ICollapsarCore.Model} ProgressCollapsar
 * @prop {xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectOut
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OffersTableMemory, inputs: !xyz.swapee.wc.IOffersTableElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IOffersTableElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__server<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._server */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.OffersTableMemory} memory The memory registers.
 * - `untilReset` _number_ How many seconds left until the offers are reset. Default `0`.
 * - `coinIm` _string_ When out coin is selected, its image is stored here. Default empty string.
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} inputs The inputs to the port.
 * - `[untilReset=90]` _&#42;?_ How many seconds left until the offers are reset. ⤴ *IOffersTableOuterCore.WeakModel.UntilReset* ⤴ *IOffersTableOuterCore.WeakModel.UntilReset* Default `90`.
 * - `[coinIm=null]` _&#42;?_ When out coin is selected, its image is stored here. ⤴ *IOffersTableOuterCore.WeakModel.CoinIm* ⤴ *IOffersTableOuterCore.WeakModel.CoinIm* Default `null`.
 * - `[cryptoSelectedImWrSel=""]` _string?_ The query to discover the _CryptoSelectedImWr_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[resetInSel=""]` _string?_ The query to discover the _ResetIn_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[resetNowBuSel=""]` _string?_ The query to discover the _ResetNowBu_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangesLoadedSel=""]` _string?_ The query to discover the _ExchangesLoaded_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangesTotalSel=""]` _string?_ The query to discover the _ExchangesTotal_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[progressCollapsarSel=""]` _string?_ The query to discover the _ProgressCollapsar_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[cryptoSelectOutSel=""]` _string?_ The query to discover the _CryptoSelectOut_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[offersAggregatorSel=""]` _string?_ The query to discover the _OffersAggregator_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOffersTableElementPort.Inputs.NoSolder* Default `false`.
 * - `[cryptoSelectedImWrOpts]` _!Object?_ The options to pass to the _CryptoSelectedImWr_ vdu. ⤴ *IOffersTableElementPort.Inputs.CryptoSelectedImWrOpts* Default `{}`.
 * - `[coinImOpts]` _!Object?_ The options to pass to the _CoinIm_ vdu. ⤴ *IOffersTableElementPort.Inputs.CoinImOpts* Default `{}`.
 * - `[changellyFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts* Default `{}`.
 * - `[changellyFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts* Default `{}`.
 * - `[changellyFixedOfferOpts]` _!Object?_ The options to pass to the _ChangellyFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts* Default `{}`.
 * - `[changellyFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts* Default `{}`.
 * - `[letsExchangeFloatingOfferOpts]` _!Object?_ The options to pass to the _LetsExchangeFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts* Default `{}`.
 * - `[letsExchangeFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts* Default `{}`.
 * - `[letsExchangeFixedOfferOpts]` _!Object?_ The options to pass to the _LetsExchangeFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts* Default `{}`.
 * - `[letsExchangeFixedOfferAmountOpts]` _!Object?_ The options to pass to the _LetsExchangeFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts* Default `{}`.
 * - `[changeNowFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangeNowFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts* Default `{}`.
 * - `[changeNowFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangeNowFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts* Default `{}`.
 * - `[changeNowFixedOfferOpts]` _!Object?_ The options to pass to the _ChangeNowFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts* Default `{}`.
 * - `[changeNowFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangeNowFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts* Default `{}`.
 * - `[offerCryptoOutOpts]` _!Object?_ The options to pass to the _OfferCryptoOut_ vdu. ⤴ *IOffersTableElementPort.Inputs.OfferCryptoOutOpts* Default `{}`.
 * - `[resetInOpts]` _!Object?_ The options to pass to the _ResetIn_ vdu. ⤴ *IOffersTableElementPort.Inputs.ResetInOpts* Default `{}`.
 * - `[resetNowBuOpts]` _!Object?_ The options to pass to the _ResetNowBu_ vdu. ⤴ *IOffersTableElementPort.Inputs.ResetNowBuOpts* Default `{}`.
 * - `[exchangesLoadedOpts]` _!Object?_ The options to pass to the _ExchangesLoaded_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangesLoadedOpts* Default `{}`.
 * - `[exchangesTotalOpts]` _!Object?_ The options to pass to the _ExchangesTotal_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangesTotalOpts* Default `{}`.
 * - `[progressCollapsarOpts]` _!Object?_ The options to pass to the _ProgressCollapsar_ vdu. ⤴ *IOffersTableElementPort.Inputs.ProgressCollapsarOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IOffersTableElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[offersAggregatorOpts]` _!Object?_ The options to pass to the _OffersAggregator_ vdu. ⤴ *IOffersTableElementPort.Inputs.OffersAggregatorOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOffersTableElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OffersTableMemory, port?: !xyz.swapee.wc.IOffersTableElement.Inputs) => ?} xyz.swapee.wc.IOffersTableElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableElement.__inducer<!xyz.swapee.wc.IOffersTableElement>} xyz.swapee.wc.IOffersTableElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.OffersTableMemory} [model] The model of the component into which to induce the state.
 * - `untilReset` _number_ How many seconds left until the offers are reset. Default `0`.
 * - `coinIm` _string_ When out coin is selected, its image is stored here. Default empty string.
 * @param {!xyz.swapee.wc.IOffersTableElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[untilReset=90]` _&#42;?_ How many seconds left until the offers are reset. ⤴ *IOffersTableOuterCore.WeakModel.UntilReset* ⤴ *IOffersTableOuterCore.WeakModel.UntilReset* Default `90`.
 * - `[coinIm=null]` _&#42;?_ When out coin is selected, its image is stored here. ⤴ *IOffersTableOuterCore.WeakModel.CoinIm* ⤴ *IOffersTableOuterCore.WeakModel.CoinIm* Default `null`.
 * - `[cryptoSelectedImWrSel=""]` _string?_ The query to discover the _CryptoSelectedImWr_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[resetInSel=""]` _string?_ The query to discover the _ResetIn_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[resetNowBuSel=""]` _string?_ The query to discover the _ResetNowBu_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangesLoadedSel=""]` _string?_ The query to discover the _ExchangesLoaded_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangesTotalSel=""]` _string?_ The query to discover the _ExchangesTotal_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[progressCollapsarSel=""]` _string?_ The query to discover the _ProgressCollapsar_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[cryptoSelectOutSel=""]` _string?_ The query to discover the _CryptoSelectOut_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[offersAggregatorSel=""]` _string?_ The query to discover the _OffersAggregator_ VDU. ⤴ *IOffersTableDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOffersTableElementPort.Inputs.NoSolder* Default `false`.
 * - `[cryptoSelectedImWrOpts]` _!Object?_ The options to pass to the _CryptoSelectedImWr_ vdu. ⤴ *IOffersTableElementPort.Inputs.CryptoSelectedImWrOpts* Default `{}`.
 * - `[coinImOpts]` _!Object?_ The options to pass to the _CoinIm_ vdu. ⤴ *IOffersTableElementPort.Inputs.CoinImOpts* Default `{}`.
 * - `[changellyFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts* Default `{}`.
 * - `[changellyFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts* Default `{}`.
 * - `[changellyFixedOfferOpts]` _!Object?_ The options to pass to the _ChangellyFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts* Default `{}`.
 * - `[changellyFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts* Default `{}`.
 * - `[letsExchangeFloatingOfferOpts]` _!Object?_ The options to pass to the _LetsExchangeFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts* Default `{}`.
 * - `[letsExchangeFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts* Default `{}`.
 * - `[letsExchangeFixedOfferOpts]` _!Object?_ The options to pass to the _LetsExchangeFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts* Default `{}`.
 * - `[letsExchangeFixedOfferAmountOpts]` _!Object?_ The options to pass to the _LetsExchangeFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts* Default `{}`.
 * - `[changeNowFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangeNowFloatingOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts* Default `{}`.
 * - `[changeNowFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangeNowFloatingOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts* Default `{}`.
 * - `[changeNowFixedOfferOpts]` _!Object?_ The options to pass to the _ChangeNowFixedOffer_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts* Default `{}`.
 * - `[changeNowFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangeNowFixedOfferAmount_ vdu. ⤴ *IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts* Default `{}`.
 * - `[offerCryptoOutOpts]` _!Object?_ The options to pass to the _OfferCryptoOut_ vdu. ⤴ *IOffersTableElementPort.Inputs.OfferCryptoOutOpts* Default `{}`.
 * - `[resetInOpts]` _!Object?_ The options to pass to the _ResetIn_ vdu. ⤴ *IOffersTableElementPort.Inputs.ResetInOpts* Default `{}`.
 * - `[resetNowBuOpts]` _!Object?_ The options to pass to the _ResetNowBu_ vdu. ⤴ *IOffersTableElementPort.Inputs.ResetNowBuOpts* Default `{}`.
 * - `[exchangesLoadedOpts]` _!Object?_ The options to pass to the _ExchangesLoaded_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangesLoadedOpts* Default `{}`.
 * - `[exchangesTotalOpts]` _!Object?_ The options to pass to the _ExchangesTotal_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangesTotalOpts* Default `{}`.
 * - `[progressCollapsarOpts]` _!Object?_ The options to pass to the _ProgressCollapsar_ vdu. ⤴ *IOffersTableElementPort.Inputs.ProgressCollapsarOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IOffersTableElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOffersTableElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[offersAggregatorOpts]` _!Object?_ The options to pass to the _OffersAggregator_ vdu. ⤴ *IOffersTableElementPort.Inputs.OffersAggregatorOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IOffersTableElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTableElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/140-IOffersTableElementPort.xml}  433a42302241b3b8156c3835be1a8930 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOffersTableElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableElementPort)} xyz.swapee.wc.AbstractOffersTableElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableElementPort} xyz.swapee.wc.OffersTableElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableElementPort
 */
xyz.swapee.wc.AbstractOffersTableElementPort = class extends /** @type {xyz.swapee.wc.AbstractOffersTableElementPort.constructor&xyz.swapee.wc.OffersTableElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableElementPort.prototype.constructor = xyz.swapee.wc.AbstractOffersTableElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableElementPort|typeof xyz.swapee.wc.OffersTableElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableElementPort|typeof xyz.swapee.wc.OffersTableElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableElementPort|typeof xyz.swapee.wc.OffersTableElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.AbstractOffersTableElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableElementPort.Initialese[]) => xyz.swapee.wc.IOffersTableElementPort} xyz.swapee.wc.OffersTableElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersTableElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersTableElementPort.Inputs>)} xyz.swapee.wc.IOffersTableElementPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IOffersTableElementPort
 */
xyz.swapee.wc.IOffersTableElementPort = class extends /** @type {xyz.swapee.wc.IOffersTableElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableElementPort.Initialese>)} xyz.swapee.wc.OffersTableElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort} xyz.swapee.wc.IOffersTableElementPort.typeof */
/**
 * A concrete class of _IOffersTableElementPort_ instances.
 * @constructor xyz.swapee.wc.OffersTableElementPort
 * @implements {xyz.swapee.wc.IOffersTableElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableElementPort.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableElementPort = class extends /** @type {xyz.swapee.wc.OffersTableElementPort.constructor&xyz.swapee.wc.IOffersTableElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableElementPort}
 */
xyz.swapee.wc.OffersTableElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableElementPort.
 * @interface xyz.swapee.wc.IOffersTableElementPortFields
 */
xyz.swapee.wc.IOffersTableElementPortFields = class { }
/**
 * The inputs to the _IOffersTableElement_'s controller via its element port.
 */
xyz.swapee.wc.IOffersTableElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersTableElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOffersTableElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IOffersTableElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableElementPort} */
xyz.swapee.wc.RecordIOffersTableElementPort

/** @typedef {xyz.swapee.wc.IOffersTableElementPort} xyz.swapee.wc.BoundIOffersTableElementPort */

/** @typedef {xyz.swapee.wc.OffersTableElementPort} xyz.swapee.wc.BoundOffersTableElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _CoinImWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts.coinImWrOpts

/**
 * The options to pass to the _ChangellyFloatingOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts.changellyFloatingOfferOpts

/**
 * The options to pass to the _ChangellyFloatingOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts.changellyFloatingOfferAmountOpts

/**
 * The options to pass to the _ChangellyFloatLink_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts.changellyFloatLinkOpts

/**
 * The options to pass to the _ChangellyFixedOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts.changellyFixedOfferOpts

/**
 * The options to pass to the _ChangellyFixedOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts.changellyFixedOfferAmountOpts

/**
 * The options to pass to the _ChangellyFixedLink_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts.changellyFixedLinkOpts

/**
 * The options to pass to the _LetsExchangeFloatingOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts.letsExchangeFloatingOfferOpts

/**
 * The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts.letsExchangeFloatingOfferAmountOpts

/**
 * The options to pass to the _LetsExchangeFixedOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts.letsExchangeFixedOfferOpts

/**
 * The options to pass to the _LetsExchangeFixedOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts.letsExchangeFixedOfferAmountOpts

/**
 * The options to pass to the _ChangeNowFloatingOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts.changeNowFloatingOfferOpts

/**
 * The options to pass to the _ChangeNowFloatingOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts.changeNowFloatingOfferAmountOpts

/**
 * The options to pass to the _ChangeNowFixedOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts.changeNowFixedOfferOpts

/**
 * The options to pass to the _ChangeNowFixedOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts.changeNowFixedOfferAmountOpts

/**
 * The options to pass to the _OfferCryptoOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts.offerCryptoOutOpts

/**
 * The options to pass to the _ResetIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts.resetInOpts

/**
 * The options to pass to the _ResetNowBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts.resetNowBuOpts

/**
 * The options to pass to the _ExchangesLoaded_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts.exchangesLoadedOpts

/**
 * The options to pass to the _ExchangesTotal_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts.exchangesTotalOpts

/**
 * The options to pass to the _ProgressCollapsar_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts.progressCollapsarOpts

/**
 * The options to pass to the _CryptoSelectOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts.cryptoSelectOutOpts

/**
 * The options to pass to the _ExchangeIntent_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

/**
 * The options to pass to the _OffersAggregator_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts.offersAggregatorOpts

/** @typedef {function(new: xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder&xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts&xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts)} xyz.swapee.wc.IOffersTableElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder} xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts} xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts.typeof */
/**
 * The inputs to the _IOffersTableElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOffersTableElementPort.Inputs
 */
xyz.swapee.wc.IOffersTableElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IOffersTableElementPort.Inputs.constructor&xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersTableElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IOffersTableElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts)} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts.typeof */
/**
 * The inputs to the _IOffersTableElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOffersTableElementPort.WeakInputs
 */
xyz.swapee.wc.IOffersTableElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOffersTableElementPort.WeakInputs.constructor&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts.typeof&xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersTableElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOffersTableElementPort.WeakInputs

/**
 * Contains getters to cast the _IOffersTableElementPort_ interface.
 * @interface xyz.swapee.wc.IOffersTableElementPortCaster
 */
xyz.swapee.wc.IOffersTableElementPortCaster = class { }
/**
 * Cast the _IOffersTableElementPort_ instance into the _BoundIOffersTableElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableElementPort}
 */
xyz.swapee.wc.IOffersTableElementPortCaster.prototype.asIOffersTableElementPort
/**
 * Access the _OffersTableElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableElementPort}
 */
xyz.swapee.wc.IOffersTableElementPortCaster.prototype.superOffersTableElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts The options to pass to the _CoinImWr_ vdu (optional overlay).
 * @prop {!Object} [coinImWrOpts] The options to pass to the _CoinImWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.CoinImWrOpts_Safe The options to pass to the _CoinImWr_ vdu (required overlay).
 * @prop {!Object} coinImWrOpts The options to pass to the _CoinImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu (optional overlay).
 * @prop {!Object} [changellyFloatingOfferOpts] The options to pass to the _ChangellyFloatingOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferOpts_Safe The options to pass to the _ChangellyFloatingOffer_ vdu (required overlay).
 * @prop {!Object} changellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [changellyFloatingOfferAmountOpts] The options to pass to the _ChangellyFloatingOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe The options to pass to the _ChangellyFloatingOfferAmount_ vdu (required overlay).
 * @prop {!Object} changellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts The options to pass to the _ChangellyFloatLink_ vdu (optional overlay).
 * @prop {!Object} [changellyFloatLinkOpts] The options to pass to the _ChangellyFloatLink_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFloatLinkOpts_Safe The options to pass to the _ChangellyFloatLink_ vdu (required overlay).
 * @prop {!Object} changellyFloatLinkOpts The options to pass to the _ChangellyFloatLink_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu (optional overlay).
 * @prop {!Object} [changellyFixedOfferOpts] The options to pass to the _ChangellyFixedOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferOpts_Safe The options to pass to the _ChangellyFixedOffer_ vdu (required overlay).
 * @prop {!Object} changellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [changellyFixedOfferAmountOpts] The options to pass to the _ChangellyFixedOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe The options to pass to the _ChangellyFixedOfferAmount_ vdu (required overlay).
 * @prop {!Object} changellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts The options to pass to the _ChangellyFixedLink_ vdu (optional overlay).
 * @prop {!Object} [changellyFixedLinkOpts] The options to pass to the _ChangellyFixedLink_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangellyFixedLinkOpts_Safe The options to pass to the _ChangellyFixedLink_ vdu (required overlay).
 * @prop {!Object} changellyFixedLinkOpts The options to pass to the _ChangellyFixedLink_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts The options to pass to the _LetsExchangeFloatingOffer_ vdu (optional overlay).
 * @prop {!Object} [letsExchangeFloatingOfferOpts] The options to pass to the _LetsExchangeFloatingOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferOpts_Safe The options to pass to the _LetsExchangeFloatingOffer_ vdu (required overlay).
 * @prop {!Object} letsExchangeFloatingOfferOpts The options to pass to the _LetsExchangeFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [letsExchangeFloatingOfferAmountOpts] The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFloatingOfferAmountOpts_Safe The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu (required overlay).
 * @prop {!Object} letsExchangeFloatingOfferAmountOpts The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts The options to pass to the _LetsExchangeFixedOffer_ vdu (optional overlay).
 * @prop {!Object} [letsExchangeFixedOfferOpts] The options to pass to the _LetsExchangeFixedOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferOpts_Safe The options to pass to the _LetsExchangeFixedOffer_ vdu (required overlay).
 * @prop {!Object} letsExchangeFixedOfferOpts The options to pass to the _LetsExchangeFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts The options to pass to the _LetsExchangeFixedOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [letsExchangeFixedOfferAmountOpts] The options to pass to the _LetsExchangeFixedOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.LetsExchangeFixedOfferAmountOpts_Safe The options to pass to the _LetsExchangeFixedOfferAmount_ vdu (required overlay).
 * @prop {!Object} letsExchangeFixedOfferAmountOpts The options to pass to the _LetsExchangeFixedOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts The options to pass to the _ChangeNowFloatingOffer_ vdu (optional overlay).
 * @prop {!Object} [changeNowFloatingOfferOpts] The options to pass to the _ChangeNowFloatingOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferOpts_Safe The options to pass to the _ChangeNowFloatingOffer_ vdu (required overlay).
 * @prop {!Object} changeNowFloatingOfferOpts The options to pass to the _ChangeNowFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts The options to pass to the _ChangeNowFloatingOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [changeNowFloatingOfferAmountOpts] The options to pass to the _ChangeNowFloatingOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFloatingOfferAmountOpts_Safe The options to pass to the _ChangeNowFloatingOfferAmount_ vdu (required overlay).
 * @prop {!Object} changeNowFloatingOfferAmountOpts The options to pass to the _ChangeNowFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts The options to pass to the _ChangeNowFixedOffer_ vdu (optional overlay).
 * @prop {!Object} [changeNowFixedOfferOpts] The options to pass to the _ChangeNowFixedOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferOpts_Safe The options to pass to the _ChangeNowFixedOffer_ vdu (required overlay).
 * @prop {!Object} changeNowFixedOfferOpts The options to pass to the _ChangeNowFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts The options to pass to the _ChangeNowFixedOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [changeNowFixedOfferAmountOpts] The options to pass to the _ChangeNowFixedOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ChangeNowFixedOfferAmountOpts_Safe The options to pass to the _ChangeNowFixedOfferAmount_ vdu (required overlay).
 * @prop {!Object} changeNowFixedOfferAmountOpts The options to pass to the _ChangeNowFixedOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts The options to pass to the _OfferCryptoOut_ vdu (optional overlay).
 * @prop {!Object} [offerCryptoOutOpts] The options to pass to the _OfferCryptoOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.OfferCryptoOutOpts_Safe The options to pass to the _OfferCryptoOut_ vdu (required overlay).
 * @prop {!Object} offerCryptoOutOpts The options to pass to the _OfferCryptoOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts The options to pass to the _ResetIn_ vdu (optional overlay).
 * @prop {!Object} [resetInOpts] The options to pass to the _ResetIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetInOpts_Safe The options to pass to the _ResetIn_ vdu (required overlay).
 * @prop {!Object} resetInOpts The options to pass to the _ResetIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts The options to pass to the _ResetNowBu_ vdu (optional overlay).
 * @prop {!Object} [resetNowBuOpts] The options to pass to the _ResetNowBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ResetNowBuOpts_Safe The options to pass to the _ResetNowBu_ vdu (required overlay).
 * @prop {!Object} resetNowBuOpts The options to pass to the _ResetNowBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts The options to pass to the _ExchangesLoaded_ vdu (optional overlay).
 * @prop {!Object} [exchangesLoadedOpts] The options to pass to the _ExchangesLoaded_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesLoadedOpts_Safe The options to pass to the _ExchangesLoaded_ vdu (required overlay).
 * @prop {!Object} exchangesLoadedOpts The options to pass to the _ExchangesLoaded_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts The options to pass to the _ExchangesTotal_ vdu (optional overlay).
 * @prop {!Object} [exchangesTotalOpts] The options to pass to the _ExchangesTotal_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangesTotalOpts_Safe The options to pass to the _ExchangesTotal_ vdu (required overlay).
 * @prop {!Object} exchangesTotalOpts The options to pass to the _ExchangesTotal_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts The options to pass to the _ProgressCollapsar_ vdu (optional overlay).
 * @prop {!Object} [progressCollapsarOpts] The options to pass to the _ProgressCollapsar_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ProgressCollapsarOpts_Safe The options to pass to the _ProgressCollapsar_ vdu (required overlay).
 * @prop {!Object} progressCollapsarOpts The options to pass to the _ProgressCollapsar_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu (optional overlay).
 * @prop {!Object} [cryptoSelectOutOpts] The options to pass to the _CryptoSelectOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.CryptoSelectOutOpts_Safe The options to pass to the _CryptoSelectOut_ vdu (required overlay).
 * @prop {!Object} cryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {!Object} [exchangeIntentOpts] The options to pass to the _ExchangeIntent_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {!Object} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts The options to pass to the _OffersAggregator_ vdu (optional overlay).
 * @prop {!Object} [offersAggregatorOpts] The options to pass to the _OffersAggregator_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.Inputs.OffersAggregatorOpts_Safe The options to pass to the _OffersAggregator_ vdu (required overlay).
 * @prop {!Object} offersAggregatorOpts The options to pass to the _OffersAggregator_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts The options to pass to the _CoinImWr_ vdu (optional overlay).
 * @prop {*} [coinImWrOpts=null] The options to pass to the _CoinImWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CoinImWrOpts_Safe The options to pass to the _CoinImWr_ vdu (required overlay).
 * @prop {*} coinImWrOpts The options to pass to the _CoinImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu (optional overlay).
 * @prop {*} [changellyFloatingOfferOpts=null] The options to pass to the _ChangellyFloatingOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe The options to pass to the _ChangellyFloatingOffer_ vdu (required overlay).
 * @prop {*} changellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu (optional overlay).
 * @prop {*} [changellyFloatingOfferAmountOpts=null] The options to pass to the _ChangellyFloatingOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe The options to pass to the _ChangellyFloatingOfferAmount_ vdu (required overlay).
 * @prop {*} changellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts The options to pass to the _ChangellyFloatLink_ vdu (optional overlay).
 * @prop {*} [changellyFloatLinkOpts=null] The options to pass to the _ChangellyFloatLink_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFloatLinkOpts_Safe The options to pass to the _ChangellyFloatLink_ vdu (required overlay).
 * @prop {*} changellyFloatLinkOpts The options to pass to the _ChangellyFloatLink_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu (optional overlay).
 * @prop {*} [changellyFixedOfferOpts=null] The options to pass to the _ChangellyFixedOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe The options to pass to the _ChangellyFixedOffer_ vdu (required overlay).
 * @prop {*} changellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu (optional overlay).
 * @prop {*} [changellyFixedOfferAmountOpts=null] The options to pass to the _ChangellyFixedOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe The options to pass to the _ChangellyFixedOfferAmount_ vdu (required overlay).
 * @prop {*} changellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts The options to pass to the _ChangellyFixedLink_ vdu (optional overlay).
 * @prop {*} [changellyFixedLinkOpts=null] The options to pass to the _ChangellyFixedLink_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangellyFixedLinkOpts_Safe The options to pass to the _ChangellyFixedLink_ vdu (required overlay).
 * @prop {*} changellyFixedLinkOpts The options to pass to the _ChangellyFixedLink_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts The options to pass to the _LetsExchangeFloatingOffer_ vdu (optional overlay).
 * @prop {*} [letsExchangeFloatingOfferOpts=null] The options to pass to the _LetsExchangeFloatingOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferOpts_Safe The options to pass to the _LetsExchangeFloatingOffer_ vdu (required overlay).
 * @prop {*} letsExchangeFloatingOfferOpts The options to pass to the _LetsExchangeFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu (optional overlay).
 * @prop {*} [letsExchangeFloatingOfferAmountOpts=null] The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFloatingOfferAmountOpts_Safe The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu (required overlay).
 * @prop {*} letsExchangeFloatingOfferAmountOpts The options to pass to the _LetsExchangeFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts The options to pass to the _LetsExchangeFixedOffer_ vdu (optional overlay).
 * @prop {*} [letsExchangeFixedOfferOpts=null] The options to pass to the _LetsExchangeFixedOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferOpts_Safe The options to pass to the _LetsExchangeFixedOffer_ vdu (required overlay).
 * @prop {*} letsExchangeFixedOfferOpts The options to pass to the _LetsExchangeFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts The options to pass to the _LetsExchangeFixedOfferAmount_ vdu (optional overlay).
 * @prop {*} [letsExchangeFixedOfferAmountOpts=null] The options to pass to the _LetsExchangeFixedOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.LetsExchangeFixedOfferAmountOpts_Safe The options to pass to the _LetsExchangeFixedOfferAmount_ vdu (required overlay).
 * @prop {*} letsExchangeFixedOfferAmountOpts The options to pass to the _LetsExchangeFixedOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts The options to pass to the _ChangeNowFloatingOffer_ vdu (optional overlay).
 * @prop {*} [changeNowFloatingOfferOpts=null] The options to pass to the _ChangeNowFloatingOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferOpts_Safe The options to pass to the _ChangeNowFloatingOffer_ vdu (required overlay).
 * @prop {*} changeNowFloatingOfferOpts The options to pass to the _ChangeNowFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts The options to pass to the _ChangeNowFloatingOfferAmount_ vdu (optional overlay).
 * @prop {*} [changeNowFloatingOfferAmountOpts=null] The options to pass to the _ChangeNowFloatingOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFloatingOfferAmountOpts_Safe The options to pass to the _ChangeNowFloatingOfferAmount_ vdu (required overlay).
 * @prop {*} changeNowFloatingOfferAmountOpts The options to pass to the _ChangeNowFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts The options to pass to the _ChangeNowFixedOffer_ vdu (optional overlay).
 * @prop {*} [changeNowFixedOfferOpts=null] The options to pass to the _ChangeNowFixedOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferOpts_Safe The options to pass to the _ChangeNowFixedOffer_ vdu (required overlay).
 * @prop {*} changeNowFixedOfferOpts The options to pass to the _ChangeNowFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts The options to pass to the _ChangeNowFixedOfferAmount_ vdu (optional overlay).
 * @prop {*} [changeNowFixedOfferAmountOpts=null] The options to pass to the _ChangeNowFixedOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ChangeNowFixedOfferAmountOpts_Safe The options to pass to the _ChangeNowFixedOfferAmount_ vdu (required overlay).
 * @prop {*} changeNowFixedOfferAmountOpts The options to pass to the _ChangeNowFixedOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts The options to pass to the _OfferCryptoOut_ vdu (optional overlay).
 * @prop {*} [offerCryptoOutOpts=null] The options to pass to the _OfferCryptoOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OfferCryptoOutOpts_Safe The options to pass to the _OfferCryptoOut_ vdu (required overlay).
 * @prop {*} offerCryptoOutOpts The options to pass to the _OfferCryptoOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts The options to pass to the _ResetIn_ vdu (optional overlay).
 * @prop {*} [resetInOpts=null] The options to pass to the _ResetIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetInOpts_Safe The options to pass to the _ResetIn_ vdu (required overlay).
 * @prop {*} resetInOpts The options to pass to the _ResetIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts The options to pass to the _ResetNowBu_ vdu (optional overlay).
 * @prop {*} [resetNowBuOpts=null] The options to pass to the _ResetNowBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ResetNowBuOpts_Safe The options to pass to the _ResetNowBu_ vdu (required overlay).
 * @prop {*} resetNowBuOpts The options to pass to the _ResetNowBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts The options to pass to the _ExchangesLoaded_ vdu (optional overlay).
 * @prop {*} [exchangesLoadedOpts=null] The options to pass to the _ExchangesLoaded_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesLoadedOpts_Safe The options to pass to the _ExchangesLoaded_ vdu (required overlay).
 * @prop {*} exchangesLoadedOpts The options to pass to the _ExchangesLoaded_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts The options to pass to the _ExchangesTotal_ vdu (optional overlay).
 * @prop {*} [exchangesTotalOpts=null] The options to pass to the _ExchangesTotal_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangesTotalOpts_Safe The options to pass to the _ExchangesTotal_ vdu (required overlay).
 * @prop {*} exchangesTotalOpts The options to pass to the _ExchangesTotal_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts The options to pass to the _ProgressCollapsar_ vdu (optional overlay).
 * @prop {*} [progressCollapsarOpts=null] The options to pass to the _ProgressCollapsar_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ProgressCollapsarOpts_Safe The options to pass to the _ProgressCollapsar_ vdu (required overlay).
 * @prop {*} progressCollapsarOpts The options to pass to the _ProgressCollapsar_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu (optional overlay).
 * @prop {*} [cryptoSelectOutOpts=null] The options to pass to the _CryptoSelectOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.CryptoSelectOutOpts_Safe The options to pass to the _CryptoSelectOut_ vdu (required overlay).
 * @prop {*} cryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {*} [exchangeIntentOpts=null] The options to pass to the _ExchangeIntent_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {*} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts The options to pass to the _OffersAggregator_ vdu (optional overlay).
 * @prop {*} [offersAggregatorOpts=null] The options to pass to the _OffersAggregator_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableElementPort.WeakInputs.OffersAggregatorOpts_Safe The options to pass to the _OffersAggregator_ vdu (required overlay).
 * @prop {*} offersAggregatorOpts The options to pass to the _OffersAggregator_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/170-IOffersTableDesigner.xml}  60eab01978215efd244238c0992912fe */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IOffersTableDesigner
 */
xyz.swapee.wc.IOffersTableDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.OffersTableClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOffersTable />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OffersTableClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOffersTable />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OffersTableClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `OffersAggregator` _typeof xyz.swapee.wc.IOffersAggregatorController_
   * - `ProgressCollapsar` _typeof com.webcircuits.ui.ICollapsarController_
   * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `OffersTable` _typeof IOffersTableController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOffersTableDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `OffersAggregator` _typeof xyz.swapee.wc.IOffersAggregatorController_
   * - `ProgressCollapsar` _typeof com.webcircuits.ui.ICollapsarController_
   * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `OffersTable` _typeof IOffersTableController_
   * - `This` _typeof IOffersTableController_
   * @param {!xyz.swapee.wc.IOffersTableDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_
   * - `OffersAggregator` _!xyz.swapee.wc.OffersAggregatorMemory_
   * - `ProgressCollapsar` _!com.webcircuits.ui.CollapsarMemory_
   * - `CryptoSelectOut` _!xyz.swapee.wc.CryptoSelectMemory_
   * - `OffersTable` _!OffersTableMemory_
   * - `This` _!OffersTableMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OffersTableClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OffersTableClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IOffersTableDesigner.prototype.constructor = xyz.swapee.wc.IOffersTableDesigner

/**
 * A concrete class of _IOffersTableDesigner_ instances.
 * @constructor xyz.swapee.wc.OffersTableDesigner
 * @implements {xyz.swapee.wc.IOffersTableDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.OffersTableDesigner = class extends xyz.swapee.wc.IOffersTableDesigner { }
xyz.swapee.wc.OffersTableDesigner.prototype.constructor = xyz.swapee.wc.OffersTableDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorController} OffersAggregator
 * @prop {typeof com.webcircuits.ui.ICollapsarController} ProgressCollapsar
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IOffersTableController} OffersTable
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorController} OffersAggregator
 * @prop {typeof com.webcircuits.ui.ICollapsarController} ProgressCollapsar
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IOffersTableController} OffersTable
 * @prop {typeof xyz.swapee.wc.IOffersTableController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.OffersAggregatorMemory} OffersAggregator
 * @prop {!com.webcircuits.ui.CollapsarMemory} ProgressCollapsar
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut
 * @prop {!xyz.swapee.wc.OffersTableMemory} OffersTable
 * @prop {!xyz.swapee.wc.OffersTableMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/200-OffersTableLand.xml}  c809f8949abfadf614845ace79826c84 */
/**
 * The surrounding of the _IOffersTable_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.OffersTableLand
 */
xyz.swapee.wc.OffersTableLand = class { }
/**
 *
 */
xyz.swapee.wc.OffersTableLand.prototype.ExchangeIntent = /** @type {xyz.swapee.wc.IExchangeIntent} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersTableLand.prototype.OffersAggregator = /** @type {xyz.swapee.wc.IOffersAggregator} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersTableLand.prototype.ProgressCollapsar = /** @type {com.webcircuits.ui.ICollapsar} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersTableLand.prototype.CryptoSelectOut = /** @type {xyz.swapee.wc.ICryptoSelect} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplay.xml}  8144168476af2d45f0214267fcf8f931 */
/**
 * @typedef {Object} $xyz.swapee.wc.IOffersTableDisplay.Initialese
 * @prop {!Array<!HTMLElement>} [CoinImWrs] The images inside offer rows with the output crypto icon.
 * @prop {HTMLDivElement} [ChangellyFloatingOffer]
 * @prop {HTMLSpanElement} [ChangellyFloatingOfferAmount]
 * @prop {HTMLAnchorElement} [ChangellyFloatLink]
 * @prop {HTMLDivElement} [ChangellyFixedOffer]
 * @prop {HTMLSpanElement} [ChangellyFixedOfferAmount]
 * @prop {HTMLAnchorElement} [ChangellyFixedLink]
 * @prop {HTMLDivElement} [LetsExchangeFloatingOffer]
 * @prop {HTMLSpanElement} [LetsExchangeFloatingOfferAmount]
 * @prop {HTMLDivElement} [LetsExchangeFixedOffer]
 * @prop {HTMLSpanElement} [LetsExchangeFixedOfferAmount]
 * @prop {HTMLDivElement} [ChangeNowFloatingOffer]
 * @prop {HTMLSpanElement} [ChangeNowFloatingOfferAmount]
 * @prop {HTMLDivElement} [ChangeNowFixedOffer]
 * @prop {HTMLSpanElement} [ChangeNowFixedOfferAmount]
 * @prop {!Array<!HTMLSpanElement>} [OfferCryptoOuts]
 * @prop {HTMLSpanElement} [ResetIn]
 * @prop {HTMLSpanElement} [ResetNowBu]
 * @prop {HTMLSpanElement} [ExchangesLoaded]
 * @prop {HTMLSpanElement} [ExchangesTotal]
 * @prop {HTMLSpanElement} [ProgressCollapsar]
 * @prop {HTMLSpanElement} [CryptoSelectOut]
 * @prop {HTMLElement} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 * @prop {HTMLElement} [OffersAggregator] The via for the _OffersAggregator_ peer.
 */
/** @typedef {$xyz.swapee.wc.IOffersTableDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings>} xyz.swapee.wc.IOffersTableDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersTableDisplay)} xyz.swapee.wc.AbstractOffersTableDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableDisplay} xyz.swapee.wc.OffersTableDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableDisplay
 */
xyz.swapee.wc.AbstractOffersTableDisplay = class extends /** @type {xyz.swapee.wc.AbstractOffersTableDisplay.constructor&xyz.swapee.wc.OffersTableDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableDisplay.prototype.constructor = xyz.swapee.wc.AbstractOffersTableDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.AbstractOffersTableDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableDisplay.Initialese[]) => xyz.swapee.wc.IOffersTableDisplay} xyz.swapee.wc.OffersTableDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersTableDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.OffersTableMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, xyz.swapee.wc.IOffersTableDisplay.Queries, null>)} xyz.swapee.wc.IOffersTableDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IOffersTable_.
 * @interface xyz.swapee.wc.IOffersTableDisplay
 */
xyz.swapee.wc.IOffersTableDisplay = class extends /** @type {xyz.swapee.wc.IOffersTableDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersTableDisplay.paint} */
xyz.swapee.wc.IOffersTableDisplay.prototype.paint = function() {}
/** @type {xyz.swapee.wc.IOffersTableDisplay.paintTableOrder} */
xyz.swapee.wc.IOffersTableDisplay.prototype.paintTableOrder = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableDisplay.Initialese>)} xyz.swapee.wc.OffersTableDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableDisplay} xyz.swapee.wc.IOffersTableDisplay.typeof */
/**
 * A concrete class of _IOffersTableDisplay_ instances.
 * @constructor xyz.swapee.wc.OffersTableDisplay
 * @implements {xyz.swapee.wc.IOffersTableDisplay} Display for presenting information from the _IOffersTable_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableDisplay.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableDisplay = class extends /** @type {xyz.swapee.wc.OffersTableDisplay.constructor&xyz.swapee.wc.IOffersTableDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableDisplay}
 */
xyz.swapee.wc.OffersTableDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableDisplay.
 * @interface xyz.swapee.wc.IOffersTableDisplayFields
 */
xyz.swapee.wc.IOffersTableDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IOffersTableDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IOffersTableDisplay.Queries} */ (void 0)
/**
 * The images inside offer rows with the output crypto icon. Default `[]`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.CoinImWrs = /** @type {!Array<!HTMLElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatingOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatingOfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFloatLink = /** @type {HTMLAnchorElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedOfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangellyFixedLink = /** @type {HTMLAnchorElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFixedOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.LetsExchangeFixedOfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFloatingOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFloatingOfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFixedOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ChangeNowFixedOfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.OfferCryptoOuts = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ResetIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ResetNowBu = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangesLoaded = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangesTotal = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ProgressCollapsar = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.CryptoSelectOut = /** @type {HTMLSpanElement} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.ExchangeIntent = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _OffersAggregator_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersTableDisplayFields.prototype.OffersAggregator = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableDisplay} */
xyz.swapee.wc.RecordIOffersTableDisplay

/** @typedef {xyz.swapee.wc.IOffersTableDisplay} xyz.swapee.wc.BoundIOffersTableDisplay */

/** @typedef {xyz.swapee.wc.OffersTableDisplay} xyz.swapee.wc.BoundOffersTableDisplay */

/** @typedef {xyz.swapee.wc.IOffersTableDisplay.Queries} xyz.swapee.wc.IOffersTableDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [resetInSel=""] The query to discover the _ResetIn_ VDU. Default empty string.
 * @prop {string} [resetNowBuSel=""] The query to discover the _ResetNowBu_ VDU. Default empty string.
 * @prop {string} [exchangesLoadedSel=""] The query to discover the _ExchangesLoaded_ VDU. Default empty string.
 * @prop {string} [exchangesTotalSel=""] The query to discover the _ExchangesTotal_ VDU. Default empty string.
 * @prop {string} [progressCollapsarSel=""] The query to discover the _ProgressCollapsar_ VDU. Default empty string.
 * @prop {string} [cryptoSelectOutSel=""] The query to discover the _CryptoSelectOut_ VDU. Default empty string.
 * @prop {string} [exchangeIntentSel=""] The query to discover the _ExchangeIntent_ VDU. Default empty string.
 * @prop {string} [offersAggregatorSel=""] The query to discover the _OffersAggregator_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IOffersTableDisplay_ interface.
 * @interface xyz.swapee.wc.IOffersTableDisplayCaster
 */
xyz.swapee.wc.IOffersTableDisplayCaster = class { }
/**
 * Cast the _IOffersTableDisplay_ instance into the _BoundIOffersTableDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableDisplay}
 */
xyz.swapee.wc.IOffersTableDisplayCaster.prototype.asIOffersTableDisplay
/**
 * Cast the _IOffersTableDisplay_ instance into the _BoundIOffersTableScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableScreen}
 */
xyz.swapee.wc.IOffersTableDisplayCaster.prototype.asIOffersTableScreen
/**
 * Access the _OffersTableDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableDisplay}
 */
xyz.swapee.wc.IOffersTableDisplayCaster.prototype.superOffersTableDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OffersTableMemory, land: null) => void} xyz.swapee.wc.IOffersTableDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableDisplay.__paint<!xyz.swapee.wc.IOffersTableDisplay>} xyz.swapee.wc.IOffersTableDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IOffersTableDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OffersTableMemory} memory The display data.
 * - `untilReset` _number_ How many seconds left until the offers are reset. Default `0`.
 * - `changellyFixedLink` _string_ Default empty string.
 * - `changellyFloatLink` _string_ Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IOffersTableDisplay.paint = function(memory, land) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory, land: { ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }) => ?} xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableDisplay.__paintTableOrder<!xyz.swapee.wc.IOffersTableDisplay>} xyz.swapee.wc.IOffersTableDisplay._paintTableOrder */
/** @typedef {typeof xyz.swapee.wc.IOffersTableDisplay.paintTableOrder} */
/**
 * Orders items in the table by the rate.
 * @param {!xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory} memory The memory.
 * @param {{ ExchangeIntent: { fixed: xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe, float: xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe }, OffersAggregator: { changeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe, changeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe, changellyFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe, changellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe, letsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe, letsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe } }} land The land for the painter.
 * @return {?}
 */
xyz.swapee.wc.IOffersTableDisplay.paintTableOrder = function(memory, land) {}

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersTableDisplay.paintTableOrder.Memory The memory. */

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTableDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/40-IOffersTableDisplayBack.xml}  7dd32ae56dc60f3b0db5e4a520b4b11e */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IOffersTableDisplay.Initialese
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [CoinImWrs] The images inside offer rows with the output crypto icon.
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFloatingOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFloatingOfferAmount]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFloatLink]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFixedOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFixedOfferAmount]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFixedLink]
 * @prop {!com.webcircuits.IHtmlTwin} [LetsExchangeFloatingOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [LetsExchangeFloatingOfferAmount]
 * @prop {!com.webcircuits.IHtmlTwin} [LetsExchangeFixedOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [LetsExchangeFixedOfferAmount]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangeNowFloatingOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangeNowFloatingOfferAmount]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangeNowFixedOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangeNowFixedOfferAmount]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [OfferCryptoOuts]
 * @prop {!com.webcircuits.IHtmlTwin} [ResetIn]
 * @prop {!com.webcircuits.IHtmlTwin} [ResetNowBu]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangesLoaded]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangesTotal]
 * @prop {!com.webcircuits.IHtmlTwin} [ProgressCollapsar]
 * @prop {!com.webcircuits.IHtmlTwin} [CryptoSelectOut]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [OffersAggregator] The via for the _OffersAggregator_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IOffersTableDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OffersTableClasses>} xyz.swapee.wc.back.IOffersTableDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OffersTableDisplay)} xyz.swapee.wc.back.AbstractOffersTableDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersTableDisplay} xyz.swapee.wc.back.OffersTableDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersTableDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersTableDisplay
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractOffersTableDisplay.constructor&xyz.swapee.wc.back.OffersTableDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersTableDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractOffersTableDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersTableDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.AbstractOffersTableDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IOffersTableDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.OffersTableClasses, !xyz.swapee.wc.OffersTableLand>)} xyz.swapee.wc.back.IOffersTableDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IOffersTableDisplay
 */
xyz.swapee.wc.back.IOffersTableDisplay = class extends /** @type {xyz.swapee.wc.back.IOffersTableDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IOffersTableDisplay.paint} */
xyz.swapee.wc.back.IOffersTableDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableDisplay.Initialese>)} xyz.swapee.wc.back.OffersTableDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOffersTableDisplay} xyz.swapee.wc.back.IOffersTableDisplay.typeof */
/**
 * A concrete class of _IOffersTableDisplay_ instances.
 * @constructor xyz.swapee.wc.back.OffersTableDisplay
 * @implements {xyz.swapee.wc.back.IOffersTableDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersTableDisplay = class extends /** @type {xyz.swapee.wc.back.OffersTableDisplay.constructor&xyz.swapee.wc.back.IOffersTableDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.OffersTableDisplay.prototype.constructor = xyz.swapee.wc.back.OffersTableDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableDisplay}
 */
xyz.swapee.wc.back.OffersTableDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableDisplay.
 * @interface xyz.swapee.wc.back.IOffersTableDisplayFields
 */
xyz.swapee.wc.back.IOffersTableDisplayFields = class { }
/**
 * The images inside offer rows with the output crypto icon. Default `[]`.
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.CoinImWrs = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatingOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatingOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFloatLink = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangellyFixedLink = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFloatingOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFixedOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.LetsExchangeFixedOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFloatingOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFloatingOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFixedOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ChangeNowFixedOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.OfferCryptoOuts = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ResetIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ResetNowBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangesLoaded = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangesTotal = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ProgressCollapsar = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.CryptoSelectOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer.
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.ExchangeIntent = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _OffersAggregator_ peer.
 */
xyz.swapee.wc.back.IOffersTableDisplayFields.prototype.OffersAggregator = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IOffersTableDisplay} */
xyz.swapee.wc.back.RecordIOffersTableDisplay

/** @typedef {xyz.swapee.wc.back.IOffersTableDisplay} xyz.swapee.wc.back.BoundIOffersTableDisplay */

/** @typedef {xyz.swapee.wc.back.OffersTableDisplay} xyz.swapee.wc.back.BoundOffersTableDisplay */

/**
 * Contains getters to cast the _IOffersTableDisplay_ interface.
 * @interface xyz.swapee.wc.back.IOffersTableDisplayCaster
 */
xyz.swapee.wc.back.IOffersTableDisplayCaster = class { }
/**
 * Cast the _IOffersTableDisplay_ instance into the _BoundIOffersTableDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersTableDisplay}
 */
xyz.swapee.wc.back.IOffersTableDisplayCaster.prototype.asIOffersTableDisplay
/**
 * Access the _OffersTableDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersTableDisplay}
 */
xyz.swapee.wc.back.IOffersTableDisplayCaster.prototype.superOffersTableDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.OffersTableMemory, land?: !xyz.swapee.wc.OffersTableLand) => void} xyz.swapee.wc.back.IOffersTableDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IOffersTableDisplay.__paint<!xyz.swapee.wc.back.IOffersTableDisplay>} xyz.swapee.wc.back.IOffersTableDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IOffersTableDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OffersTableMemory} [memory] The display data.
 * - `untilReset` _number_ How many seconds left until the offers are reset. Default `0`.
 * - `changellyFixedLink` _string_ Default empty string.
 * - `changellyFloatLink` _string_ Default empty string.
 * @param {!xyz.swapee.wc.OffersTableLand} [land] The land data.
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntent_
 * - `OffersAggregator` _xyz.swapee.wc.IOffersAggregator_
 * - `ProgressCollapsar` _com.webcircuits.ui.ICollapsar_
 * - `CryptoSelectOut` _xyz.swapee.wc.ICryptoSelect_
 * @return {void}
 */
xyz.swapee.wc.back.IOffersTableDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IOffersTableDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/41-OffersTableClasses.xml}  dd863b71dc37de8f0b54845d922301e8 */
/**
 * The classes of the _IOffersTableDisplay_.
 * @record xyz.swapee.wc.OffersTableClasses
 */
xyz.swapee.wc.OffersTableClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.OffersTableClasses.prototype.ColHeading = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersTableClasses.prototype.Loading = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersTableClasses.prototype.Hidden = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersTableClasses.prototype.SelectedRateType = /** @type {string|undefined} */ (void 0)
/**
 * Applied to the offer row which is the best offer.
 */
xyz.swapee.wc.OffersTableClasses.prototype.BestOffer = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.OffersTableClasses.prototype.props = /** @type {xyz.swapee.wc.OffersTableClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/50-IOffersTableController.xml}  d32ed6284e0a680c3bad109c94482a26 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>} xyz.swapee.wc.IOffersTableController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersTableController)} xyz.swapee.wc.AbstractOffersTableController.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableController} xyz.swapee.wc.OffersTableController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableController` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableController
 */
xyz.swapee.wc.AbstractOffersTableController = class extends /** @type {xyz.swapee.wc.AbstractOffersTableController.constructor&xyz.swapee.wc.OffersTableController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableController.prototype.constructor = xyz.swapee.wc.AbstractOffersTableController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableController.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersTableControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.AbstractOffersTableController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableController.Initialese[]) => xyz.swapee.wc.IOffersTableController} xyz.swapee.wc.OffersTableControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOffersTableControllerHyperslice
 */
xyz.swapee.wc.IOffersTableControllerHyperslice = class {
  constructor() {
    this.reset=/** @type {!xyz.swapee.wc.IOffersTableController._reset|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableController._reset>} */ (void 0)
  }
}
xyz.swapee.wc.IOffersTableControllerHyperslice.prototype.constructor = xyz.swapee.wc.IOffersTableControllerHyperslice

/**
 * A concrete class of _IOffersTableControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.OffersTableControllerHyperslice
 * @implements {xyz.swapee.wc.IOffersTableControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.OffersTableControllerHyperslice = class extends xyz.swapee.wc.IOffersTableControllerHyperslice { }
xyz.swapee.wc.OffersTableControllerHyperslice.prototype.constructor = xyz.swapee.wc.OffersTableControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOffersTableControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IOffersTableControllerBindingHyperslice = class {
  constructor() {
    this.reset=/** @type {!xyz.swapee.wc.IOffersTableController.__reset<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableController.__reset<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IOffersTableControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.IOffersTableControllerBindingHyperslice

/**
 * A concrete class of _IOffersTableControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.OffersTableControllerBindingHyperslice
 * @implements {xyz.swapee.wc.IOffersTableControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IOffersTableControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.OffersTableControllerBindingHyperslice = class extends xyz.swapee.wc.IOffersTableControllerBindingHyperslice { }
xyz.swapee.wc.OffersTableControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.OffersTableControllerBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.IOffersTableControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IOffersTableOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.IOffersTableController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOffersTableController.Inputs, !xyz.swapee.wc.OffersTableMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersTableController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOffersTableController.Inputs>)} xyz.swapee.wc.IOffersTableController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IOffersTableController
 */
xyz.swapee.wc.IOffersTableController = class extends /** @type {xyz.swapee.wc.IOffersTableController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersTableController.resetPort} */
xyz.swapee.wc.IOffersTableController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IOffersTableController.resetTick} */
xyz.swapee.wc.IOffersTableController.prototype.resetTick = function() {}
/** @type {xyz.swapee.wc.IOffersTableController.reset} */
xyz.swapee.wc.IOffersTableController.prototype.reset = function() {}
/** @type {xyz.swapee.wc.IOffersTableController.onReset} */
xyz.swapee.wc.IOffersTableController.prototype.onReset = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableController&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableController.Initialese>)} xyz.swapee.wc.OffersTableController.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableController} xyz.swapee.wc.IOffersTableController.typeof */
/**
 * A concrete class of _IOffersTableController_ instances.
 * @constructor xyz.swapee.wc.OffersTableController
 * @implements {xyz.swapee.wc.IOffersTableController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableController.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableController = class extends /** @type {xyz.swapee.wc.OffersTableController.constructor&xyz.swapee.wc.IOffersTableController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableController}
 */
xyz.swapee.wc.OffersTableController.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableController.
 * @interface xyz.swapee.wc.IOffersTableControllerFields
 */
xyz.swapee.wc.IOffersTableControllerFields = class { }
/**
 * The inputs to the _IOffersTable_'s controller.
 */
xyz.swapee.wc.IOffersTableControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersTableController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IOffersTableControllerFields.prototype.props = /** @type {xyz.swapee.wc.IOffersTableController} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableController} */
xyz.swapee.wc.RecordIOffersTableController

/** @typedef {xyz.swapee.wc.IOffersTableController} xyz.swapee.wc.BoundIOffersTableController */

/** @typedef {xyz.swapee.wc.OffersTableController} xyz.swapee.wc.BoundOffersTableController */

/** @typedef {xyz.swapee.wc.IOffersTablePort.Inputs} xyz.swapee.wc.IOffersTableController.Inputs The inputs to the _IOffersTable_'s controller. */

/** @typedef {xyz.swapee.wc.IOffersTablePort.WeakInputs} xyz.swapee.wc.IOffersTableController.WeakInputs The inputs to the _IOffersTable_'s controller. */

/**
 * Contains getters to cast the _IOffersTableController_ interface.
 * @interface xyz.swapee.wc.IOffersTableControllerCaster
 */
xyz.swapee.wc.IOffersTableControllerCaster = class { }
/**
 * Cast the _IOffersTableController_ instance into the _BoundIOffersTableController_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableController}
 */
xyz.swapee.wc.IOffersTableControllerCaster.prototype.asIOffersTableController
/**
 * Cast the _IOffersTableController_ instance into the _BoundIOffersTableProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableProcessor}
 */
xyz.swapee.wc.IOffersTableControllerCaster.prototype.asIOffersTableProcessor
/**
 * Access the _OffersTableController_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableController}
 */
xyz.swapee.wc.IOffersTableControllerCaster.prototype.superOffersTableController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersTableController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableController.__resetPort<!xyz.swapee.wc.IOffersTableController>} xyz.swapee.wc.IOffersTableController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOffersTableController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IOffersTableController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.IOffersTableController.__resetTick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableController.__resetTick<!xyz.swapee.wc.IOffersTableController>} xyz.swapee.wc.IOffersTableController._resetTick */
/** @typedef {typeof xyz.swapee.wc.IOffersTableController.resetTick} */
/**
 * Invoked by the screen to decrement the time until the next update.
 * @return {?}
 */
xyz.swapee.wc.IOffersTableController.resetTick = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.IOffersTableController.__reset
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableController.__reset<!xyz.swapee.wc.IOffersTableController>} xyz.swapee.wc.IOffersTableController._reset */
/** @typedef {typeof xyz.swapee.wc.IOffersTableController.reset} */
/**
 *  `🔗 $combine`
 * @return {?}
 */
xyz.swapee.wc.IOffersTableController.reset = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.IOffersTableController.__onReset
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableController.__onReset<!xyz.swapee.wc.IOffersTableController>} xyz.swapee.wc.IOffersTableController._onReset */
/** @typedef {typeof xyz.swapee.wc.IOffersTableController.onReset} */
/**
 * Executed when `reset` is called. Can be used in relays.
 * @return {?}
 */
xyz.swapee.wc.IOffersTableController.onReset = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTableController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/51-IOffersTableControllerFront.xml}  f3c5227a2699a2d38393fe0b6e2bde92 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IOffersTableController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OffersTableController)} xyz.swapee.wc.front.AbstractOffersTableController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OffersTableController} xyz.swapee.wc.front.OffersTableController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOffersTableController` interface.
 * @constructor xyz.swapee.wc.front.AbstractOffersTableController
 */
xyz.swapee.wc.front.AbstractOffersTableController = class extends /** @type {xyz.swapee.wc.front.AbstractOffersTableController.constructor&xyz.swapee.wc.front.OffersTableController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOffersTableController.prototype.constructor = xyz.swapee.wc.front.AbstractOffersTableController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOffersTableController.class = /** @type {typeof xyz.swapee.wc.front.AbstractOffersTableController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|!xyz.swapee.wc.front.IOffersTableControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.AbstractOffersTableController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOffersTableController.Initialese[]) => xyz.swapee.wc.front.IOffersTableController} xyz.swapee.wc.front.OffersTableControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.IOffersTableControllerHyperslice
 */
xyz.swapee.wc.front.IOffersTableControllerHyperslice = class {
  constructor() {
    this.reset=/** @type {!xyz.swapee.wc.front.IOffersTableController._reset|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersTableController._reset>} */ (void 0)
  }
}
xyz.swapee.wc.front.IOffersTableControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.IOffersTableControllerHyperslice

/**
 * A concrete class of _IOffersTableControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.OffersTableControllerHyperslice
 * @implements {xyz.swapee.wc.front.IOffersTableControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.front.OffersTableControllerHyperslice = class extends xyz.swapee.wc.front.IOffersTableControllerHyperslice { }
xyz.swapee.wc.front.OffersTableControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.OffersTableControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice = class {
  constructor() {
    this.reset=/** @type {!xyz.swapee.wc.front.IOffersTableController.__reset<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersTableController.__reset<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice

/**
 * A concrete class of _IOffersTableControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.OffersTableControllerBindingHyperslice
 * @implements {xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.front.OffersTableControllerBindingHyperslice = class extends xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice { }
xyz.swapee.wc.front.OffersTableControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.OffersTableControllerBindingHyperslice

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOffersTableControllerCaster&xyz.swapee.wc.front.IOffersTableControllerAT)} xyz.swapee.wc.front.IOffersTableController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOffersTableControllerAT} xyz.swapee.wc.front.IOffersTableControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IOffersTableController
 */
xyz.swapee.wc.front.IOffersTableController = class extends /** @type {xyz.swapee.wc.front.IOffersTableController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IOffersTableControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOffersTableController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IOffersTableController.resetTick} */
xyz.swapee.wc.front.IOffersTableController.prototype.resetTick = function() {}
/** @type {xyz.swapee.wc.front.IOffersTableController.reset} */
xyz.swapee.wc.front.IOffersTableController.prototype.reset = function() {}
/** @type {xyz.swapee.wc.front.IOffersTableController.onReset} */
xyz.swapee.wc.front.IOffersTableController.prototype.onReset = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableController.Initialese>)} xyz.swapee.wc.front.OffersTableController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOffersTableController} xyz.swapee.wc.front.IOffersTableController.typeof */
/**
 * A concrete class of _IOffersTableController_ instances.
 * @constructor xyz.swapee.wc.front.OffersTableController
 * @implements {xyz.swapee.wc.front.IOffersTableController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableController.Initialese>} ‎
 */
xyz.swapee.wc.front.OffersTableController = class extends /** @type {xyz.swapee.wc.front.OffersTableController.constructor&xyz.swapee.wc.front.IOffersTableController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersTableController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OffersTableController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableController}
 */
xyz.swapee.wc.front.OffersTableController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOffersTableController} */
xyz.swapee.wc.front.RecordIOffersTableController

/** @typedef {xyz.swapee.wc.front.IOffersTableController} xyz.swapee.wc.front.BoundIOffersTableController */

/** @typedef {xyz.swapee.wc.front.OffersTableController} xyz.swapee.wc.front.BoundOffersTableController */

/**
 * Contains getters to cast the _IOffersTableController_ interface.
 * @interface xyz.swapee.wc.front.IOffersTableControllerCaster
 */
xyz.swapee.wc.front.IOffersTableControllerCaster = class { }
/**
 * Cast the _IOffersTableController_ instance into the _BoundIOffersTableController_ type.
 * @type {!xyz.swapee.wc.front.BoundIOffersTableController}
 */
xyz.swapee.wc.front.IOffersTableControllerCaster.prototype.asIOffersTableController
/**
 * Access the _OffersTableController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOffersTableController}
 */
xyz.swapee.wc.front.IOffersTableControllerCaster.prototype.superOffersTableController

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.IOffersTableController.__resetTick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersTableController.__resetTick<!xyz.swapee.wc.front.IOffersTableController>} xyz.swapee.wc.front.IOffersTableController._resetTick */
/** @typedef {typeof xyz.swapee.wc.front.IOffersTableController.resetTick} */
/**
 * Invoked by the screen to decrement the time until the next update.
 * @return {?}
 */
xyz.swapee.wc.front.IOffersTableController.resetTick = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.IOffersTableController.__reset
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersTableController.__reset<!xyz.swapee.wc.front.IOffersTableController>} xyz.swapee.wc.front.IOffersTableController._reset */
/** @typedef {typeof xyz.swapee.wc.front.IOffersTableController.reset} */
/**
 *  `🔗 $combine` `🔗 $combine`
 * @return {?}
 */
xyz.swapee.wc.front.IOffersTableController.reset = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.IOffersTableController.__onReset
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersTableController.__onReset<!xyz.swapee.wc.front.IOffersTableController>} xyz.swapee.wc.front.IOffersTableController._onReset */
/** @typedef {typeof xyz.swapee.wc.front.IOffersTableController.onReset} */
/**
 * Executed when `reset` is called. Can be used in relays.
 * @return {?}
 */
xyz.swapee.wc.front.IOffersTableController.onReset = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IOffersTableController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/52-IOffersTableControllerBack.xml}  8296dff8d5731ec958cd53d64248b967 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOffersTableController.Inputs>&xyz.swapee.wc.IOffersTableController.Initialese} xyz.swapee.wc.back.IOffersTableController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OffersTableController)} xyz.swapee.wc.back.AbstractOffersTableController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersTableController} xyz.swapee.wc.back.OffersTableController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersTableController` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersTableController
 */
xyz.swapee.wc.back.AbstractOffersTableController = class extends /** @type {xyz.swapee.wc.back.AbstractOffersTableController.constructor&xyz.swapee.wc.back.OffersTableController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersTableController.prototype.constructor = xyz.swapee.wc.back.AbstractOffersTableController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersTableController.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersTableController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersTableController|typeof xyz.swapee.wc.back.OffersTableController)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.AbstractOffersTableController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersTableController.Initialese[]) => xyz.swapee.wc.back.IOffersTableController} xyz.swapee.wc.back.OffersTableControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersTableControllerCaster&xyz.swapee.wc.IOffersTableController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IOffersTableController.Inputs>)} xyz.swapee.wc.back.IOffersTableController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IOffersTableController
 */
xyz.swapee.wc.back.IOffersTableController = class extends /** @type {xyz.swapee.wc.back.IOffersTableController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersTableController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersTableController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableController.Initialese>)} xyz.swapee.wc.back.OffersTableController.constructor */
/**
 * A concrete class of _IOffersTableController_ instances.
 * @constructor xyz.swapee.wc.back.OffersTableController
 * @implements {xyz.swapee.wc.back.IOffersTableController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableController.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersTableController = class extends /** @type {xyz.swapee.wc.back.OffersTableController.constructor&xyz.swapee.wc.back.IOffersTableController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersTableController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableController}
 */
xyz.swapee.wc.back.OffersTableController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersTableController} */
xyz.swapee.wc.back.RecordIOffersTableController

/** @typedef {xyz.swapee.wc.back.IOffersTableController} xyz.swapee.wc.back.BoundIOffersTableController */

/** @typedef {xyz.swapee.wc.back.OffersTableController} xyz.swapee.wc.back.BoundOffersTableController */

/**
 * Contains getters to cast the _IOffersTableController_ interface.
 * @interface xyz.swapee.wc.back.IOffersTableControllerCaster
 */
xyz.swapee.wc.back.IOffersTableControllerCaster = class { }
/**
 * Cast the _IOffersTableController_ instance into the _BoundIOffersTableController_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersTableController}
 */
xyz.swapee.wc.back.IOffersTableControllerCaster.prototype.asIOffersTableController
/**
 * Access the _OffersTableController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersTableController}
 */
xyz.swapee.wc.back.IOffersTableControllerCaster.prototype.superOffersTableController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/53-IOffersTableControllerAR.xml}  a1623fb2bac9bea312e03af3f20b5bef */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOffersTableController.Initialese} xyz.swapee.wc.back.IOffersTableControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OffersTableControllerAR)} xyz.swapee.wc.back.AbstractOffersTableControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersTableControllerAR} xyz.swapee.wc.back.OffersTableControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersTableControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersTableControllerAR
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractOffersTableControllerAR.constructor&xyz.swapee.wc.back.OffersTableControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersTableControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractOffersTableControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersTableControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersTableControllerAR|typeof xyz.swapee.wc.back.OffersTableControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableController|typeof xyz.swapee.wc.OffersTableController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersTableControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersTableControllerAR.Initialese[]) => xyz.swapee.wc.back.IOffersTableControllerAR} xyz.swapee.wc.back.OffersTableControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersTableControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOffersTableController)} xyz.swapee.wc.back.IOffersTableControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOffersTableControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IOffersTableControllerAR
 */
xyz.swapee.wc.back.IOffersTableControllerAR = class extends /** @type {xyz.swapee.wc.back.IOffersTableControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOffersTableController.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersTableControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese>)} xyz.swapee.wc.back.OffersTableControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOffersTableControllerAR} xyz.swapee.wc.back.IOffersTableControllerAR.typeof */
/**
 * A concrete class of _IOffersTableControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.OffersTableControllerAR
 * @implements {xyz.swapee.wc.back.IOffersTableControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IOffersTableControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersTableControllerAR = class extends /** @type {xyz.swapee.wc.back.OffersTableControllerAR.constructor&xyz.swapee.wc.back.IOffersTableControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersTableControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableControllerAR}
 */
xyz.swapee.wc.back.OffersTableControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersTableControllerAR} */
xyz.swapee.wc.back.RecordIOffersTableControllerAR

/** @typedef {xyz.swapee.wc.back.IOffersTableControllerAR} xyz.swapee.wc.back.BoundIOffersTableControllerAR */

/** @typedef {xyz.swapee.wc.back.OffersTableControllerAR} xyz.swapee.wc.back.BoundOffersTableControllerAR */

/**
 * Contains getters to cast the _IOffersTableControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IOffersTableControllerARCaster
 */
xyz.swapee.wc.back.IOffersTableControllerARCaster = class { }
/**
 * Cast the _IOffersTableControllerAR_ instance into the _BoundIOffersTableControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersTableControllerAR}
 */
xyz.swapee.wc.back.IOffersTableControllerARCaster.prototype.asIOffersTableControllerAR
/**
 * Access the _OffersTableControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersTableControllerAR}
 */
xyz.swapee.wc.back.IOffersTableControllerARCaster.prototype.superOffersTableControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/54-IOffersTableControllerAT.xml}  0a65c4249c246efc51bd7f02216a3700 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IOffersTableControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OffersTableControllerAT)} xyz.swapee.wc.front.AbstractOffersTableControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OffersTableControllerAT} xyz.swapee.wc.front.OffersTableControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOffersTableControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractOffersTableControllerAT
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractOffersTableControllerAT.constructor&xyz.swapee.wc.front.OffersTableControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOffersTableControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractOffersTableControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractOffersTableControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOffersTableControllerAT|typeof xyz.swapee.wc.front.OffersTableControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersTableControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOffersTableControllerAT.Initialese[]) => xyz.swapee.wc.front.IOffersTableControllerAT} xyz.swapee.wc.front.OffersTableControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOffersTableControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IOffersTableControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOffersTableControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IOffersTableControllerAT
 */
xyz.swapee.wc.front.IOffersTableControllerAT = class extends /** @type {xyz.swapee.wc.front.IOffersTableControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOffersTableControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese>)} xyz.swapee.wc.front.OffersTableControllerAT.constructor */
/**
 * A concrete class of _IOffersTableControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.OffersTableControllerAT
 * @implements {xyz.swapee.wc.front.IOffersTableControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOffersTableControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.OffersTableControllerAT = class extends /** @type {xyz.swapee.wc.front.OffersTableControllerAT.constructor&xyz.swapee.wc.front.IOffersTableControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersTableControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OffersTableControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableControllerAT}
 */
xyz.swapee.wc.front.OffersTableControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOffersTableControllerAT} */
xyz.swapee.wc.front.RecordIOffersTableControllerAT

/** @typedef {xyz.swapee.wc.front.IOffersTableControllerAT} xyz.swapee.wc.front.BoundIOffersTableControllerAT */

/** @typedef {xyz.swapee.wc.front.OffersTableControllerAT} xyz.swapee.wc.front.BoundOffersTableControllerAT */

/**
 * Contains getters to cast the _IOffersTableControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IOffersTableControllerATCaster
 */
xyz.swapee.wc.front.IOffersTableControllerATCaster = class { }
/**
 * Cast the _IOffersTableControllerAT_ instance into the _BoundIOffersTableControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIOffersTableControllerAT}
 */
xyz.swapee.wc.front.IOffersTableControllerATCaster.prototype.asIOffersTableControllerAT
/**
 * Access the _OffersTableControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOffersTableControllerAT}
 */
xyz.swapee.wc.front.IOffersTableControllerATCaster.prototype.superOffersTableControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreen.xml}  8421f50c143dcc151edfa185b8391171 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.front.OffersTableInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, !xyz.swapee.wc.IOffersTableDisplay.Queries, !xyz.swapee.wc.OffersTableClasses>&xyz.swapee.wc.IOffersTableDisplay.Initialese} xyz.swapee.wc.IOffersTableScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersTableScreen)} xyz.swapee.wc.AbstractOffersTableScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableScreen} xyz.swapee.wc.OffersTableScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableScreen` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableScreen
 */
xyz.swapee.wc.AbstractOffersTableScreen = class extends /** @type {xyz.swapee.wc.AbstractOffersTableScreen.constructor&xyz.swapee.wc.OffersTableScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableScreen.prototype.constructor = xyz.swapee.wc.AbstractOffersTableScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableScreen.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)|(!xyz.swapee.wc.IOffersTableDisplay|typeof xyz.swapee.wc.OffersTableDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.AbstractOffersTableScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableScreen.Initialese[]) => xyz.swapee.wc.IOffersTableScreen} xyz.swapee.wc.OffersTableScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersTableScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.OffersTableMemory, !xyz.swapee.wc.front.OffersTableInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersTableDisplay.Settings, !xyz.swapee.wc.IOffersTableDisplay.Queries, null, !xyz.swapee.wc.OffersTableClasses>&xyz.swapee.wc.front.IOffersTableController&xyz.swapee.wc.IOffersTableDisplay)} xyz.swapee.wc.IOffersTableScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IOffersTableScreen
 */
xyz.swapee.wc.IOffersTableScreen = class extends /** @type {xyz.swapee.wc.IOffersTableScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IOffersTableController.typeof&xyz.swapee.wc.IOffersTableDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableScreen.Initialese>)} xyz.swapee.wc.OffersTableScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableScreen} xyz.swapee.wc.IOffersTableScreen.typeof */
/**
 * A concrete class of _IOffersTableScreen_ instances.
 * @constructor xyz.swapee.wc.OffersTableScreen
 * @implements {xyz.swapee.wc.IOffersTableScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableScreen.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableScreen = class extends /** @type {xyz.swapee.wc.OffersTableScreen.constructor&xyz.swapee.wc.IOffersTableScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableScreen}
 */
xyz.swapee.wc.OffersTableScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersTableScreen} */
xyz.swapee.wc.RecordIOffersTableScreen

/** @typedef {xyz.swapee.wc.IOffersTableScreen} xyz.swapee.wc.BoundIOffersTableScreen */

/** @typedef {xyz.swapee.wc.OffersTableScreen} xyz.swapee.wc.BoundOffersTableScreen */

/**
 * Contains getters to cast the _IOffersTableScreen_ interface.
 * @interface xyz.swapee.wc.IOffersTableScreenCaster
 */
xyz.swapee.wc.IOffersTableScreenCaster = class { }
/**
 * Cast the _IOffersTableScreen_ instance into the _BoundIOffersTableScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableScreen}
 */
xyz.swapee.wc.IOffersTableScreenCaster.prototype.asIOffersTableScreen
/**
 * Access the _OffersTableScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableScreen}
 */
xyz.swapee.wc.IOffersTableScreenCaster.prototype.superOffersTableScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/70-IOffersTableScreenBack.xml}  6e7722121af84b76d55c212d5c38c0f1 */
/** @typedef {xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} xyz.swapee.wc.back.IOffersTableScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OffersTableScreen)} xyz.swapee.wc.back.AbstractOffersTableScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersTableScreen} xyz.swapee.wc.back.OffersTableScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersTableScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersTableScreen
 */
xyz.swapee.wc.back.AbstractOffersTableScreen = class extends /** @type {xyz.swapee.wc.back.AbstractOffersTableScreen.constructor&xyz.swapee.wc.back.OffersTableScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersTableScreen.prototype.constructor = xyz.swapee.wc.back.AbstractOffersTableScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersTableScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersTableScreen|typeof xyz.swapee.wc.back.OffersTableScreen)|(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.AbstractOffersTableScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersTableScreen.Initialese[]) => xyz.swapee.wc.back.IOffersTableScreen} xyz.swapee.wc.back.OffersTableScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersTableScreenCaster&xyz.swapee.wc.back.IOffersTableScreenAT)} xyz.swapee.wc.back.IOffersTableScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOffersTableScreenAT} xyz.swapee.wc.back.IOffersTableScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IOffersTableScreen
 */
xyz.swapee.wc.back.IOffersTableScreen = class extends /** @type {xyz.swapee.wc.back.IOffersTableScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOffersTableScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersTableScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableScreen.Initialese>)} xyz.swapee.wc.back.OffersTableScreen.constructor */
/**
 * A concrete class of _IOffersTableScreen_ instances.
 * @constructor xyz.swapee.wc.back.OffersTableScreen
 * @implements {xyz.swapee.wc.back.IOffersTableScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersTableScreen = class extends /** @type {xyz.swapee.wc.back.OffersTableScreen.constructor&xyz.swapee.wc.back.IOffersTableScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersTableScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersTableScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableScreen}
 */
xyz.swapee.wc.back.OffersTableScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersTableScreen} */
xyz.swapee.wc.back.RecordIOffersTableScreen

/** @typedef {xyz.swapee.wc.back.IOffersTableScreen} xyz.swapee.wc.back.BoundIOffersTableScreen */

/** @typedef {xyz.swapee.wc.back.OffersTableScreen} xyz.swapee.wc.back.BoundOffersTableScreen */

/**
 * Contains getters to cast the _IOffersTableScreen_ interface.
 * @interface xyz.swapee.wc.back.IOffersTableScreenCaster
 */
xyz.swapee.wc.back.IOffersTableScreenCaster = class { }
/**
 * Cast the _IOffersTableScreen_ instance into the _BoundIOffersTableScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersTableScreen}
 */
xyz.swapee.wc.back.IOffersTableScreenCaster.prototype.asIOffersTableScreen
/**
 * Access the _OffersTableScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersTableScreen}
 */
xyz.swapee.wc.back.IOffersTableScreenCaster.prototype.superOffersTableScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/73-IOffersTableScreenAR.xml}  66abb7501297996b304e4b54c34b4e14 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOffersTableScreen.Initialese} xyz.swapee.wc.front.IOffersTableScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OffersTableScreenAR)} xyz.swapee.wc.front.AbstractOffersTableScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OffersTableScreenAR} xyz.swapee.wc.front.OffersTableScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOffersTableScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractOffersTableScreenAR
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractOffersTableScreenAR.constructor&xyz.swapee.wc.front.OffersTableScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOffersTableScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractOffersTableScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractOffersTableScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOffersTableScreenAR|typeof xyz.swapee.wc.front.OffersTableScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOffersTableScreenAR|typeof xyz.swapee.wc.front.OffersTableScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOffersTableScreenAR|typeof xyz.swapee.wc.front.OffersTableScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersTableScreen|typeof xyz.swapee.wc.OffersTableScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersTableScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOffersTableScreenAR.Initialese[]) => xyz.swapee.wc.front.IOffersTableScreenAR} xyz.swapee.wc.front.OffersTableScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOffersTableScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOffersTableScreen)} xyz.swapee.wc.front.IOffersTableScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOffersTableScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IOffersTableScreenAR
 */
xyz.swapee.wc.front.IOffersTableScreenAR = class extends /** @type {xyz.swapee.wc.front.IOffersTableScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOffersTableScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOffersTableScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOffersTableScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese>)} xyz.swapee.wc.front.OffersTableScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOffersTableScreenAR} xyz.swapee.wc.front.IOffersTableScreenAR.typeof */
/**
 * A concrete class of _IOffersTableScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.OffersTableScreenAR
 * @implements {xyz.swapee.wc.front.IOffersTableScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IOffersTableScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.OffersTableScreenAR = class extends /** @type {xyz.swapee.wc.front.OffersTableScreenAR.constructor&xyz.swapee.wc.front.IOffersTableScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersTableScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OffersTableScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersTableScreenAR}
 */
xyz.swapee.wc.front.OffersTableScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOffersTableScreenAR} */
xyz.swapee.wc.front.RecordIOffersTableScreenAR

/** @typedef {xyz.swapee.wc.front.IOffersTableScreenAR} xyz.swapee.wc.front.BoundIOffersTableScreenAR */

/** @typedef {xyz.swapee.wc.front.OffersTableScreenAR} xyz.swapee.wc.front.BoundOffersTableScreenAR */

/**
 * Contains getters to cast the _IOffersTableScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IOffersTableScreenARCaster
 */
xyz.swapee.wc.front.IOffersTableScreenARCaster = class { }
/**
 * Cast the _IOffersTableScreenAR_ instance into the _BoundIOffersTableScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIOffersTableScreenAR}
 */
xyz.swapee.wc.front.IOffersTableScreenARCaster.prototype.asIOffersTableScreenAR
/**
 * Access the _OffersTableScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOffersTableScreenAR}
 */
xyz.swapee.wc.front.IOffersTableScreenARCaster.prototype.superOffersTableScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/74-IOffersTableScreenAT.xml}  a7f5bb34b512c051d213d57888e91d38 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IOffersTableScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OffersTableScreenAT)} xyz.swapee.wc.back.AbstractOffersTableScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersTableScreenAT} xyz.swapee.wc.back.OffersTableScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersTableScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersTableScreenAT
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractOffersTableScreenAT.constructor&xyz.swapee.wc.back.OffersTableScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersTableScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractOffersTableScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersTableScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersTableScreenAT|typeof xyz.swapee.wc.back.OffersTableScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersTableScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersTableScreenAT.Initialese[]) => xyz.swapee.wc.back.IOffersTableScreenAT} xyz.swapee.wc.back.OffersTableScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersTableScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IOffersTableScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOffersTableScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IOffersTableScreenAT
 */
xyz.swapee.wc.back.IOffersTableScreenAT = class extends /** @type {xyz.swapee.wc.back.IOffersTableScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersTableScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersTableScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese>)} xyz.swapee.wc.back.OffersTableScreenAT.constructor */
/**
 * A concrete class of _IOffersTableScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.OffersTableScreenAT
 * @implements {xyz.swapee.wc.back.IOffersTableScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOffersTableScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersTableScreenAT = class extends /** @type {xyz.swapee.wc.back.OffersTableScreenAT.constructor&xyz.swapee.wc.back.IOffersTableScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersTableScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersTableScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersTableScreenAT}
 */
xyz.swapee.wc.back.OffersTableScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersTableScreenAT} */
xyz.swapee.wc.back.RecordIOffersTableScreenAT

/** @typedef {xyz.swapee.wc.back.IOffersTableScreenAT} xyz.swapee.wc.back.BoundIOffersTableScreenAT */

/** @typedef {xyz.swapee.wc.back.OffersTableScreenAT} xyz.swapee.wc.back.BoundOffersTableScreenAT */

/**
 * Contains getters to cast the _IOffersTableScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IOffersTableScreenATCaster
 */
xyz.swapee.wc.back.IOffersTableScreenATCaster = class { }
/**
 * Cast the _IOffersTableScreenAT_ instance into the _BoundIOffersTableScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersTableScreenAT}
 */
xyz.swapee.wc.back.IOffersTableScreenATCaster.prototype.asIOffersTableScreenAT
/**
 * Access the _OffersTableScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersTableScreenAT}
 */
xyz.swapee.wc.back.IOffersTableScreenATCaster.prototype.superOffersTableScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/OffersTable.mvc/design/80-IOffersTableGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IOffersTableDisplay.Initialese} xyz.swapee.wc.IOffersTableGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableGPU)} xyz.swapee.wc.AbstractOffersTableGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableGPU} xyz.swapee.wc.OffersTableGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableGPU` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableGPU
 */
xyz.swapee.wc.AbstractOffersTableGPU = class extends /** @type {xyz.swapee.wc.AbstractOffersTableGPU.constructor&xyz.swapee.wc.OffersTableGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableGPU.prototype.constructor = xyz.swapee.wc.AbstractOffersTableGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableGPU.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableGPU|typeof xyz.swapee.wc.OffersTableGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersTableDisplay|typeof xyz.swapee.wc.back.OffersTableDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.AbstractOffersTableGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableGPU.Initialese[]) => xyz.swapee.wc.IOffersTableGPU} xyz.swapee.wc.OffersTableGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersTableGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersTableGPUCaster&com.webcircuits.IBrowserView<.!OffersTableMemory,.!OffersTableLand>&xyz.swapee.wc.back.IOffersTableDisplay)} xyz.swapee.wc.IOffersTableGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!OffersTableMemory,.!OffersTableLand>} com.webcircuits.IBrowserView<.!OffersTableMemory,.!OffersTableLand>.typeof */
/**
 * Handles the periphery of the _IOffersTableDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IOffersTableGPU
 */
xyz.swapee.wc.IOffersTableGPU = class extends /** @type {xyz.swapee.wc.IOffersTableGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!OffersTableMemory,.!OffersTableLand>.typeof&xyz.swapee.wc.back.IOffersTableDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableGPU.Initialese>)} xyz.swapee.wc.OffersTableGPU.constructor */
/**
 * A concrete class of _IOffersTableGPU_ instances.
 * @constructor xyz.swapee.wc.OffersTableGPU
 * @implements {xyz.swapee.wc.IOffersTableGPU} Handles the periphery of the _IOffersTableDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableGPU.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableGPU = class extends /** @type {xyz.swapee.wc.OffersTableGPU.constructor&xyz.swapee.wc.IOffersTableGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableGPU}
 */
xyz.swapee.wc.OffersTableGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersTableGPU.
 * @interface xyz.swapee.wc.IOffersTableGPUFields
 */
xyz.swapee.wc.IOffersTableGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IOffersTableGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersTableGPU} */
xyz.swapee.wc.RecordIOffersTableGPU

/** @typedef {xyz.swapee.wc.IOffersTableGPU} xyz.swapee.wc.BoundIOffersTableGPU */

/** @typedef {xyz.swapee.wc.OffersTableGPU} xyz.swapee.wc.BoundOffersTableGPU */

/**
 * Contains getters to cast the _IOffersTableGPU_ interface.
 * @interface xyz.swapee.wc.IOffersTableGPUCaster
 */
xyz.swapee.wc.IOffersTableGPUCaster = class { }
/**
 * Cast the _IOffersTableGPU_ instance into the _BoundIOffersTableGPU_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableGPU}
 */
xyz.swapee.wc.IOffersTableGPUCaster.prototype.asIOffersTableGPU
/**
 * Access the _OffersTableGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableGPU}
 */
xyz.swapee.wc.IOffersTableGPUCaster.prototype.superOffersTableGPU

// nss:xyz.swapee.wc
/* @typal-end */