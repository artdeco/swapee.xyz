
import AbstractOffersTable from '../AbstractOffersTable'

/** @abstract {xyz.swapee.wc.IOffersTableElement} */
export default class AbstractOffersTableElement { }



AbstractOffersTableElement[$implementations]=[AbstractOffersTable,
 /** @type {!AbstractOffersTableElement} */ ({
  rootId:'OffersTable',
  __$id:1154816194,
  fqn:'xyz.swapee.wc.IOffersTable',
  maurice_element_v3:true,
 }),
]