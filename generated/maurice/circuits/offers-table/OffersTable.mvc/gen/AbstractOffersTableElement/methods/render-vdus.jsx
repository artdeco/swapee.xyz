export default function OffersTableRenderVdus(){
 return (<div $id="OffersTable">
  <vdu $id="ChangellyFixedLink" />
  <vdu $id="ChangellyFloatLink" />
  <vdu $id="CoinImWr" />
  <vdu $id="ChangellyFloatingOffer" />
  <vdu $id="ChangellyFloatingOfferAmount" />
  <vdu $id="ChangellyFixedOffer" />
  <vdu $id="ChangellyFixedOfferAmount" />
  <vdu $id="LetsExchangeFloatingOffer" />
  <vdu $id="LetsExchangeFloatingOfferAmount" />
  <vdu $id="LetsExchangeFixedOffer" />
  <vdu $id="LetsExchangeFixedOfferAmount" />
  <vdu $id="ChangeNowFloatingOffer" />
  <vdu $id="ChangeNowFloatingOfferAmount" />
  <vdu $id="ChangeNowFixedOffer" />
  <vdu $id="ChangeNowFixedOfferAmount" />
  <vdu $id="OfferCryptoOut" />
 </div>)
}