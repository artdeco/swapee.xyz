import OffersTableRenderVdus from './methods/render-vdus'
import OffersTableElementPort from '../OffersTableElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {OffersTableInputsPQs} from '../../pqs/OffersTableInputsPQs'
import {OffersTableQueriesPQs} from '../../pqs/OffersTableQueriesPQs'
import {OffersTableCachePQs} from '../../pqs/OffersTableCachePQs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractOffersTable from '../AbstractOffersTable'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableElement}
 */
function __AbstractOffersTableElement() {}
__AbstractOffersTableElement.prototype = /** @type {!_AbstractOffersTableElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableElement}
 */
class _AbstractOffersTableElement { }
/**
 * A component description.
 *
 * The _IOffersTable_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractOffersTableElement} ‎
 */
class AbstractOffersTableElement extends newAbstract(
 _AbstractOffersTableElement,115481619413,null,{
  asIOffersTableElement:1,
  superOffersTableElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableElement} */
AbstractOffersTableElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableElement} */
function AbstractOffersTableElementClass(){}

export default AbstractOffersTableElement


AbstractOffersTableElement[$implementations]=[
 __AbstractOffersTableElement,
 ElementBase,
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':until-reset':untilResetColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'until-reset':untilResetAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(untilResetAttr===undefined?{'until-reset':untilResetColAttr}:{}),
   }
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'until-reset':untilResetAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    untilReset:untilResetAttr,
   }
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 Landed,
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  render:function renderExchangeIntent(){
   const{
    asILanded:{
     land:{
      ExchangeIntent:ExchangeIntent,
     },
    },
    asIOffersTableElement:{
     buildExchangeIntent:buildExchangeIntent,
    },
   }=this
   if(!ExchangeIntent) return
   const{model:ExchangeIntentModel}=ExchangeIntent
   const{
    'cec31':fixed,
    '546ad':float,
    'c23cd':currencyTo,
   }=ExchangeIntentModel
   const res=buildExchangeIntent({
    fixed:fixed,
    float:float,
    currencyTo:currencyTo,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  render:function renderOffersAggregator(){
   const{
    asILanded:{
     land:{
      OffersAggregator:OffersAggregator,
     },
    },
    asIOffersTableElement:{
     buildOffersAggregator:buildOffersAggregator,
    },
   }=this
   if(!OffersAggregator) return
   const{model:OffersAggregatorModel}=OffersAggregator
   const{
    '77233':isAggregating,
    '7b5f4':changellyFixedOffer,
    '2a73d':changellyFloatingOffer,
    '26339':letsExchangeFixedOffer,
    'a0ea0':letsExchangeFloatingOffer,
    'b89f1':changeNowFixedOffer,
    '9f4fb':changeNowFloatingOffer,
    'b4c94':exchangesLoaded,
    '8062a':exchangesTotal,
   }=OffersAggregatorModel
   const res=buildOffersAggregator({
    isAggregating:isAggregating,
    changellyFixedOffer:changellyFixedOffer,
    changellyFloatingOffer:changellyFloatingOffer,
    letsExchangeFixedOffer:letsExchangeFixedOffer,
    letsExchangeFloatingOffer:letsExchangeFloatingOffer,
    changeNowFixedOffer:changeNowFixedOffer,
    changeNowFloatingOffer:changeNowFloatingOffer,
    exchangesLoaded:exchangesLoaded,
    exchangesTotal:exchangesTotal,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  render:function renderCryptoSelectOut(){
   const{
    asILanded:{
     land:{
      CryptoSelectOut:CryptoSelectOut,
     },
    },
    asIOffersTableElement:{
     buildCryptoSelectOut:buildCryptoSelectOut,
    },
   }=this
   if(!CryptoSelectOut) return
   const{model:CryptoSelectOutModel}=CryptoSelectOut
   const{
    '73071':selectedIcon,
   }=CryptoSelectOutModel
   const res=buildCryptoSelectOut({
    selectedIcon:selectedIcon,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.IOffersTableElement}*/({land:{
    ExchangeIntent:null,
    OffersAggregator:null,
    CryptoSelectOut:null,
   }}))
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  render:OffersTableRenderVdus,
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  classes:{
   'ColHeading': '16cc2',
   'Loading': '16bfb',
   'Hidden': '7acdf',
   'SelectedRateType': '0aa87',
   'BestOffer': 'e8977',
  },
  inputsPQs:OffersTableInputsPQs,
  queriesPQs:OffersTableQueriesPQs,
  cachePQs:OffersTableCachePQs,
  vdus:{
   'ResetIn': 'f8741',
   'ResetNowBu': 'f8742',
   'ChangellyFloatingOffer': 'f8745',
   'ChangellyFloatingOfferAmount': 'f8746',
   'ChangellyFixedOffer': 'f8747',
   'ChangellyFixedOfferAmount': 'f8748',
   'LetsExchangeFloatingOffer': 'f8749',
   'LetsExchangeFloatingOfferAmount': 'f87410',
   'LetsExchangeFixedOffer': 'f87411',
   'LetsExchangeFixedOfferAmount': 'f87412',
   'ChangeNowFloatingOffer': 'f87413',
   'ChangeNowFloatingOfferAmount': 'f87414',
   'ChangeNowFixedOffer': 'f87415',
   'ChangeNowFixedOfferAmount': 'f87416',
   'ExchangesLoaded': 'f87418',
   'ExchangesTotal': 'f87419',
   'ProgressCollapsar': 'f87420',
   'ExchangeIntent': 'f87421',
   'OffersAggregator': 'f87422',
   'CryptoSelectOut': 'f87424',
   'ChangellyFloatLink': 'f87426',
   'ChangellyFixedLink': 'f87427',
   'OfferCryptoOut': 'f87417',
   'CoinImWr': 'f87425',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','untilReset','query:reset-in','query:reset-now-bu','query:exchanges-loaded','query:exchanges-total','query:progress-collapsar','query:crypto-select-out','query:exchange-intent','query:offers-aggregator','no-solder',':no-solder','until-reset',':until-reset','fe646','c35d8','e15aa','80629','8a535','089c5','a32d6','42779','3bfbc','79b2f','d88ba','cba82','b6e73','24d49','72a1a','40ae4','b78f0','5d9e2','99ad5','a75c0','b0b27','c1fc0','700bd','c3b6b','08eb7','5bbee','children']),
   })
  },
  get Port(){
   return OffersTableElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:reset-in':resetInSel,'query:reset-now-bu':resetNowBuSel,'query:exchanges-loaded':exchangesLoadedSel,'query:exchanges-total':exchangesTotalSel,'query:progress-collapsar':progressCollapsarSel,'query:crypto-select-out':cryptoSelectOutSel,'query:exchange-intent':exchangeIntentSel,'query:offers-aggregator':offersAggregatorSel}){
   const _ret={}
   if(resetInSel) _ret.resetInSel=resetInSel
   if(resetNowBuSel) _ret.resetNowBuSel=resetNowBuSel
   if(exchangesLoadedSel) _ret.exchangesLoadedSel=exchangesLoadedSel
   if(exchangesTotalSel) _ret.exchangesTotalSel=exchangesTotalSel
   if(progressCollapsarSel) _ret.progressCollapsarSel=progressCollapsarSel
   if(cryptoSelectOutSel) _ret.cryptoSelectOutSel=cryptoSelectOutSel
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   if(offersAggregatorSel) _ret.offersAggregatorSel=offersAggregatorSel
   return _ret
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
    OffersAggregator:null,
    ProgressCollapsar:null,
    CryptoSelectOut:null,
   }
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  calibrate:async function awaitOnProgressCollapsar({progressCollapsarSel:progressCollapsarSel}){
   if(!progressCollapsarSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ProgressCollapsar=await milleu(progressCollapsarSel)
   if(!ProgressCollapsar) {
    console.warn('❗️ progressCollapsarSel %s must be present on the page for %s to work',progressCollapsarSel,fqn)
    return{}
   }
   land.ProgressCollapsar=ProgressCollapsar
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  calibrate:async function awaitOnCryptoSelectOut({cryptoSelectOutSel:cryptoSelectOutSel}){
   if(!cryptoSelectOutSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const CryptoSelectOut=await milleu(cryptoSelectOutSel)
   if(!CryptoSelectOut) {
    console.warn('❗️ cryptoSelectOutSel %s must be present on the page for %s to work',cryptoSelectOutSel,fqn)
    return{}
   }
   land.CryptoSelectOut=CryptoSelectOut
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  calibrate:async function awaitOnOffersAggregator({offersAggregatorSel:offersAggregatorSel}){
   if(!offersAggregatorSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const OffersAggregator=await milleu(offersAggregatorSel,true)
   if(!OffersAggregator) {
    console.warn('❗️ offersAggregatorSel %s must be present on the page for %s to work',offersAggregatorSel,fqn)
    return{}
   }
   land.OffersAggregator=OffersAggregator
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  solder:(_,{
   resetInSel:resetInSel,
   resetNowBuSel:resetNowBuSel,
   exchangesLoadedSel:exchangesLoadedSel,
   exchangesTotalSel:exchangesTotalSel,
   progressCollapsarSel:progressCollapsarSel,
   cryptoSelectOutSel:cryptoSelectOutSel,
   exchangeIntentSel:exchangeIntentSel,
   offersAggregatorSel:offersAggregatorSel,
  })=>{
   return{
    resetInSel:resetInSel,
    resetNowBuSel:resetNowBuSel,
    exchangesLoadedSel:exchangesLoadedSel,
    exchangesTotalSel:exchangesTotalSel,
    progressCollapsarSel:progressCollapsarSel,
    cryptoSelectOutSel:cryptoSelectOutSel,
    exchangeIntentSel:exchangeIntentSel,
    offersAggregatorSel:offersAggregatorSel,
   }
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  calibrate({"splendid":splendid}){
   if(!splendid['__$borrowed:classes']){
    splendid['__$borrowed:classes']=new Map
   }
   const borrowedClasses=splendid['__$borrowed:classes']
   borrowedClasses.set(this.__$id,[8085433869,new Map([
    ['BestOffer','BestOffer'],
   ]),this])
  },
 }),
 AbstractOffersTableElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
  constructor(){
   schedule(this,()=>{
    const{asIGuest:{asIMaurice:{queryVdu:queryVdu}}}=this
    const{resetInSel:resetInSel,resetNowBuSel:resetNowBuSel,exchangesLoadedSel:exchangesLoadedSel,exchangesTotalSel:exchangesTotalSel}=this.inputs
    this.ResetIn=queryVdu(resetInSel,false)
    this.ResetNowBu=queryVdu(resetNowBuSel,false)
    this.ExchangesLoaded=queryVdu(exchangesLoadedSel,false)
    this.ExchangesTotal=queryVdu(exchangesTotalSel,false)
    this.externalVdus={
     'ResetIn':resetInSel?this.ResetIn:void 0,
     'ResetNowBu':resetNowBuSel?this.ResetNowBu:void 0,
     'ExchangesLoaded':exchangesLoadedSel?this.ExchangesLoaded:void 0,
     'ExchangesTotal':exchangesTotalSel?this.ExchangesTotal:void 0,
    }
   })
  },
 }),
]



AbstractOffersTableElement[$implementations]=[AbstractOffersTable,
 /** @type {!AbstractOffersTableElement} */ ({
  rootId:'OffersTable',
  __$id:1154816194,
  fqn:'xyz.swapee.wc.IOffersTable',
  maurice_element_v3:true,
 }),
]