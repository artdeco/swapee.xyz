import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableScreenAT}
 */
function __AbstractOffersTableScreenAT() {}
__AbstractOffersTableScreenAT.prototype = /** @type {!_AbstractOffersTableScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersTableScreenAT}
 */
class _AbstractOffersTableScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOffersTableScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractOffersTableScreenAT} ‎
 */
class AbstractOffersTableScreenAT extends newAbstract(
 _AbstractOffersTableScreenAT,115481619434,null,{
  asIOffersTableScreenAT:1,
  superOffersTableScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableScreenAT} */
AbstractOffersTableScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableScreenAT} */
function AbstractOffersTableScreenATClass(){}

export default AbstractOffersTableScreenAT


AbstractOffersTableScreenAT[$implementations]=[
 __AbstractOffersTableScreenAT,
 UartUniversal,
]