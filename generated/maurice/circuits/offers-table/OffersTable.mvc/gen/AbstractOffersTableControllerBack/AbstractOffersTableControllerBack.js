import AbstractOffersTableControllerAR from '../AbstractOffersTableControllerAR'
import {AbstractOffersTableController} from '../AbstractOffersTableController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableControllerBack}
 */
function __AbstractOffersTableControllerBack() {}
__AbstractOffersTableControllerBack.prototype = /** @type {!_AbstractOffersTableControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersTableController}
 */
class _AbstractOffersTableControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractOffersTableController} ‎
 */
class AbstractOffersTableControllerBack extends newAbstract(
 _AbstractOffersTableControllerBack,115481619428,null,{
  asIOffersTableController:1,
  superOffersTableController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableController} */
AbstractOffersTableControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableController} */
function AbstractOffersTableControllerBackClass(){}

export default AbstractOffersTableControllerBack


AbstractOffersTableControllerBack[$implementations]=[
 __AbstractOffersTableControllerBack,
 AbstractOffersTableController,
 AbstractOffersTableControllerAR,
 DriverBack,
]