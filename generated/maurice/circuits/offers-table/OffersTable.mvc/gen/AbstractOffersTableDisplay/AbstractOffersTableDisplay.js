import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableDisplay}
 */
function __AbstractOffersTableDisplay() {}
__AbstractOffersTableDisplay.prototype = /** @type {!_AbstractOffersTableDisplay} */ ({ })
/** @this {xyz.swapee.wc.OffersTableDisplay} */ function OffersTableDisplayConstructor() {
  /** @type {!Array<!HTMLElement>} */ this.CoinImWrs=[]
  /** @type {HTMLDivElement} */ this.ChangellyFloatingOffer=null
  /** @type {HTMLSpanElement} */ this.ChangellyFloatingOfferAmount=null
  /** @type {HTMLAnchorElement} */ this.ChangellyFloatLink=null
  /** @type {HTMLDivElement} */ this.ChangellyFixedOffer=null
  /** @type {HTMLSpanElement} */ this.ChangellyFixedOfferAmount=null
  /** @type {HTMLAnchorElement} */ this.ChangellyFixedLink=null
  /** @type {HTMLDivElement} */ this.LetsExchangeFloatingOffer=null
  /** @type {HTMLSpanElement} */ this.LetsExchangeFloatingOfferAmount=null
  /** @type {HTMLDivElement} */ this.LetsExchangeFixedOffer=null
  /** @type {HTMLSpanElement} */ this.LetsExchangeFixedOfferAmount=null
  /** @type {HTMLDivElement} */ this.ChangeNowFloatingOffer=null
  /** @type {HTMLSpanElement} */ this.ChangeNowFloatingOfferAmount=null
  /** @type {HTMLDivElement} */ this.ChangeNowFixedOffer=null
  /** @type {HTMLSpanElement} */ this.ChangeNowFixedOfferAmount=null
  /** @type {!Array<!HTMLSpanElement>} */ this.OfferCryptoOuts=[]
  /** @type {HTMLSpanElement} */ this.ResetIn=null
  /** @type {HTMLSpanElement} */ this.ResetNowBu=null
  /** @type {HTMLSpanElement} */ this.ExchangesLoaded=null
  /** @type {HTMLSpanElement} */ this.ExchangesTotal=null
  /** @type {HTMLSpanElement} */ this.ProgressCollapsar=null
  /** @type {HTMLSpanElement} */ this.CryptoSelectOut=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
  /** @type {HTMLElement} */ this.OffersAggregator=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableDisplay}
 */
class _AbstractOffersTableDisplay { }
/**
 * Display for presenting information from the _IOffersTable_.
 * @extends {xyz.swapee.wc.AbstractOffersTableDisplay} ‎
 */
class AbstractOffersTableDisplay extends newAbstract(
 _AbstractOffersTableDisplay,115481619417,OffersTableDisplayConstructor,{
  asIOffersTableDisplay:1,
  superOffersTableDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableDisplay} */
AbstractOffersTableDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableDisplay} */
function AbstractOffersTableDisplayClass(){}

export default AbstractOffersTableDisplay


AbstractOffersTableDisplay[$implementations]=[
 __AbstractOffersTableDisplay,
 Display,
 AbstractOffersTableDisplayClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{resetInScopeSel:resetInScopeSel,resetNowBuScopeSel:resetNowBuScopeSel,exchangesLoadedScopeSel:exchangesLoadedScopeSel,exchangesTotalScopeSel:exchangesTotalScopeSel,progressCollapsarScopeSel:progressCollapsarScopeSel,cryptoSelectOutScopeSel:cryptoSelectOutScopeSel,exchangeIntentScopeSel:exchangeIntentScopeSel,offersAggregatorScopeSel:offersAggregatorScopeSel}}=this
    this.scan({
     resetInSel:resetInScopeSel,
     resetNowBuSel:resetNowBuScopeSel,
     exchangesLoadedSel:exchangesLoadedScopeSel,
     exchangesTotalSel:exchangesTotalScopeSel,
     progressCollapsarSel:progressCollapsarScopeSel,
     cryptoSelectOutSel:cryptoSelectOutScopeSel,
     exchangeIntentSel:exchangeIntentScopeSel,
     offersAggregatorSel:offersAggregatorScopeSel,
    })
   })
  },
  scan:function vduScan({resetInSel:resetInSelScope,resetNowBuSel:resetNowBuSelScope,exchangesLoadedSel:exchangesLoadedSelScope,exchangesTotalSel:exchangesTotalSelScope,progressCollapsarSel:progressCollapsarSelScope,cryptoSelectOutSel:cryptoSelectOutSelScope,exchangeIntentSel:exchangeIntentSelScope,offersAggregatorSel:offersAggregatorSelScope}){
   const{element:element,asIOffersTableScreen:{vdusPQs:{
    ChangellyFixedLink:ChangellyFixedLink,
    ChangellyFloatLink:ChangellyFloatLink,
    ChangellyFloatingOffer:ChangellyFloatingOffer,
    ChangellyFloatingOfferAmount:ChangellyFloatingOfferAmount,
    ChangellyFixedOffer:ChangellyFixedOffer,
    ChangellyFixedOfferAmount:ChangellyFixedOfferAmount,
    LetsExchangeFloatingOffer:LetsExchangeFloatingOffer,
    LetsExchangeFloatingOfferAmount:LetsExchangeFloatingOfferAmount,
    LetsExchangeFixedOffer:LetsExchangeFixedOffer,
    LetsExchangeFixedOfferAmount:LetsExchangeFixedOfferAmount,
    ChangeNowFloatingOffer:ChangeNowFloatingOffer,
    ChangeNowFloatingOfferAmount:ChangeNowFloatingOfferAmount,
    ChangeNowFixedOffer:ChangeNowFixedOffer,
    ChangeNowFixedOfferAmount:ChangeNowFixedOfferAmount,
   }},queries:{resetInSel,resetNowBuSel,exchangesLoadedSel,exchangesTotalSel,progressCollapsarSel,cryptoSelectOutSel,exchangeIntentSel,offersAggregatorSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLSpanElement}*/
   const ResetIn=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ResetIn=resetInSel?root.querySelector(resetInSel):void 0
    return _ResetIn
   })(resetInSelScope)
   /**@type {HTMLSpanElement}*/
   const ResetNowBu=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ResetNowBu=resetNowBuSel?root.querySelector(resetNowBuSel):void 0
    return _ResetNowBu
   })(resetNowBuSelScope)
   /**@type {HTMLSpanElement}*/
   const ExchangesLoaded=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangesLoaded=exchangesLoadedSel?root.querySelector(exchangesLoadedSel):void 0
    return _ExchangesLoaded
   })(exchangesLoadedSelScope)
   /**@type {HTMLSpanElement}*/
   const ExchangesTotal=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangesTotal=exchangesTotalSel?root.querySelector(exchangesTotalSel):void 0
    return _ExchangesTotal
   })(exchangesTotalSelScope)
   /**@type {HTMLSpanElement}*/
   const ProgressCollapsar=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ProgressCollapsar=progressCollapsarSel?root.querySelector(progressCollapsarSel):void 0
    return _ProgressCollapsar
   })(progressCollapsarSelScope)
   /**@type {HTMLSpanElement}*/
   const CryptoSelectOut=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _CryptoSelectOut=cryptoSelectOutSel?root.querySelector(cryptoSelectOutSel):void 0
    return _CryptoSelectOut
   })(cryptoSelectOutSelScope)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   /**@type {HTMLElement}*/
   const OffersAggregator=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _OffersAggregator=offersAggregatorSel?root.querySelector(offersAggregatorSel):void 0
    return _OffersAggregator
   })(offersAggregatorSelScope)
   Object.assign(this,{
    ChangellyFixedLink:/**@type {HTMLAnchorElement}*/(children[ChangellyFixedLink]),
    ChangellyFloatLink:/**@type {HTMLAnchorElement}*/(children[ChangellyFloatLink]),
    ResetIn:ResetIn,
    ResetNowBu:ResetNowBu,
    CoinImWrs:element?/**@type {!Array<!HTMLElement>}*/([...element.querySelectorAll('[data-id~=f87425]')]):[],
    ChangellyFloatingOffer:/**@type {HTMLDivElement}*/(children[ChangellyFloatingOffer]),
    ChangellyFloatingOfferAmount:/**@type {HTMLSpanElement}*/(children[ChangellyFloatingOfferAmount]),
    ChangellyFixedOffer:/**@type {HTMLDivElement}*/(children[ChangellyFixedOffer]),
    ChangellyFixedOfferAmount:/**@type {HTMLSpanElement}*/(children[ChangellyFixedOfferAmount]),
    LetsExchangeFloatingOffer:/**@type {HTMLDivElement}*/(children[LetsExchangeFloatingOffer]),
    LetsExchangeFloatingOfferAmount:/**@type {HTMLSpanElement}*/(children[LetsExchangeFloatingOfferAmount]),
    LetsExchangeFixedOffer:/**@type {HTMLDivElement}*/(children[LetsExchangeFixedOffer]),
    LetsExchangeFixedOfferAmount:/**@type {HTMLSpanElement}*/(children[LetsExchangeFixedOfferAmount]),
    ChangeNowFloatingOffer:/**@type {HTMLDivElement}*/(children[ChangeNowFloatingOffer]),
    ChangeNowFloatingOfferAmount:/**@type {HTMLSpanElement}*/(children[ChangeNowFloatingOfferAmount]),
    ChangeNowFixedOffer:/**@type {HTMLDivElement}*/(children[ChangeNowFixedOffer]),
    ChangeNowFixedOfferAmount:/**@type {HTMLSpanElement}*/(children[ChangeNowFixedOfferAmount]),
    OfferCryptoOuts:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=f87417]')]):[],
    ExchangesLoaded:ExchangesLoaded,
    ExchangesTotal:ExchangesTotal,
    ProgressCollapsar:ProgressCollapsar,
    CryptoSelectOut:CryptoSelectOut,
    ExchangeIntent:ExchangeIntent,
    OffersAggregator:OffersAggregator,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.OffersTableDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IOffersTableDisplay.Initialese}*/({
   CoinImWrs:1,
   ChangellyFloatingOffer:1,
   ChangellyFloatingOfferAmount:1,
   ChangellyFloatLink:1,
   ChangellyFixedOffer:1,
   ChangellyFixedOfferAmount:1,
   ChangellyFixedLink:1,
   LetsExchangeFloatingOffer:1,
   LetsExchangeFloatingOfferAmount:1,
   LetsExchangeFixedOffer:1,
   LetsExchangeFixedOfferAmount:1,
   ChangeNowFloatingOffer:1,
   ChangeNowFloatingOfferAmount:1,
   ChangeNowFixedOffer:1,
   ChangeNowFixedOfferAmount:1,
   OfferCryptoOuts:1,
   ResetIn:1,
   ResetNowBu:1,
   ExchangesLoaded:1,
   ExchangesTotal:1,
   ProgressCollapsar:1,
   CryptoSelectOut:1,
   ExchangeIntent:1,
   OffersAggregator:1,
  }),
  initializer({
   CoinImWrs:_CoinImWrs,
   ChangellyFloatingOffer:_ChangellyFloatingOffer,
   ChangellyFloatingOfferAmount:_ChangellyFloatingOfferAmount,
   ChangellyFloatLink:_ChangellyFloatLink,
   ChangellyFixedOffer:_ChangellyFixedOffer,
   ChangellyFixedOfferAmount:_ChangellyFixedOfferAmount,
   ChangellyFixedLink:_ChangellyFixedLink,
   LetsExchangeFloatingOffer:_LetsExchangeFloatingOffer,
   LetsExchangeFloatingOfferAmount:_LetsExchangeFloatingOfferAmount,
   LetsExchangeFixedOffer:_LetsExchangeFixedOffer,
   LetsExchangeFixedOfferAmount:_LetsExchangeFixedOfferAmount,
   ChangeNowFloatingOffer:_ChangeNowFloatingOffer,
   ChangeNowFloatingOfferAmount:_ChangeNowFloatingOfferAmount,
   ChangeNowFixedOffer:_ChangeNowFixedOffer,
   ChangeNowFixedOfferAmount:_ChangeNowFixedOfferAmount,
   OfferCryptoOuts:_OfferCryptoOuts,
   ResetIn:_ResetIn,
   ResetNowBu:_ResetNowBu,
   ExchangesLoaded:_ExchangesLoaded,
   ExchangesTotal:_ExchangesTotal,
   ProgressCollapsar:_ProgressCollapsar,
   CryptoSelectOut:_CryptoSelectOut,
   ExchangeIntent:_ExchangeIntent,
   OffersAggregator:_OffersAggregator,
  }) {
   if(_CoinImWrs!==undefined) this.CoinImWrs=_CoinImWrs
   if(_ChangellyFloatingOffer!==undefined) this.ChangellyFloatingOffer=_ChangellyFloatingOffer
   if(_ChangellyFloatingOfferAmount!==undefined) this.ChangellyFloatingOfferAmount=_ChangellyFloatingOfferAmount
   if(_ChangellyFloatLink!==undefined) this.ChangellyFloatLink=_ChangellyFloatLink
   if(_ChangellyFixedOffer!==undefined) this.ChangellyFixedOffer=_ChangellyFixedOffer
   if(_ChangellyFixedOfferAmount!==undefined) this.ChangellyFixedOfferAmount=_ChangellyFixedOfferAmount
   if(_ChangellyFixedLink!==undefined) this.ChangellyFixedLink=_ChangellyFixedLink
   if(_LetsExchangeFloatingOffer!==undefined) this.LetsExchangeFloatingOffer=_LetsExchangeFloatingOffer
   if(_LetsExchangeFloatingOfferAmount!==undefined) this.LetsExchangeFloatingOfferAmount=_LetsExchangeFloatingOfferAmount
   if(_LetsExchangeFixedOffer!==undefined) this.LetsExchangeFixedOffer=_LetsExchangeFixedOffer
   if(_LetsExchangeFixedOfferAmount!==undefined) this.LetsExchangeFixedOfferAmount=_LetsExchangeFixedOfferAmount
   if(_ChangeNowFloatingOffer!==undefined) this.ChangeNowFloatingOffer=_ChangeNowFloatingOffer
   if(_ChangeNowFloatingOfferAmount!==undefined) this.ChangeNowFloatingOfferAmount=_ChangeNowFloatingOfferAmount
   if(_ChangeNowFixedOffer!==undefined) this.ChangeNowFixedOffer=_ChangeNowFixedOffer
   if(_ChangeNowFixedOfferAmount!==undefined) this.ChangeNowFixedOfferAmount=_ChangeNowFixedOfferAmount
   if(_OfferCryptoOuts!==undefined) this.OfferCryptoOuts=_OfferCryptoOuts
   if(_ResetIn!==undefined) this.ResetIn=_ResetIn
   if(_ResetNowBu!==undefined) this.ResetNowBu=_ResetNowBu
   if(_ExchangesLoaded!==undefined) this.ExchangesLoaded=_ExchangesLoaded
   if(_ExchangesTotal!==undefined) this.ExchangesTotal=_ExchangesTotal
   if(_ProgressCollapsar!==undefined) this.ProgressCollapsar=_ProgressCollapsar
   if(_CryptoSelectOut!==undefined) this.CryptoSelectOut=_CryptoSelectOut
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_OffersAggregator!==undefined) this.OffersAggregator=_OffersAggregator
  },
 }),
]