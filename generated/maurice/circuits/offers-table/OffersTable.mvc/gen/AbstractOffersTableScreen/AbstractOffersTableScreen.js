import OffersTableClassesPQs from '../../pqs/OffersTableClassesPQs'
import AbstractOffersTableScreenAR from '../AbstractOffersTableScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {OffersTableInputsPQs} from '../../pqs/OffersTableInputsPQs'
import {OffersTableQueriesPQs} from '../../pqs/OffersTableQueriesPQs'
import {OffersTableMemoryQPs} from '../../pqs/OffersTableMemoryQPs'
import {OffersTableCacheQPs} from '../../pqs/OffersTableCacheQPs'
import {OffersTableVdusPQs} from '../../pqs/OffersTableVdusPQs'
import {OffersTableClassesQPs} from '../../pqs/OffersTableClassesQPs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableScreen}
 */
function __AbstractOffersTableScreen() {}
__AbstractOffersTableScreen.prototype = /** @type {!_AbstractOffersTableScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableScreen}
 */
class _AbstractOffersTableScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractOffersTableScreen} ‎
 */
class AbstractOffersTableScreen extends newAbstract(
 _AbstractOffersTableScreen,115481619431,null,{
  asIOffersTableScreen:1,
  superOffersTableScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableScreen} */
AbstractOffersTableScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableScreen} */
function AbstractOffersTableScreenClass(){}

export default AbstractOffersTableScreen


AbstractOffersTableScreen[$implementations]=[
 __AbstractOffersTableScreen,
 AbstractOffersTableScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableScreen}*/({
  deduceInputs(){
   const{asIOffersTableDisplay:{
    ResetIn:ResetIn,
   }}=this
   return{
    untilReset:ResetIn?.innerText,
   }
  },
 }),
 AbstractOffersTableScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableScreen}*/({
  inputsPQs:OffersTableInputsPQs,
  classesPQs:OffersTableClassesPQs,
  queriesPQs:OffersTableQueriesPQs,
  memoryQPs:OffersTableMemoryQPs,
  cacheQPs:OffersTableCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractOffersTableScreenAR,
 AbstractOffersTableScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableScreen}*/({
  vdusPQs:OffersTableVdusPQs,
 }),
 AbstractOffersTableScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableScreen}*/({
  classesQPs:OffersTableClassesQPs,
 }),
 AbstractOffersTableScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableScreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const ResetNowBu=this.ResetNowBu
    if(ResetNowBu){
     ResetNowBu.addEventListener('click',(ev)=>{
      this.reset()
     })
    }
   })
  },
 }),
]