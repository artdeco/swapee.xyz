import AbstractOffersTableProcessor from '../AbstractOffersTableProcessor'
import {OffersTableCore} from '../OffersTableCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractOffersTableComputer} from '../AbstractOffersTableComputer'
import {AbstractOffersTableController} from '../AbstractOffersTableController'
import {regulateOffersTableCache} from './methods/regulateOffersTableCache'
import {OffersTableCacheQPs} from '../../pqs/OffersTableCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTable}
 */
function __AbstractOffersTable() {}
__AbstractOffersTable.prototype = /** @type {!_AbstractOffersTable} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTable}
 */
class _AbstractOffersTable { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractOffersTable} ‎
 */
class AbstractOffersTable extends newAbstract(
 _AbstractOffersTable,11548161949,null,{
  asIOffersTable:1,
  superOffersTable:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTable} */
AbstractOffersTable.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTable} */
function AbstractOffersTableClass(){}

export default AbstractOffersTable


AbstractOffersTable[$implementations]=[
 __AbstractOffersTable,
 OffersTableCore,
 AbstractOffersTableProcessor,
 IntegratedComponent,
 AbstractOffersTableComputer,
 AbstractOffersTableController,
 AbstractOffersTableClass.prototype=/**@type {!xyz.swapee.wc.IOffersTable}*/({
  regulateState:regulateOffersTableCache,
  stateQPs:OffersTableCacheQPs,
 }),
]


export {AbstractOffersTable}