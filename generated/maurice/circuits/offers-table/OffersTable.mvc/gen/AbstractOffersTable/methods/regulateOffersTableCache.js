import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateOffersTableCache=makeBuffers({
 changellyFixedLink:String,
 changellyFloatLink:String,
},{silent:true})