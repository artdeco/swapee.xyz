import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableControllerAT}
 */
function __AbstractOffersTableControllerAT() {}
__AbstractOffersTableControllerAT.prototype = /** @type {!_AbstractOffersTableControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOffersTableControllerAT}
 */
class _AbstractOffersTableControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOffersTableControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractOffersTableControllerAT} ‎
 */
class AbstractOffersTableControllerAT extends newAbstract(
 _AbstractOffersTableControllerAT,115481619430,null,{
  asIOffersTableControllerAT:1,
  superOffersTableControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOffersTableControllerAT} */
AbstractOffersTableControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOffersTableControllerAT} */
function AbstractOffersTableControllerATClass(){}

export default AbstractOffersTableControllerAT


AbstractOffersTableControllerAT[$implementations]=[
 __AbstractOffersTableControllerAT,
 UartUniversal,
 AbstractOffersTableControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IOffersTableControllerAT}*/({
  get asIOffersTableController(){
   return this
  },
  resetTick(){
   this.uart.t("inv",{mid:'3ccb7'})
  },
  reset(){
   this.uart.t("inv",{mid:'f6174'})
  },
  onReset(){
   this.uart.t("inv",{mid:'ad42e'})
  },
 }),
]