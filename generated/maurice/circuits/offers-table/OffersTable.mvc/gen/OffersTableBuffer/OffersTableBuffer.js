import {makeBuffers} from '@webcircuits/webcircuits'

export const OffersTableBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  untilReset:Number,
 }),
})

export default OffersTableBuffer