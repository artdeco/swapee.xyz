import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableProcessor}
 */
function __AbstractOffersTableProcessor() {}
__AbstractOffersTableProcessor.prototype = /** @type {!_AbstractOffersTableProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableProcessor}
 */
class _AbstractOffersTableProcessor { }
/**
 * The processor to compute changes to the memory for the _IOffersTable_.
 * @extends {xyz.swapee.wc.AbstractOffersTableProcessor} ‎
 */
class AbstractOffersTableProcessor extends newAbstract(
 _AbstractOffersTableProcessor,11548161948,null,{
  asIOffersTableProcessor:1,
  superOffersTableProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableProcessor} */
AbstractOffersTableProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableProcessor} */
function AbstractOffersTableProcessorClass(){}

export default AbstractOffersTableProcessor


AbstractOffersTableProcessor[$implementations]=[
 __AbstractOffersTableProcessor,
]