import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersTableElementPort}
 */
function __OffersTableElementPort() {}
__OffersTableElementPort.prototype = /** @type {!_OffersTableElementPort} */ ({ })
/** @this {xyz.swapee.wc.OffersTableElementPort} */ function OffersTableElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IOffersTableElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    coinImWrOpts: {},
    changellyFloatingOfferOpts: {},
    changellyFloatingOfferAmountOpts: {},
    changellyFloatLinkOpts: {},
    changellyFixedOfferOpts: {},
    changellyFixedOfferAmountOpts: {},
    changellyFixedLinkOpts: {},
    letsExchangeFloatingOfferOpts: {},
    letsExchangeFloatingOfferAmountOpts: {},
    letsExchangeFixedOfferOpts: {},
    letsExchangeFixedOfferAmountOpts: {},
    changeNowFloatingOfferOpts: {},
    changeNowFloatingOfferAmountOpts: {},
    changeNowFixedOfferOpts: {},
    changeNowFixedOfferAmountOpts: {},
    offerCryptoOutOpts: {},
    resetInOpts: {},
    resetNowBuOpts: {},
    exchangesLoadedOpts: {},
    exchangesTotalOpts: {},
    progressCollapsarOpts: {},
    cryptoSelectOutOpts: {},
    exchangeIntentOpts: {},
    offersAggregatorOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableElementPort}
 */
class _OffersTableElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractOffersTableElementPort} ‎
 */
class OffersTableElementPort extends newAbstract(
 _OffersTableElementPort,115481619414,OffersTableElementPortConstructor,{
  asIOffersTableElementPort:1,
  superOffersTableElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableElementPort} */
OffersTableElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableElementPort} */
function OffersTableElementPortClass(){}

export default OffersTableElementPort


OffersTableElementPort[$implementations]=[
 __OffersTableElementPort,
 OffersTableElementPortClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'coin-im-wr-opts':undefined,
    'changelly-floating-offer-opts':undefined,
    'changelly-floating-offer-amount-opts':undefined,
    'changelly-float-link-opts':undefined,
    'changelly-fixed-offer-opts':undefined,
    'changelly-fixed-offer-amount-opts':undefined,
    'changelly-fixed-link-opts':undefined,
    'lets-exchange-floating-offer-opts':undefined,
    'lets-exchange-floating-offer-amount-opts':undefined,
    'lets-exchange-fixed-offer-opts':undefined,
    'lets-exchange-fixed-offer-amount-opts':undefined,
    'change-now-floating-offer-opts':undefined,
    'change-now-floating-offer-amount-opts':undefined,
    'change-now-fixed-offer-opts':undefined,
    'change-now-fixed-offer-amount-opts':undefined,
    'offer-crypto-out-opts':undefined,
    'reset-in-opts':undefined,
    'reset-now-bu-opts':undefined,
    'exchanges-loaded-opts':undefined,
    'exchanges-total-opts':undefined,
    'progress-collapsar-opts':undefined,
    'crypto-select-out-opts':undefined,
    'exchange-intent-opts':undefined,
    'offers-aggregator-opts':undefined,
    'until-reset':undefined,
   })
  },
 }),
]