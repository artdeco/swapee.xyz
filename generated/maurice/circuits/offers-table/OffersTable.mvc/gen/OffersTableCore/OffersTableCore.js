import {mountPins} from '@type.engineering/seers'
import {OffersTableMemoryPQs} from '../../pqs/OffersTableMemoryPQs'
import {OffersTableCachePQs} from '../../pqs/OffersTableCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersTableCore}
 */
function __OffersTableCore() {}
__OffersTableCore.prototype = /** @type {!_OffersTableCore} */ ({ })
/** @this {xyz.swapee.wc.OffersTableCore} */ function OffersTableCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOffersTableCore.Model}*/
  this.model={
    changellyFixedLink: '',
    changellyFloatLink: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableCore}
 */
class _OffersTableCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractOffersTableCore} ‎
 */
class OffersTableCore extends newAbstract(
 _OffersTableCore,11548161947,OffersTableCoreConstructor,{
  asIOffersTableCore:1,
  superOffersTableCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableCore} */
OffersTableCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableCore} */
function OffersTableCoreClass(){}

export default OffersTableCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersTableOuterCore}
 */
function __OffersTableOuterCore() {}
__OffersTableOuterCore.prototype = /** @type {!_OffersTableOuterCore} */ ({ })
/** @this {xyz.swapee.wc.OffersTableOuterCore} */
export function OffersTableOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOffersTableOuterCore.Model}*/
  this.model={
    untilReset: 90,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableOuterCore}
 */
class _OffersTableOuterCore { }
/**
 * The _IOffersTable_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractOffersTableOuterCore} ‎
 */
export class OffersTableOuterCore extends newAbstract(
 _OffersTableOuterCore,11548161943,OffersTableOuterCoreConstructor,{
  asIOffersTableOuterCore:1,
  superOffersTableOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableOuterCore} */
OffersTableOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableOuterCore} */
function OffersTableOuterCoreClass(){}


OffersTableOuterCore[$implementations]=[
 __OffersTableOuterCore,
 OffersTableOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableOuterCore}*/({
  constructor(){
   mountPins(this.model,OffersTableMemoryPQs)
   mountPins(this.model,OffersTableCachePQs)
  },
 }),
]

OffersTableCore[$implementations]=[
 OffersTableCoreClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableCore}*/({
  resetCore(){
   this.resetOffersTableCore()
  },
  resetOffersTableCore(){
   OffersTableCoreConstructor.call(
    /**@type {xyz.swapee.wc.OffersTableCore}*/(this),
   )
   OffersTableOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.OffersTableOuterCore}*/(
     /**@type {!xyz.swapee.wc.IOffersTableOuterCore}*/(this)),
   )
  },
 }),
 __OffersTableCore,
 OffersTableOuterCore,
]

export {OffersTableCore}