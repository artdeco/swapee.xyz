import AbstractOffersTableGPU from '../AbstractOffersTableGPU'
import AbstractOffersTableScreenBack from '../AbstractOffersTableScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {OffersTableInputsQPs} from '../../pqs/OffersTableInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractOffersTable from '../AbstractOffersTable'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableHtmlComponent}
 */
function __AbstractOffersTableHtmlComponent() {}
__AbstractOffersTableHtmlComponent.prototype = /** @type {!_AbstractOffersTableHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableHtmlComponent}
 */
class _AbstractOffersTableHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.OffersTableHtmlComponent} */ (res)
  }
}
/**
 * The _IOffersTable_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractOffersTableHtmlComponent} ‎
 */
export class AbstractOffersTableHtmlComponent extends newAbstract(
 _AbstractOffersTableHtmlComponent,115481619412,null,{
  asIOffersTableHtmlComponent:1,
  superOffersTableHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableHtmlComponent} */
AbstractOffersTableHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableHtmlComponent} */
function AbstractOffersTableHtmlComponentClass(){}


AbstractOffersTableHtmlComponent[$implementations]=[
 __AbstractOffersTableHtmlComponent,
 HtmlComponent,
 AbstractOffersTable,
 AbstractOffersTableGPU,
 AbstractOffersTableScreenBack,
 Landed,
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
    OffersAggregator:null,
    ProgressCollapsar:null,
    CryptoSelectOut:null,
   }
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  inputsQPs:OffersTableInputsQPs,
 }),

/** @type {!AbstractOffersTableHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IOffersTableHtmlComponent}*/function paintResetIn() {
   this.ResetIn.setText(this.model.untilReset)
  }
 ] }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_href_on_ChangellyFixedLink({changellyFixedLink:changellyFixedLink}){
   const{asIOffersTableGPU:{ChangellyFixedLink:ChangellyFixedLink},asIBrowserView:{attr:attr}}=this
   attr(ChangellyFixedLink,'href',changellyFixedLink)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_href_on_ChangellyFloatLink({changellyFloatLink:changellyFloatLink}){
   const{asIOffersTableGPU:{ChangellyFloatLink:ChangellyFloatLink},asIBrowserView:{attr:attr}}=this
   attr(ChangellyFloatLink,'href',changellyFloatLink)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_OfferCryptoOuts_Content(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'c23cd':currencyTo,
   }=ExchangeIntent.model
   const{asIOffersTableGPU:{OfferCryptoOuts:OfferCryptoOuts}}=this
   for(const OfferCryptoOut of OfferCryptoOuts){
    OfferCryptoOut.setText(currencyTo)
   }
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_Hidden_on_ChangellyFloatingOffer(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'cec31':fixed,
   }=ExchangeIntent.model
   const{
    asIOffersTableGPU:{
     ChangellyFloatingOffer:ChangellyFloatingOffer,
    },
    classes:{Hidden:Hidden},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(fixed) addClass(ChangellyFloatingOffer,Hidden)
   else removeClass(ChangellyFloatingOffer,Hidden)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_Hidden_on_ChangellyFixedOffer(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    '546ad':float,
   }=ExchangeIntent.model
   const{
    asIOffersTableGPU:{
     ChangellyFixedOffer:ChangellyFixedOffer,
    },
    classes:{Hidden:Hidden},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(float) addClass(ChangellyFixedOffer,Hidden)
   else removeClass(ChangellyFixedOffer,Hidden)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_Hidden_on_LetsExchangeFloatingOffer(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'cec31':fixed,
   }=ExchangeIntent.model
   const{
    asIOffersTableGPU:{
     LetsExchangeFloatingOffer:LetsExchangeFloatingOffer,
    },
    classes:{Hidden:Hidden},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(fixed) addClass(LetsExchangeFloatingOffer,Hidden)
   else removeClass(LetsExchangeFloatingOffer,Hidden)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_Hidden_on_LetsExchangeFixedOffer(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    '546ad':float,
   }=ExchangeIntent.model
   const{
    asIOffersTableGPU:{
     LetsExchangeFixedOffer:LetsExchangeFixedOffer,
    },
    classes:{Hidden:Hidden},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(float) addClass(LetsExchangeFixedOffer,Hidden)
   else removeClass(LetsExchangeFixedOffer,Hidden)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_Hidden_on_ChangeNowFloatingOffer(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'cec31':fixed,
   }=ExchangeIntent.model
   const{
    asIOffersTableGPU:{
     ChangeNowFloatingOffer:ChangeNowFloatingOffer,
    },
    classes:{Hidden:Hidden},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(fixed) addClass(ChangeNowFloatingOffer,Hidden)
   else removeClass(ChangeNowFloatingOffer,Hidden)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_Hidden_on_ChangeNowFixedOffer(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    '546ad':float,
   }=ExchangeIntent.model
   const{
    asIOffersTableGPU:{
     ChangeNowFixedOffer:ChangeNowFixedOffer,
    },
    classes:{Hidden:Hidden},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(float) addClass(ChangeNowFixedOffer,Hidden)
   else removeClass(ChangeNowFixedOffer,Hidden)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function $reveal_ChangellyFloatingOffer(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '2a73d':changellyFloatingOffer,
   }=OffersAggregator.model
   const{
    asIOffersTableGPU:{ChangellyFloatingOffer:ChangellyFloatingOffer},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ChangellyFloatingOffer,changellyFloatingOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_ChangellyFloatingOfferAmount_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '2a73d':changellyFloatingOffer,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{ChangellyFloatingOfferAmount:ChangellyFloatingOfferAmount}}=this
   ChangellyFloatingOfferAmount.setText(changellyFloatingOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function $reveal_ChangellyFixedOffer(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '7b5f4':changellyFixedOffer,
   }=OffersAggregator.model
   const{
    asIOffersTableGPU:{ChangellyFixedOffer:ChangellyFixedOffer},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ChangellyFixedOffer,changellyFixedOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_ChangellyFixedOfferAmount_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '7b5f4':changellyFixedOffer,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{ChangellyFixedOfferAmount:ChangellyFixedOfferAmount}}=this
   ChangellyFixedOfferAmount.setText(changellyFixedOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function $reveal_LetsExchangeFloatingOffer(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    'a0ea0':letsExchangeFloatingOffer,
   }=OffersAggregator.model
   const{
    asIOffersTableGPU:{LetsExchangeFloatingOffer:LetsExchangeFloatingOffer},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(LetsExchangeFloatingOffer,letsExchangeFloatingOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_LetsExchangeFloatingOfferAmount_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    'a0ea0':letsExchangeFloatingOffer,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{LetsExchangeFloatingOfferAmount:LetsExchangeFloatingOfferAmount}}=this
   LetsExchangeFloatingOfferAmount.setText(letsExchangeFloatingOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function $reveal_LetsExchangeFixedOffer(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '26339':letsExchangeFixedOffer,
   }=OffersAggregator.model
   const{
    asIOffersTableGPU:{LetsExchangeFixedOffer:LetsExchangeFixedOffer},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(LetsExchangeFixedOffer,letsExchangeFixedOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_LetsExchangeFixedOfferAmount_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '26339':letsExchangeFixedOffer,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{LetsExchangeFixedOfferAmount:LetsExchangeFixedOfferAmount}}=this
   LetsExchangeFixedOfferAmount.setText(letsExchangeFixedOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function $reveal_ChangeNowFloatingOffer(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '9f4fb':changeNowFloatingOffer,
   }=OffersAggregator.model
   const{
    asIOffersTableGPU:{ChangeNowFloatingOffer:ChangeNowFloatingOffer},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ChangeNowFloatingOffer,changeNowFloatingOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_ChangeNowFloatingOfferAmount_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '9f4fb':changeNowFloatingOffer,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{ChangeNowFloatingOfferAmount:ChangeNowFloatingOfferAmount}}=this
   ChangeNowFloatingOfferAmount.setText(changeNowFloatingOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function $reveal_ChangeNowFixedOffer(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    'b89f1':changeNowFixedOffer,
   }=OffersAggregator.model
   const{
    asIOffersTableGPU:{ChangeNowFixedOffer:ChangeNowFixedOffer},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ChangeNowFixedOffer,changeNowFixedOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_ChangeNowFixedOfferAmount_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    'b89f1':changeNowFixedOffer,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{ChangeNowFixedOfferAmount:ChangeNowFixedOfferAmount}}=this
   ChangeNowFixedOfferAmount.setText(changeNowFixedOffer)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_Loading_on_ResetNowBu(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '77233':isAggregating,
   }=OffersAggregator.model
   const{
    asIOffersTableGPU:{
     ResetNowBu:ResetNowBu,
    },
    classes:{Loading:Loading},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(isAggregating) addClass(ResetNowBu,Loading)
   else removeClass(ResetNowBu,Loading)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_ExchangesLoaded_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    'b4c94':exchangesLoaded,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{ExchangesLoaded:ExchangesLoaded}}=this
   ExchangesLoaded.setText(exchangesLoaded)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_ExchangesTotal_Content(_,{OffersAggregator:OffersAggregator}){
   if(!OffersAggregator) return
   let{
    '8062a':exchangesTotal,
   }=OffersAggregator.model
   const{asIOffersTableGPU:{ExchangesTotal:ExchangesTotal}}=this
   ExchangesTotal.setText(exchangesTotal)
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint:function paint_CoinImWrs_Content(_,{CryptoSelectOut:CryptoSelectOut}){
   if(!CryptoSelectOut) return
   let{
    '73071':selectedIcon,
   }=CryptoSelectOut.model
   const{asIOffersTableGPU:{CoinImWrs:CoinImWrs}}=this
   for(const CoinImWr of CoinImWrs){
    CoinImWr.setImg(selectedIcon)
   }
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIOffersTableGPU:{
     ExchangeIntent:ExchangeIntent,
     OffersAggregator:OffersAggregator,
     ProgressCollapsar:ProgressCollapsar,
     CryptoSelectOut:CryptoSelectOut,
    },
   }=this
   complete(7833868048,{ExchangeIntent:ExchangeIntent})
   complete(4890088757,{OffersAggregator:OffersAggregator})
   complete(2854970513,{ProgressCollapsar:ProgressCollapsar},{
    /**@this {xyz.swapee.wc.IOffersTableHtmlComponent}*/
    get['3338c'](){ // -> collapsed
     const{land:{OffersAggregator:_OffersAggregator}}=this
     if(!_OffersAggregator) return void 0
     const allExchangesLoaded=_OffersAggregator.model['7a98d'] // <- allExchangesLoaded
     return allExchangesLoaded
    },
   })
   complete(3545350742,{CryptoSelectOut:CryptoSelectOut})
  },
 }),
 AbstractOffersTableHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableHtmlComponent}*/({
  paint(_,{OffersAggregator:OffersAggregator}){
   const land={OffersAggregator:OffersAggregator}
   if(!land.OffersAggregator) return
   this.onReset=OffersAggregator['0bf66'] // onReset=loadChangellyFixedOffer
   this.onReset=OffersAggregator['89675'] // onReset=loadChangellyFloatingOffer
   this.onReset=OffersAggregator['23d7c'] // onReset=loadLetsExchangeFixedOffer
   this.onReset=OffersAggregator['3fc1d'] // onReset=loadLetsExchangeFloatingOffer
   this.onReset=OffersAggregator['816ca'] // onReset=loadChangeNowFixedOffer
   this.onReset=OffersAggregator['2523b'] // onReset=loadChangeNowFloatingOffer
  },
 }),
]