import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableDisplay}
 */
function __AbstractOffersTableDisplay() {}
__AbstractOffersTableDisplay.prototype = /** @type {!_AbstractOffersTableDisplay} */ ({ })
/** @this {xyz.swapee.wc.back.OffersTableDisplay} */ function OffersTableDisplayConstructor() {
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.CoinImWrs=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.OfferCryptoOuts=[]
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersTableDisplay}
 */
class _AbstractOffersTableDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractOffersTableDisplay} ‎
 */
class AbstractOffersTableDisplay extends newAbstract(
 _AbstractOffersTableDisplay,115481619420,OffersTableDisplayConstructor,{
  asIOffersTableDisplay:1,
  superOffersTableDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableDisplay} */
AbstractOffersTableDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableDisplay} */
function AbstractOffersTableDisplayClass(){}

export default AbstractOffersTableDisplay


AbstractOffersTableDisplay[$implementations]=[
 __AbstractOffersTableDisplay,
 GraphicsDriverBack,
 AbstractOffersTableDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IOffersTableDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IOffersTableDisplay}*/({
    ChangellyFixedLink:twinMock,
    ChangellyFloatLink:twinMock,
    ResetIn:twinMock,
    ResetNowBu:twinMock,
    ChangellyFloatingOffer:twinMock,
    ChangellyFloatingOfferAmount:twinMock,
    ChangellyFixedOffer:twinMock,
    ChangellyFixedOfferAmount:twinMock,
    LetsExchangeFloatingOffer:twinMock,
    LetsExchangeFloatingOfferAmount:twinMock,
    LetsExchangeFixedOffer:twinMock,
    LetsExchangeFixedOfferAmount:twinMock,
    ChangeNowFloatingOffer:twinMock,
    ChangeNowFloatingOfferAmount:twinMock,
    ChangeNowFixedOffer:twinMock,
    ChangeNowFixedOfferAmount:twinMock,
    ExchangesLoaded:twinMock,
    ExchangesTotal:twinMock,
    ProgressCollapsar:twinMock,
    CryptoSelectOut:twinMock,
    ExchangeIntent:twinMock,
    OffersAggregator:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.OffersTableDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IOffersTableDisplay.Initialese}*/({
   CoinImWrs:1,
   ChangellyFloatingOffer:1,
   ChangellyFloatingOfferAmount:1,
   ChangellyFloatLink:1,
   ChangellyFixedOffer:1,
   ChangellyFixedOfferAmount:1,
   ChangellyFixedLink:1,
   LetsExchangeFloatingOffer:1,
   LetsExchangeFloatingOfferAmount:1,
   LetsExchangeFixedOffer:1,
   LetsExchangeFixedOfferAmount:1,
   ChangeNowFloatingOffer:1,
   ChangeNowFloatingOfferAmount:1,
   ChangeNowFixedOffer:1,
   ChangeNowFixedOfferAmount:1,
   OfferCryptoOuts:1,
   ResetIn:1,
   ResetNowBu:1,
   ExchangesLoaded:1,
   ExchangesTotal:1,
   ProgressCollapsar:1,
   CryptoSelectOut:1,
   ExchangeIntent:1,
   OffersAggregator:1,
  }),
  initializer({
   CoinImWrs:_CoinImWrs,
   ChangellyFloatingOffer:_ChangellyFloatingOffer,
   ChangellyFloatingOfferAmount:_ChangellyFloatingOfferAmount,
   ChangellyFloatLink:_ChangellyFloatLink,
   ChangellyFixedOffer:_ChangellyFixedOffer,
   ChangellyFixedOfferAmount:_ChangellyFixedOfferAmount,
   ChangellyFixedLink:_ChangellyFixedLink,
   LetsExchangeFloatingOffer:_LetsExchangeFloatingOffer,
   LetsExchangeFloatingOfferAmount:_LetsExchangeFloatingOfferAmount,
   LetsExchangeFixedOffer:_LetsExchangeFixedOffer,
   LetsExchangeFixedOfferAmount:_LetsExchangeFixedOfferAmount,
   ChangeNowFloatingOffer:_ChangeNowFloatingOffer,
   ChangeNowFloatingOfferAmount:_ChangeNowFloatingOfferAmount,
   ChangeNowFixedOffer:_ChangeNowFixedOffer,
   ChangeNowFixedOfferAmount:_ChangeNowFixedOfferAmount,
   OfferCryptoOuts:_OfferCryptoOuts,
   ResetIn:_ResetIn,
   ResetNowBu:_ResetNowBu,
   ExchangesLoaded:_ExchangesLoaded,
   ExchangesTotal:_ExchangesTotal,
   ProgressCollapsar:_ProgressCollapsar,
   CryptoSelectOut:_CryptoSelectOut,
   ExchangeIntent:_ExchangeIntent,
   OffersAggregator:_OffersAggregator,
  }) {
   if(_CoinImWrs!==undefined) this.CoinImWrs=_CoinImWrs
   if(_ChangellyFloatingOffer!==undefined) this.ChangellyFloatingOffer=_ChangellyFloatingOffer
   if(_ChangellyFloatingOfferAmount!==undefined) this.ChangellyFloatingOfferAmount=_ChangellyFloatingOfferAmount
   if(_ChangellyFloatLink!==undefined) this.ChangellyFloatLink=_ChangellyFloatLink
   if(_ChangellyFixedOffer!==undefined) this.ChangellyFixedOffer=_ChangellyFixedOffer
   if(_ChangellyFixedOfferAmount!==undefined) this.ChangellyFixedOfferAmount=_ChangellyFixedOfferAmount
   if(_ChangellyFixedLink!==undefined) this.ChangellyFixedLink=_ChangellyFixedLink
   if(_LetsExchangeFloatingOffer!==undefined) this.LetsExchangeFloatingOffer=_LetsExchangeFloatingOffer
   if(_LetsExchangeFloatingOfferAmount!==undefined) this.LetsExchangeFloatingOfferAmount=_LetsExchangeFloatingOfferAmount
   if(_LetsExchangeFixedOffer!==undefined) this.LetsExchangeFixedOffer=_LetsExchangeFixedOffer
   if(_LetsExchangeFixedOfferAmount!==undefined) this.LetsExchangeFixedOfferAmount=_LetsExchangeFixedOfferAmount
   if(_ChangeNowFloatingOffer!==undefined) this.ChangeNowFloatingOffer=_ChangeNowFloatingOffer
   if(_ChangeNowFloatingOfferAmount!==undefined) this.ChangeNowFloatingOfferAmount=_ChangeNowFloatingOfferAmount
   if(_ChangeNowFixedOffer!==undefined) this.ChangeNowFixedOffer=_ChangeNowFixedOffer
   if(_ChangeNowFixedOfferAmount!==undefined) this.ChangeNowFixedOfferAmount=_ChangeNowFixedOfferAmount
   if(_OfferCryptoOuts!==undefined) this.OfferCryptoOuts=_OfferCryptoOuts
   if(_ResetIn!==undefined) this.ResetIn=_ResetIn
   if(_ResetNowBu!==undefined) this.ResetNowBu=_ResetNowBu
   if(_ExchangesLoaded!==undefined) this.ExchangesLoaded=_ExchangesLoaded
   if(_ExchangesTotal!==undefined) this.ExchangesTotal=_ExchangesTotal
   if(_ProgressCollapsar!==undefined) this.ProgressCollapsar=_ProgressCollapsar
   if(_CryptoSelectOut!==undefined) this.CryptoSelectOut=_CryptoSelectOut
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_OffersAggregator!==undefined) this.OffersAggregator=_OffersAggregator
  },
 }),
]