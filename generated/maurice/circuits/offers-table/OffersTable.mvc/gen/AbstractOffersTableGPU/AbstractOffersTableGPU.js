import AbstractOffersTableDisplay from '../AbstractOffersTableDisplayBack'
import OffersTableClassesPQs from '../../pqs/OffersTableClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {OffersTableClassesQPs} from '../../pqs/OffersTableClassesQPs'
import {OffersTableVdusPQs} from '../../pqs/OffersTableVdusPQs'
import {OffersTableVdusQPs} from '../../pqs/OffersTableVdusQPs'
import {OffersTableMemoryPQs} from '../../pqs/OffersTableMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableGPU}
 */
function __AbstractOffersTableGPU() {}
__AbstractOffersTableGPU.prototype = /** @type {!_AbstractOffersTableGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableGPU}
 */
class _AbstractOffersTableGPU { }
/**
 * Handles the periphery of the _IOffersTableDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractOffersTableGPU} ‎
 */
class AbstractOffersTableGPU extends newAbstract(
 _AbstractOffersTableGPU,115481619416,null,{
  asIOffersTableGPU:1,
  superOffersTableGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableGPU} */
AbstractOffersTableGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableGPU} */
function AbstractOffersTableGPUClass(){}

export default AbstractOffersTableGPU


AbstractOffersTableGPU[$implementations]=[
 __AbstractOffersTableGPU,
 AbstractOffersTableGPUClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableGPU}*/({
  classesQPs:OffersTableClassesQPs,
  vdusPQs:OffersTableVdusPQs,
  vdusQPs:OffersTableVdusQPs,
  memoryPQs:OffersTableMemoryPQs,
 }),
 AbstractOffersTableDisplay,
 BrowserView,
 AbstractOffersTableGPUClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableGPU}*/({
  allocator(){
   pressFit(this.classes,'',OffersTableClassesPQs)
  },
 }),
]