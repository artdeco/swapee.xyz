import AbstractOffersTableScreenAT from '../AbstractOffersTableScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableScreenBack}
 */
function __AbstractOffersTableScreenBack() {}
__AbstractOffersTableScreenBack.prototype = /** @type {!_AbstractOffersTableScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersTableScreen}
 */
class _AbstractOffersTableScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractOffersTableScreen} ‎
 */
class AbstractOffersTableScreenBack extends newAbstract(
 _AbstractOffersTableScreenBack,115481619432,null,{
  asIOffersTableScreen:1,
  superOffersTableScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableScreen} */
AbstractOffersTableScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableScreen} */
function AbstractOffersTableScreenBackClass(){}

export default AbstractOffersTableScreenBack


AbstractOffersTableScreenBack[$implementations]=[
 __AbstractOffersTableScreenBack,
 AbstractOffersTableScreenAT,
]