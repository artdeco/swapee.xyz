import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableControllerAR}
 */
function __AbstractOffersTableControllerAR() {}
__AbstractOffersTableControllerAR.prototype = /** @type {!_AbstractOffersTableControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersTableControllerAR}
 */
class _AbstractOffersTableControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOffersTableControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractOffersTableControllerAR} ‎
 */
class AbstractOffersTableControllerAR extends newAbstract(
 _AbstractOffersTableControllerAR,115481619429,null,{
  asIOffersTableControllerAR:1,
  superOffersTableControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableControllerAR} */
AbstractOffersTableControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersTableControllerAR} */
function AbstractOffersTableControllerARClass(){}

export default AbstractOffersTableControllerAR


AbstractOffersTableControllerAR[$implementations]=[
 __AbstractOffersTableControllerAR,
 AR,
 AbstractOffersTableControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IOffersTableControllerAR}*/({
  allocator(){
   this.methods={
    resetTick:'3ccb7',
    reset:'f6174',
    onReset:'ad42e',
   }
  },
 }),
]