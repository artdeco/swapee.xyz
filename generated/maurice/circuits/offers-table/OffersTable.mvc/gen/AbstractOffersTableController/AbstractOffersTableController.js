import OffersTableBuffer from '../OffersTableBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {OffersTablePortConnector} from '../OffersTablePort'
import {defineEmitters} from '@mauriceguest/guest2'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableController}
 */
function __AbstractOffersTableController() {}
__AbstractOffersTableController.prototype = /** @type {!_AbstractOffersTableController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableController}
 */
class _AbstractOffersTableController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractOffersTableController} ‎
 */
export class AbstractOffersTableController extends newAbstract(
 _AbstractOffersTableController,115481619424,null,{
  asIOffersTableController:1,
  superOffersTableController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableController} */
AbstractOffersTableController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableController} */
function AbstractOffersTableControllerClass(){}


AbstractOffersTableController[$implementations]=[
 AbstractOffersTableControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IOffersTablePort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractOffersTableController,
 AbstractOffersTableControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableController}*/({
  reset:precombined,
 }),
 OffersTableBuffer,
 IntegratedController,
 /**@type {!AbstractOffersTableController}*/(OffersTablePortConnector),
 AbstractOffersTableControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableController}*/({
  constructor(){
   defineEmitters(this,{
    onReset:true,
   })
  },
 }),
 AbstractOffersTableControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersTableController}*/({
  reset(){
   const{asIOffersTableController:{onReset:onReset}}=this
   onReset()
  },
 }),
]


export default AbstractOffersTableController