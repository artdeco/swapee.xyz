import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableScreenAR}
 */
function __AbstractOffersTableScreenAR() {}
__AbstractOffersTableScreenAR.prototype = /** @type {!_AbstractOffersTableScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOffersTableScreenAR}
 */
class _AbstractOffersTableScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOffersTableScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractOffersTableScreenAR} ‎
 */
class AbstractOffersTableScreenAR extends newAbstract(
 _AbstractOffersTableScreenAR,115481619433,null,{
  asIOffersTableScreenAR:1,
  superOffersTableScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOffersTableScreenAR} */
AbstractOffersTableScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOffersTableScreenAR} */
function AbstractOffersTableScreenARClass(){}

export default AbstractOffersTableScreenAR


AbstractOffersTableScreenAR[$implementations]=[
 __AbstractOffersTableScreenAR,
 AR,
 AbstractOffersTableScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IOffersTableScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractOffersTableScreenAR}