import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersTableComputer}
 */
function __AbstractOffersTableComputer() {}
__AbstractOffersTableComputer.prototype = /** @type {!_AbstractOffersTableComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTableComputer}
 */
class _AbstractOffersTableComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractOffersTableComputer} ‎
 */
export class AbstractOffersTableComputer extends newAbstract(
 _AbstractOffersTableComputer,11548161941,null,{
  asIOffersTableComputer:1,
  superOffersTableComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTableComputer} */
AbstractOffersTableComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableComputer} */
function AbstractOffersTableComputerClass(){}


AbstractOffersTableComputer[$implementations]=[
 __AbstractOffersTableComputer,
 Adapter,
]


export default AbstractOffersTableComputer