
/**@this {xyz.swapee.wc.IOffersTableComputer}*/
export function preadaptChangellyFixedLink(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFixedLink.Form}*/
 const _inputs={
  currencyFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['96c88']:void 0,
  currencyTo:this.land.ExchangeIntent?this.land.ExchangeIntent.model['c23cd']:void 0,
  amountFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['748e6']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.amountFrom=changes['748e6']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangellyFixedLink(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersTableComputer}*/
export function preadaptChangellyFloatLink(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersTableComputer.adaptChangellyFloatLink.Form}*/
 const _inputs={
  currencyFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['96c88']:void 0,
  currencyTo:this.land.ExchangeIntent?this.land.ExchangeIntent.model['c23cd']:void 0,
  amountFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['748e6']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.amountFrom=changes['748e6']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangellyFloatLink(__inputs,__changes)
 return RET
}