import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {OffersTableInputsPQs} from '../../pqs/OffersTableInputsPQs'
import {OffersTableOuterCoreConstructor} from '../OffersTableCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersTablePort}
 */
function __OffersTablePort() {}
__OffersTablePort.prototype = /** @type {!_OffersTablePort} */ ({ })
/** @this {xyz.swapee.wc.OffersTablePort} */ function OffersTablePortConstructor() {
  const self=/** @type {!xyz.swapee.wc.OffersTableOuterCore} */ ({model:null})
  OffersTableOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersTablePort}
 */
class _OffersTablePort { }
/**
 * The port that serves as an interface to the _IOffersTable_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractOffersTablePort} ‎
 */
export class OffersTablePort extends newAbstract(
 _OffersTablePort,11548161945,OffersTablePortConstructor,{
  asIOffersTablePort:1,
  superOffersTablePort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersTablePort} */
OffersTablePort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersTablePort} */
function OffersTablePortClass(){}

export const OffersTablePortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IOffersTable.Pinout>}*/({
 get Port() { return OffersTablePort },
})

OffersTablePort[$implementations]=[
 OffersTablePortClass.prototype=/**@type {!xyz.swapee.wc.IOffersTablePort}*/({
  resetPort(){
   this.resetOffersTablePort()
  },
  resetOffersTablePort(){
   OffersTablePortConstructor.call(this)
  },
 }),
 __OffersTablePort,
 Parametric,
 OffersTablePortClass.prototype=/**@type {!xyz.swapee.wc.IOffersTablePort}*/({
  constructor(){
   mountPins(this.inputs,OffersTableInputsPQs)
  },
 }),
]


export default OffersTablePort