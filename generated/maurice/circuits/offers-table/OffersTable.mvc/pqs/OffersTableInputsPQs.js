import {OffersTableMemoryPQs} from './OffersTableMemoryPQs'
export const OffersTableInputsPQs=/**@type {!xyz.swapee.wc.OffersTableInputsQPs}*/({
 ...OffersTableMemoryPQs,
})