import OffersTableClassesPQs from './OffersTableClassesPQs'
export const OffersTableClassesQPs=/**@type {!xyz.swapee.wc.OffersTableClassesQPs}*/(Object.keys(OffersTableClassesPQs)
 .reduce((a,k)=>{a[OffersTableClassesPQs[k]]=k;return a},{}))