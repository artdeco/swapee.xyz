import {OffersTableCachePQs} from './OffersTableCachePQs'
export const OffersTableCacheQPs=/**@type {!xyz.swapee.wc.OffersTableCacheQPs}*/(Object.keys(OffersTableCachePQs)
 .reduce((a,k)=>{a[OffersTableCachePQs[k]]=k;return a},{}))