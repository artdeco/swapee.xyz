import {OffersTableMemoryPQs} from './OffersTableMemoryPQs'
export const OffersTableMemoryQPs=/**@type {!xyz.swapee.wc.OffersTableMemoryQPs}*/(Object.keys(OffersTableMemoryPQs)
 .reduce((a,k)=>{a[OffersTableMemoryPQs[k]]=k;return a},{}))