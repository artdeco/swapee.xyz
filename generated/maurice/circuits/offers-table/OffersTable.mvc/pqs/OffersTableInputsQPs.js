import {OffersTableInputsPQs} from './OffersTableInputsPQs'
export const OffersTableInputsQPs=/**@type {!xyz.swapee.wc.OffersTableInputsQPs}*/(Object.keys(OffersTableInputsPQs)
 .reduce((a,k)=>{a[OffersTableInputsPQs[k]]=k;return a},{}))