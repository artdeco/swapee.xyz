import {OffersTableVdusPQs} from './OffersTableVdusPQs'
export const OffersTableVdusQPs=/**@type {!xyz.swapee.wc.OffersTableVdusQPs}*/(Object.keys(OffersTableVdusPQs)
 .reduce((a,k)=>{a[OffersTableVdusPQs[k]]=k;return a},{}))