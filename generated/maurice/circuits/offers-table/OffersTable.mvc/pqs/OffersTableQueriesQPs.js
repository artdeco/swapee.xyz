import {OffersTableQueriesPQs} from './OffersTableQueriesPQs'
export const OffersTableQueriesQPs=/**@type {!xyz.swapee.wc.OffersTableQueriesQPs}*/(Object.keys(OffersTableQueriesPQs)
 .reduce((a,k)=>{a[OffersTableQueriesPQs[k]]=k;return a},{}))