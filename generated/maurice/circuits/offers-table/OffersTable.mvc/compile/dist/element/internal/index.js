import Module from './element'

/**@extends {xyz.swapee.wc.AbstractOffersTable}*/
export class AbstractOffersTable extends Module['11548161941'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersTable} */
AbstractOffersTable.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersTablePort} */
export const OffersTablePort=Module['11548161943']
/**@extends {xyz.swapee.wc.AbstractOffersTableController}*/
export class AbstractOffersTableController extends Module['11548161944'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableController} */
AbstractOffersTableController.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersTableElement} */
export const OffersTableElement=Module['11548161948']
/** @type {typeof xyz.swapee.wc.OffersTableBuffer} */
export const OffersTableBuffer=Module['115481619411']
/**@extends {xyz.swapee.wc.AbstractOffersTableComputer}*/
export class AbstractOffersTableComputer extends Module['115481619430'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersTableComputer} */
AbstractOffersTableComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersTableController} */
export const OffersTableController=Module['115481619461']