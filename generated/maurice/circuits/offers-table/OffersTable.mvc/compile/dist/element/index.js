/**
 * An abstract class of `xyz.swapee.wc.IOffersTable` interface.
 * @extends {xyz.swapee.wc.AbstractOffersTable}
 */
class AbstractOffersTable extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOffersTable_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OffersTablePort}
 */
class OffersTablePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableController` interface.
 * @extends {xyz.swapee.wc.AbstractOffersTableController}
 */
class AbstractOffersTableController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IOffersTable_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.OffersTableElement}
 */
class OffersTableElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OffersTableBuffer}
 */
class OffersTableBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOffersTableComputer}
 */
class AbstractOffersTableComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.OffersTableController}
 */
class OffersTableController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOffersTable = AbstractOffersTable
module.exports.OffersTablePort = OffersTablePort
module.exports.AbstractOffersTableController = AbstractOffersTableController
module.exports.OffersTableElement = OffersTableElement
module.exports.OffersTableBuffer = OffersTableBuffer
module.exports.AbstractOffersTableComputer = AbstractOffersTableComputer
module.exports.OffersTableController = OffersTableController

Object.defineProperties(module.exports, {
 'AbstractOffersTable': {get: () => require('./precompile/internal')[11548161941]},
 [11548161941]: {get: () => module.exports['AbstractOffersTable']},
 'OffersTablePort': {get: () => require('./precompile/internal')[11548161943]},
 [11548161943]: {get: () => module.exports['OffersTablePort']},
 'AbstractOffersTableController': {get: () => require('./precompile/internal')[11548161944]},
 [11548161944]: {get: () => module.exports['AbstractOffersTableController']},
 'OffersTableElement': {get: () => require('./precompile/internal')[11548161948]},
 [11548161948]: {get: () => module.exports['OffersTableElement']},
 'OffersTableBuffer': {get: () => require('./precompile/internal')[115481619411]},
 [115481619411]: {get: () => module.exports['OffersTableBuffer']},
 'AbstractOffersTableComputer': {get: () => require('./precompile/internal')[115481619430]},
 [115481619430]: {get: () => module.exports['AbstractOffersTableComputer']},
 'OffersTableController': {get: () => require('./precompile/internal')[115481619461]},
 [115481619461]: {get: () => module.exports['OffersTableController']},
})