/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
function aa({},{g:{fixed:a,ia:c},h:{ea:g,ka:e,ja:m,fa:h,ga:k,ha:p}}){const {T:{element:f,i:r,j:t,o:v,s:w,l:x,m:y},ba:{classes:{ca:u}}}=this;g=new Map([[r,g],[v,m],[x,k]]);g.delete(void 0);e=new Map([[t,h],[w,e],[y,p]]);e.delete(void 0);a=[...(a?g:c?e:new Map([...g.entries(),...e.entries()])).entries()].sort(([,n],[,q])=>n||q?n?q?q-n:1:-1:0);c=0;for(const [n,q]of a)q&&(f.insertBefore(n,f.children[c+1]),c++);c=0;for(const n of f.children)1==c?n.classList.add(u):n.classList.remove(u),c++};function B(a,c){const {T:{la:g}}=this,e={},m=c["3427b"];if(m){const {cec31:h,"546ad":k}=m;e.g={fixed:h,ia:k}}else e.g={};if(c=c["89de3"]){const {b89f1:h,a0ea0:k,26339:p,"9f4fb":f,"7b5f4":r,"2a73d":t}=c;e.h={ea:h,ka:k,ja:p,fa:f,ga:r,ha:t}}else e.h={};g(a,e)}B._id="3c2666c";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const D=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const ba=D["37270038985"],ca=D["37270038986"],da=D["372700389810"],E=D["372700389811"];function F(a,c,g,e){return D["372700389812"](a,c,g,e,!1,void 0)};
const G=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const ea=G["61893096584"],fa=G["61893096586"],ha=G["618930965811"],ia=G["618930965812"],ja=G["618930965815"],ka=G["618930965819"];function M(){}M.prototype={};function la(){this.K=[];this.u=this.i=this.v=this.j=this.G=this.o=this.H=this.s=this.A=this.C=this.l=this.D=this.F=this.m=null;this.P=[];this.h=this.g=this.L=this.R=this.O=this.M=this.J=this.I=null}class ma{}class N extends F(ma,115481619417,la,{T:1,Ka:2}){}
N[E]=[M,ea,{constructor(){ba(this,()=>{const {queries:{Ha:a,Ia:c,Da:g,Ea:e,Ga:m,Aa:h,Ca:k,Fa:p}}=this;this.scan({$:a,aa:c,W:g,X:e,Z:m,U:h,V:k,Y:p})})},scan:function({$:a,aa:c,W:g,X:e,Z:m,U:h,V:k,Y:p}){const {element:f,ba:{vdusPQs:{A:r,D:t,m:v,F:w,l:x,C:y,s:u,H:n,o:q,G:H,j:I,v:J,i:K,u:L}},queries:{$:C,aa:Q,W:R,X:S,Z:T,U,V,Y:W}}=this,l=ja(f);g=(d=>{let b;d?b=f.closest(d):b=document;return R?b.querySelector(R):void 0})(g);e=(d=>{let b;d?b=f.closest(d):b=document;return S?b.querySelector(S):
void 0})(e);m=(d=>{let b;d?b=f.closest(d):b=document;return T?b.querySelector(T):void 0})(m);h=(d=>{let b;d?b=f.closest(d):b=document;return U?b.querySelector(U):void 0})(h);k=(d=>{let b;d?b=f.closest(d):b=document;return V?b.querySelector(V):void 0})(k);p=(d=>{let b;d?b=f.closest(d):b=document;return W?b.querySelector(W):void 0})(p);Object.assign(this,{A:l[r],D:l[t],I:(d=>{let b;d?b=f.closest(d):b=document;return C?b.querySelector(C):void 0})(a),J:(d=>{let b;d?b=f.closest(d):b=document;return Q?
b.querySelector(Q):void 0})(c),K:f?[...f.querySelectorAll("[data-id~=f87425]")]:[],m:l[v],F:l[w],l:l[x],C:l[y],s:l[u],H:l[n],o:l[q],G:l[H],j:l[I],v:l[J],i:l[K],u:l[L],P:f?[...f.querySelectorAll("[data-id~=f87417]")]:[],M:g,O:e,R:m,L:h,g:k,h:p})}},{[da]:{K:1,m:1,F:1,D:1,l:1,C:1,A:1,s:1,H:1,o:1,G:1,j:1,v:1,i:1,u:1,P:1,I:1,J:1,M:1,O:1,R:1,L:1,g:1,h:1},initializer({K:a,m:c,F:g,D:e,l:m,C:h,A:k,s:p,H:f,o:r,G:t,j:v,v:w,i:x,u:y,P:u,I:n,J:q,M:H,O:I,R:J,L:K,g:L,h:C}){void 0!==a&&(this.K=a);void 0!==c&&(this.m=
c);void 0!==g&&(this.F=g);void 0!==e&&(this.D=e);void 0!==m&&(this.l=m);void 0!==h&&(this.C=h);void 0!==k&&(this.A=k);void 0!==p&&(this.s=p);void 0!==f&&(this.H=f);void 0!==r&&(this.o=r);void 0!==t&&(this.G=t);void 0!==v&&(this.j=v);void 0!==w&&(this.v=w);void 0!==x&&(this.i=x);void 0!==y&&(this.u=y);void 0!==u&&(this.P=u);void 0!==n&&(this.I=n);void 0!==q&&(this.J=q);void 0!==H&&(this.M=H);void 0!==I&&(this.O=I);void 0!==J&&(this.R=J);void 0!==K&&(this.L=K);void 0!==L&&(this.g=L);void 0!==C&&(this.h=
C)}}];var O=class extends N.implements({paint:[B]},{la:aa}){};function na(){};function oa(){const {da:{ma:a}}=this;setInterval(a,1E3)};function P(){}P.prototype={};class pa{}class X extends F(pa,115481619430,null,{ta:1,Ja:2}){}X[E]=[P,ha,{get da(){return this},ma(){this.uart.t("inv",{mid:"3ccb7"})},reset(){this.uart.t("inv",{mid:"f6174"})}}];const qa={pa:"16cc2",ra:"16bfb",qa:"7acdf",sa:"0aa87",ca:"e8977"};function ra(){}ra.prototype={};class sa{}class ta extends F(sa,115481619433,null,{ua:1,Ma:2}){}ta[E]=[ra,ia,{allocator(){this.methods={}}}];const Y={oa:"5bbee",za:"b1a5b"};const ua={...Y};const va=Object.keys(Y).reduce((a,c)=>{a[Y[c]]=c;return a},{});const wa={wa:"ca106",xa:"71175"};const xa=Object.keys(wa).reduce((a,c)=>{a[wa[c]]=c;return a},{});const ya=Object.keys(qa).reduce((a,c)=>{a[qa[c]]=c;return a},{});function za(){}za.prototype={};class Aa{}class Ba extends F(Aa,115481619431,null,{ba:1,La:2}){}function Z(){}
Ba[E]=[za,Z.prototype={deduceInputs(){const {T:{I:a}}=this;return{oa:null==a?void 0:a.innerText}}},Z.prototype={inputsPQs:ua,queriesPQs:{Ba:"3c6e2",$:"0c854",aa:"1e273",W:"35cac",X:"f6836",Z:"b9642",V:"13da4",Y:"cde34",U:"c3057",ya:"7057c"},memoryQPs:va,cacheQPs:xa},fa,ka,ta,Z.prototype={vdusPQs:{I:"f8741",J:"f8742",m:"f8745",F:"f8746",l:"f8747",C:"f8748",s:"f8749",H:"f87410",o:"f87411",G:"f87412",j:"f87413",v:"f87414",i:"f87415",u:"f87416",P:"f87417",M:"f87418",O:"f87419",R:"f87420",g:"f87421",h:"f87422",
L:"f87424",K:"f87425",D:"f87426",A:"f87427"}},Z.prototype={classesQPs:ya},Z.prototype={constructor(){ca(this,()=>{const a=this.J;a&&a.addEventListener("click",()=>{this.reset()})})}}];var Ca=class extends Ba.implements(X,O,{get queries(){return this.settings}},{deduceInputs:na,constructor:oa,__$id:1154816194}){};module.exports["115481619441"]=O;module.exports["115481619471"]=Ca;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1154816194']=module.exports