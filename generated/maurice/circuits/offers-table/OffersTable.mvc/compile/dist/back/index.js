/**
 * An abstract class of `xyz.swapee.wc.IOffersTable` interface.
 * @extends {xyz.swapee.wc.AbstractOffersTable}
 */
class AbstractOffersTable extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOffersTable_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OffersTablePort}
 */
class OffersTablePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableController` interface.
 * @extends {xyz.swapee.wc.AbstractOffersTableController}
 */
class AbstractOffersTableController extends (class {/* lazy-loaded */}) {}
/**
 * The _IOffersTable_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.OffersTableHtmlComponent}
 */
class OffersTableHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OffersTableBuffer}
 */
class OffersTableBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOffersTableComputer}
 */
class AbstractOffersTableComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.OffersTableComputer}
 */
class OffersTableComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _IOffersTable_.
 * @extends {xyz.swapee.wc.OffersTableProcessor}
 */
class OffersTableProcessor extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.OffersTableController}
 */
class OffersTableController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOffersTable = AbstractOffersTable
module.exports.OffersTablePort = OffersTablePort
module.exports.AbstractOffersTableController = AbstractOffersTableController
module.exports.OffersTableHtmlComponent = OffersTableHtmlComponent
module.exports.OffersTableBuffer = OffersTableBuffer
module.exports.AbstractOffersTableComputer = AbstractOffersTableComputer
module.exports.OffersTableComputer = OffersTableComputer
module.exports.OffersTableProcessor = OffersTableProcessor
module.exports.OffersTableController = OffersTableController