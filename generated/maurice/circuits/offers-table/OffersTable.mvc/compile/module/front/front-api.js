import { OffersTableDisplay, OffersTableScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.OffersTableDisplay} */
export { OffersTableDisplay }
/** @lazy @api {xyz.swapee.wc.OffersTableScreen} */
export { OffersTableScreen }