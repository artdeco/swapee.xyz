import resetTick from './methods/reset-tick'
import reset from './methods/reset'
import AbstractOffersTableProcessor from '../../gen/AbstractOffersTableProcessor'

/** @extends {xyz.swapee.wc.OffersTableProcessor} */
export default class extends AbstractOffersTableProcessor.implements(
 /** @type {!xyz.swapee.wc.IOffersTableProcessor} */ ({
  resetTick:resetTick,
  reset:reset,
 }),
){}