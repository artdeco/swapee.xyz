/** @type {xyz.swapee.wc.IOffersTableProcessor._resetTick} */
export default function resetTick(){
 let{model:{untilReset},setInputs,asIOffersTableController:{reset:reset}}=/**@type {!xyz.swapee.wc.IOffersTableProcessor}*/(this)
 untilReset--
 if(untilReset<1) {
  // reset now, and start counting...
  untilReset=90
  reset()
 }else{
  setInputs({untilReset:untilReset})
 }
}