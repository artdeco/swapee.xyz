/** @type {xyz.swapee.wc.IOffersTableProcessor._reset} */
export default function reset(){
 let{
  asIOffersTableController:{setInputs:setInputs},
 }=/**@type {!xyz.swapee.wc.IOffersTableProcessor}*/(this)
 setInputs({untilReset:90})
}