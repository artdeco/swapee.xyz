/** @type {xyz.swapee.wc.IOffersTableScreen._constructor} */
export default function constructor() {
 const{asIOffersTableController:{resetTick:resetTick}}=/**@type {!xyz.swapee.wc.IOffersTableScreen}*/(this)
 setInterval(resetTick,1000)
}