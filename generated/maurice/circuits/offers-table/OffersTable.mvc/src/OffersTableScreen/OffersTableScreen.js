import deduceInputs from './methods/deduce-inputs'
import constructor from './methods/constructor'
import AbstractOffersTableControllerAT from '../../gen/AbstractOffersTableControllerAT'
import OffersTableDisplay from '../OffersTableDisplay'
import AbstractOffersTableScreen from '../../gen/AbstractOffersTableScreen'

/** @extends {xyz.swapee.wc.OffersTableScreen} */
export default class extends AbstractOffersTableScreen.implements(
 AbstractOffersTableControllerAT,
 OffersTableDisplay,
 /**@type {!xyz.swapee.wc.IOffersTableScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IOffersTableScreen} */ ({
  deduceInputs:deduceInputs,
  constructor:constructor,
  __$id:1154816194,
 }),
){}