/** @type {xyz.swapee.wc.IOffersTableComputer._adaptChangellyFloatLink} */
export default function adaptChangellyFloatLink({currencyFrom,currencyTo,amountFrom}){
 return {changellyFloatLink:`/partners/changelly?amount=${amountFrom}&to=${currencyTo}&from=${currencyFrom}&type=floating`}
}