/** @type {xyz.swapee.wc.IOffersTableComputer._adaptChangellyFixedLink} */
export default function adaptChangellyFixedLink({currencyFrom,currencyTo,amountFrom}){
 return {changellyFixedLink:`/partners/changelly?amount=${amountFrom}&to=${currencyTo}&from=${currencyFrom}&type=fixed`}
}