import adaptChangellyFixedLink from './methods/adapt-changelly-fixed-link'
import adaptChangellyFloatLink from './methods/adapt-changelly-float-link'
import {preadaptChangellyFixedLink,preadaptChangellyFloatLink} from '../../gen/AbstractOffersTableComputer/preadapters'
import AbstractOffersTableComputer from '../../gen/AbstractOffersTableComputer'

/** @extends {xyz.swapee.wc.OffersTableComputer} */
export default class OffersTableHtmlComputer extends AbstractOffersTableComputer.implements(
 /** @type {!xyz.swapee.wc.IOffersTableComputer} */ ({
  adaptChangellyFixedLink:adaptChangellyFixedLink,
  adaptChangellyFloatLink:adaptChangellyFloatLink,
  adapt:[preadaptChangellyFixedLink,preadaptChangellyFloatLink],
 }),
){}