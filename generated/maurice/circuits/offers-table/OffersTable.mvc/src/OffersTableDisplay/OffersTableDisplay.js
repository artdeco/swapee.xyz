import paintTableOrder from './methods/paint-table-order'
import prepaintTableOrder from '../../../parts/40-display/src/methods/prepaint-table-order'
import AbstractOffersTableDisplay from '../../gen/AbstractOffersTableDisplay'

/** @extends {xyz.swapee.wc.OffersTableDisplay} */
export default class extends AbstractOffersTableDisplay.implements(
 /**@type {!xyz.swapee.wc.IOffersTableDisplay}*/({
  paint:[
   prepaintTableOrder,
  ],
 }),
 /** @type {!xyz.swapee.wc.IOffersTableDisplay} */ ({
  paintTableOrder:paintTableOrder,
 }),
){}