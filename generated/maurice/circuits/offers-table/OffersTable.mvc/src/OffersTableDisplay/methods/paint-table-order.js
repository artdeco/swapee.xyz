/** @type {xyz.swapee.wc.IOffersTableDisplay._paintTableOrder} */
export default function paintTableOrder({},{
 ExchangeIntent:{fixed:fixed,float:float},
 OffersAggregator:{
  changeNowFixedOffer,letsExchangeFloatingOffer,letsExchangeFixedOffer,
  changeNowFloatingOffer,changellyFixedOffer,changellyFloatingOffer,
 }}){
 // todo: basically, need :first-of-class:not(.Hidden)
 const{asIOffersTableDisplay:{
  element:element,
  ChangeNowFixedOffer,ChangeNowFloatingOffer,
  LetsExchangeFixedOffer,LetsExchangeFloatingOffer,
  ChangellyFixedOffer,ChangellyFloatingOffer,
 },asIOffersTableScreen:{classes:{BestOffer}}}=/**@type {!xyz.swapee.wc.IOffersTableDisplay}*/(this)
 // order
 const fixedMap=new Map([
  [ChangeNowFixedOffer,changeNowFixedOffer],
  [LetsExchangeFixedOffer,letsExchangeFixedOffer],
  [ChangellyFixedOffer,changellyFixedOffer],
 ])
 fixedMap.delete(undefined)
 const floatingMap=new Map([
  [ChangeNowFloatingOffer,changeNowFloatingOffer],
  [LetsExchangeFloatingOffer,letsExchangeFloatingOffer],
  [ChangellyFloatingOffer,changellyFloatingOffer],
 ])
 floatingMap.delete(undefined)
 let m

 if(fixed) m=fixedMap
 else if(float) m=floatingMap
 else m=new Map([...fixedMap.entries(),...floatingMap.entries()])

 const sorted=[...m.entries()].sort(([,value],[,valueB])=>{
  if(!value&&!valueB) return 0
  if(!value) return -1
  if(!valueB) return 1
  return valueB-value
 })
 let i=0
 for(const[item,val]of sorted) {
  if(!val) continue
  const newIndex=i+1
  element.insertBefore(item,element.children[newIndex])
  i++
 }
 i=0
 for(const child of element.children) {
  if(i==1) child.classList.add(BestOffer)
  else child.classList.remove(BestOffer)
  i++
 }
}