/** @type {xyz.swapee.wc.IOffersTableElement._buildOffersAggregator} */
export default function buildOffersAggregator({
 isAggregating:isAggregating,

 changellyFixedOffer:changellyFixedOffer,
 changellyFloatingOffer:changellyFloatingOffer,

 // LetsExchange
 letsExchangeFixedOffer:letsExchangeFixedOffer,
 letsExchangeFloatingOffer:letsExchangeFloatingOffer,

 // ChangeNow
 changeNowFixedOffer:changeNowFixedOffer,
 changeNowFloatingOffer:changeNowFloatingOffer,

 exchangesLoaded:exchangesLoaded,
 exchangesTotal:exchangesTotal,
}){
 return (<div $id="OffersTable">
  {/* Changelly */}
  <div $id="ChangellyFloatingOffer" $reveal={changellyFloatingOffer}>
   <span $id="ChangellyFloatingOfferAmount">{changellyFloatingOffer}</span>
  </div>
  <div $id="ChangellyFixedOffer" $reveal={changellyFixedOffer}>
   <span $id="ChangellyFixedOfferAmount">{changellyFixedOffer}</span>
  </div>

  {/* Changelly */}
  <div $id="LetsExchangeFloatingOffer" $reveal={letsExchangeFloatingOffer}>
   <span $id="LetsExchangeFloatingOfferAmount">{letsExchangeFloatingOffer}</span>
  </div>
  <div $id="LetsExchangeFixedOffer" $reveal={letsExchangeFixedOffer}>
   <span $id="LetsExchangeFixedOfferAmount">{letsExchangeFixedOffer}</span>
  </div>

  {/* Changenow */}
  <div $id="ChangeNowFloatingOffer" $reveal={changeNowFloatingOffer}>
   <span $id="ChangeNowFloatingOfferAmount">{changeNowFloatingOffer}</span>
  </div>
  <div $id="ChangeNowFixedOffer" $reveal={changeNowFixedOffer}>
   <span $id="ChangeNowFixedOfferAmount">{changeNowFixedOffer}</span>
  </div>

  <span $id="ResetNowBu" Loading={isAggregating} />

  <span $id="ExchangesLoaded">{exchangesLoaded}</span>
  <span $id="ExchangesTotal">{exchangesTotal}</span>
 </div>)
}