/** @type {xyz.swapee.wc.IOffersTableElement._borrowClasses} */
export default function borrowClasses({BestOffer:BestOffer}){
 return <>
  <xyz.swapee.wc.OfferRowClasses BestOffer={BestOffer} />
 </>
}