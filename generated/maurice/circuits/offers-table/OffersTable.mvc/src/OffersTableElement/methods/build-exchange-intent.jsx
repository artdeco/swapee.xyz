/** @type {xyz.swapee.wc.IOffersTableElement._buildExchangeIntent} */
export default function buildExchangeIntent({fixed:fixed,float:float,currencyTo:currencyTo}) {
 return (<div $id="OffersTable">
  <div $id="OfferCryptoOuts">{currencyTo}</div>

  {/* Changelly */}
  <div $id="ChangellyFloatingOffer" Hidden={fixed} />
  <div $id="ChangellyFixedOffer" Hidden={float} />

  {/* Changelly */}
  <div $id="LetsExchangeFloatingOffer" Hidden={fixed} />
  <div $id="LetsExchangeFixedOffer" Hidden={float} />

  {/* Changenow */}
  <div $id="ChangeNowFloatingOffer" Hidden={fixed} />
  <div $id="ChangeNowFixedOffer" Hidden={float} />
 </div>)
}