/** @type {xyz.swapee.wc.IOffersTableElement._buildCryptoSelectOut} */
export default function buildCryptoSelectOut({selectedIcon:selectedIcon}){
 return (<div $id="OffersTable">
  <imgwr $id="CoinImWr">{selectedIcon}</imgwr>
 </div>)
}