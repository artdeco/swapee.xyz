/** @type {xyz.swapee.wc.IOffersTableElement._relay} */
export default function relay({OffersAggregator:OffersAggregator,This:This}){
 return (<>
  <This
   onReset={[
    OffersAggregator.loadChangellyFixedOffer,OffersAggregator.loadChangellyFloatingOffer,
    OffersAggregator.loadLetsExchangeFixedOffer,OffersAggregator.loadLetsExchangeFloatingOffer,
    OffersAggregator.loadChangeNowFixedOffer,OffersAggregator.loadChangeNowFloatingOffer,
   ]}
  />
 </>)
}