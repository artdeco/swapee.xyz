/** @type {xyz.swapee.wc.IOffersTableElement._render} */
export default function OffersTableRender({
 untilReset:untilReset,changellyFixedLink:changellyFixedLink,
 changellyFloatLink:changellyFloatLink,
},{
 reset:reset,
}) {
 return (<div $id="OffersTable">
  <a $id="ChangellyFixedLink" href={changellyFixedLink} />
  <a $id="ChangellyFloatLink" href={changellyFloatLink} />
  {/* todo: automatically recognise onChange on input and map to the value of the event. */}
  {/* todo: two-way-binding: a) via on-change b) via on-input */}
  <span $id="ResetIn">{untilReset}</span>
  <span $id="ResetNowBu" onClick={reset} />
 </div>)
}