/** @type {xyz.swapee.wc.IOffersTableElement._short} */
export default function short({},{
 ProgressCollapsar:ProgressCollapsar,
},{OffersAggregator:{
 allExchangesLoaded:allExchangesLoaded,
}}){
 return <>
  <ProgressCollapsar collapsed={allExchangesLoaded} />
 </>
}