import solder from './methods/solder'
import server from './methods/server'
import build from './methods/build'
import buildOffersAggregator from './methods/build-offers-aggregator'
import buildCryptoSelectOut from './methods/build-crypto-select-out'
import buildExchangeIntent from './methods/build-exchange-intent'
import render from './methods/render'
import borrowClasses from './methods/borrow-classes'
import OffersTableServerController from '../OffersTableServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractOffersTableElement from '../../gen/AbstractOffersTableElement'

/** @extends {xyz.swapee.wc.OffersTableElement} */
export default class OffersTableElement extends AbstractOffersTableElement.implements(
 OffersTableServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IOffersTableElement} */ ({
  solder:solder,
  server:server,
  build:build,
  buildOffersAggregator:buildOffersAggregator,
  buildCryptoSelectOut:buildCryptoSelectOut,
  buildExchangeIntent:buildExchangeIntent,
  render:render,
  borrowClasses:borrowClasses,
 }),
/**@type {!xyz.swapee.wc.IOffersTableElement}*/({
   classesMap: true,
   rootSelector:     `.OffersTable`,
   stylesheets:       [
    'html/styles/OffersTable.css',
    // 'html/styles/SwapeeLoading.css',
   ],
   globalStylesheet: 'html/styles/GlobalOffersTable.css',
   blockName:        'html/OffersTableBlock.html',
   // todo: builder needs This
   // todo: change to wire?
  }),
  /**@type {xyz.swapee.wc.IOffersTableDesigner}*/({
   // lendClasses({BestOffer:BestOffer}){
   //  return <>
   //   <xyz.swapee.wc.OffersTableClasses
   //    BestOffer={BestOffer} />
   //  </>
   // },
  }),
){}

// thank you for using web circuits
