import {Painter} from '@webcircuits/webcircuits'

const OffersTableVirtualDisplay=Painter.__trait(
 /**@type {!xyz.swapee.wc.IOffersTableVirtualDisplay}*/({
  paint:[
   function paintTableOrder({},{ExchangeIntent:ExchangeIntent,OffersAggregator:OffersAggregator}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _lan={}
    if(ExchangeIntent) {
     const{'cec31':fixed,'546ad':float}=ExchangeIntent.model
     _lan['3427b']={'cec31':fixed,'546ad':float}
    }else _lan['3427b']={}
    if(OffersAggregator) {
     const{'b89f1':changeNowFixedOffer,'9f4fb':changeNowFloatingOffer,'7b5f4':changellyFixedOffer,'2a73d':changellyFloatingOffer,'26339':letsExchangeFixedOffer,'a0ea0':letsExchangeFloatingOffer}=OffersAggregator.model
     _lan['89de3']={'b89f1':changeNowFixedOffer,'9f4fb':changeNowFloatingOffer,'7b5f4':changellyFixedOffer,'2a73d':changellyFloatingOffer,'26339':letsExchangeFixedOffer,'a0ea0':letsExchangeFloatingOffer}
    }else _lan['89de3']={}
    const _mem=serMemory({})
    t_pa({
     pid:'3c2666c',
     mem:_mem,
     lan:_lan,
    })
   },
  ],
 }),
)
export default OffersTableVirtualDisplay