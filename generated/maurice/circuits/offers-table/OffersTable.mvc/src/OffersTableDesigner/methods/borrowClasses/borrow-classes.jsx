import '@type.engineering/type-engineer'
const{h}=require(eval("'@type.engineering/web-computing'"))
/**@type {xyz.swapee.wc.IOffersTableDesigner._borrowClasses} */
export default function borrowClasses({BestOffer:BestOffer}){
 return h(Fragment,{},
  h('xyz.swapee.wc.OfferRowClasses',{ 'BestOffer':BestOffer })
 )
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLXRhYmxlL29mZmVycy10YWJsZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQWtXRyxTQUFTLGFBQWEsRUFBRSxtQkFBbUIsQ0FBQztDQUMzQyxPQUFPLFlBQUM7RUFDUCxvQ0FBK0IsWUFBYSxTQUFRO0NBQ3JEO0FBQ0QsQ0FBRiJ9