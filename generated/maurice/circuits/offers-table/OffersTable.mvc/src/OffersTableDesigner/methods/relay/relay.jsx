
/**@type {xyz.swapee.wc.IOffersTableDesigner._relay} */
export default function relay({OffersAggregator:OffersAggregator,This:This}){
 return (h(Fragment,{},
  h(This,{
   onReset:[
    OffersAggregator.loadChangellyFixedOffer,OffersAggregator.loadChangellyFloatingOffer,
    OffersAggregator.loadLetsExchangeFixedOffer,OffersAggregator.loadLetsExchangeFloatingOffer,
    OffersAggregator.loadChangeNowFixedOffer,OffersAggregator.loadChangeNowFloatingOffer,
   ]
  })
 ))
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLXRhYmxlL29mZmVycy10YWJsZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBNldHLFNBQVMsS0FBSyxFQUFFLGlDQUFpQyxDQUFDLFNBQVMsQ0FBQztDQUMzRCxPQUFPLENBQUMsWUFBQztFQUNSO0dBQ0MsUUFBUztJQUNSLGdCQUFnQixDQUFDLHVCQUF1QixDQUFDLGdCQUFnQixDQUFDLDBCQUEwQjtJQUNwRixnQkFBZ0IsQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsQ0FBQyw2QkFBNkI7SUFDMUYsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUMsZ0JBQWdCLENBQUMsMEJBQTBCO0dBQ3JGO0FBQ0g7Q0FDQyxDQUFrQztBQUNuQyxDQUFGIn0=