import OffersTableHtmlController from '../OffersTableHtmlController'
import OffersTableHtmlComputer from '../OffersTableHtmlComputer'
import OffersTableHtmlProcessor from '../OffersTableHtmlProcessor'
import OffersTableVirtualDisplay from '../OffersTableVirtualDisplay'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractOffersTableHtmlComponent} from '../../gen/AbstractOffersTableHtmlComponent'

/** @extends {xyz.swapee.wc.OffersTableHtmlComponent} */
export default class extends AbstractOffersTableHtmlComponent.implements(
 OffersTableHtmlController,
 OffersTableHtmlComputer,
 OffersTableHtmlProcessor,
 OffersTableVirtualDisplay,
 IntegratedComponentInitialiser,
){}