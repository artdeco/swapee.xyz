import AbstractHyperOffersTableInterruptLine from '../../../../gen/AbstractOffersTableInterruptLine/hyper/AbstractHyperOffersTableInterruptLine'
import OffersTableInterruptLine from '../../OffersTableInterruptLine'
import OffersTableInterruptLineGeneralAspects from '../OffersTableInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperOffersTableInterruptLine} */
export default class extends AbstractHyperOffersTableInterruptLine
 .consults(
  OffersTableInterruptLineGeneralAspects,
 )
 .implements(
  OffersTableInterruptLine,
 )
{}