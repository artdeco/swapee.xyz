/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice': {
  'id': 1,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice': {
  'id': 2,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel': {
  'id': 3,
  'symbols': {},
  'methods': {
   'beforeAtAmountInChange': 1,
   'afterAtAmountInChange': 2,
   'afterAtAmountInChangeThrows': 3,
   'afterAtAmountInChangeReturns': 4,
   'afterAtAmountInChangeCancels': 5
  }
 },
 'xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller': {
  'id': 4,
  'symbols': {},
  'methods': {
   'atAmountInChange': 1
  }
 },
 'xyz.swapee.wc.BOffersTableInterruptLineAspects': {
  'id': 5,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTableInterruptLineAspects': {
  'id': 6,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IHyperOffersTableInterruptLine': {
  'id': 7,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableInterruptLine': {
  'id': 8,
  'symbols': {},
  'methods': {
   'atAmountInChange': 1
  }
 }
})