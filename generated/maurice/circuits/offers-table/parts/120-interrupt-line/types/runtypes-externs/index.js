/** @const {?} */ $xyz.swapee.wc.IOffersTableInterruptLine
/** @const {?} */ $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
/** @const {?} */ $xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller
/** @const {?} */ $xyz.swapee.wc.IOffersTableInterruptLineAspects
/** @const {?} */ $xyz.swapee.wc.IHyperOffersTableInterruptLine
/** @const {?} */ xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
/** @const {?} */ xyz.swapee.wc.IOffersTableInterruptLine
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLine.Initialese filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @record */
$xyz.swapee.wc.IOffersTableInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLine.Initialese} */
xyz.swapee.wc.IOffersTableInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineCaster filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @interface */
$xyz.swapee.wc.IOffersTableInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableInterruptLine} */
$xyz.swapee.wc.IOffersTableInterruptLineCaster.prototype.asIOffersTableInterruptLine
/** @type {!xyz.swapee.wc.BoundIOffersTableTouchscreen} */
$xyz.swapee.wc.IOffersTableInterruptLineCaster.prototype.asIOffersTableTouchscreen
/** @type {!xyz.swapee.wc.BoundOffersTableInterruptLine} */
$xyz.swapee.wc.IOffersTableInterruptLineCaster.prototype.superOffersTableInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLineCaster}
 */
xyz.swapee.wc.IOffersTableInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.IOffersTableController}
 */
$xyz.swapee.wc.IOffersTableInterruptLine = function() {}
/** @return {(undefined|!xyz.swapee.wc.front.OffersTableInputs)} */
$xyz.swapee.wc.IOffersTableInterruptLine.prototype.atAmountInChange = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLine}
 */
xyz.swapee.wc.IOffersTableInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLine.Initialese>}
 */
$xyz.swapee.wc.OffersTableInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.OffersTableInterruptLine
/** @type {function(new: xyz.swapee.wc.IOffersTableInterruptLine)} */
xyz.swapee.wc.OffersTableInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.OffersTableInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.AbstractOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableInterruptLine}
 */
$xyz.swapee.wc.AbstractOffersTableInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableInterruptLine)} */
xyz.swapee.wc.AbstractOffersTableInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @interface */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._beforeAtAmountInChange|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._beforeAtAmountInChange>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice.prototype.beforeAtAmountInChange
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChange|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChange>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice.prototype.afterAtAmountInChange
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeThrows>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice.prototype.afterAtAmountInChangeThrows
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeReturns>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice.prototype.afterAtAmountInChangeReturns
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeCancels>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice.prototype.afterAtAmountInChangeCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice}
 */
$xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice
/** @type {function(new: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice)} */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange<THIS>>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.beforeAtAmountInChange
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange<THIS>>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAmountInChange
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows<THIS>>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAmountInChangeThrows
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns<THIS>>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAmountInChangeReturns
/** @type {(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels<THIS>>)} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAmountInChangeCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @template THIS
 * @extends {$xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice
/** @type {function(new: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>)} */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @interface */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.beforeAtAmountInChange = function(data) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChange = function(data) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChangeThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChangeReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChangeCancels = function(data) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLineJoinpointModel filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.OffersTableInterruptLineJoinpointModel = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersTableInterruptLineJoinpointModel}
 */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModel
/** @type {function(new: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel)} */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModel.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.RecordIOffersTableInterruptLineJoinpointModel filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {{ beforeAtAmountInChange: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.beforeAtAmountInChange, afterAtAmountInChange: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChange, afterAtAmountInChangeThrows: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeThrows, afterAtAmountInChangeReturns: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeReturns, afterAtAmountInChangeCancels: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeCancels }} */
xyz.swapee.wc.RecordIOffersTableInterruptLineJoinpointModel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BoundIOffersTableInterruptLineJoinpointModel filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.BoundIOffersTableInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundIOffersTableInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BoundOffersTableInterruptLineJoinpointModel filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableInterruptLineJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundOffersTableInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.beforeAtAmountInChange filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.beforeAtAmountInChange
/** @typedef {function(this: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel, !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._beforeAtAmountInChange
/** @typedef {typeof $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange

// nss:xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChange filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChange
/** @typedef {function(this: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel, !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChange
/** @typedef {typeof $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange

// nss:xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeThrows filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeThrows
/** @typedef {function(this: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel, !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeThrows
/** @typedef {typeof $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows

// nss:xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeReturns filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeReturns
/** @typedef {function(this: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel, !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeReturns
/** @typedef {typeof $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns

// nss:xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeCancels filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeCancels
/** @typedef {function(this: xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel, !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData=): void} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeCancels
/** @typedef {typeof $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels

// nss:xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @record */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese} */
xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller = function() {}
/** @type {number} */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.prototype.beforeAtAmountInChange
/** @type {number} */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.prototype.afterAtAmountInChange
/** @type {number} */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.prototype.afterAtAmountInChangeThrows
/** @type {number} */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.prototype.afterAtAmountInChangeReturns
/** @type {number} */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.prototype.afterAtAmountInChangeCancels
/** @return {void} */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.prototype.atAmountInChange = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese>}
 */
$xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller, ...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
$xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller)} */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.__extend
/**
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.continues
/**
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLineAspectsInstallerConstructor filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller, ...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.OffersTableInterruptLineAspectsInstallerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.OffersTableInputs} value
 * @return {void}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData = function() {}
/** @type {!xyz.swapee.wc.front.OffersTableInputs} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData = function() {}
/** @type {!xyz.swapee.wc.front.OffersTableInputs} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.OffersTableInputs} value
 * @return {?}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData}
 */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableInterruptLine} */
$xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster.prototype.asIOffersTableInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster<THIS>}
 */
xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BOffersTableInterruptLineAspects filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @interface
 * @extends {xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster<THIS>}
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.BOffersTableInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BOffersTableInterruptLineAspects<THIS>}
 */
xyz.swapee.wc.BOffersTableInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @record */
$xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese} */
xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersTableInterruptLineAspects
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @interface */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersTableInterruptLine} */
$xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster.prototype.asIOffersTableInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster}
 */
xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLineAspects filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster}
 * @extends {xyz.swapee.wc.BOffersTableInterruptLineAspects<!xyz.swapee.wc.IOffersTableInterruptLineAspects>}
 */
$xyz.swapee.wc.IOffersTableInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersTableInterruptLineAspects}
 */
xyz.swapee.wc.IOffersTableInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLineAspects filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese} init
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese>}
 */
$xyz.swapee.wc.OffersTableInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.OffersTableInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.IOffersTableInterruptLineAspects, ...!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese)} */
xyz.swapee.wc.OffersTableInterruptLineAspects.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.OffersTableInterruptLineAspects.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.AbstractOffersTableInterruptLineAspects filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
$xyz.swapee.wc.AbstractOffersTableInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.AbstractOffersTableInterruptLineAspects)} */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersTableInterruptLineAspects|typeof xyz.swapee.wc.OffersTableInterruptLineAspects)|(!xyz.swapee.wc.BOffersTableInterruptLineAspects|typeof xyz.swapee.wc.BOffersTableInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersTableInterruptLineAspects|typeof xyz.swapee.wc.OffersTableInterruptLineAspects)|(!xyz.swapee.wc.BOffersTableInterruptLineAspects|typeof xyz.swapee.wc.BOffersTableInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersTableInterruptLineAspects|typeof xyz.swapee.wc.OffersTableInterruptLineAspects)|(!xyz.swapee.wc.BOffersTableInterruptLineAspects|typeof xyz.swapee.wc.BOffersTableInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.OffersTableInterruptLineAspectsConstructor filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {function(new: xyz.swapee.wc.IOffersTableInterruptLineAspects, ...!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese)} */
xyz.swapee.wc.OffersTableInterruptLineAspectsConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersTableInterruptLine.Initialese}
 */
$xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese} */
xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IHyperOffersTableInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IHyperOffersTableInterruptLineCaster filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @interface */
$xyz.swapee.wc.IHyperOffersTableInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIHyperOffersTableInterruptLine} */
$xyz.swapee.wc.IHyperOffersTableInterruptLineCaster.prototype.asIHyperOffersTableInterruptLine
/** @type {!xyz.swapee.wc.BoundHyperOffersTableInterruptLine} */
$xyz.swapee.wc.IHyperOffersTableInterruptLineCaster.prototype.superHyperOffersTableInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperOffersTableInterruptLineCaster}
 */
xyz.swapee.wc.IHyperOffersTableInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IHyperOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IHyperOffersTableInterruptLineCaster}
 * @extends {xyz.swapee.wc.IOffersTableInterruptLine}
 */
$xyz.swapee.wc.IHyperOffersTableInterruptLine = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperOffersTableInterruptLine}
 */
xyz.swapee.wc.IHyperOffersTableInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.HyperOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese} init
 * @implements {xyz.swapee.wc.IHyperOffersTableInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese>}
 */
$xyz.swapee.wc.HyperOffersTableInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.HyperOffersTableInterruptLine
/** @type {function(new: xyz.swapee.wc.IHyperOffersTableInterruptLine, ...!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese)} */
xyz.swapee.wc.HyperOffersTableInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.HyperOffersTableInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.AbstractHyperOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
$xyz.swapee.wc.AbstractHyperOffersTableInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractHyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractHyperOffersTableInterruptLine)} */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IHyperOffersTableInterruptLine|typeof xyz.swapee.wc.HyperOffersTableInterruptLine)|(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractHyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.IHyperOffersTableInterruptLine|typeof xyz.swapee.wc.HyperOffersTableInterruptLine)|(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.IHyperOffersTableInterruptLine|typeof xyz.swapee.wc.HyperOffersTableInterruptLine)|(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.__trait
/**
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLineAspects|function(new: xyz.swapee.wc.IOffersTableInterruptLineAspects))} aides
 * @return {typeof xyz.swapee.wc.AbstractHyperOffersTableInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.consults

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.HyperOffersTableInterruptLineConstructor filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {function(new: xyz.swapee.wc.IHyperOffersTableInterruptLine, ...!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese)} */
xyz.swapee.wc.HyperOffersTableInterruptLineConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.RecordIHyperOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIHyperOffersTableInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BoundIHyperOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIHyperOffersTableInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IHyperOffersTableInterruptLineCaster}
 */
$xyz.swapee.wc.BoundIHyperOffersTableInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIHyperOffersTableInterruptLine} */
xyz.swapee.wc.BoundIHyperOffersTableInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BoundHyperOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIHyperOffersTableInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundHyperOffersTableInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundHyperOffersTableInterruptLine} */
xyz.swapee.wc.BoundHyperOffersTableInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.RecordIOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {{ atAmountInChange: xyz.swapee.wc.IOffersTableInterruptLine.atAmountInChange }} */
xyz.swapee.wc.RecordIOffersTableInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BoundIOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersTableInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.BoundIOffersTableController}
 */
$xyz.swapee.wc.BoundIOffersTableInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersTableInterruptLine} */
xyz.swapee.wc.BoundIOffersTableInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.BoundOffersTableInterruptLine filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersTableInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersTableInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersTableInterruptLine} */
xyz.swapee.wc.BoundOffersTableInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} xyz.swapee.wc.IOffersTableInterruptLine.atAmountInChange filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.OffersTableInputs)}
 */
$xyz.swapee.wc.IOffersTableInterruptLine.__atAmountInChange = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.OffersTableInputs)} */
xyz.swapee.wc.IOffersTableInterruptLine.atAmountInChange
/** @typedef {function(this: xyz.swapee.wc.IOffersTableInterruptLine): (undefined|!xyz.swapee.wc.front.OffersTableInputs)} */
xyz.swapee.wc.IOffersTableInterruptLine._atAmountInChange
/** @typedef {typeof $xyz.swapee.wc.IOffersTableInterruptLine.__atAmountInChange} */
xyz.swapee.wc.IOffersTableInterruptLine.__atAmountInChange

// nss:xyz.swapee.wc.IOffersTableInterruptLine,$xyz.swapee.wc.IOffersTableInterruptLine,xyz.swapee.wc
/* @typal-end */