/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IOffersTableInterruptLine={}
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel={}
xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller={}
xyz.swapee.wc.IOffersTableInterruptLineAspects={}
xyz.swapee.wc.IHyperOffersTableInterruptLine={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-table/parts/120-interrupt-line/types/design/120-IOffersTableInterruptLine.xml} filter:!ControllerPlugin~props 71cb17cf0219749bc3ce49183ede2476 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersTableInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableInterruptLine)} xyz.swapee.wc.AbstractOffersTableInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableInterruptLine} xyz.swapee.wc.OffersTableInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableInterruptLine
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractOffersTableInterruptLine.constructor&xyz.swapee.wc.OffersTableInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractOffersTableInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)|(!xyz.swapee.wc.front.IOffersTableController|typeof xyz.swapee.wc.front.OffersTableController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLine.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtAmountInChange=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._beforeAtAmountInChange|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._beforeAtAmountInChange>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtAmountInChange=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChange|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChange>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtAmountInChangeThrows=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtAmountInChangeReturns=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtAmountInChangeCancels=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeCancels>} */ (void 0)
  }
}
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice

/**
 * A concrete class of _IOffersTableInterruptLineJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice = class extends xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelHyperslice { }
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.OffersTableInterruptLineJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtAmountInChange=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtAmountInChange=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtAmountInChangeThrows=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtAmountInChangeReturns=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtAmountInChangeCancels=/** @type {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice

/**
 * A concrete class of _IOffersTableInterruptLineJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice = class extends xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice { }
xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.OffersTableInterruptLineJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IOffersTableInterruptLine`'s methods.
 * @interface xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel = class { }
/** @type {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.beforeAtAmountInChange} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.beforeAtAmountInChange = function() {}
/** @type {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChange} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChange = function() {}
/** @type {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeThrows} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChangeThrows = function() {}
/** @type {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeReturns} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChangeReturns = function() {}
/** @type {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeCancels} */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.prototype.afterAtAmountInChangeCancels = function() {}

/**
 * A concrete class of _IOffersTableInterruptLineJoinpointModel_ instances.
 * @constructor xyz.swapee.wc.OffersTableInterruptLineJoinpointModel
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel} An interface that enumerates the joinpoints of `IOffersTableInterruptLine`'s methods.
 */
xyz.swapee.wc.OffersTableInterruptLineJoinpointModel = class extends xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel { }
xyz.swapee.wc.OffersTableInterruptLineJoinpointModel.prototype.constructor = xyz.swapee.wc.OffersTableInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel} */
xyz.swapee.wc.RecordIOffersTableInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel} xyz.swapee.wc.BoundIOffersTableInterruptLineJoinpointModel */

/** @typedef {xyz.swapee.wc.OffersTableInterruptLineJoinpointModel} xyz.swapee.wc.BoundOffersTableInterruptLineJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller)} xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller} xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.prototype.constructor = xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese[]) => xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller} xyz.swapee.wc.OffersTableInterruptLineAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller */
xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeAtAmountInChange=/** @type {number} */ (void 0)
    this.afterAtAmountInChange=/** @type {number} */ (void 0)
    this.afterAtAmountInChangeThrows=/** @type {number} */ (void 0)
    this.afterAtAmountInChangeReturns=/** @type {number} */ (void 0)
    this.afterAtAmountInChangeCancels=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {void}
   */
  atAmountInChange() { }
}
/**
 * Create a new *IOffersTableInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese>)} xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller} xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersTableInterruptLineAspectsInstaller_ instances.
 * @constructor xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableInterruptLineAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.OffersTableInterruptLineAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atAmountInChange` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.OffersTableInputs) => void} sub Cancels a call to `atAmountInChange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData&xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData
 * @prop {!xyz.swapee.wc.front.OffersTableInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData&xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atAmountInChange` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData&xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData
 * @prop {!xyz.swapee.wc.front.OffersTableInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.OffersTableInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData&xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData&xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {function(new: xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster<THIS>&xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.wc.BOffersTableInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IOffersTableInterruptLine* that bind to an instance.
 * @interface xyz.swapee.wc.BOffersTableInterruptLineAspects
 * @template THIS
 */
xyz.swapee.wc.BOffersTableInterruptLineAspects = class extends /** @type {xyz.swapee.wc.BOffersTableInterruptLineAspects.constructor&xyz.swapee.wc.IOffersTableInterruptLineJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.wc.BOffersTableInterruptLineAspects.prototype.constructor = xyz.swapee.wc.BOffersTableInterruptLineAspects

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersTableInterruptLineAspects)} xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersTableInterruptLineAspects} xyz.swapee.wc.OffersTableInterruptLineAspects.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersTableInterruptLineAspects` interface.
 * @constructor xyz.swapee.wc.AbstractOffersTableInterruptLineAspects
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects = class extends /** @type {xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.constructor&xyz.swapee.wc.OffersTableInterruptLineAspects.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.prototype.constructor = xyz.swapee.wc.AbstractOffersTableInterruptLineAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.class = /** @type {typeof xyz.swapee.wc.AbstractOffersTableInterruptLineAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLineAspects|typeof xyz.swapee.wc.OffersTableInterruptLineAspects)|(!xyz.swapee.wc.BOffersTableInterruptLineAspects|typeof xyz.swapee.wc.BOffersTableInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLineAspects|typeof xyz.swapee.wc.OffersTableInterruptLineAspects)|(!xyz.swapee.wc.BOffersTableInterruptLineAspects|typeof xyz.swapee.wc.BOffersTableInterruptLineAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersTableInterruptLineAspects|typeof xyz.swapee.wc.OffersTableInterruptLineAspects)|(!xyz.swapee.wc.BOffersTableInterruptLineAspects|typeof xyz.swapee.wc.BOffersTableInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOffersTableInterruptLineAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese[]) => xyz.swapee.wc.IOffersTableInterruptLineAspects} xyz.swapee.wc.OffersTableInterruptLineAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster&xyz.swapee.wc.BOffersTableInterruptLineAspects<!xyz.swapee.wc.IOffersTableInterruptLineAspects>)} xyz.swapee.wc.IOffersTableInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.BOffersTableInterruptLineAspects} xyz.swapee.wc.BOffersTableInterruptLineAspects.typeof */
/**
 * The aspects of the *IOffersTableInterruptLine*.
 * @interface xyz.swapee.wc.IOffersTableInterruptLineAspects
 */
xyz.swapee.wc.IOffersTableInterruptLineAspects = class extends /** @type {xyz.swapee.wc.IOffersTableInterruptLineAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.BOffersTableInterruptLineAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersTableInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersTableInterruptLineAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableInterruptLineAspects&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese>)} xyz.swapee.wc.OffersTableInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineAspects} xyz.swapee.wc.IOffersTableInterruptLineAspects.typeof */
/**
 * A concrete class of _IOffersTableInterruptLineAspects_ instances.
 * @constructor xyz.swapee.wc.OffersTableInterruptLineAspects
 * @implements {xyz.swapee.wc.IOffersTableInterruptLineAspects} The aspects of the *IOffersTableInterruptLine*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableInterruptLineAspects = class extends /** @type {xyz.swapee.wc.OffersTableInterruptLineAspects.constructor&xyz.swapee.wc.IOffersTableInterruptLineAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersTableInterruptLineAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersTableInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersTableInterruptLineAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLineAspects}
 */
xyz.swapee.wc.OffersTableInterruptLineAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BOffersTableInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster
 * @template THIS
 */
xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster = class { }
/**
 * Cast the _BOffersTableInterruptLineAspects_ instance into the _BoundIOffersTableInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableInterruptLine}
 */
xyz.swapee.wc.BOffersTableInterruptLineAspectsCaster.prototype.asIOffersTableInterruptLine

/**
 * Contains getters to cast the _IOffersTableInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster
 */
xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster = class { }
/**
 * Cast the _IOffersTableInterruptLineAspects_ instance into the _BoundIOffersTableInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableInterruptLine}
 */
xyz.swapee.wc.IOffersTableInterruptLineAspectsCaster.prototype.asIOffersTableInterruptLine

/** @typedef {xyz.swapee.wc.IOffersTableInterruptLine.Initialese} xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.HyperOffersTableInterruptLine)} xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.HyperOffersTableInterruptLine} xyz.swapee.wc.HyperOffersTableInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IHyperOffersTableInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractHyperOffersTableInterruptLine
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.constructor&xyz.swapee.wc.HyperOffersTableInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractHyperOffersTableInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractHyperOffersTableInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IHyperOffersTableInterruptLine|typeof xyz.swapee.wc.HyperOffersTableInterruptLine)|(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractHyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IHyperOffersTableInterruptLine|typeof xyz.swapee.wc.HyperOffersTableInterruptLine)|(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IHyperOffersTableInterruptLine|typeof xyz.swapee.wc.HyperOffersTableInterruptLine)|(!xyz.swapee.wc.IOffersTableInterruptLine|typeof xyz.swapee.wc.OffersTableInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.wc.IOffersTableInterruptLineAspects|function(new: xyz.swapee.wc.IOffersTableInterruptLineAspects)} aides The list of aides that advise the IOffersTableInterruptLine to implement aspects.
 * @return {typeof xyz.swapee.wc.AbstractHyperOffersTableInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperOffersTableInterruptLine.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese[]) => xyz.swapee.wc.IHyperOffersTableInterruptLine} xyz.swapee.wc.HyperOffersTableInterruptLineConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IHyperOffersTableInterruptLineCaster&xyz.swapee.wc.IOffersTableInterruptLine)} xyz.swapee.wc.IHyperOffersTableInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLine} xyz.swapee.wc.IOffersTableInterruptLine.typeof */
/** @interface xyz.swapee.wc.IHyperOffersTableInterruptLine */
xyz.swapee.wc.IHyperOffersTableInterruptLine = class extends /** @type {xyz.swapee.wc.IHyperOffersTableInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersTableInterruptLine.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperOffersTableInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IHyperOffersTableInterruptLine.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IHyperOffersTableInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese>)} xyz.swapee.wc.HyperOffersTableInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IHyperOffersTableInterruptLine} xyz.swapee.wc.IHyperOffersTableInterruptLine.typeof */
/**
 * A concrete class of _IHyperOffersTableInterruptLine_ instances.
 * @constructor xyz.swapee.wc.HyperOffersTableInterruptLine
 * @implements {xyz.swapee.wc.IHyperOffersTableInterruptLine} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.HyperOffersTableInterruptLine = class extends /** @type {xyz.swapee.wc.HyperOffersTableInterruptLine.constructor&xyz.swapee.wc.IHyperOffersTableInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperOffersTableInterruptLine* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperOffersTableInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperOffersTableInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.HyperOffersTableInterruptLine.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperOffersTableInterruptLine}
 */
xyz.swapee.wc.HyperOffersTableInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IHyperOffersTableInterruptLine} */
xyz.swapee.wc.RecordIHyperOffersTableInterruptLine

/** @typedef {xyz.swapee.wc.IHyperOffersTableInterruptLine} xyz.swapee.wc.BoundIHyperOffersTableInterruptLine */

/** @typedef {xyz.swapee.wc.HyperOffersTableInterruptLine} xyz.swapee.wc.BoundHyperOffersTableInterruptLine */

/**
 * Contains getters to cast the _IHyperOffersTableInterruptLine_ interface.
 * @interface xyz.swapee.wc.IHyperOffersTableInterruptLineCaster
 */
xyz.swapee.wc.IHyperOffersTableInterruptLineCaster = class { }
/**
 * Cast the _IHyperOffersTableInterruptLine_ instance into the _BoundIHyperOffersTableInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIHyperOffersTableInterruptLine}
 */
xyz.swapee.wc.IHyperOffersTableInterruptLineCaster.prototype.asIHyperOffersTableInterruptLine
/**
 * Access the _HyperOffersTableInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundHyperOffersTableInterruptLine}
 */
xyz.swapee.wc.IHyperOffersTableInterruptLineCaster.prototype.superHyperOffersTableInterruptLine

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersTableInterruptLineCaster&xyz.swapee.wc.front.IOffersTableController)} xyz.swapee.wc.IOffersTableInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOffersTableController} xyz.swapee.wc.front.IOffersTableController.typeof */
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @interface xyz.swapee.wc.IOffersTableInterruptLine
 */
xyz.swapee.wc.IOffersTableInterruptLine = class extends /** @type {xyz.swapee.wc.IOffersTableInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IOffersTableController.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IOffersTableInterruptLine.atAmountInChange} */
xyz.swapee.wc.IOffersTableInterruptLine.prototype.atAmountInChange = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersTableInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLine.Initialese>)} xyz.swapee.wc.OffersTableInterruptLine.constructor */
/**
 * A concrete class of _IOffersTableInterruptLine_ instances.
 * @constructor xyz.swapee.wc.OffersTableInterruptLine
 * @implements {xyz.swapee.wc.IOffersTableInterruptLine} Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersTableInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.OffersTableInterruptLine = class extends /** @type {xyz.swapee.wc.OffersTableInterruptLine.constructor&xyz.swapee.wc.IOffersTableInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OffersTableInterruptLine.prototype.constructor = xyz.swapee.wc.OffersTableInterruptLine
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersTableInterruptLine}
 */
xyz.swapee.wc.OffersTableInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersTableInterruptLine} */
xyz.swapee.wc.RecordIOffersTableInterruptLine

/** @typedef {xyz.swapee.wc.IOffersTableInterruptLine} xyz.swapee.wc.BoundIOffersTableInterruptLine */

/** @typedef {xyz.swapee.wc.OffersTableInterruptLine} xyz.swapee.wc.BoundOffersTableInterruptLine */

/**
 * Contains getters to cast the _IOffersTableInterruptLine_ interface.
 * @interface xyz.swapee.wc.IOffersTableInterruptLineCaster
 */
xyz.swapee.wc.IOffersTableInterruptLineCaster = class { }
/**
 * Cast the _IOffersTableInterruptLine_ instance into the _BoundIOffersTableInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableInterruptLine}
 */
xyz.swapee.wc.IOffersTableInterruptLineCaster.prototype.asIOffersTableInterruptLine
/**
 * Cast the _IOffersTableInterruptLine_ instance into the _.BoundIOffersTableTouchscreen_ type.
 * @type {!xyz.swapee.wc.BoundIOffersTableTouchscreen}
 */
xyz.swapee.wc.IOffersTableInterruptLineCaster.prototype.asIOffersTableTouchscreen
/**
 * Access the _OffersTableInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersTableInterruptLine}
 */
xyz.swapee.wc.IOffersTableInterruptLineCaster.prototype.superOffersTableInterruptLine

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData) => void} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__beforeAtAmountInChange<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel>} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._beforeAtAmountInChange */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.beforeAtAmountInChange} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.BeforeAtAmountInChangePointcutData} [data] Metadata passed to the pointcuts of _atAmountInChange_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atAmountInChange` method from being executed.
 * - `sub` _(value: !front.OffersTableInputs) =&gt; void_ Cancels a call to `atAmountInChange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.beforeAtAmountInChange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData) => void} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChange<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel>} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChange */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChange} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterAtAmountInChangePointcutData} [data] Metadata passed to the pointcuts of _atAmountInChange_ at the `after` joinpoint.
 * - `res` _!front.OffersTableInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData) => void} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeThrows<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel>} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeThrows */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterThrowsAtAmountInChangePointcutData} [data] Metadata passed to the pointcuts of _atAmountInChange_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atAmountInChange` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData) => void} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeReturns<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel>} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeReturns */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterReturnsAtAmountInChangePointcutData} [data] Metadata passed to the pointcuts of _atAmountInChange_ at the `afterReturns` joinpoint.
 * - `res` _!front.OffersTableInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.OffersTableInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData) => void} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.__afterAtAmountInChangeCancels<!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel>} xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel._afterAtAmountInChangeCancels */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.AfterCancelsAtAmountInChangePointcutData} [data] Metadata passed to the pointcuts of _atAmountInChange_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersTableInterruptLineJoinpointModel.AtAmountInChangePointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel.afterAtAmountInChangeCancels = function(data) {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.OffersTableInputs)} xyz.swapee.wc.IOffersTableInterruptLine.__atAmountInChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersTableInterruptLine.__atAmountInChange<!xyz.swapee.wc.IOffersTableInterruptLine>} xyz.swapee.wc.IOffersTableInterruptLine._atAmountInChange */
/** @typedef {typeof xyz.swapee.wc.IOffersTableInterruptLine.atAmountInChange} */
/** @return {void|!xyz.swapee.wc.front.OffersTableInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IOffersTableInterruptLine.atAmountInChange = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersTableInterruptLineJoinpointModel,xyz.swapee.wc.IOffersTableInterruptLine
/* @typal-end */