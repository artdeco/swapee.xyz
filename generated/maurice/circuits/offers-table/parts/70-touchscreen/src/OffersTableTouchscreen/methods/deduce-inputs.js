/**@type {xyz.swapee.wc.IOffersTableTouchscreen._deduceInputs}*/
export default
function deduceInputs(){
 const{asIOffersTableDisplay:{AmountIn:AmountIn}}=this
 const val=parseFloat(AmountIn.value)
 return{
  amountIn:val, // todo: can not automatically deduce values?
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLXRhYmxlL29mZmVycy10YWJsZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBZ0tHLFNBQVMsWUFBWSxDQUFDO0NBQ3JCLE1BQU0sdUJBQXVCLGlCQUFpQixHQUFHO0NBQ2pELE1BQU0sR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7Q0FDOUI7RUFDQyxZQUFZLEVBQUUsR0FBRyxNQUFNLElBQUksSUFBSSxjQUFjLE9BQU87QUFDdEQ7QUFDQSxDQUFGIn0=