import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOffersTableTouchscreen._constructor} */
export default function constructor() {
 const{asIOffersTableController:{resetTick:resetTick}}=this
 setInterval(resetTick,1000)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLXRhYmxlL29mZmVycy10YWJsZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBNkpHLFNBQVMsV0FBVyxDQUFDO0NBQ3BCLE1BQU0sMEJBQTBCLG1CQUFtQixHQUFHO0NBQ3RELFdBQVcsQ0FBQyxTQUFTLENBQUM7QUFDdkIsQ0FBRiJ9