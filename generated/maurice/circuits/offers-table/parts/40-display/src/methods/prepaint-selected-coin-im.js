/**@type {xyz.swapee.wc.IOffersTableDisplay._paintSelectedCoinIm} */
export default function prepaintSelectedCoinIm(memory,land) {
 const{asIOffersTableDisplay:{paintSelectedCoinIm:paintSelectedCoinIm}}=this
 const _lan={}
 const CryptoSelectOut=land['7944b']
 if(CryptoSelectOut) {
  const{
   'ef7de':selected,
  }=CryptoSelectOut
  _lan.CryptoSelectOut={
   selected:selected,
  }
 }else _lan.CryptoSelectOut={}

 paintSelectedCoinIm(memory,_lan)
}
prepaintSelectedCoinIm['_id']='6fdaf8b'