/**@type {xyz.swapee.wc.IOffersTableDisplay._paintOrderTable} */
export default function prepaintOrderTable({},land,prev) {
 const{asIOffersTableDisplay:{desMemory:desMemory,paintOrderTable:paintOrderTable}}=this
 paintOrderTable({},land,prev?desMemory(prev):void 0)
}
prepaintOrderTable['_id']='e136180'