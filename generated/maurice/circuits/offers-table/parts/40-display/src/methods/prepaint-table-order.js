/**@type {xyz.swapee.wc.IOffersTableDisplay._paintTableOrder} */
export default function prepaintTableOrder(memory,land) {
 const{asIOffersTableDisplay:{paintTableOrder:paintTableOrder}}=this
 const _lan={}
 const ExchangeIntent=land['3427b']
 if(ExchangeIntent) {
  const{
   'cec31':fixed,
   '546ad':float,
  }=ExchangeIntent
  _lan.ExchangeIntent={
   fixed:fixed,
   float:float,
  }
 }else _lan.ExchangeIntent={}
 const OffersAggregator=land['89de3']
 if(OffersAggregator) {
  const{
   'b89f1':changeNowFixedOffer,
   'a0ea0':letsExchangeFloatingOffer,
   '26339':letsExchangeFixedOffer,
   '9f4fb':changeNowFloatingOffer,
   '7b5f4':changellyFixedOffer,
   '2a73d':changellyFloatingOffer,
  }=OffersAggregator
  _lan.OffersAggregator={
   changeNowFixedOffer:changeNowFixedOffer,
   letsExchangeFloatingOffer:letsExchangeFloatingOffer,
   letsExchangeFixedOffer:letsExchangeFixedOffer,
   changeNowFloatingOffer:changeNowFloatingOffer,
   changellyFixedOffer:changellyFixedOffer,
   changellyFloatingOffer:changellyFloatingOffer,
  }
 }else _lan.OffersAggregator={}

 paintTableOrder(memory,_lan)
}
prepaintTableOrder['_id']='3c2666c'