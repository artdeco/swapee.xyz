import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOffersTableDisplay._paintSelectedCoinIm} */
export default function paintSelectedCoinIm(_,{CryptoSelectOut:{
 selected:selected,
}}){
 // x-crypto-selected-im-wr
 if(!selected) return
 const{
  asIOffersTableDisplay:{CryptoSelectedImWr:CryptoSelectedImWr},
  asIOffersTableScreen:{setCoinIm:setCoinIm},
 }=this
 const img=CryptoSelectedImWr.querySelector('img')
 setCoinIm(img.src)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLXRhYmxlL29mZmVycy10YWJsZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBMElHLFNBQVMsbUJBQW1CLENBQUMsQ0FBQyxFQUFFO0NBQy9CLGlCQUFpQjtBQUNsQixFQUFFO0NBQ0QsR0FBRztDQUNILEVBQUUsQ0FBQyxXQUFXO0NBQ2Q7RUFDQyx1QkFBdUIscUNBQXFDO0VBQzVELHNCQUFzQixtQkFBbUI7R0FDeEM7Q0FDRixNQUFNLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUM7Q0FDM0MsU0FBUyxDQUFDLEdBQUcsQ0FBQztBQUNmLENBQUYifQ==