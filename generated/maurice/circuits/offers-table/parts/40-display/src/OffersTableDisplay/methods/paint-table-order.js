/**@type {xyz.swapee.wc.IOffersTableDisplay._paintTableOrder}*/
export default
function paintTableOrder({onlyFixedRate,onlyFloatingRate},{OffersAggregator:{
 changeNowFixedOffer,letsExchangeFloatingOffer,letsExchangeFixedOffer,
 changeNowFloatingOffer,changellyFixedOffer,changellyFloatingOffer,
}}){
 // todo: basically, need :first-of-class:not(.Hidden)
 const{asIOffersTableDisplay:{
  element:element,
  ChangeNowFixedOffer,ChangeNowFloatingOffer,
  LetsExchangeFixedOffer,LetsExchangeFloatingOffer,
  ChangellyFixedOffer,ChangellyFloatingOffer,
 },asIOffersTableTouchscreen:{classes:{BestOffer}}}=this
 // order
 const fixedMap=new Map([
  [ChangeNowFixedOffer,changeNowFixedOffer],
  [LetsExchangeFixedOffer,letsExchangeFixedOffer],
  [ChangellyFixedOffer,changellyFixedOffer],
 ])
 const floatingMap=new Map([
  [ChangeNowFloatingOffer,changeNowFloatingOffer],
  [LetsExchangeFloatingOffer,letsExchangeFloatingOffer],
  [ChangellyFloatingOffer,changellyFloatingOffer],
 ])
 let m

 if(onlyFixedRate) m=fixedMap
 else if(onlyFloatingRate) m=floatingMap
 else m=new Map([...fixedMap.entries(),...floatingMap.entries()])

 const sorted=[...m.entries()].sort(([,value],[,valueB])=>{
  if(!value&&!valueB) return 0
  if(!value) return -1
  if(!valueB) return 1
  return valueB-value
 })
 let i=0
 for(const[item,val]of sorted) {
  if(!val) continue
  const newIndex=i+1
  element.insertBefore(item,element.children[newIndex])
  i++
 }
 i=0
 for(const child of element.children) {
  if(i==1) child.classList.add(BestOffer)
  else child.classList.remove(BestOffer)
  i++
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLXRhYmxlL29mZmVycy10YWJsZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBMkxHLFNBQVMsZUFBZSxFQUFFLGFBQWEsQ0FBQyxnQkFBZ0IsR0FBRztDQUMxRCxtQkFBbUIsQ0FBQyx5QkFBeUIsQ0FBQyxzQkFBc0I7Q0FDcEUsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsc0JBQXNCO0FBQ2xFLEVBQUU7Q0FDRCxHQUFHLE1BQU0sU0FBUyxFQUFFLEtBQUssbUJBQW1CLENBQUMsQ0FBQztDQUM5QyxNQUFNO0VBQ0wsZUFBZTtFQUNmLG1CQUFtQixDQUFDLHNCQUFzQjtFQUMxQyxzQkFBc0IsQ0FBQyx5QkFBeUI7RUFDaEQsbUJBQW1CLENBQUMsc0JBQXNCO0FBQzVDLEdBQUcsMkJBQTJCLFNBQVMsU0FBUyxJQUFJO0NBQ25ELEdBQUc7Q0FDSCxNQUFNLFFBQVEsQ0FBQyxJQUFJLEdBQUcsQ0FBQztFQUN0QixvQkFBb0IsQ0FBQyxvQkFBb0I7RUFDekMsdUJBQXVCLENBQUMsdUJBQXVCO0VBQy9DLG9CQUFvQixDQUFDLG9CQUFvQjtDQUMxQztDQUNBLE1BQU0sV0FBVyxDQUFDLElBQUksR0FBRyxDQUFDO0VBQ3pCLHVCQUF1QixDQUFDLHVCQUF1QjtFQUMvQywwQkFBMEIsQ0FBQywwQkFBMEI7RUFDckQsdUJBQXVCLENBQUMsdUJBQXVCO0NBQ2hEO0NBQ0EsSUFBSTs7Q0FFSixFQUFFLENBQUMsZUFBZSxDQUFDLENBQUM7Q0FDcEIsS0FBSyxFQUFFLENBQUMsa0JBQWtCLENBQUMsQ0FBQztDQUM1QixLQUFLLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQzs7Q0FFN0QsTUFBTSxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0VBQzlDLEVBQUUsQ0FBQyxpQkFBaUIsT0FBTztFQUMzQixFQUFFLENBQUMsUUFBUSxPQUFPO0VBQ2xCLEVBQUUsQ0FBQyxTQUFTLE9BQU87RUFDbkIsT0FBTztBQUNULEVBQUU7Q0FDRCxJQUFJLENBQUMsQ0FBQztDQUNOLEdBQUcsQ0FBQyxVQUFVLENBQUMsT0FBTztFQUNyQixFQUFFLENBQUMsTUFBTTtFQUNULE1BQU0sUUFBUSxDQUFDO0VBQ2YsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO0VBQ2xDO0FBQ0Y7Q0FDQyxDQUFDLENBQUM7Q0FDRixHQUFHLENBQUMsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDO0VBQzFCLEVBQUUsQ0FBQyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztFQUM3QixLQUFLLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO0VBQzVCO0FBQ0Y7QUFDQSxDQUFGIn0=