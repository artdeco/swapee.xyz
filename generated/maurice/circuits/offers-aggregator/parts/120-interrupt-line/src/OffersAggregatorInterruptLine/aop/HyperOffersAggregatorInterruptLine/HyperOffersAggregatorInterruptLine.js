import AbstractHyperOffersAggregatorInterruptLine from '../../../../gen/AbstractOffersAggregatorInterruptLine/hyper/AbstractHyperOffersAggregatorInterruptLine'
import OffersAggregatorInterruptLine from '../../OffersAggregatorInterruptLine'
import OffersAggregatorInterruptLineGeneralAspects from '../OffersAggregatorInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperOffersAggregatorInterruptLine} */
export default class extends AbstractHyperOffersAggregatorInterruptLine
 .consults(
  OffersAggregatorInterruptLineGeneralAspects,
 )
 .implements(
  OffersAggregatorInterruptLine,
 )
{}