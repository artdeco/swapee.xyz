/** @extends {xyz.swapee.wc.AbstractOffersAggregator} */
export default class AbstractOffersAggregator extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractOffersAggregatorComputer} */
export class AbstractOffersAggregatorComputer extends (<computer>
   <adapter name="adaptMinAmount" min>
    <xyz.swapee.wc.IOffersAggregatorCore changellyFloatMin  />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore minAmount />
    </outputs>
   </adapter>
   <adapter name="adaptMinError">
    <xyz.swapee.wc.IOffersAggregatorCore minAmount estimatedOut />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore minError />
    </outputs>
   </adapter>
   <adapter name="adaptMaxAmount" max>
    <xyz.swapee.wc.IOffersAggregatorCore changellyFloatMax />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore maxAmount />
    </outputs>
   </adapter>
   <adapter name="adaptMaxError">
    <xyz.swapee.wc.IOffersAggregatorCore maxAmount estimatedOut />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore maxError />
    </outputs>
   </adapter>

   <adapter name="adaptEstimatedOut">
    <xyz.swapee.wc.IOffersAggregatorCore bestOffer />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore estimatedOut />
    </outputs>
   </adapter>
   <adapter name="adaptLoadingEstimate">
    <xyz.swapee.wc.IOffersAggregatorCore isAggregating estimatedOut  />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore loadingEstimate />
    </outputs>
   </adapter>

   <adapter name="adaptBestAndWorstOffers">
    <xyz.swapee.wc.IOffersAggregatorCore
     changellyFixedOffer changellyFloatingOffer
     letsExchangeFixedOffer letsExchangeFloatingOffer
     changeNowFixedOffer changeNowFloatingOffer
    />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore bestOffer worstOffer />
    </outputs>
   </adapter>

   <adapter name="adaptExchangesLoaded">
    <xyz.swapee.wc.IOffersAggregatorCore exchangesTotal="required"
     loadingChangellyFixedOffer loadingChangellyFloatingOffer
     loadingLetsExchangeFixedOffer loadingLetsExchangeFloatingOffer
     loadingChangeNowFixedOffer loadingChangeNowFloatingOffer
    />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore exchangesLoaded allExchangesLoaded />
    </outputs>
   </adapter>

   <adapter name="adaptIsAggregating">
    <xyz.swapee.wc.IOffersAggregatorCore
     loadingChangellyFloatingOffer loadingChangellyFixedOffer
     loadingLetsExchangeFloatingOffer loadingLetsExchangeFixedOffer
     loadingChangeNowFixedOffer loadingChangeNowFloatingOffer
    />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore isAggregating />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersAggregatorController} */
export class AbstractOffersAggregatorController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOffersAggregatorPort} */
export class OffersAggregatorPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOffersAggregatorView} */
export class AbstractOffersAggregatorView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersAggregatorElement} */
export class AbstractOffersAggregatorElement extends (<element v3 html mv>
 <block src="./OffersAggregator.mvc/src/OffersAggregatorElement/methods/render.jsx" />
 <inducer src="./OffersAggregator.mvc/src/OffersAggregatorElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent} */
export class AbstractOffersAggregatorHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
  </connectors>

</html-ic>) { }
// </class-end>