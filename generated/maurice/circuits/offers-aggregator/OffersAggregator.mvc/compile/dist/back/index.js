/**
 * An abstract class of `xyz.swapee.wc.IOffersAggregator` interface.
 * @extends {xyz.swapee.wc.AbstractOffersAggregator}
 */
class AbstractOffersAggregator extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOffersAggregator_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OffersAggregatorPort}
 */
class OffersAggregatorPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersAggregatorController` interface.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorController}
 */
class AbstractOffersAggregatorController extends (class {/* lazy-loaded */}) {}
/**
 * The _IOffersAggregator_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
class OffersAggregatorHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OffersAggregatorBuffer}
 */
class OffersAggregatorBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersAggregatorComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorComputer}
 */
class AbstractOffersAggregatorComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.OffersAggregatorComputer}
 */
class OffersAggregatorComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.OffersAggregatorController}
 */
class OffersAggregatorController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOffersAggregator = AbstractOffersAggregator
module.exports.OffersAggregatorPort = OffersAggregatorPort
module.exports.AbstractOffersAggregatorController = AbstractOffersAggregatorController
module.exports.OffersAggregatorHtmlComponent = OffersAggregatorHtmlComponent
module.exports.OffersAggregatorBuffer = OffersAggregatorBuffer
module.exports.AbstractOffersAggregatorComputer = AbstractOffersAggregatorComputer
module.exports.OffersAggregatorComputer = OffersAggregatorComputer
module.exports.OffersAggregatorController = OffersAggregatorController