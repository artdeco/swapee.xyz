import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractOffersAggregator}*/
export class AbstractOffersAggregator extends Module['48900887571'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregator} */
AbstractOffersAggregator.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersAggregatorPort} */
export const OffersAggregatorPort=Module['48900887573']
/**@extends {xyz.swapee.wc.AbstractOffersAggregatorController}*/
export class AbstractOffersAggregatorController extends Module['48900887574'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorController} */
AbstractOffersAggregatorController.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent} */
export const OffersAggregatorHtmlComponent=Module['489008875710']
/** @type {typeof xyz.swapee.wc.OffersAggregatorBuffer} */
export const OffersAggregatorBuffer=Module['489008875711']
/**@extends {xyz.swapee.wc.AbstractOffersAggregatorComputer}*/
export class AbstractOffersAggregatorComputer extends Module['489008875730'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorComputer} */
AbstractOffersAggregatorComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersAggregatorComputer} */
export const OffersAggregatorComputer=Module['489008875731']
/** @type {typeof xyz.swapee.wc.back.OffersAggregatorController} */
export const OffersAggregatorController=Module['489008875761']