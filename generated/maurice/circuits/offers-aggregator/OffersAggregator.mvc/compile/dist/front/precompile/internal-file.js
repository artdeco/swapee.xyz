/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const f=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const g=f["37270038985"],h=f["372700389810"],k=f["372700389811"];function n(a,b,c,l){return f["372700389812"](a,b,c,l,!1,void 0)};
const p=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const q=p["61893096584"],u=p["61893096586"],v=p["618930965811"],w=p["618930965812"],x=p["618930965815"],y=p["618930965819"];function z(){}z.prototype={};function A(){this.i=this.h=this.g=null}class B{}class C extends n(B,489008875718,A,{s:1,Ca:2}){}
C[k]=[z,q,{constructor(){g(this,()=>{const {queries:{R:a}}=this;this.scan({j:a})})},scan:function({j:a}){const {element:b,l:{vdusPQs:{g:c,h:l}},queries:{j:r}}=this,t=x(b);let m;a?m=b.closest(a):m=document;Object.assign(this,{g:t[c],h:t[l],i:r?m.querySelector(r):void 0})}},{[h]:{g:1,h:1,i:1},initializer({g:a,h:b,i:c}){void 0!==a&&(this.g=a);void 0!==b&&(this.h=b);void 0!==c&&(this.i=c)}}];var D=class extends C.implements(){};function E(){}E.prototype={};class F{}class G extends n(F,489008875725,null,{o:1,Ba:2}){}G[k]=[E,v,{}];function H(){}H.prototype={};class I{}class J extends n(I,489008875728,null,{u:1,Ea:2}){}J[k]=[H,w,{allocator(){this.methods={}}}];const K={J:"7b5f4",G:"5b11e",I:"5617d",H:"5ca90",O:"2a73d",M:"18c0d",L:"17034",K:"88705",da:"26339",ca:"80525",fa:"a0ea0",ea:"821c9",C:"b89f1",A:"1c43c",F:"9f4fb",D:"17536",U:"8062a"};const L={...K};const M=Object.keys(K).reduce((a,b)=>{a[K[b]]=b;return a},{});const N={ba:"77233",ra:"2029f",Y:"cfde8",ja:"c7da2",qa:"74365",X:"b518b",ia:"5920c",sa:"a47e0",Z:"f0a43",ka:"89dbe",wa:"2a82e",aa:"d5468",ma:"ea54b",ua:"cd6cc",$:"d5aff",la:"e7500",pa:"26941",W:"3f76e",ha:"cf39d",oa:"2780c",V:"2b8e5",ga:"47dca",ta:"c22a3",v:"32a18",Ga:"fc2c8",za:"ae0d3",xa:"d0b0c",P:"aa93d",host:"67b3d",m:"7a98d",T:"b4c94",Aa:"2eae5",ya:"b3b72"};const O=Object.keys(N).reduce((a,b)=>{a[N[b]]=b;return a},{});function P(){}P.prototype={};class Q{}class R extends n(Q,489008875726,null,{l:1,Da:2}){}function S(){}R[k]=[P,S.prototype={inputsPQs:L,queriesPQs:{j:"13da4"},memoryQPs:M,cacheQPs:O},u,y,J,S.prototype={vdusPQs:{g:"ffdf1",h:"ffdf2",i:"ffdf3"}}];var T=class extends R.implements(G,D,{get queries(){return this.settings}},{__$id:4890088757}){};module.exports["489008875741"]=D;module.exports["489008875771"]=T;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['4890088757']=module.exports