/**
 * Display for presenting information from the _IOffersAggregator_.
 * @extends {xyz.swapee.wc.OffersAggregatorDisplay}
 */
class OffersAggregatorDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.OffersAggregatorScreen}
 */
class OffersAggregatorScreen extends (class {/* lazy-loaded */}) {}

module.exports.OffersAggregatorDisplay = OffersAggregatorDisplay
module.exports.OffersAggregatorScreen = OffersAggregatorScreen