/**
 * An abstract class of `xyz.swapee.wc.IOffersAggregator` interface.
 * @extends {xyz.swapee.wc.AbstractOffersAggregator}
 */
class AbstractOffersAggregator extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOffersAggregator_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OffersAggregatorPort}
 */
class OffersAggregatorPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersAggregatorController` interface.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorController}
 */
class AbstractOffersAggregatorController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IOffersAggregator_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.OffersAggregatorElement}
 */
class OffersAggregatorElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OffersAggregatorBuffer}
 */
class OffersAggregatorBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersAggregatorComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorComputer}
 */
class AbstractOffersAggregatorComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.OffersAggregatorController}
 */
class OffersAggregatorController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOffersAggregator = AbstractOffersAggregator
module.exports.OffersAggregatorPort = OffersAggregatorPort
module.exports.AbstractOffersAggregatorController = AbstractOffersAggregatorController
module.exports.OffersAggregatorElement = OffersAggregatorElement
module.exports.OffersAggregatorBuffer = OffersAggregatorBuffer
module.exports.AbstractOffersAggregatorComputer = AbstractOffersAggregatorComputer
module.exports.OffersAggregatorController = OffersAggregatorController

Object.defineProperties(module.exports, {
 'AbstractOffersAggregator': {get: () => require('./precompile/internal')[48900887571]},
 [48900887571]: {get: () => module.exports['AbstractOffersAggregator']},
 'OffersAggregatorPort': {get: () => require('./precompile/internal')[48900887573]},
 [48900887573]: {get: () => module.exports['OffersAggregatorPort']},
 'AbstractOffersAggregatorController': {get: () => require('./precompile/internal')[48900887574]},
 [48900887574]: {get: () => module.exports['AbstractOffersAggregatorController']},
 'OffersAggregatorElement': {get: () => require('./precompile/internal')[48900887578]},
 [48900887578]: {get: () => module.exports['OffersAggregatorElement']},
 'OffersAggregatorBuffer': {get: () => require('./precompile/internal')[489008875711]},
 [489008875711]: {get: () => module.exports['OffersAggregatorBuffer']},
 'AbstractOffersAggregatorComputer': {get: () => require('./precompile/internal')[489008875730]},
 [489008875730]: {get: () => module.exports['AbstractOffersAggregatorComputer']},
 'OffersAggregatorController': {get: () => require('./precompile/internal')[489008875761]},
 [489008875761]: {get: () => module.exports['OffersAggregatorController']},
})