import { OffersAggregatorDisplay, OffersAggregatorScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.OffersAggregatorDisplay} */
export { OffersAggregatorDisplay }
/** @lazy @api {xyz.swapee.wc.OffersAggregatorScreen} */
export { OffersAggregatorScreen }