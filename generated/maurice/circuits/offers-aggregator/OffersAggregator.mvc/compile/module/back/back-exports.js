import AbstractOffersAggregator from '../../../gen/AbstractOffersAggregator/AbstractOffersAggregator'
module.exports['4890088757'+0]=AbstractOffersAggregator
module.exports['4890088757'+1]=AbstractOffersAggregator
export {AbstractOffersAggregator}

import OffersAggregatorPort from '../../../gen/OffersAggregatorPort/OffersAggregatorPort'
module.exports['4890088757'+3]=OffersAggregatorPort
export {OffersAggregatorPort}

import AbstractOffersAggregatorController from '../../../gen/AbstractOffersAggregatorController/AbstractOffersAggregatorController'
module.exports['4890088757'+4]=AbstractOffersAggregatorController
export {AbstractOffersAggregatorController}

import OffersAggregatorHtmlComponent from '../../../src/OffersAggregatorHtmlComponent/OffersAggregatorHtmlComponent'
module.exports['4890088757'+10]=OffersAggregatorHtmlComponent
export {OffersAggregatorHtmlComponent}

import OffersAggregatorBuffer from '../../../gen/OffersAggregatorBuffer/OffersAggregatorBuffer'
module.exports['4890088757'+11]=OffersAggregatorBuffer
export {OffersAggregatorBuffer}

import AbstractOffersAggregatorComputer from '../../../gen/AbstractOffersAggregatorComputer/AbstractOffersAggregatorComputer'
module.exports['4890088757'+30]=AbstractOffersAggregatorComputer
export {AbstractOffersAggregatorComputer}

import OffersAggregatorComputer from '../../../src/OffersAggregatorHtmlComputer/OffersAggregatorComputer'
module.exports['4890088757'+31]=OffersAggregatorComputer
export {OffersAggregatorComputer}

import OffersAggregatorController from '../../../src/OffersAggregatorHtmlController/OffersAggregatorController'
module.exports['4890088757'+61]=OffersAggregatorController
export {OffersAggregatorController}