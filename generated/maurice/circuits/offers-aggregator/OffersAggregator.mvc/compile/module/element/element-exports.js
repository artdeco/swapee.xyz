import AbstractOffersAggregator from '../../../gen/AbstractOffersAggregator/AbstractOffersAggregator'
module.exports['4890088757'+0]=AbstractOffersAggregator
module.exports['4890088757'+1]=AbstractOffersAggregator
export {AbstractOffersAggregator}

import OffersAggregatorPort from '../../../gen/OffersAggregatorPort/OffersAggregatorPort'
module.exports['4890088757'+3]=OffersAggregatorPort
export {OffersAggregatorPort}

import AbstractOffersAggregatorController from '../../../gen/AbstractOffersAggregatorController/AbstractOffersAggregatorController'
module.exports['4890088757'+4]=AbstractOffersAggregatorController
export {AbstractOffersAggregatorController}

import OffersAggregatorElement from '../../../src/OffersAggregatorElement/OffersAggregatorElement'
module.exports['4890088757'+8]=OffersAggregatorElement
export {OffersAggregatorElement}

import OffersAggregatorBuffer from '../../../gen/OffersAggregatorBuffer/OffersAggregatorBuffer'
module.exports['4890088757'+11]=OffersAggregatorBuffer
export {OffersAggregatorBuffer}

import AbstractOffersAggregatorComputer from '../../../gen/AbstractOffersAggregatorComputer/AbstractOffersAggregatorComputer'
module.exports['4890088757'+30]=AbstractOffersAggregatorComputer
export {AbstractOffersAggregatorComputer}

import OffersAggregatorController from '../../../src/OffersAggregatorServerController/OffersAggregatorController'
module.exports['4890088757'+61]=OffersAggregatorController
export {OffersAggregatorController}