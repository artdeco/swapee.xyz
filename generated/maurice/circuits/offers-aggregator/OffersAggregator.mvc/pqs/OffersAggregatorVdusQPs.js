import {OffersAggregatorVdusPQs} from './OffersAggregatorVdusPQs'
export const OffersAggregatorVdusQPs=/**@type {!xyz.swapee.wc.OffersAggregatorVdusQPs}*/(Object.keys(OffersAggregatorVdusPQs)
 .reduce((a,k)=>{a[OffersAggregatorVdusPQs[k]]=k;return a},{}))