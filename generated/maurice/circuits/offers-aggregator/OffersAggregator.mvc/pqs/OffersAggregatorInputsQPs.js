import {OffersAggregatorInputsPQs} from './OffersAggregatorInputsPQs'
export const OffersAggregatorInputsQPs=/**@type {!xyz.swapee.wc.OffersAggregatorInputsQPs}*/(Object.keys(OffersAggregatorInputsPQs)
 .reduce((a,k)=>{a[OffersAggregatorInputsPQs[k]]=k;return a},{}))