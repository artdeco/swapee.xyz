import {OffersAggregatorMemoryPQs} from './OffersAggregatorMemoryPQs'
export const OffersAggregatorMemoryQPs=/**@type {!xyz.swapee.wc.OffersAggregatorMemoryQPs}*/(Object.keys(OffersAggregatorMemoryPQs)
 .reduce((a,k)=>{a[OffersAggregatorMemoryPQs[k]]=k;return a},{}))