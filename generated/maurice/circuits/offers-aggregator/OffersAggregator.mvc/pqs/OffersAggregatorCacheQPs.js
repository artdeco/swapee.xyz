import {OffersAggregatorCachePQs} from './OffersAggregatorCachePQs'
export const OffersAggregatorCacheQPs=/**@type {!xyz.swapee.wc.OffersAggregatorCacheQPs}*/(Object.keys(OffersAggregatorCachePQs)
 .reduce((a,k)=>{a[OffersAggregatorCachePQs[k]]=k;return a},{}))