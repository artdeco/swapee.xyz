import {OffersAggregatorMemoryPQs} from './OffersAggregatorMemoryPQs'
export const OffersAggregatorInputsPQs=/**@type {!xyz.swapee.wc.OffersAggregatorInputsQPs}*/({
 ...OffersAggregatorMemoryPQs,
})