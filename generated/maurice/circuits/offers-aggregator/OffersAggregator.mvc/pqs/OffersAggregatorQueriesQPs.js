import {OffersAggregatorQueriesPQs} from './OffersAggregatorQueriesPQs'
export const OffersAggregatorQueriesQPs=/**@type {!xyz.swapee.wc.OffersAggregatorQueriesQPs}*/(Object.keys(OffersAggregatorQueriesPQs)
 .reduce((a,k)=>{a[OffersAggregatorQueriesPQs[k]]=k;return a},{}))