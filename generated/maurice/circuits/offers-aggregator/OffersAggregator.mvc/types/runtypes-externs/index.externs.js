/**
 * @fileoverview
 * @externs
 */

xyz.swapee.wc.IOffersAggregatorPort={}
xyz.swapee.wc.IOffersAggregatorCore={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IOffersAggregatorController={}
xyz.swapee.wc.IOffersAggregatorRadio={}
xyz.swapee.wc.IOffersAggregatorService={}
xyz.swapee.wc.IOffersAggregatorDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IOffersAggregatorDisplay={}
xyz.swapee.wc.IOffersAggregatorController={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.wc={}
$$xyz.swapee.wc.IOffersAggregatorPort={}
$$xyz.swapee.wc.IOffersAggregatorCore={}
$$xyz.swapee.wc.IOffersAggregatorRadio={}
$$xyz.swapee.wc.IOffersAggregatorService={}
$$xyz.swapee.wc.IOffersAggregatorDisplay={}
$$xyz.swapee.wc.back={}
$$xyz.swapee.wc.back.IOffersAggregatorDisplay={}
$$xyz.swapee.wc.IOffersAggregatorController={}
$$xyz.swapee.wc.front={}
$$xyz.swapee.wc.front.IOffersAggregatorController={}
$$xyz.swapee.wc.IOffersAggregatorComputer={}
$$xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
xyz.swapee.wc.IOffersAggregatorComputer.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @interface */
xyz.swapee.wc.IOffersAggregatorComputerCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorComputer} */
xyz.swapee.wc.IOffersAggregatorComputerCaster.prototype.asIOffersAggregatorComputer
/** @type {!xyz.swapee.wc.BoundOffersAggregatorComputer} */
xyz.swapee.wc.IOffersAggregatorComputerCaster.prototype.superOffersAggregatorComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 */
xyz.swapee.wc.IOffersAggregatorComputer = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangeNowFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangeNowFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMinAmount = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMinError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMaxAmount = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMaxError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptEstimatedOut = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptLoadingEstimate = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptBestAndWorstOffers = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptExchangesLoaded = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptIsAggregating = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorMemory} mem
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.compute.Land} land
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorComputer.prototype.compute = function(mem, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.OffersAggregatorComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorComputer.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorComputer = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init */
xyz.swapee.wc.OffersAggregatorComputer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.OffersAggregatorComputer.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.AbstractOffersAggregatorComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.OffersAggregatorComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorComputer, ...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese)} */
xyz.swapee.wc.OffersAggregatorComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.RecordIOffersAggregatorComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @typedef {{ adaptChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer, adaptChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer, adaptLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer, adaptLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer, adaptChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer, adaptChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer, adaptMinAmount: xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount, adaptMinError: xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError, adaptMaxAmount: xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount, adaptMaxError: xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError, adaptEstimatedOut: xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut, adaptLoadingEstimate: xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate, adaptBestAndWorstOffers: xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers, adaptExchangesLoaded: xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded, adaptIsAggregating: xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating, compute: xyz.swapee.wc.IOffersAggregatorComputer.compute }} */
xyz.swapee.wc.RecordIOffersAggregatorComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.BoundIOffersAggregatorComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 */
xyz.swapee.wc.BoundIOffersAggregatorComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.BoundOffersAggregatorComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinAmount = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMinAmount
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinAmount} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinAmount

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinError = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMinError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinError} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinError

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxAmount = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxAmount
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxAmount} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxAmount

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxError = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxError} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxError

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptEstimatedOut = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptEstimatedOut
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptEstimatedOut} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptEstimatedOut

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLoadingEstimate = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptLoadingEstimate
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLoadingEstimate} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptLoadingEstimate

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptBestAndWorstOffers = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptBestAndWorstOffers
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptBestAndWorstOffers} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptBestAndWorstOffers

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptExchangesLoaded = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptExchangesLoaded
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptExchangesLoaded} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptExchangesLoaded

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__adaptIsAggregating = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, !xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form, xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form): (undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptIsAggregating
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__adaptIsAggregating} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptIsAggregating

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.OffersAggregatorMemory} mem
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.compute.Land} land
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorComputer.__compute = function(mem, land) {}
/** @typedef {function(xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorComputer.compute.Land): void} */
xyz.swapee.wc.IOffersAggregatorComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorComputer, xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorComputer.compute.Land): void} */
xyz.swapee.wc.IOffersAggregatorComputer._compute
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorComputer.__compute} */
xyz.swapee.wc.IOffersAggregatorComputer.__compute

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer.prototype.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError.prototype.changellyFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin.prototype.changellyFloatMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax.prototype.changellyFloatMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer.prototype.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError.prototype.changellyFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin.prototype.changellyFixedMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax.prototype.changellyFixedMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer.prototype.letsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError.prototype.letsExchangeFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer.prototype.letsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError.prototype.letsExchangeFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer.prototype.changeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError.prototype.changeNowFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer.prototype.changeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError.prototype.changeNowFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe.prototype.changellyFloatMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount.prototype.minAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe.prototype.minAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe.prototype.estimatedOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError.prototype.minError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinError}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe.prototype.changellyFloatMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount.prototype.maxAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe.prototype.maxAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError.prototype.maxError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe.prototype.bestOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut.prototype.estimatedOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe.prototype.isAggregating

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate.prototype.loadingEstimate

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe.prototype.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe.prototype.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe.prototype.letsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe.prototype.letsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe.prototype.changeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe.prototype.changeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer.prototype.bestOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer.prototype.worstOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe.prototype.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe.prototype.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe.prototype.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe.prototype.loadingLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe.prototype.loadingLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe.prototype.loadingChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe.prototype.loadingChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded.prototype.exchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded.prototype.allExchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating.prototype.isAggregating

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating}
 */
xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.compute.Land exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @record */
xyz.swapee.wc.IOffersAggregatorComputer.compute.Land = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersAggregatorComputer.compute.Land.prototype.ExchangeIntent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @interface */
xyz.swapee.wc.IOffersAggregatorOuterCoreFields
/** @type {!xyz.swapee.wc.IOffersAggregatorOuterCore.Model} */
xyz.swapee.wc.IOffersAggregatorOuterCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @interface */
xyz.swapee.wc.IOffersAggregatorOuterCoreCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorOuterCore} */
xyz.swapee.wc.IOffersAggregatorOuterCoreCaster.prototype.asIOffersAggregatorOuterCore
/** @type {!xyz.swapee.wc.BoundOffersAggregatorOuterCore} */
xyz.swapee.wc.IOffersAggregatorOuterCoreCaster.prototype.superOffersAggregatorOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreCaster}
 */
xyz.swapee.wc.IOffersAggregatorOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.OffersAggregatorOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorOuterCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.OffersAggregatorOuterCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.AbstractOffersAggregatorOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore = function() {}
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersAggregatorMemoryPQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.estimatedOut
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.bestOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.worstOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFixedError
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFloatingError
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.host
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.amountIn
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.currencyIn
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.currencyOut
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.exchangesLoaded
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.allExchangesLoaded
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersAggregatorMemoryQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.aa93d
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.d2a18
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.fc2c8
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.hb5f4
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.fb11e
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.b8c0d
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.ca73d
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.g7b3d
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.i8da7
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.f3088
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.j8dbb
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.b4c94
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.ha98d
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.i062a

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.RecordIOffersAggregatorOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.BoundIOffersAggregatorOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreCaster}
 */
xyz.swapee.wc.BoundIOffersAggregatorOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.BoundOffersAggregatorOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer.changellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError.changellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError.changellyFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin.changellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin.changellyFixedMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax.changellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax.changellyFixedMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer.changellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError.changellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError.changellyFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin.changellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin.changellyFloatMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax.changellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax.changellyFloatMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer.letsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer.letsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError.letsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError.letsExchangeFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer.letsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer.letsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError.letsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError.letsExchangeFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer.changeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer.changeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError.changeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError.changeNowFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer.changeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer.changeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError.changeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError.changeNowFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal.exchangesTotal exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal.prototype.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal}
 */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer.prototype.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError.prototype.changellyFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin.prototype.changellyFixedMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax.prototype.changellyFixedMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer.prototype.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError.prototype.changellyFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin.prototype.changellyFloatMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax.prototype.changellyFloatMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer.prototype.letsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError.prototype.letsExchangeFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer.prototype.letsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError.prototype.letsExchangeFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer.prototype.changeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError.prototype.changeNowFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer.prototype.changeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError.prototype.changeNowFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal.prototype.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal}
 */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
xyz.swapee.wc.IOffersAggregatorPort.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorPortFields
/** @type {!xyz.swapee.wc.IOffersAggregatorPort.Inputs} */
xyz.swapee.wc.IOffersAggregatorPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOffersAggregatorPort.Inputs} */
xyz.swapee.wc.IOffersAggregatorPortFields.prototype.props

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorPortCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorPort} */
xyz.swapee.wc.IOffersAggregatorPortCaster.prototype.asIOffersAggregatorPort
/** @type {!xyz.swapee.wc.BoundOffersAggregatorPort} */
xyz.swapee.wc.IOffersAggregatorPortCaster.prototype.superOffersAggregatorPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersAggregatorPort.Inputs>}
 */
xyz.swapee.wc.IOffersAggregatorPort = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init */
xyz.swapee.wc.IOffersAggregatorPort.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorPort.prototype.resetPort = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorPort.prototype.resetOffersAggregatorPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.OffersAggregatorPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorPort.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorPort = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init */
xyz.swapee.wc.OffersAggregatorPort.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.OffersAggregatorPort.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.AbstractOffersAggregatorPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorPort|typeof xyz.swapee.wc.OffersAggregatorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorPort|typeof xyz.swapee.wc.OffersAggregatorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorPort|typeof xyz.swapee.wc.OffersAggregatorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.OffersAggregatorPortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorPort, ...!xyz.swapee.wc.IOffersAggregatorPort.Initialese)} */
xyz.swapee.wc.OffersAggregatorPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.OffersAggregatorMemoryPQs}
 */
xyz.swapee.wc.OffersAggregatorInputsPQs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorMemoryPQs}
 * @dict
 */
xyz.swapee.wc.OffersAggregatorInputsQPs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.RecordIOffersAggregatorPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersAggregatorPort.resetPort, resetOffersAggregatorPort: xyz.swapee.wc.IOffersAggregatorPort.resetOffersAggregatorPort }} */
xyz.swapee.wc.RecordIOffersAggregatorPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.BoundIOffersAggregatorPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorPortFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOffersAggregatorPort.Inputs>}
 */
xyz.swapee.wc.BoundIOffersAggregatorPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.BoundOffersAggregatorPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorPort): void} */
xyz.swapee.wc.IOffersAggregatorPort._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorPort.__resetPort} */
xyz.swapee.wc.IOffersAggregatorPort.__resetPort

// nss:xyz.swapee.wc.IOffersAggregatorPort,$$xyz.swapee.wc.IOffersAggregatorPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.resetOffersAggregatorPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorPort.__resetOffersAggregatorPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorPort.resetOffersAggregatorPort
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorPort): void} */
xyz.swapee.wc.IOffersAggregatorPort._resetOffersAggregatorPort
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorPort.__resetOffersAggregatorPort} */
xyz.swapee.wc.IOffersAggregatorPort.__resetOffersAggregatorPort

// nss:xyz.swapee.wc.IOffersAggregatorPort,$$xyz.swapee.wc.IOffersAggregatorPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorPortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.OffersAggregatorPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorPortInterface}
 */
xyz.swapee.wc.OffersAggregatorPortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortInterface.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @record */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedOffer
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedError
/** @type {number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedMin
/** @type {number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedMax
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatingError
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatMin
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatMax
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFixedOffer
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFixedError
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFloatingOffer
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFloatingError
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFixedOffer
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFixedError
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFloatingOffer
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFloatingError
/** @type {number|undefined} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorCoreFields
/** @type {!xyz.swapee.wc.IOffersAggregatorCore.Model} */
xyz.swapee.wc.IOffersAggregatorCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorCoreCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorCore} */
xyz.swapee.wc.IOffersAggregatorCoreCaster.prototype.asIOffersAggregatorCore
/** @type {!xyz.swapee.wc.BoundOffersAggregatorCore} */
xyz.swapee.wc.IOffersAggregatorCoreCaster.prototype.superOffersAggregatorCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore}
 */
xyz.swapee.wc.IOffersAggregatorCore = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorCore.prototype.resetCore = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorCore.prototype.resetOffersAggregatorCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.OffersAggregatorCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorCore.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.OffersAggregatorCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.AbstractOffersAggregatorCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore = function() {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorCachePQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersAggregatorCachePQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.isAggregating
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadingChangellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.hasMoreChangellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadChangellyFloatingOfferError
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadingChangellyFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.hasMoreChangellyFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadChangellyFixedOfferError
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadingChangenowOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.hasMoreChangenowOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadChangenowOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorCacheQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersAggregatorCacheQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.h7233
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.c029f
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.cfde8
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.c7da2
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.h4365
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.b518b
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.f920c
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.a47e0
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.f0a43
/** @type {string} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.i9dbe

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.RecordIOffersAggregatorCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {{ resetCore: xyz.swapee.wc.IOffersAggregatorCore.resetCore, resetOffersAggregatorCore: xyz.swapee.wc.IOffersAggregatorCore.resetOffersAggregatorCore }} */
xyz.swapee.wc.RecordIOffersAggregatorCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.BoundIOffersAggregatorCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorOuterCore}
 */
xyz.swapee.wc.BoundIOffersAggregatorCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.BoundOffersAggregatorCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorCore): void} */
xyz.swapee.wc.IOffersAggregatorCore._resetCore
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorCore.__resetCore} */
xyz.swapee.wc.IOffersAggregatorCore.__resetCore

// nss:xyz.swapee.wc.IOffersAggregatorCore,$$xyz.swapee.wc.IOffersAggregatorCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.resetOffersAggregatorCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorCore.__resetOffersAggregatorCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorCore.resetOffersAggregatorCore
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorCore): void} */
xyz.swapee.wc.IOffersAggregatorCore._resetOffersAggregatorCore
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorCore.__resetOffersAggregatorCore} */
xyz.swapee.wc.IOffersAggregatorCore.__resetOffersAggregatorCore

// nss:xyz.swapee.wc.IOffersAggregatorCore,$$xyz.swapee.wc.IOffersAggregatorCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer.bestOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer.bestOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer.worstOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer.worstOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount.minAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount.minAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinError.minError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError.minError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount.maxAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount.maxAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError.maxError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError.maxError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut.estimatedOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut.estimatedOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate.loadingEstimate exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate.loadingEstimate

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded.allExchangesLoaded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded.allExchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded.exchangesLoaded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded.exchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.Host.host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating.isAggregating exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating.isAggregating

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer.loadingLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer.loadingLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer.hasMoreLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer.hasMoreLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError.loadLetsExchangeFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError.loadLetsExchangeFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer.loadingLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer.loadingLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer.hasMoreLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer.hasMoreLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError.loadLetsExchangeFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError.loadLetsExchangeFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer.loadingChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer.loadingChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer.hasMoreChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer.hasMoreChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError.loadChangeNowFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError.loadChangeNowFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer.loadingChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer.loadingChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer.hasMoreChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer.hasMoreChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError.loadChangeNowFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError.loadChangeNowFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer.prototype.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer.prototype.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError.prototype.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer.prototype.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer.prototype.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError.prototype.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer.prototype.loadingLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer.prototype.hasMoreLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError.prototype.loadLetsExchangeFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer.prototype.loadingLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer.prototype.hasMoreLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError.prototype.loadLetsExchangeFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer.prototype.loadingChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer.prototype.hasMoreChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError.prototype.loadChangeNowFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer.prototype.loadingChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer.prototype.hasMoreChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError.prototype.loadChangeNowFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.Host}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel>}
 */
xyz.swapee.wc.IOffersAggregatorController.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.IOffersAggregatorProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
xyz.swapee.wc.IOffersAggregatorProcessor.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.IOffersAggregatorProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorProcessorCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorProcessor} */
xyz.swapee.wc.IOffersAggregatorProcessorCaster.prototype.asIOffersAggregatorProcessor
/** @type {!xyz.swapee.wc.BoundOffersAggregatorProcessor} */
xyz.swapee.wc.IOffersAggregatorProcessorCaster.prototype.superOffersAggregatorProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @interface */
xyz.swapee.wc.IOffersAggregatorControllerFields
/** @type {!xyz.swapee.wc.IOffersAggregatorController.Inputs} */
xyz.swapee.wc.IOffersAggregatorControllerFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @interface */
xyz.swapee.wc.IOffersAggregatorControllerCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorController} */
xyz.swapee.wc.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorController
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorProcessor} */
xyz.swapee.wc.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorProcessor
/** @type {!xyz.swapee.wc.BoundOffersAggregatorController} */
xyz.swapee.wc.IOffersAggregatorControllerCaster.prototype.superOffersAggregatorController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
xyz.swapee.wc.IOffersAggregatorController = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init */
xyz.swapee.wc.IOffersAggregatorController.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.resetPort = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedError = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedMin = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedMax = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatingOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatingError = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatMin = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatMax = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFixedOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFixedError = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFloatingOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFloatingError = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFixedOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFixedError = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFloatingOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFloatingError = function(val) {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFloatingError = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.loadLetsExchangeFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.loadLetsExchangeFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangeNowFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangeNowFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.IOffersAggregatorProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessorCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 */
xyz.swapee.wc.IOffersAggregatorProcessor = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init */
xyz.swapee.wc.IOffersAggregatorProcessor.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.OffersAggregatorProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorProcessor = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init */
xyz.swapee.wc.OffersAggregatorProcessor.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.OffersAggregatorProcessor.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.AbstractOffersAggregatorProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.OffersAggregatorProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorProcessor, ...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese)} */
xyz.swapee.wc.OffersAggregatorProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.RecordIOffersAggregatorProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.RecordIOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersAggregatorController.resetPort, setChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedOffer, unsetChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedOffer, setChangellyFixedError: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedError, unsetChangellyFixedError: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedError, setChangellyFixedMin: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMin, unsetChangellyFixedMin: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMin, setChangellyFixedMax: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMax, unsetChangellyFixedMax: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMax, setChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingOffer, unsetChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingOffer, setChangellyFloatingError: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingError, unsetChangellyFloatingError: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingError, setChangellyFloatMin: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMin, unsetChangellyFloatMin: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMin, setChangellyFloatMax: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMax, unsetChangellyFloatMax: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMax, setLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedOffer, unsetLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedOffer, setLetsExchangeFixedError: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedError, unsetLetsExchangeFixedError: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedError, setLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingOffer, unsetLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingOffer, setLetsExchangeFloatingError: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingError, unsetLetsExchangeFloatingError: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingError, setChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedOffer, unsetChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedOffer, setChangeNowFixedError: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedError, unsetChangeNowFixedError: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedError, setChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingOffer, unsetChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingOffer, setChangeNowFloatingError: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingError, unsetChangeNowFloatingError: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingError, loadChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangellyFixedOffer, loadLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFloatingOffer, loadLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFixedOffer, loadChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFloatingOffer, loadChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFixedOffer }} */
xyz.swapee.wc.RecordIOffersAggregatorController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.BoundIOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
xyz.swapee.wc.BoundIOffersAggregatorController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.BoundIOffersAggregatorProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorCore}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 */
xyz.swapee.wc.BoundIOffersAggregatorProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.BoundOffersAggregatorProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/100-OffersAggregatorMemory.xml} xyz.swapee.wc.OffersAggregatorMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 736b81bf56de2801d786633bdfcc2dca */
/** @record */
xyz.swapee.wc.OffersAggregatorMemory = function() {}
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedError
/** @type {number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedMin
/** @type {number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedMax
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatingError
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatMin
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatMax
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFixedError
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFloatingError
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFixedOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFixedError
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFloatingOffer
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFloatingError
/** @type {number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.exchangesTotal
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.bestOffer
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.worstOffer
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.minAmount
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.minError
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.maxAmount
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.maxError
/** @type {?number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.estimatedOut
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingEstimate
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.allExchangesLoaded
/** @type {number} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.exchangesLoaded
/** @type {string} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.host
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.isAggregating
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangellyFloatingOffer
/** @type {?boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangellyFloatingOffer
/** @type {?Error} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangellyFloatingOfferError
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangellyFixedOffer
/** @type {?boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangellyFixedOffer
/** @type {?Error} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangellyFixedOfferError
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingLetsExchangeFloatingOffer
/** @type {?boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreLetsExchangeFloatingOffer
/** @type {?Error} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadLetsExchangeFloatingOfferError
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingLetsExchangeFixedOffer
/** @type {?boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreLetsExchangeFixedOffer
/** @type {?Error} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadLetsExchangeFixedOfferError
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangeNowFloatingOffer
/** @type {?boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangeNowFloatingOffer
/** @type {?Error} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangeNowFloatingOfferError
/** @type {boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangeNowFixedOffer
/** @type {?boolean} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangeNowFixedOffer
/** @type {?Error} */
xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangeNowFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/102-OffersAggregatorInputs.xml} xyz.swapee.wc.front.OffersAggregatorInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9b6bba0df368424f1ffc16bc6c5a935f */
/** @record */
xyz.swapee.wc.front.OffersAggregatorInputs = function() {}
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedOffer
/** @type {string|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedError
/** @type {number|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedMin
/** @type {number|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedMax
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatingOffer
/** @type {string|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatingError
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatMin
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatMax
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFixedOffer
/** @type {string|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFixedError
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFloatingOffer
/** @type {string|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFloatingError
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFixedOffer
/** @type {string|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFixedError
/** @type {(?number)|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFloatingOffer
/** @type {string|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFloatingError
/** @type {number|undefined} */
xyz.swapee.wc.front.OffersAggregatorInputs.prototype.exchangesTotal

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregatorEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @record */
xyz.swapee.wc.OffersAggregatorEnv = function() {}
/** @type {xyz.swapee.wc.IOffersAggregator} */
xyz.swapee.wc.OffersAggregatorEnv.prototype.offersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
xyz.swapee.wc.IOffersAggregator.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregatorFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorFields
/** @type {!xyz.swapee.wc.IOffersAggregator.Pinout} */
xyz.swapee.wc.IOffersAggregatorFields.prototype.pinout

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregatorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregator} */
xyz.swapee.wc.IOffersAggregatorCaster.prototype.asIOffersAggregator
/** @type {!xyz.swapee.wc.BoundOffersAggregator} */
xyz.swapee.wc.IOffersAggregatorCaster.prototype.superOffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorLand>}
 */
xyz.swapee.wc.IOffersAggregator = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init */
xyz.swapee.wc.IOffersAggregator.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregator exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregator}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregator.Initialese>}
 */
xyz.swapee.wc.OffersAggregator = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init */
xyz.swapee.wc.OffersAggregator.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.OffersAggregator.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.AbstractOffersAggregator exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregator.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregatorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregator, ...!xyz.swapee.wc.IOffersAggregator.Initialese)} */
xyz.swapee.wc.OffersAggregatorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @record */
xyz.swapee.wc.IOffersAggregator.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IOffersAggregator.Pinout)|undefined} */
xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IOffersAggregator.Pinout)|undefined} */
xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IOffersAggregator.Pinout} */
xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.OffersAggregatorMemory)|undefined} */
xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.OffersAggregatorClasses)|undefined} */
xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.classes

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.RecordIOffersAggregator exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.BoundIOffersAggregator exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregator}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorLand>}
 */
xyz.swapee.wc.BoundIOffersAggregator = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.BoundOffersAggregator exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregator}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregator = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorPort.Inputs}
 */
xyz.swapee.wc.IOffersAggregatorController.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Inputs}
 */
xyz.swapee.wc.IOffersAggregator.Pinout = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregatorBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
xyz.swapee.wc.IOffersAggregatorBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregatorBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorBuffer}
 */
xyz.swapee.wc.OffersAggregatorBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese}
 */
xyz.swapee.wc.IOffersAggregatorGPU.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorController.Initialese}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregator.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer.Initialese}
 */
xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster.prototype.asIOffersAggregatorHtmlComponent
/** @type {!xyz.swapee.wc.BoundOffersAggregatorHtmlComponent} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster.prototype.superOffersAggregatorHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IOffersAggregatorGPUFields
/** @type {!Object<string, string>} */
xyz.swapee.wc.IOffersAggregatorGPUFields.prototype.vdusPQs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IOffersAggregatorGPUCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorGPU} */
xyz.swapee.wc.IOffersAggregatorGPUCaster.prototype.asIOffersAggregatorGPU
/** @type {!xyz.swapee.wc.BoundOffersAggregatorGPU} */
xyz.swapee.wc.IOffersAggregatorGPUCaster.prototype.superOffersAggregatorGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!OffersAggregatorMemory,>}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplay}
 */
xyz.swapee.wc.IOffersAggregatorGPU = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init */
xyz.swapee.wc.IOffersAggregatorGPU.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorController}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreen}
 * @extends {xyz.swapee.wc.IOffersAggregator}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer}
 */
xyz.swapee.wc.IOffersAggregatorHtmlComponent = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init */
xyz.swapee.wc.IOffersAggregatorHtmlComponent.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.OffersAggregatorHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorHtmlComponent = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init */
xyz.swapee.wc.OffersAggregatorHtmlComponent.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.OffersAggregatorHtmlComponent.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorHtmlComponent|typeof xyz.swapee.wc.OffersAggregatorHtmlComponent)|(!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorHtmlComponent|typeof xyz.swapee.wc.OffersAggregatorHtmlComponent)|(!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorHtmlComponent|typeof xyz.swapee.wc.OffersAggregatorHtmlComponent)|(!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.OffersAggregatorHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorHtmlComponent, ...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese)} */
xyz.swapee.wc.OffersAggregatorHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentUtilFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @interface */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtilFields
/** @type {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtilFields.prototype.RouterNet
/** @type {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtilFields.prototype.RouterPorts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorHtmlComponentUtilFields}
 */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts} [ports]
 * @return {?}
 */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.prototype.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.OffersAggregatorHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil}
 */
xyz.swapee.wc.OffersAggregatorHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.RecordIOffersAggregatorHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @typedef {{ router: xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.router }} */
xyz.swapee.wc.RecordIOffersAggregatorHtmlComponentUtil

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.BoundIOffersAggregatorHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorHtmlComponentUtilFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorHtmlComponentUtil}
 */
xyz.swapee.wc.BoundIOffersAggregatorHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.BoundOffersAggregatorHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorHtmlComponentUtil}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.router exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts} [ports]
 */
$$xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.__router = function(net, cores, ports) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.router
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil, !xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil._router
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.__router} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.__router

// nss:xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil,$$xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet = function() {}
/** @type {typeof xyz.swapee.wc.IExchangeIntentPort} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet.prototype.ExchangeIntent
/** @type {typeof xyz.swapee.wc.IOffersAggregatorPort} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterNet.prototype.OffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores = function() {}
/** @type {!xyz.swapee.wc.ExchangeIntentMemory} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.OffersAggregatorMemory} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterCores.prototype.OffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.element.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts = function() {}
/** @type {!xyz.swapee.wc.IExchangeIntent.Pinout} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.IOffersAggregator.Pinout} */
xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil.RouterPorts.prototype.OffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.RecordIOffersAggregatorHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.RecordIOffersAggregatorGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.BoundIOffersAggregatorGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!OffersAggregatorMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorDisplay}
 */
xyz.swapee.wc.BoundIOffersAggregatorGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorController}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreen}
 * @extends {xyz.swapee.wc.BoundIOffersAggregator}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 */
xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.BoundOffersAggregatorHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/** @record */
xyz.swapee.wc.IOffersAggregatorRadio.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadioCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorRadioCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorRadio} */
xyz.swapee.wc.IOffersAggregatorRadioCaster.prototype.asIOffersAggregatorRadio
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorComputer} */
xyz.swapee.wc.IOffersAggregatorRadioCaster.prototype.asIOffersAggregatorComputer
/** @type {!xyz.swapee.wc.BoundOffersAggregatorRadio} */
xyz.swapee.wc.IOffersAggregatorRadioCaster.prototype.superOffersAggregatorRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorRadioCaster}
 */
xyz.swapee.wc.IOffersAggregatorRadio = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangeNowFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangeNowFixedOffer = function(form, changes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.OffersAggregatorRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorRadio}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorRadio.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorRadio = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.OffersAggregatorRadio.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.AbstractOffersAggregatorRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio = function() {}
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorRadio|typeof xyz.swapee.wc.OffersAggregatorRadio)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorRadio|typeof xyz.swapee.wc.OffersAggregatorRadio)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorRadio|typeof xyz.swapee.wc.OffersAggregatorRadio)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.RecordIOffersAggregatorRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/** @typedef {{ adaptLoadChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer, adaptLoadChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer, adaptLoadLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer, adaptLoadLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer, adaptLoadChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer, adaptLoadChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer }} */
xyz.swapee.wc.RecordIOffersAggregatorRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.BoundIOffersAggregatorRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorRadio}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorRadioCaster}
 */
xyz.swapee.wc.BoundIOffersAggregatorRadio = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.BoundOffersAggregatorRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorRadio}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorRadio = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form, IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorRadio, !xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form, IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form, IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorRadio, !xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form, IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form, IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorRadio, !xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form, IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form, IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorRadio, !xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form, IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form, IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorRadio, !xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form, IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form, IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorRadio, !xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form, IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError}
 */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorDesigner = function() {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IOffersAggregatorDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IOffersAggregatorDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh} mesh
 * @return {?}
 */
xyz.swapee.wc.IOffersAggregatorDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool} memPool
 * @return {?}
 */
xyz.swapee.wc.IOffersAggregatorDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IOffersAggregatorDesigner.prototype.lendClasses = function(classes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.OffersAggregatorDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorDesigner}
 */
xyz.swapee.wc.OffersAggregatorDesigner = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @record */
xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh.prototype.ExchangeIntent
/** @type {typeof xyz.swapee.wc.IOffersAggregatorController} */
xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh.prototype.OffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @record */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh.prototype.ExchangeIntent
/** @type {typeof xyz.swapee.wc.IOffersAggregatorController} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh.prototype.OffersAggregator
/** @type {typeof xyz.swapee.wc.IOffersAggregatorController} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @record */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool = function() {}
/** @type {!Object} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.OffersAggregatorMemory} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool.prototype.OffersAggregator
/** @type {!xyz.swapee.wc.OffersAggregatorMemory} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 * @extends {io.letsexchange.ULetsExchange.Initialese}
 * @extends {io.changenow.UChangeNow.Initialese}
 */
xyz.swapee.wc.IOffersAggregatorService.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorServiceCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorServiceCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorService} */
xyz.swapee.wc.IOffersAggregatorServiceCaster.prototype.asIOffersAggregatorService
/** @type {!xyz.swapee.wc.BoundOffersAggregatorService} */
xyz.swapee.wc.IOffersAggregatorServiceCaster.prototype.superOffersAggregatorService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorServiceCaster}
 * @extends {com.changelly.UChangelly}
 * @extends {io.letsexchange.ULetsExchange}
 * @extends {io.changenow.UChangeNow}
 */
xyz.swapee.wc.IOffersAggregatorService = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>}
 */
xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangellyFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>}
 */
xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangellyFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>}
 */
xyz.swapee.wc.IOffersAggregatorService.prototype.filterLetsExchangeFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>}
 */
xyz.swapee.wc.IOffersAggregatorService.prototype.filterLetsExchangeFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>}
 */
xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangeNowFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>}
 */
xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangeNowFixedOffer = function(form) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.OffersAggregatorService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorService}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorService.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorService = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.OffersAggregatorService.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.AbstractOffersAggregatorService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService = function() {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorService|typeof xyz.swapee.wc.OffersAggregatorService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorService.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorService|typeof xyz.swapee.wc.OffersAggregatorService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorService|typeof xyz.swapee.wc.OffersAggregatorService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.RecordIOffersAggregatorService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/** @typedef {{ filterChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer, filterChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer, filterLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer, filterLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer, filterChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer, filterChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer }} */
xyz.swapee.wc.RecordIOffersAggregatorService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.BoundIOffersAggregatorService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorService}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorServiceCaster}
 * @extends {com.changelly.BoundUChangelly}
 * @extends {io.letsexchange.BoundULetsExchange}
 * @extends {io.changenow.BoundUChangeNow}
 */
xyz.swapee.wc.BoundIOffersAggregatorService = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.BoundOffersAggregatorService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorService}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorService = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>}
 */
$$xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFloatingOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorService, !xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService._filterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>}
 */
$$xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFixedOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorService, !xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService._filterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>}
 */
$$xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFloatingOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorService, !xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>}
 */
$$xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFixedOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorService, !xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>}
 */
$$xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFloatingOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorService, !xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>}
 */
$$xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFixedOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorService, !xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form): !Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>} */
xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError}
 */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError}
 */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/200-OffersAggregatorLand.xml} xyz.swapee.wc.OffersAggregatorLand exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c910d9d32396f06ace6a06ff3282dfba */
/** @record */
xyz.swapee.wc.OffersAggregatorLand = function() {}
/** @type {!Object} */
xyz.swapee.wc.OffersAggregatorLand.prototype.ExchangeIntent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings>}
 */
xyz.swapee.wc.IOffersAggregatorDisplay.Initialese = function() {}
/** @type {HTMLInputElement|undefined} */
xyz.swapee.wc.IOffersAggregatorDisplay.Initialese.prototype.AmountInIn
/** @type {HTMLInputElement|undefined} */
xyz.swapee.wc.IOffersAggregatorDisplay.Initialese.prototype.AmountOutIn
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IOffersAggregatorDisplay.Initialese.prototype.ExchangeIntent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @interface */
xyz.swapee.wc.IOffersAggregatorDisplayFields
/** @type {!xyz.swapee.wc.IOffersAggregatorDisplay.Settings} */
xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IOffersAggregatorDisplay.Queries} */
xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.queries
/** @type {HTMLInputElement} */
xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.AmountInIn
/** @type {HTMLInputElement} */
xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.AmountOutIn
/** @type {HTMLElement} */
xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.ExchangeIntent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @interface */
xyz.swapee.wc.IOffersAggregatorDisplayCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorDisplay} */
xyz.swapee.wc.IOffersAggregatorDisplayCaster.prototype.asIOffersAggregatorDisplay
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorScreen} */
xyz.swapee.wc.IOffersAggregatorDisplayCaster.prototype.asIOffersAggregatorScreen
/** @type {!xyz.swapee.wc.BoundOffersAggregatorDisplay} */
xyz.swapee.wc.IOffersAggregatorDisplayCaster.prototype.superOffersAggregatorDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.OffersAggregatorMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null>}
 */
xyz.swapee.wc.IOffersAggregatorDisplay = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init */
xyz.swapee.wc.IOffersAggregatorDisplay.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {null} land
 * @return {void}
 */
xyz.swapee.wc.IOffersAggregatorDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.OffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorDisplay = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init */
xyz.swapee.wc.OffersAggregatorDisplay.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.OffersAggregatorDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.AbstractOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.OffersAggregatorDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorDisplay, ...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese)} */
xyz.swapee.wc.OffersAggregatorDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.OffersAggregatorGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorGPU.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorGPU = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init */
xyz.swapee.wc.OffersAggregatorGPU.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.OffersAggregatorGPU.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.AbstractOffersAggregatorGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.OffersAggregatorGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorGPU, ...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese)} */
xyz.swapee.wc.OffersAggregatorGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.BoundOffersAggregatorGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.RecordIOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @typedef {{ paint: xyz.swapee.wc.IOffersAggregatorDisplay.paint }} */
xyz.swapee.wc.RecordIOffersAggregatorDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.BoundIOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.OffersAggregatorMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null>}
 */
xyz.swapee.wc.BoundIOffersAggregatorDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.BoundOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {null} land
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersAggregatorMemory, null): void} */
xyz.swapee.wc.IOffersAggregatorDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorDisplay, !xyz.swapee.wc.OffersAggregatorMemory, null): void} */
xyz.swapee.wc.IOffersAggregatorDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorDisplay.__paint} */
xyz.swapee.wc.IOffersAggregatorDisplay.__paint

// nss:xyz.swapee.wc.IOffersAggregatorDisplay,$$xyz.swapee.wc.IOffersAggregatorDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @record */
xyz.swapee.wc.IOffersAggregatorDisplay.Queries = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IOffersAggregatorDisplay.Queries.prototype.exchangeIntentSel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplay.Queries}
 */
xyz.swapee.wc.IOffersAggregatorDisplay.Settings = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorQueriesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersAggregatorQueriesPQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorQueriesPQs.prototype.exchangeIntentSel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorQueriesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersAggregatorQueriesQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorQueriesQPs.prototype.b3da4

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OffersAggregatorClasses>}
 */
xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese.prototype.AmountInIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese.prototype.AmountOutIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese.prototype.ExchangeIntent

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/** @interface */
xyz.swapee.wc.back.IOffersAggregatorDisplayFields
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersAggregatorDisplayFields.prototype.AmountInIn
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersAggregatorDisplayFields.prototype.AmountOutIn
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IOffersAggregatorDisplayFields.prototype.ExchangeIntent

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/** @interface */
xyz.swapee.wc.back.IOffersAggregatorDisplayCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorDisplay} */
xyz.swapee.wc.back.IOffersAggregatorDisplayCaster.prototype.asIOffersAggregatorDisplay
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorDisplay} */
xyz.swapee.wc.back.IOffersAggregatorDisplayCaster.prototype.superOffersAggregatorDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.OffersAggregatorClasses, !xyz.swapee.wc.OffersAggregatorLand>}
 */
xyz.swapee.wc.back.IOffersAggregatorDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [memory]
 * @param {!xyz.swapee.wc.OffersAggregatorLand} [land]
 * @return {void}
 */
xyz.swapee.wc.back.IOffersAggregatorDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.OffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IOffersAggregatorDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese>}
 */
xyz.swapee.wc.back.OffersAggregatorDisplay = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.OffersAggregatorDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.AbstractOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay = function() {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.OffersAggregatorVdusPQs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorVdusPQs.prototype.AmountInIn
/** @type {string} */
xyz.swapee.wc.OffersAggregatorVdusPQs.prototype.AmountOutIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.OffersAggregatorVdusQPs = function() {}
/** @type {string} */
xyz.swapee.wc.OffersAggregatorVdusQPs.prototype.ffdf1
/** @type {string} */
xyz.swapee.wc.OffersAggregatorVdusQPs.prototype.ffdf2

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.RecordIOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/** @typedef {{ paint: xyz.swapee.wc.back.IOffersAggregatorDisplay.paint }} */
xyz.swapee.wc.back.RecordIOffersAggregatorDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.BoundIOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.OffersAggregatorClasses, !xyz.swapee.wc.OffersAggregatorLand>}
 */
xyz.swapee.wc.back.BoundIOffersAggregatorDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.BoundOffersAggregatorDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersAggregatorDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [memory]
 * @param {!xyz.swapee.wc.OffersAggregatorLand} [land]
 * @return {void}
 */
$$xyz.swapee.wc.back.IOffersAggregatorDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersAggregatorMemory=, !xyz.swapee.wc.OffersAggregatorLand=): void} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IOffersAggregatorDisplay, !xyz.swapee.wc.OffersAggregatorMemory=, !xyz.swapee.wc.OffersAggregatorLand=): void} */
xyz.swapee.wc.back.IOffersAggregatorDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.back.IOffersAggregatorDisplay.__paint} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.__paint

// nss:xyz.swapee.wc.back.IOffersAggregatorDisplay,$$xyz.swapee.wc.back.IOffersAggregatorDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/41-OffersAggregatorClasses.xml} xyz.swapee.wc.OffersAggregatorClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3dd0769c9740adac8fdb7b7d422db5e3 */
/** @record */
xyz.swapee.wc.OffersAggregatorClasses = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.OffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorController.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorController = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init */
xyz.swapee.wc.OffersAggregatorController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.OffersAggregatorController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.AbstractOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.OffersAggregatorControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorController, ...!xyz.swapee.wc.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.OffersAggregatorControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.BoundOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__resetPort} */
xyz.swapee.wc.IOffersAggregatorController.__resetPort

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedMin
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedMin
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedMax
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedMax
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatingError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatingError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatMin
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatMin
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatMax
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatMax
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFixedError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFixedError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFloatingError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFloatingError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFixedError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFixedError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFloatingError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFloatingError
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorPort.WeakInputs}
 */
xyz.swapee.wc.IOffersAggregatorController.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @record */
xyz.swapee.wc.front.IOffersAggregatorController.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @interface */
xyz.swapee.wc.front.IOffersAggregatorControllerCaster
/** @type {!xyz.swapee.wc.front.BoundIOffersAggregatorController} */
xyz.swapee.wc.front.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorController
/** @type {!xyz.swapee.wc.front.BoundOffersAggregatorController} */
xyz.swapee.wc.front.IOffersAggregatorControllerCaster.prototype.superOffersAggregatorController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.IOffersAggregatorControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/** @interface */
xyz.swapee.wc.front.IOffersAggregatorControllerATCaster
/** @type {!xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT} */
xyz.swapee.wc.front.IOffersAggregatorControllerATCaster.prototype.asIOffersAggregatorControllerAT
/** @type {!xyz.swapee.wc.front.BoundOffersAggregatorControllerAT} */
xyz.swapee.wc.front.IOffersAggregatorControllerATCaster.prototype.superOffersAggregatorControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.IOffersAggregatorControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.front.IOffersAggregatorControllerAT = function() {}
/** @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init */
xyz.swapee.wc.front.IOffersAggregatorControllerAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.IOffersAggregatorController = function() {}
/** @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.constructor = function(...init) {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedError = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedMin = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedMax = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatingOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatingError = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatMin = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatMax = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFixedOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFixedError = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFloatingOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFloatingError = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFixedOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFixedError = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFloatingOffer = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFloatingError = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFloatingError = function() {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadLetsExchangeFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadLetsExchangeFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangeNowFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangeNowFixedOffer = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.OffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersAggregatorController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersAggregatorController.Initialese>}
 */
xyz.swapee.wc.front.OffersAggregatorController = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init */
xyz.swapee.wc.front.OffersAggregatorController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.OffersAggregatorController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.AbstractOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.OffersAggregatorControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersAggregatorController, ...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.RecordIOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @typedef {{ setChangellyFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedOffer, unsetChangellyFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedOffer, setChangellyFixedError: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedError, unsetChangellyFixedError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedError, setChangellyFixedMin: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMin, unsetChangellyFixedMin: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMin, setChangellyFixedMax: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMax, unsetChangellyFixedMax: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMax, setChangellyFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingOffer, unsetChangellyFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingOffer, setChangellyFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingError, unsetChangellyFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingError, setChangellyFloatMin: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMin, unsetChangellyFloatMin: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMin, setChangellyFloatMax: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMax, unsetChangellyFloatMax: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMax, setLetsExchangeFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedOffer, unsetLetsExchangeFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedOffer, setLetsExchangeFixedError: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedError, unsetLetsExchangeFixedError: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedError, setLetsExchangeFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingOffer, unsetLetsExchangeFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingOffer, setLetsExchangeFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingError, unsetLetsExchangeFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingError, setChangeNowFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedOffer, unsetChangeNowFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedOffer, setChangeNowFixedError: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedError, unsetChangeNowFixedError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedError, setChangeNowFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingOffer, unsetChangeNowFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingOffer, setChangeNowFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingError, unsetChangeNowFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingError, loadChangellyFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFixedOffer, loadLetsExchangeFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFloatingOffer, loadLetsExchangeFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFixedOffer, loadChangeNowFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFloatingOffer, loadChangeNowFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFixedOffer }} */
xyz.swapee.wc.front.RecordIOffersAggregatorController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.RecordIOffersAggregatorControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOffersAggregatorControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersAggregatorControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.BoundIOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersAggregatorController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.BoundIOffersAggregatorController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.BoundOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundOffersAggregatorController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedMin
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedMin
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedMax
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedMax
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatingError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatingError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatMin
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatMin
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatMax
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatMax
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFixedError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFixedError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFloatingError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFloatingError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFixedError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFixedError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFloatingError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFloatingError
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.IOffersAggregatorController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
xyz.swapee.wc.back.IOffersAggregatorController.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.IOffersAggregatorControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/** @interface */
xyz.swapee.wc.back.IOffersAggregatorControllerCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorController} */
xyz.swapee.wc.back.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorController
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorController} */
xyz.swapee.wc.back.IOffersAggregatorControllerCaster.prototype.superOffersAggregatorController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.IOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
xyz.swapee.wc.back.IOffersAggregatorController = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init */
xyz.swapee.wc.back.IOffersAggregatorController.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.OffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorController.Initialese>}
 */
xyz.swapee.wc.back.OffersAggregatorController = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init */
xyz.swapee.wc.back.OffersAggregatorController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.OffersAggregatorController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.AbstractOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.OffersAggregatorControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorController, ...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.RecordIOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.BoundIOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
xyz.swapee.wc.back.BoundIOffersAggregatorController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.BoundOffersAggregatorController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersAggregatorController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.IOffersAggregatorControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/** @interface */
xyz.swapee.wc.back.IOffersAggregatorControllerARCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR} */
xyz.swapee.wc.back.IOffersAggregatorControllerARCaster.prototype.asIOffersAggregatorControllerAR
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorControllerAR} */
xyz.swapee.wc.back.IOffersAggregatorControllerARCaster.prototype.superOffersAggregatorControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.IOffersAggregatorControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 */
xyz.swapee.wc.back.IOffersAggregatorControllerAR = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init */
xyz.swapee.wc.back.IOffersAggregatorControllerAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.OffersAggregatorControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese>}
 */
xyz.swapee.wc.back.OffersAggregatorControllerAR = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init */
xyz.swapee.wc.back.OffersAggregatorControllerAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.OffersAggregatorControllerAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorControllerAR|typeof xyz.swapee.wc.back.OffersAggregatorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorControllerAR|typeof xyz.swapee.wc.back.OffersAggregatorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorControllerAR|typeof xyz.swapee.wc.back.OffersAggregatorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.OffersAggregatorControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorControllerAR, ...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.RecordIOffersAggregatorControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 */
xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.BoundOffersAggregatorControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersAggregatorControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.OffersAggregatorControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersAggregatorControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese>}
 */
xyz.swapee.wc.front.OffersAggregatorControllerAT = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init */
xyz.swapee.wc.front.OffersAggregatorControllerAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.OffersAggregatorControllerAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.OffersAggregatorControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersAggregatorControllerAT, ...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.BoundOffersAggregatorControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundOffersAggregatorControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.IOffersAggregatorScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.front.OffersAggregatorInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, !xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplay.Initialese}
 */
xyz.swapee.wc.IOffersAggregatorScreen.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.IOffersAggregatorScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/** @interface */
xyz.swapee.wc.IOffersAggregatorScreenCaster
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorScreen} */
xyz.swapee.wc.IOffersAggregatorScreenCaster.prototype.asIOffersAggregatorScreen
/** @type {!xyz.swapee.wc.BoundOffersAggregatorScreen} */
xyz.swapee.wc.IOffersAggregatorScreenCaster.prototype.superOffersAggregatorScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.IOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.front.OffersAggregatorInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, !xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorController}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplay}
 */
xyz.swapee.wc.IOffersAggregatorScreen = function() {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init */
xyz.swapee.wc.IOffersAggregatorScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.OffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorScreen.Initialese>}
 */
xyz.swapee.wc.OffersAggregatorScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init */
xyz.swapee.wc.OffersAggregatorScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.OffersAggregatorScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.AbstractOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.OffersAggregatorScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorScreen, ...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese)} */
xyz.swapee.wc.OffersAggregatorScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.RecordIOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.BoundIOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.front.OffersAggregatorInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, !xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorController}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorDisplay}
 */
xyz.swapee.wc.BoundIOffersAggregatorScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.BoundOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundOffersAggregatorScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese}
 */
xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.IOffersAggregatorScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/** @interface */
xyz.swapee.wc.back.IOffersAggregatorScreenCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorScreen} */
xyz.swapee.wc.back.IOffersAggregatorScreenCaster.prototype.asIOffersAggregatorScreen
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorScreen} */
xyz.swapee.wc.back.IOffersAggregatorScreenCaster.prototype.superOffersAggregatorScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.IOffersAggregatorScreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/** @interface */
xyz.swapee.wc.back.IOffersAggregatorScreenATCaster
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT} */
xyz.swapee.wc.back.IOffersAggregatorScreenATCaster.prototype.asIOffersAggregatorScreenAT
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorScreenAT} */
xyz.swapee.wc.back.IOffersAggregatorScreenATCaster.prototype.superOffersAggregatorScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.IOffersAggregatorScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.back.IOffersAggregatorScreenAT = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init */
xyz.swapee.wc.back.IOffersAggregatorScreenAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.IOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenCaster}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.IOffersAggregatorScreen = function() {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init */
xyz.swapee.wc.back.IOffersAggregatorScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.OffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese>}
 */
xyz.swapee.wc.back.OffersAggregatorScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init */
xyz.swapee.wc.back.OffersAggregatorScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.OffersAggregatorScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.AbstractOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.OffersAggregatorScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorScreen, ...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.RecordIOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.RecordIOffersAggregatorScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.BoundIOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.BoundIOffersAggregatorScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.BoundOffersAggregatorScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersAggregatorScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreen.Initialese}
 */
xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.IOffersAggregatorScreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/** @interface */
xyz.swapee.wc.front.IOffersAggregatorScreenARCaster
/** @type {!xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR} */
xyz.swapee.wc.front.IOffersAggregatorScreenARCaster.prototype.asIOffersAggregatorScreenAR
/** @type {!xyz.swapee.wc.front.BoundOffersAggregatorScreenAR} */
xyz.swapee.wc.front.IOffersAggregatorScreenARCaster.prototype.superOffersAggregatorScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.IOffersAggregatorScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreen}
 */
xyz.swapee.wc.front.IOffersAggregatorScreenAR = function() {}
/** @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init */
xyz.swapee.wc.front.IOffersAggregatorScreenAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.OffersAggregatorScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersAggregatorScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese>}
 */
xyz.swapee.wc.front.OffersAggregatorScreenAR = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init */
xyz.swapee.wc.front.OffersAggregatorScreenAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.OffersAggregatorScreenAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorScreenAR|typeof xyz.swapee.wc.front.OffersAggregatorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorScreenAR|typeof xyz.swapee.wc.front.OffersAggregatorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorScreenAR|typeof xyz.swapee.wc.front.OffersAggregatorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.OffersAggregatorScreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersAggregatorScreenAR, ...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.RecordIOffersAggregatorScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOffersAggregatorScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersAggregatorScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorScreen}
 */
xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.BoundOffersAggregatorScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundOffersAggregatorScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.OffersAggregatorScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese>}
 */
xyz.swapee.wc.back.OffersAggregatorScreenAT = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init */
xyz.swapee.wc.back.OffersAggregatorScreenAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.OffersAggregatorScreenAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.OffersAggregatorScreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorScreenAT, ...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.BoundOffersAggregatorScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundOffersAggregatorScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe.prototype.changellyFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe.prototype.changellyFixedMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe.prototype.changellyFixedMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe.prototype.changellyFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe.prototype.letsExchangeFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe.prototype.letsExchangeFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe.prototype.changeNowFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe.prototype.changeNowFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe.prototype.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe.prototype.changellyFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe.prototype.changellyFixedMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe.prototype.changellyFixedMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe.prototype.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe.prototype.changellyFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe.prototype.changellyFloatMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe.prototype.changellyFloatMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe.prototype.letsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe.prototype.letsExchangeFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe.prototype.letsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe.prototype.letsExchangeFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe.prototype.changeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe.prototype.changeNowFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe.prototype.changeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe.prototype.changeNowFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe.prototype.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe.prototype.worstOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe.prototype.minError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe = function() {}
/** @type {?number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe.prototype.maxError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe.prototype.loadingEstimate

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe.prototype.allExchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe.prototype.exchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe.prototype.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe.prototype.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe.prototype.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe.prototype.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe.prototype.hasMoreLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe.prototype.loadLetsExchangeFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe.prototype.hasMoreLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe.prototype.loadLetsExchangeFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe.prototype.hasMoreChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe.prototype.loadChangeNowFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe.prototype.hasMoreChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe.prototype.loadChangeNowFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal}
 */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal = function() {}

// nss:xyz.swapee.wc
/* @typal-end */