/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorController.Initialese}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregator.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} */
xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent} */
$xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster.prototype.asIOffersAggregatorHtmlComponent
/** @type {!xyz.swapee.wc.BoundOffersAggregatorHtmlComponent} */
$xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster.prototype.superOffersAggregatorHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster}
 */
xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.IOffersAggregatorHtmlComponent filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorController}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreen}
 * @extends {xyz.swapee.wc.IOffersAggregator}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.IOffersAggregatorHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.OffersAggregatorHtmlComponent filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.OffersAggregatorHtmlComponent
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorHtmlComponent, ...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese)} */
xyz.swapee.wc.OffersAggregatorHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.OffersAggregatorHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
$xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent)} */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorHtmlComponent|typeof xyz.swapee.wc.OffersAggregatorHtmlComponent)|(!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorHtmlComponent|typeof xyz.swapee.wc.OffersAggregatorHtmlComponent)|(!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorHtmlComponent|typeof xyz.swapee.wc.OffersAggregatorHtmlComponent)|(!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.OffersAggregatorHtmlComponentConstructor filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorHtmlComponent, ...!xyz.swapee.wc.IOffersAggregatorHtmlComponent.Initialese)} */
xyz.swapee.wc.OffersAggregatorHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.RecordIOffersAggregatorHtmlComponent filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorController}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreen}
 * @extends {xyz.swapee.wc.BoundIOffersAggregator}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 */
$xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent} */
xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/12-IOffersAggregatorHtmlComponent.xml} xyz.swapee.wc.BoundOffersAggregatorHtmlComponent filter:!ControllerPlugin~props 4c1f6e57225e5126ee49a144cb9c7196 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorHtmlComponent} */
xyz.swapee.wc.BoundOffersAggregatorHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */