/** @const {?} */ $xyz.swapee.wc.back.IOffersAggregatorScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese}
 */
$xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} */
xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersAggregatorScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.IOffersAggregatorScreenCaster filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/** @interface */
$xyz.swapee.wc.back.IOffersAggregatorScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorScreen} */
$xyz.swapee.wc.back.IOffersAggregatorScreenCaster.prototype.asIOffersAggregatorScreen
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorScreen} */
$xyz.swapee.wc.back.IOffersAggregatorScreenCaster.prototype.superOffersAggregatorScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorScreenCaster}
 */
xyz.swapee.wc.back.IOffersAggregatorScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.IOffersAggregatorScreen filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenCaster}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenAT}
 */
$xyz.swapee.wc.back.IOffersAggregatorScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorScreen}
 */
xyz.swapee.wc.back.IOffersAggregatorScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.OffersAggregatorScreen filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese>}
 */
$xyz.swapee.wc.back.OffersAggregatorScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.OffersAggregatorScreen
/** @type {function(new: xyz.swapee.wc.back.IOffersAggregatorScreen, ...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.OffersAggregatorScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.AbstractOffersAggregatorScreen filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorScreen}
 */
$xyz.swapee.wc.back.AbstractOffersAggregatorScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersAggregatorScreen)} */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreen|typeof xyz.swapee.wc.back.OffersAggregatorScreen)|(!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreen}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.OffersAggregatorScreenConstructor filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorScreen, ...!xyz.swapee.wc.back.IOffersAggregatorScreen.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.RecordIOffersAggregatorScreen filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.BoundIOffersAggregatorScreen filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT}
 */
$xyz.swapee.wc.back.BoundIOffersAggregatorScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersAggregatorScreen} */
xyz.swapee.wc.back.BoundIOffersAggregatorScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreenBack.xml} xyz.swapee.wc.back.BoundOffersAggregatorScreen filter:!ControllerPlugin~props b388c83bbefc749e0115678da16f6450 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersAggregatorScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersAggregatorScreen} */
xyz.swapee.wc.back.BoundOffersAggregatorScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */