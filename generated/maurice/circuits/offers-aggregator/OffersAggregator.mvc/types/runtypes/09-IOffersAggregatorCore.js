/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorCore
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorCore.Model
/** @const {?} */ xyz.swapee.wc.IOffersAggregatorCore
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Initialese filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Initialese} */
xyz.swapee.wc.IOffersAggregatorCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCoreFields filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregatorCore.Model} */
$xyz.swapee.wc.IOffersAggregatorCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorCoreFields}
 */
xyz.swapee.wc.IOffersAggregatorCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCoreCaster filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorCore} */
$xyz.swapee.wc.IOffersAggregatorCoreCaster.prototype.asIOffersAggregatorCore
/** @type {!xyz.swapee.wc.BoundOffersAggregatorCore} */
$xyz.swapee.wc.IOffersAggregatorCoreCaster.prototype.superOffersAggregatorCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorCoreCaster}
 */
xyz.swapee.wc.IOffersAggregatorCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore}
 */
$xyz.swapee.wc.IOffersAggregatorCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorCore.prototype.resetOffersAggregatorCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorCore}
 */
xyz.swapee.wc.IOffersAggregatorCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.OffersAggregatorCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorCore.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.OffersAggregatorCore
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorCore)} */
xyz.swapee.wc.OffersAggregatorCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.OffersAggregatorCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.AbstractOffersAggregatorCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorCore}
 */
$xyz.swapee.wc.AbstractOffersAggregatorCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorCore)} */
xyz.swapee.wc.AbstractOffersAggregatorCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.RecordIOffersAggregatorCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {{ resetCore: xyz.swapee.wc.IOffersAggregatorCore.resetCore, resetOffersAggregatorCore: xyz.swapee.wc.IOffersAggregatorCore.resetOffersAggregatorCore }} */
xyz.swapee.wc.RecordIOffersAggregatorCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.BoundIOffersAggregatorCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCoreCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorOuterCore}
 */
$xyz.swapee.wc.BoundIOffersAggregatorCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorCore} */
xyz.swapee.wc.BoundIOffersAggregatorCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.BoundOffersAggregatorCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorCore} */
xyz.swapee.wc.BoundOffersAggregatorCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.resetCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorCore): void} */
xyz.swapee.wc.IOffersAggregatorCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorCore.__resetCore} */
xyz.swapee.wc.IOffersAggregatorCore.__resetCore

// nss:xyz.swapee.wc.IOffersAggregatorCore,$xyz.swapee.wc.IOffersAggregatorCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.resetOffersAggregatorCore filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorCore.__resetOffersAggregatorCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorCore.resetOffersAggregatorCore
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorCore): void} */
xyz.swapee.wc.IOffersAggregatorCore._resetOffersAggregatorCore
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorCore.__resetOffersAggregatorCore} */
xyz.swapee.wc.IOffersAggregatorCore.__resetOffersAggregatorCore

// nss:xyz.swapee.wc.IOffersAggregatorCore,$xyz.swapee.wc.IOffersAggregatorCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer.bestOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer.bestOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer.worstOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer.worstOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount.minAmount filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount.minAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinError.minError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError.minError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount.maxAmount filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount.maxAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError.maxError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError.maxError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut.estimatedOut filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut.estimatedOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate.loadingEstimate filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate.loadingEstimate

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded.allExchangesLoaded filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded.allExchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded.exchangesLoaded filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded.exchangesLoaded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.Host.host filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating.isAggregating filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating.isAggregating

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer.loadingLetsExchangeFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer.loadingLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer.hasMoreLetsExchangeFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer.hasMoreLetsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError.loadLetsExchangeFloatingOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError.loadLetsExchangeFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer.loadingLetsExchangeFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer.loadingLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer.hasMoreLetsExchangeFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer.hasMoreLetsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError.loadLetsExchangeFixedOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError.loadLetsExchangeFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer.loadingChangeNowFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer.loadingChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer.hasMoreChangeNowFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer.hasMoreChangeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError.loadChangeNowFloatingOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError.loadChangeNowFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer.loadingChangeNowFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer.loadingChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer.hasMoreChangeNowFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer.hasMoreChangeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError.loadChangeNowFixedOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @typedef {Error} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError.loadChangeNowFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer.prototype.bestOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer.prototype.worstOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount.prototype.minAmount
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinError = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinError.prototype.minError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MinError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount.prototype.maxAmount
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError.prototype.maxError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut.prototype.estimatedOut
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut} */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate.prototype.loadingEstimate
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded.prototype.allExchangesLoaded
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded} */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded.prototype.exchangesLoaded
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.Host filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.Host = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.Host.prototype.host
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.Host} */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating.prototype.isAggregating
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating} */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer.prototype.loadingChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer.prototype.hasMoreChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError.prototype.loadChangellyFloatingOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer.prototype.loadingChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer.prototype.hasMoreChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError.prototype.loadChangellyFixedOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer.prototype.loadingLetsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer.prototype.hasMoreLetsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError.prototype.loadLetsExchangeFloatingOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer.prototype.loadingLetsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer.prototype.hasMoreLetsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError.prototype.loadLetsExchangeFixedOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer.prototype.loadingChangeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer.prototype.hasMoreChangeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError.prototype.loadChangeNowFloatingOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer.prototype.loadingChangeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer.prototype.hasMoreChangeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError.prototype.loadChangeNowFixedOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.Host}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model} */
xyz.swapee.wc.IOffersAggregatorCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe.prototype.bestOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe.prototype.worstOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe.prototype.minAmount
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe.prototype.minError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MinError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe.prototype.maxAmount
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe.prototype.maxError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe.prototype.estimatedOut
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe.prototype.loadingEstimate
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe.prototype.allExchangesLoaded
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe.prototype.exchangesLoaded
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe.prototype.host
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe.prototype.isAggregating
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe.prototype.loadingChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe.prototype.hasMoreChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe.prototype.loadChangellyFloatingOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFloatingOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe.prototype.loadingChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe.prototype.hasMoreChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe.prototype.loadChangellyFixedOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangellyFixedOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe.prototype.loadingLetsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe.prototype.hasMoreLetsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe.prototype.loadLetsExchangeFloatingOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFloatingOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe.prototype.loadingLetsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe.prototype.hasMoreLetsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreLetsExchangeFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe.prototype.loadLetsExchangeFixedOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadLetsExchangeFixedOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe.prototype.loadingChangeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe.prototype.hasMoreChangeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe.prototype.loadChangeNowFloatingOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFloatingOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe.prototype.loadingChangeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe.prototype.hasMoreChangeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.HasMoreChangeNowFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/09-IOffersAggregatorCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe filter:!ControllerPlugin~props 19b96eb7bde3b3e463d15949804ed3b5 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe.prototype.loadChangeNowFixedOfferError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LoadChangeNowFixedOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */