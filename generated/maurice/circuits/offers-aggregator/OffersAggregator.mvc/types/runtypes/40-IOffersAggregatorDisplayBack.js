/** @const {?} */ $xyz.swapee.wc.back.IOffersAggregatorDisplay
/** @const {?} */ xyz.swapee.wc.back.IOffersAggregatorDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OffersAggregatorClasses>}
 */
$xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese.prototype.AmountInIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese.prototype.AmountOutIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese.prototype.ExchangeIntent
/** @typedef {$xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersAggregatorDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplayFields filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/** @interface */
$xyz.swapee.wc.back.IOffersAggregatorDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersAggregatorDisplayFields.prototype.AmountInIn
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersAggregatorDisplayFields.prototype.AmountOutIn
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOffersAggregatorDisplayFields.prototype.ExchangeIntent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorDisplayFields}
 */
xyz.swapee.wc.back.IOffersAggregatorDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplayCaster filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/** @interface */
$xyz.swapee.wc.back.IOffersAggregatorDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorDisplay} */
$xyz.swapee.wc.back.IOffersAggregatorDisplayCaster.prototype.asIOffersAggregatorDisplay
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorDisplay} */
$xyz.swapee.wc.back.IOffersAggregatorDisplayCaster.prototype.superOffersAggregatorDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorDisplayCaster}
 */
xyz.swapee.wc.back.IOffersAggregatorDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplay filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.OffersAggregatorClasses, !xyz.swapee.wc.OffersAggregatorLand>}
 */
$xyz.swapee.wc.back.IOffersAggregatorDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [memory]
 * @param {!xyz.swapee.wc.OffersAggregatorLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOffersAggregatorDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorDisplay}
 */
xyz.swapee.wc.back.IOffersAggregatorDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.OffersAggregatorDisplay filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IOffersAggregatorDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese>}
 */
$xyz.swapee.wc.back.OffersAggregatorDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.OffersAggregatorDisplay
/** @type {function(new: xyz.swapee.wc.back.IOffersAggregatorDisplay)} */
xyz.swapee.wc.back.OffersAggregatorDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.OffersAggregatorDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.AbstractOffersAggregatorDisplay filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
$xyz.swapee.wc.back.AbstractOffersAggregatorDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractOffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersAggregatorDisplay)} */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorDisplay}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.RecordIOffersAggregatorDisplay filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/** @typedef {{ paint: xyz.swapee.wc.back.IOffersAggregatorDisplay.paint }} */
xyz.swapee.wc.back.RecordIOffersAggregatorDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.BoundIOffersAggregatorDisplay filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.OffersAggregatorClasses, !xyz.swapee.wc.OffersAggregatorLand>}
 */
$xyz.swapee.wc.back.BoundIOffersAggregatorDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersAggregatorDisplay} */
xyz.swapee.wc.back.BoundIOffersAggregatorDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.BoundOffersAggregatorDisplay filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersAggregatorDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersAggregatorDisplay} */
xyz.swapee.wc.back.BoundOffersAggregatorDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplayBack.xml} xyz.swapee.wc.back.IOffersAggregatorDisplay.paint filter:!ControllerPlugin~props f1db30f27a90e6325d3d73fec2cc807b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [memory]
 * @param {!xyz.swapee.wc.OffersAggregatorLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOffersAggregatorDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersAggregatorMemory=, !xyz.swapee.wc.OffersAggregatorLand=): void} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IOffersAggregatorDisplay, !xyz.swapee.wc.OffersAggregatorMemory=, !xyz.swapee.wc.OffersAggregatorLand=): void} */
xyz.swapee.wc.back.IOffersAggregatorDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IOffersAggregatorDisplay.__paint} */
xyz.swapee.wc.back.IOffersAggregatorDisplay.__paint

// nss:xyz.swapee.wc.back.IOffersAggregatorDisplay,$xyz.swapee.wc.back.IOffersAggregatorDisplay,xyz.swapee.wc.back
/* @typal-end */