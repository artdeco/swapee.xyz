/** @const {?} */ $xyz.swapee.wc.front.IOffersAggregatorScreenAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreen.Initialese}
 */
$xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} */
xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOffersAggregatorScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.IOffersAggregatorScreenARCaster filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/** @interface */
$xyz.swapee.wc.front.IOffersAggregatorScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR} */
$xyz.swapee.wc.front.IOffersAggregatorScreenARCaster.prototype.asIOffersAggregatorScreenAR
/** @type {!xyz.swapee.wc.front.BoundOffersAggregatorScreenAR} */
$xyz.swapee.wc.front.IOffersAggregatorScreenARCaster.prototype.superOffersAggregatorScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersAggregatorScreenARCaster}
 */
xyz.swapee.wc.front.IOffersAggregatorScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.IOffersAggregatorScreenAR filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreen}
 */
$xyz.swapee.wc.front.IOffersAggregatorScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.IOffersAggregatorScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.OffersAggregatorScreenAR filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersAggregatorScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.OffersAggregatorScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.OffersAggregatorScreenAR
/** @type {function(new: xyz.swapee.wc.front.IOffersAggregatorScreenAR, ...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.OffersAggregatorScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
$xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR)} */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorScreenAR|typeof xyz.swapee.wc.front.OffersAggregatorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorScreenAR|typeof xyz.swapee.wc.front.OffersAggregatorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorScreenAR|typeof xyz.swapee.wc.front.OffersAggregatorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.OffersAggregatorScreenARConstructor filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersAggregatorScreenAR, ...!xyz.swapee.wc.front.IOffersAggregatorScreenAR.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.RecordIOffersAggregatorScreenAR filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOffersAggregatorScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersAggregatorScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorScreen}
 */
$xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR} */
xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/73-IOffersAggregatorScreenAR.xml} xyz.swapee.wc.front.BoundOffersAggregatorScreenAR filter:!ControllerPlugin~props 7aec4515e72b03b60c9eed4287073af4 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOffersAggregatorScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOffersAggregatorScreenAR} */
xyz.swapee.wc.front.BoundOffersAggregatorScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */