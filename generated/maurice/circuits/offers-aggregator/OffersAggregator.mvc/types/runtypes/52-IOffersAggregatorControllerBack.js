/** @const {?} */ $xyz.swapee.wc.back.IOffersAggregatorController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.IOffersAggregatorController.Initialese filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
$xyz.swapee.wc.back.IOffersAggregatorController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOffersAggregatorController.Initialese} */
xyz.swapee.wc.back.IOffersAggregatorController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersAggregatorController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.IOffersAggregatorControllerCaster filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/** @interface */
$xyz.swapee.wc.back.IOffersAggregatorControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorController} */
$xyz.swapee.wc.back.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorController
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorController} */
$xyz.swapee.wc.back.IOffersAggregatorControllerCaster.prototype.superOffersAggregatorController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorControllerCaster}
 */
xyz.swapee.wc.back.IOffersAggregatorControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.IOffersAggregatorController filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
$xyz.swapee.wc.back.IOffersAggregatorController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorController}
 */
xyz.swapee.wc.back.IOffersAggregatorController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.OffersAggregatorController filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorController.Initialese>}
 */
$xyz.swapee.wc.back.OffersAggregatorController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.OffersAggregatorController
/** @type {function(new: xyz.swapee.wc.back.IOffersAggregatorController, ...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.OffersAggregatorController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.AbstractOffersAggregatorController filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorController}
 */
$xyz.swapee.wc.back.AbstractOffersAggregatorController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersAggregatorController)} */
xyz.swapee.wc.back.AbstractOffersAggregatorController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorController|typeof xyz.swapee.wc.back.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorController}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.OffersAggregatorControllerConstructor filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorController, ...!xyz.swapee.wc.back.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.RecordIOffersAggregatorController filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.BoundIOffersAggregatorController filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIOffersAggregatorController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersAggregatorController} */
xyz.swapee.wc.back.BoundIOffersAggregatorController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/52-IOffersAggregatorControllerBack.xml} xyz.swapee.wc.back.BoundOffersAggregatorController filter:!ControllerPlugin~props cc5eded3330978cfc7b4420e4b17a922 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersAggregatorController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersAggregatorController} */
xyz.swapee.wc.back.BoundOffersAggregatorController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */