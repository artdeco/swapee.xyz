/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.IOffersAggregatorProcessor.Initialese filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} */
xyz.swapee.wc.IOffersAggregatorProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.IOffersAggregatorProcessorCaster filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorProcessor} */
$xyz.swapee.wc.IOffersAggregatorProcessorCaster.prototype.asIOffersAggregatorProcessor
/** @type {!xyz.swapee.wc.BoundOffersAggregatorProcessor} */
$xyz.swapee.wc.IOffersAggregatorProcessorCaster.prototype.superOffersAggregatorProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorProcessorCaster}
 */
xyz.swapee.wc.IOffersAggregatorProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.IOffersAggregatorProcessor filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessorCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 */
$xyz.swapee.wc.IOffersAggregatorProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorProcessor}
 */
xyz.swapee.wc.IOffersAggregatorProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.OffersAggregatorProcessor filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.OffersAggregatorProcessor
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorProcessor, ...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese)} */
xyz.swapee.wc.OffersAggregatorProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.OffersAggregatorProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.AbstractOffersAggregatorProcessor filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorProcessor}
 */
$xyz.swapee.wc.AbstractOffersAggregatorProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorProcessor)} */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorCore|typeof xyz.swapee.wc.OffersAggregatorCore)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorProcessor}
 */
xyz.swapee.wc.AbstractOffersAggregatorProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.OffersAggregatorProcessorConstructor filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorProcessor, ...!xyz.swapee.wc.IOffersAggregatorProcessor.Initialese)} */
xyz.swapee.wc.OffersAggregatorProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.RecordIOffersAggregatorProcessor filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.BoundIOffersAggregatorProcessor filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorCore}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 */
$xyz.swapee.wc.BoundIOffersAggregatorProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorProcessor} */
xyz.swapee.wc.BoundIOffersAggregatorProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/10-IOffersAggregatorProcessor.xml} xyz.swapee.wc.BoundOffersAggregatorProcessor filter:!ControllerPlugin~props 5a6d127cd9a6aae6aaec1d2058555190 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorProcessor} */
xyz.swapee.wc.BoundOffersAggregatorProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */