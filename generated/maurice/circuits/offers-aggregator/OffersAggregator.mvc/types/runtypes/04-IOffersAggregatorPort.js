/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorPort
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorPortInterface
/** @const {?} */ xyz.swapee.wc.IOffersAggregatorPort
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.Initialese filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Initialese} */
xyz.swapee.wc.IOffersAggregatorPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortFields filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorPortFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregatorPort.Inputs} */
$xyz.swapee.wc.IOffersAggregatorPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOffersAggregatorPort.Inputs} */
$xyz.swapee.wc.IOffersAggregatorPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorPortFields}
 */
xyz.swapee.wc.IOffersAggregatorPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortCaster filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorPort} */
$xyz.swapee.wc.IOffersAggregatorPortCaster.prototype.asIOffersAggregatorPort
/** @type {!xyz.swapee.wc.BoundOffersAggregatorPort} */
$xyz.swapee.wc.IOffersAggregatorPortCaster.prototype.superOffersAggregatorPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorPortCaster}
 */
xyz.swapee.wc.IOffersAggregatorPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersAggregatorPort.Inputs>}
 */
$xyz.swapee.wc.IOffersAggregatorPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorPort.prototype.resetOffersAggregatorPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorPort}
 */
xyz.swapee.wc.IOffersAggregatorPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.OffersAggregatorPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorPort.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.OffersAggregatorPort
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorPort, ...!xyz.swapee.wc.IOffersAggregatorPort.Initialese)} */
xyz.swapee.wc.OffersAggregatorPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.OffersAggregatorPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.AbstractOffersAggregatorPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorPort}
 */
$xyz.swapee.wc.AbstractOffersAggregatorPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorPort)} */
xyz.swapee.wc.AbstractOffersAggregatorPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorPort|typeof xyz.swapee.wc.OffersAggregatorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorPort|typeof xyz.swapee.wc.OffersAggregatorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorPort|typeof xyz.swapee.wc.OffersAggregatorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.OffersAggregatorPortConstructor filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorPort, ...!xyz.swapee.wc.IOffersAggregatorPort.Initialese)} */
xyz.swapee.wc.OffersAggregatorPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.RecordIOffersAggregatorPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersAggregatorPort.resetPort, resetOffersAggregatorPort: xyz.swapee.wc.IOffersAggregatorPort.resetOffersAggregatorPort }} */
xyz.swapee.wc.RecordIOffersAggregatorPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.BoundIOffersAggregatorPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorPortFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOffersAggregatorPort.Inputs>}
 */
$xyz.swapee.wc.BoundIOffersAggregatorPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorPort} */
xyz.swapee.wc.BoundIOffersAggregatorPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.BoundOffersAggregatorPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorPort} */
xyz.swapee.wc.BoundOffersAggregatorPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.resetPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorPort): void} */
xyz.swapee.wc.IOffersAggregatorPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorPort.__resetPort} */
xyz.swapee.wc.IOffersAggregatorPort.__resetPort

// nss:xyz.swapee.wc.IOffersAggregatorPort,$xyz.swapee.wc.IOffersAggregatorPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.resetOffersAggregatorPort filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorPort.__resetOffersAggregatorPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorPort.resetOffersAggregatorPort
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorPort): void} */
xyz.swapee.wc.IOffersAggregatorPort._resetOffersAggregatorPort
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorPort.__resetOffersAggregatorPort} */
xyz.swapee.wc.IOffersAggregatorPort.__resetOffersAggregatorPort

// nss:xyz.swapee.wc.IOffersAggregatorPort,$xyz.swapee.wc.IOffersAggregatorPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersAggregatorPort.Inputs}
 */
xyz.swapee.wc.IOffersAggregatorPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs}
 */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortInterface filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorPortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorPortInterface}
 */
xyz.swapee.wc.IOffersAggregatorPortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.OffersAggregatorPortInterface filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorPortInterface}
 */
$xyz.swapee.wc.OffersAggregatorPortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorPortInterface}
 */
xyz.swapee.wc.OffersAggregatorPortInterface
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorPortInterface)} */
xyz.swapee.wc.OffersAggregatorPortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/04-IOffersAggregatorPort.xml} xyz.swapee.wc.IOffersAggregatorPortInterface.Props filter:!ControllerPlugin~props 54380e5ee5fb646228b776fb077630c9 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedError
/** @type {number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedMin
/** @type {number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFixedMax
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatingError
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatMin
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changellyFloatMax
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFixedOffer
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFixedError
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFloatingOffer
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.letsExchangeFloatingError
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFixedOffer
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFixedError
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFloatingOffer
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.changeNowFloatingError
/** @type {number|undefined} */
$xyz.swapee.wc.IOffersAggregatorPortInterface.Props.prototype.exchangesTotal
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPortInterface.Props} */
xyz.swapee.wc.IOffersAggregatorPortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPortInterface
/* @typal-end */