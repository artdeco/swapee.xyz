/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorDisplay
/** @const {?} */ xyz.swapee.wc.IOffersAggregatorDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.Initialese filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings>}
 */
$xyz.swapee.wc.IOffersAggregatorDisplay.Initialese = function() {}
/** @type {HTMLInputElement|undefined} */
$xyz.swapee.wc.IOffersAggregatorDisplay.Initialese.prototype.AmountInIn
/** @type {HTMLInputElement|undefined} */
$xyz.swapee.wc.IOffersAggregatorDisplay.Initialese.prototype.AmountOutIn
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IOffersAggregatorDisplay.Initialese.prototype.ExchangeIntent
/** @typedef {$xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} */
xyz.swapee.wc.IOffersAggregatorDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplayFields filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregatorDisplay.Settings} */
$xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IOffersAggregatorDisplay.Queries} */
$xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.queries
/** @type {HTMLInputElement} */
$xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.AmountInIn
/** @type {HTMLInputElement} */
$xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.AmountOutIn
/** @type {HTMLElement} */
$xyz.swapee.wc.IOffersAggregatorDisplayFields.prototype.ExchangeIntent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorDisplayFields}
 */
xyz.swapee.wc.IOffersAggregatorDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplayCaster filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorDisplay} */
$xyz.swapee.wc.IOffersAggregatorDisplayCaster.prototype.asIOffersAggregatorDisplay
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorScreen} */
$xyz.swapee.wc.IOffersAggregatorDisplayCaster.prototype.asIOffersAggregatorScreen
/** @type {!xyz.swapee.wc.BoundOffersAggregatorDisplay} */
$xyz.swapee.wc.IOffersAggregatorDisplayCaster.prototype.superOffersAggregatorDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorDisplayCaster}
 */
xyz.swapee.wc.IOffersAggregatorDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.OffersAggregatorMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null>}
 */
$xyz.swapee.wc.IOffersAggregatorDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorDisplay}
 */
xyz.swapee.wc.IOffersAggregatorDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.OffersAggregatorDisplay filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.OffersAggregatorDisplay
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorDisplay, ...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese)} */
xyz.swapee.wc.OffersAggregatorDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.OffersAggregatorDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.AbstractOffersAggregatorDisplay filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorDisplay}
 */
$xyz.swapee.wc.AbstractOffersAggregatorDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorDisplay)} */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorDisplay}
 */
xyz.swapee.wc.AbstractOffersAggregatorDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.OffersAggregatorDisplayConstructor filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorDisplay, ...!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese)} */
xyz.swapee.wc.OffersAggregatorDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.RecordIOffersAggregatorDisplay filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @typedef {{ paint: xyz.swapee.wc.IOffersAggregatorDisplay.paint }} */
xyz.swapee.wc.RecordIOffersAggregatorDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.BoundIOffersAggregatorDisplay filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.OffersAggregatorMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIOffersAggregatorDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorDisplay} */
xyz.swapee.wc.BoundIOffersAggregatorDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.BoundOffersAggregatorDisplay filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorDisplay} */
xyz.swapee.wc.BoundOffersAggregatorDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.paint filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OffersAggregatorMemory, null): void} */
xyz.swapee.wc.IOffersAggregatorDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorDisplay, !xyz.swapee.wc.OffersAggregatorMemory, null): void} */
xyz.swapee.wc.IOffersAggregatorDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorDisplay.__paint} */
xyz.swapee.wc.IOffersAggregatorDisplay.__paint

// nss:xyz.swapee.wc.IOffersAggregatorDisplay,$xyz.swapee.wc.IOffersAggregatorDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.Queries filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/** @record */
$xyz.swapee.wc.IOffersAggregatorDisplay.Queries = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorDisplay.Queries.prototype.exchangeIntentSel
/** @typedef {$xyz.swapee.wc.IOffersAggregatorDisplay.Queries} */
xyz.swapee.wc.IOffersAggregatorDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/40-IOffersAggregatorDisplay.xml} xyz.swapee.wc.IOffersAggregatorDisplay.Settings filter:!ControllerPlugin~props 3dac5830f79ac462f5867a2202d5624c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplay.Queries}
 */
$xyz.swapee.wc.IOffersAggregatorDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorDisplay.Settings} */
xyz.swapee.wc.IOffersAggregatorDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorDisplay
/* @typal-end */