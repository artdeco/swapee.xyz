/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/100-OffersAggregatorMemory.xml} xyz.swapee.wc.OffersAggregatorMemory filter:!ControllerPlugin~props 736b81bf56de2801d786633bdfcc2dca */
/** @record */
$xyz.swapee.wc.OffersAggregatorMemory = __$te_Mixin()
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedError
/** @type {number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedMin
/** @type {number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFixedMax
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatingError
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatMin
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changellyFloatMax
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFixedError
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.letsExchangeFloatingError
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFixedError
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.changeNowFloatingError
/** @type {number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.exchangesTotal
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.bestOffer
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.worstOffer
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.minAmount
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.minError
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.maxAmount
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.maxError
/** @type {?number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.estimatedOut
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingEstimate
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.allExchangesLoaded
/** @type {number} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.exchangesLoaded
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.host
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.isAggregating
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangellyFloatingOffer
/** @type {?boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangellyFloatingOffer
/** @type {?Error} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangellyFloatingOfferError
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangellyFixedOffer
/** @type {?boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangellyFixedOffer
/** @type {?Error} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangellyFixedOfferError
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingLetsExchangeFloatingOffer
/** @type {?boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreLetsExchangeFloatingOffer
/** @type {?Error} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadLetsExchangeFloatingOfferError
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingLetsExchangeFixedOffer
/** @type {?boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreLetsExchangeFixedOffer
/** @type {?Error} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadLetsExchangeFixedOfferError
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangeNowFloatingOffer
/** @type {?boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangeNowFloatingOffer
/** @type {?Error} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangeNowFloatingOfferError
/** @type {boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadingChangeNowFixedOffer
/** @type {?boolean} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.hasMoreChangeNowFixedOffer
/** @type {?Error} */
$xyz.swapee.wc.OffersAggregatorMemory.prototype.loadChangeNowFixedOfferError
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OffersAggregatorMemory}
 */
xyz.swapee.wc.OffersAggregatorMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */