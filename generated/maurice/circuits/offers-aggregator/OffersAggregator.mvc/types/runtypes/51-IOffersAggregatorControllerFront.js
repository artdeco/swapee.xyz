/** @const {?} */ $xyz.swapee.wc.front.IOffersAggregatorController
/** @const {?} */ xyz.swapee.wc.front.IOffersAggregatorController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.Initialese filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @record */
$xyz.swapee.wc.front.IOffersAggregatorController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOffersAggregatorController.Initialese} */
xyz.swapee.wc.front.IOffersAggregatorController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOffersAggregatorController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorControllerCaster filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @interface */
$xyz.swapee.wc.front.IOffersAggregatorControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOffersAggregatorController} */
$xyz.swapee.wc.front.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorController
/** @type {!xyz.swapee.wc.front.BoundOffersAggregatorController} */
$xyz.swapee.wc.front.IOffersAggregatorControllerCaster.prototype.superOffersAggregatorController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersAggregatorControllerCaster}
 */
xyz.swapee.wc.front.IOffersAggregatorControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerAT}
 */
$xyz.swapee.wc.front.IOffersAggregatorController = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedMin = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFixedMax = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFixedMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatingOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatingError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatMin = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangellyFloatMax = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangellyFloatMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFixedOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFixedError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFloatingOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setLetsExchangeFloatingError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFixedOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFixedError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFloatingOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.setChangeNowFloatingError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.unsetChangeNowFloatingError = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadLetsExchangeFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadLetsExchangeFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangeNowFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IOffersAggregatorController.prototype.loadChangeNowFixedOffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersAggregatorController}
 */
xyz.swapee.wc.front.IOffersAggregatorController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.OffersAggregatorController filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersAggregatorController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersAggregatorController.Initialese>}
 */
$xyz.swapee.wc.front.OffersAggregatorController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.OffersAggregatorController
/** @type {function(new: xyz.swapee.wc.front.IOffersAggregatorController, ...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.OffersAggregatorController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.AbstractOffersAggregatorController filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersAggregatorController}
 */
$xyz.swapee.wc.front.AbstractOffersAggregatorController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController
/** @type {function(new: xyz.swapee.wc.front.AbstractOffersAggregatorController)} */
xyz.swapee.wc.front.AbstractOffersAggregatorController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorController}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.OffersAggregatorControllerConstructor filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersAggregatorController, ...!xyz.swapee.wc.front.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.RecordIOffersAggregatorController filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/** @typedef {{ setChangellyFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedOffer, unsetChangellyFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedOffer, setChangellyFixedError: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedError, unsetChangellyFixedError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedError, setChangellyFixedMin: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMin, unsetChangellyFixedMin: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMin, setChangellyFixedMax: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMax, unsetChangellyFixedMax: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMax, setChangellyFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingOffer, unsetChangellyFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingOffer, setChangellyFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingError, unsetChangellyFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingError, setChangellyFloatMin: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMin, unsetChangellyFloatMin: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMin, setChangellyFloatMax: xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMax, unsetChangellyFloatMax: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMax, setLetsExchangeFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedOffer, unsetLetsExchangeFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedOffer, setLetsExchangeFixedError: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedError, unsetLetsExchangeFixedError: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedError, setLetsExchangeFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingOffer, unsetLetsExchangeFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingOffer, setLetsExchangeFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingError, unsetLetsExchangeFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingError, setChangeNowFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedOffer, unsetChangeNowFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedOffer, setChangeNowFixedError: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedError, unsetChangeNowFixedError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedError, setChangeNowFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingOffer, unsetChangeNowFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingOffer, setChangeNowFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingError, unsetChangeNowFloatingError: xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingError, loadChangellyFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFixedOffer, loadLetsExchangeFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFloatingOffer, loadLetsExchangeFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFixedOffer, loadChangeNowFloatingOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFloatingOffer, loadChangeNowFixedOffer: xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFixedOffer }} */
xyz.swapee.wc.front.RecordIOffersAggregatorController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.BoundIOffersAggregatorController filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersAggregatorController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT}
 */
$xyz.swapee.wc.front.BoundIOffersAggregatorController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOffersAggregatorController} */
xyz.swapee.wc.front.BoundIOffersAggregatorController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.BoundOffersAggregatorController filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOffersAggregatorController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOffersAggregatorController} */
xyz.swapee.wc.front.BoundOffersAggregatorController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMin filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedMin
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMin filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedMin
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMax filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFixedMax
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFixedMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMax filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFixedMax
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFixedMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatingError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatingError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMin filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatMin
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMin filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatMin
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMin} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMin

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMax filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangellyFloatMax
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangellyFloatMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMax filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangellyFloatMax
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMax} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangellyFloatMax

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFixedError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFixedError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setLetsExchangeFloatingError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setLetsExchangeFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetLetsExchangeFloatingError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetLetsExchangeFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFixedError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFixedError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFixedError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, number): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.IOffersAggregatorController.setChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController, string): void} */
xyz.swapee.wc.front.IOffersAggregatorController._setChangeNowFloatingError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__setChangeNowFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingError filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.unsetChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._unsetChangeNowFloatingError
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingError} */
xyz.swapee.wc.front.IOffersAggregatorController.__unsetChangeNowFloatingError

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadLetsExchangeFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFloatingOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFloatingOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFloatingOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/51-IOffersAggregatorControllerFront.xml} xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFixedOffer filter:!ControllerPlugin~props bcdae99336db6709cb4a1899fbc85c7f */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IOffersAggregatorController.loadChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IOffersAggregatorController): void} */
xyz.swapee.wc.front.IOffersAggregatorController._loadChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFixedOffer} */
xyz.swapee.wc.front.IOffersAggregatorController.__loadChangeNowFixedOffer

// nss:xyz.swapee.wc.front.IOffersAggregatorController,$xyz.swapee.wc.front.IOffersAggregatorController,xyz.swapee.wc.front
/* @typal-end */