/** @const {?} */ $xyz.swapee.wc.back.IOffersAggregatorControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
$xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} */
xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersAggregatorControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.IOffersAggregatorControllerARCaster filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/** @interface */
$xyz.swapee.wc.back.IOffersAggregatorControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR} */
$xyz.swapee.wc.back.IOffersAggregatorControllerARCaster.prototype.asIOffersAggregatorControllerAR
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorControllerAR} */
$xyz.swapee.wc.back.IOffersAggregatorControllerARCaster.prototype.superOffersAggregatorControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorControllerARCaster}
 */
xyz.swapee.wc.back.IOffersAggregatorControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.IOffersAggregatorControllerAR filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 */
$xyz.swapee.wc.back.IOffersAggregatorControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.IOffersAggregatorControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.OffersAggregatorControllerAR filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.OffersAggregatorControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.OffersAggregatorControllerAR
/** @type {function(new: xyz.swapee.wc.back.IOffersAggregatorControllerAR, ...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.OffersAggregatorControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
$xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR)} */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorControllerAR|typeof xyz.swapee.wc.back.OffersAggregatorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorControllerAR|typeof xyz.swapee.wc.back.OffersAggregatorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorControllerAR|typeof xyz.swapee.wc.back.OffersAggregatorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.OffersAggregatorControllerARConstructor filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorControllerAR, ...!xyz.swapee.wc.back.IOffersAggregatorControllerAR.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.RecordIOffersAggregatorControllerAR filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 */
$xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR} */
xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/53-IOffersAggregatorControllerAR.xml} xyz.swapee.wc.back.BoundOffersAggregatorControllerAR filter:!ControllerPlugin~props a7d24ebe9ff49ab9beb2173c24f874c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersAggregatorControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersAggregatorControllerAR} */
xyz.swapee.wc.back.BoundOffersAggregatorControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */