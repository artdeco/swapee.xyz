/** @const {?} */ $xyz.swapee.wc.back.IOffersAggregatorScreenAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} */
xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOffersAggregatorScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.IOffersAggregatorScreenATCaster filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/** @interface */
$xyz.swapee.wc.back.IOffersAggregatorScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT} */
$xyz.swapee.wc.back.IOffersAggregatorScreenATCaster.prototype.asIOffersAggregatorScreenAT
/** @type {!xyz.swapee.wc.back.BoundOffersAggregatorScreenAT} */
$xyz.swapee.wc.back.IOffersAggregatorScreenATCaster.prototype.superOffersAggregatorScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorScreenATCaster}
 */
xyz.swapee.wc.back.IOffersAggregatorScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.IOffersAggregatorScreenAT filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.IOffersAggregatorScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.IOffersAggregatorScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.OffersAggregatorScreenAT filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IOffersAggregatorScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.OffersAggregatorScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.OffersAggregatorScreenAT
/** @type {function(new: xyz.swapee.wc.back.IOffersAggregatorScreenAT, ...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.OffersAggregatorScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
$xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT)} */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOffersAggregatorScreenAT|typeof xyz.swapee.wc.back.OffersAggregatorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OffersAggregatorScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.OffersAggregatorScreenATConstructor filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/** @typedef {function(new: xyz.swapee.wc.back.IOffersAggregatorScreenAT, ...!xyz.swapee.wc.back.IOffersAggregatorScreenAT.Initialese)} */
xyz.swapee.wc.back.OffersAggregatorScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.RecordIOffersAggregatorScreenAT filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOffersAggregatorScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOffersAggregatorScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT} */
xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/74-IOffersAggregatorScreenAT.xml} xyz.swapee.wc.back.BoundOffersAggregatorScreenAT filter:!ControllerPlugin~props e29e96dbf94ae040779c1d7494b89ea1 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOffersAggregatorScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOffersAggregatorScreenAT} */
xyz.swapee.wc.back.BoundOffersAggregatorScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */