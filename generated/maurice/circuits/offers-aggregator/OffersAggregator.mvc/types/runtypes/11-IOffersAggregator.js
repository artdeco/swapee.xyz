/** @const {?} */ $xyz.swapee.wc.IOffersAggregator
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregatorEnv filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @record */
$xyz.swapee.wc.OffersAggregatorEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IOffersAggregator} */
$xyz.swapee.wc.OffersAggregatorEnv.prototype.offersAggregator
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OffersAggregatorEnv}
 */
xyz.swapee.wc.OffersAggregatorEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator.Initialese filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer.Initialese}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Initialese}
 */
$xyz.swapee.wc.IOffersAggregator.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregator.Initialese} */
xyz.swapee.wc.IOffersAggregator.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregatorFields filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregator.Pinout} */
$xyz.swapee.wc.IOffersAggregatorFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorFields}
 */
xyz.swapee.wc.IOffersAggregatorFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregatorCaster filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregator} */
$xyz.swapee.wc.IOffersAggregatorCaster.prototype.asIOffersAggregator
/** @type {!xyz.swapee.wc.BoundOffersAggregator} */
$xyz.swapee.wc.IOffersAggregatorCaster.prototype.superOffersAggregator
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorCaster}
 */
xyz.swapee.wc.IOffersAggregatorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCaster}
 * @extends {xyz.swapee.wc.IOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.IOffersAggregatorController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorLand>}
 */
$xyz.swapee.wc.IOffersAggregator = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregator}
 */
xyz.swapee.wc.IOffersAggregator

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregator filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregator}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregator.Initialese>}
 */
$xyz.swapee.wc.OffersAggregator = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.OffersAggregator
/** @type {function(new: xyz.swapee.wc.IOffersAggregator, ...!xyz.swapee.wc.IOffersAggregator.Initialese)} */
xyz.swapee.wc.OffersAggregator.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.OffersAggregator.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.AbstractOffersAggregator filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregator}
 */
$xyz.swapee.wc.AbstractOffersAggregator = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregator.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregator)} */
xyz.swapee.wc.AbstractOffersAggregator.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregator.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregator|typeof xyz.swapee.wc.OffersAggregator)|(!xyz.swapee.wc.IOffersAggregatorProcessor|typeof xyz.swapee.wc.OffersAggregatorProcessor)|(!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregator}
 */
xyz.swapee.wc.AbstractOffersAggregator.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregatorConstructor filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregator, ...!xyz.swapee.wc.IOffersAggregator.Initialese)} */
xyz.swapee.wc.OffersAggregatorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator.MVCOptions filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @record */
$xyz.swapee.wc.IOffersAggregator.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IOffersAggregator.Pinout)|undefined} */
$xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IOffersAggregator.Pinout)|undefined} */
$xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IOffersAggregator.Pinout} */
$xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.OffersAggregatorMemory)|undefined} */
$xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.OffersAggregatorClasses)|undefined} */
$xyz.swapee.wc.IOffersAggregator.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IOffersAggregator.MVCOptions} */
xyz.swapee.wc.IOffersAggregator.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.RecordIOffersAggregator filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregator

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.BoundIOffersAggregator filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregator}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCaster}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorProcessor}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorLand>}
 */
$xyz.swapee.wc.BoundIOffersAggregator = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregator} */
xyz.swapee.wc.BoundIOffersAggregator

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.BoundOffersAggregator filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregator}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregator = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregator} */
xyz.swapee.wc.BoundOffersAggregator

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregator.Pinout filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Inputs}
 */
$xyz.swapee.wc.IOffersAggregator.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregator.Pinout} */
xyz.swapee.wc.IOffersAggregator.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.IOffersAggregatorBuffer filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
$xyz.swapee.wc.IOffersAggregatorBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorBuffer}
 */
xyz.swapee.wc.IOffersAggregatorBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/11-IOffersAggregator.xml} xyz.swapee.wc.OffersAggregatorBuffer filter:!ControllerPlugin~props a2fe97d3ec4abe94d04b679b42346234 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorBuffer}
 */
$xyz.swapee.wc.OffersAggregatorBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorBuffer}
 */
xyz.swapee.wc.OffersAggregatorBuffer
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorBuffer)} */
xyz.swapee.wc.OffersAggregatorBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */