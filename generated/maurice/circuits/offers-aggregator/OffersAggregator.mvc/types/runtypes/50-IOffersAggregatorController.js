/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorController
/** @const {?} */ xyz.swapee.wc.IOffersAggregatorController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.Initialese filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IOffersAggregatorController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorController.Initialese} */
xyz.swapee.wc.IOffersAggregatorController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorControllerFields filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorControllerFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregatorController.Inputs} */
$xyz.swapee.wc.IOffersAggregatorControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorControllerFields}
 */
xyz.swapee.wc.IOffersAggregatorControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorControllerCaster filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorController} */
$xyz.swapee.wc.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorController
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorProcessor} */
$xyz.swapee.wc.IOffersAggregatorControllerCaster.prototype.asIOffersAggregatorProcessor
/** @type {!xyz.swapee.wc.BoundOffersAggregatorController} */
$xyz.swapee.wc.IOffersAggregatorControllerCaster.prototype.superOffersAggregatorController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorControllerCaster}
 */
xyz.swapee.wc.IOffersAggregatorControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
$xyz.swapee.wc.IOffersAggregatorController = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.resetPort = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedMin = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFixedMax = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFixedMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatingOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatingError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatMin = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatMin = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangellyFloatMax = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangellyFloatMax = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFixedOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFixedError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFloatingOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setLetsExchangeFloatingError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetLetsExchangeFloatingError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFixedOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFixedOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFixedError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFixedError = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFloatingOffer = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFloatingOffer = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.prototype.setChangeNowFloatingError = function(val) {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.unsetChangeNowFloatingError = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.loadLetsExchangeFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.loadLetsExchangeFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangeNowFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.IOffersAggregatorController.prototype.loadChangeNowFixedOffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorController}
 */
xyz.swapee.wc.IOffersAggregatorController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.OffersAggregatorController filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorController.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.OffersAggregatorController
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorController, ...!xyz.swapee.wc.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.OffersAggregatorController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.OffersAggregatorController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.AbstractOffersAggregatorController filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorController}
 */
$xyz.swapee.wc.AbstractOffersAggregatorController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorController)} */
xyz.swapee.wc.AbstractOffersAggregatorController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorController|typeof xyz.swapee.wc.OffersAggregatorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorController}
 */
xyz.swapee.wc.AbstractOffersAggregatorController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.OffersAggregatorControllerConstructor filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorController, ...!xyz.swapee.wc.IOffersAggregatorController.Initialese)} */
xyz.swapee.wc.OffersAggregatorControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.RecordIOffersAggregatorController filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/** @typedef {{ resetPort: xyz.swapee.wc.IOffersAggregatorController.resetPort, setChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedOffer, unsetChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedOffer, setChangellyFixedError: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedError, unsetChangellyFixedError: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedError, setChangellyFixedMin: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMin, unsetChangellyFixedMin: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMin, setChangellyFixedMax: xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMax, unsetChangellyFixedMax: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMax, setChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingOffer, unsetChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingOffer, setChangellyFloatingError: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingError, unsetChangellyFloatingError: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingError, setChangellyFloatMin: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMin, unsetChangellyFloatMin: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMin, setChangellyFloatMax: xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMax, unsetChangellyFloatMax: xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMax, setLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedOffer, unsetLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedOffer, setLetsExchangeFixedError: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedError, unsetLetsExchangeFixedError: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedError, setLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingOffer, unsetLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingOffer, setLetsExchangeFloatingError: xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingError, unsetLetsExchangeFloatingError: xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingError, setChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedOffer, unsetChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedOffer, setChangeNowFixedError: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedError, unsetChangeNowFixedError: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedError, setChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingOffer, unsetChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingOffer, setChangeNowFloatingError: xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingError, unsetChangeNowFloatingError: xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingError, loadChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangellyFixedOffer, loadLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFloatingOffer, loadLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFixedOffer, loadChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFloatingOffer, loadChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFixedOffer }} */
xyz.swapee.wc.RecordIOffersAggregatorController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.BoundIOffersAggregatorController filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.IOffersAggregatorController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IOffersAggregatorController.Inputs, !xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IOffersAggregatorController.Inputs>}
 */
$xyz.swapee.wc.BoundIOffersAggregatorController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorController} */
xyz.swapee.wc.BoundIOffersAggregatorController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.BoundOffersAggregatorController filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorController = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorController} */
xyz.swapee.wc.BoundOffersAggregatorController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.resetPort filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__resetPort} */
xyz.swapee.wc.IOffersAggregatorController.__resetPort

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMin filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedMin
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMin filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedMin
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMax filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFixedMax
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFixedMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMax filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFixedMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFixedMax
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFixedMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatingError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatingError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMin filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMin = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatMin
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMin filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMin = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMin
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatMin
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMin

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMax filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMax = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangellyFloatMax
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorController.__setChangellyFloatMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMax filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMax = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangellyFloatMax
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangellyFloatMax
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangellyFloatMax

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFixedError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFixedError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setLetsExchangeFloatingError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__setLetsExchangeFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetLetsExchangeFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetLetsExchangeFloatingError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetLetsExchangeFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFixedError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFixedError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFixedError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFixedError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingOffer = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, number): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingError = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.IOffersAggregatorController.setChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController, string): void} */
xyz.swapee.wc.IOffersAggregatorController._setChangeNowFloatingError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__setChangeNowFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingError filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingError = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.unsetChangeNowFloatingError
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._unsetChangeNowFloatingError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorController.__unsetChangeNowFloatingError

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangellyFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangellyFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFloatingOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFixedOffer filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOffersAggregatorController.loadChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorController): void} */
xyz.swapee.wc.IOffersAggregatorController._loadChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorController.__loadChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorController,$xyz.swapee.wc.IOffersAggregatorController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.Inputs filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorPort.Inputs}
 */
$xyz.swapee.wc.IOffersAggregatorController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorController.Inputs} */
xyz.swapee.wc.IOffersAggregatorController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/50-IOffersAggregatorController.xml} xyz.swapee.wc.IOffersAggregatorController.WeakInputs filter:!ControllerPlugin~props 06756f3412e1074e51704103bd26bc6b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorPort.WeakInputs}
 */
$xyz.swapee.wc.IOffersAggregatorController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorController.WeakInputs} */
xyz.swapee.wc.IOffersAggregatorController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorController
/* @typal-end */