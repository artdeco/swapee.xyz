/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorDesigner
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorDesigner = function() {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersAggregatorDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersAggregatorDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IOffersAggregatorDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IOffersAggregatorDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersAggregatorDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorDesigner}
 */
xyz.swapee.wc.IOffersAggregatorDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.OffersAggregatorDesigner filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorDesigner}
 */
$xyz.swapee.wc.OffersAggregatorDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorDesigner}
 */
xyz.swapee.wc.OffersAggregatorDesigner
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorDesigner)} */
xyz.swapee.wc.OffersAggregatorDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh.prototype.ExchangeIntent
/** @type {typeof xyz.swapee.wc.IOffersAggregatorController} */
$xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh.prototype.OffersAggregator
/** @typedef {$xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh} */
xyz.swapee.wc.IOffersAggregatorDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh.prototype.ExchangeIntent
/** @type {typeof xyz.swapee.wc.IOffersAggregatorController} */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh.prototype.OffersAggregator
/** @type {typeof xyz.swapee.wc.IOffersAggregatorController} */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/170-IOffersAggregatorDesigner.xml} xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool filter:!ControllerPlugin~props 3fd876804ad6788b873fd10e0e077cb8 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.OffersAggregatorMemory} */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool.prototype.OffersAggregator
/** @type {!xyz.swapee.wc.OffersAggregatorMemory} */
$xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool} */
xyz.swapee.wc.IOffersAggregatorDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorDesigner.relay
/* @typal-end */