/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorElement
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorElement.build
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorElement.short
/** @const {?} */ xyz.swapee.wc.IOffersAggregatorElement
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.Initialese filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @record
 * @extends {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.IExchangeIntent>}
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElement.Initialese} */
xyz.swapee.wc.IOffersAggregatorElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElementFields filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorElementFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} */
$xyz.swapee.wc.IOffersAggregatorElementFields.prototype.inputs
/** @type {!Object<string, !Object<string, number>>} */
$xyz.swapee.wc.IOffersAggregatorElementFields.prototype.buildees
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorElementFields}
 */
xyz.swapee.wc.IOffersAggregatorElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElementCaster filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorElement} */
$xyz.swapee.wc.IOffersAggregatorElementCaster.prototype.asIOffersAggregatorElement
/** @type {!xyz.swapee.wc.BoundOffersAggregatorElement} */
$xyz.swapee.wc.IOffersAggregatorElementCaster.prototype.superOffersAggregatorElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorElementCaster}
 */
xyz.swapee.wc.IOffersAggregatorElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorElement.Inputs, !xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {guest.maurice.IMilleu<!xyz.swapee.wc.IExchangeIntent>}
 */
$xyz.swapee.wc.IOffersAggregatorElement = function() {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IOffersAggregatorElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersAggregatorElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Instances} instances
 * @return {?}
 */
$xyz.swapee.wc.IOffersAggregatorElement.prototype.build = function(cores, instances) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IOffersAggregatorElement.prototype.buildExchangeIntent = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Cores} cores
 * @return {?}
 */
$xyz.swapee.wc.IOffersAggregatorElement.prototype.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersAggregatorElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [model]
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.IOffersAggregatorElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorElement}
 */
xyz.swapee.wc.IOffersAggregatorElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.OffersAggregatorElement filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElement.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorElement.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorElement}
 */
xyz.swapee.wc.OffersAggregatorElement
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorElement, ...!xyz.swapee.wc.IOffersAggregatorElement.Initialese)} */
xyz.swapee.wc.OffersAggregatorElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorElement}
 */
xyz.swapee.wc.OffersAggregatorElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.AbstractOffersAggregatorElement filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElement.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorElement}
 */
$xyz.swapee.wc.AbstractOffersAggregatorElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorElement}
 */
xyz.swapee.wc.AbstractOffersAggregatorElement
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorElement)} */
xyz.swapee.wc.AbstractOffersAggregatorElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorElement|typeof xyz.swapee.wc.OffersAggregatorElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorElement}
 */
xyz.swapee.wc.AbstractOffersAggregatorElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorElement}
 */
xyz.swapee.wc.AbstractOffersAggregatorElement.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorElement|typeof xyz.swapee.wc.OffersAggregatorElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorElement}
 */
xyz.swapee.wc.AbstractOffersAggregatorElement.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorElement|typeof xyz.swapee.wc.OffersAggregatorElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorElement}
 */
xyz.swapee.wc.AbstractOffersAggregatorElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.OffersAggregatorElementConstructor filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorElement, ...!xyz.swapee.wc.IOffersAggregatorElement.Initialese)} */
xyz.swapee.wc.OffersAggregatorElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.RecordIOffersAggregatorElement filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @typedef {{ solder: xyz.swapee.wc.IOffersAggregatorElement.solder, render: xyz.swapee.wc.IOffersAggregatorElement.render, build: xyz.swapee.wc.IOffersAggregatorElement.build, buildExchangeIntent: xyz.swapee.wc.IOffersAggregatorElement.buildExchangeIntent, short: xyz.swapee.wc.IOffersAggregatorElement.short, server: xyz.swapee.wc.IOffersAggregatorElement.server, inducer: xyz.swapee.wc.IOffersAggregatorElement.inducer }} */
xyz.swapee.wc.RecordIOffersAggregatorElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.BoundIOffersAggregatorElement filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorElementFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.IOffersAggregatorElement.Inputs, !xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 * @extends {guest.maurice.BoundIMilleu<!xyz.swapee.wc.IExchangeIntent>}
 */
$xyz.swapee.wc.BoundIOffersAggregatorElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorElement} */
xyz.swapee.wc.BoundIOffersAggregatorElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.BoundOffersAggregatorElement filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorElement} */
xyz.swapee.wc.BoundOffersAggregatorElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.solder filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IOffersAggregatorElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.IOffersAggregatorElement}
 */
$xyz.swapee.wc.IOffersAggregatorElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.solder} */
xyz.swapee.wc.IOffersAggregatorElement.solder
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement._solder} */
xyz.swapee.wc.IOffersAggregatorElement._solder
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.__solder} */
xyz.swapee.wc.IOffersAggregatorElement.__solder

// nss:xyz.swapee.wc.IOffersAggregatorElement,$xyz.swapee.wc.IOffersAggregatorElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.render filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersAggregatorElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.OffersAggregatorMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IOffersAggregatorElement.render
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorElement, !xyz.swapee.wc.OffersAggregatorMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IOffersAggregatorElement._render
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.__render} */
xyz.swapee.wc.IOffersAggregatorElement.__render

// nss:xyz.swapee.wc.IOffersAggregatorElement,$xyz.swapee.wc.IOffersAggregatorElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.build filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Instances} instances
 */
$xyz.swapee.wc.IOffersAggregatorElement.build = function(cores, instances) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Instances} instances
 * @this {xyz.swapee.wc.IOffersAggregatorElement}
 */
$xyz.swapee.wc.IOffersAggregatorElement._build = function(cores, instances) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.build.Instances} instances
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorElement.__build = function(cores, instances) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.build} */
xyz.swapee.wc.IOffersAggregatorElement.build
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement._build} */
xyz.swapee.wc.IOffersAggregatorElement._build
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.__build} */
xyz.swapee.wc.IOffersAggregatorElement.__build

// nss:xyz.swapee.wc.IOffersAggregatorElement,$xyz.swapee.wc.IOffersAggregatorElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.buildExchangeIntent filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IOffersAggregatorElement.__buildExchangeIntent = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IOffersAggregatorElement.buildExchangeIntent
/** @typedef {function(this: xyz.swapee.wc.IOffersAggregatorElement, !Object, !Object)} */
xyz.swapee.wc.IOffersAggregatorElement._buildExchangeIntent
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.__buildExchangeIntent} */
xyz.swapee.wc.IOffersAggregatorElement.__buildExchangeIntent

// nss:xyz.swapee.wc.IOffersAggregatorElement,$xyz.swapee.wc.IOffersAggregatorElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.short filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Cores} cores
 */
$xyz.swapee.wc.IOffersAggregatorElement.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Cores} cores
 * @this {xyz.swapee.wc.IOffersAggregatorElement}
 */
$xyz.swapee.wc.IOffersAggregatorElement._short = function(model, ports, cores) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} model
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.short.Cores} cores
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorElement.__short = function(model, ports, cores) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.short} */
xyz.swapee.wc.IOffersAggregatorElement.short
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement._short} */
xyz.swapee.wc.IOffersAggregatorElement._short
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.__short} */
xyz.swapee.wc.IOffersAggregatorElement.__short

// nss:xyz.swapee.wc.IOffersAggregatorElement,$xyz.swapee.wc.IOffersAggregatorElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.server filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOffersAggregatorElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.IOffersAggregatorElement}
 */
$xyz.swapee.wc.IOffersAggregatorElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} memory
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.server} */
xyz.swapee.wc.IOffersAggregatorElement.server
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement._server} */
xyz.swapee.wc.IOffersAggregatorElement._server
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.__server} */
xyz.swapee.wc.IOffersAggregatorElement.__server

// nss:xyz.swapee.wc.IOffersAggregatorElement,$xyz.swapee.wc.IOffersAggregatorElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.inducer filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [model]
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} [port]
 */
$xyz.swapee.wc.IOffersAggregatorElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [model]
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} [port]
 * @this {xyz.swapee.wc.IOffersAggregatorElement}
 */
$xyz.swapee.wc.IOffersAggregatorElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OffersAggregatorMemory} [model]
 * @param {!xyz.swapee.wc.IOffersAggregatorElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.inducer} */
xyz.swapee.wc.IOffersAggregatorElement.inducer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement._inducer} */
xyz.swapee.wc.IOffersAggregatorElement._inducer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorElement.__inducer} */
xyz.swapee.wc.IOffersAggregatorElement.__inducer

// nss:xyz.swapee.wc.IOffersAggregatorElement,$xyz.swapee.wc.IOffersAggregatorElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.Inputs filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorPort.Inputs}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplay.Queries}
 * @extends {xyz.swapee.wc.IOffersAggregatorController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.Inputs}
 */
$xyz.swapee.wc.IOffersAggregatorElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElement.Inputs} */
xyz.swapee.wc.IOffersAggregatorElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.build.Cores filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElement.build.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorElement.build.Cores.prototype.ExchangeIntent
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElement.build.Cores} */
xyz.swapee.wc.IOffersAggregatorElement.build.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.build.Instances filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElement.build.Instances = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorElement.build.Instances.prototype.ExchangeIntent
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElement.build.Instances} */
xyz.swapee.wc.IOffersAggregatorElement.build.Instances

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.short.Ports filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElement.short.Ports = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorElement.short.Ports.prototype.ExchangeIntent
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElement.short.Ports} */
xyz.swapee.wc.IOffersAggregatorElement.short.Ports

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElement.short
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/130-IOffersAggregatorElement.xml} xyz.swapee.wc.IOffersAggregatorElement.short.Cores filter:!ControllerPlugin~props d83a15abe645f35f169d218468195f09 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElement.short.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorElement.short.Cores.prototype.ExchangeIntent
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElement.short.Cores} */
xyz.swapee.wc.IOffersAggregatorElement.short.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElement.short
/* @typal-end */