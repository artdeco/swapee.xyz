/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorGPU.Initialese} */
xyz.swapee.wc.IOffersAggregatorGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IOffersAggregatorGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorGPUFields}
 */
xyz.swapee.wc.IOffersAggregatorGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorGPU} */
$xyz.swapee.wc.IOffersAggregatorGPUCaster.prototype.asIOffersAggregatorGPU
/** @type {!xyz.swapee.wc.BoundOffersAggregatorGPU} */
$xyz.swapee.wc.IOffersAggregatorGPUCaster.prototype.superOffersAggregatorGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorGPUCaster}
 */
xyz.swapee.wc.IOffersAggregatorGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.IOffersAggregatorGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!OffersAggregatorMemory,>}
 * @extends {xyz.swapee.wc.back.IOffersAggregatorDisplay}
 */
$xyz.swapee.wc.IOffersAggregatorGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorGPU}
 */
xyz.swapee.wc.IOffersAggregatorGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.OffersAggregatorGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorGPU.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.OffersAggregatorGPU
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorGPU, ...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese)} */
xyz.swapee.wc.OffersAggregatorGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.OffersAggregatorGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.AbstractOffersAggregatorGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorGPU}
 */
$xyz.swapee.wc.AbstractOffersAggregatorGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorGPU)} */
xyz.swapee.wc.AbstractOffersAggregatorGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorGPU|typeof xyz.swapee.wc.OffersAggregatorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersAggregatorDisplay|typeof xyz.swapee.wc.back.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorGPU}
 */
xyz.swapee.wc.AbstractOffersAggregatorGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.OffersAggregatorGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorGPU, ...!xyz.swapee.wc.IOffersAggregatorGPU.Initialese)} */
xyz.swapee.wc.OffersAggregatorGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.RecordIOffersAggregatorGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.BoundIOffersAggregatorGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!OffersAggregatorMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIOffersAggregatorDisplay}
 */
$xyz.swapee.wc.BoundIOffersAggregatorGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorGPU} */
xyz.swapee.wc.BoundIOffersAggregatorGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/80-IOffersAggregatorGPU.xml} xyz.swapee.wc.BoundOffersAggregatorGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorGPU} */
xyz.swapee.wc.BoundOffersAggregatorGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */