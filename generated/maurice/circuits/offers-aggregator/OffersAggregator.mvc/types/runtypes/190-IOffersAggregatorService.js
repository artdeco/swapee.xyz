/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorService
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer
/** @const {?} */ xyz.swapee.wc.IOffersAggregatorService
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.Initialese filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 * @extends {io.letsexchange.ULetsExchange.Initialese}
 * @extends {io.changenow.UChangeNow.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorService.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.Initialese} */
xyz.swapee.wc.IOffersAggregatorService.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorServiceCaster filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorServiceCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorService} */
$xyz.swapee.wc.IOffersAggregatorServiceCaster.prototype.asIOffersAggregatorService
/** @type {!xyz.swapee.wc.BoundOffersAggregatorService} */
$xyz.swapee.wc.IOffersAggregatorServiceCaster.prototype.superOffersAggregatorService
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorServiceCaster}
 */
xyz.swapee.wc.IOffersAggregatorServiceCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorServiceCaster}
 * @extends {com.changelly.UChangelly}
 * @extends {io.letsexchange.ULetsExchange}
 * @extends {io.changenow.UChangeNow}
 */
$xyz.swapee.wc.IOffersAggregatorService = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangellyFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangellyFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.prototype.filterLetsExchangeFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.prototype.filterLetsExchangeFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangeNowFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.prototype.filterChangeNowFixedOffer = function(form) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorService}
 */
xyz.swapee.wc.IOffersAggregatorService

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.OffersAggregatorService filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorService}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorService.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorService = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.OffersAggregatorService
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorService)} */
xyz.swapee.wc.OffersAggregatorService.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.OffersAggregatorService.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.AbstractOffersAggregatorService filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorService}
 */
$xyz.swapee.wc.AbstractOffersAggregatorService = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorService)} */
xyz.swapee.wc.AbstractOffersAggregatorService.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorService|typeof xyz.swapee.wc.OffersAggregatorService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorService.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorService|typeof xyz.swapee.wc.OffersAggregatorService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorService|typeof xyz.swapee.wc.OffersAggregatorService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorService}
 */
xyz.swapee.wc.AbstractOffersAggregatorService.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.RecordIOffersAggregatorService filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/** @typedef {{ filterChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer, filterChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer, filterLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer, filterLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer, filterChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer, filterChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer }} */
xyz.swapee.wc.RecordIOffersAggregatorService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.BoundIOffersAggregatorService filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorService}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorServiceCaster}
 * @extends {com.changelly.BoundUChangelly}
 * @extends {io.letsexchange.BoundULetsExchange}
 * @extends {io.changenow.BoundUChangeNow}
 */
$xyz.swapee.wc.BoundIOffersAggregatorService = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorService} */
xyz.swapee.wc.BoundIOffersAggregatorService

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.BoundOffersAggregatorService filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorService}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorService = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorService} */
xyz.swapee.wc.BoundOffersAggregatorService

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>}
 * @this {xyz.swapee.wc.IOffersAggregatorService}
 */
$xyz.swapee.wc.IOffersAggregatorService._filterChangellyFloatingOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFloatingOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService._filterChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService._filterChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>}
 * @this {xyz.swapee.wc.IOffersAggregatorService}
 */
$xyz.swapee.wc.IOffersAggregatorService._filterChangellyFixedOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFixedOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService._filterChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService._filterChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>}
 * @this {xyz.swapee.wc.IOffersAggregatorService}
 */
$xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFloatingOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFloatingOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>}
 * @this {xyz.swapee.wc.IOffersAggregatorService}
 */
$xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFixedOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFixedOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>}
 * @this {xyz.swapee.wc.IOffersAggregatorService}
 */
$xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFloatingOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFloatingOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>}
 * @this {xyz.swapee.wc.IOffersAggregatorService}
 */
$xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFixedOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFixedOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorService.__filterChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorService,$xyz.swapee.wc.IOffersAggregatorService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterLetsExchangeFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/190-IOffersAggregatorService.xml} xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return filter:!ControllerPlugin~props 7204636b48f9a9a4e50cd728849db3b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorService.filterChangeNowFixedOffer
/* @typal-end */