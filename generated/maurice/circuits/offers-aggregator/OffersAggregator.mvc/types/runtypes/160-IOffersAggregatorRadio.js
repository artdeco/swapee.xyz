/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorRadio
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer
/** @const {?} */ xyz.swapee.wc.IOffersAggregatorRadio
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.Initialese filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorRadio.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.Initialese} */
xyz.swapee.wc.IOffersAggregatorRadio.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadioCaster filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorRadioCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorRadio} */
$xyz.swapee.wc.IOffersAggregatorRadioCaster.prototype.asIOffersAggregatorRadio
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorComputer} */
$xyz.swapee.wc.IOffersAggregatorRadioCaster.prototype.asIOffersAggregatorComputer
/** @type {!xyz.swapee.wc.BoundOffersAggregatorRadio} */
$xyz.swapee.wc.IOffersAggregatorRadioCaster.prototype.superOffersAggregatorRadio
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorRadioCaster}
 */
xyz.swapee.wc.IOffersAggregatorRadioCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorRadioCaster}
 */
$xyz.swapee.wc.IOffersAggregatorRadio = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangeNowFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.prototype.adaptLoadChangeNowFixedOffer = function(form, changes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorRadio}
 */
xyz.swapee.wc.IOffersAggregatorRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.OffersAggregatorRadio filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorRadio}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorRadio.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorRadio = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.OffersAggregatorRadio
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorRadio)} */
xyz.swapee.wc.OffersAggregatorRadio.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.OffersAggregatorRadio.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.AbstractOffersAggregatorRadio filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorRadio}
 */
$xyz.swapee.wc.AbstractOffersAggregatorRadio = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorRadio)} */
xyz.swapee.wc.AbstractOffersAggregatorRadio.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorRadio|typeof xyz.swapee.wc.OffersAggregatorRadio)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.__extend
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorRadio|typeof xyz.swapee.wc.OffersAggregatorRadio)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.continues
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorRadio|typeof xyz.swapee.wc.OffersAggregatorRadio)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorRadio}
 */
xyz.swapee.wc.AbstractOffersAggregatorRadio.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.RecordIOffersAggregatorRadio filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/** @typedef {{ adaptLoadChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer, adaptLoadChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer, adaptLoadLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer, adaptLoadLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer, adaptLoadChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer, adaptLoadChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer }} */
xyz.swapee.wc.RecordIOffersAggregatorRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.BoundIOffersAggregatorRadio filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorRadio}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorRadioCaster}
 */
$xyz.swapee.wc.BoundIOffersAggregatorRadio = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorRadio} */
xyz.swapee.wc.BoundIOffersAggregatorRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.BoundOffersAggregatorRadio filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorRadio}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorRadio = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorRadio} */
xyz.swapee.wc.BoundOffersAggregatorRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorRadio}
 */
$xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorRadio}
 */
$xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorRadio}
 */
$xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorRadio}
 */
$xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorRadio}
 */
$xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorRadio}
 */
$xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form} form
 * @param {IOffersAggregatorComputer.adaptLoadChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio._adaptLoadChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorRadio.__adaptLoadChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorRadio,$xyz.swapee.wc.IOffersAggregatorRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/160-IOffersAggregatorRadio.xml} xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return filter:!ControllerPlugin~props cb5cf071425727fa496161d7fff05920 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer
/* @typal-end */