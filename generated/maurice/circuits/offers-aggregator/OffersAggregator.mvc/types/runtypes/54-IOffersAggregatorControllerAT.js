/** @const {?} */ $xyz.swapee.wc.front.IOffersAggregatorControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} */
xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOffersAggregatorControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.IOffersAggregatorControllerATCaster filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/** @interface */
$xyz.swapee.wc.front.IOffersAggregatorControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT} */
$xyz.swapee.wc.front.IOffersAggregatorControllerATCaster.prototype.asIOffersAggregatorControllerAT
/** @type {!xyz.swapee.wc.front.BoundOffersAggregatorControllerAT} */
$xyz.swapee.wc.front.IOffersAggregatorControllerATCaster.prototype.superOffersAggregatorControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersAggregatorControllerATCaster}
 */
xyz.swapee.wc.front.IOffersAggregatorControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.IOffersAggregatorControllerAT filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IOffersAggregatorControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.IOffersAggregatorControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.OffersAggregatorControllerAT filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IOffersAggregatorControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.OffersAggregatorControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.OffersAggregatorControllerAT
/** @type {function(new: xyz.swapee.wc.front.IOffersAggregatorControllerAT, ...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.OffersAggregatorControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
$xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT)} */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOffersAggregatorControllerAT|typeof xyz.swapee.wc.front.OffersAggregatorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OffersAggregatorControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.OffersAggregatorControllerATConstructor filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/** @typedef {function(new: xyz.swapee.wc.front.IOffersAggregatorControllerAT, ...!xyz.swapee.wc.front.IOffersAggregatorControllerAT.Initialese)} */
xyz.swapee.wc.front.OffersAggregatorControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.RecordIOffersAggregatorControllerAT filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOffersAggregatorControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOffersAggregatorControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT} */
xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/54-IOffersAggregatorControllerAT.xml} xyz.swapee.wc.front.BoundOffersAggregatorControllerAT filter:!ControllerPlugin~props 2ec901484ad8ce918a081c6236108f97 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOffersAggregatorControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOffersAggregatorControllerAT} */
xyz.swapee.wc.front.BoundOffersAggregatorControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */