/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersAggregatorMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.estimatedOut
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.bestOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.worstOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFixedError
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFloatingError
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.changellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.host
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.amountIn
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.currencyIn
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.currencyOut
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.exchangesLoaded
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.allExchangesLoaded
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryPQs.prototype.exchangesTotal
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersAggregatorMemoryPQs}
 */
xyz.swapee.wc.OffersAggregatorMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersAggregatorMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.aa93d
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.d2a18
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.fc2c8
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.hb5f4
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.fb11e
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.b8c0d
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.ca73d
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.g7b3d
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.i8da7
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.f3088
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.j8dbb
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.b4c94
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.ha98d
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.i062a
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorMemoryQPs}
 */
xyz.swapee.wc.OffersAggregatorMemoryQPs
/** @type {function(new: xyz.swapee.wc.OffersAggregatorMemoryQPs)} */
xyz.swapee.wc.OffersAggregatorMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.OffersAggregatorMemoryPQs}
 */
$xyz.swapee.wc.OffersAggregatorInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersAggregatorInputsPQs}
 */
xyz.swapee.wc.OffersAggregatorInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.OffersAggregatorInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorInputsQPs}
 */
xyz.swapee.wc.OffersAggregatorInputsQPs
/** @type {function(new: xyz.swapee.wc.OffersAggregatorInputsQPs)} */
xyz.swapee.wc.OffersAggregatorInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorCachePQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersAggregatorCachePQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.isAggregating
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadingChangellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.hasMoreChangellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadChangellyFloatingOfferError
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadingChangellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.hasMoreChangellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadChangellyFixedOfferError
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadingChangenowOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.hasMoreChangenowOffer
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCachePQs.prototype.loadChangenowOfferError
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersAggregatorCachePQs}
 */
xyz.swapee.wc.OffersAggregatorCachePQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorCacheQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersAggregatorCacheQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.h7233
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.c029f
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.cfde8
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.c7da2
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.h4365
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.b518b
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.f920c
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.a47e0
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.f0a43
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.i9dbe
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorCacheQPs}
 */
xyz.swapee.wc.OffersAggregatorCacheQPs
/** @type {function(new: xyz.swapee.wc.OffersAggregatorCacheQPs)} */
xyz.swapee.wc.OffersAggregatorCacheQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorQueriesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersAggregatorQueriesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorQueriesPQs.prototype.exchangeIntentSel
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersAggregatorQueriesPQs}
 */
xyz.swapee.wc.OffersAggregatorQueriesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorQueriesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersAggregatorQueriesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorQueriesQPs.prototype.b3da4
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorQueriesQPs}
 */
xyz.swapee.wc.OffersAggregatorQueriesQPs
/** @type {function(new: xyz.swapee.wc.OffersAggregatorQueriesQPs)} */
xyz.swapee.wc.OffersAggregatorQueriesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OffersAggregatorVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorVdusPQs.prototype.AmountInIn
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorVdusPQs.prototype.AmountOutIn
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OffersAggregatorVdusPQs}
 */
xyz.swapee.wc.OffersAggregatorVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/110-OffersAggregatorSerDes.xml} xyz.swapee.wc.OffersAggregatorVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OffersAggregatorVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorVdusQPs.prototype.ffdf1
/** @type {string} */
$xyz.swapee.wc.OffersAggregatorVdusQPs.prototype.ffdf2
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorVdusQPs}
 */
xyz.swapee.wc.OffersAggregatorVdusQPs
/** @type {function(new: xyz.swapee.wc.OffersAggregatorVdusQPs)} */
xyz.swapee.wc.OffersAggregatorVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */