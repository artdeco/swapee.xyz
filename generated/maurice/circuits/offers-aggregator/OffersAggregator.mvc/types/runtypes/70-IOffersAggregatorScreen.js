/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.IOffersAggregatorScreen.Initialese filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.front.OffersAggregatorInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, !xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplay.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorScreen.Initialese} */
xyz.swapee.wc.IOffersAggregatorScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.IOffersAggregatorScreenCaster filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorScreen} */
$xyz.swapee.wc.IOffersAggregatorScreenCaster.prototype.asIOffersAggregatorScreen
/** @type {!xyz.swapee.wc.BoundOffersAggregatorScreen} */
$xyz.swapee.wc.IOffersAggregatorScreenCaster.prototype.superOffersAggregatorScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorScreenCaster}
 */
xyz.swapee.wc.IOffersAggregatorScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.IOffersAggregatorScreen filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.front.OffersAggregatorInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, !xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IOffersAggregatorController}
 * @extends {xyz.swapee.wc.IOffersAggregatorDisplay}
 */
$xyz.swapee.wc.IOffersAggregatorScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorScreen}
 */
xyz.swapee.wc.IOffersAggregatorScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.OffersAggregatorScreen filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorScreen.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.OffersAggregatorScreen
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorScreen, ...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese)} */
xyz.swapee.wc.OffersAggregatorScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.OffersAggregatorScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.AbstractOffersAggregatorScreen filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorScreen}
 */
$xyz.swapee.wc.AbstractOffersAggregatorScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorScreen)} */
xyz.swapee.wc.AbstractOffersAggregatorScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorScreen|typeof xyz.swapee.wc.OffersAggregatorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersAggregatorController|typeof xyz.swapee.wc.front.OffersAggregatorController)|(!xyz.swapee.wc.IOffersAggregatorDisplay|typeof xyz.swapee.wc.OffersAggregatorDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorScreen}
 */
xyz.swapee.wc.AbstractOffersAggregatorScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.OffersAggregatorScreenConstructor filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorScreen, ...!xyz.swapee.wc.IOffersAggregatorScreen.Initialese)} */
xyz.swapee.wc.OffersAggregatorScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.RecordIOffersAggregatorScreen filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.BoundIOffersAggregatorScreen filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.OffersAggregatorMemory, !xyz.swapee.wc.front.OffersAggregatorInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersAggregatorDisplay.Settings, !xyz.swapee.wc.IOffersAggregatorDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIOffersAggregatorController}
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorDisplay}
 */
$xyz.swapee.wc.BoundIOffersAggregatorScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorScreen} */
xyz.swapee.wc.BoundIOffersAggregatorScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/70-IOffersAggregatorScreen.xml} xyz.swapee.wc.BoundOffersAggregatorScreen filter:!ControllerPlugin~props 16fc8a249fcdd15301c7f93368105da4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorScreen} */
xyz.swapee.wc.BoundOffersAggregatorScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */