/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/102-OffersAggregatorInputs.xml} xyz.swapee.wc.front.OffersAggregatorInputs filter:!ControllerPlugin~props 9b6bba0df368424f1ffc16bc6c5a935f */
/** @record */
$xyz.swapee.wc.front.OffersAggregatorInputs = __$te_Mixin()
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedOffer
/** @type {string|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedError
/** @type {number|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedMin
/** @type {number|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFixedMax
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatingOffer
/** @type {string|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatingError
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatMin
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changellyFloatMax
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFixedOffer
/** @type {string|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFixedError
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFloatingOffer
/** @type {string|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.letsExchangeFloatingError
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFixedOffer
/** @type {string|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFixedError
/** @type {(?number)|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFloatingOffer
/** @type {string|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.changeNowFloatingError
/** @type {number|undefined} */
$xyz.swapee.wc.front.OffersAggregatorInputs.prototype.exchangesTotal
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.OffersAggregatorInputs}
 */
xyz.swapee.wc.front.OffersAggregatorInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */