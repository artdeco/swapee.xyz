/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.Initialese filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.Initialese} */
xyz.swapee.wc.IOffersAggregatorComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputerCaster filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorComputer} */
$xyz.swapee.wc.IOffersAggregatorComputerCaster.prototype.asIOffersAggregatorComputer
/** @type {!xyz.swapee.wc.BoundOffersAggregatorComputer} */
$xyz.swapee.wc.IOffersAggregatorComputerCaster.prototype.superOffersAggregatorComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorComputerCaster}
 */
xyz.swapee.wc.IOffersAggregatorComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 */
$xyz.swapee.wc.IOffersAggregatorComputer = function() {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangeNowFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptChangeNowFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMinAmount = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMinError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMaxAmount = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptMaxError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptEstimatedOut = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptLoadingEstimate = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptBestAndWorstOffers = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptExchangesLoaded = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.adaptIsAggregating = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorMemory} mem
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.prototype.compute = function(mem, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorComputer}
 */
xyz.swapee.wc.IOffersAggregatorComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.OffersAggregatorComputer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorComputer.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.OffersAggregatorComputer
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorComputer, ...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese)} */
xyz.swapee.wc.OffersAggregatorComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.OffersAggregatorComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.AbstractOffersAggregatorComputer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorComputer}
 */
$xyz.swapee.wc.AbstractOffersAggregatorComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorComputer)} */
xyz.swapee.wc.AbstractOffersAggregatorComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorComputer|typeof xyz.swapee.wc.OffersAggregatorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorComputer}
 */
xyz.swapee.wc.AbstractOffersAggregatorComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.OffersAggregatorComputerConstructor filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorComputer, ...!xyz.swapee.wc.IOffersAggregatorComputer.Initialese)} */
xyz.swapee.wc.OffersAggregatorComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.RecordIOffersAggregatorComputer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @typedef {{ adaptChangellyFloatingOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer, adaptChangellyFixedOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer, adaptLetsExchangeFloatingOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer, adaptLetsExchangeFixedOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer, adaptChangeNowFloatingOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer, adaptChangeNowFixedOffer: xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer, adaptMinAmount: xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount, adaptMinError: xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError, adaptMaxAmount: xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount, adaptMaxError: xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError, adaptEstimatedOut: xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut, adaptLoadingEstimate: xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate, adaptBestAndWorstOffers: xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers, adaptExchangesLoaded: xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded, adaptIsAggregating: xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating, compute: xyz.swapee.wc.IOffersAggregatorComputer.compute }} */
xyz.swapee.wc.RecordIOffersAggregatorComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.BoundIOffersAggregatorComputer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.OffersAggregatorMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.OffersAggregatorLand>}
 */
$xyz.swapee.wc.BoundIOffersAggregatorComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorComputer} */
xyz.swapee.wc.BoundIOffersAggregatorComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.BoundOffersAggregatorComputer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorComputer} */
xyz.swapee.wc.BoundOffersAggregatorComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangellyFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptLetsExchangeFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptLetsExchangeFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFloatingOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptChangeNowFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptChangeNowFixedOffer

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptMinAmount = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinAmount = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptMinAmount} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMinAmount
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinAmount} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinAmount

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptMinError = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinError = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptMinError} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMinError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinError} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMinError

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxAmount = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxAmount = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxAmount} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxAmount
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxAmount} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxAmount

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxError = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxError = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxError} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxError
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxError} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptMaxError

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptEstimatedOut = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptEstimatedOut = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptEstimatedOut} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptEstimatedOut
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptEstimatedOut} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptEstimatedOut

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptLoadingEstimate = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptLoadingEstimate = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptLoadingEstimate} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptLoadingEstimate
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptLoadingEstimate} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptLoadingEstimate

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptBestAndWorstOffers = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptBestAndWorstOffers = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptBestAndWorstOffers} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptBestAndWorstOffers
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptBestAndWorstOffers} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptBestAndWorstOffers

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptExchangesLoaded = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptExchangesLoaded = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptExchangesLoaded} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptExchangesLoaded
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptExchangesLoaded} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptExchangesLoaded

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._adaptIsAggregating = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} form
 * @param {xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} changes
 * @return {(undefined|xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__adaptIsAggregating = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._adaptIsAggregating} */
xyz.swapee.wc.IOffersAggregatorComputer._adaptIsAggregating
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__adaptIsAggregating} */
xyz.swapee.wc.IOffersAggregatorComputer.__adaptIsAggregating

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.compute filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @param {xyz.swapee.wc.OffersAggregatorMemory} mem
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.compute = function(mem, land) {}
/**
 * @param {xyz.swapee.wc.OffersAggregatorMemory} mem
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.compute.Land} land
 * @return {void}
 * @this {xyz.swapee.wc.IOffersAggregatorComputer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer._compute = function(mem, land) {}
/**
 * @template THIS
 * @param {xyz.swapee.wc.OffersAggregatorMemory} mem
 * @param {!xyz.swapee.wc.IOffersAggregatorComputer.compute.Land} land
 * @return {void}
 * @this {THIS}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.__compute = function(mem, land) {}
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.compute} */
xyz.swapee.wc.IOffersAggregatorComputer.compute
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer._compute} */
xyz.swapee.wc.IOffersAggregatorComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IOffersAggregatorComputer.__compute} */
xyz.swapee.wc.IOffersAggregatorComputer.__compute

// nss:xyz.swapee.wc.IOffersAggregatorComputer,$xyz.swapee.wc.IOffersAggregatorComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinAmount_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MinError}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxAmount_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.MaxError}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.EstimatedOut_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingEstimate}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.BestOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.WorstOffer}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesLoaded}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.AllExchangesLoaded}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangellyFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingLetsExchangeFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFixedOffer_Safe}
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.LoadingChangeNowFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorCore.Model.IsAggregating}
 */
$xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return} */
xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/02-IOffersAggregatorComputer.xml} xyz.swapee.wc.IOffersAggregatorComputer.compute.Land filter:!ControllerPlugin~props 349cc65a51fd93f887553b9f7b341d3b */
/** @record */
$xyz.swapee.wc.IOffersAggregatorComputer.compute.Land = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorComputer.compute.Land.prototype.ExchangeIntent
/** @typedef {$xyz.swapee.wc.IOffersAggregatorComputer.compute.Land} */
xyz.swapee.wc.IOffersAggregatorComputer.compute.Land

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorComputer.compute
/* @typal-end */