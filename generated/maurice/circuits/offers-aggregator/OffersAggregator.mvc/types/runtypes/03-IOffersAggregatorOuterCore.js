/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorOuterCore
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorPort
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorCore
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCoreFields filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregatorOuterCore.Model} */
$xyz.swapee.wc.IOffersAggregatorOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorOuterCoreFields}
 */
xyz.swapee.wc.IOffersAggregatorOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCoreCaster filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorOuterCore} */
$xyz.swapee.wc.IOffersAggregatorOuterCoreCaster.prototype.asIOffersAggregatorOuterCore
/** @type {!xyz.swapee.wc.BoundOffersAggregatorOuterCore} */
$xyz.swapee.wc.IOffersAggregatorOuterCoreCaster.prototype.superOffersAggregatorOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorOuterCoreCaster}
 */
xyz.swapee.wc.IOffersAggregatorOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreCaster}
 */
$xyz.swapee.wc.IOffersAggregatorOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorOuterCore}
 */
xyz.swapee.wc.IOffersAggregatorOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.OffersAggregatorOuterCore filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOffersAggregatorOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorOuterCore.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.OffersAggregatorOuterCore
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorOuterCore)} */
xyz.swapee.wc.OffersAggregatorOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.OffersAggregatorOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.AbstractOffersAggregatorOuterCore filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OffersAggregatorOuterCore}
 */
$xyz.swapee.wc.AbstractOffersAggregatorOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorOuterCore)} */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IOffersAggregatorOuterCore|typeof xyz.swapee.wc.OffersAggregatorOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorOuterCore}
 */
xyz.swapee.wc.AbstractOffersAggregatorOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.RecordIOffersAggregatorOuterCore filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.BoundIOffersAggregatorOuterCore filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIOffersAggregatorOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorOuterCore} */
xyz.swapee.wc.BoundIOffersAggregatorOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.BoundOffersAggregatorOuterCore filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorOuterCore} */
xyz.swapee.wc.BoundOffersAggregatorOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer.changellyFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError.changellyFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError.changellyFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin.changellyFixedMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin.changellyFixedMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax.changellyFixedMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax.changellyFixedMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer.changellyFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError.changellyFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError.changellyFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin.changellyFloatMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin.changellyFloatMin

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax.changellyFloatMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax.changellyFloatMax

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer.letsExchangeFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer.letsExchangeFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError.letsExchangeFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError.letsExchangeFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer.letsExchangeFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer.letsExchangeFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError.letsExchangeFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError.letsExchangeFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer.changeNowFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer.changeNowFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError.changeNowFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError.changeNowFixedError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer.changeNowFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer.changeNowFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError.changeNowFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {string} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError.changeNowFloatingError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal.exchangesTotal filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @typedef {number} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal.exchangesTotal

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer.prototype.changellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError.prototype.changellyFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin.prototype.changellyFixedMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax.prototype.changellyFixedMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer.prototype.changellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError.prototype.changellyFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin.prototype.changellyFloatMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax.prototype.changellyFloatMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer.prototype.letsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError.prototype.letsExchangeFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer.prototype.letsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError.prototype.letsExchangeFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer.prototype.changeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError.prototype.changeNowFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer = function() {}
/** @type {(?number)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer.prototype.changeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError.prototype.changeNowFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal.prototype.exchangesTotal
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal}
 */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer.prototype.changellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError.prototype.changellyFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin.prototype.changellyFixedMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax.prototype.changellyFixedMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer.prototype.changellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError.prototype.changellyFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin.prototype.changellyFloatMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax.prototype.changellyFloatMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer.prototype.letsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError.prototype.letsExchangeFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer.prototype.letsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError.prototype.letsExchangeFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer.prototype.changeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError.prototype.changeNowFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer.prototype.changeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError.prototype.changeNowFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal.prototype.exchangesTotal
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError}
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal}
 */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe.prototype.changellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe.prototype.changellyFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe.prototype.changellyFixedMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe.prototype.changellyFixedMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe.prototype.changellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe.prototype.changellyFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe.prototype.changellyFloatMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe.prototype.changellyFloatMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe.prototype.letsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe.prototype.letsExchangeFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe.prototype.letsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe.prototype.letsExchangeFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe.prototype.changeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe.prototype.changeNowFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe = function() {}
/** @type {?number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe.prototype.changeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe.prototype.changeNowFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe.prototype.exchangesTotal
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe.prototype.changellyFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe.prototype.changellyFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe.prototype.changellyFixedMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe.prototype.changellyFixedMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe.prototype.changellyFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe.prototype.changellyFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe.prototype.changellyFloatMin
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe.prototype.changellyFloatMax
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe.prototype.letsExchangeFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe.prototype.letsExchangeFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe.prototype.letsExchangeFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe.prototype.letsExchangeFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe.prototype.changeNowFixedOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe.prototype.changeNowFixedError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe.prototype.changeNowFloatingOffer
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe.prototype.changeNowFloatingError
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/** @record */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe.prototype.exchangesTotal
/** @typedef {$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe} */
xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFixedMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangellyFloatMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.LetsExchangeFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ChangeNowFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.Inputs.ExchangesTotal_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMin_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFixedMax_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFixedMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMin_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangellyFloatMax_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangellyFloatMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.LetsExchangeFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.LetsExchangeFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ChangeNowFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ChangeNowFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.WeakModel.ExchangesTotal_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal_Safe} */
xyz.swapee.wc.IOffersAggregatorPort.WeakInputs.ExchangesTotal_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMin_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFixedMax_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFixedMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMin_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMin_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangellyFloatMax_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangellyFloatMax_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.LetsExchangeFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.LetsExchangeFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFixedError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFixedError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingOffer_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ChangeNowFloatingError_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ChangeNowFloatingError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/03-IOffersAggregatorOuterCore.xml} xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe filter:!ControllerPlugin~props 79e50498a3229290a667d3e9694bfcef */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorOuterCore.Model.ExchangesTotal_Safe}
 */
$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe} */
xyz.swapee.wc.IOffersAggregatorCore.Model.ExchangesTotal_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorCore.Model
/* @typal-end */