/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorElementPort
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Initialese filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IOffersAggregatorElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Initialese} */
xyz.swapee.wc.IOffersAggregatorElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPortFields filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorElementPortFields = function() {}
/** @type {!xyz.swapee.wc.IOffersAggregatorElementPort.Inputs} */
$xyz.swapee.wc.IOffersAggregatorElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOffersAggregatorElementPort.Inputs} */
$xyz.swapee.wc.IOffersAggregatorElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorElementPortFields}
 */
xyz.swapee.wc.IOffersAggregatorElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPortCaster filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @interface */
$xyz.swapee.wc.IOffersAggregatorElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOffersAggregatorElementPort} */
$xyz.swapee.wc.IOffersAggregatorElementPortCaster.prototype.asIOffersAggregatorElementPort
/** @type {!xyz.swapee.wc.BoundOffersAggregatorElementPort} */
$xyz.swapee.wc.IOffersAggregatorElementPortCaster.prototype.superOffersAggregatorElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorElementPortCaster}
 */
xyz.swapee.wc.IOffersAggregatorElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersAggregatorElementPort.Inputs>}
 */
$xyz.swapee.wc.IOffersAggregatorElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOffersAggregatorElementPort}
 */
xyz.swapee.wc.IOffersAggregatorElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.OffersAggregatorElementPort filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElementPort.Initialese} init
 * @implements {xyz.swapee.wc.IOffersAggregatorElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersAggregatorElementPort.Initialese>}
 */
$xyz.swapee.wc.OffersAggregatorElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OffersAggregatorElementPort}
 */
xyz.swapee.wc.OffersAggregatorElementPort
/** @type {function(new: xyz.swapee.wc.IOffersAggregatorElementPort, ...!xyz.swapee.wc.IOffersAggregatorElementPort.Initialese)} */
xyz.swapee.wc.OffersAggregatorElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorElementPort}
 */
xyz.swapee.wc.OffersAggregatorElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.AbstractOffersAggregatorElementPort filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElementPort.Initialese} init
 * @extends {xyz.swapee.wc.OffersAggregatorElementPort}
 */
$xyz.swapee.wc.AbstractOffersAggregatorElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOffersAggregatorElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOffersAggregatorElementPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorElementPort
/** @type {function(new: xyz.swapee.wc.AbstractOffersAggregatorElementPort)} */
xyz.swapee.wc.AbstractOffersAggregatorElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorElementPort|typeof xyz.swapee.wc.OffersAggregatorElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersAggregatorElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOffersAggregatorElementPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersAggregatorElementPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorElementPort|typeof xyz.swapee.wc.OffersAggregatorElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorElementPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.IOffersAggregatorElementPort|typeof xyz.swapee.wc.OffersAggregatorElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OffersAggregatorElementPort}
 */
xyz.swapee.wc.AbstractOffersAggregatorElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.OffersAggregatorElementPortConstructor filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @typedef {function(new: xyz.swapee.wc.IOffersAggregatorElementPort, ...!xyz.swapee.wc.IOffersAggregatorElementPort.Initialese)} */
xyz.swapee.wc.OffersAggregatorElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.RecordIOffersAggregatorElementPort filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOffersAggregatorElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.BoundIOffersAggregatorElementPort filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPortFields}
 * @extends {xyz.swapee.wc.RecordIOffersAggregatorElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOffersAggregatorElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundIOffersAggregatorElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOffersAggregatorElementPort} */
xyz.swapee.wc.BoundIOffersAggregatorElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.BoundOffersAggregatorElementPort filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOffersAggregatorElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOffersAggregatorElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundOffersAggregatorElementPort} */
xyz.swapee.wc.BoundOffersAggregatorElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @typedef {boolean} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts.amountInInOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts.amountInInOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts.amountOutInOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts.amountOutInOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @typedef {!Object} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts.prototype.amountInInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts.prototype.amountOutInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts}
 */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs}
 */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts.prototype.amountInInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts.prototype.amountOutInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts}
 * @extends {xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts}
 */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs}
 */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts_Safe.prototype.amountInInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountInInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts_Safe.prototype.amountOutInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.AmountOutInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts_Safe.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.Inputs.ExchangeIntentOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts_Safe.prototype.amountInInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountInInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts_Safe.prototype.amountOutInOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.AmountOutInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/design/140-IOffersAggregatorElementPort.xml} xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts_Safe filter:!ControllerPlugin~props 55d6b70a1aef988ad047c7c78618aac6 */
/** @record */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts_Safe.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts_Safe} */
xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs.ExchangeIntentOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOffersAggregatorElementPort.WeakInputs
/* @typal-end */