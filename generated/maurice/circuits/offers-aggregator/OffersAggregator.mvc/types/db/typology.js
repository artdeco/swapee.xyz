/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IOffersAggregatorComputer': {
  'id': 48900887571,
  'symbols': {},
  'methods': {
   'adaptChangellyFloatingOffer': 1,
   'adaptChangellyFixedOffer': 2,
   'adaptEstimatedOut': 4,
   'adaptBestAndWorstOffers': 5,
   'adaptExchangesLoaded': 6,
   'adaptIsAggregating': 7,
   'compute': 8,
   'adaptLetsExchangeFloatingOffer': 9,
   'adaptLetsExchangeFixedOffer': 10,
   'adaptChangeNowFloatingOffer': 11,
   'adaptChangeNowFixedOffer': 12,
   'adaptLoadingEstimate': 13,
   'adaptMinAmount': 14,
   'adaptMaxAmount': 15,
   'adaptMinError': 16,
   'adaptMaxError': 17
  }
 },
 'xyz.swapee.wc.OffersAggregatorMemoryPQs': {
  'id': 48900887572,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorOuterCore': {
  'id': 48900887573,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersAggregatorInputsPQs': {
  'id': 48900887574,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorPort': {
  'id': 48900887575,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersAggregatorPort': 2
  }
 },
 'xyz.swapee.wc.OffersAggregatorCachePQs': {
  'id': 48900887576,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorCore': {
  'id': 48900887577,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersAggregatorCore': 2
  }
 },
 'xyz.swapee.wc.IOffersAggregatorProcessor': {
  'id': 48900887578,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregator': {
  'id': 48900887579,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorBuffer': {
  'id': 489008875710,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorHtmlComponent': {
  'id': 489008875711,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorElement': {
  'id': 489008875712,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4,
   'build': 5,
   'buildExchangeIntent': 6,
   'short': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorElementPort': {
  'id': 489008875713,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorRadio': {
  'id': 489008875714,
  'symbols': {},
  'methods': {
   'adaptLoadChangellyFloatingOffer': 1,
   'adaptLoadChangellyFixedOffer': 2,
   'adaptLoadLetsExchangeFloatingOffer': 4,
   'adaptLoadLetsExchangeFixedOffer': 5,
   'adaptLoadChangeNowFloatingOffer': 6,
   'adaptLoadChangeNowFixedOffer': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorDesigner': {
  'id': 489008875715,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersAggregatorService': {
  'id': 489008875716,
  'symbols': {},
  'methods': {
   'filterChangellyFloatingOffer': 1,
   'filterChangellyFixedOffer': 2,
   'filterLetsExchangeFloatingOffer': 4,
   'filterLetsExchangeFixedOffer': 5,
   'filterChangeNowFloatingOffer': 6,
   'filterChangeNowFixedOffer': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorGPU': {
  'id': 489008875717,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorDisplay': {
  'id': 489008875718,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersAggregatorVdusPQs': {
  'id': 489008875719,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersAggregatorDisplay': {
  'id': 489008875720,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorController': {
  'id': 489008875721,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setChangellyFixedOffer': 2,
   'unsetChangellyFixedOffer': 3,
   'setChangellyFixedError': 4,
   'unsetChangellyFixedError': 5,
   'setChangellyFloatingError': 6,
   'unsetChangellyFloatingError': 7,
   'setChangellyFloatingOffer': 8,
   'unsetChangellyFloatingOffer': 9,
   'loadChangellyFloatingOffer': 16,
   'loadChangellyFixedOffer': 17,
   'setLetsExchangeFixedOffer': 19,
   'unsetLetsExchangeFixedOffer': 20,
   'setLetsExchangeFixedError': 21,
   'unsetLetsExchangeFixedError': 22,
   'setLetsExchangeFloatingOffer': 23,
   'unsetLetsExchangeFloatingOffer': 24,
   'setLetsExchangeFloatingError': 25,
   'unsetLetsExchangeFloatingError': 26,
   'loadLetsExchangeFloatingOffer': 27,
   'loadLetsExchangeFixedOffer': 28,
   'setChangeNowFixedOffer': 29,
   'unsetChangeNowFixedOffer': 30,
   'setChangeNowFixedError': 31,
   'unsetChangeNowFixedError': 32,
   'setChangeNowFloatingOffer': 33,
   'unsetChangeNowFloatingOffer': 34,
   'setChangeNowFloatingError': 35,
   'unsetChangeNowFloatingError': 36,
   'loadChangeNowFloatingOffer': 37,
   'loadChangeNowFixedOffer': 38,
   'setChangellyFixedMin': 39,
   'unsetChangellyFixedMin': 40,
   'setChangellyFixedMax': 41,
   'unsetChangellyFixedMax': 42,
   'setChangellyFloatMin': 47,
   'unsetChangellyFloatMin': 48,
   'setChangellyFloatMax': 49,
   'unsetChangellyFloatMax': 50
  }
 },
 'xyz.swapee.wc.front.IOffersAggregatorController': {
  'id': 489008875722,
  'symbols': {},
  'methods': {
   'setChangellyFixedOffer': 1,
   'unsetChangellyFixedOffer': 2,
   'setChangellyFixedError': 3,
   'unsetChangellyFixedError': 4,
   'setChangellyFloatingError': 5,
   'unsetChangellyFloatingError': 6,
   'setChangellyFloatingOffer': 7,
   'unsetChangellyFloatingOffer': 8,
   'loadChangellyFloatingOffer': 15,
   'loadChangellyFixedOffer': 16,
   'setLetsExchangeFixedOffer': 18,
   'unsetLetsExchangeFixedOffer': 19,
   'setLetsExchangeFixedError': 20,
   'unsetLetsExchangeFixedError': 21,
   'setLetsExchangeFloatingOffer': 22,
   'unsetLetsExchangeFloatingOffer': 23,
   'setLetsExchangeFloatingError': 24,
   'unsetLetsExchangeFloatingError': 25,
   'loadLetsExchangeFloatingOffer': 26,
   'loadLetsExchangeFixedOffer': 27,
   'setChangeNowFixedOffer': 28,
   'unsetChangeNowFixedOffer': 29,
   'setChangeNowFixedError': 30,
   'unsetChangeNowFixedError': 31,
   'setChangeNowFloatingOffer': 32,
   'unsetChangeNowFloatingOffer': 33,
   'setChangeNowFloatingError': 34,
   'unsetChangeNowFloatingError': 35,
   'loadChangeNowFloatingOffer': 36,
   'loadChangeNowFixedOffer': 37,
   'setChangellyFixedMin': 38,
   'unsetChangellyFixedMin': 39,
   'setChangellyFixedMax': 40,
   'unsetChangellyFixedMax': 41,
   'setChangellyFloatMin': 46,
   'unsetChangellyFloatMin': 47,
   'setChangellyFloatMax': 48,
   'unsetChangellyFloatMax': 49
  }
 },
 'xyz.swapee.wc.back.IOffersAggregatorController': {
  'id': 489008875723,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorControllerAR': {
  'id': 489008875724,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersAggregatorControllerAT': {
  'id': 489008875725,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorScreen': {
  'id': 489008875726,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorScreen': {
  'id': 489008875727,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersAggregatorScreenAR': {
  'id': 489008875728,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorScreenAT': {
  'id': 489008875729,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorPortInterface': {
  'id': 489008875730,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil': {
  'id': 489008875731,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.OffersAggregatorQueriesPQs': {
  'id': 489008875732,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})