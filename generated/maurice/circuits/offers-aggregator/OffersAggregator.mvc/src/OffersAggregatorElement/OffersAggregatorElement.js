import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import OffersAggregatorServerController from '../OffersAggregatorServerController'
import OffersAggregatorService from '../../src/OffersAggregatorService'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractOffersAggregatorElement from '../../gen/AbstractOffersAggregatorElement'

/** @extends {xyz.swapee.wc.OffersAggregatorElement} */
export default class OffersAggregatorElement extends AbstractOffersAggregatorElement.implements(
 OffersAggregatorServerController,
 OffersAggregatorService,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IOffersAggregatorElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
   classesMap:       true,
   rootSelector:     `.OffersAggregator`,
   stylesheet:       'html/styles/OffersAggregator.css',
   blockName:        'html/OffersAggregatorBlock.html',
  }),
){}

// thank you for using web circuits
