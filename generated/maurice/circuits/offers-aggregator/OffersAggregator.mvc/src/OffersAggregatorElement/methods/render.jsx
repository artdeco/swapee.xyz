/** @type {xyz.swapee.wc.IOffersAggregatorElement._render} */
export default function OffersAggregatorRender({amountIn:amountIn,estimatedOut:estimatedOut},{}) {
 return (<div $id="OffersAggregator">
  <input $id="AmountInIn" value={amountIn}/>
  <input $id="AmountOutIn" value={estimatedOut}/>
 </div>)
}