/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptMinError} */
export default function adaptMinError({estimatedOut:estimatedOut,minAmount:minAmount}) {
 if(!minAmount||estimatedOut) return{minError:null}
 return{minError:minAmount}
}