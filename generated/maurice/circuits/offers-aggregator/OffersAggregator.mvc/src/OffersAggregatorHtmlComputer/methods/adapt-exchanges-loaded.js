/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptExchangesLoaded} */
export default function adaptExchangesLoaded({
 loadingChangellyFloatingOffer,
 loadingLetsExchangeFixedOffer,
 loadingLetsExchangeFloatingOffer,
 loadingChangeNowFixedOffer,
 loadingChangeNowFloatingOffer,
 loadingChangellyFixedOffer,
 exchangesTotal,
}){
 let loading=0

 if(loadingChangellyFixedOffer) loading++
 if(loadingChangellyFloatingOffer) loading++
 if(loadingLetsExchangeFixedOffer) loading++
 if(loadingLetsExchangeFloatingOffer) loading++
 if(loadingChangeNowFixedOffer) loading++
 if(loadingChangeNowFloatingOffer) loading++

 const allExchangesLoaded=loading==0
 return{
  allExchangesLoaded:allExchangesLoaded,
  exchangesLoaded:exchangesTotal-loading,
 }
}