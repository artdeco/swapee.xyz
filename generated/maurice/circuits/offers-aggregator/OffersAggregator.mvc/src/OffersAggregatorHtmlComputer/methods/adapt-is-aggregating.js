/** @type {xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating} */
export const adaptIsAggregating=({
 loadingChangellyFloatingOffer:loadingChangellyFloatingOffer,
 loadingChangellyFixedOffer:loadingChangellyFixedOffer,
 loadingChangeNowFixedOffer:loadingChangeNowFixedOffer,
 loadingChangeNowFloatingOffer:loadingChangeNowFloatingOffer,
 loadingLetsExchangeFixedOffer:loadingLetsExchangeFixedOffer,
 loadingLetsExchangeFloatingOffer:loadingLetsExchangeFloatingOffer,
})=>{
 return{ // todo: implement this in the radio?
  isAggregating:
   loadingChangellyFloatingOffer||loadingChangellyFixedOffer||
   loadingChangeNowFixedOffer||loadingChangeNowFloatingOffer||
   loadingLetsExchangeFixedOffer||loadingLetsExchangeFloatingOffer,
 }
}

export default adaptIsAggregating