/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptEstimatedOut} */
export default function adaptEstimatedOut({bestOffer:bestOffer}){
 return{estimatedOut:bestOffer}
}