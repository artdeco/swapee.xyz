/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptBestAndWorstOffers} */
export default function adaptBestAndWorstOffers({
 changellyFixedOffer:changellyFixedOffer,
 changellyFloatingOffer:changellyFloatingOffer,
 changeNowFixedOffer,changeNowFloatingOffer,
 letsExchangeFixedOffer,letsExchangeFloatingOffer,
}) {
 const offers=[
  changellyFloatingOffer,changellyFixedOffer,
  changeNowFixedOffer,changeNowFloatingOffer,
  letsExchangeFixedOffer,letsExchangeFloatingOffer,
 ].filter(Boolean)
 offers.sort().reverse()
 const bestOffer=offers.length?offers[0]:null
 const worstOffer=offers.length?offers[offers.length-1]:null
 return{
  bestOffer:bestOffer,
  worstOffer:worstOffer,
 }
}