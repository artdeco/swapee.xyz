/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptMinAmount} */
export default function adaptMinAmount({changellyFloatMin:changellyFloatMin}) {
 const vals=[]
 for(const val of [changellyFloatMin]) {
  if(val===undefined||val===null) continue
  vals.push(val)
 }
 if(vals.length==0) return {minAmount:null}
 return{minAmount:Math.min(vals)}
}