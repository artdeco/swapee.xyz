/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxError} */
export default function adaptMaxError({estimatedOut:estimatedOut,maxAmount}) {
 if(!maxAmount||estimatedOut) return{maxError:null}
 return{maxError:maxAmount}
}