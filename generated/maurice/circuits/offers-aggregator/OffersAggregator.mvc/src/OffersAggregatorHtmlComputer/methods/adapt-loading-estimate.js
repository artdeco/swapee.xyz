/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptLoadingEstimate} */
export default function adaptLoadingEstimate({isAggregating,estimatedOut:estimatedOut}) {
 if(!isAggregating||estimatedOut) return{loadingEstimate:false}
 return{loadingEstimate:true}
}