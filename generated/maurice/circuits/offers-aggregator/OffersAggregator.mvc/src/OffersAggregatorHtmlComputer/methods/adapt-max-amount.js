/** @type {xyz.swapee.wc.IOffersAggregatorComputer._adaptMaxAmount} */
export default function adaptMaxAmount({changellyFloatMax:changellyFloatMax}) {
 const vals=[]
 for(const val of[changellyFloatMax]) {
  if(val===undefined||val===null) continue
  vals.push(val)
 }
 if(vals.length==0) return {maxAmount:null}
 return{maxAmount:Math.max(vals)}
}