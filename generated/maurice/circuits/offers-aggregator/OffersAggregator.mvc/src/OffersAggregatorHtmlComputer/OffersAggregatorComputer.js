import adaptLoadingEstimate from './methods/adapt-loading-estimate'
import adaptMinError from './methods/adapt-min-error'
import adaptMaxError from './methods/adapt-max-error'
import adaptEstimatedOut from './methods/adapt-estimated-out'
import adaptBestAndWorstOffers from './methods/adapt-best-and-worst-offers'
import adaptIsAggregating from './methods/adapt-is-aggregating'
import adaptExchangesLoaded from './methods/adapt-exchanges-loaded'
import adaptMinAmount from './methods/adapt-min-amount'
import adaptMaxAmount from './methods/adapt-max-amount'
import {preadaptLoadingEstimate,preadaptMinError,preadaptMaxError,preadaptEstimatedOut,preadaptBestAndWorstOffers,preadaptIsAggregating,preadaptExchangesLoaded,preadaptMinAmount,preadaptMaxAmount} from '../../gen/AbstractOffersAggregatorComputer/preadapters'
import AbstractOffersAggregatorComputer from '../../gen/AbstractOffersAggregatorComputer'

/** @extends {xyz.swapee.wc.OffersAggregatorComputer} */
export default class OffersAggregatorHtmlComputer extends AbstractOffersAggregatorComputer.implements(
 /** @type {!xyz.swapee.wc.IOffersAggregatorComputer} */ ({
  adaptLoadingEstimate:adaptLoadingEstimate,
  adaptMinError:adaptMinError,
  adaptMaxError:adaptMaxError,
  adaptEstimatedOut:adaptEstimatedOut,
  adaptBestAndWorstOffers:adaptBestAndWorstOffers,
  adaptIsAggregating:adaptIsAggregating,
  adaptExchangesLoaded:adaptExchangesLoaded,
  adaptMinAmount:adaptMinAmount,
  adaptMaxAmount:adaptMaxAmount,
  adapt:[preadaptLoadingEstimate,preadaptMinError,preadaptMaxError,preadaptEstimatedOut,preadaptBestAndWorstOffers,preadaptIsAggregating,preadaptExchangesLoaded,preadaptMinAmount,preadaptMaxAmount],
 }),
){}