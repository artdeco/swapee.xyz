import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFloatingOffer} */
export default async function filterLetsExchangeFloatingOffer({
 amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
}){
 const{asULetsExchange:{letsExchange:letsExchange}}=this
 if(!letsExchange) return

 const{
  asILetsExchange:{
   GetInfo:GetInfo,
  },
 }=letsExchange

 try{
  const{amount:amount}=await GetInfo({
   amount:amountIn,
   from:currencyIn,
   to: currencyOut,
   affiliateId:'Zp1gafk2sd4goqJq',
   promocode:'swapee_test',
   float:true,
  })
  const amountOut=parseFloat(amount)
  return{
   letsExchangeFloatingOffer:amountOut,
  }
 }catch(err){
  return{
   letsExchangeFloatingError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQXdjRyxNQUFNLFNBQVMsK0JBQStCO0NBQzdDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLHNCQUFzQjtBQUNuRSxDQUFDO0NBQ0EsTUFBTSxpQkFBaUIseUJBQXlCLEdBQUc7Q0FDbkQsRUFBRSxDQUFDLGVBQWU7O0NBRWxCO0VBQ0M7R0FDQyxlQUFlO0FBQ2xCO0dBQ0c7O0NBRUY7RUFDQyxNQUFNLGVBQWUsTUFBTSxPQUFPO0dBQ2pDLGVBQWU7R0FDZixlQUFlO0dBQ2YsSUFBSSxXQUFXO0dBQ2YsOEJBQThCO0dBQzlCLHVCQUF1QjtHQUN2QixVQUFVO0FBQ2IsR0FBRztFQUNELE1BQU0sU0FBUyxDQUFDLFVBQVUsQ0FBQztFQUMzQjtHQUNDLG1DQUFtQztBQUN0QztBQUNBLEVBQUUsS0FBSyxDQUFDO0VBQ047R0FDQyw2QkFBNkIsQ0FBQyxPQUFPO0FBQ3hDO0FBQ0E7QUFDQSxDQUFGIn0=