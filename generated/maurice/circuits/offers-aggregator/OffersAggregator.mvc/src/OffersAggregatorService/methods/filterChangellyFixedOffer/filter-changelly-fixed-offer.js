import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOffersAggregatorService._filterChangellyFixedOffer} */
export default async function filterChangellyFixedOffer({
 amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
}) {
 // could put changelly on the land somehow?
 const{asUChangelly:{changelly:changelly}}=this
 if(!changelly) return

 const{
  asIChangellyFixedRate:{
   GetFixRateForAmount:GetFixRateForAmount,
  },
 }=changelly
 try{
  const res=await GetFixRateForAmount({
   from:     currencyIn,
   to:      currencyOut,
   amountFrom: amountIn,
  })
  const amountOut=parseFloat(res)
  return{
   changellyFixedOffer:amountOut,
  }
 }catch(err){
  if(err['code']==-32600) {
   // Invalid amount: minimal amount for matic->bat is 33.813
   if(/maximal/.test(err.message)) {
    const[max]=/[\d.]+$/.exec(err.message)||[]
    return{
     changellyFixedMax:parseFloat(max),
    }
   }
   const[min]=/[\d.]+$/.exec(err.message)||[]
   return {
    changellyFixedMin:parseFloat(min),
   }
  }
  // throw err
  return {
   changellyFixedError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQXVpQkcsTUFBTSxTQUFTLHlCQUF5QjtDQUN2QyxtQkFBbUIsQ0FBQyx1QkFBdUIsQ0FBQyxzQkFBc0I7QUFDbkUsQ0FBQztDQUNBLEdBQUcsTUFBTSxJQUFJLFVBQVUsR0FBRyxJQUFJLEtBQUs7Q0FDbkMsTUFBTSxjQUFjLG1CQUFtQixHQUFHO0NBQzFDLEVBQUUsQ0FBQyxZQUFZOztDQUVmO0VBQ0M7R0FDQyx1Q0FBdUM7QUFDMUM7R0FDRztDQUNGO0VBQ0MsTUFBTSxHQUFHLENBQUMsTUFBTSxtQkFBbUI7R0FDbEMsVUFBVSxVQUFVO0dBQ3BCLFNBQVMsV0FBVztHQUNwQixZQUFZLFFBQVE7QUFDdkIsR0FBRztFQUNELE1BQU0sU0FBUyxDQUFDLFVBQVUsQ0FBQztFQUMzQjtHQUNDLDZCQUE2QjtBQUNoQztBQUNBLEVBQUUsS0FBSyxDQUFDO0VBQ04sRUFBRSxDQUFDLFdBQVcsRUFBRTtHQUNmLEdBQUcsUUFBUSxRQUFRLFFBQVEsT0FBTyxJQUFJLE1BQU0sQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0dBQ3ZELEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNyQixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQzlCO0tBQ0MsNEJBQTRCLENBQUMsSUFBSTtBQUN0QztBQUNBO0dBQ0csVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztHQUM5QjtJQUNDLDRCQUE0QixDQUFDLElBQUk7QUFDckM7QUFDQTtFQUNFLEdBQUcsTUFBTTtFQUNUO0dBQ0MsdUJBQXVCLENBQUMsT0FBTztBQUNsQztBQUNBO0FBQ0EsQ0FBRiJ9