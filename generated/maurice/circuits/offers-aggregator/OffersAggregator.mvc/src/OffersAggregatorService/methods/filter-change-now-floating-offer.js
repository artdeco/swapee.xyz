/**@type {xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFloatingOffer}*/
export default
async function filterChangeNowFloatingOffer({
 amountIn:amountIn,currencyIn:currencyIn,currencyOut:currencyOut,
}){
 const{asUChangeNow:{changeNow:changeNow}}=this
 if(!changeNow) return

 const{
  asIChangeNow:{
   GetExchangeAmount:GetExchangeAmount,
  },
 }=changeNow

 try{
  const{toAmount:toAmount}=await GetExchangeAmount({
   fromAmount:amountIn,
   fromCurrency:currencyIn,
   toCurrency: currencyOut,
  })
  const amountOut=parseFloat(toAmount)
  return{
   changeNowFloatingOffer:amountOut,
  }
 }catch(err){
  return{
   changeNowFloatingError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQTBVRyxNQUFNLFNBQVMsNEJBQTRCO0NBQzFDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLHVCQUF1QjtBQUNoRSxDQUFDO0NBQ0EsTUFBTSxjQUFjLG1CQUFtQixHQUFHO0NBQzFDLEVBQUUsQ0FBQyxZQUFZOztDQUVmO0VBQ0M7R0FDQyxtQ0FBbUM7QUFDdEM7R0FDRzs7Q0FFRjtFQUNDLE1BQU0sbUJBQW1CLE1BQU0saUJBQWlCO0dBQy9DLG1CQUFtQjtHQUNuQix1QkFBdUI7R0FDdkIsWUFBWSxXQUFXO0FBQzFCLEdBQUc7RUFDRCxNQUFNLFNBQVMsQ0FBQyxVQUFVLENBQUM7RUFDM0I7R0FDQyxnQ0FBZ0M7QUFDbkM7QUFDQSxFQUFFLEtBQUssQ0FBQztFQUNOO0dBQ0MsMEJBQTBCLENBQUMsT0FBTztBQUNyQztBQUNBO0FBQ0EsQ0FBRiJ9