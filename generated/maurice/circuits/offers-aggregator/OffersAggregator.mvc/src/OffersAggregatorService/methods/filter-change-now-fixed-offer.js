/**@type {xyz.swapee.wc.IOffersAggregatorService._filterChangeNowFixedOffer}*/
export default
async function filterChangeNowFixedOffer({
 amountIn:amountIn,currencyIn:currencyIn,currencyOut:currencyOut,
}){
 const{asUChangeNow:{changeNow:changeNow}}=this
 if(!changeNow) return

 const{
  asIChangeNow:{
   GetFixedExchangeAmount:GetFixedExchangeAmount,
  },
 }=changeNow

 try{
  const{toAmount:toAmount}=await GetFixedExchangeAmount({
   fromAmount:amountIn,
   fromCurrency:currencyIn,
   toCurrency: currencyOut,
  })
  const amountOut=parseFloat(toAmount)
  return{
   changeNowFixedOffer:amountOut,
  }
 }catch(err){
  return{
   changeNowFixedError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQThTRyxNQUFNLFNBQVMseUJBQXlCO0NBQ3ZDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLHVCQUF1QjtBQUNoRSxDQUFDO0NBQ0EsTUFBTSxjQUFjLG1CQUFtQixHQUFHO0NBQzFDLEVBQUUsQ0FBQyxZQUFZOztDQUVmO0VBQ0M7R0FDQyw2Q0FBNkM7QUFDaEQ7R0FDRzs7Q0FFRjtFQUNDLE1BQU0sbUJBQW1CLE1BQU0sc0JBQXNCO0dBQ3BELG1CQUFtQjtHQUNuQix1QkFBdUI7R0FDdkIsWUFBWSxXQUFXO0FBQzFCLEdBQUc7RUFDRCxNQUFNLFNBQVMsQ0FBQyxVQUFVLENBQUM7RUFDM0I7R0FDQyw2QkFBNkI7QUFDaEM7QUFDQSxFQUFFLEtBQUssQ0FBQztFQUNOO0dBQ0MsdUJBQXVCLENBQUMsT0FBTztBQUNsQztBQUNBO0FBQ0EsQ0FBRiJ9