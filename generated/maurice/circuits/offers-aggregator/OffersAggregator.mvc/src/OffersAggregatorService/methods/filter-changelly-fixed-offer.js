/**@type {xyz.swapee.wc.IOffersAggregatorService._filterChangellyFixedOffer}*/
export default
async function filterChangellyFixedOffer({
 amountIn:amountIn,currencyIn:currencyIn,currencyOut:currencyOut,
}) {
 // could put changelly on the land somehow?
 const{asUChangelly:{changelly:changelly}}=this
 if(!changelly) return

 const{
  asIChangellyFixedRate:{
   GetFixRateForAmount:GetFixRateForAmount,
  },
 }=changelly
 try {
  const res=await GetFixRateForAmount({
   from:     currencyIn,
   to:      currencyOut,
   amountFrom: amountIn,
  })
  const amountOut=parseFloat(res)
  return{
   changellyFixedOffer:amountOut,
  }
 }catch(err){
  return {
   changellyFixedError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQWdjRyxNQUFNLFNBQVMseUJBQXlCO0NBQ3ZDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLHVCQUF1QjtBQUNoRSxDQUFDO0NBQ0EsR0FBRyxNQUFNLElBQUksVUFBVSxHQUFHLElBQUksS0FBSztDQUNuQyxNQUFNLGNBQWMsbUJBQW1CLEdBQUc7Q0FDMUMsRUFBRSxDQUFDLFlBQVk7O0NBRWY7RUFDQztHQUNDLHVDQUF1QztBQUMxQztHQUNHO0NBQ0Y7RUFDQyxNQUFNLEdBQUcsQ0FBQyxNQUFNLG1CQUFtQjtHQUNsQyxVQUFVLFVBQVU7R0FDcEIsU0FBUyxXQUFXO0dBQ3BCLFlBQVksUUFBUTtBQUN2QixHQUFHO0VBQ0QsTUFBTSxTQUFTLENBQUMsVUFBVSxDQUFDO0VBQzNCO0dBQ0MsNkJBQTZCO0FBQ2hDO0FBQ0EsRUFBRSxLQUFLLENBQUM7RUFDTjtHQUNDLHVCQUF1QixDQUFDLE9BQU87QUFDbEM7QUFDQTtBQUNBLENBQUYifQ==