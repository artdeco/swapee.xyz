import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOffersAggregatorService._filterChangellyFloatingOffer} */
export default async function filterChangellyFloatingOffer({
 amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
}) {
 // could put changelly on the land somehow?
 const{asUChangelly:{changelly:changelly}}=this
 if(!changelly) return

 const{
  asIChangellyExchange:{
   GetExchangeAmount:GetExchangeAmount,
   GetExtendedExchangeAmount:GetExtendedExchangeAmount,
  },
 }=changelly
 try{
  const res=await GetExtendedExchangeAmount({
   amount: amountIn,
   from:   currencyIn,
   to:     currencyOut,
  })
  if(!res) {
   try {
    const rr=await GetExchangeAmount({
     amount:amountIn,
     from:  currencyIn,
     to:    currencyOut,
    })
    return{
     changellyFloatingOffer:rr,
    }
    // debugger
   }catch(err) {
    if(err['code']==-32600) {
     // Invalid amount: minimal amount for matic->bat is 33.813
     if(/maximal/.test(err.message)) {
      const[max]=/[\d.]+$/.exec(err.message)||[]
      return {
       changellyFloatMax:parseFloat(max),
      }
     }
     const[min]=/[\d.]+$/.exec(err.message)||[]
     return {
      changellyFloatMin:parseFloat(min),
     }
    }else if(err['code']) {
     throw new Error(`!${err.message}`)
    }
    throw err
   }
  }
  let{
   result:result,networkFee:networkFee,
  }=res
  networkFee=Number(networkFee)
  const r=Number(result)-networkFee

  return{
   changellyFloatingOffer:r,
  }
 }catch(err){
  return{
   changellyFloatingError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQXVlRyxNQUFNLFNBQVMsNEJBQTRCO0NBQzFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLHNCQUFzQjtBQUNuRSxDQUFDO0NBQ0EsR0FBRyxNQUFNLElBQUksVUFBVSxHQUFHLElBQUksS0FBSztDQUNuQyxNQUFNLGNBQWMsbUJBQW1CLEdBQUc7Q0FDMUMsRUFBRSxDQUFDLFlBQVk7O0NBRWY7RUFDQztHQUNDLG1DQUFtQztHQUNuQyxtREFBbUQ7QUFDdEQ7R0FDRztDQUNGO0VBQ0MsTUFBTSxHQUFHLENBQUMsTUFBTSx5QkFBeUI7R0FDeEMsUUFBUSxRQUFRO0dBQ2hCLFFBQVEsVUFBVTtHQUNsQixRQUFRLFdBQVc7QUFDdEIsR0FBRztFQUNELEVBQUUsQ0FBQztHQUNGO0lBQ0MsTUFBTSxFQUFFLENBQUMsTUFBTSxpQkFBaUI7S0FDL0IsZUFBZTtLQUNmLE9BQU8sVUFBVTtLQUNqQixPQUFPLFdBQVc7QUFDdkIsS0FBSztJQUNEO0tBQ0MseUJBQXlCO0FBQzlCO0lBQ0ksR0FBRztBQUNQLElBQUksS0FBSyxDQUFDO0lBQ04sRUFBRSxDQUFDLFdBQVcsRUFBRTtLQUNmLEdBQUcsUUFBUSxRQUFRLFFBQVEsT0FBTyxJQUFJLE1BQU0sQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0tBQ3ZELEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztNQUNyQixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO01BQzlCO09BQ0MsNEJBQTRCLENBQUMsSUFBSTtBQUN4QztBQUNBO0tBQ0ssVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztLQUM5QjtNQUNDLDRCQUE0QixDQUFDLElBQUk7QUFDdkM7QUFDQSxLQUFLLEtBQUssRUFBRSxDQUFDO0tBQ1IsTUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUM7QUFDckM7SUFDSSxNQUFNO0FBQ1Y7QUFDQTtFQUNFO0dBQ0MsYUFBYSxDQUFDLHFCQUFxQjtJQUNsQztFQUNGLFVBQVUsQ0FBQyxNQUFNLENBQUM7RUFDbEIsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDOztFQUVmO0dBQ0Msd0JBQXdCO0FBQzNCO0FBQ0EsRUFBRSxLQUFLLENBQUM7RUFDTjtHQUNDLDBCQUEwQixDQUFDLE9BQU87QUFDckM7QUFDQTtBQUNBLENBQUYifQ==