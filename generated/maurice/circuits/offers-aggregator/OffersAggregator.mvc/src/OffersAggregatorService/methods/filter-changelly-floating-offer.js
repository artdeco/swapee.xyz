/**@type {xyz.swapee.wc.IOffersAggregatorService._filterChangellyFloatingOffer}*/
export default
async function filterChangellyFloatingOffer({
 amountIn:amountIn,currencyIn:currencyIn,currencyOut:currencyOut,
}) {
 // could put changelly on the land somehow?
 const{asUChangelly:{changelly:changelly}}=this
 if(!changelly) return

 const{
  asIChangellyExchange:{
   GetExchangeAmount:GetExchangeAmount,
  },
 }=changelly
 try {
  const res=await GetExchangeAmount({
   from:   currencyIn,
   to:     currencyOut,
   amount: amountIn,
  })
  const amountOut=parseFloat(res)
  return{
   changellyFloatingOffer:amountOut,
  }
 }catch(err){
  return{
   changellyFloatingError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQW9hRyxNQUFNLFNBQVMsNEJBQTRCO0NBQzFDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLHVCQUF1QjtBQUNoRSxDQUFDO0NBQ0EsR0FBRyxNQUFNLElBQUksVUFBVSxHQUFHLElBQUksS0FBSztDQUNuQyxNQUFNLGNBQWMsbUJBQW1CLEdBQUc7Q0FDMUMsRUFBRSxDQUFDLFlBQVk7O0NBRWY7RUFDQztHQUNDLG1DQUFtQztBQUN0QztHQUNHO0NBQ0Y7RUFDQyxNQUFNLEdBQUcsQ0FBQyxNQUFNLGlCQUFpQjtHQUNoQyxRQUFRLFVBQVU7R0FDbEIsUUFBUSxXQUFXO0dBQ25CLFFBQVEsUUFBUTtBQUNuQixHQUFHO0VBQ0QsTUFBTSxTQUFTLENBQUMsVUFBVSxDQUFDO0VBQzNCO0dBQ0MsZ0NBQWdDO0FBQ25DO0FBQ0EsRUFBRSxLQUFLLENBQUM7RUFDTjtHQUNDLDBCQUEwQixDQUFDLE9BQU87QUFDckM7QUFDQTtBQUNBLENBQUYifQ==