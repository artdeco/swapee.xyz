import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOffersAggregatorService._filterLetsExchangeFixedOffer} */
export default async function filterLetsExchangeFixedOffer({
 amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
}){
 const{asULetsExchange:{letsExchange:letsExchange}}=this
 if(!letsExchange) return

 const{
  asILetsExchange:{
   GetInfo:GetInfo,
  },
 }=letsExchange

 try{
  const{amount:amount}=await GetInfo({
   amount:amountIn,
   from:currencyIn,
   to:currencyOut,
   affiliateId:'Zp1gafk2sd4goqJq',
   promocode:'swapee_test',
   float:false,
  })
  const amountOut=parseFloat(amount)
  return{
   letsExchangeFixedOffer:amountOut,
  }
 }catch(err){
  return{
   letsExchangeFixedError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQXlhRyxNQUFNLFNBQVMsNEJBQTRCO0NBQzFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLHNCQUFzQjtBQUNuRSxDQUFDO0NBQ0EsTUFBTSxpQkFBaUIseUJBQXlCLEdBQUc7Q0FDbkQsRUFBRSxDQUFDLGVBQWU7O0NBRWxCO0VBQ0M7R0FDQyxlQUFlO0FBQ2xCO0dBQ0c7O0NBRUY7RUFDQyxNQUFNLGVBQWUsTUFBTSxPQUFPO0dBQ2pDLGVBQWU7R0FDZixlQUFlO0dBQ2YsY0FBYztHQUNkLDhCQUE4QjtHQUM5Qix1QkFBdUI7R0FDdkIsV0FBVztBQUNkLEdBQUc7RUFDRCxNQUFNLFNBQVMsQ0FBQyxVQUFVLENBQUM7RUFDM0I7R0FDQyxnQ0FBZ0M7QUFDbkM7QUFDQSxFQUFFLEtBQUssQ0FBQztFQUNOO0dBQ0MsMEJBQTBCLENBQUMsT0FBTztBQUNyQztBQUNBO0FBQ0EsQ0FBRiJ9