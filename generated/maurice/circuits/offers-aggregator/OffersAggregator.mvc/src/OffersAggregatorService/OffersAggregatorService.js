import AbstractOffersAggregatorService from '../../gen/AbstractOffersAggregatorService'
import filterChangeNowFixedOffer from './methods/filterChangeNowFixedOffer/filter-change-now-fixed-offer'
import filterChangeNowFloatingOffer from './methods/filterChangeNowFloatingOffer/filter-change-now-floating-offer'
import filterLetsExchangeFixedOffer from './methods/filterLetsExchangeFixedOffer/filter-lets-exchange-fixed-offer'
import filterLetsExchangeFloatingOffer from './methods/filterLetsExchangeFloatingOffer/filter-lets-exchange-floating-offer'
import filterChangellyFloatingOffer from './methods/filterChangellyFloatingOffer/filter-changelly-floating-offer'
import filterChangellyFixedOffer from './methods/filterChangellyFixedOffer/filter-changelly-fixed-offer'
import {ChangellyUniversal} from '@type.community/changelly.com'
import {LetsExchangeUniversal} from '@swapee.xyz/swapee.xyz'
import {ChangeNowUniversal} from '@swapee.xyz/swapee.xyz'

/**@extends {xyz.swapee.wc.OffersAggregatorService} */
export default class extends AbstractOffersAggregatorService.implements(
 ChangellyUniversal,
 LetsExchangeUniversal,
 ChangeNowUniversal,
 AbstractOffersAggregatorService.class.prototype=/**@type {!xyz.swapee.wc.OffersAggregatorService}*/({
  filterChangeNowFixedOffer:filterChangeNowFixedOffer,
  filterChangeNowFloatingOffer:filterChangeNowFloatingOffer,
  filterLetsExchangeFixedOffer:filterLetsExchangeFixedOffer,
  filterLetsExchangeFloatingOffer:filterLetsExchangeFloatingOffer,
  filterChangellyFloatingOffer:filterChangellyFloatingOffer,
  filterChangellyFixedOffer:filterChangellyFixedOffer,
 }),
){}