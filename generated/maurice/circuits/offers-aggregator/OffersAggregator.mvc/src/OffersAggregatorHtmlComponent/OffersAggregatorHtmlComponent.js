import OffersAggregatorHtmlController from '../OffersAggregatorHtmlController'
import OffersAggregatorHtmlComputer from '../OffersAggregatorHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractOffersAggregatorHtmlComponent} from '../../gen/AbstractOffersAggregatorHtmlComponent'

/** @extends {xyz.swapee.wc.OffersAggregatorHtmlComponent} */
export default class extends AbstractOffersAggregatorHtmlComponent.implements(
 OffersAggregatorHtmlController,
 OffersAggregatorHtmlComputer,
 IntegratedComponentInitialiser,
){}