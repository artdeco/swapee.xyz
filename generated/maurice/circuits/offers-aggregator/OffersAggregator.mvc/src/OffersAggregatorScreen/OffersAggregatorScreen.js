import AbstractOffersAggregatorControllerAT from '../../gen/AbstractOffersAggregatorControllerAT'
import OffersAggregatorDisplay from '../OffersAggregatorDisplay'
import AbstractOffersAggregatorScreen from '../../gen/AbstractOffersAggregatorScreen'

/** @extends {xyz.swapee.wc.OffersAggregatorScreen} */
export default class extends AbstractOffersAggregatorScreen.implements(
 AbstractOffersAggregatorControllerAT,
 OffersAggregatorDisplay,
 /**@type {!xyz.swapee.wc.IOffersAggregatorScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IOffersAggregatorScreen} */ ({
  __$id:4890088757,
 }),
){}