/**@type {xyz.swapee.wc.IOffersAggregatorDesigner._relay}*/
export default
function relay({This:This,OffersAggregator:_OffersAggregator}){
 return (h(Fragment,{},
  h(This,{
   onLoadingChangellyFixedOfferHigh:[
    _OffersAggregator.unsetChangellyFixedError(),
    _OffersAggregator.unsetChangellyFixedOffer(),
   ],
   onLoadingChangellyFloatingOfferHigh:[
    _OffersAggregator.unsetChangellyFloatingError(),
    _OffersAggregator.unsetChangellyFloatingOffer(),
   ],

   onLoadingLetsExchangeFixedOfferHigh:[
    _OffersAggregator.unsetLetsExchangeFixedError(),
    _OffersAggregator.unsetLetsExchangeFixedOffer(),
   ],
   onLoadingLetsExchangeFloatingOfferHigh:[
    _OffersAggregator.unsetLetsExchangeFloatingError(),
    _OffersAggregator.unsetLetsExchangeFloatingOffer(),
   ],

   onLoadingChangeNowFixedOfferHigh:[
    _OffersAggregator.unsetChangeNowFixedError(),
    _OffersAggregator.unsetChangeNowFixedOffer(),
   ],
   onLoadingChangeNowFloatingOfferHigh:[
    _OffersAggregator.unsetChangeNowFloatingError(),
    _OffersAggregator.unsetChangeNowFloatingOffer(),
   ]
  })
 ))
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQThkRyxTQUFTLEtBQUssRUFBRSxTQUFTLENBQUMsa0NBQWtDLENBQUM7Q0FDNUQsT0FBTyxDQUFDLFlBQUM7RUFDUjtHQUNDLGlDQUFrQztJQUNqQyxpQkFBaUIsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0lBQzVDLGlCQUFpQixDQUFDLHdCQUF3QixDQUFDLENBQUM7R0FDN0MsQ0FBRTtHQUNGLG9DQUFxQztJQUNwQyxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0lBQy9DLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDLENBQUM7R0FDaEQsQ0FBRTs7R0FFRixvQ0FBcUM7SUFDcEMsaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsQ0FBQztJQUMvQyxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0dBQ2hELENBQUU7R0FDRix1Q0FBd0M7SUFDdkMsaUJBQWlCLENBQUMsOEJBQThCLENBQUMsQ0FBQztJQUNsRCxpQkFBaUIsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0dBQ25ELENBQUU7O0dBRUYsaUNBQWtDO0lBQ2pDLGlCQUFpQixDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDNUMsaUJBQWlCLENBQUMsd0JBQXdCLENBQUMsQ0FBQztHQUM3QyxDQUFFO0dBQ0Ysb0NBQXFDO0lBQ3BDLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDLENBQUM7SUFDL0MsaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsQ0FBQztHQUNoRDtBQUNIO0NBQ0MsQ0FBa0M7QUFDbkMsQ0FBRiJ9x