
/**@type {xyz.swapee.wc.IOffersAggregatorDesigner._relay} */
export default function relay({This:This,OffersAggregator:_OffersAggregator,ExchangeIntent:ExchangeIntent}){
 return (h(Fragment,{},
  h(ExchangeIntent,{ onIntentChange:[
   _OffersAggregator.resetPort(),
  ] }),
  h(This,{
   onLoadingChangellyFixedOfferHigh:[
    _OffersAggregator.unsetChangellyFixedError(),
    // _OffersAggregator.unsetChangellyFixedOffer(),
   ],
   onLoadingChangellyFloatingOfferHigh:[
    _OffersAggregator.unsetChangellyFloatingError(),
    // _OffersAggregator.unsetChangellyFloatingOffer(),
   ],

   onLoadingLetsExchangeFixedOfferHigh:[
    _OffersAggregator.unsetLetsExchangeFixedError(),
    // _OffersAggregator.unsetLetsExchangeFixedOffer(),
   ],
   onLoadingLetsExchangeFloatingOfferHigh:[
    _OffersAggregator.unsetLetsExchangeFloatingError(),
    // _OffersAggregator.unsetLetsExchangeFloatingOffer(),
   ],

   onLoadingChangeNowFixedOfferHigh:[
    _OffersAggregator.unsetChangeNowFixedError(),
    // _OffersAggregator.unsetChangeNowFixedOffer(),
   ],
   onLoadingChangeNowFloatingOfferHigh:[
    _OffersAggregator.unsetChangeNowFloatingError(),
    // _OffersAggregator.unsetChangeNowFloatingOffer(),
   ]
  })
 ))
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWFnZ3JlZ2F0b3Ivb2ZmZXJzLWFnZ3JlZ2F0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQW1sQkcsU0FBUyxLQUFLLEVBQUUsU0FBUyxDQUFDLGtDQUFrQyxDQUFDLDZCQUE2QixDQUFDO0NBQzFGLE9BQU8sQ0FBQyxZQUFDO0VBQ1IsbUJBQWdCLGVBQWdCO0dBQy9CLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO0VBQzlCLENBQUU7RUFDRjtHQUNDLGlDQUFrQztJQUNqQyxpQkFBaUIsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0lBQzVDLEdBQUcsaUJBQWlCLENBQUMsd0JBQXdCLENBQUMsQ0FBQztHQUNoRCxDQUFFO0dBQ0Ysb0NBQXFDO0lBQ3BDLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDLENBQUM7SUFDL0MsR0FBRyxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0dBQ25ELENBQUU7O0dBRUYsb0NBQXFDO0lBQ3BDLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDLENBQUM7SUFDL0MsR0FBRyxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0dBQ25ELENBQUU7R0FDRix1Q0FBd0M7SUFDdkMsaUJBQWlCLENBQUMsOEJBQThCLENBQUMsQ0FBQztJQUNsRCxHQUFHLGlCQUFpQixDQUFDLDhCQUE4QixDQUFDLENBQUM7R0FDdEQsQ0FBRTs7R0FFRixpQ0FBa0M7SUFDakMsaUJBQWlCLENBQUMsd0JBQXdCLENBQUMsQ0FBQztJQUM1QyxHQUFHLGlCQUFpQixDQUFDLHdCQUF3QixDQUFDLENBQUM7R0FDaEQsQ0FBRTtHQUNGLG9DQUFxQztJQUNwQyxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0lBQy9DLEdBQUcsaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsQ0FBQztHQUNuRDtBQUNIO0NBQ0MsQ0FBa0M7QUFDbkMsQ0FBRiJ9