import AbstractOffersAggregatorProcessor from '../AbstractOffersAggregatorProcessor'
import {OffersAggregatorCore} from '../OffersAggregatorCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractOffersAggregatorComputer} from '../AbstractOffersAggregatorComputer'
import {AbstractOffersAggregatorController} from '../AbstractOffersAggregatorController'
import {regulateOffersAggregatorCache} from './methods/regulateOffersAggregatorCache'
import {OffersAggregatorCacheQPs} from '../../pqs/OffersAggregatorCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregator}
 */
function __AbstractOffersAggregator() {}
__AbstractOffersAggregator.prototype = /** @type {!_AbstractOffersAggregator} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregator}
 */
class _AbstractOffersAggregator { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractOffersAggregator} ‎
 */
class AbstractOffersAggregator extends newAbstract(
 _AbstractOffersAggregator,48900887579,null,{
  asIOffersAggregator:1,
  superOffersAggregator:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregator} */
AbstractOffersAggregator.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregator} */
function AbstractOffersAggregatorClass(){}

export default AbstractOffersAggregator


AbstractOffersAggregator[$implementations]=[
 __AbstractOffersAggregator,
 OffersAggregatorCore,
 AbstractOffersAggregatorProcessor,
 IntegratedComponent,
 AbstractOffersAggregatorComputer,
 AbstractOffersAggregatorController,
 AbstractOffersAggregatorClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregator}*/({
  regulateState:regulateOffersAggregatorCache,
  stateQPs:OffersAggregatorCacheQPs,
 }),
]


export {AbstractOffersAggregator}