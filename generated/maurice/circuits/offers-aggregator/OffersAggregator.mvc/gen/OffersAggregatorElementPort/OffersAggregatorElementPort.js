import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorElementPort}
 */
function __OffersAggregatorElementPort() {}
__OffersAggregatorElementPort.prototype = /** @type {!_OffersAggregatorElementPort} */ ({ })
/** @this {xyz.swapee.wc.OffersAggregatorElementPort} */ function OffersAggregatorElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IOffersAggregatorElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    amountInInOpts: {},
    amountOutInOpts: {},
    exchangeIntentOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorElementPort}
 */
class _OffersAggregatorElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorElementPort} ‎
 */
class OffersAggregatorElementPort extends newAbstract(
 _OffersAggregatorElementPort,489008875713,OffersAggregatorElementPortConstructor,{
  asIOffersAggregatorElementPort:1,
  superOffersAggregatorElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorElementPort} */
OffersAggregatorElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorElementPort} */
function OffersAggregatorElementPortClass(){}

export default OffersAggregatorElementPort


OffersAggregatorElementPort[$implementations]=[
 __OffersAggregatorElementPort,
 OffersAggregatorElementPortClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'amount-in-in-opts':undefined,
    'amount-out-in-opts':undefined,
    'exchange-intent-opts':undefined,
    'changelly-fixed-offer':undefined,
    'changelly-fixed-error':undefined,
    'changelly-fixed-min':undefined,
    'changelly-fixed-max':undefined,
    'changelly-floating-offer':undefined,
    'changelly-floating-error':undefined,
    'changelly-float-min':undefined,
    'changelly-float-max':undefined,
    'lets-exchange-fixed-offer':undefined,
    'lets-exchange-fixed-error':undefined,
    'lets-exchange-floating-offer':undefined,
    'lets-exchange-floating-error':undefined,
    'change-now-fixed-offer':undefined,
    'change-now-fixed-error':undefined,
    'change-now-floating-offer':undefined,
    'change-now-floating-error':undefined,
    'exchanges-total':undefined,
   })
  },
 }),
]