import AbstractOffersAggregatorScreenAT from '../AbstractOffersAggregatorScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorScreenBack}
 */
function __AbstractOffersAggregatorScreenBack() {}
__AbstractOffersAggregatorScreenBack.prototype = /** @type {!_AbstractOffersAggregatorScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorScreen}
 */
class _AbstractOffersAggregatorScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorScreen} ‎
 */
class AbstractOffersAggregatorScreenBack extends newAbstract(
 _AbstractOffersAggregatorScreenBack,489008875727,null,{
  asIOffersAggregatorScreen:1,
  superOffersAggregatorScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreen} */
AbstractOffersAggregatorScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreen} */
function AbstractOffersAggregatorScreenBackClass(){}

export default AbstractOffersAggregatorScreenBack


AbstractOffersAggregatorScreenBack[$implementations]=[
 __AbstractOffersAggregatorScreenBack,
 AbstractOffersAggregatorScreenAT,
]