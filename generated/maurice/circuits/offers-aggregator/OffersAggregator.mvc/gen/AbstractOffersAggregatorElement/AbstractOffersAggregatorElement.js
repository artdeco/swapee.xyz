import OffersAggregatorRenderVdus from './methods/render-vdus'
import OffersAggregatorElementPort from '../OffersAggregatorElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {OffersAggregatorInputsPQs} from '../../pqs/OffersAggregatorInputsPQs'
import {OffersAggregatorQueriesPQs} from '../../pqs/OffersAggregatorQueriesPQs'
import {OffersAggregatorCachePQs} from '../../pqs/OffersAggregatorCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractOffersAggregator from '../AbstractOffersAggregator'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorElement}
 */
function __AbstractOffersAggregatorElement() {}
__AbstractOffersAggregatorElement.prototype = /** @type {!_AbstractOffersAggregatorElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorElement}
 */
class _AbstractOffersAggregatorElement { }
/**
 * A component description.
 *
 * The _IOffersAggregator_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorElement} ‎
 */
class AbstractOffersAggregatorElement extends newAbstract(
 _AbstractOffersAggregatorElement,489008875712,null,{
  asIOffersAggregatorElement:1,
  superOffersAggregatorElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorElement} */
AbstractOffersAggregatorElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorElement} */
function AbstractOffersAggregatorElementClass(){}

export default AbstractOffersAggregatorElement


AbstractOffersAggregatorElement[$implementations]=[
 __AbstractOffersAggregatorElement,
 ElementBase,
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':changelly-fixed-offer':changellyFixedOfferColAttr,
   ':changelly-fixed-error':changellyFixedErrorColAttr,
   ':changelly-fixed-min':changellyFixedMinColAttr,
   ':changelly-fixed-max':changellyFixedMaxColAttr,
   ':changelly-floating-offer':changellyFloatingOfferColAttr,
   ':changelly-floating-error':changellyFloatingErrorColAttr,
   ':changelly-float-min':changellyFloatMinColAttr,
   ':changelly-float-max':changellyFloatMaxColAttr,
   ':lets-exchange-fixed-offer':letsExchangeFixedOfferColAttr,
   ':lets-exchange-fixed-error':letsExchangeFixedErrorColAttr,
   ':lets-exchange-floating-offer':letsExchangeFloatingOfferColAttr,
   ':lets-exchange-floating-error':letsExchangeFloatingErrorColAttr,
   ':change-now-fixed-offer':changeNowFixedOfferColAttr,
   ':change-now-fixed-error':changeNowFixedErrorColAttr,
   ':change-now-floating-offer':changeNowFloatingOfferColAttr,
   ':change-now-floating-error':changeNowFloatingErrorColAttr,
   ':exchanges-total':exchangesTotalColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'changelly-fixed-offer':changellyFixedOfferAttr,
    'changelly-fixed-error':changellyFixedErrorAttr,
    'changelly-fixed-min':changellyFixedMinAttr,
    'changelly-fixed-max':changellyFixedMaxAttr,
    'changelly-floating-offer':changellyFloatingOfferAttr,
    'changelly-floating-error':changellyFloatingErrorAttr,
    'changelly-float-min':changellyFloatMinAttr,
    'changelly-float-max':changellyFloatMaxAttr,
    'lets-exchange-fixed-offer':letsExchangeFixedOfferAttr,
    'lets-exchange-fixed-error':letsExchangeFixedErrorAttr,
    'lets-exchange-floating-offer':letsExchangeFloatingOfferAttr,
    'lets-exchange-floating-error':letsExchangeFloatingErrorAttr,
    'change-now-fixed-offer':changeNowFixedOfferAttr,
    'change-now-fixed-error':changeNowFixedErrorAttr,
    'change-now-floating-offer':changeNowFloatingOfferAttr,
    'change-now-floating-error':changeNowFloatingErrorAttr,
    'exchanges-total':exchangesTotalAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(changellyFixedOfferAttr===undefined?{'changelly-fixed-offer':changellyFixedOfferColAttr}:{}),
    ...(changellyFixedErrorAttr===undefined?{'changelly-fixed-error':changellyFixedErrorColAttr}:{}),
    ...(changellyFixedMinAttr===undefined?{'changelly-fixed-min':changellyFixedMinColAttr}:{}),
    ...(changellyFixedMaxAttr===undefined?{'changelly-fixed-max':changellyFixedMaxColAttr}:{}),
    ...(changellyFloatingOfferAttr===undefined?{'changelly-floating-offer':changellyFloatingOfferColAttr}:{}),
    ...(changellyFloatingErrorAttr===undefined?{'changelly-floating-error':changellyFloatingErrorColAttr}:{}),
    ...(changellyFloatMinAttr===undefined?{'changelly-float-min':changellyFloatMinColAttr}:{}),
    ...(changellyFloatMaxAttr===undefined?{'changelly-float-max':changellyFloatMaxColAttr}:{}),
    ...(letsExchangeFixedOfferAttr===undefined?{'lets-exchange-fixed-offer':letsExchangeFixedOfferColAttr}:{}),
    ...(letsExchangeFixedErrorAttr===undefined?{'lets-exchange-fixed-error':letsExchangeFixedErrorColAttr}:{}),
    ...(letsExchangeFloatingOfferAttr===undefined?{'lets-exchange-floating-offer':letsExchangeFloatingOfferColAttr}:{}),
    ...(letsExchangeFloatingErrorAttr===undefined?{'lets-exchange-floating-error':letsExchangeFloatingErrorColAttr}:{}),
    ...(changeNowFixedOfferAttr===undefined?{'change-now-fixed-offer':changeNowFixedOfferColAttr}:{}),
    ...(changeNowFixedErrorAttr===undefined?{'change-now-fixed-error':changeNowFixedErrorColAttr}:{}),
    ...(changeNowFloatingOfferAttr===undefined?{'change-now-floating-offer':changeNowFloatingOfferColAttr}:{}),
    ...(changeNowFloatingErrorAttr===undefined?{'change-now-floating-error':changeNowFloatingErrorColAttr}:{}),
    ...(exchangesTotalAttr===undefined?{'exchanges-total':exchangesTotalColAttr}:{}),
   }
  },
 }),
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'changelly-fixed-offer':changellyFixedOfferAttr,
   'changelly-fixed-error':changellyFixedErrorAttr,
   'changelly-fixed-min':changellyFixedMinAttr,
   'changelly-fixed-max':changellyFixedMaxAttr,
   'changelly-floating-offer':changellyFloatingOfferAttr,
   'changelly-floating-error':changellyFloatingErrorAttr,
   'changelly-float-min':changellyFloatMinAttr,
   'changelly-float-max':changellyFloatMaxAttr,
   'lets-exchange-fixed-offer':letsExchangeFixedOfferAttr,
   'lets-exchange-fixed-error':letsExchangeFixedErrorAttr,
   'lets-exchange-floating-offer':letsExchangeFloatingOfferAttr,
   'lets-exchange-floating-error':letsExchangeFloatingErrorAttr,
   'change-now-fixed-offer':changeNowFixedOfferAttr,
   'change-now-fixed-error':changeNowFixedErrorAttr,
   'change-now-floating-offer':changeNowFloatingOfferAttr,
   'change-now-floating-error':changeNowFloatingErrorAttr,
   'exchanges-total':exchangesTotalAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    changellyFixedOffer:changellyFixedOfferAttr,
    changellyFixedError:changellyFixedErrorAttr,
    changellyFixedMin:changellyFixedMinAttr,
    changellyFixedMax:changellyFixedMaxAttr,
    changellyFloatingOffer:changellyFloatingOfferAttr,
    changellyFloatingError:changellyFloatingErrorAttr,
    changellyFloatMin:changellyFloatMinAttr,
    changellyFloatMax:changellyFloatMaxAttr,
    letsExchangeFixedOffer:letsExchangeFixedOfferAttr,
    letsExchangeFixedError:letsExchangeFixedErrorAttr,
    letsExchangeFloatingOffer:letsExchangeFloatingOfferAttr,
    letsExchangeFloatingError:letsExchangeFloatingErrorAttr,
    changeNowFixedOffer:changeNowFixedOfferAttr,
    changeNowFixedError:changeNowFixedErrorAttr,
    changeNowFloatingOffer:changeNowFloatingOfferAttr,
    changeNowFloatingError:changeNowFloatingErrorAttr,
    exchangesTotal:exchangesTotalAttr,
   }
  },
 }),
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  render:OffersAggregatorRenderVdus,
 }),
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  inputsPQs:OffersAggregatorInputsPQs,
  queriesPQs:OffersAggregatorQueriesPQs,
  cachePQs:OffersAggregatorCachePQs,
  vdus:{
   'AmountInIn': 'ffdf1',
   'AmountOutIn': 'ffdf2',
   'ExchangeIntent': 'ffdf3',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','changellyFixedOffer','changellyFixedError','changellyFixedMin','changellyFixedMax','changellyFloatingOffer','changellyFloatingError','changellyFloatMin','changellyFloatMax','letsExchangeFixedOffer','letsExchangeFixedError','letsExchangeFloatingOffer','letsExchangeFloatingError','changeNowFixedOffer','changeNowFixedError','changeNowFloatingOffer','changeNowFloatingError','exchangesTotal','query:exchange-intent','no-solder',':no-solder','changelly-fixed-offer',':changelly-fixed-offer','changelly-fixed-error',':changelly-fixed-error','changelly-fixed-min',':changelly-fixed-min','changelly-fixed-max',':changelly-fixed-max','changelly-floating-offer',':changelly-floating-offer','changelly-floating-error',':changelly-floating-error','changelly-float-min',':changelly-float-min','changelly-float-max',':changelly-float-max','lets-exchange-fixed-offer',':lets-exchange-fixed-offer','lets-exchange-fixed-error',':lets-exchange-fixed-error','lets-exchange-floating-offer',':lets-exchange-floating-offer','lets-exchange-floating-error',':lets-exchange-floating-error','change-now-fixed-offer',':change-now-fixed-offer','change-now-fixed-error',':change-now-fixed-error','change-now-floating-offer',':change-now-floating-offer','change-now-floating-error',':change-now-floating-error','exchanges-total',':exchanges-total','fe646','bc2f8','7522b','c3b6b','7b5f4','5b11e','5617d','5ca90','2a73d','18c0d','17034','88705','26339','80525','a0ea0','821c9','b89f1','1c43c','9f4fb','17536','8062a','children']),
   })
  },
  get Port(){
   return OffersAggregatorElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:exchange-intent':exchangeIntentSel}){
   const _ret={}
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   return _ret
  },
 }),
 Landed,
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
   }
  },
 }),
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractOffersAggregatorElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorElement}*/({
  solder:(_,{
   exchangeIntentSel:exchangeIntentSel,
  })=>{
   return{
    exchangeIntentSel:exchangeIntentSel,
   }
  },
 }),
]



AbstractOffersAggregatorElement[$implementations]=[AbstractOffersAggregator,
 /** @type {!AbstractOffersAggregatorElement} */ ({
  rootId:'OffersAggregator',
  __$id:4890088757,
  fqn:'xyz.swapee.wc.IOffersAggregator',
  maurice_element_v3:true,
 }),
]