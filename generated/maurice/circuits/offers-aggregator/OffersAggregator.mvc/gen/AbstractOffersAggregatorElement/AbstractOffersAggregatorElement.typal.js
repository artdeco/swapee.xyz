
import AbstractOffersAggregator from '../AbstractOffersAggregator'

/** @abstract {xyz.swapee.wc.IOffersAggregatorElement} */
export default class AbstractOffersAggregatorElement { }



AbstractOffersAggregatorElement[$implementations]=[AbstractOffersAggregator,
 /** @type {!AbstractOffersAggregatorElement} */ ({
  rootId:'OffersAggregator',
  __$id:4890088757,
  fqn:'xyz.swapee.wc.IOffersAggregator',
  maurice_element_v3:true,
 }),
]