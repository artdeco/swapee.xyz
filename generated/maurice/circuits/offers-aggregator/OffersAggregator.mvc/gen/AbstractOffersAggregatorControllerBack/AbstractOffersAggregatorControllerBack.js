import AbstractOffersAggregatorControllerAR from '../AbstractOffersAggregatorControllerAR'
import {AbstractOffersAggregatorController} from '../AbstractOffersAggregatorController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorControllerBack}
 */
function __AbstractOffersAggregatorControllerBack() {}
__AbstractOffersAggregatorControllerBack.prototype = /** @type {!_AbstractOffersAggregatorControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorController}
 */
class _AbstractOffersAggregatorControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorController} ‎
 */
class AbstractOffersAggregatorControllerBack extends newAbstract(
 _AbstractOffersAggregatorControllerBack,489008875723,null,{
  asIOffersAggregatorController:1,
  superOffersAggregatorController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorController} */
AbstractOffersAggregatorControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorController} */
function AbstractOffersAggregatorControllerBackClass(){}

export default AbstractOffersAggregatorControllerBack


AbstractOffersAggregatorControllerBack[$implementations]=[
 __AbstractOffersAggregatorControllerBack,
 AbstractOffersAggregatorController,
 AbstractOffersAggregatorControllerAR,
 DriverBack,
]