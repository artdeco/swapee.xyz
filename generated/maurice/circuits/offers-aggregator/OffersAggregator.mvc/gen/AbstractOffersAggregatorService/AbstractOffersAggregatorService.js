import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorService}
 */
function __AbstractOffersAggregatorService() {}
__AbstractOffersAggregatorService.prototype = /** @type {!_AbstractOffersAggregatorService} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorService}
 */
class _AbstractOffersAggregatorService { }
/**
 * A service for the IOffersAggregator.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorService} ‎
 */
class AbstractOffersAggregatorService extends newAbstract(
 _AbstractOffersAggregatorService,489008875716,null,{
  asIOffersAggregatorService:1,
  superOffersAggregatorService:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorService} */
AbstractOffersAggregatorService.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorService} */
function AbstractOffersAggregatorServiceClass(){}

export default AbstractOffersAggregatorService


AbstractOffersAggregatorService[$implementations]=[
 __AbstractOffersAggregatorService,
]

export {AbstractOffersAggregatorService}