import AbstractOffersAggregatorDisplay from '../AbstractOffersAggregatorDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {OffersAggregatorVdusPQs} from '../../pqs/OffersAggregatorVdusPQs'
import {OffersAggregatorVdusQPs} from '../../pqs/OffersAggregatorVdusQPs'
import {OffersAggregatorMemoryPQs} from '../../pqs/OffersAggregatorMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorGPU}
 */
function __AbstractOffersAggregatorGPU() {}
__AbstractOffersAggregatorGPU.prototype = /** @type {!_AbstractOffersAggregatorGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorGPU}
 */
class _AbstractOffersAggregatorGPU { }
/**
 * Handles the periphery of the _IOffersAggregatorDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorGPU} ‎
 */
class AbstractOffersAggregatorGPU extends newAbstract(
 _AbstractOffersAggregatorGPU,489008875717,null,{
  asIOffersAggregatorGPU:1,
  superOffersAggregatorGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorGPU} */
AbstractOffersAggregatorGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorGPU} */
function AbstractOffersAggregatorGPUClass(){}

export default AbstractOffersAggregatorGPU


AbstractOffersAggregatorGPU[$implementations]=[
 __AbstractOffersAggregatorGPU,
 AbstractOffersAggregatorGPUClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorGPU}*/({
  vdusPQs:OffersAggregatorVdusPQs,
  vdusQPs:OffersAggregatorVdusQPs,
  memoryPQs:OffersAggregatorMemoryPQs,
 }),
 AbstractOffersAggregatorDisplay,
 BrowserView,
]