import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorControllerAR}
 */
function __AbstractOffersAggregatorControllerAR() {}
__AbstractOffersAggregatorControllerAR.prototype = /** @type {!_AbstractOffersAggregatorControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR}
 */
class _AbstractOffersAggregatorControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOffersAggregatorControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR} ‎
 */
class AbstractOffersAggregatorControllerAR extends newAbstract(
 _AbstractOffersAggregatorControllerAR,489008875724,null,{
  asIOffersAggregatorControllerAR:1,
  superOffersAggregatorControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR} */
AbstractOffersAggregatorControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorControllerAR} */
function AbstractOffersAggregatorControllerARClass(){}

export default AbstractOffersAggregatorControllerAR


AbstractOffersAggregatorControllerAR[$implementations]=[
 __AbstractOffersAggregatorControllerAR,
 AR,
 AbstractOffersAggregatorControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IOffersAggregatorControllerAR}*/({
  allocator(){
   this.methods={
    setChangellyFixedOffer:'0d602',
    unsetChangellyFixedOffer:'fd3bd',
    setChangellyFixedError:'500bd',
    unsetChangellyFixedError:'f3d2f',
    setChangellyFixedMin:'7805f',
    unsetChangellyFixedMin:'bf5a3',
    setChangellyFixedMax:'a47f4',
    unsetChangellyFixedMax:'1d6b7',
    setChangellyFloatingOffer:'b8e01',
    unsetChangellyFloatingOffer:'3acbd',
    setChangellyFloatingError:'a3855',
    unsetChangellyFloatingError:'bcaed',
    setChangellyFloatMin:'8f054',
    unsetChangellyFloatMin:'42a50',
    setChangellyFloatMax:'b32a2',
    unsetChangellyFloatMax:'3571e',
    setLetsExchangeFixedOffer:'5c117',
    unsetLetsExchangeFixedOffer:'c1b28',
    setLetsExchangeFixedError:'fc4b3',
    unsetLetsExchangeFixedError:'72e8a',
    setLetsExchangeFloatingOffer:'975a7',
    unsetLetsExchangeFloatingOffer:'6a275',
    setLetsExchangeFloatingError:'43747',
    unsetLetsExchangeFloatingError:'35afb',
    setChangeNowFixedOffer:'05fa9',
    unsetChangeNowFixedOffer:'5f233',
    setChangeNowFixedError:'ce6f2',
    unsetChangeNowFixedError:'6faaf',
    setChangeNowFloatingOffer:'4bfa1',
    unsetChangeNowFloatingOffer:'8c572',
    setChangeNowFloatingError:'472b5',
    unsetChangeNowFloatingError:'a6503',
    loadChangellyFloatingOffer:'89675',
    loadChangellyFixedOffer:'0bf66',
    loadLetsExchangeFloatingOffer:'3fc1d',
    loadLetsExchangeFixedOffer:'23d7c',
    loadChangeNowFloatingOffer:'2523b',
    loadChangeNowFixedOffer:'816ca',
   }
  },
 }),
]