import {makeBuffers} from '@webcircuits/webcircuits'

export const OffersAggregatorBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  changellyFixedOffer:Number,
  changellyFixedError:String,
  changellyFixedMin:Number,
  changellyFixedMax:Number,
  changellyFloatingOffer:Number,
  changellyFloatingError:String,
  changellyFloatMin:Number,
  changellyFloatMax:Number,
  letsExchangeFixedOffer:Number,
  letsExchangeFixedError:String,
  letsExchangeFloatingOffer:Number,
  letsExchangeFloatingError:String,
  changeNowFixedOffer:Number,
  changeNowFixedError:String,
  changeNowFloatingOffer:Number,
  changeNowFloatingError:String,
  exchangesTotal:Number,
 }),
})

export default OffersAggregatorBuffer