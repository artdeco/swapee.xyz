
/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptChangellyFloatingOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFloatingOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangellyFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptChangellyFixedOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangellyFixedOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangellyFixedOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptLetsExchangeFloatingOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFloatingOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLetsExchangeFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptLetsExchangeFixedOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLetsExchangeFixedOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLetsExchangeFixedOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptChangeNowFloatingOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFloatingOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangeNowFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptChangeNowFixedOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptChangeNowFixedOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangeNowFixedOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptMinAmount(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinAmount.Form}*/
 const _inputs={
  changellyFloatMin:inputs.changellyFloatMin,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptMinAmount(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptMinError(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMinError.Form}*/
 const _inputs={
  minAmount:inputs.minAmount,
  estimatedOut:inputs.estimatedOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptMinError(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptMaxAmount(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxAmount.Form}*/
 const _inputs={
  changellyFloatMax:inputs.changellyFloatMax,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptMaxAmount(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptMaxError(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptMaxError.Form}*/
 const _inputs={
  maxAmount:inputs.maxAmount,
  estimatedOut:inputs.estimatedOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptMaxError(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptEstimatedOut(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptEstimatedOut.Form}*/
 const _inputs={
  bestOffer:inputs.bestOffer,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptEstimatedOut(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptLoadingEstimate(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptLoadingEstimate.Form}*/
 const _inputs={
  isAggregating:inputs.isAggregating,
  estimatedOut:inputs.estimatedOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadingEstimate(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptBestAndWorstOffers(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptBestAndWorstOffers.Form}*/
 const _inputs={
  changellyFixedOffer:inputs.changellyFixedOffer,
  changellyFloatingOffer:inputs.changellyFloatingOffer,
  letsExchangeFixedOffer:inputs.letsExchangeFixedOffer,
  letsExchangeFloatingOffer:inputs.letsExchangeFloatingOffer,
  changeNowFixedOffer:inputs.changeNowFixedOffer,
  changeNowFloatingOffer:inputs.changeNowFloatingOffer,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptBestAndWorstOffers(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptExchangesLoaded(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptExchangesLoaded.Form}*/
 const _inputs={
  exchangesTotal:inputs.exchangesTotal,
  loadingChangellyFixedOffer:inputs.loadingChangellyFixedOffer,
  loadingChangellyFloatingOffer:inputs.loadingChangellyFloatingOffer,
  loadingLetsExchangeFixedOffer:inputs.loadingLetsExchangeFixedOffer,
  loadingLetsExchangeFloatingOffer:inputs.loadingLetsExchangeFloatingOffer,
  loadingChangeNowFixedOffer:inputs.loadingChangeNowFixedOffer,
  loadingChangeNowFloatingOffer:inputs.loadingChangeNowFloatingOffer,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.exchangesTotal) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptExchangesLoaded(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorComputer}*/
export function preadaptIsAggregating(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersAggregatorComputer.adaptIsAggregating.Form}*/
 const _inputs={
  loadingChangellyFloatingOffer:inputs.loadingChangellyFloatingOffer,
  loadingChangellyFixedOffer:inputs.loadingChangellyFixedOffer,
  loadingLetsExchangeFloatingOffer:inputs.loadingLetsExchangeFloatingOffer,
  loadingLetsExchangeFixedOffer:inputs.loadingLetsExchangeFixedOffer,
  loadingChangeNowFixedOffer:inputs.loadingChangeNowFixedOffer,
  loadingChangeNowFloatingOffer:inputs.loadingChangeNowFloatingOffer,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptIsAggregating(__inputs,__changes)
 return RET
}