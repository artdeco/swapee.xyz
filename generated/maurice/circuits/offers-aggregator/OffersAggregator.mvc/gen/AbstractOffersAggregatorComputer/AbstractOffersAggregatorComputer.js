import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorComputer}
 */
function __AbstractOffersAggregatorComputer() {}
__AbstractOffersAggregatorComputer.prototype = /** @type {!_AbstractOffersAggregatorComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorComputer}
 */
class _AbstractOffersAggregatorComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorComputer} ‎
 */
export class AbstractOffersAggregatorComputer extends newAbstract(
 _AbstractOffersAggregatorComputer,48900887571,null,{
  asIOffersAggregatorComputer:1,
  superOffersAggregatorComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorComputer} */
AbstractOffersAggregatorComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorComputer} */
function AbstractOffersAggregatorComputerClass(){}


AbstractOffersAggregatorComputer[$implementations]=[
 __AbstractOffersAggregatorComputer,
 Adapter,
]


export default AbstractOffersAggregatorComputer