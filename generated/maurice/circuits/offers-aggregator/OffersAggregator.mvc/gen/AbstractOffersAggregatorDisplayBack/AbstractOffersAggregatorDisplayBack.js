import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorDisplay}
 */
function __AbstractOffersAggregatorDisplay() {}
__AbstractOffersAggregatorDisplay.prototype = /** @type {!_AbstractOffersAggregatorDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorDisplay}
 */
class _AbstractOffersAggregatorDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorDisplay} ‎
 */
class AbstractOffersAggregatorDisplay extends newAbstract(
 _AbstractOffersAggregatorDisplay,489008875720,null,{
  asIOffersAggregatorDisplay:1,
  superOffersAggregatorDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorDisplay} */
AbstractOffersAggregatorDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorDisplay} */
function AbstractOffersAggregatorDisplayClass(){}

export default AbstractOffersAggregatorDisplay


AbstractOffersAggregatorDisplay[$implementations]=[
 __AbstractOffersAggregatorDisplay,
 GraphicsDriverBack,
 AbstractOffersAggregatorDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IOffersAggregatorDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IOffersAggregatorDisplay}*/({
    AmountInIn:twinMock,
    AmountOutIn:twinMock,
    ExchangeIntent:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.OffersAggregatorDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IOffersAggregatorDisplay.Initialese}*/({
   AmountInIn:1,
   AmountOutIn:1,
   ExchangeIntent:1,
  }),
  initializer({
   AmountInIn:_AmountInIn,
   AmountOutIn:_AmountOutIn,
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_AmountInIn!==undefined) this.AmountInIn=_AmountInIn
   if(_AmountOutIn!==undefined) this.AmountOutIn=_AmountOutIn
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]