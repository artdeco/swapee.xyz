import {preadaptLoadChangellyFloatingOffer,preadaptLoadChangellyFixedOffer,preadaptLoadLetsExchangeFloatingOffer,preadaptLoadLetsExchangeFixedOffer,preadaptLoadChangeNowFloatingOffer,preadaptLoadChangeNowFixedOffer} from './preadapters'
import {makeLoaders} from '@webcircuits/webcircuits'
import {StatefulLoader} from '@mauriceguest/guest2'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorRadio}
 */
function __AbstractOffersAggregatorRadio() {}
__AbstractOffersAggregatorRadio.prototype = /** @type {!_AbstractOffersAggregatorRadio} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorRadio}
 */
class _AbstractOffersAggregatorRadio { }
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorRadio} ‎
 */
class AbstractOffersAggregatorRadio extends newAbstract(
 _AbstractOffersAggregatorRadio,489008875714,null,{
  asIOffersAggregatorRadio:1,
  superOffersAggregatorRadio:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorRadio} */
AbstractOffersAggregatorRadio.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorRadio} */
function AbstractOffersAggregatorRadioClass(){}

export default AbstractOffersAggregatorRadio


AbstractOffersAggregatorRadio[$implementations]=[
 __AbstractOffersAggregatorRadio,
 makeLoaders(4890088757,{
  adaptLoadChangellyFloatingOffer:[
   '9ec7d',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
   },
   {
    changellyFloatingOffer:'2a73d',
    changellyFloatingError:'18c0d',
    changellyFloatMin:'17034',
    changellyFloatMax:'88705',
   },
   {
    loadingChangellyFloatingOffer:1,
    loadChangellyFloatingOfferError:2,
   },
  ],
  adaptLoadChangellyFixedOffer:[
   '542a9',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
   },
   {
    changellyFixedOffer:'7b5f4',
    changellyFixedError:'5b11e',
    changellyFixedMin:'5617d',
    changellyFixedMax:'5ca90',
   },
   {
    loadingChangellyFixedOffer:1,
    loadChangellyFixedOfferError:2,
   },
  ],
  adaptLoadLetsExchangeFloatingOffer:[
   'bfa1d',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
   },
   {
    letsExchangeFloatingOffer:'a0ea0',
    letsExchangeFloatingError:'821c9',
   },
   {
    loadingLetsExchangeFloatingOffer:1,
    loadLetsExchangeFloatingOfferError:2,
   },
  ],
  adaptLoadLetsExchangeFixedOffer:[
   '5a51e',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
   },
   {
    letsExchangeFixedOffer:'26339',
    letsExchangeFixedError:'80525',
   },
   {
    loadingLetsExchangeFixedOffer:1,
    loadLetsExchangeFixedOfferError:2,
   },
  ],
  adaptLoadChangeNowFloatingOffer:[
   'e5a7f',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
   },
   {
    changeNowFloatingOffer:'9f4fb',
    changeNowFloatingError:'17536',
   },
   {
    loadingChangeNowFloatingOffer:1,
    loadChangeNowFloatingOfferError:2,
   },
  ],
  adaptLoadChangeNowFixedOffer:[
   'a9da8',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
   },
   {
    changeNowFixedOffer:'b89f1',
    changeNowFixedError:'1c43c',
   },
   {
    loadingChangeNowFixedOffer:1,
    loadChangeNowFixedOfferError:2,
   },
  ],
 }),
 StatefulLoader,
 {adapt:[preadaptLoadChangellyFloatingOffer,preadaptLoadChangellyFixedOffer,preadaptLoadLetsExchangeFloatingOffer,preadaptLoadLetsExchangeFixedOffer,preadaptLoadChangeNowFloatingOffer,preadaptLoadChangeNowFixedOffer]},
]