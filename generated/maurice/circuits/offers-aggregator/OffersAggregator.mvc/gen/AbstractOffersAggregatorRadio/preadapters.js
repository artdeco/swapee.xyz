
/**@this {xyz.swapee.wc.IOffersAggregatorRadio}*/
export function preadaptLoadChangellyFloatingOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFloatingOffer.Form}*/
 const _inputs={
  host:inputs.host,
  region:inputs.region,
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadChangellyFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorRadio}*/
export function preadaptLoadChangellyFixedOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangellyFixedOffer.Form}*/
 const _inputs={
  host:inputs.host,
  region:inputs.region,
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadChangellyFixedOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorRadio}*/
export function preadaptLoadLetsExchangeFloatingOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFloatingOffer.Form}*/
 const _inputs={
  host:inputs.host,
  region:inputs.region,
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadLetsExchangeFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorRadio}*/
export function preadaptLoadLetsExchangeFixedOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadLetsExchangeFixedOffer.Form}*/
 const _inputs={
  host:inputs.host,
  region:inputs.region,
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadLetsExchangeFixedOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorRadio}*/
export function preadaptLoadChangeNowFloatingOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFloatingOffer.Form}*/
 const _inputs={
  host:inputs.host,
  region:inputs.region,
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadChangeNowFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOffersAggregatorRadio}*/
export function preadaptLoadChangeNowFixedOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent}=this.land
 if(!ExchangeIntent) return
 /**@type {!xyz.swapee.wc.IOffersAggregatorRadio.adaptLoadChangeNowFixedOffer.Form}*/
 const _inputs={
  host:inputs.host,
  region:inputs.region,
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadChangeNowFixedOffer(__inputs,__changes)
 return RET
}