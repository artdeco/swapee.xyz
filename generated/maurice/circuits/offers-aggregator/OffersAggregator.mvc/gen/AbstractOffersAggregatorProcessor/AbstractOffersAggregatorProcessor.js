import {preadaptLoadChangellyFloatingOffer,preadaptLoadChangellyFixedOffer,preadaptLoadLetsExchangeFloatingOffer,preadaptLoadLetsExchangeFixedOffer,preadaptLoadChangeNowFloatingOffer,preadaptLoadChangeNowFixedOffer} from '../AbstractOffersAggregatorRadio/preadapters'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorProcessor}
 */
function __AbstractOffersAggregatorProcessor() {}
__AbstractOffersAggregatorProcessor.prototype = /** @type {!_AbstractOffersAggregatorProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorProcessor}
 */
class _AbstractOffersAggregatorProcessor { }
/**
 * The processor to compute changes to the memory for the _IOffersAggregator_.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorProcessor} ‎
 */
class AbstractOffersAggregatorProcessor extends newAbstract(
 _AbstractOffersAggregatorProcessor,48900887578,null,{
  asIOffersAggregatorProcessor:1,
  superOffersAggregatorProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorProcessor} */
AbstractOffersAggregatorProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorProcessor} */
function AbstractOffersAggregatorProcessorClass(){}

export default AbstractOffersAggregatorProcessor


AbstractOffersAggregatorProcessor[$implementations]=[
 __AbstractOffersAggregatorProcessor,
 AbstractOffersAggregatorProcessorClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorProcessor}*/({
  loadChangellyFloatingOffer(){
   return preadaptLoadChangellyFloatingOffer.call(this,this.model,{}).then(this.setInfo)
  },
  loadChangellyFixedOffer(){
   return preadaptLoadChangellyFixedOffer.call(this,this.model,{}).then(this.setInfo)
  },
  loadLetsExchangeFloatingOffer(){
   return preadaptLoadLetsExchangeFloatingOffer.call(this,this.model,{}).then(this.setInfo)
  },
  loadLetsExchangeFixedOffer(){
   return preadaptLoadLetsExchangeFixedOffer.call(this,this.model,{}).then(this.setInfo)
  },
  loadChangeNowFloatingOffer(){
   return preadaptLoadChangeNowFloatingOffer.call(this,this.model,{}).then(this.setInfo)
  },
  loadChangeNowFixedOffer(){
   return preadaptLoadChangeNowFixedOffer.call(this,this.model,{}).then(this.setInfo)
  },
 }),
]