import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {OffersAggregatorInputsPQs} from '../../pqs/OffersAggregatorInputsPQs'
import {OffersAggregatorOuterCoreConstructor} from '../OffersAggregatorCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorPort}
 */
function __OffersAggregatorPort() {}
__OffersAggregatorPort.prototype = /** @type {!_OffersAggregatorPort} */ ({ })
/** @this {xyz.swapee.wc.OffersAggregatorPort} */ function OffersAggregatorPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.OffersAggregatorOuterCore} */ ({model:null})
  OffersAggregatorOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorPort}
 */
class _OffersAggregatorPort { }
/**
 * The port that serves as an interface to the _IOffersAggregator_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorPort} ‎
 */
export class OffersAggregatorPort extends newAbstract(
 _OffersAggregatorPort,48900887575,OffersAggregatorPortConstructor,{
  asIOffersAggregatorPort:1,
  superOffersAggregatorPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorPort} */
OffersAggregatorPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorPort} */
function OffersAggregatorPortClass(){}

export const OffersAggregatorPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IOffersAggregator.Pinout>}*/({
 get Port() { return OffersAggregatorPort },
})

OffersAggregatorPort[$implementations]=[
 OffersAggregatorPortClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorPort}*/({
  resetPort(){
   this.resetOffersAggregatorPort()
  },
  resetOffersAggregatorPort(){
   OffersAggregatorPortConstructor.call(this)
  },
 }),
 __OffersAggregatorPort,
 Parametric,
 OffersAggregatorPortClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorPort}*/({
  constructor(){
   mountPins(this.inputs,OffersAggregatorInputsPQs)
  },
 }),
]


export default OffersAggregatorPort