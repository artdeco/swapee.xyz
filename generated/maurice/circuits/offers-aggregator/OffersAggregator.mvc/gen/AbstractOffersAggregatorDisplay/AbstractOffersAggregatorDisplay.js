import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorDisplay}
 */
function __AbstractOffersAggregatorDisplay() {}
__AbstractOffersAggregatorDisplay.prototype = /** @type {!_AbstractOffersAggregatorDisplay} */ ({ })
/** @this {xyz.swapee.wc.OffersAggregatorDisplay} */ function OffersAggregatorDisplayConstructor() {
  /** @type {HTMLInputElement} */ this.AmountInIn=null
  /** @type {HTMLInputElement} */ this.AmountOutIn=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorDisplay}
 */
class _AbstractOffersAggregatorDisplay { }
/**
 * Display for presenting information from the _IOffersAggregator_.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorDisplay} ‎
 */
class AbstractOffersAggregatorDisplay extends newAbstract(
 _AbstractOffersAggregatorDisplay,489008875718,OffersAggregatorDisplayConstructor,{
  asIOffersAggregatorDisplay:1,
  superOffersAggregatorDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorDisplay} */
AbstractOffersAggregatorDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorDisplay} */
function AbstractOffersAggregatorDisplayClass(){}

export default AbstractOffersAggregatorDisplay


AbstractOffersAggregatorDisplay[$implementations]=[
 __AbstractOffersAggregatorDisplay,
 Display,
 AbstractOffersAggregatorDisplayClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{exchangeIntentScopeSel:exchangeIntentScopeSel}}=this
    this.scan({
     exchangeIntentSel:exchangeIntentScopeSel,
    })
   })
  },
  scan:function vduScan({exchangeIntentSel:exchangeIntentSelScope}){
   const{element:element,asIOffersAggregatorScreen:{vdusPQs:{
    AmountInIn:AmountInIn,
    AmountOutIn:AmountOutIn,
   }},queries:{exchangeIntentSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   Object.assign(this,{
    AmountInIn:/**@type {HTMLInputElement}*/(children[AmountInIn]),
    AmountOutIn:/**@type {HTMLInputElement}*/(children[AmountOutIn]),
    ExchangeIntent:ExchangeIntent,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.OffersAggregatorDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IOffersAggregatorDisplay.Initialese}*/({
   AmountInIn:1,
   AmountOutIn:1,
   ExchangeIntent:1,
  }),
  initializer({
   AmountInIn:_AmountInIn,
   AmountOutIn:_AmountOutIn,
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_AmountInIn!==undefined) this.AmountInIn=_AmountInIn
   if(_AmountOutIn!==undefined) this.AmountOutIn=_AmountOutIn
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]