import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorScreenAR}
 */
function __AbstractOffersAggregatorScreenAR() {}
__AbstractOffersAggregatorScreenAR.prototype = /** @type {!_AbstractOffersAggregatorScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR}
 */
class _AbstractOffersAggregatorScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOffersAggregatorScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR} ‎
 */
class AbstractOffersAggregatorScreenAR extends newAbstract(
 _AbstractOffersAggregatorScreenAR,489008875728,null,{
  asIOffersAggregatorScreenAR:1,
  superOffersAggregatorScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR} */
AbstractOffersAggregatorScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOffersAggregatorScreenAR} */
function AbstractOffersAggregatorScreenARClass(){}

export default AbstractOffersAggregatorScreenAR


AbstractOffersAggregatorScreenAR[$implementations]=[
 __AbstractOffersAggregatorScreenAR,
 AR,
 AbstractOffersAggregatorScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IOffersAggregatorScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractOffersAggregatorScreenAR}