import AbstractOffersAggregatorScreenAR from '../AbstractOffersAggregatorScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {OffersAggregatorInputsPQs} from '../../pqs/OffersAggregatorInputsPQs'
import {OffersAggregatorQueriesPQs} from '../../pqs/OffersAggregatorQueriesPQs'
import {OffersAggregatorMemoryQPs} from '../../pqs/OffersAggregatorMemoryQPs'
import {OffersAggregatorCacheQPs} from '../../pqs/OffersAggregatorCacheQPs'
import {OffersAggregatorVdusPQs} from '../../pqs/OffersAggregatorVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorScreen}
 */
function __AbstractOffersAggregatorScreen() {}
__AbstractOffersAggregatorScreen.prototype = /** @type {!_AbstractOffersAggregatorScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorScreen}
 */
class _AbstractOffersAggregatorScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorScreen} ‎
 */
class AbstractOffersAggregatorScreen extends newAbstract(
 _AbstractOffersAggregatorScreen,489008875726,null,{
  asIOffersAggregatorScreen:1,
  superOffersAggregatorScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorScreen} */
AbstractOffersAggregatorScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorScreen} */
function AbstractOffersAggregatorScreenClass(){}

export default AbstractOffersAggregatorScreen


AbstractOffersAggregatorScreen[$implementations]=[
 __AbstractOffersAggregatorScreen,
 AbstractOffersAggregatorScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorScreen}*/({
  inputsPQs:OffersAggregatorInputsPQs,
  queriesPQs:OffersAggregatorQueriesPQs,
  memoryQPs:OffersAggregatorMemoryQPs,
  cacheQPs:OffersAggregatorCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractOffersAggregatorScreenAR,
 AbstractOffersAggregatorScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorScreen}*/({
  vdusPQs:OffersAggregatorVdusPQs,
 }),
]