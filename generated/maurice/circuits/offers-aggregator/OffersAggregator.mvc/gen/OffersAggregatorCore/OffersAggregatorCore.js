import {mountPins} from '@type.engineering/seers'
import {OffersAggregatorMemoryPQs} from '../../pqs/OffersAggregatorMemoryPQs'
import {OffersAggregatorCachePQs} from '../../pqs/OffersAggregatorCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorCore}
 */
function __OffersAggregatorCore() {}
__OffersAggregatorCore.prototype = /** @type {!_OffersAggregatorCore} */ ({ })
/** @this {xyz.swapee.wc.OffersAggregatorCore} */ function OffersAggregatorCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOffersAggregatorCore.Model}*/
  this.model={
    bestOffer: null,
    worstOffer: null,
    minAmount: null,
    minError: null,
    maxAmount: null,
    maxError: null,
    estimatedOut: null,
    loadingEstimate: false,
    allExchangesLoaded: false,
    exchangesLoaded: 0,
    host: '',
    isAggregating: false,
    loadingChangellyFloatingOffer: false,
    hasMoreChangellyFloatingOffer: null,
    loadChangellyFloatingOfferError: null,
    loadingChangellyFixedOffer: false,
    hasMoreChangellyFixedOffer: null,
    loadChangellyFixedOfferError: null,
    loadingLetsExchangeFloatingOffer: false,
    hasMoreLetsExchangeFloatingOffer: null,
    loadLetsExchangeFloatingOfferError: null,
    loadingLetsExchangeFixedOffer: false,
    hasMoreLetsExchangeFixedOffer: null,
    loadLetsExchangeFixedOfferError: null,
    loadingChangeNowFloatingOffer: false,
    hasMoreChangeNowFloatingOffer: null,
    loadChangeNowFloatingOfferError: null,
    loadingChangeNowFixedOffer: false,
    hasMoreChangeNowFixedOffer: null,
    loadChangeNowFixedOfferError: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorCore}
 */
class _OffersAggregatorCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorCore} ‎
 */
class OffersAggregatorCore extends newAbstract(
 _OffersAggregatorCore,48900887577,OffersAggregatorCoreConstructor,{
  asIOffersAggregatorCore:1,
  superOffersAggregatorCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorCore} */
OffersAggregatorCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorCore} */
function OffersAggregatorCoreClass(){}

export default OffersAggregatorCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorOuterCore}
 */
function __OffersAggregatorOuterCore() {}
__OffersAggregatorOuterCore.prototype = /** @type {!_OffersAggregatorOuterCore} */ ({ })
/** @this {xyz.swapee.wc.OffersAggregatorOuterCore} */
export function OffersAggregatorOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOffersAggregatorOuterCore.Model}*/
  this.model={
    changellyFixedOffer: null,
    changellyFixedError: '',
    changellyFixedMin: 0,
    changellyFixedMax: 0,
    changellyFloatingOffer: null,
    changellyFloatingError: '',
    changellyFloatMin: null,
    changellyFloatMax: null,
    letsExchangeFixedOffer: null,
    letsExchangeFixedError: '',
    letsExchangeFloatingOffer: null,
    letsExchangeFloatingError: '',
    changeNowFixedOffer: null,
    changeNowFixedError: '',
    changeNowFloatingOffer: null,
    changeNowFloatingError: '',
    exchangesTotal: 6,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorOuterCore}
 */
class _OffersAggregatorOuterCore { }
/**
 * The _IOffersAggregator_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorOuterCore} ‎
 */
export class OffersAggregatorOuterCore extends newAbstract(
 _OffersAggregatorOuterCore,48900887573,OffersAggregatorOuterCoreConstructor,{
  asIOffersAggregatorOuterCore:1,
  superOffersAggregatorOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorOuterCore} */
OffersAggregatorOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorOuterCore} */
function OffersAggregatorOuterCoreClass(){}


OffersAggregatorOuterCore[$implementations]=[
 __OffersAggregatorOuterCore,
 OffersAggregatorOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorOuterCore}*/({
  constructor(){
   mountPins(this.model,OffersAggregatorMemoryPQs)
   mountPins(this.model,OffersAggregatorCachePQs)
  },
 }),
]

OffersAggregatorCore[$implementations]=[
 OffersAggregatorCoreClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorCore}*/({
  resetCore(){
   this.resetOffersAggregatorCore()
  },
  resetOffersAggregatorCore(){
   OffersAggregatorCoreConstructor.call(
    /**@type {xyz.swapee.wc.OffersAggregatorCore}*/(this),
   )
   OffersAggregatorOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.OffersAggregatorOuterCore}*/(
     /**@type {!xyz.swapee.wc.IOffersAggregatorOuterCore}*/(this)),
   )
  },
 }),
 __OffersAggregatorCore,
 OffersAggregatorOuterCore,
]

export {OffersAggregatorCore}