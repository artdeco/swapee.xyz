import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorScreenAT}
 */
function __AbstractOffersAggregatorScreenAT() {}
__AbstractOffersAggregatorScreenAT.prototype = /** @type {!_AbstractOffersAggregatorScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT}
 */
class _AbstractOffersAggregatorScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOffersAggregatorScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT} ‎
 */
class AbstractOffersAggregatorScreenAT extends newAbstract(
 _AbstractOffersAggregatorScreenAT,489008875729,null,{
  asIOffersAggregatorScreenAT:1,
  superOffersAggregatorScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT} */
AbstractOffersAggregatorScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersAggregatorScreenAT} */
function AbstractOffersAggregatorScreenATClass(){}

export default AbstractOffersAggregatorScreenAT


AbstractOffersAggregatorScreenAT[$implementations]=[
 __AbstractOffersAggregatorScreenAT,
 UartUniversal,
]