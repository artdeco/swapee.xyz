import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorControllerAT}
 */
function __AbstractOffersAggregatorControllerAT() {}
__AbstractOffersAggregatorControllerAT.prototype = /** @type {!_AbstractOffersAggregatorControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT}
 */
class _AbstractOffersAggregatorControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOffersAggregatorControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT} ‎
 */
class AbstractOffersAggregatorControllerAT extends newAbstract(
 _AbstractOffersAggregatorControllerAT,489008875725,null,{
  asIOffersAggregatorControllerAT:1,
  superOffersAggregatorControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT} */
AbstractOffersAggregatorControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOffersAggregatorControllerAT} */
function AbstractOffersAggregatorControllerATClass(){}

export default AbstractOffersAggregatorControllerAT


AbstractOffersAggregatorControllerAT[$implementations]=[
 __AbstractOffersAggregatorControllerAT,
 UartUniversal,
 AbstractOffersAggregatorControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IOffersAggregatorControllerAT}*/({
  get asIOffersAggregatorController(){
   return this
  },
  setChangellyFixedOffer(){
   this.uart.t("inv",{mid:'0d602',args:[...arguments]})
  },
  unsetChangellyFixedOffer(){
   this.uart.t("inv",{mid:'fd3bd'})
  },
  setChangellyFixedError(){
   this.uart.t("inv",{mid:'500bd',args:[...arguments]})
  },
  unsetChangellyFixedError(){
   this.uart.t("inv",{mid:'f3d2f'})
  },
  setChangellyFixedMin(){
   this.uart.t("inv",{mid:'7805f',args:[...arguments]})
  },
  unsetChangellyFixedMin(){
   this.uart.t("inv",{mid:'bf5a3'})
  },
  setChangellyFixedMax(){
   this.uart.t("inv",{mid:'a47f4',args:[...arguments]})
  },
  unsetChangellyFixedMax(){
   this.uart.t("inv",{mid:'1d6b7'})
  },
  setChangellyFloatingOffer(){
   this.uart.t("inv",{mid:'b8e01',args:[...arguments]})
  },
  unsetChangellyFloatingOffer(){
   this.uart.t("inv",{mid:'3acbd'})
  },
  setChangellyFloatingError(){
   this.uart.t("inv",{mid:'a3855',args:[...arguments]})
  },
  unsetChangellyFloatingError(){
   this.uart.t("inv",{mid:'bcaed'})
  },
  setChangellyFloatMin(){
   this.uart.t("inv",{mid:'8f054',args:[...arguments]})
  },
  unsetChangellyFloatMin(){
   this.uart.t("inv",{mid:'42a50'})
  },
  setChangellyFloatMax(){
   this.uart.t("inv",{mid:'b32a2',args:[...arguments]})
  },
  unsetChangellyFloatMax(){
   this.uart.t("inv",{mid:'3571e'})
  },
  setLetsExchangeFixedOffer(){
   this.uart.t("inv",{mid:'5c117',args:[...arguments]})
  },
  unsetLetsExchangeFixedOffer(){
   this.uart.t("inv",{mid:'c1b28'})
  },
  setLetsExchangeFixedError(){
   this.uart.t("inv",{mid:'fc4b3',args:[...arguments]})
  },
  unsetLetsExchangeFixedError(){
   this.uart.t("inv",{mid:'72e8a'})
  },
  setLetsExchangeFloatingOffer(){
   this.uart.t("inv",{mid:'975a7',args:[...arguments]})
  },
  unsetLetsExchangeFloatingOffer(){
   this.uart.t("inv",{mid:'6a275'})
  },
  setLetsExchangeFloatingError(){
   this.uart.t("inv",{mid:'43747',args:[...arguments]})
  },
  unsetLetsExchangeFloatingError(){
   this.uart.t("inv",{mid:'35afb'})
  },
  setChangeNowFixedOffer(){
   this.uart.t("inv",{mid:'05fa9',args:[...arguments]})
  },
  unsetChangeNowFixedOffer(){
   this.uart.t("inv",{mid:'5f233'})
  },
  setChangeNowFixedError(){
   this.uart.t("inv",{mid:'ce6f2',args:[...arguments]})
  },
  unsetChangeNowFixedError(){
   this.uart.t("inv",{mid:'6faaf'})
  },
  setChangeNowFloatingOffer(){
   this.uart.t("inv",{mid:'4bfa1',args:[...arguments]})
  },
  unsetChangeNowFloatingOffer(){
   this.uart.t("inv",{mid:'8c572'})
  },
  setChangeNowFloatingError(){
   this.uart.t("inv",{mid:'472b5',args:[...arguments]})
  },
  unsetChangeNowFloatingError(){
   this.uart.t("inv",{mid:'a6503'})
  },
  loadChangellyFloatingOffer(){
   this.uart.t("inv",{mid:'89675'})
  },
  loadChangellyFixedOffer(){
   this.uart.t("inv",{mid:'0bf66'})
  },
  loadLetsExchangeFloatingOffer(){
   this.uart.t("inv",{mid:'3fc1d'})
  },
  loadLetsExchangeFixedOffer(){
   this.uart.t("inv",{mid:'23d7c'})
  },
  loadChangeNowFloatingOffer(){
   this.uart.t("inv",{mid:'2523b'})
  },
  loadChangeNowFixedOffer(){
   this.uart.t("inv",{mid:'816ca'})
  },
 }),
]