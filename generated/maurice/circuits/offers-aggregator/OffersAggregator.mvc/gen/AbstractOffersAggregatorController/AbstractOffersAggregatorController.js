import OffersAggregatorBuffer from '../OffersAggregatorBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {OffersAggregatorPortConnector} from '../OffersAggregatorPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorController}
 */
function __AbstractOffersAggregatorController() {}
__AbstractOffersAggregatorController.prototype = /** @type {!_AbstractOffersAggregatorController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorController}
 */
class _AbstractOffersAggregatorController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorController} ‎
 */
export class AbstractOffersAggregatorController extends newAbstract(
 _AbstractOffersAggregatorController,489008875721,null,{
  asIOffersAggregatorController:1,
  superOffersAggregatorController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorController} */
AbstractOffersAggregatorController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorController} */
function AbstractOffersAggregatorControllerClass(){}


AbstractOffersAggregatorController[$implementations]=[
 AbstractOffersAggregatorControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IOffersAggregatorPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractOffersAggregatorController,
 OffersAggregatorBuffer,
 IntegratedController,
 /**@type {!AbstractOffersAggregatorController}*/(OffersAggregatorPortConnector),
 AbstractOffersAggregatorControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorController}*/({
  setChangellyFixedOffer(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedOffer:val})
  },
  setChangellyFixedError(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedError:val})
  },
  setChangellyFixedMin(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedMin:val})
  },
  setChangellyFixedMax(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedMax:val})
  },
  setChangellyFloatingOffer(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatingOffer:val})
  },
  setChangellyFloatingError(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatingError:val})
  },
  setChangellyFloatMin(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatMin:val})
  },
  setChangellyFloatMax(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatMax:val})
  },
  setLetsExchangeFixedOffer(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFixedOffer:val})
  },
  setLetsExchangeFixedError(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFixedError:val})
  },
  setLetsExchangeFloatingOffer(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFloatingOffer:val})
  },
  setLetsExchangeFloatingError(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFloatingError:val})
  },
  setChangeNowFixedOffer(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFixedOffer:val})
  },
  setChangeNowFixedError(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFixedError:val})
  },
  setChangeNowFloatingOffer(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFloatingOffer:val})
  },
  setChangeNowFloatingError(val){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFloatingError:val})
  },
  unsetChangellyFixedOffer(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedOffer:null})
  },
  unsetChangellyFixedError(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedError:''})
  },
  unsetChangellyFixedMin(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedMin:null})
  },
  unsetChangellyFixedMax(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFixedMax:null})
  },
  unsetChangellyFloatingOffer(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatingOffer:null})
  },
  unsetChangellyFloatingError(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatingError:''})
  },
  unsetChangellyFloatMin(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatMin:null})
  },
  unsetChangellyFloatMax(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changellyFloatMax:null})
  },
  unsetLetsExchangeFixedOffer(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFixedOffer:null})
  },
  unsetLetsExchangeFixedError(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFixedError:''})
  },
  unsetLetsExchangeFloatingOffer(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFloatingOffer:null})
  },
  unsetLetsExchangeFloatingError(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({letsExchangeFloatingError:''})
  },
  unsetChangeNowFixedOffer(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFixedOffer:null})
  },
  unsetChangeNowFixedError(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFixedError:''})
  },
  unsetChangeNowFloatingOffer(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFloatingOffer:null})
  },
  unsetChangeNowFloatingError(){
   const{asIOffersAggregatorController:{setInputs:setInputs}}=this
   setInputs({changeNowFloatingError:''})
  },
 }),
]


export default AbstractOffersAggregatorController