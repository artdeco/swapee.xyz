import AbstractOffersAggregatorGPU from '../AbstractOffersAggregatorGPU'
import AbstractOffersAggregatorScreenBack from '../AbstractOffersAggregatorScreenBack'
import AbstractOffersAggregatorRadio from '../AbstractOffersAggregatorRadio'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {OffersAggregatorInputsQPs} from '../../pqs/OffersAggregatorInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractOffersAggregator from '../AbstractOffersAggregator'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorHtmlComponent}
 */
function __AbstractOffersAggregatorHtmlComponent() {}
__AbstractOffersAggregatorHtmlComponent.prototype = /** @type {!_AbstractOffersAggregatorHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent}
 */
class _AbstractOffersAggregatorHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.OffersAggregatorHtmlComponent} */ (res)
  }
}
/**
 * The _IOffersAggregator_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent} ‎
 */
export class AbstractOffersAggregatorHtmlComponent extends newAbstract(
 _AbstractOffersAggregatorHtmlComponent,489008875711,null,{
  asIOffersAggregatorHtmlComponent:1,
  superOffersAggregatorHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent} */
AbstractOffersAggregatorHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersAggregatorHtmlComponent} */
function AbstractOffersAggregatorHtmlComponentClass(){}


AbstractOffersAggregatorHtmlComponent[$implementations]=[
 __AbstractOffersAggregatorHtmlComponent,
 HtmlComponent,
 AbstractOffersAggregator,
 AbstractOffersAggregatorGPU,
 AbstractOffersAggregatorScreenBack,
 AbstractOffersAggregatorRadio,
 Landed,
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
   }
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  inputsQPs:OffersAggregatorInputsQPs,
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint:function paint_value_on_AmountInIn({amountIn:amountIn}){
   const{asIOffersAggregatorGPU:{AmountInIn:AmountInIn}}=this
   AmountInIn.val(amountIn)
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint:function paint_value_on_AmountOutIn({estimatedOut:estimatedOut}){
   const{asIOffersAggregatorGPU:{AmountOutIn:AmountOutIn}}=this
   AmountOutIn.val(estimatedOut)
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIOffersAggregatorGPU:{
     ExchangeIntent:ExchangeIntent,
    },
   }=this
   complete(7833868048,{ExchangeIntent:ExchangeIntent})
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   const{asIOffersAggregatorController:{
    resetPort:resetPort,
   }}=this
   ExchangeIntent['bdddf']=()=>{resetPort()} // onIntentChange -> resetPort
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint({loadingChangellyFixedOffer:loadingChangellyFixedOffer}){
   const{asIOffersAggregatorController:{
    unsetChangellyFixedError:unsetChangellyFixedError,
   }}=this
   if(loadingChangellyFixedOffer) {
    unsetChangellyFixedError()
   }
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint({loadingChangellyFloatingOffer:loadingChangellyFloatingOffer}){
   const{asIOffersAggregatorController:{
    unsetChangellyFloatingError:unsetChangellyFloatingError,
   }}=this
   if(loadingChangellyFloatingOffer) {
    unsetChangellyFloatingError()
   }
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint({loadingLetsExchangeFixedOffer:loadingLetsExchangeFixedOffer}){
   const{asIOffersAggregatorController:{
    unsetLetsExchangeFixedError:unsetLetsExchangeFixedError,
   }}=this
   if(loadingLetsExchangeFixedOffer) {
    unsetLetsExchangeFixedError()
   }
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint({loadingLetsExchangeFloatingOffer:loadingLetsExchangeFloatingOffer}){
   const{asIOffersAggregatorController:{
    unsetLetsExchangeFloatingError:unsetLetsExchangeFloatingError,
   }}=this
   if(loadingLetsExchangeFloatingOffer) {
    unsetLetsExchangeFloatingError()
   }
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint({loadingChangeNowFixedOffer:loadingChangeNowFixedOffer}){
   const{asIOffersAggregatorController:{
    unsetChangeNowFixedError:unsetChangeNowFixedError,
   }}=this
   if(loadingChangeNowFixedOffer) {
    unsetChangeNowFixedError()
   }
  },
 }),
 AbstractOffersAggregatorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent}*/({
  paint({loadingChangeNowFloatingOffer:loadingChangeNowFloatingOffer}){
   const{asIOffersAggregatorController:{
    unsetChangeNowFloatingError:unsetChangeNowFloatingError,
   }}=this
   if(loadingChangeNowFloatingOffer) {
    unsetChangeNowFloatingError()
   }
  },
 }),
]