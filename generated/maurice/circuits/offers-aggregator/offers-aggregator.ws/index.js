const d=new Date
let _OffersAggregatorPage,_offersAggregatorArcsIds,_offersAggregatorMethodsIds
// _getOffersAggregator

// const PROD=true
const PROD=false

;({
 OffersAggregatorPage:_OffersAggregatorPage,
 // getOffersAggregator:_getOffersAggregator,
 offersAggregatorArcsIds:_offersAggregatorArcsIds,
 offersAggregatorMethodsIds:_offersAggregatorMethodsIds,
}=require(PROD?'xyz/swapee/rc/offers-aggregator/prod':'./offers-aggregator.page'))

const dd=new Date
console.log(`${PROD?`📦`:`📝`} Loaded %s in %sms`,'offers-aggregator',dd.getTime()-d.getTime())

import 'xyz/swapee/rc/offers-aggregator'

export const OffersAggregatorPage=/**@type {typeof xyz.swapee.rc.OffersAggregatorPage}*/(_OffersAggregatorPage)
export const offersAggregatorArcsIds=/**@type {typeof xyz.swapee.rc.offersAggregatorArcsIds}*/(_offersAggregatorArcsIds)
export const offersAggregatorMethodsIds=/**@type {typeof xyz.swapee.rc.offersAggregatorMethodsIds}*/(_offersAggregatorMethodsIds)
// export const getOffersAggregator=/**@type {xyz.swapee.rc.getOffersAggregator}*/(_getOffersAggregator)