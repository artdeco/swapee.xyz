/** @extends {xyz.swapee.wc.AbstractTransactionInfoHead} */
export default class AbstractTransactionInfoHead extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractTransactionInfoHeadComputer} */
export class AbstractTransactionInfoHeadComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTransactionInfoHeadController} */
export class AbstractTransactionInfoHeadController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTransactionInfoHeadPort} */
export class TransactionInfoHeadPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTransactionInfoHeadView} */
export class AbstractTransactionInfoHeadView extends (<view>
  <classes>
   <string opt name="Class">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTransactionInfoHeadElement} */
export class AbstractTransactionInfoHeadElement extends (<element v3 html mv>
 <block src="./TransactionInfoHead.mvc/src/TransactionInfoHeadElement/methods/render.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent} */
export class AbstractTransactionInfoHeadHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.ITransactionInfo via="TransactionInfo" />
  </connectors>

</html-ic>) { }
// </class-end>