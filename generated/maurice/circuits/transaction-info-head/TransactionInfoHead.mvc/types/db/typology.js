/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ITransactionInfoHeadComputer': {
  'id': 43005442181,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadMemoryPQs': {
  'id': 43005442182,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadOuterCore': {
  'id': 43005442183,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TransactionInfoHeadInputsPQs': {
  'id': 43005442184,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadPort': {
  'id': 43005442185,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTransactionInfoHeadPort': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadPortInterface': {
  'id': 43005442186,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadCore': {
  'id': 43005442187,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTransactionInfoHeadCore': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadProcessor': {
  'id': 43005442188,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHead': {
  'id': 43005442189,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadBuffer': {
  'id': 430054421810,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil': {
  'id': 430054421811,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadHtmlComponent': {
  'id': 430054421812,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadElement': {
  'id': 430054421813,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildTransactionInfo': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadElementPort': {
  'id': 430054421814,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadDesigner': {
  'id': 430054421815,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadGPU': {
  'id': 430054421816,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadDisplay': {
  'id': 430054421817,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadQueriesPQs': {
  'id': 430054421818,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadVdusPQs': {
  'id': 430054421819,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadDisplay': {
  'id': 430054421820,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadClassesPQs': {
  'id': 430054421821,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadController': {
  'id': 430054421822,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ITransactionInfoHeadController': {
  'id': 430054421823,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadController': {
  'id': 430054421824,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadControllerAR': {
  'id': 430054421825,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoHeadControllerAT': {
  'id': 430054421826,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadScreen': {
  'id': 430054421827,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadScreen': {
  'id': 430054421828,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoHeadScreenAR': {
  'id': 430054421829,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadScreenAT': {
  'id': 430054421830,
  'symbols': {},
  'methods': {}
 }
})