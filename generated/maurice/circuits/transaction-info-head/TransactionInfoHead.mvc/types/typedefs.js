/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ITransactionInfoHeadComputer={}
xyz.swapee.wc.ITransactionInfoHeadComputer.compute={}
xyz.swapee.wc.ITransactionInfoHeadOuterCore={}
xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model={}
xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model.Core={}
xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel={}
xyz.swapee.wc.ITransactionInfoHeadPort={}
xyz.swapee.wc.ITransactionInfoHeadPort.Inputs={}
xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs={}
xyz.swapee.wc.ITransactionInfoHeadCore={}
xyz.swapee.wc.ITransactionInfoHeadCore.Model={}
xyz.swapee.wc.ITransactionInfoHeadPortInterface={}
xyz.swapee.wc.ITransactionInfoHeadProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ITransactionInfoHeadController={}
xyz.swapee.wc.front.ITransactionInfoHeadControllerAT={}
xyz.swapee.wc.front.ITransactionInfoHeadScreenAR={}
xyz.swapee.wc.ITransactionInfoHead={}
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil={}
xyz.swapee.wc.ITransactionInfoHeadHtmlComponent={}
xyz.swapee.wc.ITransactionInfoHeadElement={}
xyz.swapee.wc.ITransactionInfoHeadElement.build={}
xyz.swapee.wc.ITransactionInfoHeadElement.short={}
xyz.swapee.wc.ITransactionInfoHeadElementPort={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts={}
xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs={}
xyz.swapee.wc.ITransactionInfoHeadDesigner={}
xyz.swapee.wc.ITransactionInfoHeadDesigner.communicator={}
xyz.swapee.wc.ITransactionInfoHeadDesigner.relay={}
xyz.swapee.wc.ITransactionInfoHeadDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ITransactionInfoHeadDisplay={}
xyz.swapee.wc.back.ITransactionInfoHeadController={}
xyz.swapee.wc.back.ITransactionInfoHeadControllerAR={}
xyz.swapee.wc.back.ITransactionInfoHeadScreen={}
xyz.swapee.wc.back.ITransactionInfoHeadScreenAT={}
xyz.swapee.wc.ITransactionInfoHeadController={}
xyz.swapee.wc.ITransactionInfoHeadScreen={}
xyz.swapee.wc.ITransactionInfoHeadGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/02-ITransactionInfoHeadComputer.xml}  db5a03e8c6b7f0746d50ece2f74fc543 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadComputer)} xyz.swapee.wc.AbstractTransactionInfoHeadComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadComputer} xyz.swapee.wc.TransactionInfoHeadComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadComputer` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadComputer
 */
xyz.swapee.wc.AbstractTransactionInfoHeadComputer = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadComputer.constructor&xyz.swapee.wc.TransactionInfoHeadComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadComputer.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadComputer.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadComputer} xyz.swapee.wc.TransactionInfoHeadComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.TransactionInfoHeadMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.TransactionInfoHeadLand>)} xyz.swapee.wc.ITransactionInfoHeadComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ITransactionInfoHeadComputer
 */
xyz.swapee.wc.ITransactionInfoHeadComputer = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadComputer.compute} */
xyz.swapee.wc.ITransactionInfoHeadComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese>)} xyz.swapee.wc.TransactionInfoHeadComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadComputer} xyz.swapee.wc.ITransactionInfoHeadComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITransactionInfoHeadComputer_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadComputer
 * @implements {xyz.swapee.wc.ITransactionInfoHeadComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadComputer = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadComputer.constructor&xyz.swapee.wc.ITransactionInfoHeadComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadComputer}
 */
xyz.swapee.wc.TransactionInfoHeadComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadComputer} */
xyz.swapee.wc.RecordITransactionInfoHeadComputer

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadComputer} xyz.swapee.wc.BoundITransactionInfoHeadComputer */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadComputer} xyz.swapee.wc.BoundTransactionInfoHeadComputer */

/**
 * Contains getters to cast the _ITransactionInfoHeadComputer_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadComputerCaster
 */
xyz.swapee.wc.ITransactionInfoHeadComputerCaster = class { }
/**
 * Cast the _ITransactionInfoHeadComputer_ instance into the _BoundITransactionInfoHeadComputer_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadComputer}
 */
xyz.swapee.wc.ITransactionInfoHeadComputerCaster.prototype.asITransactionInfoHeadComputer
/**
 * Access the _TransactionInfoHeadComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadComputer}
 */
xyz.swapee.wc.ITransactionInfoHeadComputerCaster.prototype.superTransactionInfoHeadComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.TransactionInfoHeadMemory, land: !xyz.swapee.wc.ITransactionInfoHeadComputer.compute.Land) => void} xyz.swapee.wc.ITransactionInfoHeadComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadComputer.__compute<!xyz.swapee.wc.ITransactionInfoHeadComputer>} xyz.swapee.wc.ITransactionInfoHeadComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.TransactionInfoHeadMemory} mem The memory.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadComputer.compute.Land} land The land.
 * - `TransactionInfo` _!xyz.swapee.wc.TransactionInfoMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoHeadComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} TransactionInfo `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoHeadComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/03-ITransactionInfoHeadOuterCore.xml}  bbd0f5374b7c80391a5f61008f8951d4 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITransactionInfoHeadOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadOuterCore)} xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadOuterCore} xyz.swapee.wc.TransactionInfoHeadOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore
 */
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.constructor&xyz.swapee.wc.TransactionInfoHeadOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadOuterCore|typeof xyz.swapee.wc.TransactionInfoHeadOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadOuterCore|typeof xyz.swapee.wc.TransactionInfoHeadOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadOuterCore|typeof xyz.swapee.wc.TransactionInfoHeadOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadOuterCoreCaster)} xyz.swapee.wc.ITransactionInfoHeadOuterCore.constructor */
/**
 * The _ITransactionInfoHead_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ITransactionInfoHeadOuterCore
 */
xyz.swapee.wc.ITransactionInfoHeadOuterCore = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoHeadOuterCore.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadOuterCore

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadOuterCore.Initialese>)} xyz.swapee.wc.TransactionInfoHeadOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadOuterCore} xyz.swapee.wc.ITransactionInfoHeadOuterCore.typeof */
/**
 * A concrete class of _ITransactionInfoHeadOuterCore_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadOuterCore
 * @implements {xyz.swapee.wc.ITransactionInfoHeadOuterCore} The _ITransactionInfoHead_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadOuterCore = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadOuterCore.constructor&xyz.swapee.wc.ITransactionInfoHeadOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoHeadOuterCore.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadOuterCore}
 */
xyz.swapee.wc.TransactionInfoHeadOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadOuterCore.
 * @interface xyz.swapee.wc.ITransactionInfoHeadOuterCoreFields
 */
xyz.swapee.wc.ITransactionInfoHeadOuterCoreFields = class { }
/**
 * The _ITransactionInfoHead_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ITransactionInfoHeadOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore} */
xyz.swapee.wc.RecordITransactionInfoHeadOuterCore

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore} xyz.swapee.wc.BoundITransactionInfoHeadOuterCore */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadOuterCore} xyz.swapee.wc.BoundTransactionInfoHeadOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model.Core.core

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model.Core} xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model The _ITransactionInfoHead_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.Core} xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel The _ITransactionInfoHead_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ITransactionInfoHeadOuterCore_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadOuterCoreCaster
 */
xyz.swapee.wc.ITransactionInfoHeadOuterCoreCaster = class { }
/**
 * Cast the _ITransactionInfoHeadOuterCore_ instance into the _BoundITransactionInfoHeadOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadOuterCore}
 */
xyz.swapee.wc.ITransactionInfoHeadOuterCoreCaster.prototype.asITransactionInfoHeadOuterCore
/**
 * Access the _TransactionInfoHeadOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadOuterCore}
 */
xyz.swapee.wc.ITransactionInfoHeadOuterCoreCaster.prototype.superTransactionInfoHeadOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model.Core The core property (optional overlay).
 * @prop {string} [core=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model.Core_Safe The core property (required overlay).
 * @prop {string} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.Core The core property (optional overlay).
 * @prop {*} [core=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.Core_Safe The core property (required overlay).
 * @prop {*} core The core property.
 */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.Core} xyz.swapee.wc.ITransactionInfoHeadPort.Inputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ITransactionInfoHeadPort.Inputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.Core} xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model.Core} xyz.swapee.wc.ITransactionInfoHeadCore.Model.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model.Core_Safe} xyz.swapee.wc.ITransactionInfoHeadCore.Model.Core_Safe The core property (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/04-ITransactionInfoHeadPort.xml}  3d560d879a35700496c418c3c1119e43 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITransactionInfoHeadPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadPort)} xyz.swapee.wc.AbstractTransactionInfoHeadPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadPort} xyz.swapee.wc.TransactionInfoHeadPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadPort` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadPort
 */
xyz.swapee.wc.AbstractTransactionInfoHeadPort = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadPort.constructor&xyz.swapee.wc.TransactionInfoHeadPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadPort.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadPort.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadPort|typeof xyz.swapee.wc.TransactionInfoHeadPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadPort|typeof xyz.swapee.wc.TransactionInfoHeadPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadPort|typeof xyz.swapee.wc.TransactionInfoHeadPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadPort.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadPort} xyz.swapee.wc.TransactionInfoHeadPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITransactionInfoHeadPort.Inputs>)} xyz.swapee.wc.ITransactionInfoHeadPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ITransactionInfoHead_, providing input
 * pins.
 * @interface xyz.swapee.wc.ITransactionInfoHeadPort
 */
xyz.swapee.wc.ITransactionInfoHeadPort = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadPort.resetPort} */
xyz.swapee.wc.ITransactionInfoHeadPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadPort.resetTransactionInfoHeadPort} */
xyz.swapee.wc.ITransactionInfoHeadPort.prototype.resetTransactionInfoHeadPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadPort.Initialese>)} xyz.swapee.wc.TransactionInfoHeadPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadPort} xyz.swapee.wc.ITransactionInfoHeadPort.typeof */
/**
 * A concrete class of _ITransactionInfoHeadPort_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadPort
 * @implements {xyz.swapee.wc.ITransactionInfoHeadPort} The port that serves as an interface to the _ITransactionInfoHead_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadPort.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadPort = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadPort.constructor&xyz.swapee.wc.ITransactionInfoHeadPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadPort}
 */
xyz.swapee.wc.TransactionInfoHeadPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadPort.
 * @interface xyz.swapee.wc.ITransactionInfoHeadPortFields
 */
xyz.swapee.wc.ITransactionInfoHeadPortFields = class { }
/**
 * The inputs to the _ITransactionInfoHead_'s controller via its port.
 */
xyz.swapee.wc.ITransactionInfoHeadPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoHeadPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITransactionInfoHeadPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITransactionInfoHeadPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadPort} */
xyz.swapee.wc.RecordITransactionInfoHeadPort

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadPort} xyz.swapee.wc.BoundITransactionInfoHeadPort */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadPort} xyz.swapee.wc.BoundTransactionInfoHeadPort */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel)} xyz.swapee.wc.ITransactionInfoHeadPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel} xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ITransactionInfoHead_'s controller via its port.
 * @record xyz.swapee.wc.ITransactionInfoHeadPort.Inputs
 */
xyz.swapee.wc.ITransactionInfoHeadPort.Inputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadPort.Inputs.constructor&xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoHeadPort.Inputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel)} xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs.constructor */
/**
 * The inputs to the _ITransactionInfoHead_'s controller via its port.
 * @record xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs
 */
xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs.constructor&xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadPortInterface
 */
xyz.swapee.wc.ITransactionInfoHeadPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ITransactionInfoHeadPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ITransactionInfoHeadPortInterface.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadPortInterface

/**
 * A concrete class of _ITransactionInfoHeadPortInterface_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadPortInterface
 * @implements {xyz.swapee.wc.ITransactionInfoHeadPortInterface} The port interface.
 */
xyz.swapee.wc.TransactionInfoHeadPortInterface = class extends xyz.swapee.wc.ITransactionInfoHeadPortInterface { }
xyz.swapee.wc.TransactionInfoHeadPortInterface.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadPortInterface.Props
 * @prop {string} core The core property.
 */

/**
 * Contains getters to cast the _ITransactionInfoHeadPort_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadPortCaster
 */
xyz.swapee.wc.ITransactionInfoHeadPortCaster = class { }
/**
 * Cast the _ITransactionInfoHeadPort_ instance into the _BoundITransactionInfoHeadPort_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadPort}
 */
xyz.swapee.wc.ITransactionInfoHeadPortCaster.prototype.asITransactionInfoHeadPort
/**
 * Access the _TransactionInfoHeadPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadPort}
 */
xyz.swapee.wc.ITransactionInfoHeadPortCaster.prototype.superTransactionInfoHeadPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoHeadPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadPort.__resetPort<!xyz.swapee.wc.ITransactionInfoHeadPort>} xyz.swapee.wc.ITransactionInfoHeadPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadPort.resetPort} */
/**
 * Resets the _ITransactionInfoHead_ port.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoHeadPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoHeadPort.__resetTransactionInfoHeadPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadPort.__resetTransactionInfoHeadPort<!xyz.swapee.wc.ITransactionInfoHeadPort>} xyz.swapee.wc.ITransactionInfoHeadPort._resetTransactionInfoHeadPort */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadPort.resetTransactionInfoHeadPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoHeadPort.resetTransactionInfoHeadPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoHeadPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/09-ITransactionInfoHeadCore.xml}  909d0f9b918e3355f90e627f45f3bbf3 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITransactionInfoHeadCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadCore)} xyz.swapee.wc.AbstractTransactionInfoHeadCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadCore} xyz.swapee.wc.TransactionInfoHeadCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadCore` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadCore
 */
xyz.swapee.wc.AbstractTransactionInfoHeadCore = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadCore.constructor&xyz.swapee.wc.TransactionInfoHeadCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadCore.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadCore.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadCore|typeof xyz.swapee.wc.TransactionInfoHeadCore)|(!xyz.swapee.wc.ITransactionInfoHeadOuterCore|typeof xyz.swapee.wc.TransactionInfoHeadOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadCore|typeof xyz.swapee.wc.TransactionInfoHeadCore)|(!xyz.swapee.wc.ITransactionInfoHeadOuterCore|typeof xyz.swapee.wc.TransactionInfoHeadOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadCore|typeof xyz.swapee.wc.TransactionInfoHeadCore)|(!xyz.swapee.wc.ITransactionInfoHeadOuterCore|typeof xyz.swapee.wc.TransactionInfoHeadOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadCore}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadCoreCaster&xyz.swapee.wc.ITransactionInfoHeadOuterCore)} xyz.swapee.wc.ITransactionInfoHeadCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ITransactionInfoHeadCore
 */
xyz.swapee.wc.ITransactionInfoHeadCore = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoHeadOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ITransactionInfoHeadCore.resetCore} */
xyz.swapee.wc.ITransactionInfoHeadCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadCore.resetTransactionInfoHeadCore} */
xyz.swapee.wc.ITransactionInfoHeadCore.prototype.resetTransactionInfoHeadCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadCore.Initialese>)} xyz.swapee.wc.TransactionInfoHeadCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadCore} xyz.swapee.wc.ITransactionInfoHeadCore.typeof */
/**
 * A concrete class of _ITransactionInfoHeadCore_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadCore
 * @implements {xyz.swapee.wc.ITransactionInfoHeadCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadCore.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadCore = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadCore.constructor&xyz.swapee.wc.ITransactionInfoHeadCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoHeadCore.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadCore}
 */
xyz.swapee.wc.TransactionInfoHeadCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadCore.
 * @interface xyz.swapee.wc.ITransactionInfoHeadCoreFields
 */
xyz.swapee.wc.ITransactionInfoHeadCoreFields = class { }
/**
 * The _ITransactionInfoHead_'s memory.
 */
xyz.swapee.wc.ITransactionInfoHeadCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITransactionInfoHeadCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ITransactionInfoHeadCoreFields.prototype.props = /** @type {xyz.swapee.wc.ITransactionInfoHeadCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadCore} */
xyz.swapee.wc.RecordITransactionInfoHeadCore

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadCore} xyz.swapee.wc.BoundITransactionInfoHeadCore */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadCore} xyz.swapee.wc.BoundTransactionInfoHeadCore */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model} xyz.swapee.wc.ITransactionInfoHeadCore.Model The _ITransactionInfoHead_'s memory. */

/**
 * Contains getters to cast the _ITransactionInfoHeadCore_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadCoreCaster
 */
xyz.swapee.wc.ITransactionInfoHeadCoreCaster = class { }
/**
 * Cast the _ITransactionInfoHeadCore_ instance into the _BoundITransactionInfoHeadCore_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadCore}
 */
xyz.swapee.wc.ITransactionInfoHeadCoreCaster.prototype.asITransactionInfoHeadCore
/**
 * Access the _TransactionInfoHeadCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadCore}
 */
xyz.swapee.wc.ITransactionInfoHeadCoreCaster.prototype.superTransactionInfoHeadCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoHeadCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadCore.__resetCore<!xyz.swapee.wc.ITransactionInfoHeadCore>} xyz.swapee.wc.ITransactionInfoHeadCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadCore.resetCore} */
/**
 * Resets the _ITransactionInfoHead_ core.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoHeadCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoHeadCore.__resetTransactionInfoHeadCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadCore.__resetTransactionInfoHeadCore<!xyz.swapee.wc.ITransactionInfoHeadCore>} xyz.swapee.wc.ITransactionInfoHeadCore._resetTransactionInfoHeadCore */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadCore.resetTransactionInfoHeadCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoHeadCore.resetTransactionInfoHeadCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoHeadCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/10-ITransactionInfoHeadProcessor.xml}  834bc3bffca8dd8b8d088dfa4b067a7d */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese&xyz.swapee.wc.ITransactionInfoHeadController.Initialese} xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadProcessor)} xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadProcessor} xyz.swapee.wc.TransactionInfoHeadProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadProcessor
 */
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.constructor&xyz.swapee.wc.TransactionInfoHeadProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!xyz.swapee.wc.ITransactionInfoHeadCore|typeof xyz.swapee.wc.TransactionInfoHeadCore)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!xyz.swapee.wc.ITransactionInfoHeadCore|typeof xyz.swapee.wc.TransactionInfoHeadCore)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!xyz.swapee.wc.ITransactionInfoHeadCore|typeof xyz.swapee.wc.TransactionInfoHeadCore)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadProcessor} xyz.swapee.wc.TransactionInfoHeadProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadProcessorCaster&xyz.swapee.wc.ITransactionInfoHeadComputer&xyz.swapee.wc.ITransactionInfoHeadCore&xyz.swapee.wc.ITransactionInfoHeadController)} xyz.swapee.wc.ITransactionInfoHeadProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadController} xyz.swapee.wc.ITransactionInfoHeadController.typeof */
/**
 * The processor to compute changes to the memory for the _ITransactionInfoHead_.
 * @interface xyz.swapee.wc.ITransactionInfoHeadProcessor
 */
xyz.swapee.wc.ITransactionInfoHeadProcessor = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoHeadComputer.typeof&xyz.swapee.wc.ITransactionInfoHeadCore.typeof&xyz.swapee.wc.ITransactionInfoHeadController.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese>)} xyz.swapee.wc.TransactionInfoHeadProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadProcessor} xyz.swapee.wc.ITransactionInfoHeadProcessor.typeof */
/**
 * A concrete class of _ITransactionInfoHeadProcessor_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadProcessor
 * @implements {xyz.swapee.wc.ITransactionInfoHeadProcessor} The processor to compute changes to the memory for the _ITransactionInfoHead_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadProcessor = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadProcessor.constructor&xyz.swapee.wc.ITransactionInfoHeadProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadProcessor}
 */
xyz.swapee.wc.TransactionInfoHeadProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadProcessor} */
xyz.swapee.wc.RecordITransactionInfoHeadProcessor

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadProcessor} xyz.swapee.wc.BoundITransactionInfoHeadProcessor */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadProcessor} xyz.swapee.wc.BoundTransactionInfoHeadProcessor */

/**
 * Contains getters to cast the _ITransactionInfoHeadProcessor_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadProcessorCaster
 */
xyz.swapee.wc.ITransactionInfoHeadProcessorCaster = class { }
/**
 * Cast the _ITransactionInfoHeadProcessor_ instance into the _BoundITransactionInfoHeadProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadProcessor}
 */
xyz.swapee.wc.ITransactionInfoHeadProcessorCaster.prototype.asITransactionInfoHeadProcessor
/**
 * Access the _TransactionInfoHeadProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadProcessor}
 */
xyz.swapee.wc.ITransactionInfoHeadProcessorCaster.prototype.superTransactionInfoHeadProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/100-TransactionInfoHeadMemory.xml}  6f89f4b714507e1b68463f912df3cda0 */
/**
 * The memory of the _ITransactionInfoHead_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.TransactionInfoHeadMemory
 */
xyz.swapee.wc.TransactionInfoHeadMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.TransactionInfoHeadMemory.prototype.core = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/102-TransactionInfoHeadInputs.xml}  4dbb1e5da6054ddd9262167795c2530e */
/**
 * The inputs of the _ITransactionInfoHead_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.TransactionInfoHeadInputs
 */
xyz.swapee.wc.front.TransactionInfoHeadInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoHeadInputs.prototype.core = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/11-ITransactionInfoHead.xml}  e53757e58c3d94f0276b4c24d8de3161 */
/**
 * An atomic wrapper for the _ITransactionInfoHead_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.TransactionInfoHeadEnv
 */
xyz.swapee.wc.TransactionInfoHeadEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.TransactionInfoHeadEnv.prototype.transactionInfoHead = /** @type {xyz.swapee.wc.ITransactionInfoHead} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.ITransactionInfoHeadController.Inputs>&xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese&xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese&xyz.swapee.wc.ITransactionInfoHeadController.Initialese} xyz.swapee.wc.ITransactionInfoHead.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHead)} xyz.swapee.wc.AbstractTransactionInfoHead.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHead} xyz.swapee.wc.TransactionInfoHead.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHead` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHead
 */
xyz.swapee.wc.AbstractTransactionInfoHead = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHead.constructor&xyz.swapee.wc.TransactionInfoHead.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHead.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHead
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHead.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHead} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHead|typeof xyz.swapee.wc.TransactionInfoHead)|(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHead}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHead.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHead}
 */
xyz.swapee.wc.AbstractTransactionInfoHead.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHead}
 */
xyz.swapee.wc.AbstractTransactionInfoHead.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHead|typeof xyz.swapee.wc.TransactionInfoHead)|(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHead}
 */
xyz.swapee.wc.AbstractTransactionInfoHead.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHead|typeof xyz.swapee.wc.TransactionInfoHead)|(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHead}
 */
xyz.swapee.wc.AbstractTransactionInfoHead.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHead.Initialese[]) => xyz.swapee.wc.ITransactionInfoHead} xyz.swapee.wc.TransactionInfoHeadConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHead.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ITransactionInfoHead.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ITransactionInfoHead.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ITransactionInfoHead.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.TransactionInfoHeadClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadCaster&xyz.swapee.wc.ITransactionInfoHeadProcessor&xyz.swapee.wc.ITransactionInfoHeadComputer&xyz.swapee.wc.ITransactionInfoHeadController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.ITransactionInfoHeadController.Inputs, !xyz.swapee.wc.TransactionInfoHeadLand>)} xyz.swapee.wc.ITransactionInfoHead.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ITransactionInfoHead
 */
xyz.swapee.wc.ITransactionInfoHead = class extends /** @type {xyz.swapee.wc.ITransactionInfoHead.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoHeadProcessor.typeof&xyz.swapee.wc.ITransactionInfoHeadComputer.typeof&xyz.swapee.wc.ITransactionInfoHeadController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHead* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHead.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHead.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHead&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHead.Initialese>)} xyz.swapee.wc.TransactionInfoHead.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHead} xyz.swapee.wc.ITransactionInfoHead.typeof */
/**
 * A concrete class of _ITransactionInfoHead_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHead
 * @implements {xyz.swapee.wc.ITransactionInfoHead} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHead.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHead = class extends /** @type {xyz.swapee.wc.TransactionInfoHead.constructor&xyz.swapee.wc.ITransactionInfoHead.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHead* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHead.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHead* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHead.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHead.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHead}
 */
xyz.swapee.wc.TransactionInfoHead.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHead.
 * @interface xyz.swapee.wc.ITransactionInfoHeadFields
 */
xyz.swapee.wc.ITransactionInfoHeadFields = class { }
/**
 * The input pins of the _ITransactionInfoHead_ port.
 */
xyz.swapee.wc.ITransactionInfoHeadFields.prototype.pinout = /** @type {!xyz.swapee.wc.ITransactionInfoHead.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHead} */
xyz.swapee.wc.RecordITransactionInfoHead

/** @typedef {xyz.swapee.wc.ITransactionInfoHead} xyz.swapee.wc.BoundITransactionInfoHead */

/** @typedef {xyz.swapee.wc.TransactionInfoHead} xyz.swapee.wc.BoundTransactionInfoHead */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadController.Inputs} xyz.swapee.wc.ITransactionInfoHead.Pinout The input pins of the _ITransactionInfoHead_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs>)} xyz.swapee.wc.ITransactionInfoHeadBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ITransactionInfoHeadBuffer
 */
xyz.swapee.wc.ITransactionInfoHeadBuffer = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoHeadBuffer.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadBuffer

/**
 * A concrete class of _ITransactionInfoHeadBuffer_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadBuffer
 * @implements {xyz.swapee.wc.ITransactionInfoHeadBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.TransactionInfoHeadBuffer = class extends xyz.swapee.wc.ITransactionInfoHeadBuffer { }
xyz.swapee.wc.TransactionInfoHeadBuffer.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadBuffer

/**
 * Contains getters to cast the _ITransactionInfoHead_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadCaster
 */
xyz.swapee.wc.ITransactionInfoHeadCaster = class { }
/**
 * Cast the _ITransactionInfoHead_ instance into the _BoundITransactionInfoHead_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHead}
 */
xyz.swapee.wc.ITransactionInfoHeadCaster.prototype.asITransactionInfoHead
/**
 * Access the _TransactionInfoHead_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHead}
 */
xyz.swapee.wc.ITransactionInfoHeadCaster.prototype.superTransactionInfoHead

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/110-TransactionInfoHeadSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoHeadMemoryPQs
 */
xyz.swapee.wc.TransactionInfoHeadMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TransactionInfoHeadMemoryPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoHeadMemoryQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoHeadMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.TransactionInfoHeadMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadMemoryPQs)} xyz.swapee.wc.TransactionInfoHeadInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadMemoryPQs} xyz.swapee.wc.TransactionInfoHeadMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoHeadInputsPQs
 */
xyz.swapee.wc.TransactionInfoHeadInputsPQs = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadInputsPQs.constructor&xyz.swapee.wc.TransactionInfoHeadMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoHeadInputsPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadInputsPQs

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadMemoryPQs)} xyz.swapee.wc.TransactionInfoHeadInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoHeadInputsQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoHeadInputsQPs = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadInputsQPs.constructor&xyz.swapee.wc.TransactionInfoHeadMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoHeadInputsQPs.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoHeadQueriesPQs
 */
xyz.swapee.wc.TransactionInfoHeadQueriesPQs = class {
  constructor() {
    /**
     * `g3ad3`
     */
    this.transactionInfoSel=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TransactionInfoHeadQueriesPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadQueriesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoHeadQueriesQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoHeadQueriesQPs = class { }
/**
 * `transactionInfoSel`
 */
xyz.swapee.wc.TransactionInfoHeadQueriesQPs.prototype.g3ad3 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoHeadVdusPQs
 */
xyz.swapee.wc.TransactionInfoHeadVdusPQs = class {
  constructor() {
    /**
     * `d66e1`
     */
    this.TransactionLoaderWr=/** @type {string} */ (void 0)
    /**
     * `d66e2`
     */
    this.TransactionFoundWr=/** @type {string} */ (void 0)
    /**
     * `d66e3`
     */
    this.TransactionLoInWr=/** @type {string} */ (void 0)
    /**
     * `d66e4`
     */
    this.NetworkFeeWr=/** @type {string} */ (void 0)
    /**
     * `d66e5`
     */
    this.NetworkFeeLa=/** @type {string} */ (void 0)
    /**
     * `d66e6`
     */
    this.PartnerFeeWr=/** @type {string} */ (void 0)
    /**
     * `d66e7`
     */
    this.PartnerFeeLa=/** @type {string} */ (void 0)
    /**
     * `d66e8`
     */
    this.InIm=/** @type {string} */ (void 0)
    /**
     * `d66e9`
     */
    this.OutIm=/** @type {string} */ (void 0)
    /**
     * `d66e10`
     */
    this.NotFoundWr=/** @type {string} */ (void 0)
    /**
     * `d66e11`
     */
    this.CryptoIns=/** @type {string} */ (void 0)
    /**
     * `d66e12`
     */
    this.CryptoOuts=/** @type {string} */ (void 0)
    /**
     * `d66e13`
     */
    this.RateLa=/** @type {string} */ (void 0)
    /**
     * `d66e14`
     */
    this.AmountIn=/** @type {string} */ (void 0)
    /**
     * `d66e15`
     */
    this.AmountOut=/** @type {string} */ (void 0)
    /**
     * `d66e16`
     */
    this.VisibleAmountWr=/** @type {string} */ (void 0)
    /**
     * `d66e17`
     */
    this.VisibleAmounLa=/** @type {string} */ (void 0)
    /**
     * `d66e18`
     */
    this.BreakdownBu=/** @type {string} */ (void 0)
    /**
     * `d66e19`
     */
    this.TransactionLoIn=/** @type {string} */ (void 0)
    /**
     * `d66e20`
     */
    this.ReloadGetTransactionBu=/** @type {string} */ (void 0)
    /**
     * `d66e21`
     */
    this.GetTransactionErrorWr=/** @type {string} */ (void 0)
    /**
     * `d66e22`
     */
    this.GetTransactionErrorLa=/** @type {string} */ (void 0)
    /**
     * `d66e23`
     */
    this.FixedLock=/** @type {string} */ (void 0)
    /**
     * `d66e24`
     */
    this.TransactionInfo=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TransactionInfoHeadVdusPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoHeadVdusQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs = class { }
/**
 * `TransactionLoaderWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e1 = /** @type {string} */ (void 0)
/**
 * `TransactionFoundWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e2 = /** @type {string} */ (void 0)
/**
 * `TransactionLoInWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e3 = /** @type {string} */ (void 0)
/**
 * `NetworkFeeWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e4 = /** @type {string} */ (void 0)
/**
 * `NetworkFeeLa`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e5 = /** @type {string} */ (void 0)
/**
 * `PartnerFeeWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e6 = /** @type {string} */ (void 0)
/**
 * `PartnerFeeLa`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e7 = /** @type {string} */ (void 0)
/**
 * `InIm`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e8 = /** @type {string} */ (void 0)
/**
 * `OutIm`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e9 = /** @type {string} */ (void 0)
/**
 * `NotFoundWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e10 = /** @type {string} */ (void 0)
/**
 * `CryptoIns`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e11 = /** @type {string} */ (void 0)
/**
 * `CryptoOuts`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e12 = /** @type {string} */ (void 0)
/**
 * `RateLa`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e13 = /** @type {string} */ (void 0)
/**
 * `AmountIn`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e14 = /** @type {string} */ (void 0)
/**
 * `AmountOut`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e15 = /** @type {string} */ (void 0)
/**
 * `VisibleAmountWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e16 = /** @type {string} */ (void 0)
/**
 * `VisibleAmounLa`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e17 = /** @type {string} */ (void 0)
/**
 * `BreakdownBu`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e18 = /** @type {string} */ (void 0)
/**
 * `TransactionLoIn`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e19 = /** @type {string} */ (void 0)
/**
 * `ReloadGetTransactionBu`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e20 = /** @type {string} */ (void 0)
/**
 * `GetTransactionErrorWr`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e21 = /** @type {string} */ (void 0)
/**
 * `GetTransactionErrorLa`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e22 = /** @type {string} */ (void 0)
/**
 * `FixedLock`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e23 = /** @type {string} */ (void 0)
/**
 * `TransactionInfo`
 */
xyz.swapee.wc.TransactionInfoHeadVdusQPs.prototype.d66e24 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoHeadClassesPQs
 */
xyz.swapee.wc.TransactionInfoHeadClassesPQs = class {
  constructor() {
    /**
     * `jbd81`
     */
    this.Class=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TransactionInfoHeadClassesPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadClassesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoHeadClassesQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoHeadClassesQPs = class { }
/**
 * `Class`
 */
xyz.swapee.wc.TransactionInfoHeadClassesQPs.prototype.jbd81 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/12-ITransactionInfoHeadHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtilFields)} xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.router} */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _ITransactionInfoHeadHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadHtmlComponentUtil
 * @implements {xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil} ‎
 */
xyz.swapee.wc.TransactionInfoHeadHtmlComponentUtil = class extends xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil { }
xyz.swapee.wc.TransactionInfoHeadHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadHtmlComponentUtil

/**
 * Fields of the ITransactionInfoHeadHtmlComponentUtil.
 * @interface xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtilFields
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil} */
xyz.swapee.wc.RecordITransactionInfoHeadHtmlComponentUtil

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil} xyz.swapee.wc.BoundITransactionInfoHeadHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadHtmlComponentUtil} xyz.swapee.wc.BoundTransactionInfoHeadHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.ITransactionInfoPort} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadPort} TransactionInfoHead The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} TransactionInfo
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHead
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.ITransactionInfo.Pinout} TransactionInfo
 * @prop {!xyz.swapee.wc.ITransactionInfoHead.Pinout} TransactionInfoHead
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.__router<!xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil>} xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `TransactionInfo` _typeof ITransactionInfoPort_
 * - `TransactionInfoHead` _typeof ITransactionInfoHeadPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `TransactionInfo` _!TransactionInfoMemory_
 * - `TransactionInfoHead` _!TransactionInfoHeadMemory_
 * @param {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `TransactionInfo` _!ITransactionInfo.Pinout_
 * - `TransactionInfoHead` _!ITransactionInfoHead.Pinout_
 * @return {?}
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/12-ITransactionInfoHeadHtmlComponent.xml}  b2b3ffc82d6cb206e3a95f991d94dd5f */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese&xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese&xyz.swapee.wc.ITransactionInfoHead.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ITransactionInfoHeadProcessor.Initialese&xyz.swapee.wc.ITransactionInfoHeadComputer.Initialese} xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadHtmlComponent)} xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent} xyz.swapee.wc.TransactionInfoHeadHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent
 */
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.constructor&xyz.swapee.wc.TransactionInfoHeadHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent|typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent)|(!xyz.swapee.wc.back.ITransactionInfoHeadController|typeof xyz.swapee.wc.back.TransactionInfoHeadController)|(!xyz.swapee.wc.back.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.back.TransactionInfoHeadScreen)|(!xyz.swapee.wc.ITransactionInfoHead|typeof xyz.swapee.wc.TransactionInfoHead)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ITransactionInfoHeadGPU|typeof xyz.swapee.wc.TransactionInfoHeadGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent|typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent)|(!xyz.swapee.wc.back.ITransactionInfoHeadController|typeof xyz.swapee.wc.back.TransactionInfoHeadController)|(!xyz.swapee.wc.back.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.back.TransactionInfoHeadScreen)|(!xyz.swapee.wc.ITransactionInfoHead|typeof xyz.swapee.wc.TransactionInfoHead)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ITransactionInfoHeadGPU|typeof xyz.swapee.wc.TransactionInfoHeadGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent|typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent)|(!xyz.swapee.wc.back.ITransactionInfoHeadController|typeof xyz.swapee.wc.back.TransactionInfoHeadController)|(!xyz.swapee.wc.back.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.back.TransactionInfoHeadScreen)|(!xyz.swapee.wc.ITransactionInfoHead|typeof xyz.swapee.wc.TransactionInfoHead)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ITransactionInfoHeadGPU|typeof xyz.swapee.wc.TransactionInfoHeadGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITransactionInfoHeadProcessor|typeof xyz.swapee.wc.TransactionInfoHeadProcessor)|(!xyz.swapee.wc.ITransactionInfoHeadComputer|typeof xyz.swapee.wc.TransactionInfoHeadComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadHtmlComponent} xyz.swapee.wc.TransactionInfoHeadHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadHtmlComponentCaster&xyz.swapee.wc.back.ITransactionInfoHeadController&xyz.swapee.wc.back.ITransactionInfoHeadScreen&xyz.swapee.wc.ITransactionInfoHead&com.webcircuits.ILanded<!xyz.swapee.wc.TransactionInfoHeadLand>&xyz.swapee.wc.ITransactionInfoHeadGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.ITransactionInfoHeadController.Inputs, !HTMLDivElement, !xyz.swapee.wc.TransactionInfoHeadLand>&xyz.swapee.wc.ITransactionInfoHeadProcessor&xyz.swapee.wc.ITransactionInfoHeadComputer)} xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoHeadController} xyz.swapee.wc.back.ITransactionInfoHeadController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoHeadScreen} xyz.swapee.wc.back.ITransactionInfoHeadScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHead} xyz.swapee.wc.ITransactionInfoHead.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadGPU} xyz.swapee.wc.ITransactionInfoHeadGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadProcessor} xyz.swapee.wc.ITransactionInfoHeadProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadComputer} xyz.swapee.wc.ITransactionInfoHeadComputer.typeof */
/**
 * The _ITransactionInfoHead_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ITransactionInfoHeadHtmlComponent
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponent = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITransactionInfoHeadController.typeof&xyz.swapee.wc.back.ITransactionInfoHeadScreen.typeof&xyz.swapee.wc.ITransactionInfoHead.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.ITransactionInfoHeadGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ITransactionInfoHeadProcessor.typeof&xyz.swapee.wc.ITransactionInfoHeadComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.Initialese>)} xyz.swapee.wc.TransactionInfoHeadHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadHtmlComponent} xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITransactionInfoHeadHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadHtmlComponent
 * @implements {xyz.swapee.wc.ITransactionInfoHeadHtmlComponent} The _ITransactionInfoHead_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadHtmlComponent = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadHtmlComponent.constructor&xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadHtmlComponent}
 */
xyz.swapee.wc.TransactionInfoHeadHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadHtmlComponent} */
xyz.swapee.wc.RecordITransactionInfoHeadHtmlComponent

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadHtmlComponent} xyz.swapee.wc.BoundITransactionInfoHeadHtmlComponent */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadHtmlComponent} xyz.swapee.wc.BoundTransactionInfoHeadHtmlComponent */

/**
 * Contains getters to cast the _ITransactionInfoHeadHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadHtmlComponentCaster
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentCaster = class { }
/**
 * Cast the _ITransactionInfoHeadHtmlComponent_ instance into the _BoundITransactionInfoHeadHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadHtmlComponent}
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentCaster.prototype.asITransactionInfoHeadHtmlComponent
/**
 * Access the _TransactionInfoHeadHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadHtmlComponent}
 */
xyz.swapee.wc.ITransactionInfoHeadHtmlComponentCaster.prototype.superTransactionInfoHeadHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/130-ITransactionInfoHeadElement.xml}  68676e9abe66e80f3c6c3c18c1236e94 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.TransactionInfoHeadLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.ITransactionInfo>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.ITransactionInfoHeadElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ITransactionInfoHeadElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadElement)} xyz.swapee.wc.AbstractTransactionInfoHeadElement.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadElement} xyz.swapee.wc.TransactionInfoHeadElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadElement` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadElement
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElement = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadElement.constructor&xyz.swapee.wc.TransactionInfoHeadElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadElement.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElement.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadElement|typeof xyz.swapee.wc.TransactionInfoHeadElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElement}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElement}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadElement|typeof xyz.swapee.wc.TransactionInfoHeadElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElement}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadElement|typeof xyz.swapee.wc.TransactionInfoHeadElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElement}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadElement.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadElement} xyz.swapee.wc.TransactionInfoHeadElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadElementFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.ITransactionInfoHeadElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.ITransactionInfoHeadElement.Inputs, !xyz.swapee.wc.TransactionInfoHeadLand>&com.webcircuits.ILanded<!xyz.swapee.wc.TransactionInfoHeadLand>&guest.maurice.IMilleu<!xyz.swapee.wc.ITransactionInfo>)} xyz.swapee.wc.ITransactionInfoHeadElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _ITransactionInfoHead_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ITransactionInfoHeadElement
 */
xyz.swapee.wc.ITransactionInfoHeadElement = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadElement.solder} */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadElement.render} */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadElement.build} */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadElement.buildTransactionInfo} */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.buildTransactionInfo = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadElement.short} */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadElement.server} */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadElement.inducer} */
xyz.swapee.wc.ITransactionInfoHeadElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadElement&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadElement.Initialese>)} xyz.swapee.wc.TransactionInfoHeadElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement} xyz.swapee.wc.ITransactionInfoHeadElement.typeof */
/**
 * A concrete class of _ITransactionInfoHeadElement_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadElement
 * @implements {xyz.swapee.wc.ITransactionInfoHeadElement} A component description.
 *
 * The _ITransactionInfoHead_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadElement.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadElement = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadElement.constructor&xyz.swapee.wc.ITransactionInfoHeadElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElement}
 */
xyz.swapee.wc.TransactionInfoHeadElement.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadElement.
 * @interface xyz.swapee.wc.ITransactionInfoHeadElementFields
 */
xyz.swapee.wc.ITransactionInfoHeadElementFields = class { }
/**
 * The element-specific inputs to the _ITransactionInfoHead_ component.
 */
xyz.swapee.wc.ITransactionInfoHeadElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoHeadElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.ITransactionInfoHeadElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement} */
xyz.swapee.wc.RecordITransactionInfoHeadElement

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement} xyz.swapee.wc.BoundITransactionInfoHeadElement */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadElement} xyz.swapee.wc.BoundTransactionInfoHeadElement */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadPort.Inputs&xyz.swapee.wc.ITransactionInfoHeadDisplay.Queries&xyz.swapee.wc.ITransactionInfoHeadController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs} xyz.swapee.wc.ITransactionInfoHeadElement.Inputs The element-specific inputs to the _ITransactionInfoHead_ component. */

/**
 * Contains getters to cast the _ITransactionInfoHeadElement_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadElementCaster
 */
xyz.swapee.wc.ITransactionInfoHeadElementCaster = class { }
/**
 * Cast the _ITransactionInfoHeadElement_ instance into the _BoundITransactionInfoHeadElement_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadElement}
 */
xyz.swapee.wc.ITransactionInfoHeadElementCaster.prototype.asITransactionInfoHeadElement
/**
 * Access the _TransactionInfoHeadElement_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadElement}
 */
xyz.swapee.wc.ITransactionInfoHeadElementCaster.prototype.superTransactionInfoHeadElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.TransactionInfoHeadMemory, props: !xyz.swapee.wc.ITransactionInfoHeadElement.Inputs) => Object<string, *>} xyz.swapee.wc.ITransactionInfoHeadElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement.__solder<!xyz.swapee.wc.ITransactionInfoHeadElement>} xyz.swapee.wc.ITransactionInfoHeadElement._solder */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.TransactionInfoHeadMemory} model The model.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadElement.Inputs} props The element props.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ITransactionInfoHeadOuterCore.WeakModel.Core* ⤴ *ITransactionInfoHeadOuterCore.WeakModel.Core* Default `null`.
 * - `[transactionInfoSel=""]` _string?_ The query to discover the _TransactionInfo_ VDU. ⤴ *ITransactionInfoHeadDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITransactionInfoHeadElementPort.Inputs.NoSolder* Default `false`.
 * - `[transactionLoaderWrOpts]` _!Object?_ The options to pass to the _TransactionLoaderWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts* Default `{}`.
 * - `[transactionFoundWrOpts]` _!Object?_ The options to pass to the _TransactionFoundWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts* Default `{}`.
 * - `[transactionLoInWrOpts]` _!Object?_ The options to pass to the _TransactionLoInWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts* Default `{}`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[notFoundWrOpts]` _!Object?_ The options to pass to the _NotFoundWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[rateLaOpts]` _!Object?_ The options to pass to the _RateLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.RateLaOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[transactionLoInOpts]` _!Object?_ The options to pass to the _TransactionLoIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts* Default `{}`.
 * - `[reloadGetTransactionBuOpts]` _!Object?_ The options to pass to the _ReloadGetTransactionBu_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts* Default `{}`.
 * - `[getTransactionErrorWrOpts]` _!Object?_ The options to pass to the _GetTransactionErrorWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts* Default `{}`.
 * - `[getTransactionErrorLaOpts]` _!Object?_ The options to pass to the _GetTransactionErrorLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts* Default `{}`.
 * - `[fixedLockOpts]` _!Object?_ The options to pass to the _FixedLock_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.FixedLockOpts* Default `{}`.
 * - `[transactionInfoOpts]` _!Object?_ The options to pass to the _TransactionInfo_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ITransactionInfoHeadElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TransactionInfoHeadMemory, instance?: !xyz.swapee.wc.ITransactionInfoHeadScreen&xyz.swapee.wc.ITransactionInfoHeadController) => !engineering.type.VNode} xyz.swapee.wc.ITransactionInfoHeadElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement.__render<!xyz.swapee.wc.ITransactionInfoHeadElement>} xyz.swapee.wc.ITransactionInfoHeadElement._render */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.TransactionInfoHeadMemory} [model] The model for the view.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadScreen&xyz.swapee.wc.ITransactionInfoHeadController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITransactionInfoHeadElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.ITransactionInfoHeadElement.build.Cores, instances: !xyz.swapee.wc.ITransactionInfoHeadElement.build.Instances) => ?} xyz.swapee.wc.ITransactionInfoHeadElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement.__build<!xyz.swapee.wc.ITransactionInfoHeadElement>} xyz.swapee.wc.ITransactionInfoHeadElement._build */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadElement.build.Cores} cores The models of components on the land.
 * - `TransactionInfo` _!xyz.swapee.wc.ITransactionInfoCore.Model_
 * @param {!xyz.swapee.wc.ITransactionInfoHeadElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `TransactionInfo` _!xyz.swapee.wc.ITransactionInfo_
 * @return {?}
 */
xyz.swapee.wc.ITransactionInfoHeadElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.ITransactionInfoCore.Model} TransactionInfo
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.ITransactionInfo} TransactionInfo
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ITransactionInfoCore.Model, instance: !xyz.swapee.wc.ITransactionInfo) => ?} xyz.swapee.wc.ITransactionInfoHeadElement.__buildTransactionInfo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement.__buildTransactionInfo<!xyz.swapee.wc.ITransactionInfoHeadElement>} xyz.swapee.wc.ITransactionInfoHeadElement._buildTransactionInfo */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement.buildTransactionInfo} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ITransactionInfo_ component.
 * @param {!xyz.swapee.wc.ITransactionInfoCore.Model} model
 * @param {!xyz.swapee.wc.ITransactionInfo} instance
 * @return {?}
 */
xyz.swapee.wc.ITransactionInfoHeadElement.buildTransactionInfo = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.TransactionInfoHeadMemory, ports: !xyz.swapee.wc.ITransactionInfoHeadElement.short.Ports, cores: !xyz.swapee.wc.ITransactionInfoHeadElement.short.Cores) => ?} xyz.swapee.wc.ITransactionInfoHeadElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement.__short<!xyz.swapee.wc.ITransactionInfoHeadElement>} xyz.swapee.wc.ITransactionInfoHeadElement._short */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.TransactionInfoHeadMemory} model The model from which to feed properties to peer's ports.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadElement.short.Ports} ports The ports of the peers.
 * - `TransactionInfo` _typeof xyz.swapee.wc.ITransactionInfoPortInterface_
 * @param {!xyz.swapee.wc.ITransactionInfoHeadElement.short.Cores} cores The cores of the peers.
 * - `TransactionInfo` _xyz.swapee.wc.ITransactionInfoCore.Model_
 * @return {?}
 */
xyz.swapee.wc.ITransactionInfoHeadElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.ITransactionInfoPortInterface} TransactionInfo
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.ITransactionInfoCore.Model} TransactionInfo
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TransactionInfoHeadMemory, inputs: !xyz.swapee.wc.ITransactionInfoHeadElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ITransactionInfoHeadElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement.__server<!xyz.swapee.wc.ITransactionInfoHeadElement>} xyz.swapee.wc.ITransactionInfoHeadElement._server */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.TransactionInfoHeadMemory} memory The memory registers.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadElement.Inputs} inputs The inputs to the port.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ITransactionInfoHeadOuterCore.WeakModel.Core* ⤴ *ITransactionInfoHeadOuterCore.WeakModel.Core* Default `null`.
 * - `[transactionInfoSel=""]` _string?_ The query to discover the _TransactionInfo_ VDU. ⤴ *ITransactionInfoHeadDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITransactionInfoHeadElementPort.Inputs.NoSolder* Default `false`.
 * - `[transactionLoaderWrOpts]` _!Object?_ The options to pass to the _TransactionLoaderWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts* Default `{}`.
 * - `[transactionFoundWrOpts]` _!Object?_ The options to pass to the _TransactionFoundWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts* Default `{}`.
 * - `[transactionLoInWrOpts]` _!Object?_ The options to pass to the _TransactionLoInWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts* Default `{}`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[notFoundWrOpts]` _!Object?_ The options to pass to the _NotFoundWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[rateLaOpts]` _!Object?_ The options to pass to the _RateLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.RateLaOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[transactionLoInOpts]` _!Object?_ The options to pass to the _TransactionLoIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts* Default `{}`.
 * - `[reloadGetTransactionBuOpts]` _!Object?_ The options to pass to the _ReloadGetTransactionBu_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts* Default `{}`.
 * - `[getTransactionErrorWrOpts]` _!Object?_ The options to pass to the _GetTransactionErrorWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts* Default `{}`.
 * - `[getTransactionErrorLaOpts]` _!Object?_ The options to pass to the _GetTransactionErrorLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts* Default `{}`.
 * - `[fixedLockOpts]` _!Object?_ The options to pass to the _FixedLock_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.FixedLockOpts* Default `{}`.
 * - `[transactionInfoOpts]` _!Object?_ The options to pass to the _TransactionInfo_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITransactionInfoHeadElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TransactionInfoHeadMemory, port?: !xyz.swapee.wc.ITransactionInfoHeadElement.Inputs) => ?} xyz.swapee.wc.ITransactionInfoHeadElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElement.__inducer<!xyz.swapee.wc.ITransactionInfoHeadElement>} xyz.swapee.wc.ITransactionInfoHeadElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.TransactionInfoHeadMemory} [model] The model of the component into which to induce the state.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ITransactionInfoHeadOuterCore.WeakModel.Core* ⤴ *ITransactionInfoHeadOuterCore.WeakModel.Core* Default `null`.
 * - `[transactionInfoSel=""]` _string?_ The query to discover the _TransactionInfo_ VDU. ⤴ *ITransactionInfoHeadDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITransactionInfoHeadElementPort.Inputs.NoSolder* Default `false`.
 * - `[transactionLoaderWrOpts]` _!Object?_ The options to pass to the _TransactionLoaderWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts* Default `{}`.
 * - `[transactionFoundWrOpts]` _!Object?_ The options to pass to the _TransactionFoundWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts* Default `{}`.
 * - `[transactionLoInWrOpts]` _!Object?_ The options to pass to the _TransactionLoInWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts* Default `{}`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[notFoundWrOpts]` _!Object?_ The options to pass to the _NotFoundWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[rateLaOpts]` _!Object?_ The options to pass to the _RateLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.RateLaOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[transactionLoInOpts]` _!Object?_ The options to pass to the _TransactionLoIn_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts* Default `{}`.
 * - `[reloadGetTransactionBuOpts]` _!Object?_ The options to pass to the _ReloadGetTransactionBu_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts* Default `{}`.
 * - `[getTransactionErrorWrOpts]` _!Object?_ The options to pass to the _GetTransactionErrorWr_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts* Default `{}`.
 * - `[getTransactionErrorLaOpts]` _!Object?_ The options to pass to the _GetTransactionErrorLa_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts* Default `{}`.
 * - `[fixedLockOpts]` _!Object?_ The options to pass to the _FixedLock_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.FixedLockOpts* Default `{}`.
 * - `[transactionInfoOpts]` _!Object?_ The options to pass to the _TransactionInfo_ vdu. ⤴ *ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ITransactionInfoHeadElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoHeadElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/140-ITransactionInfoHeadElementPort.xml}  8f59633275c553dcb0cdab325e9e4af6 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITransactionInfoHeadElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadElementPort)} xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadElementPort} xyz.swapee.wc.TransactionInfoHeadElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadElementPort
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.constructor&xyz.swapee.wc.TransactionInfoHeadElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadElementPort|typeof xyz.swapee.wc.TransactionInfoHeadElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadElementPort|typeof xyz.swapee.wc.TransactionInfoHeadElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadElementPort|typeof xyz.swapee.wc.TransactionInfoHeadElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadElementPort.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadElementPort} xyz.swapee.wc.TransactionInfoHeadElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs>)} xyz.swapee.wc.ITransactionInfoHeadElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ITransactionInfoHeadElementPort
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadElementPort.Initialese>)} xyz.swapee.wc.TransactionInfoHeadElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort} xyz.swapee.wc.ITransactionInfoHeadElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITransactionInfoHeadElementPort_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadElementPort
 * @implements {xyz.swapee.wc.ITransactionInfoHeadElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadElementPort.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadElementPort = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadElementPort.constructor&xyz.swapee.wc.ITransactionInfoHeadElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadElementPort}
 */
xyz.swapee.wc.TransactionInfoHeadElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadElementPort.
 * @interface xyz.swapee.wc.ITransactionInfoHeadElementPortFields
 */
xyz.swapee.wc.ITransactionInfoHeadElementPortFields = class { }
/**
 * The inputs to the _ITransactionInfoHeadElement_'s controller via its element port.
 */
xyz.swapee.wc.ITransactionInfoHeadElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITransactionInfoHeadElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElementPort} */
xyz.swapee.wc.RecordITransactionInfoHeadElementPort

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadElementPort} xyz.swapee.wc.BoundITransactionInfoHeadElementPort */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadElementPort} xyz.swapee.wc.BoundTransactionInfoHeadElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _TransactionLoaderWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts.transactionLoaderWrOpts

/**
 * The options to pass to the _TransactionFoundWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts.transactionFoundWrOpts

/**
 * The options to pass to the _TransactionLoInWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts.transactionLoInWrOpts

/**
 * The options to pass to the _NetworkFeeWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts.networkFeeWrOpts

/**
 * The options to pass to the _NetworkFeeLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts.networkFeeLaOpts

/**
 * The options to pass to the _PartnerFeeWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts.partnerFeeWrOpts

/**
 * The options to pass to the _PartnerFeeLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts.partnerFeeLaOpts

/**
 * The options to pass to the _NotFoundLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts.notFoundLaOpts

/**
 * The options to pass to the _InIm_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts.inImOpts

/**
 * The options to pass to the _OutIm_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts.outImOpts

/**
 * The options to pass to the _NotFoundWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts.notFoundWrOpts

/**
 * The options to pass to the _CryptoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts.cryptoInOpts

/**
 * The options to pass to the _CryptoOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts.cryptoOutOpts

/**
 * The options to pass to the _RateLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts.rateLaOpts

/**
 * The options to pass to the _AmountIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts.amountInOpts

/**
 * The options to pass to the _AmountOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts.amountOutOpts

/**
 * The options to pass to the _VisibleAmountWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts.visibleAmountWrOpts

/**
 * The options to pass to the _VisibleAmounLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts.visibleAmounLaOpts

/**
 * The options to pass to the _BreakdownBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts.breakdownBuOpts

/**
 * The options to pass to the _TransactionLoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts.transactionLoInOpts

/**
 * The options to pass to the _ReloadGetTransactionBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts.reloadGetTransactionBuOpts

/**
 * The options to pass to the _GetTransactionErrorWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts.getTransactionErrorWrOpts

/**
 * The options to pass to the _GetTransactionErrorLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts.getTransactionErrorLaOpts

/**
 * The options to pass to the _FixedLock_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts.fixedLockOpts

/**
 * The options to pass to the _TransactionInfo_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts.transactionInfoOpts

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts)} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts.typeof */
/**
 * The inputs to the _ITransactionInfoHeadElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.constructor&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NoSolder&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoaderWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionFoundWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.InImOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.OutImOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoInOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoOutOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.RateLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountInOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountOutOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmountWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmounLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.BreakdownBuOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.ReloadGetTransactionBuOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorWrOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorLaOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.FixedLockOpts&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionInfoOpts)} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoaderWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoaderWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionFoundWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionFoundWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.InImOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.InImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.OutImOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.OutImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoInOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoOutOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.RateLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.RateLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountInOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountOutOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmountWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmounLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmounLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.BreakdownBuOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.BreakdownBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.ReloadGetTransactionBuOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.ReloadGetTransactionBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorWrOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorLaOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.FixedLockOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.FixedLockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionInfoOpts} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionInfoOpts.typeof */
/**
 * The inputs to the _ITransactionInfoHeadElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs
 */
xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.constructor&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoaderWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionFoundWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.InImOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.OutImOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoInOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoOutOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.RateLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountInOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountOutOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmountWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmounLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.BreakdownBuOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.ReloadGetTransactionBuOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorWrOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorLaOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.FixedLockOpts.typeof&xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionInfoOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs

/**
 * Contains getters to cast the _ITransactionInfoHeadElementPort_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadElementPortCaster
 */
xyz.swapee.wc.ITransactionInfoHeadElementPortCaster = class { }
/**
 * Cast the _ITransactionInfoHeadElementPort_ instance into the _BoundITransactionInfoHeadElementPort_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadElementPort}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPortCaster.prototype.asITransactionInfoHeadElementPort
/**
 * Access the _TransactionInfoHeadElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadElementPort}
 */
xyz.swapee.wc.ITransactionInfoHeadElementPortCaster.prototype.superTransactionInfoHeadElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts The options to pass to the _TransactionLoaderWr_ vdu (optional overlay).
 * @prop {!Object} [transactionLoaderWrOpts] The options to pass to the _TransactionLoaderWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoaderWrOpts_Safe The options to pass to the _TransactionLoaderWr_ vdu (required overlay).
 * @prop {!Object} transactionLoaderWrOpts The options to pass to the _TransactionLoaderWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts The options to pass to the _TransactionFoundWr_ vdu (optional overlay).
 * @prop {!Object} [transactionFoundWrOpts] The options to pass to the _TransactionFoundWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionFoundWrOpts_Safe The options to pass to the _TransactionFoundWr_ vdu (required overlay).
 * @prop {!Object} transactionFoundWrOpts The options to pass to the _TransactionFoundWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts The options to pass to the _TransactionLoInWr_ vdu (optional overlay).
 * @prop {!Object} [transactionLoInWrOpts] The options to pass to the _TransactionLoInWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInWrOpts_Safe The options to pass to the _TransactionLoInWr_ vdu (required overlay).
 * @prop {!Object} transactionLoInWrOpts The options to pass to the _TransactionLoInWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu (optional overlay).
 * @prop {!Object} [networkFeeWrOpts] The options to pass to the _NetworkFeeWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeWrOpts_Safe The options to pass to the _NetworkFeeWr_ vdu (required overlay).
 * @prop {!Object} networkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu (optional overlay).
 * @prop {!Object} [networkFeeLaOpts] The options to pass to the _NetworkFeeLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NetworkFeeLaOpts_Safe The options to pass to the _NetworkFeeLa_ vdu (required overlay).
 * @prop {!Object} networkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu (optional overlay).
 * @prop {!Object} [partnerFeeWrOpts] The options to pass to the _PartnerFeeWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeWrOpts_Safe The options to pass to the _PartnerFeeWr_ vdu (required overlay).
 * @prop {!Object} partnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu (optional overlay).
 * @prop {!Object} [partnerFeeLaOpts] The options to pass to the _PartnerFeeLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.PartnerFeeLaOpts_Safe The options to pass to the _PartnerFeeLa_ vdu (required overlay).
 * @prop {!Object} partnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts The options to pass to the _NotFoundLa_ vdu (optional overlay).
 * @prop {!Object} [notFoundLaOpts] The options to pass to the _NotFoundLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundLaOpts_Safe The options to pass to the _NotFoundLa_ vdu (required overlay).
 * @prop {!Object} notFoundLaOpts The options to pass to the _NotFoundLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts The options to pass to the _InIm_ vdu (optional overlay).
 * @prop {!Object} [inImOpts] The options to pass to the _InIm_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.InImOpts_Safe The options to pass to the _InIm_ vdu (required overlay).
 * @prop {!Object} inImOpts The options to pass to the _InIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts The options to pass to the _OutIm_ vdu (optional overlay).
 * @prop {!Object} [outImOpts] The options to pass to the _OutIm_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.OutImOpts_Safe The options to pass to the _OutIm_ vdu (required overlay).
 * @prop {!Object} outImOpts The options to pass to the _OutIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts The options to pass to the _NotFoundWr_ vdu (optional overlay).
 * @prop {!Object} [notFoundWrOpts] The options to pass to the _NotFoundWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.NotFoundWrOpts_Safe The options to pass to the _NotFoundWr_ vdu (required overlay).
 * @prop {!Object} notFoundWrOpts The options to pass to the _NotFoundWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts The options to pass to the _CryptoIn_ vdu (optional overlay).
 * @prop {!Object} [cryptoInOpts] The options to pass to the _CryptoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoInOpts_Safe The options to pass to the _CryptoIn_ vdu (required overlay).
 * @prop {!Object} cryptoInOpts The options to pass to the _CryptoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts The options to pass to the _CryptoOut_ vdu (optional overlay).
 * @prop {!Object} [cryptoOutOpts] The options to pass to the _CryptoOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.CryptoOutOpts_Safe The options to pass to the _CryptoOut_ vdu (required overlay).
 * @prop {!Object} cryptoOutOpts The options to pass to the _CryptoOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts The options to pass to the _RateLa_ vdu (optional overlay).
 * @prop {!Object} [rateLaOpts] The options to pass to the _RateLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.RateLaOpts_Safe The options to pass to the _RateLa_ vdu (required overlay).
 * @prop {!Object} rateLaOpts The options to pass to the _RateLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {!Object} [amountInOpts] The options to pass to the _AmountIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {!Object} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts The options to pass to the _AmountOut_ vdu (optional overlay).
 * @prop {!Object} [amountOutOpts] The options to pass to the _AmountOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.AmountOutOpts_Safe The options to pass to the _AmountOut_ vdu (required overlay).
 * @prop {!Object} amountOutOpts The options to pass to the _AmountOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts The options to pass to the _VisibleAmountWr_ vdu (optional overlay).
 * @prop {!Object} [visibleAmountWrOpts] The options to pass to the _VisibleAmountWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmountWrOpts_Safe The options to pass to the _VisibleAmountWr_ vdu (required overlay).
 * @prop {!Object} visibleAmountWrOpts The options to pass to the _VisibleAmountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts The options to pass to the _VisibleAmounLa_ vdu (optional overlay).
 * @prop {!Object} [visibleAmounLaOpts] The options to pass to the _VisibleAmounLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.VisibleAmounLaOpts_Safe The options to pass to the _VisibleAmounLa_ vdu (required overlay).
 * @prop {!Object} visibleAmounLaOpts The options to pass to the _VisibleAmounLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts The options to pass to the _BreakdownBu_ vdu (optional overlay).
 * @prop {!Object} [breakdownBuOpts] The options to pass to the _BreakdownBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.BreakdownBuOpts_Safe The options to pass to the _BreakdownBu_ vdu (required overlay).
 * @prop {!Object} breakdownBuOpts The options to pass to the _BreakdownBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts The options to pass to the _TransactionLoIn_ vdu (optional overlay).
 * @prop {!Object} [transactionLoInOpts] The options to pass to the _TransactionLoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionLoInOpts_Safe The options to pass to the _TransactionLoIn_ vdu (required overlay).
 * @prop {!Object} transactionLoInOpts The options to pass to the _TransactionLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts The options to pass to the _ReloadGetTransactionBu_ vdu (optional overlay).
 * @prop {!Object} [reloadGetTransactionBuOpts] The options to pass to the _ReloadGetTransactionBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.ReloadGetTransactionBuOpts_Safe The options to pass to the _ReloadGetTransactionBu_ vdu (required overlay).
 * @prop {!Object} reloadGetTransactionBuOpts The options to pass to the _ReloadGetTransactionBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts The options to pass to the _GetTransactionErrorWr_ vdu (optional overlay).
 * @prop {!Object} [getTransactionErrorWrOpts] The options to pass to the _GetTransactionErrorWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorWrOpts_Safe The options to pass to the _GetTransactionErrorWr_ vdu (required overlay).
 * @prop {!Object} getTransactionErrorWrOpts The options to pass to the _GetTransactionErrorWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts The options to pass to the _GetTransactionErrorLa_ vdu (optional overlay).
 * @prop {!Object} [getTransactionErrorLaOpts] The options to pass to the _GetTransactionErrorLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.GetTransactionErrorLaOpts_Safe The options to pass to the _GetTransactionErrorLa_ vdu (required overlay).
 * @prop {!Object} getTransactionErrorLaOpts The options to pass to the _GetTransactionErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts The options to pass to the _FixedLock_ vdu (optional overlay).
 * @prop {!Object} [fixedLockOpts] The options to pass to the _FixedLock_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.FixedLockOpts_Safe The options to pass to the _FixedLock_ vdu (required overlay).
 * @prop {!Object} fixedLockOpts The options to pass to the _FixedLock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts The options to pass to the _TransactionInfo_ vdu (optional overlay).
 * @prop {!Object} [transactionInfoOpts] The options to pass to the _TransactionInfo_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs.TransactionInfoOpts_Safe The options to pass to the _TransactionInfo_ vdu (required overlay).
 * @prop {!Object} transactionInfoOpts The options to pass to the _TransactionInfo_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoaderWrOpts The options to pass to the _TransactionLoaderWr_ vdu (optional overlay).
 * @prop {*} [transactionLoaderWrOpts=null] The options to pass to the _TransactionLoaderWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoaderWrOpts_Safe The options to pass to the _TransactionLoaderWr_ vdu (required overlay).
 * @prop {*} transactionLoaderWrOpts The options to pass to the _TransactionLoaderWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionFoundWrOpts The options to pass to the _TransactionFoundWr_ vdu (optional overlay).
 * @prop {*} [transactionFoundWrOpts=null] The options to pass to the _TransactionFoundWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionFoundWrOpts_Safe The options to pass to the _TransactionFoundWr_ vdu (required overlay).
 * @prop {*} transactionFoundWrOpts The options to pass to the _TransactionFoundWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInWrOpts The options to pass to the _TransactionLoInWr_ vdu (optional overlay).
 * @prop {*} [transactionLoInWrOpts=null] The options to pass to the _TransactionLoInWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInWrOpts_Safe The options to pass to the _TransactionLoInWr_ vdu (required overlay).
 * @prop {*} transactionLoInWrOpts The options to pass to the _TransactionLoInWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu (optional overlay).
 * @prop {*} [networkFeeWrOpts=null] The options to pass to the _NetworkFeeWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeWrOpts_Safe The options to pass to the _NetworkFeeWr_ vdu (required overlay).
 * @prop {*} networkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu (optional overlay).
 * @prop {*} [networkFeeLaOpts=null] The options to pass to the _NetworkFeeLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NetworkFeeLaOpts_Safe The options to pass to the _NetworkFeeLa_ vdu (required overlay).
 * @prop {*} networkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu (optional overlay).
 * @prop {*} [partnerFeeWrOpts=null] The options to pass to the _PartnerFeeWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeWrOpts_Safe The options to pass to the _PartnerFeeWr_ vdu (required overlay).
 * @prop {*} partnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu (optional overlay).
 * @prop {*} [partnerFeeLaOpts=null] The options to pass to the _PartnerFeeLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.PartnerFeeLaOpts_Safe The options to pass to the _PartnerFeeLa_ vdu (required overlay).
 * @prop {*} partnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundLaOpts The options to pass to the _NotFoundLa_ vdu (optional overlay).
 * @prop {*} [notFoundLaOpts=null] The options to pass to the _NotFoundLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundLaOpts_Safe The options to pass to the _NotFoundLa_ vdu (required overlay).
 * @prop {*} notFoundLaOpts The options to pass to the _NotFoundLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.InImOpts The options to pass to the _InIm_ vdu (optional overlay).
 * @prop {*} [inImOpts=null] The options to pass to the _InIm_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.InImOpts_Safe The options to pass to the _InIm_ vdu (required overlay).
 * @prop {*} inImOpts The options to pass to the _InIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.OutImOpts The options to pass to the _OutIm_ vdu (optional overlay).
 * @prop {*} [outImOpts=null] The options to pass to the _OutIm_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.OutImOpts_Safe The options to pass to the _OutIm_ vdu (required overlay).
 * @prop {*} outImOpts The options to pass to the _OutIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundWrOpts The options to pass to the _NotFoundWr_ vdu (optional overlay).
 * @prop {*} [notFoundWrOpts=null] The options to pass to the _NotFoundWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.NotFoundWrOpts_Safe The options to pass to the _NotFoundWr_ vdu (required overlay).
 * @prop {*} notFoundWrOpts The options to pass to the _NotFoundWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoInOpts The options to pass to the _CryptoIn_ vdu (optional overlay).
 * @prop {*} [cryptoInOpts=null] The options to pass to the _CryptoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoInOpts_Safe The options to pass to the _CryptoIn_ vdu (required overlay).
 * @prop {*} cryptoInOpts The options to pass to the _CryptoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoOutOpts The options to pass to the _CryptoOut_ vdu (optional overlay).
 * @prop {*} [cryptoOutOpts=null] The options to pass to the _CryptoOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.CryptoOutOpts_Safe The options to pass to the _CryptoOut_ vdu (required overlay).
 * @prop {*} cryptoOutOpts The options to pass to the _CryptoOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.RateLaOpts The options to pass to the _RateLa_ vdu (optional overlay).
 * @prop {*} [rateLaOpts=null] The options to pass to the _RateLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.RateLaOpts_Safe The options to pass to the _RateLa_ vdu (required overlay).
 * @prop {*} rateLaOpts The options to pass to the _RateLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {*} [amountInOpts=null] The options to pass to the _AmountIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {*} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountOutOpts The options to pass to the _AmountOut_ vdu (optional overlay).
 * @prop {*} [amountOutOpts=null] The options to pass to the _AmountOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.AmountOutOpts_Safe The options to pass to the _AmountOut_ vdu (required overlay).
 * @prop {*} amountOutOpts The options to pass to the _AmountOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmountWrOpts The options to pass to the _VisibleAmountWr_ vdu (optional overlay).
 * @prop {*} [visibleAmountWrOpts=null] The options to pass to the _VisibleAmountWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmountWrOpts_Safe The options to pass to the _VisibleAmountWr_ vdu (required overlay).
 * @prop {*} visibleAmountWrOpts The options to pass to the _VisibleAmountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmounLaOpts The options to pass to the _VisibleAmounLa_ vdu (optional overlay).
 * @prop {*} [visibleAmounLaOpts=null] The options to pass to the _VisibleAmounLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.VisibleAmounLaOpts_Safe The options to pass to the _VisibleAmounLa_ vdu (required overlay).
 * @prop {*} visibleAmounLaOpts The options to pass to the _VisibleAmounLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.BreakdownBuOpts The options to pass to the _BreakdownBu_ vdu (optional overlay).
 * @prop {*} [breakdownBuOpts=null] The options to pass to the _BreakdownBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.BreakdownBuOpts_Safe The options to pass to the _BreakdownBu_ vdu (required overlay).
 * @prop {*} breakdownBuOpts The options to pass to the _BreakdownBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInOpts The options to pass to the _TransactionLoIn_ vdu (optional overlay).
 * @prop {*} [transactionLoInOpts=null] The options to pass to the _TransactionLoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionLoInOpts_Safe The options to pass to the _TransactionLoIn_ vdu (required overlay).
 * @prop {*} transactionLoInOpts The options to pass to the _TransactionLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.ReloadGetTransactionBuOpts The options to pass to the _ReloadGetTransactionBu_ vdu (optional overlay).
 * @prop {*} [reloadGetTransactionBuOpts=null] The options to pass to the _ReloadGetTransactionBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.ReloadGetTransactionBuOpts_Safe The options to pass to the _ReloadGetTransactionBu_ vdu (required overlay).
 * @prop {*} reloadGetTransactionBuOpts The options to pass to the _ReloadGetTransactionBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorWrOpts The options to pass to the _GetTransactionErrorWr_ vdu (optional overlay).
 * @prop {*} [getTransactionErrorWrOpts=null] The options to pass to the _GetTransactionErrorWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorWrOpts_Safe The options to pass to the _GetTransactionErrorWr_ vdu (required overlay).
 * @prop {*} getTransactionErrorWrOpts The options to pass to the _GetTransactionErrorWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorLaOpts The options to pass to the _GetTransactionErrorLa_ vdu (optional overlay).
 * @prop {*} [getTransactionErrorLaOpts=null] The options to pass to the _GetTransactionErrorLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.GetTransactionErrorLaOpts_Safe The options to pass to the _GetTransactionErrorLa_ vdu (required overlay).
 * @prop {*} getTransactionErrorLaOpts The options to pass to the _GetTransactionErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.FixedLockOpts The options to pass to the _FixedLock_ vdu (optional overlay).
 * @prop {*} [fixedLockOpts=null] The options to pass to the _FixedLock_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.FixedLockOpts_Safe The options to pass to the _FixedLock_ vdu (required overlay).
 * @prop {*} fixedLockOpts The options to pass to the _FixedLock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionInfoOpts The options to pass to the _TransactionInfo_ vdu (optional overlay).
 * @prop {*} [transactionInfoOpts=null] The options to pass to the _TransactionInfo_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadElementPort.WeakInputs.TransactionInfoOpts_Safe The options to pass to the _TransactionInfo_ vdu (required overlay).
 * @prop {*} transactionInfoOpts The options to pass to the _TransactionInfo_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/170-ITransactionInfoHeadDesigner.xml}  db533f341305073524d9b109cba927dd */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ITransactionInfoHeadDesigner
 */
xyz.swapee.wc.ITransactionInfoHeadDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.TransactionInfoHeadClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITransactionInfoHead />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TransactionInfoHeadClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITransactionInfoHead />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TransactionInfoHeadClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITransactionInfoHeadDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `TransactionInfo` _typeof xyz.swapee.wc.ITransactionInfoController_
   * - `TransactionInfoHead` _typeof ITransactionInfoHeadController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITransactionInfoHeadDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `TransactionInfo` _typeof xyz.swapee.wc.ITransactionInfoController_
   * - `TransactionInfoHead` _typeof ITransactionInfoHeadController_
   * - `This` _typeof ITransactionInfoHeadController_
   * @param {!xyz.swapee.wc.ITransactionInfoHeadDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `TransactionInfo` _!xyz.swapee.wc.TransactionInfoMemory_
   * - `TransactionInfoHead` _!TransactionInfoHeadMemory_
   * - `This` _!TransactionInfoHeadMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TransactionInfoHeadClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TransactionInfoHeadClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ITransactionInfoHeadDesigner.prototype.constructor = xyz.swapee.wc.ITransactionInfoHeadDesigner

/**
 * A concrete class of _ITransactionInfoHeadDesigner_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadDesigner
 * @implements {xyz.swapee.wc.ITransactionInfoHeadDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.TransactionInfoHeadDesigner = class extends xyz.swapee.wc.ITransactionInfoHeadDesigner { }
xyz.swapee.wc.TransactionInfoHeadDesigner.prototype.constructor = xyz.swapee.wc.TransactionInfoHeadDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITransactionInfoController} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadController} TransactionInfoHead
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITransactionInfoController} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadController} TransactionInfoHead
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} TransactionInfo
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHead
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/200-TransactionInfoHeadLand.xml}  1fbfe4017da20198ba4fd994f23a5ec3 */
/**
 * The surrounding of the _ITransactionInfoHead_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.TransactionInfoHeadLand
 */
xyz.swapee.wc.TransactionInfoHeadLand = class { }
/**
 *
 */
xyz.swapee.wc.TransactionInfoHeadLand.prototype.TransactionInfo = /** @type {xyz.swapee.wc.ITransactionInfo} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/40-ITransactionInfoHeadDisplay.xml}  819a5fcefcc7f5ef612753aedbb79f9b */
/**
 * @typedef {Object} $xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese
 * @prop {HTMLDivElement} [TransactionLoaderWr]
 * @prop {HTMLDivElement} [TransactionFoundWr]
 * @prop {HTMLDivElement} [TransactionLoInWr]
 * @prop {HTMLSpanElement} [NetworkFeeWr]
 * @prop {HTMLSpanElement} [NetworkFeeLa]
 * @prop {HTMLSpanElement} [PartnerFeeWr]
 * @prop {HTMLSpanElement} [PartnerFeeLa]
 * @prop {HTMLSpanElement} [NotFoundLa]
 * @prop {HTMLImageElement} [InIm]
 * @prop {HTMLImageElement} [OutIm]
 * @prop {HTMLDivElement} [NotFoundWr]
 * @prop {!Array<!HTMLSpanElement>} [CryptoIns]
 * @prop {!Array<!HTMLSpanElement>} [CryptoOuts]
 * @prop {HTMLSpanElement} [RateLa]
 * @prop {HTMLSpanElement} [AmountIn]
 * @prop {HTMLSpanElement} [AmountOut]
 * @prop {HTMLSpanElement} [VisibleAmountWr]
 * @prop {HTMLSpanElement} [VisibleAmounLa]
 * @prop {HTMLButtonElement} [BreakdownBu]
 * @prop {HTMLSpanElement} [TransactionLoIn]
 * @prop {HTMLButtonElement} [ReloadGetTransactionBu]
 * @prop {HTMLSpanElement} [GetTransactionErrorWr]
 * @prop {HTMLSpanElement} [GetTransactionErrorLa]
 * @prop {HTMLSpanElement} [FixedLock]
 * @prop {HTMLElement} [TransactionInfo] The via for the _TransactionInfo_ peer.
 */
/** @typedef {$xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITransactionInfoHeadDisplay.Settings>} xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadDisplay)} xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadDisplay} xyz.swapee.wc.TransactionInfoHeadDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadDisplay
 */
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.constructor&xyz.swapee.wc.TransactionInfoHeadDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.TransactionInfoHeadDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.TransactionInfoHeadDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.TransactionInfoHeadDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadDisplay} xyz.swapee.wc.TransactionInfoHeadDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.TransactionInfoHeadMemory, !HTMLDivElement, !xyz.swapee.wc.ITransactionInfoHeadDisplay.Settings, xyz.swapee.wc.ITransactionInfoHeadDisplay.Queries, null>)} xyz.swapee.wc.ITransactionInfoHeadDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ITransactionInfoHead_.
 * @interface xyz.swapee.wc.ITransactionInfoHeadDisplay
 */
xyz.swapee.wc.ITransactionInfoHeadDisplay = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadDisplay.paint} */
xyz.swapee.wc.ITransactionInfoHeadDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese>)} xyz.swapee.wc.TransactionInfoHeadDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadDisplay} xyz.swapee.wc.ITransactionInfoHeadDisplay.typeof */
/**
 * A concrete class of _ITransactionInfoHeadDisplay_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadDisplay
 * @implements {xyz.swapee.wc.ITransactionInfoHeadDisplay} Display for presenting information from the _ITransactionInfoHead_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadDisplay = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadDisplay.constructor&xyz.swapee.wc.ITransactionInfoHeadDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.TransactionInfoHeadDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadDisplay.
 * @interface xyz.swapee.wc.ITransactionInfoHeadDisplayFields
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ITransactionInfoHeadDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ITransactionInfoHeadDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.TransactionLoaderWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.TransactionFoundWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.TransactionLoInWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.NetworkFeeWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.NetworkFeeLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.PartnerFeeWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.PartnerFeeLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.NotFoundLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.InIm = /** @type {HTMLImageElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.OutIm = /** @type {HTMLImageElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.NotFoundWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.CryptoIns = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.CryptoOuts = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.RateLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.AmountIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.AmountOut = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.VisibleAmountWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.VisibleAmounLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.BreakdownBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.TransactionLoIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.ReloadGetTransactionBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.GetTransactionErrorWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.GetTransactionErrorLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.FixedLock = /** @type {HTMLSpanElement} */ (void 0)
/**
 * The via for the _TransactionInfo_ peer. Default `null`.
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayFields.prototype.TransactionInfo = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadDisplay} */
xyz.swapee.wc.RecordITransactionInfoHeadDisplay

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadDisplay} xyz.swapee.wc.BoundITransactionInfoHeadDisplay */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadDisplay} xyz.swapee.wc.BoundTransactionInfoHeadDisplay */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadDisplay.Queries} xyz.swapee.wc.ITransactionInfoHeadDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoHeadDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [transactionInfoSel=""] The query to discover the _TransactionInfo_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _ITransactionInfoHeadDisplay_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadDisplayCaster
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayCaster = class { }
/**
 * Cast the _ITransactionInfoHeadDisplay_ instance into the _BoundITransactionInfoHeadDisplay_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadDisplay}
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayCaster.prototype.asITransactionInfoHeadDisplay
/**
 * Cast the _ITransactionInfoHeadDisplay_ instance into the _BoundITransactionInfoHeadScreen_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadScreen}
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayCaster.prototype.asITransactionInfoHeadScreen
/**
 * Access the _TransactionInfoHeadDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadDisplay}
 */
xyz.swapee.wc.ITransactionInfoHeadDisplayCaster.prototype.superTransactionInfoHeadDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TransactionInfoHeadMemory, land: null) => void} xyz.swapee.wc.ITransactionInfoHeadDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadDisplay.__paint<!xyz.swapee.wc.ITransactionInfoHeadDisplay>} xyz.swapee.wc.ITransactionInfoHeadDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TransactionInfoHeadMemory} memory The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoHeadDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoHeadDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/40-ITransactionInfoHeadDisplayBack.xml}  aa5db1a28894729d7b980160b8fcaaba */
/**
 * @typedef {Object} $xyz.swapee.wc.back.ITransactionInfoHeadDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionLoaderWr]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionFoundWr]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionLoInWr]
 * @prop {!com.webcircuits.IHtmlTwin} [NetworkFeeWr]
 * @prop {!com.webcircuits.IHtmlTwin} [NetworkFeeLa]
 * @prop {!com.webcircuits.IHtmlTwin} [PartnerFeeWr]
 * @prop {!com.webcircuits.IHtmlTwin} [PartnerFeeLa]
 * @prop {!com.webcircuits.IHtmlTwin} [NotFoundLa]
 * @prop {!com.webcircuits.IHtmlTwin} [InIm]
 * @prop {!com.webcircuits.IHtmlTwin} [OutIm]
 * @prop {!com.webcircuits.IHtmlTwin} [NotFoundWr]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [CryptoIns]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [CryptoOuts]
 * @prop {!com.webcircuits.IHtmlTwin} [RateLa]
 * @prop {!com.webcircuits.IHtmlTwin} [AmountIn]
 * @prop {!com.webcircuits.IHtmlTwin} [AmountOut]
 * @prop {!com.webcircuits.IHtmlTwin} [VisibleAmountWr]
 * @prop {!com.webcircuits.IHtmlTwin} [VisibleAmounLa]
 * @prop {!com.webcircuits.IHtmlTwin} [BreakdownBu]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionLoIn]
 * @prop {!com.webcircuits.IHtmlTwin} [ReloadGetTransactionBu]
 * @prop {!com.webcircuits.IHtmlTwin} [GetTransactionErrorWr]
 * @prop {!com.webcircuits.IHtmlTwin} [GetTransactionErrorLa]
 * @prop {!com.webcircuits.IHtmlTwin} [FixedLock]
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionInfo] The via for the _TransactionInfo_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.ITransactionInfoHeadDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TransactionInfoHeadClasses>} xyz.swapee.wc.back.ITransactionInfoHeadDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoHeadDisplay)} xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay} xyz.swapee.wc.back.TransactionInfoHeadDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoHeadDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.constructor&xyz.swapee.wc.back.TransactionInfoHeadDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoHeadDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.TransactionInfoHeadClasses, !xyz.swapee.wc.TransactionInfoHeadLand>)} xyz.swapee.wc.back.ITransactionInfoHeadDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadDisplay
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplay = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoHeadDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ITransactionInfoHeadDisplay.paint} */
xyz.swapee.wc.back.ITransactionInfoHeadDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoHeadDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadDisplay.Initialese>)} xyz.swapee.wc.back.TransactionInfoHeadDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoHeadDisplay} xyz.swapee.wc.back.ITransactionInfoHeadDisplay.typeof */
/**
 * A concrete class of _ITransactionInfoHeadDisplay_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoHeadDisplay
 * @implements {xyz.swapee.wc.back.ITransactionInfoHeadDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoHeadDisplay = class extends /** @type {xyz.swapee.wc.back.TransactionInfoHeadDisplay.constructor&xyz.swapee.wc.back.ITransactionInfoHeadDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.TransactionInfoHeadDisplay.prototype.constructor = xyz.swapee.wc.back.TransactionInfoHeadDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay}
 */
xyz.swapee.wc.back.TransactionInfoHeadDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadDisplay.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.TransactionLoaderWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.TransactionFoundWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.TransactionLoInWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.NetworkFeeWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.NetworkFeeLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.PartnerFeeWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.PartnerFeeLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.NotFoundLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.InIm = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.OutIm = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.NotFoundWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.CryptoIns = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.CryptoOuts = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.RateLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.AmountIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.AmountOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.VisibleAmountWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.VisibleAmounLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.BreakdownBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.TransactionLoIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.ReloadGetTransactionBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.GetTransactionErrorWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.GetTransactionErrorLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.FixedLock = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _TransactionInfo_ peer.
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayFields.prototype.TransactionInfo = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadDisplay} */
xyz.swapee.wc.back.RecordITransactionInfoHeadDisplay

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadDisplay} xyz.swapee.wc.back.BoundITransactionInfoHeadDisplay */

/** @typedef {xyz.swapee.wc.back.TransactionInfoHeadDisplay} xyz.swapee.wc.back.BoundTransactionInfoHeadDisplay */

/**
 * Contains getters to cast the _ITransactionInfoHeadDisplay_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadDisplayCaster
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayCaster = class { }
/**
 * Cast the _ITransactionInfoHeadDisplay_ instance into the _BoundITransactionInfoHeadDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoHeadDisplay}
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayCaster.prototype.asITransactionInfoHeadDisplay
/**
 * Access the _TransactionInfoHeadDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoHeadDisplay}
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplayCaster.prototype.superTransactionInfoHeadDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.TransactionInfoHeadMemory, land?: !xyz.swapee.wc.TransactionInfoHeadLand) => void} xyz.swapee.wc.back.ITransactionInfoHeadDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadDisplay.__paint<!xyz.swapee.wc.back.ITransactionInfoHeadDisplay>} xyz.swapee.wc.back.ITransactionInfoHeadDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoHeadDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TransactionInfoHeadMemory} [memory] The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.TransactionInfoHeadLand} [land] The land data.
 * - `TransactionInfo` _xyz.swapee.wc.ITransactionInfo_
 * @return {void}
 */
xyz.swapee.wc.back.ITransactionInfoHeadDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ITransactionInfoHeadDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/41-TransactionInfoHeadClasses.xml}  ea463044500413e2dcf270d5831fccd4 */
/**
 * The classes of the _ITransactionInfoHeadDisplay_.
 * @record xyz.swapee.wc.TransactionInfoHeadClasses
 */
xyz.swapee.wc.TransactionInfoHeadClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.TransactionInfoHeadClasses.prototype.Class = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.TransactionInfoHeadClasses.prototype.props = /** @type {xyz.swapee.wc.TransactionInfoHeadClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/50-ITransactionInfoHeadController.xml}  406a844ab77fa11de0afeed27e823dd8 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs, !xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel>} xyz.swapee.wc.ITransactionInfoHeadController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadController)} xyz.swapee.wc.AbstractTransactionInfoHeadController.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadController} xyz.swapee.wc.TransactionInfoHeadController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadController` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadController
 */
xyz.swapee.wc.AbstractTransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadController.constructor&xyz.swapee.wc.TransactionInfoHeadController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadController.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadController.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadController}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadController}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadController}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadController}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadController.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadController} xyz.swapee.wc.TransactionInfoHeadControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs, !xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ITransactionInfoHeadOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs, !xyz.swapee.wc.ITransactionInfoHeadController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs, !xyz.swapee.wc.TransactionInfoHeadMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs>)} xyz.swapee.wc.ITransactionInfoHeadController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ITransactionInfoHeadController
 */
xyz.swapee.wc.ITransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoHeadController.resetPort} */
xyz.swapee.wc.ITransactionInfoHeadController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadController&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadController.Initialese>)} xyz.swapee.wc.TransactionInfoHeadController.constructor */
/**
 * A concrete class of _ITransactionInfoHeadController_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadController
 * @implements {xyz.swapee.wc.ITransactionInfoHeadController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadController.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadController.constructor&xyz.swapee.wc.ITransactionInfoHeadController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadController}
 */
xyz.swapee.wc.TransactionInfoHeadController.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadController.
 * @interface xyz.swapee.wc.ITransactionInfoHeadControllerFields
 */
xyz.swapee.wc.ITransactionInfoHeadControllerFields = class { }
/**
 * The inputs to the _ITransactionInfoHead_'s controller.
 */
xyz.swapee.wc.ITransactionInfoHeadControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoHeadController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ITransactionInfoHeadControllerFields.prototype.props = /** @type {xyz.swapee.wc.ITransactionInfoHeadController} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadController} */
xyz.swapee.wc.RecordITransactionInfoHeadController

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadController} xyz.swapee.wc.BoundITransactionInfoHeadController */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadController} xyz.swapee.wc.BoundTransactionInfoHeadController */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadPort.Inputs} xyz.swapee.wc.ITransactionInfoHeadController.Inputs The inputs to the _ITransactionInfoHead_'s controller. */

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadPort.WeakInputs} xyz.swapee.wc.ITransactionInfoHeadController.WeakInputs The inputs to the _ITransactionInfoHead_'s controller. */

/**
 * Contains getters to cast the _ITransactionInfoHeadController_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadControllerCaster
 */
xyz.swapee.wc.ITransactionInfoHeadControllerCaster = class { }
/**
 * Cast the _ITransactionInfoHeadController_ instance into the _BoundITransactionInfoHeadController_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadController}
 */
xyz.swapee.wc.ITransactionInfoHeadControllerCaster.prototype.asITransactionInfoHeadController
/**
 * Cast the _ITransactionInfoHeadController_ instance into the _BoundITransactionInfoHeadProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadProcessor}
 */
xyz.swapee.wc.ITransactionInfoHeadControllerCaster.prototype.asITransactionInfoHeadProcessor
/**
 * Access the _TransactionInfoHeadController_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadController}
 */
xyz.swapee.wc.ITransactionInfoHeadControllerCaster.prototype.superTransactionInfoHeadController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoHeadController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoHeadController.__resetPort<!xyz.swapee.wc.ITransactionInfoHeadController>} xyz.swapee.wc.ITransactionInfoHeadController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoHeadController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoHeadController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/51-ITransactionInfoHeadControllerFront.xml}  6b364fd5c86fdc566c07f77d7549c6a6 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ITransactionInfoHeadController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TransactionInfoHeadController)} xyz.swapee.wc.front.AbstractTransactionInfoHeadController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TransactionInfoHeadController} xyz.swapee.wc.front.TransactionInfoHeadController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITransactionInfoHeadController` interface.
 * @constructor xyz.swapee.wc.front.AbstractTransactionInfoHeadController
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.front.AbstractTransactionInfoHeadController.constructor&xyz.swapee.wc.front.TransactionInfoHeadController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTransactionInfoHeadController.prototype.constructor = xyz.swapee.wc.front.AbstractTransactionInfoHeadController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadController.class = /** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadController|typeof xyz.swapee.wc.front.TransactionInfoHeadController)|(!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT|typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadController|typeof xyz.swapee.wc.front.TransactionInfoHeadController)|(!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT|typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadController|typeof xyz.swapee.wc.front.TransactionInfoHeadController)|(!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT|typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITransactionInfoHeadController.Initialese[]) => xyz.swapee.wc.front.ITransactionInfoHeadController} xyz.swapee.wc.front.TransactionInfoHeadControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITransactionInfoHeadControllerCaster&xyz.swapee.wc.front.ITransactionInfoHeadControllerAT)} xyz.swapee.wc.front.ITransactionInfoHeadController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITransactionInfoHeadControllerAT} xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ITransactionInfoHeadController
 */
xyz.swapee.wc.front.ITransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.front.ITransactionInfoHeadController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITransactionInfoHeadController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITransactionInfoHeadController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoHeadController.Initialese>)} xyz.swapee.wc.front.TransactionInfoHeadController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITransactionInfoHeadController} xyz.swapee.wc.front.ITransactionInfoHeadController.typeof */
/**
 * A concrete class of _ITransactionInfoHeadController_ instances.
 * @constructor xyz.swapee.wc.front.TransactionInfoHeadController
 * @implements {xyz.swapee.wc.front.ITransactionInfoHeadController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoHeadController.Initialese>} ‎
 */
xyz.swapee.wc.front.TransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.front.TransactionInfoHeadController.constructor&xyz.swapee.wc.front.ITransactionInfoHeadController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TransactionInfoHeadController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadController}
 */
xyz.swapee.wc.front.TransactionInfoHeadController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITransactionInfoHeadController} */
xyz.swapee.wc.front.RecordITransactionInfoHeadController

/** @typedef {xyz.swapee.wc.front.ITransactionInfoHeadController} xyz.swapee.wc.front.BoundITransactionInfoHeadController */

/** @typedef {xyz.swapee.wc.front.TransactionInfoHeadController} xyz.swapee.wc.front.BoundTransactionInfoHeadController */

/**
 * Contains getters to cast the _ITransactionInfoHeadController_ interface.
 * @interface xyz.swapee.wc.front.ITransactionInfoHeadControllerCaster
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerCaster = class { }
/**
 * Cast the _ITransactionInfoHeadController_ instance into the _BoundITransactionInfoHeadController_ type.
 * @type {!xyz.swapee.wc.front.BoundITransactionInfoHeadController}
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerCaster.prototype.asITransactionInfoHeadController
/**
 * Access the _TransactionInfoHeadController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTransactionInfoHeadController}
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerCaster.prototype.superTransactionInfoHeadController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/52-ITransactionInfoHeadControllerBack.xml}  08e14b34a901a52fd8c99562180b43d0 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs>&xyz.swapee.wc.ITransactionInfoHeadController.Initialese} xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoHeadController)} xyz.swapee.wc.back.AbstractTransactionInfoHeadController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoHeadController} xyz.swapee.wc.back.TransactionInfoHeadController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoHeadController` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoHeadController
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoHeadController.constructor&xyz.swapee.wc.back.TransactionInfoHeadController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoHeadController.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoHeadController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadController.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadController|typeof xyz.swapee.wc.back.TransactionInfoHeadController)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadController|typeof xyz.swapee.wc.back.TransactionInfoHeadController)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadController|typeof xyz.swapee.wc.back.TransactionInfoHeadController)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoHeadController} xyz.swapee.wc.back.TransactionInfoHeadControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoHeadControllerCaster&xyz.swapee.wc.ITransactionInfoHeadController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ITransactionInfoHeadController.Inputs>)} xyz.swapee.wc.back.ITransactionInfoHeadController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadController
 */
xyz.swapee.wc.back.ITransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoHeadController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoHeadController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoHeadController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoHeadController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese>)} xyz.swapee.wc.back.TransactionInfoHeadController.constructor */
/**
 * A concrete class of _ITransactionInfoHeadController_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoHeadController
 * @implements {xyz.swapee.wc.back.ITransactionInfoHeadController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoHeadController = class extends /** @type {xyz.swapee.wc.back.TransactionInfoHeadController.constructor&xyz.swapee.wc.back.ITransactionInfoHeadController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoHeadController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadController}
 */
xyz.swapee.wc.back.TransactionInfoHeadController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadController} */
xyz.swapee.wc.back.RecordITransactionInfoHeadController

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadController} xyz.swapee.wc.back.BoundITransactionInfoHeadController */

/** @typedef {xyz.swapee.wc.back.TransactionInfoHeadController} xyz.swapee.wc.back.BoundTransactionInfoHeadController */

/**
 * Contains getters to cast the _ITransactionInfoHeadController_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadControllerCaster
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerCaster = class { }
/**
 * Cast the _ITransactionInfoHeadController_ instance into the _BoundITransactionInfoHeadController_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoHeadController}
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerCaster.prototype.asITransactionInfoHeadController
/**
 * Access the _TransactionInfoHeadController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoHeadController}
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerCaster.prototype.superTransactionInfoHeadController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/53-ITransactionInfoHeadControllerAR.xml}  87e46f5f1433ed819e1c4948f50e88b8 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITransactionInfoHeadController.Initialese} xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoHeadControllerAR)} xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR} xyz.swapee.wc.back.TransactionInfoHeadControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoHeadControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.constructor&xyz.swapee.wc.back.TransactionInfoHeadControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR|typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR|typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR|typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoHeadController|typeof xyz.swapee.wc.TransactionInfoHeadController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoHeadControllerAR} xyz.swapee.wc.back.TransactionInfoHeadControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoHeadControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITransactionInfoHeadController)} xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITransactionInfoHeadControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadControllerAR
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerAR = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITransactionInfoHeadController.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoHeadControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.Initialese>)} xyz.swapee.wc.back.TransactionInfoHeadControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoHeadControllerAR} xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.typeof */
/**
 * A concrete class of _ITransactionInfoHeadControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoHeadControllerAR
 * @implements {xyz.swapee.wc.back.ITransactionInfoHeadControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ITransactionInfoHeadControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoHeadControllerAR = class extends /** @type {xyz.swapee.wc.back.TransactionInfoHeadControllerAR.constructor&xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoHeadControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadControllerAR}
 */
xyz.swapee.wc.back.TransactionInfoHeadControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadControllerAR} */
xyz.swapee.wc.back.RecordITransactionInfoHeadControllerAR

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadControllerAR} xyz.swapee.wc.back.BoundITransactionInfoHeadControllerAR */

/** @typedef {xyz.swapee.wc.back.TransactionInfoHeadControllerAR} xyz.swapee.wc.back.BoundTransactionInfoHeadControllerAR */

/**
 * Contains getters to cast the _ITransactionInfoHeadControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadControllerARCaster
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerARCaster = class { }
/**
 * Cast the _ITransactionInfoHeadControllerAR_ instance into the _BoundITransactionInfoHeadControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoHeadControllerAR}
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerARCaster.prototype.asITransactionInfoHeadControllerAR
/**
 * Access the _TransactionInfoHeadControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoHeadControllerAR}
 */
xyz.swapee.wc.back.ITransactionInfoHeadControllerARCaster.prototype.superTransactionInfoHeadControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/54-ITransactionInfoHeadControllerAT.xml}  6dcbbd67cccedc68957699ca524235cf */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TransactionInfoHeadControllerAT)} xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT} xyz.swapee.wc.front.TransactionInfoHeadControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITransactionInfoHeadControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.constructor&xyz.swapee.wc.front.TransactionInfoHeadControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT|typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT|typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT|typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.Initialese[]) => xyz.swapee.wc.front.ITransactionInfoHeadControllerAT} xyz.swapee.wc.front.TransactionInfoHeadControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITransactionInfoHeadControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITransactionInfoHeadControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ITransactionInfoHeadControllerAT
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerAT = class extends /** @type {xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITransactionInfoHeadControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.Initialese>)} xyz.swapee.wc.front.TransactionInfoHeadControllerAT.constructor */
/**
 * A concrete class of _ITransactionInfoHeadControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.TransactionInfoHeadControllerAT
 * @implements {xyz.swapee.wc.front.ITransactionInfoHeadControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITransactionInfoHeadControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.TransactionInfoHeadControllerAT = class extends /** @type {xyz.swapee.wc.front.TransactionInfoHeadControllerAT.constructor&xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TransactionInfoHeadControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadControllerAT}
 */
xyz.swapee.wc.front.TransactionInfoHeadControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITransactionInfoHeadControllerAT} */
xyz.swapee.wc.front.RecordITransactionInfoHeadControllerAT

/** @typedef {xyz.swapee.wc.front.ITransactionInfoHeadControllerAT} xyz.swapee.wc.front.BoundITransactionInfoHeadControllerAT */

/** @typedef {xyz.swapee.wc.front.TransactionInfoHeadControllerAT} xyz.swapee.wc.front.BoundTransactionInfoHeadControllerAT */

/**
 * Contains getters to cast the _ITransactionInfoHeadControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ITransactionInfoHeadControllerATCaster
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerATCaster = class { }
/**
 * Cast the _ITransactionInfoHeadControllerAT_ instance into the _BoundITransactionInfoHeadControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundITransactionInfoHeadControllerAT}
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerATCaster.prototype.asITransactionInfoHeadControllerAT
/**
 * Access the _TransactionInfoHeadControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTransactionInfoHeadControllerAT}
 */
xyz.swapee.wc.front.ITransactionInfoHeadControllerATCaster.prototype.superTransactionInfoHeadControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/70-ITransactionInfoHeadScreen.xml}  c4cb6944ed0d09c99a11d38438cf38fa */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.front.TransactionInfoHeadInputs, !HTMLDivElement, !xyz.swapee.wc.ITransactionInfoHeadDisplay.Settings, !xyz.swapee.wc.ITransactionInfoHeadDisplay.Queries, null>&xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese} xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadScreen)} xyz.swapee.wc.AbstractTransactionInfoHeadScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadScreen} xyz.swapee.wc.TransactionInfoHeadScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadScreen` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadScreen
 */
xyz.swapee.wc.AbstractTransactionInfoHeadScreen = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadScreen.constructor&xyz.swapee.wc.TransactionInfoHeadScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadScreen.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadScreen.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.TransactionInfoHeadScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITransactionInfoHeadController|typeof xyz.swapee.wc.front.TransactionInfoHeadController)|(!xyz.swapee.wc.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.TransactionInfoHeadDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.TransactionInfoHeadScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITransactionInfoHeadController|typeof xyz.swapee.wc.front.TransactionInfoHeadController)|(!xyz.swapee.wc.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.TransactionInfoHeadDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.TransactionInfoHeadScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITransactionInfoHeadController|typeof xyz.swapee.wc.front.TransactionInfoHeadController)|(!xyz.swapee.wc.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.TransactionInfoHeadDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadScreen} xyz.swapee.wc.TransactionInfoHeadScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.TransactionInfoHeadMemory, !xyz.swapee.wc.front.TransactionInfoHeadInputs, !HTMLDivElement, !xyz.swapee.wc.ITransactionInfoHeadDisplay.Settings, !xyz.swapee.wc.ITransactionInfoHeadDisplay.Queries, null, null>&xyz.swapee.wc.front.ITransactionInfoHeadController&xyz.swapee.wc.ITransactionInfoHeadDisplay)} xyz.swapee.wc.ITransactionInfoHeadScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ITransactionInfoHeadScreen
 */
xyz.swapee.wc.ITransactionInfoHeadScreen = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ITransactionInfoHeadController.typeof&xyz.swapee.wc.ITransactionInfoHeadDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese>)} xyz.swapee.wc.TransactionInfoHeadScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHeadScreen} xyz.swapee.wc.ITransactionInfoHeadScreen.typeof */
/**
 * A concrete class of _ITransactionInfoHeadScreen_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadScreen
 * @implements {xyz.swapee.wc.ITransactionInfoHeadScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadScreen = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadScreen.constructor&xyz.swapee.wc.ITransactionInfoHeadScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.TransactionInfoHeadScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadScreen} */
xyz.swapee.wc.RecordITransactionInfoHeadScreen

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadScreen} xyz.swapee.wc.BoundITransactionInfoHeadScreen */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadScreen} xyz.swapee.wc.BoundTransactionInfoHeadScreen */

/**
 * Contains getters to cast the _ITransactionInfoHeadScreen_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadScreenCaster
 */
xyz.swapee.wc.ITransactionInfoHeadScreenCaster = class { }
/**
 * Cast the _ITransactionInfoHeadScreen_ instance into the _BoundITransactionInfoHeadScreen_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadScreen}
 */
xyz.swapee.wc.ITransactionInfoHeadScreenCaster.prototype.asITransactionInfoHeadScreen
/**
 * Access the _TransactionInfoHeadScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadScreen}
 */
xyz.swapee.wc.ITransactionInfoHeadScreenCaster.prototype.superTransactionInfoHeadScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/70-ITransactionInfoHeadScreenBack.xml}  34c64e172e878b44fb3a0884f76f7a6a */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese} xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoHeadScreen)} xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoHeadScreen} xyz.swapee.wc.back.TransactionInfoHeadScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoHeadScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.constructor&xyz.swapee.wc.back.TransactionInfoHeadScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.back.TransactionInfoHeadScreen)|(!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT|typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.back.TransactionInfoHeadScreen)|(!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT|typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.back.TransactionInfoHeadScreen)|(!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT|typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoHeadScreen} xyz.swapee.wc.back.TransactionInfoHeadScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoHeadScreenCaster&xyz.swapee.wc.back.ITransactionInfoHeadScreenAT)} xyz.swapee.wc.back.ITransactionInfoHeadScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoHeadScreenAT} xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadScreen
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreen = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoHeadScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoHeadScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese>)} xyz.swapee.wc.back.TransactionInfoHeadScreen.constructor */
/**
 * A concrete class of _ITransactionInfoHeadScreen_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoHeadScreen
 * @implements {xyz.swapee.wc.back.ITransactionInfoHeadScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoHeadScreen = class extends /** @type {xyz.swapee.wc.back.TransactionInfoHeadScreen.constructor&xyz.swapee.wc.back.ITransactionInfoHeadScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoHeadScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreen}
 */
xyz.swapee.wc.back.TransactionInfoHeadScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadScreen} */
xyz.swapee.wc.back.RecordITransactionInfoHeadScreen

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadScreen} xyz.swapee.wc.back.BoundITransactionInfoHeadScreen */

/** @typedef {xyz.swapee.wc.back.TransactionInfoHeadScreen} xyz.swapee.wc.back.BoundTransactionInfoHeadScreen */

/**
 * Contains getters to cast the _ITransactionInfoHeadScreen_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadScreenCaster
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenCaster = class { }
/**
 * Cast the _ITransactionInfoHeadScreen_ instance into the _BoundITransactionInfoHeadScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoHeadScreen}
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenCaster.prototype.asITransactionInfoHeadScreen
/**
 * Access the _TransactionInfoHeadScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoHeadScreen}
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenCaster.prototype.superTransactionInfoHeadScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/73-ITransactionInfoHeadScreenAR.xml}  c0321ce4c897f114f302af150c598c02 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITransactionInfoHeadScreen.Initialese} xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TransactionInfoHeadScreenAR)} xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR} xyz.swapee.wc.front.TransactionInfoHeadScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITransactionInfoHeadScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.constructor&xyz.swapee.wc.front.TransactionInfoHeadScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR|typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.TransactionInfoHeadScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR|typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.TransactionInfoHeadScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR|typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoHeadScreen|typeof xyz.swapee.wc.TransactionInfoHeadScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.Initialese[]) => xyz.swapee.wc.front.ITransactionInfoHeadScreenAR} xyz.swapee.wc.front.TransactionInfoHeadScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITransactionInfoHeadScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITransactionInfoHeadScreen)} xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITransactionInfoHeadScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ITransactionInfoHeadScreenAR
 */
xyz.swapee.wc.front.ITransactionInfoHeadScreenAR = class extends /** @type {xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITransactionInfoHeadScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITransactionInfoHeadScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.Initialese>)} xyz.swapee.wc.front.TransactionInfoHeadScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITransactionInfoHeadScreenAR} xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.typeof */
/**
 * A concrete class of _ITransactionInfoHeadScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.TransactionInfoHeadScreenAR
 * @implements {xyz.swapee.wc.front.ITransactionInfoHeadScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ITransactionInfoHeadScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.TransactionInfoHeadScreenAR = class extends /** @type {xyz.swapee.wc.front.TransactionInfoHeadScreenAR.constructor&xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TransactionInfoHeadScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TransactionInfoHeadScreenAR}
 */
xyz.swapee.wc.front.TransactionInfoHeadScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITransactionInfoHeadScreenAR} */
xyz.swapee.wc.front.RecordITransactionInfoHeadScreenAR

/** @typedef {xyz.swapee.wc.front.ITransactionInfoHeadScreenAR} xyz.swapee.wc.front.BoundITransactionInfoHeadScreenAR */

/** @typedef {xyz.swapee.wc.front.TransactionInfoHeadScreenAR} xyz.swapee.wc.front.BoundTransactionInfoHeadScreenAR */

/**
 * Contains getters to cast the _ITransactionInfoHeadScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ITransactionInfoHeadScreenARCaster
 */
xyz.swapee.wc.front.ITransactionInfoHeadScreenARCaster = class { }
/**
 * Cast the _ITransactionInfoHeadScreenAR_ instance into the _BoundITransactionInfoHeadScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundITransactionInfoHeadScreenAR}
 */
xyz.swapee.wc.front.ITransactionInfoHeadScreenARCaster.prototype.asITransactionInfoHeadScreenAR
/**
 * Access the _TransactionInfoHeadScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTransactionInfoHeadScreenAR}
 */
xyz.swapee.wc.front.ITransactionInfoHeadScreenARCaster.prototype.superTransactionInfoHeadScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/74-ITransactionInfoHeadScreenAT.xml}  860279f2595e906f622c86a2ab673d40 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoHeadScreenAT)} xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT} xyz.swapee.wc.back.TransactionInfoHeadScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoHeadScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.constructor&xyz.swapee.wc.back.TransactionInfoHeadScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT|typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT|typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT|typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoHeadScreenAT} xyz.swapee.wc.back.TransactionInfoHeadScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoHeadScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITransactionInfoHeadScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadScreenAT
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenAT = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoHeadScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese>)} xyz.swapee.wc.back.TransactionInfoHeadScreenAT.constructor */
/**
 * A concrete class of _ITransactionInfoHeadScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoHeadScreenAT
 * @implements {xyz.swapee.wc.back.ITransactionInfoHeadScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITransactionInfoHeadScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoHeadScreenAT = class extends /** @type {xyz.swapee.wc.back.TransactionInfoHeadScreenAT.constructor&xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoHeadScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoHeadScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoHeadScreenAT}
 */
xyz.swapee.wc.back.TransactionInfoHeadScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadScreenAT} */
xyz.swapee.wc.back.RecordITransactionInfoHeadScreenAT

/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadScreenAT} xyz.swapee.wc.back.BoundITransactionInfoHeadScreenAT */

/** @typedef {xyz.swapee.wc.back.TransactionInfoHeadScreenAT} xyz.swapee.wc.back.BoundTransactionInfoHeadScreenAT */

/**
 * Contains getters to cast the _ITransactionInfoHeadScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoHeadScreenATCaster
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenATCaster = class { }
/**
 * Cast the _ITransactionInfoHeadScreenAT_ instance into the _BoundITransactionInfoHeadScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoHeadScreenAT}
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenATCaster.prototype.asITransactionInfoHeadScreenAT
/**
 * Access the _TransactionInfoHeadScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoHeadScreenAT}
 */
xyz.swapee.wc.back.ITransactionInfoHeadScreenATCaster.prototype.superTransactionInfoHeadScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/design/80-ITransactionInfoHeadGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoHeadDisplay.Initialese} xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHeadGPU)} xyz.swapee.wc.AbstractTransactionInfoHeadGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHeadGPU} xyz.swapee.wc.TransactionInfoHeadGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadGPU` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHeadGPU
 */
xyz.swapee.wc.AbstractTransactionInfoHeadGPU = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHeadGPU.constructor&xyz.swapee.wc.TransactionInfoHeadGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHeadGPU.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHeadGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHeadGPU.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadGPU|typeof xyz.swapee.wc.TransactionInfoHeadGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHeadGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHeadGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadGPU|typeof xyz.swapee.wc.TransactionInfoHeadGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHeadGPU|typeof xyz.swapee.wc.TransactionInfoHeadGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITransactionInfoHeadDisplay|typeof xyz.swapee.wc.back.TransactionInfoHeadDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoHeadGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese[]) => xyz.swapee.wc.ITransactionInfoHeadGPU} xyz.swapee.wc.TransactionInfoHeadGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHeadGPUCaster&com.webcircuits.IBrowserView<.!TransactionInfoHeadMemory,.!TransactionInfoHeadLand>&xyz.swapee.wc.back.ITransactionInfoHeadDisplay)} xyz.swapee.wc.ITransactionInfoHeadGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!TransactionInfoHeadMemory,.!TransactionInfoHeadLand>} com.webcircuits.IBrowserView<.!TransactionInfoHeadMemory,.!TransactionInfoHeadLand>.typeof */
/**
 * Handles the periphery of the _ITransactionInfoHeadDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ITransactionInfoHeadGPU
 */
xyz.swapee.wc.ITransactionInfoHeadGPU = class extends /** @type {xyz.swapee.wc.ITransactionInfoHeadGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!TransactionInfoHeadMemory,.!TransactionInfoHeadLand>.typeof&xyz.swapee.wc.back.ITransactionInfoHeadDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHeadGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHeadGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHeadGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese>)} xyz.swapee.wc.TransactionInfoHeadGPU.constructor */
/**
 * A concrete class of _ITransactionInfoHeadGPU_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHeadGPU
 * @implements {xyz.swapee.wc.ITransactionInfoHeadGPU} Handles the periphery of the _ITransactionInfoHeadDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHeadGPU = class extends /** @type {xyz.swapee.wc.TransactionInfoHeadGPU.constructor&xyz.swapee.wc.ITransactionInfoHeadGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHeadGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHeadGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHeadGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHeadGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHeadGPU}
 */
xyz.swapee.wc.TransactionInfoHeadGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoHeadGPU.
 * @interface xyz.swapee.wc.ITransactionInfoHeadGPUFields
 */
xyz.swapee.wc.ITransactionInfoHeadGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ITransactionInfoHeadGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadGPU} */
xyz.swapee.wc.RecordITransactionInfoHeadGPU

/** @typedef {xyz.swapee.wc.ITransactionInfoHeadGPU} xyz.swapee.wc.BoundITransactionInfoHeadGPU */

/** @typedef {xyz.swapee.wc.TransactionInfoHeadGPU} xyz.swapee.wc.BoundTransactionInfoHeadGPU */

/**
 * Contains getters to cast the _ITransactionInfoHeadGPU_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHeadGPUCaster
 */
xyz.swapee.wc.ITransactionInfoHeadGPUCaster = class { }
/**
 * Cast the _ITransactionInfoHeadGPU_ instance into the _BoundITransactionInfoHeadGPU_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHeadGPU}
 */
xyz.swapee.wc.ITransactionInfoHeadGPUCaster.prototype.asITransactionInfoHeadGPU
/**
 * Access the _TransactionInfoHeadGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHeadGPU}
 */
xyz.swapee.wc.ITransactionInfoHeadGPUCaster.prototype.superTransactionInfoHeadGPU

// nss:xyz.swapee.wc
/* @typal-end */