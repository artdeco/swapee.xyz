import TransactionInfoHeadHtmlController from '../TransactionInfoHeadHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractTransactionInfoHeadHtmlComponent} from '../../gen/AbstractTransactionInfoHeadHtmlComponent'

/** @extends {xyz.swapee.wc.TransactionInfoHeadHtmlComponent} */
export default class extends AbstractTransactionInfoHeadHtmlComponent.implements(
 TransactionInfoHeadHtmlController,
 IntegratedComponentInitialiser,
){}