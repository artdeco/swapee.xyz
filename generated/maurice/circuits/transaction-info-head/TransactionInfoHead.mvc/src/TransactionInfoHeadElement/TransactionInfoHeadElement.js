import buildTransactionInfo from './methods/build-transaction-info'
import render from './methods/render'
import TransactionInfoHeadServerController from '../TransactionInfoHeadServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractTransactionInfoHeadElement from '../../gen/AbstractTransactionInfoHeadElement'

/** @extends {xyz.swapee.wc.TransactionInfoHeadElement} */
export default class TransactionInfoHeadElement extends AbstractTransactionInfoHeadElement.implements(
 TransactionInfoHeadServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ITransactionInfoHeadElement} */ ({
  buildTransactionInfo:buildTransactionInfo,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
   classesMap: true,
   rootSelector:     `.TransactionInfoHead`,
   stylesheet:       'html/styles/TransactionInfoHead.css',
   blockName:        'html/TransactionInfoHeadBlock.html',
   // server(){
   //  return (<div $id="TransactionInfoHead">
   //   <div $id="TransactionLoaderWr" $reveal={true}>
   //    <span $id="TransactionLoInWr" $reveal={true} />
   //   </div>
   //  </div>)
   // },
  }),
){}

// thank you for using web circuits
