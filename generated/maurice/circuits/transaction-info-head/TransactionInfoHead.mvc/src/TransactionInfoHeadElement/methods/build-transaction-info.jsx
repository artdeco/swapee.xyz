/** @type {xyz.swapee.wc.ITransactionInfoHeadElement._buildTransactionInfo} */
export default function buildTransactionInfo({
 currencyFrom:currencyFrom,currencyTo:currencyTo,amountFrom:amountFrom,
 amountTo:amountTo,rate:rate,fixed:fixed,iconsFolder:iconsFolder,
 gettingTransaction:gettingTransaction, // how to make this true
 getTransactionError:getTransactionError,
 networkFee:networkFee,partnerFee:partnerFee,
 notFound:notFound,visibleAmount:visibleAmount,
},{pulseGetTransaction:pulseGetTransaction}) {
 return (<div $id="TransactionInfoHead">
  <div $id="TransactionLoaderWr" $reveal={getTransactionError||gettingTransaction||notFound}>
   <span $id="TransactionLoInWr" $reveal={gettingTransaction} />
   {/* <span $id="TransactionLoIn" $reveal={gettingTransaction} /> */}
   <span $id="GetTransactionErrorWr" $reveal={getTransactionError} onClick={pulseGetTransaction}>
    <span $id="GetTransactionErrorLa">
     {getTransactionError}
    </span>
   </span>
   <div $id="NotFoundWr" $reveal={notFound} />
  </div>

  {/* <button $id="ReloadGetOfferBu" onClick={pulseGetOffer} /> */}

  <div $id="TransactionFoundWr" $reveal={amountFrom}>
   <span $id="CryptoIn" $show={currencyFrom}>{currencyFrom}</span>
   <span $id="CryptoOut" $show={currencyTo}>{currencyTo}</span>
   <span $id="RateLa">{rate}</span>
   <span $id="AmountIn" $show={amountFrom}>{amountFrom||0}</span>
   <span $id="AmountOut" $show={amountTo}>{amountTo||0}</span>

   <img $id="InIm" src={currencyFrom&&(iconsFolder+'/'+currencyFrom+'.png')} />
   <img $id="OutIm" src={currencyTo&&(iconsFolder+'/'+currencyTo+'.png')} />

   <span $id="FixedLock" $reveal={fixed} />

   <span $id="NetworkFeeWr" $reveal={currencyFrom}>
    <span $id="NetworkFeeLa">{networkFee||'?'}</span>
   </span>
   <span $id="PartnerFeeWr" $reveal={partnerFee}>
    <span $id="PartnerFeeLa">{partnerFee}</span>
   </span>
   <span $id="VisibleAmountWr" $reveal={visibleAmount}>
    <span $id="VisibleAmounLa">{visibleAmount}</span>
   </span>

   <button $id="BreakdownBu" $reveal={networkFee} />
  </div>
 </div>)
}