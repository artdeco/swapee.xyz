import deduceInputs from './methods/deduce-inputs'
import AbstractTransactionInfoHeadControllerAT from '../../gen/AbstractTransactionInfoHeadControllerAT'
import TransactionInfoHeadDisplay from '../TransactionInfoHeadDisplay'
import AbstractTransactionInfoHeadScreen from '../../gen/AbstractTransactionInfoHeadScreen'

/** @extends {xyz.swapee.wc.TransactionInfoHeadScreen} */
export default class extends AbstractTransactionInfoHeadScreen.implements(
 AbstractTransactionInfoHeadControllerAT,
 TransactionInfoHeadDisplay,
 /**@type {!xyz.swapee.wc.ITransactionInfoHeadScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ITransactionInfoHeadScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:4300544218,
 }),
){}