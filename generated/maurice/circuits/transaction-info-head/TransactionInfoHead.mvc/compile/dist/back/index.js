/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHead` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHead}
 */
class AbstractTransactionInfoHead extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITransactionInfoHead_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TransactionInfoHeadPort}
 */
class TransactionInfoHeadPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadController` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadController}
 */
class AbstractTransactionInfoHeadController extends (class {/* lazy-loaded */}) {}
/**
 * The _ITransactionInfoHead_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.TransactionInfoHeadHtmlComponent}
 */
class TransactionInfoHeadHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TransactionInfoHeadBuffer}
 */
class TransactionInfoHeadBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadComputer}
 */
class AbstractTransactionInfoHeadComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.TransactionInfoHeadController}
 */
class TransactionInfoHeadController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractTransactionInfoHead = AbstractTransactionInfoHead
module.exports.TransactionInfoHeadPort = TransactionInfoHeadPort
module.exports.AbstractTransactionInfoHeadController = AbstractTransactionInfoHeadController
module.exports.TransactionInfoHeadHtmlComponent = TransactionInfoHeadHtmlComponent
module.exports.TransactionInfoHeadBuffer = TransactionInfoHeadBuffer
module.exports.AbstractTransactionInfoHeadComputer = AbstractTransactionInfoHeadComputer
module.exports.TransactionInfoHeadController = TransactionInfoHeadController