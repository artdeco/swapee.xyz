import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {4300544218} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const l=k["37270038985"],m=k["372700389810"],n=k["372700389811"];function L(b,c,d,e){return k["372700389812"](b,c,d,e,!1,void 0)};
const M=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const N=M["61893096584"],O=M["61893096586"],aa=M["618930965811"],ba=M["618930965812"],ca=M["618930965815"];function P(){}P.prototype={};function da(){this.A=this.C=this.o=this.v=this.D=this.F=this.s=this.u=this.K=this.I=this.L=null;this.P=[];this.R=[];this.T=this.j=this.l=this.m=this.H=this.J=this.i=this.M=this.O=this.h=this.g=this.G=null}class ea{}class Q extends L(ea,430054421817,da,{X:1,aa:2}){}
Q[n]=[P,N,{constructor(){l(this,()=>{const {queries:{da:b}}=this;this.scan({U:b})})},scan:function({U:b}){const {element:c,V:{vdusPQs:{L:d,I:e,K:p,u:q,s:r,F:t,D:u,v,o:w,C:x,A:y,G:z,g:A,h:B,O:C,M:D,i:E,J:F,H:G,m:H,l:I,j:J}},queries:{U:h}}=this,a=ca(c);let K;b?K=c.closest(b):K=document;b=h?K.querySelector(h):void 0;Object.assign(this,{L:a[d],I:a[e],K:a[p],u:a[q],s:a[r],F:a[t],D:a[u],v:a[v],o:a[w],C:a[x],A:a[y],P:c?[...c.querySelectorAll("[data-id~=d66e11]")]:[],R:c?[...c.querySelectorAll("[data-id~=d66e12]")]:
[],G:a[z],g:a[A],h:a[B],O:a[C],M:a[D],i:a[E],J:a[F],H:a[G],m:a[H],l:a[I],j:a[J],T:b})}},{[m]:{L:1,I:1,K:1,u:1,s:1,F:1,D:1,v:1,o:1,C:1,A:1,P:1,R:1,G:1,g:1,h:1,O:1,M:1,i:1,J:1,H:1,m:1,l:1,j:1,T:1},initializer({L:b,I:c,K:d,u:e,s:p,F:q,D:r,v:t,o:u,C:v,A:w,P:x,R:y,G:z,g:A,h:B,O:C,M:D,i:E,J:F,H:G,m:H,l:I,j:J,T:h}){void 0!==b&&(this.L=b);void 0!==c&&(this.I=c);void 0!==d&&(this.K=d);void 0!==e&&(this.u=e);void 0!==p&&(this.s=p);void 0!==q&&(this.F=q);void 0!==r&&(this.D=r);void 0!==t&&(this.v=t);void 0!==
u&&(this.o=u);void 0!==v&&(this.C=v);void 0!==w&&(this.A=w);void 0!==x&&(this.P=x);void 0!==y&&(this.R=y);void 0!==z&&(this.G=z);void 0!==A&&(this.g=A);void 0!==B&&(this.h=B);void 0!==C&&(this.O=C);void 0!==D&&(this.M=D);void 0!==E&&(this.i=E);void 0!==F&&(this.J=F);void 0!==G&&(this.H=G);void 0!==H&&(this.m=H);void 0!==I&&(this.l=I);void 0!==J&&(this.j=J);void 0!==h&&(this.T=h)}}];var R=class extends Q.implements(){};function fa(){};function S(){}S.prototype={};class ha{}class T extends L(ha,430054421826,null,{W:1,$:2}){}T[n]=[S,aa,{}];function U(){}U.prototype={};class ia{}class V extends L(ia,430054421829,null,{Y:1,ca:2}){}V[n]=[U,ba,{allocator(){this.methods={}}}];const W={Z:"a74ad"};const ja={...W};const ka=Object.keys(W).reduce((b,c)=>{b[W[c]]=c;return b},{});function X(){}X.prototype={};class la{}class Y extends L(la,430054421827,null,{V:1,ba:2}){}function Z(){}Y[n]=[X,Z.prototype={inputsPQs:ja,queriesPQs:{U:"63ad3"},memoryQPs:ka},O,V,Z.prototype={vdusPQs:{L:"d66e1",I:"d66e2",K:"d66e3",u:"d66e4",s:"d66e5",F:"d66e6",D:"d66e7",o:"d66e8",C:"d66e9",A:"d66e10",P:"d66e11",R:"d66e12",G:"d66e13",g:"d66e14",h:"d66e15",O:"d66e16",M:"d66e17",i:"d66e18",J:"d66e19",H:"d66e20",m:"d66e21",l:"d66e22",j:"d66e23",T:"d66e24",v:"d66e25"}}];var ma=class extends Y.implements(T,R,{get queries(){return this.settings}},{deduceInputs:fa,__$id:4300544218}){};module.exports["430054421841"]=R;module.exports["430054421871"]=ma;
/*! @embed-object-end {4300544218} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule