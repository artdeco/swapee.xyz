import Module from './browser'

/** @type {typeof xyz.swapee.wc.TransactionInfoHeadDisplay} */
export const TransactionInfoHeadDisplay=Module['430054421841']
/** @type {typeof xyz.swapee.wc.TransactionInfoHeadScreen} */
export const TransactionInfoHeadScreen=Module['430054421871']