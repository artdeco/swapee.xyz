/**
 * Display for presenting information from the _ITransactionInfoHead_.
 * @extends {xyz.swapee.wc.TransactionInfoHeadDisplay}
 */
class TransactionInfoHeadDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.TransactionInfoHeadScreen}
 */
class TransactionInfoHeadScreen extends (class {/* lazy-loaded */}) {}

module.exports.TransactionInfoHeadDisplay = TransactionInfoHeadDisplay
module.exports.TransactionInfoHeadScreen = TransactionInfoHeadScreen