/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const a=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=a["372700389811"];function e(b,d,g,h){return a["372700389812"](b,d,g,h,!1,void 0)};function f(){}f.prototype={};class aa{}class k extends e(aa,43005442188,null,{X:1,xa:2}){}k[c]=[f];

const n=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ba=n["615055805212"],ca=n["615055805218"],p=n["615055805233"],da=n["615055805235"];const q={core:"a74ad"};function r(){}r.prototype={};class ea{}class z extends e(ea,43005442187,null,{T:1,sa:2}){}function A(){}A.prototype={};function D(){this.model={core:""}}class fa{}class F extends e(fa,43005442183,D,{V:1,va:2}){}F[c]=[A,{constructor(){p(this.model,"",q)}}];z[c]=[{},r,F];
const G=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ha=G.IntegratedComponentInitialiser,H=G.IntegratedComponent,ia=G["95173443851"],ja=G["95173443852"];function I(){}I.prototype={};class ka{}class J extends e(ka,43005442181,null,{P:1,qa:2}){}J[c]=[I,ca];const K={regulate:ba({core:String})};
const L=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=L.IntegratedController,ma=L.Parametric;const M={...q};function N(){}N.prototype={};function na(){const b={model:null};D.call(b);this.inputs=b.model}class oa{}class O extends e(oa,43005442185,na,{W:1,wa:2}){}function P(){}O[c]=[P.prototype={},N,ma,P.prototype={constructor(){p(this.inputs,"",M)}}];function Q(){}Q.prototype={};class pa{}class R extends e(pa,430054421822,null,{R:1,ra:2}){}R[c]=[{},Q,K,la,{get Port(){return O}}];function S(){}S.prototype={};class qa{}class T extends e(qa,43005442189,null,{M:1,pa:2}){}T[c]=[S,z,k,H,J,R];const U=require(eval('"@type.engineering/web-computing"')).h;
function ra({s:b,u:d,j:g,l:h,rate:l,fixed:B,D:t,A:u,v:m,F:v,H:w,G:x,I:y},{ma:C}){return U("div",{$id:"TransactionInfoHead"},U("div",{$id:"TransactionLoaderWr",$reveal:m||u||x},U("span",{$id:"TransactionLoInWr",$reveal:u}),U("span",{$id:"GetTransactionErrorWr",$reveal:m,onClick:C},U("span",{$id:"GetTransactionErrorLa"},m)),U("div",{$id:"NotFoundWr",$reveal:x})),U("div",{$id:"TransactionFoundWr",$reveal:g},U("span",{$id:"CryptoIn",$show:b},b),U("span",{$id:"CryptoOut",$show:d},d),U("span",{$id:"RateLa"},
l),U("span",{$id:"AmountIn",$show:g},g||0),U("span",{$id:"AmountOut",$show:h},h||0),U("img",{$id:"InIm",src:b&&t+"/"+b+".png"}),U("img",{$id:"OutIm",src:d&&t+"/"+d+".png"}),U("span",{$id:"FixedLock",$reveal:B}),U("span",{$id:"NetworkFeeWr",$reveal:b},U("span",{$id:"NetworkFeeLa"},v||"?")),U("span",{$id:"PartnerFeeWr",$reveal:w},U("span",{$id:"PartnerFeeLa"},w)),U("span",{$id:"VisibleAmountWr",$reveal:y},U("span",{$id:"VisibleAmounLa"},y)),U("button",{$id:"BreakdownBu",$reveal:v})))};const sa=require(eval('"@type.engineering/web-computing"')).h;function ta(){return sa("div",{$id:"TransactionInfoHead"})};var V=class extends R.implements(){};require("https");require("http");const ua=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(b){}ua("aqt");require("fs");require("child_process");
const W=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const va=W.ElementBase,wa=W.HTMLBlocker;const X=require(eval('"@type.engineering/web-computing"')).h;function xa(){}xa.prototype={};function ya(){this.inputs={noSolder:!1,Ca:{},ya:{},Ba:{},ga:{},fa:{},la:{},ka:{},ha:{},ea:{},ja:{},ia:{},Z:{},$:{},na:{},J:{},K:{},Ea:{},Da:{},Y:{},Aa:{},oa:{},da:{},ba:{},aa:{},za:{}}}class za{}class Aa extends e(za,430054421814,ya,{U:1,ua:2}){}
Aa[c]=[xa,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"transaction-loader-wr-opts":void 0,"transaction-found-wr-opts":void 0,"transaction-lo-in-wr-opts":void 0,"network-fee-wr-opts":void 0,"network-fee-la-opts":void 0,"partner-fee-wr-opts":void 0,"partner-fee-la-opts":void 0,"not-found-la-opts":void 0,"in-im-opts":void 0,"out-im-opts":void 0,"not-found-wr-opts":void 0,"crypto-in-opts":void 0,"crypto-out-opts":void 0,"rate-la-opts":void 0,"amount-in-opts":void 0,
"amount-out-opts":void 0,"visible-amount-wr-opts":void 0,"visible-amoun-la-opts":void 0,"breakdown-bu-opts":void 0,"transaction-lo-in-opts":void 0,"reload-get-transaction-bu-opts":void 0,"get-transaction-error-wr-opts":void 0,"get-transaction-error-la-opts":void 0,"fixed-lock-opts":void 0,"transaction-info-opts":void 0})}}];function Ba(){}Ba.prototype={};class Ca{}class Y extends e(Ca,430054421813,null,{m:1,ta:2}){}function Z(){}
Y[c]=[Ba,va,da,Z.prototype={constructor(){Object.assign(this,{land:{i:null}})},render:function(){const {asILanded:{land:{i:b}},m:{o:d}}=this;if(b){var {"96c88":g,c23cd:h,"748e6":l,"9dc9f":B,67942:t,cec31:u,ffba8:m,"7c389":v,"43e5c":w,"5fd9c":x,"96f44":y,"5ccf4":C,"4685c":Da}=b.model,E=d({s:g,u:h,j:l,l:B,rate:t,fixed:u,D:m,A:v,v:w,F:x,H:y,G:C,I:Da},{});E.attributes.$id=this.rootId;E.nodeName="div";return E}}},Z.prototype={render:function(){return X("div",{$id:"TransactionInfoHead"},X("vdu",{$id:"TransactionLoaderWr"}),
X("vdu",{$id:"TransactionFoundWr"}),X("vdu",{$id:"TransactionLoInWr"}),X("vdu",{$id:"NetworkFeeWr"}),X("vdu",{$id:"NetworkFeeLa"}),X("vdu",{$id:"PartnerFeeWr"}),X("vdu",{$id:"PartnerFeeLa"}),X("vdu",{$id:"NotFoundLa"}),X("vdu",{$id:"InIm"}),X("vdu",{$id:"OutIm"}),X("vdu",{$id:"NotFoundWr"}),X("vdu",{$id:"CryptoIn"}),X("vdu",{$id:"CryptoOut"}),X("vdu",{$id:"RateLa"}),X("vdu",{$id:"AmountIn"}),X("vdu",{$id:"AmountOut"}),X("vdu",{$id:"VisibleAmountWr"}),X("vdu",{$id:"VisibleAmounLa"}),X("vdu",{$id:"BreakdownBu"}),
X("vdu",{$id:"TransactionLoIn"}),X("vdu",{$id:"ReloadGetTransactionBu"}),X("vdu",{$id:"GetTransactionErrorWr"}),X("vdu",{$id:"GetTransactionErrorLa"}),X("vdu",{$id:"FixedLock"}))}},Z.prototype={classes:{Class:"9bd81"},inputsPQs:M,queriesPQs:{g:"63ad3"},vdus:{TransactionLoaderWr:"d66e1",TransactionFoundWr:"d66e2",TransactionLoInWr:"d66e3",NetworkFeeWr:"d66e4",NetworkFeeLa:"d66e5",PartnerFeeWr:"d66e6",PartnerFeeLa:"d66e7",InIm:"d66e8",OutIm:"d66e9",NotFoundWr:"d66e10",RateLa:"d66e13",AmountIn:"d66e14",
AmountOut:"d66e15",VisibleAmountWr:"d66e16",VisibleAmounLa:"d66e17",BreakdownBu:"d66e18",TransactionLoIn:"d66e19",ReloadGetTransactionBu:"d66e20",GetTransactionErrorWr:"d66e21",GetTransactionErrorLa:"d66e22",FixedLock:"d66e23",TransactionInfo:"d66e24",NotFoundLa:"d66e25",CryptoIn:"d66e11",CryptoOut:"d66e12"}},H,ja,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder core query:transaction-info no-solder :no-solder :core fe646 fb1e2 aa6df 8edfe 6c941 72e15 97166 ed9b2 678aa 5cba9 4991f f6dea 3ce39 2de4e e0e01 b88f2 cd1af 20882 0fc90 54094 9726d 13d7a 00869 22b42 666c3 6b245 a74ad children".split(" "))})},
get Port(){return Aa},calibrate:function({"query:transaction-info":b}){const d={};b&&(d.g=b);return d}},Z.prototype={constructor(){this.land={i:null}}},Z.prototype={calibrate:async function({g:b}){if(!b)return{};const {asIMilleu:{milleu:d},asILanded:{land:g},asIElement:{fqn:h}}=this,l=await d(b,!0);if(!l)return console.warn("\u2757\ufe0f transactionInfoSel %s must be present on the page for %s to work",b,h),{};g.i=l}},Z.prototype={solder:(b,{g:d})=>({g:d})}];
Y[c]=[T,{rootId:"TransactionInfoHead",__$id:4300544218,fqn:"xyz.swapee.wc.ITransactionInfoHead",maurice_element_v3:!0}];class Ea extends Y.implements(V,ia,wa,ha,{o:ra,render:ta},{classesMap:!0,rootSelector:".TransactionInfoHead",stylesheet:"html/styles/TransactionInfoHead.css",blockName:"html/TransactionInfoHeadBlock.html"}){};module.exports["43005442180"]=T;module.exports["43005442181"]=T;module.exports["43005442183"]=O;module.exports["43005442184"]=R;module.exports["43005442188"]=Ea;module.exports["430054421811"]=K;module.exports["430054421830"]=J;module.exports["430054421861"]=V;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['4300544218']=module.exports