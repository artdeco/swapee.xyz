import Module from './element'

/**@extends {xyz.swapee.wc.AbstractTransactionInfoHead}*/
export class AbstractTransactionInfoHead extends Module['43005442181'] {}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHead} */
AbstractTransactionInfoHead.class=function(){}
/** @type {typeof xyz.swapee.wc.TransactionInfoHeadPort} */
export const TransactionInfoHeadPort=Module['43005442183']
/**@extends {xyz.swapee.wc.AbstractTransactionInfoHeadController}*/
export class AbstractTransactionInfoHeadController extends Module['43005442184'] {}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadController} */
AbstractTransactionInfoHeadController.class=function(){}
/** @type {typeof xyz.swapee.wc.TransactionInfoHeadElement} */
export const TransactionInfoHeadElement=Module['43005442188']
/** @type {typeof xyz.swapee.wc.TransactionInfoHeadBuffer} */
export const TransactionInfoHeadBuffer=Module['430054421811']
/**@extends {xyz.swapee.wc.AbstractTransactionInfoHeadComputer}*/
export class AbstractTransactionInfoHeadComputer extends Module['430054421830'] {}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadComputer} */
AbstractTransactionInfoHeadComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.TransactionInfoHeadController} */
export const TransactionInfoHeadController=Module['430054421861']