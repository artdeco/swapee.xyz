/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHead` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHead}
 */
class AbstractTransactionInfoHead extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITransactionInfoHead_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TransactionInfoHeadPort}
 */
class TransactionInfoHeadPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadController` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadController}
 */
class AbstractTransactionInfoHeadController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ITransactionInfoHead_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.TransactionInfoHeadElement}
 */
class TransactionInfoHeadElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TransactionInfoHeadBuffer}
 */
class TransactionInfoHeadBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHeadComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadComputer}
 */
class AbstractTransactionInfoHeadComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.TransactionInfoHeadController}
 */
class TransactionInfoHeadController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractTransactionInfoHead = AbstractTransactionInfoHead
module.exports.TransactionInfoHeadPort = TransactionInfoHeadPort
module.exports.AbstractTransactionInfoHeadController = AbstractTransactionInfoHeadController
module.exports.TransactionInfoHeadElement = TransactionInfoHeadElement
module.exports.TransactionInfoHeadBuffer = TransactionInfoHeadBuffer
module.exports.AbstractTransactionInfoHeadComputer = AbstractTransactionInfoHeadComputer
module.exports.TransactionInfoHeadController = TransactionInfoHeadController

Object.defineProperties(module.exports, {
 'AbstractTransactionInfoHead': {get: () => require('./precompile/internal')[43005442181]},
 [43005442181]: {get: () => module.exports['AbstractTransactionInfoHead']},
 'TransactionInfoHeadPort': {get: () => require('./precompile/internal')[43005442183]},
 [43005442183]: {get: () => module.exports['TransactionInfoHeadPort']},
 'AbstractTransactionInfoHeadController': {get: () => require('./precompile/internal')[43005442184]},
 [43005442184]: {get: () => module.exports['AbstractTransactionInfoHeadController']},
 'TransactionInfoHeadElement': {get: () => require('./precompile/internal')[43005442188]},
 [43005442188]: {get: () => module.exports['TransactionInfoHeadElement']},
 'TransactionInfoHeadBuffer': {get: () => require('./precompile/internal')[430054421811]},
 [430054421811]: {get: () => module.exports['TransactionInfoHeadBuffer']},
 'AbstractTransactionInfoHeadComputer': {get: () => require('./precompile/internal')[430054421830]},
 [430054421830]: {get: () => module.exports['AbstractTransactionInfoHeadComputer']},
 'TransactionInfoHeadController': {get: () => require('./precompile/internal')[430054421861]},
 [430054421861]: {get: () => module.exports['TransactionInfoHeadController']},
})