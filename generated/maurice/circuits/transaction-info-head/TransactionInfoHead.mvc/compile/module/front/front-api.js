import { TransactionInfoHeadDisplay, TransactionInfoHeadScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.TransactionInfoHeadDisplay} */
export { TransactionInfoHeadDisplay }
/** @lazy @api {xyz.swapee.wc.TransactionInfoHeadScreen} */
export { TransactionInfoHeadScreen }