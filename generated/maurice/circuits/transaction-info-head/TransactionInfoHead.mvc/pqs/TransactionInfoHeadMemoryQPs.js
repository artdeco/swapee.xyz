import {TransactionInfoHeadMemoryPQs} from './TransactionInfoHeadMemoryPQs'
export const TransactionInfoHeadMemoryQPs=/**@type {!xyz.swapee.wc.TransactionInfoHeadMemoryQPs}*/(Object.keys(TransactionInfoHeadMemoryPQs)
 .reduce((a,k)=>{a[TransactionInfoHeadMemoryPQs[k]]=k;return a},{}))