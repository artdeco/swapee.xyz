import {TransactionInfoHeadVdusPQs} from './TransactionInfoHeadVdusPQs'
export const TransactionInfoHeadVdusQPs=/**@type {!xyz.swapee.wc.TransactionInfoHeadVdusQPs}*/(Object.keys(TransactionInfoHeadVdusPQs)
 .reduce((a,k)=>{a[TransactionInfoHeadVdusPQs[k]]=k;return a},{}))