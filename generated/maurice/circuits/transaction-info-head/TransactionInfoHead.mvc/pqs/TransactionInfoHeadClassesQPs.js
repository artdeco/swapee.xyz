import TransactionInfoHeadClassesPQs from './TransactionInfoHeadClassesPQs'
export const TransactionInfoHeadClassesQPs=/**@type {!xyz.swapee.wc.TransactionInfoHeadClassesQPs}*/(Object.keys(TransactionInfoHeadClassesPQs)
 .reduce((a,k)=>{a[TransactionInfoHeadClassesPQs[k]]=k;return a},{}))