import {TransactionInfoHeadQueriesPQs} from './TransactionInfoHeadQueriesPQs'
export const TransactionInfoHeadQueriesQPs=/**@type {!xyz.swapee.wc.TransactionInfoHeadQueriesQPs}*/(Object.keys(TransactionInfoHeadQueriesPQs)
 .reduce((a,k)=>{a[TransactionInfoHeadQueriesPQs[k]]=k;return a},{}))