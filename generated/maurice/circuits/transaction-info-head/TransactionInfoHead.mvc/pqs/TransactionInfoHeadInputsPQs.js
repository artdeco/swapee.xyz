import {TransactionInfoHeadMemoryPQs} from './TransactionInfoHeadMemoryPQs'
export const TransactionInfoHeadInputsPQs=/**@type {!xyz.swapee.wc.TransactionInfoHeadInputsQPs}*/({
 ...TransactionInfoHeadMemoryPQs,
})