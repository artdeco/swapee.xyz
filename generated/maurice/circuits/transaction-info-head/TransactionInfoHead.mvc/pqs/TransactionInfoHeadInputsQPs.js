import {TransactionInfoHeadInputsPQs} from './TransactionInfoHeadInputsPQs'
export const TransactionInfoHeadInputsQPs=/**@type {!xyz.swapee.wc.TransactionInfoHeadInputsQPs}*/(Object.keys(TransactionInfoHeadInputsPQs)
 .reduce((a,k)=>{a[TransactionInfoHeadInputsPQs[k]]=k;return a},{}))