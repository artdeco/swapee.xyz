import TransactionInfoHeadClassesPQs from '../../pqs/TransactionInfoHeadClassesPQs'
import AbstractTransactionInfoHeadScreenAR from '../AbstractTransactionInfoHeadScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {TransactionInfoHeadInputsPQs} from '../../pqs/TransactionInfoHeadInputsPQs'
import {TransactionInfoHeadQueriesPQs} from '../../pqs/TransactionInfoHeadQueriesPQs'
import {TransactionInfoHeadMemoryQPs} from '../../pqs/TransactionInfoHeadMemoryQPs'
import {TransactionInfoHeadVdusPQs} from '../../pqs/TransactionInfoHeadVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadScreen}
 */
function __AbstractTransactionInfoHeadScreen() {}
__AbstractTransactionInfoHeadScreen.prototype = /** @type {!_AbstractTransactionInfoHeadScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadScreen}
 */
class _AbstractTransactionInfoHeadScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadScreen} ‎
 */
class AbstractTransactionInfoHeadScreen extends newAbstract(
 _AbstractTransactionInfoHeadScreen,430054421827,null,{
  asITransactionInfoHeadScreen:1,
  superTransactionInfoHeadScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadScreen} */
AbstractTransactionInfoHeadScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadScreen} */
function AbstractTransactionInfoHeadScreenClass(){}

export default AbstractTransactionInfoHeadScreen


AbstractTransactionInfoHeadScreen[$implementations]=[
 __AbstractTransactionInfoHeadScreen,
 AbstractTransactionInfoHeadScreenClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadScreen}*/({
  inputsPQs:TransactionInfoHeadInputsPQs,
  classesPQs:TransactionInfoHeadClassesPQs,
  queriesPQs:TransactionInfoHeadQueriesPQs,
  memoryQPs:TransactionInfoHeadMemoryQPs,
 }),
 Screen,
 AbstractTransactionInfoHeadScreenAR,
 AbstractTransactionInfoHeadScreenClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadScreen}*/({
  vdusPQs:TransactionInfoHeadVdusPQs,
 }),
]