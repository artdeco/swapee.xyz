import AbstractTransactionInfoHeadScreenAT from '../AbstractTransactionInfoHeadScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadScreenBack}
 */
function __AbstractTransactionInfoHeadScreenBack() {}
__AbstractTransactionInfoHeadScreenBack.prototype = /** @type {!_AbstractTransactionInfoHeadScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen}
 */
class _AbstractTransactionInfoHeadScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen} ‎
 */
class AbstractTransactionInfoHeadScreenBack extends newAbstract(
 _AbstractTransactionInfoHeadScreenBack,430054421828,null,{
  asITransactionInfoHeadScreen:1,
  superTransactionInfoHeadScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen} */
AbstractTransactionInfoHeadScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreen} */
function AbstractTransactionInfoHeadScreenBackClass(){}

export default AbstractTransactionInfoHeadScreenBack


AbstractTransactionInfoHeadScreenBack[$implementations]=[
 __AbstractTransactionInfoHeadScreenBack,
 AbstractTransactionInfoHeadScreenAT,
]