import AbstractTransactionInfoHeadProcessor from '../AbstractTransactionInfoHeadProcessor'
import {TransactionInfoHeadCore} from '../TransactionInfoHeadCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractTransactionInfoHeadComputer} from '../AbstractTransactionInfoHeadComputer'
import {AbstractTransactionInfoHeadController} from '../AbstractTransactionInfoHeadController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHead}
 */
function __AbstractTransactionInfoHead() {}
__AbstractTransactionInfoHead.prototype = /** @type {!_AbstractTransactionInfoHead} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHead}
 */
class _AbstractTransactionInfoHead { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHead} ‎
 */
class AbstractTransactionInfoHead extends newAbstract(
 _AbstractTransactionInfoHead,43005442189,null,{
  asITransactionInfoHead:1,
  superTransactionInfoHead:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHead} */
AbstractTransactionInfoHead.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHead} */
function AbstractTransactionInfoHeadClass(){}

export default AbstractTransactionInfoHead


AbstractTransactionInfoHead[$implementations]=[
 __AbstractTransactionInfoHead,
 TransactionInfoHeadCore,
 AbstractTransactionInfoHeadProcessor,
 IntegratedComponent,
 AbstractTransactionInfoHeadComputer,
 AbstractTransactionInfoHeadController,
]


export {AbstractTransactionInfoHead}