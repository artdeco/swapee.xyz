import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadControllerAT}
 */
function __AbstractTransactionInfoHeadControllerAT() {}
__AbstractTransactionInfoHeadControllerAT.prototype = /** @type {!_AbstractTransactionInfoHeadControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT}
 */
class _AbstractTransactionInfoHeadControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITransactionInfoHeadControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT} ‎
 */
class AbstractTransactionInfoHeadControllerAT extends newAbstract(
 _AbstractTransactionInfoHeadControllerAT,430054421826,null,{
  asITransactionInfoHeadControllerAT:1,
  superTransactionInfoHeadControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT} */
AbstractTransactionInfoHeadControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadControllerAT} */
function AbstractTransactionInfoHeadControllerATClass(){}

export default AbstractTransactionInfoHeadControllerAT


AbstractTransactionInfoHeadControllerAT[$implementations]=[
 __AbstractTransactionInfoHeadControllerAT,
 UartUniversal,
 AbstractTransactionInfoHeadControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ITransactionInfoHeadControllerAT}*/({
  get asITransactionInfoHeadController(){
   return this
  },
 }),
]