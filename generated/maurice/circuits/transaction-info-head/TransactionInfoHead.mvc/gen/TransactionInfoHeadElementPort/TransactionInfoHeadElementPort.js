import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoHeadElementPort}
 */
function __TransactionInfoHeadElementPort() {}
__TransactionInfoHeadElementPort.prototype = /** @type {!_TransactionInfoHeadElementPort} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoHeadElementPort} */ function TransactionInfoHeadElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ITransactionInfoHeadElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    transactionLoaderWrOpts: {},
    transactionFoundWrOpts: {},
    transactionLoInWrOpts: {},
    networkFeeWrOpts: {},
    networkFeeLaOpts: {},
    partnerFeeWrOpts: {},
    partnerFeeLaOpts: {},
    notFoundLaOpts: {},
    inImOpts: {},
    outImOpts: {},
    notFoundWrOpts: {},
    cryptoInOpts: {},
    cryptoOutOpts: {},
    rateLaOpts: {},
    amountInOpts: {},
    amountOutOpts: {},
    visibleAmountWrOpts: {},
    visibleAmounLaOpts: {},
    breakdownBuOpts: {},
    transactionLoInOpts: {},
    reloadGetTransactionBuOpts: {},
    getTransactionErrorWrOpts: {},
    getTransactionErrorLaOpts: {},
    fixedLockOpts: {},
    transactionInfoOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadElementPort}
 */
class _TransactionInfoHeadElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadElementPort} ‎
 */
class TransactionInfoHeadElementPort extends newAbstract(
 _TransactionInfoHeadElementPort,430054421814,TransactionInfoHeadElementPortConstructor,{
  asITransactionInfoHeadElementPort:1,
  superTransactionInfoHeadElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElementPort} */
TransactionInfoHeadElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElementPort} */
function TransactionInfoHeadElementPortClass(){}

export default TransactionInfoHeadElementPort


TransactionInfoHeadElementPort[$implementations]=[
 __TransactionInfoHeadElementPort,
 TransactionInfoHeadElementPortClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'transaction-loader-wr-opts':undefined,
    'transaction-found-wr-opts':undefined,
    'transaction-lo-in-wr-opts':undefined,
    'network-fee-wr-opts':undefined,
    'network-fee-la-opts':undefined,
    'partner-fee-wr-opts':undefined,
    'partner-fee-la-opts':undefined,
    'not-found-la-opts':undefined,
    'in-im-opts':undefined,
    'out-im-opts':undefined,
    'not-found-wr-opts':undefined,
    'crypto-in-opts':undefined,
    'crypto-out-opts':undefined,
    'rate-la-opts':undefined,
    'amount-in-opts':undefined,
    'amount-out-opts':undefined,
    'visible-amount-wr-opts':undefined,
    'visible-amoun-la-opts':undefined,
    'breakdown-bu-opts':undefined,
    'transaction-lo-in-opts':undefined,
    'reload-get-transaction-bu-opts':undefined,
    'get-transaction-error-wr-opts':undefined,
    'get-transaction-error-la-opts':undefined,
    'fixed-lock-opts':undefined,
    'transaction-info-opts':undefined,
   })
  },
 }),
]