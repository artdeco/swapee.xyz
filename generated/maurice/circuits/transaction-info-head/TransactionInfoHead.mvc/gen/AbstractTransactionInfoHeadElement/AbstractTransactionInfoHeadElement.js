import TransactionInfoHeadRenderVdus from './methods/render-vdus'
import TransactionInfoHeadElementPort from '../TransactionInfoHeadElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {TransactionInfoHeadInputsPQs} from '../../pqs/TransactionInfoHeadInputsPQs'
import {TransactionInfoHeadQueriesPQs} from '../../pqs/TransactionInfoHeadQueriesPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractTransactionInfoHead from '../AbstractTransactionInfoHead'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadElement}
 */
function __AbstractTransactionInfoHeadElement() {}
__AbstractTransactionInfoHeadElement.prototype = /** @type {!_AbstractTransactionInfoHeadElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadElement}
 */
class _AbstractTransactionInfoHeadElement { }
/**
 * A component description.
 *
 * The _ITransactionInfoHead_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadElement} ‎
 */
class AbstractTransactionInfoHeadElement extends newAbstract(
 _AbstractTransactionInfoHeadElement,430054421813,null,{
  asITransactionInfoHeadElement:1,
  superTransactionInfoHeadElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElement} */
AbstractTransactionInfoHeadElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadElement} */
function AbstractTransactionInfoHeadElementClass(){}

export default AbstractTransactionInfoHeadElement


AbstractTransactionInfoHeadElement[$implementations]=[
 __AbstractTransactionInfoHeadElement,
 ElementBase,
 Landed,
 AbstractTransactionInfoHeadElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.ITransactionInfoHeadElement}*/({land:{
    TransactionInfo:null,
   }}))
  },
  render:function renderTransactionInfo(){
   const{
    asILanded:{
     land:{
      TransactionInfo:TransactionInfo,
     },
    },
    asITransactionInfoHeadElement:{
     buildTransactionInfo:buildTransactionInfo,
    },
   }=this
   if(!TransactionInfo) return
   const{model:TransactionInfoModel}=TransactionInfo
   const{
    '96c88':currencyFrom,
    'c23cd':currencyTo,
    '748e6':amountFrom,
    '9dc9f':amountTo,
    '67942':rate,
    'cec31':fixed,
    'ffba8':iconsFolder,
    '7c389':gettingTransaction,
    '43e5c':getTransactionError,
    '5fd9c':networkFee,
    '96f44':partnerFee,
    '5ccf4':notFound,
    '4685c':visibleAmount,
   }=TransactionInfoModel
   const res=buildTransactionInfo({
    currencyFrom:currencyFrom,
    currencyTo:currencyTo,
    amountFrom:amountFrom,
    amountTo:amountTo,
    rate:rate,
    fixed:fixed,
    iconsFolder:iconsFolder,
    gettingTransaction:gettingTransaction,
    getTransactionError:getTransactionError,
    networkFee:networkFee,
    partnerFee:partnerFee,
    notFound:notFound,
    visibleAmount:visibleAmount,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractTransactionInfoHeadElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
  render:TransactionInfoHeadRenderVdus,
 }),
 AbstractTransactionInfoHeadElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
  classes:{
   'Class': '9bd81',
  },
  inputsPQs:TransactionInfoHeadInputsPQs,
  queriesPQs:TransactionInfoHeadQueriesPQs,
  vdus:{
   'TransactionLoaderWr': 'd66e1',
   'TransactionFoundWr': 'd66e2',
   'TransactionLoInWr': 'd66e3',
   'NetworkFeeWr': 'd66e4',
   'NetworkFeeLa': 'd66e5',
   'PartnerFeeWr': 'd66e6',
   'PartnerFeeLa': 'd66e7',
   'InIm': 'd66e8',
   'OutIm': 'd66e9',
   'NotFoundWr': 'd66e10',
   'RateLa': 'd66e13',
   'AmountIn': 'd66e14',
   'AmountOut': 'd66e15',
   'VisibleAmountWr': 'd66e16',
   'VisibleAmounLa': 'd66e17',
   'BreakdownBu': 'd66e18',
   'TransactionLoIn': 'd66e19',
   'ReloadGetTransactionBu': 'd66e20',
   'GetTransactionErrorWr': 'd66e21',
   'GetTransactionErrorLa': 'd66e22',
   'FixedLock': 'd66e23',
   'TransactionInfo': 'd66e24',
   'NotFoundLa': 'd66e25',
   'CryptoIn': 'd66e11',
   'CryptoOut': 'd66e12',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractTransactionInfoHeadElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','core','query:transaction-info','no-solder',':no-solder',':core','fe646','fb1e2','aa6df','8edfe','6c941','72e15','97166','ed9b2','678aa','5cba9','4991f','f6dea','3ce39','2de4e','e0e01','b88f2','cd1af','20882','0fc90','54094','9726d','13d7a','00869','22b42','666c3','6b245','a74ad','children']),
   })
  },
  get Port(){
   return TransactionInfoHeadElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:transaction-info':transactionInfoSel}){
   const _ret={}
   if(transactionInfoSel) _ret.transactionInfoSel=transactionInfoSel
   return _ret
  },
 }),
 AbstractTransactionInfoHeadElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
  constructor(){
   this.land={
    TransactionInfo:null,
   }
  },
 }),
 AbstractTransactionInfoHeadElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
  calibrate:async function awaitOnTransactionInfo({transactionInfoSel:transactionInfoSel}){
   if(!transactionInfoSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const TransactionInfo=await milleu(transactionInfoSel,true)
   if(!TransactionInfo) {
    console.warn('❗️ transactionInfoSel %s must be present on the page for %s to work',transactionInfoSel,fqn)
    return{}
   }
   land.TransactionInfo=TransactionInfo
  },
 }),
 AbstractTransactionInfoHeadElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadElement}*/({
  solder:(_,{
   transactionInfoSel:transactionInfoSel,
  })=>{
   return{
    transactionInfoSel:transactionInfoSel,
   }
  },
 }),
]



AbstractTransactionInfoHeadElement[$implementations]=[AbstractTransactionInfoHead,
 /** @type {!AbstractTransactionInfoHeadElement} */ ({
  rootId:'TransactionInfoHead',
  __$id:4300544218,
  fqn:'xyz.swapee.wc.ITransactionInfoHead',
  maurice_element_v3:true,
 }),
]