export default function TransactionInfoHeadRenderVdus(){
 return (<div $id="TransactionInfoHead">
  <vdu $id="TransactionLoaderWr" />
  <vdu $id="TransactionFoundWr" />
  <vdu $id="TransactionLoInWr" />
  <vdu $id="NetworkFeeWr" />
  <vdu $id="NetworkFeeLa" />
  <vdu $id="PartnerFeeWr" />
  <vdu $id="PartnerFeeLa" />
  <vdu $id="NotFoundLa" />
  <vdu $id="InIm" />
  <vdu $id="OutIm" />
  <vdu $id="NotFoundWr" />
  <vdu $id="CryptoIn" />
  <vdu $id="CryptoOut" />
  <vdu $id="RateLa" />
  <vdu $id="AmountIn" />
  <vdu $id="AmountOut" />
  <vdu $id="VisibleAmountWr" />
  <vdu $id="VisibleAmounLa" />
  <vdu $id="BreakdownBu" />
  <vdu $id="TransactionLoIn" />
  <vdu $id="ReloadGetTransactionBu" />
  <vdu $id="GetTransactionErrorWr" />
  <vdu $id="GetTransactionErrorLa" />
  <vdu $id="FixedLock" />
 </div>)
}