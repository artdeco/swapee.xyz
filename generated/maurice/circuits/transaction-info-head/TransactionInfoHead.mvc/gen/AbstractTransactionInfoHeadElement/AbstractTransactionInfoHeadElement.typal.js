
import AbstractTransactionInfoHead from '../AbstractTransactionInfoHead'

/** @abstract {xyz.swapee.wc.ITransactionInfoHeadElement} */
export default class AbstractTransactionInfoHeadElement { }



AbstractTransactionInfoHeadElement[$implementations]=[AbstractTransactionInfoHead,
 /** @type {!AbstractTransactionInfoHeadElement} */ ({
  rootId:'TransactionInfoHead',
  __$id:4300544218,
  fqn:'xyz.swapee.wc.ITransactionInfoHead',
  maurice_element_v3:true,
 }),
]