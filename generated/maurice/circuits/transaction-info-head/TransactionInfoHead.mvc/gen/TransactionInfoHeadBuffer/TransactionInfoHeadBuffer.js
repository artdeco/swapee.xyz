import {makeBuffers} from '@webcircuits/webcircuits'

export const TransactionInfoHeadBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  core:String,
 }),
})

export default TransactionInfoHeadBuffer