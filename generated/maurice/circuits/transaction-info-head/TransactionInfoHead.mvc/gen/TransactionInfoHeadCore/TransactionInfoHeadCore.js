import {mountPins} from '@webcircuits/webcircuits'
import {TransactionInfoHeadMemoryPQs} from '../../pqs/TransactionInfoHeadMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoHeadCore}
 */
function __TransactionInfoHeadCore() {}
__TransactionInfoHeadCore.prototype = /** @type {!_TransactionInfoHeadCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadCore}
 */
class _TransactionInfoHeadCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadCore} ‎
 */
class TransactionInfoHeadCore extends newAbstract(
 _TransactionInfoHeadCore,43005442187,null,{
  asITransactionInfoHeadCore:1,
  superTransactionInfoHeadCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadCore} */
TransactionInfoHeadCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadCore} */
function TransactionInfoHeadCoreClass(){}

export default TransactionInfoHeadCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoHeadOuterCore}
 */
function __TransactionInfoHeadOuterCore() {}
__TransactionInfoHeadOuterCore.prototype = /** @type {!_TransactionInfoHeadOuterCore} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoHeadOuterCore} */
export function TransactionInfoHeadOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ITransactionInfoHeadOuterCore.Model}*/
  this.model={
    core: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore}
 */
class _TransactionInfoHeadOuterCore { }
/**
 * The _ITransactionInfoHead_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore} ‎
 */
export class TransactionInfoHeadOuterCore extends newAbstract(
 _TransactionInfoHeadOuterCore,43005442183,TransactionInfoHeadOuterCoreConstructor,{
  asITransactionInfoHeadOuterCore:1,
  superTransactionInfoHeadOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore} */
TransactionInfoHeadOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadOuterCore} */
function TransactionInfoHeadOuterCoreClass(){}


TransactionInfoHeadOuterCore[$implementations]=[
 __TransactionInfoHeadOuterCore,
 TransactionInfoHeadOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadOuterCore}*/({
  constructor(){
   mountPins(this.model,'',TransactionInfoHeadMemoryPQs)

  },
 }),
]

TransactionInfoHeadCore[$implementations]=[
 TransactionInfoHeadCoreClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadCore}*/({
  resetCore(){
   this.resetTransactionInfoHeadCore()
  },
  resetTransactionInfoHeadCore(){
   TransactionInfoHeadOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.TransactionInfoHeadOuterCore}*/(
     /**@type {!xyz.swapee.wc.ITransactionInfoHeadOuterCore}*/(this)),
   )
  },
 }),
 __TransactionInfoHeadCore,
 TransactionInfoHeadOuterCore,
]

export {TransactionInfoHeadCore}