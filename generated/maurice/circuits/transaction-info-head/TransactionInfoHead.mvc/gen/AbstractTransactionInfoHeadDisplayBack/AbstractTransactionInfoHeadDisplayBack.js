import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadDisplay}
 */
function __AbstractTransactionInfoHeadDisplay() {}
__AbstractTransactionInfoHeadDisplay.prototype = /** @type {!_AbstractTransactionInfoHeadDisplay} */ ({ })
/** @this {xyz.swapee.wc.back.TransactionInfoHeadDisplay} */ function TransactionInfoHeadDisplayConstructor() {
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.CryptoIns=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.CryptoOuts=[]
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay}
 */
class _AbstractTransactionInfoHeadDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay} ‎
 */
class AbstractTransactionInfoHeadDisplay extends newAbstract(
 _AbstractTransactionInfoHeadDisplay,430054421820,TransactionInfoHeadDisplayConstructor,{
  asITransactionInfoHeadDisplay:1,
  superTransactionInfoHeadDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay} */
AbstractTransactionInfoHeadDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadDisplay} */
function AbstractTransactionInfoHeadDisplayClass(){}

export default AbstractTransactionInfoHeadDisplay


AbstractTransactionInfoHeadDisplay[$implementations]=[
 __AbstractTransactionInfoHeadDisplay,
 GraphicsDriverBack,
 AbstractTransactionInfoHeadDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.ITransactionInfoHeadDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.ITransactionInfoHeadDisplay}*/({
    TransactionLoaderWr:twinMock,
    TransactionFoundWr:twinMock,
    TransactionLoInWr:twinMock,
    NetworkFeeWr:twinMock,
    NetworkFeeLa:twinMock,
    PartnerFeeWr:twinMock,
    PartnerFeeLa:twinMock,
    NotFoundLa:twinMock,
    InIm:twinMock,
    OutIm:twinMock,
    NotFoundWr:twinMock,
    RateLa:twinMock,
    AmountIn:twinMock,
    AmountOut:twinMock,
    VisibleAmountWr:twinMock,
    VisibleAmounLa:twinMock,
    BreakdownBu:twinMock,
    TransactionLoIn:twinMock,
    ReloadGetTransactionBu:twinMock,
    GetTransactionErrorWr:twinMock,
    GetTransactionErrorLa:twinMock,
    FixedLock:twinMock,
    TransactionInfo:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.TransactionInfoHeadDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.ITransactionInfoHeadDisplay.Initialese}*/({
   TransactionLoaderWr:1,
   TransactionFoundWr:1,
   TransactionLoInWr:1,
   NetworkFeeWr:1,
   NetworkFeeLa:1,
   PartnerFeeWr:1,
   PartnerFeeLa:1,
   NotFoundLa:1,
   InIm:1,
   OutIm:1,
   NotFoundWr:1,
   CryptoIns:1,
   CryptoOuts:1,
   RateLa:1,
   AmountIn:1,
   AmountOut:1,
   VisibleAmountWr:1,
   VisibleAmounLa:1,
   BreakdownBu:1,
   TransactionLoIn:1,
   ReloadGetTransactionBu:1,
   GetTransactionErrorWr:1,
   GetTransactionErrorLa:1,
   FixedLock:1,
   TransactionInfo:1,
  }),
  initializer({
   TransactionLoaderWr:_TransactionLoaderWr,
   TransactionFoundWr:_TransactionFoundWr,
   TransactionLoInWr:_TransactionLoInWr,
   NetworkFeeWr:_NetworkFeeWr,
   NetworkFeeLa:_NetworkFeeLa,
   PartnerFeeWr:_PartnerFeeWr,
   PartnerFeeLa:_PartnerFeeLa,
   NotFoundLa:_NotFoundLa,
   InIm:_InIm,
   OutIm:_OutIm,
   NotFoundWr:_NotFoundWr,
   CryptoIns:_CryptoIns,
   CryptoOuts:_CryptoOuts,
   RateLa:_RateLa,
   AmountIn:_AmountIn,
   AmountOut:_AmountOut,
   VisibleAmountWr:_VisibleAmountWr,
   VisibleAmounLa:_VisibleAmounLa,
   BreakdownBu:_BreakdownBu,
   TransactionLoIn:_TransactionLoIn,
   ReloadGetTransactionBu:_ReloadGetTransactionBu,
   GetTransactionErrorWr:_GetTransactionErrorWr,
   GetTransactionErrorLa:_GetTransactionErrorLa,
   FixedLock:_FixedLock,
   TransactionInfo:_TransactionInfo,
  }) {
   if(_TransactionLoaderWr!==undefined) this.TransactionLoaderWr=_TransactionLoaderWr
   if(_TransactionFoundWr!==undefined) this.TransactionFoundWr=_TransactionFoundWr
   if(_TransactionLoInWr!==undefined) this.TransactionLoInWr=_TransactionLoInWr
   if(_NetworkFeeWr!==undefined) this.NetworkFeeWr=_NetworkFeeWr
   if(_NetworkFeeLa!==undefined) this.NetworkFeeLa=_NetworkFeeLa
   if(_PartnerFeeWr!==undefined) this.PartnerFeeWr=_PartnerFeeWr
   if(_PartnerFeeLa!==undefined) this.PartnerFeeLa=_PartnerFeeLa
   if(_NotFoundLa!==undefined) this.NotFoundLa=_NotFoundLa
   if(_InIm!==undefined) this.InIm=_InIm
   if(_OutIm!==undefined) this.OutIm=_OutIm
   if(_NotFoundWr!==undefined) this.NotFoundWr=_NotFoundWr
   if(_CryptoIns!==undefined) this.CryptoIns=_CryptoIns
   if(_CryptoOuts!==undefined) this.CryptoOuts=_CryptoOuts
   if(_RateLa!==undefined) this.RateLa=_RateLa
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_AmountOut!==undefined) this.AmountOut=_AmountOut
   if(_VisibleAmountWr!==undefined) this.VisibleAmountWr=_VisibleAmountWr
   if(_VisibleAmounLa!==undefined) this.VisibleAmounLa=_VisibleAmounLa
   if(_BreakdownBu!==undefined) this.BreakdownBu=_BreakdownBu
   if(_TransactionLoIn!==undefined) this.TransactionLoIn=_TransactionLoIn
   if(_ReloadGetTransactionBu!==undefined) this.ReloadGetTransactionBu=_ReloadGetTransactionBu
   if(_GetTransactionErrorWr!==undefined) this.GetTransactionErrorWr=_GetTransactionErrorWr
   if(_GetTransactionErrorLa!==undefined) this.GetTransactionErrorLa=_GetTransactionErrorLa
   if(_FixedLock!==undefined) this.FixedLock=_FixedLock
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
  },
 }),
]