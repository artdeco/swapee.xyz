import AbstractTransactionInfoHeadControllerAR from '../AbstractTransactionInfoHeadControllerAR'
import {AbstractTransactionInfoHeadController} from '../AbstractTransactionInfoHeadController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadControllerBack}
 */
function __AbstractTransactionInfoHeadControllerBack() {}
__AbstractTransactionInfoHeadControllerBack.prototype = /** @type {!_AbstractTransactionInfoHeadControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadController}
 */
class _AbstractTransactionInfoHeadControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadController} ‎
 */
class AbstractTransactionInfoHeadControllerBack extends newAbstract(
 _AbstractTransactionInfoHeadControllerBack,430054421824,null,{
  asITransactionInfoHeadController:1,
  superTransactionInfoHeadController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadController} */
AbstractTransactionInfoHeadControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadController} */
function AbstractTransactionInfoHeadControllerBackClass(){}

export default AbstractTransactionInfoHeadControllerBack


AbstractTransactionInfoHeadControllerBack[$implementations]=[
 __AbstractTransactionInfoHeadControllerBack,
 AbstractTransactionInfoHeadController,
 AbstractTransactionInfoHeadControllerAR,
 DriverBack,
]