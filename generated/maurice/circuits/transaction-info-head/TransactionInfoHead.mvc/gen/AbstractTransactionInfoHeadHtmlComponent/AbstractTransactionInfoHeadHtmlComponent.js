import AbstractTransactionInfoHeadGPU from '../AbstractTransactionInfoHeadGPU'
import AbstractTransactionInfoHeadScreenBack from '../AbstractTransactionInfoHeadScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {TransactionInfoHeadInputsQPs} from '../../pqs/TransactionInfoHeadInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractTransactionInfoHead from '../AbstractTransactionInfoHead'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadHtmlComponent}
 */
function __AbstractTransactionInfoHeadHtmlComponent() {}
__AbstractTransactionInfoHeadHtmlComponent.prototype = /** @type {!_AbstractTransactionInfoHeadHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent}
 */
class _AbstractTransactionInfoHeadHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.TransactionInfoHeadHtmlComponent} */ (res)
  }
}
/**
 * The _ITransactionInfoHead_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent} ‎
 */
export class AbstractTransactionInfoHeadHtmlComponent extends newAbstract(
 _AbstractTransactionInfoHeadHtmlComponent,430054421812,null,{
  asITransactionInfoHeadHtmlComponent:1,
  superTransactionInfoHeadHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent} */
AbstractTransactionInfoHeadHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadHtmlComponent} */
function AbstractTransactionInfoHeadHtmlComponentClass(){}


AbstractTransactionInfoHeadHtmlComponent[$implementations]=[
 __AbstractTransactionInfoHeadHtmlComponent,
 HtmlComponent,
 AbstractTransactionInfoHead,
 AbstractTransactionInfoHeadGPU,
 AbstractTransactionInfoHeadScreenBack,
 Landed,
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  constructor(){
   this.land={
    TransactionInfo:null,
   }
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  inputsQPs:TransactionInfoHeadInputsQPs,
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_TransactionLoaderWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '43e5c':getTransactionError,
    '7c389':gettingTransaction,
    '5ccf4':notFound,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{TransactionLoaderWr:TransactionLoaderWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(TransactionLoaderWr,getTransactionError||gettingTransaction||notFound)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_TransactionLoInWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '7c389':gettingTransaction,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{TransactionLoInWr:TransactionLoInWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(TransactionLoInWr,gettingTransaction)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_GetTransactionErrorWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '43e5c':getTransactionError,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{GetTransactionErrorWr:GetTransactionErrorWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(GetTransactionErrorWr,getTransactionError)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function interrupt_click_on_GetTransactionErrorWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   const{asITransactionInfoHeadGPU:{GetTransactionErrorWr:GetTransactionErrorWr}}=this
   GetTransactionErrorWr.listen('click',(ev)=>{
    TransactionInfo['f303b']()
   })
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_GetTransactionErrorLa_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '43e5c':getTransactionError,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{GetTransactionErrorLa:GetTransactionErrorLa}}=this
   GetTransactionErrorLa.setText(getTransactionError)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_NotFoundWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '5ccf4':notFound,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{NotFoundWr:NotFoundWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(NotFoundWr,notFound)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_TransactionFoundWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '748e6':amountFrom,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{TransactionFoundWr:TransactionFoundWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(TransactionFoundWr,amountFrom)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $show_CryptoIn(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96c88':currencyFrom,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{CryptoIns:CryptoIns},
    asIBrowserView:{style:style},
   }=this
   style(CryptoIns,'opacity',currencyFrom?null:0,true)
   style(CryptoIns,'pointer-events',currencyFrom?null:'none',true)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_CryptoIns_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96c88':currencyFrom,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{CryptoIns:CryptoIns}}=this
   for(const CryptoIn of CryptoIns){
    CryptoIn.setText(currencyFrom)
   }
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $show_CryptoOut(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    'c23cd':currencyTo,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{CryptoOuts:CryptoOuts},
    asIBrowserView:{style:style},
   }=this
   style(CryptoOuts,'opacity',currencyTo?null:0,true)
   style(CryptoOuts,'pointer-events',currencyTo?null:'none',true)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_CryptoOuts_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    'c23cd':currencyTo,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{CryptoOuts:CryptoOuts}}=this
   for(const CryptoOut of CryptoOuts){
    CryptoOut.setText(currencyTo)
   }
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_RateLa_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '67942':rate,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{RateLa:RateLa}}=this
   RateLa.setText(rate)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $show_AmountIn(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '748e6':amountFrom,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{AmountIn:AmountIn},
    asIBrowserView:{style:style},
   }=this
   style(AmountIn,'opacity',amountFrom?null:0,true)
   style(AmountIn,'pointer-events',amountFrom?null:'none',true)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_AmountIn_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '748e6':amountFrom,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{AmountIn:AmountIn}}=this
   AmountIn.setText(amountFrom||0)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $show_AmountOut(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '9dc9f':amountTo,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{AmountOut:AmountOut},
    asIBrowserView:{style:style},
   }=this
   style(AmountOut,'opacity',amountTo?null:0,true)
   style(AmountOut,'pointer-events',amountTo?null:'none',true)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_AmountOut_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '9dc9f':amountTo,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{AmountOut:AmountOut}}=this
   AmountOut.setText(amountTo||0)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_src_on_InIm(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96c88':currencyFrom,
    'ffba8':iconsFolder,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{InIm:InIm},asIBrowserView:{attr:attr}}=this
   attr(InIm,'src',currencyFrom&&(iconsFolder+'/'+currencyFrom+'.png'))
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_src_on_OutIm(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    'c23cd':currencyTo,
    'ffba8':iconsFolder,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{OutIm:OutIm},asIBrowserView:{attr:attr}}=this
   attr(OutIm,'src',currencyTo&&(iconsFolder+'/'+currencyTo+'.png'))
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_FixedLock(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    'cec31':fixed,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{FixedLock:FixedLock},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(FixedLock,fixed)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_NetworkFeeWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96c88':currencyFrom,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{NetworkFeeWr:NetworkFeeWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(NetworkFeeWr,currencyFrom)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_NetworkFeeLa_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '5fd9c':networkFee,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{NetworkFeeLa:NetworkFeeLa}}=this
   NetworkFeeLa.setText(networkFee||'?')
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_PartnerFeeWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96f44':partnerFee,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{PartnerFeeWr:PartnerFeeWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(PartnerFeeWr,partnerFee)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_PartnerFeeLa_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96f44':partnerFee,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{PartnerFeeLa:PartnerFeeLa}}=this
   PartnerFeeLa.setText(partnerFee)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_VisibleAmountWr(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '4685c':visibleAmount,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{VisibleAmountWr:VisibleAmountWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(VisibleAmountWr,visibleAmount)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function paint_VisibleAmounLa_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '4685c':visibleAmount,
   }=TransactionInfo.model
   const{asITransactionInfoHeadGPU:{VisibleAmounLa:VisibleAmounLa}}=this
   VisibleAmounLa.setText(visibleAmount)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  paint:function $reveal_BreakdownBu(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '5fd9c':networkFee,
   }=TransactionInfo.model
   const{
    asITransactionInfoHeadGPU:{BreakdownBu:BreakdownBu},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(BreakdownBu,networkFee)
  },
 }),
 AbstractTransactionInfoHeadHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asITransactionInfoHeadGPU:{
     TransactionInfo:TransactionInfo,
    },
   }=this
   complete(1988051819,{TransactionInfo:TransactionInfo})
  },
 }),
]