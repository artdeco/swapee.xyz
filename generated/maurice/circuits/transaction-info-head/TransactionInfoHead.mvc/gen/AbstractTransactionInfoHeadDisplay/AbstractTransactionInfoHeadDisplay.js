import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadDisplay}
 */
function __AbstractTransactionInfoHeadDisplay() {}
__AbstractTransactionInfoHeadDisplay.prototype = /** @type {!_AbstractTransactionInfoHeadDisplay} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoHeadDisplay} */ function TransactionInfoHeadDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.TransactionLoaderWr=null
  /** @type {HTMLDivElement} */ this.TransactionFoundWr=null
  /** @type {HTMLDivElement} */ this.TransactionLoInWr=null
  /** @type {HTMLSpanElement} */ this.NetworkFeeWr=null
  /** @type {HTMLSpanElement} */ this.NetworkFeeLa=null
  /** @type {HTMLSpanElement} */ this.PartnerFeeWr=null
  /** @type {HTMLSpanElement} */ this.PartnerFeeLa=null
  /** @type {HTMLSpanElement} */ this.NotFoundLa=null
  /** @type {HTMLImageElement} */ this.InIm=null
  /** @type {HTMLImageElement} */ this.OutIm=null
  /** @type {HTMLDivElement} */ this.NotFoundWr=null
  /** @type {!Array<!HTMLSpanElement>} */ this.CryptoIns=[]
  /** @type {!Array<!HTMLSpanElement>} */ this.CryptoOuts=[]
  /** @type {HTMLSpanElement} */ this.RateLa=null
  /** @type {HTMLSpanElement} */ this.AmountIn=null
  /** @type {HTMLSpanElement} */ this.AmountOut=null
  /** @type {HTMLSpanElement} */ this.VisibleAmountWr=null
  /** @type {HTMLSpanElement} */ this.VisibleAmounLa=null
  /** @type {HTMLButtonElement} */ this.BreakdownBu=null
  /** @type {HTMLSpanElement} */ this.TransactionLoIn=null
  /** @type {HTMLButtonElement} */ this.ReloadGetTransactionBu=null
  /** @type {HTMLSpanElement} */ this.GetTransactionErrorWr=null
  /** @type {HTMLSpanElement} */ this.GetTransactionErrorLa=null
  /** @type {HTMLSpanElement} */ this.FixedLock=null
  /** @type {HTMLElement} */ this.TransactionInfo=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadDisplay}
 */
class _AbstractTransactionInfoHeadDisplay { }
/**
 * Display for presenting information from the _ITransactionInfoHead_.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadDisplay} ‎
 */
class AbstractTransactionInfoHeadDisplay extends newAbstract(
 _AbstractTransactionInfoHeadDisplay,430054421817,TransactionInfoHeadDisplayConstructor,{
  asITransactionInfoHeadDisplay:1,
  superTransactionInfoHeadDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadDisplay} */
AbstractTransactionInfoHeadDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadDisplay} */
function AbstractTransactionInfoHeadDisplayClass(){}

export default AbstractTransactionInfoHeadDisplay


AbstractTransactionInfoHeadDisplay[$implementations]=[
 __AbstractTransactionInfoHeadDisplay,
 Display,
 AbstractTransactionInfoHeadDisplayClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{transactionInfoScopeSel:transactionInfoScopeSel}}=this
    this.scan({
     transactionInfoSel:transactionInfoScopeSel,
    })
   })
  },
  scan:function vduScan({transactionInfoSel:transactionInfoSelScope}){
   const{element:element,asITransactionInfoHeadScreen:{vdusPQs:{
    TransactionLoaderWr:TransactionLoaderWr,
    TransactionFoundWr:TransactionFoundWr,
    TransactionLoInWr:TransactionLoInWr,
    NetworkFeeWr:NetworkFeeWr,
    NetworkFeeLa:NetworkFeeLa,
    PartnerFeeWr:PartnerFeeWr,
    PartnerFeeLa:PartnerFeeLa,
    NotFoundLa:NotFoundLa,
    InIm:InIm,
    OutIm:OutIm,
    NotFoundWr:NotFoundWr,
    RateLa:RateLa,
    AmountIn:AmountIn,
    AmountOut:AmountOut,
    VisibleAmountWr:VisibleAmountWr,
    VisibleAmounLa:VisibleAmounLa,
    BreakdownBu:BreakdownBu,
    TransactionLoIn:TransactionLoIn,
    ReloadGetTransactionBu:ReloadGetTransactionBu,
    GetTransactionErrorWr:GetTransactionErrorWr,
    GetTransactionErrorLa:GetTransactionErrorLa,
    FixedLock:FixedLock,
   }},queries:{transactionInfoSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const TransactionInfo=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _TransactionInfo=transactionInfoSel?root.querySelector(transactionInfoSel):void 0
    return _TransactionInfo
   })(transactionInfoSelScope)
   Object.assign(this,{
    TransactionLoaderWr:/**@type {HTMLDivElement}*/(children[TransactionLoaderWr]),
    TransactionFoundWr:/**@type {HTMLDivElement}*/(children[TransactionFoundWr]),
    TransactionLoInWr:/**@type {HTMLDivElement}*/(children[TransactionLoInWr]),
    NetworkFeeWr:/**@type {HTMLSpanElement}*/(children[NetworkFeeWr]),
    NetworkFeeLa:/**@type {HTMLSpanElement}*/(children[NetworkFeeLa]),
    PartnerFeeWr:/**@type {HTMLSpanElement}*/(children[PartnerFeeWr]),
    PartnerFeeLa:/**@type {HTMLSpanElement}*/(children[PartnerFeeLa]),
    NotFoundLa:/**@type {HTMLSpanElement}*/(children[NotFoundLa]),
    InIm:/**@type {HTMLImageElement}*/(children[InIm]),
    OutIm:/**@type {HTMLImageElement}*/(children[OutIm]),
    NotFoundWr:/**@type {HTMLDivElement}*/(children[NotFoundWr]),
    CryptoIns:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=d66e11]')]):[],
    CryptoOuts:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=d66e12]')]):[],
    RateLa:/**@type {HTMLSpanElement}*/(children[RateLa]),
    AmountIn:/**@type {HTMLSpanElement}*/(children[AmountIn]),
    AmountOut:/**@type {HTMLSpanElement}*/(children[AmountOut]),
    VisibleAmountWr:/**@type {HTMLSpanElement}*/(children[VisibleAmountWr]),
    VisibleAmounLa:/**@type {HTMLSpanElement}*/(children[VisibleAmounLa]),
    BreakdownBu:/**@type {HTMLButtonElement}*/(children[BreakdownBu]),
    TransactionLoIn:/**@type {HTMLSpanElement}*/(children[TransactionLoIn]),
    ReloadGetTransactionBu:/**@type {HTMLButtonElement}*/(children[ReloadGetTransactionBu]),
    GetTransactionErrorWr:/**@type {HTMLSpanElement}*/(children[GetTransactionErrorWr]),
    GetTransactionErrorLa:/**@type {HTMLSpanElement}*/(children[GetTransactionErrorLa]),
    FixedLock:/**@type {HTMLSpanElement}*/(children[FixedLock]),
    TransactionInfo:TransactionInfo,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.TransactionInfoHeadDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.ITransactionInfoHeadDisplay.Initialese}*/({
   TransactionLoaderWr:1,
   TransactionFoundWr:1,
   TransactionLoInWr:1,
   NetworkFeeWr:1,
   NetworkFeeLa:1,
   PartnerFeeWr:1,
   PartnerFeeLa:1,
   NotFoundLa:1,
   InIm:1,
   OutIm:1,
   NotFoundWr:1,
   CryptoIns:1,
   CryptoOuts:1,
   RateLa:1,
   AmountIn:1,
   AmountOut:1,
   VisibleAmountWr:1,
   VisibleAmounLa:1,
   BreakdownBu:1,
   TransactionLoIn:1,
   ReloadGetTransactionBu:1,
   GetTransactionErrorWr:1,
   GetTransactionErrorLa:1,
   FixedLock:1,
   TransactionInfo:1,
  }),
  initializer({
   TransactionLoaderWr:_TransactionLoaderWr,
   TransactionFoundWr:_TransactionFoundWr,
   TransactionLoInWr:_TransactionLoInWr,
   NetworkFeeWr:_NetworkFeeWr,
   NetworkFeeLa:_NetworkFeeLa,
   PartnerFeeWr:_PartnerFeeWr,
   PartnerFeeLa:_PartnerFeeLa,
   NotFoundLa:_NotFoundLa,
   InIm:_InIm,
   OutIm:_OutIm,
   NotFoundWr:_NotFoundWr,
   CryptoIns:_CryptoIns,
   CryptoOuts:_CryptoOuts,
   RateLa:_RateLa,
   AmountIn:_AmountIn,
   AmountOut:_AmountOut,
   VisibleAmountWr:_VisibleAmountWr,
   VisibleAmounLa:_VisibleAmounLa,
   BreakdownBu:_BreakdownBu,
   TransactionLoIn:_TransactionLoIn,
   ReloadGetTransactionBu:_ReloadGetTransactionBu,
   GetTransactionErrorWr:_GetTransactionErrorWr,
   GetTransactionErrorLa:_GetTransactionErrorLa,
   FixedLock:_FixedLock,
   TransactionInfo:_TransactionInfo,
  }) {
   if(_TransactionLoaderWr!==undefined) this.TransactionLoaderWr=_TransactionLoaderWr
   if(_TransactionFoundWr!==undefined) this.TransactionFoundWr=_TransactionFoundWr
   if(_TransactionLoInWr!==undefined) this.TransactionLoInWr=_TransactionLoInWr
   if(_NetworkFeeWr!==undefined) this.NetworkFeeWr=_NetworkFeeWr
   if(_NetworkFeeLa!==undefined) this.NetworkFeeLa=_NetworkFeeLa
   if(_PartnerFeeWr!==undefined) this.PartnerFeeWr=_PartnerFeeWr
   if(_PartnerFeeLa!==undefined) this.PartnerFeeLa=_PartnerFeeLa
   if(_NotFoundLa!==undefined) this.NotFoundLa=_NotFoundLa
   if(_InIm!==undefined) this.InIm=_InIm
   if(_OutIm!==undefined) this.OutIm=_OutIm
   if(_NotFoundWr!==undefined) this.NotFoundWr=_NotFoundWr
   if(_CryptoIns!==undefined) this.CryptoIns=_CryptoIns
   if(_CryptoOuts!==undefined) this.CryptoOuts=_CryptoOuts
   if(_RateLa!==undefined) this.RateLa=_RateLa
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_AmountOut!==undefined) this.AmountOut=_AmountOut
   if(_VisibleAmountWr!==undefined) this.VisibleAmountWr=_VisibleAmountWr
   if(_VisibleAmounLa!==undefined) this.VisibleAmounLa=_VisibleAmounLa
   if(_BreakdownBu!==undefined) this.BreakdownBu=_BreakdownBu
   if(_TransactionLoIn!==undefined) this.TransactionLoIn=_TransactionLoIn
   if(_ReloadGetTransactionBu!==undefined) this.ReloadGetTransactionBu=_ReloadGetTransactionBu
   if(_GetTransactionErrorWr!==undefined) this.GetTransactionErrorWr=_GetTransactionErrorWr
   if(_GetTransactionErrorLa!==undefined) this.GetTransactionErrorLa=_GetTransactionErrorLa
   if(_FixedLock!==undefined) this.FixedLock=_FixedLock
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
  },
 }),
]