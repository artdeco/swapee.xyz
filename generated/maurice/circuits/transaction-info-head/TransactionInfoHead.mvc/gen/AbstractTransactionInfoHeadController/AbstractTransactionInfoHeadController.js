import TransactionInfoHeadBuffer from '../TransactionInfoHeadBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {TransactionInfoHeadPortConnector} from '../TransactionInfoHeadPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadController}
 */
function __AbstractTransactionInfoHeadController() {}
__AbstractTransactionInfoHeadController.prototype = /** @type {!_AbstractTransactionInfoHeadController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadController}
 */
class _AbstractTransactionInfoHeadController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadController} ‎
 */
export class AbstractTransactionInfoHeadController extends newAbstract(
 _AbstractTransactionInfoHeadController,430054421822,null,{
  asITransactionInfoHeadController:1,
  superTransactionInfoHeadController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadController} */
AbstractTransactionInfoHeadController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadController} */
function AbstractTransactionInfoHeadControllerClass(){}


AbstractTransactionInfoHeadController[$implementations]=[
 AbstractTransactionInfoHeadControllerClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.ITransactionInfoHeadPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractTransactionInfoHeadController,
 TransactionInfoHeadBuffer,
 IntegratedController,
 /**@type {!AbstractTransactionInfoHeadController}*/(TransactionInfoHeadPortConnector),
]


export default AbstractTransactionInfoHeadController