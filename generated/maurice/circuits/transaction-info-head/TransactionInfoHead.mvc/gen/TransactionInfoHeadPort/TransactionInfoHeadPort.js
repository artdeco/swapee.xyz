import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {TransactionInfoHeadInputsPQs} from '../../pqs/TransactionInfoHeadInputsPQs'
import {TransactionInfoHeadOuterCoreConstructor} from '../TransactionInfoHeadCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoHeadPort}
 */
function __TransactionInfoHeadPort() {}
__TransactionInfoHeadPort.prototype = /** @type {!_TransactionInfoHeadPort} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoHeadPort} */ function TransactionInfoHeadPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.TransactionInfoHeadOuterCore} */ ({model:null})
  TransactionInfoHeadOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadPort}
 */
class _TransactionInfoHeadPort { }
/**
 * The port that serves as an interface to the _ITransactionInfoHead_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadPort} ‎
 */
export class TransactionInfoHeadPort extends newAbstract(
 _TransactionInfoHeadPort,43005442185,TransactionInfoHeadPortConstructor,{
  asITransactionInfoHeadPort:1,
  superTransactionInfoHeadPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadPort} */
TransactionInfoHeadPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadPort} */
function TransactionInfoHeadPortClass(){}

export const TransactionInfoHeadPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ITransactionInfoHead.Pinout>}*/({
 get Port() { return TransactionInfoHeadPort },
})

TransactionInfoHeadPort[$implementations]=[
 TransactionInfoHeadPortClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadPort}*/({
  resetPort(){
   this.resetTransactionInfoHeadPort()
  },
  resetTransactionInfoHeadPort(){
   TransactionInfoHeadPortConstructor.call(this)
  },
 }),
 __TransactionInfoHeadPort,
 Parametric,
 TransactionInfoHeadPortClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadPort}*/({
  constructor(){
   mountPins(this.inputs,'',TransactionInfoHeadInputsPQs)
  },
 }),
]


export default TransactionInfoHeadPort