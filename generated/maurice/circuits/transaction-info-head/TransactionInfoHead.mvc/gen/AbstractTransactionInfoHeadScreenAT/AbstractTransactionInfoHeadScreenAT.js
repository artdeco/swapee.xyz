import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadScreenAT}
 */
function __AbstractTransactionInfoHeadScreenAT() {}
__AbstractTransactionInfoHeadScreenAT.prototype = /** @type {!_AbstractTransactionInfoHeadScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT}
 */
class _AbstractTransactionInfoHeadScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITransactionInfoHeadScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT} ‎
 */
class AbstractTransactionInfoHeadScreenAT extends newAbstract(
 _AbstractTransactionInfoHeadScreenAT,430054421830,null,{
  asITransactionInfoHeadScreenAT:1,
  superTransactionInfoHeadScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT} */
AbstractTransactionInfoHeadScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadScreenAT} */
function AbstractTransactionInfoHeadScreenATClass(){}

export default AbstractTransactionInfoHeadScreenAT


AbstractTransactionInfoHeadScreenAT[$implementations]=[
 __AbstractTransactionInfoHeadScreenAT,
 UartUniversal,
]