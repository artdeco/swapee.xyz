import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadProcessor}
 */
function __AbstractTransactionInfoHeadProcessor() {}
__AbstractTransactionInfoHeadProcessor.prototype = /** @type {!_AbstractTransactionInfoHeadProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadProcessor}
 */
class _AbstractTransactionInfoHeadProcessor { }
/**
 * The processor to compute changes to the memory for the _ITransactionInfoHead_.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadProcessor} ‎
 */
class AbstractTransactionInfoHeadProcessor extends newAbstract(
 _AbstractTransactionInfoHeadProcessor,43005442188,null,{
  asITransactionInfoHeadProcessor:1,
  superTransactionInfoHeadProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadProcessor} */
AbstractTransactionInfoHeadProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadProcessor} */
function AbstractTransactionInfoHeadProcessorClass(){}

export default AbstractTransactionInfoHeadProcessor


AbstractTransactionInfoHeadProcessor[$implementations]=[
 __AbstractTransactionInfoHeadProcessor,
]