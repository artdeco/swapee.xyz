import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadScreenAR}
 */
function __AbstractTransactionInfoHeadScreenAR() {}
__AbstractTransactionInfoHeadScreenAR.prototype = /** @type {!_AbstractTransactionInfoHeadScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR}
 */
class _AbstractTransactionInfoHeadScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITransactionInfoHeadScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR} ‎
 */
class AbstractTransactionInfoHeadScreenAR extends newAbstract(
 _AbstractTransactionInfoHeadScreenAR,430054421829,null,{
  asITransactionInfoHeadScreenAR:1,
  superTransactionInfoHeadScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR} */
AbstractTransactionInfoHeadScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoHeadScreenAR} */
function AbstractTransactionInfoHeadScreenARClass(){}

export default AbstractTransactionInfoHeadScreenAR


AbstractTransactionInfoHeadScreenAR[$implementations]=[
 __AbstractTransactionInfoHeadScreenAR,
 AR,
 AbstractTransactionInfoHeadScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ITransactionInfoHeadScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractTransactionInfoHeadScreenAR}