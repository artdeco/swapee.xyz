import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadComputer}
 */
function __AbstractTransactionInfoHeadComputer() {}
__AbstractTransactionInfoHeadComputer.prototype = /** @type {!_AbstractTransactionInfoHeadComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadComputer}
 */
class _AbstractTransactionInfoHeadComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadComputer} ‎
 */
export class AbstractTransactionInfoHeadComputer extends newAbstract(
 _AbstractTransactionInfoHeadComputer,43005442181,null,{
  asITransactionInfoHeadComputer:1,
  superTransactionInfoHeadComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadComputer} */
AbstractTransactionInfoHeadComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadComputer} */
function AbstractTransactionInfoHeadComputerClass(){}


AbstractTransactionInfoHeadComputer[$implementations]=[
 __AbstractTransactionInfoHeadComputer,
 Adapter,
]


export default AbstractTransactionInfoHeadComputer