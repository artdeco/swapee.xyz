import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadControllerAR}
 */
function __AbstractTransactionInfoHeadControllerAR() {}
__AbstractTransactionInfoHeadControllerAR.prototype = /** @type {!_AbstractTransactionInfoHeadControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR}
 */
class _AbstractTransactionInfoHeadControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITransactionInfoHeadControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR} ‎
 */
class AbstractTransactionInfoHeadControllerAR extends newAbstract(
 _AbstractTransactionInfoHeadControllerAR,430054421825,null,{
  asITransactionInfoHeadControllerAR:1,
  superTransactionInfoHeadControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR} */
AbstractTransactionInfoHeadControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoHeadControllerAR} */
function AbstractTransactionInfoHeadControllerARClass(){}

export default AbstractTransactionInfoHeadControllerAR


AbstractTransactionInfoHeadControllerAR[$implementations]=[
 __AbstractTransactionInfoHeadControllerAR,
 AR,
 AbstractTransactionInfoHeadControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ITransactionInfoHeadControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]