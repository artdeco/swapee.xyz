import AbstractTransactionInfoHeadDisplay from '../AbstractTransactionInfoHeadDisplayBack'
import TransactionInfoHeadClassesPQs from '../../pqs/TransactionInfoHeadClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {TransactionInfoHeadClassesQPs} from '../../pqs/TransactionInfoHeadClassesQPs'
import {TransactionInfoHeadVdusPQs} from '../../pqs/TransactionInfoHeadVdusPQs'
import {TransactionInfoHeadVdusQPs} from '../../pqs/TransactionInfoHeadVdusQPs'
import {TransactionInfoHeadMemoryPQs} from '../../pqs/TransactionInfoHeadMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHeadGPU}
 */
function __AbstractTransactionInfoHeadGPU() {}
__AbstractTransactionInfoHeadGPU.prototype = /** @type {!_AbstractTransactionInfoHeadGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadGPU}
 */
class _AbstractTransactionInfoHeadGPU { }
/**
 * Handles the periphery of the _ITransactionInfoHeadDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHeadGPU} ‎
 */
class AbstractTransactionInfoHeadGPU extends newAbstract(
 _AbstractTransactionInfoHeadGPU,430054421816,null,{
  asITransactionInfoHeadGPU:1,
  superTransactionInfoHeadGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadGPU} */
AbstractTransactionInfoHeadGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHeadGPU} */
function AbstractTransactionInfoHeadGPUClass(){}

export default AbstractTransactionInfoHeadGPU


AbstractTransactionInfoHeadGPU[$implementations]=[
 __AbstractTransactionInfoHeadGPU,
 AbstractTransactionInfoHeadGPUClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadGPU}*/({
  classesQPs:TransactionInfoHeadClassesQPs,
  vdusPQs:TransactionInfoHeadVdusPQs,
  vdusQPs:TransactionInfoHeadVdusQPs,
  memoryPQs:TransactionInfoHeadMemoryPQs,
 }),
 AbstractTransactionInfoHeadDisplay,
 BrowserView,
 AbstractTransactionInfoHeadGPUClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHeadGPU}*/({
  allocator(){
   pressFit(this.classes,'',TransactionInfoHeadClassesPQs)
  },
 }),
]