import AbstractHyperTransactionInfoHeadInterruptLine from '../../../../gen/AbstractTransactionInfoHeadInterruptLine/hyper/AbstractHyperTransactionInfoHeadInterruptLine'
import TransactionInfoHeadInterruptLine from '../../TransactionInfoHeadInterruptLine'
import TransactionInfoHeadInterruptLineGeneralAspects from '../TransactionInfoHeadInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperTransactionInfoHeadInterruptLine} */
export default class extends AbstractHyperTransactionInfoHeadInterruptLine
 .consults(
  TransactionInfoHeadInterruptLineGeneralAspects,
 )
 .implements(
  TransactionInfoHeadInterruptLine,
 )
{}