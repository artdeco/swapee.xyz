import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonDisplay}
 */
function __AbstractSwapeeButtonDisplay() {}
__AbstractSwapeeButtonDisplay.prototype = /** @type {!_AbstractSwapeeButtonDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonDisplay}
 */
class _AbstractSwapeeButtonDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonDisplay} ‎
 */
class AbstractSwapeeButtonDisplay extends newAbstract(
 _AbstractSwapeeButtonDisplay,317913044518,null,{
  asISwapeeButtonDisplay:1,
  superSwapeeButtonDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonDisplay} */
AbstractSwapeeButtonDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonDisplay} */
function AbstractSwapeeButtonDisplayClass(){}

export default AbstractSwapeeButtonDisplay


AbstractSwapeeButtonDisplay[$implementations]=[
 __AbstractSwapeeButtonDisplay,
 GraphicsDriverBack,
 AbstractSwapeeButtonDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeButtonDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.ISwapeeButtonDisplay}*/({
    BuLa:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.SwapeeButtonDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.ISwapeeButtonDisplay.Initialese}*/({
   BuLa:1,
  }),
  initializer({
   BuLa:_BuLa,
  }) {
   if(_BuLa!==undefined) this.BuLa=_BuLa
  },
 }),
]