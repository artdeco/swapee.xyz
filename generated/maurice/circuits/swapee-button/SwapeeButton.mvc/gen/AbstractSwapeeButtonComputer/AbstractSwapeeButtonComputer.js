import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonComputer}
 */
function __AbstractSwapeeButtonComputer() {}
__AbstractSwapeeButtonComputer.prototype = /** @type {!_AbstractSwapeeButtonComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonComputer}
 */
class _AbstractSwapeeButtonComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonComputer} ‎
 */
export class AbstractSwapeeButtonComputer extends newAbstract(
 _AbstractSwapeeButtonComputer,31791304451,null,{
  asISwapeeButtonComputer:1,
  superSwapeeButtonComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonComputer} */
AbstractSwapeeButtonComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonComputer} */
function AbstractSwapeeButtonComputerClass(){}


AbstractSwapeeButtonComputer[$implementations]=[
 __AbstractSwapeeButtonComputer,
 Adapter,
]


export default AbstractSwapeeButtonComputer