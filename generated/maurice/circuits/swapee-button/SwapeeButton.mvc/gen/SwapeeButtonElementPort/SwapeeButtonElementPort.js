import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeButtonElementPort}
 */
function __SwapeeButtonElementPort() {}
__SwapeeButtonElementPort.prototype = /** @type {!_SwapeeButtonElementPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeButtonElementPort} */ function SwapeeButtonElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeButtonElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    buLaOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonElementPort}
 */
class _SwapeeButtonElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonElementPort} ‎
 */
class SwapeeButtonElementPort extends newAbstract(
 _SwapeeButtonElementPort,317913044513,SwapeeButtonElementPortConstructor,{
  asISwapeeButtonElementPort:1,
  superSwapeeButtonElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonElementPort} */
SwapeeButtonElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonElementPort} */
function SwapeeButtonElementPortClass(){}

export default SwapeeButtonElementPort


SwapeeButtonElementPort[$implementations]=[
 __SwapeeButtonElementPort,
 SwapeeButtonElementPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'bu-la-opts':undefined,
   })
  },
 }),
]