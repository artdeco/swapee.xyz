import AbstractSwapeeButtonProcessor from '../AbstractSwapeeButtonProcessor'
import {SwapeeButtonCore} from '../SwapeeButtonCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractSwapeeButtonComputer} from '../AbstractSwapeeButtonComputer'
import {AbstractSwapeeButtonController} from '../AbstractSwapeeButtonController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButton}
 */
function __AbstractSwapeeButton() {}
__AbstractSwapeeButton.prototype = /** @type {!_AbstractSwapeeButton} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButton}
 */
class _AbstractSwapeeButton { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractSwapeeButton} ‎
 */
class AbstractSwapeeButton extends newAbstract(
 _AbstractSwapeeButton,31791304459,null,{
  asISwapeeButton:1,
  superSwapeeButton:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButton} */
AbstractSwapeeButton.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButton} */
function AbstractSwapeeButtonClass(){}

export default AbstractSwapeeButton


AbstractSwapeeButton[$implementations]=[
 __AbstractSwapeeButton,
 SwapeeButtonCore,
 AbstractSwapeeButtonProcessor,
 IntegratedComponent,
 AbstractSwapeeButtonComputer,
 AbstractSwapeeButtonController,
]


export {AbstractSwapeeButton}