import AbstractSwapeeButtonControllerAR from '../AbstractSwapeeButtonControllerAR'
import {AbstractSwapeeButtonController} from '../AbstractSwapeeButtonController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonControllerBack}
 */
function __AbstractSwapeeButtonControllerBack() {}
__AbstractSwapeeButtonControllerBack.prototype = /** @type {!_AbstractSwapeeButtonControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonController}
 */
class _AbstractSwapeeButtonControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonController} ‎
 */
class AbstractSwapeeButtonControllerBack extends newAbstract(
 _AbstractSwapeeButtonControllerBack,317913044521,null,{
  asISwapeeButtonController:1,
  superSwapeeButtonController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonController} */
AbstractSwapeeButtonControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonController} */
function AbstractSwapeeButtonControllerBackClass(){}

export default AbstractSwapeeButtonControllerBack


AbstractSwapeeButtonControllerBack[$implementations]=[
 __AbstractSwapeeButtonControllerBack,
 AbstractSwapeeButtonController,
 AbstractSwapeeButtonControllerAR,
 DriverBack,
]