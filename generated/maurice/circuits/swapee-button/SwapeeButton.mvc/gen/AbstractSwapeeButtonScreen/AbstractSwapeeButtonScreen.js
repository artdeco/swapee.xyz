import AbstractSwapeeButtonScreenAR from '../AbstractSwapeeButtonScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {SwapeeButtonInputsPQs} from '../../pqs/SwapeeButtonInputsPQs'
import {SwapeeButtonMemoryQPs} from '../../pqs/SwapeeButtonMemoryQPs'
import {SwapeeButtonVdusPQs} from '../../pqs/SwapeeButtonVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonScreen}
 */
function __AbstractSwapeeButtonScreen() {}
__AbstractSwapeeButtonScreen.prototype = /** @type {!_AbstractSwapeeButtonScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonScreen}
 */
class _AbstractSwapeeButtonScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonScreen} ‎
 */
class AbstractSwapeeButtonScreen extends newAbstract(
 _AbstractSwapeeButtonScreen,317913044524,null,{
  asISwapeeButtonScreen:1,
  superSwapeeButtonScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonScreen} */
AbstractSwapeeButtonScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonScreen} */
function AbstractSwapeeButtonScreenClass(){}

export default AbstractSwapeeButtonScreen


AbstractSwapeeButtonScreen[$implementations]=[
 __AbstractSwapeeButtonScreen,
 AbstractSwapeeButtonScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonScreen}*/({
  inputsPQs:SwapeeButtonInputsPQs,
  memoryQPs:SwapeeButtonMemoryQPs,
 }),
 Screen,
 AbstractSwapeeButtonScreenAR,
 AbstractSwapeeButtonScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonScreen}*/({
  vdusPQs:SwapeeButtonVdusPQs,
 }),
]