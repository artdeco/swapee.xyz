import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonControllerAR}
 */
function __AbstractSwapeeButtonControllerAR() {}
__AbstractSwapeeButtonControllerAR.prototype = /** @type {!_AbstractSwapeeButtonControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR}
 */
class _AbstractSwapeeButtonControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeButtonControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR} ‎
 */
class AbstractSwapeeButtonControllerAR extends newAbstract(
 _AbstractSwapeeButtonControllerAR,317913044522,null,{
  asISwapeeButtonControllerAR:1,
  superSwapeeButtonControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR} */
AbstractSwapeeButtonControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR} */
function AbstractSwapeeButtonControllerARClass(){}

export default AbstractSwapeeButtonControllerAR


AbstractSwapeeButtonControllerAR[$implementations]=[
 __AbstractSwapeeButtonControllerAR,
 AR,
 AbstractSwapeeButtonControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeButtonControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]