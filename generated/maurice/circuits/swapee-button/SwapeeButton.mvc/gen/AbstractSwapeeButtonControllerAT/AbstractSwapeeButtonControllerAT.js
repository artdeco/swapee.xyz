import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonControllerAT}
 */
function __AbstractSwapeeButtonControllerAT() {}
__AbstractSwapeeButtonControllerAT.prototype = /** @type {!_AbstractSwapeeButtonControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT}
 */
class _AbstractSwapeeButtonControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeButtonControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT} ‎
 */
class AbstractSwapeeButtonControllerAT extends newAbstract(
 _AbstractSwapeeButtonControllerAT,317913044523,null,{
  asISwapeeButtonControllerAT:1,
  superSwapeeButtonControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT} */
AbstractSwapeeButtonControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT} */
function AbstractSwapeeButtonControllerATClass(){}

export default AbstractSwapeeButtonControllerAT


AbstractSwapeeButtonControllerAT[$implementations]=[
 __AbstractSwapeeButtonControllerAT,
 UartUniversal,
 AbstractSwapeeButtonControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeButtonControllerAT}*/({
  get asISwapeeButtonController(){
   return this
  },
 }),
]