import AbstractSwapeeButtonGPU from '../AbstractSwapeeButtonGPU'
import AbstractSwapeeButtonScreenBack from '../AbstractSwapeeButtonScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {SwapeeButtonInputsQPs} from '../../pqs/SwapeeButtonInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeButton from '../AbstractSwapeeButton'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonHtmlComponent}
 */
function __AbstractSwapeeButtonHtmlComponent() {}
__AbstractSwapeeButtonHtmlComponent.prototype = /** @type {!_AbstractSwapeeButtonHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent}
 */
class _AbstractSwapeeButtonHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.SwapeeButtonHtmlComponent} */ (res)
  }
}
/**
 * The _ISwapeeButton_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent} ‎
 */
export class AbstractSwapeeButtonHtmlComponent extends newAbstract(
 _AbstractSwapeeButtonHtmlComponent,317913044511,null,{
  asISwapeeButtonHtmlComponent:1,
  superSwapeeButtonHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent} */
AbstractSwapeeButtonHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent} */
function AbstractSwapeeButtonHtmlComponentClass(){}


AbstractSwapeeButtonHtmlComponent[$implementations]=[
 __AbstractSwapeeButtonHtmlComponent,
 HtmlComponent,
 AbstractSwapeeButton,
 AbstractSwapeeButtonGPU,
 AbstractSwapeeButtonScreenBack,
 AbstractSwapeeButtonHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonHtmlComponent}*/({
  inputsQPs:SwapeeButtonInputsQPs,
 }),
]