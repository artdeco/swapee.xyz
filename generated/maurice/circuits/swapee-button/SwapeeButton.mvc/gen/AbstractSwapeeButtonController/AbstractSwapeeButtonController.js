import SwapeeButtonBuffer from '../SwapeeButtonBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {SwapeeButtonPortConnector} from '../SwapeeButtonPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonController}
 */
function __AbstractSwapeeButtonController() {}
__AbstractSwapeeButtonController.prototype = /** @type {!_AbstractSwapeeButtonController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonController}
 */
class _AbstractSwapeeButtonController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonController} ‎
 */
export class AbstractSwapeeButtonController extends newAbstract(
 _AbstractSwapeeButtonController,317913044519,null,{
  asISwapeeButtonController:1,
  superSwapeeButtonController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonController} */
AbstractSwapeeButtonController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonController} */
function AbstractSwapeeButtonControllerClass(){}


AbstractSwapeeButtonController[$implementations]=[
 AbstractSwapeeButtonControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.ISwapeeButtonPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractSwapeeButtonController,
 SwapeeButtonBuffer,
 IntegratedController,
 /**@type {!AbstractSwapeeButtonController}*/(SwapeeButtonPortConnector),
]


export default AbstractSwapeeButtonController