import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {SwapeeButtonInputsPQs} from '../../pqs/SwapeeButtonInputsPQs'
import {SwapeeButtonOuterCoreConstructor} from '../SwapeeButtonCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeButtonPort}
 */
function __SwapeeButtonPort() {}
__SwapeeButtonPort.prototype = /** @type {!_SwapeeButtonPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeButtonPort} */ function SwapeeButtonPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.SwapeeButtonOuterCore} */ ({model:null})
  SwapeeButtonOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonPort}
 */
class _SwapeeButtonPort { }
/**
 * The port that serves as an interface to the _ISwapeeButton_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonPort} ‎
 */
export class SwapeeButtonPort extends newAbstract(
 _SwapeeButtonPort,31791304455,SwapeeButtonPortConstructor,{
  asISwapeeButtonPort:1,
  superSwapeeButtonPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonPort} */
SwapeeButtonPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonPort} */
function SwapeeButtonPortClass(){}

export const SwapeeButtonPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ISwapeeButton.Pinout>}*/({
 get Port() { return SwapeeButtonPort },
})

SwapeeButtonPort[$implementations]=[
 SwapeeButtonPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonPort}*/({
  resetPort(){
   this.resetSwapeeButtonPort()
  },
  resetSwapeeButtonPort(){
   SwapeeButtonPortConstructor.call(this)
  },
 }),
 __SwapeeButtonPort,
 Parametric,
 SwapeeButtonPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonPort}*/({
  constructor(){
   mountPins(this.inputs,'',SwapeeButtonInputsPQs)
  },
 }),
]


export default SwapeeButtonPort