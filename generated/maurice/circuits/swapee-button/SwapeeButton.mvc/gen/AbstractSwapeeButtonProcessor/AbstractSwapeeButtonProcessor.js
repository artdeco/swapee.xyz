import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonProcessor}
 */
function __AbstractSwapeeButtonProcessor() {}
__AbstractSwapeeButtonProcessor.prototype = /** @type {!_AbstractSwapeeButtonProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonProcessor}
 */
class _AbstractSwapeeButtonProcessor { }
/**
 * The processor to compute changes to the memory for the _ISwapeeButton_.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonProcessor} ‎
 */
class AbstractSwapeeButtonProcessor extends newAbstract(
 _AbstractSwapeeButtonProcessor,31791304458,null,{
  asISwapeeButtonProcessor:1,
  superSwapeeButtonProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonProcessor} */
AbstractSwapeeButtonProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonProcessor} */
function AbstractSwapeeButtonProcessorClass(){}

export default AbstractSwapeeButtonProcessor


AbstractSwapeeButtonProcessor[$implementations]=[
 __AbstractSwapeeButtonProcessor,
]