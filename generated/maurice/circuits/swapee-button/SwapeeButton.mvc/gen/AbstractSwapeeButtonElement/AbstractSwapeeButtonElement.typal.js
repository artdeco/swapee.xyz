
import AbstractSwapeeButton from '../AbstractSwapeeButton'

/** @abstract {xyz.swapee.wc.ISwapeeButtonElement} */
export default class AbstractSwapeeButtonElement { }



AbstractSwapeeButtonElement[$implementations]=[AbstractSwapeeButton,
 /** @type {!AbstractSwapeeButtonElement} */ ({
  rootId:'SwapeeButton',
  __$id:3179130445,
  fqn:'xyz.swapee.wc.ISwapeeButton',
  maurice_element_v3:true,
 }),
]