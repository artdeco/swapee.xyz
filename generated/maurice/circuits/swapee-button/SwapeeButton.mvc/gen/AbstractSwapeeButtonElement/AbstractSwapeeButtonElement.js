import SwapeeButtonRenderVdus from './methods/render-vdus'
import SwapeeButtonElementPort from '../SwapeeButtonElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {SwapeeButtonInputsPQs} from '../../pqs/SwapeeButtonInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeButton from '../AbstractSwapeeButton'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonElement}
 */
function __AbstractSwapeeButtonElement() {}
__AbstractSwapeeButtonElement.prototype = /** @type {!_AbstractSwapeeButtonElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonElement}
 */
class _AbstractSwapeeButtonElement { }
/**
 * A component description.
 *
 * The _ISwapeeButton_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonElement} ‎
 */
class AbstractSwapeeButtonElement extends newAbstract(
 _AbstractSwapeeButtonElement,317913044512,null,{
  asISwapeeButtonElement:1,
  superSwapeeButtonElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonElement} */
AbstractSwapeeButtonElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonElement} */
function AbstractSwapeeButtonElementClass(){}

export default AbstractSwapeeButtonElement


AbstractSwapeeButtonElement[$implementations]=[
 __AbstractSwapeeButtonElement,
 ElementBase,
 AbstractSwapeeButtonElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':active':activeColAttr,
   ':hover':hoverColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'active':activeAttr,
    'hover':hoverAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(activeAttr===undefined?{'active':activeColAttr}:{}),
    ...(hoverAttr===undefined?{'hover':hoverColAttr}:{}),
   }
  },
 }),
 AbstractSwapeeButtonElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'active':activeAttr,
   'hover':hoverAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    active:activeAttr,
    hover:hoverAttr,
   }
  },
 }),
 AbstractSwapeeButtonElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractSwapeeButtonElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonElement}*/({
  render:SwapeeButtonRenderVdus,
 }),
 AbstractSwapeeButtonElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonElement}*/({
  customs:{
   'BORDER': '2px',
   'BACKGROUND': 'white',
   'COLOR': 'initial',
   'CLIP_PATH': '10px',
   'INNER_BORDER_COLOR': 'lightgrey',
  },
  inputsPQs:SwapeeButtonInputsPQs,
  vdus:{
   'BuLa': 'ie9d1',
  },
 }),
 IntegratedComponent,
 AbstractSwapeeButtonElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','active','hover','no-solder',':no-solder',':active',':hover','fe646','bd74d','c76a5','e0542','BORDER','BACKGROUND','COLOR','CLIP_PATH','INNER_BORDER_COLOR','children']),
   })
  },
  get Port(){
   return SwapeeButtonElementPort
  },
 }),
]



AbstractSwapeeButtonElement[$implementations]=[AbstractSwapeeButton,
 /** @type {!AbstractSwapeeButtonElement} */ ({
  rootId:'SwapeeButton',
  __$id:3179130445,
  fqn:'xyz.swapee.wc.ISwapeeButton',
  maurice_element_v3:true,
 }),
]