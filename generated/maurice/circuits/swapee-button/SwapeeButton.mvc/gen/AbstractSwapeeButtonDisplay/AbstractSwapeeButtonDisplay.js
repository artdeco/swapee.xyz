import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonDisplay}
 */
function __AbstractSwapeeButtonDisplay() {}
__AbstractSwapeeButtonDisplay.prototype = /** @type {!_AbstractSwapeeButtonDisplay} */ ({ })
/** @this {xyz.swapee.wc.SwapeeButtonDisplay} */ function SwapeeButtonDisplayConstructor() {
  /** @type {HTMLButtonElement} */ this.BuLa=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonDisplay}
 */
class _AbstractSwapeeButtonDisplay { }
/**
 * Display for presenting information from the _ISwapeeButton_.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonDisplay} ‎
 */
class AbstractSwapeeButtonDisplay extends newAbstract(
 _AbstractSwapeeButtonDisplay,317913044516,SwapeeButtonDisplayConstructor,{
  asISwapeeButtonDisplay:1,
  superSwapeeButtonDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonDisplay} */
AbstractSwapeeButtonDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonDisplay} */
function AbstractSwapeeButtonDisplayClass(){}

export default AbstractSwapeeButtonDisplay


AbstractSwapeeButtonDisplay[$implementations]=[
 __AbstractSwapeeButtonDisplay,
 Display,
 AbstractSwapeeButtonDisplayClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asISwapeeButtonScreen:{vdusPQs:{
    BuLa:BuLa,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    BuLa:/**@type {HTMLButtonElement}*/(children[BuLa]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.SwapeeButtonDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.ISwapeeButtonDisplay.Initialese}*/({
   BuLa:1,
  }),
  initializer({
   BuLa:_BuLa,
  }) {
   if(_BuLa!==undefined) this.BuLa=_BuLa
  },
 }),
]