import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonScreenAT}
 */
function __AbstractSwapeeButtonScreenAT() {}
__AbstractSwapeeButtonScreenAT.prototype = /** @type {!_AbstractSwapeeButtonScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT}
 */
class _AbstractSwapeeButtonScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeButtonScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT} ‎
 */
class AbstractSwapeeButtonScreenAT extends newAbstract(
 _AbstractSwapeeButtonScreenAT,317913044527,null,{
  asISwapeeButtonScreenAT:1,
  superSwapeeButtonScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT} */
AbstractSwapeeButtonScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT} */
function AbstractSwapeeButtonScreenATClass(){}

export default AbstractSwapeeButtonScreenAT


AbstractSwapeeButtonScreenAT[$implementations]=[
 __AbstractSwapeeButtonScreenAT,
 UartUniversal,
]