import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonScreenAR}
 */
function __AbstractSwapeeButtonScreenAR() {}
__AbstractSwapeeButtonScreenAR.prototype = /** @type {!_AbstractSwapeeButtonScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR}
 */
class _AbstractSwapeeButtonScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeButtonScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR} ‎
 */
class AbstractSwapeeButtonScreenAR extends newAbstract(
 _AbstractSwapeeButtonScreenAR,317913044526,null,{
  asISwapeeButtonScreenAR:1,
  superSwapeeButtonScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR} */
AbstractSwapeeButtonScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR} */
function AbstractSwapeeButtonScreenARClass(){}

export default AbstractSwapeeButtonScreenAR


AbstractSwapeeButtonScreenAR[$implementations]=[
 __AbstractSwapeeButtonScreenAR,
 AR,
 AbstractSwapeeButtonScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeButtonScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractSwapeeButtonScreenAR}