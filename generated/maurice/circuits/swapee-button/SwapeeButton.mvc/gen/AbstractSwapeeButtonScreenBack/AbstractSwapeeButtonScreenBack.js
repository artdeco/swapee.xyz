import AbstractSwapeeButtonScreenAT from '../AbstractSwapeeButtonScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonScreenBack}
 */
function __AbstractSwapeeButtonScreenBack() {}
__AbstractSwapeeButtonScreenBack.prototype = /** @type {!_AbstractSwapeeButtonScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonScreen}
 */
class _AbstractSwapeeButtonScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeButtonScreen} ‎
 */
class AbstractSwapeeButtonScreenBack extends newAbstract(
 _AbstractSwapeeButtonScreenBack,317913044525,null,{
  asISwapeeButtonScreen:1,
  superSwapeeButtonScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreen} */
AbstractSwapeeButtonScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreen} */
function AbstractSwapeeButtonScreenBackClass(){}

export default AbstractSwapeeButtonScreenBack


AbstractSwapeeButtonScreenBack[$implementations]=[
 __AbstractSwapeeButtonScreenBack,
 AbstractSwapeeButtonScreenAT,
]