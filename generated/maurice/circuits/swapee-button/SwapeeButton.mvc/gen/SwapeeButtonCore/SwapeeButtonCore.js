import {mountPins} from '@webcircuits/webcircuits'
import {SwapeeButtonMemoryPQs} from '../../pqs/SwapeeButtonMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeButtonCore}
 */
function __SwapeeButtonCore() {}
__SwapeeButtonCore.prototype = /** @type {!_SwapeeButtonCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonCore}
 */
class _SwapeeButtonCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonCore} ‎
 */
class SwapeeButtonCore extends newAbstract(
 _SwapeeButtonCore,31791304457,null,{
  asISwapeeButtonCore:1,
  superSwapeeButtonCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonCore} */
SwapeeButtonCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonCore} */
function SwapeeButtonCoreClass(){}

export default SwapeeButtonCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeButtonOuterCore}
 */
function __SwapeeButtonOuterCore() {}
__SwapeeButtonOuterCore.prototype = /** @type {!_SwapeeButtonOuterCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeButtonOuterCore} */
export function SwapeeButtonOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeButtonOuterCore.Model}*/
  this.model={
    active: false,
    hover: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonOuterCore}
 */
class _SwapeeButtonOuterCore { }
/**
 * The _ISwapeeButton_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonOuterCore} ‎
 */
export class SwapeeButtonOuterCore extends newAbstract(
 _SwapeeButtonOuterCore,31791304453,SwapeeButtonOuterCoreConstructor,{
  asISwapeeButtonOuterCore:1,
  superSwapeeButtonOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonOuterCore} */
SwapeeButtonOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonOuterCore} */
function SwapeeButtonOuterCoreClass(){}


SwapeeButtonOuterCore[$implementations]=[
 __SwapeeButtonOuterCore,
 SwapeeButtonOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonOuterCore}*/({
  constructor(){
   mountPins(this.model,'',SwapeeButtonMemoryPQs)

  },
 }),
]

SwapeeButtonCore[$implementations]=[
 SwapeeButtonCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonCore}*/({
  resetCore(){
   this.resetSwapeeButtonCore()
  },
  resetSwapeeButtonCore(){
   SwapeeButtonOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.SwapeeButtonOuterCore}*/(
     /**@type {!xyz.swapee.wc.ISwapeeButtonOuterCore}*/(this)),
   )
  },
 }),
 __SwapeeButtonCore,
 SwapeeButtonOuterCore,
]

export {SwapeeButtonCore}