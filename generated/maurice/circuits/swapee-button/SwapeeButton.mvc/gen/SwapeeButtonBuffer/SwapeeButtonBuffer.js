import {makeBuffers} from '@webcircuits/webcircuits'

export const SwapeeButtonBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  active:Boolean,
  hover:Boolean,
 }),
})

export default SwapeeButtonBuffer