import AbstractSwapeeButtonDisplay from '../AbstractSwapeeButtonDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {SwapeeButtonVdusPQs} from '../../pqs/SwapeeButtonVdusPQs'
import {SwapeeButtonVdusQPs} from '../../pqs/SwapeeButtonVdusQPs'
import {SwapeeButtonMemoryPQs} from '../../pqs/SwapeeButtonMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeButtonGPU}
 */
function __AbstractSwapeeButtonGPU() {}
__AbstractSwapeeButtonGPU.prototype = /** @type {!_AbstractSwapeeButtonGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonGPU}
 */
class _AbstractSwapeeButtonGPU { }
/**
 * Handles the periphery of the _ISwapeeButtonDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonGPU} ‎
 */
class AbstractSwapeeButtonGPU extends newAbstract(
 _AbstractSwapeeButtonGPU,317913044515,null,{
  asISwapeeButtonGPU:1,
  superSwapeeButtonGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonGPU} */
AbstractSwapeeButtonGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonGPU} */
function AbstractSwapeeButtonGPUClass(){}

export default AbstractSwapeeButtonGPU


AbstractSwapeeButtonGPU[$implementations]=[
 __AbstractSwapeeButtonGPU,
 AbstractSwapeeButtonGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeButtonGPU}*/({
  vdusPQs:SwapeeButtonVdusPQs,
  vdusQPs:SwapeeButtonVdusQPs,
  memoryPQs:SwapeeButtonMemoryPQs,
 }),
 AbstractSwapeeButtonDisplay,
 BrowserView,
]