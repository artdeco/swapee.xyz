/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ISwapeeButtonComputer={}
xyz.swapee.wc.ISwapeeButtonOuterCore={}
xyz.swapee.wc.ISwapeeButtonOuterCore.Model={}
xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Active={}
xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Hover={}
xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel={}
xyz.swapee.wc.ISwapeeButtonPort={}
xyz.swapee.wc.ISwapeeButtonPort.Inputs={}
xyz.swapee.wc.ISwapeeButtonPort.WeakInputs={}
xyz.swapee.wc.ISwapeeButtonCore={}
xyz.swapee.wc.ISwapeeButtonCore.Model={}
xyz.swapee.wc.ISwapeeButtonPortInterface={}
xyz.swapee.wc.ISwapeeButtonProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ISwapeeButtonController={}
xyz.swapee.wc.front.ISwapeeButtonControllerAT={}
xyz.swapee.wc.front.ISwapeeButtonScreenAR={}
xyz.swapee.wc.ISwapeeButton={}
xyz.swapee.wc.ISwapeeButtonHtmlComponent={}
xyz.swapee.wc.ISwapeeButtonElement={}
xyz.swapee.wc.ISwapeeButtonElementPort={}
xyz.swapee.wc.ISwapeeButtonElementPort.Inputs={}
xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts={}
xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs={}
xyz.swapee.wc.ISwapeeButtonDesigner={}
xyz.swapee.wc.ISwapeeButtonDesigner.communicator={}
xyz.swapee.wc.ISwapeeButtonDesigner.relay={}
xyz.swapee.wc.ISwapeeButtonDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ISwapeeButtonDisplay={}
xyz.swapee.wc.back.ISwapeeButtonController={}
xyz.swapee.wc.back.ISwapeeButtonControllerAR={}
xyz.swapee.wc.back.ISwapeeButtonScreen={}
xyz.swapee.wc.back.ISwapeeButtonScreenAT={}
xyz.swapee.wc.ISwapeeButtonController={}
xyz.swapee.wc.ISwapeeButtonScreen={}
xyz.swapee.wc.ISwapeeButtonGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/02-ISwapeeButtonComputer.xml}  efc7fdb46454d36cb7b6b227940e1974 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ISwapeeButtonComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonComputer)} xyz.swapee.wc.AbstractSwapeeButtonComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonComputer} xyz.swapee.wc.SwapeeButtonComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonComputer` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonComputer
 */
xyz.swapee.wc.AbstractSwapeeButtonComputer = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonComputer.constructor&xyz.swapee.wc.SwapeeButtonComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonComputer.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonComputer.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonComputer}
 */
xyz.swapee.wc.AbstractSwapeeButtonComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonComputer}
 */
xyz.swapee.wc.AbstractSwapeeButtonComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonComputer}
 */
xyz.swapee.wc.AbstractSwapeeButtonComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonComputer}
 */
xyz.swapee.wc.AbstractSwapeeButtonComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonComputer.Initialese[]) => xyz.swapee.wc.ISwapeeButtonComputer} xyz.swapee.wc.SwapeeButtonComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.SwapeeButtonMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.ISwapeeButtonComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ISwapeeButtonComputer
 */
xyz.swapee.wc.ISwapeeButtonComputer = class extends /** @type {xyz.swapee.wc.ISwapeeButtonComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeButtonComputer.compute} */
xyz.swapee.wc.ISwapeeButtonComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonComputer.Initialese>)} xyz.swapee.wc.SwapeeButtonComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonComputer} xyz.swapee.wc.ISwapeeButtonComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeButtonComputer_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonComputer
 * @implements {xyz.swapee.wc.ISwapeeButtonComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonComputer.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonComputer = class extends /** @type {xyz.swapee.wc.SwapeeButtonComputer.constructor&xyz.swapee.wc.ISwapeeButtonComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonComputer}
 */
xyz.swapee.wc.SwapeeButtonComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeButtonComputer} */
xyz.swapee.wc.RecordISwapeeButtonComputer

/** @typedef {xyz.swapee.wc.ISwapeeButtonComputer} xyz.swapee.wc.BoundISwapeeButtonComputer */

/** @typedef {xyz.swapee.wc.SwapeeButtonComputer} xyz.swapee.wc.BoundSwapeeButtonComputer */

/**
 * Contains getters to cast the _ISwapeeButtonComputer_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonComputerCaster
 */
xyz.swapee.wc.ISwapeeButtonComputerCaster = class { }
/**
 * Cast the _ISwapeeButtonComputer_ instance into the _BoundISwapeeButtonComputer_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonComputer}
 */
xyz.swapee.wc.ISwapeeButtonComputerCaster.prototype.asISwapeeButtonComputer
/**
 * Access the _SwapeeButtonComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonComputer}
 */
xyz.swapee.wc.ISwapeeButtonComputerCaster.prototype.superSwapeeButtonComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.SwapeeButtonMemory) => void} xyz.swapee.wc.ISwapeeButtonComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonComputer.__compute<!xyz.swapee.wc.ISwapeeButtonComputer>} xyz.swapee.wc.ISwapeeButtonComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.SwapeeButtonMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeButtonComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeButtonComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/03-ISwapeeButtonOuterCore.xml}  d89782b309b1b9a918d1cde04c4f60e7 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeButtonOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonOuterCore)} xyz.swapee.wc.AbstractSwapeeButtonOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonOuterCore} xyz.swapee.wc.SwapeeButtonOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonOuterCore
 */
xyz.swapee.wc.AbstractSwapeeButtonOuterCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonOuterCore.constructor&xyz.swapee.wc.SwapeeButtonOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonOuterCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ISwapeeButtonOuterCore|typeof xyz.swapee.wc.SwapeeButtonOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ISwapeeButtonOuterCore|typeof xyz.swapee.wc.SwapeeButtonOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ISwapeeButtonOuterCore|typeof xyz.swapee.wc.SwapeeButtonOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonOuterCoreCaster)} xyz.swapee.wc.ISwapeeButtonOuterCore.constructor */
/**
 * The _ISwapeeButton_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ISwapeeButtonOuterCore
 */
xyz.swapee.wc.ISwapeeButtonOuterCore = class extends /** @type {xyz.swapee.wc.ISwapeeButtonOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeButtonOuterCore.prototype.constructor = xyz.swapee.wc.ISwapeeButtonOuterCore

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonOuterCore.Initialese>)} xyz.swapee.wc.SwapeeButtonOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonOuterCore} xyz.swapee.wc.ISwapeeButtonOuterCore.typeof */
/**
 * A concrete class of _ISwapeeButtonOuterCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonOuterCore
 * @implements {xyz.swapee.wc.ISwapeeButtonOuterCore} The _ISwapeeButton_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonOuterCore = class extends /** @type {xyz.swapee.wc.SwapeeButtonOuterCore.constructor&xyz.swapee.wc.ISwapeeButtonOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeButtonOuterCore.prototype.constructor = xyz.swapee.wc.SwapeeButtonOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonOuterCore}
 */
xyz.swapee.wc.SwapeeButtonOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonOuterCore.
 * @interface xyz.swapee.wc.ISwapeeButtonOuterCoreFields
 */
xyz.swapee.wc.ISwapeeButtonOuterCoreFields = class { }
/**
 * The _ISwapeeButton_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ISwapeeButtonOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeButtonOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore} */
xyz.swapee.wc.RecordISwapeeButtonOuterCore

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore} xyz.swapee.wc.BoundISwapeeButtonOuterCore */

/** @typedef {xyz.swapee.wc.SwapeeButtonOuterCore} xyz.swapee.wc.BoundSwapeeButtonOuterCore */

/**
 * Whether the button is in the active state.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Active.active

/**
 * Whether the button is in the hover state.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Hover.hover

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Active&xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Hover} xyz.swapee.wc.ISwapeeButtonOuterCore.Model The _ISwapeeButton_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Active&xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Hover} xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel The _ISwapeeButton_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ISwapeeButtonOuterCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonOuterCoreCaster
 */
xyz.swapee.wc.ISwapeeButtonOuterCoreCaster = class { }
/**
 * Cast the _ISwapeeButtonOuterCore_ instance into the _BoundISwapeeButtonOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonOuterCore}
 */
xyz.swapee.wc.ISwapeeButtonOuterCoreCaster.prototype.asISwapeeButtonOuterCore
/**
 * Access the _SwapeeButtonOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonOuterCore}
 */
xyz.swapee.wc.ISwapeeButtonOuterCoreCaster.prototype.superSwapeeButtonOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Active Whether the button is in the active state (optional overlay).
 * @prop {boolean} [active=false] Whether the button is in the active state. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Active_Safe Whether the button is in the active state (required overlay).
 * @prop {boolean} active Whether the button is in the active state.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Hover Whether the button is in the hover state (optional overlay).
 * @prop {boolean} [hover=false] Whether the button is in the hover state. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Hover_Safe Whether the button is in the hover state (required overlay).
 * @prop {boolean} hover Whether the button is in the hover state.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Active Whether the button is in the active state (optional overlay).
 * @prop {*} [active=null] Whether the button is in the active state. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Active_Safe Whether the button is in the active state (required overlay).
 * @prop {*} active Whether the button is in the active state.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Hover Whether the button is in the hover state (optional overlay).
 * @prop {*} [hover=null] Whether the button is in the hover state. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Hover_Safe Whether the button is in the hover state (required overlay).
 * @prop {*} hover Whether the button is in the hover state.
 */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Active} xyz.swapee.wc.ISwapeeButtonPort.Inputs.Active Whether the button is in the active state (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Active_Safe} xyz.swapee.wc.ISwapeeButtonPort.Inputs.Active_Safe Whether the button is in the active state (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Hover} xyz.swapee.wc.ISwapeeButtonPort.Inputs.Hover Whether the button is in the hover state (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Hover_Safe} xyz.swapee.wc.ISwapeeButtonPort.Inputs.Hover_Safe Whether the button is in the hover state (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Active} xyz.swapee.wc.ISwapeeButtonPort.WeakInputs.Active Whether the button is in the active state (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Active_Safe} xyz.swapee.wc.ISwapeeButtonPort.WeakInputs.Active_Safe Whether the button is in the active state (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Hover} xyz.swapee.wc.ISwapeeButtonPort.WeakInputs.Hover Whether the button is in the hover state (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.Hover_Safe} xyz.swapee.wc.ISwapeeButtonPort.WeakInputs.Hover_Safe Whether the button is in the hover state (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Active} xyz.swapee.wc.ISwapeeButtonCore.Model.Active Whether the button is in the active state (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Active_Safe} xyz.swapee.wc.ISwapeeButtonCore.Model.Active_Safe Whether the button is in the active state (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Hover} xyz.swapee.wc.ISwapeeButtonCore.Model.Hover Whether the button is in the hover state (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.Model.Hover_Safe} xyz.swapee.wc.ISwapeeButtonCore.Model.Hover_Safe Whether the button is in the hover state (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/04-ISwapeeButtonPort.xml}  5916e62eb0114cf2f5919f119328075a */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeButtonPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonPort)} xyz.swapee.wc.AbstractSwapeeButtonPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonPort} xyz.swapee.wc.SwapeeButtonPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonPort
 */
xyz.swapee.wc.AbstractSwapeeButtonPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonPort.constructor&xyz.swapee.wc.SwapeeButtonPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonPort|typeof xyz.swapee.wc.SwapeeButtonPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonPort|typeof xyz.swapee.wc.SwapeeButtonPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonPort|typeof xyz.swapee.wc.SwapeeButtonPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonPort.Initialese[]) => xyz.swapee.wc.ISwapeeButtonPort} xyz.swapee.wc.SwapeeButtonPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeButtonPort.Inputs>)} xyz.swapee.wc.ISwapeeButtonPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ISwapeeButton_, providing input
 * pins.
 * @interface xyz.swapee.wc.ISwapeeButtonPort
 */
xyz.swapee.wc.ISwapeeButtonPort = class extends /** @type {xyz.swapee.wc.ISwapeeButtonPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeButtonPort.resetPort} */
xyz.swapee.wc.ISwapeeButtonPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeButtonPort.resetSwapeeButtonPort} */
xyz.swapee.wc.ISwapeeButtonPort.prototype.resetSwapeeButtonPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonPort.Initialese>)} xyz.swapee.wc.SwapeeButtonPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonPort} xyz.swapee.wc.ISwapeeButtonPort.typeof */
/**
 * A concrete class of _ISwapeeButtonPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonPort
 * @implements {xyz.swapee.wc.ISwapeeButtonPort} The port that serves as an interface to the _ISwapeeButton_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonPort = class extends /** @type {xyz.swapee.wc.SwapeeButtonPort.constructor&xyz.swapee.wc.ISwapeeButtonPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonPort}
 */
xyz.swapee.wc.SwapeeButtonPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonPort.
 * @interface xyz.swapee.wc.ISwapeeButtonPortFields
 */
xyz.swapee.wc.ISwapeeButtonPortFields = class { }
/**
 * The inputs to the _ISwapeeButton_'s controller via its port.
 */
xyz.swapee.wc.ISwapeeButtonPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeButtonPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeButtonPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeButtonPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonPort} */
xyz.swapee.wc.RecordISwapeeButtonPort

/** @typedef {xyz.swapee.wc.ISwapeeButtonPort} xyz.swapee.wc.BoundISwapeeButtonPort */

/** @typedef {xyz.swapee.wc.SwapeeButtonPort} xyz.swapee.wc.BoundSwapeeButtonPort */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeButtonPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel} xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ISwapeeButton_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeButtonPort.Inputs
 */
xyz.swapee.wc.ISwapeeButtonPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeButtonPort.Inputs.constructor&xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeButtonPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeButtonPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeButtonPort.WeakInputs.constructor */
/**
 * The inputs to the _ISwapeeButton_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeButtonPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeButtonPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeButtonPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeButtonPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeButtonPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ISwapeeButtonPortInterface
 */
xyz.swapee.wc.ISwapeeButtonPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ISwapeeButtonPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeButtonPortInterface.prototype.constructor = xyz.swapee.wc.ISwapeeButtonPortInterface

/**
 * A concrete class of _ISwapeeButtonPortInterface_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonPortInterface
 * @implements {xyz.swapee.wc.ISwapeeButtonPortInterface} The port interface.
 */
xyz.swapee.wc.SwapeeButtonPortInterface = class extends xyz.swapee.wc.ISwapeeButtonPortInterface { }
xyz.swapee.wc.SwapeeButtonPortInterface.prototype.constructor = xyz.swapee.wc.SwapeeButtonPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonPortInterface.Props
 * @prop {boolean} active Whether the button is in the active state.
 * @prop {boolean} hover Whether the button is in the hover state.
 */

/**
 * Contains getters to cast the _ISwapeeButtonPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonPortCaster
 */
xyz.swapee.wc.ISwapeeButtonPortCaster = class { }
/**
 * Cast the _ISwapeeButtonPort_ instance into the _BoundISwapeeButtonPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonPort}
 */
xyz.swapee.wc.ISwapeeButtonPortCaster.prototype.asISwapeeButtonPort
/**
 * Access the _SwapeeButtonPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonPort}
 */
xyz.swapee.wc.ISwapeeButtonPortCaster.prototype.superSwapeeButtonPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeButtonPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonPort.__resetPort<!xyz.swapee.wc.ISwapeeButtonPort>} xyz.swapee.wc.ISwapeeButtonPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonPort.resetPort} */
/**
 * Resets the _ISwapeeButton_ port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeButtonPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeButtonPort.__resetSwapeeButtonPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonPort.__resetSwapeeButtonPort<!xyz.swapee.wc.ISwapeeButtonPort>} xyz.swapee.wc.ISwapeeButtonPort._resetSwapeeButtonPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonPort.resetSwapeeButtonPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeButtonPort.resetSwapeeButtonPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeButtonPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/09-ISwapeeButtonCore.xml}  fac27b0e81d0fb4de168fc63b67bc3e9 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeButtonCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonCore)} xyz.swapee.wc.AbstractSwapeeButtonCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonCore} xyz.swapee.wc.SwapeeButtonCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonCore
 */
xyz.swapee.wc.AbstractSwapeeButtonCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonCore.constructor&xyz.swapee.wc.SwapeeButtonCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonCore|typeof xyz.swapee.wc.SwapeeButtonCore)|(!xyz.swapee.wc.ISwapeeButtonOuterCore|typeof xyz.swapee.wc.SwapeeButtonOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonCore|typeof xyz.swapee.wc.SwapeeButtonCore)|(!xyz.swapee.wc.ISwapeeButtonOuterCore|typeof xyz.swapee.wc.SwapeeButtonOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonCore|typeof xyz.swapee.wc.SwapeeButtonCore)|(!xyz.swapee.wc.ISwapeeButtonOuterCore|typeof xyz.swapee.wc.SwapeeButtonOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonCore}
 */
xyz.swapee.wc.AbstractSwapeeButtonCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonCoreCaster&xyz.swapee.wc.ISwapeeButtonOuterCore)} xyz.swapee.wc.ISwapeeButtonCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ISwapeeButtonCore
 */
xyz.swapee.wc.ISwapeeButtonCore = class extends /** @type {xyz.swapee.wc.ISwapeeButtonCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeButtonOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeButtonCore.resetCore} */
xyz.swapee.wc.ISwapeeButtonCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ISwapeeButtonCore.resetSwapeeButtonCore} */
xyz.swapee.wc.ISwapeeButtonCore.prototype.resetSwapeeButtonCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonCore.Initialese>)} xyz.swapee.wc.SwapeeButtonCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonCore} xyz.swapee.wc.ISwapeeButtonCore.typeof */
/**
 * A concrete class of _ISwapeeButtonCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonCore
 * @implements {xyz.swapee.wc.ISwapeeButtonCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonCore = class extends /** @type {xyz.swapee.wc.SwapeeButtonCore.constructor&xyz.swapee.wc.ISwapeeButtonCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeButtonCore.prototype.constructor = xyz.swapee.wc.SwapeeButtonCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonCore}
 */
xyz.swapee.wc.SwapeeButtonCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonCore.
 * @interface xyz.swapee.wc.ISwapeeButtonCoreFields
 */
xyz.swapee.wc.ISwapeeButtonCoreFields = class { }
/**
 * The _ISwapeeButton_'s memory.
 */
xyz.swapee.wc.ISwapeeButtonCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeButtonCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ISwapeeButtonCoreFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeButtonCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonCore} */
xyz.swapee.wc.RecordISwapeeButtonCore

/** @typedef {xyz.swapee.wc.ISwapeeButtonCore} xyz.swapee.wc.BoundISwapeeButtonCore */

/** @typedef {xyz.swapee.wc.SwapeeButtonCore} xyz.swapee.wc.BoundSwapeeButtonCore */

/** @typedef {xyz.swapee.wc.ISwapeeButtonOuterCore.Model} xyz.swapee.wc.ISwapeeButtonCore.Model The _ISwapeeButton_'s memory. */

/**
 * Contains getters to cast the _ISwapeeButtonCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonCoreCaster
 */
xyz.swapee.wc.ISwapeeButtonCoreCaster = class { }
/**
 * Cast the _ISwapeeButtonCore_ instance into the _BoundISwapeeButtonCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonCore}
 */
xyz.swapee.wc.ISwapeeButtonCoreCaster.prototype.asISwapeeButtonCore
/**
 * Access the _SwapeeButtonCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonCore}
 */
xyz.swapee.wc.ISwapeeButtonCoreCaster.prototype.superSwapeeButtonCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeButtonCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonCore.__resetCore<!xyz.swapee.wc.ISwapeeButtonCore>} xyz.swapee.wc.ISwapeeButtonCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonCore.resetCore} */
/**
 * Resets the _ISwapeeButton_ core.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeButtonCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeButtonCore.__resetSwapeeButtonCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonCore.__resetSwapeeButtonCore<!xyz.swapee.wc.ISwapeeButtonCore>} xyz.swapee.wc.ISwapeeButtonCore._resetSwapeeButtonCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonCore.resetSwapeeButtonCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeButtonCore.resetSwapeeButtonCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeButtonCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/10-ISwapeeButtonProcessor.xml}  b2bd1fcf2e1d09a9b32bd7a3719c568c */
/** @typedef {xyz.swapee.wc.ISwapeeButtonComputer.Initialese&xyz.swapee.wc.ISwapeeButtonController.Initialese} xyz.swapee.wc.ISwapeeButtonProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonProcessor)} xyz.swapee.wc.AbstractSwapeeButtonProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonProcessor} xyz.swapee.wc.SwapeeButtonProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonProcessor
 */
xyz.swapee.wc.AbstractSwapeeButtonProcessor = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonProcessor.constructor&xyz.swapee.wc.SwapeeButtonProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonProcessor.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!xyz.swapee.wc.ISwapeeButtonCore|typeof xyz.swapee.wc.SwapeeButtonCore)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonProcessor}
 */
xyz.swapee.wc.AbstractSwapeeButtonProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonProcessor}
 */
xyz.swapee.wc.AbstractSwapeeButtonProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!xyz.swapee.wc.ISwapeeButtonCore|typeof xyz.swapee.wc.SwapeeButtonCore)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonProcessor}
 */
xyz.swapee.wc.AbstractSwapeeButtonProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!xyz.swapee.wc.ISwapeeButtonCore|typeof xyz.swapee.wc.SwapeeButtonCore)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonProcessor}
 */
xyz.swapee.wc.AbstractSwapeeButtonProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonProcessor.Initialese[]) => xyz.swapee.wc.ISwapeeButtonProcessor} xyz.swapee.wc.SwapeeButtonProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonProcessorCaster&xyz.swapee.wc.ISwapeeButtonComputer&xyz.swapee.wc.ISwapeeButtonCore&xyz.swapee.wc.ISwapeeButtonController)} xyz.swapee.wc.ISwapeeButtonProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonController} xyz.swapee.wc.ISwapeeButtonController.typeof */
/**
 * The processor to compute changes to the memory for the _ISwapeeButton_.
 * @interface xyz.swapee.wc.ISwapeeButtonProcessor
 */
xyz.swapee.wc.ISwapeeButtonProcessor = class extends /** @type {xyz.swapee.wc.ISwapeeButtonProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeButtonComputer.typeof&xyz.swapee.wc.ISwapeeButtonCore.typeof&xyz.swapee.wc.ISwapeeButtonController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonProcessor.Initialese>)} xyz.swapee.wc.SwapeeButtonProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonProcessor} xyz.swapee.wc.ISwapeeButtonProcessor.typeof */
/**
 * A concrete class of _ISwapeeButtonProcessor_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonProcessor
 * @implements {xyz.swapee.wc.ISwapeeButtonProcessor} The processor to compute changes to the memory for the _ISwapeeButton_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonProcessor.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonProcessor = class extends /** @type {xyz.swapee.wc.SwapeeButtonProcessor.constructor&xyz.swapee.wc.ISwapeeButtonProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonProcessor}
 */
xyz.swapee.wc.SwapeeButtonProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeButtonProcessor} */
xyz.swapee.wc.RecordISwapeeButtonProcessor

/** @typedef {xyz.swapee.wc.ISwapeeButtonProcessor} xyz.swapee.wc.BoundISwapeeButtonProcessor */

/** @typedef {xyz.swapee.wc.SwapeeButtonProcessor} xyz.swapee.wc.BoundSwapeeButtonProcessor */

/**
 * Contains getters to cast the _ISwapeeButtonProcessor_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonProcessorCaster
 */
xyz.swapee.wc.ISwapeeButtonProcessorCaster = class { }
/**
 * Cast the _ISwapeeButtonProcessor_ instance into the _BoundISwapeeButtonProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonProcessor}
 */
xyz.swapee.wc.ISwapeeButtonProcessorCaster.prototype.asISwapeeButtonProcessor
/**
 * Access the _SwapeeButtonProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonProcessor}
 */
xyz.swapee.wc.ISwapeeButtonProcessorCaster.prototype.superSwapeeButtonProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/100-SwapeeButtonMemory.xml}  c6a47daf06c05529752108b6d29e4dd4 */
/**
 * The memory of the _ISwapeeButton_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.SwapeeButtonMemory
 */
xyz.swapee.wc.SwapeeButtonMemory = class { }
/**
 * Whether the button is in the active state. Default `false`.
 */
xyz.swapee.wc.SwapeeButtonMemory.prototype.active = /** @type {boolean} */ (void 0)
/**
 * Whether the button is in the hover state. Default `false`.
 */
xyz.swapee.wc.SwapeeButtonMemory.prototype.hover = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/102-SwapeeButtonInputs.xml}  00aba3376ac80257432835a6b9a53f3c */
/**
 * The inputs of the _ISwapeeButton_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.SwapeeButtonInputs
 */
xyz.swapee.wc.front.SwapeeButtonInputs = class { }
/**
 * Whether the button is in the active state. Default `false`.
 */
xyz.swapee.wc.front.SwapeeButtonInputs.prototype.active = /** @type {boolean|undefined} */ (void 0)
/**
 * Whether the button is in the hover state. Default `false`.
 */
xyz.swapee.wc.front.SwapeeButtonInputs.prototype.hover = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/11-ISwapeeButton.xml}  debb8816ada965b36f452680d5fbfd01 */
/**
 * An atomic wrapper for the _ISwapeeButton_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.SwapeeButtonEnv
 */
xyz.swapee.wc.SwapeeButtonEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.SwapeeButtonEnv.prototype.swapeeButton = /** @type {xyz.swapee.wc.ISwapeeButton} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.ISwapeeButtonController.Inputs>&xyz.swapee.wc.ISwapeeButtonProcessor.Initialese&xyz.swapee.wc.ISwapeeButtonComputer.Initialese&xyz.swapee.wc.ISwapeeButtonController.Initialese} xyz.swapee.wc.ISwapeeButton.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButton)} xyz.swapee.wc.AbstractSwapeeButton.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButton} xyz.swapee.wc.SwapeeButton.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButton` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButton
 */
xyz.swapee.wc.AbstractSwapeeButton = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButton.constructor&xyz.swapee.wc.SwapeeButton.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButton.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButton
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButton.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButton} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButton|typeof xyz.swapee.wc.SwapeeButton)|(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButton}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButton.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButton}
 */
xyz.swapee.wc.AbstractSwapeeButton.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButton}
 */
xyz.swapee.wc.AbstractSwapeeButton.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButton|typeof xyz.swapee.wc.SwapeeButton)|(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButton}
 */
xyz.swapee.wc.AbstractSwapeeButton.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButton|typeof xyz.swapee.wc.SwapeeButton)|(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButton}
 */
xyz.swapee.wc.AbstractSwapeeButton.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButton.Initialese[]) => xyz.swapee.wc.ISwapeeButton} xyz.swapee.wc.SwapeeButtonConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButton.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ISwapeeButton.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ISwapeeButton.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ISwapeeButton.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.SwapeeButtonMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.SwapeeButtonClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonCaster&xyz.swapee.wc.ISwapeeButtonProcessor&xyz.swapee.wc.ISwapeeButtonComputer&xyz.swapee.wc.ISwapeeButtonController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.ISwapeeButtonController.Inputs, null>)} xyz.swapee.wc.ISwapeeButton.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ISwapeeButton
 */
xyz.swapee.wc.ISwapeeButton = class extends /** @type {xyz.swapee.wc.ISwapeeButton.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeButtonProcessor.typeof&xyz.swapee.wc.ISwapeeButtonComputer.typeof&xyz.swapee.wc.ISwapeeButtonController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButton* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButton.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButton.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButton&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButton.Initialese>)} xyz.swapee.wc.SwapeeButton.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButton} xyz.swapee.wc.ISwapeeButton.typeof */
/**
 * A concrete class of _ISwapeeButton_ instances.
 * @constructor xyz.swapee.wc.SwapeeButton
 * @implements {xyz.swapee.wc.ISwapeeButton} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButton.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButton = class extends /** @type {xyz.swapee.wc.SwapeeButton.constructor&xyz.swapee.wc.ISwapeeButton.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButton* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButton.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButton* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButton.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButton.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButton}
 */
xyz.swapee.wc.SwapeeButton.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButton.
 * @interface xyz.swapee.wc.ISwapeeButtonFields
 */
xyz.swapee.wc.ISwapeeButtonFields = class { }
/**
 * The input pins of the _ISwapeeButton_ port.
 */
xyz.swapee.wc.ISwapeeButtonFields.prototype.pinout = /** @type {!xyz.swapee.wc.ISwapeeButton.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButton} */
xyz.swapee.wc.RecordISwapeeButton

/** @typedef {xyz.swapee.wc.ISwapeeButton} xyz.swapee.wc.BoundISwapeeButton */

/** @typedef {xyz.swapee.wc.SwapeeButton} xyz.swapee.wc.BoundSwapeeButton */

/** @typedef {xyz.swapee.wc.ISwapeeButtonController.Inputs} xyz.swapee.wc.ISwapeeButton.Pinout The input pins of the _ISwapeeButton_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeButtonController.Inputs>)} xyz.swapee.wc.ISwapeeButtonBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ISwapeeButtonBuffer
 */
xyz.swapee.wc.ISwapeeButtonBuffer = class extends /** @type {xyz.swapee.wc.ISwapeeButtonBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeButtonBuffer.prototype.constructor = xyz.swapee.wc.ISwapeeButtonBuffer

/**
 * A concrete class of _ISwapeeButtonBuffer_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonBuffer
 * @implements {xyz.swapee.wc.ISwapeeButtonBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.SwapeeButtonBuffer = class extends xyz.swapee.wc.ISwapeeButtonBuffer { }
xyz.swapee.wc.SwapeeButtonBuffer.prototype.constructor = xyz.swapee.wc.SwapeeButtonBuffer

/**
 * Contains getters to cast the _ISwapeeButton_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonCaster
 */
xyz.swapee.wc.ISwapeeButtonCaster = class { }
/**
 * Cast the _ISwapeeButton_ instance into the _BoundISwapeeButton_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButton}
 */
xyz.swapee.wc.ISwapeeButtonCaster.prototype.asISwapeeButton
/**
 * Access the _SwapeeButton_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButton}
 */
xyz.swapee.wc.ISwapeeButtonCaster.prototype.superSwapeeButton

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/110-SwapeeButtonSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeButtonMemoryPQs
 */
xyz.swapee.wc.SwapeeButtonMemoryPQs = class {
  constructor() {
    /**
     * `c76a5`
     */
    this.active=/** @type {string} */ (void 0)
    /**
     * `e0542`
     */
    this.hover=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeButtonMemoryPQs.prototype.constructor = xyz.swapee.wc.SwapeeButtonMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeButtonMemoryQPs
 * @dict
 */
xyz.swapee.wc.SwapeeButtonMemoryQPs = class { }
/**
 * `active`
 */
xyz.swapee.wc.SwapeeButtonMemoryQPs.prototype.c76a5 = /** @type {string} */ (void 0)
/**
 * `hover`
 */
xyz.swapee.wc.SwapeeButtonMemoryQPs.prototype.e0542 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonMemoryPQs)} xyz.swapee.wc.SwapeeButtonInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonMemoryPQs} xyz.swapee.wc.SwapeeButtonMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeButtonInputsPQs
 */
xyz.swapee.wc.SwapeeButtonInputsPQs = class extends /** @type {xyz.swapee.wc.SwapeeButtonInputsPQs.constructor&xyz.swapee.wc.SwapeeButtonMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeButtonInputsPQs.prototype.constructor = xyz.swapee.wc.SwapeeButtonInputsPQs

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonMemoryPQs)} xyz.swapee.wc.SwapeeButtonInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeButtonInputsQPs
 * @dict
 */
xyz.swapee.wc.SwapeeButtonInputsQPs = class extends /** @type {xyz.swapee.wc.SwapeeButtonInputsQPs.constructor&xyz.swapee.wc.SwapeeButtonMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeButtonInputsQPs.prototype.constructor = xyz.swapee.wc.SwapeeButtonInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeButtonVdusPQs
 */
xyz.swapee.wc.SwapeeButtonVdusPQs = class { }
xyz.swapee.wc.SwapeeButtonVdusPQs.prototype.constructor = xyz.swapee.wc.SwapeeButtonVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeButtonVdusQPs
 * @dict
 */
xyz.swapee.wc.SwapeeButtonVdusQPs = class { }
xyz.swapee.wc.SwapeeButtonVdusQPs.prototype.constructor = xyz.swapee.wc.SwapeeButtonVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/12-ISwapeeButtonHtmlComponent.xml}  a643c7019c4c9930a35dccfa00a6cebb */
/** @typedef {xyz.swapee.wc.back.ISwapeeButtonController.Initialese&xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese&xyz.swapee.wc.ISwapeeButton.Initialese&xyz.swapee.wc.ISwapeeButtonGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ISwapeeButtonProcessor.Initialese&xyz.swapee.wc.ISwapeeButtonComputer.Initialese} xyz.swapee.wc.ISwapeeButtonHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonHtmlComponent)} xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonHtmlComponent} xyz.swapee.wc.SwapeeButtonHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent
 */
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.constructor&xyz.swapee.wc.SwapeeButtonHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonHtmlComponent|typeof xyz.swapee.wc.SwapeeButtonHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeButtonController|typeof xyz.swapee.wc.back.SwapeeButtonController)|(!xyz.swapee.wc.back.ISwapeeButtonScreen|typeof xyz.swapee.wc.back.SwapeeButtonScreen)|(!xyz.swapee.wc.ISwapeeButton|typeof xyz.swapee.wc.SwapeeButton)|(!xyz.swapee.wc.ISwapeeButtonGPU|typeof xyz.swapee.wc.SwapeeButtonGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonHtmlComponent|typeof xyz.swapee.wc.SwapeeButtonHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeButtonController|typeof xyz.swapee.wc.back.SwapeeButtonController)|(!xyz.swapee.wc.back.ISwapeeButtonScreen|typeof xyz.swapee.wc.back.SwapeeButtonScreen)|(!xyz.swapee.wc.ISwapeeButton|typeof xyz.swapee.wc.SwapeeButton)|(!xyz.swapee.wc.ISwapeeButtonGPU|typeof xyz.swapee.wc.SwapeeButtonGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonHtmlComponent|typeof xyz.swapee.wc.SwapeeButtonHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeButtonController|typeof xyz.swapee.wc.back.SwapeeButtonController)|(!xyz.swapee.wc.back.ISwapeeButtonScreen|typeof xyz.swapee.wc.back.SwapeeButtonScreen)|(!xyz.swapee.wc.ISwapeeButton|typeof xyz.swapee.wc.SwapeeButton)|(!xyz.swapee.wc.ISwapeeButtonGPU|typeof xyz.swapee.wc.SwapeeButtonGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeButtonProcessor|typeof xyz.swapee.wc.SwapeeButtonProcessor)|(!xyz.swapee.wc.ISwapeeButtonComputer|typeof xyz.swapee.wc.SwapeeButtonComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonHtmlComponent.Initialese[]) => xyz.swapee.wc.ISwapeeButtonHtmlComponent} xyz.swapee.wc.SwapeeButtonHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonHtmlComponentCaster&xyz.swapee.wc.back.ISwapeeButtonController&xyz.swapee.wc.back.ISwapeeButtonScreen&xyz.swapee.wc.ISwapeeButton&xyz.swapee.wc.ISwapeeButtonGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.ISwapeeButtonController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.ISwapeeButtonProcessor&xyz.swapee.wc.ISwapeeButtonComputer)} xyz.swapee.wc.ISwapeeButtonHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeButtonController} xyz.swapee.wc.back.ISwapeeButtonController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeButtonScreen} xyz.swapee.wc.back.ISwapeeButtonScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonGPU} xyz.swapee.wc.ISwapeeButtonGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _ISwapeeButton_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ISwapeeButtonHtmlComponent
 */
xyz.swapee.wc.ISwapeeButtonHtmlComponent = class extends /** @type {xyz.swapee.wc.ISwapeeButtonHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeButtonController.typeof&xyz.swapee.wc.back.ISwapeeButtonScreen.typeof&xyz.swapee.wc.ISwapeeButton.typeof&xyz.swapee.wc.ISwapeeButtonGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ISwapeeButtonProcessor.typeof&xyz.swapee.wc.ISwapeeButtonComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonHtmlComponent.Initialese>)} xyz.swapee.wc.SwapeeButtonHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonHtmlComponent} xyz.swapee.wc.ISwapeeButtonHtmlComponent.typeof */
/**
 * A concrete class of _ISwapeeButtonHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonHtmlComponent
 * @implements {xyz.swapee.wc.ISwapeeButtonHtmlComponent} The _ISwapeeButton_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonHtmlComponent = class extends /** @type {xyz.swapee.wc.SwapeeButtonHtmlComponent.constructor&xyz.swapee.wc.ISwapeeButtonHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonHtmlComponent}
 */
xyz.swapee.wc.SwapeeButtonHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeButtonHtmlComponent} */
xyz.swapee.wc.RecordISwapeeButtonHtmlComponent

/** @typedef {xyz.swapee.wc.ISwapeeButtonHtmlComponent} xyz.swapee.wc.BoundISwapeeButtonHtmlComponent */

/** @typedef {xyz.swapee.wc.SwapeeButtonHtmlComponent} xyz.swapee.wc.BoundSwapeeButtonHtmlComponent */

/**
 * Contains getters to cast the _ISwapeeButtonHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonHtmlComponentCaster
 */
xyz.swapee.wc.ISwapeeButtonHtmlComponentCaster = class { }
/**
 * Cast the _ISwapeeButtonHtmlComponent_ instance into the _BoundISwapeeButtonHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonHtmlComponent}
 */
xyz.swapee.wc.ISwapeeButtonHtmlComponentCaster.prototype.asISwapeeButtonHtmlComponent
/**
 * Access the _SwapeeButtonHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonHtmlComponent}
 */
xyz.swapee.wc.ISwapeeButtonHtmlComponentCaster.prototype.superSwapeeButtonHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/130-ISwapeeButtonElement.xml}  1da573df2a876e999a9c5e993a64251a */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.ISwapeeButtonElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ISwapeeButtonElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonElement)} xyz.swapee.wc.AbstractSwapeeButtonElement.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonElement} xyz.swapee.wc.SwapeeButtonElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonElement` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonElement
 */
xyz.swapee.wc.AbstractSwapeeButtonElement = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonElement.constructor&xyz.swapee.wc.SwapeeButtonElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonElement.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonElement.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonElement|typeof xyz.swapee.wc.SwapeeButtonElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonElement}
 */
xyz.swapee.wc.AbstractSwapeeButtonElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElement}
 */
xyz.swapee.wc.AbstractSwapeeButtonElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonElement|typeof xyz.swapee.wc.SwapeeButtonElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElement}
 */
xyz.swapee.wc.AbstractSwapeeButtonElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonElement|typeof xyz.swapee.wc.SwapeeButtonElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElement}
 */
xyz.swapee.wc.AbstractSwapeeButtonElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonElement.Initialese[]) => xyz.swapee.wc.ISwapeeButtonElement} xyz.swapee.wc.SwapeeButtonElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonElementFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.ISwapeeButtonElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.ISwapeeButtonElement.Inputs, null>)} xyz.swapee.wc.ISwapeeButtonElement.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 *
 * The _ISwapeeButton_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ISwapeeButtonElement
 */
xyz.swapee.wc.ISwapeeButtonElement = class extends /** @type {xyz.swapee.wc.ISwapeeButtonElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeButtonElement.solder} */
xyz.swapee.wc.ISwapeeButtonElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ISwapeeButtonElement.render} */
xyz.swapee.wc.ISwapeeButtonElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ISwapeeButtonElement.server} */
xyz.swapee.wc.ISwapeeButtonElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ISwapeeButtonElement.inducer} */
xyz.swapee.wc.ISwapeeButtonElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonElement&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonElement.Initialese>)} xyz.swapee.wc.SwapeeButtonElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElement} xyz.swapee.wc.ISwapeeButtonElement.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeButtonElement_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonElement
 * @implements {xyz.swapee.wc.ISwapeeButtonElement} A component description.
 *
 * The _ISwapeeButton_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonElement.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonElement = class extends /** @type {xyz.swapee.wc.SwapeeButtonElement.constructor&xyz.swapee.wc.ISwapeeButtonElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonElement}
 */
xyz.swapee.wc.SwapeeButtonElement.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonElement.
 * @interface xyz.swapee.wc.ISwapeeButtonElementFields
 */
xyz.swapee.wc.ISwapeeButtonElementFields = class { }
/**
 * The available customisations of _ISwapeeButton_.
 */
xyz.swapee.wc.ISwapeeButtonElementFields.prototype.customs = /** @type {!xyz.swapee.wc.ISwapeeButtonElement.Customs} */ (void 0)
/**
 * The element-specific inputs to the _ISwapeeButton_ component.
 */
xyz.swapee.wc.ISwapeeButtonElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeButtonElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonElement} */
xyz.swapee.wc.RecordISwapeeButtonElement

/** @typedef {xyz.swapee.wc.ISwapeeButtonElement} xyz.swapee.wc.BoundISwapeeButtonElement */

/** @typedef {xyz.swapee.wc.SwapeeButtonElement} xyz.swapee.wc.BoundSwapeeButtonElement */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElement.Customs The available customisations of _ISwapeeButton_.
 * @prop {string} [BORDER="2px"] The border width. Default `2px`.
 * @prop {string} [BACKGROUND="white"] The background color. Default `white`.
 * @prop {string} [COLOR="initial"] The text color. Default `initial`.
 * @prop {string} [CLIP_PATH="10px"] How much to cut corners by. Default `10px`.
 * @prop {string} [INNER_BORDER_COLOR="lightgrey"] The color of the inner border. Default `lightgrey`.
 */

/** @typedef {xyz.swapee.wc.ISwapeeButtonPort.Inputs&xyz.swapee.wc.ISwapeeButtonDisplay.Queries&xyz.swapee.wc.ISwapeeButtonController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ISwapeeButtonElementPort.Inputs} xyz.swapee.wc.ISwapeeButtonElement.Inputs The element-specific inputs to the _ISwapeeButton_ component. */

/**
 * Contains getters to cast the _ISwapeeButtonElement_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonElementCaster
 */
xyz.swapee.wc.ISwapeeButtonElementCaster = class { }
/**
 * Cast the _ISwapeeButtonElement_ instance into the _BoundISwapeeButtonElement_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonElement}
 */
xyz.swapee.wc.ISwapeeButtonElementCaster.prototype.asISwapeeButtonElement
/**
 * Access the _SwapeeButtonElement_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonElement}
 */
xyz.swapee.wc.ISwapeeButtonElementCaster.prototype.superSwapeeButtonElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeButtonMemory, props: !xyz.swapee.wc.ISwapeeButtonElement.Inputs) => Object<string, *>} xyz.swapee.wc.ISwapeeButtonElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonElement.__solder<!xyz.swapee.wc.ISwapeeButtonElement>} xyz.swapee.wc.ISwapeeButtonElement._solder */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.SwapeeButtonMemory} model The model.
 * - `active` _boolean_ Whether the button is in the active state. Default `false`.
 * - `hover` _boolean_ Whether the button is in the hover state. Default `false`.
 * @param {!xyz.swapee.wc.ISwapeeButtonElement.Inputs} props The element props.
 * - `[active=null]` _&#42;?_ Whether the button is in the active state. ⤴ *ISwapeeButtonOuterCore.WeakModel.Active* ⤴ *ISwapeeButtonOuterCore.WeakModel.Active* Default `null`.
 * - `[hover=null]` _&#42;?_ Whether the button is in the hover state. ⤴ *ISwapeeButtonOuterCore.WeakModel.Hover* ⤴ *ISwapeeButtonOuterCore.WeakModel.Hover* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeButtonElementPort.Inputs.NoSolder* Default `false`.
 * - `[buLaOpts]` _!Object?_ The options to pass to the _BuLa_ vdu. ⤴ *ISwapeeButtonElementPort.Inputs.BuLaOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ISwapeeButtonElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeButtonMemory, instance?: !xyz.swapee.wc.ISwapeeButtonScreen&xyz.swapee.wc.ISwapeeButtonController) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeButtonElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonElement.__render<!xyz.swapee.wc.ISwapeeButtonElement>} xyz.swapee.wc.ISwapeeButtonElement._render */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.SwapeeButtonMemory} [model] The model for the view.
 * - `active` _boolean_ Whether the button is in the active state. Default `false`.
 * - `hover` _boolean_ Whether the button is in the hover state. Default `false`.
 * @param {!xyz.swapee.wc.ISwapeeButtonScreen&xyz.swapee.wc.ISwapeeButtonController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeButtonElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeButtonMemory, inputs: !xyz.swapee.wc.ISwapeeButtonElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeButtonElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonElement.__server<!xyz.swapee.wc.ISwapeeButtonElement>} xyz.swapee.wc.ISwapeeButtonElement._server */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.SwapeeButtonMemory} memory The memory registers.
 * - `active` _boolean_ Whether the button is in the active state. Default `false`.
 * - `hover` _boolean_ Whether the button is in the hover state. Default `false`.
 * @param {!xyz.swapee.wc.ISwapeeButtonElement.Inputs} inputs The inputs to the port.
 * - `[active=null]` _&#42;?_ Whether the button is in the active state. ⤴ *ISwapeeButtonOuterCore.WeakModel.Active* ⤴ *ISwapeeButtonOuterCore.WeakModel.Active* Default `null`.
 * - `[hover=null]` _&#42;?_ Whether the button is in the hover state. ⤴ *ISwapeeButtonOuterCore.WeakModel.Hover* ⤴ *ISwapeeButtonOuterCore.WeakModel.Hover* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeButtonElementPort.Inputs.NoSolder* Default `false`.
 * - `[buLaOpts]` _!Object?_ The options to pass to the _BuLa_ vdu. ⤴ *ISwapeeButtonElementPort.Inputs.BuLaOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeButtonElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeButtonMemory, port?: !xyz.swapee.wc.ISwapeeButtonElement.Inputs) => ?} xyz.swapee.wc.ISwapeeButtonElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonElement.__inducer<!xyz.swapee.wc.ISwapeeButtonElement>} xyz.swapee.wc.ISwapeeButtonElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.SwapeeButtonMemory} [model] The model of the component into which to induce the state.
 * - `active` _boolean_ Whether the button is in the active state. Default `false`.
 * - `hover` _boolean_ Whether the button is in the hover state. Default `false`.
 * @param {!xyz.swapee.wc.ISwapeeButtonElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[active=null]` _&#42;?_ Whether the button is in the active state. ⤴ *ISwapeeButtonOuterCore.WeakModel.Active* ⤴ *ISwapeeButtonOuterCore.WeakModel.Active* Default `null`.
 * - `[hover=null]` _&#42;?_ Whether the button is in the hover state. ⤴ *ISwapeeButtonOuterCore.WeakModel.Hover* ⤴ *ISwapeeButtonOuterCore.WeakModel.Hover* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeButtonElementPort.Inputs.NoSolder* Default `false`.
 * - `[buLaOpts]` _!Object?_ The options to pass to the _BuLa_ vdu. ⤴ *ISwapeeButtonElementPort.Inputs.BuLaOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeButtonElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeButtonElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/140-ISwapeeButtonElementPort.xml}  dd4f3f5906548e338bc3f91d4715a394 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeButtonElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonElementPort)} xyz.swapee.wc.AbstractSwapeeButtonElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonElementPort} xyz.swapee.wc.SwapeeButtonElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonElementPort
 */
xyz.swapee.wc.AbstractSwapeeButtonElementPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonElementPort.constructor&xyz.swapee.wc.SwapeeButtonElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonElementPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonElementPort|typeof xyz.swapee.wc.SwapeeButtonElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonElementPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElementPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonElementPort|typeof xyz.swapee.wc.SwapeeButtonElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElementPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonElementPort|typeof xyz.swapee.wc.SwapeeButtonElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonElementPort}
 */
xyz.swapee.wc.AbstractSwapeeButtonElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonElementPort.Initialese[]) => xyz.swapee.wc.ISwapeeButtonElementPort} xyz.swapee.wc.SwapeeButtonElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeButtonElementPort.Inputs>)} xyz.swapee.wc.ISwapeeButtonElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ISwapeeButtonElementPort
 */
xyz.swapee.wc.ISwapeeButtonElementPort = class extends /** @type {xyz.swapee.wc.ISwapeeButtonElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonElementPort.Initialese>)} xyz.swapee.wc.SwapeeButtonElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElementPort} xyz.swapee.wc.ISwapeeButtonElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeButtonElementPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonElementPort
 * @implements {xyz.swapee.wc.ISwapeeButtonElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonElementPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonElementPort = class extends /** @type {xyz.swapee.wc.SwapeeButtonElementPort.constructor&xyz.swapee.wc.ISwapeeButtonElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonElementPort}
 */
xyz.swapee.wc.SwapeeButtonElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonElementPort.
 * @interface xyz.swapee.wc.ISwapeeButtonElementPortFields
 */
xyz.swapee.wc.ISwapeeButtonElementPortFields = class { }
/**
 * The inputs to the _ISwapeeButtonElement_'s controller via its element port.
 */
xyz.swapee.wc.ISwapeeButtonElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeButtonElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeButtonElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeButtonElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonElementPort} */
xyz.swapee.wc.RecordISwapeeButtonElementPort

/** @typedef {xyz.swapee.wc.ISwapeeButtonElementPort} xyz.swapee.wc.BoundISwapeeButtonElementPort */

/** @typedef {xyz.swapee.wc.SwapeeButtonElementPort} xyz.swapee.wc.BoundSwapeeButtonElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _BuLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts.buLaOpts

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder&xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts)} xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder} xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts} xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts.typeof */
/**
 * The inputs to the _ISwapeeButtonElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeButtonElementPort.Inputs
 */
xyz.swapee.wc.ISwapeeButtonElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.constructor&xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeButtonElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.NoSolder&xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.BuLaOpts)} xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.BuLaOpts} xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.BuLaOpts.typeof */
/**
 * The inputs to the _ISwapeeButtonElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.BuLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs

/**
 * Contains getters to cast the _ISwapeeButtonElementPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonElementPortCaster
 */
xyz.swapee.wc.ISwapeeButtonElementPortCaster = class { }
/**
 * Cast the _ISwapeeButtonElementPort_ instance into the _BoundISwapeeButtonElementPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonElementPort}
 */
xyz.swapee.wc.ISwapeeButtonElementPortCaster.prototype.asISwapeeButtonElementPort
/**
 * Access the _SwapeeButtonElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonElementPort}
 */
xyz.swapee.wc.ISwapeeButtonElementPortCaster.prototype.superSwapeeButtonElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts The options to pass to the _BuLa_ vdu (optional overlay).
 * @prop {!Object} [buLaOpts] The options to pass to the _BuLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.Inputs.BuLaOpts_Safe The options to pass to the _BuLa_ vdu (required overlay).
 * @prop {!Object} buLaOpts The options to pass to the _BuLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.BuLaOpts The options to pass to the _BuLa_ vdu (optional overlay).
 * @prop {*} [buLaOpts=null] The options to pass to the _BuLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonElementPort.WeakInputs.BuLaOpts_Safe The options to pass to the _BuLa_ vdu (required overlay).
 * @prop {*} buLaOpts The options to pass to the _BuLa_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/170-ISwapeeButtonDesigner.xml}  f4adddae0f6d22753a7788eb9ad694fb */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ISwapeeButtonDesigner
 */
xyz.swapee.wc.ISwapeeButtonDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.SwapeeButtonClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeButton />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeButtonClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeButton />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeButtonClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeButtonDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeButton` _typeof ISwapeeButtonController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeButtonDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeButton` _typeof ISwapeeButtonController_
   * - `This` _typeof ISwapeeButtonController_
   * @param {!xyz.swapee.wc.ISwapeeButtonDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `SwapeeButton` _!SwapeeButtonMemory_
   * - `This` _!SwapeeButtonMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeButtonClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeButtonClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ISwapeeButtonDesigner.prototype.constructor = xyz.swapee.wc.ISwapeeButtonDesigner

/**
 * A concrete class of _ISwapeeButtonDesigner_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonDesigner
 * @implements {xyz.swapee.wc.ISwapeeButtonDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.SwapeeButtonDesigner = class extends xyz.swapee.wc.ISwapeeButtonDesigner { }
xyz.swapee.wc.SwapeeButtonDesigner.prototype.constructor = xyz.swapee.wc.SwapeeButtonDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeButtonController} SwapeeButton
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeButtonController} SwapeeButton
 * @prop {typeof xyz.swapee.wc.ISwapeeButtonController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeButtonDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.SwapeeButtonMemory} SwapeeButton
 * @prop {!xyz.swapee.wc.SwapeeButtonMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/40-ISwapeeButtonDisplay.xml}  5342196554e2e6cd032a728fc588a576 */
/**
 * @typedef {Object} $xyz.swapee.wc.ISwapeeButtonDisplay.Initialese
 * @prop {HTMLButtonElement} [BuLa]
 */
/** @typedef {$xyz.swapee.wc.ISwapeeButtonDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ISwapeeButtonDisplay.Settings>} xyz.swapee.wc.ISwapeeButtonDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonDisplay)} xyz.swapee.wc.AbstractSwapeeButtonDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonDisplay} xyz.swapee.wc.SwapeeButtonDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonDisplay
 */
xyz.swapee.wc.AbstractSwapeeButtonDisplay = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonDisplay.constructor&xyz.swapee.wc.SwapeeButtonDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonDisplay.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonDisplay|typeof xyz.swapee.wc.SwapeeButtonDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonDisplay}
 */
xyz.swapee.wc.AbstractSwapeeButtonDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonDisplay}
 */
xyz.swapee.wc.AbstractSwapeeButtonDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonDisplay|typeof xyz.swapee.wc.SwapeeButtonDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonDisplay}
 */
xyz.swapee.wc.AbstractSwapeeButtonDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonDisplay|typeof xyz.swapee.wc.SwapeeButtonDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonDisplay}
 */
xyz.swapee.wc.AbstractSwapeeButtonDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonDisplay.Initialese[]) => xyz.swapee.wc.ISwapeeButtonDisplay} xyz.swapee.wc.SwapeeButtonDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.SwapeeButtonMemory, !HTMLDivElement, !xyz.swapee.wc.ISwapeeButtonDisplay.Settings, xyz.swapee.wc.ISwapeeButtonDisplay.Queries, null>)} xyz.swapee.wc.ISwapeeButtonDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ISwapeeButton_.
 * @interface xyz.swapee.wc.ISwapeeButtonDisplay
 */
xyz.swapee.wc.ISwapeeButtonDisplay = class extends /** @type {xyz.swapee.wc.ISwapeeButtonDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeButtonDisplay.paint} */
xyz.swapee.wc.ISwapeeButtonDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonDisplay.Initialese>)} xyz.swapee.wc.SwapeeButtonDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonDisplay} xyz.swapee.wc.ISwapeeButtonDisplay.typeof */
/**
 * A concrete class of _ISwapeeButtonDisplay_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonDisplay
 * @implements {xyz.swapee.wc.ISwapeeButtonDisplay} Display for presenting information from the _ISwapeeButton_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonDisplay.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonDisplay = class extends /** @type {xyz.swapee.wc.SwapeeButtonDisplay.constructor&xyz.swapee.wc.ISwapeeButtonDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonDisplay}
 */
xyz.swapee.wc.SwapeeButtonDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonDisplay.
 * @interface xyz.swapee.wc.ISwapeeButtonDisplayFields
 */
xyz.swapee.wc.ISwapeeButtonDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ISwapeeButtonDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ISwapeeButtonDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ISwapeeButtonDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ISwapeeButtonDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeButtonDisplayFields.prototype.BuLa = /** @type {HTMLButtonElement} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonDisplay} */
xyz.swapee.wc.RecordISwapeeButtonDisplay

/** @typedef {xyz.swapee.wc.ISwapeeButtonDisplay} xyz.swapee.wc.BoundISwapeeButtonDisplay */

/** @typedef {xyz.swapee.wc.SwapeeButtonDisplay} xyz.swapee.wc.BoundSwapeeButtonDisplay */

/** @typedef {xyz.swapee.wc.ISwapeeButtonDisplay.Queries} xyz.swapee.wc.ISwapeeButtonDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeButtonDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _ISwapeeButtonDisplay_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonDisplayCaster
 */
xyz.swapee.wc.ISwapeeButtonDisplayCaster = class { }
/**
 * Cast the _ISwapeeButtonDisplay_ instance into the _BoundISwapeeButtonDisplay_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonDisplay}
 */
xyz.swapee.wc.ISwapeeButtonDisplayCaster.prototype.asISwapeeButtonDisplay
/**
 * Cast the _ISwapeeButtonDisplay_ instance into the _BoundISwapeeButtonScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonScreen}
 */
xyz.swapee.wc.ISwapeeButtonDisplayCaster.prototype.asISwapeeButtonScreen
/**
 * Access the _SwapeeButtonDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonDisplay}
 */
xyz.swapee.wc.ISwapeeButtonDisplayCaster.prototype.superSwapeeButtonDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeButtonMemory, land: null) => void} xyz.swapee.wc.ISwapeeButtonDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonDisplay.__paint<!xyz.swapee.wc.ISwapeeButtonDisplay>} xyz.swapee.wc.ISwapeeButtonDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeButtonMemory} memory The display data.
 * - `active` _boolean_ Whether the button is in the active state. Default `false`.
 * - `hover` _boolean_ Whether the button is in the hover state. Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeButtonDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeButtonDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/40-ISwapeeButtonDisplayBack.xml}  72829db0e01cdf96d037cb8435d5114f */
/**
 * @typedef {Object} $xyz.swapee.wc.back.ISwapeeButtonDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [BuLa]
 */
/** @typedef {$xyz.swapee.wc.back.ISwapeeButtonDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.SwapeeButtonClasses>} xyz.swapee.wc.back.ISwapeeButtonDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeButtonDisplay)} xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeButtonDisplay} xyz.swapee.wc.back.SwapeeButtonDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeButtonDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeButtonDisplay
 */
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.constructor&xyz.swapee.wc.back.SwapeeButtonDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeButtonDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonDisplay|typeof xyz.swapee.wc.back.SwapeeButtonDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeButtonDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonDisplay|typeof xyz.swapee.wc.back.SwapeeButtonDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonDisplay|typeof xyz.swapee.wc.back.SwapeeButtonDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeButtonDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeButtonDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.SwapeeButtonClasses, null>)} xyz.swapee.wc.back.ISwapeeButtonDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ISwapeeButtonDisplay
 */
xyz.swapee.wc.back.ISwapeeButtonDisplay = class extends /** @type {xyz.swapee.wc.back.ISwapeeButtonDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ISwapeeButtonDisplay.paint} */
xyz.swapee.wc.back.ISwapeeButtonDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeButtonDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonDisplay.Initialese>)} xyz.swapee.wc.back.SwapeeButtonDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeButtonDisplay} xyz.swapee.wc.back.ISwapeeButtonDisplay.typeof */
/**
 * A concrete class of _ISwapeeButtonDisplay_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeButtonDisplay
 * @implements {xyz.swapee.wc.back.ISwapeeButtonDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeButtonDisplay = class extends /** @type {xyz.swapee.wc.back.SwapeeButtonDisplay.constructor&xyz.swapee.wc.back.ISwapeeButtonDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.SwapeeButtonDisplay.prototype.constructor = xyz.swapee.wc.back.SwapeeButtonDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonDisplay}
 */
xyz.swapee.wc.back.SwapeeButtonDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonDisplay.
 * @interface xyz.swapee.wc.back.ISwapeeButtonDisplayFields
 */
xyz.swapee.wc.back.ISwapeeButtonDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.ISwapeeButtonDisplayFields.prototype.BuLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonDisplay} */
xyz.swapee.wc.back.RecordISwapeeButtonDisplay

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonDisplay} xyz.swapee.wc.back.BoundISwapeeButtonDisplay */

/** @typedef {xyz.swapee.wc.back.SwapeeButtonDisplay} xyz.swapee.wc.back.BoundSwapeeButtonDisplay */

/**
 * Contains getters to cast the _ISwapeeButtonDisplay_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeButtonDisplayCaster
 */
xyz.swapee.wc.back.ISwapeeButtonDisplayCaster = class { }
/**
 * Cast the _ISwapeeButtonDisplay_ instance into the _BoundISwapeeButtonDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeButtonDisplay}
 */
xyz.swapee.wc.back.ISwapeeButtonDisplayCaster.prototype.asISwapeeButtonDisplay
/**
 * Access the _SwapeeButtonDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeButtonDisplay}
 */
xyz.swapee.wc.back.ISwapeeButtonDisplayCaster.prototype.superSwapeeButtonDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.SwapeeButtonMemory, land?: null) => void} xyz.swapee.wc.back.ISwapeeButtonDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeButtonDisplay.__paint<!xyz.swapee.wc.back.ISwapeeButtonDisplay>} xyz.swapee.wc.back.ISwapeeButtonDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeButtonDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeButtonMemory} [memory] The display data.
 * - `active` _boolean_ Whether the button is in the active state. Default `false`.
 * - `hover` _boolean_ Whether the button is in the hover state. Default `false`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeButtonDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeButtonDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/41-SwapeeButtonClasses.xml}  90a9b6d53529449cf95456d937e340e6 */
/**
 * The classes of the _ISwapeeButtonDisplay_.
 * @record xyz.swapee.wc.SwapeeButtonClasses
 */
xyz.swapee.wc.SwapeeButtonClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.SwapeeButtonClasses.prototype.props = /** @type {xyz.swapee.wc.SwapeeButtonClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/50-ISwapeeButtonController.xml}  aed33702686d8abf0be65ed9fc312d45 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ISwapeeButtonController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ISwapeeButtonController.Inputs, !xyz.swapee.wc.ISwapeeButtonOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel>} xyz.swapee.wc.ISwapeeButtonController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonController)} xyz.swapee.wc.AbstractSwapeeButtonController.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonController} xyz.swapee.wc.SwapeeButtonController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonController` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonController
 */
xyz.swapee.wc.AbstractSwapeeButtonController = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonController.constructor&xyz.swapee.wc.SwapeeButtonController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonController.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonController.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonController}
 */
xyz.swapee.wc.AbstractSwapeeButtonController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonController}
 */
xyz.swapee.wc.AbstractSwapeeButtonController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonController}
 */
xyz.swapee.wc.AbstractSwapeeButtonController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonController}
 */
xyz.swapee.wc.AbstractSwapeeButtonController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonController.Initialese[]) => xyz.swapee.wc.ISwapeeButtonController} xyz.swapee.wc.SwapeeButtonControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ISwapeeButtonController.Inputs, !xyz.swapee.wc.ISwapeeButtonOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ISwapeeButtonOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ISwapeeButtonController.Inputs, !xyz.swapee.wc.ISwapeeButtonController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ISwapeeButtonController.Inputs, !xyz.swapee.wc.SwapeeButtonMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeButtonController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ISwapeeButtonController.Inputs>)} xyz.swapee.wc.ISwapeeButtonController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ISwapeeButtonController
 */
xyz.swapee.wc.ISwapeeButtonController = class extends /** @type {xyz.swapee.wc.ISwapeeButtonController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeButtonController.resetPort} */
xyz.swapee.wc.ISwapeeButtonController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonController&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonController.Initialese>)} xyz.swapee.wc.SwapeeButtonController.constructor */
/**
 * A concrete class of _ISwapeeButtonController_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonController
 * @implements {xyz.swapee.wc.ISwapeeButtonController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonController.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonController = class extends /** @type {xyz.swapee.wc.SwapeeButtonController.constructor&xyz.swapee.wc.ISwapeeButtonController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonController}
 */
xyz.swapee.wc.SwapeeButtonController.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonController.
 * @interface xyz.swapee.wc.ISwapeeButtonControllerFields
 */
xyz.swapee.wc.ISwapeeButtonControllerFields = class { }
/**
 * The inputs to the _ISwapeeButton_'s controller.
 */
xyz.swapee.wc.ISwapeeButtonControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeButtonController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeButtonControllerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeButtonController} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonController} */
xyz.swapee.wc.RecordISwapeeButtonController

/** @typedef {xyz.swapee.wc.ISwapeeButtonController} xyz.swapee.wc.BoundISwapeeButtonController */

/** @typedef {xyz.swapee.wc.SwapeeButtonController} xyz.swapee.wc.BoundSwapeeButtonController */

/** @typedef {xyz.swapee.wc.ISwapeeButtonPort.Inputs} xyz.swapee.wc.ISwapeeButtonController.Inputs The inputs to the _ISwapeeButton_'s controller. */

/** @typedef {xyz.swapee.wc.ISwapeeButtonPort.WeakInputs} xyz.swapee.wc.ISwapeeButtonController.WeakInputs The inputs to the _ISwapeeButton_'s controller. */

/**
 * Contains getters to cast the _ISwapeeButtonController_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonControllerCaster
 */
xyz.swapee.wc.ISwapeeButtonControllerCaster = class { }
/**
 * Cast the _ISwapeeButtonController_ instance into the _BoundISwapeeButtonController_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonController}
 */
xyz.swapee.wc.ISwapeeButtonControllerCaster.prototype.asISwapeeButtonController
/**
 * Cast the _ISwapeeButtonController_ instance into the _BoundISwapeeButtonProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonProcessor}
 */
xyz.swapee.wc.ISwapeeButtonControllerCaster.prototype.asISwapeeButtonProcessor
/**
 * Access the _SwapeeButtonController_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonController}
 */
xyz.swapee.wc.ISwapeeButtonControllerCaster.prototype.superSwapeeButtonController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeButtonController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeButtonController.__resetPort<!xyz.swapee.wc.ISwapeeButtonController>} xyz.swapee.wc.ISwapeeButtonController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeButtonController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeButtonController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/51-ISwapeeButtonControllerFront.xml}  4d6b1b59d3fbc3dee92507f92334e478 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ISwapeeButtonController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeButtonController)} xyz.swapee.wc.front.AbstractSwapeeButtonController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeButtonController} xyz.swapee.wc.front.SwapeeButtonController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeButtonController` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeButtonController
 */
xyz.swapee.wc.front.AbstractSwapeeButtonController = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeButtonController.constructor&xyz.swapee.wc.front.SwapeeButtonController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeButtonController.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeButtonController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeButtonController.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeButtonController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonController|typeof xyz.swapee.wc.front.SwapeeButtonController)|(!xyz.swapee.wc.front.ISwapeeButtonControllerAT|typeof xyz.swapee.wc.front.SwapeeButtonControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeButtonController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeButtonController}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonController}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonController|typeof xyz.swapee.wc.front.SwapeeButtonController)|(!xyz.swapee.wc.front.ISwapeeButtonControllerAT|typeof xyz.swapee.wc.front.SwapeeButtonControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonController}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonController|typeof xyz.swapee.wc.front.SwapeeButtonController)|(!xyz.swapee.wc.front.ISwapeeButtonControllerAT|typeof xyz.swapee.wc.front.SwapeeButtonControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonController}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeButtonController.Initialese[]) => xyz.swapee.wc.front.ISwapeeButtonController} xyz.swapee.wc.front.SwapeeButtonControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeButtonControllerCaster&xyz.swapee.wc.front.ISwapeeButtonControllerAT)} xyz.swapee.wc.front.ISwapeeButtonController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeButtonControllerAT} xyz.swapee.wc.front.ISwapeeButtonControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ISwapeeButtonController
 */
xyz.swapee.wc.front.ISwapeeButtonController = class extends /** @type {xyz.swapee.wc.front.ISwapeeButtonController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ISwapeeButtonControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeButtonController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeButtonController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeButtonController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeButtonController.Initialese>)} xyz.swapee.wc.front.SwapeeButtonController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeButtonController} xyz.swapee.wc.front.ISwapeeButtonController.typeof */
/**
 * A concrete class of _ISwapeeButtonController_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeButtonController
 * @implements {xyz.swapee.wc.front.ISwapeeButtonController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeButtonController.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeButtonController = class extends /** @type {xyz.swapee.wc.front.SwapeeButtonController.constructor&xyz.swapee.wc.front.ISwapeeButtonController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeButtonController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeButtonController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeButtonController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonController}
 */
xyz.swapee.wc.front.SwapeeButtonController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeButtonController} */
xyz.swapee.wc.front.RecordISwapeeButtonController

/** @typedef {xyz.swapee.wc.front.ISwapeeButtonController} xyz.swapee.wc.front.BoundISwapeeButtonController */

/** @typedef {xyz.swapee.wc.front.SwapeeButtonController} xyz.swapee.wc.front.BoundSwapeeButtonController */

/**
 * Contains getters to cast the _ISwapeeButtonController_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeButtonControllerCaster
 */
xyz.swapee.wc.front.ISwapeeButtonControllerCaster = class { }
/**
 * Cast the _ISwapeeButtonController_ instance into the _BoundISwapeeButtonController_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeButtonController}
 */
xyz.swapee.wc.front.ISwapeeButtonControllerCaster.prototype.asISwapeeButtonController
/**
 * Access the _SwapeeButtonController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeButtonController}
 */
xyz.swapee.wc.front.ISwapeeButtonControllerCaster.prototype.superSwapeeButtonController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/52-ISwapeeButtonControllerBack.xml}  ef670d7a3707aa2e40a2f03cbd3640ae */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ISwapeeButtonController.Inputs>&xyz.swapee.wc.ISwapeeButtonController.Initialese} xyz.swapee.wc.back.ISwapeeButtonController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeButtonController)} xyz.swapee.wc.back.AbstractSwapeeButtonController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeButtonController} xyz.swapee.wc.back.SwapeeButtonController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeButtonController` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeButtonController
 */
xyz.swapee.wc.back.AbstractSwapeeButtonController = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeButtonController.constructor&xyz.swapee.wc.back.SwapeeButtonController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeButtonController.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeButtonController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeButtonController.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonController|typeof xyz.swapee.wc.back.SwapeeButtonController)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeButtonController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeButtonController}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonController}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonController|typeof xyz.swapee.wc.back.SwapeeButtonController)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonController}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonController|typeof xyz.swapee.wc.back.SwapeeButtonController)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonController}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeButtonController.Initialese[]) => xyz.swapee.wc.back.ISwapeeButtonController} xyz.swapee.wc.back.SwapeeButtonControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeButtonControllerCaster&xyz.swapee.wc.ISwapeeButtonController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ISwapeeButtonController.Inputs>)} xyz.swapee.wc.back.ISwapeeButtonController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ISwapeeButtonController
 */
xyz.swapee.wc.back.ISwapeeButtonController = class extends /** @type {xyz.swapee.wc.back.ISwapeeButtonController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeButtonController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeButtonController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeButtonController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonController.Initialese>)} xyz.swapee.wc.back.SwapeeButtonController.constructor */
/**
 * A concrete class of _ISwapeeButtonController_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeButtonController
 * @implements {xyz.swapee.wc.back.ISwapeeButtonController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonController.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeButtonController = class extends /** @type {xyz.swapee.wc.back.SwapeeButtonController.constructor&xyz.swapee.wc.back.ISwapeeButtonController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeButtonController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeButtonController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonController}
 */
xyz.swapee.wc.back.SwapeeButtonController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonController} */
xyz.swapee.wc.back.RecordISwapeeButtonController

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonController} xyz.swapee.wc.back.BoundISwapeeButtonController */

/** @typedef {xyz.swapee.wc.back.SwapeeButtonController} xyz.swapee.wc.back.BoundSwapeeButtonController */

/**
 * Contains getters to cast the _ISwapeeButtonController_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeButtonControllerCaster
 */
xyz.swapee.wc.back.ISwapeeButtonControllerCaster = class { }
/**
 * Cast the _ISwapeeButtonController_ instance into the _BoundISwapeeButtonController_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeButtonController}
 */
xyz.swapee.wc.back.ISwapeeButtonControllerCaster.prototype.asISwapeeButtonController
/**
 * Access the _SwapeeButtonController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeButtonController}
 */
xyz.swapee.wc.back.ISwapeeButtonControllerCaster.prototype.superSwapeeButtonController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/53-ISwapeeButtonControllerAR.xml}  b6040912139b3b64cc4ea953a3228e28 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeButtonController.Initialese} xyz.swapee.wc.back.ISwapeeButtonControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeButtonControllerAR)} xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeButtonControllerAR} xyz.swapee.wc.back.SwapeeButtonControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeButtonControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR
 */
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.constructor&xyz.swapee.wc.back.SwapeeButtonControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonControllerAR|typeof xyz.swapee.wc.back.SwapeeButtonControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonControllerAR|typeof xyz.swapee.wc.back.SwapeeButtonControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonControllerAR|typeof xyz.swapee.wc.back.SwapeeButtonControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeButtonController|typeof xyz.swapee.wc.SwapeeButtonController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeButtonControllerAR.Initialese[]) => xyz.swapee.wc.back.ISwapeeButtonControllerAR} xyz.swapee.wc.back.SwapeeButtonControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeButtonControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeButtonController)} xyz.swapee.wc.back.ISwapeeButtonControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeButtonControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ISwapeeButtonControllerAR
 */
xyz.swapee.wc.back.ISwapeeButtonControllerAR = class extends /** @type {xyz.swapee.wc.back.ISwapeeButtonControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeButtonController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeButtonControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeButtonControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonControllerAR.Initialese>)} xyz.swapee.wc.back.SwapeeButtonControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeButtonControllerAR} xyz.swapee.wc.back.ISwapeeButtonControllerAR.typeof */
/**
 * A concrete class of _ISwapeeButtonControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeButtonControllerAR
 * @implements {xyz.swapee.wc.back.ISwapeeButtonControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeButtonControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeButtonControllerAR = class extends /** @type {xyz.swapee.wc.back.SwapeeButtonControllerAR.constructor&xyz.swapee.wc.back.ISwapeeButtonControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeButtonControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeButtonControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonControllerAR}
 */
xyz.swapee.wc.back.SwapeeButtonControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonControllerAR} */
xyz.swapee.wc.back.RecordISwapeeButtonControllerAR

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonControllerAR} xyz.swapee.wc.back.BoundISwapeeButtonControllerAR */

/** @typedef {xyz.swapee.wc.back.SwapeeButtonControllerAR} xyz.swapee.wc.back.BoundSwapeeButtonControllerAR */

/**
 * Contains getters to cast the _ISwapeeButtonControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeButtonControllerARCaster
 */
xyz.swapee.wc.back.ISwapeeButtonControllerARCaster = class { }
/**
 * Cast the _ISwapeeButtonControllerAR_ instance into the _BoundISwapeeButtonControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeButtonControllerAR}
 */
xyz.swapee.wc.back.ISwapeeButtonControllerARCaster.prototype.asISwapeeButtonControllerAR
/**
 * Access the _SwapeeButtonControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeButtonControllerAR}
 */
xyz.swapee.wc.back.ISwapeeButtonControllerARCaster.prototype.superSwapeeButtonControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/54-ISwapeeButtonControllerAT.xml}  52833a5cb21aa5b0328fdfeddc9b776d */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ISwapeeButtonControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeButtonControllerAT)} xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeButtonControllerAT} xyz.swapee.wc.front.SwapeeButtonControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeButtonControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT
 */
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.constructor&xyz.swapee.wc.front.SwapeeButtonControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonControllerAT|typeof xyz.swapee.wc.front.SwapeeButtonControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonControllerAT|typeof xyz.swapee.wc.front.SwapeeButtonControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonControllerAT|typeof xyz.swapee.wc.front.SwapeeButtonControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeButtonControllerAT.Initialese[]) => xyz.swapee.wc.front.ISwapeeButtonControllerAT} xyz.swapee.wc.front.SwapeeButtonControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeButtonControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ISwapeeButtonControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeButtonControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ISwapeeButtonControllerAT
 */
xyz.swapee.wc.front.ISwapeeButtonControllerAT = class extends /** @type {xyz.swapee.wc.front.ISwapeeButtonControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeButtonControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeButtonControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeButtonControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeButtonControllerAT.Initialese>)} xyz.swapee.wc.front.SwapeeButtonControllerAT.constructor */
/**
 * A concrete class of _ISwapeeButtonControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeButtonControllerAT
 * @implements {xyz.swapee.wc.front.ISwapeeButtonControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeButtonControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeButtonControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeButtonControllerAT = class extends /** @type {xyz.swapee.wc.front.SwapeeButtonControllerAT.constructor&xyz.swapee.wc.front.ISwapeeButtonControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeButtonControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeButtonControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeButtonControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonControllerAT}
 */
xyz.swapee.wc.front.SwapeeButtonControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeButtonControllerAT} */
xyz.swapee.wc.front.RecordISwapeeButtonControllerAT

/** @typedef {xyz.swapee.wc.front.ISwapeeButtonControllerAT} xyz.swapee.wc.front.BoundISwapeeButtonControllerAT */

/** @typedef {xyz.swapee.wc.front.SwapeeButtonControllerAT} xyz.swapee.wc.front.BoundSwapeeButtonControllerAT */

/**
 * Contains getters to cast the _ISwapeeButtonControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeButtonControllerATCaster
 */
xyz.swapee.wc.front.ISwapeeButtonControllerATCaster = class { }
/**
 * Cast the _ISwapeeButtonControllerAT_ instance into the _BoundISwapeeButtonControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeButtonControllerAT}
 */
xyz.swapee.wc.front.ISwapeeButtonControllerATCaster.prototype.asISwapeeButtonControllerAT
/**
 * Access the _SwapeeButtonControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeButtonControllerAT}
 */
xyz.swapee.wc.front.ISwapeeButtonControllerATCaster.prototype.superSwapeeButtonControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/70-ISwapeeButtonScreen.xml}  84c44cd42075903c87e11ed1003f7c47 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.front.SwapeeButtonInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeButtonDisplay.Settings, !xyz.swapee.wc.ISwapeeButtonDisplay.Queries, null>&xyz.swapee.wc.ISwapeeButtonDisplay.Initialese} xyz.swapee.wc.ISwapeeButtonScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonScreen)} xyz.swapee.wc.AbstractSwapeeButtonScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonScreen} xyz.swapee.wc.SwapeeButtonScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonScreen` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonScreen
 */
xyz.swapee.wc.AbstractSwapeeButtonScreen = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonScreen.constructor&xyz.swapee.wc.SwapeeButtonScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonScreen.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonScreen.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonScreen|typeof xyz.swapee.wc.SwapeeButtonScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeButtonController|typeof xyz.swapee.wc.front.SwapeeButtonController)|(!xyz.swapee.wc.ISwapeeButtonDisplay|typeof xyz.swapee.wc.SwapeeButtonDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonScreen}
 */
xyz.swapee.wc.AbstractSwapeeButtonScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonScreen}
 */
xyz.swapee.wc.AbstractSwapeeButtonScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonScreen|typeof xyz.swapee.wc.SwapeeButtonScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeButtonController|typeof xyz.swapee.wc.front.SwapeeButtonController)|(!xyz.swapee.wc.ISwapeeButtonDisplay|typeof xyz.swapee.wc.SwapeeButtonDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonScreen}
 */
xyz.swapee.wc.AbstractSwapeeButtonScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonScreen|typeof xyz.swapee.wc.SwapeeButtonScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeButtonController|typeof xyz.swapee.wc.front.SwapeeButtonController)|(!xyz.swapee.wc.ISwapeeButtonDisplay|typeof xyz.swapee.wc.SwapeeButtonDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonScreen}
 */
xyz.swapee.wc.AbstractSwapeeButtonScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonScreen.Initialese[]) => xyz.swapee.wc.ISwapeeButtonScreen} xyz.swapee.wc.SwapeeButtonScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.SwapeeButtonMemory, !xyz.swapee.wc.front.SwapeeButtonInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeButtonDisplay.Settings, !xyz.swapee.wc.ISwapeeButtonDisplay.Queries, null, null>&xyz.swapee.wc.front.ISwapeeButtonController&xyz.swapee.wc.ISwapeeButtonDisplay)} xyz.swapee.wc.ISwapeeButtonScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ISwapeeButtonScreen
 */
xyz.swapee.wc.ISwapeeButtonScreen = class extends /** @type {xyz.swapee.wc.ISwapeeButtonScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ISwapeeButtonController.typeof&xyz.swapee.wc.ISwapeeButtonDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonScreen.Initialese>)} xyz.swapee.wc.SwapeeButtonScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeButtonScreen} xyz.swapee.wc.ISwapeeButtonScreen.typeof */
/**
 * A concrete class of _ISwapeeButtonScreen_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonScreen
 * @implements {xyz.swapee.wc.ISwapeeButtonScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonScreen.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonScreen = class extends /** @type {xyz.swapee.wc.SwapeeButtonScreen.constructor&xyz.swapee.wc.ISwapeeButtonScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonScreen}
 */
xyz.swapee.wc.SwapeeButtonScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeButtonScreen} */
xyz.swapee.wc.RecordISwapeeButtonScreen

/** @typedef {xyz.swapee.wc.ISwapeeButtonScreen} xyz.swapee.wc.BoundISwapeeButtonScreen */

/** @typedef {xyz.swapee.wc.SwapeeButtonScreen} xyz.swapee.wc.BoundSwapeeButtonScreen */

/**
 * Contains getters to cast the _ISwapeeButtonScreen_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonScreenCaster
 */
xyz.swapee.wc.ISwapeeButtonScreenCaster = class { }
/**
 * Cast the _ISwapeeButtonScreen_ instance into the _BoundISwapeeButtonScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonScreen}
 */
xyz.swapee.wc.ISwapeeButtonScreenCaster.prototype.asISwapeeButtonScreen
/**
 * Access the _SwapeeButtonScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonScreen}
 */
xyz.swapee.wc.ISwapeeButtonScreenCaster.prototype.superSwapeeButtonScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/70-ISwapeeButtonScreenBack.xml}  fd6829cfa0424cd0aa1111c13614077c */
/** @typedef {xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese} xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeButtonScreen)} xyz.swapee.wc.back.AbstractSwapeeButtonScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeButtonScreen} xyz.swapee.wc.back.SwapeeButtonScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeButtonScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeButtonScreen
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreen = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeButtonScreen.constructor&xyz.swapee.wc.back.SwapeeButtonScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeButtonScreen.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeButtonScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonScreen|typeof xyz.swapee.wc.back.SwapeeButtonScreen)|(!xyz.swapee.wc.back.ISwapeeButtonScreenAT|typeof xyz.swapee.wc.back.SwapeeButtonScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonScreen|typeof xyz.swapee.wc.back.SwapeeButtonScreen)|(!xyz.swapee.wc.back.ISwapeeButtonScreenAT|typeof xyz.swapee.wc.back.SwapeeButtonScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonScreen|typeof xyz.swapee.wc.back.SwapeeButtonScreen)|(!xyz.swapee.wc.back.ISwapeeButtonScreenAT|typeof xyz.swapee.wc.back.SwapeeButtonScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese[]) => xyz.swapee.wc.back.ISwapeeButtonScreen} xyz.swapee.wc.back.SwapeeButtonScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeButtonScreenCaster&xyz.swapee.wc.back.ISwapeeButtonScreenAT)} xyz.swapee.wc.back.ISwapeeButtonScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeButtonScreenAT} xyz.swapee.wc.back.ISwapeeButtonScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ISwapeeButtonScreen
 */
xyz.swapee.wc.back.ISwapeeButtonScreen = class extends /** @type {xyz.swapee.wc.back.ISwapeeButtonScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeButtonScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeButtonScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeButtonScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese>)} xyz.swapee.wc.back.SwapeeButtonScreen.constructor */
/**
 * A concrete class of _ISwapeeButtonScreen_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeButtonScreen
 * @implements {xyz.swapee.wc.back.ISwapeeButtonScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeButtonScreen = class extends /** @type {xyz.swapee.wc.back.SwapeeButtonScreen.constructor&xyz.swapee.wc.back.ISwapeeButtonScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeButtonScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreen}
 */
xyz.swapee.wc.back.SwapeeButtonScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonScreen} */
xyz.swapee.wc.back.RecordISwapeeButtonScreen

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonScreen} xyz.swapee.wc.back.BoundISwapeeButtonScreen */

/** @typedef {xyz.swapee.wc.back.SwapeeButtonScreen} xyz.swapee.wc.back.BoundSwapeeButtonScreen */

/**
 * Contains getters to cast the _ISwapeeButtonScreen_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeButtonScreenCaster
 */
xyz.swapee.wc.back.ISwapeeButtonScreenCaster = class { }
/**
 * Cast the _ISwapeeButtonScreen_ instance into the _BoundISwapeeButtonScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeButtonScreen}
 */
xyz.swapee.wc.back.ISwapeeButtonScreenCaster.prototype.asISwapeeButtonScreen
/**
 * Access the _SwapeeButtonScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeButtonScreen}
 */
xyz.swapee.wc.back.ISwapeeButtonScreenCaster.prototype.superSwapeeButtonScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/73-ISwapeeButtonScreenAR.xml}  1e2b754c8ae0cb874347763763a34a16 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeButtonScreen.Initialese} xyz.swapee.wc.front.ISwapeeButtonScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeButtonScreenAR)} xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeButtonScreenAR} xyz.swapee.wc.front.SwapeeButtonScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeButtonScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR
 */
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.constructor&xyz.swapee.wc.front.SwapeeButtonScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonScreenAR|typeof xyz.swapee.wc.front.SwapeeButtonScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeButtonScreen|typeof xyz.swapee.wc.SwapeeButtonScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonScreenAR|typeof xyz.swapee.wc.front.SwapeeButtonScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeButtonScreen|typeof xyz.swapee.wc.SwapeeButtonScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeButtonScreenAR|typeof xyz.swapee.wc.front.SwapeeButtonScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeButtonScreen|typeof xyz.swapee.wc.SwapeeButtonScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeButtonScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeButtonScreenAR.Initialese[]) => xyz.swapee.wc.front.ISwapeeButtonScreenAR} xyz.swapee.wc.front.SwapeeButtonScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeButtonScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeButtonScreen)} xyz.swapee.wc.front.ISwapeeButtonScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeButtonScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ISwapeeButtonScreenAR
 */
xyz.swapee.wc.front.ISwapeeButtonScreenAR = class extends /** @type {xyz.swapee.wc.front.ISwapeeButtonScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeButtonScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeButtonScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeButtonScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeButtonScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeButtonScreenAR.Initialese>)} xyz.swapee.wc.front.SwapeeButtonScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeButtonScreenAR} xyz.swapee.wc.front.ISwapeeButtonScreenAR.typeof */
/**
 * A concrete class of _ISwapeeButtonScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeButtonScreenAR
 * @implements {xyz.swapee.wc.front.ISwapeeButtonScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeButtonScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeButtonScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeButtonScreenAR = class extends /** @type {xyz.swapee.wc.front.SwapeeButtonScreenAR.constructor&xyz.swapee.wc.front.ISwapeeButtonScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeButtonScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeButtonScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeButtonScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeButtonScreenAR}
 */
xyz.swapee.wc.front.SwapeeButtonScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeButtonScreenAR} */
xyz.swapee.wc.front.RecordISwapeeButtonScreenAR

/** @typedef {xyz.swapee.wc.front.ISwapeeButtonScreenAR} xyz.swapee.wc.front.BoundISwapeeButtonScreenAR */

/** @typedef {xyz.swapee.wc.front.SwapeeButtonScreenAR} xyz.swapee.wc.front.BoundSwapeeButtonScreenAR */

/**
 * Contains getters to cast the _ISwapeeButtonScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeButtonScreenARCaster
 */
xyz.swapee.wc.front.ISwapeeButtonScreenARCaster = class { }
/**
 * Cast the _ISwapeeButtonScreenAR_ instance into the _BoundISwapeeButtonScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeButtonScreenAR}
 */
xyz.swapee.wc.front.ISwapeeButtonScreenARCaster.prototype.asISwapeeButtonScreenAR
/**
 * Access the _SwapeeButtonScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeButtonScreenAR}
 */
xyz.swapee.wc.front.ISwapeeButtonScreenARCaster.prototype.superSwapeeButtonScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/74-ISwapeeButtonScreenAT.xml}  1ac8f4ad9a7d6fd33b1c2179ec0ad09c */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeButtonScreenAT)} xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeButtonScreenAT} xyz.swapee.wc.back.SwapeeButtonScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeButtonScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.constructor&xyz.swapee.wc.back.SwapeeButtonScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonScreenAT|typeof xyz.swapee.wc.back.SwapeeButtonScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonScreenAT|typeof xyz.swapee.wc.back.SwapeeButtonScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeButtonScreenAT|typeof xyz.swapee.wc.back.SwapeeButtonScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeButtonScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese[]) => xyz.swapee.wc.back.ISwapeeButtonScreenAT} xyz.swapee.wc.back.SwapeeButtonScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeButtonScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ISwapeeButtonScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeButtonScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ISwapeeButtonScreenAT
 */
xyz.swapee.wc.back.ISwapeeButtonScreenAT = class extends /** @type {xyz.swapee.wc.back.ISwapeeButtonScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeButtonScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeButtonScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese>)} xyz.swapee.wc.back.SwapeeButtonScreenAT.constructor */
/**
 * A concrete class of _ISwapeeButtonScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeButtonScreenAT
 * @implements {xyz.swapee.wc.back.ISwapeeButtonScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeButtonScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeButtonScreenAT = class extends /** @type {xyz.swapee.wc.back.SwapeeButtonScreenAT.constructor&xyz.swapee.wc.back.ISwapeeButtonScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeButtonScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeButtonScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeButtonScreenAT}
 */
xyz.swapee.wc.back.SwapeeButtonScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonScreenAT} */
xyz.swapee.wc.back.RecordISwapeeButtonScreenAT

/** @typedef {xyz.swapee.wc.back.ISwapeeButtonScreenAT} xyz.swapee.wc.back.BoundISwapeeButtonScreenAT */

/** @typedef {xyz.swapee.wc.back.SwapeeButtonScreenAT} xyz.swapee.wc.back.BoundSwapeeButtonScreenAT */

/**
 * Contains getters to cast the _ISwapeeButtonScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeButtonScreenATCaster
 */
xyz.swapee.wc.back.ISwapeeButtonScreenATCaster = class { }
/**
 * Cast the _ISwapeeButtonScreenAT_ instance into the _BoundISwapeeButtonScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeButtonScreenAT}
 */
xyz.swapee.wc.back.ISwapeeButtonScreenATCaster.prototype.asISwapeeButtonScreenAT
/**
 * Access the _SwapeeButtonScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeButtonScreenAT}
 */
xyz.swapee.wc.back.ISwapeeButtonScreenATCaster.prototype.superSwapeeButtonScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-button/SwapeeButton.mvc/design/80-ISwapeeButtonGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ISwapeeButtonDisplay.Initialese} xyz.swapee.wc.ISwapeeButtonGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeButtonGPU)} xyz.swapee.wc.AbstractSwapeeButtonGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeButtonGPU} xyz.swapee.wc.SwapeeButtonGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonGPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeButtonGPU
 */
xyz.swapee.wc.AbstractSwapeeButtonGPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeButtonGPU.constructor&xyz.swapee.wc.SwapeeButtonGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeButtonGPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeButtonGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeButtonGPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonGPU|typeof xyz.swapee.wc.SwapeeButtonGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeButtonDisplay|typeof xyz.swapee.wc.back.SwapeeButtonDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeButtonGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeButtonGPU}
 */
xyz.swapee.wc.AbstractSwapeeButtonGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonGPU}
 */
xyz.swapee.wc.AbstractSwapeeButtonGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonGPU|typeof xyz.swapee.wc.SwapeeButtonGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeButtonDisplay|typeof xyz.swapee.wc.back.SwapeeButtonDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonGPU}
 */
xyz.swapee.wc.AbstractSwapeeButtonGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeButtonGPU|typeof xyz.swapee.wc.SwapeeButtonGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeButtonDisplay|typeof xyz.swapee.wc.back.SwapeeButtonDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeButtonGPU}
 */
xyz.swapee.wc.AbstractSwapeeButtonGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeButtonGPU.Initialese[]) => xyz.swapee.wc.ISwapeeButtonGPU} xyz.swapee.wc.SwapeeButtonGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeButtonGPUCaster&com.webcircuits.IBrowserView<.!SwapeeButtonMemory,>&xyz.swapee.wc.back.ISwapeeButtonDisplay)} xyz.swapee.wc.ISwapeeButtonGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!SwapeeButtonMemory,>} com.webcircuits.IBrowserView<.!SwapeeButtonMemory,>.typeof */
/**
 * Handles the periphery of the _ISwapeeButtonDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ISwapeeButtonGPU
 */
xyz.swapee.wc.ISwapeeButtonGPU = class extends /** @type {xyz.swapee.wc.ISwapeeButtonGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!SwapeeButtonMemory,>.typeof&xyz.swapee.wc.back.ISwapeeButtonDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeButtonGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeButtonGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeButtonGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonGPU.Initialese>)} xyz.swapee.wc.SwapeeButtonGPU.constructor */
/**
 * A concrete class of _ISwapeeButtonGPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeButtonGPU
 * @implements {xyz.swapee.wc.ISwapeeButtonGPU} Handles the periphery of the _ISwapeeButtonDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeButtonGPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeButtonGPU = class extends /** @type {xyz.swapee.wc.SwapeeButtonGPU.constructor&xyz.swapee.wc.ISwapeeButtonGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeButtonGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeButtonGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeButtonGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeButtonGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeButtonGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeButtonGPU}
 */
xyz.swapee.wc.SwapeeButtonGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeButtonGPU.
 * @interface xyz.swapee.wc.ISwapeeButtonGPUFields
 */
xyz.swapee.wc.ISwapeeButtonGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ISwapeeButtonGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeButtonGPU} */
xyz.swapee.wc.RecordISwapeeButtonGPU

/** @typedef {xyz.swapee.wc.ISwapeeButtonGPU} xyz.swapee.wc.BoundISwapeeButtonGPU */

/** @typedef {xyz.swapee.wc.SwapeeButtonGPU} xyz.swapee.wc.BoundSwapeeButtonGPU */

/**
 * Contains getters to cast the _ISwapeeButtonGPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeButtonGPUCaster
 */
xyz.swapee.wc.ISwapeeButtonGPUCaster = class { }
/**
 * Cast the _ISwapeeButtonGPU_ instance into the _BoundISwapeeButtonGPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeButtonGPU}
 */
xyz.swapee.wc.ISwapeeButtonGPUCaster.prototype.asISwapeeButtonGPU
/**
 * Access the _SwapeeButtonGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeButtonGPU}
 */
xyz.swapee.wc.ISwapeeButtonGPUCaster.prototype.superSwapeeButtonGPU

// nss:xyz.swapee.wc
/* @typal-end */