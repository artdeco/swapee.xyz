/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ISwapeeButtonComputer': {
  'id': 31791304451,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.SwapeeButtonMemoryPQs': {
  'id': 31791304452,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeButtonOuterCore': {
  'id': 31791304453,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeButtonInputsPQs': {
  'id': 31791304454,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeButtonPort': {
  'id': 31791304455,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeButtonPort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeButtonPortInterface': {
  'id': 31791304456,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonCore': {
  'id': 31791304457,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeButtonCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeButtonProcessor': {
  'id': 31791304458,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButton': {
  'id': 31791304459,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonBuffer': {
  'id': 317913044510,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonHtmlComponent': {
  'id': 317913044511,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonElement': {
  'id': 317913044512,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ISwapeeButtonElementPort': {
  'id': 317913044513,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonDesigner': {
  'id': 317913044514,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeButtonGPU': {
  'id': 317913044515,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonDisplay': {
  'id': 317913044516,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeButtonVdusPQs': {
  'id': 317913044517,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeButtonDisplay': {
  'id': 317913044518,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ISwapeeButtonController': {
  'id': 317913044519,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ISwapeeButtonController': {
  'id': 317913044520,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonController': {
  'id': 317913044521,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonControllerAR': {
  'id': 317913044522,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeButtonControllerAT': {
  'id': 317913044523,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonScreen': {
  'id': 317913044524,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonScreen': {
  'id': 317913044525,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeButtonScreenAR': {
  'id': 317913044526,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonScreenAT': {
  'id': 317913044527,
  'symbols': {},
  'methods': {}
 }
})