import {SwapeeButtonMemoryPQs} from './SwapeeButtonMemoryPQs'
export const SwapeeButtonMemoryQPs=/**@type {!xyz.swapee.wc.SwapeeButtonMemoryQPs}*/(Object.keys(SwapeeButtonMemoryPQs)
 .reduce((a,k)=>{a[SwapeeButtonMemoryPQs[k]]=k;return a},{}))