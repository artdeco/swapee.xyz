import {SwapeeButtonInputsPQs} from './SwapeeButtonInputsPQs'
export const SwapeeButtonInputsQPs=/**@type {!xyz.swapee.wc.SwapeeButtonInputsQPs}*/(Object.keys(SwapeeButtonInputsPQs)
 .reduce((a,k)=>{a[SwapeeButtonInputsPQs[k]]=k;return a},{}))