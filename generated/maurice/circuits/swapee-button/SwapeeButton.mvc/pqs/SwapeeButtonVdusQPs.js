import {SwapeeButtonVdusPQs} from './SwapeeButtonVdusPQs'
export const SwapeeButtonVdusQPs=/**@type {!xyz.swapee.wc.SwapeeButtonVdusQPs}*/(Object.keys(SwapeeButtonVdusPQs)
 .reduce((a,k)=>{a[SwapeeButtonVdusPQs[k]]=k;return a},{}))