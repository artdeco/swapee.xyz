/** @type {xyz.swapee.wc.ISwapeeButtonElement._server} */
export default function server(_,{child:child}) {
 return (<div $id="SwapeeButton">
  <span $id="BuLa" dangerouslySetInnerHTML={{__html:child.trim()}} />
 </div>)
}