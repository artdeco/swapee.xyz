import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import SwapeeButtonServerController from '../SwapeeButtonServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractSwapeeButtonElement from '../../gen/AbstractSwapeeButtonElement'

/** @extends {xyz.swapee.wc.SwapeeButtonElement} */
export default class SwapeeButtonElement extends AbstractSwapeeButtonElement.implements(
 SwapeeButtonServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ISwapeeButtonElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ISwapeeButtonElement}*/({
   classesMap: true,
   rootSelector:     `.SwapeeButton`,
   stylesheet:       'html/styles/SwapeeButton.css',
   blockName:        'html/SwapeeButtonBlock.html',
  }),
){}

// thank you for using web circuits
