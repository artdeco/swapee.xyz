import SwapeeButtonHtmlController from '../SwapeeButtonHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractSwapeeButtonHtmlComponent} from '../../gen/AbstractSwapeeButtonHtmlComponent'

/** @extends {xyz.swapee.wc.SwapeeButtonHtmlComponent} */
export default class extends AbstractSwapeeButtonHtmlComponent.implements(
 SwapeeButtonHtmlController,
 IntegratedComponentInitialiser,
){}