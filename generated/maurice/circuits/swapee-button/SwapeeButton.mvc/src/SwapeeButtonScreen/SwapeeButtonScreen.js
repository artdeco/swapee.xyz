import deduceInputs from './methods/deduce-inputs'
import AbstractSwapeeButtonControllerAT from '../../gen/AbstractSwapeeButtonControllerAT'
import SwapeeButtonDisplay from '../SwapeeButtonDisplay'
import AbstractSwapeeButtonScreen from '../../gen/AbstractSwapeeButtonScreen'

/** @extends {xyz.swapee.wc.SwapeeButtonScreen} */
export default class extends AbstractSwapeeButtonScreen.implements(
 AbstractSwapeeButtonControllerAT,
 SwapeeButtonDisplay,
 /**@type {!xyz.swapee.wc.ISwapeeButtonScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ISwapeeButtonScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:3179130445,
 }),
){}