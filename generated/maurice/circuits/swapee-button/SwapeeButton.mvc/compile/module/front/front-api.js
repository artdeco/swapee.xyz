import { SwapeeButtonDisplay, SwapeeButtonScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.SwapeeButtonDisplay} */
export { SwapeeButtonDisplay }
/** @lazy @api {xyz.swapee.wc.SwapeeButtonScreen} */
export { SwapeeButtonScreen }