/**
 * Display for presenting information from the _ISwapeeButton_.
 * @extends {xyz.swapee.wc.SwapeeButtonDisplay}
 */
class SwapeeButtonDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.SwapeeButtonScreen}
 */
class SwapeeButtonScreen extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeButtonDisplay = SwapeeButtonDisplay
module.exports.SwapeeButtonScreen = SwapeeButtonScreen