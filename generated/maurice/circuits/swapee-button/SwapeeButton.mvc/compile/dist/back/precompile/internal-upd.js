import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {3179130445} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const b=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=b["372700389810"],e=b["372700389811"];function f(a,d,l,Z){return b["372700389812"](a,d,l,Z,!1,void 0)};function g(){}g.prototype={};class aa{}class h extends f(aa,31791304458,null,{F:1,U:2}){}h[e]=[g];
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ba=k["61505580523"],ca=k["615055805212"],da=k["615055805218"],ea=k["615055805221"],fa=k["615055805223"],m=k["615055805233"];const n={active:"c76a5",h:"e0542"};function p(){}p.prototype={};class ha{}class q extends f(ha,31791304457,null,{s:1,L:2}){}function r(){}r.prototype={};function t(){this.model={active:!1,h:!1}}class ia{}class u extends f(ia,31791304453,t,{C:1,R:2}){}u[e]=[r,{constructor(){m(this.model,"",n)}}];q[e]=[{},p,u];

const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ja=v.IntegratedController,ka=v.Parametric;
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const la=w.IntegratedComponentInitialiser,ma=w.IntegratedComponent;function x(){}x.prototype={};class na{}class y extends f(na,31791304451,null,{m:1,J:2}){}y[e]=[x,da];const z={regulate:ca({active:Boolean,h:Boolean})};const A={...n};function B(){}B.prototype={};function oa(){const a={model:null};t.call(a);this.inputs=a.model}class pa{}class C extends f(pa,31791304455,oa,{D:1,T:2}){}function D(){}C[e]=[D.prototype={},B,ka,D.prototype={constructor(){m(this.inputs,"",A)}}];function E(){}E.prototype={};class qa{}class F extends f(qa,317913044519,null,{i:1,j:2}){}F[e]=[{},E,z,ja,{get Port(){return C}}];function G(){}G.prototype={};class ra{}class H extends f(ra,31791304459,null,{l:1,I:2}){}H[e]=[G,q,h,ma,y,F];
const I=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const sa=I["12817393923"],ta=I["12817393924"],ua=I["12817393925"],va=I["12817393926"];function J(){}J.prototype={};class wa{}class K extends f(wa,317913044522,null,{o:1,K:2}){}K[e]=[J,va,{allocator(){this.methods={}}}];function L(){}L.prototype={};class xa{}class M extends f(xa,317913044521,null,{i:1,j:2}){}M[e]=[L,F,K,sa];var N=class extends M.implements(){};function O(){}O.prototype={};class ya{}class P extends f(ya,317913044518,null,{u:1,M:2}){}P[e]=[O,ta,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{g:a})}},{[c]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];const Q={g:"ie9d1"};const za=Object.keys(Q).reduce((a,d)=>{a[Q[d]]=d;return a},{});function R(){}R.prototype={};class Aa{}class S extends f(Aa,317913044515,null,{v:1,O:2}){}S[e]=[R,{vdusQPs:za,memoryPQs:n},P,ba];function T(){}T.prototype={};class Ba{}class U extends f(Ba,317913044527,null,{H:1,W:2}){}U[e]=[T,ua];function V(){}V.prototype={};class Ca{}class W extends f(Ca,317913044525,null,{G:1,V:2}){}W[e]=[V,U];const Da=Object.keys(A).reduce((a,d)=>{a[A[d]]=d;return a},{});function X(){}X.prototype={};class Ea{static mvc(a,d,l){return fa(this,a,d,null,l)}}class Y extends f(Ea,317913044511,null,{A:1,P:2}){}Y[e]=[X,ea,H,S,W,{inputsQPs:Da}];var Fa=class extends Y.implements(N,la){};module.exports["31791304450"]=H;module.exports["31791304451"]=H;module.exports["31791304453"]=C;module.exports["31791304454"]=F;module.exports["317913044510"]=Fa;module.exports["317913044511"]=z;module.exports["317913044530"]=y;module.exports["317913044561"]=N;
/*! @embed-object-end {3179130445} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule