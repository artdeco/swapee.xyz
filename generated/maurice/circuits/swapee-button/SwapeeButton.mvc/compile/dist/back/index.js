/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButton` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeButton}
 */
class AbstractSwapeeButton extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeButton_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeButtonPort}
 */
class SwapeeButtonPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonController}
 */
class AbstractSwapeeButtonController extends (class {/* lazy-loaded */}) {}
/**
 * The _ISwapeeButton_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.SwapeeButtonHtmlComponent}
 */
class SwapeeButtonHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeButtonBuffer}
 */
class SwapeeButtonBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonComputer}
 */
class AbstractSwapeeButtonComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.SwapeeButtonController}
 */
class SwapeeButtonController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeButton = AbstractSwapeeButton
module.exports.SwapeeButtonPort = SwapeeButtonPort
module.exports.AbstractSwapeeButtonController = AbstractSwapeeButtonController
module.exports.SwapeeButtonHtmlComponent = SwapeeButtonHtmlComponent
module.exports.SwapeeButtonBuffer = SwapeeButtonBuffer
module.exports.AbstractSwapeeButtonComputer = AbstractSwapeeButtonComputer
module.exports.SwapeeButtonController = SwapeeButtonController