import Module from './element'

/**@extends {xyz.swapee.wc.AbstractSwapeeButton}*/
export class AbstractSwapeeButton extends Module['31791304451'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButton} */
AbstractSwapeeButton.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeButtonPort} */
export const SwapeeButtonPort=Module['31791304453']
/**@extends {xyz.swapee.wc.AbstractSwapeeButtonController}*/
export class AbstractSwapeeButtonController extends Module['31791304454'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonController} */
AbstractSwapeeButtonController.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeButtonElement} */
export const SwapeeButtonElement=Module['31791304458']
/** @type {typeof xyz.swapee.wc.SwapeeButtonBuffer} */
export const SwapeeButtonBuffer=Module['317913044511']
/**@extends {xyz.swapee.wc.AbstractSwapeeButtonComputer}*/
export class AbstractSwapeeButtonComputer extends Module['317913044530'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeButtonComputer} */
AbstractSwapeeButtonComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeButtonController} */
export const SwapeeButtonController=Module['317913044561']