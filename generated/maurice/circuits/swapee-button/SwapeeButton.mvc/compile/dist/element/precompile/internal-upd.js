import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {3179130445} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const a=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=a["372700389811"];function e(b,d,f,k){return a["372700389812"](b,d,f,k,!1,void 0)};function g(){}g.prototype={};class h{}class l extends e(h,31791304458,null,{A:1,R:2}){}l[c]=[g];

const m=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const n=m["615055805212"],p=m["615055805218"],q=m["615055805233"];const r={active:"c76a5",g:"e0542"};function t(){}t.prototype={};class u{}class v extends e(u,31791304457,null,{m:1,I:2}){}function w(){}w.prototype={};function x(){this.model={active:!1,g:!1}}class y{}class z extends e(y,31791304453,x,{u:1,M:2}){}z[c]=[w,{constructor(){q(this.model,"",r)}}];v[c]=[{},t,z];
const A=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const B=A.IntegratedComponentInitialiser,C=A.IntegratedComponent,D=A["95173443851"];function E(){}E.prototype={};class F{}class G extends e(F,31791304451,null,{j:1,G:2}){}G[c]=[E,p];const H={regulate:n({active:Boolean,g:Boolean})};
const I=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const aa=I.IntegratedController,ba=I.Parametric;const J={...r};function K(){}K.prototype={};function ca(){const b={model:null};x.call(b);this.inputs=b.model}class da{}class L extends e(da,31791304455,ca,{v:1,P:2}){}function M(){}L[c]=[M.prototype={},K,ba,M.prototype={constructor(){q(this.inputs,"",J)}}];function N(){}N.prototype={};class ea{}class O extends e(ea,317913044519,null,{l:1,H:2}){}O[c]=[{},N,H,aa,{get Port(){return L}}];function P(){}P.prototype={};class fa{}class Q extends e(fa,31791304459,null,{i:1,F:2}){}Q[c]=[P,v,l,C,G,O];function ha(){return null};const R=require(eval('"@type.engineering/web-computing"')).h;function ia(b,{child:d}){return R("div",{$id:"SwapeeButton"},R("span",{$id:"BuLa",dangerouslySetInnerHTML:{__html:d.trim()}}))};const ja=require(eval('"@type.engineering/web-computing"')).h;function ka(){return ja("div",{$id:"SwapeeButton"})};var S=class extends O.implements(){};require("https");require("http");const la=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(b){}la("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const ma=T.ElementBase,na=T.HTMLBlocker;const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function oa(){this.inputs={noSolder:!1,D:{}}}class pa{}class W extends e(pa,317913044513,oa,{s:1,K:2}){}W[c]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"bu-la-opts":void 0})}}];function X(){}X.prototype={};class qa{}class Y extends e(qa,317913044512,null,{o:1,J:2}){}function Z(){}
Y[c]=[X,ma,Z.prototype={calibrate:function({":no-solder":b,":active":d,":hover":f}){const {attributes:{"no-solder":k,active:ra,hover:sa}}=this;return{...(void 0===k?{"no-solder":b}:{}),...(void 0===ra?{active:d}:{}),...(void 0===sa?{hover:f}:{})}}},Z.prototype={calibrate:({"no-solder":b,active:d,hover:f})=>({noSolder:b,active:d,g:f})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return U("div",{$id:"SwapeeButton"},U("vdu",{$id:"BuLa"}))}},Z.prototype={customs:{BORDER:"2px",
BACKGROUND:"white",COLOR:"initial",CLIP_PATH:"10px",INNER_BORDER_COLOR:"lightgrey"},inputsPQs:J,vdus:{BuLa:"ie9d1"}},C,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder active hover no-solder :no-solder :active :hover fe646 bd74d c76a5 e0542 BORDER BACKGROUND COLOR CLIP_PATH INNER_BORDER_COLOR children".split(" "))})},get Port(){return W}}];Y[c]=[Q,{rootId:"SwapeeButton",__$id:3179130445,fqn:"xyz.swapee.wc.ISwapeeButton",maurice_element_v3:!0}];class ta extends Y.implements(S,D,na,B,{solder:ha,server:ia,render:ka},{classesMap:!0,rootSelector:".SwapeeButton",stylesheet:"html/styles/SwapeeButton.css",blockName:"html/SwapeeButtonBlock.html"}){};module.exports["31791304450"]=Q;module.exports["31791304451"]=Q;module.exports["31791304453"]=L;module.exports["31791304454"]=O;module.exports["31791304458"]=ta;module.exports["317913044511"]=H;module.exports["317913044530"]=G;module.exports["317913044561"]=S;
/*! @embed-object-end {3179130445} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule