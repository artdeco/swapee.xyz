/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButton` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeButton}
 */
class AbstractSwapeeButton extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeButton_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeButtonPort}
 */
class SwapeeButtonPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonController}
 */
class AbstractSwapeeButtonController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ISwapeeButton_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.SwapeeButtonElement}
 */
class SwapeeButtonElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeButtonBuffer}
 */
class SwapeeButtonBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeButtonComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeButtonComputer}
 */
class AbstractSwapeeButtonComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.SwapeeButtonController}
 */
class SwapeeButtonController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeButton = AbstractSwapeeButton
module.exports.SwapeeButtonPort = SwapeeButtonPort
module.exports.AbstractSwapeeButtonController = AbstractSwapeeButtonController
module.exports.SwapeeButtonElement = SwapeeButtonElement
module.exports.SwapeeButtonBuffer = SwapeeButtonBuffer
module.exports.AbstractSwapeeButtonComputer = AbstractSwapeeButtonComputer
module.exports.SwapeeButtonController = SwapeeButtonController

Object.defineProperties(module.exports, {
 'AbstractSwapeeButton': {get: () => require('./precompile/internal')[31791304451]},
 [31791304451]: {get: () => module.exports['AbstractSwapeeButton']},
 'SwapeeButtonPort': {get: () => require('./precompile/internal')[31791304453]},
 [31791304453]: {get: () => module.exports['SwapeeButtonPort']},
 'AbstractSwapeeButtonController': {get: () => require('./precompile/internal')[31791304454]},
 [31791304454]: {get: () => module.exports['AbstractSwapeeButtonController']},
 'SwapeeButtonElement': {get: () => require('./precompile/internal')[31791304458]},
 [31791304458]: {get: () => module.exports['SwapeeButtonElement']},
 'SwapeeButtonBuffer': {get: () => require('./precompile/internal')[317913044511]},
 [317913044511]: {get: () => module.exports['SwapeeButtonBuffer']},
 'AbstractSwapeeButtonComputer': {get: () => require('./precompile/internal')[317913044530]},
 [317913044530]: {get: () => module.exports['AbstractSwapeeButtonComputer']},
 'SwapeeButtonController': {get: () => require('./precompile/internal')[317913044561]},
 [317913044561]: {get: () => module.exports['SwapeeButtonController']},
})