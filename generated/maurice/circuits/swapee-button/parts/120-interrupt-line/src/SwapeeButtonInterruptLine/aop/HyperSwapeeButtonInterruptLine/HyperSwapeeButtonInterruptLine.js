import AbstractHyperSwapeeButtonInterruptLine from '../../../../gen/AbstractSwapeeButtonInterruptLine/hyper/AbstractHyperSwapeeButtonInterruptLine'
import SwapeeButtonInterruptLine from '../../SwapeeButtonInterruptLine'
import SwapeeButtonInterruptLineGeneralAspects from '../SwapeeButtonInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperSwapeeButtonInterruptLine} */
export default class extends AbstractHyperSwapeeButtonInterruptLine
 .consults(
  SwapeeButtonInterruptLineGeneralAspects,
 )
 .implements(
  SwapeeButtonInterruptLine,
 )
{}