/** @extends {xyz.swapee.wc.AbstractSwapeeButton} */
export default class AbstractSwapeeButton extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractSwapeeButtonComputer} */
export class AbstractSwapeeButtonComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeButtonController} */
export class AbstractSwapeeButtonController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeButtonPort} */
export class SwapeeButtonPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeButtonView} */
export class AbstractSwapeeButtonView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeButtonElement} */
export class AbstractSwapeeButtonElement extends (<element v3 html mv>
 <block src="./SwapeeButton.mvc/src/SwapeeButtonElement/methods/render.jsx" />
 <inducer src="./SwapeeButton.mvc/src/SwapeeButtonElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeButtonHtmlComponent} */
export class AbstractSwapeeButtonHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>