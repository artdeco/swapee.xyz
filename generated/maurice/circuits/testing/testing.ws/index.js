const d=new Date
let _TestingPage,_testingArcsIds,_testingMethodsIds
// _getTesting

// const PROD=true
const PROD=false

;({
 TestingPage:_TestingPage,
 // getTesting:_getTesting,
 testingArcsIds:_testingArcsIds,
 testingMethodsIds:_testingMethodsIds,
}=require(PROD?'xyz/swapee/rc/testing/prod':'./testing.page'))

const dd=new Date
console.log(`${PROD?`📦`:`📝`} Loaded %s in %sms`,'testing',dd.getTime()-d.getTime())

import 'xyz/swapee/rc/testing'

export const TestingPage=/**@type {typeof xyz.swapee.rc.TestingPage}*/(_TestingPage)
export const testingArcsIds=/**@type {typeof xyz.swapee.rc.testingArcsIds}*/(_testingArcsIds)
export const testingMethodsIds=/**@type {typeof xyz.swapee.rc.testingMethodsIds}*/(_testingMethodsIds)
// export const getTesting=/**@type {xyz.swapee.rc.getTesting}*/(_getTesting)