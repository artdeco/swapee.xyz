import AbstractHyperTestingInterruptLine from '../../../../gen/AbstractTestingInterruptLine/hyper/AbstractHyperTestingInterruptLine'
import TestingInterruptLine from '../../TestingInterruptLine'
import TestingInterruptLineGeneralAspects from '../TestingInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperTestingInterruptLine} */
export default class extends AbstractHyperTestingInterruptLine
 .consults(
  TestingInterruptLineGeneralAspects,
 )
 .implements(
  TestingInterruptLine,
 )
{}