/** @extends {xyz.swapee.wc.AbstractTesting} */
export default class AbstractTesting extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractTestingComputer} */
export class AbstractTestingComputer extends (<computer>
   <adapter name="adaptAnyLoading">
    <xyz.swapee.wc.ITestingCore loadingChangellyFloatingOffer loadingChangenowOffer loadingChangellyFixedOffer />
    <outputs>
     <xyz.swapee.wc.ITestingCore anyLoading />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTestingController} */
export class AbstractTestingController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTestingPort} */
export class TestingPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTestingView} */
export class AbstractTestingView extends (<view>
  <classes>

  </classes>

  <children>
   <input name="AmountIn" />
   <button name="IncreaseBu" />
   <button name="DecreaseBu" />
   <span name="RatesLoIn" />
   <span name="RatesLoEr" />
   <div name="ChangellyFloatingOffer" />
   <span name="ChangellyFloatingOfferAmount" />
   <div name="ChangellyFixedOffer" />
   <span name="ChangellyFixedOfferAmount" />

  </children>
</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTestingElement} */
export class AbstractTestingElement extends (<element v3 html mv>
 <block src="./Testing.mvc/src/TestingElement/methods/render.jsx" />
 <inducer src="./Testing.mvc/src/TestingElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTestingHtmlComponent} */
export class AbstractTestingHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>