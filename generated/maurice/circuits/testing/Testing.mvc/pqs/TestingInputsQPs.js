import {TestingInputsPQs} from './TestingInputsPQs'
export const TestingInputsQPs=/**@type {!xyz.swapee.wc.TestingInputsQPs}*/(Object.keys(TestingInputsPQs)
 .reduce((a,k)=>{a[TestingInputsPQs[k]]=k;return a},{}))