export const TestingMemoryPQs=/**@type {!xyz.swapee.wc.TestingMemoryPQs}*/({
 core:'a74ad',
 clicks:'c9e9e',
 amountIn:'88da7',
 currencyIn:'f3088',
 currencyOut:'98dbb',
 host:'67b3d',
 changeNow:'94af8',
})