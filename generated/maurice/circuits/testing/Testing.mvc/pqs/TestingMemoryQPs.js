import {TestingMemoryPQs} from './TestingMemoryPQs'
export const TestingMemoryQPs=/**@type {!xyz.swapee.wc.TestingMemoryQPs}*/(Object.keys(TestingMemoryPQs)
 .reduce((a,k)=>{a[TestingMemoryPQs[k]]=k;return a},{}))