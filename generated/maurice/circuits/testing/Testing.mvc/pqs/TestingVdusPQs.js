export const TestingVdusPQs=/**@type {!xyz.swapee.wc.TestingVdusPQs}*/({
 RatesLoIn:'g1ee3',
 RatesLoEr:'g1ee4',
 ChangellyFloatingOffer:'g1ee7',
 ChangellyFloatingOfferAmount:'g1ee8',
 AmountIn:'g1ee9',
 IncreaseBu:'g1ee10',
 DecreaseBu:'g1ee11',
 ChangellyFixedOffer:'g1ee12',
 ChangellyFixedOfferAmount:'g1ee13',
})