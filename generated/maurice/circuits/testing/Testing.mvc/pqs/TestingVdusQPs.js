import {TestingVdusPQs} from './TestingVdusPQs'
export const TestingVdusQPs=/**@type {!xyz.swapee.wc.TestingVdusQPs}*/(Object.keys(TestingVdusPQs)
 .reduce((a,k)=>{a[TestingVdusPQs[k]]=k;return a},{}))