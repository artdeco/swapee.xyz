import {TestingCachePQs} from './TestingCachePQs'
export const TestingCacheQPs=/**@type {!xyz.swapee.wc.TestingCacheQPs}*/(Object.keys(TestingCachePQs)
 .reduce((a,k)=>{a[TestingCachePQs[k]]=k;return a},{}))