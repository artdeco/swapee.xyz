/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ITestingComputer': {
  'id': 62696530441,
  'symbols': {},
  'methods': {
   'compute': 2,
   'adaptChangenowOffer': 5,
   'adaptAnyLoading': 6,
   'adaptChangellyFloatingOffer': 7,
   'adaptChangellyFixedOffer': 8
  }
 },
 'xyz.swapee.wc.TestingMemoryPQs': {
  'id': 62696530442,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingOuterCore': {
  'id': 62696530443,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TestingInputsPQs': {
  'id': 62696530444,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingPort': {
  'id': 62696530445,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTestingPort': 2
  }
 },
 'xyz.swapee.wc.TestingCachePQs': {
  'id': 62696530446,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingCore': {
  'id': 62696530447,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTestingCore': 2
  }
 },
 'xyz.swapee.wc.ITestingProcessor': {
  'id': 62696530448,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITesting': {
  'id': 62696530449,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingBuffer': {
  'id': 626965304410,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingHtmlComponent': {
  'id': 626965304411,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingElement': {
  'id': 626965304412,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ITestingElementPort': {
  'id': 626965304413,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingDesigner': {
  'id': 626965304414,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITestingGPU': {
  'id': 626965304415,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingDisplay': {
  'id': 626965304416,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TestingVdusPQs': {
  'id': 626965304417,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITestingDisplay': {
  'id': 626965304418,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ITestingController': {
  'id': 626965304419,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setCore': 4,
   'unsetCore': 5,
   'setAmountIn': 6,
   'unsetAmountIn': 7,
   'setCurrencyIn': 8,
   'unsetCurrencyIn': 9,
   'setCurrencyOut': 10,
   'unsetCurrencyOut': 11,
   'loadChangenowOffer': 14,
   'loadChangellyFloatingOffer': 15,
   'increaseAmount': 16,
   'decreaseAmount': 18,
   'loadChangellyFixedOffer': 19
  }
 },
 'xyz.swapee.wc.front.ITestingController': {
  'id': 626965304420,
  'symbols': {},
  'methods': {
   'setCore': 3,
   'unsetCore': 4,
   'setAmountIn': 5,
   'unsetAmountIn': 6,
   'setCurrencyIn': 7,
   'unsetCurrencyIn': 8,
   'setCurrencyOut': 9,
   'unsetCurrencyOut': 10,
   'loadChangenowOffer': 13,
   'loadChangellyFloatingOffer': 14,
   'increaseAmount': 15,
   'decreaseAmount': 17,
   'loadChangellyFixedOffer': 18
  }
 },
 'xyz.swapee.wc.back.ITestingController': {
  'id': 626965304421,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingControllerAR': {
  'id': 626965304422,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITestingControllerAT': {
  'id': 626965304423,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingScreen': {
  'id': 626965304424,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingScreen': {
  'id': 626965304425,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITestingScreenAR': {
  'id': 626965304426,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingScreenAT': {
  'id': 626965304427,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingRadio': {
  'id': 626965304428,
  'symbols': {},
  'methods': {
   'adaptLoadChangenowOffer': 3,
   'adaptLoadChangellyFloatingOffer': 4,
   'adaptLoadChangellyFixedOffer': 5
  }
 },
 'xyz.swapee.wc.ITestingService': {
  'id': 626965304429,
  'symbols': {},
  'methods': {
   'filterChangenowOffer': 3,
   'filterChangellyFloatingOffer': 4,
   'filterChangellyFixedOffer': 5
  }
 }
})