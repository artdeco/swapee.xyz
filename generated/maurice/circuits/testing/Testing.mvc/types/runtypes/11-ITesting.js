/** @const {?} */ $xyz.swapee.wc.ITesting
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.TestingEnv filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @record */
$xyz.swapee.wc.TestingEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.ITesting} */
$xyz.swapee.wc.TestingEnv.prototype.testing
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.TestingEnv}
 */
xyz.swapee.wc.TestingEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting.Initialese filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {xyz.swapee.wc.ITestingProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITestingComputer.Initialese}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
$xyz.swapee.wc.ITesting.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITesting.Initialese} */
xyz.swapee.wc.ITesting.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITesting
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITestingFields filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @interface */
$xyz.swapee.wc.ITestingFields = function() {}
/** @type {!xyz.swapee.wc.ITesting.Pinout} */
$xyz.swapee.wc.ITestingFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingFields}
 */
xyz.swapee.wc.ITestingFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITestingCaster filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @interface */
$xyz.swapee.wc.ITestingCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITesting} */
$xyz.swapee.wc.ITestingCaster.prototype.asITesting
/** @type {!xyz.swapee.wc.BoundTesting} */
$xyz.swapee.wc.ITestingCaster.prototype.superTesting
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingCaster}
 */
xyz.swapee.wc.ITestingCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingCaster}
 * @extends {xyz.swapee.wc.ITestingProcessor}
 * @extends {xyz.swapee.wc.ITestingComputer}
 * @extends {xyz.swapee.wc.ITestingController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, null>}
 */
$xyz.swapee.wc.ITesting = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITesting}
 */
xyz.swapee.wc.ITesting

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.Testing filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init
 * @implements {xyz.swapee.wc.ITesting}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITesting.Initialese>}
 */
$xyz.swapee.wc.Testing = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.Testing
/** @type {function(new: xyz.swapee.wc.ITesting, ...!xyz.swapee.wc.ITesting.Initialese)} */
xyz.swapee.wc.Testing.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.Testing.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.AbstractTesting filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init
 * @extends {xyz.swapee.wc.Testing}
 */
$xyz.swapee.wc.AbstractTesting = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTesting}
 */
xyz.swapee.wc.AbstractTesting
/** @type {function(new: xyz.swapee.wc.AbstractTesting)} */
xyz.swapee.wc.AbstractTesting.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.Testing}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTesting.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTesting}
 */
xyz.swapee.wc.AbstractTesting.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.__extend
/**
 * @param {...((!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.continues
/**
 * @param {...((!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.TestingConstructor filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @typedef {function(new: xyz.swapee.wc.ITesting, ...!xyz.swapee.wc.ITesting.Initialese)} */
xyz.swapee.wc.TestingConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting.MVCOptions filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @record */
$xyz.swapee.wc.ITesting.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.ITesting.Pinout)|undefined} */
$xyz.swapee.wc.ITesting.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.ITesting.Pinout)|undefined} */
$xyz.swapee.wc.ITesting.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.ITesting.Pinout} */
$xyz.swapee.wc.ITesting.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.TestingMemory)|undefined} */
$xyz.swapee.wc.ITesting.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.TestingClasses)|undefined} */
$xyz.swapee.wc.ITesting.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.ITesting.MVCOptions} */
xyz.swapee.wc.ITesting.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITesting
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.RecordITesting filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITesting

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.BoundITesting filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingFields}
 * @extends {xyz.swapee.wc.RecordITesting}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingCaster}
 * @extends {xyz.swapee.wc.BoundITestingProcessor}
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {xyz.swapee.wc.BoundITestingController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, null>}
 */
$xyz.swapee.wc.BoundITesting = function() {}
/** @typedef {$xyz.swapee.wc.BoundITesting} */
xyz.swapee.wc.BoundITesting

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.BoundTesting filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITesting}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTesting = function() {}
/** @typedef {$xyz.swapee.wc.BoundTesting} */
xyz.swapee.wc.BoundTesting

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting.Pinout filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingController.Inputs}
 */
$xyz.swapee.wc.ITesting.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.ITesting.Pinout} */
xyz.swapee.wc.ITesting.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITesting
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITestingBuffer filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITestingController.Inputs>}
 */
$xyz.swapee.wc.ITestingBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingBuffer}
 */
xyz.swapee.wc.ITestingBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.TestingBuffer filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingBuffer}
 */
$xyz.swapee.wc.TestingBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingBuffer}
 */
xyz.swapee.wc.TestingBuffer
/** @type {function(new: xyz.swapee.wc.ITestingBuffer)} */
xyz.swapee.wc.TestingBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */