/** @const {?} */ $xyz.swapee.wc.back.ITestingScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.ITestingScreen.Initialese filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingScreenAT.Initialese}
 */
$xyz.swapee.wc.back.ITestingScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITestingScreen.Initialese} */
xyz.swapee.wc.back.ITestingScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITestingScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.ITestingScreenCaster filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/** @interface */
$xyz.swapee.wc.back.ITestingScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITestingScreen} */
$xyz.swapee.wc.back.ITestingScreenCaster.prototype.asITestingScreen
/** @type {!xyz.swapee.wc.back.BoundTestingScreen} */
$xyz.swapee.wc.back.ITestingScreenCaster.prototype.superTestingScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingScreenCaster}
 */
xyz.swapee.wc.back.ITestingScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.ITestingScreen filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenCaster}
 * @extends {xyz.swapee.wc.back.ITestingScreenAT}
 */
$xyz.swapee.wc.back.ITestingScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingScreen}
 */
xyz.swapee.wc.back.ITestingScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.TestingScreen filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreen.Initialese>}
 */
$xyz.swapee.wc.back.TestingScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.TestingScreen
/** @type {function(new: xyz.swapee.wc.back.ITestingScreen, ...!xyz.swapee.wc.back.ITestingScreen.Initialese)} */
xyz.swapee.wc.back.TestingScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.TestingScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.AbstractTestingScreen filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingScreen}
 */
$xyz.swapee.wc.back.AbstractTestingScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractTestingScreen)} */
xyz.swapee.wc.back.AbstractTestingScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.TestingScreenConstructor filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingScreen, ...!xyz.swapee.wc.back.ITestingScreen.Initialese)} */
xyz.swapee.wc.back.TestingScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.RecordITestingScreen filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.BoundITestingScreen filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundITestingScreenAT}
 */
$xyz.swapee.wc.back.BoundITestingScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITestingScreen} */
xyz.swapee.wc.back.BoundITestingScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.BoundTestingScreen filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTestingScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTestingScreen} */
xyz.swapee.wc.back.BoundTestingScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */