/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.Initialese filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 */
$xyz.swapee.wc.ITestingService.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingService.Initialese} */
xyz.swapee.wc.ITestingService.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingService
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingServiceCaster filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/** @interface */
$xyz.swapee.wc.ITestingServiceCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingService} */
$xyz.swapee.wc.ITestingServiceCaster.prototype.asITestingService
/** @type {!xyz.swapee.wc.BoundTestingService} */
$xyz.swapee.wc.ITestingServiceCaster.prototype.superTestingService
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingServiceCaster}
 */
xyz.swapee.wc.ITestingServiceCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingServiceCaster}
 * @extends {com.changelly.UChangelly}
 */
$xyz.swapee.wc.ITestingService = function() {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>}
 */
$xyz.swapee.wc.ITestingService.prototype.filterChangellyFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>}
 */
$xyz.swapee.wc.ITestingService.prototype.filterChangellyFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>}
 */
$xyz.swapee.wc.ITestingService.prototype.filterChangenowOffer = function(form) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingService}
 */
xyz.swapee.wc.ITestingService

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.TestingService filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingService}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingService.Initialese>}
 */
$xyz.swapee.wc.TestingService = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.TestingService
/** @type {function(new: xyz.swapee.wc.ITestingService)} */
xyz.swapee.wc.TestingService.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.TestingService.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.AbstractTestingService filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingService}
 */
$xyz.swapee.wc.AbstractTestingService = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTestingService}
 */
xyz.swapee.wc.AbstractTestingService
/** @type {function(new: xyz.swapee.wc.AbstractTestingService)} */
xyz.swapee.wc.AbstractTestingService.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.wc.TestingService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingService.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingService}
 */
xyz.swapee.wc.AbstractTestingService.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.RecordITestingService filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/** @typedef {{ filterChangellyFloatingOffer: xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer, filterChangellyFixedOffer: xyz.swapee.wc.ITestingService.filterChangellyFixedOffer, filterChangenowOffer: xyz.swapee.wc.ITestingService.filterChangenowOffer }} */
xyz.swapee.wc.RecordITestingService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.BoundITestingService filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingService}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingServiceCaster}
 * @extends {com.changelly.BoundUChangelly}
 */
$xyz.swapee.wc.BoundITestingService = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingService} */
xyz.swapee.wc.BoundITestingService

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.BoundTestingService filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingService}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingService = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingService} */
xyz.swapee.wc.BoundTestingService

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>}
 */
$xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>}
 * @this {xyz.swapee.wc.ITestingService}
 */
$xyz.swapee.wc.ITestingService._filterChangellyFloatingOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer} */
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingService._filterChangellyFloatingOffer} */
xyz.swapee.wc.ITestingService._filterChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer} */
xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingService,$xyz.swapee.wc.ITestingService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>}
 */
$xyz.swapee.wc.ITestingService.filterChangellyFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>}
 * @this {xyz.swapee.wc.ITestingService}
 */
$xyz.swapee.wc.ITestingService._filterChangellyFixedOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingService.filterChangellyFixedOffer} */
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingService._filterChangellyFixedOffer} */
xyz.swapee.wc.ITestingService._filterChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer} */
xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingService,$xyz.swapee.wc.ITestingService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangenowOffer filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>}
 */
$xyz.swapee.wc.ITestingService.filterChangenowOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>}
 * @this {xyz.swapee.wc.ITestingService}
 */
$xyz.swapee.wc.ITestingService._filterChangenowOffer = function(form) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingService.__filterChangenowOffer = function(form) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingService.filterChangenowOffer} */
xyz.swapee.wc.ITestingService.filterChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingService._filterChangenowOffer} */
xyz.swapee.wc.ITestingService._filterChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingService.__filterChangenowOffer} */
xyz.swapee.wc.ITestingService.__filterChangenowOffer

// nss:xyz.swapee.wc.ITestingService,$xyz.swapee.wc.ITestingService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} */
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 */
$xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return} */
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} */
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingService.filterChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 */
$xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return} */
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingService.filterChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangenowOffer.Form filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingService.filterChangenowOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} */
xyz.swapee.wc.ITestingService.filterChangenowOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingService.filterChangenowOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangenowOffer.Return filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 */
$xyz.swapee.wc.ITestingService.filterChangenowOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingService.filterChangenowOffer.Return} */
xyz.swapee.wc.ITestingService.filterChangenowOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingService.filterChangenowOffer
/* @typal-end */