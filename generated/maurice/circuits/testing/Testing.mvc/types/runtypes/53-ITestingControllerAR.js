/** @const {?} */ $xyz.swapee.wc.back.ITestingControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.ITestingControllerAR.Initialese filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
$xyz.swapee.wc.back.ITestingControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITestingControllerAR.Initialese} */
xyz.swapee.wc.back.ITestingControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITestingControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.ITestingControllerARCaster filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/** @interface */
$xyz.swapee.wc.back.ITestingControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITestingControllerAR} */
$xyz.swapee.wc.back.ITestingControllerARCaster.prototype.asITestingControllerAR
/** @type {!xyz.swapee.wc.back.BoundTestingControllerAR} */
$xyz.swapee.wc.back.ITestingControllerARCaster.prototype.superTestingControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingControllerARCaster}
 */
xyz.swapee.wc.back.ITestingControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.ITestingControllerAR filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITestingController}
 */
$xyz.swapee.wc.back.ITestingControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingControllerAR}
 */
xyz.swapee.wc.back.ITestingControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.TestingControllerAR filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.TestingControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.TestingControllerAR
/** @type {function(new: xyz.swapee.wc.back.ITestingControllerAR, ...!xyz.swapee.wc.back.ITestingControllerAR.Initialese)} */
xyz.swapee.wc.back.TestingControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.TestingControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.AbstractTestingControllerAR filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingControllerAR}
 */
$xyz.swapee.wc.back.AbstractTestingControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractTestingControllerAR)} */
xyz.swapee.wc.back.AbstractTestingControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.TestingControllerARConstructor filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingControllerAR, ...!xyz.swapee.wc.back.ITestingControllerAR.Initialese)} */
xyz.swapee.wc.back.TestingControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.RecordITestingControllerAR filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.BoundITestingControllerAR filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITestingController}
 */
$xyz.swapee.wc.back.BoundITestingControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITestingControllerAR} */
xyz.swapee.wc.back.BoundITestingControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.BoundTestingControllerAR filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTestingControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTestingControllerAR} */
xyz.swapee.wc.back.BoundTestingControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */