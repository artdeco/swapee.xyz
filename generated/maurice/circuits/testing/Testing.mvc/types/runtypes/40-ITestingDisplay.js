/** @const {?} */ $xyz.swapee.wc.ITestingDisplay
/** @const {?} */ xyz.swapee.wc.ITestingDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.Initialese filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings>}
 */
$xyz.swapee.wc.ITestingDisplay.Initialese = function() {}
/** @type {HTMLInputElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.AmountIn
/** @type {HTMLButtonElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.IncreaseBu
/** @type {HTMLButtonElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.DecreaseBu
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.RatesLoIn
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.RatesLoEr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFixedOfferAmount
/** @typedef {$xyz.swapee.wc.ITestingDisplay.Initialese} */
xyz.swapee.wc.ITestingDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplayFields filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @interface */
$xyz.swapee.wc.ITestingDisplayFields = function() {}
/** @type {!xyz.swapee.wc.ITestingDisplay.Settings} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.ITestingDisplay.Queries} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.queries
/** @type {HTMLInputElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.AmountIn
/** @type {HTMLButtonElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.IncreaseBu
/** @type {HTMLButtonElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.DecreaseBu
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.RatesLoIn
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.RatesLoEr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFixedOfferAmount
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingDisplayFields}
 */
xyz.swapee.wc.ITestingDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplayCaster filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @interface */
$xyz.swapee.wc.ITestingDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingDisplay} */
$xyz.swapee.wc.ITestingDisplayCaster.prototype.asITestingDisplay
/** @type {!xyz.swapee.wc.BoundITestingScreen} */
$xyz.swapee.wc.ITestingDisplayCaster.prototype.asITestingScreen
/** @type {!xyz.swapee.wc.BoundTestingDisplay} */
$xyz.swapee.wc.ITestingDisplayCaster.prototype.superTestingDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingDisplayCaster}
 */
xyz.swapee.wc.ITestingDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.TestingMemory, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, xyz.swapee.wc.ITestingDisplay.Queries, null>}
 */
$xyz.swapee.wc.ITestingDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ITestingDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingDisplay}
 */
xyz.swapee.wc.ITestingDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.TestingDisplay filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init
 * @implements {xyz.swapee.wc.ITestingDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingDisplay.Initialese>}
 */
$xyz.swapee.wc.TestingDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.TestingDisplay
/** @type {function(new: xyz.swapee.wc.ITestingDisplay, ...!xyz.swapee.wc.ITestingDisplay.Initialese)} */
xyz.swapee.wc.TestingDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.TestingDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.AbstractTestingDisplay filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init
 * @extends {xyz.swapee.wc.TestingDisplay}
 */
$xyz.swapee.wc.AbstractTestingDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay
/** @type {function(new: xyz.swapee.wc.AbstractTestingDisplay)} */
xyz.swapee.wc.AbstractTestingDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.TestingDisplayConstructor filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @typedef {function(new: xyz.swapee.wc.ITestingDisplay, ...!xyz.swapee.wc.ITestingDisplay.Initialese)} */
xyz.swapee.wc.TestingDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.RecordITestingDisplay filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @typedef {{ paint: xyz.swapee.wc.ITestingDisplay.paint }} */
xyz.swapee.wc.RecordITestingDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.BoundITestingDisplay filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingDisplayFields}
 * @extends {xyz.swapee.wc.RecordITestingDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.TestingMemory, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, xyz.swapee.wc.ITestingDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundITestingDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingDisplay} */
xyz.swapee.wc.BoundITestingDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.BoundTestingDisplay filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingDisplay} */
xyz.swapee.wc.BoundTestingDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.paint filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ITestingDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory, null): void} */
xyz.swapee.wc.ITestingDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.ITestingDisplay, !xyz.swapee.wc.TestingMemory, null): void} */
xyz.swapee.wc.ITestingDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.ITestingDisplay.__paint} */
xyz.swapee.wc.ITestingDisplay.__paint

// nss:xyz.swapee.wc.ITestingDisplay,$xyz.swapee.wc.ITestingDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.Queries filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @record */
$xyz.swapee.wc.ITestingDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.ITestingDisplay.Queries} */
xyz.swapee.wc.ITestingDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.Settings filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingDisplay.Queries}
 */
$xyz.swapee.wc.ITestingDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.ITestingDisplay.Settings} */
xyz.swapee.wc.ITestingDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingDisplay
/* @typal-end */