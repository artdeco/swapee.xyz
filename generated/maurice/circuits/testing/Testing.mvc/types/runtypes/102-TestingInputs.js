/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/102-TestingInputs.xml} xyz.swapee.wc.front.TestingInputs filter:!ControllerPlugin~props 9fbb89c3bc77932cdb4b0ecab631b8a6 */
/** @record */
$xyz.swapee.wc.front.TestingInputs = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.front.TestingInputs.prototype.core
/** @type {string|undefined} */
$xyz.swapee.wc.front.TestingInputs.prototype.host
/** @type {number|undefined} */
$xyz.swapee.wc.front.TestingInputs.prototype.amountIn
/** @type {string|undefined} */
$xyz.swapee.wc.front.TestingInputs.prototype.currencyIn
/** @type {string|undefined} */
$xyz.swapee.wc.front.TestingInputs.prototype.currencyOut
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.TestingInputs.prototype.changeNow
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.TestingInputs}
 */
xyz.swapee.wc.front.TestingInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */