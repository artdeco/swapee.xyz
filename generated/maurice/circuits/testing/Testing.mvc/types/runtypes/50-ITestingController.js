/** @const {?} */ $xyz.swapee.wc.ITestingController
/** @const {?} */ xyz.swapee.wc.ITestingController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.Initialese filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITestingOuterCore.WeakModel>}
 */
$xyz.swapee.wc.ITestingController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingController.Initialese} */
xyz.swapee.wc.ITestingController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingControllerFields filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @interface */
$xyz.swapee.wc.ITestingControllerFields = function() {}
/** @type {!xyz.swapee.wc.ITestingController.Inputs} */
$xyz.swapee.wc.ITestingControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingControllerFields}
 */
xyz.swapee.wc.ITestingControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingControllerCaster filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @interface */
$xyz.swapee.wc.ITestingControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingController} */
$xyz.swapee.wc.ITestingControllerCaster.prototype.asITestingController
/** @type {!xyz.swapee.wc.BoundITestingProcessor} */
$xyz.swapee.wc.ITestingControllerCaster.prototype.asITestingProcessor
/** @type {!xyz.swapee.wc.BoundTestingController} */
$xyz.swapee.wc.ITestingControllerCaster.prototype.superTestingController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingControllerCaster}
 */
xyz.swapee.wc.ITestingControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.ITestingOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.TestingMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITestingController.Inputs>}
 */
$xyz.swapee.wc.ITestingController = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.resetPort = function() {}
/** @return {?} */
$xyz.swapee.wc.ITestingController.prototype.increaseAmount = function() {}
/** @return {?} */
$xyz.swapee.wc.ITestingController.prototype.decreaseAmount = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.prototype.setCore = function(val) {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.unsetCore = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.prototype.setAmountIn = function(val) {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.unsetAmountIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.prototype.setCurrencyIn = function(val) {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.unsetCurrencyIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.prototype.setCurrencyOut = function(val) {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.unsetCurrencyOut = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingController.prototype.loadChangenowOffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingController}
 */
xyz.swapee.wc.ITestingController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.TestingController filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init
 * @implements {xyz.swapee.wc.ITestingController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingController.Initialese>}
 */
$xyz.swapee.wc.TestingController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.TestingController
/** @type {function(new: xyz.swapee.wc.ITestingController, ...!xyz.swapee.wc.ITestingController.Initialese)} */
xyz.swapee.wc.TestingController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.TestingController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.AbstractTestingController filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init
 * @extends {xyz.swapee.wc.TestingController}
 */
$xyz.swapee.wc.AbstractTestingController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingController}
 */
xyz.swapee.wc.AbstractTestingController
/** @type {function(new: xyz.swapee.wc.AbstractTestingController)} */
xyz.swapee.wc.AbstractTestingController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingController}
 */
xyz.swapee.wc.AbstractTestingController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.TestingControllerConstructor filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @typedef {function(new: xyz.swapee.wc.ITestingController, ...!xyz.swapee.wc.ITestingController.Initialese)} */
xyz.swapee.wc.TestingControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.RecordITestingController filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITestingController.resetPort, increaseAmount: xyz.swapee.wc.ITestingController.increaseAmount, decreaseAmount: xyz.swapee.wc.ITestingController.decreaseAmount, setCore: xyz.swapee.wc.ITestingController.setCore, unsetCore: xyz.swapee.wc.ITestingController.unsetCore, setAmountIn: xyz.swapee.wc.ITestingController.setAmountIn, unsetAmountIn: xyz.swapee.wc.ITestingController.unsetAmountIn, setCurrencyIn: xyz.swapee.wc.ITestingController.setCurrencyIn, unsetCurrencyIn: xyz.swapee.wc.ITestingController.unsetCurrencyIn, setCurrencyOut: xyz.swapee.wc.ITestingController.setCurrencyOut, unsetCurrencyOut: xyz.swapee.wc.ITestingController.unsetCurrencyOut, loadChangellyFloatingOffer: xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.ITestingController.loadChangellyFixedOffer, loadChangenowOffer: xyz.swapee.wc.ITestingController.loadChangenowOffer }} */
xyz.swapee.wc.RecordITestingController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.BoundITestingController filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingControllerFields}
 * @extends {xyz.swapee.wc.RecordITestingController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.ITestingOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.TestingMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.ITestingController.Inputs>}
 */
$xyz.swapee.wc.BoundITestingController = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingController} */
xyz.swapee.wc.BoundITestingController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.BoundTestingController filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingController = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingController} */
xyz.swapee.wc.BoundTestingController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.resetPort filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._resetPort
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__resetPort} */
xyz.swapee.wc.ITestingController.__resetPort

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.increaseAmount filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.ITestingController.__increaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ITestingController.increaseAmount
/** @typedef {function(this: xyz.swapee.wc.ITestingController)} */
xyz.swapee.wc.ITestingController._increaseAmount
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__increaseAmount} */
xyz.swapee.wc.ITestingController.__increaseAmount

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.decreaseAmount filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.ITestingController.__decreaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ITestingController.decreaseAmount
/** @typedef {function(this: xyz.swapee.wc.ITestingController)} */
xyz.swapee.wc.ITestingController._decreaseAmount
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__decreaseAmount} */
xyz.swapee.wc.ITestingController.__decreaseAmount

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setCore filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__setCore = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ITestingController.setCore
/** @typedef {function(this: xyz.swapee.wc.ITestingController, string): void} */
xyz.swapee.wc.ITestingController._setCore
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__setCore} */
xyz.swapee.wc.ITestingController.__setCore

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetCore filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__unsetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetCore
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetCore
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__unsetCore} */
xyz.swapee.wc.ITestingController.__unsetCore

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setAmountIn filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__setAmountIn = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.ITestingController.setAmountIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController, number): void} */
xyz.swapee.wc.ITestingController._setAmountIn
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__setAmountIn} */
xyz.swapee.wc.ITestingController.__setAmountIn

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetAmountIn filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__unsetAmountIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetAmountIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetAmountIn
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__unsetAmountIn} */
xyz.swapee.wc.ITestingController.__unsetAmountIn

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setCurrencyIn filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__setCurrencyIn = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ITestingController.setCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController, string): void} */
xyz.swapee.wc.ITestingController._setCurrencyIn
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__setCurrencyIn} */
xyz.swapee.wc.ITestingController.__setCurrencyIn

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetCurrencyIn filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__unsetCurrencyIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetCurrencyIn
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__unsetCurrencyIn} */
xyz.swapee.wc.ITestingController.__unsetCurrencyIn

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setCurrencyOut filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__setCurrencyOut = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ITestingController.setCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.ITestingController, string): void} */
xyz.swapee.wc.ITestingController._setCurrencyOut
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__setCurrencyOut} */
xyz.swapee.wc.ITestingController.__setCurrencyOut

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetCurrencyOut filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__unsetCurrencyOut = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetCurrencyOut
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__unsetCurrencyOut} */
xyz.swapee.wc.ITestingController.__unsetCurrencyOut

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._loadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.loadChangellyFixedOffer filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._loadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer} */
xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.loadChangenowOffer filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingController.__loadChangenowOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.loadChangenowOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._loadChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingController.__loadChangenowOffer} */
xyz.swapee.wc.ITestingController.__loadChangenowOffer

// nss:xyz.swapee.wc.ITestingController,$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.Inputs filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPort.Inputs}
 */
$xyz.swapee.wc.ITestingController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.ITestingController.Inputs} */
xyz.swapee.wc.ITestingController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.WeakInputs filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPort.WeakInputs}
 */
$xyz.swapee.wc.ITestingController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.ITestingController.WeakInputs} */
xyz.swapee.wc.ITestingController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingController
/* @typal-end */