/** @const {?} */ $xyz.swapee.wc.back.ITestingDisplay
/** @const {?} */ xyz.swapee.wc.back.ITestingDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplay.Initialese filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TestingClasses>}
 */
$xyz.swapee.wc.back.ITestingDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.AmountIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.IncreaseBu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.DecreaseBu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.RatesLoIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.RatesLoEr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFixedOfferAmount
/** @typedef {$xyz.swapee.wc.back.ITestingDisplay.Initialese} */
xyz.swapee.wc.back.ITestingDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITestingDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplayFields filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/** @interface */
$xyz.swapee.wc.back.ITestingDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.AmountIn
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.IncreaseBu
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.DecreaseBu
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.RatesLoIn
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.RatesLoEr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFixedOfferAmount
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingDisplayFields}
 */
xyz.swapee.wc.back.ITestingDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplayCaster filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/** @interface */
$xyz.swapee.wc.back.ITestingDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITestingDisplay} */
$xyz.swapee.wc.back.ITestingDisplayCaster.prototype.asITestingDisplay
/** @type {!xyz.swapee.wc.back.BoundTestingDisplay} */
$xyz.swapee.wc.back.ITestingDisplayCaster.prototype.superTestingDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingDisplayCaster}
 */
xyz.swapee.wc.back.ITestingDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplay filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.ITestingDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.TestingClasses, null>}
 */
$xyz.swapee.wc.back.ITestingDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ITestingDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingDisplay}
 */
xyz.swapee.wc.back.ITestingDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.TestingDisplay filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.ITestingDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingDisplay.Initialese>}
 */
$xyz.swapee.wc.back.TestingDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.TestingDisplay
/** @type {function(new: xyz.swapee.wc.back.ITestingDisplay)} */
xyz.swapee.wc.back.TestingDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.TestingDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.AbstractTestingDisplay filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.TestingDisplay}
 */
$xyz.swapee.wc.back.AbstractTestingDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractTestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractTestingDisplay)} */
xyz.swapee.wc.back.AbstractTestingDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.RecordITestingDisplay filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/** @typedef {{ paint: xyz.swapee.wc.back.ITestingDisplay.paint }} */
xyz.swapee.wc.back.RecordITestingDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.BoundITestingDisplay filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordITestingDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.TestingClasses, null>}
 */
$xyz.swapee.wc.back.BoundITestingDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITestingDisplay} */
xyz.swapee.wc.back.BoundITestingDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.BoundTestingDisplay filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTestingDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTestingDisplay} */
xyz.swapee.wc.back.BoundTestingDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplay.paint filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ITestingDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory=, null=): void} */
xyz.swapee.wc.back.ITestingDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.ITestingDisplay, !xyz.swapee.wc.TestingMemory=, null=): void} */
xyz.swapee.wc.back.ITestingDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.ITestingDisplay.__paint} */
xyz.swapee.wc.back.ITestingDisplay.__paint

// nss:xyz.swapee.wc.back.ITestingDisplay,$xyz.swapee.wc.back.ITestingDisplay,xyz.swapee.wc.back
/* @typal-end */