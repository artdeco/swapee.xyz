/** @const {?} */ $xyz.swapee.wc.front.ITestingController
/** @const {?} */ xyz.swapee.wc.front.ITestingController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.Initialese filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @record */
$xyz.swapee.wc.front.ITestingController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITestingController.Initialese} */
xyz.swapee.wc.front.ITestingController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITestingController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingControllerCaster filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @interface */
$xyz.swapee.wc.front.ITestingControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITestingController} */
$xyz.swapee.wc.front.ITestingControllerCaster.prototype.asITestingController
/** @type {!xyz.swapee.wc.front.BoundTestingController} */
$xyz.swapee.wc.front.ITestingControllerCaster.prototype.superTestingController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITestingControllerCaster}
 */
xyz.swapee.wc.front.ITestingControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.front.ITestingControllerAT}
 */
$xyz.swapee.wc.front.ITestingController = function() {}
/** @return {?} */
$xyz.swapee.wc.front.ITestingController.prototype.increaseAmount = function() {}
/** @return {?} */
$xyz.swapee.wc.front.ITestingController.prototype.decreaseAmount = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.prototype.setCore = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.ITestingController.prototype.unsetCore = function() {}
/**
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.prototype.setAmountIn = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.ITestingController.prototype.unsetAmountIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.prototype.setCurrencyIn = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.ITestingController.prototype.unsetCurrencyIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.prototype.setCurrencyOut = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.ITestingController.prototype.unsetCurrencyOut = function() {}
/** @return {void} */
$xyz.swapee.wc.front.ITestingController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.ITestingController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.ITestingController.prototype.loadChangenowOffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITestingController}
 */
xyz.swapee.wc.front.ITestingController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.TestingController filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init
 * @implements {xyz.swapee.wc.front.ITestingController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingController.Initialese>}
 */
$xyz.swapee.wc.front.TestingController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.TestingController
/** @type {function(new: xyz.swapee.wc.front.ITestingController, ...!xyz.swapee.wc.front.ITestingController.Initialese)} */
xyz.swapee.wc.front.TestingController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.TestingController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.AbstractTestingController filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init
 * @extends {xyz.swapee.wc.front.TestingController}
 */
$xyz.swapee.wc.front.AbstractTestingController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTestingController}
 */
xyz.swapee.wc.front.AbstractTestingController
/** @type {function(new: xyz.swapee.wc.front.AbstractTestingController)} */
xyz.swapee.wc.front.AbstractTestingController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.TestingControllerConstructor filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @typedef {function(new: xyz.swapee.wc.front.ITestingController, ...!xyz.swapee.wc.front.ITestingController.Initialese)} */
xyz.swapee.wc.front.TestingControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.RecordITestingController filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @typedef {{ increaseAmount: xyz.swapee.wc.front.ITestingController.increaseAmount, decreaseAmount: xyz.swapee.wc.front.ITestingController.decreaseAmount, setCore: xyz.swapee.wc.front.ITestingController.setCore, unsetCore: xyz.swapee.wc.front.ITestingController.unsetCore, setAmountIn: xyz.swapee.wc.front.ITestingController.setAmountIn, unsetAmountIn: xyz.swapee.wc.front.ITestingController.unsetAmountIn, setCurrencyIn: xyz.swapee.wc.front.ITestingController.setCurrencyIn, unsetCurrencyIn: xyz.swapee.wc.front.ITestingController.unsetCurrencyIn, setCurrencyOut: xyz.swapee.wc.front.ITestingController.setCurrencyOut, unsetCurrencyOut: xyz.swapee.wc.front.ITestingController.unsetCurrencyOut, loadChangellyFloatingOffer: xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer, loadChangenowOffer: xyz.swapee.wc.front.ITestingController.loadChangenowOffer }} */
xyz.swapee.wc.front.RecordITestingController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.BoundITestingController filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITestingController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundITestingControllerAT}
 */
$xyz.swapee.wc.front.BoundITestingController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITestingController} */
xyz.swapee.wc.front.BoundITestingController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.BoundTestingController filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITestingController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTestingController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTestingController} */
xyz.swapee.wc.front.BoundTestingController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.increaseAmount filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.ITestingController.__increaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ITestingController.increaseAmount
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController)} */
xyz.swapee.wc.front.ITestingController._increaseAmount
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__increaseAmount} */
xyz.swapee.wc.front.ITestingController.__increaseAmount

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.decreaseAmount filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.ITestingController.__decreaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ITestingController.decreaseAmount
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController)} */
xyz.swapee.wc.front.ITestingController._decreaseAmount
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__decreaseAmount} */
xyz.swapee.wc.front.ITestingController.__decreaseAmount

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setCore filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__setCore = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ITestingController.setCore
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, string): void} */
xyz.swapee.wc.front.ITestingController._setCore
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__setCore} */
xyz.swapee.wc.front.ITestingController.__setCore

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetCore filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__unsetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetCore
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetCore
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__unsetCore} */
xyz.swapee.wc.front.ITestingController.__unsetCore

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setAmountIn filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__setAmountIn = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.ITestingController.setAmountIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, number): void} */
xyz.swapee.wc.front.ITestingController._setAmountIn
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__setAmountIn} */
xyz.swapee.wc.front.ITestingController.__setAmountIn

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetAmountIn filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__unsetAmountIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetAmountIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetAmountIn
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__unsetAmountIn} */
xyz.swapee.wc.front.ITestingController.__unsetAmountIn

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setCurrencyIn filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__setCurrencyIn = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ITestingController.setCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, string): void} */
xyz.swapee.wc.front.ITestingController._setCurrencyIn
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__setCurrencyIn} */
xyz.swapee.wc.front.ITestingController.__setCurrencyIn

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetCurrencyIn filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetCurrencyIn
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn} */
xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setCurrencyOut filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__setCurrencyOut = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ITestingController.setCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, string): void} */
xyz.swapee.wc.front.ITestingController._setCurrencyOut
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__setCurrencyOut} */
xyz.swapee.wc.front.ITestingController.__setCurrencyOut

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetCurrencyOut filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetCurrencyOut
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut} */
xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._loadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._loadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer} */
xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.loadChangenowOffer filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ITestingController.__loadChangenowOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.loadChangenowOffer
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._loadChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.front.ITestingController.__loadChangenowOffer} */
xyz.swapee.wc.front.ITestingController.__loadChangenowOffer

// nss:xyz.swapee.wc.front.ITestingController,$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */