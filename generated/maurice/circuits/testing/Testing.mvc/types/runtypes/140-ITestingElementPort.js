/** @const {?} */ $xyz.swapee.wc.ITestingElementPort
/** @const {?} */ $xyz.swapee.wc.ITestingElementPort.Inputs
/** @const {?} */ $xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Initialese filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.ITestingElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Initialese} */
xyz.swapee.wc.ITestingElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPortFields filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @interface */
$xyz.swapee.wc.ITestingElementPortFields = function() {}
/** @type {!xyz.swapee.wc.ITestingElementPort.Inputs} */
$xyz.swapee.wc.ITestingElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ITestingElementPort.Inputs} */
$xyz.swapee.wc.ITestingElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingElementPortFields}
 */
xyz.swapee.wc.ITestingElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPortCaster filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @interface */
$xyz.swapee.wc.ITestingElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingElementPort} */
$xyz.swapee.wc.ITestingElementPortCaster.prototype.asITestingElementPort
/** @type {!xyz.swapee.wc.BoundTestingElementPort} */
$xyz.swapee.wc.ITestingElementPortCaster.prototype.superTestingElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingElementPortCaster}
 */
xyz.swapee.wc.ITestingElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ITestingElementPort.Inputs>}
 */
$xyz.swapee.wc.ITestingElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingElementPort}
 */
xyz.swapee.wc.ITestingElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.TestingElementPort filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElementPort.Initialese} init
 * @implements {xyz.swapee.wc.ITestingElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingElementPort.Initialese>}
 */
$xyz.swapee.wc.TestingElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.TestingElementPort
/** @type {function(new: xyz.swapee.wc.ITestingElementPort, ...!xyz.swapee.wc.ITestingElementPort.Initialese)} */
xyz.swapee.wc.TestingElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.TestingElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.AbstractTestingElementPort filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElementPort.Initialese} init
 * @extends {xyz.swapee.wc.TestingElementPort}
 */
$xyz.swapee.wc.AbstractTestingElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort
/** @type {function(new: xyz.swapee.wc.AbstractTestingElementPort)} */
xyz.swapee.wc.AbstractTestingElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingElementPort|typeof xyz.swapee.wc.TestingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingElementPort|typeof xyz.swapee.wc.TestingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingElementPort|typeof xyz.swapee.wc.TestingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.TestingElementPortConstructor filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {function(new: xyz.swapee.wc.ITestingElementPort, ...!xyz.swapee.wc.ITestingElementPort.Initialese)} */
xyz.swapee.wc.TestingElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.RecordITestingElementPort filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.BoundITestingElementPort filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingElementPortFields}
 * @extends {xyz.swapee.wc.RecordITestingElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ITestingElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundITestingElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingElementPort} */
xyz.swapee.wc.BoundITestingElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.BoundTestingElementPort filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingElementPort} */
xyz.swapee.wc.BoundTestingElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts.amountInOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts.amountInOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts.increaseBuOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts.increaseBuOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts.decreaseBuOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts.decreaseBuOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts.ratesLoInOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts.ratesLoInOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts.ratesLoErOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts.ratesLoErOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts.changellyFloatingOfferOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts.changellyFloatingOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts.changellyFloatingOfferAmountOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts.changellyFloatingOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts.changellyFixedOfferOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts.changellyFixedOfferOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts.changellyFixedOfferAmountOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {!Object} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts.changellyFixedOfferAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder} */
xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts.prototype.amountInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts.prototype.increaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts.prototype.decreaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts.prototype.ratesLoInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts.prototype.ratesLoErOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts}
 */
$xyz.swapee.wc.ITestingElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITestingElementPort.Inputs}
 */
xyz.swapee.wc.ITestingElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts.prototype.amountInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts.prototype.increaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts.prototype.decreaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts.prototype.ratesLoInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts.prototype.ratesLoErOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts}
 * @extends {xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts}
 */
$xyz.swapee.wc.ITestingElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITestingElementPort.WeakInputs}
 */
xyz.swapee.wc.ITestingElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts_Safe.prototype.amountInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts_Safe.prototype.increaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts_Safe.prototype.decreaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts_Safe.prototype.ratesLoInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts_Safe.prototype.ratesLoErOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts_Safe.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts_Safe.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts_Safe.prototype.amountInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts_Safe.prototype.increaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts_Safe.prototype.decreaseBuOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts_Safe.prototype.ratesLoInOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts_Safe.prototype.ratesLoErOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe.prototype.changellyFloatingOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe.prototype.changellyFloatingOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe.prototype.changellyFixedOfferOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe filter:!ControllerPlugin~props 7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @record */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe.prototype.changellyFixedOfferAmountOpts
/** @typedef {$xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe} */
xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElementPort.WeakInputs
/* @typal-end */