/** @const {?} */ $xyz.swapee.wc.front.ITestingControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.ITestingControllerAT.Initialese filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.ITestingControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITestingControllerAT.Initialese} */
xyz.swapee.wc.front.ITestingControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITestingControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.ITestingControllerATCaster filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/** @interface */
$xyz.swapee.wc.front.ITestingControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITestingControllerAT} */
$xyz.swapee.wc.front.ITestingControllerATCaster.prototype.asITestingControllerAT
/** @type {!xyz.swapee.wc.front.BoundTestingControllerAT} */
$xyz.swapee.wc.front.ITestingControllerATCaster.prototype.superTestingControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITestingControllerATCaster}
 */
xyz.swapee.wc.front.ITestingControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.ITestingControllerAT filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.ITestingControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITestingControllerAT}
 */
xyz.swapee.wc.front.ITestingControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.TestingControllerAT filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.ITestingControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.TestingControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.TestingControllerAT
/** @type {function(new: xyz.swapee.wc.front.ITestingControllerAT, ...!xyz.swapee.wc.front.ITestingControllerAT.Initialese)} */
xyz.swapee.wc.front.TestingControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.TestingControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.AbstractTestingControllerAT filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.TestingControllerAT}
 */
$xyz.swapee.wc.front.AbstractTestingControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractTestingControllerAT)} */
xyz.swapee.wc.front.AbstractTestingControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.TestingControllerATConstructor filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/** @typedef {function(new: xyz.swapee.wc.front.ITestingControllerAT, ...!xyz.swapee.wc.front.ITestingControllerAT.Initialese)} */
xyz.swapee.wc.front.TestingControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.RecordITestingControllerAT filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITestingControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.BoundITestingControllerAT filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITestingControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundITestingControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITestingControllerAT} */
xyz.swapee.wc.front.BoundITestingControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.BoundTestingControllerAT filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITestingControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTestingControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTestingControllerAT} */
xyz.swapee.wc.front.BoundTestingControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */