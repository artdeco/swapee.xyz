/** @const {?} */ $xyz.swapee.wc.ITestingProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.ITestingProcessor.Initialese filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingComputer.Initialese}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
$xyz.swapee.wc.ITestingProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingProcessor.Initialese} */
xyz.swapee.wc.ITestingProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.ITestingProcessorCaster filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/** @interface */
$xyz.swapee.wc.ITestingProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingProcessor} */
$xyz.swapee.wc.ITestingProcessorCaster.prototype.asITestingProcessor
/** @type {!xyz.swapee.wc.BoundTestingProcessor} */
$xyz.swapee.wc.ITestingProcessorCaster.prototype.superTestingProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingProcessorCaster}
 */
xyz.swapee.wc.ITestingProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.ITestingProcessor filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingProcessorCaster}
 * @extends {xyz.swapee.wc.ITestingComputer}
 * @extends {xyz.swapee.wc.ITestingCore}
 * @extends {xyz.swapee.wc.ITestingController}
 */
$xyz.swapee.wc.ITestingProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingProcessor}
 */
xyz.swapee.wc.ITestingProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.TestingProcessor filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init
 * @implements {xyz.swapee.wc.ITestingProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingProcessor.Initialese>}
 */
$xyz.swapee.wc.TestingProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.TestingProcessor
/** @type {function(new: xyz.swapee.wc.ITestingProcessor, ...!xyz.swapee.wc.ITestingProcessor.Initialese)} */
xyz.swapee.wc.TestingProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.TestingProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.AbstractTestingProcessor filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init
 * @extends {xyz.swapee.wc.TestingProcessor}
 */
$xyz.swapee.wc.AbstractTestingProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor
/** @type {function(new: xyz.swapee.wc.AbstractTestingProcessor)} */
xyz.swapee.wc.AbstractTestingProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.TestingProcessorConstructor filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/** @typedef {function(new: xyz.swapee.wc.ITestingProcessor, ...!xyz.swapee.wc.ITestingProcessor.Initialese)} */
xyz.swapee.wc.TestingProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.RecordITestingProcessor filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.BoundITestingProcessor filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingProcessorCaster}
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {xyz.swapee.wc.BoundITestingCore}
 * @extends {xyz.swapee.wc.BoundITestingController}
 */
$xyz.swapee.wc.BoundITestingProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingProcessor} */
xyz.swapee.wc.BoundITestingProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.BoundTestingProcessor filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingProcessor} */
xyz.swapee.wc.BoundTestingProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */