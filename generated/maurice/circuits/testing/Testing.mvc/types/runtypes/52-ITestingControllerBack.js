/** @const {?} */ $xyz.swapee.wc.back.ITestingController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.ITestingController.Initialese filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
$xyz.swapee.wc.back.ITestingController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITestingController.Initialese} */
xyz.swapee.wc.back.ITestingController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITestingController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.ITestingControllerCaster filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/** @interface */
$xyz.swapee.wc.back.ITestingControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITestingController} */
$xyz.swapee.wc.back.ITestingControllerCaster.prototype.asITestingController
/** @type {!xyz.swapee.wc.back.BoundTestingController} */
$xyz.swapee.wc.back.ITestingControllerCaster.prototype.superTestingController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingControllerCaster}
 */
xyz.swapee.wc.back.ITestingControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.ITestingController filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.ITestingController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.ITestingController.Inputs>}
 */
$xyz.swapee.wc.back.ITestingController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingController}
 */
xyz.swapee.wc.back.ITestingController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.TestingController filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingController.Initialese>}
 */
$xyz.swapee.wc.back.TestingController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.TestingController
/** @type {function(new: xyz.swapee.wc.back.ITestingController, ...!xyz.swapee.wc.back.ITestingController.Initialese)} */
xyz.swapee.wc.back.TestingController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.TestingController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.AbstractTestingController filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingController}
 */
$xyz.swapee.wc.back.AbstractTestingController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTestingController}
 */
xyz.swapee.wc.back.AbstractTestingController
/** @type {function(new: xyz.swapee.wc.back.AbstractTestingController)} */
xyz.swapee.wc.back.AbstractTestingController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.TestingControllerConstructor filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingController, ...!xyz.swapee.wc.back.ITestingController.Initialese)} */
xyz.swapee.wc.back.TestingControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.RecordITestingController filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.BoundITestingController filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.BoundITestingController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.ITestingController.Inputs>}
 */
$xyz.swapee.wc.back.BoundITestingController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITestingController} */
xyz.swapee.wc.back.BoundITestingController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.BoundTestingController filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTestingController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTestingController} */
xyz.swapee.wc.back.BoundTestingController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */