/** @const {?} */ $xyz.swapee.wc.front.ITestingScreenAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.ITestingScreenAR.Initialese filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITestingScreen.Initialese}
 */
$xyz.swapee.wc.front.ITestingScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITestingScreenAR.Initialese} */
xyz.swapee.wc.front.ITestingScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITestingScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.ITestingScreenARCaster filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/** @interface */
$xyz.swapee.wc.front.ITestingScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITestingScreenAR} */
$xyz.swapee.wc.front.ITestingScreenARCaster.prototype.asITestingScreenAR
/** @type {!xyz.swapee.wc.front.BoundTestingScreenAR} */
$xyz.swapee.wc.front.ITestingScreenARCaster.prototype.superTestingScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITestingScreenARCaster}
 */
xyz.swapee.wc.front.ITestingScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.ITestingScreenAR filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITestingScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITestingScreen}
 */
$xyz.swapee.wc.front.ITestingScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITestingScreenAR}
 */
xyz.swapee.wc.front.ITestingScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.TestingScreenAR filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.ITestingScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.TestingScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.TestingScreenAR
/** @type {function(new: xyz.swapee.wc.front.ITestingScreenAR, ...!xyz.swapee.wc.front.ITestingScreenAR.Initialese)} */
xyz.swapee.wc.front.TestingScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.TestingScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.AbstractTestingScreenAR filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.TestingScreenAR}
 */
$xyz.swapee.wc.front.AbstractTestingScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractTestingScreenAR)} */
xyz.swapee.wc.front.AbstractTestingScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.TestingScreenARConstructor filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/** @typedef {function(new: xyz.swapee.wc.front.ITestingScreenAR, ...!xyz.swapee.wc.front.ITestingScreenAR.Initialese)} */
xyz.swapee.wc.front.TestingScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.RecordITestingScreenAR filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITestingScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.BoundITestingScreenAR filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITestingScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITestingScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITestingScreen}
 */
$xyz.swapee.wc.front.BoundITestingScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITestingScreenAR} */
xyz.swapee.wc.front.BoundITestingScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.BoundTestingScreenAR filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITestingScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTestingScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTestingScreenAR} */
xyz.swapee.wc.front.BoundTestingScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */