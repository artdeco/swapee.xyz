/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/100-TestingMemory.xml} xyz.swapee.wc.TestingMemory filter:!ControllerPlugin~props 94ffaa9b2f697bec16dae4e498534806 */
/** @record */
$xyz.swapee.wc.TestingMemory = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.TestingMemory.prototype.core
/** @type {string} */
$xyz.swapee.wc.TestingMemory.prototype.host
/** @type {number} */
$xyz.swapee.wc.TestingMemory.prototype.amountIn
/** @type {string} */
$xyz.swapee.wc.TestingMemory.prototype.currencyIn
/** @type {string} */
$xyz.swapee.wc.TestingMemory.prototype.currencyOut
/** @type {boolean|undefined} */
$xyz.swapee.wc.TestingMemory.prototype.changeNow
/** @type {number} */
$xyz.swapee.wc.TestingMemory.prototype.changellyFixedOffer
/** @type {number} */
$xyz.swapee.wc.TestingMemory.prototype.changellyFloatingOffer
/** @type {{ amountOut: string, type: string }} */
$xyz.swapee.wc.TestingMemory.prototype.changenowOffer
/** @type {boolean} */
$xyz.swapee.wc.TestingMemory.prototype.anyLoading
/** @type {boolean} */
$xyz.swapee.wc.TestingMemory.prototype.loadingChangellyFloatingOffer
/** @type {?boolean} */
$xyz.swapee.wc.TestingMemory.prototype.hasMoreChangellyFloatingOffer
/** @type {?Error} */
$xyz.swapee.wc.TestingMemory.prototype.loadChangellyFloatingOfferError
/** @type {boolean} */
$xyz.swapee.wc.TestingMemory.prototype.loadingChangellyFixedOffer
/** @type {?boolean} */
$xyz.swapee.wc.TestingMemory.prototype.hasMoreChangellyFixedOffer
/** @type {?Error} */
$xyz.swapee.wc.TestingMemory.prototype.loadChangellyFixedOfferError
/** @type {boolean} */
$xyz.swapee.wc.TestingMemory.prototype.loadingChangenowOffer
/** @type {?boolean} */
$xyz.swapee.wc.TestingMemory.prototype.hasMoreChangenowOffer
/** @type {?Error} */
$xyz.swapee.wc.TestingMemory.prototype.loadChangenowOfferError
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.TestingMemory}
 */
xyz.swapee.wc.TestingMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */