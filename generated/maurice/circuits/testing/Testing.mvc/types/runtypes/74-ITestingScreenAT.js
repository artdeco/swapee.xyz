/** @const {?} */ $xyz.swapee.wc.back.ITestingScreenAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.ITestingScreenAT.Initialese filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.ITestingScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITestingScreenAT.Initialese} */
xyz.swapee.wc.back.ITestingScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITestingScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.ITestingScreenATCaster filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/** @interface */
$xyz.swapee.wc.back.ITestingScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITestingScreenAT} */
$xyz.swapee.wc.back.ITestingScreenATCaster.prototype.asITestingScreenAT
/** @type {!xyz.swapee.wc.back.BoundTestingScreenAT} */
$xyz.swapee.wc.back.ITestingScreenATCaster.prototype.superTestingScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingScreenATCaster}
 */
xyz.swapee.wc.back.ITestingScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.ITestingScreenAT filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.ITestingScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITestingScreenAT}
 */
xyz.swapee.wc.back.ITestingScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.TestingScreenAT filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.TestingScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.TestingScreenAT
/** @type {function(new: xyz.swapee.wc.back.ITestingScreenAT, ...!xyz.swapee.wc.back.ITestingScreenAT.Initialese)} */
xyz.swapee.wc.back.TestingScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.TestingScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.AbstractTestingScreenAT filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingScreenAT}
 */
$xyz.swapee.wc.back.AbstractTestingScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractTestingScreenAT)} */
xyz.swapee.wc.back.AbstractTestingScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.TestingScreenATConstructor filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingScreenAT, ...!xyz.swapee.wc.back.ITestingScreenAT.Initialese)} */
xyz.swapee.wc.back.TestingScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.RecordITestingScreenAT filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.BoundITestingScreenAT filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundITestingScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITestingScreenAT} */
xyz.swapee.wc.back.BoundITestingScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.BoundTestingScreenAT filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTestingScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTestingScreenAT} */
xyz.swapee.wc.back.BoundTestingScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */