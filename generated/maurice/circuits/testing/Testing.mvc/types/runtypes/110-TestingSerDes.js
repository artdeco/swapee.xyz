/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.TestingMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.TestingMemoryPQs.prototype.core
/** @type {string} */
$xyz.swapee.wc.TestingMemoryPQs.prototype.host
/** @type {string} */
$xyz.swapee.wc.TestingMemoryPQs.prototype.amountIn
/** @type {string} */
$xyz.swapee.wc.TestingMemoryPQs.prototype.currencyIn
/** @type {string} */
$xyz.swapee.wc.TestingMemoryPQs.prototype.currencyOut
/** @type {string} */
$xyz.swapee.wc.TestingMemoryPQs.prototype.changeNow
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TestingMemoryPQs}
 */
xyz.swapee.wc.TestingMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.TestingMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.TestingMemoryQPs.prototype.a74ad
/** @type {string} */
$xyz.swapee.wc.TestingMemoryQPs.prototype.g7b3d
/** @type {string} */
$xyz.swapee.wc.TestingMemoryQPs.prototype.i8da7
/** @type {string} */
$xyz.swapee.wc.TestingMemoryQPs.prototype.f3088
/** @type {string} */
$xyz.swapee.wc.TestingMemoryQPs.prototype.j8dbb
/** @type {string} */
$xyz.swapee.wc.TestingMemoryQPs.prototype.j4af8
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingMemoryQPs}
 */
xyz.swapee.wc.TestingMemoryQPs
/** @type {function(new: xyz.swapee.wc.TestingMemoryQPs)} */
xyz.swapee.wc.TestingMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.TestingMemoryPQs}
 */
$xyz.swapee.wc.TestingInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TestingInputsPQs}
 */
xyz.swapee.wc.TestingInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.TestingInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingInputsQPs}
 */
xyz.swapee.wc.TestingInputsQPs
/** @type {function(new: xyz.swapee.wc.TestingInputsQPs)} */
xyz.swapee.wc.TestingInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingCachePQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.TestingCachePQs = function() {}
/** @type {string} */
$xyz.swapee.wc.TestingCachePQs.prototype.changellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.TestingCachePQs.prototype.changellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.TestingCachePQs.prototype.changenowOffer
/** @type {string} */
$xyz.swapee.wc.TestingCachePQs.prototype.anyLoading
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TestingCachePQs}
 */
xyz.swapee.wc.TestingCachePQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingCacheQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.TestingCacheQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.TestingCacheQPs.prototype.hb5f4
/** @type {string} */
$xyz.swapee.wc.TestingCacheQPs.prototype.ca73d
/** @type {string} */
$xyz.swapee.wc.TestingCacheQPs.prototype.dc0e8
/** @type {string} */
$xyz.swapee.wc.TestingCacheQPs.prototype.ac77f
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingCacheQPs}
 */
xyz.swapee.wc.TestingCacheQPs
/** @type {function(new: xyz.swapee.wc.TestingCacheQPs)} */
xyz.swapee.wc.TestingCacheQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.TestingVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.RatesLoIn
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFloatingOffer
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFloatingOfferAmount
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFixedOffer
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFixedOfferAmount
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.AmountIn
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.IncreaseBu
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.DecreaseBu
/** @type {string} */
$xyz.swapee.wc.TestingVdusPQs.prototype.RatesLoEr
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TestingVdusPQs}
 */
xyz.swapee.wc.TestingVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.TestingVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee3
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee7
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee8
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee12
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee13
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee9
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee10
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee11
/** @type {string} */
$xyz.swapee.wc.TestingVdusQPs.prototype.g1ee4
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingVdusQPs}
 */
xyz.swapee.wc.TestingVdusQPs
/** @type {function(new: xyz.swapee.wc.TestingVdusQPs)} */
xyz.swapee.wc.TestingVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */