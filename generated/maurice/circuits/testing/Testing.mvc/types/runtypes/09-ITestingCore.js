/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Initialese filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Initialese} */
xyz.swapee.wc.ITestingCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCoreFields filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @interface */
$xyz.swapee.wc.ITestingCoreFields = function() {}
/** @type {!xyz.swapee.wc.ITestingCore.Model} */
$xyz.swapee.wc.ITestingCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingCoreFields}
 */
xyz.swapee.wc.ITestingCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCoreCaster filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @interface */
$xyz.swapee.wc.ITestingCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingCore} */
$xyz.swapee.wc.ITestingCoreCaster.prototype.asITestingCore
/** @type {!xyz.swapee.wc.BoundTestingCore} */
$xyz.swapee.wc.ITestingCoreCaster.prototype.superTestingCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingCoreCaster}
 */
xyz.swapee.wc.ITestingCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingCoreCaster}
 * @extends {xyz.swapee.wc.ITestingOuterCore}
 */
$xyz.swapee.wc.ITestingCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingCore.prototype.resetTestingCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingCore}
 */
xyz.swapee.wc.ITestingCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.TestingCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingCore.Initialese>}
 */
$xyz.swapee.wc.TestingCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.TestingCore
/** @type {function(new: xyz.swapee.wc.ITestingCore)} */
xyz.swapee.wc.TestingCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.TestingCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.AbstractTestingCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingCore}
 */
$xyz.swapee.wc.AbstractTestingCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTestingCore}
 */
xyz.swapee.wc.AbstractTestingCore
/** @type {function(new: xyz.swapee.wc.AbstractTestingCore)} */
xyz.swapee.wc.AbstractTestingCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TestingCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.RecordITestingCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {{ resetCore: xyz.swapee.wc.ITestingCore.resetCore, resetTestingCore: xyz.swapee.wc.ITestingCore.resetTestingCore }} */
xyz.swapee.wc.RecordITestingCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.BoundITestingCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCoreFields}
 * @extends {xyz.swapee.wc.RecordITestingCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingCoreCaster}
 * @extends {xyz.swapee.wc.BoundITestingOuterCore}
 */
$xyz.swapee.wc.BoundITestingCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingCore} */
xyz.swapee.wc.BoundITestingCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.BoundTestingCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingCore} */
xyz.swapee.wc.BoundTestingCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.resetCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.ITestingCore): void} */
xyz.swapee.wc.ITestingCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.ITestingCore.__resetCore} */
xyz.swapee.wc.ITestingCore.__resetCore

// nss:xyz.swapee.wc.ITestingCore,$xyz.swapee.wc.ITestingCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.resetTestingCore filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingCore.__resetTestingCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingCore.resetTestingCore
/** @typedef {function(this: xyz.swapee.wc.ITestingCore): void} */
xyz.swapee.wc.ITestingCore._resetTestingCore
/** @typedef {typeof $xyz.swapee.wc.ITestingCore.__resetTestingCore} */
xyz.swapee.wc.ITestingCore.__resetTestingCore

// nss:xyz.swapee.wc.ITestingCore,$xyz.swapee.wc.ITestingCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer.changellyFixedOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer.changellyFloatingOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer.changenowOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {{ amountOut: string, type: string }} */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer.changenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.AnyLoading.anyLoading filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.AnyLoading.anyLoading

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer.loadingChangenowOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer.loadingChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer.hasMoreChangenowOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer.hasMoreChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError.loadChangenowOfferError filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError.loadChangenowOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer.prototype.changellyFixedOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer.prototype.changellyFloatingOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.ChangenowOffer = function() {}
/** @type {({ amountOut: string, type: string })|undefined} */
$xyz.swapee.wc.ITestingCore.Model.ChangenowOffer.prototype.changenowOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangenowOffer} */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.AnyLoading filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.AnyLoading = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ITestingCore.Model.AnyLoading.prototype.anyLoading
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.AnyLoading} */
xyz.swapee.wc.ITestingCore.Model.AnyLoading

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer.prototype.loadingChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer.prototype.hasMoreChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError.prototype.loadChangellyFloatingOfferError
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer.prototype.loadingChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer.prototype.hasMoreChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError.prototype.loadChangellyFixedOfferError
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer.prototype.loadingChangenowOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer.prototype.hasMoreChangenowOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError.prototype.loadChangenowOfferError
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError} */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model}
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AnyLoading}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError}
 */
$xyz.swapee.wc.ITestingCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model} */
xyz.swapee.wc.ITestingCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe.prototype.changellyFixedOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe.prototype.changellyFloatingOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe = function() {}
/** @type {{ amountOut: string, type: string }} */
$xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe.prototype.changenowOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe.prototype.anyLoading
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe} */
xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe.prototype.loadingChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe.prototype.hasMoreChangellyFloatingOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe.prototype.loadChangellyFloatingOfferError
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe.prototype.loadingChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe.prototype.hasMoreChangellyFixedOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe.prototype.loadChangellyFixedOfferError
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe.prototype.loadingChangenowOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe.prototype.hasMoreChangenowOffer
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
$xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe.prototype.loadChangenowOfferError
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe} */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */