/** @const {?} */ $xyz.swapee.wc.ITestingOuterCore
/** @const {?} */ $xyz.swapee.wc.ITestingOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.ITestingOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.ITestingPort
/** @const {?} */ $xyz.swapee.wc.ITestingPort.Inputs
/** @const {?} */ $xyz.swapee.wc.ITestingPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.ITestingCore
/** @const {?} */ $xyz.swapee.wc.ITestingCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Initialese filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Initialese} */
xyz.swapee.wc.ITestingOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCoreFields filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @interface */
$xyz.swapee.wc.ITestingOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.ITestingOuterCore.Model} */
$xyz.swapee.wc.ITestingOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingOuterCoreFields}
 */
xyz.swapee.wc.ITestingOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCoreCaster filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @interface */
$xyz.swapee.wc.ITestingOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingOuterCore} */
$xyz.swapee.wc.ITestingOuterCoreCaster.prototype.asITestingOuterCore
/** @type {!xyz.swapee.wc.BoundTestingOuterCore} */
$xyz.swapee.wc.ITestingOuterCoreCaster.prototype.superTestingOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingOuterCoreCaster}
 */
xyz.swapee.wc.ITestingOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingOuterCoreCaster}
 */
$xyz.swapee.wc.ITestingOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingOuterCore}
 */
xyz.swapee.wc.ITestingOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.TestingOuterCore filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingOuterCore.Initialese>}
 */
$xyz.swapee.wc.TestingOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.TestingOuterCore
/** @type {function(new: xyz.swapee.wc.ITestingOuterCore)} */
xyz.swapee.wc.TestingOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.TestingOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.AbstractTestingOuterCore filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingOuterCore}
 */
$xyz.swapee.wc.AbstractTestingOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractTestingOuterCore)} */
xyz.swapee.wc.AbstractTestingOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.RecordITestingOuterCore filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.BoundITestingOuterCore filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordITestingOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingOuterCoreCaster}
 */
$xyz.swapee.wc.BoundITestingOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingOuterCore} */
xyz.swapee.wc.BoundITestingOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.BoundTestingOuterCore filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingOuterCore} */
xyz.swapee.wc.BoundTestingOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Core.core filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.Core.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Host.host filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.Host.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn.amountIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {number} */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn.amountIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn.currencyIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn.currencyIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut.currencyOut filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut.currencyOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow.changeNow filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow.changeNow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Core filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.Core = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITestingOuterCore.Model.Core.prototype.core
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.Core} */
xyz.swapee.wc.ITestingOuterCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Host filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.Host = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITestingOuterCore.Model.Host.prototype.host
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.Host} */
xyz.swapee.wc.ITestingOuterCore.Model.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.AmountIn = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITestingOuterCore.Model.AmountIn.prototype.amountIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.AmountIn} */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn.prototype.currencyIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut.prototype.currencyOut
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow.prototype.changeNow
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow} */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Core}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Host}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow}
 */
$xyz.swapee.wc.ITestingOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model} */
xyz.swapee.wc.ITestingOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Core filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Core = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Core.prototype.core
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.Core} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Host filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Host = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Host.prototype.host
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.Host} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn.prototype.amountIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn.prototype.currencyIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut.prototype.currencyOut
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow.prototype.changeNow
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow}
 */
$xyz.swapee.wc.ITestingOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel} */
xyz.swapee.wc.ITestingOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe} */
xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe.prototype.host
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe} */
xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe.prototype.amountIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe} */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe.prototype.currencyIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe.prototype.currencyOut
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe.prototype.changeNow
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe} */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe.prototype.host
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe.prototype.amountIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe.prototype.currencyIn
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe.prototype.currencyOut
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe.prototype.changeNow
/** @typedef {$xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Core filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.ITestingPort.Inputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.Core} */
xyz.swapee.wc.ITestingPort.Inputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Core_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.ITestingPort.Inputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.Core_Safe} */
xyz.swapee.wc.ITestingPort.Inputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Host filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host}
 */
$xyz.swapee.wc.ITestingPort.Inputs.Host = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.Host} */
xyz.swapee.wc.ITestingPort.Inputs.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Host_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe}
 */
$xyz.swapee.wc.ITestingPort.Inputs.Host_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.Host_Safe} */
xyz.swapee.wc.ITestingPort.Inputs.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.AmountIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn}
 */
$xyz.swapee.wc.ITestingPort.Inputs.AmountIn = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.AmountIn} */
xyz.swapee.wc.ITestingPort.Inputs.AmountIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.AmountIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe}
 */
$xyz.swapee.wc.ITestingPort.Inputs.AmountIn_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.AmountIn_Safe} */
xyz.swapee.wc.ITestingPort.Inputs.AmountIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn}
 */
$xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn} */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe}
 */
$xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn_Safe} */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut}
 */
$xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut} */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut_Safe} */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.ChangeNow filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow}
 */
$xyz.swapee.wc.ITestingPort.Inputs.ChangeNow = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.ChangeNow} */
xyz.swapee.wc.ITestingPort.Inputs.ChangeNow

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.ChangeNow_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe}
 */
$xyz.swapee.wc.ITestingPort.Inputs.ChangeNow_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Inputs.ChangeNow_Safe} */
xyz.swapee.wc.ITestingPort.Inputs.ChangeNow_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Core filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.Core} */
xyz.swapee.wc.ITestingPort.WeakInputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Core_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.Core_Safe} */
xyz.swapee.wc.ITestingPort.WeakInputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Host filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.Host = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.Host} */
xyz.swapee.wc.ITestingPort.WeakInputs.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Host_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.Host_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.Host_Safe} */
xyz.swapee.wc.ITestingPort.WeakInputs.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn} */
xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn_Safe} */
xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn} */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn_Safe} */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut} */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut_Safe} */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow} */
xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow_Safe} */
xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Core filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Core}
 */
$xyz.swapee.wc.ITestingCore.Model.Core = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.Core} */
xyz.swapee.wc.ITestingCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Core_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe}
 */
$xyz.swapee.wc.ITestingCore.Model.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.Core_Safe} */
xyz.swapee.wc.ITestingCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Host filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Host}
 */
$xyz.swapee.wc.ITestingCore.Model.Host = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.Host} */
xyz.swapee.wc.ITestingCore.Model.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Host_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe}
 */
$xyz.swapee.wc.ITestingCore.Model.Host_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.Host_Safe} */
xyz.swapee.wc.ITestingCore.Model.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.AmountIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn}
 */
$xyz.swapee.wc.ITestingCore.Model.AmountIn = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.AmountIn} */
xyz.swapee.wc.ITestingCore.Model.AmountIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe}
 */
$xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe} */
xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyIn filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn}
 */
$xyz.swapee.wc.ITestingCore.Model.CurrencyIn = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.CurrencyIn} */
xyz.swapee.wc.ITestingCore.Model.CurrencyIn

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe}
 */
$xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe} */
xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyOut filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut}
 */
$xyz.swapee.wc.ITestingCore.Model.CurrencyOut = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.CurrencyOut} */
xyz.swapee.wc.ITestingCore.Model.CurrencyOut

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} */
xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangeNow filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow}
 */
$xyz.swapee.wc.ITestingCore.Model.ChangeNow = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangeNow} */
xyz.swapee.wc.ITestingCore.Model.ChangeNow

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe}
 */
$xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe} */
xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingCore.Model
/* @typal-end */