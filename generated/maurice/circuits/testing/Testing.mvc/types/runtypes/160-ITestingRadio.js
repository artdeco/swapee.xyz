/** @const {?} */ $xyz.swapee.wc.ITestingRadio
/** @const {?} */ $xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer
/** @const {?} */ $xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer
/** @const {?} */ $xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer
/** @const {?} */ xyz.swapee.wc.ITestingRadio
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.Initialese filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/** @record */
$xyz.swapee.wc.ITestingRadio.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingRadio.Initialese} */
xyz.swapee.wc.ITestingRadio.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingRadio
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadioCaster filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/** @interface */
$xyz.swapee.wc.ITestingRadioCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingRadio} */
$xyz.swapee.wc.ITestingRadioCaster.prototype.asITestingRadio
/** @type {!xyz.swapee.wc.BoundITestingComputer} */
$xyz.swapee.wc.ITestingRadioCaster.prototype.asITestingComputer
/** @type {!xyz.swapee.wc.BoundTestingRadio} */
$xyz.swapee.wc.ITestingRadioCaster.prototype.superTestingRadio
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingRadioCaster}
 */
xyz.swapee.wc.ITestingRadioCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingRadioCaster}
 */
$xyz.swapee.wc.ITestingRadio = function() {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangenowOffer = function(form, changes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingRadio}
 */
xyz.swapee.wc.ITestingRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.TestingRadio filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingRadio}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingRadio.Initialese>}
 */
$xyz.swapee.wc.TestingRadio = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.TestingRadio
/** @type {function(new: xyz.swapee.wc.ITestingRadio)} */
xyz.swapee.wc.TestingRadio.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.TestingRadio.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.AbstractTestingRadio filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingRadio}
 */
$xyz.swapee.wc.AbstractTestingRadio = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio
/** @type {function(new: xyz.swapee.wc.AbstractTestingRadio)} */
xyz.swapee.wc.AbstractTestingRadio.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio)} Implementations
 * @return {typeof xyz.swapee.wc.TestingRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingRadio.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.__extend
/**
 * @param {...(!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio)} Implementations
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.continues
/**
 * @param {...(!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio)} Implementations
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.RecordITestingRadio filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/** @typedef {{ adaptLoadChangellyFloatingOffer: xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer, adaptLoadChangellyFixedOffer: xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer, adaptLoadChangenowOffer: xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer }} */
xyz.swapee.wc.RecordITestingRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.BoundITestingRadio filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingRadio}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingRadioCaster}
 */
$xyz.swapee.wc.BoundITestingRadio = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingRadio} */
xyz.swapee.wc.BoundITestingRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.BoundTestingRadio filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingRadio}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingRadio = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingRadio} */
xyz.swapee.wc.BoundTestingRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.ITestingRadio}
 */
$xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingRadio,$xyz.swapee.wc.ITestingRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.ITestingRadio}
 */
$xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingRadio,$xyz.swapee.wc.ITestingRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)}
 * @this {xyz.swapee.wc.ITestingRadio}
 */
$xyz.swapee.wc.ITestingRadio._adaptLoadChangenowOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio._adaptLoadChangenowOffer} */
xyz.swapee.wc.ITestingRadio._adaptLoadChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer} */
xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer

// nss:xyz.swapee.wc.ITestingRadio,$xyz.swapee.wc.ITestingRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 */
$xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer
/* @typal-end */