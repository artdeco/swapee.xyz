/** @const {?} */ $xyz.swapee.wc.ITestingGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingDisplay.Initialese}
 */
$xyz.swapee.wc.ITestingGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingGPU.Initialese} */
xyz.swapee.wc.ITestingGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ITestingGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.ITestingGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingGPUFields}
 */
xyz.swapee.wc.ITestingGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ITestingGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingGPU} */
$xyz.swapee.wc.ITestingGPUCaster.prototype.asITestingGPU
/** @type {!xyz.swapee.wc.BoundTestingGPU} */
$xyz.swapee.wc.ITestingGPUCaster.prototype.superTestingGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingGPUCaster}
 */
xyz.swapee.wc.ITestingGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!TestingMemory,>}
 * @extends {xyz.swapee.wc.back.ITestingDisplay}
 */
$xyz.swapee.wc.ITestingGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingGPU}
 */
xyz.swapee.wc.ITestingGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.TestingGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init
 * @implements {xyz.swapee.wc.ITestingGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingGPU.Initialese>}
 */
$xyz.swapee.wc.TestingGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.TestingGPU
/** @type {function(new: xyz.swapee.wc.ITestingGPU, ...!xyz.swapee.wc.ITestingGPU.Initialese)} */
xyz.swapee.wc.TestingGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.TestingGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.AbstractTestingGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init
 * @extends {xyz.swapee.wc.TestingGPU}
 */
$xyz.swapee.wc.AbstractTestingGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU
/** @type {function(new: xyz.swapee.wc.AbstractTestingGPU)} */
xyz.swapee.wc.AbstractTestingGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.TestingGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.ITestingGPU, ...!xyz.swapee.wc.ITestingGPU.Initialese)} */
xyz.swapee.wc.TestingGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.RecordITestingGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.BoundITestingGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingGPUFields}
 * @extends {xyz.swapee.wc.RecordITestingGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!TestingMemory,>}
 * @extends {xyz.swapee.wc.back.BoundITestingDisplay}
 */
$xyz.swapee.wc.BoundITestingGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingGPU} */
xyz.swapee.wc.BoundITestingGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.BoundTestingGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingGPU} */
xyz.swapee.wc.BoundTestingGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */