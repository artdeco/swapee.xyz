/** @const {?} */ $xyz.swapee.wc.ITestingScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.ITestingScreen.Initialese filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.ITestingDisplay.Initialese}
 */
$xyz.swapee.wc.ITestingScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingScreen.Initialese} */
xyz.swapee.wc.ITestingScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.ITestingScreenCaster filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/** @interface */
$xyz.swapee.wc.ITestingScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingScreen} */
$xyz.swapee.wc.ITestingScreenCaster.prototype.asITestingScreen
/** @type {!xyz.swapee.wc.BoundTestingScreen} */
$xyz.swapee.wc.ITestingScreenCaster.prototype.superTestingScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingScreenCaster}
 */
xyz.swapee.wc.ITestingScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.ITestingScreen filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.ITestingController}
 * @extends {xyz.swapee.wc.ITestingDisplay}
 */
$xyz.swapee.wc.ITestingScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingScreen}
 */
xyz.swapee.wc.ITestingScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.TestingScreen filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init
 * @implements {xyz.swapee.wc.ITestingScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingScreen.Initialese>}
 */
$xyz.swapee.wc.TestingScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.TestingScreen
/** @type {function(new: xyz.swapee.wc.ITestingScreen, ...!xyz.swapee.wc.ITestingScreen.Initialese)} */
xyz.swapee.wc.TestingScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.TestingScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.AbstractTestingScreen filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init
 * @extends {xyz.swapee.wc.TestingScreen}
 */
$xyz.swapee.wc.AbstractTestingScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen
/** @type {function(new: xyz.swapee.wc.AbstractTestingScreen)} */
xyz.swapee.wc.AbstractTestingScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.TestingScreenConstructor filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/** @typedef {function(new: xyz.swapee.wc.ITestingScreen, ...!xyz.swapee.wc.ITestingScreen.Initialese)} */
xyz.swapee.wc.TestingScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.RecordITestingScreen filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.BoundITestingScreen filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundITestingController}
 * @extends {xyz.swapee.wc.BoundITestingDisplay}
 */
$xyz.swapee.wc.BoundITestingScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingScreen} */
xyz.swapee.wc.BoundITestingScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.BoundTestingScreen filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingScreen} */
xyz.swapee.wc.BoundTestingScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */