/** @const {?} */ $xyz.swapee.wc.ITestingElement
/** @const {?} */ xyz.swapee.wc.ITestingElement
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.Initialese filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.ITestingElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingElement.Initialese} */
xyz.swapee.wc.ITestingElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElementFields filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @interface */
$xyz.swapee.wc.ITestingElementFields = function() {}
/** @type {!xyz.swapee.wc.ITestingElement.Inputs} */
$xyz.swapee.wc.ITestingElementFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingElementFields}
 */
xyz.swapee.wc.ITestingElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElementCaster filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @interface */
$xyz.swapee.wc.ITestingElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingElement} */
$xyz.swapee.wc.ITestingElementCaster.prototype.asITestingElement
/** @type {!xyz.swapee.wc.BoundTestingElement} */
$xyz.swapee.wc.ITestingElementCaster.prototype.superTestingElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingElementCaster}
 */
xyz.swapee.wc.ITestingElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs, null>}
 */
$xyz.swapee.wc.ITestingElement = function() {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} model
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.ITestingElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITestingElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITestingElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.ITestingElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingElement}
 */
xyz.swapee.wc.ITestingElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.TestingElement filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init
 * @implements {xyz.swapee.wc.ITestingElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingElement.Initialese>}
 */
$xyz.swapee.wc.TestingElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.TestingElement
/** @type {function(new: xyz.swapee.wc.ITestingElement, ...!xyz.swapee.wc.ITestingElement.Initialese)} */
xyz.swapee.wc.TestingElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.TestingElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.AbstractTestingElement filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init
 * @extends {xyz.swapee.wc.TestingElement}
 */
$xyz.swapee.wc.AbstractTestingElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingElement}
 */
xyz.swapee.wc.AbstractTestingElement
/** @type {function(new: xyz.swapee.wc.AbstractTestingElement)} */
xyz.swapee.wc.AbstractTestingElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.TestingElementConstructor filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @typedef {function(new: xyz.swapee.wc.ITestingElement, ...!xyz.swapee.wc.ITestingElement.Initialese)} */
xyz.swapee.wc.TestingElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.RecordITestingElement filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @typedef {{ solder: xyz.swapee.wc.ITestingElement.solder, render: xyz.swapee.wc.ITestingElement.render, server: xyz.swapee.wc.ITestingElement.server, inducer: xyz.swapee.wc.ITestingElement.inducer }} */
xyz.swapee.wc.RecordITestingElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.BoundITestingElement filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingElementFields}
 * @extends {xyz.swapee.wc.RecordITestingElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs, null>}
 */
$xyz.swapee.wc.BoundITestingElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingElement} */
xyz.swapee.wc.BoundITestingElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.BoundTestingElement filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingElement} */
xyz.swapee.wc.BoundTestingElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.solder filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @param {!xyz.swapee.wc.TestingMemory} model
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.ITestingElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} model
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.ITestingElement}
 */
$xyz.swapee.wc.ITestingElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} model
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingElement.solder} */
xyz.swapee.wc.ITestingElement.solder
/** @typedef {typeof $xyz.swapee.wc.ITestingElement._solder} */
xyz.swapee.wc.ITestingElement._solder
/** @typedef {typeof $xyz.swapee.wc.ITestingElement.__solder} */
xyz.swapee.wc.ITestingElement.__solder

// nss:xyz.swapee.wc.ITestingElement,$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.render filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITestingElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ITestingElement.render
/** @typedef {function(this: xyz.swapee.wc.ITestingElement, !xyz.swapee.wc.TestingMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ITestingElement._render
/** @typedef {typeof $xyz.swapee.wc.ITestingElement.__render} */
xyz.swapee.wc.ITestingElement.__render

// nss:xyz.swapee.wc.ITestingElement,$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.server filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITestingElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.ITestingElement}
 */
$xyz.swapee.wc.ITestingElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingElement.server} */
xyz.swapee.wc.ITestingElement.server
/** @typedef {typeof $xyz.swapee.wc.ITestingElement._server} */
xyz.swapee.wc.ITestingElement._server
/** @typedef {typeof $xyz.swapee.wc.ITestingElement.__server} */
xyz.swapee.wc.ITestingElement.__server

// nss:xyz.swapee.wc.ITestingElement,$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.inducer filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} [port]
 */
$xyz.swapee.wc.ITestingElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} [port]
 * @this {xyz.swapee.wc.ITestingElement}
 */
$xyz.swapee.wc.ITestingElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingElement.inducer} */
xyz.swapee.wc.ITestingElement.inducer
/** @typedef {typeof $xyz.swapee.wc.ITestingElement._inducer} */
xyz.swapee.wc.ITestingElement._inducer
/** @typedef {typeof $xyz.swapee.wc.ITestingElement.__inducer} */
xyz.swapee.wc.ITestingElement.__inducer

// nss:xyz.swapee.wc.ITestingElement,$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.Inputs filter:!ControllerPlugin~props 2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPort.Inputs}
 * @extends {xyz.swapee.wc.ITestingDisplay.Queries}
 * @extends {xyz.swapee.wc.ITestingController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs}
 */
$xyz.swapee.wc.ITestingElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.ITestingElement.Inputs} */
xyz.swapee.wc.ITestingElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingElement
/* @typal-end */