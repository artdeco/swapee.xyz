/** @const {?} */ $xyz.swapee.wc.ITestingDesigner
/** @const {?} */ $xyz.swapee.wc.ITestingDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.ITestingDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @interface */
$xyz.swapee.wc.ITestingDesigner = function() {}
/**
 * @param {xyz.swapee.wc.TestingClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITestingDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.TestingClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITestingDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.ITestingDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.ITestingDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.ITestingDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.ITestingDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.ITestingDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.TestingClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITestingDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingDesigner}
 */
xyz.swapee.wc.ITestingDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.TestingDesigner filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingDesigner}
 */
$xyz.swapee.wc.TestingDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TestingDesigner}
 */
xyz.swapee.wc.TestingDesigner
/** @type {function(new: xyz.swapee.wc.ITestingDesigner)} */
xyz.swapee.wc.TestingDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner.communicator.Mesh filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @record */
$xyz.swapee.wc.ITestingDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.ITestingController} */
$xyz.swapee.wc.ITestingDesigner.communicator.Mesh.prototype.Testing
/** @typedef {$xyz.swapee.wc.ITestingDesigner.communicator.Mesh} */
xyz.swapee.wc.ITestingDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner.relay.Mesh filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @record */
$xyz.swapee.wc.ITestingDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.ITestingController} */
$xyz.swapee.wc.ITestingDesigner.relay.Mesh.prototype.Testing
/** @type {typeof xyz.swapee.wc.ITestingController} */
$xyz.swapee.wc.ITestingDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.ITestingDesigner.relay.Mesh} */
xyz.swapee.wc.ITestingDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner.relay.MemPool filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @record */
$xyz.swapee.wc.ITestingDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.TestingMemory} */
$xyz.swapee.wc.ITestingDesigner.relay.MemPool.prototype.Testing
/** @type {!xyz.swapee.wc.TestingMemory} */
$xyz.swapee.wc.ITestingDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.ITestingDesigner.relay.MemPool} */
xyz.swapee.wc.ITestingDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingDesigner.relay
/* @typal-end */