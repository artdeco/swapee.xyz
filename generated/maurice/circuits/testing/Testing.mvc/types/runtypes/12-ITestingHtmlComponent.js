/** @const {?} */ $xyz.swapee.wc.ITestingHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.ITestingHtmlComponent.Initialese filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingController.Initialese}
 * @extends {xyz.swapee.wc.back.ITestingScreen.Initialese}
 * @extends {xyz.swapee.wc.ITesting.Initialese}
 * @extends {xyz.swapee.wc.ITestingGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.ITestingProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITestingComputer.Initialese}
 * @extends {ITestingGenerator.Initialese}
 */
$xyz.swapee.wc.ITestingHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingHtmlComponent.Initialese} */
xyz.swapee.wc.ITestingHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.ITestingHtmlComponentCaster filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/** @interface */
$xyz.swapee.wc.ITestingHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingHtmlComponent} */
$xyz.swapee.wc.ITestingHtmlComponentCaster.prototype.asITestingHtmlComponent
/** @type {!xyz.swapee.wc.BoundTestingHtmlComponent} */
$xyz.swapee.wc.ITestingHtmlComponentCaster.prototype.superTestingHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingHtmlComponentCaster}
 */
xyz.swapee.wc.ITestingHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.ITestingHtmlComponent filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.ITestingController}
 * @extends {xyz.swapee.wc.back.ITestingScreen}
 * @extends {xyz.swapee.wc.ITesting}
 * @extends {xyz.swapee.wc.ITestingGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.ITestingProcessor}
 * @extends {xyz.swapee.wc.ITestingComputer}
 * @extends {ITestingGenerator}
 */
$xyz.swapee.wc.ITestingHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingHtmlComponent}
 */
xyz.swapee.wc.ITestingHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.TestingHtmlComponent filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.ITestingHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.TestingHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.TestingHtmlComponent
/** @type {function(new: xyz.swapee.wc.ITestingHtmlComponent, ...!xyz.swapee.wc.ITestingHtmlComponent.Initialese)} */
xyz.swapee.wc.TestingHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.TestingHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.AbstractTestingHtmlComponent filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.TestingHtmlComponent}
 */
$xyz.swapee.wc.AbstractTestingHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractTestingHtmlComponent)} */
xyz.swapee.wc.AbstractTestingHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.TestingHtmlComponentConstructor filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/** @typedef {function(new: xyz.swapee.wc.ITestingHtmlComponent, ...!xyz.swapee.wc.ITestingHtmlComponent.Initialese)} */
xyz.swapee.wc.TestingHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.RecordITestingHtmlComponent filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.BoundITestingHtmlComponent filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundITestingController}
 * @extends {xyz.swapee.wc.back.BoundITestingScreen}
 * @extends {xyz.swapee.wc.BoundITesting}
 * @extends {xyz.swapee.wc.BoundITestingGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundITestingProcessor}
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {BoundITestingGenerator}
 */
$xyz.swapee.wc.BoundITestingHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingHtmlComponent} */
xyz.swapee.wc.BoundITestingHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.BoundTestingHtmlComponent filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingHtmlComponent} */
xyz.swapee.wc.BoundTestingHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */