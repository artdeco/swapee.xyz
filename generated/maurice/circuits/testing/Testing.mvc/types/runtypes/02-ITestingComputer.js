/** @const {?} */ $xyz.swapee.wc.ITestingComputer
/** @const {?} */ $xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer
/** @const {?} */ $xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer
/** @const {?} */ $xyz.swapee.wc.ITestingComputer.adaptChangenowOffer
/** @const {?} */ $xyz.swapee.wc.ITestingComputer.adaptAnyLoading
/** @const {?} */ xyz.swapee.wc.ITestingComputer
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.Initialese filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.ITestingComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.Initialese} */
xyz.swapee.wc.ITestingComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputerCaster filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/** @interface */
$xyz.swapee.wc.ITestingComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingComputer} */
$xyz.swapee.wc.ITestingComputerCaster.prototype.asITestingComputer
/** @type {!xyz.swapee.wc.BoundTestingComputer} */
$xyz.swapee.wc.ITestingComputerCaster.prototype.superTestingComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingComputerCaster}
 */
xyz.swapee.wc.ITestingComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.TestingMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.ITestingComputer = function() {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingComputer.prototype.adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingComputer.prototype.adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingComputer.prototype.adaptChangenowOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)}
 */
$xyz.swapee.wc.ITestingComputer.prototype.adaptAnyLoading = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.TestingMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.ITestingComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingComputer}
 */
xyz.swapee.wc.ITestingComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.TestingComputer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init
 * @implements {xyz.swapee.wc.ITestingComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingComputer.Initialese>}
 */
$xyz.swapee.wc.TestingComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.TestingComputer
/** @type {function(new: xyz.swapee.wc.ITestingComputer, ...!xyz.swapee.wc.ITestingComputer.Initialese)} */
xyz.swapee.wc.TestingComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.TestingComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.AbstractTestingComputer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init
 * @extends {xyz.swapee.wc.TestingComputer}
 */
$xyz.swapee.wc.AbstractTestingComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer
/** @type {function(new: xyz.swapee.wc.AbstractTestingComputer)} */
xyz.swapee.wc.AbstractTestingComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TestingComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.TestingComputerConstructor filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/** @typedef {function(new: xyz.swapee.wc.ITestingComputer, ...!xyz.swapee.wc.ITestingComputer.Initialese)} */
xyz.swapee.wc.TestingComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.RecordITestingComputer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/** @typedef {{ adaptChangellyFloatingOffer: xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer, adaptChangellyFixedOffer: xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer, adaptChangenowOffer: xyz.swapee.wc.ITestingComputer.adaptChangenowOffer, adaptAnyLoading: xyz.swapee.wc.ITestingComputer.adaptAnyLoading, compute: xyz.swapee.wc.ITestingComputer.compute }} */
xyz.swapee.wc.RecordITestingComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.BoundITestingComputer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.TestingMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundITestingComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingComputer} */
xyz.swapee.wc.BoundITestingComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.BoundTestingComputer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingComputer} */
xyz.swapee.wc.BoundTestingComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)}
 * @this {xyz.swapee.wc.ITestingComputer}
 */
$xyz.swapee.wc.ITestingComputer._adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer._adaptChangellyFloatingOffer} */
xyz.swapee.wc.ITestingComputer._adaptChangellyFloatingOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer} */
xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingComputer,$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.ITestingComputer}
 */
$xyz.swapee.wc.ITestingComputer._adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer._adaptChangellyFixedOffer} */
xyz.swapee.wc.ITestingComputer._adaptChangellyFixedOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer} */
xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingComputer,$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangenowOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)}
 * @this {xyz.swapee.wc.ITestingComputer}
 */
$xyz.swapee.wc.ITestingComputer._adaptChangenowOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.adaptChangenowOffer} */
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer._adaptChangenowOffer} */
xyz.swapee.wc.ITestingComputer._adaptChangenowOffer
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer} */
xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer

// nss:xyz.swapee.wc.ITestingComputer,$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptAnyLoading filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)}
 */
$xyz.swapee.wc.ITestingComputer.adaptAnyLoading = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)}
 * @this {xyz.swapee.wc.ITestingComputer}
 */
$xyz.swapee.wc.ITestingComputer._adaptAnyLoading = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITestingComputer.__adaptAnyLoading = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.adaptAnyLoading} */
xyz.swapee.wc.ITestingComputer.adaptAnyLoading
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer._adaptAnyLoading} */
xyz.swapee.wc.ITestingComputer._adaptAnyLoading
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.__adaptAnyLoading} */
xyz.swapee.wc.ITestingComputer.__adaptAnyLoading

// nss:xyz.swapee.wc.ITestingComputer,$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.compute filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.TestingMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.ITestingComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.TestingMemory): void} */
xyz.swapee.wc.ITestingComputer.compute
/** @typedef {function(this: xyz.swapee.wc.ITestingComputer, xyz.swapee.wc.TestingMemory): void} */
xyz.swapee.wc.ITestingComputer._compute
/** @typedef {typeof $xyz.swapee.wc.ITestingComputer.__compute} */
xyz.swapee.wc.ITestingComputer.__compute

// nss:xyz.swapee.wc.ITestingComputer,$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} */
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptChangenowOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 */
$xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return} */
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptChangenowOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe}
 */
$xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} */
xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptAnyLoading
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AnyLoading}
 */
$xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return} */
xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingComputer.adaptAnyLoading
/* @typal-end */