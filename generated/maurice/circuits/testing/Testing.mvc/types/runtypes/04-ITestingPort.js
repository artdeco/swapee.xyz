/** @const {?} */ $xyz.swapee.wc.ITestingPort
/** @const {?} */ xyz.swapee.wc.ITestingPort
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.Initialese filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.ITestingPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITestingPort.Initialese} */
xyz.swapee.wc.ITestingPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPortFields filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @interface */
$xyz.swapee.wc.ITestingPortFields = function() {}
/** @type {!xyz.swapee.wc.ITestingPort.Inputs} */
$xyz.swapee.wc.ITestingPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ITestingPort.Inputs} */
$xyz.swapee.wc.ITestingPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingPortFields}
 */
xyz.swapee.wc.ITestingPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPortCaster filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @interface */
$xyz.swapee.wc.ITestingPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITestingPort} */
$xyz.swapee.wc.ITestingPortCaster.prototype.asITestingPort
/** @type {!xyz.swapee.wc.BoundTestingPort} */
$xyz.swapee.wc.ITestingPortCaster.prototype.superTestingPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingPortCaster}
 */
xyz.swapee.wc.ITestingPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ITestingPort.Inputs>}
 */
$xyz.swapee.wc.ITestingPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ITestingPort.prototype.resetTestingPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITestingPort}
 */
xyz.swapee.wc.ITestingPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.TestingPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init
 * @implements {xyz.swapee.wc.ITestingPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingPort.Initialese>}
 */
$xyz.swapee.wc.TestingPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.TestingPort
/** @type {function(new: xyz.swapee.wc.ITestingPort, ...!xyz.swapee.wc.ITestingPort.Initialese)} */
xyz.swapee.wc.TestingPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.TestingPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.AbstractTestingPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init
 * @extends {xyz.swapee.wc.TestingPort}
 */
$xyz.swapee.wc.AbstractTestingPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTestingPort}
 */
xyz.swapee.wc.AbstractTestingPort
/** @type {function(new: xyz.swapee.wc.AbstractTestingPort)} */
xyz.swapee.wc.AbstractTestingPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.__extend
/**
 * @param {...((!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.continues
/**
 * @param {...((!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.TestingPortConstructor filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @typedef {function(new: xyz.swapee.wc.ITestingPort, ...!xyz.swapee.wc.ITestingPort.Initialese)} */
xyz.swapee.wc.TestingPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.RecordITestingPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITestingPort.resetPort, resetTestingPort: xyz.swapee.wc.ITestingPort.resetTestingPort }} */
xyz.swapee.wc.RecordITestingPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.BoundITestingPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPortFields}
 * @extends {xyz.swapee.wc.RecordITestingPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ITestingPort.Inputs>}
 */
$xyz.swapee.wc.BoundITestingPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundITestingPort} */
xyz.swapee.wc.BoundITestingPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.BoundTestingPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTestingPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundTestingPort} */
xyz.swapee.wc.BoundTestingPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.resetPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITestingPort): void} */
xyz.swapee.wc.ITestingPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.ITestingPort.__resetPort} */
xyz.swapee.wc.ITestingPort.__resetPort

// nss:xyz.swapee.wc.ITestingPort,$xyz.swapee.wc.ITestingPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.resetTestingPort filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITestingPort.__resetTestingPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingPort.resetTestingPort
/** @typedef {function(this: xyz.swapee.wc.ITestingPort): void} */
xyz.swapee.wc.ITestingPort._resetTestingPort
/** @typedef {typeof $xyz.swapee.wc.ITestingPort.__resetTestingPort} */
xyz.swapee.wc.ITestingPort.__resetTestingPort

// nss:xyz.swapee.wc.ITestingPort,$xyz.swapee.wc.ITestingPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.Inputs filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel}
 */
$xyz.swapee.wc.ITestingPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITestingPort.Inputs}
 */
xyz.swapee.wc.ITestingPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.WeakInputs filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel}
 */
$xyz.swapee.wc.ITestingPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITestingPort.WeakInputs}
 */
xyz.swapee.wc.ITestingPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITestingPort
/* @typal-end */