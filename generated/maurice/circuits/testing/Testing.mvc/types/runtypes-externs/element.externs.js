/**
 * @fileoverview
 * @externs
 */

/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.Initialese  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
xyz.swapee.wc.ITestingElement.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElementFields  2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @interface */
xyz.swapee.wc.ITestingElementFields
/** @type {!xyz.swapee.wc.ITestingElement.Inputs} */
xyz.swapee.wc.ITestingElementFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElementCaster  2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @interface */
xyz.swapee.wc.ITestingElementCaster
/** @type {!xyz.swapee.wc.BoundITestingElement} */
xyz.swapee.wc.ITestingElementCaster.prototype.asITestingElement
/** @type {!xyz.swapee.wc.BoundTestingElement} */
xyz.swapee.wc.ITestingElementCaster.prototype.superTestingElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs, null>}
 */
xyz.swapee.wc.ITestingElement = function() {}
/** @param {...!xyz.swapee.wc.ITestingElement.Initialese} init */
xyz.swapee.wc.ITestingElement.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} model
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} props
 * @return {Object<string, *>}
 */
xyz.swapee.wc.ITestingElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ITestingElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ITestingElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} [port]
 * @return {?}
 */
xyz.swapee.wc.ITestingElement.prototype.inducer = function(model, port) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.TestingElement  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init
 * @implements {xyz.swapee.wc.ITestingElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingElement.Initialese>}
 */
xyz.swapee.wc.TestingElement = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingElement.Initialese} init */
xyz.swapee.wc.TestingElement.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.TestingElement.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.AbstractTestingElement  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init
 * @extends {xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingElement.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.TestingElementConstructor  2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @typedef {function(new: xyz.swapee.wc.ITestingElement, ...!xyz.swapee.wc.ITestingElement.Initialese)} */
xyz.swapee.wc.TestingElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.RecordITestingElement  2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @typedef {{ solder: xyz.swapee.wc.ITestingElement.solder, render: xyz.swapee.wc.ITestingElement.render, server: xyz.swapee.wc.ITestingElement.server, inducer: xyz.swapee.wc.ITestingElement.inducer }} */
xyz.swapee.wc.RecordITestingElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.BoundITestingElement  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingElementFields}
 * @extends {xyz.swapee.wc.RecordITestingElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs, null>}
 */
xyz.swapee.wc.BoundITestingElement = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.BoundTestingElement  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingElement = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.solder  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} model
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} props
 * @return {Object<string, *>}
 */
$$xyz.swapee.wc.ITestingElement.__solder = function(model, props) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs): Object<string, *>} */
xyz.swapee.wc.ITestingElement.solder
/** @typedef {function(this: xyz.swapee.wc.ITestingElement, !xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs): Object<string, *>} */
xyz.swapee.wc.ITestingElement._solder
/** @typedef {typeof $$xyz.swapee.wc.ITestingElement.__solder} */
xyz.swapee.wc.ITestingElement.__solder

// nss:xyz.swapee.wc.ITestingElement,$$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.render  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$$xyz.swapee.wc.ITestingElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ITestingElement.render
/** @typedef {function(this: xyz.swapee.wc.ITestingElement, !xyz.swapee.wc.TestingMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ITestingElement._render
/** @typedef {typeof $$xyz.swapee.wc.ITestingElement.__render} */
xyz.swapee.wc.ITestingElement.__render

// nss:xyz.swapee.wc.ITestingElement,$$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.server  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$$xyz.swapee.wc.ITestingElement.__server = function(memory, inputs) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs): !engineering.type.VNode} */
xyz.swapee.wc.ITestingElement.server
/** @typedef {function(this: xyz.swapee.wc.ITestingElement, !xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs): !engineering.type.VNode} */
xyz.swapee.wc.ITestingElement._server
/** @typedef {typeof $$xyz.swapee.wc.ITestingElement.__server} */
xyz.swapee.wc.ITestingElement.__server

// nss:xyz.swapee.wc.ITestingElement,$$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.inducer  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} [model]
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} [port]
 */
$$xyz.swapee.wc.ITestingElement.__inducer = function(model, port) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory=, !xyz.swapee.wc.ITestingElement.Inputs=)} */
xyz.swapee.wc.ITestingElement.inducer
/** @typedef {function(this: xyz.swapee.wc.ITestingElement, !xyz.swapee.wc.TestingMemory=, !xyz.swapee.wc.ITestingElement.Inputs=)} */
xyz.swapee.wc.ITestingElement._inducer
/** @typedef {typeof $$xyz.swapee.wc.ITestingElement.__inducer} */
xyz.swapee.wc.ITestingElement.__inducer

// nss:xyz.swapee.wc.ITestingElement,$$xyz.swapee.wc.ITestingElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml} xyz.swapee.wc.ITestingElement.Inputs  2aa1e8d9657ecdde39955a7c15cdefb8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPort.Inputs}
 * @extends {xyz.swapee.wc.ITestingDisplay.Queries}
 * @extends {xyz.swapee.wc.ITestingController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.ITestingElementPort.Inputs}
 */
xyz.swapee.wc.ITestingElement.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */