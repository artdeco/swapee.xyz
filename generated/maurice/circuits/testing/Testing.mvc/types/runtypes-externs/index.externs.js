/**
 * @fileoverview
 * @externs
 */

xyz.swapee.wc.ITestingComputer={}
xyz.swapee.wc.ITestingPort={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ITestingController={}
xyz.swapee.wc.ITestingRadio={}
xyz.swapee.wc.ITestingDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ITestingDisplay={}
xyz.swapee.wc.ITestingController={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.wc={}
$$xyz.swapee.wc.ITestingComputer={}
$$xyz.swapee.wc.ITestingPort={}
$$xyz.swapee.wc.ITestingRadio={}
$$xyz.swapee.wc.ITestingDisplay={}
$$xyz.swapee.wc.back={}
$$xyz.swapee.wc.back.ITestingDisplay={}
$$xyz.swapee.wc.ITestingController={}
$$xyz.swapee.wc.front={}
$$xyz.swapee.wc.front.ITestingController={}
$$xyz.swapee.wc.ITestingCore={}
$$xyz.swapee.wc.ITestingService={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
xyz.swapee.wc.ITestingComputer.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/** @interface */
xyz.swapee.wc.ITestingComputerCaster
/** @type {!xyz.swapee.wc.BoundITestingComputer} */
xyz.swapee.wc.ITestingComputerCaster.prototype.asITestingComputer
/** @type {!xyz.swapee.wc.BoundTestingComputer} */
xyz.swapee.wc.ITestingComputerCaster.prototype.superTestingComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.TestingMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
xyz.swapee.wc.ITestingComputer = function() {}
/** @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init */
xyz.swapee.wc.ITestingComputer.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.ITestingComputer.prototype.adaptChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.ITestingComputer.prototype.adaptChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)}
 */
xyz.swapee.wc.ITestingComputer.prototype.adaptChangenowOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)}
 */
xyz.swapee.wc.ITestingComputer.prototype.adaptAnyLoading = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.TestingMemory} mem
 * @return {void}
 */
xyz.swapee.wc.ITestingComputer.prototype.compute = function(mem) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.TestingComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init
 * @implements {xyz.swapee.wc.ITestingComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingComputer.Initialese>}
 */
xyz.swapee.wc.TestingComputer = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init */
xyz.swapee.wc.TestingComputer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.TestingComputer.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.AbstractTestingComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init
 * @extends {xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TestingComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingComputer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.TestingComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/** @typedef {function(new: xyz.swapee.wc.ITestingComputer, ...!xyz.swapee.wc.ITestingComputer.Initialese)} */
xyz.swapee.wc.TestingComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.RecordITestingComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/** @typedef {{ adaptChangellyFloatingOffer: xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer, adaptChangellyFixedOffer: xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer, adaptChangenowOffer: xyz.swapee.wc.ITestingComputer.adaptChangenowOffer, adaptAnyLoading: xyz.swapee.wc.ITestingComputer.adaptAnyLoading, compute: xyz.swapee.wc.ITestingComputer.compute }} */
xyz.swapee.wc.RecordITestingComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.BoundITestingComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.TestingMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
xyz.swapee.wc.BoundITestingComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.BoundTestingComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form, xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingComputer, !xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form, xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.ITestingComputer._adaptChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer} */
xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingComputer,$$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form, xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingComputer, !xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form, xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.ITestingComputer._adaptChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer} */
xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingComputer,$$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)}
 */
$$xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form, xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)} */
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingComputer, !xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form, xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)} */
xyz.swapee.wc.ITestingComputer._adaptChangenowOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer} */
xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer

// nss:xyz.swapee.wc.ITestingComputer,$$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptAnyLoading exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} form
 * @param {xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)}
 */
$$xyz.swapee.wc.ITestingComputer.__adaptAnyLoading = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form, xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form): (undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)} */
xyz.swapee.wc.ITestingComputer.adaptAnyLoading
/** @typedef {function(this: xyz.swapee.wc.ITestingComputer, !xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form, xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form): (undefined|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)} */
xyz.swapee.wc.ITestingComputer._adaptAnyLoading
/** @typedef {typeof $$xyz.swapee.wc.ITestingComputer.__adaptAnyLoading} */
xyz.swapee.wc.ITestingComputer.__adaptAnyLoading

// nss:xyz.swapee.wc.ITestingComputer,$$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.TestingMemory} mem
 * @return {void}
 */
$$xyz.swapee.wc.ITestingComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.TestingMemory): void} */
xyz.swapee.wc.ITestingComputer.compute
/** @typedef {function(this: xyz.swapee.wc.ITestingComputer, xyz.swapee.wc.TestingMemory): void} */
xyz.swapee.wc.ITestingComputer._compute
/** @typedef {typeof $$xyz.swapee.wc.ITestingComputer.__compute} */
xyz.swapee.wc.ITestingComputer.__compute

// nss:xyz.swapee.wc.ITestingComputer,$$xyz.swapee.wc.ITestingComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe.prototype.amountIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe}
 */
xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe.prototype.currencyIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe}
 */
xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe.prototype.currencyOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer.prototype.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 */
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer.prototype.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 */
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe.prototype.changeNow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe}
 */
xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer = function() {}
/** @type {({ amountOut: string, type: string })|undefined} */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer.prototype.changenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 */
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe.prototype.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe.prototype.loadingChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe.prototype.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe}
 */
xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.AnyLoading exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.AnyLoading = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ITestingCore.Model.AnyLoading.prototype.anyLoading

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml} xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e754d1027b8b1dbebf0af18ecfecef48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AnyLoading}
 */
xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @interface */
xyz.swapee.wc.ITestingOuterCoreFields
/** @type {!xyz.swapee.wc.ITestingOuterCore.Model} */
xyz.swapee.wc.ITestingOuterCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @interface */
xyz.swapee.wc.ITestingOuterCoreCaster
/** @type {!xyz.swapee.wc.BoundITestingOuterCore} */
xyz.swapee.wc.ITestingOuterCoreCaster.prototype.asITestingOuterCore
/** @type {!xyz.swapee.wc.BoundTestingOuterCore} */
xyz.swapee.wc.ITestingOuterCoreCaster.prototype.superTestingOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingOuterCoreCaster}
 */
xyz.swapee.wc.ITestingOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.TestingOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingOuterCore.Initialese>}
 */
xyz.swapee.wc.TestingOuterCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.TestingOuterCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.AbstractTestingOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore = function() {}
/**
 * @param {...(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingOuterCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.TestingMemoryPQs = function() {}
/** @type {string} */
xyz.swapee.wc.TestingMemoryPQs.prototype.core
/** @type {string} */
xyz.swapee.wc.TestingMemoryPQs.prototype.host
/** @type {string} */
xyz.swapee.wc.TestingMemoryPQs.prototype.amountIn
/** @type {string} */
xyz.swapee.wc.TestingMemoryPQs.prototype.currencyIn
/** @type {string} */
xyz.swapee.wc.TestingMemoryPQs.prototype.currencyOut
/** @type {string} */
xyz.swapee.wc.TestingMemoryPQs.prototype.changeNow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.TestingMemoryQPs = function() {}
/** @type {string} */
xyz.swapee.wc.TestingMemoryQPs.prototype.a74ad
/** @type {string} */
xyz.swapee.wc.TestingMemoryQPs.prototype.g7b3d
/** @type {string} */
xyz.swapee.wc.TestingMemoryQPs.prototype.i8da7
/** @type {string} */
xyz.swapee.wc.TestingMemoryQPs.prototype.f3088
/** @type {string} */
xyz.swapee.wc.TestingMemoryQPs.prototype.j8dbb
/** @type {string} */
xyz.swapee.wc.TestingMemoryQPs.prototype.j4af8

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.RecordITestingOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.BoundITestingOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordITestingOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingOuterCoreCaster}
 */
xyz.swapee.wc.BoundITestingOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.BoundTestingOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Core.core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.Core.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Host.host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.Host.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn.amountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {number} */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn.amountIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn.currencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn.currencyIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut.currencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut.currencyOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow.changeNow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow.changeNow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.Core = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ITestingOuterCore.Model.Core.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.Host = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ITestingOuterCore.Model.Host.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn.prototype.amountIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn.prototype.currencyIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut.prototype.currencyOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow.prototype.changeNow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Core}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Host}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut}
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow}
 */
xyz.swapee.wc.ITestingOuterCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Core = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Core.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Host = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Host.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn.prototype.amountIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn.prototype.currencyIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut.prototype.currencyOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow.prototype.changeNow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut}
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow}
 */
xyz.swapee.wc.ITestingOuterCore.WeakModel = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
xyz.swapee.wc.ITestingPort.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @interface */
xyz.swapee.wc.ITestingPortFields
/** @type {!xyz.swapee.wc.ITestingPort.Inputs} */
xyz.swapee.wc.ITestingPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ITestingPort.Inputs} */
xyz.swapee.wc.ITestingPortFields.prototype.props

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @interface */
xyz.swapee.wc.ITestingPortCaster
/** @type {!xyz.swapee.wc.BoundITestingPort} */
xyz.swapee.wc.ITestingPortCaster.prototype.asITestingPort
/** @type {!xyz.swapee.wc.BoundTestingPort} */
xyz.swapee.wc.ITestingPortCaster.prototype.superTestingPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ITestingPort.Inputs>}
 */
xyz.swapee.wc.ITestingPort = function() {}
/** @param {...!xyz.swapee.wc.ITestingPort.Initialese} init */
xyz.swapee.wc.ITestingPort.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.ITestingPort.prototype.resetPort = function() {}
/** @return {void} */
xyz.swapee.wc.ITestingPort.prototype.resetTestingPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.TestingPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init
 * @implements {xyz.swapee.wc.ITestingPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingPort.Initialese>}
 */
xyz.swapee.wc.TestingPort = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingPort.Initialese} init */
xyz.swapee.wc.TestingPort.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.TestingPort.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.AbstractTestingPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init
 * @extends {xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingPort.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.TestingPortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @typedef {function(new: xyz.swapee.wc.ITestingPort, ...!xyz.swapee.wc.ITestingPort.Initialese)} */
xyz.swapee.wc.TestingPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.TestingMemoryPQs}
 */
xyz.swapee.wc.TestingInputsPQs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingMemoryPQs}
 * @dict
 */
xyz.swapee.wc.TestingInputsQPs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.RecordITestingPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITestingPort.resetPort, resetTestingPort: xyz.swapee.wc.ITestingPort.resetTestingPort }} */
xyz.swapee.wc.RecordITestingPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.BoundITestingPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPortFields}
 * @extends {xyz.swapee.wc.RecordITestingPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ITestingPort.Inputs>}
 */
xyz.swapee.wc.BoundITestingPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.BoundTestingPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITestingPort): void} */
xyz.swapee.wc.ITestingPort._resetPort
/** @typedef {typeof $$xyz.swapee.wc.ITestingPort.__resetPort} */
xyz.swapee.wc.ITestingPort.__resetPort

// nss:xyz.swapee.wc.ITestingPort,$$xyz.swapee.wc.ITestingPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.resetTestingPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingPort.__resetTestingPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingPort.resetTestingPort
/** @typedef {function(this: xyz.swapee.wc.ITestingPort): void} */
xyz.swapee.wc.ITestingPort._resetTestingPort
/** @typedef {typeof $$xyz.swapee.wc.ITestingPort.__resetTestingPort} */
xyz.swapee.wc.ITestingPort.__resetTestingPort

// nss:xyz.swapee.wc.ITestingPort,$$xyz.swapee.wc.ITestingPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel}
 */
xyz.swapee.wc.ITestingPort.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml} xyz.swapee.wc.ITestingPort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7bbf16f1eee9067a729c193c30ea8b29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel}
 */
xyz.swapee.wc.ITestingPort.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @interface */
xyz.swapee.wc.ITestingCoreFields
/** @type {!xyz.swapee.wc.ITestingCore.Model} */
xyz.swapee.wc.ITestingCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @interface */
xyz.swapee.wc.ITestingCoreCaster
/** @type {!xyz.swapee.wc.BoundITestingCore} */
xyz.swapee.wc.ITestingCoreCaster.prototype.asITestingCore
/** @type {!xyz.swapee.wc.BoundTestingCore} */
xyz.swapee.wc.ITestingCoreCaster.prototype.superTestingCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingCoreCaster}
 * @extends {xyz.swapee.wc.ITestingOuterCore}
 */
xyz.swapee.wc.ITestingCore = function() {}
/** @return {void} */
xyz.swapee.wc.ITestingCore.prototype.resetCore = function() {}
/** @return {void} */
xyz.swapee.wc.ITestingCore.prototype.resetTestingCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.TestingCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingCore.Initialese>}
 */
xyz.swapee.wc.TestingCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.TestingCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.AbstractTestingCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore = function() {}
/**
 * @param {...((!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TestingCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingCachePQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.TestingCachePQs = function() {}
/** @type {string} */
xyz.swapee.wc.TestingCachePQs.prototype.changellyFixedOffer
/** @type {string} */
xyz.swapee.wc.TestingCachePQs.prototype.changellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.TestingCachePQs.prototype.changenowOffer
/** @type {string} */
xyz.swapee.wc.TestingCachePQs.prototype.anyLoading

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingCacheQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.TestingCacheQPs = function() {}
/** @type {string} */
xyz.swapee.wc.TestingCacheQPs.prototype.hb5f4
/** @type {string} */
xyz.swapee.wc.TestingCacheQPs.prototype.ca73d
/** @type {string} */
xyz.swapee.wc.TestingCacheQPs.prototype.dc0e8
/** @type {string} */
xyz.swapee.wc.TestingCacheQPs.prototype.ac77f

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.RecordITestingCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {{ resetCore: xyz.swapee.wc.ITestingCore.resetCore, resetTestingCore: xyz.swapee.wc.ITestingCore.resetTestingCore }} */
xyz.swapee.wc.RecordITestingCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.BoundITestingCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCoreFields}
 * @extends {xyz.swapee.wc.RecordITestingCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingCoreCaster}
 * @extends {xyz.swapee.wc.BoundITestingOuterCore}
 */
xyz.swapee.wc.BoundITestingCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.BoundTestingCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.ITestingCore): void} */
xyz.swapee.wc.ITestingCore._resetCore
/** @typedef {typeof $$xyz.swapee.wc.ITestingCore.__resetCore} */
xyz.swapee.wc.ITestingCore.__resetCore

// nss:xyz.swapee.wc.ITestingCore,$$xyz.swapee.wc.ITestingCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.resetTestingCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingCore.__resetTestingCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingCore.resetTestingCore
/** @typedef {function(this: xyz.swapee.wc.ITestingCore): void} */
xyz.swapee.wc.ITestingCore._resetTestingCore
/** @typedef {typeof $$xyz.swapee.wc.ITestingCore.__resetTestingCore} */
xyz.swapee.wc.ITestingCore.__resetTestingCore

// nss:xyz.swapee.wc.ITestingCore,$$xyz.swapee.wc.ITestingCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer.changellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer.changellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer.changenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {{ amountOut: string, type: string }} */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer.changenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.AnyLoading.anyLoading exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.AnyLoading.anyLoading

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer.loadingChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer.loadingChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer.hasMoreChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer.hasMoreChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError.loadChangenowOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError.loadChangenowOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer.prototype.loadingChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer.prototype.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError.prototype.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer.prototype.loadingChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer.prototype.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError.prototype.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer.prototype.loadingChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer.prototype.hasMoreChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError.prototype.loadChangenowOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model}
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AnyLoading}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer}
 * @extends {xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError}
 */
xyz.swapee.wc.ITestingCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITestingOuterCore.WeakModel>}
 */
xyz.swapee.wc.ITestingController.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.ITestingProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingComputer.Initialese}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
xyz.swapee.wc.ITestingProcessor.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.ITestingProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/** @interface */
xyz.swapee.wc.ITestingProcessorCaster
/** @type {!xyz.swapee.wc.BoundITestingProcessor} */
xyz.swapee.wc.ITestingProcessorCaster.prototype.asITestingProcessor
/** @type {!xyz.swapee.wc.BoundTestingProcessor} */
xyz.swapee.wc.ITestingProcessorCaster.prototype.superTestingProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @interface */
xyz.swapee.wc.ITestingControllerFields
/** @type {!xyz.swapee.wc.ITestingController.Inputs} */
xyz.swapee.wc.ITestingControllerFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @interface */
xyz.swapee.wc.ITestingControllerCaster
/** @type {!xyz.swapee.wc.BoundITestingController} */
xyz.swapee.wc.ITestingControllerCaster.prototype.asITestingController
/** @type {!xyz.swapee.wc.BoundITestingProcessor} */
xyz.swapee.wc.ITestingControllerCaster.prototype.asITestingProcessor
/** @type {!xyz.swapee.wc.BoundTestingController} */
xyz.swapee.wc.ITestingControllerCaster.prototype.superTestingController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.ITestingOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.TestingMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITestingController.Inputs>}
 */
xyz.swapee.wc.ITestingController = function() {}
/** @param {...!xyz.swapee.wc.ITestingController.Initialese} init */
xyz.swapee.wc.ITestingController.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.resetPort = function() {}
/** @return {?} */
xyz.swapee.wc.ITestingController.prototype.increaseAmount = function() {}
/** @return {?} */
xyz.swapee.wc.ITestingController.prototype.decreaseAmount = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.ITestingController.prototype.setCore = function(val) {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.unsetCore = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.ITestingController.prototype.setAmountIn = function(val) {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.unsetAmountIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.ITestingController.prototype.setCurrencyIn = function(val) {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.unsetCurrencyIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.ITestingController.prototype.setCurrencyOut = function(val) {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.unsetCurrencyOut = function() {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.ITestingController.prototype.loadChangenowOffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.ITestingProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingProcessorCaster}
 * @extends {xyz.swapee.wc.ITestingComputer}
 * @extends {xyz.swapee.wc.ITestingCore}
 * @extends {xyz.swapee.wc.ITestingController}
 */
xyz.swapee.wc.ITestingProcessor = function() {}
/** @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init */
xyz.swapee.wc.ITestingProcessor.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.TestingProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init
 * @implements {xyz.swapee.wc.ITestingProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingProcessor.Initialese>}
 */
xyz.swapee.wc.TestingProcessor = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init */
xyz.swapee.wc.TestingProcessor.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.TestingProcessor.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.AbstractTestingProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init
 * @extends {xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingProcessor.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.TestingProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/** @typedef {function(new: xyz.swapee.wc.ITestingProcessor, ...!xyz.swapee.wc.ITestingProcessor.Initialese)} */
xyz.swapee.wc.TestingProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.RecordITestingProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.RecordITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITestingController.resetPort, increaseAmount: xyz.swapee.wc.ITestingController.increaseAmount, decreaseAmount: xyz.swapee.wc.ITestingController.decreaseAmount, setCore: xyz.swapee.wc.ITestingController.setCore, unsetCore: xyz.swapee.wc.ITestingController.unsetCore, setAmountIn: xyz.swapee.wc.ITestingController.setAmountIn, unsetAmountIn: xyz.swapee.wc.ITestingController.unsetAmountIn, setCurrencyIn: xyz.swapee.wc.ITestingController.setCurrencyIn, unsetCurrencyIn: xyz.swapee.wc.ITestingController.unsetCurrencyIn, setCurrencyOut: xyz.swapee.wc.ITestingController.setCurrencyOut, unsetCurrencyOut: xyz.swapee.wc.ITestingController.unsetCurrencyOut, loadChangellyFloatingOffer: xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.ITestingController.loadChangellyFixedOffer, loadChangenowOffer: xyz.swapee.wc.ITestingController.loadChangenowOffer }} */
xyz.swapee.wc.RecordITestingController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.BoundITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingControllerFields}
 * @extends {xyz.swapee.wc.RecordITestingController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.ITestingOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.TestingMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.ITestingController.Inputs>}
 */
xyz.swapee.wc.BoundITestingController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.BoundITestingProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingProcessorCaster}
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {xyz.swapee.wc.BoundITestingCore}
 * @extends {xyz.swapee.wc.BoundITestingController}
 */
xyz.swapee.wc.BoundITestingProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml} xyz.swapee.wc.BoundTestingProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 90553037e58411bed2cb20baf14ddb8d */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/100-TestingMemory.xml} xyz.swapee.wc.TestingMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94ffaa9b2f697bec16dae4e498534806 */
/** @record */
xyz.swapee.wc.TestingMemory = function() {}
/** @type {string} */
xyz.swapee.wc.TestingMemory.prototype.core
/** @type {string} */
xyz.swapee.wc.TestingMemory.prototype.host
/** @type {number} */
xyz.swapee.wc.TestingMemory.prototype.amountIn
/** @type {string} */
xyz.swapee.wc.TestingMemory.prototype.currencyIn
/** @type {string} */
xyz.swapee.wc.TestingMemory.prototype.currencyOut
/** @type {boolean|undefined} */
xyz.swapee.wc.TestingMemory.prototype.changeNow
/** @type {number} */
xyz.swapee.wc.TestingMemory.prototype.changellyFixedOffer
/** @type {number} */
xyz.swapee.wc.TestingMemory.prototype.changellyFloatingOffer
/** @type {{ amountOut: string, type: string }} */
xyz.swapee.wc.TestingMemory.prototype.changenowOffer
/** @type {boolean} */
xyz.swapee.wc.TestingMemory.prototype.anyLoading
/** @type {boolean} */
xyz.swapee.wc.TestingMemory.prototype.loadingChangellyFloatingOffer
/** @type {?boolean} */
xyz.swapee.wc.TestingMemory.prototype.hasMoreChangellyFloatingOffer
/** @type {?Error} */
xyz.swapee.wc.TestingMemory.prototype.loadChangellyFloatingOfferError
/** @type {boolean} */
xyz.swapee.wc.TestingMemory.prototype.loadingChangellyFixedOffer
/** @type {?boolean} */
xyz.swapee.wc.TestingMemory.prototype.hasMoreChangellyFixedOffer
/** @type {?Error} */
xyz.swapee.wc.TestingMemory.prototype.loadChangellyFixedOfferError
/** @type {boolean} */
xyz.swapee.wc.TestingMemory.prototype.loadingChangenowOffer
/** @type {?boolean} */
xyz.swapee.wc.TestingMemory.prototype.hasMoreChangenowOffer
/** @type {?Error} */
xyz.swapee.wc.TestingMemory.prototype.loadChangenowOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/102-TestingInputs.xml} xyz.swapee.wc.front.TestingInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9fbb89c3bc77932cdb4b0ecab631b8a6 */
/** @record */
xyz.swapee.wc.front.TestingInputs = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.front.TestingInputs.prototype.core
/** @type {string|undefined} */
xyz.swapee.wc.front.TestingInputs.prototype.host
/** @type {number|undefined} */
xyz.swapee.wc.front.TestingInputs.prototype.amountIn
/** @type {string|undefined} */
xyz.swapee.wc.front.TestingInputs.prototype.currencyIn
/** @type {string|undefined} */
xyz.swapee.wc.front.TestingInputs.prototype.currencyOut
/** @type {boolean|undefined} */
xyz.swapee.wc.front.TestingInputs.prototype.changeNow

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.TestingEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @record */
xyz.swapee.wc.TestingEnv = function() {}
/** @type {xyz.swapee.wc.ITesting} */
xyz.swapee.wc.TestingEnv.prototype.testing

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {xyz.swapee.wc.ITestingProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITestingComputer.Initialese}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
xyz.swapee.wc.ITesting.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITestingFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @interface */
xyz.swapee.wc.ITestingFields
/** @type {!xyz.swapee.wc.ITesting.Pinout} */
xyz.swapee.wc.ITestingFields.prototype.pinout

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITestingCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @interface */
xyz.swapee.wc.ITestingCaster
/** @type {!xyz.swapee.wc.BoundITesting} */
xyz.swapee.wc.ITestingCaster.prototype.asITesting
/** @type {!xyz.swapee.wc.BoundTesting} */
xyz.swapee.wc.ITestingCaster.prototype.superTesting

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingCaster}
 * @extends {xyz.swapee.wc.ITestingProcessor}
 * @extends {xyz.swapee.wc.ITestingComputer}
 * @extends {xyz.swapee.wc.ITestingController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, null>}
 */
xyz.swapee.wc.ITesting = function() {}
/** @param {...!xyz.swapee.wc.ITesting.Initialese} init */
xyz.swapee.wc.ITesting.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.Testing exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init
 * @implements {xyz.swapee.wc.ITesting}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITesting.Initialese>}
 */
xyz.swapee.wc.Testing = function(...init) {}
/** @param {...!xyz.swapee.wc.ITesting.Initialese} init */
xyz.swapee.wc.Testing.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.Testing.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.AbstractTesting exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init
 * @extends {xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.Testing}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTesting.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTesting}
 */
xyz.swapee.wc.AbstractTesting.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.TestingConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @typedef {function(new: xyz.swapee.wc.ITesting, ...!xyz.swapee.wc.ITesting.Initialese)} */
xyz.swapee.wc.TestingConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @record */
xyz.swapee.wc.ITesting.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.ITesting.Pinout)|undefined} */
xyz.swapee.wc.ITesting.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.ITesting.Pinout)|undefined} */
xyz.swapee.wc.ITesting.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.ITesting.Pinout} */
xyz.swapee.wc.ITesting.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.TestingMemory)|undefined} */
xyz.swapee.wc.ITesting.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.TestingClasses)|undefined} */
xyz.swapee.wc.ITesting.MVCOptions.prototype.classes

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.RecordITesting exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITesting

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.BoundITesting exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingFields}
 * @extends {xyz.swapee.wc.RecordITesting}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingCaster}
 * @extends {xyz.swapee.wc.BoundITestingProcessor}
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {xyz.swapee.wc.BoundITestingController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, null>}
 */
xyz.swapee.wc.BoundITesting = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.BoundTesting exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITesting}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTesting = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPort.Inputs}
 */
xyz.swapee.wc.ITestingController.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITesting.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingController.Inputs}
 */
xyz.swapee.wc.ITesting.Pinout = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.ITestingBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITestingController.Inputs>}
 */
xyz.swapee.wc.ITestingBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml} xyz.swapee.wc.TestingBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ece243f8167c8e6edeafbf2de7ea210f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingBuffer}
 */
xyz.swapee.wc.TestingBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingDisplay.Initialese}
 */
xyz.swapee.wc.ITestingGPU.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.ITestingHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingController.Initialese}
 * @extends {xyz.swapee.wc.back.ITestingScreen.Initialese}
 * @extends {xyz.swapee.wc.ITesting.Initialese}
 * @extends {xyz.swapee.wc.ITestingGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.ITestingProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITestingComputer.Initialese}
 * @extends {ITestingGenerator.Initialese}
 */
xyz.swapee.wc.ITestingHtmlComponent.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.ITestingHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/** @interface */
xyz.swapee.wc.ITestingHtmlComponentCaster
/** @type {!xyz.swapee.wc.BoundITestingHtmlComponent} */
xyz.swapee.wc.ITestingHtmlComponentCaster.prototype.asITestingHtmlComponent
/** @type {!xyz.swapee.wc.BoundTestingHtmlComponent} */
xyz.swapee.wc.ITestingHtmlComponentCaster.prototype.superTestingHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.ITestingGPUFields
/** @type {!Object<string, string>} */
xyz.swapee.wc.ITestingGPUFields.prototype.vdusPQs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.ITestingGPUCaster
/** @type {!xyz.swapee.wc.BoundITestingGPU} */
xyz.swapee.wc.ITestingGPUCaster.prototype.asITestingGPU
/** @type {!xyz.swapee.wc.BoundTestingGPU} */
xyz.swapee.wc.ITestingGPUCaster.prototype.superTestingGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.ITestingGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!TestingMemory,>}
 * @extends {xyz.swapee.wc.back.ITestingDisplay}
 */
xyz.swapee.wc.ITestingGPU = function() {}
/** @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init */
xyz.swapee.wc.ITestingGPU.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.ITestingHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.ITestingController}
 * @extends {xyz.swapee.wc.back.ITestingScreen}
 * @extends {xyz.swapee.wc.ITesting}
 * @extends {xyz.swapee.wc.ITestingGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.ITestingProcessor}
 * @extends {xyz.swapee.wc.ITestingComputer}
 * @extends {ITestingGenerator}
 */
xyz.swapee.wc.ITestingHtmlComponent = function() {}
/** @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init */
xyz.swapee.wc.ITestingHtmlComponent.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.TestingHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.ITestingHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingHtmlComponent.Initialese>}
 */
xyz.swapee.wc.TestingHtmlComponent = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init */
xyz.swapee.wc.TestingHtmlComponent.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.TestingHtmlComponent.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.AbstractTestingHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.TestingHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/** @typedef {function(new: xyz.swapee.wc.ITestingHtmlComponent, ...!xyz.swapee.wc.ITestingHtmlComponent.Initialese)} */
xyz.swapee.wc.TestingHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.RecordITestingHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.RecordITestingGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.BoundITestingGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingGPUFields}
 * @extends {xyz.swapee.wc.RecordITestingGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!TestingMemory,>}
 * @extends {xyz.swapee.wc.back.BoundITestingDisplay}
 */
xyz.swapee.wc.BoundITestingGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.BoundITestingHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundITestingController}
 * @extends {xyz.swapee.wc.back.BoundITestingScreen}
 * @extends {xyz.swapee.wc.BoundITesting}
 * @extends {xyz.swapee.wc.BoundITestingGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundITestingProcessor}
 * @extends {xyz.swapee.wc.BoundITestingComputer}
 * @extends {BoundITestingGenerator}
 */
xyz.swapee.wc.BoundITestingHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml} xyz.swapee.wc.BoundTestingHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f51bae791f21de4e567d55afcd7478fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/** @record */
xyz.swapee.wc.ITestingRadio.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadioCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/** @interface */
xyz.swapee.wc.ITestingRadioCaster
/** @type {!xyz.swapee.wc.BoundITestingRadio} */
xyz.swapee.wc.ITestingRadioCaster.prototype.asITestingRadio
/** @type {!xyz.swapee.wc.BoundITestingComputer} */
xyz.swapee.wc.ITestingRadioCaster.prototype.asITestingComputer
/** @type {!xyz.swapee.wc.BoundTestingRadio} */
xyz.swapee.wc.ITestingRadioCaster.prototype.superTestingRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingRadioCaster}
 */
xyz.swapee.wc.ITestingRadio = function() {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangellyFloatingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangellyFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)}
 */
xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangenowOffer = function(form, changes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.TestingRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingRadio}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingRadio.Initialese>}
 */
xyz.swapee.wc.TestingRadio = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.TestingRadio.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.AbstractTestingRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio = function() {}
/**
 * @param {...(!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio)} Implementations
 * @return {typeof xyz.swapee.wc.TestingRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingRadio.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio)} Implementations
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio)} Implementations
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.RecordITestingRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/** @typedef {{ adaptLoadChangellyFloatingOffer: xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer, adaptLoadChangellyFixedOffer: xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer, adaptLoadChangenowOffer: xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer }} */
xyz.swapee.wc.RecordITestingRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.BoundITestingRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingRadio}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingRadioCaster}
 */
xyz.swapee.wc.BoundITestingRadio = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.BoundTestingRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingRadio}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingRadio = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFloatingOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)}
 */
$$xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form, ITestingComputer.adaptLoadChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingRadio, !xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form, ITestingComputer.adaptLoadChangellyFloatingOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)} */
xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingRadio,$$xyz.swapee.wc.ITestingRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangellyFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form, ITestingComputer.adaptLoadChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingRadio, !xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form, ITestingComputer.adaptLoadChangellyFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)} */
xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingRadio,$$xyz.swapee.wc.ITestingRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} form
 * @param {ITestingComputer.adaptLoadChangenowOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)}
 */
$$xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form, ITestingComputer.adaptLoadChangenowOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)} */
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingRadio, !xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form, ITestingComputer.adaptLoadChangenowOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)} */
xyz.swapee.wc.ITestingRadio._adaptLoadChangenowOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer} */
xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer

// nss:xyz.swapee.wc.ITestingRadio,$$xyz.swapee.wc.ITestingRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6784c6cd178aa37d1320837bc5623b05 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @interface */
xyz.swapee.wc.ITestingDesigner = function() {}
/**
 * @param {xyz.swapee.wc.TestingClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ITestingDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.TestingClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ITestingDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.ITestingDesigner.communicator.Mesh} mesh
 * @return {?}
 */
xyz.swapee.wc.ITestingDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.ITestingDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.ITestingDesigner.relay.MemPool} memPool
 * @return {?}
 */
xyz.swapee.wc.ITestingDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.TestingClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ITestingDesigner.prototype.lendClasses = function(classes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.TestingDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingDesigner}
 */
xyz.swapee.wc.TestingDesigner = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @record */
xyz.swapee.wc.ITestingDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.ITestingController} */
xyz.swapee.wc.ITestingDesigner.communicator.Mesh.prototype.Testing

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @record */
xyz.swapee.wc.ITestingDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.ITestingController} */
xyz.swapee.wc.ITestingDesigner.relay.Mesh.prototype.Testing
/** @type {typeof xyz.swapee.wc.ITestingController} */
xyz.swapee.wc.ITestingDesigner.relay.Mesh.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml} xyz.swapee.wc.ITestingDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e42bfd9db932c8a96df7ccf11d571aa1 */
/** @record */
xyz.swapee.wc.ITestingDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.TestingMemory} */
xyz.swapee.wc.ITestingDesigner.relay.MemPool.prototype.Testing
/** @type {!xyz.swapee.wc.TestingMemory} */
xyz.swapee.wc.ITestingDesigner.relay.MemPool.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 */
xyz.swapee.wc.ITestingService.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingServiceCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/** @interface */
xyz.swapee.wc.ITestingServiceCaster
/** @type {!xyz.swapee.wc.BoundITestingService} */
xyz.swapee.wc.ITestingServiceCaster.prototype.asITestingService
/** @type {!xyz.swapee.wc.BoundTestingService} */
xyz.swapee.wc.ITestingServiceCaster.prototype.superTestingService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingServiceCaster}
 * @extends {com.changelly.UChangelly}
 */
xyz.swapee.wc.ITestingService = function() {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>}
 */
xyz.swapee.wc.ITestingService.prototype.filterChangellyFloatingOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>}
 */
xyz.swapee.wc.ITestingService.prototype.filterChangellyFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>}
 */
xyz.swapee.wc.ITestingService.prototype.filterChangenowOffer = function(form) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.TestingService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITestingService}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingService.Initialese>}
 */
xyz.swapee.wc.TestingService = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.TestingService.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.AbstractTestingService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService = function() {}
/**
 * @param {...((!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.wc.TestingService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingService.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingService}
 */
xyz.swapee.wc.AbstractTestingService.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.RecordITestingService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/** @typedef {{ filterChangellyFloatingOffer: xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer, filterChangellyFixedOffer: xyz.swapee.wc.ITestingService.filterChangellyFixedOffer, filterChangenowOffer: xyz.swapee.wc.ITestingService.filterChangenowOffer }} */
xyz.swapee.wc.RecordITestingService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.BoundITestingService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingService}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingServiceCaster}
 * @extends {com.changelly.BoundUChangelly}
 */
xyz.swapee.wc.BoundITestingService = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.BoundTestingService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingService}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingService = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>}
 */
$$xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form): !Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>} */
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingService, !xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form): !Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>} */
xyz.swapee.wc.ITestingService._filterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer} */
xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingService,$$xyz.swapee.wc.ITestingService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>}
 */
$$xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form): !Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>} */
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingService, !xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form): !Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>} */
xyz.swapee.wc.ITestingService._filterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer} */
xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingService,$$xyz.swapee.wc.ITestingService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>}
 */
$$xyz.swapee.wc.ITestingService.__filterChangenowOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form): !Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>} */
xyz.swapee.wc.ITestingService.filterChangenowOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingService, !xyz.swapee.wc.ITestingService.filterChangenowOffer.Form): !Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>} */
xyz.swapee.wc.ITestingService._filterChangenowOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingService.__filterChangenowOffer} */
xyz.swapee.wc.ITestingService.__filterChangenowOffer

// nss:xyz.swapee.wc.ITestingService,$$xyz.swapee.wc.ITestingService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer}
 */
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer}
 */
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangenowOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe}
 * @extends {xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingService.filterChangenowOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml} xyz.swapee.wc.ITestingService.filterChangenowOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99d624fec12d9421dd2770cdb1d81b8f */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer}
 */
xyz.swapee.wc.ITestingService.filterChangenowOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings>}
 */
xyz.swapee.wc.ITestingDisplay.Initialese = function() {}
/** @type {HTMLInputElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.AmountIn
/** @type {HTMLButtonElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.IncreaseBu
/** @type {HTMLButtonElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.DecreaseBu
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.RatesLoIn
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.RatesLoEr
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.ITestingDisplay.Initialese.prototype.ChangellyFixedOfferAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @interface */
xyz.swapee.wc.ITestingDisplayFields
/** @type {!xyz.swapee.wc.ITestingDisplay.Settings} */
xyz.swapee.wc.ITestingDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.ITestingDisplay.Queries} */
xyz.swapee.wc.ITestingDisplayFields.prototype.queries
/** @type {HTMLInputElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.AmountIn
/** @type {HTMLButtonElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.IncreaseBu
/** @type {HTMLButtonElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.DecreaseBu
/** @type {HTMLSpanElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.RatesLoIn
/** @type {HTMLSpanElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.RatesLoEr
/** @type {HTMLDivElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFloatingOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {HTMLDivElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFixedOffer
/** @type {HTMLSpanElement} */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFixedOfferAmount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @interface */
xyz.swapee.wc.ITestingDisplayCaster
/** @type {!xyz.swapee.wc.BoundITestingDisplay} */
xyz.swapee.wc.ITestingDisplayCaster.prototype.asITestingDisplay
/** @type {!xyz.swapee.wc.BoundITestingScreen} */
xyz.swapee.wc.ITestingDisplayCaster.prototype.asITestingScreen
/** @type {!xyz.swapee.wc.BoundTestingDisplay} */
xyz.swapee.wc.ITestingDisplayCaster.prototype.superTestingDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITestingDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.TestingMemory, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, xyz.swapee.wc.ITestingDisplay.Queries, null>}
 */
xyz.swapee.wc.ITestingDisplay = function() {}
/** @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init */
xyz.swapee.wc.ITestingDisplay.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {null} land
 * @return {void}
 */
xyz.swapee.wc.ITestingDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.TestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init
 * @implements {xyz.swapee.wc.ITestingDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingDisplay.Initialese>}
 */
xyz.swapee.wc.TestingDisplay = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init */
xyz.swapee.wc.TestingDisplay.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.TestingDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.AbstractTestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init
 * @extends {xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.TestingDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @typedef {function(new: xyz.swapee.wc.ITestingDisplay, ...!xyz.swapee.wc.ITestingDisplay.Initialese)} */
xyz.swapee.wc.TestingDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.TestingGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init
 * @implements {xyz.swapee.wc.ITestingGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingGPU.Initialese>}
 */
xyz.swapee.wc.TestingGPU = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init */
xyz.swapee.wc.TestingGPU.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.TestingGPU.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.AbstractTestingGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init
 * @extends {xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingGPU.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.TestingGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.ITestingGPU, ...!xyz.swapee.wc.ITestingGPU.Initialese)} */
xyz.swapee.wc.TestingGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml} xyz.swapee.wc.BoundTestingGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.RecordITestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @typedef {{ paint: xyz.swapee.wc.ITestingDisplay.paint }} */
xyz.swapee.wc.RecordITestingDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.BoundITestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingDisplayFields}
 * @extends {xyz.swapee.wc.RecordITestingDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.TestingMemory, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, xyz.swapee.wc.ITestingDisplay.Queries, null>}
 */
xyz.swapee.wc.BoundITestingDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.BoundTestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} memory
 * @param {null} land
 * @return {void}
 */
$$xyz.swapee.wc.ITestingDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory, null): void} */
xyz.swapee.wc.ITestingDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.ITestingDisplay, !xyz.swapee.wc.TestingMemory, null): void} */
xyz.swapee.wc.ITestingDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.ITestingDisplay.__paint} */
xyz.swapee.wc.ITestingDisplay.__paint

// nss:xyz.swapee.wc.ITestingDisplay,$$xyz.swapee.wc.ITestingDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/** @record */
xyz.swapee.wc.ITestingDisplay.Queries = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml} xyz.swapee.wc.ITestingDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingDisplay.Queries}
 */
xyz.swapee.wc.ITestingDisplay.Settings = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TestingClasses>}
 */
xyz.swapee.wc.back.ITestingDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.AmountIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.IncreaseBu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.DecreaseBu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.RatesLoIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.RatesLoEr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFloatingOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFloatingOfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFixedOffer
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ITestingDisplay.Initialese.prototype.ChangellyFixedOfferAmount

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/** @interface */
xyz.swapee.wc.back.ITestingDisplayFields
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.AmountIn
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.IncreaseBu
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.DecreaseBu
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.RatesLoIn
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.RatesLoEr
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFloatingOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFloatingOfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFixedOffer
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFixedOfferAmount

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/** @interface */
xyz.swapee.wc.back.ITestingDisplayCaster
/** @type {!xyz.swapee.wc.back.BoundITestingDisplay} */
xyz.swapee.wc.back.ITestingDisplayCaster.prototype.asITestingDisplay
/** @type {!xyz.swapee.wc.back.BoundTestingDisplay} */
xyz.swapee.wc.back.ITestingDisplayCaster.prototype.superTestingDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.ITestingDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.TestingClasses, null>}
 */
xyz.swapee.wc.back.ITestingDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.TestingMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
xyz.swapee.wc.back.ITestingDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.TestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.ITestingDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingDisplay.Initialese>}
 */
xyz.swapee.wc.back.TestingDisplay = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.TestingDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.AbstractTestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay = function() {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.TestingVdusPQs = function() {}
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.RatesLoIn
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFloatingOffer
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFloatingOfferAmount
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFixedOffer
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.ChangellyFixedOfferAmount
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.AmountIn
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.IncreaseBu
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.DecreaseBu
/** @type {string} */
xyz.swapee.wc.TestingVdusPQs.prototype.RatesLoEr

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml} xyz.swapee.wc.TestingVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.TestingVdusQPs = function() {}
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee3
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee7
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee8
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee12
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee13
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee9
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee10
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee11
/** @type {string} */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee4

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.RecordITestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/** @typedef {{ paint: xyz.swapee.wc.back.ITestingDisplay.paint }} */
xyz.swapee.wc.back.RecordITestingDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.BoundITestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordITestingDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.TestingClasses, null>}
 */
xyz.swapee.wc.back.BoundITestingDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.BoundTestingDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundTestingDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml} xyz.swapee.wc.back.ITestingDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73086c107a0da1832c11e3ad27aaf1de */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TestingMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$$xyz.swapee.wc.back.ITestingDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TestingMemory=, null=): void} */
xyz.swapee.wc.back.ITestingDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.ITestingDisplay, !xyz.swapee.wc.TestingMemory=, null=): void} */
xyz.swapee.wc.back.ITestingDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.back.ITestingDisplay.__paint} */
xyz.swapee.wc.back.ITestingDisplay.__paint

// nss:xyz.swapee.wc.back.ITestingDisplay,$$xyz.swapee.wc.back.ITestingDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/41-TestingClasses.xml} xyz.swapee.wc.TestingClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7a824f2729d6d33681c7fe7eef4cded0 */
/** @record */
xyz.swapee.wc.TestingClasses = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.TestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init
 * @implements {xyz.swapee.wc.ITestingController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingController.Initialese>}
 */
xyz.swapee.wc.TestingController = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingController.Initialese} init */
xyz.swapee.wc.TestingController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.TestingController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.AbstractTestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init
 * @extends {xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingController}
 */
xyz.swapee.wc.AbstractTestingController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.TestingControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @typedef {function(new: xyz.swapee.wc.ITestingController, ...!xyz.swapee.wc.ITestingController.Initialese)} */
xyz.swapee.wc.TestingControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.BoundTestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._resetPort
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__resetPort} */
xyz.swapee.wc.ITestingController.__resetPort

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.increaseAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.ITestingController.__increaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ITestingController.increaseAmount
/** @typedef {function(this: xyz.swapee.wc.ITestingController)} */
xyz.swapee.wc.ITestingController._increaseAmount
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__increaseAmount} */
xyz.swapee.wc.ITestingController.__increaseAmount

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.decreaseAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.ITestingController.__decreaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ITestingController.decreaseAmount
/** @typedef {function(this: xyz.swapee.wc.ITestingController)} */
xyz.swapee.wc.ITestingController._decreaseAmount
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__decreaseAmount} */
xyz.swapee.wc.ITestingController.__decreaseAmount

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__setCore = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ITestingController.setCore
/** @typedef {function(this: xyz.swapee.wc.ITestingController, string): void} */
xyz.swapee.wc.ITestingController._setCore
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__setCore} */
xyz.swapee.wc.ITestingController.__setCore

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__unsetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetCore
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetCore
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__unsetCore} */
xyz.swapee.wc.ITestingController.__unsetCore

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setAmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__setAmountIn = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.ITestingController.setAmountIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController, number): void} */
xyz.swapee.wc.ITestingController._setAmountIn
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__setAmountIn} */
xyz.swapee.wc.ITestingController.__setAmountIn

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetAmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__unsetAmountIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetAmountIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetAmountIn
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__unsetAmountIn} */
xyz.swapee.wc.ITestingController.__unsetAmountIn

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setCurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__setCurrencyIn = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ITestingController.setCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController, string): void} */
xyz.swapee.wc.ITestingController._setCurrencyIn
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__setCurrencyIn} */
xyz.swapee.wc.ITestingController.__setCurrencyIn

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetCurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__unsetCurrencyIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetCurrencyIn
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__unsetCurrencyIn} */
xyz.swapee.wc.ITestingController.__unsetCurrencyIn

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.setCurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__setCurrencyOut = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ITestingController.setCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.ITestingController, string): void} */
xyz.swapee.wc.ITestingController._setCurrencyOut
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__setCurrencyOut} */
xyz.swapee.wc.ITestingController.__setCurrencyOut

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.unsetCurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__unsetCurrencyOut = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.unsetCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._unsetCurrencyOut
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__unsetCurrencyOut} */
xyz.swapee.wc.ITestingController.__unsetCurrencyOut

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._loadChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.loadChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._loadChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer} */
xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.loadChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ITestingController.__loadChangenowOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITestingController.loadChangenowOffer
/** @typedef {function(this: xyz.swapee.wc.ITestingController): void} */
xyz.swapee.wc.ITestingController._loadChangenowOffer
/** @typedef {typeof $$xyz.swapee.wc.ITestingController.__loadChangenowOffer} */
xyz.swapee.wc.ITestingController.__loadChangenowOffer

// nss:xyz.swapee.wc.ITestingController,$$xyz.swapee.wc.ITestingController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml} xyz.swapee.wc.ITestingController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 30f453fc9e3f19e7cb4f387d5d2f4973 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingPort.WeakInputs}
 */
xyz.swapee.wc.ITestingController.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @record */
xyz.swapee.wc.front.ITestingController.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @interface */
xyz.swapee.wc.front.ITestingControllerCaster
/** @type {!xyz.swapee.wc.front.BoundITestingController} */
xyz.swapee.wc.front.ITestingControllerCaster.prototype.asITestingController
/** @type {!xyz.swapee.wc.front.BoundTestingController} */
xyz.swapee.wc.front.ITestingControllerCaster.prototype.superTestingController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.ITestingControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/** @interface */
xyz.swapee.wc.front.ITestingControllerATCaster
/** @type {!xyz.swapee.wc.front.BoundITestingControllerAT} */
xyz.swapee.wc.front.ITestingControllerATCaster.prototype.asITestingControllerAT
/** @type {!xyz.swapee.wc.front.BoundTestingControllerAT} */
xyz.swapee.wc.front.ITestingControllerATCaster.prototype.superTestingControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.ITestingControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.front.ITestingControllerAT = function() {}
/** @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init */
xyz.swapee.wc.front.ITestingControllerAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.front.ITestingControllerAT}
 */
xyz.swapee.wc.front.ITestingController = function() {}
/** @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init */
xyz.swapee.wc.front.ITestingController.prototype.constructor = function(...init) {}
/** @return {?} */
xyz.swapee.wc.front.ITestingController.prototype.increaseAmount = function() {}
/** @return {?} */
xyz.swapee.wc.front.ITestingController.prototype.decreaseAmount = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.prototype.setCore = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.ITestingController.prototype.unsetCore = function() {}
/**
 * @param {number} val
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.prototype.setAmountIn = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.ITestingController.prototype.unsetAmountIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.prototype.setCurrencyIn = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.ITestingController.prototype.unsetCurrencyIn = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.prototype.setCurrencyOut = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.ITestingController.prototype.unsetCurrencyOut = function() {}
/** @return {void} */
xyz.swapee.wc.front.ITestingController.prototype.loadChangellyFloatingOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.ITestingController.prototype.loadChangellyFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.ITestingController.prototype.loadChangenowOffer = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.TestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init
 * @implements {xyz.swapee.wc.front.ITestingController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingController.Initialese>}
 */
xyz.swapee.wc.front.TestingController = function(...init) {}
/** @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init */
xyz.swapee.wc.front.TestingController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.TestingController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.AbstractTestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init
 * @extends {xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.TestingControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @typedef {function(new: xyz.swapee.wc.front.ITestingController, ...!xyz.swapee.wc.front.ITestingController.Initialese)} */
xyz.swapee.wc.front.TestingControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.RecordITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/** @typedef {{ increaseAmount: xyz.swapee.wc.front.ITestingController.increaseAmount, decreaseAmount: xyz.swapee.wc.front.ITestingController.decreaseAmount, setCore: xyz.swapee.wc.front.ITestingController.setCore, unsetCore: xyz.swapee.wc.front.ITestingController.unsetCore, setAmountIn: xyz.swapee.wc.front.ITestingController.setAmountIn, unsetAmountIn: xyz.swapee.wc.front.ITestingController.unsetAmountIn, setCurrencyIn: xyz.swapee.wc.front.ITestingController.setCurrencyIn, unsetCurrencyIn: xyz.swapee.wc.front.ITestingController.unsetCurrencyIn, setCurrencyOut: xyz.swapee.wc.front.ITestingController.setCurrencyOut, unsetCurrencyOut: xyz.swapee.wc.front.ITestingController.unsetCurrencyOut, loadChangellyFloatingOffer: xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer, loadChangellyFixedOffer: xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer, loadChangenowOffer: xyz.swapee.wc.front.ITestingController.loadChangenowOffer }} */
xyz.swapee.wc.front.RecordITestingController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.RecordITestingControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITestingControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.BoundITestingControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITestingControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.front.BoundITestingControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.BoundITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITestingController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundITestingControllerAT}
 */
xyz.swapee.wc.front.BoundITestingController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.BoundTestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITestingController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundTestingController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.increaseAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.front.ITestingController.__increaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ITestingController.increaseAmount
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController)} */
xyz.swapee.wc.front.ITestingController._increaseAmount
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__increaseAmount} */
xyz.swapee.wc.front.ITestingController.__increaseAmount

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.decreaseAmount exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.front.ITestingController.__decreaseAmount = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ITestingController.decreaseAmount
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController)} */
xyz.swapee.wc.front.ITestingController._decreaseAmount
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__decreaseAmount} */
xyz.swapee.wc.front.ITestingController.__decreaseAmount

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__setCore = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ITestingController.setCore
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, string): void} */
xyz.swapee.wc.front.ITestingController._setCore
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__setCore} */
xyz.swapee.wc.front.ITestingController.__setCore

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__unsetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetCore
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetCore
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__unsetCore} */
xyz.swapee.wc.front.ITestingController.__unsetCore

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setAmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} val
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__setAmountIn = function(val) {}
/** @typedef {function(number): void} */
xyz.swapee.wc.front.ITestingController.setAmountIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, number): void} */
xyz.swapee.wc.front.ITestingController._setAmountIn
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__setAmountIn} */
xyz.swapee.wc.front.ITestingController.__setAmountIn

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetAmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__unsetAmountIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetAmountIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetAmountIn
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__unsetAmountIn} */
xyz.swapee.wc.front.ITestingController.__unsetAmountIn

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setCurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__setCurrencyIn = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ITestingController.setCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, string): void} */
xyz.swapee.wc.front.ITestingController._setCurrencyIn
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__setCurrencyIn} */
xyz.swapee.wc.front.ITestingController.__setCurrencyIn

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetCurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetCurrencyIn
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetCurrencyIn
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn} */
xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.setCurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__setCurrencyOut = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ITestingController.setCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController, string): void} */
xyz.swapee.wc.front.ITestingController._setCurrencyOut
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__setCurrencyOut} */
xyz.swapee.wc.front.ITestingController.__setCurrencyOut

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.unsetCurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.unsetCurrencyOut
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._unsetCurrencyOut
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut} */
xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._loadChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer} */
xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._loadChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer} */
xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml} xyz.swapee.wc.front.ITestingController.loadChangenowOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 21e94a389e2974b8d3849a9b18275303 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ITestingController.__loadChangenowOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ITestingController.loadChangenowOffer
/** @typedef {function(this: xyz.swapee.wc.front.ITestingController): void} */
xyz.swapee.wc.front.ITestingController._loadChangenowOffer
/** @typedef {typeof $$xyz.swapee.wc.front.ITestingController.__loadChangenowOffer} */
xyz.swapee.wc.front.ITestingController.__loadChangenowOffer

// nss:xyz.swapee.wc.front.ITestingController,$$xyz.swapee.wc.front.ITestingController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.ITestingController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITestingController.Inputs>}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
xyz.swapee.wc.back.ITestingController.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.ITestingControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/** @interface */
xyz.swapee.wc.back.ITestingControllerCaster
/** @type {!xyz.swapee.wc.back.BoundITestingController} */
xyz.swapee.wc.back.ITestingControllerCaster.prototype.asITestingController
/** @type {!xyz.swapee.wc.back.BoundTestingController} */
xyz.swapee.wc.back.ITestingControllerCaster.prototype.superTestingController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.ITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.ITestingController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.ITestingController.Inputs>}
 */
xyz.swapee.wc.back.ITestingController = function() {}
/** @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init */
xyz.swapee.wc.back.ITestingController.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.TestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingController.Initialese>}
 */
xyz.swapee.wc.back.TestingController = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init */
xyz.swapee.wc.back.TestingController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.TestingController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.AbstractTestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.TestingControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingController, ...!xyz.swapee.wc.back.ITestingController.Initialese)} */
xyz.swapee.wc.back.TestingControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.RecordITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.BoundITestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerCaster}
 * @extends {xyz.swapee.wc.BoundITestingController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.ITestingController.Inputs>}
 */
xyz.swapee.wc.back.BoundITestingController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml} xyz.swapee.wc.back.BoundTestingController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad66f4c22c7e206ea30befc91211aa70 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundTestingController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.ITestingControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITestingController.Initialese}
 */
xyz.swapee.wc.back.ITestingControllerAR.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.ITestingControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/** @interface */
xyz.swapee.wc.back.ITestingControllerARCaster
/** @type {!xyz.swapee.wc.back.BoundITestingControllerAR} */
xyz.swapee.wc.back.ITestingControllerARCaster.prototype.asITestingControllerAR
/** @type {!xyz.swapee.wc.back.BoundTestingControllerAR} */
xyz.swapee.wc.back.ITestingControllerARCaster.prototype.superTestingControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.ITestingControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITestingController}
 */
xyz.swapee.wc.back.ITestingControllerAR = function() {}
/** @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init */
xyz.swapee.wc.back.ITestingControllerAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.TestingControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingControllerAR.Initialese>}
 */
xyz.swapee.wc.back.TestingControllerAR = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init */
xyz.swapee.wc.back.TestingControllerAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.TestingControllerAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.AbstractTestingControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.TestingControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingControllerAR, ...!xyz.swapee.wc.back.ITestingControllerAR.Initialese)} */
xyz.swapee.wc.back.TestingControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.RecordITestingControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.BoundITestingControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITestingController}
 */
xyz.swapee.wc.back.BoundITestingControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml} xyz.swapee.wc.back.BoundTestingControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c07367ba21e2046b21caf793ec8355d5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundTestingControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.ITestingControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.front.ITestingControllerAT.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.TestingControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.ITestingControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingControllerAT.Initialese>}
 */
xyz.swapee.wc.front.TestingControllerAT = function(...init) {}
/** @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init */
xyz.swapee.wc.front.TestingControllerAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.TestingControllerAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.AbstractTestingControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.TestingControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/** @typedef {function(new: xyz.swapee.wc.front.ITestingControllerAT, ...!xyz.swapee.wc.front.ITestingControllerAT.Initialese)} */
xyz.swapee.wc.front.TestingControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml} xyz.swapee.wc.front.BoundTestingControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 27c4eacb43cc3ed7ba58b3ead27de161 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITestingControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundTestingControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.ITestingScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.ITestingDisplay.Initialese}
 */
xyz.swapee.wc.ITestingScreen.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.ITestingScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/** @interface */
xyz.swapee.wc.ITestingScreenCaster
/** @type {!xyz.swapee.wc.BoundITestingScreen} */
xyz.swapee.wc.ITestingScreenCaster.prototype.asITestingScreen
/** @type {!xyz.swapee.wc.BoundTestingScreen} */
xyz.swapee.wc.ITestingScreenCaster.prototype.superTestingScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.ITestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITestingScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.ITestingController}
 * @extends {xyz.swapee.wc.ITestingDisplay}
 */
xyz.swapee.wc.ITestingScreen = function() {}
/** @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init */
xyz.swapee.wc.ITestingScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.TestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init
 * @implements {xyz.swapee.wc.ITestingScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingScreen.Initialese>}
 */
xyz.swapee.wc.TestingScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init */
xyz.swapee.wc.TestingScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.TestingScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.AbstractTestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init
 * @extends {xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.TestingScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/** @typedef {function(new: xyz.swapee.wc.ITestingScreen, ...!xyz.swapee.wc.ITestingScreen.Initialese)} */
xyz.swapee.wc.TestingScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.RecordITestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITestingScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.BoundITestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITestingScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITestingScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundITestingController}
 * @extends {xyz.swapee.wc.BoundITestingDisplay}
 */
xyz.swapee.wc.BoundITestingScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml} xyz.swapee.wc.BoundTestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 63a4a20096abf567f1e3978b1011119e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITestingScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTestingScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.ITestingScreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.back.ITestingScreenAT.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.ITestingScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITestingScreenAT.Initialese}
 */
xyz.swapee.wc.back.ITestingScreen.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.ITestingScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/** @interface */
xyz.swapee.wc.back.ITestingScreenCaster
/** @type {!xyz.swapee.wc.back.BoundITestingScreen} */
xyz.swapee.wc.back.ITestingScreenCaster.prototype.asITestingScreen
/** @type {!xyz.swapee.wc.back.BoundTestingScreen} */
xyz.swapee.wc.back.ITestingScreenCaster.prototype.superTestingScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.ITestingScreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/** @interface */
xyz.swapee.wc.back.ITestingScreenATCaster
/** @type {!xyz.swapee.wc.back.BoundITestingScreenAT} */
xyz.swapee.wc.back.ITestingScreenATCaster.prototype.asITestingScreenAT
/** @type {!xyz.swapee.wc.back.BoundTestingScreenAT} */
xyz.swapee.wc.back.ITestingScreenATCaster.prototype.superTestingScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.ITestingScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.back.ITestingScreenAT = function() {}
/** @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init */
xyz.swapee.wc.back.ITestingScreenAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.ITestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenCaster}
 * @extends {xyz.swapee.wc.back.ITestingScreenAT}
 */
xyz.swapee.wc.back.ITestingScreen = function() {}
/** @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init */
xyz.swapee.wc.back.ITestingScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.TestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreen.Initialese>}
 */
xyz.swapee.wc.back.TestingScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init */
xyz.swapee.wc.back.TestingScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.TestingScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.AbstractTestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.TestingScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingScreen, ...!xyz.swapee.wc.back.ITestingScreen.Initialese)} */
xyz.swapee.wc.back.TestingScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.RecordITestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.RecordITestingScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITestingScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.BoundITestingScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.back.BoundITestingScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.BoundITestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITestingScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITestingScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundITestingScreenAT}
 */
xyz.swapee.wc.back.BoundITestingScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml} xyz.swapee.wc.back.BoundTestingScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad0d1e9f036cfe43b6812e95808fd9c8 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundTestingScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.ITestingScreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITestingScreen.Initialese}
 */
xyz.swapee.wc.front.ITestingScreenAR.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.ITestingScreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/** @interface */
xyz.swapee.wc.front.ITestingScreenARCaster
/** @type {!xyz.swapee.wc.front.BoundITestingScreenAR} */
xyz.swapee.wc.front.ITestingScreenARCaster.prototype.asITestingScreenAR
/** @type {!xyz.swapee.wc.front.BoundTestingScreenAR} */
xyz.swapee.wc.front.ITestingScreenARCaster.prototype.superTestingScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.ITestingScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITestingScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITestingScreen}
 */
xyz.swapee.wc.front.ITestingScreenAR = function() {}
/** @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init */
xyz.swapee.wc.front.ITestingScreenAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.TestingScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.ITestingScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingScreenAR.Initialese>}
 */
xyz.swapee.wc.front.TestingScreenAR = function(...init) {}
/** @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init */
xyz.swapee.wc.front.TestingScreenAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.TestingScreenAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.AbstractTestingScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.TestingScreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/** @typedef {function(new: xyz.swapee.wc.front.ITestingScreenAR, ...!xyz.swapee.wc.front.ITestingScreenAR.Initialese)} */
xyz.swapee.wc.front.TestingScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.RecordITestingScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITestingScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.BoundITestingScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITestingScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITestingScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITestingScreen}
 */
xyz.swapee.wc.front.BoundITestingScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml} xyz.swapee.wc.front.BoundTestingScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5ac8bd34d62efd74e59eb68510b5e450 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITestingScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundTestingScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.TestingScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.ITestingScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreenAT.Initialese>}
 */
xyz.swapee.wc.back.TestingScreenAT = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init */
xyz.swapee.wc.back.TestingScreenAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.TestingScreenAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.AbstractTestingScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.TestingScreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/** @typedef {function(new: xyz.swapee.wc.back.ITestingScreenAT, ...!xyz.swapee.wc.back.ITestingScreenAT.Initialese)} */
xyz.swapee.wc.back.TestingScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml} xyz.swapee.wc.back.BoundTestingScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b532d0e0809c65f97d3d5d500feea001 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITestingScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundTestingScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe.prototype.amountIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe.prototype.currencyIn

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe.prototype.currencyOut

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @record */
xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe.prototype.changeNow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core}
 */
xyz.swapee.wc.ITestingPort.Inputs.Core = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe}
 */
xyz.swapee.wc.ITestingPort.Inputs.Core_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host}
 */
xyz.swapee.wc.ITestingPort.Inputs.Host = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe}
 */
xyz.swapee.wc.ITestingPort.Inputs.Host_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.AmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn}
 */
xyz.swapee.wc.ITestingPort.Inputs.AmountIn = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.AmountIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe}
 */
xyz.swapee.wc.ITestingPort.Inputs.AmountIn_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn}
 */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe}
 */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut}
 */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.ChangeNow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow}
 */
xyz.swapee.wc.ITestingPort.Inputs.ChangeNow = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.Inputs.ChangeNow_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe}
 */
xyz.swapee.wc.ITestingPort.Inputs.ChangeNow_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.Core = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.Core_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.Host = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.Host_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe}
 */
xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe.prototype.changellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe.prototype.changellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe = function() {}
/** @type {{ amountOut: string, type: string }} */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe.prototype.changenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe.prototype.anyLoading

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe.prototype.hasMoreChangellyFloatingOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe.prototype.loadChangellyFloatingOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe.prototype.hasMoreChangellyFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe.prototype.loadChangellyFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe.prototype.hasMoreChangenowOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3c4ab2b7ecb21388e618cdbfd73517ab */
/** @record */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe.prototype.loadChangenowOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Core}
 */
xyz.swapee.wc.ITestingCore.Model.Core = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe}
 */
xyz.swapee.wc.ITestingCore.Model.Core_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Host}
 */
xyz.swapee.wc.ITestingCore.Model.Host = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe}
 */
xyz.swapee.wc.ITestingCore.Model.Host_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.AmountIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn}
 */
xyz.swapee.wc.ITestingCore.Model.AmountIn = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyIn exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn}
 */
xyz.swapee.wc.ITestingCore.Model.CurrencyIn = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.CurrencyOut exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut}
 */
xyz.swapee.wc.ITestingCore.Model.CurrencyOut = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml} xyz.swapee.wc.ITestingCore.Model.ChangeNow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d12b9a4a74014f8eaed9f185a8d9d35 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow}
 */
xyz.swapee.wc.ITestingCore.Model.ChangeNow = function() {}

// nss:xyz.swapee.wc
/* @typal-end */