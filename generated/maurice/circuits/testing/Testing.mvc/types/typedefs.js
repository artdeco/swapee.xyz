/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ITestingComputer={}
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer={}
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer={}
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer={}
xyz.swapee.wc.ITestingComputer.adaptAnyLoading={}
xyz.swapee.wc.ITestingOuterCore={}
xyz.swapee.wc.ITestingOuterCore.Model={}
xyz.swapee.wc.ITestingOuterCore.Model.Core={}
xyz.swapee.wc.ITestingOuterCore.Model.Host={}
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn={}
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn={}
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut={}
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow={}
xyz.swapee.wc.ITestingOuterCore.WeakModel={}
xyz.swapee.wc.ITestingPort={}
xyz.swapee.wc.ITestingPort.Inputs={}
xyz.swapee.wc.ITestingPort.WeakInputs={}
xyz.swapee.wc.ITestingCore={}
xyz.swapee.wc.ITestingCore.Model={}
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer={}
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer={}
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer={}
xyz.swapee.wc.ITestingCore.Model.AnyLoading={}
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer={}
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer={}
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError={}
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer={}
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer={}
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError={}
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer={}
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer={}
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError={}
xyz.swapee.wc.ITestingProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ITestingController={}
xyz.swapee.wc.front.ITestingControllerAT={}
xyz.swapee.wc.front.ITestingScreenAR={}
xyz.swapee.wc.ITesting={}
xyz.swapee.wc.ITestingHtmlComponent={}
xyz.swapee.wc.ITestingElement={}
xyz.swapee.wc.ITestingElementPort={}
xyz.swapee.wc.ITestingElementPort.Inputs={}
xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts={}
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts={}
xyz.swapee.wc.ITestingElementPort.WeakInputs={}
xyz.swapee.wc.ITestingRadio={}
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer={}
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer={}
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer={}
xyz.swapee.wc.ITestingDesigner={}
xyz.swapee.wc.ITestingDesigner.communicator={}
xyz.swapee.wc.ITestingDesigner.relay={}
xyz.swapee.wc.ITestingService={}
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer={}
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer={}
xyz.swapee.wc.ITestingService.filterChangenowOffer={}
xyz.swapee.wc.ITestingDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ITestingDisplay={}
xyz.swapee.wc.back.ITestingController={}
xyz.swapee.wc.back.ITestingControllerAR={}
xyz.swapee.wc.back.ITestingScreen={}
xyz.swapee.wc.back.ITestingScreenAT={}
xyz.swapee.wc.ITestingController={}
xyz.swapee.wc.ITestingScreen={}
xyz.swapee.wc.ITestingGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/02-ITestingComputer.xml}  e754d1027b8b1dbebf0af18ecfecef48 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ITestingComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingComputer)} xyz.swapee.wc.AbstractTestingComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingComputer} xyz.swapee.wc.TestingComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingComputer` interface.
 * @constructor xyz.swapee.wc.AbstractTestingComputer
 */
xyz.swapee.wc.AbstractTestingComputer = class extends /** @type {xyz.swapee.wc.AbstractTestingComputer.constructor&xyz.swapee.wc.TestingComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingComputer.prototype.constructor = xyz.swapee.wc.AbstractTestingComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingComputer.class = /** @type {typeof xyz.swapee.wc.AbstractTestingComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.AbstractTestingComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingComputer.Initialese[]) => xyz.swapee.wc.ITestingComputer} xyz.swapee.wc.TestingComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITestingComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.TestingMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.ITestingComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ITestingComputer
 */
xyz.swapee.wc.ITestingComputer = class extends /** @type {xyz.swapee.wc.ITestingComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer} */
xyz.swapee.wc.ITestingComputer.prototype.adaptChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer} */
xyz.swapee.wc.ITestingComputer.prototype.adaptChangellyFixedOffer = function() {}
/** @type {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer} */
xyz.swapee.wc.ITestingComputer.prototype.adaptChangenowOffer = function() {}
/** @type {xyz.swapee.wc.ITestingComputer.adaptAnyLoading} */
xyz.swapee.wc.ITestingComputer.prototype.adaptAnyLoading = function() {}
/** @type {xyz.swapee.wc.ITestingComputer.compute} */
xyz.swapee.wc.ITestingComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingComputer.Initialese>)} xyz.swapee.wc.TestingComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingComputer} xyz.swapee.wc.ITestingComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITestingComputer_ instances.
 * @constructor xyz.swapee.wc.TestingComputer
 * @implements {xyz.swapee.wc.ITestingComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingComputer.Initialese>} ‎
 */
xyz.swapee.wc.TestingComputer = class extends /** @type {xyz.swapee.wc.TestingComputer.constructor&xyz.swapee.wc.ITestingComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingComputer}
 */
xyz.swapee.wc.TestingComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITestingComputer} */
xyz.swapee.wc.RecordITestingComputer

/** @typedef {xyz.swapee.wc.ITestingComputer} xyz.swapee.wc.BoundITestingComputer */

/** @typedef {xyz.swapee.wc.TestingComputer} xyz.swapee.wc.BoundTestingComputer */

/**
 * Contains getters to cast the _ITestingComputer_ interface.
 * @interface xyz.swapee.wc.ITestingComputerCaster
 */
xyz.swapee.wc.ITestingComputerCaster = class { }
/**
 * Cast the _ITestingComputer_ instance into the _BoundITestingComputer_ type.
 * @type {!xyz.swapee.wc.BoundITestingComputer}
 */
xyz.swapee.wc.ITestingComputerCaster.prototype.asITestingComputer
/**
 * Access the _TestingComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingComputer}
 */
xyz.swapee.wc.ITestingComputerCaster.prototype.superTestingComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form, changes: xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>)} xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingComputer.__adaptChangellyFloatingOffer<!xyz.swapee.wc.ITestingComputer>} xyz.swapee.wc.ITestingComputer._adaptChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer} */
/**
 * Loads the _Changelly_ floating offer.
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} form The form with inputs.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form} changes The previous values of the form.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer} xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form, changes: xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>)} xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingComputer.__adaptChangellyFixedOffer<!xyz.swapee.wc.ITestingComputer>} xyz.swapee.wc.ITestingComputer._adaptChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer} */
/**
 * Loads the _Changelly_ fixed offer.
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} form The form with inputs.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form} changes The previous values of the form.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer} xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form, changes: xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>)} xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingComputer.__adaptChangenowOffer<!xyz.swapee.wc.ITestingComputer>} xyz.swapee.wc.ITestingComputer._adaptChangenowOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingComputer.adaptChangenowOffer} */
/**
 * Loads the _Changenow_ offer.
 * @param {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} form The form with inputs.
 * - `changeNow` _boolean_ ⤴ *ITestingOuterCore.Model.ChangeNow_Safe*
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @param {xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form} changes The previous values of the form.
 * - `changeNow` _boolean_ ⤴ *ITestingOuterCore.Model.ChangeNow_Safe*
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ITestingComputer.adaptChangenowOffer = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe&xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer} xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form, changes: xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form) => (void|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return)} xyz.swapee.wc.ITestingComputer.__adaptAnyLoading
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingComputer.__adaptAnyLoading<!xyz.swapee.wc.ITestingComputer>} xyz.swapee.wc.ITestingComputer._adaptAnyLoading */
/** @typedef {typeof xyz.swapee.wc.ITestingComputer.adaptAnyLoading} */
/**
 * @param {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} form The form with inputs.
 * @param {xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return} The form with outputs.
 */
xyz.swapee.wc.ITestingComputer.adaptAnyLoading = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe&xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe&xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe} xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.AnyLoading} xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.TestingMemory) => void} xyz.swapee.wc.ITestingComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingComputer.__compute<!xyz.swapee.wc.ITestingComputer>} xyz.swapee.wc.ITestingComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ITestingComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.TestingMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.ITestingComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/03-ITestingOuterCore.xml}  0d12b9a4a74014f8eaed9f185a8d9d35 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITestingOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingOuterCore)} xyz.swapee.wc.AbstractTestingOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingOuterCore} xyz.swapee.wc.TestingOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractTestingOuterCore
 */
xyz.swapee.wc.AbstractTestingOuterCore = class extends /** @type {xyz.swapee.wc.AbstractTestingOuterCore.constructor&xyz.swapee.wc.TestingOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingOuterCore.prototype.constructor = xyz.swapee.wc.AbstractTestingOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractTestingOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.AbstractTestingOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITestingOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingOuterCoreCaster)} xyz.swapee.wc.ITestingOuterCore.constructor */
/**
 * The _ITesting_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ITestingOuterCore
 */
xyz.swapee.wc.ITestingOuterCore = class extends /** @type {xyz.swapee.wc.ITestingOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ITestingOuterCore.prototype.constructor = xyz.swapee.wc.ITestingOuterCore

/** @typedef {function(new: xyz.swapee.wc.ITestingOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingOuterCore.Initialese>)} xyz.swapee.wc.TestingOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingOuterCore} xyz.swapee.wc.ITestingOuterCore.typeof */
/**
 * A concrete class of _ITestingOuterCore_ instances.
 * @constructor xyz.swapee.wc.TestingOuterCore
 * @implements {xyz.swapee.wc.ITestingOuterCore} The _ITesting_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.TestingOuterCore = class extends /** @type {xyz.swapee.wc.TestingOuterCore.constructor&xyz.swapee.wc.ITestingOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TestingOuterCore.prototype.constructor = xyz.swapee.wc.TestingOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingOuterCore}
 */
xyz.swapee.wc.TestingOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingOuterCore.
 * @interface xyz.swapee.wc.ITestingOuterCoreFields
 */
xyz.swapee.wc.ITestingOuterCoreFields = class { }
/**
 * The _ITesting_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ITestingOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITestingOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingOuterCore} */
xyz.swapee.wc.RecordITestingOuterCore

/** @typedef {xyz.swapee.wc.ITestingOuterCore} xyz.swapee.wc.BoundITestingOuterCore */

/** @typedef {xyz.swapee.wc.TestingOuterCore} xyz.swapee.wc.BoundTestingOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.ITestingOuterCore.Model.Core.core

/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.Host.host

/** @typedef {number} */
xyz.swapee.wc.ITestingOuterCore.Model.AmountIn.amountIn

/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn.currencyIn

/** @typedef {string} */
xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut.currencyOut

/** @typedef {boolean} */
xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow.changeNow

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.Core&xyz.swapee.wc.ITestingOuterCore.Model.Host&xyz.swapee.wc.ITestingOuterCore.Model.AmountIn&xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn&xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut&xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow} xyz.swapee.wc.ITestingOuterCore.Model The _ITesting_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core&xyz.swapee.wc.ITestingOuterCore.WeakModel.Host&xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn&xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn&xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut&xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow} xyz.swapee.wc.ITestingOuterCore.WeakModel The _ITesting_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ITestingOuterCore_ interface.
 * @interface xyz.swapee.wc.ITestingOuterCoreCaster
 */
xyz.swapee.wc.ITestingOuterCoreCaster = class { }
/**
 * Cast the _ITestingOuterCore_ instance into the _BoundITestingOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundITestingOuterCore}
 */
xyz.swapee.wc.ITestingOuterCoreCaster.prototype.asITestingOuterCore
/**
 * Access the _TestingOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingOuterCore}
 */
xyz.swapee.wc.ITestingOuterCoreCaster.prototype.superTestingOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.Core The core property (optional overlay).
 * @prop {string} [core=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe The core property (required overlay).
 * @prop {string} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.Host  (optional overlay).
 * @prop {string} [host=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe  (required overlay).
 * @prop {string} host
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn  (optional overlay).
 * @prop {number} [amountIn=0.1] Default `0.1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe  (required overlay).
 * @prop {number} amountIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn  (optional overlay).
 * @prop {string} [currencyIn="btc"] Default `btc`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe  (required overlay).
 * @prop {string} currencyIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut  (optional overlay).
 * @prop {string} [currencyOut="eth"] Default `eth`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe  (required overlay).
 * @prop {string} currencyOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow  (optional overlay).
 * @prop {boolean} [changeNow=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe  (required overlay).
 * @prop {boolean} changeNow
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.Core The core property (optional overlay).
 * @prop {*} [core=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe The core property (required overlay).
 * @prop {*} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.Host  (optional overlay).
 * @prop {*} [host=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe  (required overlay).
 * @prop {*} host
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn  (optional overlay).
 * @prop {*} [amountIn="0.1"] Default `0.1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe  (required overlay).
 * @prop {*} amountIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn  (optional overlay).
 * @prop {*} [currencyIn="btc"] Default `btc`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe  (required overlay).
 * @prop {*} currencyIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut  (optional overlay).
 * @prop {*} [currencyOut="eth"] Default `eth`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe  (required overlay).
 * @prop {*} currencyOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow  (optional overlay).
 * @prop {*} [changeNow=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe  (required overlay).
 * @prop {*} changeNow
 */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core} xyz.swapee.wc.ITestingPort.Inputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ITestingPort.Inputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host} xyz.swapee.wc.ITestingPort.Inputs.Host  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.ITestingPort.Inputs.Host_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn} xyz.swapee.wc.ITestingPort.Inputs.AmountIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe} xyz.swapee.wc.ITestingPort.Inputs.AmountIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn} xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe} xyz.swapee.wc.ITestingPort.Inputs.CurrencyIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut} xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe} xyz.swapee.wc.ITestingPort.Inputs.CurrencyOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow} xyz.swapee.wc.ITestingPort.Inputs.ChangeNow  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe} xyz.swapee.wc.ITestingPort.Inputs.ChangeNow_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core} xyz.swapee.wc.ITestingPort.WeakInputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ITestingPort.WeakInputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host} xyz.swapee.wc.ITestingPort.WeakInputs.Host  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.ITestingPort.WeakInputs.Host_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn} xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.AmountIn_Safe} xyz.swapee.wc.ITestingPort.WeakInputs.AmountIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyIn_Safe} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.CurrencyOut_Safe} xyz.swapee.wc.ITestingPort.WeakInputs.CurrencyOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow} xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.WeakModel.ChangeNow_Safe} xyz.swapee.wc.ITestingPort.WeakInputs.ChangeNow_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.Core} xyz.swapee.wc.ITestingCore.Model.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.Core_Safe} xyz.swapee.wc.ITestingCore.Model.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.Host} xyz.swapee.wc.ITestingCore.Model.Host  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.Host_Safe} xyz.swapee.wc.ITestingCore.Model.Host_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn} xyz.swapee.wc.ITestingCore.Model.AmountIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.AmountIn_Safe} xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn} xyz.swapee.wc.ITestingCore.Model.CurrencyIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyIn_Safe} xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut} xyz.swapee.wc.ITestingCore.Model.CurrencyOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow} xyz.swapee.wc.ITestingCore.Model.ChangeNow  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model.ChangeNow_Safe} xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/04-ITestingPort.xml}  7bbf16f1eee9067a729c193c30ea8b29 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITestingPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingPort)} xyz.swapee.wc.AbstractTestingPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingPort} xyz.swapee.wc.TestingPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingPort` interface.
 * @constructor xyz.swapee.wc.AbstractTestingPort
 */
xyz.swapee.wc.AbstractTestingPort = class extends /** @type {xyz.swapee.wc.AbstractTestingPort.constructor&xyz.swapee.wc.TestingPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingPort.prototype.constructor = xyz.swapee.wc.AbstractTestingPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingPort.class = /** @type {typeof xyz.swapee.wc.AbstractTestingPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingPort|typeof xyz.swapee.wc.TestingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.AbstractTestingPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingPort.Initialese[]) => xyz.swapee.wc.ITestingPort} xyz.swapee.wc.TestingPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITestingPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITestingPort.Inputs>)} xyz.swapee.wc.ITestingPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ITesting_, providing input
 * pins.
 * @interface xyz.swapee.wc.ITestingPort
 */
xyz.swapee.wc.ITestingPort = class extends /** @type {xyz.swapee.wc.ITestingPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITestingPort.resetPort} */
xyz.swapee.wc.ITestingPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ITestingPort.resetTestingPort} */
xyz.swapee.wc.ITestingPort.prototype.resetTestingPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingPort.Initialese>)} xyz.swapee.wc.TestingPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingPort} xyz.swapee.wc.ITestingPort.typeof */
/**
 * A concrete class of _ITestingPort_ instances.
 * @constructor xyz.swapee.wc.TestingPort
 * @implements {xyz.swapee.wc.ITestingPort} The port that serves as an interface to the _ITesting_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingPort.Initialese>} ‎
 */
xyz.swapee.wc.TestingPort = class extends /** @type {xyz.swapee.wc.TestingPort.constructor&xyz.swapee.wc.ITestingPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingPort}
 */
xyz.swapee.wc.TestingPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingPort.
 * @interface xyz.swapee.wc.ITestingPortFields
 */
xyz.swapee.wc.ITestingPortFields = class { }
/**
 * The inputs to the _ITesting_'s controller via its port.
 */
xyz.swapee.wc.ITestingPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITestingPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITestingPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITestingPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingPort} */
xyz.swapee.wc.RecordITestingPort

/** @typedef {xyz.swapee.wc.ITestingPort} xyz.swapee.wc.BoundITestingPort */

/** @typedef {xyz.swapee.wc.TestingPort} xyz.swapee.wc.BoundTestingPort */

/** @typedef {function(new: xyz.swapee.wc.ITestingOuterCore.WeakModel)} xyz.swapee.wc.ITestingPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingOuterCore.WeakModel} xyz.swapee.wc.ITestingOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ITesting_'s controller via its port.
 * @record xyz.swapee.wc.ITestingPort.Inputs
 */
xyz.swapee.wc.ITestingPort.Inputs = class extends /** @type {xyz.swapee.wc.ITestingPort.Inputs.constructor&xyz.swapee.wc.ITestingOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITestingPort.Inputs.prototype.constructor = xyz.swapee.wc.ITestingPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITestingOuterCore.WeakModel)} xyz.swapee.wc.ITestingPort.WeakInputs.constructor */
/**
 * The inputs to the _ITesting_'s controller via its port.
 * @record xyz.swapee.wc.ITestingPort.WeakInputs
 */
xyz.swapee.wc.ITestingPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITestingPort.WeakInputs.constructor&xyz.swapee.wc.ITestingOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITestingPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITestingPort.WeakInputs

/**
 * Contains getters to cast the _ITestingPort_ interface.
 * @interface xyz.swapee.wc.ITestingPortCaster
 */
xyz.swapee.wc.ITestingPortCaster = class { }
/**
 * Cast the _ITestingPort_ instance into the _BoundITestingPort_ type.
 * @type {!xyz.swapee.wc.BoundITestingPort}
 */
xyz.swapee.wc.ITestingPortCaster.prototype.asITestingPort
/**
 * Access the _TestingPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingPort}
 */
xyz.swapee.wc.ITestingPortCaster.prototype.superTestingPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingPort.__resetPort<!xyz.swapee.wc.ITestingPort>} xyz.swapee.wc.ITestingPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITestingPort.resetPort} */
/**
 * Resets the _ITesting_ port.
 * @return {void}
 */
xyz.swapee.wc.ITestingPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingPort.__resetTestingPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingPort.__resetTestingPort<!xyz.swapee.wc.ITestingPort>} xyz.swapee.wc.ITestingPort._resetTestingPort */
/** @typedef {typeof xyz.swapee.wc.ITestingPort.resetTestingPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITestingPort.resetTestingPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/09-ITestingCore.xml}  3c4ab2b7ecb21388e618cdbfd73517ab */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITestingCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingCore)} xyz.swapee.wc.AbstractTestingCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingCore} xyz.swapee.wc.TestingCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingCore` interface.
 * @constructor xyz.swapee.wc.AbstractTestingCore
 */
xyz.swapee.wc.AbstractTestingCore = class extends /** @type {xyz.swapee.wc.AbstractTestingCore.constructor&xyz.swapee.wc.TestingCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingCore.prototype.constructor = xyz.swapee.wc.AbstractTestingCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingCore.class = /** @type {typeof xyz.swapee.wc.AbstractTestingCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingOuterCore|typeof xyz.swapee.wc.TestingOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.AbstractTestingCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITestingCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingCoreCaster&xyz.swapee.wc.ITestingOuterCore)} xyz.swapee.wc.ITestingCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingOuterCore} xyz.swapee.wc.ITestingOuterCore.typeof */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ITestingCore
 */
xyz.swapee.wc.ITestingCore = class extends /** @type {xyz.swapee.wc.ITestingCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITestingOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ITestingCore.resetCore} */
xyz.swapee.wc.ITestingCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ITestingCore.resetTestingCore} */
xyz.swapee.wc.ITestingCore.prototype.resetTestingCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingCore.Initialese>)} xyz.swapee.wc.TestingCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingCore} xyz.swapee.wc.ITestingCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITestingCore_ instances.
 * @constructor xyz.swapee.wc.TestingCore
 * @implements {xyz.swapee.wc.ITestingCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingCore.Initialese>} ‎
 */
xyz.swapee.wc.TestingCore = class extends /** @type {xyz.swapee.wc.TestingCore.constructor&xyz.swapee.wc.ITestingCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TestingCore.prototype.constructor = xyz.swapee.wc.TestingCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingCore}
 */
xyz.swapee.wc.TestingCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingCore.
 * @interface xyz.swapee.wc.ITestingCoreFields
 */
xyz.swapee.wc.ITestingCoreFields = class { }
/**
 * The _ITesting_'s memory.
 */
xyz.swapee.wc.ITestingCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITestingCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ITestingCoreFields.prototype.props = /** @type {xyz.swapee.wc.ITestingCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingCore} */
xyz.swapee.wc.RecordITestingCore

/** @typedef {xyz.swapee.wc.ITestingCore} xyz.swapee.wc.BoundITestingCore */

/** @typedef {xyz.swapee.wc.TestingCore} xyz.swapee.wc.BoundTestingCore */

/** @typedef {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer.changellyFixedOffer

/** @typedef {number} */
xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer.changellyFloatingOffer

/** @typedef {{ amountOut: string, type: string }} */
xyz.swapee.wc.ITestingCore.Model.ChangenowOffer.changenowOffer

/** @typedef {boolean} */
xyz.swapee.wc.ITestingCore.Model.AnyLoading.anyLoading

/**
 * Whether the items are being loaded from the remote.
 * @typedef {boolean}
 */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer.loadingChangellyFloatingOffer

/**
 * Whether there are more items to load.
 * @typedef {boolean}
 */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer.hasMoreChangellyFloatingOffer

/**
 * An error during loading of items from the remote.
 * @typedef {Error}
 */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError.loadChangellyFloatingOfferError

/**
 * Whether the items are being loaded from the remote.
 * @typedef {boolean}
 */
xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer.loadingChangellyFixedOffer

/**
 * Whether there are more items to load.
 * @typedef {boolean}
 */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer.hasMoreChangellyFixedOffer

/**
 * An error during loading of items from the remote.
 * @typedef {Error}
 */
xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError.loadChangellyFixedOfferError

/**
 * Whether the items are being loaded from the remote.
 * @typedef {boolean}
 */
xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer.loadingChangenowOffer

/**
 * Whether there are more items to load.
 * @typedef {boolean}
 */
xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer.hasMoreChangenowOffer

/**
 * An error during loading of items from the remote.
 * @typedef {Error}
 */
xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError.loadChangenowOfferError

/** @typedef {xyz.swapee.wc.ITestingOuterCore.Model&xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer&xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer&xyz.swapee.wc.ITestingCore.Model.ChangenowOffer&xyz.swapee.wc.ITestingCore.Model.AnyLoading&xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer&xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer&xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError&xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer&xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer&xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError&xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer&xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer&xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError} xyz.swapee.wc.ITestingCore.Model The _ITesting_'s memory. */

/**
 * Contains getters to cast the _ITestingCore_ interface.
 * @interface xyz.swapee.wc.ITestingCoreCaster
 */
xyz.swapee.wc.ITestingCoreCaster = class { }
/**
 * Cast the _ITestingCore_ instance into the _BoundITestingCore_ type.
 * @type {!xyz.swapee.wc.BoundITestingCore}
 */
xyz.swapee.wc.ITestingCoreCaster.prototype.asITestingCore
/**
 * Access the _TestingCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingCore}
 */
xyz.swapee.wc.ITestingCoreCaster.prototype.superTestingCore

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer  (optional overlay).
 * @prop {number} [changellyFixedOffer=0] Default `0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer_Safe  (required overlay).
 * @prop {number} changellyFixedOffer
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer  (optional overlay).
 * @prop {number} [changellyFloatingOffer=0] Default `0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer_Safe  (required overlay).
 * @prop {number} changellyFloatingOffer
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer  (optional overlay).
 * @prop {{ amountOut: string, type: string }} [changenowOffer=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.ChangenowOffer_Safe  (required overlay).
 * @prop {{ amountOut: string, type: string }} changenowOffer
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.AnyLoading  (optional overlay).
 * @prop {boolean} [anyLoading=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.AnyLoading_Safe  (required overlay).
 * @prop {boolean} anyLoading
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer Whether the items are being loaded from the remote (optional overlay).
 * @prop {boolean} [loadingChangellyFloatingOffer=false] Whether the items are being loaded from the remote. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFloatingOffer_Safe Whether the items are being loaded from the remote (required overlay).
 * @prop {boolean} loadingChangellyFloatingOffer Whether the items are being loaded from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer Whether there are more items to load (optional overlay).
 * @prop {?boolean} [hasMoreChangellyFloatingOffer=null] Whether there are more items to load. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFloatingOffer_Safe Whether there are more items to load (required overlay).
 * @prop {?boolean} hasMoreChangellyFloatingOffer Whether there are more items to load.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError An error during loading of items from the remote (optional overlay).
 * @prop {?Error} [loadChangellyFloatingOfferError=null] An error during loading of items from the remote. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFloatingOfferError_Safe An error during loading of items from the remote (required overlay).
 * @prop {?Error} loadChangellyFloatingOfferError An error during loading of items from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer Whether the items are being loaded from the remote (optional overlay).
 * @prop {boolean} [loadingChangellyFixedOffer=false] Whether the items are being loaded from the remote. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadingChangellyFixedOffer_Safe Whether the items are being loaded from the remote (required overlay).
 * @prop {boolean} loadingChangellyFixedOffer Whether the items are being loaded from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer Whether there are more items to load (optional overlay).
 * @prop {?boolean} [hasMoreChangellyFixedOffer=null] Whether there are more items to load. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.HasMoreChangellyFixedOffer_Safe Whether there are more items to load (required overlay).
 * @prop {?boolean} hasMoreChangellyFixedOffer Whether there are more items to load.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError An error during loading of items from the remote (optional overlay).
 * @prop {?Error} [loadChangellyFixedOfferError=null] An error during loading of items from the remote. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadChangellyFixedOfferError_Safe An error during loading of items from the remote (required overlay).
 * @prop {?Error} loadChangellyFixedOfferError An error during loading of items from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer Whether the items are being loaded from the remote (optional overlay).
 * @prop {boolean} [loadingChangenowOffer=false] Whether the items are being loaded from the remote. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadingChangenowOffer_Safe Whether the items are being loaded from the remote (required overlay).
 * @prop {boolean} loadingChangenowOffer Whether the items are being loaded from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer Whether there are more items to load (optional overlay).
 * @prop {?boolean} [hasMoreChangenowOffer=null] Whether there are more items to load. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.HasMoreChangenowOffer_Safe Whether there are more items to load (required overlay).
 * @prop {?boolean} hasMoreChangenowOffer Whether there are more items to load.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError An error during loading of items from the remote (optional overlay).
 * @prop {?Error} [loadChangenowOfferError=null] An error during loading of items from the remote. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingCore.Model.LoadChangenowOfferError_Safe An error during loading of items from the remote (required overlay).
 * @prop {?Error} loadChangenowOfferError An error during loading of items from the remote.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingCore.__resetCore<!xyz.swapee.wc.ITestingCore>} xyz.swapee.wc.ITestingCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ITestingCore.resetCore} */
/**
 * Resets the _ITesting_ core.
 * @return {void}
 */
xyz.swapee.wc.ITestingCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingCore.__resetTestingCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingCore.__resetTestingCore<!xyz.swapee.wc.ITestingCore>} xyz.swapee.wc.ITestingCore._resetTestingCore */
/** @typedef {typeof xyz.swapee.wc.ITestingCore.resetTestingCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITestingCore.resetTestingCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/10-ITestingProcessor.xml}  90553037e58411bed2cb20baf14ddb8d */
/** @typedef {xyz.swapee.wc.ITestingComputer.Initialese&xyz.swapee.wc.ITestingController.Initialese} xyz.swapee.wc.ITestingProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingProcessor)} xyz.swapee.wc.AbstractTestingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingProcessor} xyz.swapee.wc.TestingProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractTestingProcessor
 */
xyz.swapee.wc.AbstractTestingProcessor = class extends /** @type {xyz.swapee.wc.AbstractTestingProcessor.constructor&xyz.swapee.wc.TestingProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingProcessor.prototype.constructor = xyz.swapee.wc.AbstractTestingProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractTestingProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingCore|typeof xyz.swapee.wc.TestingCore)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.AbstractTestingProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingProcessor.Initialese[]) => xyz.swapee.wc.ITestingProcessor} xyz.swapee.wc.TestingProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITestingProcessorCaster&xyz.swapee.wc.ITestingComputer&xyz.swapee.wc.ITestingCore&xyz.swapee.wc.ITestingController)} xyz.swapee.wc.ITestingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingController} xyz.swapee.wc.ITestingController.typeof */
/**
 * The processor to compute changes to the memory for the _ITesting_.
 * @interface xyz.swapee.wc.ITestingProcessor
 */
xyz.swapee.wc.ITestingProcessor = class extends /** @type {xyz.swapee.wc.ITestingProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITestingComputer.typeof&xyz.swapee.wc.ITestingCore.typeof&xyz.swapee.wc.ITestingController.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITestingProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingProcessor.Initialese>)} xyz.swapee.wc.TestingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingProcessor} xyz.swapee.wc.ITestingProcessor.typeof */
/**
 * A concrete class of _ITestingProcessor_ instances.
 * @constructor xyz.swapee.wc.TestingProcessor
 * @implements {xyz.swapee.wc.ITestingProcessor} The processor to compute changes to the memory for the _ITesting_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingProcessor.Initialese>} ‎
 */
xyz.swapee.wc.TestingProcessor = class extends /** @type {xyz.swapee.wc.TestingProcessor.constructor&xyz.swapee.wc.ITestingProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingProcessor}
 */
xyz.swapee.wc.TestingProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITestingProcessor} */
xyz.swapee.wc.RecordITestingProcessor

/** @typedef {xyz.swapee.wc.ITestingProcessor} xyz.swapee.wc.BoundITestingProcessor */

/** @typedef {xyz.swapee.wc.TestingProcessor} xyz.swapee.wc.BoundTestingProcessor */

/**
 * Contains getters to cast the _ITestingProcessor_ interface.
 * @interface xyz.swapee.wc.ITestingProcessorCaster
 */
xyz.swapee.wc.ITestingProcessorCaster = class { }
/**
 * Cast the _ITestingProcessor_ instance into the _BoundITestingProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITestingProcessor}
 */
xyz.swapee.wc.ITestingProcessorCaster.prototype.asITestingProcessor
/**
 * Access the _TestingProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingProcessor}
 */
xyz.swapee.wc.ITestingProcessorCaster.prototype.superTestingProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/100-TestingMemory.xml}  94ffaa9b2f697bec16dae4e498534806 */
/**
 * The memory of the _ITesting_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.TestingMemory
 */
xyz.swapee.wc.TestingMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.TestingMemory.prototype.core = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TestingMemory.prototype.host = /** @type {string} */ (void 0)
/**
 * Default `0`.
 */
xyz.swapee.wc.TestingMemory.prototype.amountIn = /** @type {number} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TestingMemory.prototype.currencyIn = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TestingMemory.prototype.currencyOut = /** @type {string} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.TestingMemory.prototype.changeNow = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `0`.
 */
xyz.swapee.wc.TestingMemory.prototype.changellyFixedOffer = /** @type {number} */ (void 0)
/**
 * Default `0`.
 */
xyz.swapee.wc.TestingMemory.prototype.changellyFloatingOffer = /** @type {number} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.TestingMemory.prototype.changenowOffer = /** @type {{ amountOut: string, type: string }} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.TestingMemory.prototype.anyLoading = /** @type {boolean} */ (void 0)
/**
 * Whether the items are being loaded from the remote. Default `false`.
 */
xyz.swapee.wc.TestingMemory.prototype.loadingChangellyFloatingOffer = /** @type {boolean} */ (void 0)
/**
 * Whether there are more items to load. Default `null`.
 */
xyz.swapee.wc.TestingMemory.prototype.hasMoreChangellyFloatingOffer = /** @type {?boolean} */ (void 0)
/**
 * An error during loading of items from the remote. Default `null`.
 */
xyz.swapee.wc.TestingMemory.prototype.loadChangellyFloatingOfferError = /** @type {?Error} */ (void 0)
/**
 * Whether the items are being loaded from the remote. Default `false`.
 */
xyz.swapee.wc.TestingMemory.prototype.loadingChangellyFixedOffer = /** @type {boolean} */ (void 0)
/**
 * Whether there are more items to load. Default `null`.
 */
xyz.swapee.wc.TestingMemory.prototype.hasMoreChangellyFixedOffer = /** @type {?boolean} */ (void 0)
/**
 * An error during loading of items from the remote. Default `null`.
 */
xyz.swapee.wc.TestingMemory.prototype.loadChangellyFixedOfferError = /** @type {?Error} */ (void 0)
/**
 * Whether the items are being loaded from the remote. Default `false`.
 */
xyz.swapee.wc.TestingMemory.prototype.loadingChangenowOffer = /** @type {boolean} */ (void 0)
/**
 * Whether there are more items to load. Default `null`.
 */
xyz.swapee.wc.TestingMemory.prototype.hasMoreChangenowOffer = /** @type {?boolean} */ (void 0)
/**
 * An error during loading of items from the remote. Default `null`.
 */
xyz.swapee.wc.TestingMemory.prototype.loadChangenowOfferError = /** @type {?Error} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/102-TestingInputs.xml}  9fbb89c3bc77932cdb4b0ecab631b8a6 */
/**
 * The inputs of the _ITesting_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.TestingInputs
 */
xyz.swapee.wc.front.TestingInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.TestingInputs.prototype.core = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TestingInputs.prototype.host = /** @type {string|undefined} */ (void 0)
/**
 * Default `0.1`.
 */
xyz.swapee.wc.front.TestingInputs.prototype.amountIn = /** @type {number|undefined} */ (void 0)
/**
 * Default `btc`.
 */
xyz.swapee.wc.front.TestingInputs.prototype.currencyIn = /** @type {string|undefined} */ (void 0)
/**
 * Default `eth`.
 */
xyz.swapee.wc.front.TestingInputs.prototype.currencyOut = /** @type {string|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.TestingInputs.prototype.changeNow = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/11-ITesting.xml}  ece243f8167c8e6edeafbf2de7ea210f */
/**
 * An atomic wrapper for the _ITesting_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.TestingEnv
 */
xyz.swapee.wc.TestingEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.TestingEnv.prototype.testing = /** @type {xyz.swapee.wc.ITesting} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs>&xyz.swapee.wc.ITestingProcessor.Initialese&xyz.swapee.wc.ITestingComputer.Initialese&xyz.swapee.wc.ITestingController.Initialese} xyz.swapee.wc.ITesting.Initialese */

/** @typedef {function(new: xyz.swapee.wc.Testing)} xyz.swapee.wc.AbstractTesting.constructor */
/** @typedef {typeof xyz.swapee.wc.Testing} xyz.swapee.wc.Testing.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITesting` interface.
 * @constructor xyz.swapee.wc.AbstractTesting
 */
xyz.swapee.wc.AbstractTesting = class extends /** @type {xyz.swapee.wc.AbstractTesting.constructor&xyz.swapee.wc.Testing.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTesting.prototype.constructor = xyz.swapee.wc.AbstractTesting
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTesting.class = /** @type {typeof xyz.swapee.wc.AbstractTesting} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.Testing}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTesting.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTesting}
 */
xyz.swapee.wc.AbstractTesting.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.AbstractTesting.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITesting.Initialese[]) => xyz.swapee.wc.ITesting} xyz.swapee.wc.TestingConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ITesting.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ITesting.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ITesting.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ITesting.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.TestingMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.TestingClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ITestingFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingCaster&xyz.swapee.wc.ITestingProcessor&xyz.swapee.wc.ITestingComputer&xyz.swapee.wc.ITestingController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, null>)} xyz.swapee.wc.ITesting.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ITesting
 */
xyz.swapee.wc.ITesting = class extends /** @type {xyz.swapee.wc.ITesting.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITestingProcessor.typeof&xyz.swapee.wc.ITestingComputer.typeof&xyz.swapee.wc.ITestingController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ITesting* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITesting.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITesting&engineering.type.IInitialiser<!xyz.swapee.wc.ITesting.Initialese>)} xyz.swapee.wc.Testing.constructor */
/** @typedef {typeof xyz.swapee.wc.ITesting} xyz.swapee.wc.ITesting.typeof */
/**
 * A concrete class of _ITesting_ instances.
 * @constructor xyz.swapee.wc.Testing
 * @implements {xyz.swapee.wc.ITesting} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITesting.Initialese>} ‎
 */
xyz.swapee.wc.Testing = class extends /** @type {xyz.swapee.wc.Testing.constructor&xyz.swapee.wc.ITesting.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITesting* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITesting.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITesting* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITesting.Initialese} init The initialisation options.
 */
xyz.swapee.wc.Testing.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.Testing}
 */
xyz.swapee.wc.Testing.__extend = function(...Extensions) {}

/**
 * Fields of the ITesting.
 * @interface xyz.swapee.wc.ITestingFields
 */
xyz.swapee.wc.ITestingFields = class { }
/**
 * The input pins of the _ITesting_ port.
 */
xyz.swapee.wc.ITestingFields.prototype.pinout = /** @type {!xyz.swapee.wc.ITesting.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ITesting} */
xyz.swapee.wc.RecordITesting

/** @typedef {xyz.swapee.wc.ITesting} xyz.swapee.wc.BoundITesting */

/** @typedef {xyz.swapee.wc.Testing} xyz.swapee.wc.BoundTesting */

/** @typedef {xyz.swapee.wc.ITestingController.Inputs} xyz.swapee.wc.ITesting.Pinout The input pins of the _ITesting_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITestingController.Inputs>)} xyz.swapee.wc.ITestingBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ITestingBuffer
 */
xyz.swapee.wc.ITestingBuffer = class extends /** @type {xyz.swapee.wc.ITestingBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ITestingBuffer.prototype.constructor = xyz.swapee.wc.ITestingBuffer

/**
 * A concrete class of _ITestingBuffer_ instances.
 * @constructor xyz.swapee.wc.TestingBuffer
 * @implements {xyz.swapee.wc.ITestingBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.TestingBuffer = class extends xyz.swapee.wc.ITestingBuffer { }
xyz.swapee.wc.TestingBuffer.prototype.constructor = xyz.swapee.wc.TestingBuffer

/**
 * Contains getters to cast the _ITesting_ interface.
 * @interface xyz.swapee.wc.ITestingCaster
 */
xyz.swapee.wc.ITestingCaster = class { }
/**
 * Cast the _ITesting_ instance into the _BoundITesting_ type.
 * @type {!xyz.swapee.wc.BoundITesting}
 */
xyz.swapee.wc.ITestingCaster.prototype.asITesting
/**
 * Access the _Testing_ prototype.
 * @type {!xyz.swapee.wc.BoundTesting}
 */
xyz.swapee.wc.ITestingCaster.prototype.superTesting

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/110-TestingSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TestingMemoryPQs
 */
xyz.swapee.wc.TestingMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
    /**
     * `g7b3d`
     */
    this.host=/** @type {string} */ (void 0)
    /**
     * `i8da7`
     */
    this.amountIn=/** @type {string} */ (void 0)
    /**
     * `f3088`
     */
    this.currencyIn=/** @type {string} */ (void 0)
    /**
     * `j8dbb`
     */
    this.currencyOut=/** @type {string} */ (void 0)
    /**
     * `j4af8`
     */
    this.changeNow=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TestingMemoryPQs.prototype.constructor = xyz.swapee.wc.TestingMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TestingMemoryQPs
 * @dict
 */
xyz.swapee.wc.TestingMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.TestingMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)
/**
 * `host`
 */
xyz.swapee.wc.TestingMemoryQPs.prototype.g7b3d = /** @type {string} */ (void 0)
/**
 * `amountIn`
 */
xyz.swapee.wc.TestingMemoryQPs.prototype.i8da7 = /** @type {string} */ (void 0)
/**
 * `currencyIn`
 */
xyz.swapee.wc.TestingMemoryQPs.prototype.f3088 = /** @type {string} */ (void 0)
/**
 * `currencyOut`
 */
xyz.swapee.wc.TestingMemoryQPs.prototype.j8dbb = /** @type {string} */ (void 0)
/**
 * `changeNow`
 */
xyz.swapee.wc.TestingMemoryQPs.prototype.j4af8 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.TestingMemoryPQs)} xyz.swapee.wc.TestingInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingMemoryPQs} xyz.swapee.wc.TestingMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TestingInputsPQs
 */
xyz.swapee.wc.TestingInputsPQs = class extends /** @type {xyz.swapee.wc.TestingInputsPQs.constructor&xyz.swapee.wc.TestingMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TestingInputsPQs.prototype.constructor = xyz.swapee.wc.TestingInputsPQs

/** @typedef {function(new: xyz.swapee.wc.TestingMemoryPQs)} xyz.swapee.wc.TestingInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TestingInputsQPs
 * @dict
 */
xyz.swapee.wc.TestingInputsQPs = class extends /** @type {xyz.swapee.wc.TestingInputsQPs.constructor&xyz.swapee.wc.TestingMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TestingInputsQPs.prototype.constructor = xyz.swapee.wc.TestingInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TestingCachePQs
 */
xyz.swapee.wc.TestingCachePQs = class {
  constructor() {
    /**
     * `hb5f4`
     */
    this.changellyFixedOffer=/** @type {string} */ (void 0)
    /**
     * `ca73d`
     */
    this.changellyFloatingOffer=/** @type {string} */ (void 0)
    /**
     * `dc0e8`
     */
    this.changenowOffer=/** @type {string} */ (void 0)
    /**
     * `ac77f`
     */
    this.anyLoading=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TestingCachePQs.prototype.constructor = xyz.swapee.wc.TestingCachePQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TestingCacheQPs
 * @dict
 */
xyz.swapee.wc.TestingCacheQPs = class { }
/**
 * `changellyFixedOffer`
 */
xyz.swapee.wc.TestingCacheQPs.prototype.hb5f4 = /** @type {string} */ (void 0)
/**
 * `changellyFloatingOffer`
 */
xyz.swapee.wc.TestingCacheQPs.prototype.ca73d = /** @type {string} */ (void 0)
/**
 * `changenowOffer`
 */
xyz.swapee.wc.TestingCacheQPs.prototype.dc0e8 = /** @type {string} */ (void 0)
/**
 * `anyLoading`
 */
xyz.swapee.wc.TestingCacheQPs.prototype.ac77f = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TestingVdusPQs
 */
xyz.swapee.wc.TestingVdusPQs = class {
  constructor() {
    /**
     * `g1ee3`
     */
    this.RatesLoIn=/** @type {string} */ (void 0)
    /**
     * `g1ee7`
     */
    this.ChangellyFloatingOffer=/** @type {string} */ (void 0)
    /**
     * `g1ee8`
     */
    this.ChangellyFloatingOfferAmount=/** @type {string} */ (void 0)
    /**
     * `g1ee12`
     */
    this.ChangellyFixedOffer=/** @type {string} */ (void 0)
    /**
     * `g1ee13`
     */
    this.ChangellyFixedOfferAmount=/** @type {string} */ (void 0)
    /**
     * `g1ee9`
     */
    this.AmountIn=/** @type {string} */ (void 0)
    /**
     * `g1ee10`
     */
    this.IncreaseBu=/** @type {string} */ (void 0)
    /**
     * `g1ee11`
     */
    this.DecreaseBu=/** @type {string} */ (void 0)
    /**
     * `g1ee4`
     */
    this.RatesLoEr=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TestingVdusPQs.prototype.constructor = xyz.swapee.wc.TestingVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TestingVdusQPs
 * @dict
 */
xyz.swapee.wc.TestingVdusQPs = class { }
/**
 * `RatesLoIn`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee3 = /** @type {string} */ (void 0)
/**
 * `ChangellyFloatingOffer`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee7 = /** @type {string} */ (void 0)
/**
 * `ChangellyFloatingOfferAmount`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee8 = /** @type {string} */ (void 0)
/**
 * `ChangellyFixedOffer`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee12 = /** @type {string} */ (void 0)
/**
 * `ChangellyFixedOfferAmount`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee13 = /** @type {string} */ (void 0)
/**
 * `AmountIn`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee9 = /** @type {string} */ (void 0)
/**
 * `IncreaseBu`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee10 = /** @type {string} */ (void 0)
/**
 * `DecreaseBu`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee11 = /** @type {string} */ (void 0)
/**
 * `RatesLoEr`
 */
xyz.swapee.wc.TestingVdusQPs.prototype.g1ee4 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/12-ITestingHtmlComponent.xml}  f51bae791f21de4e567d55afcd7478fc */
/** @typedef {xyz.swapee.wc.back.ITestingController.Initialese&xyz.swapee.wc.back.ITestingScreen.Initialese&xyz.swapee.wc.ITesting.Initialese&xyz.swapee.wc.ITestingGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ITestingProcessor.Initialese&xyz.swapee.wc.ITestingComputer.Initialese&ITestingGenerator.Initialese} xyz.swapee.wc.ITestingHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingHtmlComponent)} xyz.swapee.wc.AbstractTestingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingHtmlComponent} xyz.swapee.wc.TestingHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractTestingHtmlComponent
 */
xyz.swapee.wc.AbstractTestingHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractTestingHtmlComponent.constructor&xyz.swapee.wc.TestingHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractTestingHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractTestingHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingHtmlComponent|typeof xyz.swapee.wc.TestingHtmlComponent)|(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.ITesting|typeof xyz.swapee.wc.Testing)|(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITestingProcessor|typeof xyz.swapee.wc.TestingProcessor)|(!xyz.swapee.wc.ITestingComputer|typeof xyz.swapee.wc.TestingComputer)|(!ITestingGenerator|typeof TestingGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.AbstractTestingHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingHtmlComponent.Initialese[]) => xyz.swapee.wc.ITestingHtmlComponent} xyz.swapee.wc.TestingHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITestingHtmlComponentCaster&xyz.swapee.wc.back.ITestingController&xyz.swapee.wc.back.ITestingScreen&xyz.swapee.wc.ITesting&xyz.swapee.wc.ITestingGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.ITestingProcessor&xyz.swapee.wc.ITestingComputer&ITestingGenerator)} xyz.swapee.wc.ITestingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITestingController} xyz.swapee.wc.back.ITestingController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ITestingScreen} xyz.swapee.wc.back.ITestingScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingGPU} xyz.swapee.wc.ITestingGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof ITestingGenerator} ITestingGenerator.typeof */
/**
 * The _ITesting_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ITestingHtmlComponent
 */
xyz.swapee.wc.ITestingHtmlComponent = class extends /** @type {xyz.swapee.wc.ITestingHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITestingController.typeof&xyz.swapee.wc.back.ITestingScreen.typeof&xyz.swapee.wc.ITesting.typeof&xyz.swapee.wc.ITestingGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ITestingProcessor.typeof&xyz.swapee.wc.ITestingComputer.typeof&ITestingGenerator.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITestingHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingHtmlComponent.Initialese>)} xyz.swapee.wc.TestingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingHtmlComponent} xyz.swapee.wc.ITestingHtmlComponent.typeof */
/**
 * A concrete class of _ITestingHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.TestingHtmlComponent
 * @implements {xyz.swapee.wc.ITestingHtmlComponent} The _ITesting_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.TestingHtmlComponent = class extends /** @type {xyz.swapee.wc.TestingHtmlComponent.constructor&xyz.swapee.wc.ITestingHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingHtmlComponent}
 */
xyz.swapee.wc.TestingHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITestingHtmlComponent} */
xyz.swapee.wc.RecordITestingHtmlComponent

/** @typedef {xyz.swapee.wc.ITestingHtmlComponent} xyz.swapee.wc.BoundITestingHtmlComponent */

/** @typedef {xyz.swapee.wc.TestingHtmlComponent} xyz.swapee.wc.BoundTestingHtmlComponent */

/**
 * Contains getters to cast the _ITestingHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ITestingHtmlComponentCaster
 */
xyz.swapee.wc.ITestingHtmlComponentCaster = class { }
/**
 * Cast the _ITestingHtmlComponent_ instance into the _BoundITestingHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundITestingHtmlComponent}
 */
xyz.swapee.wc.ITestingHtmlComponentCaster.prototype.asITestingHtmlComponent
/**
 * Access the _TestingHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingHtmlComponent}
 */
xyz.swapee.wc.ITestingHtmlComponentCaster.prototype.superTestingHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/130-ITestingElement.xml}  2aa1e8d9657ecdde39955a7c15cdefb8 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ITestingElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.TestingElement)} xyz.swapee.wc.AbstractTestingElement.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingElement} xyz.swapee.wc.TestingElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingElement` interface.
 * @constructor xyz.swapee.wc.AbstractTestingElement
 */
xyz.swapee.wc.AbstractTestingElement = class extends /** @type {xyz.swapee.wc.AbstractTestingElement.constructor&xyz.swapee.wc.TestingElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingElement.prototype.constructor = xyz.swapee.wc.AbstractTestingElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingElement.class = /** @type {typeof xyz.swapee.wc.AbstractTestingElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingElement|typeof xyz.swapee.wc.TestingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.AbstractTestingElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingElement.Initialese[]) => xyz.swapee.wc.ITestingElement} xyz.swapee.wc.TestingElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITestingElementFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.ITestingElement.Inputs, null>)} xyz.swapee.wc.ITestingElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _ITesting_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ITestingElement
 */
xyz.swapee.wc.ITestingElement = class extends /** @type {xyz.swapee.wc.ITestingElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITestingElement.solder} */
xyz.swapee.wc.ITestingElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ITestingElement.render} */
xyz.swapee.wc.ITestingElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ITestingElement.server} */
xyz.swapee.wc.ITestingElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ITestingElement.inducer} */
xyz.swapee.wc.ITestingElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingElement&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingElement.Initialese>)} xyz.swapee.wc.TestingElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingElement} xyz.swapee.wc.ITestingElement.typeof */
/**
 * A concrete class of _ITestingElement_ instances.
 * @constructor xyz.swapee.wc.TestingElement
 * @implements {xyz.swapee.wc.ITestingElement} A component description.
 *
 * The _ITesting_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingElement.Initialese>} ‎
 */
xyz.swapee.wc.TestingElement = class extends /** @type {xyz.swapee.wc.TestingElement.constructor&xyz.swapee.wc.ITestingElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElement}
 */
xyz.swapee.wc.TestingElement.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingElement.
 * @interface xyz.swapee.wc.ITestingElementFields
 */
xyz.swapee.wc.ITestingElementFields = class { }
/**
 * The element-specific inputs to the _ITesting_ component.
 */
xyz.swapee.wc.ITestingElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITestingElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingElement} */
xyz.swapee.wc.RecordITestingElement

/** @typedef {xyz.swapee.wc.ITestingElement} xyz.swapee.wc.BoundITestingElement */

/** @typedef {xyz.swapee.wc.TestingElement} xyz.swapee.wc.BoundTestingElement */

/** @typedef {xyz.swapee.wc.ITestingPort.Inputs&xyz.swapee.wc.ITestingDisplay.Queries&xyz.swapee.wc.ITestingController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ITestingElementPort.Inputs} xyz.swapee.wc.ITestingElement.Inputs The element-specific inputs to the _ITesting_ component. */

/**
 * Contains getters to cast the _ITestingElement_ interface.
 * @interface xyz.swapee.wc.ITestingElementCaster
 */
xyz.swapee.wc.ITestingElementCaster = class { }
/**
 * Cast the _ITestingElement_ instance into the _BoundITestingElement_ type.
 * @type {!xyz.swapee.wc.BoundITestingElement}
 */
xyz.swapee.wc.ITestingElementCaster.prototype.asITestingElement
/**
 * Access the _TestingElement_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingElement}
 */
xyz.swapee.wc.ITestingElementCaster.prototype.superTestingElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.TestingMemory, props: !xyz.swapee.wc.ITestingElement.Inputs) => Object<string, *>} xyz.swapee.wc.ITestingElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingElement.__solder<!xyz.swapee.wc.ITestingElement>} xyz.swapee.wc.ITestingElement._solder */
/** @typedef {typeof xyz.swapee.wc.ITestingElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.TestingMemory} model The model.
 * - `core` _string_ The core property. Default empty string.
 * - `host` _string_ Default empty string.
 * - `amountIn` _number_ Default `0`.
 * - `currencyIn` _string_ Default empty string.
 * - `currencyOut` _string_ Default empty string.
 * - `changellyFixedOffer` _number_ Default `0`.
 * - `changellyFloatingOffer` _number_ Default `0`.
 * - `changenowOffer` _{ amountOut: string, type: string }_ Default `null`.
 * - `anyLoading` _boolean_ Default `false`.
 * - `[changeNow=false]` _boolean?_ Default `false`.
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} props The element props.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ITestingOuterCore.WeakModel.Core* ⤴ *ITestingOuterCore.WeakModel.Core* Default `null`.
 * - `[host=null]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.Host* ⤴ *ITestingOuterCore.WeakModel.Host* Default `null`.
 * - `[amountIn="0.1"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.AmountIn* ⤴ *ITestingOuterCore.WeakModel.AmountIn* Default `0.1`.
 * - `[currencyIn="btc"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.CurrencyIn* ⤴ *ITestingOuterCore.WeakModel.CurrencyIn* Default `btc`.
 * - `[currencyOut="eth"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.CurrencyOut* ⤴ *ITestingOuterCore.WeakModel.CurrencyOut* Default `eth`.
 * - `[changeNow=null]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.ChangeNow* ⤴ *ITestingOuterCore.WeakModel.ChangeNow* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITestingElementPort.Inputs.NoSolder* Default `false`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITestingElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[increaseBuOpts]` _!Object?_ The options to pass to the _IncreaseBu_ vdu. ⤴ *ITestingElementPort.Inputs.IncreaseBuOpts* Default `{}`.
 * - `[decreaseBuOpts]` _!Object?_ The options to pass to the _DecreaseBu_ vdu. ⤴ *ITestingElementPort.Inputs.DecreaseBuOpts* Default `{}`.
 * - `[ratesLoInOpts]` _!Object?_ The options to pass to the _RatesLoIn_ vdu. ⤴ *ITestingElementPort.Inputs.RatesLoInOpts* Default `{}`.
 * - `[ratesLoErOpts]` _!Object?_ The options to pass to the _RatesLoEr_ vdu. ⤴ *ITestingElementPort.Inputs.RatesLoErOpts* Default `{}`.
 * - `[changellyFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOffer_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFloatingOfferOpts* Default `{}`.
 * - `[changellyFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOfferAmount_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts* Default `{}`.
 * - `[changellyFixedOfferOpts]` _!Object?_ The options to pass to the _ChangellyFixedOffer_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFixedOfferOpts* Default `{}`.
 * - `[changellyFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFixedOfferAmount_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ITestingElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TestingMemory, instance?: !xyz.swapee.wc.ITestingScreen&xyz.swapee.wc.ITestingController) => !engineering.type.VNode} xyz.swapee.wc.ITestingElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingElement.__render<!xyz.swapee.wc.ITestingElement>} xyz.swapee.wc.ITestingElement._render */
/** @typedef {typeof xyz.swapee.wc.ITestingElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.TestingMemory} [model] The model for the view.
 * - `core` _string_ The core property. Default empty string.
 * - `host` _string_ Default empty string.
 * - `amountIn` _number_ Default `0`.
 * - `currencyIn` _string_ Default empty string.
 * - `currencyOut` _string_ Default empty string.
 * - `changellyFixedOffer` _number_ Default `0`.
 * - `changellyFloatingOffer` _number_ Default `0`.
 * - `changenowOffer` _{ amountOut: string, type: string }_ Default `null`.
 * - `anyLoading` _boolean_ Default `false`.
 * - `[changeNow=false]` _boolean?_ Default `false`.
 * @param {!xyz.swapee.wc.ITestingScreen&xyz.swapee.wc.ITestingController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITestingElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TestingMemory, inputs: !xyz.swapee.wc.ITestingElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ITestingElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingElement.__server<!xyz.swapee.wc.ITestingElement>} xyz.swapee.wc.ITestingElement._server */
/** @typedef {typeof xyz.swapee.wc.ITestingElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.TestingMemory} memory The memory registers.
 * - `core` _string_ The core property. Default empty string.
 * - `host` _string_ Default empty string.
 * - `amountIn` _number_ Default `0`.
 * - `currencyIn` _string_ Default empty string.
 * - `currencyOut` _string_ Default empty string.
 * - `changellyFixedOffer` _number_ Default `0`.
 * - `changellyFloatingOffer` _number_ Default `0`.
 * - `changenowOffer` _{ amountOut: string, type: string }_ Default `null`.
 * - `anyLoading` _boolean_ Default `false`.
 * - `[changeNow=false]` _boolean?_ Default `false`.
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} inputs The inputs to the port.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ITestingOuterCore.WeakModel.Core* ⤴ *ITestingOuterCore.WeakModel.Core* Default `null`.
 * - `[host=null]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.Host* ⤴ *ITestingOuterCore.WeakModel.Host* Default `null`.
 * - `[amountIn="0.1"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.AmountIn* ⤴ *ITestingOuterCore.WeakModel.AmountIn* Default `0.1`.
 * - `[currencyIn="btc"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.CurrencyIn* ⤴ *ITestingOuterCore.WeakModel.CurrencyIn* Default `btc`.
 * - `[currencyOut="eth"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.CurrencyOut* ⤴ *ITestingOuterCore.WeakModel.CurrencyOut* Default `eth`.
 * - `[changeNow=null]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.ChangeNow* ⤴ *ITestingOuterCore.WeakModel.ChangeNow* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITestingElementPort.Inputs.NoSolder* Default `false`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITestingElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[increaseBuOpts]` _!Object?_ The options to pass to the _IncreaseBu_ vdu. ⤴ *ITestingElementPort.Inputs.IncreaseBuOpts* Default `{}`.
 * - `[decreaseBuOpts]` _!Object?_ The options to pass to the _DecreaseBu_ vdu. ⤴ *ITestingElementPort.Inputs.DecreaseBuOpts* Default `{}`.
 * - `[ratesLoInOpts]` _!Object?_ The options to pass to the _RatesLoIn_ vdu. ⤴ *ITestingElementPort.Inputs.RatesLoInOpts* Default `{}`.
 * - `[ratesLoErOpts]` _!Object?_ The options to pass to the _RatesLoEr_ vdu. ⤴ *ITestingElementPort.Inputs.RatesLoErOpts* Default `{}`.
 * - `[changellyFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOffer_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFloatingOfferOpts* Default `{}`.
 * - `[changellyFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOfferAmount_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts* Default `{}`.
 * - `[changellyFixedOfferOpts]` _!Object?_ The options to pass to the _ChangellyFixedOffer_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFixedOfferOpts* Default `{}`.
 * - `[changellyFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFixedOfferAmount_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITestingElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TestingMemory, port?: !xyz.swapee.wc.ITestingElement.Inputs) => ?} xyz.swapee.wc.ITestingElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingElement.__inducer<!xyz.swapee.wc.ITestingElement>} xyz.swapee.wc.ITestingElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ITestingElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.TestingMemory} [model] The model of the component into which to induce the state.
 * - `core` _string_ The core property. Default empty string.
 * - `host` _string_ Default empty string.
 * - `amountIn` _number_ Default `0`.
 * - `currencyIn` _string_ Default empty string.
 * - `currencyOut` _string_ Default empty string.
 * - `changellyFixedOffer` _number_ Default `0`.
 * - `changellyFloatingOffer` _number_ Default `0`.
 * - `changenowOffer` _{ amountOut: string, type: string }_ Default `null`.
 * - `anyLoading` _boolean_ Default `false`.
 * - `[changeNow=false]` _boolean?_ Default `false`.
 * @param {!xyz.swapee.wc.ITestingElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ITestingOuterCore.WeakModel.Core* ⤴ *ITestingOuterCore.WeakModel.Core* Default `null`.
 * - `[host=null]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.Host* ⤴ *ITestingOuterCore.WeakModel.Host* Default `null`.
 * - `[amountIn="0.1"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.AmountIn* ⤴ *ITestingOuterCore.WeakModel.AmountIn* Default `0.1`.
 * - `[currencyIn="btc"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.CurrencyIn* ⤴ *ITestingOuterCore.WeakModel.CurrencyIn* Default `btc`.
 * - `[currencyOut="eth"]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.CurrencyOut* ⤴ *ITestingOuterCore.WeakModel.CurrencyOut* Default `eth`.
 * - `[changeNow=null]` _&#42;?_ ⤴ *ITestingOuterCore.WeakModel.ChangeNow* ⤴ *ITestingOuterCore.WeakModel.ChangeNow* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITestingElementPort.Inputs.NoSolder* Default `false`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITestingElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[increaseBuOpts]` _!Object?_ The options to pass to the _IncreaseBu_ vdu. ⤴ *ITestingElementPort.Inputs.IncreaseBuOpts* Default `{}`.
 * - `[decreaseBuOpts]` _!Object?_ The options to pass to the _DecreaseBu_ vdu. ⤴ *ITestingElementPort.Inputs.DecreaseBuOpts* Default `{}`.
 * - `[ratesLoInOpts]` _!Object?_ The options to pass to the _RatesLoIn_ vdu. ⤴ *ITestingElementPort.Inputs.RatesLoInOpts* Default `{}`.
 * - `[ratesLoErOpts]` _!Object?_ The options to pass to the _RatesLoEr_ vdu. ⤴ *ITestingElementPort.Inputs.RatesLoErOpts* Default `{}`.
 * - `[changellyFloatingOfferOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOffer_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFloatingOfferOpts* Default `{}`.
 * - `[changellyFloatingOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFloatingOfferAmount_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts* Default `{}`.
 * - `[changellyFixedOfferOpts]` _!Object?_ The options to pass to the _ChangellyFixedOffer_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFixedOfferOpts* Default `{}`.
 * - `[changellyFixedOfferAmountOpts]` _!Object?_ The options to pass to the _ChangellyFixedOfferAmount_ vdu. ⤴ *ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ITestingElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/140-ITestingElementPort.xml}  7c5edbb615bc5cf41d6e1be76eae30b4 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITestingElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingElementPort)} xyz.swapee.wc.AbstractTestingElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingElementPort} xyz.swapee.wc.TestingElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractTestingElementPort
 */
xyz.swapee.wc.AbstractTestingElementPort = class extends /** @type {xyz.swapee.wc.AbstractTestingElementPort.constructor&xyz.swapee.wc.TestingElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingElementPort.prototype.constructor = xyz.swapee.wc.AbstractTestingElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractTestingElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingElementPort|typeof xyz.swapee.wc.TestingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingElementPort|typeof xyz.swapee.wc.TestingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingElementPort|typeof xyz.swapee.wc.TestingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.AbstractTestingElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingElementPort.Initialese[]) => xyz.swapee.wc.ITestingElementPort} xyz.swapee.wc.TestingElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITestingElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITestingElementPort.Inputs>)} xyz.swapee.wc.ITestingElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ITestingElementPort
 */
xyz.swapee.wc.ITestingElementPort = class extends /** @type {xyz.swapee.wc.ITestingElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITestingElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingElementPort.Initialese>)} xyz.swapee.wc.TestingElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort} xyz.swapee.wc.ITestingElementPort.typeof */
/**
 * A concrete class of _ITestingElementPort_ instances.
 * @constructor xyz.swapee.wc.TestingElementPort
 * @implements {xyz.swapee.wc.ITestingElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingElementPort.Initialese>} ‎
 */
xyz.swapee.wc.TestingElementPort = class extends /** @type {xyz.swapee.wc.TestingElementPort.constructor&xyz.swapee.wc.ITestingElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingElementPort}
 */
xyz.swapee.wc.TestingElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingElementPort.
 * @interface xyz.swapee.wc.ITestingElementPortFields
 */
xyz.swapee.wc.ITestingElementPortFields = class { }
/**
 * The inputs to the _ITestingElement_'s controller via its element port.
 */
xyz.swapee.wc.ITestingElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITestingElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITestingElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITestingElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingElementPort} */
xyz.swapee.wc.RecordITestingElementPort

/** @typedef {xyz.swapee.wc.ITestingElementPort} xyz.swapee.wc.BoundITestingElementPort */

/** @typedef {xyz.swapee.wc.TestingElementPort} xyz.swapee.wc.BoundTestingElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _AmountIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts.amountInOpts

/**
 * The options to pass to the _IncreaseBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts.increaseBuOpts

/**
 * The options to pass to the _DecreaseBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts.decreaseBuOpts

/**
 * The options to pass to the _RatesLoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts.ratesLoInOpts

/**
 * The options to pass to the _RatesLoEr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts.ratesLoErOpts

/**
 * The options to pass to the _ChangellyFloatingOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts.changellyFloatingOfferOpts

/**
 * The options to pass to the _ChangellyFloatingOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts.changellyFloatingOfferAmountOpts

/**
 * The options to pass to the _ChangellyFixedOffer_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts.changellyFixedOfferOpts

/**
 * The options to pass to the _ChangellyFixedOfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts.changellyFixedOfferAmountOpts

/** @typedef {function(new: xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder&xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts&xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts&xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts&xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts&xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts)} xyz.swapee.wc.ITestingElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder} xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts} xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts} xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts} xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts.typeof */
/**
 * The inputs to the _ITestingElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITestingElementPort.Inputs
 */
xyz.swapee.wc.ITestingElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ITestingElementPort.Inputs.constructor&xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts.typeof&xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ITestingElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ITestingElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder&xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts)} xyz.swapee.wc.ITestingElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts.typeof */
/**
 * The inputs to the _ITestingElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITestingElementPort.WeakInputs
 */
xyz.swapee.wc.ITestingElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITestingElementPort.WeakInputs.constructor&xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts.typeof&xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ITestingElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITestingElementPort.WeakInputs

/**
 * Contains getters to cast the _ITestingElementPort_ interface.
 * @interface xyz.swapee.wc.ITestingElementPortCaster
 */
xyz.swapee.wc.ITestingElementPortCaster = class { }
/**
 * Cast the _ITestingElementPort_ instance into the _BoundITestingElementPort_ type.
 * @type {!xyz.swapee.wc.BoundITestingElementPort}
 */
xyz.swapee.wc.ITestingElementPortCaster.prototype.asITestingElementPort
/**
 * Access the _TestingElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingElementPort}
 */
xyz.swapee.wc.ITestingElementPortCaster.prototype.superTestingElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {!Object} [amountInOpts] The options to pass to the _AmountIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {!Object} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts The options to pass to the _IncreaseBu_ vdu (optional overlay).
 * @prop {!Object} [increaseBuOpts] The options to pass to the _IncreaseBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.IncreaseBuOpts_Safe The options to pass to the _IncreaseBu_ vdu (required overlay).
 * @prop {!Object} increaseBuOpts The options to pass to the _IncreaseBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts The options to pass to the _DecreaseBu_ vdu (optional overlay).
 * @prop {!Object} [decreaseBuOpts] The options to pass to the _DecreaseBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.DecreaseBuOpts_Safe The options to pass to the _DecreaseBu_ vdu (required overlay).
 * @prop {!Object} decreaseBuOpts The options to pass to the _DecreaseBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts The options to pass to the _RatesLoIn_ vdu (optional overlay).
 * @prop {!Object} [ratesLoInOpts] The options to pass to the _RatesLoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoInOpts_Safe The options to pass to the _RatesLoIn_ vdu (required overlay).
 * @prop {!Object} ratesLoInOpts The options to pass to the _RatesLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts The options to pass to the _RatesLoEr_ vdu (optional overlay).
 * @prop {!Object} [ratesLoErOpts] The options to pass to the _RatesLoEr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.RatesLoErOpts_Safe The options to pass to the _RatesLoEr_ vdu (required overlay).
 * @prop {!Object} ratesLoErOpts The options to pass to the _RatesLoEr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu (optional overlay).
 * @prop {!Object} [changellyFloatingOfferOpts] The options to pass to the _ChangellyFloatingOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferOpts_Safe The options to pass to the _ChangellyFloatingOffer_ vdu (required overlay).
 * @prop {!Object} changellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [changellyFloatingOfferAmountOpts] The options to pass to the _ChangellyFloatingOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFloatingOfferAmountOpts_Safe The options to pass to the _ChangellyFloatingOfferAmount_ vdu (required overlay).
 * @prop {!Object} changellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu (optional overlay).
 * @prop {!Object} [changellyFixedOfferOpts] The options to pass to the _ChangellyFixedOffer_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferOpts_Safe The options to pass to the _ChangellyFixedOffer_ vdu (required overlay).
 * @prop {!Object} changellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu (optional overlay).
 * @prop {!Object} [changellyFixedOfferAmountOpts] The options to pass to the _ChangellyFixedOfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.Inputs.ChangellyFixedOfferAmountOpts_Safe The options to pass to the _ChangellyFixedOfferAmount_ vdu (required overlay).
 * @prop {!Object} changellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {*} [amountInOpts=null] The options to pass to the _AmountIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {*} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts The options to pass to the _IncreaseBu_ vdu (optional overlay).
 * @prop {*} [increaseBuOpts=null] The options to pass to the _IncreaseBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.IncreaseBuOpts_Safe The options to pass to the _IncreaseBu_ vdu (required overlay).
 * @prop {*} increaseBuOpts The options to pass to the _IncreaseBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts The options to pass to the _DecreaseBu_ vdu (optional overlay).
 * @prop {*} [decreaseBuOpts=null] The options to pass to the _DecreaseBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.DecreaseBuOpts_Safe The options to pass to the _DecreaseBu_ vdu (required overlay).
 * @prop {*} decreaseBuOpts The options to pass to the _DecreaseBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts The options to pass to the _RatesLoIn_ vdu (optional overlay).
 * @prop {*} [ratesLoInOpts=null] The options to pass to the _RatesLoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoInOpts_Safe The options to pass to the _RatesLoIn_ vdu (required overlay).
 * @prop {*} ratesLoInOpts The options to pass to the _RatesLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts The options to pass to the _RatesLoEr_ vdu (optional overlay).
 * @prop {*} [ratesLoErOpts=null] The options to pass to the _RatesLoEr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.RatesLoErOpts_Safe The options to pass to the _RatesLoEr_ vdu (required overlay).
 * @prop {*} ratesLoErOpts The options to pass to the _RatesLoEr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu (optional overlay).
 * @prop {*} [changellyFloatingOfferOpts=null] The options to pass to the _ChangellyFloatingOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferOpts_Safe The options to pass to the _ChangellyFloatingOffer_ vdu (required overlay).
 * @prop {*} changellyFloatingOfferOpts The options to pass to the _ChangellyFloatingOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu (optional overlay).
 * @prop {*} [changellyFloatingOfferAmountOpts=null] The options to pass to the _ChangellyFloatingOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFloatingOfferAmountOpts_Safe The options to pass to the _ChangellyFloatingOfferAmount_ vdu (required overlay).
 * @prop {*} changellyFloatingOfferAmountOpts The options to pass to the _ChangellyFloatingOfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu (optional overlay).
 * @prop {*} [changellyFixedOfferOpts=null] The options to pass to the _ChangellyFixedOffer_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferOpts_Safe The options to pass to the _ChangellyFixedOffer_ vdu (required overlay).
 * @prop {*} changellyFixedOfferOpts The options to pass to the _ChangellyFixedOffer_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu (optional overlay).
 * @prop {*} [changellyFixedOfferAmountOpts=null] The options to pass to the _ChangellyFixedOfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingElementPort.WeakInputs.ChangellyFixedOfferAmountOpts_Safe The options to pass to the _ChangellyFixedOfferAmount_ vdu (required overlay).
 * @prop {*} changellyFixedOfferAmountOpts The options to pass to the _ChangellyFixedOfferAmount_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/160-ITestingRadio.xml}  6784c6cd178aa37d1320837bc5623b05 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITestingRadio.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingRadio)} xyz.swapee.wc.AbstractTestingRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingRadio} xyz.swapee.wc.TestingRadio.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingRadio` interface.
 * @constructor xyz.swapee.wc.AbstractTestingRadio
 */
xyz.swapee.wc.AbstractTestingRadio = class extends /** @type {xyz.swapee.wc.AbstractTestingRadio.constructor&xyz.swapee.wc.TestingRadio.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingRadio.prototype.constructor = xyz.swapee.wc.AbstractTestingRadio
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingRadio.class = /** @type {typeof xyz.swapee.wc.AbstractTestingRadio} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingRadio.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ITestingRadio|typeof xyz.swapee.wc.TestingRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.AbstractTestingRadio.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITestingRadioCaster)} xyz.swapee.wc.ITestingRadio.constructor */
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @interface xyz.swapee.wc.ITestingRadio
 */
xyz.swapee.wc.ITestingRadio = class extends /** @type {xyz.swapee.wc.ITestingRadio.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer} */
xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangellyFixedOffer = function() {}
/** @type {xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer} */
xyz.swapee.wc.ITestingRadio.prototype.adaptLoadChangenowOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingRadio&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingRadio.Initialese>)} xyz.swapee.wc.TestingRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingRadio} xyz.swapee.wc.ITestingRadio.typeof */
/**
 * A concrete class of _ITestingRadio_ instances.
 * @constructor xyz.swapee.wc.TestingRadio
 * @implements {xyz.swapee.wc.ITestingRadio} Transmits data _to-_ and _fro-_ off-chip system.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingRadio.Initialese>} ‎
 */
xyz.swapee.wc.TestingRadio = class extends /** @type {xyz.swapee.wc.TestingRadio.constructor&xyz.swapee.wc.ITestingRadio.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TestingRadio.prototype.constructor = xyz.swapee.wc.TestingRadio
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingRadio}
 */
xyz.swapee.wc.TestingRadio.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITestingRadio} */
xyz.swapee.wc.RecordITestingRadio

/** @typedef {xyz.swapee.wc.ITestingRadio} xyz.swapee.wc.BoundITestingRadio */

/** @typedef {xyz.swapee.wc.TestingRadio} xyz.swapee.wc.BoundTestingRadio */

/**
 * Contains getters to cast the _ITestingRadio_ interface.
 * @interface xyz.swapee.wc.ITestingRadioCaster
 */
xyz.swapee.wc.ITestingRadioCaster = class { }
/**
 * Cast the _ITestingRadio_ instance into the _BoundITestingRadio_ type.
 * @type {!xyz.swapee.wc.BoundITestingRadio}
 */
xyz.swapee.wc.ITestingRadioCaster.prototype.asITestingRadio
/**
 * Cast the _ITestingRadio_ instance into the _BoundITestingComputer_ type.
 * @type {!xyz.swapee.wc.BoundITestingComputer}
 */
xyz.swapee.wc.ITestingRadioCaster.prototype.asITestingComputer
/**
 * Access the _TestingRadio_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingRadio}
 */
xyz.swapee.wc.ITestingRadioCaster.prototype.superTestingRadio

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form, changes: ITestingComputer.adaptLoadChangellyFloatingOffer.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>)} xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFloatingOffer<!xyz.swapee.wc.ITestingRadio>} xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer} */
/**
 * Loads the _Changelly_ floating offer.
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form} form The form with inputs.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @param {ITestingComputer.adaptLoadChangellyFloatingOffer.Form} changes The previous values of the form.
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form, changes: ITestingComputer.adaptLoadChangellyFixedOffer.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>)} xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingRadio.__adaptLoadChangellyFixedOffer<!xyz.swapee.wc.ITestingRadio>} xyz.swapee.wc.ITestingRadio._adaptLoadChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer} */
/**
 * Loads the _Changelly_ fixed offer.
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form} form The form with inputs.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @param {ITestingComputer.adaptLoadChangellyFixedOffer.Form} changes The previous values of the form.
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer} xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form, changes: ITestingComputer.adaptLoadChangenowOffer.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>)} xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingRadio.__adaptLoadChangenowOffer<!xyz.swapee.wc.ITestingRadio>} xyz.swapee.wc.ITestingRadio._adaptLoadChangenowOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer} */
/**
 * Loads the _Changenow_ offer.
 * @param {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form} form The form with inputs.
 * - `changeNow` _boolean_ ⤴ *ITestingOuterCore.Model.ChangeNow_Safe*
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @param {ITestingComputer.adaptLoadChangenowOffer.Form} changes The previous values of the form.
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe&xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer} xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Return The form with outputs. */

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingRadio
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/170-ITestingDesigner.xml}  e42bfd9db932c8a96df7ccf11d571aa1 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ITestingDesigner
 */
xyz.swapee.wc.ITestingDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.TestingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITesting />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TestingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITesting />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TestingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITestingDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `Testing` _typeof ITestingController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITestingDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `Testing` _typeof ITestingController_
   * - `This` _typeof ITestingController_
   * @param {!xyz.swapee.wc.ITestingDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `Testing` _!TestingMemory_
   * - `This` _!TestingMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TestingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TestingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ITestingDesigner.prototype.constructor = xyz.swapee.wc.ITestingDesigner

/**
 * A concrete class of _ITestingDesigner_ instances.
 * @constructor xyz.swapee.wc.TestingDesigner
 * @implements {xyz.swapee.wc.ITestingDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.TestingDesigner = class extends xyz.swapee.wc.ITestingDesigner { }
xyz.swapee.wc.TestingDesigner.prototype.constructor = xyz.swapee.wc.TestingDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ITestingDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITestingController} Testing
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITestingController} Testing
 * @prop {typeof xyz.swapee.wc.ITestingController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITestingDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.TestingMemory} Testing
 * @prop {!xyz.swapee.wc.TestingMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/190-ITestingService.xml}  99d624fec12d9421dd2770cdb1d81b8f */
/** @typedef {com.changelly.UChangelly.Initialese} xyz.swapee.wc.ITestingService.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingService)} xyz.swapee.wc.AbstractTestingService.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingService} xyz.swapee.wc.TestingService.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingService` interface.
 * @constructor xyz.swapee.wc.AbstractTestingService
 */
xyz.swapee.wc.AbstractTestingService = class extends /** @type {xyz.swapee.wc.AbstractTestingService.constructor&xyz.swapee.wc.TestingService.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingService.prototype.constructor = xyz.swapee.wc.AbstractTestingService
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingService.class = /** @type {typeof xyz.swapee.wc.AbstractTestingService} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingService.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingService}
 */
xyz.swapee.wc.AbstractTestingService.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingService|typeof xyz.swapee.wc.TestingService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.AbstractTestingService.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITestingServiceCaster&com.changelly.UChangelly)} xyz.swapee.wc.ITestingService.constructor */
/** @typedef {typeof com.changelly.UChangelly} com.changelly.UChangelly.typeof */
/**
 * A service for the ITesting.
 * @interface xyz.swapee.wc.ITestingService
 */
xyz.swapee.wc.ITestingService = class extends /** @type {xyz.swapee.wc.ITestingService.constructor&engineering.type.IEngineer.typeof&com.changelly.UChangelly.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer} */
xyz.swapee.wc.ITestingService.prototype.filterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.wc.ITestingService.filterChangellyFixedOffer} */
xyz.swapee.wc.ITestingService.prototype.filterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.wc.ITestingService.filterChangenowOffer} */
xyz.swapee.wc.ITestingService.prototype.filterChangenowOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingService&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingService.Initialese>)} xyz.swapee.wc.TestingService.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingService} xyz.swapee.wc.ITestingService.typeof */
/**
 * A concrete class of _ITestingService_ instances.
 * @constructor xyz.swapee.wc.TestingService
 * @implements {xyz.swapee.wc.ITestingService} A service for the ITesting.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingService.Initialese>} ‎
 */
xyz.swapee.wc.TestingService = class extends /** @type {xyz.swapee.wc.TestingService.constructor&xyz.swapee.wc.ITestingService.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TestingService.prototype.constructor = xyz.swapee.wc.TestingService
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingService}
 */
xyz.swapee.wc.TestingService.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITestingService} */
xyz.swapee.wc.RecordITestingService

/** @typedef {xyz.swapee.wc.ITestingService} xyz.swapee.wc.BoundITestingService */

/** @typedef {xyz.swapee.wc.TestingService} xyz.swapee.wc.BoundTestingService */

/**
 * Contains getters to cast the _ITestingService_ interface.
 * @interface xyz.swapee.wc.ITestingServiceCaster
 */
xyz.swapee.wc.ITestingServiceCaster = class { }
/**
 * Cast the _ITestingService_ instance into the _BoundITestingService_ type.
 * @type {!xyz.swapee.wc.BoundITestingService}
 */
xyz.swapee.wc.ITestingServiceCaster.prototype.asITestingService
/**
 * Access the _TestingService_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingService}
 */
xyz.swapee.wc.ITestingServiceCaster.prototype.superTestingService

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form) => !Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>} xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingService.__filterChangellyFloatingOffer<!xyz.swapee.wc.ITestingService>} xyz.swapee.wc.ITestingService._filterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer} */
/**
 * Loads the _Changelly_ floating offer.
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form} form The form.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return>}
 */
xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer = function(form) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Form The form. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangellyFloatingOffer} xyz.swapee.wc.ITestingService.filterChangellyFloatingOffer.Return */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form) => !Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>} xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingService.__filterChangellyFixedOffer<!xyz.swapee.wc.ITestingService>} xyz.swapee.wc.ITestingService._filterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingService.filterChangellyFixedOffer} */
/**
 * Loads the _Changelly_ fixed offer.
 * @param {!xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form} form The form.
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return>}
 */
xyz.swapee.wc.ITestingService.filterChangellyFixedOffer = function(form) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Form The form. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangellyFixedOffer} xyz.swapee.wc.ITestingService.filterChangellyFixedOffer.Return */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITestingService.filterChangenowOffer.Form) => !Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>} xyz.swapee.wc.ITestingService.__filterChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingService.__filterChangenowOffer<!xyz.swapee.wc.ITestingService>} xyz.swapee.wc.ITestingService._filterChangenowOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingService.filterChangenowOffer} */
/**
 * Loads the _Changenow_ offer.
 * @param {!xyz.swapee.wc.ITestingService.filterChangenowOffer.Form} form The form.
 * - `changeNow` _boolean_ ⤴ *ITestingOuterCore.Model.ChangeNow_Safe*
 * - `amountIn` _number_ ⤴ *ITestingOuterCore.Model.AmountIn_Safe*
 * - `currencyIn` _string_ ⤴ *ITestingOuterCore.Model.CurrencyIn_Safe*
 * - `currencyOut` _string_ ⤴ *ITestingOuterCore.Model.CurrencyOut_Safe*
 * @return {!Promise<xyz.swapee.wc.ITestingService.filterChangenowOffer.Return>}
 */
xyz.swapee.wc.ITestingService.filterChangenowOffer = function(form) {}

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangeNow_Safe&xyz.swapee.wc.ITestingCore.Model.AmountIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyIn_Safe&xyz.swapee.wc.ITestingCore.Model.CurrencyOut_Safe} xyz.swapee.wc.ITestingService.filterChangenowOffer.Form The form. */

/** @typedef {xyz.swapee.wc.ITestingCore.Model.ChangenowOffer} xyz.swapee.wc.ITestingService.filterChangenowOffer.Return */

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingService
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplay.xml}  de9c478d70f4352136a1dc57ca7dcb29 */
/**
 * @typedef {Object} $xyz.swapee.wc.ITestingDisplay.Initialese
 * @prop {HTMLInputElement} [AmountIn]
 * @prop {HTMLButtonElement} [IncreaseBu]
 * @prop {HTMLButtonElement} [DecreaseBu]
 * @prop {HTMLSpanElement} [RatesLoIn]
 * @prop {HTMLSpanElement} [RatesLoEr]
 * @prop {HTMLDivElement} [ChangellyFloatingOffer]
 * @prop {HTMLSpanElement} [ChangellyFloatingOfferAmount]
 * @prop {HTMLDivElement} [ChangellyFixedOffer]
 * @prop {HTMLSpanElement} [ChangellyFixedOfferAmount]
 */
/** @typedef {$xyz.swapee.wc.ITestingDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings>} xyz.swapee.wc.ITestingDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TestingDisplay)} xyz.swapee.wc.AbstractTestingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingDisplay} xyz.swapee.wc.TestingDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractTestingDisplay
 */
xyz.swapee.wc.AbstractTestingDisplay = class extends /** @type {xyz.swapee.wc.AbstractTestingDisplay.constructor&xyz.swapee.wc.TestingDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingDisplay.prototype.constructor = xyz.swapee.wc.AbstractTestingDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractTestingDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.AbstractTestingDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingDisplay.Initialese[]) => xyz.swapee.wc.ITestingDisplay} xyz.swapee.wc.TestingDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITestingDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.TestingMemory, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, xyz.swapee.wc.ITestingDisplay.Queries, null>)} xyz.swapee.wc.ITestingDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ITesting_.
 * @interface xyz.swapee.wc.ITestingDisplay
 */
xyz.swapee.wc.ITestingDisplay = class extends /** @type {xyz.swapee.wc.ITestingDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITestingDisplay.paint} */
xyz.swapee.wc.ITestingDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingDisplay.Initialese>)} xyz.swapee.wc.TestingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingDisplay} xyz.swapee.wc.ITestingDisplay.typeof */
/**
 * A concrete class of _ITestingDisplay_ instances.
 * @constructor xyz.swapee.wc.TestingDisplay
 * @implements {xyz.swapee.wc.ITestingDisplay} Display for presenting information from the _ITesting_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingDisplay.Initialese>} ‎
 */
xyz.swapee.wc.TestingDisplay = class extends /** @type {xyz.swapee.wc.TestingDisplay.constructor&xyz.swapee.wc.ITestingDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingDisplay}
 */
xyz.swapee.wc.TestingDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingDisplay.
 * @interface xyz.swapee.wc.ITestingDisplayFields
 */
xyz.swapee.wc.ITestingDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ITestingDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ITestingDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.AmountIn = /** @type {HTMLInputElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.IncreaseBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.DecreaseBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.RatesLoIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.RatesLoEr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFloatingOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFloatingOfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFixedOffer = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ITestingDisplayFields.prototype.ChangellyFixedOfferAmount = /** @type {HTMLSpanElement} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingDisplay} */
xyz.swapee.wc.RecordITestingDisplay

/** @typedef {xyz.swapee.wc.ITestingDisplay} xyz.swapee.wc.BoundITestingDisplay */

/** @typedef {xyz.swapee.wc.TestingDisplay} xyz.swapee.wc.BoundTestingDisplay */

/** @typedef {xyz.swapee.wc.ITestingDisplay.Queries} xyz.swapee.wc.ITestingDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITestingDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _ITestingDisplay_ interface.
 * @interface xyz.swapee.wc.ITestingDisplayCaster
 */
xyz.swapee.wc.ITestingDisplayCaster = class { }
/**
 * Cast the _ITestingDisplay_ instance into the _BoundITestingDisplay_ type.
 * @type {!xyz.swapee.wc.BoundITestingDisplay}
 */
xyz.swapee.wc.ITestingDisplayCaster.prototype.asITestingDisplay
/**
 * Cast the _ITestingDisplay_ instance into the _BoundITestingScreen_ type.
 * @type {!xyz.swapee.wc.BoundITestingScreen}
 */
xyz.swapee.wc.ITestingDisplayCaster.prototype.asITestingScreen
/**
 * Access the _TestingDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingDisplay}
 */
xyz.swapee.wc.ITestingDisplayCaster.prototype.superTestingDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TestingMemory, land: null) => void} xyz.swapee.wc.ITestingDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingDisplay.__paint<!xyz.swapee.wc.ITestingDisplay>} xyz.swapee.wc.ITestingDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ITestingDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TestingMemory} memory The display data.
 * - `core` _string_ The core property. Default empty string.
 * - `host` _string_ Default empty string.
 * - `amountIn` _number_ Default `0`.
 * - `currencyIn` _string_ Default empty string.
 * - `currencyOut` _string_ Default empty string.
 * - `changellyFixedOffer` _number_ Default `0`.
 * - `changellyFloatingOffer` _number_ Default `0`.
 * - `changenowOffer` _{ amountOut: string, type: string }_ Default `null`.
 * - `anyLoading` _boolean_ Default `false`.
 * - `[changeNow=false]` _boolean?_ Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ITestingDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/40-ITestingDisplayBack.xml}  73086c107a0da1832c11e3ad27aaf1de */
/**
 * @typedef {Object} $xyz.swapee.wc.back.ITestingDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [AmountIn]
 * @prop {!com.webcircuits.IHtmlTwin} [IncreaseBu]
 * @prop {!com.webcircuits.IHtmlTwin} [DecreaseBu]
 * @prop {!com.webcircuits.IHtmlTwin} [RatesLoIn]
 * @prop {!com.webcircuits.IHtmlTwin} [RatesLoEr]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFloatingOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFloatingOfferAmount]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFixedOffer]
 * @prop {!com.webcircuits.IHtmlTwin} [ChangellyFixedOfferAmount]
 */
/** @typedef {$xyz.swapee.wc.back.ITestingDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TestingClasses>} xyz.swapee.wc.back.ITestingDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TestingDisplay)} xyz.swapee.wc.back.AbstractTestingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TestingDisplay} xyz.swapee.wc.back.TestingDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITestingDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractTestingDisplay
 */
xyz.swapee.wc.back.AbstractTestingDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractTestingDisplay.constructor&xyz.swapee.wc.back.TestingDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTestingDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractTestingDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTestingDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractTestingDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.AbstractTestingDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITestingDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.ITestingDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.TestingClasses, null>)} xyz.swapee.wc.back.ITestingDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ITestingDisplay
 */
xyz.swapee.wc.back.ITestingDisplay = class extends /** @type {xyz.swapee.wc.back.ITestingDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ITestingDisplay.paint} */
xyz.swapee.wc.back.ITestingDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ITestingDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingDisplay.Initialese>)} xyz.swapee.wc.back.TestingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITestingDisplay} xyz.swapee.wc.back.ITestingDisplay.typeof */
/**
 * A concrete class of _ITestingDisplay_ instances.
 * @constructor xyz.swapee.wc.back.TestingDisplay
 * @implements {xyz.swapee.wc.back.ITestingDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.TestingDisplay = class extends /** @type {xyz.swapee.wc.back.TestingDisplay.constructor&xyz.swapee.wc.back.ITestingDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.TestingDisplay.prototype.constructor = xyz.swapee.wc.back.TestingDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingDisplay}
 */
xyz.swapee.wc.back.TestingDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingDisplay.
 * @interface xyz.swapee.wc.back.ITestingDisplayFields
 */
xyz.swapee.wc.back.ITestingDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.AmountIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.IncreaseBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.DecreaseBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.RatesLoIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.RatesLoEr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFloatingOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFloatingOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFixedOffer = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ITestingDisplayFields.prototype.ChangellyFixedOfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.ITestingDisplay} */
xyz.swapee.wc.back.RecordITestingDisplay

/** @typedef {xyz.swapee.wc.back.ITestingDisplay} xyz.swapee.wc.back.BoundITestingDisplay */

/** @typedef {xyz.swapee.wc.back.TestingDisplay} xyz.swapee.wc.back.BoundTestingDisplay */

/**
 * Contains getters to cast the _ITestingDisplay_ interface.
 * @interface xyz.swapee.wc.back.ITestingDisplayCaster
 */
xyz.swapee.wc.back.ITestingDisplayCaster = class { }
/**
 * Cast the _ITestingDisplay_ instance into the _BoundITestingDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundITestingDisplay}
 */
xyz.swapee.wc.back.ITestingDisplayCaster.prototype.asITestingDisplay
/**
 * Access the _TestingDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTestingDisplay}
 */
xyz.swapee.wc.back.ITestingDisplayCaster.prototype.superTestingDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.TestingMemory, land?: null) => void} xyz.swapee.wc.back.ITestingDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ITestingDisplay.__paint<!xyz.swapee.wc.back.ITestingDisplay>} xyz.swapee.wc.back.ITestingDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ITestingDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TestingMemory} [memory] The display data.
 * - `core` _string_ The core property. Default empty string.
 * - `host` _string_ Default empty string.
 * - `amountIn` _number_ Default `0`.
 * - `currencyIn` _string_ Default empty string.
 * - `currencyOut` _string_ Default empty string.
 * - `changellyFixedOffer` _number_ Default `0`.
 * - `changellyFloatingOffer` _number_ Default `0`.
 * - `changenowOffer` _{ amountOut: string, type: string }_ Default `null`.
 * - `anyLoading` _boolean_ Default `false`.
 * - `[changeNow=false]` _boolean?_ Default `false`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.ITestingDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ITestingDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/41-TestingClasses.xml}  7a824f2729d6d33681c7fe7eef4cded0 */
/**
 * The classes of the _ITestingDisplay_.
 * @record xyz.swapee.wc.TestingClasses
 */
xyz.swapee.wc.TestingClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.TestingClasses.prototype.props = /** @type {xyz.swapee.wc.TestingClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/50-ITestingController.xml}  30f453fc9e3f19e7cb4f387d5d2f4973 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITestingController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITestingOuterCore.WeakModel>} xyz.swapee.wc.ITestingController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TestingController)} xyz.swapee.wc.AbstractTestingController.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingController} xyz.swapee.wc.TestingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingController` interface.
 * @constructor xyz.swapee.wc.AbstractTestingController
 */
xyz.swapee.wc.AbstractTestingController = class extends /** @type {xyz.swapee.wc.AbstractTestingController.constructor&xyz.swapee.wc.TestingController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingController.prototype.constructor = xyz.swapee.wc.AbstractTestingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingController.class = /** @type {typeof xyz.swapee.wc.AbstractTestingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingController}
 */
xyz.swapee.wc.AbstractTestingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.AbstractTestingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingController.Initialese[]) => xyz.swapee.wc.ITestingController} xyz.swapee.wc.TestingControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITestingControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ITestingOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.ITestingController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITestingController.Inputs, !xyz.swapee.wc.TestingMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITestingController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITestingController.Inputs>)} xyz.swapee.wc.ITestingController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ITestingController
 */
xyz.swapee.wc.ITestingController = class extends /** @type {xyz.swapee.wc.ITestingController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITestingController.resetPort} */
xyz.swapee.wc.ITestingController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ITestingController.increaseAmount} */
xyz.swapee.wc.ITestingController.prototype.increaseAmount = function() {}
/** @type {xyz.swapee.wc.ITestingController.decreaseAmount} */
xyz.swapee.wc.ITestingController.prototype.decreaseAmount = function() {}
/** @type {xyz.swapee.wc.ITestingController.setCore} */
xyz.swapee.wc.ITestingController.prototype.setCore = function() {}
/** @type {xyz.swapee.wc.ITestingController.unsetCore} */
xyz.swapee.wc.ITestingController.prototype.unsetCore = function() {}
/** @type {xyz.swapee.wc.ITestingController.setAmountIn} */
xyz.swapee.wc.ITestingController.prototype.setAmountIn = function() {}
/** @type {xyz.swapee.wc.ITestingController.unsetAmountIn} */
xyz.swapee.wc.ITestingController.prototype.unsetAmountIn = function() {}
/** @type {xyz.swapee.wc.ITestingController.setCurrencyIn} */
xyz.swapee.wc.ITestingController.prototype.setCurrencyIn = function() {}
/** @type {xyz.swapee.wc.ITestingController.unsetCurrencyIn} */
xyz.swapee.wc.ITestingController.prototype.unsetCurrencyIn = function() {}
/** @type {xyz.swapee.wc.ITestingController.setCurrencyOut} */
xyz.swapee.wc.ITestingController.prototype.setCurrencyOut = function() {}
/** @type {xyz.swapee.wc.ITestingController.unsetCurrencyOut} */
xyz.swapee.wc.ITestingController.prototype.unsetCurrencyOut = function() {}
/** @type {xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer} */
xyz.swapee.wc.ITestingController.prototype.loadChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.wc.ITestingController.loadChangellyFixedOffer} */
xyz.swapee.wc.ITestingController.prototype.loadChangellyFixedOffer = function() {}
/** @type {xyz.swapee.wc.ITestingController.loadChangenowOffer} */
xyz.swapee.wc.ITestingController.prototype.loadChangenowOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITestingController&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingController.Initialese>)} xyz.swapee.wc.TestingController.constructor */
/**
 * A concrete class of _ITestingController_ instances.
 * @constructor xyz.swapee.wc.TestingController
 * @implements {xyz.swapee.wc.ITestingController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingController.Initialese>} ‎
 */
xyz.swapee.wc.TestingController = class extends /** @type {xyz.swapee.wc.TestingController.constructor&xyz.swapee.wc.ITestingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingController}
 */
xyz.swapee.wc.TestingController.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingController.
 * @interface xyz.swapee.wc.ITestingControllerFields
 */
xyz.swapee.wc.ITestingControllerFields = class { }
/**
 * The inputs to the _ITesting_'s controller.
 */
xyz.swapee.wc.ITestingControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITestingController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ITestingControllerFields.prototype.props = /** @type {xyz.swapee.wc.ITestingController} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingController} */
xyz.swapee.wc.RecordITestingController

/** @typedef {xyz.swapee.wc.ITestingController} xyz.swapee.wc.BoundITestingController */

/** @typedef {xyz.swapee.wc.TestingController} xyz.swapee.wc.BoundTestingController */

/** @typedef {xyz.swapee.wc.ITestingPort.Inputs} xyz.swapee.wc.ITestingController.Inputs The inputs to the _ITesting_'s controller. */

/** @typedef {xyz.swapee.wc.ITestingPort.WeakInputs} xyz.swapee.wc.ITestingController.WeakInputs The inputs to the _ITesting_'s controller. */

/**
 * Contains getters to cast the _ITestingController_ interface.
 * @interface xyz.swapee.wc.ITestingControllerCaster
 */
xyz.swapee.wc.ITestingControllerCaster = class { }
/**
 * Cast the _ITestingController_ instance into the _BoundITestingController_ type.
 * @type {!xyz.swapee.wc.BoundITestingController}
 */
xyz.swapee.wc.ITestingControllerCaster.prototype.asITestingController
/**
 * Cast the _ITestingController_ instance into the _BoundITestingProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITestingProcessor}
 */
xyz.swapee.wc.ITestingControllerCaster.prototype.asITestingProcessor
/**
 * Access the _TestingController_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingController}
 */
xyz.swapee.wc.ITestingControllerCaster.prototype.superTestingController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__resetPort<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITestingController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.ITestingController.__increaseAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__increaseAmount<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._increaseAmount */
/** @typedef {typeof xyz.swapee.wc.ITestingController.increaseAmount} */
/** @return {?} */
xyz.swapee.wc.ITestingController.increaseAmount = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.ITestingController.__decreaseAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__decreaseAmount<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._decreaseAmount */
/** @typedef {typeof xyz.swapee.wc.ITestingController.decreaseAmount} */
/** @return {?} */
xyz.swapee.wc.ITestingController.decreaseAmount = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ITestingController.__setCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__setCore<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._setCore */
/** @typedef {typeof xyz.swapee.wc.ITestingController.setCore} */
/**
 * Sets the `core` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.setCore = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__unsetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__unsetCore<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._unsetCore */
/** @typedef {typeof xyz.swapee.wc.ITestingController.unsetCore} */
/**
 * Clears the `core` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.unsetCore = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.ITestingController.__setAmountIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__setAmountIn<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._setAmountIn */
/** @typedef {typeof xyz.swapee.wc.ITestingController.setAmountIn} */
/**
 * Sets the `amountIn` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.setAmountIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__unsetAmountIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__unsetAmountIn<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._unsetAmountIn */
/** @typedef {typeof xyz.swapee.wc.ITestingController.unsetAmountIn} */
/**
 * Clears the `amountIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.unsetAmountIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ITestingController.__setCurrencyIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__setCurrencyIn<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._setCurrencyIn */
/** @typedef {typeof xyz.swapee.wc.ITestingController.setCurrencyIn} */
/**
 * Sets the `currencyIn` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.setCurrencyIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__unsetCurrencyIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__unsetCurrencyIn<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._unsetCurrencyIn */
/** @typedef {typeof xyz.swapee.wc.ITestingController.unsetCurrencyIn} */
/**
 * Clears the `currencyIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.unsetCurrencyIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ITestingController.__setCurrencyOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__setCurrencyOut<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._setCurrencyOut */
/** @typedef {typeof xyz.swapee.wc.ITestingController.setCurrencyOut} */
/**
 * Sets the `currencyOut` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.setCurrencyOut = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__unsetCurrencyOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__unsetCurrencyOut<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._unsetCurrencyOut */
/** @typedef {typeof xyz.swapee.wc.ITestingController.unsetCurrencyOut} */
/**
 * Clears the `currencyOut` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.unsetCurrencyOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__loadChangellyFloatingOffer<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._loadChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.loadChangellyFloatingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__loadChangellyFixedOffer<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._loadChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingController.loadChangellyFixedOffer} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.loadChangellyFixedOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITestingController.__loadChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITestingController.__loadChangenowOffer<!xyz.swapee.wc.ITestingController>} xyz.swapee.wc.ITestingController._loadChangenowOffer */
/** @typedef {typeof xyz.swapee.wc.ITestingController.loadChangenowOffer} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.ITestingController.loadChangenowOffer = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITestingController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/51-ITestingControllerFront.xml}  21e94a389e2974b8d3849a9b18275303 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ITestingController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TestingController)} xyz.swapee.wc.front.AbstractTestingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TestingController} xyz.swapee.wc.front.TestingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITestingController` interface.
 * @constructor xyz.swapee.wc.front.AbstractTestingController
 */
xyz.swapee.wc.front.AbstractTestingController = class extends /** @type {xyz.swapee.wc.front.AbstractTestingController.constructor&xyz.swapee.wc.front.TestingController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTestingController.prototype.constructor = xyz.swapee.wc.front.AbstractTestingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTestingController.class = /** @type {typeof xyz.swapee.wc.front.AbstractTestingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.AbstractTestingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITestingController.Initialese[]) => xyz.swapee.wc.front.ITestingController} xyz.swapee.wc.front.TestingControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITestingControllerCaster&xyz.swapee.wc.front.ITestingControllerAT)} xyz.swapee.wc.front.ITestingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITestingControllerAT} xyz.swapee.wc.front.ITestingControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ITestingController
 */
xyz.swapee.wc.front.ITestingController = class extends /** @type {xyz.swapee.wc.front.ITestingController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ITestingControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITestingController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.ITestingController.increaseAmount} */
xyz.swapee.wc.front.ITestingController.prototype.increaseAmount = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.decreaseAmount} */
xyz.swapee.wc.front.ITestingController.prototype.decreaseAmount = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.setCore} */
xyz.swapee.wc.front.ITestingController.prototype.setCore = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.unsetCore} */
xyz.swapee.wc.front.ITestingController.prototype.unsetCore = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.setAmountIn} */
xyz.swapee.wc.front.ITestingController.prototype.setAmountIn = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.unsetAmountIn} */
xyz.swapee.wc.front.ITestingController.prototype.unsetAmountIn = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.setCurrencyIn} */
xyz.swapee.wc.front.ITestingController.prototype.setCurrencyIn = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.unsetCurrencyIn} */
xyz.swapee.wc.front.ITestingController.prototype.unsetCurrencyIn = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.setCurrencyOut} */
xyz.swapee.wc.front.ITestingController.prototype.setCurrencyOut = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.unsetCurrencyOut} */
xyz.swapee.wc.front.ITestingController.prototype.unsetCurrencyOut = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer} */
xyz.swapee.wc.front.ITestingController.prototype.loadChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer} */
xyz.swapee.wc.front.ITestingController.prototype.loadChangellyFixedOffer = function() {}
/** @type {xyz.swapee.wc.front.ITestingController.loadChangenowOffer} */
xyz.swapee.wc.front.ITestingController.prototype.loadChangenowOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.ITestingController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingController.Initialese>)} xyz.swapee.wc.front.TestingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController} xyz.swapee.wc.front.ITestingController.typeof */
/**
 * A concrete class of _ITestingController_ instances.
 * @constructor xyz.swapee.wc.front.TestingController
 * @implements {xyz.swapee.wc.front.ITestingController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingController.Initialese>} ‎
 */
xyz.swapee.wc.front.TestingController = class extends /** @type {xyz.swapee.wc.front.TestingController.constructor&xyz.swapee.wc.front.ITestingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITestingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TestingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingController}
 */
xyz.swapee.wc.front.TestingController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITestingController} */
xyz.swapee.wc.front.RecordITestingController

/** @typedef {xyz.swapee.wc.front.ITestingController} xyz.swapee.wc.front.BoundITestingController */

/** @typedef {xyz.swapee.wc.front.TestingController} xyz.swapee.wc.front.BoundTestingController */

/**
 * Contains getters to cast the _ITestingController_ interface.
 * @interface xyz.swapee.wc.front.ITestingControllerCaster
 */
xyz.swapee.wc.front.ITestingControllerCaster = class { }
/**
 * Cast the _ITestingController_ instance into the _BoundITestingController_ type.
 * @type {!xyz.swapee.wc.front.BoundITestingController}
 */
xyz.swapee.wc.front.ITestingControllerCaster.prototype.asITestingController
/**
 * Access the _TestingController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTestingController}
 */
xyz.swapee.wc.front.ITestingControllerCaster.prototype.superTestingController

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.ITestingController.__increaseAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__increaseAmount<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._increaseAmount */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.increaseAmount} */
/** @return {?} */
xyz.swapee.wc.front.ITestingController.increaseAmount = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.ITestingController.__decreaseAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__decreaseAmount<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._decreaseAmount */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.decreaseAmount} */
/** @return {?} */
xyz.swapee.wc.front.ITestingController.decreaseAmount = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ITestingController.__setCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__setCore<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._setCore */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.setCore} */
/**
 * Sets the `core` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.setCore = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITestingController.__unsetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__unsetCore<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._unsetCore */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.unsetCore} */
/**
 * Clears the `core` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.unsetCore = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.front.ITestingController.__setAmountIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__setAmountIn<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._setAmountIn */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.setAmountIn} */
/**
 * Sets the `amountIn` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.setAmountIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITestingController.__unsetAmountIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__unsetAmountIn<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._unsetAmountIn */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.unsetAmountIn} */
/**
 * Clears the `amountIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.unsetAmountIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ITestingController.__setCurrencyIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__setCurrencyIn<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._setCurrencyIn */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.setCurrencyIn} */
/**
 * Sets the `currencyIn` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.setCurrencyIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__unsetCurrencyIn<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._unsetCurrencyIn */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.unsetCurrencyIn} */
/**
 * Clears the `currencyIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.unsetCurrencyIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ITestingController.__setCurrencyOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__setCurrencyOut<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._setCurrencyOut */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.setCurrencyOut} */
/**
 * Sets the `currencyOut` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.setCurrencyOut = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__unsetCurrencyOut<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._unsetCurrencyOut */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.unsetCurrencyOut} */
/**
 * Clears the `currencyOut` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.unsetCurrencyOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__loadChangellyFloatingOffer<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._loadChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.loadChangellyFloatingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__loadChangellyFixedOffer<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._loadChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.loadChangellyFixedOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITestingController.__loadChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITestingController.__loadChangenowOffer<!xyz.swapee.wc.front.ITestingController>} xyz.swapee.wc.front.ITestingController._loadChangenowOffer */
/** @typedef {typeof xyz.swapee.wc.front.ITestingController.loadChangenowOffer} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.front.ITestingController.loadChangenowOffer = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.ITestingController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/52-ITestingControllerBack.xml}  ad66f4c22c7e206ea30befc91211aa70 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITestingController.Inputs>&xyz.swapee.wc.ITestingController.Initialese} xyz.swapee.wc.back.ITestingController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TestingController)} xyz.swapee.wc.back.AbstractTestingController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TestingController} xyz.swapee.wc.back.TestingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITestingController` interface.
 * @constructor xyz.swapee.wc.back.AbstractTestingController
 */
xyz.swapee.wc.back.AbstractTestingController = class extends /** @type {xyz.swapee.wc.back.AbstractTestingController.constructor&xyz.swapee.wc.back.TestingController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTestingController.prototype.constructor = xyz.swapee.wc.back.AbstractTestingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTestingController.class = /** @type {typeof xyz.swapee.wc.back.AbstractTestingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITestingController|typeof xyz.swapee.wc.back.TestingController)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.AbstractTestingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITestingController.Initialese[]) => xyz.swapee.wc.back.ITestingController} xyz.swapee.wc.back.TestingControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITestingControllerCaster&xyz.swapee.wc.ITestingController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ITestingController.Inputs>)} xyz.swapee.wc.back.ITestingController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ITestingController
 */
xyz.swapee.wc.back.ITestingController = class extends /** @type {xyz.swapee.wc.back.ITestingController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITestingController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITestingController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITestingController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingController.Initialese>)} xyz.swapee.wc.back.TestingController.constructor */
/**
 * A concrete class of _ITestingController_ instances.
 * @constructor xyz.swapee.wc.back.TestingController
 * @implements {xyz.swapee.wc.back.ITestingController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingController.Initialese>} ‎
 */
xyz.swapee.wc.back.TestingController = class extends /** @type {xyz.swapee.wc.back.TestingController.constructor&xyz.swapee.wc.back.ITestingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TestingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingController}
 */
xyz.swapee.wc.back.TestingController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITestingController} */
xyz.swapee.wc.back.RecordITestingController

/** @typedef {xyz.swapee.wc.back.ITestingController} xyz.swapee.wc.back.BoundITestingController */

/** @typedef {xyz.swapee.wc.back.TestingController} xyz.swapee.wc.back.BoundTestingController */

/**
 * Contains getters to cast the _ITestingController_ interface.
 * @interface xyz.swapee.wc.back.ITestingControllerCaster
 */
xyz.swapee.wc.back.ITestingControllerCaster = class { }
/**
 * Cast the _ITestingController_ instance into the _BoundITestingController_ type.
 * @type {!xyz.swapee.wc.back.BoundITestingController}
 */
xyz.swapee.wc.back.ITestingControllerCaster.prototype.asITestingController
/**
 * Access the _TestingController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTestingController}
 */
xyz.swapee.wc.back.ITestingControllerCaster.prototype.superTestingController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/53-ITestingControllerAR.xml}  c07367ba21e2046b21caf793ec8355d5 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITestingController.Initialese} xyz.swapee.wc.back.ITestingControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TestingControllerAR)} xyz.swapee.wc.back.AbstractTestingControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TestingControllerAR} xyz.swapee.wc.back.TestingControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITestingControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractTestingControllerAR
 */
xyz.swapee.wc.back.AbstractTestingControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractTestingControllerAR.constructor&xyz.swapee.wc.back.TestingControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTestingControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractTestingControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractTestingControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITestingControllerAR|typeof xyz.swapee.wc.back.TestingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingController|typeof xyz.swapee.wc.TestingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.AbstractTestingControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITestingControllerAR.Initialese[]) => xyz.swapee.wc.back.ITestingControllerAR} xyz.swapee.wc.back.TestingControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITestingControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITestingController)} xyz.swapee.wc.back.ITestingControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITestingControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ITestingControllerAR
 */
xyz.swapee.wc.back.ITestingControllerAR = class extends /** @type {xyz.swapee.wc.back.ITestingControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITestingController.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITestingControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITestingControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingControllerAR.Initialese>)} xyz.swapee.wc.back.TestingControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITestingControllerAR} xyz.swapee.wc.back.ITestingControllerAR.typeof */
/**
 * A concrete class of _ITestingControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.TestingControllerAR
 * @implements {xyz.swapee.wc.back.ITestingControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ITestingControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.TestingControllerAR = class extends /** @type {xyz.swapee.wc.back.TestingControllerAR.constructor&xyz.swapee.wc.back.ITestingControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TestingControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingControllerAR}
 */
xyz.swapee.wc.back.TestingControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITestingControllerAR} */
xyz.swapee.wc.back.RecordITestingControllerAR

/** @typedef {xyz.swapee.wc.back.ITestingControllerAR} xyz.swapee.wc.back.BoundITestingControllerAR */

/** @typedef {xyz.swapee.wc.back.TestingControllerAR} xyz.swapee.wc.back.BoundTestingControllerAR */

/**
 * Contains getters to cast the _ITestingControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ITestingControllerARCaster
 */
xyz.swapee.wc.back.ITestingControllerARCaster = class { }
/**
 * Cast the _ITestingControllerAR_ instance into the _BoundITestingControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundITestingControllerAR}
 */
xyz.swapee.wc.back.ITestingControllerARCaster.prototype.asITestingControllerAR
/**
 * Access the _TestingControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTestingControllerAR}
 */
xyz.swapee.wc.back.ITestingControllerARCaster.prototype.superTestingControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/54-ITestingControllerAT.xml}  27c4eacb43cc3ed7ba58b3ead27de161 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ITestingControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TestingControllerAT)} xyz.swapee.wc.front.AbstractTestingControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TestingControllerAT} xyz.swapee.wc.front.TestingControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITestingControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractTestingControllerAT
 */
xyz.swapee.wc.front.AbstractTestingControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractTestingControllerAT.constructor&xyz.swapee.wc.front.TestingControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTestingControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractTestingControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractTestingControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITestingControllerAT|typeof xyz.swapee.wc.front.TestingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.AbstractTestingControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITestingControllerAT.Initialese[]) => xyz.swapee.wc.front.ITestingControllerAT} xyz.swapee.wc.front.TestingControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITestingControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ITestingControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITestingControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ITestingControllerAT
 */
xyz.swapee.wc.front.ITestingControllerAT = class extends /** @type {xyz.swapee.wc.front.ITestingControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITestingControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITestingControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingControllerAT.Initialese>)} xyz.swapee.wc.front.TestingControllerAT.constructor */
/**
 * A concrete class of _ITestingControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.TestingControllerAT
 * @implements {xyz.swapee.wc.front.ITestingControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITestingControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.TestingControllerAT = class extends /** @type {xyz.swapee.wc.front.TestingControllerAT.constructor&xyz.swapee.wc.front.ITestingControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITestingControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TestingControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingControllerAT}
 */
xyz.swapee.wc.front.TestingControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITestingControllerAT} */
xyz.swapee.wc.front.RecordITestingControllerAT

/** @typedef {xyz.swapee.wc.front.ITestingControllerAT} xyz.swapee.wc.front.BoundITestingControllerAT */

/** @typedef {xyz.swapee.wc.front.TestingControllerAT} xyz.swapee.wc.front.BoundTestingControllerAT */

/**
 * Contains getters to cast the _ITestingControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ITestingControllerATCaster
 */
xyz.swapee.wc.front.ITestingControllerATCaster = class { }
/**
 * Cast the _ITestingControllerAT_ instance into the _BoundITestingControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundITestingControllerAT}
 */
xyz.swapee.wc.front.ITestingControllerATCaster.prototype.asITestingControllerAT
/**
 * Access the _TestingControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTestingControllerAT}
 */
xyz.swapee.wc.front.ITestingControllerATCaster.prototype.superTestingControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreen.xml}  63a4a20096abf567f1e3978b1011119e */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null>&xyz.swapee.wc.ITestingDisplay.Initialese} xyz.swapee.wc.ITestingScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TestingScreen)} xyz.swapee.wc.AbstractTestingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingScreen} xyz.swapee.wc.TestingScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingScreen` interface.
 * @constructor xyz.swapee.wc.AbstractTestingScreen
 */
xyz.swapee.wc.AbstractTestingScreen = class extends /** @type {xyz.swapee.wc.AbstractTestingScreen.constructor&xyz.swapee.wc.TestingScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingScreen.prototype.constructor = xyz.swapee.wc.AbstractTestingScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingScreen.class = /** @type {typeof xyz.swapee.wc.AbstractTestingScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITestingController|typeof xyz.swapee.wc.front.TestingController)|(!xyz.swapee.wc.ITestingDisplay|typeof xyz.swapee.wc.TestingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.AbstractTestingScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingScreen.Initialese[]) => xyz.swapee.wc.ITestingScreen} xyz.swapee.wc.TestingScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITestingScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.TestingMemory, !xyz.swapee.wc.front.TestingInputs, !HTMLDivElement, !xyz.swapee.wc.ITestingDisplay.Settings, !xyz.swapee.wc.ITestingDisplay.Queries, null, null>&xyz.swapee.wc.front.ITestingController&xyz.swapee.wc.ITestingDisplay)} xyz.swapee.wc.ITestingScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ITestingScreen
 */
xyz.swapee.wc.ITestingScreen = class extends /** @type {xyz.swapee.wc.ITestingScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ITestingController.typeof&xyz.swapee.wc.ITestingDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITestingScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingScreen.Initialese>)} xyz.swapee.wc.TestingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ITestingScreen} xyz.swapee.wc.ITestingScreen.typeof */
/**
 * A concrete class of _ITestingScreen_ instances.
 * @constructor xyz.swapee.wc.TestingScreen
 * @implements {xyz.swapee.wc.ITestingScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingScreen.Initialese>} ‎
 */
xyz.swapee.wc.TestingScreen = class extends /** @type {xyz.swapee.wc.TestingScreen.constructor&xyz.swapee.wc.ITestingScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingScreen}
 */
xyz.swapee.wc.TestingScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITestingScreen} */
xyz.swapee.wc.RecordITestingScreen

/** @typedef {xyz.swapee.wc.ITestingScreen} xyz.swapee.wc.BoundITestingScreen */

/** @typedef {xyz.swapee.wc.TestingScreen} xyz.swapee.wc.BoundTestingScreen */

/**
 * Contains getters to cast the _ITestingScreen_ interface.
 * @interface xyz.swapee.wc.ITestingScreenCaster
 */
xyz.swapee.wc.ITestingScreenCaster = class { }
/**
 * Cast the _ITestingScreen_ instance into the _BoundITestingScreen_ type.
 * @type {!xyz.swapee.wc.BoundITestingScreen}
 */
xyz.swapee.wc.ITestingScreenCaster.prototype.asITestingScreen
/**
 * Access the _TestingScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingScreen}
 */
xyz.swapee.wc.ITestingScreenCaster.prototype.superTestingScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/70-ITestingScreenBack.xml}  ad0d1e9f036cfe43b6812e95808fd9c8 */
/** @typedef {xyz.swapee.wc.back.ITestingScreenAT.Initialese} xyz.swapee.wc.back.ITestingScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TestingScreen)} xyz.swapee.wc.back.AbstractTestingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TestingScreen} xyz.swapee.wc.back.TestingScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITestingScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractTestingScreen
 */
xyz.swapee.wc.back.AbstractTestingScreen = class extends /** @type {xyz.swapee.wc.back.AbstractTestingScreen.constructor&xyz.swapee.wc.back.TestingScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTestingScreen.prototype.constructor = xyz.swapee.wc.back.AbstractTestingScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTestingScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractTestingScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITestingScreen|typeof xyz.swapee.wc.back.TestingScreen)|(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.AbstractTestingScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITestingScreen.Initialese[]) => xyz.swapee.wc.back.ITestingScreen} xyz.swapee.wc.back.TestingScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITestingScreenCaster&xyz.swapee.wc.back.ITestingScreenAT)} xyz.swapee.wc.back.ITestingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITestingScreenAT} xyz.swapee.wc.back.ITestingScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ITestingScreen
 */
xyz.swapee.wc.back.ITestingScreen = class extends /** @type {xyz.swapee.wc.back.ITestingScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITestingScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITestingScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITestingScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreen.Initialese>)} xyz.swapee.wc.back.TestingScreen.constructor */
/**
 * A concrete class of _ITestingScreen_ instances.
 * @constructor xyz.swapee.wc.back.TestingScreen
 * @implements {xyz.swapee.wc.back.ITestingScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.TestingScreen = class extends /** @type {xyz.swapee.wc.back.TestingScreen.constructor&xyz.swapee.wc.back.ITestingScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TestingScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreen}
 */
xyz.swapee.wc.back.TestingScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITestingScreen} */
xyz.swapee.wc.back.RecordITestingScreen

/** @typedef {xyz.swapee.wc.back.ITestingScreen} xyz.swapee.wc.back.BoundITestingScreen */

/** @typedef {xyz.swapee.wc.back.TestingScreen} xyz.swapee.wc.back.BoundTestingScreen */

/**
 * Contains getters to cast the _ITestingScreen_ interface.
 * @interface xyz.swapee.wc.back.ITestingScreenCaster
 */
xyz.swapee.wc.back.ITestingScreenCaster = class { }
/**
 * Cast the _ITestingScreen_ instance into the _BoundITestingScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundITestingScreen}
 */
xyz.swapee.wc.back.ITestingScreenCaster.prototype.asITestingScreen
/**
 * Access the _TestingScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTestingScreen}
 */
xyz.swapee.wc.back.ITestingScreenCaster.prototype.superTestingScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/73-ITestingScreenAR.xml}  5ac8bd34d62efd74e59eb68510b5e450 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITestingScreen.Initialese} xyz.swapee.wc.front.ITestingScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TestingScreenAR)} xyz.swapee.wc.front.AbstractTestingScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TestingScreenAR} xyz.swapee.wc.front.TestingScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITestingScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractTestingScreenAR
 */
xyz.swapee.wc.front.AbstractTestingScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractTestingScreenAR.constructor&xyz.swapee.wc.front.TestingScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTestingScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractTestingScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractTestingScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITestingScreenAR|typeof xyz.swapee.wc.front.TestingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITestingScreen|typeof xyz.swapee.wc.TestingScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.AbstractTestingScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITestingScreenAR.Initialese[]) => xyz.swapee.wc.front.ITestingScreenAR} xyz.swapee.wc.front.TestingScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITestingScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITestingScreen)} xyz.swapee.wc.front.ITestingScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITestingScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ITestingScreenAR
 */
xyz.swapee.wc.front.ITestingScreenAR = class extends /** @type {xyz.swapee.wc.front.ITestingScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITestingScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITestingScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITestingScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingScreenAR.Initialese>)} xyz.swapee.wc.front.TestingScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITestingScreenAR} xyz.swapee.wc.front.ITestingScreenAR.typeof */
/**
 * A concrete class of _ITestingScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.TestingScreenAR
 * @implements {xyz.swapee.wc.front.ITestingScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ITestingScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITestingScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.TestingScreenAR = class extends /** @type {xyz.swapee.wc.front.TestingScreenAR.constructor&xyz.swapee.wc.front.ITestingScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITestingScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TestingScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TestingScreenAR}
 */
xyz.swapee.wc.front.TestingScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITestingScreenAR} */
xyz.swapee.wc.front.RecordITestingScreenAR

/** @typedef {xyz.swapee.wc.front.ITestingScreenAR} xyz.swapee.wc.front.BoundITestingScreenAR */

/** @typedef {xyz.swapee.wc.front.TestingScreenAR} xyz.swapee.wc.front.BoundTestingScreenAR */

/**
 * Contains getters to cast the _ITestingScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ITestingScreenARCaster
 */
xyz.swapee.wc.front.ITestingScreenARCaster = class { }
/**
 * Cast the _ITestingScreenAR_ instance into the _BoundITestingScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundITestingScreenAR}
 */
xyz.swapee.wc.front.ITestingScreenARCaster.prototype.asITestingScreenAR
/**
 * Access the _TestingScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTestingScreenAR}
 */
xyz.swapee.wc.front.ITestingScreenARCaster.prototype.superTestingScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/74-ITestingScreenAT.xml}  b532d0e0809c65f97d3d5d500feea001 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ITestingScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TestingScreenAT)} xyz.swapee.wc.back.AbstractTestingScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TestingScreenAT} xyz.swapee.wc.back.TestingScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITestingScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractTestingScreenAT
 */
xyz.swapee.wc.back.AbstractTestingScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractTestingScreenAT.constructor&xyz.swapee.wc.back.TestingScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTestingScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractTestingScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractTestingScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITestingScreenAT|typeof xyz.swapee.wc.back.TestingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.AbstractTestingScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITestingScreenAT.Initialese[]) => xyz.swapee.wc.back.ITestingScreenAT} xyz.swapee.wc.back.TestingScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITestingScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ITestingScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITestingScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ITestingScreenAT
 */
xyz.swapee.wc.back.ITestingScreenAT = class extends /** @type {xyz.swapee.wc.back.ITestingScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITestingScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITestingScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreenAT.Initialese>)} xyz.swapee.wc.back.TestingScreenAT.constructor */
/**
 * A concrete class of _ITestingScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.TestingScreenAT
 * @implements {xyz.swapee.wc.back.ITestingScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITestingScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITestingScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.TestingScreenAT = class extends /** @type {xyz.swapee.wc.back.TestingScreenAT.constructor&xyz.swapee.wc.back.ITestingScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITestingScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TestingScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TestingScreenAT}
 */
xyz.swapee.wc.back.TestingScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITestingScreenAT} */
xyz.swapee.wc.back.RecordITestingScreenAT

/** @typedef {xyz.swapee.wc.back.ITestingScreenAT} xyz.swapee.wc.back.BoundITestingScreenAT */

/** @typedef {xyz.swapee.wc.back.TestingScreenAT} xyz.swapee.wc.back.BoundTestingScreenAT */

/**
 * Contains getters to cast the _ITestingScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ITestingScreenATCaster
 */
xyz.swapee.wc.back.ITestingScreenATCaster = class { }
/**
 * Cast the _ITestingScreenAT_ instance into the _BoundITestingScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundITestingScreenAT}
 */
xyz.swapee.wc.back.ITestingScreenATCaster.prototype.asITestingScreenAT
/**
 * Access the _TestingScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTestingScreenAT}
 */
xyz.swapee.wc.back.ITestingScreenATCaster.prototype.superTestingScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/testing/Testing.mvc/design/80-ITestingGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ITestingDisplay.Initialese} xyz.swapee.wc.ITestingGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TestingGPU)} xyz.swapee.wc.AbstractTestingGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.TestingGPU} xyz.swapee.wc.TestingGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITestingGPU` interface.
 * @constructor xyz.swapee.wc.AbstractTestingGPU
 */
xyz.swapee.wc.AbstractTestingGPU = class extends /** @type {xyz.swapee.wc.AbstractTestingGPU.constructor&xyz.swapee.wc.TestingGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTestingGPU.prototype.constructor = xyz.swapee.wc.AbstractTestingGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTestingGPU.class = /** @type {typeof xyz.swapee.wc.AbstractTestingGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTestingGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITestingGPU|typeof xyz.swapee.wc.TestingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITestingDisplay|typeof xyz.swapee.wc.back.TestingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.AbstractTestingGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITestingGPU.Initialese[]) => xyz.swapee.wc.ITestingGPU} xyz.swapee.wc.TestingGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITestingGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ITestingGPUCaster&com.webcircuits.IBrowserView<.!TestingMemory,>&xyz.swapee.wc.back.ITestingDisplay)} xyz.swapee.wc.ITestingGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!TestingMemory,>} com.webcircuits.IBrowserView<.!TestingMemory,>.typeof */
/**
 * Handles the periphery of the _ITestingDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ITestingGPU
 */
xyz.swapee.wc.ITestingGPU = class extends /** @type {xyz.swapee.wc.ITestingGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!TestingMemory,>.typeof&xyz.swapee.wc.back.ITestingDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITestingGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITestingGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ITestingGPU.Initialese>)} xyz.swapee.wc.TestingGPU.constructor */
/**
 * A concrete class of _ITestingGPU_ instances.
 * @constructor xyz.swapee.wc.TestingGPU
 * @implements {xyz.swapee.wc.ITestingGPU} Handles the periphery of the _ITestingDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITestingGPU.Initialese>} ‎
 */
xyz.swapee.wc.TestingGPU = class extends /** @type {xyz.swapee.wc.TestingGPU.constructor&xyz.swapee.wc.ITestingGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITestingGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TestingGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TestingGPU}
 */
xyz.swapee.wc.TestingGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingGPU.
 * @interface xyz.swapee.wc.ITestingGPUFields
 */
xyz.swapee.wc.ITestingGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ITestingGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ITestingGPU} */
xyz.swapee.wc.RecordITestingGPU

/** @typedef {xyz.swapee.wc.ITestingGPU} xyz.swapee.wc.BoundITestingGPU */

/** @typedef {xyz.swapee.wc.TestingGPU} xyz.swapee.wc.BoundTestingGPU */

/**
 * Contains getters to cast the _ITestingGPU_ interface.
 * @interface xyz.swapee.wc.ITestingGPUCaster
 */
xyz.swapee.wc.ITestingGPUCaster = class { }
/**
 * Cast the _ITestingGPU_ instance into the _BoundITestingGPU_ type.
 * @type {!xyz.swapee.wc.BoundITestingGPU}
 */
xyz.swapee.wc.ITestingGPUCaster.prototype.asITestingGPU
/**
 * Access the _TestingGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundTestingGPU}
 */
xyz.swapee.wc.ITestingGPUCaster.prototype.superTestingGPU

// nss:xyz.swapee.wc
/* @typal-end */