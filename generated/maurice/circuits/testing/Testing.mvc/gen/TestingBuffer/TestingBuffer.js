import {makeBuffers} from '@webcircuits/webcircuits'

export const TestingBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  core:String,
  host:String,
  amountIn:Number,
  currencyIn:String,
  currencyOut:String,
  changeNow:Boolean,
 }),
})

export default TestingBuffer