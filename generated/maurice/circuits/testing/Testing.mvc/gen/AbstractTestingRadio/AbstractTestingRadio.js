import {preadaptLoadChangellyFloatingOffer,preadaptLoadChangellyFixedOffer,preadaptLoadChangenowOffer} from './preadapters'
import {makeLoaders} from '@webcircuits/webcircuits'
import {StatefulLoader} from '@mauriceguest/guest2'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingRadio}
 */
function __AbstractTestingRadio() {}
__AbstractTestingRadio.prototype = /** @type {!_AbstractTestingRadio} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingRadio}
 */
class _AbstractTestingRadio { }
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @extends {xyz.swapee.wc.AbstractTestingRadio} ‎
 */
class AbstractTestingRadio extends newAbstract(
 _AbstractTestingRadio,'ITestingRadio',null,{
  asITestingRadio:1,
  superTestingRadio:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingRadio} */
AbstractTestingRadio.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingRadio} */
function AbstractTestingRadioClass(){}

export default AbstractTestingRadio


AbstractTestingRadio[$implementations]=[
 __AbstractTestingRadio,
 makeLoaders(6269653044,{
  adaptLoadChangellyFloatingOffer:[
   '9ec7d',
   {
    amountIn:'88da7',
    currencyIn:'f3088',
    currencyOut:'98dbb',
   },
   {
    changellyFloatingOffer:'2a73d',
   },
   {
    loadingChangellyFloatingOffer:1,
    loadChangellyFloatingOfferError:2,
   },
  ],
  adaptLoadChangellyFixedOffer:[
   '542a9',
   {
    amountIn:'88da7',
    currencyIn:'f3088',
    currencyOut:'98dbb',
   },
   {
    changellyFixedOffer:'7b5f4',
   },
   {
    loadingChangellyFixedOffer:1,
    loadChangellyFixedOfferError:2,
   },
  ],
  adaptLoadChangenowOffer:[
   '99a35',
   {
    changeNow:'94af8',
    amountIn:'88da7',
    currencyIn:'f3088',
    currencyOut:'98dbb',
   },
   {
    changenowOffer:'dc0e8',
   },
   {
    loadingChangenowOffer:1,
    loadChangenowOfferError:2,
   },
  ],
 }),
 StatefulLoader,
 {adapt:[preadaptLoadChangellyFloatingOffer,preadaptLoadChangellyFixedOffer,preadaptLoadChangenowOffer]},
]