
/**@this {xyz.swapee.wc.ITestingRadio}*/
export function preadaptLoadChangellyFloatingOffer(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFloatingOffer.Form}*/
 const _inputs={
  host:inputs.host,
  amountIn:inputs.amountIn,
  currencyIn:inputs.currencyIn,
  currencyOut:inputs.currencyOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountIn)||(!__inputs.currencyIn)||(!__inputs.currencyOut)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadChangellyFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ITestingRadio}*/
export function preadaptLoadChangellyFixedOffer(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITestingRadio.adaptLoadChangellyFixedOffer.Form}*/
 const _inputs={
  host:inputs.host,
  amountIn:inputs.amountIn,
  currencyIn:inputs.currencyIn,
  currencyOut:inputs.currencyOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountIn)||(!__inputs.currencyIn)||(!__inputs.currencyOut)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadChangellyFixedOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ITestingRadio}*/
export function preadaptLoadChangenowOffer(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITestingRadio.adaptLoadChangenowOffer.Form}*/
 const _inputs={
  host:inputs.host,
  changeNow:inputs.changeNow,
  amountIn:inputs.amountIn,
  currencyIn:inputs.currencyIn,
  currencyOut:inputs.currencyOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.changeNow)||(!__inputs.amountIn)||(!__inputs.currencyIn)||(!__inputs.currencyOut)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadChangenowOffer(__inputs,__changes)
 return RET
}