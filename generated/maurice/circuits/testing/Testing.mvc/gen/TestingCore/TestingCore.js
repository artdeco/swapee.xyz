import {mountPins} from '@webcircuits/webcircuits'
import {TestingMemoryPQs} from '../../pqs/TestingMemoryPQs'
import {TestingCachePQs} from '../../pqs/TestingCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingCore}
 */
function __TestingCore() {}
__TestingCore.prototype = /** @type {!_TestingCore} */ ({ })
/** @this {xyz.swapee.wc.TestingCore} */ function TestingCoreConstructor() {
  /**@type {!xyz.swapee.wc.ITestingCore.Model}*/
  this.model={
    changellyFixedOffer: 0,
    changellyFloatingOffer: 0,
    changenowOffer: null,
    anyLoading: false,
    loadingChangellyFloatingOffer: false,
    hasMoreChangellyFloatingOffer: null,
    loadChangellyFloatingOfferError: null,
    loadingChangellyFixedOffer: false,
    hasMoreChangellyFixedOffer: null,
    loadChangellyFixedOfferError: null,
    loadingChangenowOffer: false,
    hasMoreChangenowOffer: null,
    loadChangenowOfferError: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingCore}
 */
class _TestingCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractTestingCore} ‎
 */
class TestingCore extends newAbstract(
 _TestingCore,'ITestingCore',TestingCoreConstructor,{
  asITestingCore:1,
  superTestingCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingCore} */
TestingCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingCore} */
function TestingCoreClass(){}

export default TestingCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingOuterCore}
 */
function __TestingOuterCore() {}
__TestingOuterCore.prototype = /** @type {!_TestingOuterCore} */ ({ })
/** @this {xyz.swapee.wc.TestingOuterCore} */
export function TestingOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ITestingOuterCore.Model}*/
  this.model={
    core: '',
    host: '',
    amountIn: 0.1,
    currencyIn: 'btc',
    currencyOut: 'eth',
    changeNow: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingOuterCore}
 */
class _TestingOuterCore { }
/**
 * The _ITesting_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractTestingOuterCore} ‎
 */
export class TestingOuterCore extends newAbstract(
 _TestingOuterCore,'ITestingOuterCore',TestingOuterCoreConstructor,{
  asITestingOuterCore:1,
  superTestingOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingOuterCore} */
TestingOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingOuterCore} */
function TestingOuterCoreClass(){}


TestingOuterCore[$implementations]=[
 __TestingOuterCore,
 TestingOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ITestingOuterCore}*/({
  constructor(){
   mountPins(this.model,'',TestingMemoryPQs)
   mountPins(this.model,'',TestingCachePQs)
  },
 }),
]

TestingCore[$implementations]=[
 TestingCoreClass.prototype=/**@type {!xyz.swapee.wc.ITestingCore}*/({
  resetCore(){
   this.resetTestingCore()
  },
  resetTestingCore(){
   TestingCoreConstructor.call(
    /**@type {xyz.swapee.wc.TestingCore}*/(this),
   )
   TestingOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.TestingOuterCore}*/(
     /**@type {!xyz.swapee.wc.ITestingOuterCore}*/(this)),
   )
  },
 }),
 __TestingCore,
 TestingOuterCore,
]

export {TestingCore}