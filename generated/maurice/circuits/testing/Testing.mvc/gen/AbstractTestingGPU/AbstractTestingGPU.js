import AbstractTestingDisplay from '../AbstractTestingDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {TestingVdusPQs} from '../../pqs/TestingVdusPQs'
import {TestingVdusQPs} from '../../pqs/TestingVdusQPs'
import {TestingMemoryPQs} from '../../pqs/TestingMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingGPU}
 */
function __AbstractTestingGPU() {}
__AbstractTestingGPU.prototype = /** @type {!_AbstractTestingGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingGPU}
 */
class _AbstractTestingGPU { }
/**
 * Handles the periphery of the _ITestingDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractTestingGPU} ‎
 */
class AbstractTestingGPU extends newAbstract(
 _AbstractTestingGPU,'ITestingGPU',null,{
  asITestingGPU:1,
  superTestingGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingGPU} */
AbstractTestingGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingGPU} */
function AbstractTestingGPUClass(){}

export default AbstractTestingGPU


AbstractTestingGPU[$implementations]=[
 __AbstractTestingGPU,
 AbstractTestingGPUClass.prototype=/**@type {!xyz.swapee.wc.ITestingGPU}*/({
  vdusPQs:TestingVdusPQs,
  vdusQPs:TestingVdusQPs,
  memoryPQs:TestingMemoryPQs,
 }),
 AbstractTestingDisplay,
 BrowserView,
]