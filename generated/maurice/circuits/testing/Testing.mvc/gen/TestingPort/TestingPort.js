import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {TestingInputsPQs} from '../../pqs/TestingInputsPQs'
import {TestingOuterCoreConstructor} from '../TestingCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingPort}
 */
function __TestingPort() {}
__TestingPort.prototype = /** @type {!_TestingPort} */ ({ })
/** @this {xyz.swapee.wc.TestingPort} */ function TestingPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.TestingOuterCore} */ ({model:null})
  TestingOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingPort}
 */
class _TestingPort { }
/**
 * The port that serves as an interface to the _ITesting_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractTestingPort} ‎
 */
export class TestingPort extends newAbstract(
 _TestingPort,'ITestingPort',TestingPortConstructor,{
  asITestingPort:1,
  superTestingPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingPort} */
TestingPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingPort} */
function TestingPortClass(){}

export const TestingPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ITesting.Pinout>}*/({
 get Port() { return TestingPort },
})

TestingPort[$implementations]=[
 TestingPortClass.prototype=/**@type {!xyz.swapee.wc.ITestingPort}*/({
  resetPort(){
   this.resetTestingPort()
  },
  resetTestingPort(){
   TestingPortConstructor.call(this)
  },
 }),
 __TestingPort,
 Parametric,
 TestingPortClass.prototype=/**@type {!xyz.swapee.wc.ITestingPort}*/({
  constructor(){
   mountPins(this.inputs,'',TestingInputsPQs)
  },
 }),
]


export default TestingPort