import AbstractTestingScreenAT from '../AbstractTestingScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingScreenBack}
 */
function __AbstractTestingScreenBack() {}
__AbstractTestingScreenBack.prototype = /** @type {!_AbstractTestingScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTestingScreen}
 */
class _AbstractTestingScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractTestingScreen} ‎
 */
class AbstractTestingScreenBack extends newAbstract(
 _AbstractTestingScreenBack,'ITestingScreen',null,{
  asITestingScreen:1,
  superTestingScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTestingScreen} */
AbstractTestingScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTestingScreen} */
function AbstractTestingScreenBackClass(){}

export default AbstractTestingScreenBack


AbstractTestingScreenBack[$implementations]=[
 __AbstractTestingScreenBack,
 AbstractTestingScreenAT,
]