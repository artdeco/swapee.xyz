import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingComputer}
 */
function __AbstractTestingComputer() {}
__AbstractTestingComputer.prototype = /** @type {!_AbstractTestingComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingComputer}
 */
class _AbstractTestingComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractTestingComputer} ‎
 */
export class AbstractTestingComputer extends newAbstract(
 _AbstractTestingComputer,'ITestingComputer',null,{
  asITestingComputer:1,
  superTestingComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingComputer} */
AbstractTestingComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingComputer} */
function AbstractTestingComputerClass(){}


AbstractTestingComputer[$implementations]=[
 __AbstractTestingComputer,
 Adapter,
]


export default AbstractTestingComputer