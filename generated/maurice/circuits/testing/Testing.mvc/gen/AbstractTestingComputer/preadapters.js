
/**@this {xyz.swapee.wc.ITestingComputer}*/
export function preadaptChangellyFloatingOffer(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITestingComputer.adaptChangellyFloatingOffer.Form}*/
 const _inputs={
  amountIn:inputs.amountIn,
  currencyIn:inputs.currencyIn,
  currencyOut:inputs.currencyOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountIn)||(!__inputs.currencyIn)||(!__inputs.currencyOut)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangellyFloatingOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ITestingComputer}*/
export function preadaptChangellyFixedOffer(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITestingComputer.adaptChangellyFixedOffer.Form}*/
 const _inputs={
  amountIn:inputs.amountIn,
  currencyIn:inputs.currencyIn,
  currencyOut:inputs.currencyOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountIn)||(!__inputs.currencyIn)||(!__inputs.currencyOut)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangellyFixedOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ITestingComputer}*/
export function preadaptChangenowOffer(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITestingComputer.adaptChangenowOffer.Form}*/
 const _inputs={
  changeNow:inputs.changeNow,
  amountIn:inputs.amountIn,
  currencyIn:inputs.currencyIn,
  currencyOut:inputs.currencyOut,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.changeNow)||(!__inputs.amountIn)||(!__inputs.currencyIn)||(!__inputs.currencyOut)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptChangenowOffer(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ITestingComputer}*/
export function preadaptAnyLoading(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITestingComputer.adaptAnyLoading.Form}*/
 const _inputs={
  loadingChangellyFloatingOffer:inputs.loadingChangellyFloatingOffer,
  loadingChangenowOffer:inputs.loadingChangenowOffer,
  loadingChangellyFixedOffer:inputs.loadingChangellyFixedOffer,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptAnyLoading(__inputs,__changes)
 return RET
}