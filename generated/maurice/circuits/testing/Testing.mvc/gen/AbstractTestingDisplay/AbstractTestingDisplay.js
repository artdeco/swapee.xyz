import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingDisplay}
 */
function __AbstractTestingDisplay() {}
__AbstractTestingDisplay.prototype = /** @type {!_AbstractTestingDisplay} */ ({ })
/** @this {xyz.swapee.wc.TestingDisplay} */ function TestingDisplayConstructor() {
  /** @type {HTMLInputElement} */ this.AmountIn=null
  /** @type {HTMLButtonElement} */ this.IncreaseBu=null
  /** @type {HTMLButtonElement} */ this.DecreaseBu=null
  /** @type {HTMLSpanElement} */ this.RatesLoIn=null
  /** @type {HTMLSpanElement} */ this.RatesLoEr=null
  /** @type {HTMLDivElement} */ this.ChangellyFloatingOffer=null
  /** @type {HTMLSpanElement} */ this.ChangellyFloatingOfferAmount=null
  /** @type {HTMLDivElement} */ this.ChangellyFixedOffer=null
  /** @type {HTMLSpanElement} */ this.ChangellyFixedOfferAmount=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingDisplay}
 */
class _AbstractTestingDisplay { }
/**
 * Display for presenting information from the _ITesting_.
 * @extends {xyz.swapee.wc.AbstractTestingDisplay} ‎
 */
class AbstractTestingDisplay extends newAbstract(
 _AbstractTestingDisplay,'ITestingDisplay',TestingDisplayConstructor,{
  asITestingDisplay:1,
  superTestingDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingDisplay} */
AbstractTestingDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingDisplay} */
function AbstractTestingDisplayClass(){}

export default AbstractTestingDisplay


AbstractTestingDisplay[$implementations]=[
 __AbstractTestingDisplay,
 Display,
 AbstractTestingDisplayClass.prototype=/**@type {!xyz.swapee.wc.ITestingDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>this.scan())
  },
  scan:function vduScan(){
   const{element:element,asITestingScreen:{vdusPQs:{
    RatesLoIn:RatesLoIn,
    ChangellyFloatingOffer:ChangellyFloatingOffer,
    ChangellyFloatingOfferAmount:ChangellyFloatingOfferAmount,
    ChangellyFixedOffer:ChangellyFixedOffer,
    ChangellyFixedOfferAmount:ChangellyFixedOfferAmount,
    AmountIn:AmountIn,
    IncreaseBu:IncreaseBu,
    DecreaseBu:DecreaseBu,
    RatesLoEr:RatesLoEr,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    RatesLoIn:/**@type {HTMLSpanElement}*/(children[RatesLoIn]),
    ChangellyFloatingOffer:/**@type {HTMLDivElement}*/(children[ChangellyFloatingOffer]),
    ChangellyFloatingOfferAmount:/**@type {HTMLSpanElement}*/(children[ChangellyFloatingOfferAmount]),
    ChangellyFixedOffer:/**@type {HTMLDivElement}*/(children[ChangellyFixedOffer]),
    ChangellyFixedOfferAmount:/**@type {HTMLSpanElement}*/(children[ChangellyFixedOfferAmount]),
    AmountIn:/**@type {HTMLInputElement}*/(children[AmountIn]),
    IncreaseBu:/**@type {HTMLButtonElement}*/(children[IncreaseBu]),
    DecreaseBu:/**@type {HTMLButtonElement}*/(children[DecreaseBu]),
    RatesLoEr:/**@type {HTMLSpanElement}*/(children[RatesLoEr]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.TestingDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.ITestingDisplay.Initialese}*/({
   AmountIn:1,
   IncreaseBu:1,
   DecreaseBu:1,
   RatesLoIn:1,
   RatesLoEr:1,
   ChangellyFloatingOffer:1,
   ChangellyFloatingOfferAmount:1,
   ChangellyFixedOffer:1,
   ChangellyFixedOfferAmount:1,
  }),
  initializer({
   AmountIn:_AmountIn,
   IncreaseBu:_IncreaseBu,
   DecreaseBu:_DecreaseBu,
   RatesLoIn:_RatesLoIn,
   RatesLoEr:_RatesLoEr,
   ChangellyFloatingOffer:_ChangellyFloatingOffer,
   ChangellyFloatingOfferAmount:_ChangellyFloatingOfferAmount,
   ChangellyFixedOffer:_ChangellyFixedOffer,
   ChangellyFixedOfferAmount:_ChangellyFixedOfferAmount,
  }) {
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_IncreaseBu!==undefined) this.IncreaseBu=_IncreaseBu
   if(_DecreaseBu!==undefined) this.DecreaseBu=_DecreaseBu
   if(_RatesLoIn!==undefined) this.RatesLoIn=_RatesLoIn
   if(_RatesLoEr!==undefined) this.RatesLoEr=_RatesLoEr
   if(_ChangellyFloatingOffer!==undefined) this.ChangellyFloatingOffer=_ChangellyFloatingOffer
   if(_ChangellyFloatingOfferAmount!==undefined) this.ChangellyFloatingOfferAmount=_ChangellyFloatingOfferAmount
   if(_ChangellyFixedOffer!==undefined) this.ChangellyFixedOffer=_ChangellyFixedOffer
   if(_ChangellyFixedOfferAmount!==undefined) this.ChangellyFixedOfferAmount=_ChangellyFixedOfferAmount
  },
 }),
]