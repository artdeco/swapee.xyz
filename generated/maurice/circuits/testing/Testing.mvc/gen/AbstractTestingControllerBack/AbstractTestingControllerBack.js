import AbstractTestingControllerAR from '../AbstractTestingControllerAR'
import {AbstractTestingController} from '../AbstractTestingController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingControllerBack}
 */
function __AbstractTestingControllerBack() {}
__AbstractTestingControllerBack.prototype = /** @type {!_AbstractTestingControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTestingController}
 */
class _AbstractTestingControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractTestingController} ‎
 */
class AbstractTestingControllerBack extends newAbstract(
 _AbstractTestingControllerBack,'ITestingController',null,{
  asITestingController:1,
  superTestingController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTestingController} */
AbstractTestingControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTestingController} */
function AbstractTestingControllerBackClass(){}

export default AbstractTestingControllerBack


AbstractTestingControllerBack[$implementations]=[
 __AbstractTestingControllerBack,
 AbstractTestingController,
 AbstractTestingControllerAR,
 DriverBack,
]