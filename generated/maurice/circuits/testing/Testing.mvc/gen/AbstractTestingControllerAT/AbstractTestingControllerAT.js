import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingControllerAT}
 */
function __AbstractTestingControllerAT() {}
__AbstractTestingControllerAT.prototype = /** @type {!_AbstractTestingControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTestingControllerAT}
 */
class _AbstractTestingControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITestingControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractTestingControllerAT} ‎
 */
class AbstractTestingControllerAT extends newAbstract(
 _AbstractTestingControllerAT,'ITestingControllerAT',null,{
  asITestingControllerAT:1,
  superTestingControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTestingControllerAT} */
AbstractTestingControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTestingControllerAT} */
function AbstractTestingControllerATClass(){}

export default AbstractTestingControllerAT


AbstractTestingControllerAT[$implementations]=[
 __AbstractTestingControllerAT,
 UartUniversal,
 AbstractTestingControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ITestingControllerAT}*/({
  get asITestingController(){
   return this
  },
  increaseAmount(){
   this.uart.t("inv",{mid:'9540f'})
  },
  decreaseAmount(){
   this.uart.t("inv",{mid:'0e9bb'})
  },
  setCore(){
   this.uart.t("inv",{mid:'9d9b6',args:[...arguments]})
  },
  unsetCore(){
   this.uart.t("inv",{mid:'d22f7'})
  },
  setAmountIn(){
   this.uart.t("inv",{mid:'f1723',args:[...arguments]})
  },
  unsetAmountIn(){
   this.uart.t("inv",{mid:'be5b1'})
  },
  setCurrencyIn(){
   this.uart.t("inv",{mid:'7334d',args:[...arguments]})
  },
  unsetCurrencyIn(){
   this.uart.t("inv",{mid:'b0735'})
  },
  setCurrencyOut(){
   this.uart.t("inv",{mid:'3a0fc',args:[...arguments]})
  },
  unsetCurrencyOut(){
   this.uart.t("inv",{mid:'b401f'})
  },
  loadChangellyFloatingOffer(){
   this.uart.t("inv",{mid:'7fe64'})
  },
  loadChangellyFixedOffer(){
   this.uart.t("inv",{mid:'46678'})
  },
  loadChangenowOffer(){
   this.uart.t("inv",{mid:'925a9'})
  },
 }),
]