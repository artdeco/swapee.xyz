import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingScreenAR}
 */
function __AbstractTestingScreenAR() {}
__AbstractTestingScreenAR.prototype = /** @type {!_AbstractTestingScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTestingScreenAR}
 */
class _AbstractTestingScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITestingScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractTestingScreenAR} ‎
 */
class AbstractTestingScreenAR extends newAbstract(
 _AbstractTestingScreenAR,'ITestingScreenAR',null,{
  asITestingScreenAR:1,
  superTestingScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTestingScreenAR} */
AbstractTestingScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTestingScreenAR} */
function AbstractTestingScreenARClass(){}

export default AbstractTestingScreenAR


AbstractTestingScreenAR[$implementations]=[
 __AbstractTestingScreenAR,
 AR,
 AbstractTestingScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ITestingScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractTestingScreenAR}