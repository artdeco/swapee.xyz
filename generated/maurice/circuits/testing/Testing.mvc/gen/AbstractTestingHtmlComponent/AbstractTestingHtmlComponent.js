import AbstractTestingGPU from '../AbstractTestingGPU'
import AbstractTestingScreenBack from '../AbstractTestingScreenBack'
import AbstractTestingRadio from '../AbstractTestingRadio'
import {HtmlComponent,makeRevealConcealPaints,mvc} from '@webcircuits/webcircuits'
import {TestingInputsQPs} from '../../pqs/TestingInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractTesting from '../AbstractTesting'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingHtmlComponent}
 */
function __AbstractTestingHtmlComponent() {}
__AbstractTestingHtmlComponent.prototype = /** @type {!_AbstractTestingHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingHtmlComponent}
 */
class _AbstractTestingHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.TestingHtmlComponent} */ (res)
  }
}
/**
 * The _ITesting_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractTestingHtmlComponent} ‎
 */
export class AbstractTestingHtmlComponent extends newAbstract(
 _AbstractTestingHtmlComponent,'ITestingHtmlComponent',null,{
  asITestingHtmlComponent:1,
  superTestingHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingHtmlComponent} */
AbstractTestingHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingHtmlComponent} */
function AbstractTestingHtmlComponentClass(){}


AbstractTestingHtmlComponent[$implementations]=[
 __AbstractTestingHtmlComponent,
 HtmlComponent,
 AbstractTesting,
 AbstractTestingGPU,
 AbstractTestingScreenBack,
 AbstractTestingRadio,
 AbstractTestingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITestingHtmlComponent}*/({
  inputsQPs:TestingInputsQPs,
 }),

/** @type {!AbstractTestingHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.ITestingHtmlComponent}*/function paintChangellyFloatingOfferAmount() {
     this.ChangellyFloatingOfferAmount.setText(this.model.changellyFloatingOffer)
   },/**@this {!xyz.swapee.wc.ITestingHtmlComponent}*/function paintChangellyFixedOfferAmount() {
     this.ChangellyFixedOfferAmount.setText(this.model.changellyFixedOffer)
   }
 ] }),
 AbstractTestingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITestingHtmlComponent}*/({
  paint:function $reveal_ifNot_$conceal_ChangellyFloatingOffer({loadingChangellyFloatingOffer:loadingChangellyFloatingOffer,changellyFloatingOffer:changellyFloatingOffer}){
   const{
    asITestingGPU:{ChangellyFloatingOffer:ChangellyFloatingOffer},
    asIBrowserView:{conceal:conceal,reveal:reveal},
   }=this
   const state={loadingChangellyFloatingOffer:loadingChangellyFloatingOffer,changellyFloatingOffer:changellyFloatingOffer}
   ;({loadingChangellyFloatingOffer:loadingChangellyFloatingOffer,changellyFloatingOffer:changellyFloatingOffer}=state)
   if(loadingChangellyFloatingOffer) conceal(ChangellyFloatingOffer,true)
   else reveal(ChangellyFloatingOffer,changellyFloatingOffer)
  },
 }),
 AbstractTestingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITestingHtmlComponent}*/({
  paint:function $reveal_ifNot_$conceal_ChangellyFixedOffer({loadingChangellyFixedOffer:loadingChangellyFixedOffer,changellyFixedOffer:changellyFixedOffer}){
   const{
    asITestingGPU:{ChangellyFixedOffer:ChangellyFixedOffer},
    asIBrowserView:{conceal:conceal,reveal:reveal},
   }=this
   const state={loadingChangellyFixedOffer:loadingChangellyFixedOffer,changellyFixedOffer:changellyFixedOffer}
   ;({loadingChangellyFixedOffer:loadingChangellyFixedOffer,changellyFixedOffer:changellyFixedOffer}=state)
   if(loadingChangellyFixedOffer) conceal(ChangellyFixedOffer,true)
   else reveal(ChangellyFixedOffer,changellyFixedOffer)
  },
 }),
 AbstractTestingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITestingHtmlComponent}*/({
  paint:function paint_value_on_AmountIn({amountIn:amountIn}){
   const{asITestingGPU:{AmountIn:AmountIn}}=this
   AmountIn.val(amountIn)
  },
 }),
 AbstractTestingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITestingHtmlComponent}*/({
  paint:makeRevealConcealPaints({
   RatesLoIn:{anyLoading:1},
  }),
 }),
]