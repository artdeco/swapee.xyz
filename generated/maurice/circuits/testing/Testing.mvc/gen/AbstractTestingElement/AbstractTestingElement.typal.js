
import AbstractTesting from '../AbstractTesting'

/** @abstract {xyz.swapee.wc.ITestingElement} */
export default class AbstractTestingElement { }



AbstractTestingElement[$implementations]=[AbstractTesting,
 /** @type {!AbstractTestingElement} */ ({
  rootId:'Testing',
  __$id:6269653044,
  fqn:'xyz.swapee.wc.ITesting',
  maurice_element_v3:true,
 }),
]