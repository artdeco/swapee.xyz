export default function TestingRenderVdus(){
 return (<div $id="Testing">
  <vdu $id="RatesLoIn" />
  <vdu $id="ChangellyFloatingOffer" />
  <vdu $id="ChangellyFloatingOfferAmount" />
  <vdu $id="ChangellyFixedOffer" />
  <vdu $id="ChangellyFixedOfferAmount" />
  <vdu $id="AmountIn" />
  <vdu $id="IncreaseBu" />
  <vdu $id="DecreaseBu" />
  <vdu $id="RatesLoEr" />
 </div>)
}