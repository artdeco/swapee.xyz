import TestingRenderVdus from './methods/render-vdus'
import TestingElementPort from '../TestingElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {TestingInputsPQs} from '../../pqs/TestingInputsPQs'
import {TestingCachePQs} from '../../pqs/TestingCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractTesting from '../AbstractTesting'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingElement}
 */
function __AbstractTestingElement() {}
__AbstractTestingElement.prototype = /** @type {!_AbstractTestingElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingElement}
 */
class _AbstractTestingElement { }
/**
 * A component description.
 *
 * The _ITesting_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractTestingElement} ‎
 */
class AbstractTestingElement extends newAbstract(
 _AbstractTestingElement,'ITestingElement',null,{
  asITestingElement:1,
  superTestingElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingElement} */
AbstractTestingElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingElement} */
function AbstractTestingElementClass(){}

export default AbstractTestingElement


AbstractTestingElement[$implementations]=[
 __AbstractTestingElement,
 ElementBase,
 AbstractTestingElementClass.prototype=/**@type {!xyz.swapee.wc.ITestingElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':core':coreColAttr,
   ':host':hostColAttr,
   ':amount-in':amountInColAttr,
   ':currency-in':currencyInColAttr,
   ':currency-out':currencyOutColAttr,
   ':change-now':changeNowColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'core':coreAttr,
    'host':hostAttr,
    'amount-in':amountInAttr,
    'currency-in':currencyInAttr,
    'currency-out':currencyOutAttr,
    'change-now':changeNowAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(coreAttr===undefined?{'core':coreColAttr}:{}),
    ...(hostAttr===undefined?{'host':hostColAttr}:{}),
    ...(amountInAttr===undefined?{'amount-in':amountInColAttr}:{}),
    ...(currencyInAttr===undefined?{'currency-in':currencyInColAttr}:{}),
    ...(currencyOutAttr===undefined?{'currency-out':currencyOutColAttr}:{}),
    ...(changeNowAttr===undefined?{'change-now':changeNowColAttr}:{}),
   }
  },
 }),
 AbstractTestingElementClass.prototype=/**@type {!xyz.swapee.wc.ITestingElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'core':coreAttr,
   'host':hostAttr,
   'amount-in':amountInAttr,
   'currency-in':currencyInAttr,
   'currency-out':currencyOutAttr,
   'change-now':changeNowAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    core:coreAttr,
    host:hostAttr,
    amountIn:amountInAttr,
    currencyIn:currencyInAttr,
    currencyOut:currencyOutAttr,
    changeNow:changeNowAttr,
   }
  },
 }),
 AbstractTestingElementClass.prototype=/**@type {!xyz.swapee.wc.ITestingElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractTestingElementClass.prototype=/**@type {!xyz.swapee.wc.ITestingElement}*/({
  render:TestingRenderVdus,
 }),
 AbstractTestingElementClass.prototype=/**@type {!xyz.swapee.wc.ITestingElement}*/({
  inputsPQs:TestingInputsPQs,
  cachePQs:TestingCachePQs,
  vdus:{
   'RatesLoIn': 'g1ee3',
   'RatesLoEr': 'g1ee4',
   'ChangellyFloatingOffer': 'g1ee7',
   'ChangellyFloatingOfferAmount': 'g1ee8',
   'AmountIn': 'g1ee9',
   'IncreaseBu': 'g1ee10',
   'DecreaseBu': 'g1ee11',
   'ChangellyFixedOffer': 'g1ee12',
   'ChangellyFixedOfferAmount': 'g1ee13',
  },
 }),
 IntegratedComponent,
 AbstractTestingElementClass.prototype=/**@type {!xyz.swapee.wc.ITestingElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','core','host','amountIn','currencyIn','currencyOut','changeNow','no-solder',':no-solder',':core',':host','amount-in',':amount-in','currency-in',':currency-in','currency-out',':currency-out','change-now',':change-now','fe646','b88f2','463fd','6cd83','e4b4a','1b880','e15aa','80629','089c5','a32d6','a74ad','67b3d','88da7','f3088','98dbb','94af8','children']),
   })
  },
  get Port(){
   return TestingElementPort
  },
 }),
]



AbstractTestingElement[$implementations]=[AbstractTesting,
 /** @type {!AbstractTestingElement} */ ({
  rootId:'Testing',
  __$id:6269653044,
  fqn:'xyz.swapee.wc.ITesting',
  maurice_element_v3:true,
 }),
]