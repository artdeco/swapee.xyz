import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingScreenAT}
 */
function __AbstractTestingScreenAT() {}
__AbstractTestingScreenAT.prototype = /** @type {!_AbstractTestingScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTestingScreenAT}
 */
class _AbstractTestingScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITestingScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractTestingScreenAT} ‎
 */
class AbstractTestingScreenAT extends newAbstract(
 _AbstractTestingScreenAT,'ITestingScreenAT',null,{
  asITestingScreenAT:1,
  superTestingScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTestingScreenAT} */
AbstractTestingScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTestingScreenAT} */
function AbstractTestingScreenATClass(){}

export default AbstractTestingScreenAT


AbstractTestingScreenAT[$implementations]=[
 __AbstractTestingScreenAT,
 UartUniversal,
]