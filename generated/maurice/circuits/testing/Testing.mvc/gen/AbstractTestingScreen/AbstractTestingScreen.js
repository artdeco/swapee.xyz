import AbstractTestingScreenAR from '../AbstractTestingScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {TestingInputsPQs} from '../../pqs/TestingInputsPQs'
import {TestingMemoryQPs} from '../../pqs/TestingMemoryQPs'
import {TestingCacheQPs} from '../../pqs/TestingCacheQPs'
import {TestingVdusPQs} from '../../pqs/TestingVdusPQs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingScreen}
 */
function __AbstractTestingScreen() {}
__AbstractTestingScreen.prototype = /** @type {!_AbstractTestingScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingScreen}
 */
class _AbstractTestingScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractTestingScreen} ‎
 */
class AbstractTestingScreen extends newAbstract(
 _AbstractTestingScreen,'ITestingScreen',null,{
  asITestingScreen:1,
  superTestingScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingScreen} */
AbstractTestingScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingScreen} */
function AbstractTestingScreenClass(){}

export default AbstractTestingScreen


AbstractTestingScreen[$implementations]=[
 __AbstractTestingScreen,
 AbstractTestingScreenClass.prototype=/**@type {!xyz.swapee.wc.ITestingScreen}*/({
  inputsPQs:TestingInputsPQs,
  memoryQPs:TestingMemoryQPs,
  cacheQPs:TestingCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractTestingScreenAR,
 AbstractTestingScreenClass.prototype=/**@type {!xyz.swapee.wc.ITestingScreen}*/({
  vdusPQs:TestingVdusPQs,
 }),
 AbstractTestingScreenClass.prototype=/**@type {!xyz.swapee.wc.ITestingScreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const IncreaseBu=this.IncreaseBu
    if(IncreaseBu){
     IncreaseBu.addEventListener('click',(ev)=>{
     ev.preventDefault()
      this.increaseAmount()
     return false
     })
    }
    const DecreaseBu=this.DecreaseBu
    if(DecreaseBu){
     DecreaseBu.addEventListener('click',(ev)=>{
     ev.preventDefault()
      this.decreaseAmount()
     return false
     })
    }
   })
  },
 }),
 AbstractTestingScreenClass.prototype=/**@type {!xyz.swapee.wc.ITestingScreen}*/({
  deduceInputs(){
   const{asITestingDisplay:{ChangellyFloatingOfferAmount:ChangellyFloatingOfferAmount,ChangellyFixedOfferAmount:ChangellyFixedOfferAmount}}=this
   return{
    changellyFloatingOffer:ChangellyFloatingOfferAmount?ChangellyFloatingOfferAmount.innerText:void 0,
    changellyFixedOffer:ChangellyFixedOfferAmount?ChangellyFixedOfferAmount.innerText:void 0,
   }
  },
 }),
]