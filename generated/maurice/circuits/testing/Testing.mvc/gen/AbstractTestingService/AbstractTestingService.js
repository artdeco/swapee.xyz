import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingService}
 */
function __AbstractTestingService() {}
__AbstractTestingService.prototype = /** @type {!_AbstractTestingService} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingService}
 */
class _AbstractTestingService { }
/**
 * A service for the ITesting.
 * @extends {xyz.swapee.wc.AbstractTestingService} ‎
 */
class AbstractTestingService extends newAbstract(
 _AbstractTestingService,'ITestingService',null,{
  asITestingService:1,
  superTestingService:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingService} */
AbstractTestingService.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingService} */
function AbstractTestingServiceClass(){}

export default AbstractTestingService


AbstractTestingService[$implementations]=[
 __AbstractTestingService,
]

export {AbstractTestingService}