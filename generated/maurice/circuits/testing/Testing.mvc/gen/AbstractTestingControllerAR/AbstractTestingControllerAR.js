import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingControllerAR}
 */
function __AbstractTestingControllerAR() {}
__AbstractTestingControllerAR.prototype = /** @type {!_AbstractTestingControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTestingControllerAR}
 */
class _AbstractTestingControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITestingControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractTestingControllerAR} ‎
 */
class AbstractTestingControllerAR extends newAbstract(
 _AbstractTestingControllerAR,'ITestingControllerAR',null,{
  asITestingControllerAR:1,
  superTestingControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTestingControllerAR} */
AbstractTestingControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTestingControllerAR} */
function AbstractTestingControllerARClass(){}

export default AbstractTestingControllerAR


AbstractTestingControllerAR[$implementations]=[
 __AbstractTestingControllerAR,
 AR,
 AbstractTestingControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ITestingControllerAR}*/({
  allocator(){
   this.methods={
    increaseAmount:'9540f',
    decreaseAmount:'0e9bb',
    setCore:'9d9b6',
    unsetCore:'d22f7',
    setAmountIn:'f1723',
    unsetAmountIn:'be5b1',
    setCurrencyIn:'7334d',
    unsetCurrencyIn:'b0735',
    setCurrencyOut:'3a0fc',
    unsetCurrencyOut:'b401f',
    loadChangellyFloatingOffer:'7fe64',
    loadChangellyFixedOffer:'46678',
    loadChangenowOffer:'925a9',
   }
  },
 }),
]