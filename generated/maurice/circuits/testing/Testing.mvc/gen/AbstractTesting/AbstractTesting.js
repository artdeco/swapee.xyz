import AbstractTestingProcessor from '../AbstractTestingProcessor'
import {TestingCore} from '../TestingCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractTestingComputer} from '../AbstractTestingComputer'
import {AbstractTestingController} from '../AbstractTestingController'
import {regulateTestingCache} from './methods/regulateTestingCache'
import {TestingCacheQPs} from '../../pqs/TestingCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTesting}
 */
function __AbstractTesting() {}
__AbstractTesting.prototype = /** @type {!_AbstractTesting} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTesting}
 */
class _AbstractTesting { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractTesting} ‎
 */
class AbstractTesting extends newAbstract(
 _AbstractTesting,'ITesting',null,{
  asITesting:1,
  superTesting:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTesting} */
AbstractTesting.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTesting} */
function AbstractTestingClass(){}

export default AbstractTesting


AbstractTesting[$implementations]=[
 __AbstractTesting,
 TestingCore,
 AbstractTestingProcessor,
 IntegratedComponent,
 AbstractTestingComputer,
 AbstractTestingController,
 AbstractTestingClass.prototype=/**@type {!xyz.swapee.wc.ITesting}*/({
  regulateState:regulateTestingCache,
  stateQPs:TestingCacheQPs,
 }),
]


export {AbstractTesting}