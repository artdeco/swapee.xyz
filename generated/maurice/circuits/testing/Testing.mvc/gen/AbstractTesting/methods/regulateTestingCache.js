import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateTestingCache=makeBuffers({
 changellyFixedOffer:Number,
 changellyFloatingOffer:Number,
 changenowOffer:5,
 anyLoading:Boolean,
 loadingChangellyFloatingOffer:Boolean,
 hasMoreChangellyFloatingOffer:Boolean,
 loadChangellyFloatingOfferError:5,
 loadingChangellyFixedOffer:Boolean,
 hasMoreChangellyFixedOffer:Boolean,
 loadChangellyFixedOfferError:5,
 loadingChangenowOffer:Boolean,
 hasMoreChangenowOffer:Boolean,
 loadChangenowOfferError:5,
},{silent:true})