import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingElementPort}
 */
function __TestingElementPort() {}
__TestingElementPort.prototype = /** @type {!_TestingElementPort} */ ({ })
/** @this {xyz.swapee.wc.TestingElementPort} */ function TestingElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ITestingElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    amountInOpts: {},
    increaseBuOpts: {},
    decreaseBuOpts: {},
    ratesLoInOpts: {},
    ratesLoErOpts: {},
    changellyFloatingOfferOpts: {},
    changellyFloatingOfferAmountOpts: {},
    changellyFixedOfferOpts: {},
    changellyFixedOfferAmountOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingElementPort}
 */
class _TestingElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractTestingElementPort} ‎
 */
class TestingElementPort extends newAbstract(
 _TestingElementPort,'ITestingElementPort',TestingElementPortConstructor,{
  asITestingElementPort:1,
  superTestingElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingElementPort} */
TestingElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingElementPort} */
function TestingElementPortClass(){}

export default TestingElementPort


TestingElementPort[$implementations]=[
 __TestingElementPort,
 TestingElementPortClass.prototype=/**@type {!xyz.swapee.wc.ITestingElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'amount-in-opts':undefined,
    'increase-bu-opts':undefined,
    'decrease-bu-opts':undefined,
    'rates-lo-in-opts':undefined,
    'rates-lo-er-opts':undefined,
    'changelly-floating-offer-opts':undefined,
    'changelly-floating-offer-amount-opts':undefined,
    'changelly-fixed-offer-opts':undefined,
    'changelly-fixed-offer-amount-opts':undefined,
    'amount-in':undefined,
    'currency-in':undefined,
    'currency-out':undefined,
    'change-now':undefined,
   })
  },
 }),
]