import {preadaptLoadChangellyFloatingOffer,preadaptLoadChangellyFixedOffer,preadaptLoadChangenowOffer} from '../AbstractTestingRadio/preadapters'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingProcessor}
 */
function __AbstractTestingProcessor() {}
__AbstractTestingProcessor.prototype = /** @type {!_AbstractTestingProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingProcessor}
 */
class _AbstractTestingProcessor { }
/**
 * The processor to compute changes to the memory for the _ITesting_.
 * @extends {xyz.swapee.wc.AbstractTestingProcessor} ‎
 */
class AbstractTestingProcessor extends newAbstract(
 _AbstractTestingProcessor,'ITestingProcessor',null,{
  asITestingProcessor:1,
  superTestingProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingProcessor} */
AbstractTestingProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingProcessor} */
function AbstractTestingProcessorClass(){}

export default AbstractTestingProcessor


AbstractTestingProcessor[$implementations]=[
 __AbstractTestingProcessor,
 AbstractTestingProcessorClass.prototype=/**@type {!xyz.swapee.wc.ITestingProcessor}*/({
  loadChangellyFloatingOffer(){
   return preadaptLoadChangellyFloatingOffer.call(this,this.model).then(this.setInfo)
  },
  loadChangellyFixedOffer(){
   return preadaptLoadChangellyFixedOffer.call(this,this.model).then(this.setInfo)
  },
  loadChangenowOffer(){
   return preadaptLoadChangenowOffer.call(this,this.model).then(this.setInfo)
  },
 }),
]