import TestingBuffer from '../TestingBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {TestingPortConnector} from '../TestingPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingController}
 */
function __AbstractTestingController() {}
__AbstractTestingController.prototype = /** @type {!_AbstractTestingController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTestingController}
 */
class _AbstractTestingController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractTestingController} ‎
 */
export class AbstractTestingController extends newAbstract(
 _AbstractTestingController,'ITestingController',null,{
  asITestingController:1,
  superTestingController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTestingController} */
AbstractTestingController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTestingController} */
function AbstractTestingControllerClass(){}


AbstractTestingController[$implementations]=[
 AbstractTestingControllerClass.prototype=/**@type {!xyz.swapee.wc.ITestingController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.ITestingPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractTestingController,
 TestingBuffer,
 IntegratedController,
 /**@type {!AbstractTestingController}*/(TestingPortConnector),
 AbstractTestingControllerClass.prototype=/**@type {!xyz.swapee.wc.ITestingController}*/({
  setCore(val){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({core:val})
  },
  setAmountIn(val){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({amountIn:val})
  },
  setCurrencyIn(val){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({currencyIn:val})
  },
  setCurrencyOut(val){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({currencyOut:val})
  },
  unsetCore(){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({core:''})
  },
  unsetAmountIn(){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({amountIn:null})
  },
  unsetCurrencyIn(){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({currencyIn:''})
  },
  unsetCurrencyOut(){
   const{asITestingController:{setInputs:setInputs}}=this
   setInputs({currencyOut:''})
  },
 }),
]


export default AbstractTestingController