import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingDisplay}
 */
function __AbstractTestingDisplay() {}
__AbstractTestingDisplay.prototype = /** @type {!_AbstractTestingDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTestingDisplay}
 */
class _AbstractTestingDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractTestingDisplay} ‎
 */
class AbstractTestingDisplay extends newAbstract(
 _AbstractTestingDisplay,'ITestingDisplay',null,{
  asITestingDisplay:1,
  superTestingDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTestingDisplay} */
AbstractTestingDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTestingDisplay} */
function AbstractTestingDisplayClass(){}

export default AbstractTestingDisplay


AbstractTestingDisplay[$implementations]=[
 __AbstractTestingDisplay,
 GraphicsDriverBack,
 AbstractTestingDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.ITestingDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.ITestingDisplay}*/({
    RatesLoIn:twinMock,
    ChangellyFloatingOffer:twinMock,
    ChangellyFloatingOfferAmount:twinMock,
    ChangellyFixedOffer:twinMock,
    ChangellyFixedOfferAmount:twinMock,
    AmountIn:twinMock,
    IncreaseBu:twinMock,
    DecreaseBu:twinMock,
    RatesLoEr:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.TestingDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.ITestingDisplay.Initialese}*/({
   AmountIn:1,
   IncreaseBu:1,
   DecreaseBu:1,
   RatesLoIn:1,
   RatesLoEr:1,
   ChangellyFloatingOffer:1,
   ChangellyFloatingOfferAmount:1,
   ChangellyFixedOffer:1,
   ChangellyFixedOfferAmount:1,
  }),
  initializer({
   AmountIn:_AmountIn,
   IncreaseBu:_IncreaseBu,
   DecreaseBu:_DecreaseBu,
   RatesLoIn:_RatesLoIn,
   RatesLoEr:_RatesLoEr,
   ChangellyFloatingOffer:_ChangellyFloatingOffer,
   ChangellyFloatingOfferAmount:_ChangellyFloatingOfferAmount,
   ChangellyFixedOffer:_ChangellyFixedOffer,
   ChangellyFixedOfferAmount:_ChangellyFixedOfferAmount,
  }) {
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_IncreaseBu!==undefined) this.IncreaseBu=_IncreaseBu
   if(_DecreaseBu!==undefined) this.DecreaseBu=_DecreaseBu
   if(_RatesLoIn!==undefined) this.RatesLoIn=_RatesLoIn
   if(_RatesLoEr!==undefined) this.RatesLoEr=_RatesLoEr
   if(_ChangellyFloatingOffer!==undefined) this.ChangellyFloatingOffer=_ChangellyFloatingOffer
   if(_ChangellyFloatingOfferAmount!==undefined) this.ChangellyFloatingOfferAmount=_ChangellyFloatingOfferAmount
   if(_ChangellyFixedOffer!==undefined) this.ChangellyFixedOffer=_ChangellyFixedOffer
   if(_ChangellyFixedOfferAmount!==undefined) this.ChangellyFixedOfferAmount=_ChangellyFixedOfferAmount
  },
 }),
]