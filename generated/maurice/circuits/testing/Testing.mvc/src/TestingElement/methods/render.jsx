/** @type {xyz.swapee.wc.ITestingElement._render} */
export default function TestingRender({
 amountIn:amountIn,anyLoading:anyLoading,
 changellyFloatingOffer:changellyFloatingOffer,
 changellyFixedOffer:changellyFixedOffer,
 loadingChangellyFixedOffer:loadingChangellyFixedOffer,
 loadingChangellyFloatingOffer:loadingChangellyFloatingOffer,
},{increaseAmount:increaseAmount,decreaseAmount:decreaseAmount}) {
 return (<div $id="Testing">
  <span $id="RatesLoIn" $reveal={anyLoading} />
  <div $id="ChangellyFloatingOffer" $reveal={changellyFloatingOffer} $conceal={loadingChangellyFloatingOffer}>
   <span $id="ChangellyFloatingOfferAmount">{changellyFloatingOffer}</span>
  </div>
  <div $id="ChangellyFixedOffer" $reveal={changellyFixedOffer} $conceal={loadingChangellyFixedOffer}>
   <span $id="ChangellyFixedOfferAmount">{changellyFixedOffer}</span>
  </div>
  <input $id="AmountIn" value={amountIn}/>
  <button $id="IncreaseBu" onClick={increaseAmount} />
  <button $id="DecreaseBu" onClick={decreaseAmount} />
 </div>)
}