import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import TestingServerController from '../TestingServerController'
import TestingService from '../../src/TestingService'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractTestingElement from '../../gen/AbstractTestingElement'

/** @extends {xyz.swapee.wc.TestingElement} */
export default class TestingElement extends AbstractTestingElement.implements(
 TestingServerController,
 TestingService,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ITestingElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ITestingElement}*/({
   classesMap: true,
   rootSelector:     `.Testing`,
   stylesheet:       'html/styles/Testing.css',
   blockName:        'html/TestingBlock.html',
  }),
  // todo: generate testing.page now!
){}

// thank you for using web circuits
