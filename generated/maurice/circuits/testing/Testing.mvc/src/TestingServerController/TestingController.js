import TestingSharedController from '../TestingSharedController'
import AbstractTestingController from '../../gen/AbstractTestingController'

/** @extends {xyz.swapee.wc.TestingController} */
export default class extends AbstractTestingController.implements(
 TestingSharedController,
){}