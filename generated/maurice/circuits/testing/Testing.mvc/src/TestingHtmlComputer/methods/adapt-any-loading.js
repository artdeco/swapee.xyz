/** @type {xyz.swapee.wc.ITestingComputer.adaptAnyLoading} */
export const adaptAnyLoading=({
 loadingChangellyFloatingOffer:loadingChangellyFloatingOffer,
 loadingChangenowOffer:loadingChangenowOffer,
 loadingChangellyFixedOffer:loadingChangellyFixedOffer,
})=>{
 return{ // todo: implement this in the radio?
  anyLoading:loadingChangellyFloatingOffer||loadingChangellyFixedOffer||loadingChangenowOffer,
 }
}

export default adaptAnyLoading