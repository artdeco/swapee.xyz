import adaptAnyLoading from './methods/adapt-any-loading'
import {preadaptAnyLoading} from '../../gen/AbstractTestingComputer/preadapters'
import AbstractTestingComputer from '../../gen/AbstractTestingComputer'

/** @extends {xyz.swapee.wc.TestingComputer} */
export default class TestingHtmlComputer extends AbstractTestingComputer.implements(
 /** @type {!xyz.swapee.wc.ITestingComputer} */ ({
  adaptAnyLoading:adaptAnyLoading,
  adapt:[preadaptAnyLoading],
 }),
){}