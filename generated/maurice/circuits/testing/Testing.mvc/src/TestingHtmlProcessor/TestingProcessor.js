import increaseAmount from './methods/increase-amount'
import decreaseAmount from './methods/decrease-amount'
import AbstractTestingProcessor from '../../gen/AbstractTestingProcessor'

/** @extends {xyz.swapee.wc.TestingProcessor} */
export default class extends AbstractTestingProcessor.implements(
 /** @type {!xyz.swapee.wc.ITestingProcessor} */ ({
  increaseAmount:increaseAmount,
  decreaseAmount:decreaseAmount,
 }),
){}