/** @type {xyz.swapee.wc.ITestingProcessor._increaseAmount} */
export default function increaseAmount(){
 const{
  asITestingCore:{model:{amountIn:amountIn}},
  asITestingController:{setInputs:setInputs},
 }=/**@type {!xyz.swapee.wc.ITestingProcessor}*/(this)
 // test
 setInputs({amountIn:amountIn+1})
}