/** @type {xyz.swapee.wc.ITestingProcessor._decreaseAmount} */
export default function decreaseAmount(){
 const{
  asITestingCore:{model:{amountIn:amountIn}},
  asITestingController:{setInputs:setInputs},
 }=/**@type {!xyz.swapee.wc.ITestingProcessor}*/(this)
 // test
 setInputs({amountIn:Math.max(amountIn-1,1)})
}