/**@type {xyz.swapee.wc.ITestingService._filterChangellyFloatingOffer}*/
export default
async function filterChangellyFloatingOffer({
 amountIn:amountIn,currencyIn:currencyIn,currencyOut:currencyOut,
}) {
 // could put changelly on the land somehow?
 const{asUChangelly:{changelly:changelly}}=this
 if(!changelly) return

 const{
  asIChangellyExchange:{
   GetExchangeAmount:GetExchangeAmount,
  },
 }=changelly
 const res=await GetExchangeAmount({
  from:   currencyIn,
  to:     currencyOut,
  amount: amountIn,
 })
 const amountOut=parseFloat(res)
 return{
  changellyFloatingOffer:amountOut,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvdGVzdGluZy90ZXN0aW5nLndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUE4TEcsTUFBTSxTQUFTLDRCQUE0QjtDQUMxQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyx1QkFBdUI7QUFDaEUsQ0FBQztDQUNBLEdBQUcsTUFBTSxJQUFJLFVBQVUsR0FBRyxJQUFJLEtBQUs7Q0FDbkMsTUFBTSxjQUFjLG1CQUFtQixHQUFHO0NBQzFDLEVBQUUsQ0FBQyxZQUFZOztDQUVmO0VBQ0M7R0FDQyxtQ0FBbUM7QUFDdEM7R0FDRztDQUNGLE1BQU0sR0FBRyxDQUFDLE1BQU0saUJBQWlCO0VBQ2hDLFFBQVEsVUFBVTtFQUNsQixRQUFRLFdBQVc7RUFDbkIsUUFBUSxRQUFRO0FBQ2xCLEVBQUU7Q0FDRCxNQUFNLFNBQVMsQ0FBQyxVQUFVLENBQUM7Q0FDM0I7RUFDQyxnQ0FBZ0M7QUFDbEM7QUFDQSxDQUFGIn0=