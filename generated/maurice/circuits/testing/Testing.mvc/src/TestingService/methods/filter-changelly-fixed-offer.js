/**@type {xyz.swapee.wc.ITestingService._filterChangellyFixedOffer}*/
export default
async function filterChangellyFixedOffer({
 amountIn:amountIn,currencyIn:currencyIn,currencyOut:currencyOut,
}) {
 // could put changelly on the land somehow?
 const{asUChangelly:{changelly:changelly}}=this
 if(!changelly) return

 const{
  asIChangellyFixedRate:{
   GetFixRateForAmount:GetFixRateForAmount,
  },
 }=changelly
 const res=await GetFixRateForAmount({
  from:     currencyIn,
  to:      currencyOut,
  amountFrom: amountIn,
 })
 const amountOut=parseFloat(res)
 return{
  changellyFixedOffer:amountOut,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvdGVzdGluZy90ZXN0aW5nLndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFvTkcsTUFBTSxTQUFTLHlCQUF5QjtDQUN2QyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyx1QkFBdUI7QUFDaEUsQ0FBQztDQUNBLEdBQUcsTUFBTSxJQUFJLFVBQVUsR0FBRyxJQUFJLEtBQUs7Q0FDbkMsTUFBTSxjQUFjLG1CQUFtQixHQUFHO0NBQzFDLEVBQUUsQ0FBQyxZQUFZOztDQUVmO0VBQ0M7R0FDQyx1Q0FBdUM7QUFDMUM7R0FDRztDQUNGLE1BQU0sR0FBRyxDQUFDLE1BQU0sbUJBQW1CO0VBQ2xDLFVBQVUsVUFBVTtFQUNwQixTQUFTLFdBQVc7RUFDcEIsWUFBWSxRQUFRO0FBQ3RCLEVBQUU7Q0FDRCxNQUFNLFNBQVMsQ0FBQyxVQUFVLENBQUM7Q0FDM0I7RUFDQyw2QkFBNkI7QUFDL0I7QUFDQSxDQUFGIn0=