import AbstractTestingService from '../../gen/AbstractTestingService'
import filterChangenowOffer from './methods/filter-changenow-offer'
import {ChangellyUniversal} from '@type.community/changelly.com'

/**@extends {xyz.swapee.wc.TestingService} */
export default class extends AbstractTestingService.implements(
 ChangellyUniversal,
 AbstractTestingService.class.prototype=/**@type {!xyz.swapee.wc.TestingService}*/({
  filterChangenowOffer:filterChangenowOffer,
 }),
){}