import deduceInputs from './methods/deduce-inputs'
import AbstractTestingControllerAT from '../../gen/AbstractTestingControllerAT'
import TestingDisplay from '../TestingDisplay'
import AbstractTestingScreen from '../../gen/AbstractTestingScreen'

/** @extends {xyz.swapee.wc.TestingScreen} */
export default class extends AbstractTestingScreen.implements(
 /**@type {!xyz.swapee.wc.ITestingScreen}*/({get queries(){return this.settings}}),
 AbstractTestingControllerAT,
 TestingDisplay,
 /** @type {!xyz.swapee.wc.ITestingScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:6269653044,
 }),
){}