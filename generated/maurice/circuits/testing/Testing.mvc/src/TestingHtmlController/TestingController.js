import TestingSharedController from '../TestingSharedController'
import AbstractTestingControllerBack from '../../gen/AbstractTestingControllerBack'

/** @extends {xyz.swapee.wc.back.TestingController} */
export default class extends AbstractTestingControllerBack.implements(
 TestingSharedController,
){}