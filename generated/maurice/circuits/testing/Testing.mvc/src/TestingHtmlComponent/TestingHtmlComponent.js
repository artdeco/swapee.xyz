import TestingHtmlController from '../TestingHtmlController'
import TestingHtmlComputer from '../TestingHtmlComputer'
import TestingHtmlProcessor from '../TestingHtmlProcessor'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractTestingHtmlComponent} from '../../gen/AbstractTestingHtmlComponent'

/** @extends {xyz.swapee.wc.TestingHtmlComponent} */
export default class extends AbstractTestingHtmlComponent.implements(
 TestingHtmlController,
 TestingHtmlComputer,
 TestingHtmlProcessor,
 IntegratedComponentInitialiser,
){}