import constructor from './methods/constructor'
import AbstractTestingController from '../../gen/AbstractTestingController'

const TestingSharedController=AbstractTestingController.__trait(
 /** @type {!xyz.swapee.wc.ITestingController} */ ({
  constructor:constructor,
 }),
)
export default TestingSharedController