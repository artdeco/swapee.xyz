/**
 * An abstract class of `xyz.swapee.wc.ITesting` interface.
 * @extends {xyz.swapee.wc.AbstractTesting}
 */
class AbstractTesting extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITesting_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TestingPort}
 */
class TestingPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITestingController` interface.
 * @extends {xyz.swapee.wc.AbstractTestingController}
 */
class AbstractTestingController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ITesting_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.TestingElement}
 */
class TestingElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TestingBuffer}
 */
class TestingBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITestingComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTestingComputer}
 */
class AbstractTestingComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.TestingController}
 */
class TestingController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractTesting = AbstractTesting
module.exports.TestingPort = TestingPort
module.exports.AbstractTestingController = AbstractTestingController
module.exports.TestingElement = TestingElement
module.exports.TestingBuffer = TestingBuffer
module.exports.AbstractTestingComputer = AbstractTestingComputer
module.exports.TestingController = TestingController

Object.defineProperties(module.exports, {
 'AbstractTesting': {get: () => require('./precompile/internal')[62696530441]},
 [62696530441]: {get: () => module.exports['AbstractTesting']},
 'TestingPort': {get: () => require('./precompile/internal')[62696530443]},
 [62696530443]: {get: () => module.exports['TestingPort']},
 'AbstractTestingController': {get: () => require('./precompile/internal')[62696530444]},
 [62696530444]: {get: () => module.exports['AbstractTestingController']},
 'TestingElement': {get: () => require('./precompile/internal')[62696530448]},
 [62696530448]: {get: () => module.exports['TestingElement']},
 'TestingBuffer': {get: () => require('./precompile/internal')[626965304411]},
 [626965304411]: {get: () => module.exports['TestingBuffer']},
 'AbstractTestingComputer': {get: () => require('./precompile/internal')[626965304430]},
 [626965304430]: {get: () => module.exports['AbstractTestingComputer']},
 'TestingController': {get: () => require('./precompile/internal')[626965304461]},
 [626965304461]: {get: () => module.exports['TestingController']},
})