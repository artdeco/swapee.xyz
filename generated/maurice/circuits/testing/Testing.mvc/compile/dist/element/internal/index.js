import Module from './element'

/**@extends {xyz.swapee.wc.AbstractTesting}*/
export class AbstractTesting extends Module['62696530441'] {}
/** @type {typeof xyz.swapee.wc.AbstractTesting} */
AbstractTesting.class=function(){}
/** @type {typeof xyz.swapee.wc.TestingPort} */
export const TestingPort=Module['62696530443']
/**@extends {xyz.swapee.wc.AbstractTestingController}*/
export class AbstractTestingController extends Module['62696530444'] {}
/** @type {typeof xyz.swapee.wc.AbstractTestingController} */
AbstractTestingController.class=function(){}
/** @type {typeof xyz.swapee.wc.TestingElement} */
export const TestingElement=Module['62696530448']
/** @type {typeof xyz.swapee.wc.TestingBuffer} */
export const TestingBuffer=Module['626965304411']
/**@extends {xyz.swapee.wc.AbstractTestingComputer}*/
export class AbstractTestingComputer extends Module['626965304430'] {}
/** @type {typeof xyz.swapee.wc.AbstractTestingComputer} */
AbstractTestingComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.TestingController} */
export const TestingController=Module['626965304461']