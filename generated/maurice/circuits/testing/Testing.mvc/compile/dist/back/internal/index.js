import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractTesting}*/
export class AbstractTesting extends Module['62696530441'] {}
/** @type {typeof xyz.swapee.wc.AbstractTesting} */
AbstractTesting.class=function(){}
/** @type {typeof xyz.swapee.wc.TestingPort} */
export const TestingPort=Module['62696530443']
/**@extends {xyz.swapee.wc.AbstractTestingController}*/
export class AbstractTestingController extends Module['62696530444'] {}
/** @type {typeof xyz.swapee.wc.AbstractTestingController} */
AbstractTestingController.class=function(){}
/** @type {typeof xyz.swapee.wc.TestingHtmlComponent} */
export const TestingHtmlComponent=Module['626965304410']
/** @type {typeof xyz.swapee.wc.TestingBuffer} */
export const TestingBuffer=Module['626965304411']
/**@extends {xyz.swapee.wc.AbstractTestingComputer}*/
export class AbstractTestingComputer extends Module['626965304430'] {}
/** @type {typeof xyz.swapee.wc.AbstractTestingComputer} */
AbstractTestingComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.TestingComputer} */
export const TestingComputer=Module['626965304431']
/** @type {typeof xyz.swapee.wc.TestingProcessor} */
export const TestingProcessor=Module['626965304451']
/** @type {typeof xyz.swapee.wc.back.TestingController} */
export const TestingController=Module['626965304461']