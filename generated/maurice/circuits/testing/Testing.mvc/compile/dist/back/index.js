/**
 * An abstract class of `xyz.swapee.wc.ITesting` interface.
 * @extends {xyz.swapee.wc.AbstractTesting}
 */
class AbstractTesting extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITesting_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TestingPort}
 */
class TestingPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITestingController` interface.
 * @extends {xyz.swapee.wc.AbstractTestingController}
 */
class AbstractTestingController extends (class {/* lazy-loaded */}) {}
/**
 * The _ITesting_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.TestingHtmlComponent}
 */
class TestingHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TestingBuffer}
 */
class TestingBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITestingComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTestingComputer}
 */
class AbstractTestingComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.TestingComputer}
 */
class TestingComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _ITesting_.
 * @extends {xyz.swapee.wc.TestingProcessor}
 */
class TestingProcessor extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.TestingController}
 */
class TestingController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractTesting = AbstractTesting
module.exports.TestingPort = TestingPort
module.exports.AbstractTestingController = AbstractTestingController
module.exports.TestingHtmlComponent = TestingHtmlComponent
module.exports.TestingBuffer = TestingBuffer
module.exports.AbstractTestingComputer = AbstractTestingComputer
module.exports.TestingComputer = TestingComputer
module.exports.TestingProcessor = TestingProcessor
module.exports.TestingController = TestingController