import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {6269653044} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const k=h["37270038985"],l=h["37270038986"],m=h["372700389810"],u=h["372700389811"];function v(a,b,d,e){return h["372700389812"](a,b,d,e,!1,void 0)};
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const x=w["61893096584"],y=w["61893096586"],z=w["618930965811"],A=w["618930965812"],B=w["618930965815"],C=w["618930965819"];function D(){}D.prototype={};function E(){this.g=this.m=this.h=this.o=this.s=this.u=this.i=this.j=this.l=null}class F{}class G extends v(F,626965304416,E,{v:1,ka:2}){}
G[u]=[D,x,{constructor(){k(this,()=>this.scan())},scan:function(){const {element:a,A:{vdusPQs:{u:b,o:d,h:e,m:n,g:p,l:q,j:r,i:t,s:R}}}=this,c=B(a);Object.assign(this,{u:c[b],o:c[d],h:c[e],m:c[n],g:c[p],l:c[q],j:c[r],i:c[t],s:c[R]})}},{[m]:{l:1,j:1,i:1,u:1,s:1,o:1,h:1,m:1,g:1},initializer({l:a,j:b,i:d,u:e,s:n,o:p,h:q,m:r,g:t}){void 0!==a&&(this.l=a);void 0!==b&&(this.j=b);void 0!==d&&(this.i=d);void 0!==e&&(this.u=e);void 0!==n&&(this.s=n);void 0!==p&&(this.o=p);void 0!==q&&(this.h=
q);void 0!==r&&(this.m=r);void 0!==t&&(this.g=t)}}];var H=class extends G.implements(){};function I(){};function J(){}J.prototype={};class K{}class L extends v(K,626965304423,null,{H:1,ja:2}){}L[u]=[J,z,{}];function M(){}M.prototype={};class N{}class O extends v(N,626965304426,null,{I:1,ma:2}){}O[u]=[M,A,{allocator(){this.methods={}}}];const P={O:"a74ad",M:"c9e9e",F:"88da7",P:"f3088",R:"98dbb",host:"67b3d",J:"94af8"};const Q={...P};const S=Object.keys(P).reduce((a,b)=>{a[P[b]]=b;return a},{});const T={ia:"23a5b",ha:"b2bf1",ga:"175c7",X:"cafa5",ba:"2220b",K:"b617a",L:"dc0e8",ea:"a9955",V:"b94e6",$:"338b3",fa:"a47e0",W:"f0a43",aa:"89dbe",G:"0c77f",C:"7b5f4",D:"2a73d",da:"2029f",U:"cfde8",Z:"c7da2",ca:"74365",T:"b518b",Y:"5920c"};const U=Object.keys(T).reduce((a,b)=>{a[T[b]]=b;return a},{});function V(){}V.prototype={};class W{}class X extends v(W,626965304424,null,{A:1,la:2}){}function Y(){}
X[u]=[V,Y.prototype={inputsPQs:Q,memoryQPs:S,cacheQPs:U},y,C,O,Y.prototype={vdusPQs:{u:"g1ee3",s:"g1ee4",o:"g1ee7",h:"g1ee8",l:"g1ee9",j:"g1ee10",i:"g1ee11",m:"g1ee12",g:"g1ee13"}},Y.prototype={constructor(){l(this,()=>{var a=this.j;a&&a.addEventListener("click",b=>{b.preventDefault();this.uart.t("inv",{mid:"9540f"});return!1});(a=this.i)&&a.addEventListener("click",b=>{b.preventDefault();this.uart.t("inv",{mid:"0e9bb"});return!1})})}},Y.prototype={deduceInputs(){const {v:{h:a,g:b}}=this;return{D:a?
a.innerText:void 0,C:b?b.innerText:void 0}}}];var Z=class extends X.implements({get queries(){return this.settings}},L,H,{deduceInputs:I,__$id:6269653044}){};module.exports["626965304441"]=H;module.exports["626965304471"]=Z;
/*! @embed-object-end {6269653044} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule