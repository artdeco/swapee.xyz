/**
 * Display for presenting information from the _ITesting_.
 * @extends {xyz.swapee.wc.TestingDisplay}
 */
class TestingDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.TestingScreen}
 */
class TestingScreen extends (class {/* lazy-loaded */}) {}

module.exports.TestingDisplay = TestingDisplay
module.exports.TestingScreen = TestingScreen