import { TestingDisplay, TestingScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.TestingDisplay} */
export { TestingDisplay }
/** @lazy @api {xyz.swapee.wc.TestingScreen} */
export { TestingScreen }