import { AbstractTesting, TestingPort, AbstractTestingController, TestingHtmlComponent,
 TestingBuffer, AbstractTestingComputer, TestingComputer, TestingProcessor,
 TestingController } from './back-exports'

/** @lazy @api {xyz.swapee.wc.AbstractTesting} */
export { AbstractTesting }
/** @lazy @api {xyz.swapee.wc.TestingPort} */
export { TestingPort }
/** @lazy @api {xyz.swapee.wc.AbstractTestingController} */
export { AbstractTestingController }
/** @lazy @api {xyz.swapee.wc.TestingHtmlComponent} */
export { TestingHtmlComponent }
/** @lazy @api {xyz.swapee.wc.TestingBuffer} */
export { TestingBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractTestingComputer} */
export { AbstractTestingComputer }
/** @lazy @api {xyz.swapee.wc.TestingComputer} */
export { TestingComputer }
/** @lazy @api {xyz.swapee.wc.TestingProcessor} */
export { TestingProcessor }
/** @lazy @api {xyz.swapee.wc.back.TestingController} */
export { TestingController }