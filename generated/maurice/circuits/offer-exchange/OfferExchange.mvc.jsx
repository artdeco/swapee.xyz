/** @extends {xyz.swapee.wc.AbstractOfferExchange} */
export default class AbstractOfferExchange extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractOfferExchangeComputer} */
export class AbstractOfferExchangeComputer extends (<computer>
   <adapter name="adaptPickBtc">
    <xyz.swapee.wc.ISwapeeWalletPickerCore invoice="required" />
    <xyz.swapee.wc.IOfferExchangeCore btcInput btcOutput />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerPort address refundAddress />
    </outputs>
   </adapter>

   <adapter name="adaptBtc">
    <xyz.swapee.wc.IExchangeIntentCore currencyFrom="real" currencyTo  />
    <outputs>
     <xyz.swapee.wc.IOfferExchangeCore btcInput btcOutput />
    </outputs>
   </adapter>

   <adapter name="adaptCanCreateTransaction">
    <xyz.swapee.wc.IOfferExchangeCore tocAccepted />
    <xyz.swapee.wc.IExchangeIntentCore fixed />
    <xyz.swapee.wc.IExchangeBrokerCore creatingTransaction address="real" refundAddress />
    <outputs>
     <xyz.swapee.wc.IOfferExchangeCore canCreateTransaction />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentFailed">
    <xyz.swapee.wc.IExchangeBrokerCore status="real" />
    <outputs>
     <xyz.swapee.wc.IOfferExchangeCore paymentFailed />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferExchangeController} */
export class AbstractOfferExchangeController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOfferExchangePort} */
export class OfferExchangePort extends (<ic-port>
  <inputs>
   <htm name="exchangeApiContent" />

  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOfferExchangeView} */
export class AbstractOfferExchangeView extends (<view>
  <classes>
   <string opt name="Loading">Added to buttons when loading.</string>
   <string opt name="InputActive">Added to inputs' wrappers when active.</string>
   <string opt name="ProgressVertLine">.</string>
   <string opt name="PaymentReceived" />
   <string opt name="CircleLoading" />
   <string opt name="CircleComplete"/>
   <string opt name="CircleWaiting"/>
   <string opt name="HintRed"/>

   <string opt name="Copied"/>
   <string opt name="CopiedRemoving"/>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferExchangeElement} */
export class AbstractOfferExchangeElement extends (<element v3 html mv>
 <block src="./OfferExchange.mvc/src/OfferExchangeElement/methods/render.jsx" />
 <inducer src="./OfferExchange.mvc/src/OfferExchangeElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferExchangeHtmlComponent} */
export class AbstractOfferExchangeHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IProgressColumn via="ProgressColumnMobTop" />
   <xyz.swapee.wc.IProgressColumn via="ProgressColumnMobBot" />
   <xyz.swapee.wc.ITransactionInfo via="TransactionInfo" />
   <com.webcircuits.ui.IPopup via="AddressPopup" />
   <com.webcircuits.ui.IPopup via="RefundPopup" />
   <com.webcircuits.ui.ICollapsar via="SendPaymentCollapsar" />
   <com.webcircuits.ui.ICollapsar via="CreateTransactionCollapsar" />
   <com.webcircuits.ui.ICollapsar via="FinishedCollapsar" />
   <xyz.swapee.wc.IProgressColumn via="ProgressColumn" />
   <xyz.swapee.wc.ISwapeeWalletPicker via="SwapeeWalletPicker" />
   <xyz.swapee.wc.IExchangeBroker via="ExchangeBroker" />
   <xyz.swapee.wc.IDealBroker via="DealBroker" />
   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
   <xyz.swapee.wc.IExchangeStatusHint via="ExchangeStatusHint" />
   <xyz.swapee.wc.IOfferExchangeInfoRow via="OfferExchangeInfoRow" />
   <xyz.swapee.wc.IOfferExchangeInfoRow via="OfferExchangeInfoRowMob" />
   <xyz.swapee.wc.IExchangeStatusTitle via="ExchangeStatusTitle" />
   <com.webcircuits.ui.ITimeAgo via="PaymentLastChecked" />
   <com.webcircuits.ui.ITimeInterval via="Duration" />
  </connectors>

</html-ic>) { }
// </class-end>