/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IOfferExchangeComputer': {
  'id': 65293396511,
  'symbols': {},
  'methods': {
   'adaptCanCreateTransaction': 1,
   'adaptPaymentFailed': 2,
   'compute': 3,
   'adaptBtc': 4,
   'adaptPickBtc': 5
  }
 },
 'xyz.swapee.wc.OfferExchangeMemoryPQs': {
  'id': 65293396512,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeOuterCore': {
  'id': 65293396513,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferExchangeInputsPQs': {
  'id': 65293396514,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangePort': {
  'id': 65293396515,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOfferExchangePort': 2
  }
 },
 'xyz.swapee.wc.IOfferExchangePortInterface': {
  'id': 65293396516,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferExchangeCachePQs': {
  'id': 65293396517,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeCore': {
  'id': 65293396518,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOfferExchangeCore': 2
  }
 },
 'xyz.swapee.wc.IOfferExchangeProcessor': {
  'id': 65293396519,
  'symbols': {},
  'methods': {
   'pulseCopyPayinAddress': 2,
   'pulseCopyPayinExtra': 3
  }
 },
 'xyz.swapee.wc.IOfferExchange': {
  'id': 652933965110,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeBuffer': {
  'id': 652933965111,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeHtmlComponentUtil': {
  'id': 652933965112,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeHtmlComponent': {
  'id': 652933965113,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeElement': {
  'id': 652933965114,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildAddressPopup': 4,
   'buildRefundPopup': 5,
   'buildSendPaymentCollapsar': 6,
   'buildCreateTransactionCollapsar': 7,
   'buildProgressColumn': 8,
   'buildSwapeeWalletPicker': 9,
   'buildExchangeBroker': 10,
   'buildPaymentLastChecked': 11,
   'short': 12,
   'server': 13,
   'inducer': 14,
   'buildExchangeStatusHint': 15,
   'buildExchangeStatusTitle': 16,
   'buildFinishedCollapsar': 17,
   'buildDuration': 18,
   'buildProgressColumnMobTop': 19,
   'buildProgressColumnMobBot': 20,
   'buildDealBroker': 21,
   'buildExchangeIntent': 22,
   'buildOfferExchangeInfoRow': 23,
   'buildOfferExchangeInfoRowMob': 24,
   'buildTransactionInfo': 25
  }
 },
 'xyz.swapee.wc.IOfferExchangeElementPort': {
  'id': 652933965115,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeDesigner': {
  'id': 652933965116,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOfferExchangeGPU': {
  'id': 652933965117,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeDisplay': {
  'id': 652933965118,
  'symbols': {},
  'methods': {
   'paint': 1,
   'paintCopyPayinAddress': 4,
   'paintCopyPayinExtra': 5
  }
 },
 'xyz.swapee.wc.OfferExchangeVdusPQs': {
  'id': 652933965119,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOfferExchangeDisplay': {
  'id': 652933965120,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OfferExchangeClassesPQs': {
  'id': 652933965121,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeController': {
  'id': 652933965122,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'selectAddress': 2,
   'selectRefund': 3,
   'onSelectAddress': 4,
   'onSelectRefund': 5,
   'setTocAccepted': 6,
   'unsetTocAccepted': 7,
   'pulseCopyPayinAddress': 9,
   'pulseCopyPayinExtra': 10,
   'flipTocAccepted': 11,
   'reset': 12,
   'onReset': 13,
   'swapAddresses': 14,
   'onSwapAddresses': 15
  }
 },
 'xyz.swapee.wc.front.IOfferExchangeController': {
  'id': 652933965123,
  'symbols': {},
  'methods': {
   'selectAddress': 1,
   'selectRefund': 2,
   'onSelectAddress': 3,
   'onSelectRefund': 4,
   'setTocAccepted': 5,
   'unsetTocAccepted': 6,
   'pulseCopyPayinAddress': 8,
   'pulseCopyPayinExtra': 9,
   'flipTocAccepted': 10,
   'reset': 11,
   'onReset': 12,
   'swapAddresses': 13,
   'onSwapAddresses': 14
  }
 },
 'xyz.swapee.wc.back.IOfferExchangeController': {
  'id': 652933965124,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeControllerAR': {
  'id': 652933965125,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeControllerAT': {
  'id': 652933965126,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeTouchscreen': {
  'id': 652933965127,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeTouchscreen': {
  'id': 652933965128,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeTouchscreenAR': {
  'id': 652933965129,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeTouchscreenAT': {
  'id': 652933965130,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferExchangeQueriesPQs': {
  'id': 652933965131,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeControllerHyperslice': {
  'id': 652933965132,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeControllerBindingHyperslice': {
  'id': 652933965133,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeControllerHyperslice': {
  'id': 652933965134,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeControllerBindingHyperslice': {
  'id': 652933965135,
  'symbols': {},
  'methods': {}
 }
})