import {OfferExchangeMemoryPQs} from './OfferExchangeMemoryPQs'
export const OfferExchangeMemoryQPs=/**@type {!xyz.swapee.wc.OfferExchangeMemoryQPs}*/(Object.keys(OfferExchangeMemoryPQs)
 .reduce((a,k)=>{a[OfferExchangeMemoryPQs[k]]=k;return a},{}))