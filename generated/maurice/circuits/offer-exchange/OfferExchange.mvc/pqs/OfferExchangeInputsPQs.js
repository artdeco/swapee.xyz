import {OfferExchangeMemoryPQs} from './OfferExchangeMemoryPQs'
export const OfferExchangeInputsPQs=/**@type {!xyz.swapee.wc.OfferExchangeInputsQPs}*/({
 ...OfferExchangeMemoryPQs,
})