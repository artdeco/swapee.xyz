import {OfferExchangeInputsPQs} from './OfferExchangeInputsPQs'
export const OfferExchangeInputsQPs=/**@type {!xyz.swapee.wc.OfferExchangeInputsQPs}*/(Object.keys(OfferExchangeInputsPQs)
 .reduce((a,k)=>{a[OfferExchangeInputsPQs[k]]=k;return a},{}))