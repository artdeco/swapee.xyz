export const OfferExchangeMemoryPQs=/**@type {!xyz.swapee.wc.OfferExchangeMemoryPQs}*/({
 tocAccepted:'27b63',
 step:'2764c',
 paymentFailed:'02236',
 paymentCompleted:'497c0',
 pulseCopyPaymentAddress:'082ae',
 copyPayinAddress:'16e61',
 copyPayinExtra:'4bb1a',
 payinAddressCopied:'141c4',
 payinAddressJustCopied:'45cad',
 kycEmail:'d5b6f',
})