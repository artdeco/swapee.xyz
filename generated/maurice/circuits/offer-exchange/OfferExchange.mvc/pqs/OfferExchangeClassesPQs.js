const OfferExchangeClassesPQs=/**@type {!xyz.swapee.wc.OfferExchangeClassesPQs}*/({
 Loading:'16bfb',
 InputActive:'f21bb',
 ProgressVertLine:'a7a04',
 StepLabelWaiting:'e4d45',
 CircleLoading:'5e522',
 CircleComplete:'a5ad9',
 CircleWaiting:'9912b',
 PaymentReceived:'bb3b5',
 HintRed:'b940b',
 Copied:'a588b',
 CopiedRemoving:'4d2b6',
})
export default OfferExchangeClassesPQs