import {OfferExchangeQueriesPQs} from './OfferExchangeQueriesPQs'
export const OfferExchangeQueriesQPs=/**@type {!xyz.swapee.wc.OfferExchangeQueriesQPs}*/(Object.keys(OfferExchangeQueriesPQs)
 .reduce((a,k)=>{a[OfferExchangeQueriesPQs[k]]=k;return a},{}))