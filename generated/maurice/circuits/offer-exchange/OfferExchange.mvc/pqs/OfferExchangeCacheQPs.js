import {OfferExchangeCachePQs} from './OfferExchangeCachePQs'
export const OfferExchangeCacheQPs=/**@type {!xyz.swapee.wc.OfferExchangeCacheQPs}*/(Object.keys(OfferExchangeCachePQs)
 .reduce((a,k)=>{a[OfferExchangeCachePQs[k]]=k;return a},{}))