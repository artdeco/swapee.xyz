import OfferExchangeClassesPQs from './OfferExchangeClassesPQs'
export const OfferExchangeClassesQPs=/**@type {!xyz.swapee.wc.OfferExchangeClassesQPs}*/(Object.keys(OfferExchangeClassesPQs)
 .reduce((a,k)=>{a[OfferExchangeClassesPQs[k]]=k;return a},{}))