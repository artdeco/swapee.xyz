import {OfferExchangeVdusPQs} from './OfferExchangeVdusPQs'
export const OfferExchangeVdusQPs=/**@type {!xyz.swapee.wc.OfferExchangeVdusQPs}*/(Object.keys(OfferExchangeVdusPQs)
 .reduce((a,k)=>{a[OfferExchangeVdusPQs[k]]=k;return a},{}))