import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {OfferExchangeInputsPQs} from '../../pqs/OfferExchangeInputsPQs'
import {OfferExchangeOuterCoreConstructor} from '../OfferExchangeCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangePort}
 */
function __OfferExchangePort() {}
__OfferExchangePort.prototype = /** @type {!_OfferExchangePort} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangePort} */ function OfferExchangePortConstructor() {
  const self=/** @type {!xyz.swapee.wc.OfferExchangeOuterCore} */ ({model:null})
  OfferExchangeOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangePort}
 */
class _OfferExchangePort { }
/**
 * The port that serves as an interface to the _IOfferExchange_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractOfferExchangePort} ‎
 */
export class OfferExchangePort extends newAbstract(
 _OfferExchangePort,65293396515,OfferExchangePortConstructor,{
  asIOfferExchangePort:1,
  superOfferExchangePort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangePort} */
OfferExchangePort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangePort} */
function OfferExchangePortClass(){}

export const OfferExchangePortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IOfferExchange.Pinout>}*/({
 get Port() { return OfferExchangePort },
})

OfferExchangePort[$implementations]=[
 OfferExchangePortClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangePort}*/({
  resetPort(){
   this.resetOfferExchangePort()
  },
  resetOfferExchangePort(){
   OfferExchangePortConstructor.call(this)
  },
 }),
 __OfferExchangePort,
 Parametric,
 OfferExchangePortClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangePort}*/({
  constructor(){
   mountPins(this.inputs,OfferExchangeInputsPQs)
  },
 }),
]


export default OfferExchangePort