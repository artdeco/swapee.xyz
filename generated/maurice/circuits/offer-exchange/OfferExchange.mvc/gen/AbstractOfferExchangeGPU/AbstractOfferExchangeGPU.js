import AbstractOfferExchangeDisplay from '../AbstractOfferExchangeDisplayBack'
import OfferExchangeClassesPQs from '../../pqs/OfferExchangeClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {OfferExchangeClassesQPs} from '../../pqs/OfferExchangeClassesQPs'
import {OfferExchangeVdusPQs} from '../../pqs/OfferExchangeVdusPQs'
import {OfferExchangeVdusQPs} from '../../pqs/OfferExchangeVdusQPs'
import {OfferExchangeMemoryPQs} from '../../pqs/OfferExchangeMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeGPU}
 */
function __AbstractOfferExchangeGPU() {}
__AbstractOfferExchangeGPU.prototype = /** @type {!_AbstractOfferExchangeGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeGPU}
 */
class _AbstractOfferExchangeGPU { }
/**
 * Handles the periphery of the _IOfferExchangeDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeGPU} ‎
 */
class AbstractOfferExchangeGPU extends newAbstract(
 _AbstractOfferExchangeGPU,652933965117,null,{
  asIOfferExchangeGPU:1,
  superOfferExchangeGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeGPU} */
AbstractOfferExchangeGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeGPU} */
function AbstractOfferExchangeGPUClass(){}

export default AbstractOfferExchangeGPU


AbstractOfferExchangeGPU[$implementations]=[
 __AbstractOfferExchangeGPU,
 AbstractOfferExchangeGPUClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeGPU}*/({
  classesQPs:OfferExchangeClassesQPs,
  vdusPQs:OfferExchangeVdusPQs,
  vdusQPs:OfferExchangeVdusQPs,
  memoryPQs:OfferExchangeMemoryPQs,
 }),
 AbstractOfferExchangeDisplay,
 BrowserView,
 AbstractOfferExchangeGPUClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeGPU}*/({
  allocator(){
   pressFit(this.classes,'',OfferExchangeClassesPQs)
  },
 }),
]