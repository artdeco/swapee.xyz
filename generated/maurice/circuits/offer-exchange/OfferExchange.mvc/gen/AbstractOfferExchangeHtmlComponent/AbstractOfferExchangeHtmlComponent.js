import AbstractOfferExchangeGPU from '../AbstractOfferExchangeGPU'
import AbstractOfferExchangeTouchscreenBack from '../AbstractOfferExchangeTouchscreenBack'
import {HtmlComponent,Landed,Shorter,mvc} from '@webcircuits/webcircuits'
import {OfferExchangeInputsQPs} from '../../pqs/OfferExchangeInputsQPs'
import { newAbstract, $implementations, create, schedule } from '@type.engineering/type-engineer'
import AbstractOfferExchange from '../AbstractOfferExchange'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeHtmlComponent}
 */
function __AbstractOfferExchangeHtmlComponent() {}
__AbstractOfferExchangeHtmlComponent.prototype = /** @type {!_AbstractOfferExchangeHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeHtmlComponent}
 */
class _AbstractOfferExchangeHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.OfferExchangeHtmlComponent} */ (res)
  }
}
/**
 * The _IOfferExchange_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeHtmlComponent} ‎
 */
export class AbstractOfferExchangeHtmlComponent extends newAbstract(
 _AbstractOfferExchangeHtmlComponent,652933965113,null,{
  asIOfferExchangeHtmlComponent:1,
  superOfferExchangeHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeHtmlComponent} */
AbstractOfferExchangeHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeHtmlComponent} */
function AbstractOfferExchangeHtmlComponentClass(){}


AbstractOfferExchangeHtmlComponent[$implementations]=[
 __AbstractOfferExchangeHtmlComponent,
 HtmlComponent,
 AbstractOfferExchange,
 AbstractOfferExchangeGPU,
 AbstractOfferExchangeTouchscreenBack,
 Landed,
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  constructor(){
   this.land={
    AddressPopup:null,
    RefundPopup:null,
    SendPaymentCollapsar:null,
    CreateTransactionCollapsar:null,
    FinishedCollapsar:null,
    ProgressColumn:null,
    ProgressColumnMobTop:null,
    ProgressColumnMobBot:null,
    SwapeeWalletPicker:null,
    ExchangeBroker:null,
    DealBroker:null,
    ExchangeIntent:null,
    ExchangeStatusHint:null,
    TransactionInfo:null,
    OfferExchangeInfoRow:null,
    OfferExchangeInfoRowMob:null,
    ExchangeStatusTitle:null,
    PaymentLastChecked:null,
    Duration:null,
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  inputsQPs:OfferExchangeInputsQPs,
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_checked_on_TocIn({tocAccepted:tocAccepted}){
   const{asIOfferExchangeGPU:{TocIn:TocIn},asIBrowserView:{attr:attr}}=this
   attr(TocIn,'checked',(tocAccepted)||null)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_disabled_on_CreateTransactionBu({canCreateTransaction:canCreateTransaction}){
   const{asIOfferExchangeGPU:{CreateTransactionBu:CreateTransactionBu},asIBrowserView:{attr:attr}}=this
   attr(CreateTransactionBu,'disabled',(!canCreateTransaction)||null)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_Copied_on_PayinAddressCopiedLa({payinAddressCopied:payinAddressCopied}){
   const{
    asIOfferExchangeGPU:{
     PayinAddressCopiedLa:PayinAddressCopiedLa,
    },
    classes:{Copied:Copied},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(payinAddressCopied) addClass(PayinAddressCopiedLa,Copied)
   else removeClass(PayinAddressCopiedLa,Copied)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_CopiedRemoving_on_PayinAddressCopiedLa({payinAddressJustCopied:payinAddressJustCopied}){
   const{
    asIOfferExchangeGPU:{
     PayinAddressCopiedLa:PayinAddressCopiedLa,
    },
    classes:{CopiedRemoving:CopiedRemoving},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(payinAddressJustCopied) addClass(PayinAddressCopiedLa,CopiedRemoving)
   else removeClass(PayinAddressCopiedLa,CopiedRemoving)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_Loading_on_CreateTransactionBu(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '89fc6':creatingTransaction,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{
     CreateTransactionBu:CreateTransactionBu,
    },
    classes:{Loading:Loading},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(creatingTransaction) addClass(CreateTransactionBu,Loading)
   else removeClass(CreateTransactionBu,Loading)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function interrupt_click_on_CreateTransactionBu(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   const{asIOfferExchangeGPU:{CreateTransactionBu:CreateTransactionBu}}=this
   CreateTransactionBu.listen('click',(ev)=>{
    ExchangeBroker['2e29f']()
   })
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_CreateTransactionErrorWr(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '601f8':createTransactionError,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{CreateTransactionErrorWr:CreateTransactionErrorWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(CreateTransactionErrorWr,createTransactionError)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_CreateTransactionError_Content(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '601f8':createTransactionError,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{CreateTransactionError:CreateTransactionError}}=this
   CreateTransactionError.setText(createTransactionError)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_KycRequired(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '2b803':kycRequired,
    '9acb4':status,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{KycRequireds:KycRequireds},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(KycRequireds,kycRequired===true||status=='hold')
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_KycNotRequired(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '2b803':kycRequired,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{KycNotRequireds:KycNotRequireds},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(KycNotRequireds,kycRequired===false)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_value_on_AddressIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '884d9':address,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{AddressIn:AddressIn}}=this
   AddressIn.val(address)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function interrupt_input_on_AddressIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   const{asIOfferExchangeGPU:{AddressIn:AddressIn}}=this
   AddressIn.listen('input',(ev)=>{
    ExchangeBroker['c73ca'](ev.target.value)
   },false,{target:{value:1}})
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_value_on_RefundAddressIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'be03a':refundAddress,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{RefundAddressIn:RefundAddressIn}}=this
   RefundAddressIn.val(refundAddress)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function interrupt_input_on_RefundAddressIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   const{asIOfferExchangeGPU:{RefundAddressIn:RefundAddressIn}}=this
   RefundAddressIn.listen('input',(ev)=>{
    ExchangeBroker['3e295'](ev.target.value)
   },false,{target:{value:1}})
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_PaymentAddressInWr(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'b978a':paymentExpiredOrOverdue,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{PaymentAddressInWr:PaymentAddressInWr},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(PaymentAddressInWr,paymentExpiredOrOverdue)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_value_on_PaymentAddressIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '59af8':payinAddress,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{PaymentAddressIn:PaymentAddressIn}}=this
   PaymentAddressIn.val(payinAddress)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_disabled_on_PaymentAddressIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '37b54':waitingForPayment,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{PaymentAddressIn:PaymentAddressIn},asIBrowserView:{attr:attr}}=this
   attr(PaymentAddressIn,'disabled',(!waitingForPayment)||null)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_disabled_on_CheckStatusBu(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'adfbe':checkingStatus,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{CheckStatusBu:CheckStatusBu},asIBrowserView:{attr:attr}}=this
   attr(CheckStatusBu,'disabled',(checkingStatus)||null)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_Loading_on_CheckStatusBu(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'adfbe':checkingStatus,
    '9acb4':status,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{
     CheckStatusBu:CheckStatusBu,
    },
    classes:{Loading:Loading},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(checkingStatus&&status!='new') addClass(CheckStatusBu,Loading)
   else removeClass(CheckStatusBu,Loading)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_ifNot_$conceal_CheckStatusBu(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'b978a':paymentExpiredOrOverdue,
    '37b54':waitingForPayment,
    '355f3':processingPayment,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{CheckStatusBu:CheckStatusBu},
    asIBrowserView:{conceal:conceal,reveal:reveal},
   }=this
   const state={paymentExpiredOrOverdue:paymentExpiredOrOverdue,waitingForPayment:waitingForPayment,processingPayment:processingPayment}
   ;({paymentExpiredOrOverdue:paymentExpiredOrOverdue,waitingForPayment:waitingForPayment,processingPayment:processingPayment}=state)
   if(paymentExpiredOrOverdue) conceal(CheckStatusBu,true)
   else reveal(CheckStatusBu,waitingForPayment||processingPayment)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function interrupt_click_on_CheckStatusBu(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   const{asIOfferExchangeGPU:{CheckStatusBu:CheckStatusBu}}=this
   CheckStatusBu.listen('click',(ev)=>{
    ExchangeBroker['b71b0']()
   })
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_SendPaymentErrorWr(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'afbad':checkPaymentError,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{SendPaymentErrorWr:SendPaymentErrorWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(SendPaymentErrorWr,checkPaymentError)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_SendPaymentError_Content(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'afbad':checkPaymentError,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{SendPaymentError:SendPaymentError}}=this
   SendPaymentError.setText(checkPaymentError)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_value_on_PaymentAddressHiddenIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '59af8':payinAddress,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{PaymentAddressHiddenIn:PaymentAddressHiddenIn}}=this
   PaymentAddressHiddenIn.val('*'.repeat(payinAddress.length))
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_PaymentAddressHiddenIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'b978a':paymentExpiredOrOverdue,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{PaymentAddressHiddenIn:PaymentAddressHiddenIn},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(PaymentAddressHiddenIn,paymentExpiredOrOverdue)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_CheckedWr(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'adfbe':checkingStatus,
    'a99c4':exchangeComplete,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{CheckedWr:CheckedWr},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(CheckedWr,checkingStatus||exchangeComplete)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_HintRed_on_ExchangeStatusHintWr(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '9acb4':status,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{
     ExchangeStatusHintWr:ExchangeStatusHintWr,
    },
    classes:{HintRed:HintRed},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(['failed','refunded','overdue','hold','expired'].includes(status)) addClass(ExchangeStatusHintWr,HintRed)
   else removeClass(ExchangeStatusHintWr,HintRed)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_ExchangeStatusHintWr(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '9acb4':status,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{ExchangeStatusHintWr:ExchangeStatusHintWr},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(ExchangeStatusHintWr,status=='new')
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_CheckingLoIn(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'adfbe':checkingStatus,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{CheckingLoIn:CheckingLoIn},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(CheckingLoIn,checkingStatus)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_ConfirmedAmountFrom_Content(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '6103f':confirmedAmountFrom,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{ConfirmedAmountFrom:ConfirmedAmountFrom}}=this
   ConfirmedAmountFrom.setText(confirmedAmountFrom)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_StatusLa_Content(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '9acb4':status,
   }=ExchangeBroker.model
   const{asIOfferExchangeGPU:{StatusLa:StatusLa}}=this
   StatusLa.setText(status)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_RestartBu(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'b978a':paymentExpiredOrOverdue,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{RestartBu:RestartBu},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(RestartBu,paymentExpiredOrOverdue)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_PaymentReceived_on_SendFo(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '37b54':waitingForPayment,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{
     SendFo:SendFo,
    },
    classes:{PaymentReceived:PaymentReceived},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(!waitingForPayment) addClass(SendFo,PaymentReceived)
   else removeClass(SendFo,PaymentReceived)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_SendFo(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '39957':paymentReceived,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{SendFo:SendFo},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(SendFo,paymentReceived)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_PaymentSentLa(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '37b54':waitingForPayment,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{PaymentSentLa:PaymentSentLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(PaymentSentLa,waitingForPayment)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_UpdateStatusLa(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '355f3':processingPayment,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{UpdateStatusLa:UpdateStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(UpdateStatusLa,processingPayment)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_Step2CoMobTop(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '490aa':Id,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step2CoMobTop:Step2CoMobTop},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(Step2CoMobTop,Id)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_Step3CoMobTop(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'a99c4':exchangeComplete,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step3CoMobTop:Step3CoMobTop},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(Step3CoMobTop,exchangeComplete)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_Step1CoMobBot(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '490aa':Id,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step1CoMobBot:Step1CoMobBot},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(Step1CoMobBot,Id)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_Step1ProgressCircleWrMobBot(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '0b951':notId,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step1ProgressCircleWrMobBot:Step1ProgressCircleWrMobBot},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(Step1ProgressCircleWrMobBot,notId)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_Step1LaMobBot(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '0b951':notId,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step1LaMobBot:Step1LaMobBot},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(Step1LaMobBot,notId)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_Step2ProgressCircleWrMobBot(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '490aa':Id,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step2ProgressCircleWrMobBot:Step2ProgressCircleWrMobBot},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(Step2ProgressCircleWrMobBot,Id)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_Step2LaMobBot(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    '490aa':Id,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step2LaMobBot:Step2LaMobBot},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(Step2LaMobBot,Id)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_Step2CoMobBot(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'a99c4':exchangeComplete,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step2CoMobBot:Step2CoMobBot},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(Step2CoMobBot,exchangeComplete)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $conceal_Step3CoMobBot(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   let{
    'a99c4':exchangeComplete,
   }=ExchangeBroker.model
   const{
    asIOfferExchangeGPU:{Step3CoMobBot:Step3CoMobBot},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(Step3CoMobBot,exchangeComplete)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_RequiredLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'cec31':fixed,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeGPU:{RequiredLa:RequiredLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(RequiredLa,fixed)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $show_RequiredLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeGPU:{RequiredLa:RequiredLa},
    asIBrowserView:{style:style},
   }=this
   style(RequiredLa,'opacity',ready?null:0,true)
   style(RequiredLa,'pointer-events',ready?null:'none',true)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_OptionalLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'cec31':fixed,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeGPU:{OptionalLa:OptionalLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(OptionalLa,!fixed)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $show_OptionalLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeGPU:{OptionalLa:OptionalLa},
    asIBrowserView:{style:style},
   }=this
   style(OptionalLa,'opacity',ready?null:0,true)
   style(OptionalLa,'pointer-events',ready?null:'none',true)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_CurrencyFromLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeGPU:{CurrencyFromLas:CurrencyFromLas},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(CurrencyFromLas,ready)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_CurrencyFromLas_Content(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    '96c88':currencyFrom,
   }=ExchangeIntent.model
   const{asIOfferExchangeGPU:{CurrencyFromLas:CurrencyFromLas}}=this
   for(const CurrencyFromLa of CurrencyFromLas){
    CurrencyFromLa.setText(currencyFrom)
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_CurrencyToLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeGPU:{CurrencyToLas:CurrencyToLas},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(CurrencyToLas,ready)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_CurrencyToLas_Content(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'c23cd':currencyTo,
   }=ExchangeIntent.model
   const{asIOfferExchangeGPU:{CurrencyToLas:CurrencyToLas}}=this
   for(const CurrencyToLa of CurrencyToLas){
    CurrencyToLa.setText(currencyTo)
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function $reveal_TransactionCurrencyFromLa(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96c88':currencyFrom,
   }=TransactionInfo.model
   const{
    asIOfferExchangeGPU:{TransactionCurrencyFromLa:TransactionCurrencyFromLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(TransactionCurrencyFromLa,currencyFrom)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_TransactionCurrencyFromLa_Content(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '96c88':currencyFrom,
   }=TransactionInfo.model
   const{asIOfferExchangeGPU:{TransactionCurrencyFromLa:TransactionCurrencyFromLa}}=this
   TransactionCurrencyFromLa.setText(currencyFrom)
  },
 }),
 Shorter,
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete,build:build},
    asIOfferExchangeGPU:{
     AddressPopup:AddressPopup,
     RefundPopup:RefundPopup,
     SendPaymentCollapsar:SendPaymentCollapsar,
     CreateTransactionCollapsar:CreateTransactionCollapsar,
     FinishedCollapsar:FinishedCollapsar,
     ProgressColumn:ProgressColumn,
     ProgressColumnMobTop:ProgressColumnMobTop,
     ProgressColumnMobBot:ProgressColumnMobBot,
     SwapeeWalletPicker:SwapeeWalletPicker,
     ExchangeBroker:ExchangeBroker,
     DealBroker:DealBroker,
     ExchangeIntent:ExchangeIntent,
     ExchangeStatusHint:ExchangeStatusHint,
     OfferExchangeInfoRow:OfferExchangeInfoRow,
     OfferExchangeInfoRowMob:OfferExchangeInfoRowMob,
     ExchangeStatusTitle:ExchangeStatusTitle,
     PaymentLastChecked:PaymentLastChecked,
     Duration:Duration,
    },
   }=this
   complete(2166517291,{AddressPopup:AddressPopup})
   complete(2166517291,{RefundPopup:RefundPopup})
   complete(2854970513,{SendPaymentCollapsar:SendPaymentCollapsar},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['3338c'](){ // -> collapsed
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const notId=_ExchangeBroker.model['0b951'] // <- notId
     return notId
    },
   })
   complete(2854970513,{CreateTransactionCollapsar:CreateTransactionCollapsar},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['3338c'](){ // -> collapsed
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const Id=_ExchangeBroker.model['490aa'] // <- Id
     return Id
    },
   })
   complete(2854970513,{FinishedCollapsar:FinishedCollapsar},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['1a613'](){ // -> expanded
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const exchangeFinished=_ExchangeBroker.model['bb39a'] // <- exchangeFinished
     return exchangeFinished
    },
   })
   build(6526983971,{ProgressColumn:ProgressColumn},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['da35f'](){ // -> loadingStep1
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const creatingTransaction=_ExchangeBroker.model['89fc6'] // <- creatingTransaction
     return creatingTransaction
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['277b1'](){ // -> loadingStep2
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const checkingPaymentStatus=_ExchangeBroker.model['b0696'] // <- checkingPaymentStatus
     return checkingPaymentStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['d7265'](){ // -> loadingStep3
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const checkingExchangeStatus=_ExchangeBroker.model['6832e'] // <- checkingExchangeStatus
     return checkingExchangeStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['67113'](){ // -> transactionId
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const id=_ExchangeBroker.model['b80bb'] // <- id
     return id
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['497c0'](){ // -> paymentCompleted
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentCompleted=_ExchangeBroker.model['497c0'] // <- paymentCompleted
     return paymentCompleted
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['b5666'](){ // -> paymentStatus
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentStatus=_ExchangeBroker.model['b5666'] // <- paymentStatus
     return paymentStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['9acb4'](){ // -> status
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const status=_ExchangeBroker.model['9acb4'] // <- status
     return status
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['a7265'](){ // -> finishedOnPayment
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentExpiredOrOverdue=_ExchangeBroker.model['b978a'] // <- paymentExpiredOrOverdue
     return paymentExpiredOrOverdue
    },
    paymentFailed:'02236', // -> paymentFailed
   })
   build(6526983971,{ProgressColumnMobTop:ProgressColumnMobTop},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['da35f'](){ // -> loadingStep1
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const creatingTransaction=_ExchangeBroker.model['89fc6'] // <- creatingTransaction
     return creatingTransaction
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['277b1'](){ // -> loadingStep2
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const checkingPaymentStatus=_ExchangeBroker.model['b0696'] // <- checkingPaymentStatus
     return checkingPaymentStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['d7265'](){ // -> loadingStep3
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const checkingExchangeStatus=_ExchangeBroker.model['6832e'] // <- checkingExchangeStatus
     return checkingExchangeStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['67113'](){ // -> transactionId
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const id=_ExchangeBroker.model['b80bb'] // <- id
     return id
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['497c0'](){ // -> paymentCompleted
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentCompleted=_ExchangeBroker.model['497c0'] // <- paymentCompleted
     return paymentCompleted
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['b5666'](){ // -> paymentStatus
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentStatus=_ExchangeBroker.model['b5666'] // <- paymentStatus
     return paymentStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['9acb4'](){ // -> status
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const status=_ExchangeBroker.model['9acb4'] // <- status
     return status
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['a7265'](){ // -> finishedOnPayment
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentExpiredOrOverdue=_ExchangeBroker.model['b978a'] // <- paymentExpiredOrOverdue
     return paymentExpiredOrOverdue
    },
    paymentFailed:'02236', // -> paymentFailed
   })
   build(6526983971,{ProgressColumnMobBot:ProgressColumnMobBot},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['da35f'](){ // -> loadingStep1
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const creatingTransaction=_ExchangeBroker.model['89fc6'] // <- creatingTransaction
     return creatingTransaction
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['277b1'](){ // -> loadingStep2
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const checkingPaymentStatus=_ExchangeBroker.model['b0696'] // <- checkingPaymentStatus
     return checkingPaymentStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['d7265'](){ // -> loadingStep3
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const checkingExchangeStatus=_ExchangeBroker.model['6832e'] // <- checkingExchangeStatus
     return checkingExchangeStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['67113'](){ // -> transactionId
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const id=_ExchangeBroker.model['b80bb'] // <- id
     return id
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['497c0'](){ // -> paymentCompleted
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentCompleted=_ExchangeBroker.model['497c0'] // <- paymentCompleted
     return paymentCompleted
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['b5666'](){ // -> paymentStatus
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentStatus=_ExchangeBroker.model['b5666'] // <- paymentStatus
     return paymentStatus
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['9acb4'](){ // -> status
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const status=_ExchangeBroker.model['9acb4'] // <- status
     return status
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['a7265'](){ // -> finishedOnPayment
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const paymentExpiredOrOverdue=_ExchangeBroker.model['b978a'] // <- paymentExpiredOrOverdue
     return paymentExpiredOrOverdue
    },
    paymentFailed:'02236', // -> paymentFailed
   })
   build(5141253786,{SwapeeWalletPicker:SwapeeWalletPicker})
   build(1683059665,{ExchangeBroker:ExchangeBroker})
   complete(3427226314,{DealBroker:DealBroker})
   complete(7833868048,{ExchangeIntent:ExchangeIntent})
   build(2700153076,{ExchangeStatusHint:ExchangeStatusHint},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['9acb4'](){ // -> status
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const status=_ExchangeBroker.model['9acb4'] // <- status
     return status
    },
   })
   complete(8711883565,{OfferExchangeInfoRow:OfferExchangeInfoRow})
   complete(8711883565,{OfferExchangeInfoRowMob:OfferExchangeInfoRowMob})
   build(7837799807,{ExchangeStatusTitle:ExchangeStatusTitle},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['9acb4'](){ // -> status
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const status=_ExchangeBroker.model['9acb4'] // <- status
     return status
    },
   })
   build(6947859297,{PaymentLastChecked:PaymentLastChecked},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['d5e14'](){ // -> refDate
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const statusLastChecked=_ExchangeBroker.model['43b98'] // <- statusLastChecked
     return statusLastChecked
    },
   })
   complete(2324158635,{Duration:Duration},{
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['e4dfb'](){ // -> startDate
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const createdDate=_ExchangeBroker.model['272bc'] // <- createdDate
     return createdDate
    },
    /**@this {xyz.swapee.wc.IOfferExchangeHtmlComponent}*/
    get['813e9'](){ // -> endDate
     const{land:{ExchangeBroker:_ExchangeBroker}}=this
     if(!_ExchangeBroker) return void 0
     const finishedDate=_ExchangeBroker.model['bd1a8'] // <- finishedDate
     return finishedDate
    },
   })
  },
  short:function shortTransactionInfo({ExchangeBroker:_ExchangeBroker}){
   if(!_ExchangeBroker) return
   if([undefined,null].includes(_ExchangeBroker.model['b80bb'])) return // id
   const{
    asIHtmlComponent:{build:build},
    asIOfferExchangeGPU:{
     TransactionInfo:TransactionInfo,
    },
   }=this
   build(1988051819,{TransactionInfo:TransactionInfo})
   return true
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint(_,{ExchangeBroker:ExchangeBroker}){
   const land={ExchangeBroker:ExchangeBroker}
   if(!land.ExchangeBroker) return
   this.onSelectAddress=ExchangeBroker['c73ca'] // onSelectAddress=setAddress
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   this.onSwapAddresses=()=>{
    const setAddress=/**@type {xyz.swapee.wc.IExchangeBrokerController.setAddress}*/(ExchangeBroker['c73ca'])
    setAddress(this.land.ExchangeBroker?this.land.ExchangeBroker.model['be03a']:void 0)
   }
   this.onSwapAddresses=()=>{
    const setRefundAddress=/**@type {xyz.swapee.wc.IExchangeBrokerController.setRefundAddress}*/(ExchangeBroker['3e295'])
    setRefundAddress(this.land.ExchangeBroker?this.land.ExchangeBroker.model['884d9']:void 0)
   }
   this.onReset=()=>{
    const reset=/**@type {xyz.swapee.wc.IExchangeBrokerController.reset}*/(ExchangeBroker['70605'])
    reset()
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint(_,{AddressPopup:AddressPopup}){
   if(!AddressPopup) return
   this.onSelectAddress=()=>{
    const hide=/**@type {com.webcircuits.ui.IPopupController.hide}*/(AddressPopup['d671b'])
    hide()
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint(_,{RefundPopup:RefundPopup}){
   if(!RefundPopup) return
   this.onSelectRefund=()=>{
    const hide=/**@type {com.webcircuits.ui.IPopupController.hide}*/(RefundPopup['d671b'])
    hide()
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   this.onReset=()=>{
    const resetPort=/**@type {xyz.swapee.wc.ITransactionInfoController.resetPort}*/(TransactionInfo.resetPort)
    resetPort()
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint(_,{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker||!ExchangeBroker) return
   const id=ExchangeBroker.model['b80bb']
   if(id) {
    ExchangeBroker['b71b0'](id) // pulseCheckPayment(id)
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint(_,{SwapeeWalletPicker:SwapeeWalletPicker,RefundPopup:RefundPopup}){
   if(!SwapeeWalletPicker||!RefundPopup) return
   const invoice=SwapeeWalletPicker.model['e5f96']
   if(invoice) {
    RefundPopup['d671b']() // hide()
   }
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_append_BtcWalletPicker_in_BtcWalletPickerCont({btcInput,btcOutput}){
   const{
    asIOfferExchangeGPU:{BtcWalletPickerCont:BtcWalletPickerCont,BtcWalletPicker:BtcWalletPicker},
   }=this
   if(!btcInput&&!btcOutput) BtcWalletPickerCont.appendChild(BtcWalletPicker)
   else BtcWalletPickerCont.removeChild(BtcWalletPicker)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_append_BtcWalletPicker_in_WalletOut({btcInput}){
   const{
    asIOfferExchangeGPU:{WalletOut:WalletOut,BtcWalletPicker:BtcWalletPicker},
   }=this
   if(btcInput) WalletOut.appendChild(BtcWalletPicker)
   else WalletOut.removeChild(BtcWalletPicker)
  },
 }),
 AbstractOfferExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeHtmlComponent}*/({
  paint:function paint_append_BtcWalletPicker_in_WalletIn({btcOutput}){
   const{
    asIOfferExchangeGPU:{WalletIn:WalletIn,BtcWalletPicker:BtcWalletPicker},
   }=this
   if(btcOutput) WalletIn.appendChild(BtcWalletPicker)
   else WalletIn.removeChild(BtcWalletPicker)
  },
 }),
]