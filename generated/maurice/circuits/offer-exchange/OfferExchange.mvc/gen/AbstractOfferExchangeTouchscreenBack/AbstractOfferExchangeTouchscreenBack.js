import AbstractOfferExchangeTouchscreenAT from '../AbstractOfferExchangeTouchscreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeTouchscreenBack}
 */
function __AbstractOfferExchangeTouchscreenBack() {}
__AbstractOfferExchangeTouchscreenBack.prototype = /** @type {!_AbstractOfferExchangeTouchscreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeTouchscreen}
 */
class _AbstractOfferExchangeTouchscreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeTouchscreen} ‎
 */
class AbstractOfferExchangeTouchscreenBack extends newAbstract(
 _AbstractOfferExchangeTouchscreenBack,652933965128,null,{
  asIOfferExchangeTouchscreen:1,
  superOfferExchangeTouchscreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeTouchscreen} */
AbstractOfferExchangeTouchscreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeTouchscreen} */
function AbstractOfferExchangeTouchscreenBackClass(){}

export default AbstractOfferExchangeTouchscreenBack


AbstractOfferExchangeTouchscreenBack[$implementations]=[
 __AbstractOfferExchangeTouchscreenBack,
 AbstractOfferExchangeTouchscreenAT,
]