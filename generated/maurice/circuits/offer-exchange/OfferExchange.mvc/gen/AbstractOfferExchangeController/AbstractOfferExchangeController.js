import OfferExchangeBuffer from '../OfferExchangeBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {OfferExchangePortConnector} from '../OfferExchangePort'
import {defineEmitters} from '@mauriceguest/guest2'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeController}
 */
function __AbstractOfferExchangeController() {}
__AbstractOfferExchangeController.prototype = /** @type {!_AbstractOfferExchangeController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeController}
 */
class _AbstractOfferExchangeController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeController} ‎
 */
export class AbstractOfferExchangeController extends newAbstract(
 _AbstractOfferExchangeController,652933965122,null,{
  asIOfferExchangeController:1,
  superOfferExchangeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeController} */
AbstractOfferExchangeController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeController} */
function AbstractOfferExchangeControllerClass(){}


AbstractOfferExchangeController[$implementations]=[
 AbstractOfferExchangeControllerClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IOfferExchangePort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractOfferExchangeController,
 AbstractOfferExchangeControllerClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeController}*/({
  reset:precombined,
 }),
 /**@type {!xyz.swapee.wc.IOfferExchangeController}*/({
  calibrate:[
    /**@this {!xyz.swapee.wc.IOfferExchangeController}*/ function calibrateCopyPayinAddress({copyPayinAddress:copyPayinAddress}){
     if(!copyPayinAddress) return
     setTimeout(()=>{
      const{asIOfferExchangeController:{setInputs:setInputs}}=this
      setInputs({
       copyPayinAddress:false,
      })
     },1)
    },
    /**@this {!xyz.swapee.wc.IOfferExchangeController}*/ function calibrateCopyPayinExtra({copyPayinExtra:copyPayinExtra}){
     if(!copyPayinExtra) return
     setTimeout(()=>{
      const{asIOfferExchangeController:{setInputs:setInputs}}=this
      setInputs({
       copyPayinExtra:false,
      })
     },1)
    },
  ],
 }),
 OfferExchangeBuffer,
 IntegratedController,
 /**@type {!AbstractOfferExchangeController}*/(OfferExchangePortConnector),
 AbstractOfferExchangeControllerClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeController}*/({
  constructor(){
   defineEmitters(this,{
    onSelectAddress:true,
    onSwapAddresses:true,
    onSelectRefund:true,
    onReset:true,
   })
  },
 }),
 AbstractOfferExchangeControllerClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeController}*/({
  setTocAccepted(){
   const{asIOfferExchangeController:{setInputs:setInputs}}=this
   setInputs({tocAccepted:true})
  },
  unsetTocAccepted(){
   const{asIOfferExchangeController:{setInputs:setInputs}}=this
   setInputs({tocAccepted:false})
  },
 }),
 AbstractOfferExchangeControllerClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeController}*/({
  selectAddress(address){
   const{asIOfferExchangeController:{onSelectAddress:onSelectAddress}}=this
   onSelectAddress(address)
  },
  swapAddresses(){
   const{asIOfferExchangeController:{onSwapAddresses:onSwapAddresses}}=this
   onSwapAddresses()
  },
  selectRefund(refund){
   const{asIOfferExchangeController:{onSelectRefund:onSelectRefund}}=this
   onSelectRefund(refund)
  },
  reset(){
   const{asIOfferExchangeController:{onReset:onReset}}=this
   onReset()
  },
 }),
]


export default AbstractOfferExchangeController