import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeTouchscreenAR}
 */
function __AbstractOfferExchangeTouchscreenAR() {}
__AbstractOfferExchangeTouchscreenAR.prototype = /** @type {!_AbstractOfferExchangeTouchscreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeTouchscreenAR}
 */
class _AbstractOfferExchangeTouchscreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOfferExchangeTouchscreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeTouchscreenAR} ‎
 */
class AbstractOfferExchangeTouchscreenAR extends newAbstract(
 _AbstractOfferExchangeTouchscreenAR,652933965129,null,{
  asIOfferExchangeTouchscreenAR:1,
  superOfferExchangeTouchscreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeTouchscreenAR} */
AbstractOfferExchangeTouchscreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeTouchscreenAR} */
function AbstractOfferExchangeTouchscreenARClass(){}

export default AbstractOfferExchangeTouchscreenAR


AbstractOfferExchangeTouchscreenAR[$implementations]=[
 __AbstractOfferExchangeTouchscreenAR,
 AR,
 AbstractOfferExchangeTouchscreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IOfferExchangeTouchscreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractOfferExchangeTouchscreenAR}