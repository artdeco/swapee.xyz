import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeControllerAR}
 */
function __AbstractOfferExchangeControllerAR() {}
__AbstractOfferExchangeControllerAR.prototype = /** @type {!_AbstractOfferExchangeControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeControllerAR}
 */
class _AbstractOfferExchangeControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOfferExchangeControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeControllerAR} ‎
 */
class AbstractOfferExchangeControllerAR extends newAbstract(
 _AbstractOfferExchangeControllerAR,652933965125,null,{
  asIOfferExchangeControllerAR:1,
  superOfferExchangeControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeControllerAR} */
AbstractOfferExchangeControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeControllerAR} */
function AbstractOfferExchangeControllerARClass(){}

export default AbstractOfferExchangeControllerAR


AbstractOfferExchangeControllerAR[$implementations]=[
 __AbstractOfferExchangeControllerAR,
 AR,
 AbstractOfferExchangeControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IOfferExchangeControllerAR}*/({
  allocator(){
   this.methods={
    selectAddress:'d2a09',
    swapAddresses:'d98fc',
    selectRefund:'8bf21',
    reset:'a4b41',
    onSelectAddress:'ef440',
    onSwapAddresses:'b8b6b',
    onSelectRefund:'e3d35',
    onReset:'df85f',
    flipTocAccepted:'2ae2e',
    setTocAccepted:'1eda3',
    unsetTocAccepted:'4b605',
    pulseCopyPayinAddress:'1858e',
    pulseCopyPayinExtra:'f4bb5',
   }
  },
 }),
]