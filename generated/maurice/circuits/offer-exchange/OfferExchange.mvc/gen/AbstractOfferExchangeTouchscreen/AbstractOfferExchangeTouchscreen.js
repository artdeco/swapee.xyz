import OfferExchangeClassesPQs from '../../pqs/OfferExchangeClassesPQs'
import AbstractOfferExchangeTouchscreenAR from '../AbstractOfferExchangeTouchscreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {OfferExchangeInputsPQs} from '../../pqs/OfferExchangeInputsPQs'
import {OfferExchangeQueriesPQs} from '../../pqs/OfferExchangeQueriesPQs'
import {OfferExchangeMemoryQPs} from '../../pqs/OfferExchangeMemoryQPs'
import {OfferExchangeCacheQPs} from '../../pqs/OfferExchangeCacheQPs'
import {OfferExchangeVdusPQs} from '../../pqs/OfferExchangeVdusPQs'
import {OfferExchangeClassesQPs} from '../../pqs/OfferExchangeClassesQPs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeTouchscreen}
 */
function __AbstractOfferExchangeTouchscreen() {}
__AbstractOfferExchangeTouchscreen.prototype = /** @type {!_AbstractOfferExchangeTouchscreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeTouchscreen}
 */
class _AbstractOfferExchangeTouchscreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeTouchscreen} ‎
 */
class AbstractOfferExchangeTouchscreen extends newAbstract(
 _AbstractOfferExchangeTouchscreen,652933965127,null,{
  asIOfferExchangeTouchscreen:1,
  superOfferExchangeTouchscreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeTouchscreen} */
AbstractOfferExchangeTouchscreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeTouchscreen} */
function AbstractOfferExchangeTouchscreenClass(){}

export default AbstractOfferExchangeTouchscreen


AbstractOfferExchangeTouchscreen[$implementations]=[
 __AbstractOfferExchangeTouchscreen,
 AbstractOfferExchangeTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeTouchscreen}*/({
  deduceInputs(){
   const{asIOfferExchangeDisplay:{
    TocIn:TocIn,
   }}=this
   return{
    tocAccepted:TocIn?.checked,
   }
  },
 }),
 AbstractOfferExchangeTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeTouchscreen}*/({
  inputsPQs:OfferExchangeInputsPQs,
  classesPQs:OfferExchangeClassesPQs,
  queriesPQs:OfferExchangeQueriesPQs,
  memoryQPs:OfferExchangeMemoryQPs,
  cacheQPs:OfferExchangeCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractOfferExchangeTouchscreenAR,
 AbstractOfferExchangeTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeTouchscreen}*/({
  vdusPQs:OfferExchangeVdusPQs,
 }),
 AbstractOfferExchangeTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeTouchscreen}*/({
  classesQPs:OfferExchangeClassesQPs,
 }),
 AbstractOfferExchangeTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeTouchscreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const AddressIn=this.AddressIn
    if(AddressIn){
     AddressIn.addEventListener('focus',(ev)=>{
      this.atInputFocus(/**@type {!FocusEvent}*/(ev))
     })
     AddressIn.addEventListener('blur',(ev)=>{
      this.atInputBlur(/**@type {!FocusEvent}*/(ev))
     })
    }
    const RefundAddressIn=this.RefundAddressIn
    if(RefundAddressIn){
     RefundAddressIn.addEventListener('focus',(ev)=>{
      this.atInputFocus(/**@type {!FocusEvent}*/(ev))
     })
     RefundAddressIn.addEventListener('blur',(ev)=>{
      this.atInputBlur(/**@type {!FocusEvent}*/(ev))
     })
    }
    const TocIn=this.TocIn
    if(TocIn){
     TocIn.addEventListener('change',(ev)=>{
      ev.target.checked?this.setTocAccepted():this.unsetTocAccepted()
     })
    }
    const CopyPayinAddressBu=this.CopyPayinAddressBu
    if(CopyPayinAddressBu){
     CopyPayinAddressBu.addEventListener('click',(ev)=>{
     ev.preventDefault()
      this.pulseCopyPayinAddress()
     return false
     })
    }
    const PaymentAddressIn=this.PaymentAddressIn
    if(PaymentAddressIn){
     PaymentAddressIn.addEventListener('click',(ev)=>{
      ev.target.select()
     })
    }
    const RestartBu=this.RestartBu
    if(RestartBu){
     RestartBu.addEventListener('click',(ev)=>{
     ev.preventDefault()
      this.reset()
     return false
     })
    }
    const AddressPopup=this.AddressPopup
    if(AddressPopup){
     AddressPopup.addEventListener('click',(ev)=>{
      this.atAddressPopupClick(/**@type {!MouseEvent}*/(ev))
     })
    }
   })
  },
 }),
]