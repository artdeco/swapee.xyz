import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeDisplay}
 */
function __AbstractOfferExchangeDisplay() {}
__AbstractOfferExchangeDisplay.prototype = /** @type {!_AbstractOfferExchangeDisplay} */ ({ })
/** @this {xyz.swapee.wc.back.OfferExchangeDisplay} */ function OfferExchangeDisplayConstructor() {
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.CurrencyFromLas=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.KycRequireds=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.KycNotRequireds=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.ExchangeEmails=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.CurrencyToLas=[]
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeDisplay}
 */
class _AbstractOfferExchangeDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeDisplay} ‎
 */
class AbstractOfferExchangeDisplay extends newAbstract(
 _AbstractOfferExchangeDisplay,652933965120,OfferExchangeDisplayConstructor,{
  asIOfferExchangeDisplay:1,
  superOfferExchangeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeDisplay} */
AbstractOfferExchangeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeDisplay} */
function AbstractOfferExchangeDisplayClass(){}

export default AbstractOfferExchangeDisplay


AbstractOfferExchangeDisplay[$implementations]=[
 __AbstractOfferExchangeDisplay,
 GraphicsDriverBack,
 AbstractOfferExchangeDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IOfferExchangeDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IOfferExchangeDisplay}*/({
    BtcWalletPickerCont:twinMock,
    WalletOut:twinMock,
    WalletIn:twinMock,
    AddressIn:twinMock,
    RefundAddressIn:twinMock,
    TocIn:twinMock,
    CreateTransactionBu:twinMock,
    CopyPayinAddressBu:twinMock,
    PaymentAddressIn:twinMock,
    PayinAddressCopiedLa:twinMock,
    RestartBu:twinMock,
    AddressPopup:twinMock,
    TransactionCurrencyFromLa:twinMock,
    ConfirmedAmountFrom:twinMock,
    ExchangeApiWr:twinMock,
    PaymentAddressHiddenIn:twinMock,
    AddReviewErrorWr:twinMock,
    AddReviewErrorLa:twinMock,
    AddReviewBu:twinMock,
    BtcWalletPicker:twinMock,
    OfferExchangeInfoRowMobWr:twinMock,
    OptionalLa:twinMock,
    RequiredLa:twinMock,
    CreateTransactionWr:twinMock,
    TransactionInfoWr:twinMock,
    TransactionInfo:twinMock,
    TransactionInfoHeadWr:twinMock,
    TransactionInfoHead:twinMock,
    TransactionInfoHeadMobWr:twinMock,
    TransactionInfoHeadMob:twinMock,
    Step1ProgressCircleWrMobBot:twinMock,
    Step1LaMobBot:twinMock,
    Step2ProgressCircleWrMobBot:twinMock,
    Step2LaMobBot:twinMock,
    Step2CoMobTop:twinMock,
    Step3CoMobTop:twinMock,
    Step1CoMobBot:twinMock,
    Step2CoMobBot:twinMock,
    Step3CoMobBot:twinMock,
    ReviewForm:twinMock,
    StatusLa:twinMock,
    CheckedWr:twinMock,
    ExchangeStatusHintWr:twinMock,
    SendFo:twinMock,
    UpdateStatusLa:twinMock,
    PaymentSentLa:twinMock,
    CheckingLoIn:twinMock,
    SendPaymentErrorWr:twinMock,
    SendPaymentError:twinMock,
    CheckStatusBu:twinMock,
    Step2Circle:twinMock,
    CreateTransactionCol:twinMock,
    SendPaymentCol:twinMock,
    PaymentAddressInWr:twinMock,
    CreateTransactionErrorWr:twinMock,
    CreateTransactionError:twinMock,
    AddressLa:twinMock,
    ExtraId:twinMock,
    TocLa:twinMock,
    RefundAddressLa:twinMock,
    RefundPopup:twinMock,
    SendPaymentCollapsar:twinMock,
    CreateTransactionCollapsar:twinMock,
    FinishedCollapsar:twinMock,
    ProgressColumn:twinMock,
    ProgressColumnMobTop:twinMock,
    ProgressColumnMobBot:twinMock,
    SwapeeWalletPicker:twinMock,
    ExchangeBroker:twinMock,
    DealBroker:twinMock,
    ExchangeIntent:twinMock,
    ExchangeStatusHint:twinMock,
    OfferExchangeInfoRow:twinMock,
    OfferExchangeInfoRowMob:twinMock,
    ExchangeStatusTitle:twinMock,
    PaymentLastChecked:twinMock,
    Duration:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.OfferExchangeDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IOfferExchangeDisplay.Initialese}*/({
   TransactionCurrencyFromLa:1,
   ConfirmedAmountFrom:1,
   RestartBu:1,
   ExchangeApiWr:1,
   PaymentAddressHiddenIn:1,
   CurrencyFromLas:1,
   AddressIn:1,
   TocIn:1,
   CreateTransactionBu:1,
   AddReviewErrorWr:1,
   AddReviewErrorLa:1,
   AddReviewBu:1,
   WalletIn:1,
   WalletOut:1,
   BtcWalletPicker:1,
   BtcWalletPickerCont:1,
   KycRequireds:1,
   KycNotRequireds:1,
   ExchangeEmails:1,
   OfferExchangeInfoRowMobWr:1,
   OptionalLa:1,
   RequiredLa:1,
   CreateTransactionWr:1,
   TransactionInfoWr:1,
   TransactionInfo:1,
   TransactionInfoHeadWr:1,
   TransactionInfoHead:1,
   TransactionInfoHeadMobWr:1,
   TransactionInfoHeadMob:1,
   PayinAddressCopiedLa:1,
   CopyPayinAddressBu:1,
   Step1ProgressCircleWrMobBot:1,
   Step1LaMobBot:1,
   Step2ProgressCircleWrMobBot:1,
   Step2LaMobBot:1,
   Step2CoMobTop:1,
   Step3CoMobTop:1,
   Step1CoMobBot:1,
   Step2CoMobBot:1,
   Step3CoMobBot:1,
   ReviewForm:1,
   StatusLa:1,
   CheckedWr:1,
   ExchangeStatusHintWr:1,
   SendFo:1,
   UpdateStatusLa:1,
   PaymentSentLa:1,
   CheckingLoIn:1,
   SendPaymentErrorWr:1,
   SendPaymentError:1,
   CheckStatusBu:1,
   Step2Circle:1,
   CreateTransactionCol:1,
   SendPaymentCol:1,
   PaymentAddressInWr:1,
   PaymentAddressIn:1,
   CreateTransactionErrorWr:1,
   CreateTransactionError:1,
   CurrencyToLas:1,
   AddressLa:1,
   ExtraId:1,
   TocLa:1,
   RefundAddressIn:1,
   RefundAddressLa:1,
   AddressPopup:1,
   RefundPopup:1,
   SendPaymentCollapsar:1,
   CreateTransactionCollapsar:1,
   FinishedCollapsar:1,
   ProgressColumn:1,
   ProgressColumnMobTop:1,
   ProgressColumnMobBot:1,
   SwapeeWalletPicker:1,
   ExchangeBroker:1,
   DealBroker:1,
   ExchangeIntent:1,
   ExchangeStatusHint:1,
   OfferExchangeInfoRow:1,
   OfferExchangeInfoRowMob:1,
   ExchangeStatusTitle:1,
   PaymentLastChecked:1,
   Duration:1,
  }),
  initializer({
   TransactionCurrencyFromLa:_TransactionCurrencyFromLa,
   ConfirmedAmountFrom:_ConfirmedAmountFrom,
   RestartBu:_RestartBu,
   ExchangeApiWr:_ExchangeApiWr,
   PaymentAddressHiddenIn:_PaymentAddressHiddenIn,
   CurrencyFromLas:_CurrencyFromLas,
   AddressIn:_AddressIn,
   TocIn:_TocIn,
   CreateTransactionBu:_CreateTransactionBu,
   AddReviewErrorWr:_AddReviewErrorWr,
   AddReviewErrorLa:_AddReviewErrorLa,
   AddReviewBu:_AddReviewBu,
   WalletIn:_WalletIn,
   WalletOut:_WalletOut,
   BtcWalletPicker:_BtcWalletPicker,
   BtcWalletPickerCont:_BtcWalletPickerCont,
   KycRequireds:_KycRequireds,
   KycNotRequireds:_KycNotRequireds,
   ExchangeEmails:_ExchangeEmails,
   OfferExchangeInfoRowMobWr:_OfferExchangeInfoRowMobWr,
   OptionalLa:_OptionalLa,
   RequiredLa:_RequiredLa,
   CreateTransactionWr:_CreateTransactionWr,
   TransactionInfoWr:_TransactionInfoWr,
   TransactionInfo:_TransactionInfo,
   TransactionInfoHeadWr:_TransactionInfoHeadWr,
   TransactionInfoHead:_TransactionInfoHead,
   TransactionInfoHeadMobWr:_TransactionInfoHeadMobWr,
   TransactionInfoHeadMob:_TransactionInfoHeadMob,
   PayinAddressCopiedLa:_PayinAddressCopiedLa,
   CopyPayinAddressBu:_CopyPayinAddressBu,
   Step1ProgressCircleWrMobBot:_Step1ProgressCircleWrMobBot,
   Step1LaMobBot:_Step1LaMobBot,
   Step2ProgressCircleWrMobBot:_Step2ProgressCircleWrMobBot,
   Step2LaMobBot:_Step2LaMobBot,
   Step2CoMobTop:_Step2CoMobTop,
   Step3CoMobTop:_Step3CoMobTop,
   Step1CoMobBot:_Step1CoMobBot,
   Step2CoMobBot:_Step2CoMobBot,
   Step3CoMobBot:_Step3CoMobBot,
   ReviewForm:_ReviewForm,
   StatusLa:_StatusLa,
   CheckedWr:_CheckedWr,
   ExchangeStatusHintWr:_ExchangeStatusHintWr,
   SendFo:_SendFo,
   UpdateStatusLa:_UpdateStatusLa,
   PaymentSentLa:_PaymentSentLa,
   CheckingLoIn:_CheckingLoIn,
   SendPaymentErrorWr:_SendPaymentErrorWr,
   SendPaymentError:_SendPaymentError,
   CheckStatusBu:_CheckStatusBu,
   Step2Circle:_Step2Circle,
   CreateTransactionCol:_CreateTransactionCol,
   SendPaymentCol:_SendPaymentCol,
   PaymentAddressInWr:_PaymentAddressInWr,
   PaymentAddressIn:_PaymentAddressIn,
   CreateTransactionErrorWr:_CreateTransactionErrorWr,
   CreateTransactionError:_CreateTransactionError,
   CurrencyToLas:_CurrencyToLas,
   AddressLa:_AddressLa,
   ExtraId:_ExtraId,
   TocLa:_TocLa,
   RefundAddressIn:_RefundAddressIn,
   RefundAddressLa:_RefundAddressLa,
   AddressPopup:_AddressPopup,
   RefundPopup:_RefundPopup,
   SendPaymentCollapsar:_SendPaymentCollapsar,
   CreateTransactionCollapsar:_CreateTransactionCollapsar,
   FinishedCollapsar:_FinishedCollapsar,
   ProgressColumn:_ProgressColumn,
   ProgressColumnMobTop:_ProgressColumnMobTop,
   ProgressColumnMobBot:_ProgressColumnMobBot,
   SwapeeWalletPicker:_SwapeeWalletPicker,
   ExchangeBroker:_ExchangeBroker,
   DealBroker:_DealBroker,
   ExchangeIntent:_ExchangeIntent,
   ExchangeStatusHint:_ExchangeStatusHint,
   OfferExchangeInfoRow:_OfferExchangeInfoRow,
   OfferExchangeInfoRowMob:_OfferExchangeInfoRowMob,
   ExchangeStatusTitle:_ExchangeStatusTitle,
   PaymentLastChecked:_PaymentLastChecked,
   Duration:_Duration,
  }) {
   if(_TransactionCurrencyFromLa!==undefined) this.TransactionCurrencyFromLa=_TransactionCurrencyFromLa
   if(_ConfirmedAmountFrom!==undefined) this.ConfirmedAmountFrom=_ConfirmedAmountFrom
   if(_RestartBu!==undefined) this.RestartBu=_RestartBu
   if(_ExchangeApiWr!==undefined) this.ExchangeApiWr=_ExchangeApiWr
   if(_PaymentAddressHiddenIn!==undefined) this.PaymentAddressHiddenIn=_PaymentAddressHiddenIn
   if(_CurrencyFromLas!==undefined) this.CurrencyFromLas=_CurrencyFromLas
   if(_AddressIn!==undefined) this.AddressIn=_AddressIn
   if(_TocIn!==undefined) this.TocIn=_TocIn
   if(_CreateTransactionBu!==undefined) this.CreateTransactionBu=_CreateTransactionBu
   if(_AddReviewErrorWr!==undefined) this.AddReviewErrorWr=_AddReviewErrorWr
   if(_AddReviewErrorLa!==undefined) this.AddReviewErrorLa=_AddReviewErrorLa
   if(_AddReviewBu!==undefined) this.AddReviewBu=_AddReviewBu
   if(_WalletIn!==undefined) this.WalletIn=_WalletIn
   if(_WalletOut!==undefined) this.WalletOut=_WalletOut
   if(_BtcWalletPicker!==undefined) this.BtcWalletPicker=_BtcWalletPicker
   if(_BtcWalletPickerCont!==undefined) this.BtcWalletPickerCont=_BtcWalletPickerCont
   if(_KycRequireds!==undefined) this.KycRequireds=_KycRequireds
   if(_KycNotRequireds!==undefined) this.KycNotRequireds=_KycNotRequireds
   if(_ExchangeEmails!==undefined) this.ExchangeEmails=_ExchangeEmails
   if(_OfferExchangeInfoRowMobWr!==undefined) this.OfferExchangeInfoRowMobWr=_OfferExchangeInfoRowMobWr
   if(_OptionalLa!==undefined) this.OptionalLa=_OptionalLa
   if(_RequiredLa!==undefined) this.RequiredLa=_RequiredLa
   if(_CreateTransactionWr!==undefined) this.CreateTransactionWr=_CreateTransactionWr
   if(_TransactionInfoWr!==undefined) this.TransactionInfoWr=_TransactionInfoWr
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
   if(_TransactionInfoHeadWr!==undefined) this.TransactionInfoHeadWr=_TransactionInfoHeadWr
   if(_TransactionInfoHead!==undefined) this.TransactionInfoHead=_TransactionInfoHead
   if(_TransactionInfoHeadMobWr!==undefined) this.TransactionInfoHeadMobWr=_TransactionInfoHeadMobWr
   if(_TransactionInfoHeadMob!==undefined) this.TransactionInfoHeadMob=_TransactionInfoHeadMob
   if(_PayinAddressCopiedLa!==undefined) this.PayinAddressCopiedLa=_PayinAddressCopiedLa
   if(_CopyPayinAddressBu!==undefined) this.CopyPayinAddressBu=_CopyPayinAddressBu
   if(_Step1ProgressCircleWrMobBot!==undefined) this.Step1ProgressCircleWrMobBot=_Step1ProgressCircleWrMobBot
   if(_Step1LaMobBot!==undefined) this.Step1LaMobBot=_Step1LaMobBot
   if(_Step2ProgressCircleWrMobBot!==undefined) this.Step2ProgressCircleWrMobBot=_Step2ProgressCircleWrMobBot
   if(_Step2LaMobBot!==undefined) this.Step2LaMobBot=_Step2LaMobBot
   if(_Step2CoMobTop!==undefined) this.Step2CoMobTop=_Step2CoMobTop
   if(_Step3CoMobTop!==undefined) this.Step3CoMobTop=_Step3CoMobTop
   if(_Step1CoMobBot!==undefined) this.Step1CoMobBot=_Step1CoMobBot
   if(_Step2CoMobBot!==undefined) this.Step2CoMobBot=_Step2CoMobBot
   if(_Step3CoMobBot!==undefined) this.Step3CoMobBot=_Step3CoMobBot
   if(_ReviewForm!==undefined) this.ReviewForm=_ReviewForm
   if(_StatusLa!==undefined) this.StatusLa=_StatusLa
   if(_CheckedWr!==undefined) this.CheckedWr=_CheckedWr
   if(_ExchangeStatusHintWr!==undefined) this.ExchangeStatusHintWr=_ExchangeStatusHintWr
   if(_SendFo!==undefined) this.SendFo=_SendFo
   if(_UpdateStatusLa!==undefined) this.UpdateStatusLa=_UpdateStatusLa
   if(_PaymentSentLa!==undefined) this.PaymentSentLa=_PaymentSentLa
   if(_CheckingLoIn!==undefined) this.CheckingLoIn=_CheckingLoIn
   if(_SendPaymentErrorWr!==undefined) this.SendPaymentErrorWr=_SendPaymentErrorWr
   if(_SendPaymentError!==undefined) this.SendPaymentError=_SendPaymentError
   if(_CheckStatusBu!==undefined) this.CheckStatusBu=_CheckStatusBu
   if(_Step2Circle!==undefined) this.Step2Circle=_Step2Circle
   if(_CreateTransactionCol!==undefined) this.CreateTransactionCol=_CreateTransactionCol
   if(_SendPaymentCol!==undefined) this.SendPaymentCol=_SendPaymentCol
   if(_PaymentAddressInWr!==undefined) this.PaymentAddressInWr=_PaymentAddressInWr
   if(_PaymentAddressIn!==undefined) this.PaymentAddressIn=_PaymentAddressIn
   if(_CreateTransactionErrorWr!==undefined) this.CreateTransactionErrorWr=_CreateTransactionErrorWr
   if(_CreateTransactionError!==undefined) this.CreateTransactionError=_CreateTransactionError
   if(_CurrencyToLas!==undefined) this.CurrencyToLas=_CurrencyToLas
   if(_AddressLa!==undefined) this.AddressLa=_AddressLa
   if(_ExtraId!==undefined) this.ExtraId=_ExtraId
   if(_TocLa!==undefined) this.TocLa=_TocLa
   if(_RefundAddressIn!==undefined) this.RefundAddressIn=_RefundAddressIn
   if(_RefundAddressLa!==undefined) this.RefundAddressLa=_RefundAddressLa
   if(_AddressPopup!==undefined) this.AddressPopup=_AddressPopup
   if(_RefundPopup!==undefined) this.RefundPopup=_RefundPopup
   if(_SendPaymentCollapsar!==undefined) this.SendPaymentCollapsar=_SendPaymentCollapsar
   if(_CreateTransactionCollapsar!==undefined) this.CreateTransactionCollapsar=_CreateTransactionCollapsar
   if(_FinishedCollapsar!==undefined) this.FinishedCollapsar=_FinishedCollapsar
   if(_ProgressColumn!==undefined) this.ProgressColumn=_ProgressColumn
   if(_ProgressColumnMobTop!==undefined) this.ProgressColumnMobTop=_ProgressColumnMobTop
   if(_ProgressColumnMobBot!==undefined) this.ProgressColumnMobBot=_ProgressColumnMobBot
   if(_SwapeeWalletPicker!==undefined) this.SwapeeWalletPicker=_SwapeeWalletPicker
   if(_ExchangeBroker!==undefined) this.ExchangeBroker=_ExchangeBroker
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_ExchangeStatusHint!==undefined) this.ExchangeStatusHint=_ExchangeStatusHint
   if(_OfferExchangeInfoRow!==undefined) this.OfferExchangeInfoRow=_OfferExchangeInfoRow
   if(_OfferExchangeInfoRowMob!==undefined) this.OfferExchangeInfoRowMob=_OfferExchangeInfoRowMob
   if(_ExchangeStatusTitle!==undefined) this.ExchangeStatusTitle=_ExchangeStatusTitle
   if(_PaymentLastChecked!==undefined) this.PaymentLastChecked=_PaymentLastChecked
   if(_Duration!==undefined) this.Duration=_Duration
  },
 }),
]