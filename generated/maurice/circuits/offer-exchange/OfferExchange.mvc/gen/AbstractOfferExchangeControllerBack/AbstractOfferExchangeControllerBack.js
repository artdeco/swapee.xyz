import AbstractOfferExchangeControllerAR from '../AbstractOfferExchangeControllerAR'
import {AbstractOfferExchangeController} from '../AbstractOfferExchangeController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeControllerBack}
 */
function __AbstractOfferExchangeControllerBack() {}
__AbstractOfferExchangeControllerBack.prototype = /** @type {!_AbstractOfferExchangeControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeController}
 */
class _AbstractOfferExchangeControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeController} ‎
 */
class AbstractOfferExchangeControllerBack extends newAbstract(
 _AbstractOfferExchangeControllerBack,652933965124,null,{
  asIOfferExchangeController:1,
  superOfferExchangeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeController} */
AbstractOfferExchangeControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeController} */
function AbstractOfferExchangeControllerBackClass(){}

export default AbstractOfferExchangeControllerBack


AbstractOfferExchangeControllerBack[$implementations]=[
 __AbstractOfferExchangeControllerBack,
 AbstractOfferExchangeController,
 AbstractOfferExchangeControllerAR,
 DriverBack,
]