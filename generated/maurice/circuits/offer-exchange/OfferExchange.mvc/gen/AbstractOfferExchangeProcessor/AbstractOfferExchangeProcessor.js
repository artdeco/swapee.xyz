import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeProcessor}
 */
function __AbstractOfferExchangeProcessor() {}
__AbstractOfferExchangeProcessor.prototype = /** @type {!_AbstractOfferExchangeProcessor} */ ({
  /** @return {void} */
  pulseCopyPayinAddress() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     copyPayinAddress:true,
    })
  },
  /** @return {void} */
  pulseCopyPayinExtra() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     copyPayinExtra:true,
    })
  },
})
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeProcessor}
 */
class _AbstractOfferExchangeProcessor { }
/**
 * The processor to compute changes to the memory for the _IOfferExchange_.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeProcessor} ‎
 */
class AbstractOfferExchangeProcessor extends newAbstract(
 _AbstractOfferExchangeProcessor,65293396519,null,{
  asIOfferExchangeProcessor:1,
  superOfferExchangeProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeProcessor} */
AbstractOfferExchangeProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeProcessor} */
function AbstractOfferExchangeProcessorClass(){}

export default AbstractOfferExchangeProcessor


AbstractOfferExchangeProcessor[$implementations]=[
 __AbstractOfferExchangeProcessor,
 AbstractOfferExchangeProcessorClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeProcessor}*/({
  flipTocAccepted(){
   const{
    asIOfferExchangeComputer:{
     model:{tocAccepted:tocAccepted},
     setInfo:setInfo,
    },
   }=this
   setInfo({tocAccepted:!tocAccepted})
  },
 }),
]