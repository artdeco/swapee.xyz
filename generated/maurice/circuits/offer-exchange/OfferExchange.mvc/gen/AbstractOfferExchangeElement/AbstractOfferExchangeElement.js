import OfferExchangeRenderVdus from './methods/render-vdus'
import OfferExchangeElementPort from '../OfferExchangeElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {OfferExchangeInputsPQs} from '../../pqs/OfferExchangeInputsPQs'
import {OfferExchangeQueriesPQs} from '../../pqs/OfferExchangeQueriesPQs'
import {OfferExchangeCachePQs} from '../../pqs/OfferExchangeCachePQs'
import { newAbstract, $implementations, create } from '@type.engineering/type-engineer'
import AbstractOfferExchange from '../AbstractOfferExchange'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeElement}
 */
function __AbstractOfferExchangeElement() {}
__AbstractOfferExchangeElement.prototype = /** @type {!_AbstractOfferExchangeElement} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeElement} */ function OfferExchangeElementConstructor() {
  /** @type {string} */ this.X_ADDRESS_IN='x-address-in'
  /** @type {string} */ this.X_TOC_IN='x-toc-in'
  /** @type {string} */ this.X_PAYMENT_ADDRESS_IN='x-payment-address-in'
  /** @type {string} */ this.X_REFUND_ADDRESS_IN='x-refund-address-in'
  /** @type {string} */ this.X_EXCHANGE_BROKER='x-exchange-broker'
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeElement}
 */
class _AbstractOfferExchangeElement { }
/**
 * A component description.
 *
 * The _IOfferExchange_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeElement} ‎
 */
class AbstractOfferExchangeElement extends newAbstract(
 _AbstractOfferExchangeElement,652933965114,OfferExchangeElementConstructor,{
  asIOfferExchangeElement:1,
  superOfferExchangeElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeElement} */
AbstractOfferExchangeElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeElement} */
function AbstractOfferExchangeElementClass(){}

export default AbstractOfferExchangeElement


AbstractOfferExchangeElement[$implementations]=[
 __AbstractOfferExchangeElement,
 ElementBase,
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  calibrate:function induceTags({
   children:children,
  }){
   const{
    'exchange-api':exchangeApi,
   }=this.extractTagsWithAttrs(children,['exchange-api'])
   /**@type {!xyz.swapee.wc.IOfferExchangeElement.Inputs}*/
   const _ret={}
   if(exchangeApi[0]) _ret.exchangeApiContent=exchangeApi[0].content
   return _ret
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':toc-accepted':tocAcceptedColAttr,
   ':step':stepColAttr,
   ':kyc-email':kycEmailColAttr,
   ':payment-failed':paymentFailedColAttr,
   ':copy-payin-address':copyPayinAddressColAttr,
   ':payin-address-just-copied':payinAddressJustCopiedColAttr,
   ':payin-address-copied':payinAddressCopiedColAttr,
   ':copy-payin-extra':copyPayinExtraColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'toc-accepted':tocAcceptedAttr,
    'step':stepAttr,
    'kyc-email':kycEmailAttr,
    'payment-failed':paymentFailedAttr,
    'copy-payin-address':copyPayinAddressAttr,
    'payin-address-just-copied':payinAddressJustCopiedAttr,
    'payin-address-copied':payinAddressCopiedAttr,
    'copy-payin-extra':copyPayinExtraAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(tocAcceptedAttr===undefined?{'toc-accepted':tocAcceptedColAttr}:{}),
    ...(stepAttr===undefined?{'step':stepColAttr}:{}),
    ...(kycEmailAttr===undefined?{'kyc-email':kycEmailColAttr}:{}),
    ...(paymentFailedAttr===undefined?{'payment-failed':paymentFailedColAttr}:{}),
    ...(copyPayinAddressAttr===undefined?{'copy-payin-address':copyPayinAddressColAttr}:{}),
    ...(payinAddressJustCopiedAttr===undefined?{'payin-address-just-copied':payinAddressJustCopiedColAttr}:{}),
    ...(payinAddressCopiedAttr===undefined?{'payin-address-copied':payinAddressCopiedColAttr}:{}),
    ...(copyPayinExtraAttr===undefined?{'copy-payin-extra':copyPayinExtraColAttr}:{}),
   }
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'toc-accepted':tocAcceptedAttr,
   'step':stepAttr,
   'kyc-email':kycEmailAttr,
   'payment-failed':paymentFailedAttr,
   'copy-payin-address':copyPayinAddressAttr,
   'payin-address-just-copied':payinAddressJustCopiedAttr,
   'payin-address-copied':payinAddressCopiedAttr,
   'copy-payin-extra':copyPayinExtraAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    tocAccepted:tocAcceptedAttr,
    step:stepAttr,
    kycEmail:kycEmailAttr,
    paymentFailed:paymentFailedAttr,
    copyPayinAddress:copyPayinAddressAttr,
    payinAddressJustCopied:payinAddressJustCopiedAttr,
    payinAddressCopied:payinAddressCopiedAttr,
    copyPayinExtra:copyPayinExtraAttr,
   }
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 Landed,
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  render:function renderExchangeBroker(){
   const{
    asILanded:{
     land:{
      ExchangeBroker:ExchangeBroker,
     },
    },
    asIOfferExchangeElement:{
     buildExchangeBroker:buildExchangeBroker,
    },
   }=this
   if(!ExchangeBroker) return
   const{model:ExchangeBrokerModel}=ExchangeBroker
   const{
    '59af8':payinAddress,
    '884d9':address,
    '89fc6':creatingTransaction,
    '601f8':createTransactionError,
    'afbad':checkPaymentError,
    'adfbe':checkingStatus,
    '9acb4':status,
    '490aa':Id,
    '0b951':notId,
    '2b803':kycRequired,
    '39957':paymentReceived,
    'b978a':paymentExpiredOrOverdue,
    'a99c4':exchangeComplete,
    'be03a':refundAddress,
    '6103f':confirmedAmountFrom,
    '37b54':waitingForPayment,
    '355f3':processingPayment,
   }=ExchangeBrokerModel
   const res=buildExchangeBroker({
    payinAddress:payinAddress,
    address:address,
    creatingTransaction:creatingTransaction,
    createTransactionError:createTransactionError,
    checkPaymentError:checkPaymentError,
    checkingStatus:checkingStatus,
    status:status,
    Id:Id,
    notId:notId,
    kycRequired:kycRequired,
    paymentReceived:paymentReceived,
    paymentExpiredOrOverdue:paymentExpiredOrOverdue,
    exchangeComplete:exchangeComplete,
    refundAddress:refundAddress,
    confirmedAmountFrom:confirmedAmountFrom,
    waitingForPayment:waitingForPayment,
    processingPayment:processingPayment,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  render:function renderExchangeIntent(){
   const{
    asILanded:{
     land:{
      ExchangeIntent:ExchangeIntent,
     },
    },
    asIOfferExchangeElement:{
     buildExchangeIntent:buildExchangeIntent,
    },
   }=this
   if(!ExchangeIntent) return
   const{model:ExchangeIntentModel}=ExchangeIntent
   const{
    'cec31':fixed,
    'b2fda':ready,
    'c23cd':currencyTo,
    '96c88':currencyFrom,
   }=ExchangeIntentModel
   const res=buildExchangeIntent({
    fixed:fixed,
    ready:ready,
    currencyTo:currencyTo,
    currencyFrom:currencyFrom,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  render:function renderTransactionInfo(){
   const{
    asILanded:{
     land:{
      TransactionInfo:TransactionInfo,
     },
    },
    asIOfferExchangeElement:{
     buildTransactionInfo:buildTransactionInfo,
    },
   }=this
   if(!TransactionInfo) return
   const{model:TransactionInfoModel}=TransactionInfo
   const{
    '96c88':currencyFrom,
   }=TransactionInfoModel
   const res=buildTransactionInfo({
    currencyFrom:currencyFrom,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.IOfferExchangeElement}*/({land:{
    ExchangeBroker:null,
    ExchangeIntent:null,
    TransactionInfo:null,
   }}))
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  render:OfferExchangeRenderVdus,
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  classes:{
   'Loading': '16bfb',
   'InputActive': 'f21bb',
   'ProgressVertLine': 'a7a04',
   'PaymentReceived': 'bb3b5',
   'CircleLoading': '5e522',
   'CircleComplete': 'a5ad9',
   'CircleWaiting': '9912b',
   'HintRed': 'b940b',
   'Copied': 'a588b',
   'CopiedRemoving': '4d2b6',
  },
  inputsPQs:OfferExchangeInputsPQs,
  queriesPQs:OfferExchangeQueriesPQs,
  cachePQs:OfferExchangeCachePQs,
  vdus:{
   'AddressIn': 'b69d1',
   'RefundAddressIn': 'b69d2',
   'TocIn': 'b69d3',
   'CreateTransactionBu': 'b69d4',
   'AddressPopup': 'b69d5',
   'ConfirmedAmountFrom': 'b69d6',
   'RestartBu': 'b69d7',
   'ExchangeApiWr': 'b69d8',
   'PaymentAddressHiddenIn': 'b69d9',
   'StatusLa': 'b69d12',
   'CheckedWr': 'b69d13',
   'CheckingLoIn': 'b69d14',
   'SendPaymentErrorWr': 'b69d25',
   'SendPaymentError': 'b69d26',
   'Step2Circle': 'b69d28',
   'CreateTransactionCol': 'b69d29',
   'SendPaymentCol': 'b69d30',
   'PaymentAddressInWr': 'b69d31',
   'PaymentAddressIn': 'b69d32',
   'CreateTransactionErrorWr': 'b69d33',
   'CreateTransactionError': 'b69d34',
   'AddressLa': 'b69d37',
   'ExtraId': 'b69d38',
   'TocLa': 'b69d39',
   'RefundAddressLa': 'b69d40',
   'RefundPopup': 'b69d41',
   'SendPaymentCollapsar': 'b69d42',
   'CreateTransactionCollapsar': 'b69d43',
   'ProgressColumn': 'b69d44',
   'SwapeeWalletPicker': 'b69d45',
   'ExchangeBroker': 'b69d46',
   'PaymentLastChecked': 'b69d47',
   'ExchangeStatusHint': 'b69d48',
   'UpdateStatusLa': 'b69d49',
   'PaymentSentLa': 'b69d50',
   'ExchangeStatusTitle': 'b69d51',
   'SendFo': 'b69d52',
   'ExchangeStatusHintWr': 'b69d53',
   'ReviewForm': 'b69d55',
   'CheckStatusBu': 'b69d56',
   'FinishedCollapsar': 'b69d57',
   'Duration': 'b69d58',
   'ProgressColumnMobTop': 'b69d59',
   'ProgressColumnMobBot': 'b69d60',
   'Step1ProgressCircleWrMobBot': 'b69d66',
   'Step1LaMobBot': 'b69d67',
   'Step2CoMobTop': 'b69d68',
   'Step3CoMobTop': 'b69d69',
   'Step1CoMobBot': 'b69d70',
   'Step2CoMobBot': 'b69d71',
   'Step3CoMobBot': 'b69d72',
   'Step2ProgressCircleWrMobBot': 'b69d73',
   'Step2LaMobBot': 'b69d74',
   'CopyPayinAddressBu': 'b69d76',
   'PayinAddressCopiedLa': 'b69d77',
   'AddReviewErrorWr': 'b69d79',
   'AddReviewErrorLa': 'b69d80',
   'AddReviewBu': 'b69d81',
   'OfferExchangeInfoRow': 'b69d101',
   'OfferExchangeInfoRowMob': 'b69d102',
   'DealBroker': 'b69d103',
   'ExchangeIntent': 'b69d104',
   'OptionalLa': 'b69d105',
   'RequiredLa': 'b69d106',
   'TransactionInfo': 'b69d111',
   'TransactionInfoWr': 'b69d112',
   'CreateTransactionWr': 'b69d113',
   'OfferExchangeInfoRowMobWr': 'b69d114',
   'TransactionInfoHead': 'b69d117',
   'TransactionInfoHeadMob': 'b69d118',
   'TransactionInfoHeadWr': 'b69d119',
   'TransactionInfoHeadMobWr': 'b69d120',
   'TransactionCurrencyFromLa': 'b69d121',
   'WalletIn': 'b69d128',
   'WalletOut': 'b69d129',
   'BtcWalletPicker': 'b69d130',
   'BtcWalletPickerCont': 'b69d131',
   'CurrencyFromLa': 'b69d35',
   'CurrencyToLa': 'b69d36',
   'KycRequired': 'b69d125',
   'KycNotRequired': 'b69d126',
   'ExchangeEmail': 'b69d127',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','tocAccepted','step','kycEmail','paymentFailed','copyPayinAddress','payinAddressJustCopied','payinAddressCopied','copyPayinExtra','query:deal-broker','query:exchange-intent','no-solder',':no-solder','toc-accepted',':toc-accepted',':step','kyc-email',':kyc-email','payment-failed',':payment-failed','copy-payin-address',':copy-payin-address','payin-address-just-copied',':payin-address-just-copied','payin-address-copied',':payin-address-copied','copy-payin-extra',':copy-payin-extra','fe646','cb9cc','b2300','7a978','0a9f3','525dc','8becc','98456','4eefe','3d3aa','e223d','35b7c','effbb','82b2e','2a37c','03ffb','d86f3','6c5cd','35399','c8e3d','c8757','5bd11','e6d7a','1275c','998db','6b245','98f2d','047a7','4e368','6ee13','ae8e7','ab64c','8b62e','ded13','b6ffb','b1bfd','1580a','cb2c4','9b30a','7cda0','093ac','67737','7622a','edb6b','cea06','ddb42','160ff','393c2','d5e40','e0c69','8f8e9','f00dd','41102','900c0','95a06','2cf7a','3653f','09ef4','de24e','698a5','8f30a','071c3','a04a2','c0113','4dba9','3b77d','773fc','74d62','6de0e','9c52c','89978','83974','22f5d','7cb9c','38bd9','c9e36','c3b6b','52b90','59671','6c249','bd259','2b1c1','88e1e','27b63','2764c','d5b6f','02236','16e61','45cad','141c4','4bb1a','children']),
   })
  },
  get Port(){
   return OfferExchangeElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:deal-broker':dealBrokerSel,'query:exchange-intent':exchangeIntentSel}){
   const _ret={}
   if(dealBrokerSel) _ret.dealBrokerSel=dealBrokerSel
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   return _ret
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  constructor(){
   Object.assign(this.buildees,{
    'ExchangeBroker':{ExchangeBroker:1683059665},
    'TransactionInfo':{TransactionInfo:1988051819},
   })
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  constructor(){
   this.land={
    AddressPopup:null,
    RefundPopup:null,
    SendPaymentCollapsar:null,
    CreateTransactionCollapsar:null,
    FinishedCollapsar:null,
    ProgressColumn:null,
    ProgressColumnMobTop:null,
    ProgressColumnMobBot:null,
    SwapeeWalletPicker:null,
    ExchangeBroker:null,
    DealBroker:null,
    ExchangeIntent:null,
    ExchangeStatusHint:null,
    TransactionInfo:null,
    OfferExchangeInfoRow:null,
    OfferExchangeInfoRowMob:null,
    ExchangeStatusTitle:null,
    PaymentLastChecked:null,
    Duration:null,
   }
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  calibrate:async function awaitOnDealBroker({dealBrokerSel:dealBrokerSel}){
   if(!dealBrokerSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const DealBroker=await milleu(dealBrokerSel)
   if(!DealBroker) {
    console.warn('❗️ dealBrokerSel %s must be present on the page for %s to work',dealBrokerSel,fqn)
    return{}
   }
   land.DealBroker=DealBroker
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel,true)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  solder:(_,{
   dealBrokerSel:dealBrokerSel,
   exchangeIntentSel:exchangeIntentSel,
  })=>{
   return{
    dealBrokerSel:dealBrokerSel,
    exchangeIntentSel:exchangeIntentSel,
   }
  },
 }),
 AbstractOfferExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
  calibrate({"splendid":splendid}){
   if(!splendid['__$borrowed:classes']){
    splendid['__$borrowed:classes']=new Map
   }
   const borrowedClasses=splendid['__$borrowed:classes']
   borrowedClasses.set(this.__$id,[6526983971,new Map([
   ]),this])
  },
 }),
]



AbstractOfferExchangeElement[$implementations]=[AbstractOfferExchange,
 /** @type {!AbstractOfferExchangeElement} */ ({
  rootId:'OfferExchange',
  __$id:6529339651,
  fqn:'xyz.swapee.wc.IOfferExchange',
  maurice_element_v3:true,
 }),
]