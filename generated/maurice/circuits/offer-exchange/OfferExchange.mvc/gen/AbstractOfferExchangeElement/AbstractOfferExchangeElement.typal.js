
import AbstractOfferExchange from '../AbstractOfferExchange'

/** @abstract {xyz.swapee.wc.IOfferExchangeElement} */
export default class AbstractOfferExchangeElement { }



AbstractOfferExchangeElement[$implementations]=[AbstractOfferExchange,
 /** @type {!AbstractOfferExchangeElement} */ ({
  rootId:'OfferExchange',
  __$id:6529339651,
  fqn:'xyz.swapee.wc.IOfferExchange',
  maurice_element_v3:true,
 }),
]