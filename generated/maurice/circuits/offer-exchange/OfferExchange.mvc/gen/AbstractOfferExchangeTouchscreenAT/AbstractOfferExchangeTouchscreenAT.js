import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeTouchscreenAT}
 */
function __AbstractOfferExchangeTouchscreenAT() {}
__AbstractOfferExchangeTouchscreenAT.prototype = /** @type {!_AbstractOfferExchangeTouchscreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeTouchscreenAT}
 */
class _AbstractOfferExchangeTouchscreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOfferExchangeTouchscreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeTouchscreenAT} ‎
 */
class AbstractOfferExchangeTouchscreenAT extends newAbstract(
 _AbstractOfferExchangeTouchscreenAT,652933965130,null,{
  asIOfferExchangeTouchscreenAT:1,
  superOfferExchangeTouchscreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeTouchscreenAT} */
AbstractOfferExchangeTouchscreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeTouchscreenAT} */
function AbstractOfferExchangeTouchscreenATClass(){}

export default AbstractOfferExchangeTouchscreenAT


AbstractOfferExchangeTouchscreenAT[$implementations]=[
 __AbstractOfferExchangeTouchscreenAT,
 UartUniversal,
]