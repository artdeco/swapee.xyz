import {makeBuffers} from '@webcircuits/webcircuits'

export const OfferExchangeBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  tocAccepted:Boolean,
  step:String,
  kycEmail:String,
  paymentFailed:Boolean,
  copyPayinAddress:Boolean,
  payinAddressJustCopied:Boolean,
  payinAddressCopied:Boolean,
  copyPayinExtra:Boolean,
 }),
})

export default OfferExchangeBuffer