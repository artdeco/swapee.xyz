import {mountPins} from '@type.engineering/seers'
import {OfferExchangeMemoryPQs} from '../../pqs/OfferExchangeMemoryPQs'
import {OfferExchangeCachePQs} from '../../pqs/OfferExchangeCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangeCore}
 */
function __OfferExchangeCore() {}
__OfferExchangeCore.prototype = /** @type {!_OfferExchangeCore} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeCore} */ function OfferExchangeCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOfferExchangeCore.Model}*/
  this.model={
    canCreateTransaction: false,
    btcInput: false,
    btcOutput: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeCore}
 */
class _OfferExchangeCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeCore} ‎
 */
class OfferExchangeCore extends newAbstract(
 _OfferExchangeCore,65293396518,OfferExchangeCoreConstructor,{
  asIOfferExchangeCore:1,
  superOfferExchangeCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeCore} */
OfferExchangeCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeCore} */
function OfferExchangeCoreClass(){}

export default OfferExchangeCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangeOuterCore}
 */
function __OfferExchangeOuterCore() {}
__OfferExchangeOuterCore.prototype = /** @type {!_OfferExchangeOuterCore} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeOuterCore} */
export function OfferExchangeOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOfferExchangeOuterCore.Model}*/
  this.model={
    tocAccepted: true,
    step: '',
    kycEmail: '',
    paymentFailed: false,
    copyPayinAddress: false,
    payinAddressJustCopied: false,
    payinAddressCopied: false,
    copyPayinExtra: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeOuterCore}
 */
class _OfferExchangeOuterCore { }
/**
 * The _IOfferExchange_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeOuterCore} ‎
 */
export class OfferExchangeOuterCore extends newAbstract(
 _OfferExchangeOuterCore,65293396513,OfferExchangeOuterCoreConstructor,{
  asIOfferExchangeOuterCore:1,
  superOfferExchangeOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeOuterCore} */
OfferExchangeOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeOuterCore} */
function OfferExchangeOuterCoreClass(){}


OfferExchangeOuterCore[$implementations]=[
 __OfferExchangeOuterCore,
 OfferExchangeOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeOuterCore}*/({
  constructor(){
   mountPins(this.model,OfferExchangeMemoryPQs)
   mountPins(this.model,OfferExchangeCachePQs)
  },
 }),
]

OfferExchangeCore[$implementations]=[
 OfferExchangeCoreClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeCore}*/({
  resetCore(){
   this.resetOfferExchangeCore()
  },
  resetOfferExchangeCore(){
   OfferExchangeCoreConstructor.call(
    /**@type {xyz.swapee.wc.OfferExchangeCore}*/(this),
   )
   OfferExchangeOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.OfferExchangeOuterCore}*/(
     /**@type {!xyz.swapee.wc.IOfferExchangeOuterCore}*/(this)),
   )
  },
 }),
 __OfferExchangeCore,
 OfferExchangeOuterCore,
]

export {OfferExchangeCore}