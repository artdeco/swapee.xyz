import AbstractOfferExchangeProcessor from '../AbstractOfferExchangeProcessor'
import {OfferExchangeCore} from '../OfferExchangeCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractOfferExchangeComputer} from '../AbstractOfferExchangeComputer'
import {AbstractOfferExchangeController} from '../AbstractOfferExchangeController'
import {regulateOfferExchangeCache} from './methods/regulateOfferExchangeCache'
import {OfferExchangeCacheQPs} from '../../pqs/OfferExchangeCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchange}
 */
function __AbstractOfferExchange() {}
__AbstractOfferExchange.prototype = /** @type {!_AbstractOfferExchange} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchange}
 */
class _AbstractOfferExchange { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractOfferExchange} ‎
 */
class AbstractOfferExchange extends newAbstract(
 _AbstractOfferExchange,652933965110,null,{
  asIOfferExchange:1,
  superOfferExchange:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchange} */
AbstractOfferExchange.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchange} */
function AbstractOfferExchangeClass(){}

export default AbstractOfferExchange


AbstractOfferExchange[$implementations]=[
 __AbstractOfferExchange,
 OfferExchangeCore,
 AbstractOfferExchangeProcessor,
 IntegratedComponent,
 AbstractOfferExchangeComputer,
 AbstractOfferExchangeController,
 AbstractOfferExchangeClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchange}*/({
  regulateState:regulateOfferExchangeCache,
  stateQPs:OfferExchangeCacheQPs,
 }),
]


export {AbstractOfferExchange}