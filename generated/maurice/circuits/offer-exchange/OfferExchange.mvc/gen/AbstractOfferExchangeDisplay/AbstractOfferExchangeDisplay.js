import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeDisplay}
 */
function __AbstractOfferExchangeDisplay() {}
__AbstractOfferExchangeDisplay.prototype = /** @type {!_AbstractOfferExchangeDisplay} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeDisplay} */ function OfferExchangeDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.TransactionCurrencyFromLa=null
  /** @type {HTMLSpanElement} */ this.ConfirmedAmountFrom=null
  /** @type {HTMLButtonElement} */ this.RestartBu=null
  /** @type {HTMLDivElement} */ this.ExchangeApiWr=null
  /** @type {HTMLInputElement} */ this.PaymentAddressHiddenIn=null
  /** @type {!Array<!HTMLSpanElement>} */ this.CurrencyFromLas=[]
  /** @type {HTMLInputElement} */ this.AddressIn=null
  /** @type {HTMLInputElement} */ this.TocIn=null
  /** @type {HTMLButtonElement} */ this.CreateTransactionBu=null
  /** @type {HTMLDivElement} */ this.AddReviewErrorWr=null
  /** @type {HTMLSpanElement} */ this.AddReviewErrorLa=null
  /** @type {HTMLButtonElement} */ this.AddReviewBu=null
  /** @type {HTMLDivElement} */ this.WalletIn=null
  /** @type {HTMLDivElement} */ this.WalletOut=null
  /** @type {HTMLDivElement} */ this.BtcWalletPicker=null
  /** @type {HTMLDivElement} */ this.BtcWalletPickerCont=null
  /** @type {!Array<!HTMLDivElement>} */ this.KycRequireds=[]
  /** @type {!Array<!HTMLDivElement>} */ this.KycNotRequireds=[]
  /** @type {!Array<!HTMLDivElement>} */ this.ExchangeEmails=[]
  /** @type {HTMLDivElement} */ this.OfferExchangeInfoRowMobWr=null
  /** @type {HTMLSpanElement} */ this.OptionalLa=null
  /** @type {HTMLSpanElement} */ this.RequiredLa=null
  /** @type {HTMLSpanElement} */ this.CreateTransactionWr=null
  /** @type {HTMLDivElement} */ this.TransactionInfoWr=null
  /** @type {HTMLDivElement} */ this.TransactionInfo=null
  /** @type {HTMLDivElement} */ this.TransactionInfoHeadWr=null
  /** @type {HTMLDivElement} */ this.TransactionInfoHead=null
  /** @type {HTMLDivElement} */ this.TransactionInfoHeadMobWr=null
  /** @type {HTMLDivElement} */ this.TransactionInfoHeadMob=null
  /** @type {HTMLSpanElement} */ this.PayinAddressCopiedLa=null
  /** @type {HTMLButtonElement} */ this.CopyPayinAddressBu=null
  /** @type {HTMLDivElement} */ this.Step1ProgressCircleWrMobBot=null
  /** @type {HTMLDivElement} */ this.Step1LaMobBot=null
  /** @type {HTMLDivElement} */ this.Step2ProgressCircleWrMobBot=null
  /** @type {HTMLDivElement} */ this.Step2LaMobBot=null
  /** @type {HTMLDivElement} */ this.Step2CoMobTop=null
  /** @type {HTMLDivElement} */ this.Step3CoMobTop=null
  /** @type {HTMLDivElement} */ this.Step1CoMobBot=null
  /** @type {HTMLDivElement} */ this.Step2CoMobBot=null
  /** @type {HTMLDivElement} */ this.Step3CoMobBot=null
  /** @type {HTMLFormElement} */ this.ReviewForm=null
  /** @type {HTMLSpanElement} */ this.StatusLa=null
  /** @type {HTMLSpanElement} */ this.CheckedWr=null
  /** @type {HTMLDivElement} */ this.ExchangeStatusHintWr=null
  /** @type {HTMLFormElement} */ this.SendFo=null
  /** @type {HTMLSpanElement} */ this.UpdateStatusLa=null
  /** @type {HTMLSpanElement} */ this.PaymentSentLa=null
  /** @type {HTMLSpanElement} */ this.CheckingLoIn=null
  /** @type {HTMLDivElement} */ this.SendPaymentErrorWr=null
  /** @type {HTMLSpanElement} */ this.SendPaymentError=null
  /** @type {HTMLButtonElement} */ this.CheckStatusBu=null
  /** @type {HTMLSpanElement} */ this.Step2Circle=null
  /** @type {HTMLDivElement} */ this.CreateTransactionCol=null
  /** @type {HTMLDivElement} */ this.SendPaymentCol=null
  /** @type {HTMLDivElement} */ this.PaymentAddressInWr=null
  /** @type {HTMLInputElement} */ this.PaymentAddressIn=null
  /** @type {HTMLDivElement} */ this.CreateTransactionErrorWr=null
  /** @type {HTMLSpanElement} */ this.CreateTransactionError=null
  /** @type {!Array<!HTMLSpanElement>} */ this.CurrencyToLas=[]
  /** @type {HTMLLabelElement} */ this.AddressLa=null
  /** @type {HTMLInputElement} */ this.ExtraId=null
  /** @type {HTMLSpanElement} */ this.TocLa=null
  /** @type {HTMLInputElement} */ this.RefundAddressIn=null
  /** @type {HTMLLabelElement} */ this.RefundAddressLa=null
  /** @type {HTMLElement} */ this.AddressPopup=null
  /** @type {HTMLElement} */ this.RefundPopup=null
  /** @type {HTMLElement} */ this.SendPaymentCollapsar=null
  /** @type {HTMLElement} */ this.CreateTransactionCollapsar=null
  /** @type {HTMLElement} */ this.FinishedCollapsar=null
  /** @type {HTMLElement} */ this.ProgressColumn=null
  /** @type {HTMLElement} */ this.ProgressColumnMobTop=null
  /** @type {HTMLElement} */ this.ProgressColumnMobBot=null
  /** @type {HTMLElement} */ this.SwapeeWalletPicker=null
  /** @type {HTMLElement} */ this.ExchangeBroker=null
  /** @type {HTMLElement} */ this.DealBroker=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
  /** @type {HTMLElement} */ this.ExchangeStatusHint=null
  /** @type {HTMLElement} */ this.OfferExchangeInfoRow=null
  /** @type {HTMLElement} */ this.OfferExchangeInfoRowMob=null
  /** @type {HTMLElement} */ this.ExchangeStatusTitle=null
  /** @type {HTMLElement} */ this.PaymentLastChecked=null
  /** @type {HTMLElement} */ this.Duration=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeDisplay}
 */
class _AbstractOfferExchangeDisplay { }
/**
 * Display for presenting information from the _IOfferExchange_.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeDisplay} ‎
 */
class AbstractOfferExchangeDisplay extends newAbstract(
 _AbstractOfferExchangeDisplay,652933965118,OfferExchangeDisplayConstructor,{
  asIOfferExchangeDisplay:1,
  superOfferExchangeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeDisplay} */
AbstractOfferExchangeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeDisplay} */
function AbstractOfferExchangeDisplayClass(){}

export default AbstractOfferExchangeDisplay


AbstractOfferExchangeDisplay[$implementations]=[
 __AbstractOfferExchangeDisplay,
 Display,
 AbstractOfferExchangeDisplayClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{dealBrokerScopeSel:dealBrokerScopeSel,exchangeIntentScopeSel:exchangeIntentScopeSel}}=this
    this.scan({
     dealBrokerSel:dealBrokerScopeSel,
     exchangeIntentSel:exchangeIntentScopeSel,
    })
   })
  },
  scan:function vduScan({dealBrokerSel:dealBrokerSelScope,exchangeIntentSel:exchangeIntentSelScope}){
   const{element:element,asIOfferExchangeTouchscreen:{vdusPQs:{
    BtcWalletPickerCont:BtcWalletPickerCont,
    WalletOut:WalletOut,
    WalletIn:WalletIn,
    AddressIn:AddressIn,
    RefundAddressIn:RefundAddressIn,
    TocIn:TocIn,
    CreateTransactionBu:CreateTransactionBu,
    CopyPayinAddressBu:CopyPayinAddressBu,
    PaymentAddressIn:PaymentAddressIn,
    PayinAddressCopiedLa:PayinAddressCopiedLa,
    RestartBu:RestartBu,
    AddressPopup:AddressPopup,
    TransactionCurrencyFromLa:TransactionCurrencyFromLa,
    ConfirmedAmountFrom:ConfirmedAmountFrom,
    ExchangeApiWr:ExchangeApiWr,
    PaymentAddressHiddenIn:PaymentAddressHiddenIn,
    AddReviewErrorWr:AddReviewErrorWr,
    AddReviewErrorLa:AddReviewErrorLa,
    AddReviewBu:AddReviewBu,
    BtcWalletPicker:BtcWalletPicker,
    OfferExchangeInfoRowMobWr:OfferExchangeInfoRowMobWr,
    OptionalLa:OptionalLa,
    RequiredLa:RequiredLa,
    CreateTransactionWr:CreateTransactionWr,
    TransactionInfoWr:TransactionInfoWr,
    TransactionInfo:TransactionInfo,
    TransactionInfoHeadWr:TransactionInfoHeadWr,
    TransactionInfoHead:TransactionInfoHead,
    TransactionInfoHeadMobWr:TransactionInfoHeadMobWr,
    TransactionInfoHeadMob:TransactionInfoHeadMob,
    Step1ProgressCircleWrMobBot:Step1ProgressCircleWrMobBot,
    Step1LaMobBot:Step1LaMobBot,
    Step2ProgressCircleWrMobBot:Step2ProgressCircleWrMobBot,
    Step2LaMobBot:Step2LaMobBot,
    Step2CoMobTop:Step2CoMobTop,
    Step3CoMobTop:Step3CoMobTop,
    Step1CoMobBot:Step1CoMobBot,
    Step2CoMobBot:Step2CoMobBot,
    Step3CoMobBot:Step3CoMobBot,
    ReviewForm:ReviewForm,
    StatusLa:StatusLa,
    CheckedWr:CheckedWr,
    ExchangeStatusHintWr:ExchangeStatusHintWr,
    SendFo:SendFo,
    UpdateStatusLa:UpdateStatusLa,
    PaymentSentLa:PaymentSentLa,
    CheckingLoIn:CheckingLoIn,
    SendPaymentErrorWr:SendPaymentErrorWr,
    SendPaymentError:SendPaymentError,
    CheckStatusBu:CheckStatusBu,
    Step2Circle:Step2Circle,
    CreateTransactionCol:CreateTransactionCol,
    SendPaymentCol:SendPaymentCol,
    PaymentAddressInWr:PaymentAddressInWr,
    CreateTransactionErrorWr:CreateTransactionErrorWr,
    CreateTransactionError:CreateTransactionError,
    AddressLa:AddressLa,
    ExtraId:ExtraId,
    TocLa:TocLa,
    RefundAddressLa:RefundAddressLa,
    RefundPopup:RefundPopup,
    SendPaymentCollapsar:SendPaymentCollapsar,
    CreateTransactionCollapsar:CreateTransactionCollapsar,
    FinishedCollapsar:FinishedCollapsar,
    ProgressColumn:ProgressColumn,
    ProgressColumnMobTop:ProgressColumnMobTop,
    ProgressColumnMobBot:ProgressColumnMobBot,
    SwapeeWalletPicker:SwapeeWalletPicker,
    ExchangeBroker:ExchangeBroker,
    ExchangeStatusHint:ExchangeStatusHint,
    OfferExchangeInfoRow:OfferExchangeInfoRow,
    OfferExchangeInfoRowMob:OfferExchangeInfoRowMob,
    ExchangeStatusTitle:ExchangeStatusTitle,
    PaymentLastChecked:PaymentLastChecked,
    Duration:Duration,
   }},queries:{dealBrokerSel,exchangeIntentSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const DealBroker=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _DealBroker=dealBrokerSel?root.querySelector(dealBrokerSel):void 0
    return _DealBroker
   })(dealBrokerSelScope)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   Object.assign(this,{
    BtcWalletPickerCont:/**@type {HTMLDivElement}*/(children[BtcWalletPickerCont]),
    WalletOut:/**@type {HTMLDivElement}*/(children[WalletOut]),
    WalletIn:/**@type {HTMLDivElement}*/(children[WalletIn]),
    AddressIn:/**@type {HTMLInputElement}*/(children[AddressIn]),
    RefundAddressIn:/**@type {HTMLInputElement}*/(children[RefundAddressIn]),
    TocIn:/**@type {HTMLInputElement}*/(children[TocIn]),
    CreateTransactionBu:/**@type {HTMLButtonElement}*/(children[CreateTransactionBu]),
    CopyPayinAddressBu:/**@type {HTMLButtonElement}*/(children[CopyPayinAddressBu]),
    PaymentAddressIn:/**@type {HTMLInputElement}*/(children[PaymentAddressIn]),
    PayinAddressCopiedLa:/**@type {HTMLSpanElement}*/(children[PayinAddressCopiedLa]),
    RestartBu:/**@type {HTMLButtonElement}*/(children[RestartBu]),
    AddressPopup:/**@type {HTMLElement}*/(children[AddressPopup]),
    TransactionCurrencyFromLa:/**@type {HTMLSpanElement}*/(children[TransactionCurrencyFromLa]),
    ConfirmedAmountFrom:/**@type {HTMLSpanElement}*/(children[ConfirmedAmountFrom]),
    ExchangeApiWr:/**@type {HTMLDivElement}*/(children[ExchangeApiWr]),
    PaymentAddressHiddenIn:/**@type {HTMLInputElement}*/(children[PaymentAddressHiddenIn]),
    CurrencyFromLas:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=b69d35]')]):[],
    AddReviewErrorWr:/**@type {HTMLDivElement}*/(children[AddReviewErrorWr]),
    AddReviewErrorLa:/**@type {HTMLSpanElement}*/(children[AddReviewErrorLa]),
    AddReviewBu:/**@type {HTMLButtonElement}*/(children[AddReviewBu]),
    BtcWalletPicker:/**@type {HTMLDivElement}*/(children[BtcWalletPicker]),
    KycRequireds:element?/**@type {!Array<!HTMLDivElement>}*/([...element.querySelectorAll('[data-id~=b69d125]')]):[],
    KycNotRequireds:element?/**@type {!Array<!HTMLDivElement>}*/([...element.querySelectorAll('[data-id~=b69d126]')]):[],
    ExchangeEmails:element?/**@type {!Array<!HTMLDivElement>}*/([...element.querySelectorAll('[data-id~=b69d127]')]):[],
    OfferExchangeInfoRowMobWr:/**@type {HTMLDivElement}*/(children[OfferExchangeInfoRowMobWr]),
    OptionalLa:/**@type {HTMLSpanElement}*/(children[OptionalLa]),
    RequiredLa:/**@type {HTMLSpanElement}*/(children[RequiredLa]),
    CreateTransactionWr:/**@type {HTMLSpanElement}*/(children[CreateTransactionWr]),
    TransactionInfoWr:/**@type {HTMLDivElement}*/(children[TransactionInfoWr]),
    TransactionInfo:/**@type {HTMLDivElement}*/(children[TransactionInfo]),
    TransactionInfoHeadWr:/**@type {HTMLDivElement}*/(children[TransactionInfoHeadWr]),
    TransactionInfoHead:/**@type {HTMLDivElement}*/(children[TransactionInfoHead]),
    TransactionInfoHeadMobWr:/**@type {HTMLDivElement}*/(children[TransactionInfoHeadMobWr]),
    TransactionInfoHeadMob:/**@type {HTMLDivElement}*/(children[TransactionInfoHeadMob]),
    Step1ProgressCircleWrMobBot:/**@type {HTMLDivElement}*/(children[Step1ProgressCircleWrMobBot]),
    Step1LaMobBot:/**@type {HTMLDivElement}*/(children[Step1LaMobBot]),
    Step2ProgressCircleWrMobBot:/**@type {HTMLDivElement}*/(children[Step2ProgressCircleWrMobBot]),
    Step2LaMobBot:/**@type {HTMLDivElement}*/(children[Step2LaMobBot]),
    Step2CoMobTop:/**@type {HTMLDivElement}*/(children[Step2CoMobTop]),
    Step3CoMobTop:/**@type {HTMLDivElement}*/(children[Step3CoMobTop]),
    Step1CoMobBot:/**@type {HTMLDivElement}*/(children[Step1CoMobBot]),
    Step2CoMobBot:/**@type {HTMLDivElement}*/(children[Step2CoMobBot]),
    Step3CoMobBot:/**@type {HTMLDivElement}*/(children[Step3CoMobBot]),
    ReviewForm:/**@type {HTMLFormElement}*/(children[ReviewForm]),
    StatusLa:/**@type {HTMLSpanElement}*/(children[StatusLa]),
    CheckedWr:/**@type {HTMLSpanElement}*/(children[CheckedWr]),
    ExchangeStatusHintWr:/**@type {HTMLDivElement}*/(children[ExchangeStatusHintWr]),
    SendFo:/**@type {HTMLFormElement}*/(children[SendFo]),
    UpdateStatusLa:/**@type {HTMLSpanElement}*/(children[UpdateStatusLa]),
    PaymentSentLa:/**@type {HTMLSpanElement}*/(children[PaymentSentLa]),
    CheckingLoIn:/**@type {HTMLSpanElement}*/(children[CheckingLoIn]),
    SendPaymentErrorWr:/**@type {HTMLDivElement}*/(children[SendPaymentErrorWr]),
    SendPaymentError:/**@type {HTMLSpanElement}*/(children[SendPaymentError]),
    CheckStatusBu:/**@type {HTMLButtonElement}*/(children[CheckStatusBu]),
    Step2Circle:/**@type {HTMLSpanElement}*/(children[Step2Circle]),
    CreateTransactionCol:/**@type {HTMLDivElement}*/(children[CreateTransactionCol]),
    SendPaymentCol:/**@type {HTMLDivElement}*/(children[SendPaymentCol]),
    PaymentAddressInWr:/**@type {HTMLDivElement}*/(children[PaymentAddressInWr]),
    CreateTransactionErrorWr:/**@type {HTMLDivElement}*/(children[CreateTransactionErrorWr]),
    CreateTransactionError:/**@type {HTMLSpanElement}*/(children[CreateTransactionError]),
    CurrencyToLas:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=b69d36]')]):[],
    AddressLa:/**@type {HTMLLabelElement}*/(children[AddressLa]),
    ExtraId:/**@type {HTMLInputElement}*/(children[ExtraId]),
    TocLa:/**@type {HTMLSpanElement}*/(children[TocLa]),
    RefundAddressLa:/**@type {HTMLLabelElement}*/(children[RefundAddressLa]),
    RefundPopup:/**@type {HTMLElement}*/(children[RefundPopup]),
    SendPaymentCollapsar:/**@type {HTMLElement}*/(children[SendPaymentCollapsar]),
    CreateTransactionCollapsar:/**@type {HTMLElement}*/(children[CreateTransactionCollapsar]),
    FinishedCollapsar:/**@type {HTMLElement}*/(children[FinishedCollapsar]),
    ProgressColumn:/**@type {HTMLElement}*/(children[ProgressColumn]),
    ProgressColumnMobTop:/**@type {HTMLElement}*/(children[ProgressColumnMobTop]),
    ProgressColumnMobBot:/**@type {HTMLElement}*/(children[ProgressColumnMobBot]),
    SwapeeWalletPicker:/**@type {HTMLElement}*/(children[SwapeeWalletPicker]),
    ExchangeBroker:/**@type {HTMLElement}*/(children[ExchangeBroker]),
    DealBroker:DealBroker,
    ExchangeIntent:ExchangeIntent,
    ExchangeStatusHint:/**@type {HTMLElement}*/(children[ExchangeStatusHint]),
    OfferExchangeInfoRow:/**@type {HTMLElement}*/(children[OfferExchangeInfoRow]),
    OfferExchangeInfoRowMob:/**@type {HTMLElement}*/(children[OfferExchangeInfoRowMob]),
    ExchangeStatusTitle:/**@type {HTMLElement}*/(children[ExchangeStatusTitle]),
    PaymentLastChecked:/**@type {HTMLElement}*/(children[PaymentLastChecked]),
    Duration:/**@type {HTMLElement}*/(children[Duration]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.OfferExchangeDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IOfferExchangeDisplay.Initialese}*/({
   TransactionCurrencyFromLa:1,
   ConfirmedAmountFrom:1,
   RestartBu:1,
   ExchangeApiWr:1,
   PaymentAddressHiddenIn:1,
   CurrencyFromLas:1,
   AddressIn:1,
   TocIn:1,
   CreateTransactionBu:1,
   AddReviewErrorWr:1,
   AddReviewErrorLa:1,
   AddReviewBu:1,
   WalletIn:1,
   WalletOut:1,
   BtcWalletPicker:1,
   BtcWalletPickerCont:1,
   KycRequireds:1,
   KycNotRequireds:1,
   ExchangeEmails:1,
   OfferExchangeInfoRowMobWr:1,
   OptionalLa:1,
   RequiredLa:1,
   CreateTransactionWr:1,
   TransactionInfoWr:1,
   TransactionInfo:1,
   TransactionInfoHeadWr:1,
   TransactionInfoHead:1,
   TransactionInfoHeadMobWr:1,
   TransactionInfoHeadMob:1,
   PayinAddressCopiedLa:1,
   CopyPayinAddressBu:1,
   Step1ProgressCircleWrMobBot:1,
   Step1LaMobBot:1,
   Step2ProgressCircleWrMobBot:1,
   Step2LaMobBot:1,
   Step2CoMobTop:1,
   Step3CoMobTop:1,
   Step1CoMobBot:1,
   Step2CoMobBot:1,
   Step3CoMobBot:1,
   ReviewForm:1,
   StatusLa:1,
   CheckedWr:1,
   ExchangeStatusHintWr:1,
   SendFo:1,
   UpdateStatusLa:1,
   PaymentSentLa:1,
   CheckingLoIn:1,
   SendPaymentErrorWr:1,
   SendPaymentError:1,
   CheckStatusBu:1,
   Step2Circle:1,
   CreateTransactionCol:1,
   SendPaymentCol:1,
   PaymentAddressInWr:1,
   PaymentAddressIn:1,
   CreateTransactionErrorWr:1,
   CreateTransactionError:1,
   CurrencyToLas:1,
   AddressLa:1,
   ExtraId:1,
   TocLa:1,
   RefundAddressIn:1,
   RefundAddressLa:1,
   AddressPopup:1,
   RefundPopup:1,
   SendPaymentCollapsar:1,
   CreateTransactionCollapsar:1,
   FinishedCollapsar:1,
   ProgressColumn:1,
   ProgressColumnMobTop:1,
   ProgressColumnMobBot:1,
   SwapeeWalletPicker:1,
   ExchangeBroker:1,
   DealBroker:1,
   ExchangeIntent:1,
   ExchangeStatusHint:1,
   OfferExchangeInfoRow:1,
   OfferExchangeInfoRowMob:1,
   ExchangeStatusTitle:1,
   PaymentLastChecked:1,
   Duration:1,
  }),
  initializer({
   TransactionCurrencyFromLa:_TransactionCurrencyFromLa,
   ConfirmedAmountFrom:_ConfirmedAmountFrom,
   RestartBu:_RestartBu,
   ExchangeApiWr:_ExchangeApiWr,
   PaymentAddressHiddenIn:_PaymentAddressHiddenIn,
   CurrencyFromLas:_CurrencyFromLas,
   AddressIn:_AddressIn,
   TocIn:_TocIn,
   CreateTransactionBu:_CreateTransactionBu,
   AddReviewErrorWr:_AddReviewErrorWr,
   AddReviewErrorLa:_AddReviewErrorLa,
   AddReviewBu:_AddReviewBu,
   WalletIn:_WalletIn,
   WalletOut:_WalletOut,
   BtcWalletPicker:_BtcWalletPicker,
   BtcWalletPickerCont:_BtcWalletPickerCont,
   KycRequireds:_KycRequireds,
   KycNotRequireds:_KycNotRequireds,
   ExchangeEmails:_ExchangeEmails,
   OfferExchangeInfoRowMobWr:_OfferExchangeInfoRowMobWr,
   OptionalLa:_OptionalLa,
   RequiredLa:_RequiredLa,
   CreateTransactionWr:_CreateTransactionWr,
   TransactionInfoWr:_TransactionInfoWr,
   TransactionInfo:_TransactionInfo,
   TransactionInfoHeadWr:_TransactionInfoHeadWr,
   TransactionInfoHead:_TransactionInfoHead,
   TransactionInfoHeadMobWr:_TransactionInfoHeadMobWr,
   TransactionInfoHeadMob:_TransactionInfoHeadMob,
   PayinAddressCopiedLa:_PayinAddressCopiedLa,
   CopyPayinAddressBu:_CopyPayinAddressBu,
   Step1ProgressCircleWrMobBot:_Step1ProgressCircleWrMobBot,
   Step1LaMobBot:_Step1LaMobBot,
   Step2ProgressCircleWrMobBot:_Step2ProgressCircleWrMobBot,
   Step2LaMobBot:_Step2LaMobBot,
   Step2CoMobTop:_Step2CoMobTop,
   Step3CoMobTop:_Step3CoMobTop,
   Step1CoMobBot:_Step1CoMobBot,
   Step2CoMobBot:_Step2CoMobBot,
   Step3CoMobBot:_Step3CoMobBot,
   ReviewForm:_ReviewForm,
   StatusLa:_StatusLa,
   CheckedWr:_CheckedWr,
   ExchangeStatusHintWr:_ExchangeStatusHintWr,
   SendFo:_SendFo,
   UpdateStatusLa:_UpdateStatusLa,
   PaymentSentLa:_PaymentSentLa,
   CheckingLoIn:_CheckingLoIn,
   SendPaymentErrorWr:_SendPaymentErrorWr,
   SendPaymentError:_SendPaymentError,
   CheckStatusBu:_CheckStatusBu,
   Step2Circle:_Step2Circle,
   CreateTransactionCol:_CreateTransactionCol,
   SendPaymentCol:_SendPaymentCol,
   PaymentAddressInWr:_PaymentAddressInWr,
   PaymentAddressIn:_PaymentAddressIn,
   CreateTransactionErrorWr:_CreateTransactionErrorWr,
   CreateTransactionError:_CreateTransactionError,
   CurrencyToLas:_CurrencyToLas,
   AddressLa:_AddressLa,
   ExtraId:_ExtraId,
   TocLa:_TocLa,
   RefundAddressIn:_RefundAddressIn,
   RefundAddressLa:_RefundAddressLa,
   AddressPopup:_AddressPopup,
   RefundPopup:_RefundPopup,
   SendPaymentCollapsar:_SendPaymentCollapsar,
   CreateTransactionCollapsar:_CreateTransactionCollapsar,
   FinishedCollapsar:_FinishedCollapsar,
   ProgressColumn:_ProgressColumn,
   ProgressColumnMobTop:_ProgressColumnMobTop,
   ProgressColumnMobBot:_ProgressColumnMobBot,
   SwapeeWalletPicker:_SwapeeWalletPicker,
   ExchangeBroker:_ExchangeBroker,
   DealBroker:_DealBroker,
   ExchangeIntent:_ExchangeIntent,
   ExchangeStatusHint:_ExchangeStatusHint,
   OfferExchangeInfoRow:_OfferExchangeInfoRow,
   OfferExchangeInfoRowMob:_OfferExchangeInfoRowMob,
   ExchangeStatusTitle:_ExchangeStatusTitle,
   PaymentLastChecked:_PaymentLastChecked,
   Duration:_Duration,
  }) {
   if(_TransactionCurrencyFromLa!==undefined) this.TransactionCurrencyFromLa=_TransactionCurrencyFromLa
   if(_ConfirmedAmountFrom!==undefined) this.ConfirmedAmountFrom=_ConfirmedAmountFrom
   if(_RestartBu!==undefined) this.RestartBu=_RestartBu
   if(_ExchangeApiWr!==undefined) this.ExchangeApiWr=_ExchangeApiWr
   if(_PaymentAddressHiddenIn!==undefined) this.PaymentAddressHiddenIn=_PaymentAddressHiddenIn
   if(_CurrencyFromLas!==undefined) this.CurrencyFromLas=_CurrencyFromLas
   if(_AddressIn!==undefined) this.AddressIn=_AddressIn
   if(_TocIn!==undefined) this.TocIn=_TocIn
   if(_CreateTransactionBu!==undefined) this.CreateTransactionBu=_CreateTransactionBu
   if(_AddReviewErrorWr!==undefined) this.AddReviewErrorWr=_AddReviewErrorWr
   if(_AddReviewErrorLa!==undefined) this.AddReviewErrorLa=_AddReviewErrorLa
   if(_AddReviewBu!==undefined) this.AddReviewBu=_AddReviewBu
   if(_WalletIn!==undefined) this.WalletIn=_WalletIn
   if(_WalletOut!==undefined) this.WalletOut=_WalletOut
   if(_BtcWalletPicker!==undefined) this.BtcWalletPicker=_BtcWalletPicker
   if(_BtcWalletPickerCont!==undefined) this.BtcWalletPickerCont=_BtcWalletPickerCont
   if(_KycRequireds!==undefined) this.KycRequireds=_KycRequireds
   if(_KycNotRequireds!==undefined) this.KycNotRequireds=_KycNotRequireds
   if(_ExchangeEmails!==undefined) this.ExchangeEmails=_ExchangeEmails
   if(_OfferExchangeInfoRowMobWr!==undefined) this.OfferExchangeInfoRowMobWr=_OfferExchangeInfoRowMobWr
   if(_OptionalLa!==undefined) this.OptionalLa=_OptionalLa
   if(_RequiredLa!==undefined) this.RequiredLa=_RequiredLa
   if(_CreateTransactionWr!==undefined) this.CreateTransactionWr=_CreateTransactionWr
   if(_TransactionInfoWr!==undefined) this.TransactionInfoWr=_TransactionInfoWr
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
   if(_TransactionInfoHeadWr!==undefined) this.TransactionInfoHeadWr=_TransactionInfoHeadWr
   if(_TransactionInfoHead!==undefined) this.TransactionInfoHead=_TransactionInfoHead
   if(_TransactionInfoHeadMobWr!==undefined) this.TransactionInfoHeadMobWr=_TransactionInfoHeadMobWr
   if(_TransactionInfoHeadMob!==undefined) this.TransactionInfoHeadMob=_TransactionInfoHeadMob
   if(_PayinAddressCopiedLa!==undefined) this.PayinAddressCopiedLa=_PayinAddressCopiedLa
   if(_CopyPayinAddressBu!==undefined) this.CopyPayinAddressBu=_CopyPayinAddressBu
   if(_Step1ProgressCircleWrMobBot!==undefined) this.Step1ProgressCircleWrMobBot=_Step1ProgressCircleWrMobBot
   if(_Step1LaMobBot!==undefined) this.Step1LaMobBot=_Step1LaMobBot
   if(_Step2ProgressCircleWrMobBot!==undefined) this.Step2ProgressCircleWrMobBot=_Step2ProgressCircleWrMobBot
   if(_Step2LaMobBot!==undefined) this.Step2LaMobBot=_Step2LaMobBot
   if(_Step2CoMobTop!==undefined) this.Step2CoMobTop=_Step2CoMobTop
   if(_Step3CoMobTop!==undefined) this.Step3CoMobTop=_Step3CoMobTop
   if(_Step1CoMobBot!==undefined) this.Step1CoMobBot=_Step1CoMobBot
   if(_Step2CoMobBot!==undefined) this.Step2CoMobBot=_Step2CoMobBot
   if(_Step3CoMobBot!==undefined) this.Step3CoMobBot=_Step3CoMobBot
   if(_ReviewForm!==undefined) this.ReviewForm=_ReviewForm
   if(_StatusLa!==undefined) this.StatusLa=_StatusLa
   if(_CheckedWr!==undefined) this.CheckedWr=_CheckedWr
   if(_ExchangeStatusHintWr!==undefined) this.ExchangeStatusHintWr=_ExchangeStatusHintWr
   if(_SendFo!==undefined) this.SendFo=_SendFo
   if(_UpdateStatusLa!==undefined) this.UpdateStatusLa=_UpdateStatusLa
   if(_PaymentSentLa!==undefined) this.PaymentSentLa=_PaymentSentLa
   if(_CheckingLoIn!==undefined) this.CheckingLoIn=_CheckingLoIn
   if(_SendPaymentErrorWr!==undefined) this.SendPaymentErrorWr=_SendPaymentErrorWr
   if(_SendPaymentError!==undefined) this.SendPaymentError=_SendPaymentError
   if(_CheckStatusBu!==undefined) this.CheckStatusBu=_CheckStatusBu
   if(_Step2Circle!==undefined) this.Step2Circle=_Step2Circle
   if(_CreateTransactionCol!==undefined) this.CreateTransactionCol=_CreateTransactionCol
   if(_SendPaymentCol!==undefined) this.SendPaymentCol=_SendPaymentCol
   if(_PaymentAddressInWr!==undefined) this.PaymentAddressInWr=_PaymentAddressInWr
   if(_PaymentAddressIn!==undefined) this.PaymentAddressIn=_PaymentAddressIn
   if(_CreateTransactionErrorWr!==undefined) this.CreateTransactionErrorWr=_CreateTransactionErrorWr
   if(_CreateTransactionError!==undefined) this.CreateTransactionError=_CreateTransactionError
   if(_CurrencyToLas!==undefined) this.CurrencyToLas=_CurrencyToLas
   if(_AddressLa!==undefined) this.AddressLa=_AddressLa
   if(_ExtraId!==undefined) this.ExtraId=_ExtraId
   if(_TocLa!==undefined) this.TocLa=_TocLa
   if(_RefundAddressIn!==undefined) this.RefundAddressIn=_RefundAddressIn
   if(_RefundAddressLa!==undefined) this.RefundAddressLa=_RefundAddressLa
   if(_AddressPopup!==undefined) this.AddressPopup=_AddressPopup
   if(_RefundPopup!==undefined) this.RefundPopup=_RefundPopup
   if(_SendPaymentCollapsar!==undefined) this.SendPaymentCollapsar=_SendPaymentCollapsar
   if(_CreateTransactionCollapsar!==undefined) this.CreateTransactionCollapsar=_CreateTransactionCollapsar
   if(_FinishedCollapsar!==undefined) this.FinishedCollapsar=_FinishedCollapsar
   if(_ProgressColumn!==undefined) this.ProgressColumn=_ProgressColumn
   if(_ProgressColumnMobTop!==undefined) this.ProgressColumnMobTop=_ProgressColumnMobTop
   if(_ProgressColumnMobBot!==undefined) this.ProgressColumnMobBot=_ProgressColumnMobBot
   if(_SwapeeWalletPicker!==undefined) this.SwapeeWalletPicker=_SwapeeWalletPicker
   if(_ExchangeBroker!==undefined) this.ExchangeBroker=_ExchangeBroker
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_ExchangeStatusHint!==undefined) this.ExchangeStatusHint=_ExchangeStatusHint
   if(_OfferExchangeInfoRow!==undefined) this.OfferExchangeInfoRow=_OfferExchangeInfoRow
   if(_OfferExchangeInfoRowMob!==undefined) this.OfferExchangeInfoRowMob=_OfferExchangeInfoRowMob
   if(_ExchangeStatusTitle!==undefined) this.ExchangeStatusTitle=_ExchangeStatusTitle
   if(_PaymentLastChecked!==undefined) this.PaymentLastChecked=_PaymentLastChecked
   if(_Duration!==undefined) this.Duration=_Duration
  },
 }),
]