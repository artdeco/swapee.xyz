import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeControllerAT}
 */
function __AbstractOfferExchangeControllerAT() {}
__AbstractOfferExchangeControllerAT.prototype = /** @type {!_AbstractOfferExchangeControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeControllerAT}
 */
class _AbstractOfferExchangeControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOfferExchangeControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeControllerAT} ‎
 */
class AbstractOfferExchangeControllerAT extends newAbstract(
 _AbstractOfferExchangeControllerAT,652933965126,null,{
  asIOfferExchangeControllerAT:1,
  superOfferExchangeControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeControllerAT} */
AbstractOfferExchangeControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeControllerAT} */
function AbstractOfferExchangeControllerATClass(){}

export default AbstractOfferExchangeControllerAT


AbstractOfferExchangeControllerAT[$implementations]=[
 __AbstractOfferExchangeControllerAT,
 UartUniversal,
 AbstractOfferExchangeControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IOfferExchangeControllerAT}*/({
  get asIOfferExchangeController(){
   return this
  },
  selectAddress(){
   this.uart.t("inv",{mid:'d2a09',args:[...arguments]})
  },
  swapAddresses(){
   this.uart.t("inv",{mid:'d98fc'})
  },
  selectRefund(){
   this.uart.t("inv",{mid:'8bf21',args:[...arguments]})
  },
  reset(){
   this.uart.t("inv",{mid:'a4b41'})
  },
  onSelectAddress(){
   this.uart.t("inv",{mid:'ef440',args:[...arguments]})
  },
  onSwapAddresses(){
   this.uart.t("inv",{mid:'b8b6b'})
  },
  onSelectRefund(){
   this.uart.t("inv",{mid:'e3d35',args:[...arguments]})
  },
  onReset(){
   this.uart.t("inv",{mid:'df85f'})
  },
  flipTocAccepted(){
   this.uart.t("inv",{mid:'2ae2e'})
  },
  setTocAccepted(){
   this.uart.t("inv",{mid:'1eda3'})
  },
  unsetTocAccepted(){
   this.uart.t("inv",{mid:'4b605'})
  },
  pulseCopyPayinAddress(){
   this.uart.t("inv",{mid:'1858e'})
  },
  pulseCopyPayinExtra(){
   this.uart.t("inv",{mid:'f4bb5'})
  },
 }),
]