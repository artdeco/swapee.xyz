import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeComputer}
 */
function __AbstractOfferExchangeComputer() {}
__AbstractOfferExchangeComputer.prototype = /** @type {!_AbstractOfferExchangeComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeComputer}
 */
class _AbstractOfferExchangeComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeComputer} ‎
 */
export class AbstractOfferExchangeComputer extends newAbstract(
 _AbstractOfferExchangeComputer,65293396511,null,{
  asIOfferExchangeComputer:1,
  superOfferExchangeComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeComputer} */
AbstractOfferExchangeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeComputer} */
function AbstractOfferExchangeComputerClass(){}


AbstractOfferExchangeComputer[$implementations]=[
 __AbstractOfferExchangeComputer,
 Adapter,
]


export default AbstractOfferExchangeComputer