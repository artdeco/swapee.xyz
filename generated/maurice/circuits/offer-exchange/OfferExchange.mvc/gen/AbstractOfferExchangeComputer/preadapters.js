
/**@this {xyz.swapee.wc.IOfferExchangeComputer}*/
export function preadaptPickBtc(inputs,changes,mapForm) {
 const{SwapeeWalletPicker:SwapeeWalletPicker}=this.land
 if(!SwapeeWalletPicker) return
 /**@type {!xyz.swapee.wc.IOfferExchangeComputer.adaptPickBtc.Form}*/
 const _inputs={
  invoice:SwapeeWalletPicker.model['e5f96'],
  btcInput:inputs.btcInput,
  btcOutput:inputs.btcOutput,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.invoice) return
 changes.invoice=changes['e5f96']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPickBtc(__inputs,__changes)
 const{address:address,refundAddress:refundAddress,...REST}=RET
 const{land:{ExchangeBroker:ExchangeBroker}}=this
 if(ExchangeBroker) ExchangeBroker.port.inputs['884d9']=address
 if(ExchangeBroker) ExchangeBroker.port.inputs['be03a']=refundAddress
 return REST
}

/**@this {xyz.swapee.wc.IOfferExchangeComputer}*/
export function preadaptBtc(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOfferExchangeComputer.adaptBtc.Form}*/
 const _inputs={
  currencyFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['96c88']:void 0,
  currencyTo:this.land.ExchangeIntent?this.land.ExchangeIntent.model['c23cd']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.currencyFrom)) return
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptBtc(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOfferExchangeComputer}*/
export function preadaptCanCreateTransaction(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOfferExchangeComputer.adaptCanCreateTransaction.Form}*/
 const _inputs={
  tocAccepted:inputs.tocAccepted,
  fixed:this.land.ExchangeIntent?this.land.ExchangeIntent.model['cec31']:void 0,
  creatingTransaction:this.land.ExchangeBroker?this.land.ExchangeBroker.model['89fc6']:void 0,
  address:this.land.ExchangeBroker?this.land.ExchangeBroker.model['884d9']:void 0,
  refundAddress:this.land.ExchangeBroker?this.land.ExchangeBroker.model['be03a']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.address)) return
 changes.fixed=changes['cec31']
 changes.creatingTransaction=changes['89fc6']
 changes.address=changes['884d9']
 changes.refundAddress=changes['be03a']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCanCreateTransaction(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IOfferExchangeComputer}*/
export function preadaptPaymentFailed(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOfferExchangeComputer.adaptPaymentFailed.Form}*/
 const _inputs={
  status:this.land.ExchangeBroker?this.land.ExchangeBroker.model['9acb4']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.status)) return
 changes.status=changes['9acb4']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPaymentFailed(__inputs,__changes)
 return RET
}