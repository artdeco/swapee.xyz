/** @type {xyz.swapee.wc.IOfferExchangeDisplay._paintCopyPayinAddress} */
export default function paintCopyPayinAddress(_,{ExchangeBroker:{payinAddress:payinAddress}}) { // todo: somehow make this reusable via "copy" attribute?
 const{asIOfferExchangeTouchscreen:{
  classes:{Copied:Copied,CopiedRemoving:CopiedRemoving},
  asIScreen:{copyToClipboard:copyToClipboard},
  // pulsePayinAddressCopied:pulsePayinAddressCopied,
  setInputs:setInputs,
 },asIOfferExchangeDisplay:{PayinAddressCopiedLa},
 }=/**@type {!xyz.swapee.wc.IOfferExchangeDisplay}*/(this)
 copyToClipboard(payinAddress)
 // pulsePayinAddressCopied()
 if(PayinAddressCopiedLa.classList.contains(Copied)||PayinAddressCopiedLa.classList.contains(CopiedRemoving)) {
  return
 }
 // setTimeout(()=>{
 //  setInputs({payinAddressCopied:true,payinAddressJustCopied:false})
 // },1)
 const play=()=>{
  setInputs({payinAddressCopied:true,payinAddressJustCopied:false})
  setTimeout(()=>{
   setInputs({payinAddressJustCopied:true})
   setTimeout(()=>{
    setInputs({payinAddressCopied:false,payinAddressJustCopied:false})
   },310)
  },1100)
 }

 play()
}