import paintCopyPayinAddress from './methods/paint-copy-payin-address'
import prepaintCopyPayinAddress from '../../../parts/40-display/src/methods/prepaint-copy-payin-address'
import AbstractOfferExchangeDisplay from '../../gen/AbstractOfferExchangeDisplay'

/** @extends {xyz.swapee.wc.OfferExchangeDisplay} */
export default class extends AbstractOfferExchangeDisplay.implements(
 /**@type {!xyz.swapee.wc.IOfferExchangeDisplay}*/({
  paint:[
   prepaintCopyPayinAddress,
  ],
 }),
 /** @type {!xyz.swapee.wc.IOfferExchangeDisplay} */ ({
  paintCopyPayinAddress:paintCopyPayinAddress,
 }),
){}