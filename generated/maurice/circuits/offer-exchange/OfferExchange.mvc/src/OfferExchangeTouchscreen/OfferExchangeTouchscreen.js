import deduceInputs from './methods/deduce-inputs'
import AbstractOfferExchangeControllerAT from '../../gen/AbstractOfferExchangeControllerAT'
import HyperOfferExchangeInterruptLine from '../../../parts/120-interrupt-line/src/OfferExchangeInterruptLine/aop/HyperOfferExchangeInterruptLine'
import OfferExchangeDisplay from '../OfferExchangeDisplay'
import AbstractOfferExchangeTouchscreen from '../../gen/AbstractOfferExchangeTouchscreen'

/** @extends {xyz.swapee.wc.OfferExchangeTouchscreen} */
export default class extends AbstractOfferExchangeTouchscreen.implements(
 AbstractOfferExchangeControllerAT,
 HyperOfferExchangeInterruptLine,
 OfferExchangeDisplay,
 /**@type {!xyz.swapee.wc.IOfferExchangeTouchscreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IOfferExchangeTouchscreen} */ ({
  deduceInputs:deduceInputs,
  __$id:6529339651,
 }),
){}