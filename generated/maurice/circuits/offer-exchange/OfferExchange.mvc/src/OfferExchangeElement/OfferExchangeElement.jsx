import solder from './methods/solder'
import buildTransactionInfo from './methods/build-transaction-info'
import buildExchangeIntent from './methods/build-exchange-intent'
import buildExchangeBroker from './methods/build-exchange-broker'
import server from './methods/server'
import render from './methods/render'
import borrowClasses from './methods/borrow-classes'
import OfferExchangeServerController from '../OfferExchangeServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractOfferExchangeElement from '../../gen/AbstractOfferExchangeElement'

/** @extends {xyz.swapee.wc.OfferExchangeElement} */
export default class OfferExchangeElement extends AbstractOfferExchangeElement.implements(
 OfferExchangeServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /**@type {!xyz.swapee.wc.OfferExchangeElement}*/({
 get addressInSel(){
 const{asIOfferExchangeElement:{xAddressIn:xAddressIn}}=this
 return`[${xAddressIn}]`
 },
 get tocInSel(){
 const{asIOfferExchangeElement:{xTocIn:xTocIn}}=this
 return`[${xTocIn}]`
 },
 get paymentAddressInSel(){
 const{asIOfferExchangeElement:{xPaymentAddressIn:xPaymentAddressIn}}=this
 return`[${xPaymentAddressIn}]`
 },
 get refundAddressInSel(){
 const{asIOfferExchangeElement:{xRefundAddressIn:xRefundAddressIn}}=this
 return`[${xRefundAddressIn}]`
 },
 get exchangeBrokerSel(){
 const{asIOfferExchangeElement:{xExchangeBroker:xExchangeBroker}}=this
 return`[${xExchangeBroker}]`
 },
 render: function renderSelectors(){
 const{asIOfferExchangeElement:{
  xAddressIn:xAddressIn,
  xTocIn:xTocIn,
  xPaymentAddressIn:xPaymentAddressIn,
  xRefundAddressIn:xRefundAddressIn,
  xExchangeBroker:xExchangeBroker,
 }}=this
 return (<div $id="OfferExchange">
  <div $id="AddressIn" {...{[xAddressIn]:true}}  />
  <div $id="TocIn" {...{[xTocIn]:true}}  />
  <div $id="PaymentAddressIn" {...{[xPaymentAddressIn]:true}}  />
  <div $id="RefundAddressIn" {...{[xRefundAddressIn]:true}}  />
  <div $id="ExchangeBroker" {...{[xExchangeBroker]:true}}  />
 </div>)
 },
 allocator: function allocateSelectors(){
 const{
  attributes:{id:id},asIMaurice:{page:page},
  asIOfferExchangeElement:{
   X_ADDRESS_IN:X_ADDRESS_IN,
   X_TOC_IN:X_TOC_IN,
   X_PAYMENT_ADDRESS_IN:X_PAYMENT_ADDRESS_IN,
   X_REFUND_ADDRESS_IN:X_REFUND_ADDRESS_IN,
   X_EXCHANGE_BROKER:X_EXCHANGE_BROKER,
  },
  asIElement:{ids:ids},
  asIShortId:{getShortName:getShortName},
 }=this
 if(!page) return // buildees
 let xAddressIn=X_ADDRESS_IN
 const addressInKey='x:xyz.swapee.wc.IOfferExchange::X_ADDRESS_IN'
 if(id){
  xAddressIn+=`-${id}`
 }else if(addressInKey in page){
  let c=page[addressInKey]
  c++
  page[addressInKey]=c
  xAddressIn+=`-${c}`
 }else{
  page[addressInKey]=0
 }
 this.xAddressIn=xAddressIn
 Object.defineProperty(ids,'AddressIn',{get(){
  const _id='i'+getShortName(xAddressIn,7)
  return _id
 }})
 let xTocIn=X_TOC_IN
 const tocInKey='x:xyz.swapee.wc.IOfferExchange::X_TOC_IN'
 if(id){
  xTocIn+=`-${id}`
 }else if(tocInKey in page){
  let c=page[tocInKey]
  c++
  page[tocInKey]=c
  xTocIn+=`-${c}`
 }else{
  page[tocInKey]=0
 }
 this.xTocIn=xTocIn
 Object.defineProperty(ids,'TocIn',{get(){
  const _id='i'+getShortName(xTocIn,7)
  return _id
 }})
 let xPaymentAddressIn=X_PAYMENT_ADDRESS_IN
 const paymentAddressInKey='x:xyz.swapee.wc.IOfferExchange::X_PAYMENT_ADDRESS_IN'
 if(id){
  xPaymentAddressIn+=`-${id}`
 }else if(paymentAddressInKey in page){
  let c=page[paymentAddressInKey]
  c++
  page[paymentAddressInKey]=c
  xPaymentAddressIn+=`-${c}`
 }else{
  page[paymentAddressInKey]=0
 }
 this.xPaymentAddressIn=xPaymentAddressIn
 Object.defineProperty(ids,'PaymentAddressIn',{get(){
  const _id='i'+getShortName(xPaymentAddressIn,7)
  return _id
 }})
 let xRefundAddressIn=X_REFUND_ADDRESS_IN
 const refundAddressInKey='x:xyz.swapee.wc.IOfferExchange::X_REFUND_ADDRESS_IN'
 if(id){
  xRefundAddressIn+=`-${id}`
 }else if(refundAddressInKey in page){
  let c=page[refundAddressInKey]
  c++
  page[refundAddressInKey]=c
  xRefundAddressIn+=`-${c}`
 }else{
  page[refundAddressInKey]=0
 }
 this.xRefundAddressIn=xRefundAddressIn
 Object.defineProperty(ids,'RefundAddressIn',{get(){
  const _id='i'+getShortName(xRefundAddressIn,7)
  return _id
 }})
 let xExchangeBroker=X_EXCHANGE_BROKER
 const exchangeBrokerKey='x:xyz.swapee.wc.IOfferExchange::X_EXCHANGE_BROKER'
 if(id){
  xExchangeBroker+=`-${id}`
 }else if(exchangeBrokerKey in page){
  let c=page[exchangeBrokerKey]
  c++
  page[exchangeBrokerKey]=c
  xExchangeBroker+=`-${c}`
 }else{
  page[exchangeBrokerKey]=0
 }
 this.xExchangeBroker=xExchangeBroker
 },
 }),
 /** @type {!xyz.swapee.wc.IOfferExchangeElement} */ ({
  solder:solder,
  buildTransactionInfo:buildTransactionInfo,
  buildExchangeIntent:buildExchangeIntent,
  buildExchangeBroker:buildExchangeBroker,
  server:server,
  render:render,
  borrowClasses:borrowClasses,
 }),
/**@type {!xyz.swapee.wc.IOfferExchangeElement}*/({
   classesMap: true,
   rootSelector:     `.OfferExchange`,
   blockers: [
    'xyz.swapee.wc.IProgressColumn',
   ],
   preserveClasses:  ['ProgressColumn'],
   stylesheet:       [
    'html/styles/OfferExchange.css',
    // 'html/styles/ExchangeIdRow.css',
   ],
   blockName:        'html/OfferExchangeBlock.html',
   // select({}) { // todo: selectors wiring.
   //  return <>
   //  </>
   // },
   // todo: select method that accepts selectors
   // query:exchange-intent={`[${xExchangeIntent}]`}
   // query:exchange-intent={`[${xExchangeIntent}]`}
  }),
  /** @type {xyz.swapee.wc.IOfferExchangeDesigner} */({
  }),
){}

// thank you for using web circuits
