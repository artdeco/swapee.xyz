/** @type {xyz.swapee.wc.IOfferExchangeElement._inducer} */
export default function inducer({},{exchangeApiContent:exchangeApiContent}) {
 return(<offer-exchange>
  <exchange-api>{exchangeApiContent}</exchange-api>
 </offer-exchange>)
}