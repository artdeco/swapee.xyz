/** @type {xyz.swapee.wc.IOfferExchangeElement._short} */
export default function short({
 paymentFailed:paymentFailed,
},{
 ProgressColumn:ProgressColumn,
 ProgressColumnMobBot:ProgressColumnMobBot,
 ProgressColumnMobTop:ProgressColumnMobTop,

 CreateTransactionCollapsar:CreateTransactionCollapsar,
 SendPaymentCollapsar:SendPaymentCollapsar,
 PaymentLastChecked:PaymentLastChecked,
 ExchangeStatusHint:ExchangeStatusHint,
 ExchangeStatusTitle:ExchangeStatusTitle,
 FinishedCollapsar:FinishedCollapsar,
 Duration:Duration,
},{ExchangeBroker:{
 paymentCompleted:paymentCompleted,
 paymentExpiredOrOverdue:paymentExpiredOrOverdue,
 id:id,Id:Id,notId:notId,status:status,
 paymentStatus:paymentStatus,
 createdDate:createdDate,
 finishedDate:finishedDate,
 exchangeFinished:exchangeFinished,
 statusLastChecked:statusLastChecked,
 creatingTransaction:creatingTransaction,
 checkingPaymentStatus:checkingPaymentStatus,
 checkingExchangeStatus:checkingExchangeStatus,
}}){
 // todo: $revealIn -> $collapseIn
 return(<>
  <Duration startDate={createdDate} endDate={finishedDate} />
  <ProgressColumn
   loadingStep1={creatingTransaction}
   loadingStep2={checkingPaymentStatus}
   loadingStep3={checkingExchangeStatus}
   transactionId={id}
   paymentFailed={paymentFailed}
   paymentCompleted={paymentCompleted}
   paymentStatus={paymentStatus}
   status={status}
   finishedOnPayment={paymentExpiredOrOverdue}
  />
  <ProgressColumnMobTop
   loadingStep1={creatingTransaction}
   loadingStep2={checkingPaymentStatus}
   loadingStep3={checkingExchangeStatus}
   transactionId={id}
   paymentFailed={paymentFailed}
   paymentCompleted={paymentCompleted}
   paymentStatus={paymentStatus}
   status={status}
   finishedOnPayment={paymentExpiredOrOverdue}
  />
  <ProgressColumnMobBot
   loadingStep1={creatingTransaction}
   loadingStep2={checkingPaymentStatus}
   loadingStep3={checkingExchangeStatus}
   transactionId={id}
   paymentFailed={paymentFailed}
   paymentCompleted={paymentCompleted}
   paymentStatus={paymentStatus}
   status={status}
   finishedOnPayment={paymentExpiredOrOverdue}
  />
  <CreateTransactionCollapsar collapsed={Id} />
  <SendPaymentCollapsar       collapsed={notId} />
  <FinishedCollapsar          expanded={exchangeFinished} />

  <ExchangeStatusTitle        status={status} />
  <ExchangeStatusHint         status={status} />

  <PaymentLastChecked refDate={statusLastChecked} />
 </>)
}