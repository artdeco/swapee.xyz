/** @type {xyz.swapee.wc.IOfferExchangeElement._buildTransactionInfo} */
export default function buildTransactionInfo({currencyFrom:currencyFrom}) {
 return (<div $id="OfferExchange">
  <label $id="TransactionCurrencyFromLa" $reveal={currencyFrom}>{currencyFrom}</label>
 </div>)
}