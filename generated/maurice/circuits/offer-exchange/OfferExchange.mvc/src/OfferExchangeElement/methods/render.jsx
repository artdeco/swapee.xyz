/** @type {xyz.swapee.wc.IOfferExchangeElement._render} */
export default function OfferExchangeRender({
 // currencyTo:currencyTo,currencyFrom:currencyFrom,
 // address:address,refundAddress:refundAddress,
 // payinAddress:payinAddress,
 // id:id,
 btcInput:btcInput,btcOutput:btcOutput,
 payinAddressCopied:payinAddressCopied,
 payinAddressJustCopied:payinAddressJustCopied,
 canCreateTransaction:canCreateTransaction,
 // creatingTransaction:creatingTransaction, // todo: mesh for render
 // address:address, // todo: mesh for render
 tocAccepted:tocAccepted,
},{
 atInputFocus:atInputFocus,atInputBlur:atInputBlur,
 setTocAccepted,unsetTocAccepted,atAddressPopupClick,
 pulseCopyPayinAddress,reset:reset,BtcWalletPicker,
}) {
 let ev={target:{}}

 return (<div $id="OfferExchange">
  {/* todo: easy two way binding? */}

  <div $id="BtcWalletPickerCont">
   <BtcWalletPicker $append={!btcInput&&!btcOutput} />
  </div>
  <div $id="WalletOut">
   <BtcWalletPicker $append={btcInput} />
  </div>
  <div $id="WalletIn">
   <BtcWalletPicker $append={btcOutput} />
  </div>

  <input $id="AddressIn" onFocus={atInputFocus(ev)} onBlur={atInputBlur(ev)} />
  <input $id="RefundAddressIn" onFocus={atInputFocus(ev)} onBlur={atInputBlur(ev)} />

  {/* todo: allow to pass value to boolean setters */}
  {/* todo: easy checkers  */}
  <input $id="TocIn" checked={tocAccepted} onChange={ev.target.checked?setTocAccepted():unsetTocAccepted()} />
  {/* todo: enabled attribute */}
  <button $id="CreateTransactionBu" disabled={!canCreateTransaction} />
  <button $id="CopyPayinAddressBu" onClick={pulseCopyPayinAddress} />

  <input $id="PaymentAddressIn" onClick={ev.target.select} />
  <span $id="PayinAddressCopiedLa" Copied={payinAddressCopied} CopiedRemoving={payinAddressJustCopied} />

  <button $id="RestartBu" onClick={reset} />

  {/* CircleWaiting={canCreateTransaction} */}
  <div $id="AddressPopup" onClick={atAddressPopupClick(ev)} />
 </div>)
}