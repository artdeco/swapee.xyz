/** @type {xyz.swapee.wc.IOfferExchangeElement._buildExchangeBroker} */
export default function buildExchangeBroker({ // exchange broker is inside the offer-exchange
 payinAddress,address,
 creatingTransaction:creatingTransaction,
 createTransactionError:createTransactionError,
 checkPaymentError:checkPaymentError,
 checkingStatus:checkingStatus,
 status:status,
 Id:Id,notId,
 kycRequired:kycRequired,
 // paymentCompleted:paymentCompleted,
 paymentReceived:paymentReceived,
 paymentExpiredOrOverdue:paymentExpiredOrOverdue,
 exchangeComplete:exchangeComplete,
 refundAddress:refundAddress, // todo: deduce inputs to the peer component?
 confirmedAmountFrom:confirmedAmountFrom,
 waitingForPayment:waitingForPayment,
 processingPayment:processingPayment,
 // exchangeFinished:exchangeFinished,
},{
 pulseCreateTransaction:pulseCreateTransaction,
 setRefundAddress:setRefundAddress,setAddress:setAddress,
 pulseCheckPayment:pulseCheckPayment,
}){
 return <div $id="OfferExchange">
  <button $id="CreateTransactionBu" onClick={pulseCreateTransaction} Loading={creatingTransaction} />
  <span $id="CreateTransactionErrorWr" $reveal={createTransactionError}>
   <span $id="CreateTransactionError">{createTransactionError}</span>
  </span>

  <div $id="KycRequired" $reveal={kycRequired===true||status=='hold'} />
  <div $id="KycNotRequired" $reveal={kycRequired===false} />

  <input $id="AddressIn" value={address} onInput={setAddress} />
  <input $id="RefundAddressIn" value={refundAddress} onInput={setRefundAddress} />

  <div $id="PaymentAddressInWr" $conceal={paymentExpiredOrOverdue}>
   <input $id="PaymentAddressIn" value={payinAddress} disabled={!waitingForPayment} />
  </div>

  <button $id="CheckStatusBu" $reveal={waitingForPayment||processingPayment} $conceal={paymentExpiredOrOverdue}
   onClick={pulseCheckPayment}
   disabled={checkingStatus}
   Loading={checkingStatus&&status!='new'}
  />
  <span $id="SendPaymentErrorWr" $reveal={checkPaymentError}>
   <span $id="SendPaymentError">{checkPaymentError}</span>
  </span>

  <input $id="PaymentAddressHiddenIn" $reveal={paymentExpiredOrOverdue} value={'*'.repeat(payinAddress.length)} />

  <span $id="CheckedWr" $conceal={checkingStatus||exchangeComplete} />
  <span $id="ExchangeStatusHintWr" $conceal={status=='new'} HintRed={['failed','refunded','overdue','hold','expired'].includes(status)} />

  <span $id="CheckingLoIn" $reveal={checkingStatus} />
  <span $id="ConfirmedAmountFrom">{confirmedAmountFrom}</span>

  <span $id="StatusLa">{status}</span>

  <button $id="RestartBu" $reveal={paymentExpiredOrOverdue} />

  <button $id="SendFo" PaymentReceived={!waitingForPayment} />

  <button $id="PaymentSentLa" $reveal={waitingForPayment} />
  <button $id="UpdateStatusLa" $reveal={processingPayment} />

  <div $id="SendFo" $conceal={paymentReceived} />
  {/* <div $id="ReviewForm" $reveal={exchangeFinished} /> */}

  <div $id="Step2CoMobTop" $reveal={Id} />
  <div $id="Step3CoMobTop" $reveal={exchangeComplete} />

  <div $id="Step1CoMobBot" $conceal={Id} />

  <div $id="Step1ProgressCircleWrMobBot" $conceal={notId} />
  <div $id="Step1LaMobBot" $conceal={notId} />

  <div $id="Step2ProgressCircleWrMobBot" $conceal={Id} />
  <div $id="Step2LaMobBot" $conceal={Id} />
  <div $id="Step2CoMobBot" $conceal={exchangeComplete} />
  <div $id="Step3CoMobBot" $conceal={exchangeComplete} />
 </div>
}