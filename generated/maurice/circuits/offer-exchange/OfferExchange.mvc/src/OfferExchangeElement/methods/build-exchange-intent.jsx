/** @type {xyz.swapee.wc.IOfferExchangeElement._buildExchangeIntent} */
export default function buildExchangeIntent({
 fixed:fixed,ready:ready,currencyTo:currencyTo,currencyFrom:currencyFrom,
}){
 return (<div $id="OfferExchange">
  <span $id="RequiredLa" $reveal={fixed} $show={ready} />
  <span $id="OptionalLa" $reveal={!fixed} $show={ready} />
  <label $id="CurrencyFromLa" $reveal={ready}>{currencyFrom}</label>
  <label $id="CurrencyToLa" $reveal={ready}>{currencyTo}</label>
 </div>)
}