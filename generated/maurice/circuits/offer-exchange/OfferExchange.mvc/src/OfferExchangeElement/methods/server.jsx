/** @type {xyz.swapee.wc.IOfferExchangeElement._server} */
export default function server({kycEmail:kycEmail},{exchangeApiContent:exchangeApiContent,exchangeIntentSel,dealBrokerSel}) {
 // const{xExchangeBroker}=this
 // exchangeApiContent=exchangeApiContent.replace(/(<\S+\s)/,`$1query:exchange-broker="[${xExchangeBroker}]"
 //  query:deal-broker="[${xDealBroker}]"
 //  query:exchange-intent="[${exchangeIntentSel}]" $fil `.replace(/\n +/g,' '))
 return (<div $id="OfferExchange">
  {/* <div $id="ExchangeApiWr" dangerouslySetInnerHTML={{__html:exchangeApiContent}} /> */}
  <div $id="OfferExchangeInfoRow" query:deal-broker={dealBrokerSel} query:exchange-intent={exchangeIntentSel}  />
  <div $id="OfferExchangeInfoRowMob" query:deal-broker={dealBrokerSel} query:exchange-intent={exchangeIntentSel} />
  <a $id="ExchangeEmail" href={`mailto:${kycEmail}`}>{kycEmail}</a>
 </div>)
}