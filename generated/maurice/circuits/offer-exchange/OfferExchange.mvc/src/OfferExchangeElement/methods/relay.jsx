/** @type {xyz.swapee.wc.IOfferExchangeElement._relay} */
export default function relay({
 This:This,
 AddressPopup:AddressPopup,
 RefundPopup:RefundPopup,
 SwapeeWalletPicker:SwapeeWalletPicker,
 ExchangeBroker:ExchangeBroker,
 TransactionInfo:TransactionInfo,
},{ExchangeBroker:{refundAddress,address}}){
 return <>
  <This
   onSwapAddresses={[
    ExchangeBroker.setAddress(refundAddress),
    ExchangeBroker.setRefundAddress(address),
   ]}
   onSelectAddress={[AddressPopup.hide(),ExchangeBroker.setAddress]}
   onSelectRefund={RefundPopup.hide()}
   onReset={[
    ExchangeBroker.reset(),
    TransactionInfo.resetPort(),
   ]}
  />
  <ExchangeBroker onIdHigh={ExchangeBroker.pulseCheckPayment} />
  <SwapeeWalletPicker onInvoiceHigh={[
   RefundPopup.hide(),
  ]} />
 </>
}