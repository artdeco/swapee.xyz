/** @type {xyz.swapee.wc.IOfferExchangeElement._solder} */
export default function solder({
 currencyFrom:currencyFrom,currencyTo:currencyTo,amountFrom:amountFrom,
}) {
 return{
  amountFrom:amountFrom,
  currencyTo:currencyTo,
  currencyFrom:currencyFrom,
 }
}