import OfferExchangeHtmlController from '../OfferExchangeHtmlController'
import OfferExchangeHtmlComputer from '../OfferExchangeHtmlComputer'
import OfferExchangeVirtualDisplay from '../OfferExchangeVirtualDisplay'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractOfferExchangeHtmlComponent} from '../../gen/AbstractOfferExchangeHtmlComponent'

/** @extends {xyz.swapee.wc.OfferExchangeHtmlComponent} */
export default class extends AbstractOfferExchangeHtmlComponent.implements(
 OfferExchangeHtmlController,
 OfferExchangeHtmlComputer,
 OfferExchangeVirtualDisplay,
 IntegratedComponentInitialiser,
){}