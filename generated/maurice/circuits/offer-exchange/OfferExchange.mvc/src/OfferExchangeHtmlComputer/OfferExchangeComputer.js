import adaptPickBtc from './methods/adapt-pick-btc'
import adaptBtc from './methods/adapt-btc'
import adaptPaymentFailed from './methods/adapt-payment-failed'
import adaptCanCreateTransaction from './methods/adapt-can-create-transaction'
import {preadaptPickBtc,preadaptBtc,preadaptPaymentFailed,preadaptCanCreateTransaction} from '../../gen/AbstractOfferExchangeComputer/preadapters'
import AbstractOfferExchangeComputer from '../../gen/AbstractOfferExchangeComputer'

/** @extends {xyz.swapee.wc.OfferExchangeComputer} */
export default class OfferExchangeHtmlComputer extends AbstractOfferExchangeComputer.implements(
 /** @type {!xyz.swapee.wc.IOfferExchangeComputer} */ ({
  adaptPickBtc:adaptPickBtc,
  adaptBtc:adaptBtc,
  adaptPaymentFailed:adaptPaymentFailed,
  adaptCanCreateTransaction:adaptCanCreateTransaction,
  adapt:[preadaptPickBtc,preadaptBtc,preadaptPaymentFailed,preadaptCanCreateTransaction],
 }),
/**@type {!xyz.swapee.wc.IOfferExchangeComputer}*/({
   // adaptSyncBroker({address,creatingTransaction}) {
   //  return{address,creatingTransaction}
   // },
  }),
){}