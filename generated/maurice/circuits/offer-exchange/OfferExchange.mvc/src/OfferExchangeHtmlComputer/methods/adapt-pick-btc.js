/** @type {xyz.swapee.wc.IOfferExchangeComputer._adaptPickBtc} */
export default function adaptPickBtc({invoice:invoice,btcInput:btcInput,btcOutput:btcOutput},{invoice:invoiceBeforeChange}) {
 if(invoiceBeforeChange===undefined) return {}
 if(btcInput) {
  return{
   refundAddress:invoice,
  }
 }
 if(btcOutput) {
  return{
   address:invoice,
  }
 }
}