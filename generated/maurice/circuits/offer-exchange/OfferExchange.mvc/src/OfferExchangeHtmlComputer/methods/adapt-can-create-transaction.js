/** @type {xyz.swapee.wc.IOfferExchangeComputer._adaptCanCreateTransaction} */
export default function adaptCanCreateTransaction({
 address:address,creatingTransaction:creatingTransaction,
 tocAccepted:tocAccepted,fixed:fixed,refundAddress:refundAddress,
}) {
 return{
  canCreateTransaction:!!((!creatingTransaction&&address&&tocAccepted)&&(fixed?refundAddress:true)),
 }
}