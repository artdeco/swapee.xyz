/** @type {xyz.swapee.wc.IOfferExchangeComputer._adaptPaymentFailed} */
export default function adaptPaymentFailed({status:status}) {
 return{
  paymentFailed:[
   'failed','refunded','overdue','expired',
  ].includes(status),
 }
}