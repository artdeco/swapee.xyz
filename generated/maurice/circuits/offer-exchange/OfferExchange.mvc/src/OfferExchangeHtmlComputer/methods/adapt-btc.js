/** @type {xyz.swapee.wc.IOfferExchangeComputer._adaptBtc} */
export default function adaptBtc({currencyFrom:currencyFrom,currencyTo:currencyTo}) {
 return {
  btcInput:`${currencyFrom}`.toLowerCase()=='btc',
  btcOutput:`${currencyTo}`.toLowerCase()=='btc',
 }
}