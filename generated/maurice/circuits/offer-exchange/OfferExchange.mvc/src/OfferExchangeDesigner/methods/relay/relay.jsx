
/**@type {xyz.swapee.wc.IOfferExchangeDesigner._relay} */
export default function relay({
 This:This,
 AddressPopup:AddressPopup,
 RefundPopup:RefundPopup,
 SwapeeWalletPicker:SwapeeWalletPicker,
 ExchangeBroker:ExchangeBroker,
 TransactionInfo:TransactionInfo,
},{ExchangeBroker:{refundAddress,address}}){
 return h(Fragment,{},
  h(This,{
   onSwapAddresses:[
    ExchangeBroker.setAddress(refundAddress),
    ExchangeBroker.setRefundAddress(address),
   ],
   onSelectAddress:[AddressPopup.hide(),ExchangeBroker.setAddress],
   onSelectRefund:RefundPopup.hide(),
   onReset:[
    ExchangeBroker.reset(),
    TransactionInfo.resetPort(),
   ]
  }),
  h(ExchangeBroker,{ onIdHigh:ExchangeBroker.pulseCheckPayment }),
  h(SwapeeWalletPicker,{ onInvoiceHigh:[
   RefundPopup.hide(),
  ] })
 )
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXItZXhjaGFuZ2Uvb2ZmZXItZXhjaGFuZ2Uud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQTBvQkcsU0FBUyxLQUFLO0NBQ2IsU0FBUztDQUNULHlCQUF5QjtDQUN6Qix1QkFBdUI7Q0FDdkIscUNBQXFDO0NBQ3JDLDZCQUE2QjtDQUM3QiwrQkFBK0I7QUFDaEMsR0FBRyxnQkFBZ0IsYUFBYSxDQUFDLE9BQU8sRUFBRTtDQUN6QyxPQUFPLFlBQUM7RUFDUDtHQUNDLGdCQUFpQjtJQUNoQixjQUFjLENBQUMsVUFBVSxDQUFDLGNBQWM7SUFDeEMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFFBQVE7R0FDekMsQ0FBRTtHQUNGLGdCQUFpQixhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsV0FBWTtHQUNqRSxlQUFnQixXQUFXLENBQUMsSUFBSSxDQUFDLENBQUU7R0FDbkMsUUFBUztJQUNSLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QixlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7R0FDNUI7QUFDSDtFQUNFLG1CQUFnQixTQUFVLGNBQWMsQ0FBQyxpQkFBa0I7RUFDM0QsdUJBQW9CLGNBQWU7R0FDbEMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0VBQ25CLENBQUU7Q0FDSDtBQUNELENBQUYifQ==