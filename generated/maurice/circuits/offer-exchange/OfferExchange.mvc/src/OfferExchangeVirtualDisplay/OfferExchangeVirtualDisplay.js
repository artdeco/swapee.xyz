import {Painter} from '@webcircuits/webcircuits'

const OfferExchangeVirtualDisplay=Painter.__trait(
 /**@type {!xyz.swapee.wc.IOfferExchangeVirtualDisplay}*/({
  paint:[
   function paintCopyPayinAddress({copyPayinAddress:copyPayinAddress},{ExchangeBroker:ExchangeBroker}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _lan={}
    if(ExchangeBroker) {
     const{'59af8':payinAddress}=ExchangeBroker.model
     _lan['9cfd8']={'59af8':payinAddress}
    }else _lan['9cfd8']={}
    if(!copyPayinAddress) return
    const _mem=serMemory({copyPayinAddress:copyPayinAddress})
    t_pa({
     pid:'f5321a4',
     mem:_mem,
     lan:_lan,
    })
   },
  ],
 }),
)
export default OfferExchangeVirtualDisplay