import { OfferExchangeDisplay, OfferExchangeTouchscreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.OfferExchangeDisplay} */
export { OfferExchangeDisplay }
/** @lazy @api {xyz.swapee.wc.OfferExchangeTouchscreen} */
export { OfferExchangeTouchscreen }