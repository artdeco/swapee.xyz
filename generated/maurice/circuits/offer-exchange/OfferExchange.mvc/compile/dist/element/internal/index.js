import Module from './element'

/**@extends {xyz.swapee.wc.AbstractOfferExchange}*/
export class AbstractOfferExchange extends Module['65293396511'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchange} */
AbstractOfferExchange.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferExchangePort} */
export const OfferExchangePort=Module['65293396513']
/**@extends {xyz.swapee.wc.AbstractOfferExchangeController}*/
export class AbstractOfferExchangeController extends Module['65293396514'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeController} */
AbstractOfferExchangeController.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferExchangeElement} */
export const OfferExchangeElement=Module['65293396518']
/** @type {typeof xyz.swapee.wc.OfferExchangeBuffer} */
export const OfferExchangeBuffer=Module['652933965111']
/**@extends {xyz.swapee.wc.AbstractOfferExchangeComputer}*/
export class AbstractOfferExchangeComputer extends Module['652933965130'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeComputer} */
AbstractOfferExchangeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferExchangeController} */
export const OfferExchangeController=Module['652933965161']