/**
 * An abstract class of `xyz.swapee.wc.IOfferExchange` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchange}
 */
class AbstractOfferExchange extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOfferExchange_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OfferExchangePort}
 */
class OfferExchangePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeController` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeController}
 */
class AbstractOfferExchangeController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IOfferExchange_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.OfferExchangeElement}
 */
class OfferExchangeElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OfferExchangeBuffer}
 */
class OfferExchangeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeComputer}
 */
class AbstractOfferExchangeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.OfferExchangeController}
 */
class OfferExchangeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOfferExchange = AbstractOfferExchange
module.exports.OfferExchangePort = OfferExchangePort
module.exports.AbstractOfferExchangeController = AbstractOfferExchangeController
module.exports.OfferExchangeElement = OfferExchangeElement
module.exports.OfferExchangeBuffer = OfferExchangeBuffer
module.exports.AbstractOfferExchangeComputer = AbstractOfferExchangeComputer
module.exports.OfferExchangeController = OfferExchangeController

Object.defineProperties(module.exports, {
 'AbstractOfferExchange': {get: () => require('./precompile/internal')[65293396511]},
 [65293396511]: {get: () => module.exports['AbstractOfferExchange']},
 'OfferExchangePort': {get: () => require('./precompile/internal')[65293396513]},
 [65293396513]: {get: () => module.exports['OfferExchangePort']},
 'AbstractOfferExchangeController': {get: () => require('./precompile/internal')[65293396514]},
 [65293396514]: {get: () => module.exports['AbstractOfferExchangeController']},
 'OfferExchangeElement': {get: () => require('./precompile/internal')[65293396518]},
 [65293396518]: {get: () => module.exports['OfferExchangeElement']},
 'OfferExchangeBuffer': {get: () => require('./precompile/internal')[652933965111]},
 [652933965111]: {get: () => module.exports['OfferExchangeBuffer']},
 'AbstractOfferExchangeComputer': {get: () => require('./precompile/internal')[652933965130]},
 [652933965130]: {get: () => module.exports['AbstractOfferExchangeComputer']},
 'OfferExchangeController': {get: () => require('./precompile/internal')[652933965161]},
 [652933965161]: {get: () => module.exports['OfferExchangeController']},
})