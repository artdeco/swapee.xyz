/**
 * Display for presenting information from the _IOfferExchange_.
 * @extends {xyz.swapee.wc.OfferExchangeDisplay}
 */
class OfferExchangeDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @extends {xyz.swapee.wc.OfferExchangeTouchscreen}
 */
class OfferExchangeTouchscreen extends (class {/* lazy-loaded */}) {}

module.exports.OfferExchangeDisplay = OfferExchangeDisplay
module.exports.OfferExchangeTouchscreen = OfferExchangeTouchscreen