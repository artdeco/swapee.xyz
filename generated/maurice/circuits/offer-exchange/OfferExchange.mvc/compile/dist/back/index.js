/**
 * An abstract class of `xyz.swapee.wc.IOfferExchange` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchange}
 */
class AbstractOfferExchange extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOfferExchange_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OfferExchangePort}
 */
class OfferExchangePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeController` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeController}
 */
class AbstractOfferExchangeController extends (class {/* lazy-loaded */}) {}
/**
 * The _IOfferExchange_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.OfferExchangeHtmlComponent}
 */
class OfferExchangeHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OfferExchangeBuffer}
 */
class OfferExchangeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeComputer}
 */
class AbstractOfferExchangeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.OfferExchangeComputer}
 */
class OfferExchangeComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.OfferExchangeController}
 */
class OfferExchangeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOfferExchange = AbstractOfferExchange
module.exports.OfferExchangePort = OfferExchangePort
module.exports.AbstractOfferExchangeController = AbstractOfferExchangeController
module.exports.OfferExchangeHtmlComponent = OfferExchangeHtmlComponent
module.exports.OfferExchangeBuffer = OfferExchangeBuffer
module.exports.AbstractOfferExchangeComputer = AbstractOfferExchangeComputer
module.exports.OfferExchangeComputer = OfferExchangeComputer
module.exports.OfferExchangeController = OfferExchangeController