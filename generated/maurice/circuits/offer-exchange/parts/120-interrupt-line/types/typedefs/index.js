/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IOfferExchangeInterruptLine={}
xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller={}
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel={}
xyz.swapee.wc.IOfferExchangeInterruptLineAspects={}
xyz.swapee.wc.IHyperOfferExchangeInterruptLine={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange/parts/120-interrupt-line/types/design/120-IOfferExchangeInterruptLine.xml} filter:!ControllerPlugin~props 0ad4edc72b61001ffb174863dfbecbb0 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferExchangeInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInterruptLine)} xyz.swapee.wc.AbstractOfferExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInterruptLine} xyz.swapee.wc.OfferExchangeInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInterruptLine
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInterruptLine.constructor&xyz.swapee.wc.OfferExchangeInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInterruptLine|typeof xyz.swapee.wc.OfferExchangeInterruptLine)|(!xyz.swapee.wc.front.IOfferExchangeController|typeof xyz.swapee.wc.front.OfferExchangeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInterruptLine|typeof xyz.swapee.wc.OfferExchangeInterruptLine)|(!xyz.swapee.wc.front.IOfferExchangeController|typeof xyz.swapee.wc.front.OfferExchangeController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInterruptLine|typeof xyz.swapee.wc.OfferExchangeInterruptLine)|(!xyz.swapee.wc.front.IOfferExchangeController|typeof xyz.swapee.wc.front.OfferExchangeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLine.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelHyperslice
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputFocus=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtInputFocus>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputFocus=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocus>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputFocusThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputFocusReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputFocusCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputBlur=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtInputBlur>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputBlur=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlur>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputBlurThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputBlurReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputBlurCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtAddressPopupClick=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtAddressPopupClick=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtAddressPopupClickThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtAddressPopupClickReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtAddressPopupClickCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePulseCopyPayinAddress=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforePulseCopyPayinAddress|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforePulseCopyPayinAddress>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPulseCopyPayinAddress=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddress|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddress>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPulseCopyPayinAddressThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPulseCopyPayinAddressReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPulseCopyPayinAddressCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePulseCopyPayinExtra=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforePulseCopyPayinExtra|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforePulseCopyPayinExtra>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPulseCopyPayinExtra=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtra|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtra>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPulseCopyPayinExtraThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPulseCopyPayinExtraReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPulseCopyPayinExtraCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraCancels>} */ (void 0)
  }
}
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelHyperslice

/**
 * A concrete class of _IOfferExchangeInterruptLineJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelHyperslice
 * @implements {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelHyperslice = class extends xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelHyperslice { }
xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputFocus=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputFocus=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocus<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputFocusThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputFocusReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputFocusCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputBlur=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputBlur=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlur<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputBlurThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputBlurReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputBlurCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtAddressPopupClick=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtAddressPopupClick=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtAddressPopupClickThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtAddressPopupClickReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtAddressPopupClickCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePulseCopyPayinAddress=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinAddress<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinAddress<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPulseCopyPayinAddress=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddress<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddress<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPulseCopyPayinAddressThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPulseCopyPayinAddressReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPulseCopyPayinAddressCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePulseCopyPayinExtra=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinExtra<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinExtra<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPulseCopyPayinExtra=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtra<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtra<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPulseCopyPayinExtraThrows=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPulseCopyPayinExtraReturns=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPulseCopyPayinExtraCancels=/** @type {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraCancels<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice

/**
 * A concrete class of _IOfferExchangeInterruptLineJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelBindingHyperslice = class extends xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice { }
xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IOfferExchangeInterruptLine`'s methods.
 * @interface xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel = class { }
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtInputFocus} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.beforeAtInputFocus = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocus} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocus = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusThrows = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusReturns = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusCancels = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtInputBlur} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.beforeAtInputBlur = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlur} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlur = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurThrows = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurReturns = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurCancels = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.beforeAtAddressPopupClick = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClick = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickThrows = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickReturns = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickCancels = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforePulseCopyPayinAddress} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.beforePulseCopyPayinAddress = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddress} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinAddress = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressThrows} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinAddressThrows = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressReturns} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinAddressReturns = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressCancels} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinAddressCancels = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforePulseCopyPayinExtra} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.beforePulseCopyPayinExtra = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtra} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinExtra = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraThrows} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinExtraThrows = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraReturns} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinExtraReturns = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraCancels} */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.prototype.afterPulseCopyPayinExtraCancels = function() {}

/**
 * A concrete class of _IOfferExchangeInterruptLineJoinpointModel_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModel
 * @implements {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel} An interface that enumerates the joinpoints of `IOfferExchangeInterruptLine`'s methods.
 */
xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModel = class extends xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel { }
xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModel.prototype.constructor = xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel} */
xyz.swapee.wc.RecordIOfferExchangeInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel} xyz.swapee.wc.BoundIOfferExchangeInterruptLineJoinpointModel */

/** @typedef {xyz.swapee.wc.OfferExchangeInterruptLineJoinpointModel} xyz.swapee.wc.BoundOfferExchangeInterruptLineJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller)} xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller} xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.Initialese[]) => xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller} xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller */
xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeAtInputFocus=/** @type {number} */ (void 0)
    this.afterAtInputFocus=/** @type {number} */ (void 0)
    this.afterAtInputFocusThrows=/** @type {number} */ (void 0)
    this.afterAtInputFocusReturns=/** @type {number} */ (void 0)
    this.afterAtInputFocusCancels=/** @type {number} */ (void 0)
    this.beforeAtInputBlur=/** @type {number} */ (void 0)
    this.afterAtInputBlur=/** @type {number} */ (void 0)
    this.afterAtInputBlurThrows=/** @type {number} */ (void 0)
    this.afterAtInputBlurReturns=/** @type {number} */ (void 0)
    this.afterAtInputBlurCancels=/** @type {number} */ (void 0)
    this.beforeAtAddressPopupClick=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClick=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClickThrows=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClickReturns=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClickCancels=/** @type {number} */ (void 0)
    this.beforePulseCopyPayinAddress=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinAddress=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinAddressThrows=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinAddressReturns=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinAddressCancels=/** @type {number} */ (void 0)
    this.beforePulseCopyPayinExtra=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinExtra=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinExtraThrows=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinExtraReturns=/** @type {number} */ (void 0)
    this.afterPulseCopyPayinExtraCancels=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {void}
   */
  atInputFocus() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  atInputBlur() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  atAddressPopupClick() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  pulseCopyPayinAddress() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  pulseCopyPayinExtra() { }
}
/**
 * Create a new *IOfferExchangeInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.Initialese>)} xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller} xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferExchangeInterruptLineAspectsInstaller_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller
 * @implements {xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInterruptLineAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.OfferExchangeInterruptLineAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atInputFocus` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.OfferExchangeInputs) => void} sub Cancels a call to `atInputFocus` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData
 * @prop {!xyz.swapee.wc.front.OfferExchangeInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atInputFocus` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData
 * @prop {!xyz.swapee.wc.front.OfferExchangeInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.OfferExchangeInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atInputBlur` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.OfferExchangeInputs) => void} sub Cancels a call to `atInputBlur` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData
 * @prop {!xyz.swapee.wc.front.OfferExchangeInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atInputBlur` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData
 * @prop {!xyz.swapee.wc.front.OfferExchangeInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.OfferExchangeInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atAddressPopupClick` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.OfferExchangeInputs) => void} sub Cancels a call to `atAddressPopupClick` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData
 * @prop {!xyz.swapee.wc.front.OfferExchangeInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atAddressPopupClick` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData
 * @prop {!xyz.swapee.wc.front.OfferExchangeInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.OfferExchangeInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinAddressPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `pulseCopyPayinAddress` method from being executed.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinAddressPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinAddressPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterPulseCopyPayinAddressPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinAddressPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `pulseCopyPayinAddress` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinAddressPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinAddressPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsPulseCopyPayinAddressPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinAddressPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinAddressPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinAddressPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinExtraPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `pulseCopyPayinExtra` method from being executed.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinExtraPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinExtraPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterPulseCopyPayinExtraPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinExtraPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `pulseCopyPayinExtra` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinExtraPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinExtraPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsPulseCopyPayinExtraPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinExtraPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinExtraPointcutData&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinExtraPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {function(new: xyz.swapee.wc.BOfferExchangeInterruptLineAspectsCaster<THIS>&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.wc.BOfferExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IOfferExchangeInterruptLine* that bind to an instance.
 * @interface xyz.swapee.wc.BOfferExchangeInterruptLineAspects
 * @template THIS
 */
xyz.swapee.wc.BOfferExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.BOfferExchangeInterruptLineAspects.constructor&xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.wc.BOfferExchangeInterruptLineAspects.prototype.constructor = xyz.swapee.wc.BOfferExchangeInterruptLineAspects

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferExchangeInterruptLineAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInterruptLineAspects)} xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects} xyz.swapee.wc.OfferExchangeInterruptLineAspects.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInterruptLineAspects` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.constructor&xyz.swapee.wc.OfferExchangeInterruptLineAspects.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInterruptLineAspects|typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects)|(!xyz.swapee.wc.BOfferExchangeInterruptLineAspects|typeof xyz.swapee.wc.BOfferExchangeInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInterruptLineAspects|typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects)|(!xyz.swapee.wc.BOfferExchangeInterruptLineAspects|typeof xyz.swapee.wc.BOfferExchangeInterruptLineAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInterruptLineAspects|typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects)|(!xyz.swapee.wc.BOfferExchangeInterruptLineAspects|typeof xyz.swapee.wc.BOfferExchangeInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInterruptLineAspects.Initialese[]) => xyz.swapee.wc.IOfferExchangeInterruptLineAspects} xyz.swapee.wc.OfferExchangeInterruptLineAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInterruptLineAspectsCaster&xyz.swapee.wc.BOfferExchangeInterruptLineAspects<!xyz.swapee.wc.IOfferExchangeInterruptLineAspects>)} xyz.swapee.wc.IOfferExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.BOfferExchangeInterruptLineAspects} xyz.swapee.wc.BOfferExchangeInterruptLineAspects.typeof */
/**
 * The aspects of the *IOfferExchangeInterruptLine*.
 * @interface xyz.swapee.wc.IOfferExchangeInterruptLineAspects
 */
xyz.swapee.wc.IOfferExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.IOfferExchangeInterruptLineAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.BOfferExchangeInterruptLineAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInterruptLineAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInterruptLineAspects&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInterruptLineAspects.Initialese>)} xyz.swapee.wc.OfferExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineAspects} xyz.swapee.wc.IOfferExchangeInterruptLineAspects.typeof */
/**
 * A concrete class of _IOfferExchangeInterruptLineAspects_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInterruptLineAspects
 * @implements {xyz.swapee.wc.IOfferExchangeInterruptLineAspects} The aspects of the *IOfferExchangeInterruptLine*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInterruptLineAspects.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.OfferExchangeInterruptLineAspects.constructor&xyz.swapee.wc.IOfferExchangeInterruptLineAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInterruptLineAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInterruptLineAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLineAspects}
 */
xyz.swapee.wc.OfferExchangeInterruptLineAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BOfferExchangeInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.BOfferExchangeInterruptLineAspectsCaster
 * @template THIS
 */
xyz.swapee.wc.BOfferExchangeInterruptLineAspectsCaster = class { }
/**
 * Cast the _BOfferExchangeInterruptLineAspects_ instance into the _BoundIOfferExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInterruptLine}
 */
xyz.swapee.wc.BOfferExchangeInterruptLineAspectsCaster.prototype.asIOfferExchangeInterruptLine

/**
 * Contains getters to cast the _IOfferExchangeInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInterruptLineAspectsCaster
 */
xyz.swapee.wc.IOfferExchangeInterruptLineAspectsCaster = class { }
/**
 * Cast the _IOfferExchangeInterruptLineAspects_ instance into the _BoundIOfferExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInterruptLine}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineAspectsCaster.prototype.asIOfferExchangeInterruptLine

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine.Initialese} xyz.swapee.wc.IHyperOfferExchangeInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.HyperOfferExchangeInterruptLine)} xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine} xyz.swapee.wc.HyperOfferExchangeInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IHyperOfferExchangeInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.constructor&xyz.swapee.wc.HyperOfferExchangeInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IHyperOfferExchangeInterruptLine|typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine)|(!xyz.swapee.wc.IOfferExchangeInterruptLine|typeof xyz.swapee.wc.OfferExchangeInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IHyperOfferExchangeInterruptLine|typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine)|(!xyz.swapee.wc.IOfferExchangeInterruptLine|typeof xyz.swapee.wc.OfferExchangeInterruptLine)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IHyperOfferExchangeInterruptLine|typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine)|(!xyz.swapee.wc.IOfferExchangeInterruptLine|typeof xyz.swapee.wc.OfferExchangeInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.wc.IOfferExchangeInterruptLineAspects|function(new: xyz.swapee.wc.IOfferExchangeInterruptLineAspects)} aides The list of aides that advise the IOfferExchangeInterruptLine to implement aspects.
 * @return {typeof xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.wc.IHyperOfferExchangeInterruptLine.Initialese[]) => xyz.swapee.wc.IHyperOfferExchangeInterruptLine} xyz.swapee.wc.HyperOfferExchangeInterruptLineConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IHyperOfferExchangeInterruptLineCaster&xyz.swapee.wc.IOfferExchangeInterruptLine)} xyz.swapee.wc.IHyperOfferExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLine} xyz.swapee.wc.IOfferExchangeInterruptLine.typeof */
/** @interface xyz.swapee.wc.IHyperOfferExchangeInterruptLine */
xyz.swapee.wc.IHyperOfferExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.IHyperOfferExchangeInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferExchangeInterruptLine.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperOfferExchangeInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperOfferExchangeInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IHyperOfferExchangeInterruptLine.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IHyperOfferExchangeInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IHyperOfferExchangeInterruptLine.Initialese>)} xyz.swapee.wc.HyperOfferExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IHyperOfferExchangeInterruptLine} xyz.swapee.wc.IHyperOfferExchangeInterruptLine.typeof */
/**
 * A concrete class of _IHyperOfferExchangeInterruptLine_ instances.
 * @constructor xyz.swapee.wc.HyperOfferExchangeInterruptLine
 * @implements {xyz.swapee.wc.IHyperOfferExchangeInterruptLine} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperOfferExchangeInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.HyperOfferExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.HyperOfferExchangeInterruptLine.constructor&xyz.swapee.wc.IHyperOfferExchangeInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperOfferExchangeInterruptLine* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IHyperOfferExchangeInterruptLine.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperOfferExchangeInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperOfferExchangeInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.HyperOfferExchangeInterruptLine.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperOfferExchangeInterruptLine}
 */
xyz.swapee.wc.HyperOfferExchangeInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IHyperOfferExchangeInterruptLine} */
xyz.swapee.wc.RecordIHyperOfferExchangeInterruptLine

/** @typedef {xyz.swapee.wc.IHyperOfferExchangeInterruptLine} xyz.swapee.wc.BoundIHyperOfferExchangeInterruptLine */

/** @typedef {xyz.swapee.wc.HyperOfferExchangeInterruptLine} xyz.swapee.wc.BoundHyperOfferExchangeInterruptLine */

/**
 * Contains getters to cast the _IHyperOfferExchangeInterruptLine_ interface.
 * @interface xyz.swapee.wc.IHyperOfferExchangeInterruptLineCaster
 */
xyz.swapee.wc.IHyperOfferExchangeInterruptLineCaster = class { }
/**
 * Cast the _IHyperOfferExchangeInterruptLine_ instance into the _BoundIHyperOfferExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIHyperOfferExchangeInterruptLine}
 */
xyz.swapee.wc.IHyperOfferExchangeInterruptLineCaster.prototype.asIHyperOfferExchangeInterruptLine
/**
 * Access the _HyperOfferExchangeInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundHyperOfferExchangeInterruptLine}
 */
xyz.swapee.wc.IHyperOfferExchangeInterruptLineCaster.prototype.superHyperOfferExchangeInterruptLine

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInterruptLineCaster&xyz.swapee.wc.front.IOfferExchangeController)} xyz.swapee.wc.IOfferExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOfferExchangeController} xyz.swapee.wc.front.IOfferExchangeController.typeof */
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @interface xyz.swapee.wc.IOfferExchangeInterruptLine
 */
xyz.swapee.wc.IOfferExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.IOfferExchangeInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IOfferExchangeController.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLine.atInputFocus} */
xyz.swapee.wc.IOfferExchangeInterruptLine.prototype.atInputFocus = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLine.atInputBlur} */
xyz.swapee.wc.IOfferExchangeInterruptLine.prototype.atInputBlur = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLine.atAddressPopupClick} */
xyz.swapee.wc.IOfferExchangeInterruptLine.prototype.atAddressPopupClick = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLine.pulseCopyPayinAddress} */
xyz.swapee.wc.IOfferExchangeInterruptLine.prototype.pulseCopyPayinAddress = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInterruptLine.pulseCopyPayinExtra} */
xyz.swapee.wc.IOfferExchangeInterruptLine.prototype.pulseCopyPayinExtra = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInterruptLine.Initialese>)} xyz.swapee.wc.OfferExchangeInterruptLine.constructor */
/**
 * A concrete class of _IOfferExchangeInterruptLine_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInterruptLine
 * @implements {xyz.swapee.wc.IOfferExchangeInterruptLine} Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.OfferExchangeInterruptLine.constructor&xyz.swapee.wc.IOfferExchangeInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OfferExchangeInterruptLine.prototype.constructor = xyz.swapee.wc.OfferExchangeInterruptLine
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInterruptLine}
 */
xyz.swapee.wc.OfferExchangeInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine} */
xyz.swapee.wc.RecordIOfferExchangeInterruptLine

/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine} xyz.swapee.wc.BoundIOfferExchangeInterruptLine */

/** @typedef {xyz.swapee.wc.OfferExchangeInterruptLine} xyz.swapee.wc.BoundOfferExchangeInterruptLine */

/**
 * Contains getters to cast the _IOfferExchangeInterruptLine_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInterruptLineCaster
 */
xyz.swapee.wc.IOfferExchangeInterruptLineCaster = class { }
/**
 * Cast the _IOfferExchangeInterruptLine_ instance into the _BoundIOfferExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInterruptLine}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineCaster.prototype.asIOfferExchangeInterruptLine
/**
 * Cast the _IOfferExchangeInterruptLine_ instance into the _.BoundIOfferExchangeTouchscreen_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeTouchscreen}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineCaster.prototype.asIOfferExchangeTouchscreen
/**
 * Access the _OfferExchangeInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInterruptLine}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineCaster.prototype.superOfferExchangeInterruptLine

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtInputFocus */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtInputFocus} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atInputFocus` method from being executed.
 * - `sub` _(value: !front.OfferExchangeInputs) =&gt; void_ Cancels a call to `atInputFocus` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtInputFocus = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocus<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocus */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocus} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `after` joinpoint.
 * - `res` _!front.OfferExchangeInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocus = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atInputFocus` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `afterReturns` joinpoint.
 * - `res` _!front.OfferExchangeInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.OfferExchangeInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtInputBlur */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtInputBlur} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atInputBlur` method from being executed.
 * - `sub` _(value: !front.OfferExchangeInputs) =&gt; void_ Cancels a call to `atInputBlur` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtInputBlur = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlur<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlur */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlur} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `after` joinpoint.
 * - `res` _!front.OfferExchangeInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlur = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atInputBlur` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `afterReturns` joinpoint.
 * - `res` _!front.OfferExchangeInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.OfferExchangeInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atAddressPopupClick` method from being executed.
 * - `sub` _(value: !front.OfferExchangeInputs) =&gt; void_ Cancels a call to `atAddressPopupClick` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `after` joinpoint.
 * - `res` _!front.OfferExchangeInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atAddressPopupClick` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `afterReturns` joinpoint.
 * - `res` _!front.OfferExchangeInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.OfferExchangeInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinAddressPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinAddress
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinAddress<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforePulseCopyPayinAddress */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforePulseCopyPayinAddress} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinAddressPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinAddress_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `pulseCopyPayinAddress` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforePulseCopyPayinAddress = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterPulseCopyPayinAddressPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddress
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddress<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddress */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddress} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterPulseCopyPayinAddressPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinAddress_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddress = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinAddressPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressThrows<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressThrows */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinAddressPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinAddress_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `pulseCopyPayinAddress` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsPulseCopyPayinAddressPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressReturns<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressReturns */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsPulseCopyPayinAddressPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinAddress_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinAddressPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinAddressCancels<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinAddressCancels */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinAddressPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinAddress_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinAddressPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinAddressCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinExtraPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinExtra
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__beforePulseCopyPayinExtra<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._beforePulseCopyPayinExtra */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforePulseCopyPayinExtra} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.BeforePulseCopyPayinExtraPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinExtra_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `pulseCopyPayinExtra` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.beforePulseCopyPayinExtra = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterPulseCopyPayinExtraPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtra
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtra<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtra */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtra} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterPulseCopyPayinExtraPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinExtra_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtra = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinExtraPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraThrows<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraThrows */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterThrowsPulseCopyPayinExtraPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinExtra_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `pulseCopyPayinExtra` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsPulseCopyPayinExtraPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraReturns<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraReturns */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterReturnsPulseCopyPayinExtraPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinExtra_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinExtraPointcutData) => void} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.__afterPulseCopyPayinExtraCancels<!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel._afterPulseCopyPayinExtraCancels */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.AfterCancelsPulseCopyPayinExtraPointcutData} [data] Metadata passed to the pointcuts of _pulseCopyPayinExtra_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOfferExchangeInterruptLineJoinpointModel.PulseCopyPayinExtraPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel.afterPulseCopyPayinExtraCancels = function(data) {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.OfferExchangeInputs)} xyz.swapee.wc.IOfferExchangeInterruptLine.__atInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine.__atInputFocus<!xyz.swapee.wc.IOfferExchangeInterruptLine>} xyz.swapee.wc.IOfferExchangeInterruptLine._atInputFocus */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLine.atInputFocus} */
/** @return {void|!xyz.swapee.wc.front.OfferExchangeInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IOfferExchangeInterruptLine.atInputFocus = function() {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.OfferExchangeInputs)} xyz.swapee.wc.IOfferExchangeInterruptLine.__atInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine.__atInputBlur<!xyz.swapee.wc.IOfferExchangeInterruptLine>} xyz.swapee.wc.IOfferExchangeInterruptLine._atInputBlur */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLine.atInputBlur} */
/** @return {void|!xyz.swapee.wc.front.OfferExchangeInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IOfferExchangeInterruptLine.atInputBlur = function() {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.OfferExchangeInputs)} xyz.swapee.wc.IOfferExchangeInterruptLine.__atAddressPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine.__atAddressPopupClick<!xyz.swapee.wc.IOfferExchangeInterruptLine>} xyz.swapee.wc.IOfferExchangeInterruptLine._atAddressPopupClick */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLine.atAddressPopupClick} */
/** @return {void|!xyz.swapee.wc.front.OfferExchangeInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IOfferExchangeInterruptLine.atAddressPopupClick = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferExchangeInterruptLine.__pulseCopyPayinAddress
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine.__pulseCopyPayinAddress<!xyz.swapee.wc.IOfferExchangeInterruptLine>} xyz.swapee.wc.IOfferExchangeInterruptLine._pulseCopyPayinAddress */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLine.pulseCopyPayinAddress} */
/** @return {void} */
xyz.swapee.wc.IOfferExchangeInterruptLine.pulseCopyPayinAddress = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferExchangeInterruptLine.__pulseCopyPayinExtra
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInterruptLine.__pulseCopyPayinExtra<!xyz.swapee.wc.IOfferExchangeInterruptLine>} xyz.swapee.wc.IOfferExchangeInterruptLine._pulseCopyPayinExtra */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInterruptLine.pulseCopyPayinExtra} */
/** @return {void} */
xyz.swapee.wc.IOfferExchangeInterruptLine.pulseCopyPayinExtra = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInterruptLineJoinpointModel,xyz.swapee.wc.IOfferExchangeInterruptLine
/* @typal-end */