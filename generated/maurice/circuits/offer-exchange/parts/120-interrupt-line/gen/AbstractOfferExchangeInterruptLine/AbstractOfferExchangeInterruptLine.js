import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInterruptLine}
 */
function __AbstractOfferExchangeInterruptLine() {}
__AbstractOfferExchangeInterruptLine.prototype = /** @type {!_AbstractOfferExchangeInterruptLine} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInterruptLine}
 */
class _AbstractOfferExchangeInterruptLine { }
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInterruptLine} ‎
 */
class AbstractOfferExchangeInterruptLine extends newAbstract(
 _AbstractOfferExchangeInterruptLine,8,null,{
  asIOfferExchangeInterruptLine:1,
  superOfferExchangeInterruptLine:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLine} */
AbstractOfferExchangeInterruptLine.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLine} */
function AbstractOfferExchangeInterruptLineClass(){}

export default AbstractOfferExchangeInterruptLine


AbstractOfferExchangeInterruptLine[$implementations]=[
 __AbstractOfferExchangeInterruptLine,
]

export {AbstractOfferExchangeInterruptLine}