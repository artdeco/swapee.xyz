import OfferExchangeInterruptLineAspectsInstaller from '../../aspects-installers/OfferExchangeInterruptLineAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperOfferExchangeInterruptLine}
 */
function __AbstractHyperOfferExchangeInterruptLine() {}
__AbstractHyperOfferExchangeInterruptLine.prototype = /** @type {!_AbstractHyperOfferExchangeInterruptLine} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine}
 */
class _AbstractHyperOfferExchangeInterruptLine {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperOfferExchangeInterruptLine
    .clone({aspectsInstaller:OfferExchangeInterruptLineAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine} ‎ */
class AbstractHyperOfferExchangeInterruptLine extends newAbstract(
 _AbstractHyperOfferExchangeInterruptLine,7,null,{
  asIHyperOfferExchangeInterruptLine:1,
  superHyperOfferExchangeInterruptLine:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine} */
AbstractHyperOfferExchangeInterruptLine.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractHyperOfferExchangeInterruptLine} */
function AbstractHyperOfferExchangeInterruptLineClass(){}

export default AbstractHyperOfferExchangeInterruptLine


AbstractHyperOfferExchangeInterruptLine[$implementations]=[
 __AbstractHyperOfferExchangeInterruptLine,
]