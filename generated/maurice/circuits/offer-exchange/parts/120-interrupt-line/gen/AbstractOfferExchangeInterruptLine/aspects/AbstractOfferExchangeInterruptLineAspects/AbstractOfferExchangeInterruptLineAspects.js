import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInterruptLineAspects}
 */
function __AbstractOfferExchangeInterruptLineAspects() {}
__AbstractOfferExchangeInterruptLineAspects.prototype = /** @type {!_AbstractOfferExchangeInterruptLineAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects}
 */
class _AbstractOfferExchangeInterruptLineAspects { }
/**
 * The aspects of the *IOfferExchangeInterruptLine*.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects} ‎
 */
class AbstractOfferExchangeInterruptLineAspects extends newAspects(
 _AbstractOfferExchangeInterruptLineAspects,6,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects} */
AbstractOfferExchangeInterruptLineAspects.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspects} */
function AbstractOfferExchangeInterruptLineAspectsClass(){}

export default AbstractOfferExchangeInterruptLineAspects


AbstractOfferExchangeInterruptLineAspects[$implementations]=[
 __AbstractOfferExchangeInterruptLineAspects,
]