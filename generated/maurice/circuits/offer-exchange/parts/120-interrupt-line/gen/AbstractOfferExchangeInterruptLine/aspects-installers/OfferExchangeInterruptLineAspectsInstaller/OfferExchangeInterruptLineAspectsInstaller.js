import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangeInterruptLineAspectsInstaller}
 */
function __OfferExchangeInterruptLineAspectsInstaller() {}
__OfferExchangeInterruptLineAspectsInstaller.prototype = /** @type {!_OfferExchangeInterruptLineAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller}
 */
class _OfferExchangeInterruptLineAspectsInstaller { }

_OfferExchangeInterruptLineAspectsInstaller.prototype[$advice]=__OfferExchangeInterruptLineAspectsInstaller

/** @extends {xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller} ‎ */
class OfferExchangeInterruptLineAspectsInstaller extends newAbstract(
 _OfferExchangeInterruptLineAspectsInstaller,4,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller} */
OfferExchangeInterruptLineAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInterruptLineAspectsInstaller} */
function OfferExchangeInterruptLineAspectsInstallerClass(){}

export default OfferExchangeInterruptLineAspectsInstaller


OfferExchangeInterruptLineAspectsInstaller[$implementations]=[
 OfferExchangeInterruptLineAspectsInstallerClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInterruptLineAspectsInstaller}*/({
  atInputFocus(){
   this.beforeAtInputFocus=1
   this.afterAtInputFocus=2
   this.aroundAtInputFocus=3
   this.afterAtInputFocusThrows=4
   this.afterAtInputFocusReturns=5
   this.afterAtInputFocusCancels=7
  },
  atInputBlur(){
   this.beforeAtInputBlur=1
   this.afterAtInputBlur=2
   this.aroundAtInputBlur=3
   this.afterAtInputBlurThrows=4
   this.afterAtInputBlurReturns=5
   this.afterAtInputBlurCancels=7
  },
  atAddressPopupClick(){
   this.beforeAtAddressPopupClick=1
   this.afterAtAddressPopupClick=2
   this.aroundAtAddressPopupClick=3
   this.afterAtAddressPopupClickThrows=4
   this.afterAtAddressPopupClickReturns=5
   this.afterAtAddressPopupClickCancels=7
  },
  pulseCopyPayinAddress(){
   this.beforePulseCopyPayinAddress=1
   this.afterPulseCopyPayinAddress=2
   this.aroundPulseCopyPayinAddress=3
   this.afterPulseCopyPayinAddressThrows=4
   this.afterPulseCopyPayinAddressReturns=5
   this.afterPulseCopyPayinAddressCancels=7
  },
  pulseCopyPayinExtra(){
   this.beforePulseCopyPayinExtra=1
   this.afterPulseCopyPayinExtra=2
   this.aroundPulseCopyPayinExtra=3
   this.afterPulseCopyPayinExtraThrows=4
   this.afterPulseCopyPayinExtraReturns=5
   this.afterPulseCopyPayinExtraCancels=7
  },
 }),
 __OfferExchangeInterruptLineAspectsInstaller,
]