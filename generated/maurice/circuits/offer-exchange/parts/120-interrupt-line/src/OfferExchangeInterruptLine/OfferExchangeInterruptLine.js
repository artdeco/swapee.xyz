import AbstractOfferExchangeInterruptLine from '../../gen/AbstractOfferExchangeInterruptLine'
import atAddressPopupClick from './methods/atAddressPopupClick/at-address-popup-click'
import atInputBlur from './methods/atInputBlur/at-input-blur'
import atInputFocus from './methods/atInputFocus/at-input-focus'

/**@extends {xyz.swapee.wc.OfferExchangeInterruptLine} */
export default class extends AbstractOfferExchangeInterruptLine.implements(
 AbstractOfferExchangeInterruptLine.class.prototype=/**@type {!xyz.swapee.wc.OfferExchangeInterruptLine}*/({
  atAddressPopupClick:atAddressPopupClick,
  atInputBlur:atInputBlur,
  atInputFocus:atInputFocus,
 }),
){}