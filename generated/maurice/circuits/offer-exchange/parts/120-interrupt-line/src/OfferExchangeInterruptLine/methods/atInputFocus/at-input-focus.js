import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOfferExchangeInterruptLine._atInputFocus} */
export default function atInputFocus(ev){
 const{
  asIOfferExchangeTouchscreen:{
   classes:{InputActive:InputActive},
  },
 }=this
 // can there be component to do this declaratively.
 const{target}=ev
 target.parentElement.classList.add(InputActive)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXItZXhjaGFuZ2Uvb2ZmZXItZXhjaGFuZ2Uud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQStURyxTQUFTLFlBQVksQ0FBQztDQUNyQjtFQUNDO0dBQ0MsU0FBUyx1QkFBdUI7QUFDbkM7R0FDRztDQUNGLEdBQUcsSUFBSSxNQUFNLEdBQUcsVUFBVSxHQUFHLEdBQUcsS0FBSyxhQUFhO0NBQ2xELE1BQU0sUUFBUTtDQUNkLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztBQUNwQyxDQUFGIn0=