import AbstractOfferExchangeInterruptLineAspects from '../../../../gen/AbstractOfferExchangeInterruptLine/aspects/AbstractOfferExchangeInterruptLineAspects'
import {SetInputs} from '@webcircuits/front'

/**@extends {xyz.swapee.wc.OfferExchangeInterruptLineAspects} */
export default class extends AbstractOfferExchangeInterruptLineAspects.continues(
 /**@type {!xyz.swapee.wc.OfferExchangeInterruptLineAspects}*/({
  afterAtInputFocusReturns:SetInputs,
  afterAtInputBlurReturns:SetInputs,
  afterAtAddressPopupClickReturns:SetInputs,
 }),
){}