import AbstractHyperOfferExchangeInterruptLine from '../../../../gen/AbstractOfferExchangeInterruptLine/hyper/AbstractHyperOfferExchangeInterruptLine'
import OfferExchangeInterruptLine from '../../OfferExchangeInterruptLine'
import OfferExchangeInterruptLineGeneralAspects from '../OfferExchangeInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperOfferExchangeInterruptLine} */
export default class extends AbstractHyperOfferExchangeInterruptLine
 .consults(
  OfferExchangeInterruptLineGeneralAspects,
 )
 .implements(
  OfferExchangeInterruptLine,
 )
{}