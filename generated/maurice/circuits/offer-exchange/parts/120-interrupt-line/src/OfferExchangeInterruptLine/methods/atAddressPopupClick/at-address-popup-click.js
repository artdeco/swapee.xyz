import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOfferExchangeInterruptLine._atAddressPopupClick} */
export default function atAddressPopupClick(ev) {
 const{target:{dataset:{'address':address}}}=ev
 if(!address) return
 const{asIOfferExchangeTouchscreen:{selectAddress:selectAddress}}=this
 selectAddress(address)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXItZXhjaGFuZ2Uvb2ZmZXItZXhjaGFuZ2Uud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQStTRyxTQUFTLG1CQUFtQixDQUFDO0NBQzVCLE1BQU0sUUFBUSxTQUFTLGlCQUFpQixJQUFJO0NBQzVDLEVBQUUsQ0FBQyxVQUFVO0NBQ2IsTUFBTSw2QkFBNkIsMkJBQTJCLEdBQUc7Q0FDakUsYUFBYSxDQUFDO0FBQ2YsQ0FBRiJ9