import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IOfferExchangeDisplay._paintCopyPayinAddress} */
export default function paintCopyPayinAddress(_,{ExchangeBroker:{payinAddress:payinAddress}}) { // todo: somehow make this reusable via "copy" attribute?
 const{asIOfferExchangeTouchscreen:{
  classes:{Copied:Copied,CopiedRemoving:CopiedRemoving},
  asIScreen:{copyToClipboard:copyToClipboard},
  // pulsePayinAddressCopied:pulsePayinAddressCopied,
  setInputs:setInputs,
 },asIOfferExchangeDisplay:{PayinAddressCopiedLa},
 }=this
 copyToClipboard(payinAddress)
 // pulsePayinAddressCopied()
 if(PayinAddressCopiedLa.classList.contains(Copied)||PayinAddressCopiedLa.classList.contains(CopiedRemoving)) {
  return
 }
 // setTimeout(()=>{
 //  setInputs({payinAddressCopied:true,payinAddressJustCopied:false})
 // },1)
 const play=()=>{
  setInputs({payinAddressCopied:true,payinAddressJustCopied:false})
  setTimeout(()=>{
   setInputs({payinAddressJustCopied:true})
   setTimeout(()=>{
    setInputs({payinAddressCopied:false,payinAddressJustCopied:false})
   },310)
  },1100)
 }

 play()
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXItZXhjaGFuZ2Uvb2ZmZXItZXhjaGFuZ2Uud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQWlSRyxTQUFTLHFCQUFxQixDQUFDLENBQUMsRUFBRSxnQkFBZ0IseUJBQXlCLEVBQUUsSUFBSSxHQUFHLE1BQU0sUUFBUSxLQUFLLEtBQUssU0FBUyxJQUFJLE9BQU87Q0FDL0gsTUFBTTtFQUNMLFNBQVMsYUFBYSxDQUFDLDZCQUE2QjtFQUNwRCxXQUFXLCtCQUErQjtFQUMxQyxHQUFHLCtDQUErQztFQUNsRCxtQkFBbUI7QUFDckIsR0FBRyx5QkFBeUIsb0JBQW9CO0dBQzdDO0NBQ0YsZUFBZSxDQUFDO0NBQ2hCLEdBQUcsdUJBQXVCLENBQUM7Q0FDM0IsRUFBRSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsNkJBQTZCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztFQUMzRjtBQUNGO0NBQ0MsR0FBRyxVQUFVLENBQUMsQ0FBQztDQUNmLElBQUksU0FBUyxFQUFFLHVCQUF1QixDQUFDLDRCQUE0QixDQUFDO0NBQ3BFLEVBQUUsR0FBRztDQUNMLE1BQU0sSUFBSSxDQUFDLENBQUM7RUFDWCxTQUFTLEVBQUUsdUJBQXVCLENBQUMsNEJBQTRCLENBQUM7RUFDaEUsVUFBVSxDQUFDLENBQUM7R0FDWCxTQUFTLEVBQUUsMkJBQTJCLENBQUM7R0FDdkMsVUFBVSxDQUFDLENBQUM7SUFDWCxTQUFTLEVBQUUsd0JBQXdCLENBQUMsNEJBQTRCLENBQUM7QUFDckUsS0FBSztBQUNMLElBQUk7QUFDSjs7Q0FFQyxJQUFJLENBQUM7QUFDTixDQUFGIn0=