/**@type {xyz.swapee.wc.IOfferExchangeDisplay._paintCopyPaymentAddress} */
export default function prepaintCopyPaymentAddress(memory,land) {
 const{asIOfferExchangeDisplay:{paintCopyPaymentAddress:paintCopyPaymentAddress}}=this

 paintCopyPaymentAddress(memory,null)
}
prepaintCopyPaymentAddress['_id']='0a75838'