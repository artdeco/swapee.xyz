/**@type {xyz.swapee.wc.IOfferExchangeDisplay._paintCopyPayinAddress} */
export default function prepaintCopyPayinAddress(memory,land) {
 const{asIOfferExchangeDisplay:{paintCopyPayinAddress:paintCopyPayinAddress}}=this
 const _lan={}
 const ExchangeBroker=land['9cfd8']
 if(ExchangeBroker) {
  const{
   '59af8':payinAddress,
  }=ExchangeBroker
  _lan.ExchangeBroker={
   payinAddress:payinAddress,
  }
 }else _lan.ExchangeBroker={}

 paintCopyPayinAddress(memory,_lan)
}
prepaintCopyPayinAddress['_id']='f5321a4'