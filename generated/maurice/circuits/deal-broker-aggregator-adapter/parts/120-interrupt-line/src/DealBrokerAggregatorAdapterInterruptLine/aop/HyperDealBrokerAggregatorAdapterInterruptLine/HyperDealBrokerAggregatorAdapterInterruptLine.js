import AbstractHyperDealBrokerAggregatorAdapterInterruptLine from '../../../../gen/AbstractDealBrokerAggregatorAdapterInterruptLine/hyper/AbstractHyperDealBrokerAggregatorAdapterInterruptLine'
import DealBrokerAggregatorAdapterInterruptLine from '../../DealBrokerAggregatorAdapterInterruptLine'
import DealBrokerAggregatorAdapterInterruptLineGeneralAspects from '../DealBrokerAggregatorAdapterInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperDealBrokerAggregatorAdapterInterruptLine} */
export default class extends AbstractHyperDealBrokerAggregatorAdapterInterruptLine
 .consults(
  DealBrokerAggregatorAdapterInterruptLineGeneralAspects,
 )
 .implements(
  DealBrokerAggregatorAdapterInterruptLine,
 )
{}