import {DealBrokerAggregatorAdapterVdusPQs} from './DealBrokerAggregatorAdapterVdusPQs'
export const DealBrokerAggregatorAdapterVdusQPs=/**@type {!xyz.swapee.wc.DealBrokerAggregatorAdapterVdusQPs}*/(Object.keys(DealBrokerAggregatorAdapterVdusPQs)
 .reduce((a,k)=>{a[DealBrokerAggregatorAdapterVdusPQs[k]]=k;return a},{}))