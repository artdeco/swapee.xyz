import {DealBrokerAggregatorAdapterMemoryPQs} from './DealBrokerAggregatorAdapterMemoryPQs'
export const DealBrokerAggregatorAdapterInputsPQs=/**@type {!xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs}*/({
 ...DealBrokerAggregatorAdapterMemoryPQs,
})