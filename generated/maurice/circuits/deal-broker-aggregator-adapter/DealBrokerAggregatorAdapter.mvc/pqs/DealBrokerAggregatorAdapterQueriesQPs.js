import {DealBrokerAggregatorAdapterQueriesPQs} from './DealBrokerAggregatorAdapterQueriesPQs'
export const DealBrokerAggregatorAdapterQueriesQPs=/**@type {!xyz.swapee.wc.DealBrokerAggregatorAdapterQueriesQPs}*/(Object.keys(DealBrokerAggregatorAdapterQueriesPQs)
 .reduce((a,k)=>{a[DealBrokerAggregatorAdapterQueriesPQs[k]]=k;return a},{}))