import {DealBrokerAggregatorAdapterMemoryPQs} from './DealBrokerAggregatorAdapterMemoryPQs'
export const DealBrokerAggregatorAdapterMemoryQPs=/**@type {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryQPs}*/(Object.keys(DealBrokerAggregatorAdapterMemoryPQs)
 .reduce((a,k)=>{a[DealBrokerAggregatorAdapterMemoryPQs[k]]=k;return a},{}))