import {DealBrokerAggregatorAdapterInputsPQs} from './DealBrokerAggregatorAdapterInputsPQs'
export const DealBrokerAggregatorAdapterInputsQPs=/**@type {!xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs}*/(Object.keys(DealBrokerAggregatorAdapterInputsPQs)
 .reduce((a,k)=>{a[DealBrokerAggregatorAdapterInputsPQs[k]]=k;return a},{}))