import AbstractDealBrokerAggregatorAdapterGPU from '../AbstractDealBrokerAggregatorAdapterGPU'
import AbstractDealBrokerAggregatorAdapterScreenBack from '../AbstractDealBrokerAggregatorAdapterScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {DealBrokerAggregatorAdapterInputsQPs} from '../../pqs/DealBrokerAggregatorAdapterInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractDealBrokerAggregatorAdapter from '../AbstractDealBrokerAggregatorAdapter'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterHtmlComponent}
 */
function __AbstractDealBrokerAggregatorAdapterHtmlComponent() {}
__AbstractDealBrokerAggregatorAdapterHtmlComponent.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent}
 */
class _AbstractDealBrokerAggregatorAdapterHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent} */ (res)
  }
}
/**
 * The _IDealBrokerAggregatorAdapter_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent} ‎
 */
export class AbstractDealBrokerAggregatorAdapterHtmlComponent extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterHtmlComponent,109642787711,null,{
  asIDealBrokerAggregatorAdapterHtmlComponent:1,
  superDealBrokerAggregatorAdapterHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent} */
AbstractDealBrokerAggregatorAdapterHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent} */
function AbstractDealBrokerAggregatorAdapterHtmlComponentClass(){}


AbstractDealBrokerAggregatorAdapterHtmlComponent[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterHtmlComponent,
 HtmlComponent,
 AbstractDealBrokerAggregatorAdapter,
 AbstractDealBrokerAggregatorAdapterGPU,
 AbstractDealBrokerAggregatorAdapterScreenBack,
 Landed,
 AbstractDealBrokerAggregatorAdapterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent}*/({
  constructor(){
   this.land={
    OffersAggregator:null,
   }
  },
 }),
 AbstractDealBrokerAggregatorAdapterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent}*/({
  inputsQPs:DealBrokerAggregatorAdapterInputsQPs,
 }),
 AbstractDealBrokerAggregatorAdapterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIDealBrokerAggregatorAdapterGPU:{
     OffersAggregator:OffersAggregator,
    },
   }=this
   complete(4890088757,{OffersAggregator:OffersAggregator},void 0,{
    estimatedAmountTo:'aa93d', // <- estimatedOut
    gettingOffer:'c22a3', // <- loadingEstimate
    minAmount:'2eae5', // <- minError
    maxAmount:'b3b72', // <- maxError
   })
  },
 }),
]