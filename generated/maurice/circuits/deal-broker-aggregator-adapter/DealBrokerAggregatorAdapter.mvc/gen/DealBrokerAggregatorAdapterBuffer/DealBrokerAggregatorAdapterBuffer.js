import {makeBuffers} from '@webcircuits/webcircuits'

export const DealBrokerAggregatorAdapterBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  estimatedAmountTo:String,
  minAmount:String,
  maxAmount:String,
  gettingOffer:Boolean,
 }),
})

export default DealBrokerAggregatorAdapterBuffer