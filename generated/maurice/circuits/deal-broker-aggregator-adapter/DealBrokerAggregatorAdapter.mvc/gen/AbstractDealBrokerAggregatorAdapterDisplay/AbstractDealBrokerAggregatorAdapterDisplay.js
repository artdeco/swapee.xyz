import {Display} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterDisplay}
 */
function __AbstractDealBrokerAggregatorAdapterDisplay() {}
__AbstractDealBrokerAggregatorAdapterDisplay.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterDisplay} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay} */ function DealBrokerAggregatorAdapterDisplayConstructor() {
  /** @type {HTMLElement} */ this.OffersAggregator=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay}
 */
class _AbstractDealBrokerAggregatorAdapterDisplay { }
/**
 * Display for presenting information from the _IDealBrokerAggregatorAdapter_.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay} ‎
 */
class AbstractDealBrokerAggregatorAdapterDisplay extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterDisplay,109642787716,DealBrokerAggregatorAdapterDisplayConstructor,{
  asIDealBrokerAggregatorAdapterDisplay:1,
  superDealBrokerAggregatorAdapterDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay} */
AbstractDealBrokerAggregatorAdapterDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay} */
function AbstractDealBrokerAggregatorAdapterDisplayClass(){}

export default AbstractDealBrokerAggregatorAdapterDisplay


AbstractDealBrokerAggregatorAdapterDisplay[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterDisplay,
 Display,
 AbstractDealBrokerAggregatorAdapterDisplayClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{offersAggregatorScopeSel:offersAggregatorScopeSel}}=this
    this.scan({
     offersAggregatorSel:offersAggregatorScopeSel,
    })
   })
  },
  scan:function vduScan({offersAggregatorSel:offersAggregatorSelScope}){
   const{element:element,queries:{offersAggregatorSel}}=this

   /**@type {HTMLElement}*/
   const OffersAggregator=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _OffersAggregator=offersAggregatorSel?root.querySelector(offersAggregatorSel):void 0
    return _OffersAggregator
   })(offersAggregatorSelScope)
   Object.assign(this,{
    OffersAggregator:OffersAggregator,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese}*/({
   OffersAggregator:1,
  }),
  initializer({
   OffersAggregator:_OffersAggregator,
  }) {
   if(_OffersAggregator!==undefined) this.OffersAggregator=_OffersAggregator
  },
 }),
]