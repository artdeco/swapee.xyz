import {mountPins} from '@type.engineering/seers'
import {DealBrokerAggregatorAdapterMemoryPQs} from '../../pqs/DealBrokerAggregatorAdapterMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerAggregatorAdapterCore}
 */
function __DealBrokerAggregatorAdapterCore() {}
__DealBrokerAggregatorAdapterCore.prototype = /** @type {!_DealBrokerAggregatorAdapterCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore}
 */
class _DealBrokerAggregatorAdapterCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore} ‎
 */
class DealBrokerAggregatorAdapterCore extends newAbstract(
 _DealBrokerAggregatorAdapterCore,10964278777,null,{
  asIDealBrokerAggregatorAdapterCore:1,
  superDealBrokerAggregatorAdapterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore} */
DealBrokerAggregatorAdapterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore} */
function DealBrokerAggregatorAdapterCoreClass(){}

export default DealBrokerAggregatorAdapterCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerAggregatorAdapterOuterCore}
 */
function __DealBrokerAggregatorAdapterOuterCore() {}
__DealBrokerAggregatorAdapterOuterCore.prototype = /** @type {!_DealBrokerAggregatorAdapterOuterCore} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore} */
export function DealBrokerAggregatorAdapterOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model}*/
  this.model={
    estimatedAmountTo: null,
    minAmount: '',
    maxAmount: '',
    gettingOffer: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore}
 */
class _DealBrokerAggregatorAdapterOuterCore { }
/**
 * The _IDealBrokerAggregatorAdapter_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore} ‎
 */
export class DealBrokerAggregatorAdapterOuterCore extends newAbstract(
 _DealBrokerAggregatorAdapterOuterCore,10964278773,DealBrokerAggregatorAdapterOuterCoreConstructor,{
  asIDealBrokerAggregatorAdapterOuterCore:1,
  superDealBrokerAggregatorAdapterOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore} */
DealBrokerAggregatorAdapterOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore} */
function DealBrokerAggregatorAdapterOuterCoreClass(){}


DealBrokerAggregatorAdapterOuterCore[$implementations]=[
 __DealBrokerAggregatorAdapterOuterCore,
 DealBrokerAggregatorAdapterOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore}*/({
  constructor(){
   mountPins(this.model,DealBrokerAggregatorAdapterMemoryPQs)

  },
 }),
]

DealBrokerAggregatorAdapterCore[$implementations]=[
 DealBrokerAggregatorAdapterCoreClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore}*/({
  resetCore(){
   this.resetDealBrokerAggregatorAdapterCore()
  },
  resetDealBrokerAggregatorAdapterCore(){
   DealBrokerAggregatorAdapterOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore}*/(
     /**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore}*/(this)),
   )
  },
 }),
 __DealBrokerAggregatorAdapterCore,
 DealBrokerAggregatorAdapterOuterCore,
]

export {DealBrokerAggregatorAdapterCore}