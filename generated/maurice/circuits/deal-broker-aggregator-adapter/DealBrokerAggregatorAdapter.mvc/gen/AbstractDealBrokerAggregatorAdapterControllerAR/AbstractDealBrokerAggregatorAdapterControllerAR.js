import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterControllerAR}
 */
function __AbstractDealBrokerAggregatorAdapterControllerAR() {}
__AbstractDealBrokerAggregatorAdapterControllerAR.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR}
 */
class _AbstractDealBrokerAggregatorAdapterControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IDealBrokerAggregatorAdapterControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR} ‎
 */
class AbstractDealBrokerAggregatorAdapterControllerAR extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterControllerAR,109642787722,null,{
  asIDealBrokerAggregatorAdapterControllerAR:1,
  superDealBrokerAggregatorAdapterControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR} */
AbstractDealBrokerAggregatorAdapterControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR} */
function AbstractDealBrokerAggregatorAdapterControllerARClass(){}

export default AbstractDealBrokerAggregatorAdapterControllerAR


AbstractDealBrokerAggregatorAdapterControllerAR[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterControllerAR,
 AR,
 AbstractDealBrokerAggregatorAdapterControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR}*/({
  allocator(){
   this.methods={
    flipGettingOffer:'eaff5',
    setGettingOffer:'77447',
    unsetGettingOffer:'b1596',
   }
  },
 }),
]