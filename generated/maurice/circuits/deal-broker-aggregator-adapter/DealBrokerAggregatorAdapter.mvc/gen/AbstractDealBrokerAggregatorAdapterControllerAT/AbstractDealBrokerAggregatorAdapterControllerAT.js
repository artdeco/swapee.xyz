import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterControllerAT}
 */
function __AbstractDealBrokerAggregatorAdapterControllerAT() {}
__AbstractDealBrokerAggregatorAdapterControllerAT.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT}
 */
class _AbstractDealBrokerAggregatorAdapterControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IDealBrokerAggregatorAdapterControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT} ‎
 */
class AbstractDealBrokerAggregatorAdapterControllerAT extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterControllerAT,109642787723,null,{
  asIDealBrokerAggregatorAdapterControllerAT:1,
  superDealBrokerAggregatorAdapterControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT} */
AbstractDealBrokerAggregatorAdapterControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT} */
function AbstractDealBrokerAggregatorAdapterControllerATClass(){}

export default AbstractDealBrokerAggregatorAdapterControllerAT


AbstractDealBrokerAggregatorAdapterControllerAT[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterControllerAT,
 UartUniversal,
 AbstractDealBrokerAggregatorAdapterControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT}*/({
  get asIDealBrokerAggregatorAdapterController(){
   return this
  },
  flipGettingOffer(){
   this.uart.t("inv",{mid:'eaff5'})
  },
  setGettingOffer(){
   this.uart.t("inv",{mid:'77447'})
  },
  unsetGettingOffer(){
   this.uart.t("inv",{mid:'b1596'})
  },
 }),
]