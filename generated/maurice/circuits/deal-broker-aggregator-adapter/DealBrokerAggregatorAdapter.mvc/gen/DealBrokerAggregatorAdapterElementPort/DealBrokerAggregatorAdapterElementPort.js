import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerAggregatorAdapterElementPort}
 */
function __DealBrokerAggregatorAdapterElementPort() {}
__DealBrokerAggregatorAdapterElementPort.prototype = /** @type {!_DealBrokerAggregatorAdapterElementPort} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort} */ function DealBrokerAggregatorAdapterElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    offersAggregatorOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort}
 */
class _DealBrokerAggregatorAdapterElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort} ‎
 */
class DealBrokerAggregatorAdapterElementPort extends newAbstract(
 _DealBrokerAggregatorAdapterElementPort,109642787713,DealBrokerAggregatorAdapterElementPortConstructor,{
  asIDealBrokerAggregatorAdapterElementPort:1,
  superDealBrokerAggregatorAdapterElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort} */
DealBrokerAggregatorAdapterElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort} */
function DealBrokerAggregatorAdapterElementPortClass(){}

export default DealBrokerAggregatorAdapterElementPort


DealBrokerAggregatorAdapterElementPort[$implementations]=[
 __DealBrokerAggregatorAdapterElementPort,
 DealBrokerAggregatorAdapterElementPortClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'offers-aggregator-opts':undefined,
    'estimated-amount-to':undefined,
    'min-amount':undefined,
    'max-amount':undefined,
    'getting-offer':undefined,
   })
  },
 }),
]