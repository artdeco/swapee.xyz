import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterProcessor}
 */
function __AbstractDealBrokerAggregatorAdapterProcessor() {}
__AbstractDealBrokerAggregatorAdapterProcessor.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor}
 */
class _AbstractDealBrokerAggregatorAdapterProcessor { }
/**
 * The processor to compute changes to the memory for the _IDealBrokerAggregatorAdapter_.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor} ‎
 */
class AbstractDealBrokerAggregatorAdapterProcessor extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterProcessor,10964278778,null,{
  asIDealBrokerAggregatorAdapterProcessor:1,
  superDealBrokerAggregatorAdapterProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor} */
AbstractDealBrokerAggregatorAdapterProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor} */
function AbstractDealBrokerAggregatorAdapterProcessorClass(){}

export default AbstractDealBrokerAggregatorAdapterProcessor


AbstractDealBrokerAggregatorAdapterProcessor[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterProcessor,
 AbstractDealBrokerAggregatorAdapterProcessorClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor}*/({
  flipGettingOffer(){
   const{
    asIDealBrokerAggregatorAdapterComputer:{
     model:{gettingOffer:gettingOffer},
     setInfo:setInfo,
    },
   }=this
   setInfo({gettingOffer:!gettingOffer})
  },
 }),
]