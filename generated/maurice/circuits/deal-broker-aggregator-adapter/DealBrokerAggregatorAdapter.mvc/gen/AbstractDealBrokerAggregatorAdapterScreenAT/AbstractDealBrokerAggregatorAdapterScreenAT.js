import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterScreenAT}
 */
function __AbstractDealBrokerAggregatorAdapterScreenAT() {}
__AbstractDealBrokerAggregatorAdapterScreenAT.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT}
 */
class _AbstractDealBrokerAggregatorAdapterScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IDealBrokerAggregatorAdapterScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT} ‎
 */
class AbstractDealBrokerAggregatorAdapterScreenAT extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterScreenAT,109642787727,null,{
  asIDealBrokerAggregatorAdapterScreenAT:1,
  superDealBrokerAggregatorAdapterScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT} */
AbstractDealBrokerAggregatorAdapterScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT} */
function AbstractDealBrokerAggregatorAdapterScreenATClass(){}

export default AbstractDealBrokerAggregatorAdapterScreenAT


AbstractDealBrokerAggregatorAdapterScreenAT[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterScreenAT,
 UartUniversal,
]