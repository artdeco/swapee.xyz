import AbstractDealBrokerAggregatorAdapterScreenAT from '../AbstractDealBrokerAggregatorAdapterScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterScreenBack}
 */
function __AbstractDealBrokerAggregatorAdapterScreenBack() {}
__AbstractDealBrokerAggregatorAdapterScreenBack.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen}
 */
class _AbstractDealBrokerAggregatorAdapterScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen} ‎
 */
class AbstractDealBrokerAggregatorAdapterScreenBack extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterScreenBack,109642787725,null,{
  asIDealBrokerAggregatorAdapterScreen:1,
  superDealBrokerAggregatorAdapterScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen} */
AbstractDealBrokerAggregatorAdapterScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen} */
function AbstractDealBrokerAggregatorAdapterScreenBackClass(){}

export default AbstractDealBrokerAggregatorAdapterScreenBack


AbstractDealBrokerAggregatorAdapterScreenBack[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterScreenBack,
 AbstractDealBrokerAggregatorAdapterScreenAT,
]