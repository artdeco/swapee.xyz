import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterDisplay}
 */
function __AbstractDealBrokerAggregatorAdapterDisplay() {}
__AbstractDealBrokerAggregatorAdapterDisplay.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay}
 */
class _AbstractDealBrokerAggregatorAdapterDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay} ‎
 */
class AbstractDealBrokerAggregatorAdapterDisplay extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterDisplay,109642787718,null,{
  asIDealBrokerAggregatorAdapterDisplay:1,
  superDealBrokerAggregatorAdapterDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay} */
AbstractDealBrokerAggregatorAdapterDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay} */
function AbstractDealBrokerAggregatorAdapterDisplayClass(){}

export default AbstractDealBrokerAggregatorAdapterDisplay


AbstractDealBrokerAggregatorAdapterDisplay[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterDisplay,
 GraphicsDriverBack,
 AbstractDealBrokerAggregatorAdapterDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay}*/({
    OffersAggregator:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.Initialese}*/({
   OffersAggregator:1,
  }),
  initializer({
   OffersAggregator:_OffersAggregator,
  }) {
   if(_OffersAggregator!==undefined) this.OffersAggregator=_OffersAggregator
  },
 }),
]