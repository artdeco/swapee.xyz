import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {DealBrokerAggregatorAdapterInputsPQs} from '../../pqs/DealBrokerAggregatorAdapterInputsPQs'
import {DealBrokerAggregatorAdapterOuterCoreConstructor} from '../DealBrokerAggregatorAdapterCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerAggregatorAdapterPort}
 */
function __DealBrokerAggregatorAdapterPort() {}
__DealBrokerAggregatorAdapterPort.prototype = /** @type {!_DealBrokerAggregatorAdapterPort} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerAggregatorAdapterPort} */ function DealBrokerAggregatorAdapterPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore} */ ({model:null})
  DealBrokerAggregatorAdapterOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort}
 */
class _DealBrokerAggregatorAdapterPort { }
/**
 * The port that serves as an interface to the _IDealBrokerAggregatorAdapter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort} ‎
 */
export class DealBrokerAggregatorAdapterPort extends newAbstract(
 _DealBrokerAggregatorAdapterPort,10964278775,DealBrokerAggregatorAdapterPortConstructor,{
  asIDealBrokerAggregatorAdapterPort:1,
  superDealBrokerAggregatorAdapterPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort} */
DealBrokerAggregatorAdapterPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort} */
function DealBrokerAggregatorAdapterPortClass(){}

export const DealBrokerAggregatorAdapterPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Pinout>}*/({
 get Port() { return DealBrokerAggregatorAdapterPort },
})

DealBrokerAggregatorAdapterPort[$implementations]=[
 DealBrokerAggregatorAdapterPortClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort}*/({
  resetPort(){
   this.resetDealBrokerAggregatorAdapterPort()
  },
  resetDealBrokerAggregatorAdapterPort(){
   DealBrokerAggregatorAdapterPortConstructor.call(this)
  },
 }),
 __DealBrokerAggregatorAdapterPort,
 Parametric,
 DealBrokerAggregatorAdapterPortClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort}*/({
  constructor(){
   mountPins(this.inputs,DealBrokerAggregatorAdapterInputsPQs)
  },
 }),
]


export default DealBrokerAggregatorAdapterPort