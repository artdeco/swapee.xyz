import AbstractDealBrokerAggregatorAdapterDisplay from '../AbstractDealBrokerAggregatorAdapterDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {DealBrokerAggregatorAdapterVdusPQs} from '../../pqs/DealBrokerAggregatorAdapterVdusPQs'
import {DealBrokerAggregatorAdapterVdusQPs} from '../../pqs/DealBrokerAggregatorAdapterVdusQPs'
import {DealBrokerAggregatorAdapterMemoryPQs} from '../../pqs/DealBrokerAggregatorAdapterMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterGPU}
 */
function __AbstractDealBrokerAggregatorAdapterGPU() {}
__AbstractDealBrokerAggregatorAdapterGPU.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU}
 */
class _AbstractDealBrokerAggregatorAdapterGPU { }
/**
 * Handles the periphery of the _IDealBrokerAggregatorAdapterDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU} ‎
 */
class AbstractDealBrokerAggregatorAdapterGPU extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterGPU,109642787715,null,{
  asIDealBrokerAggregatorAdapterGPU:1,
  superDealBrokerAggregatorAdapterGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU} */
AbstractDealBrokerAggregatorAdapterGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU} */
function AbstractDealBrokerAggregatorAdapterGPUClass(){}

export default AbstractDealBrokerAggregatorAdapterGPU


AbstractDealBrokerAggregatorAdapterGPU[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterGPU,
 AbstractDealBrokerAggregatorAdapterGPUClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU}*/({
  vdusPQs:DealBrokerAggregatorAdapterVdusPQs,
  vdusQPs:DealBrokerAggregatorAdapterVdusQPs,
  memoryPQs:DealBrokerAggregatorAdapterMemoryPQs,
 }),
 AbstractDealBrokerAggregatorAdapterDisplay,
 BrowserView,
]