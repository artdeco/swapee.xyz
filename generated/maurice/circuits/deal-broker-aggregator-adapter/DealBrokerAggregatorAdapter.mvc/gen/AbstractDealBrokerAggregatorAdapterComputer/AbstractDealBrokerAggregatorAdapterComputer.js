import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterComputer}
 */
function __AbstractDealBrokerAggregatorAdapterComputer() {}
__AbstractDealBrokerAggregatorAdapterComputer.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer}
 */
class _AbstractDealBrokerAggregatorAdapterComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer} ‎
 */
export class AbstractDealBrokerAggregatorAdapterComputer extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterComputer,10964278771,null,{
  asIDealBrokerAggregatorAdapterComputer:1,
  superDealBrokerAggregatorAdapterComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer} */
AbstractDealBrokerAggregatorAdapterComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer} */
function AbstractDealBrokerAggregatorAdapterComputerClass(){}


AbstractDealBrokerAggregatorAdapterComputer[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterComputer,
 Adapter,
]


export default AbstractDealBrokerAggregatorAdapterComputer