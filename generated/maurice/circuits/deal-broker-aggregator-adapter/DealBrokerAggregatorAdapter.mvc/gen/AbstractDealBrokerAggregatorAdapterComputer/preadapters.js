
/**@this {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer}*/
export function preadaptTest(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.adaptTest.Form}*/
 const _inputs={
  estimatedAmountTo:inputs.estimatedAmountTo,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptTest(__inputs,__changes)
 return RET
}