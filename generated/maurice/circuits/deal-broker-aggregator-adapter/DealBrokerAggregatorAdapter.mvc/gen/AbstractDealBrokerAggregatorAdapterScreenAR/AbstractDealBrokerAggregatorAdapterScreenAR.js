import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterScreenAR}
 */
function __AbstractDealBrokerAggregatorAdapterScreenAR() {}
__AbstractDealBrokerAggregatorAdapterScreenAR.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR}
 */
class _AbstractDealBrokerAggregatorAdapterScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IDealBrokerAggregatorAdapterScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR} ‎
 */
class AbstractDealBrokerAggregatorAdapterScreenAR extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterScreenAR,109642787726,null,{
  asIDealBrokerAggregatorAdapterScreenAR:1,
  superDealBrokerAggregatorAdapterScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR} */
AbstractDealBrokerAggregatorAdapterScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR} */
function AbstractDealBrokerAggregatorAdapterScreenARClass(){}

export default AbstractDealBrokerAggregatorAdapterScreenAR


AbstractDealBrokerAggregatorAdapterScreenAR[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterScreenAR,
 AR,
 AbstractDealBrokerAggregatorAdapterScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractDealBrokerAggregatorAdapterScreenAR}