import DealBrokerAggregatorAdapterBuffer from '../DealBrokerAggregatorAdapterBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {DealBrokerAggregatorAdapterPortConnector} from '../DealBrokerAggregatorAdapterPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterController}
 */
function __AbstractDealBrokerAggregatorAdapterController() {}
__AbstractDealBrokerAggregatorAdapterController.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController}
 */
class _AbstractDealBrokerAggregatorAdapterController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController} ‎
 */
export class AbstractDealBrokerAggregatorAdapterController extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterController,109642787719,null,{
  asIDealBrokerAggregatorAdapterController:1,
  superDealBrokerAggregatorAdapterController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController} */
AbstractDealBrokerAggregatorAdapterController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController} */
function AbstractDealBrokerAggregatorAdapterControllerClass(){}


AbstractDealBrokerAggregatorAdapterController[$implementations]=[
 AbstractDealBrokerAggregatorAdapterControllerClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractDealBrokerAggregatorAdapterController,
 DealBrokerAggregatorAdapterBuffer,
 IntegratedController,
 /**@type {!AbstractDealBrokerAggregatorAdapterController}*/(DealBrokerAggregatorAdapterPortConnector),
 AbstractDealBrokerAggregatorAdapterControllerClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterController}*/({
  setGettingOffer(){
   const{asIDealBrokerAggregatorAdapterController:{setInputs:setInputs}}=this
   setInputs({gettingOffer:true})
  },
  unsetGettingOffer(){
   const{asIDealBrokerAggregatorAdapterController:{setInputs:setInputs}}=this
   setInputs({gettingOffer:false})
  },
 }),
]


export default AbstractDealBrokerAggregatorAdapterController