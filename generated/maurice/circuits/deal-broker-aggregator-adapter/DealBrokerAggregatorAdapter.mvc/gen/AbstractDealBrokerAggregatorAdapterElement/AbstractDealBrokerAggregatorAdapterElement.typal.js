
import AbstractDealBrokerAggregatorAdapter from '../AbstractDealBrokerAggregatorAdapter'

/** @abstract {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement} */
export default class AbstractDealBrokerAggregatorAdapterElement { }



AbstractDealBrokerAggregatorAdapterElement[$implementations]=[AbstractDealBrokerAggregatorAdapter,
 /** @type {!AbstractDealBrokerAggregatorAdapterElement} */ ({
  rootId:'DealBrokerAggregatorAdapter',
  __$id:1096427877,
  fqn:'xyz.swapee.wc.IDealBrokerAggregatorAdapter',
  maurice_element_v3:true,
 }),
]