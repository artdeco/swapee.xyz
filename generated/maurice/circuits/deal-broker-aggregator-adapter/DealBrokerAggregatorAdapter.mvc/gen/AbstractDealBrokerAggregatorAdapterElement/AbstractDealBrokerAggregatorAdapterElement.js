import DealBrokerAggregatorAdapterElementPort from '../DealBrokerAggregatorAdapterElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {DealBrokerAggregatorAdapterInputsPQs} from '../../pqs/DealBrokerAggregatorAdapterInputsPQs'
import {DealBrokerAggregatorAdapterQueriesPQs} from '../../pqs/DealBrokerAggregatorAdapterQueriesPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractDealBrokerAggregatorAdapter from '../AbstractDealBrokerAggregatorAdapter'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterElement}
 */
function __AbstractDealBrokerAggregatorAdapterElement() {}
__AbstractDealBrokerAggregatorAdapterElement.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement}
 */
class _AbstractDealBrokerAggregatorAdapterElement { }
/**
 * A component description.
 *
 * The _IDealBrokerAggregatorAdapter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement} ‎
 */
class AbstractDealBrokerAggregatorAdapterElement extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterElement,109642787712,null,{
  asIDealBrokerAggregatorAdapterElement:1,
  superDealBrokerAggregatorAdapterElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement} */
AbstractDealBrokerAggregatorAdapterElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement} */
function AbstractDealBrokerAggregatorAdapterElementClass(){}

export default AbstractDealBrokerAggregatorAdapterElement


AbstractDealBrokerAggregatorAdapterElement[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterElement,
 ElementBase,
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':estimated-amount-to':estimatedAmountToColAttr,
   ':min-amount':minAmountColAttr,
   ':max-amount':maxAmountColAttr,
   ':getting-offer':gettingOfferColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'estimated-amount-to':estimatedAmountToAttr,
    'min-amount':minAmountAttr,
    'max-amount':maxAmountAttr,
    'getting-offer':gettingOfferAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(estimatedAmountToAttr===undefined?{'estimated-amount-to':estimatedAmountToColAttr}:{}),
    ...(minAmountAttr===undefined?{'min-amount':minAmountColAttr}:{}),
    ...(maxAmountAttr===undefined?{'max-amount':maxAmountColAttr}:{}),
    ...(gettingOfferAttr===undefined?{'getting-offer':gettingOfferColAttr}:{}),
   }
  },
 }),
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'estimated-amount-to':estimatedAmountToAttr,
   'min-amount':minAmountAttr,
   'max-amount':maxAmountAttr,
   'getting-offer':gettingOfferAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    estimatedAmountTo:estimatedAmountToAttr,
    minAmount:minAmountAttr,
    maxAmount:maxAmountAttr,
    gettingOffer:gettingOfferAttr,
   }
  },
 }),
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  inputsPQs:DealBrokerAggregatorAdapterInputsPQs,
  queriesPQs:DealBrokerAggregatorAdapterQueriesPQs,
  vdus:{
   'OffersAggregator': 'ce711',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','estimatedAmountTo','minAmount','maxAmount','gettingOffer','query:offers-aggregator','no-solder',':no-solder','estimated-amount-to',':estimated-amount-to','min-amount',':min-amount','max-amount',':max-amount','getting-offer',':getting-offer','fe646','08eb7','c63f7','ae0d3','d0b0c','341da','children']),
   })
  },
  get Port(){
   return DealBrokerAggregatorAdapterElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:offers-aggregator':offersAggregatorSel}){
   const _ret={}
   if(offersAggregatorSel) _ret.offersAggregatorSel=offersAggregatorSel
   return _ret
  },
 }),
 Landed,
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  constructor(){
   this.land={
    OffersAggregator:null,
   }
  },
 }),
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  calibrate:async function awaitOnOffersAggregator({offersAggregatorSel:offersAggregatorSel}){
   if(!offersAggregatorSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const OffersAggregator=await milleu(offersAggregatorSel)
   if(!OffersAggregator) {
    console.warn('❗️ offersAggregatorSel %s must be present on the page for %s to work',offersAggregatorSel,fqn)
    return{}
   }
   land.OffersAggregator=OffersAggregator
  },
 }),
 AbstractDealBrokerAggregatorAdapterElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
  solder:(_,{
   offersAggregatorSel:offersAggregatorSel,
  })=>{
   return{
    offersAggregatorSel:offersAggregatorSel,
   }
  },
 }),
]



AbstractDealBrokerAggregatorAdapterElement[$implementations]=[AbstractDealBrokerAggregatorAdapter,
 /** @type {!AbstractDealBrokerAggregatorAdapterElement} */ ({
  rootId:'DealBrokerAggregatorAdapter',
  __$id:1096427877,
  fqn:'xyz.swapee.wc.IDealBrokerAggregatorAdapter',
  maurice_element_v3:true,
 }),
]