import AbstractDealBrokerAggregatorAdapterControllerAR from '../AbstractDealBrokerAggregatorAdapterControllerAR'
import {AbstractDealBrokerAggregatorAdapterController} from '../AbstractDealBrokerAggregatorAdapterController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterControllerBack}
 */
function __AbstractDealBrokerAggregatorAdapterControllerBack() {}
__AbstractDealBrokerAggregatorAdapterControllerBack.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController}
 */
class _AbstractDealBrokerAggregatorAdapterControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController} ‎
 */
class AbstractDealBrokerAggregatorAdapterControllerBack extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterControllerBack,109642787721,null,{
  asIDealBrokerAggregatorAdapterController:1,
  superDealBrokerAggregatorAdapterController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController} */
AbstractDealBrokerAggregatorAdapterControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController} */
function AbstractDealBrokerAggregatorAdapterControllerBackClass(){}

export default AbstractDealBrokerAggregatorAdapterControllerBack


AbstractDealBrokerAggregatorAdapterControllerBack[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterControllerBack,
 AbstractDealBrokerAggregatorAdapterController,
 AbstractDealBrokerAggregatorAdapterControllerAR,
 DriverBack,
]