import AbstractDealBrokerAggregatorAdapterProcessor from '../AbstractDealBrokerAggregatorAdapterProcessor'
import {DealBrokerAggregatorAdapterCore} from '../DealBrokerAggregatorAdapterCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractDealBrokerAggregatorAdapterComputer} from '../AbstractDealBrokerAggregatorAdapterComputer'
import {AbstractDealBrokerAggregatorAdapterController} from '../AbstractDealBrokerAggregatorAdapterController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapter}
 */
function __AbstractDealBrokerAggregatorAdapter() {}
__AbstractDealBrokerAggregatorAdapter.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapter} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter}
 */
class _AbstractDealBrokerAggregatorAdapter { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter} ‎
 */
class AbstractDealBrokerAggregatorAdapter extends newAbstract(
 _AbstractDealBrokerAggregatorAdapter,10964278779,null,{
  asIDealBrokerAggregatorAdapter:1,
  superDealBrokerAggregatorAdapter:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter} */
AbstractDealBrokerAggregatorAdapter.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter} */
function AbstractDealBrokerAggregatorAdapterClass(){}

export default AbstractDealBrokerAggregatorAdapter


AbstractDealBrokerAggregatorAdapter[$implementations]=[
 __AbstractDealBrokerAggregatorAdapter,
 DealBrokerAggregatorAdapterCore,
 AbstractDealBrokerAggregatorAdapterProcessor,
 IntegratedComponent,
 AbstractDealBrokerAggregatorAdapterComputer,
 AbstractDealBrokerAggregatorAdapterController,
]


export {AbstractDealBrokerAggregatorAdapter}