import AbstractDealBrokerAggregatorAdapterScreenAR from '../AbstractDealBrokerAggregatorAdapterScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {DealBrokerAggregatorAdapterInputsPQs} from '../../pqs/DealBrokerAggregatorAdapterInputsPQs'
import {DealBrokerAggregatorAdapterQueriesPQs} from '../../pqs/DealBrokerAggregatorAdapterQueriesPQs'
import {DealBrokerAggregatorAdapterMemoryQPs} from '../../pqs/DealBrokerAggregatorAdapterMemoryQPs'
import {DealBrokerAggregatorAdapterVdusPQs} from '../../pqs/DealBrokerAggregatorAdapterVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerAggregatorAdapterScreen}
 */
function __AbstractDealBrokerAggregatorAdapterScreen() {}
__AbstractDealBrokerAggregatorAdapterScreen.prototype = /** @type {!_AbstractDealBrokerAggregatorAdapterScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen}
 */
class _AbstractDealBrokerAggregatorAdapterScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen} ‎
 */
class AbstractDealBrokerAggregatorAdapterScreen extends newAbstract(
 _AbstractDealBrokerAggregatorAdapterScreen,109642787724,null,{
  asIDealBrokerAggregatorAdapterScreen:1,
  superDealBrokerAggregatorAdapterScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen} */
AbstractDealBrokerAggregatorAdapterScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen} */
function AbstractDealBrokerAggregatorAdapterScreenClass(){}

export default AbstractDealBrokerAggregatorAdapterScreen


AbstractDealBrokerAggregatorAdapterScreen[$implementations]=[
 __AbstractDealBrokerAggregatorAdapterScreen,
 AbstractDealBrokerAggregatorAdapterScreenClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen}*/({
  inputsPQs:DealBrokerAggregatorAdapterInputsPQs,
  queriesPQs:DealBrokerAggregatorAdapterQueriesPQs,
  memoryQPs:DealBrokerAggregatorAdapterMemoryQPs,
 }),
 Screen,
 AbstractDealBrokerAggregatorAdapterScreenAR,
 AbstractDealBrokerAggregatorAdapterScreenClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen}*/({
  vdusPQs:DealBrokerAggregatorAdapterVdusPQs,
 }),
]