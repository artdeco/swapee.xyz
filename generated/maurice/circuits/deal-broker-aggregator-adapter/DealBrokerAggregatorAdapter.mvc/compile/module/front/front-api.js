import { DealBrokerAggregatorAdapterDisplay, DealBrokerAggregatorAdapterScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay} */
export { DealBrokerAggregatorAdapterDisplay }
/** @lazy @api {xyz.swapee.wc.DealBrokerAggregatorAdapterScreen} */
export { DealBrokerAggregatorAdapterScreen }