import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1096427877} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const f=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const g=f["37270038985"],h=f["372700389810"],l=f["372700389811"];function m(a,b,k,d){return f["372700389812"](a,b,k,d,!1,void 0)};
const n=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const p=n["61893096584"],q=n["61893096586"],r=n["618930965811"],t=n["618930965812"];function u(){}u.prototype={};function v(){this.g=null}class w{}class x extends m(w,109642787716,v,{j:1,D:2}){}x[l]=[u,p,{constructor(){g(this,()=>{const {queries:{A:a}}=this;this.scan({h:a})})},scan:function({h:a}){const {element:b,queries:{h:k}}=this;let d;a?d=b.closest(a):d=document;Object.assign(this,{g:k?d.querySelector(k):void 0})}},{[h]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var y=class extends x.implements(){};function z(){}z.prototype={};class A{}class B extends m(A,109642787723,null,{i:1,C:2}){}B[l]=[z,r,{}];function C(){}C.prototype={};class D{}class E extends m(D,109642787726,null,{m:1,G:2}){}E[l]=[C,t,{allocator(){this.methods={}}}];const F={o:"c63f7",v:"ae0d3",u:"d0b0c",s:"341da"};const G={...F};const H=Object.keys(F).reduce((a,b)=>{a[F[b]]=b;return a},{});function I(){}I.prototype={};class J{}class K extends m(J,109642787724,null,{l:1,F:2}){}function L(){}K[l]=[I,L.prototype={inputsPQs:G,queriesPQs:{h:"cde34"},memoryQPs:H},q,E,L.prototype={vdusPQs:{g:"ce711"}}];var M=class extends K.implements(B,y,{get queries(){return this.settings}},{__$id:1096427877}){};module.exports["109642787741"]=y;module.exports["109642787771"]=M;
/*! @embed-object-end {1096427877} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule