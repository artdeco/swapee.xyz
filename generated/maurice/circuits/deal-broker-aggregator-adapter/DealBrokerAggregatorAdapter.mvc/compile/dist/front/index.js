/**
 * Display for presenting information from the _IDealBrokerAggregatorAdapter_.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay}
 */
class DealBrokerAggregatorAdapterDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterScreen}
 */
class DealBrokerAggregatorAdapterScreen extends (class {/* lazy-loaded */}) {}

module.exports.DealBrokerAggregatorAdapterDisplay = DealBrokerAggregatorAdapterDisplay
module.exports.DealBrokerAggregatorAdapterScreen = DealBrokerAggregatorAdapterScreen