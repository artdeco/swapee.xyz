/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapter` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter}
 */
class AbstractDealBrokerAggregatorAdapter extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IDealBrokerAggregatorAdapter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterPort}
 */
class DealBrokerAggregatorAdapterPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterController` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController}
 */
class AbstractDealBrokerAggregatorAdapterController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IDealBrokerAggregatorAdapter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterElement}
 */
class DealBrokerAggregatorAdapterElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterBuffer}
 */
class DealBrokerAggregatorAdapterBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer}
 */
class AbstractDealBrokerAggregatorAdapterComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterController}
 */
class DealBrokerAggregatorAdapterController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractDealBrokerAggregatorAdapter = AbstractDealBrokerAggregatorAdapter
module.exports.DealBrokerAggregatorAdapterPort = DealBrokerAggregatorAdapterPort
module.exports.AbstractDealBrokerAggregatorAdapterController = AbstractDealBrokerAggregatorAdapterController
module.exports.DealBrokerAggregatorAdapterElement = DealBrokerAggregatorAdapterElement
module.exports.DealBrokerAggregatorAdapterBuffer = DealBrokerAggregatorAdapterBuffer
module.exports.AbstractDealBrokerAggregatorAdapterComputer = AbstractDealBrokerAggregatorAdapterComputer
module.exports.DealBrokerAggregatorAdapterController = DealBrokerAggregatorAdapterController

Object.defineProperties(module.exports, {
 'AbstractDealBrokerAggregatorAdapter': {get: () => require('./precompile/internal')[10964278771]},
 [10964278771]: {get: () => module.exports['AbstractDealBrokerAggregatorAdapter']},
 'DealBrokerAggregatorAdapterPort': {get: () => require('./precompile/internal')[10964278773]},
 [10964278773]: {get: () => module.exports['DealBrokerAggregatorAdapterPort']},
 'AbstractDealBrokerAggregatorAdapterController': {get: () => require('./precompile/internal')[10964278774]},
 [10964278774]: {get: () => module.exports['AbstractDealBrokerAggregatorAdapterController']},
 'DealBrokerAggregatorAdapterElement': {get: () => require('./precompile/internal')[10964278778]},
 [10964278778]: {get: () => module.exports['DealBrokerAggregatorAdapterElement']},
 'DealBrokerAggregatorAdapterBuffer': {get: () => require('./precompile/internal')[109642787711]},
 [109642787711]: {get: () => module.exports['DealBrokerAggregatorAdapterBuffer']},
 'AbstractDealBrokerAggregatorAdapterComputer': {get: () => require('./precompile/internal')[109642787730]},
 [109642787730]: {get: () => module.exports['AbstractDealBrokerAggregatorAdapterComputer']},
 'DealBrokerAggregatorAdapterController': {get: () => require('./precompile/internal')[109642787761]},
 [109642787761]: {get: () => module.exports['DealBrokerAggregatorAdapterController']},
})