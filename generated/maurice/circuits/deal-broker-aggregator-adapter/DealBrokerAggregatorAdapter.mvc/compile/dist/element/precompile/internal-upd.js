import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1096427877} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function e(a,b,f,g){return c["372700389812"](a,b,f,g,!1,void 0)};function k(){}k.prototype={};class l{}class m extends e(l,10964278778,null,{I:1,X:2}){}m[d]=[k,{}];


const n=function(){return require(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];const p={i:"c63f7",m:"ae0d3",l:"d0b0c",j:"341da"};function q(){}q.prototype={};class r{}class t extends e(r,10964278777,null,{A:1,R:2}){}function u(){}u.prototype={};function v(){this.model={i:null,m:"",l:"",j:!1}}class w{}class x extends e(w,10964278773,v,{G:1,V:2}){}x[d]=[u,{constructor(){n(this.model,p)}}];t[d]=[{},q,x];

const y=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const z=y.IntegratedComponentInitialiser,A=y.IntegratedComponent,B=y["95173443851"],C=y["95173443852"];
const D=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const E=D["615055805212"],aa=D["615055805218"],ba=D["615055805235"];function F(){}F.prototype={};class ca{}class G extends e(ca,10964278771,null,{u:1,M:2}){}G[d]=[F,aa];const H={regulate:E({i:String,m:String,l:String,j:Boolean})};
const I=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const da=I.IntegratedController,ea=I.Parametric;const J={...p};function K(){}K.prototype={};function L(){const a={model:null};v.call(a);this.inputs=a.model}class fa{}class M extends e(fa,10964278775,L,{H:1,W:2}){}function N(){}M[d]=[N.prototype={resetPort(){L.call(this)}},K,ea,N.prototype={constructor(){n(this.inputs,J)}}];function O(){}O.prototype={};class ha{}class P extends e(ha,109642787719,null,{v:1,P:2}){}function Q(){}P[d]=[Q.prototype={resetPort(){this.port.resetPort()}},O,H,da,{get Port(){return M}},Q.prototype={}];function R(){}R.prototype={};class ia{}class S extends e(ia,10964278779,null,{s:1,K:2}){}S[d]=[R,t,m,A,G,P];function ja(){return{}};const ka=require(eval('"@type.engineering/web-computing"')).h;function la(){return ka("div",{$id:"DealBrokerAggregatorAdapter"})};const ma=require(eval('"@type.engineering/web-computing"')).h;function na(){return ma("div",{$id:"DealBrokerAggregatorAdapter"})};var T=class extends P.implements(){};require("https");require("http");const oa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}oa("aqt");require("fs");require("child_process");
const U=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const pa=U.ElementBase,qa=U.HTMLBlocker;function V(){}V.prototype={};function ra(){this.inputs={noSolder:!1,J:{}}}class sa{}class W extends e(sa,109642787713,ra,{F:1,U:2}){}W[d]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"offers-aggregator-opts":void 0,"estimated-amount-to":void 0,"min-amount":void 0,"max-amount":void 0,"getting-offer":void 0})}}];function X(){}X.prototype={};class ta{}class Y extends e(ta,109642787712,null,{D:1,T:2}){}function Z(){}
Y[d]=[X,pa,Z.prototype={calibrate:function({":no-solder":a,":estimated-amount-to":b,":min-amount":f,":max-amount":g,":getting-offer":h}){const {attributes:{"no-solder":ua,"estimated-amount-to":va,"min-amount":wa,"max-amount":xa,"getting-offer":ya}}=this;return{...(void 0===ua?{"no-solder":a}:{}),...(void 0===va?{"estimated-amount-to":b}:{}),...(void 0===wa?{"min-amount":f}:{}),...(void 0===xa?{"max-amount":g}:{}),...(void 0===ya?{"getting-offer":h}:{})}}},Z.prototype={calibrate:({"no-solder":a,"estimated-amount-to":b,
"min-amount":f,"max-amount":g,"getting-offer":h})=>({noSolder:a,i:b,m:f,l:g,j:h})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={inputsPQs:J,queriesPQs:{g:"cde34"},vdus:{OffersAggregator:"ce711"}},A,C,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder estimatedAmountTo minAmount maxAmount gettingOffer query:offers-aggregator no-solder :no-solder estimated-amount-to :estimated-amount-to min-amount :min-amount max-amount :max-amount getting-offer :getting-offer fe646 08eb7 c63f7 ae0d3 d0b0c 341da children".split(" "))})},
get Port(){return W},calibrate:function({"query:offers-aggregator":a}){const b={};a&&(b.g=a);return b}},ba,Z.prototype={constructor(){this.land={o:null}}},Z.prototype={calibrate:async function({g:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:f},asIElement:{fqn:g}}=this,h=await b(a);if(!h)return console.warn("\\u2757\\ufe0f offersAggregatorSel %s must be present on the page for %s to work",a,g),{};f.o=h}},Z.prototype={solder:(a,{g:b})=>({g:b})}];
Y[d]=[S,{rootId:"DealBrokerAggregatorAdapter",__$id:1096427877,fqn:"xyz.swapee.wc.IDealBrokerAggregatorAdapter",maurice_element_v3:!0}];class za extends Y.implements(T,B,qa,z,{solder:ja,server:la,render:na},{classesMap:!0,rootSelector:".DealBrokerAggregatorAdapter",stylesheet:"html/styles/DealBrokerAggregatorAdapter.css",blockName:"html/DealBrokerAggregatorAdapterBlock.html"}){};module.exports["10964278770"]=S;module.exports["10964278771"]=S;module.exports["10964278773"]=M;module.exports["10964278774"]=P;module.exports["10964278778"]=za;module.exports["109642787711"]=H;module.exports["109642787730"]=G;module.exports["109642787761"]=T;
/*! @embed-object-end {1096427877} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule