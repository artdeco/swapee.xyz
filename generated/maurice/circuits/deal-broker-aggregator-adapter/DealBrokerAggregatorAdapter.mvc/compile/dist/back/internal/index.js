import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter}*/
export class AbstractDealBrokerAggregatorAdapter extends Module['10964278771'] {}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter} */
AbstractDealBrokerAggregatorAdapter.class=function(){}
/** @type {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort} */
export const DealBrokerAggregatorAdapterPort=Module['10964278773']
/**@extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController}*/
export class AbstractDealBrokerAggregatorAdapterController extends Module['10964278774'] {}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController} */
AbstractDealBrokerAggregatorAdapterController.class=function(){}
/** @type {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent} */
export const DealBrokerAggregatorAdapterHtmlComponent=Module['109642787710']
/** @type {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterBuffer} */
export const DealBrokerAggregatorAdapterBuffer=Module['109642787711']
/**@extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer}*/
export class AbstractDealBrokerAggregatorAdapterComputer extends Module['109642787730'] {}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer} */
AbstractDealBrokerAggregatorAdapterComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer} */
export const DealBrokerAggregatorAdapterComputer=Module['109642787731']
/** @type {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController} */
export const DealBrokerAggregatorAdapterController=Module['109642787761']