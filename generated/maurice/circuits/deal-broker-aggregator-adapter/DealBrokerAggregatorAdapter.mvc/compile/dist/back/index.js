/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapter` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter}
 */
class AbstractDealBrokerAggregatorAdapter extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IDealBrokerAggregatorAdapter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterPort}
 */
class DealBrokerAggregatorAdapterPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterController` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController}
 */
class AbstractDealBrokerAggregatorAdapterController extends (class {/* lazy-loaded */}) {}
/**
 * The _IDealBrokerAggregatorAdapter_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent}
 */
class DealBrokerAggregatorAdapterHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterBuffer}
 */
class DealBrokerAggregatorAdapterBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer}
 */
class AbstractDealBrokerAggregatorAdapterComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterComputer}
 */
class DealBrokerAggregatorAdapterComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.DealBrokerAggregatorAdapterController}
 */
class DealBrokerAggregatorAdapterController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractDealBrokerAggregatorAdapter = AbstractDealBrokerAggregatorAdapter
module.exports.DealBrokerAggregatorAdapterPort = DealBrokerAggregatorAdapterPort
module.exports.AbstractDealBrokerAggregatorAdapterController = AbstractDealBrokerAggregatorAdapterController
module.exports.DealBrokerAggregatorAdapterHtmlComponent = DealBrokerAggregatorAdapterHtmlComponent
module.exports.DealBrokerAggregatorAdapterBuffer = DealBrokerAggregatorAdapterBuffer
module.exports.AbstractDealBrokerAggregatorAdapterComputer = AbstractDealBrokerAggregatorAdapterComputer
module.exports.DealBrokerAggregatorAdapterComputer = DealBrokerAggregatorAdapterComputer
module.exports.DealBrokerAggregatorAdapterController = DealBrokerAggregatorAdapterController