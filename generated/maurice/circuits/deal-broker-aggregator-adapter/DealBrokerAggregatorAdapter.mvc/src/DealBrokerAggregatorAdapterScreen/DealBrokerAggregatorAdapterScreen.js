import AbstractDealBrokerAggregatorAdapterControllerAT from '../../gen/AbstractDealBrokerAggregatorAdapterControllerAT'
import DealBrokerAggregatorAdapterDisplay from '../DealBrokerAggregatorAdapterDisplay'
import AbstractDealBrokerAggregatorAdapterScreen from '../../gen/AbstractDealBrokerAggregatorAdapterScreen'

/** @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterScreen} */
export default class extends AbstractDealBrokerAggregatorAdapterScreen.implements(
 AbstractDealBrokerAggregatorAdapterControllerAT,
 DealBrokerAggregatorAdapterDisplay,
 /**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen} */ ({
  __$id:1096427877,
 }),
){}