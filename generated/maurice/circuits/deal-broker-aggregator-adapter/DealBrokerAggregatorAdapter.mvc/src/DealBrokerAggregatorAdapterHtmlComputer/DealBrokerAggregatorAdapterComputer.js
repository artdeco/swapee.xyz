import adaptTest from './methods/adapt-test'
import {preadaptTest} from '../../gen/AbstractDealBrokerAggregatorAdapterComputer/preadapters'
import AbstractDealBrokerAggregatorAdapterComputer from '../../gen/AbstractDealBrokerAggregatorAdapterComputer'

/** @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterComputer} */
export default class DealBrokerAggregatorAdapterHtmlComputer extends AbstractDealBrokerAggregatorAdapterComputer.implements(
 /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer} */ ({
  adaptTest:adaptTest,
  adapt:[preadaptTest],
 }),
){}