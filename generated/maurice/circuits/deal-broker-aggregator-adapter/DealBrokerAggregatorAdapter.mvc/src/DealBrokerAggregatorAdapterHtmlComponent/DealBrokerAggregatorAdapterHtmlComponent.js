import DealBrokerAggregatorAdapterHtmlController from '../DealBrokerAggregatorAdapterHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractDealBrokerAggregatorAdapterHtmlComponent} from '../../gen/AbstractDealBrokerAggregatorAdapterHtmlComponent'

/** @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent} */
export default class extends AbstractDealBrokerAggregatorAdapterHtmlComponent.implements(
 DealBrokerAggregatorAdapterHtmlController,
 IntegratedComponentInitialiser,
/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent}*/({}),
){}