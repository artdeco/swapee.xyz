/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._short} */
export default function short(_,{
 This:This,
},{OffersAggregator:{
 estimatedOut:estimatedOut,loadingEstimate:loadingEstimate,
 minError:minAmount,maxError:maxError,
}}) {
 return (<>
  <This estimatedAmountTo={estimatedOut} gettingOffer={loadingEstimate}
   minAmount={minAmount} maxAmount={maxError} />
 </>)
}