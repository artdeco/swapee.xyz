import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import DealBrokerAggregatorAdapterServerController from '../DealBrokerAggregatorAdapterServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractDealBrokerAggregatorAdapterElement from '../../gen/AbstractDealBrokerAggregatorAdapterElement'

/** @extends {xyz.swapee.wc.DealBrokerAggregatorAdapterElement} */
export default class DealBrokerAggregatorAdapterElement extends AbstractDealBrokerAggregatorAdapterElement.implements(
 DealBrokerAggregatorAdapterServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement}*/({
   classesMap: true,
   rootSelector:     `.DealBrokerAggregatorAdapter`,
   stylesheet:       'html/styles/DealBrokerAggregatorAdapter.css',
   blockName:        'html/DealBrokerAggregatorAdapterBlock.html',
  }),
){}

// thank you for using web circuits
