/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.compute={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.EstimatedAmountTo={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MinAmount={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MaxAmount={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.GettingOffer={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterCore={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController={}
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT={}
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR={}
xyz.swapee.wc.IDealBrokerAggregatorAdapter={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.communicator={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.relay={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay={}
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController={}
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR={}
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen={}
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterController={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen={}
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/02-IDealBrokerAggregatorAdapterComputer.xml}  c6deddba7ebee8800ff3b9d85fff5bfd */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer} xyz.swapee.wc.DealBrokerAggregatorAdapterComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer} xyz.swapee.wc.DealBrokerAggregatorAdapterComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.DealBrokerAggregatorAdapterLand>)} xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.compute} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer} xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterComputer_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterComputer
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterComputer = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterComputer.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterComputer

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterComputer */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterComputer} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterComputer */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterComputer_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterComputerCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputerCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterComputer_ instance into the _BoundIDealBrokerAggregatorAdapterComputer_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterComputer}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputerCaster.prototype.asIDealBrokerAggregatorAdapterComputer
/**
 * Access the _DealBrokerAggregatorAdapterComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterComputer}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputerCaster.prototype.superDealBrokerAggregatorAdapterComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, land: !xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.compute.Land) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.__compute<!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer>} xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} mem The memory.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.compute.Land} land The land.
 * - `OffersAggregator` _!xyz.swapee.wc.OffersAggregatorMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.OffersAggregatorMemory} OffersAggregator `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/03-IDealBrokerAggregatorAdapterOuterCore.xml}  bf27fcb09e013be3988e75ac676f2ad5 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore} xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreCaster)} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.constructor */
/**
 * The _IDealBrokerAggregatorAdapter_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterOuterCore_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore} The _IDealBrokerAggregatorAdapter_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterOuterCore.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreFields = class { }
/**
 * The _IDealBrokerAggregatorAdapter_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterOuterCore

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterOuterCore */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterOuterCore */

/**
 *   Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.EstimatedAmountTo.estimatedAmountTo

/**
 * The minimum required amount extracted from the error.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MinAmount.minAmount

/**
 * The maximum amount that can be exchanged extracted from the error.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MaxAmount.maxAmount

/**
 * Whether loading either fixed or floating rate offer.
 * @typedef {boolean}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.GettingOffer.gettingOffer

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.EstimatedAmountTo&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MinAmount&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MaxAmount&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.GettingOffer} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model The _IDealBrokerAggregatorAdapter_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel The _IDealBrokerAggregatorAdapter_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterOuterCore_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterOuterCore_ instance into the _BoundIDealBrokerAggregatorAdapterOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterOuterCore}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreCaster.prototype.asIDealBrokerAggregatorAdapterOuterCore
/**
 * Access the _DealBrokerAggregatorAdapterOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterOuterCore}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCoreCaster.prototype.superDealBrokerAggregatorAdapterOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.EstimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (optional overlay).
 * @prop {string} [estimatedAmountTo=null] Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.EstimatedAmountTo_Safe Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (required overlay).
 * @prop {string} estimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MinAmount The minimum required amount extracted from the error (optional overlay).
 * @prop {string} [minAmount=""] The minimum required amount extracted from the error. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MinAmount_Safe The minimum required amount extracted from the error (required overlay).
 * @prop {string} minAmount The minimum required amount extracted from the error.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay).
 * @prop {string} [maxAmount=""] The maximum amount that can be exchanged extracted from the error. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay).
 * @prop {string} maxAmount The maximum amount that can be exchanged extracted from the error.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.GettingOffer Whether loading either fixed or floating rate offer (optional overlay).
 * @prop {boolean} [gettingOffer=false] Whether loading either fixed or floating rate offer. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay).
 * @prop {boolean} gettingOffer Whether loading either fixed or floating rate offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (optional overlay).
 * @prop {*} [estimatedAmountTo=null] Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo_Safe Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (required overlay).
 * @prop {*} estimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount The minimum required amount extracted from the error (optional overlay).
 * @prop {*} [minAmount=null] The minimum required amount extracted from the error. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount_Safe The minimum required amount extracted from the error (required overlay).
 * @prop {*} minAmount The minimum required amount extracted from the error.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay).
 * @prop {*} [maxAmount=null] The maximum amount that can be exchanged extracted from the error. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay).
 * @prop {*} maxAmount The maximum amount that can be exchanged extracted from the error.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer Whether loading either fixed or floating rate offer (optional overlay).
 * @prop {*} [gettingOffer=null] Whether loading either fixed or floating rate offer. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay).
 * @prop {*} gettingOffer Whether loading either fixed or floating rate offer.
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.EstimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.EstimatedAmountTo_Safe Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.MinAmount The minimum required amount extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.MinAmount_Safe The minimum required amount extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.GettingOffer Whether loading either fixed or floating rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.EstimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.EstimatedAmountTo_Safe Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.MinAmount The minimum required amount extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.MinAmount_Safe The minimum required amount extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.GettingOffer Whether loading either fixed or floating rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.EstimatedAmountTo} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.EstimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.EstimatedAmountTo_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.EstimatedAmountTo_Safe Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MinAmount} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.MinAmount The minimum required amount extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MinAmount_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.MinAmount_Safe The minimum required amount extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MaxAmount} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.MaxAmount_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.GettingOffer} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.GettingOffer Whether loading either fixed or floating rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model.GettingOffer_Safe} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/04-IDealBrokerAggregatorAdapterPort.xml}  f6f1931e8ee706b43dc3111baa04f030 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterPort)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort} xyz.swapee.wc.DealBrokerAggregatorAdapterPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterPort` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterPort} xyz.swapee.wc.DealBrokerAggregatorAdapterPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterPortFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs>)} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IDealBrokerAggregatorAdapter_, providing input
 * pins.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterPort
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.resetPort} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.resetDealBrokerAggregatorAdapterPort} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.prototype.resetDealBrokerAggregatorAdapterPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterPort&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterPort} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterPort_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterPort
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort} The port that serves as an interface to the _IDealBrokerAggregatorAdapter_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterPort = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterPort.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterPort}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterPort.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterPort.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterPortFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortFields = class { }
/**
 * The inputs to the _IDealBrokerAggregatorAdapter_'s controller via its port.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortFields.prototype.props = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterPort

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterPort */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterPort} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterPort */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel)} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel} xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IDealBrokerAggregatorAdapter_'s controller via its port.
 * @record xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel)} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.constructor */
/**
 * The inputs to the _IDealBrokerAggregatorAdapter_'s controller via its port.
 * @record xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface

/**
 * A concrete class of _IDealBrokerAggregatorAdapterPortInterface_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterPortInterface
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface} The port interface.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterPortInterface = class extends xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface { }
xyz.swapee.wc.DealBrokerAggregatorAdapterPortInterface.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface.Props
 * @prop {string} minAmount The minimum required amount extracted from the error.
 * @prop {string} maxAmount The maximum amount that can be exchanged extracted from the error.
 * @prop {boolean} gettingOffer Whether loading either fixed or floating rate offer.
 * @prop {string} [estimatedAmountTo=null] Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change. Default `null`.
 */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterPort_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterPortCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterPort_ instance into the _BoundIDealBrokerAggregatorAdapterPort_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterPort}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortCaster.prototype.asIDealBrokerAggregatorAdapterPort
/**
 * Access the _DealBrokerAggregatorAdapterPort_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterPort}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPortCaster.prototype.superDealBrokerAggregatorAdapterPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.__resetPort<!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort>} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.resetPort} */
/**
 * Resets the _IDealBrokerAggregatorAdapter_ port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.__resetDealBrokerAggregatorAdapterPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.__resetDealBrokerAggregatorAdapterPort<!xyz.swapee.wc.IDealBrokerAggregatorAdapterPort>} xyz.swapee.wc.IDealBrokerAggregatorAdapterPort._resetDealBrokerAggregatorAdapterPort */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.resetDealBrokerAggregatorAdapterPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.resetDealBrokerAggregatorAdapterPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerAggregatorAdapterPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/09-IDealBrokerAggregatorAdapterCore.xml}  23a2228611bb2e9ae4f518a823cfbf8e */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterCore)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore} xyz.swapee.wc.DealBrokerAggregatorAdapterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterCore` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreCaster&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore)} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterCore
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCore = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.resetCore} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.resetDealBrokerAggregatorAdapterCore} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.prototype.resetDealBrokerAggregatorAdapterCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterCore} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterCore_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterCore
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterCore = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterCore.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerAggregatorAdapterCore.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterCore.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreFields = class { }
/**
 * The _IDealBrokerAggregatorAdapter_'s memory.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreFields.prototype.props = /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterCore

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterCore */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterCore} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterCore */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.Model The _IDealBrokerAggregatorAdapter_'s memory. */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterCore_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterCore_ instance into the _BoundIDealBrokerAggregatorAdapterCore_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterCore}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreCaster.prototype.asIDealBrokerAggregatorAdapterCore
/**
 * Access the _DealBrokerAggregatorAdapterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterCore}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCoreCaster.prototype.superDealBrokerAggregatorAdapterCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.__resetCore<!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore>} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.resetCore} */
/**
 * Resets the _IDealBrokerAggregatorAdapter_ core.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.__resetDealBrokerAggregatorAdapterCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.__resetDealBrokerAggregatorAdapterCore<!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore>} xyz.swapee.wc.IDealBrokerAggregatorAdapterCore._resetDealBrokerAggregatorAdapterCore */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.resetDealBrokerAggregatorAdapterCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.resetDealBrokerAggregatorAdapterCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerAggregatorAdapterCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/10-IDealBrokerAggregatorAdapterProcessor.xml}  d70232aac1408b838abaab5d218912a3 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor} xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterCore|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterCore)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor} xyz.swapee.wc.DealBrokerAggregatorAdapterProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessorCaster&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer&xyz.swapee.wc.IDealBrokerAggregatorAdapterCore&xyz.swapee.wc.IDealBrokerAggregatorAdapterController)} xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.typeof */
/**
 * The processor to compute changes to the memory for the _IDealBrokerAggregatorAdapter_.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterCore.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor} xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterProcessor_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor} The processor to compute changes to the memory for the _IDealBrokerAggregatorAdapter_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterProcessor

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterProcessor */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterProcessor */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterProcessor_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessorCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessorCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterProcessor_ instance into the _BoundIDealBrokerAggregatorAdapterProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessorCaster.prototype.asIDealBrokerAggregatorAdapterProcessor
/**
 * Access the _DealBrokerAggregatorAdapterProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessorCaster.prototype.superDealBrokerAggregatorAdapterProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/100-DealBrokerAggregatorAdapterMemory.xml}  536db1ff1d60611cfe82d7ad6db0ae7d */
/**
 * The memory of the _IDealBrokerAggregatorAdapter_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.DealBrokerAggregatorAdapterMemory
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemory = class { }
/**
 * Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change. Default empty string.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemory.prototype.estimatedAmountTo = /** @type {string} */ (void 0)
/**
 * The minimum required amount extracted from the error. Default empty string.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemory.prototype.minAmount = /** @type {string} */ (void 0)
/**
 * The maximum amount that can be exchanged extracted from the error. Default empty string.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemory.prototype.maxAmount = /** @type {string} */ (void 0)
/**
 * Whether loading either fixed or floating rate offer. Default `false`.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemory.prototype.gettingOffer = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/102-DealBrokerAggregatorAdapterInputs.xml}  4ef444edb9a52cd7f2259005f7caf4fe */
/**
 * The inputs of the _IDealBrokerAggregatorAdapter_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs = class { }
/**
 * Either fixed-rate or floating-rate amount that the user can get. After the
 * actual transaction is created, this value may change. Default `null`.
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs.prototype.estimatedAmountTo = /** @type {string|undefined} */ (void 0)
/**
 * The minimum required amount extracted from the error. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs.prototype.minAmount = /** @type {string|undefined} */ (void 0)
/**
 * The maximum amount that can be exchanged extracted from the error. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs.prototype.maxAmount = /** @type {string|undefined} */ (void 0)
/**
 * Whether loading either fixed or floating rate offer. Default `false`.
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs.prototype.gettingOffer = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/11-IDealBrokerAggregatorAdapter.xml}  b301b8726244ebc0a790cc538fd691d7 */
/**
 * An atomic wrapper for the _IDealBrokerAggregatorAdapter_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.DealBrokerAggregatorAdapterEnv
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterEnv.prototype.dealBrokerAggregatorAdapter = /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapter} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs>&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapter)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapter} xyz.swapee.wc.DealBrokerAggregatorAdapter.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapter` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapter.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapter|typeof xyz.swapee.wc.DealBrokerAggregatorAdapter)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapter}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapter}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapter|typeof xyz.swapee.wc.DealBrokerAggregatorAdapter)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapter}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapter|typeof xyz.swapee.wc.DealBrokerAggregatorAdapter)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapter}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapter} xyz.swapee.wc.DealBrokerAggregatorAdapterConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapter.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.DealBrokerAggregatorAdapterClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterCaster&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer&xyz.swapee.wc.IDealBrokerAggregatorAdapterController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs, !xyz.swapee.wc.DealBrokerAggregatorAdapterLand>)} xyz.swapee.wc.IDealBrokerAggregatorAdapter.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor} xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapter
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapter = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapter.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapter* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapter.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapter&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapter.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapter} xyz.swapee.wc.IDealBrokerAggregatorAdapter.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapter_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapter
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapter} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapter = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapter.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapter.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapter* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapter* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapter.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapter}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapter.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapter.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterFields = class { }
/**
 * The input pins of the _IDealBrokerAggregatorAdapter_ port.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterFields.prototype.pinout = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapter} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapter

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapter} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapter */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapter} xyz.swapee.wc.BoundDealBrokerAggregatorAdapter */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs} xyz.swapee.wc.IDealBrokerAggregatorAdapter.Pinout The input pins of the _IDealBrokerAggregatorAdapter_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs>)} xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer

/**
 * A concrete class of _IDealBrokerAggregatorAdapterBuffer_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterBuffer
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterBuffer = class extends xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer { }
xyz.swapee.wc.DealBrokerAggregatorAdapterBuffer.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterBuffer

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapter_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapter_ instance into the _BoundIDealBrokerAggregatorAdapter_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapter}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCaster.prototype.asIDealBrokerAggregatorAdapter
/**
 * Access the _DealBrokerAggregatorAdapter_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapter}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterCaster.prototype.superDealBrokerAggregatorAdapter

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/110-DealBrokerAggregatorAdapterSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs = class {
  constructor() {
    /**
     * `c63f7`
     */
    this.estimatedAmountTo=/** @type {string} */ (void 0)
    /**
     * `ae0d3`
     */
    this.minAmount=/** @type {string} */ (void 0)
    /**
     * `d0b0c`
     */
    this.maxAmount=/** @type {string} */ (void 0)
    /**
     * `d41da`
     */
    this.gettingOffer=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryQPs
 * @dict
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryQPs = class { }
/**
 * `estimatedAmountTo`
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryQPs.prototype.c63f7 = /** @type {string} */ (void 0)
/**
 * `minAmount`
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryQPs.prototype.ae0d3 = /** @type {string} */ (void 0)
/**
 * `maxAmount`
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryQPs.prototype.d0b0c = /** @type {string} */ (void 0)
/**
 * `gettingOffer`
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryQPs.prototype.d41da = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs)} xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs} xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs)} xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs
 * @dict
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.DealBrokerAggregatorAdapterVdusPQs
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterVdusPQs = class { }
xyz.swapee.wc.DealBrokerAggregatorAdapterVdusPQs.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterVdusQPs
 * @dict
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterVdusQPs = class { }
xyz.swapee.wc.DealBrokerAggregatorAdapterVdusQPs.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/12-IDealBrokerAggregatorAdapterHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtilFields)} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.router} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IDealBrokerAggregatorAdapterHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponentUtil
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponentUtil = class extends xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil { }
xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponentUtil

/**
 * Fields of the IDealBrokerAggregatorAdapterHtmlComponentUtil.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtilFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponentUtil} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorPort} OffersAggregator
 * @prop {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterPort} DealBrokerAggregatorAdapter The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.OffersAggregatorMemory} OffersAggregator
 * @prop {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} DealBrokerAggregatorAdapter
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.IOffersAggregator.Pinout} OffersAggregator
 * @prop {!xyz.swapee.wc.IDealBrokerAggregatorAdapter.Pinout} DealBrokerAggregatorAdapter
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.__router<!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil>} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `OffersAggregator` _typeof IOffersAggregatorPort_
 * - `DealBrokerAggregatorAdapter` _typeof IDealBrokerAggregatorAdapterPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `OffersAggregator` _!OffersAggregatorMemory_
 * - `DealBrokerAggregatorAdapter` _!DealBrokerAggregatorAdapterMemory_
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `OffersAggregator` _!IOffersAggregator.Pinout_
 * - `DealBrokerAggregatorAdapter` _!IDealBrokerAggregatorAdapter.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/12-IDealBrokerAggregatorAdapterHtmlComponent.xml}  f8f2d27d866437bbd54f997d347587d9 */
/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapter.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent} xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapter|typeof xyz.swapee.wc.DealBrokerAggregatorAdapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapter|typeof xyz.swapee.wc.DealBrokerAggregatorAdapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapter|typeof xyz.swapee.wc.DealBrokerAggregatorAdapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterProcessor)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent} xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentCaster&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen&xyz.swapee.wc.IDealBrokerAggregatorAdapter&com.webcircuits.ILanded<!xyz.swapee.wc.DealBrokerAggregatorAdapterLand>&xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs, !HTMLDivElement, !xyz.swapee.wc.DealBrokerAggregatorAdapterLand>&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer)} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU} xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IDealBrokerAggregatorAdapter_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.typeof&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapter.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent} xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent} The _IDealBrokerAggregatorAdapter_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterHtmlComponent

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterHtmlComponent */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterHtmlComponent} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterHtmlComponent */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterHtmlComponent_ instance into the _BoundIDealBrokerAggregatorAdapterHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterHtmlComponent}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentCaster.prototype.asIDealBrokerAggregatorAdapterHtmlComponent
/**
 * Access the _DealBrokerAggregatorAdapterHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterHtmlComponent}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentCaster.prototype.superDealBrokerAggregatorAdapterHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/130-IDealBrokerAggregatorAdapterElement.xml}  919a9a8e30426d18afc0b831486059b3 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.DealBrokerAggregatorAdapterLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.IOffersAggregator>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterElement)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement} xyz.swapee.wc.DealBrokerAggregatorAdapterElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterElement` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterElement} xyz.swapee.wc.DealBrokerAggregatorAdapterElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterElementFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs, !xyz.swapee.wc.DealBrokerAggregatorAdapterLand>&com.webcircuits.ILanded<!xyz.swapee.wc.DealBrokerAggregatorAdapterLand>&guest.maurice.IMilleu<!xyz.swapee.wc.IOffersAggregator>)} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IDealBrokerAggregatorAdapter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterElement
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.solder} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.render} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.buildOffersAggregator} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.buildOffersAggregator = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.server} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.inducer} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterElement&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterElement_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterElement
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement} A component description.
 *
 * The _IDealBrokerAggregatorAdapter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterElement = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterElement.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElement}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterElement.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterElement.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterElementFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementFields = class { }
/**
 * The element-specific inputs to the _IDealBrokerAggregatorAdapter_ component.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterElement

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterElement */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterElement} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterElement */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs&xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Queries&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs The element-specific inputs to the _IDealBrokerAggregatorAdapter_ component. */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterElement_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterElementCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterElement_ instance into the _BoundIDealBrokerAggregatorAdapterElement_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterElement}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementCaster.prototype.asIDealBrokerAggregatorAdapterElement
/**
 * Access the _DealBrokerAggregatorAdapterElement_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterElement}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementCaster.prototype.superDealBrokerAggregatorAdapterElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, props: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs) => Object<string, *>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__solder<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._solder */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} model The model.
 * - `estimatedAmountTo` _string_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 * - `minAmount` _string_ The minimum required amount extracted from the error. Default empty string.
 * - `maxAmount` _string_ The maximum amount that can be exchanged extracted from the error. Default empty string.
 * - `gettingOffer` _boolean_ Whether loading either fixed or floating rate offer. Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs} props The element props.
 * - `[estimatedAmountTo=null]` _&#42;?_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo* Default `null`.
 * - `[minAmount=null]` _&#42;?_ The minimum required amount extracted from the error. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount* Default `null`.
 * - `[maxAmount=null]` _&#42;?_ The maximum amount that can be exchanged extracted from the error. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount* Default `null`.
 * - `[gettingOffer=null]` _&#42;?_ Whether loading either fixed or floating rate offer. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer* Default `null`.
 * - `[offersAggregatorSel=""]` _string?_ The query to discover the _OffersAggregator_ VDU. ⤴ *IDealBrokerAggregatorAdapterDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder* Default `false`.
 * - `[offersAggregatorOpts]` _!Object?_ The options to pass to the _OffersAggregator_ vdu. ⤴ *IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, instance?: !xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen&xyz.swapee.wc.IDealBrokerAggregatorAdapterController) => !engineering.type.VNode} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__render<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._render */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} [model] The model for the view.
 * - `estimatedAmountTo` _string_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 * - `minAmount` _string_ The minimum required amount extracted from the error. Default empty string.
 * - `maxAmount` _string_ The maximum amount that can be exchanged extracted from the error. Default empty string.
 * - `gettingOffer` _boolean_ Whether loading either fixed or floating rate offer. Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen&xyz.swapee.wc.IDealBrokerAggregatorAdapterController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build.Cores, instances: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build.Instances) => ?} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__build<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._build */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build.Cores} cores The models of components on the land.
 * - `OffersAggregator` _!xyz.swapee.wc.IOffersAggregatorCore.Model_
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `OffersAggregator` _!xyz.swapee.wc.IOffersAggregator_
 * @return {?}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.IOffersAggregatorCore.Model} OffersAggregator
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.IOffersAggregator} OffersAggregator
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IOffersAggregatorCore.Model, instance: !xyz.swapee.wc.IOffersAggregator) => ?} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__buildOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__buildOffersAggregator<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._buildOffersAggregator */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.buildOffersAggregator} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IOffersAggregator_ component.
 * @param {!xyz.swapee.wc.IOffersAggregatorCore.Model} model
 * @param {!xyz.swapee.wc.IOffersAggregator} instance
 * @return {?}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.buildOffersAggregator = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, ports: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short.Ports, cores: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short.Cores) => ?} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__short<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._short */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} model The model from which to feed properties to peer's ports.
 * - `estimatedAmountTo` _string_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 * - `minAmount` _string_ The minimum required amount extracted from the error. Default empty string.
 * - `maxAmount` _string_ The maximum amount that can be exchanged extracted from the error. Default empty string.
 * - `gettingOffer` _boolean_ Whether loading either fixed or floating rate offer. Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short.Ports} ports The ports of the peers.
 * - `OffersAggregator` _typeof xyz.swapee.wc.IOffersAggregatorPortInterface_
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short.Cores} cores The cores of the peers.
 * - `OffersAggregator` _xyz.swapee.wc.IOffersAggregatorCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorPortInterface} OffersAggregator
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.IOffersAggregatorCore.Model} OffersAggregator
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, inputs: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__server<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._server */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} memory The memory registers.
 * - `estimatedAmountTo` _string_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 * - `minAmount` _string_ The minimum required amount extracted from the error. Default empty string.
 * - `maxAmount` _string_ The maximum amount that can be exchanged extracted from the error. Default empty string.
 * - `gettingOffer` _boolean_ Whether loading either fixed or floating rate offer. Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs} inputs The inputs to the port.
 * - `[estimatedAmountTo=null]` _&#42;?_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo* Default `null`.
 * - `[minAmount=null]` _&#42;?_ The minimum required amount extracted from the error. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount* Default `null`.
 * - `[maxAmount=null]` _&#42;?_ The maximum amount that can be exchanged extracted from the error. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount* Default `null`.
 * - `[gettingOffer=null]` _&#42;?_ Whether loading either fixed or floating rate offer. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer* Default `null`.
 * - `[offersAggregatorSel=""]` _string?_ The query to discover the _OffersAggregator_ VDU. ⤴ *IDealBrokerAggregatorAdapterDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder* Default `false`.
 * - `[offersAggregatorOpts]` _!Object?_ The options to pass to the _OffersAggregator_ vdu. ⤴ *IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, port?: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs) => ?} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.__inducer<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement>} xyz.swapee.wc.IDealBrokerAggregatorAdapterElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} [model] The model of the component into which to induce the state.
 * - `estimatedAmountTo` _string_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 * - `minAmount` _string_ The minimum required amount extracted from the error. Default empty string.
 * - `maxAmount` _string_ The maximum amount that can be exchanged extracted from the error. Default empty string.
 * - `gettingOffer` _boolean_ Whether loading either fixed or floating rate offer. Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[estimatedAmountTo=null]` _&#42;?_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.EstimatedAmountTo* Default `null`.
 * - `[minAmount=null]` _&#42;?_ The minimum required amount extracted from the error. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MinAmount* Default `null`.
 * - `[maxAmount=null]` _&#42;?_ The maximum amount that can be exchanged extracted from the error. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.MaxAmount* Default `null`.
 * - `[gettingOffer=null]` _&#42;?_ Whether loading either fixed or floating rate offer. ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer* ⤴ *IDealBrokerAggregatorAdapterOuterCore.WeakModel.GettingOffer* Default `null`.
 * - `[offersAggregatorSel=""]` _string?_ The query to discover the _OffersAggregator_ VDU. ⤴ *IDealBrokerAggregatorAdapterDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder* Default `false`.
 * - `[offersAggregatorOpts]` _!Object?_ The options to pass to the _OffersAggregator_ vdu. ⤴ *IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerAggregatorAdapterElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/140-IDealBrokerAggregatorAdapterElementPort.xml}  97460f89fa65b01dc77f0b8e5f63c880 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort} xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort} xyz.swapee.wc.DealBrokerAggregatorAdapterElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs>)} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterElementPort_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterElementPort.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortFields = class { }
/**
 * The inputs to the _IDealBrokerAggregatorAdapterElement_'s controller via its element port.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterElementPort

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterElementPort */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterElementPort} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _OffersAggregator_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts.offersAggregatorOpts

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts)} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts.typeof */
/**
 * The inputs to the _IDealBrokerAggregatorAdapterElement_'s controller via its element port.
 * @record xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.OffersAggregatorOpts)} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.OffersAggregatorOpts} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.OffersAggregatorOpts.typeof */
/**
 * The inputs to the _IDealBrokerAggregatorAdapterElement_'s controller via its element port.
 * @record xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.OffersAggregatorOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterElementPort_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterElementPort_ instance into the _BoundIDealBrokerAggregatorAdapterElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterElementPort}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortCaster.prototype.asIDealBrokerAggregatorAdapterElementPort
/**
 * Access the _DealBrokerAggregatorAdapterElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterElementPort}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPortCaster.prototype.superDealBrokerAggregatorAdapterElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts The options to pass to the _OffersAggregator_ vdu (optional overlay).
 * @prop {!Object} [offersAggregatorOpts] The options to pass to the _OffersAggregator_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.Inputs.OffersAggregatorOpts_Safe The options to pass to the _OffersAggregator_ vdu (required overlay).
 * @prop {!Object} offersAggregatorOpts The options to pass to the _OffersAggregator_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.OffersAggregatorOpts The options to pass to the _OffersAggregator_ vdu (optional overlay).
 * @prop {*} [offersAggregatorOpts=null] The options to pass to the _OffersAggregator_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort.WeakInputs.OffersAggregatorOpts_Safe The options to pass to the _OffersAggregator_ vdu (required overlay).
 * @prop {*} offersAggregatorOpts The options to pass to the _OffersAggregator_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/170-IDealBrokerAggregatorAdapterDesigner.xml}  c3d7b489de414a674cd1d7dfd1743bfc */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.DealBrokerAggregatorAdapterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IDealBrokerAggregatorAdapter />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.DealBrokerAggregatorAdapterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IDealBrokerAggregatorAdapter />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.DealBrokerAggregatorAdapterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `OffersAggregator` _typeof xyz.swapee.wc.IOffersAggregatorController_
   * - `DealBrokerAggregatorAdapter` _typeof IDealBrokerAggregatorAdapterController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `OffersAggregator` _typeof xyz.swapee.wc.IOffersAggregatorController_
   * - `DealBrokerAggregatorAdapter` _typeof IDealBrokerAggregatorAdapterController_
   * - `This` _typeof IDealBrokerAggregatorAdapterController_
   * @param {!xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `OffersAggregator` _!xyz.swapee.wc.OffersAggregatorMemory_
   * - `DealBrokerAggregatorAdapter` _!DealBrokerAggregatorAdapterMemory_
   * - `This` _!DealBrokerAggregatorAdapterMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.DealBrokerAggregatorAdapterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.DealBrokerAggregatorAdapterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.prototype.constructor = xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner

/**
 * A concrete class of _IDealBrokerAggregatorAdapterDesigner_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterDesigner
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterDesigner = class extends xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner { }
xyz.swapee.wc.DealBrokerAggregatorAdapterDesigner.prototype.constructor = xyz.swapee.wc.DealBrokerAggregatorAdapterDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorController} OffersAggregator
 * @prop {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController} DealBrokerAggregatorAdapter
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IOffersAggregatorController} OffersAggregator
 * @prop {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController} DealBrokerAggregatorAdapter
 * @prop {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.OffersAggregatorMemory} OffersAggregator
 * @prop {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} DealBrokerAggregatorAdapter
 * @prop {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/200-DealBrokerAggregatorAdapterLand.xml}  7d0c843b84c9f14dfdf68f6d96e24e94 */
/**
 * The surrounding of the _IDealBrokerAggregatorAdapter_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.DealBrokerAggregatorAdapterLand
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterLand = class { }
/**
 *
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterLand.prototype.OffersAggregator = /** @type {xyz.swapee.wc.IOffersAggregator} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/40-IDealBrokerAggregatorAdapterDisplay.xml}  5cbc8310ff4f87483bca9a730d43e048 */
/**
 * @typedef {Object} $xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese
 * @prop {HTMLElement} [OffersAggregator] The via for the _OffersAggregator_ peer.
 */
/** @typedef {$xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Settings>} xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.DealBrokerAggregatorAdapterDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !HTMLDivElement, !xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Settings, xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Queries, null>)} xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IDealBrokerAggregatorAdapter_.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.paint} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterDisplay_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay} Display for presenting information from the _IDealBrokerAggregatorAdapter_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterDisplay.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Queries} */ (void 0)
/**
 * The via for the _OffersAggregator_ peer. Default `null`.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayFields.prototype.OffersAggregator = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterDisplay

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterDisplay */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterDisplay */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Queries} xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [offersAggregatorSel=""] The query to discover the _OffersAggregator_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterDisplay_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterDisplay_ instance into the _BoundIDealBrokerAggregatorAdapterDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayCaster.prototype.asIDealBrokerAggregatorAdapterDisplay
/**
 * Cast the _IDealBrokerAggregatorAdapterDisplay_ instance into the _BoundIDealBrokerAggregatorAdapterScreen_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayCaster.prototype.asIDealBrokerAggregatorAdapterScreen
/**
 * Access the _DealBrokerAggregatorAdapterDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplayCaster.prototype.superDealBrokerAggregatorAdapterDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, land: null) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.__paint<!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay>} xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} memory The display data.
 * - `estimatedAmountTo` _string_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 * - `minAmount` _string_ The minimum required amount extracted from the error. Default empty string.
 * - `maxAmount` _string_ The maximum amount that can be exchanged extracted from the error. Default empty string.
 * - `gettingOffer` _boolean_ Whether loading either fixed or floating rate offer. Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/40-IDealBrokerAggregatorAdapterDisplayBack.xml}  53e6a81bebc1c47026961247c15e96ca */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [OffersAggregator] The via for the _OffersAggregator_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.DealBrokerAggregatorAdapterClasses>} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay)} xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.constructor&xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.DealBrokerAggregatorAdapterClasses, !xyz.swapee.wc.DealBrokerAggregatorAdapterLand>)} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay = class extends /** @type {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.paint} */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.Initialese>)} xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterDisplay_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay
 * @implements {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay = class extends /** @type {xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay.constructor&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay.prototype.constructor = xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterDisplay.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayFields
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayFields = class { }
/**
 * The via for the _OffersAggregator_ peer.
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayFields.prototype.OffersAggregator = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay} */
xyz.swapee.wc.back.RecordIDealBrokerAggregatorAdapterDisplay

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterDisplay */

/** @typedef {xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay} xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterDisplay */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterDisplay_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayCaster
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterDisplay_ instance into the _BoundIDealBrokerAggregatorAdapterDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayCaster.prototype.asIDealBrokerAggregatorAdapterDisplay
/**
 * Access the _DealBrokerAggregatorAdapterDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterDisplay}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplayCaster.prototype.superDealBrokerAggregatorAdapterDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, land?: !xyz.swapee.wc.DealBrokerAggregatorAdapterLand) => void} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.__paint<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay>} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory} [memory] The display data.
 * - `estimatedAmountTo` _string_ Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 * - `minAmount` _string_ The minimum required amount extracted from the error. Default empty string.
 * - `maxAmount` _string_ The maximum amount that can be exchanged extracted from the error. Default empty string.
 * - `gettingOffer` _boolean_ Whether loading either fixed or floating rate offer. Default `false`.
 * @param {!xyz.swapee.wc.DealBrokerAggregatorAdapterLand} [land] The land data.
 * - `OffersAggregator` _xyz.swapee.wc.IOffersAggregator_
 * @return {void}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/41-DealBrokerAggregatorAdapterClasses.xml}  91f667357e7c7170d606091f6c54ad52 */
/**
 * The classes of the _IDealBrokerAggregatorAdapterDisplay_.
 * @record xyz.swapee.wc.DealBrokerAggregatorAdapterClasses
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterClasses.prototype.props = /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/50-IDealBrokerAggregatorAdapterController.xml}  4c4d47fcd0b86f1e1965bf87c0298b77 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs, !xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel>} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterController)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController} xyz.swapee.wc.DealBrokerAggregatorAdapterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterController` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.DealBrokerAggregatorAdapterControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs, !xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs, !xyz.swapee.wc.IDealBrokerAggregatorAdapterController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs, !xyz.swapee.wc.DealBrokerAggregatorAdapterMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs>)} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterController
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.resetPort} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.flipGettingOffer} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.prototype.flipGettingOffer = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.setGettingOffer} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.prototype.setGettingOffer = function() {}
/** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.unsetGettingOffer} */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.prototype.unsetGettingOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterController&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterController.constructor */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterController_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterController
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterController.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterController.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterController.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerFields = class { }
/**
 * The inputs to the _IDealBrokerAggregatorAdapter_'s controller.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerFields.prototype.props = /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterController} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterController} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterController

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterController */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterController} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterController */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.Inputs} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs The inputs to the _IDealBrokerAggregatorAdapter_'s controller. */

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterPort.WeakInputs} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.WeakInputs The inputs to the _IDealBrokerAggregatorAdapter_'s controller. */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterController_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterController_ instance into the _BoundIDealBrokerAggregatorAdapterController_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerCaster.prototype.asIDealBrokerAggregatorAdapterController
/**
 * Cast the _IDealBrokerAggregatorAdapterController_ instance into the _BoundIDealBrokerAggregatorAdapterProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterProcessor}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerCaster.prototype.asIDealBrokerAggregatorAdapterProcessor
/**
 * Access the _DealBrokerAggregatorAdapterController_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterControllerCaster.prototype.superDealBrokerAggregatorAdapterController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__resetPort<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController>} xyz.swapee.wc.IDealBrokerAggregatorAdapterController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__flipGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__flipGettingOffer<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController>} xyz.swapee.wc.IDealBrokerAggregatorAdapterController._flipGettingOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController.flipGettingOffer} */
/**
 * Flips between the positive and negative values of the `gettingOffer`.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.flipGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__setGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__setGettingOffer<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController>} xyz.swapee.wc.IDealBrokerAggregatorAdapterController._setGettingOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController.setGettingOffer} */
/**
 * Sets the `gettingOffer` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.setGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__unsetGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterController.__unsetGettingOffer<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController>} xyz.swapee.wc.IDealBrokerAggregatorAdapterController._unsetGettingOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterController.unsetGettingOffer} */
/**
 * Clears the `gettingOffer` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterController.unsetGettingOffer = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerAggregatorAdapterController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/51-IDealBrokerAggregatorAdapterControllerFront.xml}  200a2ff4711ffa3dce83a5af699272d9 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.DealBrokerAggregatorAdapterController)} xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController} xyz.swapee.wc.front.DealBrokerAggregatorAdapterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController` interface.
 * @constructor xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.constructor&xyz.swapee.wc.front.DealBrokerAggregatorAdapterController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.prototype.constructor = xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.class = /** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.Initialese[]) => xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerCaster&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT)} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.flipGettingOffer} */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.prototype.flipGettingOffer = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.setGettingOffer} */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.prototype.setGettingOffer = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.unsetGettingOffer} */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.prototype.unsetGettingOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.Initialese>)} xyz.swapee.wc.front.DealBrokerAggregatorAdapterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterController_ instances.
 * @constructor xyz.swapee.wc.front.DealBrokerAggregatorAdapterController
 * @implements {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.Initialese>} ‎
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.front.DealBrokerAggregatorAdapterController.constructor&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController} */
xyz.swapee.wc.front.RecordIDealBrokerAggregatorAdapterController

/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.front.BoundIDealBrokerAggregatorAdapterController */

/** @typedef {xyz.swapee.wc.front.DealBrokerAggregatorAdapterController} xyz.swapee.wc.front.BoundDealBrokerAggregatorAdapterController */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterController_ interface.
 * @interface xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerCaster
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterController_ instance into the _BoundIDealBrokerAggregatorAdapterController_ type.
 * @type {!xyz.swapee.wc.front.BoundIDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerCaster.prototype.asIDealBrokerAggregatorAdapterController
/**
 * Access the _DealBrokerAggregatorAdapterController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerCaster.prototype.superDealBrokerAggregatorAdapterController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.__flipGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.__flipGettingOffer<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController>} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController._flipGettingOffer */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.flipGettingOffer} */
/**
 * Flips between the positive and negative values of the `gettingOffer`.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.flipGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.__setGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.__setGettingOffer<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController>} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController._setGettingOffer */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.setGettingOffer} */
/**
 * Sets the `gettingOffer` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.setGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.__unsetGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.__unsetGettingOffer<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController>} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController._unsetGettingOffer */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.unsetGettingOffer} */
/**
 * Clears the `gettingOffer` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.unsetGettingOffer = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/52-IDealBrokerAggregatorAdapterControllerBack.xml}  13b1f58829f9c3094964e7645716fd7c */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs>&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerAggregatorAdapterController)} xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController} xyz.swapee.wc.back.DealBrokerAggregatorAdapterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.constructor&xyz.swapee.wc.back.DealBrokerAggregatorAdapterController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese[]) => xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerCaster&xyz.swapee.wc.IDealBrokerAggregatorAdapterController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Inputs>)} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese>)} xyz.swapee.wc.back.DealBrokerAggregatorAdapterController.constructor */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterController_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerAggregatorAdapterController
 * @implements {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterController = class extends /** @type {xyz.swapee.wc.back.DealBrokerAggregatorAdapterController.constructor&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController} */
xyz.swapee.wc.back.RecordIDealBrokerAggregatorAdapterController

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController} xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterController */

/** @typedef {xyz.swapee.wc.back.DealBrokerAggregatorAdapterController} xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterController */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterController_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerCaster
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterController_ instance into the _BoundIDealBrokerAggregatorAdapterController_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerCaster.prototype.asIDealBrokerAggregatorAdapterController
/**
 * Access the _DealBrokerAggregatorAdapterController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterController}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerCaster.prototype.superDealBrokerAggregatorAdapterController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/53-IDealBrokerAggregatorAdapterControllerAR.xml}  86be0b8c2c77235f6adc04eec4bc2229 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.Initialese} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR)} xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR} xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.constructor&xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.Initialese[]) => xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR} xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IDealBrokerAggregatorAdapterController)} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IDealBrokerAggregatorAdapterControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR = class extends /** @type {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterController.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.Initialese>)} xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR
 * @implements {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IDealBrokerAggregatorAdapterControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR = class extends /** @type {xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR.constructor&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR}
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR} */
xyz.swapee.wc.back.RecordIDealBrokerAggregatorAdapterControllerAR

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR} xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterControllerAR */

/** @typedef {xyz.swapee.wc.back.DealBrokerAggregatorAdapterControllerAR} xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterControllerAR */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerARCaster
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerARCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterControllerAR_ instance into the _BoundIDealBrokerAggregatorAdapterControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterControllerAR}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerARCaster.prototype.asIDealBrokerAggregatorAdapterControllerAR
/**
 * Access the _DealBrokerAggregatorAdapterControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterControllerAR}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerARCaster.prototype.superDealBrokerAggregatorAdapterControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/54-IDealBrokerAggregatorAdapterControllerAT.xml}  cad04fd00d2dd0467639dd59b438eef3 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT)} xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT} xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.constructor&xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.Initialese[]) => xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT} xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IDealBrokerAggregatorAdapterControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT = class extends /** @type {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.Initialese>)} xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT.constructor */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT
 * @implements {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IDealBrokerAggregatorAdapterControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT = class extends /** @type {xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT.constructor&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT}
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT} */
xyz.swapee.wc.front.RecordIDealBrokerAggregatorAdapterControllerAT

/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT} xyz.swapee.wc.front.BoundIDealBrokerAggregatorAdapterControllerAT */

/** @typedef {xyz.swapee.wc.front.DealBrokerAggregatorAdapterControllerAT} xyz.swapee.wc.front.BoundDealBrokerAggregatorAdapterControllerAT */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerATCaster
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerATCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterControllerAT_ instance into the _BoundIDealBrokerAggregatorAdapterControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIDealBrokerAggregatorAdapterControllerAT}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerATCaster.prototype.asIDealBrokerAggregatorAdapterControllerAT
/**
 * Access the _DealBrokerAggregatorAdapterControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundDealBrokerAggregatorAdapterControllerAT}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerATCaster.prototype.superDealBrokerAggregatorAdapterControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/70-IDealBrokerAggregatorAdapterScreen.xml}  4ca21bd9e721d4fc00dccddf1018ceeb */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs, !HTMLDivElement, !xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Settings, !xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Queries, null>&xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterScreen)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen} xyz.swapee.wc.DealBrokerAggregatorAdapterScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterController)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen} xyz.swapee.wc.DealBrokerAggregatorAdapterScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.DealBrokerAggregatorAdapterMemory, !xyz.swapee.wc.front.DealBrokerAggregatorAdapterInputs, !HTMLDivElement, !xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Settings, !xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.Queries, null, null>&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController&xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay)} xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen} xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterScreen_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterScreen
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterScreen = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterScreen.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterScreen

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterScreen */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterScreen} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterScreen */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterScreen_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterScreenCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterScreenCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterScreen_ instance into the _BoundIDealBrokerAggregatorAdapterScreen_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterScreenCaster.prototype.asIDealBrokerAggregatorAdapterScreen
/**
 * Access the _DealBrokerAggregatorAdapterScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterScreenCaster.prototype.superDealBrokerAggregatorAdapterScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/70-IDealBrokerAggregatorAdapterScreenBack.xml}  543657ddc3a9ad5599342acde9f8576b */
/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen)} xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen} xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.constructor&xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese[]) => xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen} xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenCaster&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT)} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen = class extends /** @type {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese>)} xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen.constructor */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterScreen_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen
 * @implements {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen = class extends /** @type {xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen.constructor&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen} */
xyz.swapee.wc.back.RecordIDealBrokerAggregatorAdapterScreen

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen} xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterScreen */

/** @typedef {xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreen} xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterScreen */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterScreen_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenCaster
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterScreen_ instance into the _BoundIDealBrokerAggregatorAdapterScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenCaster.prototype.asIDealBrokerAggregatorAdapterScreen
/**
 * Access the _DealBrokerAggregatorAdapterScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterScreen}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenCaster.prototype.superDealBrokerAggregatorAdapterScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/73-IDealBrokerAggregatorAdapterScreenAR.xml}  ae181608656b34be7531e67c59f7e6da */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.Initialese} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR)} xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR} xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.constructor&xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR|typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerAggregatorAdapterScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.Initialese[]) => xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR} xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen)} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IDealBrokerAggregatorAdapterScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR = class extends /** @type {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.Initialese>)} xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR} xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.typeof */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR
 * @implements {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IDealBrokerAggregatorAdapterScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR = class extends /** @type {xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR.constructor&xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR}
 */
xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR} */
xyz.swapee.wc.front.RecordIDealBrokerAggregatorAdapterScreenAR

/** @typedef {xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR} xyz.swapee.wc.front.BoundIDealBrokerAggregatorAdapterScreenAR */

/** @typedef {xyz.swapee.wc.front.DealBrokerAggregatorAdapterScreenAR} xyz.swapee.wc.front.BoundDealBrokerAggregatorAdapterScreenAR */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenARCaster
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenARCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterScreenAR_ instance into the _BoundIDealBrokerAggregatorAdapterScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIDealBrokerAggregatorAdapterScreenAR}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenARCaster.prototype.asIDealBrokerAggregatorAdapterScreenAR
/**
 * Access the _DealBrokerAggregatorAdapterScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundDealBrokerAggregatorAdapterScreenAR}
 */
xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenARCaster.prototype.superDealBrokerAggregatorAdapterScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/74-IDealBrokerAggregatorAdapterScreenAT.xml}  70d672576f524da8c4bd36cbcd3a718d */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT)} xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT} xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.constructor&xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerAggregatorAdapterScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese[]) => xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT} xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IDealBrokerAggregatorAdapterScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT = class extends /** @type {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese>)} xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT.constructor */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT
 * @implements {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IDealBrokerAggregatorAdapterScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT = class extends /** @type {xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT.constructor&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT}
 */
xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT} */
xyz.swapee.wc.back.RecordIDealBrokerAggregatorAdapterScreenAT

/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT} xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterScreenAT */

/** @typedef {xyz.swapee.wc.back.DealBrokerAggregatorAdapterScreenAT} xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterScreenAT */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenATCaster
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenATCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterScreenAT_ instance into the _BoundIDealBrokerAggregatorAdapterScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerAggregatorAdapterScreenAT}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenATCaster.prototype.asIDealBrokerAggregatorAdapterScreenAT
/**
 * Access the _DealBrokerAggregatorAdapterScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerAggregatorAdapterScreenAT}
 */
xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenATCaster.prototype.superDealBrokerAggregatorAdapterScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/design/80-IDealBrokerAggregatorAdapterGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.Initialese} xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerAggregatorAdapterGPU)} xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU} xyz.swapee.wc.DealBrokerAggregatorAdapterGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.constructor&xyz.swapee.wc.DealBrokerAggregatorAdapterGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU|typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay|typeof xyz.swapee.wc.back.DealBrokerAggregatorAdapterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU}
 */
xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese[]) => xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU} xyz.swapee.wc.DealBrokerAggregatorAdapterGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUCaster&com.webcircuits.IBrowserView<.!DealBrokerAggregatorAdapterMemory,>&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay)} xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!DealBrokerAggregatorAdapterMemory,>} com.webcircuits.IBrowserView<.!DealBrokerAggregatorAdapterMemory,>.typeof */
/**
 * Handles the periphery of the _IDealBrokerAggregatorAdapterDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU = class extends /** @type {xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!DealBrokerAggregatorAdapterMemory,>.typeof&xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerAggregatorAdapterGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese>)} xyz.swapee.wc.DealBrokerAggregatorAdapterGPU.constructor */
/**
 * A concrete class of _IDealBrokerAggregatorAdapterGPU_ instances.
 * @constructor xyz.swapee.wc.DealBrokerAggregatorAdapterGPU
 * @implements {xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU} Handles the periphery of the _IDealBrokerAggregatorAdapterDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterGPU = class extends /** @type {xyz.swapee.wc.DealBrokerAggregatorAdapterGPU.constructor&xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerAggregatorAdapterGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerAggregatorAdapterGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerAggregatorAdapterGPU}
 */
xyz.swapee.wc.DealBrokerAggregatorAdapterGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerAggregatorAdapterGPU.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUFields
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU} */
xyz.swapee.wc.RecordIDealBrokerAggregatorAdapterGPU

/** @typedef {xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU} xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterGPU */

/** @typedef {xyz.swapee.wc.DealBrokerAggregatorAdapterGPU} xyz.swapee.wc.BoundDealBrokerAggregatorAdapterGPU */

/**
 * Contains getters to cast the _IDealBrokerAggregatorAdapterGPU_ interface.
 * @interface xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUCaster
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUCaster = class { }
/**
 * Cast the _IDealBrokerAggregatorAdapterGPU_ instance into the _BoundIDealBrokerAggregatorAdapterGPU_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerAggregatorAdapterGPU}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUCaster.prototype.asIDealBrokerAggregatorAdapterGPU
/**
 * Access the _DealBrokerAggregatorAdapterGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerAggregatorAdapterGPU}
 */
xyz.swapee.wc.IDealBrokerAggregatorAdapterGPUCaster.prototype.superDealBrokerAggregatorAdapterGPU

// nss:xyz.swapee.wc
/* @typal-end */