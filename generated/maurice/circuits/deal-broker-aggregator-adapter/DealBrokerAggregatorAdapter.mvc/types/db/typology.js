/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer': {
  'id': 10964278771,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs': {
  'id': 10964278772,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore': {
  'id': 10964278773,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs': {
  'id': 10964278774,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterPort': {
  'id': 10964278775,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetDealBrokerAggregatorAdapterPort': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface': {
  'id': 10964278776,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterCore': {
  'id': 10964278777,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetDealBrokerAggregatorAdapterCore': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor': {
  'id': 10964278778,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapter': {
  'id': 10964278779,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer': {
  'id': 109642787710,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent': {
  'id': 109642787711,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterElement': {
  'id': 109642787712,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4,
   'build': 5,
   'buildOffersAggregator': 6,
   'short': 7
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort': {
  'id': 109642787713,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner': {
  'id': 109642787714,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU': {
  'id': 109642787715,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay': {
  'id': 109642787716,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterVdusPQs': {
  'id': 109642787717,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay': {
  'id': 109642787718,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterController': {
  'id': 109642787719,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'flipGettingOffer': 2,
   'setGettingOffer': 3,
   'unsetGettingOffer': 4
  }
 },
 'xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController': {
  'id': 109642787720,
  'symbols': {},
  'methods': {
   'flipGettingOffer': 1,
   'setGettingOffer': 2,
   'unsetGettingOffer': 3
  }
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController': {
  'id': 109642787721,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR': {
  'id': 109642787722,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT': {
  'id': 109642787723,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen': {
  'id': 109642787724,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen': {
  'id': 109642787725,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR': {
  'id': 109642787726,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT': {
  'id': 109642787727,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil': {
  'id': 109642787728,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterQueriesPQs': {
  'id': 109642787729,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})