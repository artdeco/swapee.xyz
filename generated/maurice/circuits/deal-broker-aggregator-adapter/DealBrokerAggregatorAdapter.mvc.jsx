/** @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapter} */
export default class AbstractDealBrokerAggregatorAdapter extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterComputer} */
export class AbstractDealBrokerAggregatorAdapterComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterController} */
export class AbstractDealBrokerAggregatorAdapterController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterPort} */
export class DealBrokerAggregatorAdapterPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterView} */
export class AbstractDealBrokerAggregatorAdapterView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterElement} */
export class AbstractDealBrokerAggregatorAdapterElement extends (<element v3 html mv>
 <block src="./DealBrokerAggregatorAdapter.mvc/src/DealBrokerAggregatorAdapterElement/methods/render.jsx" />
 <inducer src="./DealBrokerAggregatorAdapter.mvc/src/DealBrokerAggregatorAdapterElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractDealBrokerAggregatorAdapterHtmlComponent} */
export class AbstractDealBrokerAggregatorAdapterHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IOffersAggregator via="OffersAggregator" />
  </connectors>

</html-ic>) { }
// </class-end>