import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IExchangePageCircuitScreen._buildTitle} */
export default function buildTitle(state,baseTitle){
 const{
  amountFrom:amount,
  cryptoIn:from,
  cryptoOut:to,
  id,
 }=state
 if(id) return ['Transaction #'+id,amount,from,'to',to,'|',baseTitle].join(' ')
 if(!from||!to) return baseTitle
 return['Exchange',amount,from,'to',to,'|',baseTitle].join(' ')
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZXhjaGFuZ2UtcGFnZS1jaXJjdWl0L2V4Y2hhbmdlLXBhZ2UtY2lyY3VpdC53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBa0pHLFNBQVMsVUFBVSxDQUFDLEtBQUssQ0FBQztDQUN6QjtFQUNDLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtFQUNaLEVBQUU7R0FDRDtDQUNGLEVBQUUsQ0FBQyxJQUFJLE9BQU8sY0FBYyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHO0NBQzVFLEVBQUUsQ0FBQyxZQUFZLE9BQU87Q0FDdEIsaUJBQWlCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHO0FBQzdELENBQUYifQ==