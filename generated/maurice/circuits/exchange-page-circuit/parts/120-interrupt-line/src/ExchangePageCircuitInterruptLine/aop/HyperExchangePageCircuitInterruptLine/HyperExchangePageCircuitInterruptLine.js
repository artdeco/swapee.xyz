import AbstractHyperExchangePageCircuitInterruptLine from '../../../../gen/AbstractExchangePageCircuitInterruptLine/hyper/AbstractHyperExchangePageCircuitInterruptLine'
import ExchangePageCircuitInterruptLine from '../../ExchangePageCircuitInterruptLine'
import ExchangePageCircuitInterruptLineGeneralAspects from '../ExchangePageCircuitInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperExchangePageCircuitInterruptLine} */
export default class extends AbstractHyperExchangePageCircuitInterruptLine
 .consults(
  ExchangePageCircuitInterruptLineGeneralAspects,
 )
 .implements(
  ExchangePageCircuitInterruptLine,
 )
{}