import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IExchangePageCircuitDisplay._paintCryptoOutIm} */
export default function paintCryptoOutIm() {
 setTimeout(()=>{
  const img=this.CryptoOutImWr.querySelector('img').src
  this.setInputs({outImg:img})
 },10)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZXhjaGFuZ2UtcGFnZS1jaXJjdWl0L2V4Y2hhbmdlLXBhZ2UtY2lyY3VpdC53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBK0tHLFNBQVMsZ0JBQWdCLENBQUM7Q0FDekIsVUFBVSxDQUFDLENBQUM7RUFDWCxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7RUFDbEQsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7QUFDN0IsR0FBRztBQUNILENBQUYifQ==