import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IExchangePageCircuitDisplay._paintCryptoInIm} */
export default function paintCryptoInIm() {
 setTimeout(()=>{
  const img=this.CryptoInImWr.querySelector('img').src
  this.setInputs({inImg:img})
 },10)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZXhjaGFuZ2UtcGFnZS1jaXJjdWl0L2V4Y2hhbmdlLXBhZ2UtY2lyY3VpdC53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBeUtHLFNBQVMsZUFBZSxDQUFDO0NBQ3hCLFVBQVUsQ0FBQyxDQUFDO0VBQ1gsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO0VBQ2pELElBQUksQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDO0FBQzVCLEdBQUc7QUFDSCxDQUFGIn0=