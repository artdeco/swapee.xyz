/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IExchangePageCircuitComputer': {
  'id': 77359065961,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptType': 3,
   'adaptIntent': 4,
   'adaptId': 5,
   'adaptNotId': 6
  }
 },
 'xyz.swapee.wc.ExchangePageCircuitMemoryPQs': {
  'id': 77359065962,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitOuterCore': {
  'id': 77359065963,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangePageCircuitInputsPQs': {
  'id': 77359065964,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitPort': {
  'id': 77359065965,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangePageCircuitPort': 2
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitPortInterface': {
  'id': 77359065966,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitCore': {
  'id': 77359065967,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangePageCircuitCore': 2
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitProcessor': {
  'id': 77359065968,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuit': {
  'id': 77359065969,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitBuffer': {
  'id': 773590659610,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil': {
  'id': 773590659611,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitHtmlComponent': {
  'id': 773590659612,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitElement': {
  'id': 773590659613,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'short': 6,
   'server': 7,
   'inducer': 8,
   'buildOffersFilter': 9,
   'buildExchangeBroker': 10,
   'buildDealBroker': 11,
   'buildExchangeIntent': 12,
   'buildExchangeIdRow': 13,
   'buildTransactionInfo': 14,
   'buildTransactionInfoHead': 16,
   'buildTransactionInfoHeadMob': 17,
   'buildOfferExchange': 18,
   'buildCryptoSelectIn': 19,
   'buildCryptoSelectOut': 20
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitElementPort': {
  'id': 773590659614,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitDesigner': {
  'id': 773590659615,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitGPU': {
  'id': 773590659616,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitDisplay': {
  'id': 773590659617,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangePageCircuitVdusPQs': {
  'id': 773590659618,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangePageCircuitDisplay': {
  'id': 773590659619,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitController': {
  'id': 773590659620,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setAmountFrom': 8,
   'unsetAmountFrom': 9,
   'setCryptoIn': 12,
   'unsetCryptoIn': 13,
   'setCryptoOut': 14,
   'unsetCryptoOut': 15,
   'setId': 20,
   'unsetId': 21
  }
 },
 'xyz.swapee.wc.front.IExchangePageCircuitController': {
  'id': 773590659621,
  'symbols': {},
  'methods': {
   'setAmountFrom': 7,
   'unsetAmountFrom': 8,
   'setCryptoIn': 11,
   'unsetCryptoIn': 12,
   'setCryptoOut': 13,
   'unsetCryptoOut': 14,
   'setId': 19,
   'unsetId': 20
  }
 },
 'xyz.swapee.wc.back.IExchangePageCircuitController': {
  'id': 773590659622,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangePageCircuitControllerAR': {
  'id': 773590659623,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangePageCircuitControllerAT': {
  'id': 773590659624,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitScreen': {
  'id': 773590659625,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangePageCircuitScreen': {
  'id': 773590659626,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangePageCircuitScreenAR': {
  'id': 773590659627,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangePageCircuitScreenAT': {
  'id': 773590659628,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangePageCircuitQueriesPQs': {
  'id': 773590659629,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ExchangePageCircuitClassesPQs': {
  'id': 773590659630,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})