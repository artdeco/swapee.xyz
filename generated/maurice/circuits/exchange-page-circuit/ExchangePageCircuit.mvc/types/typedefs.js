/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IExchangePageCircuitComputer={}
xyz.swapee.wc.IExchangePageCircuitComputer.adaptId={}
xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId={}
xyz.swapee.wc.IExchangePageCircuitComputer.adaptType={}
xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent={}
xyz.swapee.wc.IExchangePageCircuitComputer.compute={}
xyz.swapee.wc.IExchangePageCircuitOuterCore={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Id={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Type={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.AmountFrom={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoIn={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoOut={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.NotId={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FixedRate={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FloatingRate={}
xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel={}
xyz.swapee.wc.IExchangePageCircuitPort={}
xyz.swapee.wc.IExchangePageCircuitPort.Inputs={}
xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs={}
xyz.swapee.wc.IExchangePageCircuitCore={}
xyz.swapee.wc.IExchangePageCircuitCore.Model={}
xyz.swapee.wc.IExchangePageCircuitPortInterface={}
xyz.swapee.wc.IExchangePageCircuitProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IExchangePageCircuitController={}
xyz.swapee.wc.front.IExchangePageCircuitControllerAT={}
xyz.swapee.wc.front.IExchangePageCircuitScreenAR={}
xyz.swapee.wc.IExchangePageCircuit={}
xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil={}
xyz.swapee.wc.IExchangePageCircuitHtmlComponent={}
xyz.swapee.wc.IExchangePageCircuitElement={}
xyz.swapee.wc.IExchangePageCircuitElement.build={}
xyz.swapee.wc.IExchangePageCircuitElement.short={}
xyz.swapee.wc.IExchangePageCircuitElementPort={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts={}
xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs={}
xyz.swapee.wc.IExchangePageCircuitDesigner={}
xyz.swapee.wc.IExchangePageCircuitDesigner.communicator={}
xyz.swapee.wc.IExchangePageCircuitDesigner.relay={}
xyz.swapee.wc.IExchangePageCircuitDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IExchangePageCircuitDisplay={}
xyz.swapee.wc.back.IExchangePageCircuitController={}
xyz.swapee.wc.back.IExchangePageCircuitControllerAR={}
xyz.swapee.wc.back.IExchangePageCircuitScreen={}
xyz.swapee.wc.back.IExchangePageCircuitScreenAT={}
xyz.swapee.wc.IExchangePageCircuitController={}
xyz.swapee.wc.IExchangePageCircuitScreen={}
xyz.swapee.wc.IExchangePageCircuitGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/02-IExchangePageCircuitComputer.xml}  2405f6cdde6aebe09888d1f9f591b80c */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IExchangePageCircuitComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitComputer)} xyz.swapee.wc.AbstractExchangePageCircuitComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitComputer} xyz.swapee.wc.ExchangePageCircuitComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitComputer` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitComputer
 */
xyz.swapee.wc.AbstractExchangePageCircuitComputer = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitComputer.constructor&xyz.swapee.wc.ExchangePageCircuitComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitComputer.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitComputer.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitComputer}
 */
xyz.swapee.wc.AbstractExchangePageCircuitComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitComputer}
 */
xyz.swapee.wc.AbstractExchangePageCircuitComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitComputer}
 */
xyz.swapee.wc.AbstractExchangePageCircuitComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitComputer}
 */
xyz.swapee.wc.AbstractExchangePageCircuitComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitComputer.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitComputer} xyz.swapee.wc.ExchangePageCircuitComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.ExchangePageCircuitMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.ExchangePageCircuitLand>)} xyz.swapee.wc.IExchangePageCircuitComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IExchangePageCircuitComputer
 */
xyz.swapee.wc.IExchangePageCircuitComputer = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangePageCircuitComputer.adaptId} */
xyz.swapee.wc.IExchangePageCircuitComputer.prototype.adaptId = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId} */
xyz.swapee.wc.IExchangePageCircuitComputer.prototype.adaptNotId = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitComputer.adaptType} */
xyz.swapee.wc.IExchangePageCircuitComputer.prototype.adaptType = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent} */
xyz.swapee.wc.IExchangePageCircuitComputer.prototype.adaptIntent = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitComputer.compute} */
xyz.swapee.wc.IExchangePageCircuitComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitComputer.Initialese>)} xyz.swapee.wc.ExchangePageCircuitComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitComputer} xyz.swapee.wc.IExchangePageCircuitComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangePageCircuitComputer_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitComputer
 * @implements {xyz.swapee.wc.IExchangePageCircuitComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitComputer.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitComputer = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitComputer.constructor&xyz.swapee.wc.IExchangePageCircuitComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitComputer}
 */
xyz.swapee.wc.ExchangePageCircuitComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer} */
xyz.swapee.wc.RecordIExchangePageCircuitComputer

/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer} xyz.swapee.wc.BoundIExchangePageCircuitComputer */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitComputer} xyz.swapee.wc.BoundExchangePageCircuitComputer */

/**
 * Contains getters to cast the _IExchangePageCircuitComputer_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitComputerCaster
 */
xyz.swapee.wc.IExchangePageCircuitComputerCaster = class { }
/**
 * Cast the _IExchangePageCircuitComputer_ instance into the _BoundIExchangePageCircuitComputer_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitComputer}
 */
xyz.swapee.wc.IExchangePageCircuitComputerCaster.prototype.asIExchangePageCircuitComputer
/**
 * Access the _ExchangePageCircuitComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitComputer}
 */
xyz.swapee.wc.IExchangePageCircuitComputerCaster.prototype.superExchangePageCircuitComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Form, changes: xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Form) => (void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Return)} xyz.swapee.wc.IExchangePageCircuitComputer.__adaptId
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer.__adaptId<!xyz.swapee.wc.IExchangePageCircuitComputer>} xyz.swapee.wc.IExchangePageCircuitComputer._adaptId */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitComputer.adaptId} */
/**
 * @param {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Form} form The form with inputs.
 * @param {xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Return} The form with outputs.
 */
xyz.swapee.wc.IExchangePageCircuitComputer.adaptId = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangeBrokerCore.Model.Id_Safe} xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.Model.Id} xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Form, changes: xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Form) => (void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Return)} xyz.swapee.wc.IExchangePageCircuitComputer.__adaptNotId
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer.__adaptNotId<!xyz.swapee.wc.IExchangePageCircuitComputer>} xyz.swapee.wc.IExchangePageCircuitComputer._adaptNotId */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId} */
/**
 * @param {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Form} form The form with inputs.
 * - `id` _string_ The id of a created transaction. ⤴ *IExchangePageCircuitOuterCore.Model.Id_Safe*
 * @param {xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Form} changes The previous values of the form.
 * - `id` _string_ The id of a created transaction. ⤴ *IExchangePageCircuitOuterCore.Model.Id_Safe*
 * @return {void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Return} The form with outputs.
 */
xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.Model.Id_Safe} xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.Model.NotId} xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Form, changes: xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Form) => (void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Return)} xyz.swapee.wc.IExchangePageCircuitComputer.__adaptType
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer.__adaptType<!xyz.swapee.wc.IExchangePageCircuitComputer>} xyz.swapee.wc.IExchangePageCircuitComputer._adaptType */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitComputer.adaptType} */
/**
 * @param {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Form} form The form with inputs.
 * - `type` _string_ ⤴ *IExchangePageCircuitOuterCore.Model.Type_Safe*
 * Can be either:
 * > - _fixed_
 * > - _floating_
 * @param {xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Form} changes The previous values of the form.
 * - `type` _string_ ⤴ *IExchangePageCircuitOuterCore.Model.Type_Safe*
 * Can be either:
 * > - _fixed_
 * > - _floating_
 * @return {void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Return} The form with outputs.
 */
xyz.swapee.wc.IExchangePageCircuitComputer.adaptType = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.Model.Type_Safe} xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.Model.FixedRate&xyz.swapee.wc.IExchangePageCircuitCore.Model.FloatingRate} xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Form, changes: xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Form) => (void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Return)} xyz.swapee.wc.IExchangePageCircuitComputer.__adaptIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer.__adaptIntent<!xyz.swapee.wc.IExchangePageCircuitComputer>} xyz.swapee.wc.IExchangePageCircuitComputer._adaptIntent */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent} */
/**
 * From the intent, reads the data.
 * @param {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Form} form The form with inputs.
 * @param {xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Return} The form with outputs.
 */
xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe} xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.Model.Type&xyz.swapee.wc.IExchangePageCircuitCore.Model.CryptoIn&xyz.swapee.wc.IExchangePageCircuitCore.Model.CryptoOut&xyz.swapee.wc.IExchangePageCircuitCore.Model.AmountFrom} xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.ExchangePageCircuitMemory, land: !xyz.swapee.wc.IExchangePageCircuitComputer.compute.Land) => void} xyz.swapee.wc.IExchangePageCircuitComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer.__compute<!xyz.swapee.wc.IExchangePageCircuitComputer>} xyz.swapee.wc.IExchangePageCircuitComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.ExchangePageCircuitMemory} mem The memory.
 * @param {!xyz.swapee.wc.IExchangePageCircuitComputer.compute.Land} land The land.
 * - `CryptoSelectIn` _!xyz.swapee.wc.CryptoSelectMemory_ `.`
 * - `CryptoSelectOut` _!xyz.swapee.wc.CryptoSelectMemory_ `.`
 * - `OffersFilter` _!xyz.swapee.wc.OffersFilterMemory_ `.`
 * - `ExchangeBroker` _!xyz.swapee.wc.ExchangeBrokerMemory_ `.`
 * - `DealBroker` _!xyz.swapee.wc.DealBrokerMemory_ `.`
 * - `OfferExchange` _!xyz.swapee.wc.OfferExchangeMemory_ `.`
 * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_ `.`
 * - `ExchangeIdRow` _!xyz.swapee.wc.ExchangeIdRowMemory_ `.`
 * - `TransactionInfo` _!xyz.swapee.wc.TransactionInfoMemory_ `.`
 * - `TransactionInfoHead` _!xyz.swapee.wc.TransactionInfoHeadMemory_ `.`
 * - `TransactionInfoHeadMob` _!xyz.swapee.wc.TransactionInfoHeadMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectIn `.`
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut `.`
 * @prop {!xyz.swapee.wc.OffersFilterMemory} OffersFilter `.`
 * @prop {!xyz.swapee.wc.ExchangeBrokerMemory} ExchangeBroker `.`
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker `.`
 * @prop {!xyz.swapee.wc.OfferExchangeMemory} OfferExchange `.`
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent `.`
 * @prop {!xyz.swapee.wc.ExchangeIdRowMemory} ExchangeIdRow `.`
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} TransactionInfo `.`
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHead `.`
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHeadMob `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangePageCircuitComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/03-IExchangePageCircuitOuterCore.xml}  e109a784643cf023195647706b12498d */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangePageCircuitOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitOuterCore)} xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitOuterCore} xyz.swapee.wc.ExchangePageCircuitOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitOuterCore
 */
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.constructor&xyz.swapee.wc.ExchangePageCircuitOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitOuterCore|typeof xyz.swapee.wc.ExchangePageCircuitOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitOuterCore|typeof xyz.swapee.wc.ExchangePageCircuitOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitOuterCore|typeof xyz.swapee.wc.ExchangePageCircuitOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitOuterCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitOuterCoreCaster)} xyz.swapee.wc.IExchangePageCircuitOuterCore.constructor */
/**
 * The _IExchangePageCircuit_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IExchangePageCircuitOuterCore
 */
xyz.swapee.wc.IExchangePageCircuitOuterCore = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangePageCircuitOuterCore.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitOuterCore

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitOuterCore.Initialese>)} xyz.swapee.wc.ExchangePageCircuitOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitOuterCore} xyz.swapee.wc.IExchangePageCircuitOuterCore.typeof */
/**
 * A concrete class of _IExchangePageCircuitOuterCore_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitOuterCore
 * @implements {xyz.swapee.wc.IExchangePageCircuitOuterCore} The _IExchangePageCircuit_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitOuterCore = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitOuterCore.constructor&xyz.swapee.wc.IExchangePageCircuitOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangePageCircuitOuterCore.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitOuterCore}
 */
xyz.swapee.wc.ExchangePageCircuitOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitOuterCore.
 * @interface xyz.swapee.wc.IExchangePageCircuitOuterCoreFields
 */
xyz.swapee.wc.IExchangePageCircuitOuterCoreFields = class { }
/**
 * The _IExchangePageCircuit_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IExchangePageCircuitOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangePageCircuitOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore} */
xyz.swapee.wc.RecordIExchangePageCircuitOuterCore

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore} xyz.swapee.wc.BoundIExchangePageCircuitOuterCore */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitOuterCore} xyz.swapee.wc.BoundExchangePageCircuitOuterCore */

/**
 * The id of a created transaction.
 * @typedef {string}
 */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Id.id

/** @typedef {string} */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Type.type

/** @typedef {string} */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.AmountFrom.amountFrom

/** @typedef {string} */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoIn.cryptoIn

/** @typedef {string} */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoOut.cryptoOut

/** @typedef {boolean} */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.NotId.notId

/** @typedef {boolean} */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FixedRate.fixedRate

/** @typedef {boolean} */
xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FloatingRate.floatingRate

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Id&xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Type&xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.AmountFrom&xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoIn&xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoOut&xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.NotId&xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FixedRate&xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FloatingRate} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model The _IExchangePageCircuit_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Id&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Type&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.AmountFrom&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoIn&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoOut&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.NotId&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FixedRate&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FloatingRate} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel The _IExchangePageCircuit_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IExchangePageCircuitOuterCore_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitOuterCoreCaster
 */
xyz.swapee.wc.IExchangePageCircuitOuterCoreCaster = class { }
/**
 * Cast the _IExchangePageCircuitOuterCore_ instance into the _BoundIExchangePageCircuitOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitOuterCore}
 */
xyz.swapee.wc.IExchangePageCircuitOuterCoreCaster.prototype.asIExchangePageCircuitOuterCore
/**
 * Access the _ExchangePageCircuitOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitOuterCore}
 */
xyz.swapee.wc.IExchangePageCircuitOuterCoreCaster.prototype.superExchangePageCircuitOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Id The id of a created transaction (optional overlay).
 * @prop {string} [id=""] The id of a created transaction. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Id_Safe The id of a created transaction (required overlay).
 * @prop {string} id The id of a created transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Type  (optional overlay).
 * @prop {'fixed'|'floating'} [type=""]
 * Can be either:
 * - _fixed_
 * - _floating_
 *
 * Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Type_Safe  (required overlay).
 * @prop {'fixed'|'floating'} type
 * Can be either:
 * - _fixed_
 * - _floating_
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.AmountFrom  (optional overlay).
 * @prop {string} [amountFrom="0.1"] Default `0.1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.AmountFrom_Safe  (required overlay).
 * @prop {string} amountFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoIn  (optional overlay).
 * @prop {string} [cryptoIn="BTC"] Default `BTC`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoIn_Safe  (required overlay).
 * @prop {string} cryptoIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoOut  (optional overlay).
 * @prop {string} [cryptoOut="ETH"] Default `ETH`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoOut_Safe  (required overlay).
 * @prop {string} cryptoOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.NotId  (optional overlay).
 * @prop {boolean} [notId=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.NotId_Safe  (required overlay).
 * @prop {boolean} notId
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FixedRate  (optional overlay).
 * @prop {boolean} [fixedRate=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FixedRate_Safe  (required overlay).
 * @prop {boolean} fixedRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FloatingRate  (optional overlay).
 * @prop {boolean} [floatingRate=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FloatingRate_Safe  (required overlay).
 * @prop {boolean} floatingRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Id The id of a created transaction (optional overlay).
 * @prop {*} [id=null] The id of a created transaction. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Id_Safe The id of a created transaction (required overlay).
 * @prop {*} id The id of a created transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Type  (optional overlay).
 * @prop {'fixed'|'floating'} [type=null]
 * Can be either:
 * - _fixed_
 * - _floating_
 *
 * Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Type_Safe  (required overlay).
 * @prop {'fixed'|'floating'} type
 * Can be either:
 * - _fixed_
 * - _floating_
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.AmountFrom  (optional overlay).
 * @prop {*} [amountFrom="0.1"] Default `0.1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.AmountFrom_Safe  (required overlay).
 * @prop {*} amountFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoIn  (optional overlay).
 * @prop {*} [cryptoIn="BTC"] Default `BTC`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoIn_Safe  (required overlay).
 * @prop {*} cryptoIn
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoOut  (optional overlay).
 * @prop {*} [cryptoOut="ETH"] Default `ETH`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoOut_Safe  (required overlay).
 * @prop {*} cryptoOut
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.NotId  (optional overlay).
 * @prop {*} [notId=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.NotId_Safe  (required overlay).
 * @prop {*} notId
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FixedRate  (optional overlay).
 * @prop {*} [fixedRate=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FixedRate_Safe  (required overlay).
 * @prop {*} fixedRate
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FloatingRate  (optional overlay).
 * @prop {*} [floatingRate=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FloatingRate_Safe  (required overlay).
 * @prop {*} floatingRate
 */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Id} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.Id The id of a created transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Id_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.Id_Safe The id of a created transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Type} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.Type  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Type_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.Type_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoIn} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.CryptoIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoIn_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.CryptoIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoOut} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.CryptoOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoOut_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.CryptoOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.NotId} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.NotId  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.NotId_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.NotId_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FixedRate} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.FixedRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FixedRate_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.FixedRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FloatingRate} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.FloatingRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FloatingRate_Safe} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.FloatingRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Id} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.Id The id of a created transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Id_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.Id_Safe The id of a created transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Type} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.Type  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.Type_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.Type_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoIn} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.CryptoIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoIn_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.CryptoIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoOut} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.CryptoOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.CryptoOut_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.CryptoOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.NotId} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.NotId  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.NotId_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.NotId_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FixedRate} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.FixedRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FixedRate_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.FixedRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FloatingRate} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.FloatingRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.FloatingRate_Safe} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.FloatingRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Id} xyz.swapee.wc.IExchangePageCircuitCore.Model.Id The id of a created transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Id_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.Id_Safe The id of a created transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Type} xyz.swapee.wc.IExchangePageCircuitCore.Model.Type  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.Type_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.Type_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.AmountFrom} xyz.swapee.wc.IExchangePageCircuitCore.Model.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.AmountFrom_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoIn} xyz.swapee.wc.IExchangePageCircuitCore.Model.CryptoIn  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoIn_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.CryptoIn_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoOut} xyz.swapee.wc.IExchangePageCircuitCore.Model.CryptoOut  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.CryptoOut_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.CryptoOut_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.NotId} xyz.swapee.wc.IExchangePageCircuitCore.Model.NotId  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.NotId_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.NotId_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FixedRate} xyz.swapee.wc.IExchangePageCircuitCore.Model.FixedRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FixedRate_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.FixedRate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FloatingRate} xyz.swapee.wc.IExchangePageCircuitCore.Model.FloatingRate  (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model.FloatingRate_Safe} xyz.swapee.wc.IExchangePageCircuitCore.Model.FloatingRate_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/04-IExchangePageCircuitPort.xml}  8bf75af8303b06b27e0c8d81ef7cc73e */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangePageCircuitPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitPort)} xyz.swapee.wc.AbstractExchangePageCircuitPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitPort} xyz.swapee.wc.ExchangePageCircuitPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitPort
 */
xyz.swapee.wc.AbstractExchangePageCircuitPort = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitPort.constructor&xyz.swapee.wc.ExchangePageCircuitPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitPort.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitPort|typeof xyz.swapee.wc.ExchangePageCircuitPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitPort|typeof xyz.swapee.wc.ExchangePageCircuitPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitPort|typeof xyz.swapee.wc.ExchangePageCircuitPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitPort.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitPort} xyz.swapee.wc.ExchangePageCircuitPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangePageCircuitPort.Inputs>)} xyz.swapee.wc.IExchangePageCircuitPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IExchangePageCircuit_, providing input
 * pins.
 * @interface xyz.swapee.wc.IExchangePageCircuitPort
 */
xyz.swapee.wc.IExchangePageCircuitPort = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangePageCircuitPort.resetPort} */
xyz.swapee.wc.IExchangePageCircuitPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitPort.resetExchangePageCircuitPort} */
xyz.swapee.wc.IExchangePageCircuitPort.prototype.resetExchangePageCircuitPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitPort.Initialese>)} xyz.swapee.wc.ExchangePageCircuitPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitPort} xyz.swapee.wc.IExchangePageCircuitPort.typeof */
/**
 * A concrete class of _IExchangePageCircuitPort_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitPort
 * @implements {xyz.swapee.wc.IExchangePageCircuitPort} The port that serves as an interface to the _IExchangePageCircuit_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitPort = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitPort.constructor&xyz.swapee.wc.IExchangePageCircuitPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitPort}
 */
xyz.swapee.wc.ExchangePageCircuitPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitPort.
 * @interface xyz.swapee.wc.IExchangePageCircuitPortFields
 */
xyz.swapee.wc.IExchangePageCircuitPortFields = class { }
/**
 * The inputs to the _IExchangePageCircuit_'s controller via its port.
 */
xyz.swapee.wc.IExchangePageCircuitPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangePageCircuitPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangePageCircuitPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangePageCircuitPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitPort} */
xyz.swapee.wc.RecordIExchangePageCircuitPort

/** @typedef {xyz.swapee.wc.IExchangePageCircuitPort} xyz.swapee.wc.BoundIExchangePageCircuitPort */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitPort} xyz.swapee.wc.BoundExchangePageCircuitPort */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel)} xyz.swapee.wc.IExchangePageCircuitPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel} xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IExchangePageCircuit_'s controller via its port.
 * @record xyz.swapee.wc.IExchangePageCircuitPort.Inputs
 */
xyz.swapee.wc.IExchangePageCircuitPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitPort.Inputs.constructor&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangePageCircuitPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel)} xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.constructor */
/**
 * The inputs to the _IExchangePageCircuit_'s controller via its port.
 * @record xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs
 */
xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.constructor&xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitPortInterface
 */
xyz.swapee.wc.IExchangePageCircuitPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IExchangePageCircuitPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IExchangePageCircuitPortInterface.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitPortInterface

/**
 * A concrete class of _IExchangePageCircuitPortInterface_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitPortInterface
 * @implements {xyz.swapee.wc.IExchangePageCircuitPortInterface} The port interface.
 */
xyz.swapee.wc.ExchangePageCircuitPortInterface = class extends xyz.swapee.wc.IExchangePageCircuitPortInterface { }
xyz.swapee.wc.ExchangePageCircuitPortInterface.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitPortInterface.Props
 * @prop {string} id The id of a created transaction.
 * @prop {'fixed'|'floating'} type
 * Can be either:
 * - _fixed_
 * - _floating_
 * @prop {boolean} notId
 * @prop {boolean} fixedRate
 * @prop {boolean} floatingRate
 * @prop {string} [amountFrom="0.1"] Default `0.1`.
 * @prop {string} [cryptoIn="BTC"] Default `BTC`.
 * @prop {string} [cryptoOut="ETH"] Default `ETH`.
 */

/**
 * Contains getters to cast the _IExchangePageCircuitPort_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitPortCaster
 */
xyz.swapee.wc.IExchangePageCircuitPortCaster = class { }
/**
 * Cast the _IExchangePageCircuitPort_ instance into the _BoundIExchangePageCircuitPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitPort}
 */
xyz.swapee.wc.IExchangePageCircuitPortCaster.prototype.asIExchangePageCircuitPort
/**
 * Access the _ExchangePageCircuitPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitPort}
 */
xyz.swapee.wc.IExchangePageCircuitPortCaster.prototype.superExchangePageCircuitPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitPort.__resetPort<!xyz.swapee.wc.IExchangePageCircuitPort>} xyz.swapee.wc.IExchangePageCircuitPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitPort.resetPort} */
/**
 * Resets the _IExchangePageCircuit_ port.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitPort.__resetExchangePageCircuitPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitPort.__resetExchangePageCircuitPort<!xyz.swapee.wc.IExchangePageCircuitPort>} xyz.swapee.wc.IExchangePageCircuitPort._resetExchangePageCircuitPort */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitPort.resetExchangePageCircuitPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitPort.resetExchangePageCircuitPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangePageCircuitPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/09-IExchangePageCircuitCore.xml}  97fce1e6c02101261a8218478ff262c4 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangePageCircuitCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitCore)} xyz.swapee.wc.AbstractExchangePageCircuitCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitCore} xyz.swapee.wc.ExchangePageCircuitCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitCore
 */
xyz.swapee.wc.AbstractExchangePageCircuitCore = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitCore.constructor&xyz.swapee.wc.ExchangePageCircuitCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitCore.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitCore|typeof xyz.swapee.wc.ExchangePageCircuitCore)|(!xyz.swapee.wc.IExchangePageCircuitOuterCore|typeof xyz.swapee.wc.ExchangePageCircuitOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitCore|typeof xyz.swapee.wc.ExchangePageCircuitCore)|(!xyz.swapee.wc.IExchangePageCircuitOuterCore|typeof xyz.swapee.wc.ExchangePageCircuitOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitCore|typeof xyz.swapee.wc.ExchangePageCircuitCore)|(!xyz.swapee.wc.IExchangePageCircuitOuterCore|typeof xyz.swapee.wc.ExchangePageCircuitOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitCore}
 */
xyz.swapee.wc.AbstractExchangePageCircuitCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitCoreCaster&xyz.swapee.wc.IExchangePageCircuitOuterCore)} xyz.swapee.wc.IExchangePageCircuitCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IExchangePageCircuitCore
 */
xyz.swapee.wc.IExchangePageCircuitCore = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangePageCircuitOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IExchangePageCircuitCore.resetCore} */
xyz.swapee.wc.IExchangePageCircuitCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitCore.resetExchangePageCircuitCore} */
xyz.swapee.wc.IExchangePageCircuitCore.prototype.resetExchangePageCircuitCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitCore.Initialese>)} xyz.swapee.wc.ExchangePageCircuitCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitCore} xyz.swapee.wc.IExchangePageCircuitCore.typeof */
/**
 * A concrete class of _IExchangePageCircuitCore_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitCore
 * @implements {xyz.swapee.wc.IExchangePageCircuitCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitCore = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitCore.constructor&xyz.swapee.wc.IExchangePageCircuitCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangePageCircuitCore.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitCore}
 */
xyz.swapee.wc.ExchangePageCircuitCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitCore.
 * @interface xyz.swapee.wc.IExchangePageCircuitCoreFields
 */
xyz.swapee.wc.IExchangePageCircuitCoreFields = class { }
/**
 * The _IExchangePageCircuit_'s memory.
 */
xyz.swapee.wc.IExchangePageCircuitCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangePageCircuitCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IExchangePageCircuitCoreFields.prototype.props = /** @type {xyz.swapee.wc.IExchangePageCircuitCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore} */
xyz.swapee.wc.RecordIExchangePageCircuitCore

/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore} xyz.swapee.wc.BoundIExchangePageCircuitCore */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitCore} xyz.swapee.wc.BoundExchangePageCircuitCore */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitOuterCore.Model} xyz.swapee.wc.IExchangePageCircuitCore.Model The _IExchangePageCircuit_'s memory. */

/**
 * Contains getters to cast the _IExchangePageCircuitCore_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitCoreCaster
 */
xyz.swapee.wc.IExchangePageCircuitCoreCaster = class { }
/**
 * Cast the _IExchangePageCircuitCore_ instance into the _BoundIExchangePageCircuitCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitCore}
 */
xyz.swapee.wc.IExchangePageCircuitCoreCaster.prototype.asIExchangePageCircuitCore
/**
 * Access the _ExchangePageCircuitCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitCore}
 */
xyz.swapee.wc.IExchangePageCircuitCoreCaster.prototype.superExchangePageCircuitCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.__resetCore<!xyz.swapee.wc.IExchangePageCircuitCore>} xyz.swapee.wc.IExchangePageCircuitCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitCore.resetCore} */
/**
 * Resets the _IExchangePageCircuit_ core.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitCore.__resetExchangePageCircuitCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitCore.__resetExchangePageCircuitCore<!xyz.swapee.wc.IExchangePageCircuitCore>} xyz.swapee.wc.IExchangePageCircuitCore._resetExchangePageCircuitCore */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitCore.resetExchangePageCircuitCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitCore.resetExchangePageCircuitCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangePageCircuitCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/10-IExchangePageCircuitProcessor.xml}  ba22b05868e01854079ba8d098f256ca */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitComputer.Initialese&xyz.swapee.wc.IExchangePageCircuitController.Initialese} xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitProcessor)} xyz.swapee.wc.AbstractExchangePageCircuitProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitProcessor} xyz.swapee.wc.ExchangePageCircuitProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitProcessor
 */
xyz.swapee.wc.AbstractExchangePageCircuitProcessor = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitProcessor.constructor&xyz.swapee.wc.ExchangePageCircuitProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitProcessor.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!xyz.swapee.wc.IExchangePageCircuitCore|typeof xyz.swapee.wc.ExchangePageCircuitCore)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitProcessor}
 */
xyz.swapee.wc.AbstractExchangePageCircuitProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitProcessor}
 */
xyz.swapee.wc.AbstractExchangePageCircuitProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!xyz.swapee.wc.IExchangePageCircuitCore|typeof xyz.swapee.wc.ExchangePageCircuitCore)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitProcessor}
 */
xyz.swapee.wc.AbstractExchangePageCircuitProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!xyz.swapee.wc.IExchangePageCircuitCore|typeof xyz.swapee.wc.ExchangePageCircuitCore)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitProcessor}
 */
xyz.swapee.wc.AbstractExchangePageCircuitProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitProcessor} xyz.swapee.wc.ExchangePageCircuitProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitProcessorCaster&xyz.swapee.wc.IExchangePageCircuitComputer&xyz.swapee.wc.IExchangePageCircuitCore&xyz.swapee.wc.IExchangePageCircuitController)} xyz.swapee.wc.IExchangePageCircuitProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController} xyz.swapee.wc.IExchangePageCircuitController.typeof */
/**
 * The processor to compute changes to the memory for the _IExchangePageCircuit_.
 * @interface xyz.swapee.wc.IExchangePageCircuitProcessor
 */
xyz.swapee.wc.IExchangePageCircuitProcessor = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangePageCircuitComputer.typeof&xyz.swapee.wc.IExchangePageCircuitCore.typeof&xyz.swapee.wc.IExchangePageCircuitController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese>)} xyz.swapee.wc.ExchangePageCircuitProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitProcessor} xyz.swapee.wc.IExchangePageCircuitProcessor.typeof */
/**
 * A concrete class of _IExchangePageCircuitProcessor_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitProcessor
 * @implements {xyz.swapee.wc.IExchangePageCircuitProcessor} The processor to compute changes to the memory for the _IExchangePageCircuit_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitProcessor = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitProcessor.constructor&xyz.swapee.wc.IExchangePageCircuitProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitProcessor}
 */
xyz.swapee.wc.ExchangePageCircuitProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangePageCircuitProcessor} */
xyz.swapee.wc.RecordIExchangePageCircuitProcessor

/** @typedef {xyz.swapee.wc.IExchangePageCircuitProcessor} xyz.swapee.wc.BoundIExchangePageCircuitProcessor */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitProcessor} xyz.swapee.wc.BoundExchangePageCircuitProcessor */

/**
 * Contains getters to cast the _IExchangePageCircuitProcessor_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitProcessorCaster
 */
xyz.swapee.wc.IExchangePageCircuitProcessorCaster = class { }
/**
 * Cast the _IExchangePageCircuitProcessor_ instance into the _BoundIExchangePageCircuitProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitProcessor}
 */
xyz.swapee.wc.IExchangePageCircuitProcessorCaster.prototype.asIExchangePageCircuitProcessor
/**
 * Access the _ExchangePageCircuitProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitProcessor}
 */
xyz.swapee.wc.IExchangePageCircuitProcessorCaster.prototype.superExchangePageCircuitProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/100-ExchangePageCircuitMemory.xml}  e1ded2ea3333ec0506a0560c0d820ce1 */
/**
 * The memory of the _IExchangePageCircuit_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.ExchangePageCircuitMemory
 */
xyz.swapee.wc.ExchangePageCircuitMemory = class { }
/**
 * The id of a created transaction. Default empty string.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.id = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.type = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.amountFrom = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.cryptoIn = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.cryptoOut = /** @type {string} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.notId = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.fixedRate = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.ExchangePageCircuitMemory.prototype.floatingRate = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/102-ExchangePageCircuitInputs.xml}  048b96ddd2e9a681a62fc3f01ea2d614 */
/**
 * The inputs of the _IExchangePageCircuit_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.ExchangePageCircuitInputs
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs = class { }
/**
 * The id of a created transaction. Default empty string.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.id = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.type = /** @type {string|undefined} */ (void 0)
/**
 * Default `0.1`.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.amountFrom = /** @type {string|undefined} */ (void 0)
/**
 * Default `BTC`.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.cryptoIn = /** @type {string|undefined} */ (void 0)
/**
 * Default `ETH`.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.cryptoOut = /** @type {string|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.notId = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.fixedRate = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.ExchangePageCircuitInputs.prototype.floatingRate = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/11-IExchangePageCircuit.xml}  fbc10566fe355dc19dcf60d9e7f6042b */
/**
 * An atomic wrapper for the _IExchangePageCircuit_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.ExchangePageCircuitEnv
 */
xyz.swapee.wc.ExchangePageCircuitEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.ExchangePageCircuitEnv.prototype.exchangePageCircuit = /** @type {xyz.swapee.wc.IExchangePageCircuit} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.IExchangePageCircuitController.Inputs>&xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese&xyz.swapee.wc.IExchangePageCircuitComputer.Initialese&xyz.swapee.wc.IExchangePageCircuitController.Initialese} xyz.swapee.wc.IExchangePageCircuit.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuit)} xyz.swapee.wc.AbstractExchangePageCircuit.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuit} xyz.swapee.wc.ExchangePageCircuit.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuit` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuit
 */
xyz.swapee.wc.AbstractExchangePageCircuit = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuit.constructor&xyz.swapee.wc.ExchangePageCircuit.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuit.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuit
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuit.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuit} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuit|typeof xyz.swapee.wc.ExchangePageCircuit)|(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuit}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuit.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuit}
 */
xyz.swapee.wc.AbstractExchangePageCircuit.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuit}
 */
xyz.swapee.wc.AbstractExchangePageCircuit.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuit|typeof xyz.swapee.wc.ExchangePageCircuit)|(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuit}
 */
xyz.swapee.wc.AbstractExchangePageCircuit.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuit|typeof xyz.swapee.wc.ExchangePageCircuit)|(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuit}
 */
xyz.swapee.wc.AbstractExchangePageCircuit.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuit.Initialese[]) => xyz.swapee.wc.IExchangePageCircuit} xyz.swapee.wc.ExchangePageCircuitConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuit.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IExchangePageCircuit.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IExchangePageCircuit.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IExchangePageCircuit.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.ExchangePageCircuitMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.ExchangePageCircuitClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitCaster&xyz.swapee.wc.IExchangePageCircuitProcessor&xyz.swapee.wc.IExchangePageCircuitComputer&xyz.swapee.wc.IExchangePageCircuitController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.IExchangePageCircuitController.Inputs, !xyz.swapee.wc.ExchangePageCircuitLand>)} xyz.swapee.wc.IExchangePageCircuit.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IExchangePageCircuit
 */
xyz.swapee.wc.IExchangePageCircuit = class extends /** @type {xyz.swapee.wc.IExchangePageCircuit.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangePageCircuitProcessor.typeof&xyz.swapee.wc.IExchangePageCircuitComputer.typeof&xyz.swapee.wc.IExchangePageCircuitController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuit* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuit.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuit.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuit&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuit.Initialese>)} xyz.swapee.wc.ExchangePageCircuit.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuit} xyz.swapee.wc.IExchangePageCircuit.typeof */
/**
 * A concrete class of _IExchangePageCircuit_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuit
 * @implements {xyz.swapee.wc.IExchangePageCircuit} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuit.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuit = class extends /** @type {xyz.swapee.wc.ExchangePageCircuit.constructor&xyz.swapee.wc.IExchangePageCircuit.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuit* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuit.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuit* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuit.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuit.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuit}
 */
xyz.swapee.wc.ExchangePageCircuit.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuit.
 * @interface xyz.swapee.wc.IExchangePageCircuitFields
 */
xyz.swapee.wc.IExchangePageCircuitFields = class { }
/**
 * The input pins of the _IExchangePageCircuit_ port.
 */
xyz.swapee.wc.IExchangePageCircuitFields.prototype.pinout = /** @type {!xyz.swapee.wc.IExchangePageCircuit.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuit} */
xyz.swapee.wc.RecordIExchangePageCircuit

/** @typedef {xyz.swapee.wc.IExchangePageCircuit} xyz.swapee.wc.BoundIExchangePageCircuit */

/** @typedef {xyz.swapee.wc.ExchangePageCircuit} xyz.swapee.wc.BoundExchangePageCircuit */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.Inputs} xyz.swapee.wc.IExchangePageCircuit.Pinout The input pins of the _IExchangePageCircuit_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangePageCircuitController.Inputs>)} xyz.swapee.wc.IExchangePageCircuitBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IExchangePageCircuitBuffer
 */
xyz.swapee.wc.IExchangePageCircuitBuffer = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangePageCircuitBuffer.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitBuffer

/**
 * A concrete class of _IExchangePageCircuitBuffer_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitBuffer
 * @implements {xyz.swapee.wc.IExchangePageCircuitBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.ExchangePageCircuitBuffer = class extends xyz.swapee.wc.IExchangePageCircuitBuffer { }
xyz.swapee.wc.ExchangePageCircuitBuffer.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitBuffer

/**
 * Contains getters to cast the _IExchangePageCircuit_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitCaster
 */
xyz.swapee.wc.IExchangePageCircuitCaster = class { }
/**
 * Cast the _IExchangePageCircuit_ instance into the _BoundIExchangePageCircuit_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuit}
 */
xyz.swapee.wc.IExchangePageCircuitCaster.prototype.asIExchangePageCircuit
/**
 * Access the _ExchangePageCircuit_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuit}
 */
xyz.swapee.wc.IExchangePageCircuitCaster.prototype.superExchangePageCircuit

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/110-ExchangePageCircuitSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangePageCircuitMemoryPQs
 */
xyz.swapee.wc.ExchangePageCircuitMemoryPQs = class {
  constructor() {
    /**
     * `i9b02`
     */
    this.onlyFixedRate=/** @type {string} */ (void 0)
    /**
     * `h2c0a`
     */
    this.onlyFloatingRate=/** @type {string} */ (void 0)
    /**
     * `h48e6`
     */
    this.amountFrom=/** @type {string} */ (void 0)
    /**
     * `jdc9f`
     */
    this.amountTo=/** @type {string} */ (void 0)
    /**
     * `e212d`
     */
    this.cryptoIn=/** @type {string} */ (void 0)
    /**
     * `c60b5`
     */
    this.cryptoOut=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangePageCircuitMemoryPQs.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangePageCircuitMemoryQPs
 * @dict
 */
xyz.swapee.wc.ExchangePageCircuitMemoryQPs = class { }
/**
 * `onlyFixedRate`
 */
xyz.swapee.wc.ExchangePageCircuitMemoryQPs.prototype.i9b02 = /** @type {string} */ (void 0)
/**
 * `onlyFloatingRate`
 */
xyz.swapee.wc.ExchangePageCircuitMemoryQPs.prototype.h2c0a = /** @type {string} */ (void 0)
/**
 * `amountFrom`
 */
xyz.swapee.wc.ExchangePageCircuitMemoryQPs.prototype.h48e6 = /** @type {string} */ (void 0)
/**
 * `amountTo`
 */
xyz.swapee.wc.ExchangePageCircuitMemoryQPs.prototype.jdc9f = /** @type {string} */ (void 0)
/**
 * `cryptoIn`
 */
xyz.swapee.wc.ExchangePageCircuitMemoryQPs.prototype.e212d = /** @type {string} */ (void 0)
/**
 * `cryptoOut`
 */
xyz.swapee.wc.ExchangePageCircuitMemoryQPs.prototype.c60b5 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitMemoryPQs)} xyz.swapee.wc.ExchangePageCircuitInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitMemoryPQs} xyz.swapee.wc.ExchangePageCircuitMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangePageCircuitInputsPQs
 */
xyz.swapee.wc.ExchangePageCircuitInputsPQs = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitInputsPQs.constructor&xyz.swapee.wc.ExchangePageCircuitMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangePageCircuitInputsPQs.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitInputsPQs

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitMemoryPQs)} xyz.swapee.wc.ExchangePageCircuitInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangePageCircuitInputsQPs
 * @dict
 */
xyz.swapee.wc.ExchangePageCircuitInputsQPs = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitInputsQPs.constructor&xyz.swapee.wc.ExchangePageCircuitMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangePageCircuitInputsQPs.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangePageCircuitVdusPQs
 */
xyz.swapee.wc.ExchangePageCircuitVdusPQs = class {
  constructor() {
    /**
     * `c3c11`
     */
    this.CryptoInLa=/** @type {string} */ (void 0)
    /**
     * `c3c12`
     */
    this.CryptoOutLa=/** @type {string} */ (void 0)
    /**
     * `c3c13`
     */
    this.AmountFromLa=/** @type {string} */ (void 0)
    /**
     * `c3c14`
     */
    this.CryptoSelectIn=/** @type {string} */ (void 0)
    /**
     * `c3c15`
     */
    this.CryptoSelectOut=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangePageCircuitVdusPQs.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangePageCircuitVdusQPs
 * @dict
 */
xyz.swapee.wc.ExchangePageCircuitVdusQPs = class { }
/**
 * `CryptoInLa`
 */
xyz.swapee.wc.ExchangePageCircuitVdusQPs.prototype.c3c11 = /** @type {string} */ (void 0)
/**
 * `CryptoOutLa`
 */
xyz.swapee.wc.ExchangePageCircuitVdusQPs.prototype.c3c12 = /** @type {string} */ (void 0)
/**
 * `AmountFromLa`
 */
xyz.swapee.wc.ExchangePageCircuitVdusQPs.prototype.c3c13 = /** @type {string} */ (void 0)
/**
 * `CryptoSelectIn`
 */
xyz.swapee.wc.ExchangePageCircuitVdusQPs.prototype.c3c14 = /** @type {string} */ (void 0)
/**
 * `CryptoSelectOut`
 */
xyz.swapee.wc.ExchangePageCircuitVdusQPs.prototype.c3c15 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/12-IExchangePageCircuitHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtilFields)} xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.router} */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IExchangePageCircuitHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitHtmlComponentUtil
 * @implements {xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil} ‎
 */
xyz.swapee.wc.ExchangePageCircuitHtmlComponentUtil = class extends xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil { }
xyz.swapee.wc.ExchangePageCircuitHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitHtmlComponentUtil

/**
 * Fields of the IExchangePageCircuitHtmlComponentUtil.
 * @interface xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtilFields
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil} */
xyz.swapee.wc.RecordIExchangePageCircuitHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil} xyz.swapee.wc.BoundIExchangePageCircuitHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitHtmlComponentUtil} xyz.swapee.wc.BoundExchangePageCircuitHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPort} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPort} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IOffersFilterPort} OffersFilter
 * @prop {typeof xyz.swapee.wc.IExchangeBrokerPort} ExchangeBroker
 * @prop {typeof xyz.swapee.wc.IDealBrokerPort} DealBroker
 * @prop {typeof xyz.swapee.wc.IOfferExchangePort} OfferExchange
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPort} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowPort} ExchangeIdRow
 * @prop {typeof xyz.swapee.wc.ITransactionInfoPort} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadPort} TransactionInfoHead
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadPort} TransactionInfoHeadMob
 * @prop {typeof xyz.swapee.wc.IExchangePageCircuitPort} ExchangePageCircuit The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectIn
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut
 * @prop {!xyz.swapee.wc.OffersFilterMemory} OffersFilter
 * @prop {!xyz.swapee.wc.ExchangeBrokerMemory} ExchangeBroker
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker
 * @prop {!xyz.swapee.wc.OfferExchangeMemory} OfferExchange
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.ExchangeIdRowMemory} ExchangeIdRow
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} TransactionInfo
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHead
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHeadMob
 * @prop {!xyz.swapee.wc.ExchangePageCircuitMemory} ExchangePageCircuit
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.ICryptoSelect.Pinout} CryptoSelectIn
 * @prop {!xyz.swapee.wc.ICryptoSelect.Pinout} CryptoSelectOut
 * @prop {!xyz.swapee.wc.IOffersFilter.Pinout} OffersFilter
 * @prop {!xyz.swapee.wc.IExchangeBroker.Pinout} ExchangeBroker
 * @prop {!xyz.swapee.wc.IDealBroker.Pinout} DealBroker
 * @prop {!xyz.swapee.wc.IOfferExchange.Pinout} OfferExchange
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} ExchangeIntent
 * @prop {!xyz.swapee.wc.IExchangeIdRow.Pinout} ExchangeIdRow
 * @prop {!xyz.swapee.wc.ITransactionInfo.Pinout} TransactionInfo
 * @prop {!xyz.swapee.wc.ITransactionInfoHead.Pinout} TransactionInfoHead
 * @prop {!xyz.swapee.wc.ITransactionInfoHead.Pinout} TransactionInfoHeadMob
 * @prop {!xyz.swapee.wc.IExchangePageCircuit.Pinout} ExchangePageCircuit
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.__router<!xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil>} xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `CryptoSelectIn` _typeof ICryptoSelectPort_
 * - `CryptoSelectOut` _typeof ICryptoSelectPort_
 * - `OffersFilter` _typeof IOffersFilterPort_
 * - `ExchangeBroker` _typeof IExchangeBrokerPort_
 * - `DealBroker` _typeof IDealBrokerPort_
 * - `OfferExchange` _typeof IOfferExchangePort_
 * - `ExchangeIntent` _typeof IExchangeIntentPort_
 * - `ExchangeIdRow` _typeof IExchangeIdRowPort_
 * - `TransactionInfo` _typeof ITransactionInfoPort_
 * - `TransactionInfoHead` _typeof ITransactionInfoHeadPort_
 * - `TransactionInfoHeadMob` _typeof ITransactionInfoHeadPort_
 * - `ExchangePageCircuit` _typeof IExchangePageCircuitPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `CryptoSelectIn` _!CryptoSelectMemory_
 * - `CryptoSelectOut` _!CryptoSelectMemory_
 * - `OffersFilter` _!OffersFilterMemory_
 * - `ExchangeBroker` _!ExchangeBrokerMemory_
 * - `DealBroker` _!DealBrokerMemory_
 * - `OfferExchange` _!OfferExchangeMemory_
 * - `ExchangeIntent` _!ExchangeIntentMemory_
 * - `ExchangeIdRow` _!ExchangeIdRowMemory_
 * - `TransactionInfo` _!TransactionInfoMemory_
 * - `TransactionInfoHead` _!TransactionInfoHeadMemory_
 * - `TransactionInfoHeadMob` _!TransactionInfoHeadMemory_
 * - `ExchangePageCircuit` _!ExchangePageCircuitMemory_
 * @param {!xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `CryptoSelectIn` _!ICryptoSelect.Pinout_
 * - `CryptoSelectOut` _!ICryptoSelect.Pinout_
 * - `OffersFilter` _!IOffersFilter.Pinout_
 * - `ExchangeBroker` _!IExchangeBroker.Pinout_
 * - `DealBroker` _!IDealBroker.Pinout_
 * - `OfferExchange` _!IOfferExchange.Pinout_
 * - `ExchangeIntent` _!IExchangeIntent.Pinout_
 * - `ExchangeIdRow` _!IExchangeIdRow.Pinout_
 * - `TransactionInfo` _!ITransactionInfo.Pinout_
 * - `TransactionInfoHead` _!ITransactionInfoHead.Pinout_
 * - `TransactionInfoHeadMob` _!ITransactionInfoHead.Pinout_
 * - `ExchangePageCircuit` _!IExchangePageCircuit.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/12-IExchangePageCircuitHtmlComponent.xml}  d8e7e8d67a76bc78583036b72bd89366 */
/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitController.Initialese&xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese&xyz.swapee.wc.IExchangePageCircuit.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IExchangePageCircuitGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IExchangePageCircuitProcessor.Initialese&xyz.swapee.wc.IExchangePageCircuitComputer.Initialese} xyz.swapee.wc.IExchangePageCircuitHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitHtmlComponent)} xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent} xyz.swapee.wc.ExchangePageCircuitHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent
 */
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.constructor&xyz.swapee.wc.ExchangePageCircuitHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitHtmlComponent|typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent)|(!xyz.swapee.wc.back.IExchangePageCircuitController|typeof xyz.swapee.wc.back.ExchangePageCircuitController)|(!xyz.swapee.wc.back.IExchangePageCircuitScreen|typeof xyz.swapee.wc.back.ExchangePageCircuitScreen)|(!xyz.swapee.wc.IExchangePageCircuit|typeof xyz.swapee.wc.ExchangePageCircuit)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IExchangePageCircuitGPU|typeof xyz.swapee.wc.ExchangePageCircuitGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitHtmlComponent|typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent)|(!xyz.swapee.wc.back.IExchangePageCircuitController|typeof xyz.swapee.wc.back.ExchangePageCircuitController)|(!xyz.swapee.wc.back.IExchangePageCircuitScreen|typeof xyz.swapee.wc.back.ExchangePageCircuitScreen)|(!xyz.swapee.wc.IExchangePageCircuit|typeof xyz.swapee.wc.ExchangePageCircuit)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IExchangePageCircuitGPU|typeof xyz.swapee.wc.ExchangePageCircuitGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitHtmlComponent|typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent)|(!xyz.swapee.wc.back.IExchangePageCircuitController|typeof xyz.swapee.wc.back.ExchangePageCircuitController)|(!xyz.swapee.wc.back.IExchangePageCircuitScreen|typeof xyz.swapee.wc.back.ExchangePageCircuitScreen)|(!xyz.swapee.wc.IExchangePageCircuit|typeof xyz.swapee.wc.ExchangePageCircuit)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IExchangePageCircuitGPU|typeof xyz.swapee.wc.ExchangePageCircuitGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangePageCircuitProcessor|typeof xyz.swapee.wc.ExchangePageCircuitProcessor)|(!xyz.swapee.wc.IExchangePageCircuitComputer|typeof xyz.swapee.wc.ExchangePageCircuitComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitHtmlComponent.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitHtmlComponent} xyz.swapee.wc.ExchangePageCircuitHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitHtmlComponentCaster&xyz.swapee.wc.back.IExchangePageCircuitController&xyz.swapee.wc.back.IExchangePageCircuitScreen&xyz.swapee.wc.IExchangePageCircuit&com.webcircuits.ILanded<!xyz.swapee.wc.ExchangePageCircuitLand>&xyz.swapee.wc.IExchangePageCircuitGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.IExchangePageCircuitController.Inputs, !HTMLDivElement, !xyz.swapee.wc.ExchangePageCircuitLand>&xyz.swapee.wc.IExchangePageCircuitProcessor&xyz.swapee.wc.IExchangePageCircuitComputer)} xyz.swapee.wc.IExchangePageCircuitHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangePageCircuitController} xyz.swapee.wc.back.IExchangePageCircuitController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IExchangePageCircuitScreen} xyz.swapee.wc.back.IExchangePageCircuitScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuit} xyz.swapee.wc.IExchangePageCircuit.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitGPU} xyz.swapee.wc.IExchangePageCircuitGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitProcessor} xyz.swapee.wc.IExchangePageCircuitProcessor.typeof */
/**
 * The _IExchangePageCircuit_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IExchangePageCircuitHtmlComponent
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponent = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangePageCircuitController.typeof&xyz.swapee.wc.back.IExchangePageCircuitScreen.typeof&xyz.swapee.wc.IExchangePageCircuit.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IExchangePageCircuitGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IExchangePageCircuitProcessor.typeof&xyz.swapee.wc.IExchangePageCircuitComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitHtmlComponent.Initialese>)} xyz.swapee.wc.ExchangePageCircuitHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitHtmlComponent} xyz.swapee.wc.IExchangePageCircuitHtmlComponent.typeof */
/**
 * A concrete class of _IExchangePageCircuitHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitHtmlComponent
 * @implements {xyz.swapee.wc.IExchangePageCircuitHtmlComponent} The _IExchangePageCircuit_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitHtmlComponent = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitHtmlComponent.constructor&xyz.swapee.wc.IExchangePageCircuitHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent}
 */
xyz.swapee.wc.ExchangePageCircuitHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangePageCircuitHtmlComponent} */
xyz.swapee.wc.RecordIExchangePageCircuitHtmlComponent

/** @typedef {xyz.swapee.wc.IExchangePageCircuitHtmlComponent} xyz.swapee.wc.BoundIExchangePageCircuitHtmlComponent */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitHtmlComponent} xyz.swapee.wc.BoundExchangePageCircuitHtmlComponent */

/**
 * Contains getters to cast the _IExchangePageCircuitHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitHtmlComponentCaster
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentCaster = class { }
/**
 * Cast the _IExchangePageCircuitHtmlComponent_ instance into the _BoundIExchangePageCircuitHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitHtmlComponent}
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentCaster.prototype.asIExchangePageCircuitHtmlComponent
/**
 * Access the _ExchangePageCircuitHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitHtmlComponent}
 */
xyz.swapee.wc.IExchangePageCircuitHtmlComponentCaster.prototype.superExchangePageCircuitHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/130-IExchangePageCircuitElement.xml}  8ef968829df5a65181d3c78c6880b75f */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.ExchangePageCircuitLand>&guest.maurice.IMilleu.Initialese<!(xyz.swapee.wc.ICryptoSelect|xyz.swapee.wc.IOffersFilter|xyz.swapee.wc.IExchangeBroker|xyz.swapee.wc.IDealBroker|xyz.swapee.wc.IOfferExchange|xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IExchangeIdRow|xyz.swapee.wc.ITransactionInfo|xyz.swapee.wc.ITransactionInfoHead)>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.IExchangePageCircuitElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IExchangePageCircuitElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitElement)} xyz.swapee.wc.AbstractExchangePageCircuitElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitElement} xyz.swapee.wc.ExchangePageCircuitElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitElement` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitElement
 */
xyz.swapee.wc.AbstractExchangePageCircuitElement = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitElement.constructor&xyz.swapee.wc.ExchangePageCircuitElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitElement.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitElement.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitElement|typeof xyz.swapee.wc.ExchangePageCircuitElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitElement}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElement}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitElement|typeof xyz.swapee.wc.ExchangePageCircuitElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElement}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitElement|typeof xyz.swapee.wc.ExchangePageCircuitElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElement}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitElement.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitElement} xyz.swapee.wc.ExchangePageCircuitElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitElementFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.IExchangePageCircuitElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.IExchangePageCircuitElement.Inputs, !xyz.swapee.wc.ExchangePageCircuitLand>&com.webcircuits.ILanded<!xyz.swapee.wc.ExchangePageCircuitLand>&guest.maurice.IMilleu<!(xyz.swapee.wc.ICryptoSelect|xyz.swapee.wc.IOffersFilter|xyz.swapee.wc.IExchangeBroker|xyz.swapee.wc.IDealBroker|xyz.swapee.wc.IOfferExchange|xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IExchangeIdRow|xyz.swapee.wc.ITransactionInfo|xyz.swapee.wc.ITransactionInfoHead)>)} xyz.swapee.wc.IExchangePageCircuitElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IExchangePageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IExchangePageCircuitElement
 */
xyz.swapee.wc.IExchangePageCircuitElement = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.solder} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.render} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.build} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildCryptoSelectIn} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildCryptoSelectIn = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildCryptoSelectOut} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildCryptoSelectOut = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildOffersFilter} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildOffersFilter = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeBroker} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildExchangeBroker = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildDealBroker} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildDealBroker = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildOfferExchange} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildOfferExchange = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeIntent} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildExchangeIntent = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeIdRow} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildExchangeIdRow = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfo} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildTransactionInfo = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfoHead} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildTransactionInfoHead = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfoHeadMob} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.buildTransactionInfoHeadMob = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.short} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.server} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitElement.inducer} */
xyz.swapee.wc.IExchangePageCircuitElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitElement&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitElement.Initialese>)} xyz.swapee.wc.ExchangePageCircuitElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement} xyz.swapee.wc.IExchangePageCircuitElement.typeof */
/**
 * A concrete class of _IExchangePageCircuitElement_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitElement
 * @implements {xyz.swapee.wc.IExchangePageCircuitElement} A component description.
 *
 * The _IExchangePageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitElement.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitElement = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitElement.constructor&xyz.swapee.wc.IExchangePageCircuitElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElement}
 */
xyz.swapee.wc.ExchangePageCircuitElement.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitElement.
 * @interface xyz.swapee.wc.IExchangePageCircuitElementFields
 */
xyz.swapee.wc.IExchangePageCircuitElementFields = class { }
/**
 * The element-specific inputs to the _IExchangePageCircuit_ component.
 */
xyz.swapee.wc.IExchangePageCircuitElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangePageCircuitElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IExchangePageCircuitElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement} */
xyz.swapee.wc.RecordIExchangePageCircuitElement

/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement} xyz.swapee.wc.BoundIExchangePageCircuitElement */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitElement} xyz.swapee.wc.BoundExchangePageCircuitElement */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitPort.Inputs&xyz.swapee.wc.IExchangePageCircuitDisplay.Queries&xyz.swapee.wc.IExchangePageCircuitController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs} xyz.swapee.wc.IExchangePageCircuitElement.Inputs The element-specific inputs to the _IExchangePageCircuit_ component. */

/**
 * Contains getters to cast the _IExchangePageCircuitElement_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitElementCaster
 */
xyz.swapee.wc.IExchangePageCircuitElementCaster = class { }
/**
 * Cast the _IExchangePageCircuitElement_ instance into the _BoundIExchangePageCircuitElement_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitElement}
 */
xyz.swapee.wc.IExchangePageCircuitElementCaster.prototype.asIExchangePageCircuitElement
/**
 * Access the _ExchangePageCircuitElement_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitElement}
 */
xyz.swapee.wc.IExchangePageCircuitElementCaster.prototype.superExchangePageCircuitElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ExchangePageCircuitMemory, props: !xyz.swapee.wc.IExchangePageCircuitElement.Inputs) => Object<string, *>} xyz.swapee.wc.IExchangePageCircuitElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__solder<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._solder */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.ExchangePageCircuitMemory} model The model.
 * - `id` _string_ The id of a created transaction. Default empty string.
 * - `type` _string_
 * Choose between:
 * > - _fixed_
 * > - _floating_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `notId` _boolean_ Default `false`.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatingRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IExchangePageCircuitElement.Inputs} props The element props.
 * - `[id=null]` _&#42;?_ The id of a created transaction. ⤴ *IExchangePageCircuitOuterCore.WeakModel.Id* ⤴ *IExchangePageCircuitOuterCore.WeakModel.Id* Default `null`.
 * - `[type=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.Type* ⤴ *IExchangePageCircuitOuterCore.WeakModel.Type*
 * Choose between:
 * > - _fixed_
 * > - _floating_Default `null`.
 * - `[amountFrom="0.1"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.AmountFrom* ⤴ *IExchangePageCircuitOuterCore.WeakModel.AmountFrom* Default `0.1`.
 * - `[cryptoIn="BTC"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoIn* ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoIn* Default `BTC`.
 * - `[cryptoOut="ETH"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoOut* ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoOut* Default `ETH`.
 * - `[notId=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.NotId* ⤴ *IExchangePageCircuitOuterCore.WeakModel.NotId* Default `null`.
 * - `[fixedRate=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.FixedRate* ⤴ *IExchangePageCircuitOuterCore.WeakModel.FixedRate* Default `null`.
 * - `[floatingRate=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.FloatingRate* ⤴ *IExchangePageCircuitOuterCore.WeakModel.FloatingRate* Default `null`.
 * - `[concealWithTidSel=""]` _string?_ The query to discover the _ConcealWithTid_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[revealWithTidSel=""]` _string?_ The query to discover the _RevealWithTid_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[restartButtonSel=""]` _string?_ The query to discover the _RestartButton_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[concealWithIdSel=""]` _string?_ The query to discover the _ConcealWithId_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[revealWithIdSel=""]` _string?_ The query to discover the _RevealWithId_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[inImWrSel=""]` _string?_ The query to discover the _InImWr_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[outImWrSel=""]` _string?_ The query to discover the _OutImWr_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[cryptoSelectInSel=""]` _string?_ The query to discover the _CryptoSelectIn_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[cryptoSelectOutSel=""]` _string?_ The query to discover the _CryptoSelectOut_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[offersFilterSel=""]` _string?_ The query to discover the _OffersFilter_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeBrokerSel=""]` _string?_ The query to discover the _ExchangeBroker_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[offerExchangeSel=""]` _string?_ The query to discover the _OfferExchange_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeIdRowSel=""]` _string?_ The query to discover the _ExchangeIdRow_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoSel=""]` _string?_ The query to discover the _TransactionInfo_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoHeadSel=""]` _string?_ The query to discover the _TransactionInfoHead_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoHeadMobSel=""]` _string?_ The query to discover the _TransactionInfoHeadMob_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangePageCircuitElementPort.Inputs.NoSolder* Default `false`.
 * - `[concealWithTidOpts]` _!Object?_ The options to pass to the _ConcealWithTid_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts* Default `{}`.
 * - `[revealWithTidOpts]` _!Object?_ The options to pass to the _RevealWithTid_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts* Default `{}`.
 * - `[restartButtonOpts]` _!Object?_ The options to pass to the _RestartButton_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RestartButtonOpts* Default `{}`.
 * - `[cryptoInLaOpts]` _!Object?_ The options to pass to the _CryptoInLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts* Default `{}`.
 * - `[concealWithIdOpts]` _!Object?_ The options to pass to the _ConcealWithId_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts* Default `{}`.
 * - `[revealWithIdOpts]` _!Object?_ The options to pass to the _RevealWithId_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts* Default `{}`.
 * - `[cryptoOutLaOpts]` _!Object?_ The options to pass to the _CryptoOutLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts* Default `{}`.
 * - `[amountFromLaOpts]` _!Object?_ The options to pass to the _AmountFromLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts* Default `{}`.
 * - `[inImWrOpts]` _!Object?_ The options to pass to the _InImWr_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.InImWrOpts* Default `{}`.
 * - `[outImWrOpts]` _!Object?_ The options to pass to the _OutImWr_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OutImWrOpts* Default `{}`.
 * - `[cryptoSelectInOpts]` _!Object?_ The options to pass to the _CryptoSelectIn_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[offersFilterOpts]` _!Object?_ The options to pass to the _OffersFilter_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OffersFilterOpts* Default `{}`.
 * - `[exchangeBrokerOpts]` _!Object?_ The options to pass to the _ExchangeBroker_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * - `[offerExchangeOpts]` _!Object?_ The options to pass to the _OfferExchange_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[exchangeIdRowOpts]` _!Object?_ The options to pass to the _ExchangeIdRow_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts* Default `{}`.
 * - `[transactionInfoOpts]` _!Object?_ The options to pass to the _TransactionInfo_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts* Default `{}`.
 * - `[transactionInfoHeadOpts]` _!Object?_ The options to pass to the _TransactionInfoHead_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts* Default `{}`.
 * - `[transactionInfoHeadMobOpts]` _!Object?_ The options to pass to the _TransactionInfoHeadMob_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IExchangePageCircuitElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangePageCircuitMemory, instance?: !xyz.swapee.wc.IExchangePageCircuitScreen&xyz.swapee.wc.IExchangePageCircuitController) => !engineering.type.VNode} xyz.swapee.wc.IExchangePageCircuitElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__render<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._render */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.ExchangePageCircuitMemory} [model] The model for the view.
 * - `id` _string_ The id of a created transaction. Default empty string.
 * - `type` _string_
 * Choose between:
 * > - _fixed_
 * > - _floating_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `notId` _boolean_ Default `false`.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatingRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IExchangePageCircuitScreen&xyz.swapee.wc.IExchangePageCircuitController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangePageCircuitElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IExchangePageCircuitElement.build.Cores, instances: !xyz.swapee.wc.IExchangePageCircuitElement.build.Instances) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__build<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._build */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IExchangePageCircuitElement.build.Cores} cores The models of components on the land.
 * - `CryptoSelectIn` _!xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `CryptoSelectOut` _!xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `OffersFilter` _!xyz.swapee.wc.IOffersFilterCore.Model_
 * - `ExchangeBroker` _!xyz.swapee.wc.IExchangeBrokerCore.Model_
 * - `DealBroker` _!xyz.swapee.wc.IDealBrokerCore.Model_
 * - `OfferExchange` _!xyz.swapee.wc.IOfferExchangeCore.Model_
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntentCore.Model_
 * - `ExchangeIdRow` _!xyz.swapee.wc.IExchangeIdRowCore.Model_
 * - `TransactionInfo` _!xyz.swapee.wc.ITransactionInfoCore.Model_
 * - `TransactionInfoHead` _!xyz.swapee.wc.ITransactionInfoHeadCore.Model_
 * - `TransactionInfoHeadMob` _!xyz.swapee.wc.ITransactionInfoHeadCore.Model_
 * @param {!xyz.swapee.wc.IExchangePageCircuitElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `CryptoSelectIn` _!xyz.swapee.wc.ICryptoSelect_
 * - `CryptoSelectOut` _!xyz.swapee.wc.ICryptoSelect_
 * - `OffersFilter` _!xyz.swapee.wc.IOffersFilter_
 * - `ExchangeBroker` _!xyz.swapee.wc.IExchangeBroker_
 * - `DealBroker` _!xyz.swapee.wc.IDealBroker_
 * - `OfferExchange` _!xyz.swapee.wc.IOfferExchange_
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntent_
 * - `ExchangeIdRow` _!xyz.swapee.wc.IExchangeIdRow_
 * - `TransactionInfo` _!xyz.swapee.wc.ITransactionInfo_
 * - `TransactionInfoHead` _!xyz.swapee.wc.ITransactionInfoHead_
 * - `TransactionInfoHeadMob` _!xyz.swapee.wc.ITransactionInfoHead_
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectIn
 * @prop {!xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectOut
 * @prop {!xyz.swapee.wc.IOffersFilterCore.Model} OffersFilter
 * @prop {!xyz.swapee.wc.IExchangeBrokerCore.Model} ExchangeBroker
 * @prop {!xyz.swapee.wc.IDealBrokerCore.Model} DealBroker
 * @prop {!xyz.swapee.wc.IOfferExchangeCore.Model} OfferExchange
 * @prop {!xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 * @prop {!xyz.swapee.wc.IExchangeIdRowCore.Model} ExchangeIdRow
 * @prop {!xyz.swapee.wc.ITransactionInfoCore.Model} TransactionInfo
 * @prop {!xyz.swapee.wc.ITransactionInfoHeadCore.Model} TransactionInfoHead
 * @prop {!xyz.swapee.wc.ITransactionInfoHeadCore.Model} TransactionInfoHeadMob
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.ICryptoSelect} CryptoSelectIn
 * @prop {!xyz.swapee.wc.ICryptoSelect} CryptoSelectOut
 * @prop {!xyz.swapee.wc.IOffersFilter} OffersFilter
 * @prop {!xyz.swapee.wc.IExchangeBroker} ExchangeBroker
 * @prop {!xyz.swapee.wc.IDealBroker} DealBroker
 * @prop {!xyz.swapee.wc.IOfferExchange} OfferExchange
 * @prop {!xyz.swapee.wc.IExchangeIntent} ExchangeIntent
 * @prop {!xyz.swapee.wc.IExchangeIdRow} ExchangeIdRow
 * @prop {!xyz.swapee.wc.ITransactionInfo} TransactionInfo
 * @prop {!xyz.swapee.wc.ITransactionInfoHead} TransactionInfoHead
 * @prop {!xyz.swapee.wc.ITransactionInfoHead} TransactionInfoHeadMob
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ICryptoSelectCore.Model, instance: !xyz.swapee.wc.ICryptoSelect) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildCryptoSelectIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildCryptoSelectIn<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildCryptoSelectIn */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildCryptoSelectIn} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ICryptoSelect_ component.
 * @param {!xyz.swapee.wc.ICryptoSelectCore.Model} model
 * @param {!xyz.swapee.wc.ICryptoSelect} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildCryptoSelectIn = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ICryptoSelectCore.Model, instance: !xyz.swapee.wc.ICryptoSelect) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildCryptoSelectOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildCryptoSelectOut<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildCryptoSelectOut */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildCryptoSelectOut} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ICryptoSelect_ component.
 * @param {!xyz.swapee.wc.ICryptoSelectCore.Model} model
 * @param {!xyz.swapee.wc.ICryptoSelect} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildCryptoSelectOut = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IOffersFilterCore.Model, instance: !xyz.swapee.wc.IOffersFilter) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildOffersFilter
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildOffersFilter<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildOffersFilter */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildOffersFilter} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IOffersFilter_ component.
 * @param {!xyz.swapee.wc.IOffersFilterCore.Model} model
 * @param {!xyz.swapee.wc.IOffersFilter} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildOffersFilter = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeBrokerCore.Model, instance: !xyz.swapee.wc.IExchangeBroker) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildExchangeBroker
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildExchangeBroker<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildExchangeBroker */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeBroker} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeBroker_ component.
 * @param {!xyz.swapee.wc.IExchangeBrokerCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeBroker} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeBroker = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IDealBrokerCore.Model, instance: !xyz.swapee.wc.IDealBroker) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildDealBroker
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildDealBroker<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildDealBroker */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildDealBroker} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IDealBroker_ component.
 * @param {!xyz.swapee.wc.IDealBrokerCore.Model} model
 * @param {!xyz.swapee.wc.IDealBroker} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildDealBroker = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IOfferExchangeCore.Model, instance: !xyz.swapee.wc.IOfferExchange) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildOfferExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildOfferExchange<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildOfferExchange */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildOfferExchange} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IOfferExchange_ component.
 * @param {!xyz.swapee.wc.IOfferExchangeCore.Model} model
 * @param {!xyz.swapee.wc.IOfferExchange} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildOfferExchange = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeIntentCore.Model, instance: !xyz.swapee.wc.IExchangeIntent) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildExchangeIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildExchangeIntent<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildExchangeIntent */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeIntent} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeIntent_ component.
 * @param {!xyz.swapee.wc.IExchangeIntentCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeIntent} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeIntent = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeIdRowCore.Model, instance: !xyz.swapee.wc.IExchangeIdRow) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildExchangeIdRow
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildExchangeIdRow<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildExchangeIdRow */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeIdRow} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeIdRow_ component.
 * @param {!xyz.swapee.wc.IExchangeIdRowCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeIdRow} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildExchangeIdRow = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ITransactionInfoCore.Model, instance: !xyz.swapee.wc.ITransactionInfo) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildTransactionInfo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildTransactionInfo<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildTransactionInfo */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfo} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ITransactionInfo_ component.
 * @param {!xyz.swapee.wc.ITransactionInfoCore.Model} model
 * @param {!xyz.swapee.wc.ITransactionInfo} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfo = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ITransactionInfoHeadCore.Model, instance: !xyz.swapee.wc.ITransactionInfoHead) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildTransactionInfoHead
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildTransactionInfoHead<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildTransactionInfoHead */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfoHead} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ITransactionInfoHead_ component.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadCore.Model} model
 * @param {!xyz.swapee.wc.ITransactionInfoHead} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfoHead = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ITransactionInfoHeadCore.Model, instance: !xyz.swapee.wc.ITransactionInfoHead) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__buildTransactionInfoHeadMob
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__buildTransactionInfoHeadMob<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._buildTransactionInfoHeadMob */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfoHeadMob} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ITransactionInfoHead_ component.
 * @param {!xyz.swapee.wc.ITransactionInfoHeadCore.Model} model
 * @param {!xyz.swapee.wc.ITransactionInfoHead} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.buildTransactionInfoHeadMob = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ExchangePageCircuitMemory, ports: !xyz.swapee.wc.IExchangePageCircuitElement.short.Ports, cores: !xyz.swapee.wc.IExchangePageCircuitElement.short.Cores) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__short<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._short */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.ExchangePageCircuitMemory} model The model from which to feed properties to peer's ports.
 * - `id` _string_ The id of a created transaction. Default empty string.
 * - `type` _string_
 * Choose between:
 * > - _fixed_
 * > - _floating_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `notId` _boolean_ Default `false`.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatingRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IExchangePageCircuitElement.short.Ports} ports The ports of the peers.
 * - `CryptoSelectIn` _typeof xyz.swapee.wc.ICryptoSelectPortInterface_
 * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectPortInterface_
 * - `OffersFilter` _typeof xyz.swapee.wc.IOffersFilterPortInterface_
 * - `ExchangeBroker` _typeof xyz.swapee.wc.IExchangeBrokerPortInterface_
 * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerPortInterface_
 * - `OfferExchange` _typeof xyz.swapee.wc.IOfferExchangePortInterface_
 * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentPortInterface_
 * - `ExchangeIdRow` _typeof xyz.swapee.wc.IExchangeIdRowPortInterface_
 * - `TransactionInfo` _typeof xyz.swapee.wc.ITransactionInfoPortInterface_
 * - `TransactionInfoHead` _typeof xyz.swapee.wc.ITransactionInfoHeadPortInterface_
 * - `TransactionInfoHeadMob` _typeof xyz.swapee.wc.ITransactionInfoHeadPortInterface_
 * @param {!xyz.swapee.wc.IExchangePageCircuitElement.short.Cores} cores The cores of the peers.
 * - `CryptoSelectIn` _xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `CryptoSelectOut` _xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `OffersFilter` _xyz.swapee.wc.IOffersFilterCore.Model_
 * - `ExchangeBroker` _xyz.swapee.wc.IExchangeBrokerCore.Model_
 * - `DealBroker` _xyz.swapee.wc.IDealBrokerCore.Model_
 * - `OfferExchange` _xyz.swapee.wc.IOfferExchangeCore.Model_
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntentCore.Model_
 * - `ExchangeIdRow` _xyz.swapee.wc.IExchangeIdRowCore.Model_
 * - `TransactionInfo` _xyz.swapee.wc.ITransactionInfoCore.Model_
 * - `TransactionInfoHead` _xyz.swapee.wc.ITransactionInfoHeadCore.Model_
 * - `TransactionInfoHeadMob` _xyz.swapee.wc.ITransactionInfoHeadCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPortInterface} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPortInterface} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IOffersFilterPortInterface} OffersFilter
 * @prop {typeof xyz.swapee.wc.IExchangeBrokerPortInterface} ExchangeBroker
 * @prop {typeof xyz.swapee.wc.IDealBrokerPortInterface} DealBroker
 * @prop {typeof xyz.swapee.wc.IOfferExchangePortInterface} OfferExchange
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPortInterface} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowPortInterface} ExchangeIdRow
 * @prop {typeof xyz.swapee.wc.ITransactionInfoPortInterface} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadPortInterface} TransactionInfoHead
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadPortInterface} TransactionInfoHeadMob
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectIn
 * @prop {xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectOut
 * @prop {xyz.swapee.wc.IOffersFilterCore.Model} OffersFilter
 * @prop {xyz.swapee.wc.IExchangeBrokerCore.Model} ExchangeBroker
 * @prop {xyz.swapee.wc.IDealBrokerCore.Model} DealBroker
 * @prop {xyz.swapee.wc.IOfferExchangeCore.Model} OfferExchange
 * @prop {xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 * @prop {xyz.swapee.wc.IExchangeIdRowCore.Model} ExchangeIdRow
 * @prop {xyz.swapee.wc.ITransactionInfoCore.Model} TransactionInfo
 * @prop {xyz.swapee.wc.ITransactionInfoHeadCore.Model} TransactionInfoHead
 * @prop {xyz.swapee.wc.ITransactionInfoHeadCore.Model} TransactionInfoHeadMob
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangePageCircuitMemory, inputs: !xyz.swapee.wc.IExchangePageCircuitElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IExchangePageCircuitElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__server<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._server */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.ExchangePageCircuitMemory} memory The memory registers.
 * - `id` _string_ The id of a created transaction. Default empty string.
 * - `type` _string_
 * Choose between:
 * > - _fixed_
 * > - _floating_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `notId` _boolean_ Default `false`.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatingRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IExchangePageCircuitElement.Inputs} inputs The inputs to the port.
 * - `[id=null]` _&#42;?_ The id of a created transaction. ⤴ *IExchangePageCircuitOuterCore.WeakModel.Id* ⤴ *IExchangePageCircuitOuterCore.WeakModel.Id* Default `null`.
 * - `[type=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.Type* ⤴ *IExchangePageCircuitOuterCore.WeakModel.Type*
 * Choose between:
 * > - _fixed_
 * > - _floating_Default `null`.
 * - `[amountFrom="0.1"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.AmountFrom* ⤴ *IExchangePageCircuitOuterCore.WeakModel.AmountFrom* Default `0.1`.
 * - `[cryptoIn="BTC"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoIn* ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoIn* Default `BTC`.
 * - `[cryptoOut="ETH"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoOut* ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoOut* Default `ETH`.
 * - `[notId=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.NotId* ⤴ *IExchangePageCircuitOuterCore.WeakModel.NotId* Default `null`.
 * - `[fixedRate=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.FixedRate* ⤴ *IExchangePageCircuitOuterCore.WeakModel.FixedRate* Default `null`.
 * - `[floatingRate=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.FloatingRate* ⤴ *IExchangePageCircuitOuterCore.WeakModel.FloatingRate* Default `null`.
 * - `[concealWithTidSel=""]` _string?_ The query to discover the _ConcealWithTid_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[revealWithTidSel=""]` _string?_ The query to discover the _RevealWithTid_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[restartButtonSel=""]` _string?_ The query to discover the _RestartButton_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[concealWithIdSel=""]` _string?_ The query to discover the _ConcealWithId_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[revealWithIdSel=""]` _string?_ The query to discover the _RevealWithId_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[inImWrSel=""]` _string?_ The query to discover the _InImWr_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[outImWrSel=""]` _string?_ The query to discover the _OutImWr_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[cryptoSelectInSel=""]` _string?_ The query to discover the _CryptoSelectIn_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[cryptoSelectOutSel=""]` _string?_ The query to discover the _CryptoSelectOut_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[offersFilterSel=""]` _string?_ The query to discover the _OffersFilter_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeBrokerSel=""]` _string?_ The query to discover the _ExchangeBroker_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[offerExchangeSel=""]` _string?_ The query to discover the _OfferExchange_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeIdRowSel=""]` _string?_ The query to discover the _ExchangeIdRow_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoSel=""]` _string?_ The query to discover the _TransactionInfo_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoHeadSel=""]` _string?_ The query to discover the _TransactionInfoHead_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoHeadMobSel=""]` _string?_ The query to discover the _TransactionInfoHeadMob_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangePageCircuitElementPort.Inputs.NoSolder* Default `false`.
 * - `[concealWithTidOpts]` _!Object?_ The options to pass to the _ConcealWithTid_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts* Default `{}`.
 * - `[revealWithTidOpts]` _!Object?_ The options to pass to the _RevealWithTid_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts* Default `{}`.
 * - `[restartButtonOpts]` _!Object?_ The options to pass to the _RestartButton_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RestartButtonOpts* Default `{}`.
 * - `[cryptoInLaOpts]` _!Object?_ The options to pass to the _CryptoInLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts* Default `{}`.
 * - `[concealWithIdOpts]` _!Object?_ The options to pass to the _ConcealWithId_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts* Default `{}`.
 * - `[revealWithIdOpts]` _!Object?_ The options to pass to the _RevealWithId_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts* Default `{}`.
 * - `[cryptoOutLaOpts]` _!Object?_ The options to pass to the _CryptoOutLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts* Default `{}`.
 * - `[amountFromLaOpts]` _!Object?_ The options to pass to the _AmountFromLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts* Default `{}`.
 * - `[inImWrOpts]` _!Object?_ The options to pass to the _InImWr_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.InImWrOpts* Default `{}`.
 * - `[outImWrOpts]` _!Object?_ The options to pass to the _OutImWr_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OutImWrOpts* Default `{}`.
 * - `[cryptoSelectInOpts]` _!Object?_ The options to pass to the _CryptoSelectIn_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[offersFilterOpts]` _!Object?_ The options to pass to the _OffersFilter_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OffersFilterOpts* Default `{}`.
 * - `[exchangeBrokerOpts]` _!Object?_ The options to pass to the _ExchangeBroker_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * - `[offerExchangeOpts]` _!Object?_ The options to pass to the _OfferExchange_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[exchangeIdRowOpts]` _!Object?_ The options to pass to the _ExchangeIdRow_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts* Default `{}`.
 * - `[transactionInfoOpts]` _!Object?_ The options to pass to the _TransactionInfo_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts* Default `{}`.
 * - `[transactionInfoHeadOpts]` _!Object?_ The options to pass to the _TransactionInfoHead_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts* Default `{}`.
 * - `[transactionInfoHeadMobOpts]` _!Object?_ The options to pass to the _TransactionInfoHeadMob_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangePageCircuitElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangePageCircuitMemory, port?: !xyz.swapee.wc.IExchangePageCircuitElement.Inputs) => ?} xyz.swapee.wc.IExchangePageCircuitElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitElement.__inducer<!xyz.swapee.wc.IExchangePageCircuitElement>} xyz.swapee.wc.IExchangePageCircuitElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.ExchangePageCircuitMemory} [model] The model of the component into which to induce the state.
 * - `id` _string_ The id of a created transaction. Default empty string.
 * - `type` _string_
 * Choose between:
 * > - _fixed_
 * > - _floating_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `notId` _boolean_ Default `false`.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatingRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IExchangePageCircuitElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[id=null]` _&#42;?_ The id of a created transaction. ⤴ *IExchangePageCircuitOuterCore.WeakModel.Id* ⤴ *IExchangePageCircuitOuterCore.WeakModel.Id* Default `null`.
 * - `[type=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.Type* ⤴ *IExchangePageCircuitOuterCore.WeakModel.Type*
 * Choose between:
 * > - _fixed_
 * > - _floating_Default `null`.
 * - `[amountFrom="0.1"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.AmountFrom* ⤴ *IExchangePageCircuitOuterCore.WeakModel.AmountFrom* Default `0.1`.
 * - `[cryptoIn="BTC"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoIn* ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoIn* Default `BTC`.
 * - `[cryptoOut="ETH"]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoOut* ⤴ *IExchangePageCircuitOuterCore.WeakModel.CryptoOut* Default `ETH`.
 * - `[notId=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.NotId* ⤴ *IExchangePageCircuitOuterCore.WeakModel.NotId* Default `null`.
 * - `[fixedRate=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.FixedRate* ⤴ *IExchangePageCircuitOuterCore.WeakModel.FixedRate* Default `null`.
 * - `[floatingRate=null]` _&#42;?_ ⤴ *IExchangePageCircuitOuterCore.WeakModel.FloatingRate* ⤴ *IExchangePageCircuitOuterCore.WeakModel.FloatingRate* Default `null`.
 * - `[concealWithTidSel=""]` _string?_ The query to discover the _ConcealWithTid_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[revealWithTidSel=""]` _string?_ The query to discover the _RevealWithTid_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[restartButtonSel=""]` _string?_ The query to discover the _RestartButton_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[concealWithIdSel=""]` _string?_ The query to discover the _ConcealWithId_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[revealWithIdSel=""]` _string?_ The query to discover the _RevealWithId_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[inImWrSel=""]` _string?_ The query to discover the _InImWr_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[outImWrSel=""]` _string?_ The query to discover the _OutImWr_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[cryptoSelectInSel=""]` _string?_ The query to discover the _CryptoSelectIn_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[cryptoSelectOutSel=""]` _string?_ The query to discover the _CryptoSelectOut_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[offersFilterSel=""]` _string?_ The query to discover the _OffersFilter_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeBrokerSel=""]` _string?_ The query to discover the _ExchangeBroker_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[offerExchangeSel=""]` _string?_ The query to discover the _OfferExchange_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[exchangeIdRowSel=""]` _string?_ The query to discover the _ExchangeIdRow_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoSel=""]` _string?_ The query to discover the _TransactionInfo_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoHeadSel=""]` _string?_ The query to discover the _TransactionInfoHead_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[transactionInfoHeadMobSel=""]` _string?_ The query to discover the _TransactionInfoHeadMob_ VDU. ⤴ *IExchangePageCircuitDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangePageCircuitElementPort.Inputs.NoSolder* Default `false`.
 * - `[concealWithTidOpts]` _!Object?_ The options to pass to the _ConcealWithTid_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts* Default `{}`.
 * - `[revealWithTidOpts]` _!Object?_ The options to pass to the _RevealWithTid_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts* Default `{}`.
 * - `[restartButtonOpts]` _!Object?_ The options to pass to the _RestartButton_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RestartButtonOpts* Default `{}`.
 * - `[cryptoInLaOpts]` _!Object?_ The options to pass to the _CryptoInLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts* Default `{}`.
 * - `[concealWithIdOpts]` _!Object?_ The options to pass to the _ConcealWithId_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts* Default `{}`.
 * - `[revealWithIdOpts]` _!Object?_ The options to pass to the _RevealWithId_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts* Default `{}`.
 * - `[cryptoOutLaOpts]` _!Object?_ The options to pass to the _CryptoOutLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts* Default `{}`.
 * - `[amountFromLaOpts]` _!Object?_ The options to pass to the _AmountFromLa_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts* Default `{}`.
 * - `[inImWrOpts]` _!Object?_ The options to pass to the _InImWr_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.InImWrOpts* Default `{}`.
 * - `[outImWrOpts]` _!Object?_ The options to pass to the _OutImWr_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OutImWrOpts* Default `{}`.
 * - `[cryptoSelectInOpts]` _!Object?_ The options to pass to the _CryptoSelectIn_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[offersFilterOpts]` _!Object?_ The options to pass to the _OffersFilter_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OffersFilterOpts* Default `{}`.
 * - `[exchangeBrokerOpts]` _!Object?_ The options to pass to the _ExchangeBroker_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * - `[offerExchangeOpts]` _!Object?_ The options to pass to the _OfferExchange_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[exchangeIdRowOpts]` _!Object?_ The options to pass to the _ExchangeIdRow_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts* Default `{}`.
 * - `[transactionInfoOpts]` _!Object?_ The options to pass to the _TransactionInfo_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts* Default `{}`.
 * - `[transactionInfoHeadOpts]` _!Object?_ The options to pass to the _TransactionInfoHead_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts* Default `{}`.
 * - `[transactionInfoHeadMobOpts]` _!Object?_ The options to pass to the _TransactionInfoHeadMob_ vdu. ⤴ *IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IExchangePageCircuitElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangePageCircuitElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/140-IExchangePageCircuitElementPort.xml}  53243c8e819a3c0958f3ed89b9d2e43a */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangePageCircuitElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitElementPort)} xyz.swapee.wc.AbstractExchangePageCircuitElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitElementPort} xyz.swapee.wc.ExchangePageCircuitElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitElementPort
 */
xyz.swapee.wc.AbstractExchangePageCircuitElementPort = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitElementPort.constructor&xyz.swapee.wc.ExchangePageCircuitElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitElementPort.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitElementPort|typeof xyz.swapee.wc.ExchangePageCircuitElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitElementPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElementPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitElementPort|typeof xyz.swapee.wc.ExchangePageCircuitElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElementPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitElementPort|typeof xyz.swapee.wc.ExchangePageCircuitElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElementPort}
 */
xyz.swapee.wc.AbstractExchangePageCircuitElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitElementPort.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitElementPort} xyz.swapee.wc.ExchangePageCircuitElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs>)} xyz.swapee.wc.IExchangePageCircuitElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IExchangePageCircuitElementPort
 */
xyz.swapee.wc.IExchangePageCircuitElementPort = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitElementPort.Initialese>)} xyz.swapee.wc.ExchangePageCircuitElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort} xyz.swapee.wc.IExchangePageCircuitElementPort.typeof */
/**
 * A concrete class of _IExchangePageCircuitElementPort_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitElementPort
 * @implements {xyz.swapee.wc.IExchangePageCircuitElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitElementPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitElementPort = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitElementPort.constructor&xyz.swapee.wc.IExchangePageCircuitElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitElementPort}
 */
xyz.swapee.wc.ExchangePageCircuitElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitElementPort.
 * @interface xyz.swapee.wc.IExchangePageCircuitElementPortFields
 */
xyz.swapee.wc.IExchangePageCircuitElementPortFields = class { }
/**
 * The inputs to the _IExchangePageCircuitElement_'s controller via its element port.
 */
xyz.swapee.wc.IExchangePageCircuitElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangePageCircuitElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitElementPort} */
xyz.swapee.wc.RecordIExchangePageCircuitElementPort

/** @typedef {xyz.swapee.wc.IExchangePageCircuitElementPort} xyz.swapee.wc.BoundIExchangePageCircuitElementPort */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitElementPort} xyz.swapee.wc.BoundExchangePageCircuitElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _ConcealWithTid_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts.concealWithTidOpts

/**
 * The options to pass to the _RevealWithTid_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts.revealWithTidOpts

/**
 * The options to pass to the _RestartButton_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts.restartButtonOpts

/**
 * The options to pass to the _CryptoInLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts.cryptoInLaOpts

/**
 * The options to pass to the _ConcealWithId_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts.concealWithIdOpts

/**
 * The options to pass to the _RevealWithId_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts.revealWithIdOpts

/**
 * The options to pass to the _CryptoOutLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts.cryptoOutLaOpts

/**
 * The options to pass to the _AmountFromLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts.amountFromLaOpts

/**
 * The options to pass to the _InImWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts.inImWrOpts

/**
 * The options to pass to the _OutImWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts.outImWrOpts

/**
 * The options to pass to the _CryptoSelectIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts.cryptoSelectInOpts

/**
 * The options to pass to the _CryptoSelectOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts.cryptoSelectOutOpts

/**
 * The options to pass to the _OffersFilter_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts.offersFilterOpts

/**
 * The options to pass to the _ExchangeBroker_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts.exchangeBrokerOpts

/**
 * The options to pass to the _DealBroker_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts.dealBrokerOpts

/**
 * The options to pass to the _OfferExchange_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts.offerExchangeOpts

/**
 * The options to pass to the _ExchangeIntent_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

/**
 * The options to pass to the _ExchangeIdRow_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts.exchangeIdRowOpts

/**
 * The options to pass to the _TransactionInfo_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts.transactionInfoOpts

/**
 * The options to pass to the _TransactionInfoHead_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts.transactionInfoHeadOpts

/**
 * The options to pass to the _TransactionInfoHeadMob_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts.transactionInfoHeadMobOpts

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts)} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts.typeof */
/**
 * The inputs to the _IExchangePageCircuitElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.constructor&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithTidOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithTidOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RestartButtonOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoInLaOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithIdOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithIdOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoOutLaOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.AmountFromLaOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.InImWrOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OutImWrOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectInOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectOutOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OffersFilterOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeBrokerOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.DealBrokerOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OfferExchangeOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIntentOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIdRowOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadOpts&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadMobOpts)} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithTidOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithTidOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithTidOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithTidOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RestartButtonOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RestartButtonOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoInLaOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoInLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithIdOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithIdOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithIdOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithIdOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoOutLaOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoOutLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.AmountFromLaOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.AmountFromLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.InImWrOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.InImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OutImWrOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OutImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectInOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectOutOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OffersFilterOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OffersFilterOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeBrokerOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeBrokerOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.DealBrokerOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.DealBrokerOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OfferExchangeOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OfferExchangeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIntentOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIntentOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIdRowOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIdRowOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadMobOpts} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadMobOpts.typeof */
/**
 * The inputs to the _IExchangePageCircuitElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs
 */
xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.constructor&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithTidOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithTidOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RestartButtonOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoInLaOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithIdOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithIdOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoOutLaOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.AmountFromLaOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.InImWrOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OutImWrOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectInOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectOutOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OffersFilterOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeBrokerOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.DealBrokerOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OfferExchangeOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIntentOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIdRowOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadOpts.typeof&xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadMobOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs

/**
 * Contains getters to cast the _IExchangePageCircuitElementPort_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitElementPortCaster
 */
xyz.swapee.wc.IExchangePageCircuitElementPortCaster = class { }
/**
 * Cast the _IExchangePageCircuitElementPort_ instance into the _BoundIExchangePageCircuitElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitElementPort}
 */
xyz.swapee.wc.IExchangePageCircuitElementPortCaster.prototype.asIExchangePageCircuitElementPort
/**
 * Access the _ExchangePageCircuitElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitElementPort}
 */
xyz.swapee.wc.IExchangePageCircuitElementPortCaster.prototype.superExchangePageCircuitElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts The options to pass to the _ConcealWithTid_ vdu (optional overlay).
 * @prop {!Object} [concealWithTidOpts] The options to pass to the _ConcealWithTid_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithTidOpts_Safe The options to pass to the _ConcealWithTid_ vdu (required overlay).
 * @prop {!Object} concealWithTidOpts The options to pass to the _ConcealWithTid_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts The options to pass to the _RevealWithTid_ vdu (optional overlay).
 * @prop {!Object} [revealWithTidOpts] The options to pass to the _RevealWithTid_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithTidOpts_Safe The options to pass to the _RevealWithTid_ vdu (required overlay).
 * @prop {!Object} revealWithTidOpts The options to pass to the _RevealWithTid_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts The options to pass to the _RestartButton_ vdu (optional overlay).
 * @prop {!Object} [restartButtonOpts] The options to pass to the _RestartButton_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RestartButtonOpts_Safe The options to pass to the _RestartButton_ vdu (required overlay).
 * @prop {!Object} restartButtonOpts The options to pass to the _RestartButton_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts The options to pass to the _CryptoInLa_ vdu (optional overlay).
 * @prop {!Object} [cryptoInLaOpts] The options to pass to the _CryptoInLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoInLaOpts_Safe The options to pass to the _CryptoInLa_ vdu (required overlay).
 * @prop {!Object} cryptoInLaOpts The options to pass to the _CryptoInLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts The options to pass to the _ConcealWithId_ vdu (optional overlay).
 * @prop {!Object} [concealWithIdOpts] The options to pass to the _ConcealWithId_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ConcealWithIdOpts_Safe The options to pass to the _ConcealWithId_ vdu (required overlay).
 * @prop {!Object} concealWithIdOpts The options to pass to the _ConcealWithId_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts The options to pass to the _RevealWithId_ vdu (optional overlay).
 * @prop {!Object} [revealWithIdOpts] The options to pass to the _RevealWithId_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.RevealWithIdOpts_Safe The options to pass to the _RevealWithId_ vdu (required overlay).
 * @prop {!Object} revealWithIdOpts The options to pass to the _RevealWithId_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts The options to pass to the _CryptoOutLa_ vdu (optional overlay).
 * @prop {!Object} [cryptoOutLaOpts] The options to pass to the _CryptoOutLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoOutLaOpts_Safe The options to pass to the _CryptoOutLa_ vdu (required overlay).
 * @prop {!Object} cryptoOutLaOpts The options to pass to the _CryptoOutLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts The options to pass to the _AmountFromLa_ vdu (optional overlay).
 * @prop {!Object} [amountFromLaOpts] The options to pass to the _AmountFromLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.AmountFromLaOpts_Safe The options to pass to the _AmountFromLa_ vdu (required overlay).
 * @prop {!Object} amountFromLaOpts The options to pass to the _AmountFromLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts The options to pass to the _InImWr_ vdu (optional overlay).
 * @prop {!Object} [inImWrOpts] The options to pass to the _InImWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.InImWrOpts_Safe The options to pass to the _InImWr_ vdu (required overlay).
 * @prop {!Object} inImWrOpts The options to pass to the _InImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts The options to pass to the _OutImWr_ vdu (optional overlay).
 * @prop {!Object} [outImWrOpts] The options to pass to the _OutImWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OutImWrOpts_Safe The options to pass to the _OutImWr_ vdu (required overlay).
 * @prop {!Object} outImWrOpts The options to pass to the _OutImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu (optional overlay).
 * @prop {!Object} [cryptoSelectInOpts] The options to pass to the _CryptoSelectIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectInOpts_Safe The options to pass to the _CryptoSelectIn_ vdu (required overlay).
 * @prop {!Object} cryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu (optional overlay).
 * @prop {!Object} [cryptoSelectOutOpts] The options to pass to the _CryptoSelectOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.CryptoSelectOutOpts_Safe The options to pass to the _CryptoSelectOut_ vdu (required overlay).
 * @prop {!Object} cryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts The options to pass to the _OffersFilter_ vdu (optional overlay).
 * @prop {!Object} [offersFilterOpts] The options to pass to the _OffersFilter_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OffersFilterOpts_Safe The options to pass to the _OffersFilter_ vdu (required overlay).
 * @prop {!Object} offersFilterOpts The options to pass to the _OffersFilter_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts The options to pass to the _ExchangeBroker_ vdu (optional overlay).
 * @prop {!Object} [exchangeBrokerOpts] The options to pass to the _ExchangeBroker_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeBrokerOpts_Safe The options to pass to the _ExchangeBroker_ vdu (required overlay).
 * @prop {!Object} exchangeBrokerOpts The options to pass to the _ExchangeBroker_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts The options to pass to the _DealBroker_ vdu (optional overlay).
 * @prop {!Object} [dealBrokerOpts] The options to pass to the _DealBroker_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.DealBrokerOpts_Safe The options to pass to the _DealBroker_ vdu (required overlay).
 * @prop {!Object} dealBrokerOpts The options to pass to the _DealBroker_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts The options to pass to the _OfferExchange_ vdu (optional overlay).
 * @prop {!Object} [offerExchangeOpts] The options to pass to the _OfferExchange_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.OfferExchangeOpts_Safe The options to pass to the _OfferExchange_ vdu (required overlay).
 * @prop {!Object} offerExchangeOpts The options to pass to the _OfferExchange_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {!Object} [exchangeIntentOpts] The options to pass to the _ExchangeIntent_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {!Object} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts The options to pass to the _ExchangeIdRow_ vdu (optional overlay).
 * @prop {!Object} [exchangeIdRowOpts] The options to pass to the _ExchangeIdRow_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.ExchangeIdRowOpts_Safe The options to pass to the _ExchangeIdRow_ vdu (required overlay).
 * @prop {!Object} exchangeIdRowOpts The options to pass to the _ExchangeIdRow_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts The options to pass to the _TransactionInfo_ vdu (optional overlay).
 * @prop {!Object} [transactionInfoOpts] The options to pass to the _TransactionInfo_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoOpts_Safe The options to pass to the _TransactionInfo_ vdu (required overlay).
 * @prop {!Object} transactionInfoOpts The options to pass to the _TransactionInfo_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts The options to pass to the _TransactionInfoHead_ vdu (optional overlay).
 * @prop {!Object} [transactionInfoHeadOpts] The options to pass to the _TransactionInfoHead_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadOpts_Safe The options to pass to the _TransactionInfoHead_ vdu (required overlay).
 * @prop {!Object} transactionInfoHeadOpts The options to pass to the _TransactionInfoHead_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts The options to pass to the _TransactionInfoHeadMob_ vdu (optional overlay).
 * @prop {!Object} [transactionInfoHeadMobOpts] The options to pass to the _TransactionInfoHeadMob_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs.TransactionInfoHeadMobOpts_Safe The options to pass to the _TransactionInfoHeadMob_ vdu (required overlay).
 * @prop {!Object} transactionInfoHeadMobOpts The options to pass to the _TransactionInfoHeadMob_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithTidOpts The options to pass to the _ConcealWithTid_ vdu (optional overlay).
 * @prop {*} [concealWithTidOpts=null] The options to pass to the _ConcealWithTid_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithTidOpts_Safe The options to pass to the _ConcealWithTid_ vdu (required overlay).
 * @prop {*} concealWithTidOpts The options to pass to the _ConcealWithTid_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithTidOpts The options to pass to the _RevealWithTid_ vdu (optional overlay).
 * @prop {*} [revealWithTidOpts=null] The options to pass to the _RevealWithTid_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithTidOpts_Safe The options to pass to the _RevealWithTid_ vdu (required overlay).
 * @prop {*} revealWithTidOpts The options to pass to the _RevealWithTid_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RestartButtonOpts The options to pass to the _RestartButton_ vdu (optional overlay).
 * @prop {*} [restartButtonOpts=null] The options to pass to the _RestartButton_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RestartButtonOpts_Safe The options to pass to the _RestartButton_ vdu (required overlay).
 * @prop {*} restartButtonOpts The options to pass to the _RestartButton_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoInLaOpts The options to pass to the _CryptoInLa_ vdu (optional overlay).
 * @prop {*} [cryptoInLaOpts=null] The options to pass to the _CryptoInLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoInLaOpts_Safe The options to pass to the _CryptoInLa_ vdu (required overlay).
 * @prop {*} cryptoInLaOpts The options to pass to the _CryptoInLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithIdOpts The options to pass to the _ConcealWithId_ vdu (optional overlay).
 * @prop {*} [concealWithIdOpts=null] The options to pass to the _ConcealWithId_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ConcealWithIdOpts_Safe The options to pass to the _ConcealWithId_ vdu (required overlay).
 * @prop {*} concealWithIdOpts The options to pass to the _ConcealWithId_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithIdOpts The options to pass to the _RevealWithId_ vdu (optional overlay).
 * @prop {*} [revealWithIdOpts=null] The options to pass to the _RevealWithId_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.RevealWithIdOpts_Safe The options to pass to the _RevealWithId_ vdu (required overlay).
 * @prop {*} revealWithIdOpts The options to pass to the _RevealWithId_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoOutLaOpts The options to pass to the _CryptoOutLa_ vdu (optional overlay).
 * @prop {*} [cryptoOutLaOpts=null] The options to pass to the _CryptoOutLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoOutLaOpts_Safe The options to pass to the _CryptoOutLa_ vdu (required overlay).
 * @prop {*} cryptoOutLaOpts The options to pass to the _CryptoOutLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.AmountFromLaOpts The options to pass to the _AmountFromLa_ vdu (optional overlay).
 * @prop {*} [amountFromLaOpts=null] The options to pass to the _AmountFromLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.AmountFromLaOpts_Safe The options to pass to the _AmountFromLa_ vdu (required overlay).
 * @prop {*} amountFromLaOpts The options to pass to the _AmountFromLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.InImWrOpts The options to pass to the _InImWr_ vdu (optional overlay).
 * @prop {*} [inImWrOpts=null] The options to pass to the _InImWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.InImWrOpts_Safe The options to pass to the _InImWr_ vdu (required overlay).
 * @prop {*} inImWrOpts The options to pass to the _InImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OutImWrOpts The options to pass to the _OutImWr_ vdu (optional overlay).
 * @prop {*} [outImWrOpts=null] The options to pass to the _OutImWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OutImWrOpts_Safe The options to pass to the _OutImWr_ vdu (required overlay).
 * @prop {*} outImWrOpts The options to pass to the _OutImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu (optional overlay).
 * @prop {*} [cryptoSelectInOpts=null] The options to pass to the _CryptoSelectIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectInOpts_Safe The options to pass to the _CryptoSelectIn_ vdu (required overlay).
 * @prop {*} cryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu (optional overlay).
 * @prop {*} [cryptoSelectOutOpts=null] The options to pass to the _CryptoSelectOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.CryptoSelectOutOpts_Safe The options to pass to the _CryptoSelectOut_ vdu (required overlay).
 * @prop {*} cryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OffersFilterOpts The options to pass to the _OffersFilter_ vdu (optional overlay).
 * @prop {*} [offersFilterOpts=null] The options to pass to the _OffersFilter_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OffersFilterOpts_Safe The options to pass to the _OffersFilter_ vdu (required overlay).
 * @prop {*} offersFilterOpts The options to pass to the _OffersFilter_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeBrokerOpts The options to pass to the _ExchangeBroker_ vdu (optional overlay).
 * @prop {*} [exchangeBrokerOpts=null] The options to pass to the _ExchangeBroker_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeBrokerOpts_Safe The options to pass to the _ExchangeBroker_ vdu (required overlay).
 * @prop {*} exchangeBrokerOpts The options to pass to the _ExchangeBroker_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.DealBrokerOpts The options to pass to the _DealBroker_ vdu (optional overlay).
 * @prop {*} [dealBrokerOpts=null] The options to pass to the _DealBroker_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.DealBrokerOpts_Safe The options to pass to the _DealBroker_ vdu (required overlay).
 * @prop {*} dealBrokerOpts The options to pass to the _DealBroker_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OfferExchangeOpts The options to pass to the _OfferExchange_ vdu (optional overlay).
 * @prop {*} [offerExchangeOpts=null] The options to pass to the _OfferExchange_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.OfferExchangeOpts_Safe The options to pass to the _OfferExchange_ vdu (required overlay).
 * @prop {*} offerExchangeOpts The options to pass to the _OfferExchange_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {*} [exchangeIntentOpts=null] The options to pass to the _ExchangeIntent_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {*} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIdRowOpts The options to pass to the _ExchangeIdRow_ vdu (optional overlay).
 * @prop {*} [exchangeIdRowOpts=null] The options to pass to the _ExchangeIdRow_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.ExchangeIdRowOpts_Safe The options to pass to the _ExchangeIdRow_ vdu (required overlay).
 * @prop {*} exchangeIdRowOpts The options to pass to the _ExchangeIdRow_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoOpts The options to pass to the _TransactionInfo_ vdu (optional overlay).
 * @prop {*} [transactionInfoOpts=null] The options to pass to the _TransactionInfo_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoOpts_Safe The options to pass to the _TransactionInfo_ vdu (required overlay).
 * @prop {*} transactionInfoOpts The options to pass to the _TransactionInfo_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadOpts The options to pass to the _TransactionInfoHead_ vdu (optional overlay).
 * @prop {*} [transactionInfoHeadOpts=null] The options to pass to the _TransactionInfoHead_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadOpts_Safe The options to pass to the _TransactionInfoHead_ vdu (required overlay).
 * @prop {*} transactionInfoHeadOpts The options to pass to the _TransactionInfoHead_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadMobOpts The options to pass to the _TransactionInfoHeadMob_ vdu (optional overlay).
 * @prop {*} [transactionInfoHeadMobOpts=null] The options to pass to the _TransactionInfoHeadMob_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitElementPort.WeakInputs.TransactionInfoHeadMobOpts_Safe The options to pass to the _TransactionInfoHeadMob_ vdu (required overlay).
 * @prop {*} transactionInfoHeadMobOpts The options to pass to the _TransactionInfoHeadMob_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/170-IExchangePageCircuitDesigner.xml}  845856450d0363331607a067ce14094d */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IExchangePageCircuitDesigner
 */
xyz.swapee.wc.IExchangePageCircuitDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.ExchangePageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangePageCircuit />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangePageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangePageCircuit />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangePageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangePageCircuitDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `CryptoSelectIn` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `OffersFilter` _typeof xyz.swapee.wc.IOffersFilterController_
   * - `ExchangeBroker` _typeof xyz.swapee.wc.IExchangeBrokerController_
   * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerController_
   * - `OfferExchange` _typeof xyz.swapee.wc.IOfferExchangeController_
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `ExchangeIdRow` _typeof xyz.swapee.wc.IExchangeIdRowController_
   * - `TransactionInfo` _typeof xyz.swapee.wc.ITransactionInfoController_
   * - `TransactionInfoHead` _typeof xyz.swapee.wc.ITransactionInfoHeadController_
   * - `TransactionInfoHeadMob` _typeof xyz.swapee.wc.ITransactionInfoHeadController_
   * - `ExchangePageCircuit` _typeof IExchangePageCircuitController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangePageCircuitDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `CryptoSelectIn` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `OffersFilter` _typeof xyz.swapee.wc.IOffersFilterController_
   * - `ExchangeBroker` _typeof xyz.swapee.wc.IExchangeBrokerController_
   * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerController_
   * - `OfferExchange` _typeof xyz.swapee.wc.IOfferExchangeController_
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `ExchangeIdRow` _typeof xyz.swapee.wc.IExchangeIdRowController_
   * - `TransactionInfo` _typeof xyz.swapee.wc.ITransactionInfoController_
   * - `TransactionInfoHead` _typeof xyz.swapee.wc.ITransactionInfoHeadController_
   * - `TransactionInfoHeadMob` _typeof xyz.swapee.wc.ITransactionInfoHeadController_
   * - `ExchangePageCircuit` _typeof IExchangePageCircuitController_
   * - `This` _typeof IExchangePageCircuitController_
   * @param {!xyz.swapee.wc.IExchangePageCircuitDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `CryptoSelectIn` _!xyz.swapee.wc.CryptoSelectMemory_
   * - `CryptoSelectOut` _!xyz.swapee.wc.CryptoSelectMemory_
   * - `OffersFilter` _!xyz.swapee.wc.OffersFilterMemory_
   * - `ExchangeBroker` _!xyz.swapee.wc.ExchangeBrokerMemory_
   * - `DealBroker` _!xyz.swapee.wc.DealBrokerMemory_
   * - `OfferExchange` _!xyz.swapee.wc.OfferExchangeMemory_
   * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_
   * - `ExchangeIdRow` _!xyz.swapee.wc.ExchangeIdRowMemory_
   * - `TransactionInfo` _!xyz.swapee.wc.TransactionInfoMemory_
   * - `TransactionInfoHead` _!xyz.swapee.wc.TransactionInfoHeadMemory_
   * - `TransactionInfoHeadMob` _!xyz.swapee.wc.TransactionInfoHeadMemory_
   * - `ExchangePageCircuit` _!ExchangePageCircuitMemory_
   * - `This` _!ExchangePageCircuitMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangePageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangePageCircuitClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IExchangePageCircuitDesigner.prototype.constructor = xyz.swapee.wc.IExchangePageCircuitDesigner

/**
 * A concrete class of _IExchangePageCircuitDesigner_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitDesigner
 * @implements {xyz.swapee.wc.IExchangePageCircuitDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.ExchangePageCircuitDesigner = class extends xyz.swapee.wc.IExchangePageCircuitDesigner { }
xyz.swapee.wc.ExchangePageCircuitDesigner.prototype.constructor = xyz.swapee.wc.ExchangePageCircuitDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IOffersFilterController} OffersFilter
 * @prop {typeof xyz.swapee.wc.IExchangeBrokerController} ExchangeBroker
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 * @prop {typeof xyz.swapee.wc.IOfferExchangeController} OfferExchange
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowController} ExchangeIdRow
 * @prop {typeof xyz.swapee.wc.ITransactionInfoController} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadController} TransactionInfoHead
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadController} TransactionInfoHeadMob
 * @prop {typeof xyz.swapee.wc.IExchangePageCircuitController} ExchangePageCircuit
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IOffersFilterController} OffersFilter
 * @prop {typeof xyz.swapee.wc.IExchangeBrokerController} ExchangeBroker
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 * @prop {typeof xyz.swapee.wc.IOfferExchangeController} OfferExchange
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowController} ExchangeIdRow
 * @prop {typeof xyz.swapee.wc.ITransactionInfoController} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadController} TransactionInfoHead
 * @prop {typeof xyz.swapee.wc.ITransactionInfoHeadController} TransactionInfoHeadMob
 * @prop {typeof xyz.swapee.wc.IExchangePageCircuitController} ExchangePageCircuit
 * @prop {typeof xyz.swapee.wc.IExchangePageCircuitController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectIn
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut
 * @prop {!xyz.swapee.wc.OffersFilterMemory} OffersFilter
 * @prop {!xyz.swapee.wc.ExchangeBrokerMemory} ExchangeBroker
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker
 * @prop {!xyz.swapee.wc.OfferExchangeMemory} OfferExchange
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.ExchangeIdRowMemory} ExchangeIdRow
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} TransactionInfo
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHead
 * @prop {!xyz.swapee.wc.TransactionInfoHeadMemory} TransactionInfoHeadMob
 * @prop {!xyz.swapee.wc.ExchangePageCircuitMemory} ExchangePageCircuit
 * @prop {!xyz.swapee.wc.ExchangePageCircuitMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/200-ExchangePageCircuitLand.xml}  1dfe7e68703348f48f2f37674f3d4360 */
/**
 * The surrounding of the _IExchangePageCircuit_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.ExchangePageCircuitLand
 */
xyz.swapee.wc.ExchangePageCircuitLand = class { }
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.CryptoSelectIn = /** @type {xyz.swapee.wc.ICryptoSelect} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.CryptoSelectOut = /** @type {xyz.swapee.wc.ICryptoSelect} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.OffersFilter = /** @type {xyz.swapee.wc.IOffersFilter} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.ExchangeBroker = /** @type {xyz.swapee.wc.IExchangeBroker} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.DealBroker = /** @type {xyz.swapee.wc.IDealBroker} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.OfferExchange = /** @type {xyz.swapee.wc.IOfferExchange} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.ExchangeIntent = /** @type {xyz.swapee.wc.IExchangeIntent} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.ExchangeIdRow = /** @type {xyz.swapee.wc.IExchangeIdRow} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.TransactionInfo = /** @type {xyz.swapee.wc.ITransactionInfo} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.TransactionInfoHead = /** @type {xyz.swapee.wc.ITransactionInfoHead} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ExchangePageCircuitLand.prototype.TransactionInfoHeadMob = /** @type {xyz.swapee.wc.ITransactionInfoHead} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/40-IExchangePageCircuitDisplay.xml}  960c3f870343ad942ab2a806a4f95455 */
/**
 * @typedef {Object} $xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese
 * @prop {!Array<!HTMLSpanElement>} [ConcealWithTids]     Those elements will be hidden when transaction ID is available, in which
 * case the info about transaction is displayed.
 * @prop {!Array<!HTMLSpanElement>} [RevealWithTids]     Those elements will be shown when transaction ID is available, in which
 * case the info about transaction is displayed.
 * @prop {!Array<!HTMLSpanElement>} [RestartButtons]
 * @prop {HTMLSpanElement} [CryptoInLa]
 * @prop {!Array<!HTMLSpanElement>} [ConcealWithIds]
 * @prop {!Array<!HTMLSpanElement>} [RevealWithIds]
 * @prop {HTMLSpanElement} [CryptoOutLa]
 * @prop {HTMLSpanElement} [AmountFromLa]
 * @prop {!Array<!HTMLElement>} [InImWrs]
 * @prop {!Array<!HTMLElement>} [OutImWrs]
 * @prop {HTMLElement} [CryptoSelectIn] The via for the _CryptoSelectIn_ peer.
 * @prop {HTMLElement} [CryptoSelectOut] The via for the _CryptoSelectOut_ peer.
 * @prop {HTMLElement} [OffersFilter] The via for the _OffersFilter_ peer.
 * @prop {HTMLElement} [ExchangeBroker] The via for the _ExchangeBroker_ peer.
 * @prop {HTMLElement} [DealBroker] The via for the _DealBroker_ peer.
 * @prop {HTMLElement} [OfferExchange] The via for the _OfferExchange_ peer.
 * @prop {HTMLElement} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 * @prop {HTMLElement} [ExchangeIdRow] The via for the _ExchangeIdRow_ peer.
 * @prop {HTMLElement} [TransactionInfo] The via for the _TransactionInfo_ peer.
 * @prop {HTMLElement} [TransactionInfoHead] The via for the _TransactionInfoHead_ peer.
 * @prop {HTMLElement} [TransactionInfoHeadMob] The via for the _TransactionInfoHeadMob_ peer.
 */
/** @typedef {$xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IExchangePageCircuitDisplay.Settings>} xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitDisplay)} xyz.swapee.wc.AbstractExchangePageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitDisplay} xyz.swapee.wc.ExchangePageCircuitDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitDisplay
 */
xyz.swapee.wc.AbstractExchangePageCircuitDisplay = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitDisplay.constructor&xyz.swapee.wc.ExchangePageCircuitDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitDisplay.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.ExchangePageCircuitDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitDisplay}
 */
xyz.swapee.wc.AbstractExchangePageCircuitDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.AbstractExchangePageCircuitDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.ExchangePageCircuitDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.AbstractExchangePageCircuitDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.ExchangePageCircuitDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.AbstractExchangePageCircuitDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitDisplay} xyz.swapee.wc.ExchangePageCircuitDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.ExchangePageCircuitMemory, !HTMLDivElement, !xyz.swapee.wc.IExchangePageCircuitDisplay.Settings, xyz.swapee.wc.IExchangePageCircuitDisplay.Queries, null>)} xyz.swapee.wc.IExchangePageCircuitDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IExchangePageCircuit_.
 * @interface xyz.swapee.wc.IExchangePageCircuitDisplay
 */
xyz.swapee.wc.IExchangePageCircuitDisplay = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangePageCircuitDisplay.paint} */
xyz.swapee.wc.IExchangePageCircuitDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese>)} xyz.swapee.wc.ExchangePageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitDisplay} xyz.swapee.wc.IExchangePageCircuitDisplay.typeof */
/**
 * A concrete class of _IExchangePageCircuitDisplay_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitDisplay
 * @implements {xyz.swapee.wc.IExchangePageCircuitDisplay} Display for presenting information from the _IExchangePageCircuit_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitDisplay = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitDisplay.constructor&xyz.swapee.wc.IExchangePageCircuitDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.ExchangePageCircuitDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitDisplay.
 * @interface xyz.swapee.wc.IExchangePageCircuitDisplayFields
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IExchangePageCircuitDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IExchangePageCircuitDisplay.Queries} */ (void 0)
/**
 *     Those elements will be hidden when transaction ID is available, in which
 * case the info about transaction is displayed. Default `[]`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.ConcealWithTids = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 *     Those elements will be shown when transaction ID is available, in which
 * case the info about transaction is displayed. Default `[]`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.RevealWithTids = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.RestartButtons = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.CryptoInLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.ConcealWithIds = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.RevealWithIds = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.CryptoOutLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.AmountFromLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.InImWrs = /** @type {!Array<!HTMLElement>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.OutImWrs = /** @type {!Array<!HTMLElement>} */ (void 0)
/**
 * The via for the _CryptoSelectIn_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.CryptoSelectIn = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _CryptoSelectOut_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.CryptoSelectOut = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _OffersFilter_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.OffersFilter = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _ExchangeBroker_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.ExchangeBroker = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _DealBroker_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.DealBroker = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _OfferExchange_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.OfferExchange = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.ExchangeIntent = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _ExchangeIdRow_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.ExchangeIdRow = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _TransactionInfo_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.TransactionInfo = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _TransactionInfoHead_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.TransactionInfoHead = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _TransactionInfoHeadMob_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangePageCircuitDisplayFields.prototype.TransactionInfoHeadMob = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitDisplay} */
xyz.swapee.wc.RecordIExchangePageCircuitDisplay

/** @typedef {xyz.swapee.wc.IExchangePageCircuitDisplay} xyz.swapee.wc.BoundIExchangePageCircuitDisplay */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitDisplay} xyz.swapee.wc.BoundExchangePageCircuitDisplay */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitDisplay.Queries} xyz.swapee.wc.IExchangePageCircuitDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangePageCircuitDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [concealWithTidSel=""] The query to discover the _ConcealWithTid_ VDU. Default empty string.
 * @prop {string} [revealWithTidSel=""] The query to discover the _RevealWithTid_ VDU. Default empty string.
 * @prop {string} [restartButtonSel=""] The query to discover the _RestartButton_ VDU. Default empty string.
 * @prop {string} [concealWithIdSel=""] The query to discover the _ConcealWithId_ VDU. Default empty string.
 * @prop {string} [revealWithIdSel=""] The query to discover the _RevealWithId_ VDU. Default empty string.
 * @prop {string} [inImWrSel=""] The query to discover the _InImWr_ VDU. Default empty string.
 * @prop {string} [outImWrSel=""] The query to discover the _OutImWr_ VDU. Default empty string.
 * @prop {string} [cryptoSelectInSel=""] The query to discover the _CryptoSelectIn_ VDU. Default empty string.
 * @prop {string} [cryptoSelectOutSel=""] The query to discover the _CryptoSelectOut_ VDU. Default empty string.
 * @prop {string} [offersFilterSel=""] The query to discover the _OffersFilter_ VDU. Default empty string.
 * @prop {string} [exchangeBrokerSel=""] The query to discover the _ExchangeBroker_ VDU. Default empty string.
 * @prop {string} [dealBrokerSel=""] The query to discover the _DealBroker_ VDU. Default empty string.
 * @prop {string} [offerExchangeSel=""] The query to discover the _OfferExchange_ VDU. Default empty string.
 * @prop {string} [exchangeIntentSel=""] The query to discover the _ExchangeIntent_ VDU. Default empty string.
 * @prop {string} [exchangeIdRowSel=""] The query to discover the _ExchangeIdRow_ VDU. Default empty string.
 * @prop {string} [transactionInfoSel=""] The query to discover the _TransactionInfo_ VDU. Default empty string.
 * @prop {string} [transactionInfoHeadSel=""] The query to discover the _TransactionInfoHead_ VDU. Default empty string.
 * @prop {string} [transactionInfoHeadMobSel=""] The query to discover the _TransactionInfoHeadMob_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IExchangePageCircuitDisplay_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitDisplayCaster
 */
xyz.swapee.wc.IExchangePageCircuitDisplayCaster = class { }
/**
 * Cast the _IExchangePageCircuitDisplay_ instance into the _BoundIExchangePageCircuitDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitDisplay}
 */
xyz.swapee.wc.IExchangePageCircuitDisplayCaster.prototype.asIExchangePageCircuitDisplay
/**
 * Cast the _IExchangePageCircuitDisplay_ instance into the _BoundIExchangePageCircuitScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitScreen}
 */
xyz.swapee.wc.IExchangePageCircuitDisplayCaster.prototype.asIExchangePageCircuitScreen
/**
 * Access the _ExchangePageCircuitDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitDisplay}
 */
xyz.swapee.wc.IExchangePageCircuitDisplayCaster.prototype.superExchangePageCircuitDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangePageCircuitMemory, land: null) => void} xyz.swapee.wc.IExchangePageCircuitDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitDisplay.__paint<!xyz.swapee.wc.IExchangePageCircuitDisplay>} xyz.swapee.wc.IExchangePageCircuitDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangePageCircuitMemory} memory The display data.
 * - `id` _string_ The id of a created transaction. Default empty string.
 * - `type` _string_
 * Choose between:
 * > - _fixed_
 * > - _floating_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `notId` _boolean_ Default `false`.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatingRate` _boolean_ Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangePageCircuitDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/40-IExchangePageCircuitDisplayBack.xml}  526636b0bb558cf5d4fa89c88e3740fd */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IExchangePageCircuitDisplay.Initialese
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [ConcealWithTids]     Those elements will be hidden when transaction ID is available, in which
 * case the info about transaction is displayed.
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [RevealWithTids]     Those elements will be shown when transaction ID is available, in which
 * case the info about transaction is displayed.
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [RestartButtons]
 * @prop {!com.webcircuits.IHtmlTwin} [CryptoInLa]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [ConcealWithIds]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [RevealWithIds]
 * @prop {!com.webcircuits.IHtmlTwin} [CryptoOutLa]
 * @prop {!com.webcircuits.IHtmlTwin} [AmountFromLa]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [InImWrs]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [OutImWrs]
 * @prop {!com.webcircuits.IHtmlTwin} [CryptoSelectIn] The via for the _CryptoSelectIn_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [CryptoSelectOut] The via for the _CryptoSelectOut_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [OffersFilter] The via for the _OffersFilter_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeBroker] The via for the _ExchangeBroker_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [DealBroker] The via for the _DealBroker_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [OfferExchange] The via for the _OfferExchange_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIdRow] The via for the _ExchangeIdRow_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionInfo] The via for the _TransactionInfo_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionInfoHead] The via for the _TransactionInfoHead_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [TransactionInfoHeadMob] The via for the _TransactionInfoHeadMob_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IExchangePageCircuitDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ExchangePageCircuitClasses>} xyz.swapee.wc.back.IExchangePageCircuitDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangePageCircuitDisplay)} xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay} xyz.swapee.wc.back.ExchangePageCircuitDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangePageCircuitDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.constructor&xyz.swapee.wc.back.ExchangePageCircuitDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangePageCircuitDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IExchangePageCircuitDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.ExchangePageCircuitClasses, !xyz.swapee.wc.ExchangePageCircuitLand>)} xyz.swapee.wc.back.IExchangePageCircuitDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitDisplay
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplay = class extends /** @type {xyz.swapee.wc.back.IExchangePageCircuitDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IExchangePageCircuitDisplay.paint} */
xyz.swapee.wc.back.IExchangePageCircuitDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangePageCircuitDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitDisplay.Initialese>)} xyz.swapee.wc.back.ExchangePageCircuitDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangePageCircuitDisplay} xyz.swapee.wc.back.IExchangePageCircuitDisplay.typeof */
/**
 * A concrete class of _IExchangePageCircuitDisplay_ instances.
 * @constructor xyz.swapee.wc.back.ExchangePageCircuitDisplay
 * @implements {xyz.swapee.wc.back.IExchangePageCircuitDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangePageCircuitDisplay = class extends /** @type {xyz.swapee.wc.back.ExchangePageCircuitDisplay.constructor&xyz.swapee.wc.back.IExchangePageCircuitDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.ExchangePageCircuitDisplay.prototype.constructor = xyz.swapee.wc.back.ExchangePageCircuitDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay}
 */
xyz.swapee.wc.back.ExchangePageCircuitDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitDisplay.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitDisplayFields
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields = class { }
/**
 *     Those elements will be hidden when transaction ID is available, in which
 * case the info about transaction is displayed. Default `[]`.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.ConcealWithTids = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *     Those elements will be shown when transaction ID is available, in which
 * case the info about transaction is displayed. Default `[]`.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.RevealWithTids = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.RestartButtons = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.CryptoInLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.ConcealWithIds = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.RevealWithIds = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.CryptoOutLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.AmountFromLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.InImWrs = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.OutImWrs = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 * The via for the _CryptoSelectIn_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.CryptoSelectIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _CryptoSelectOut_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.CryptoSelectOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _OffersFilter_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.OffersFilter = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ExchangeBroker_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.ExchangeBroker = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _DealBroker_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.DealBroker = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _OfferExchange_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.OfferExchange = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.ExchangeIntent = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ExchangeIdRow_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.ExchangeIdRow = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _TransactionInfo_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.TransactionInfo = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _TransactionInfoHead_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.TransactionInfoHead = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _TransactionInfoHeadMob_ peer.
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayFields.prototype.TransactionInfoHeadMob = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitDisplay} */
xyz.swapee.wc.back.RecordIExchangePageCircuitDisplay

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitDisplay} xyz.swapee.wc.back.BoundIExchangePageCircuitDisplay */

/** @typedef {xyz.swapee.wc.back.ExchangePageCircuitDisplay} xyz.swapee.wc.back.BoundExchangePageCircuitDisplay */

/**
 * Contains getters to cast the _IExchangePageCircuitDisplay_ interface.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitDisplayCaster
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayCaster = class { }
/**
 * Cast the _IExchangePageCircuitDisplay_ instance into the _BoundIExchangePageCircuitDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangePageCircuitDisplay}
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayCaster.prototype.asIExchangePageCircuitDisplay
/**
 * Access the _ExchangePageCircuitDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangePageCircuitDisplay}
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplayCaster.prototype.superExchangePageCircuitDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.ExchangePageCircuitMemory, land?: !xyz.swapee.wc.ExchangePageCircuitLand) => void} xyz.swapee.wc.back.IExchangePageCircuitDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitDisplay.__paint<!xyz.swapee.wc.back.IExchangePageCircuitDisplay>} xyz.swapee.wc.back.IExchangePageCircuitDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IExchangePageCircuitDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangePageCircuitMemory} [memory] The display data.
 * - `id` _string_ The id of a created transaction. Default empty string.
 * - `type` _string_
 * Choose between:
 * > - _fixed_
 * > - _floating_Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `cryptoIn` _string_ Default empty string.
 * - `cryptoOut` _string_ Default empty string.
 * - `notId` _boolean_ Default `false`.
 * - `fixedRate` _boolean_ Default `false`.
 * - `floatingRate` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.ExchangePageCircuitLand} [land] The land data.
 * - `CryptoSelectIn` _xyz.swapee.wc.ICryptoSelect_
 * - `CryptoSelectOut` _xyz.swapee.wc.ICryptoSelect_
 * - `OffersFilter` _xyz.swapee.wc.IOffersFilter_
 * - `ExchangeBroker` _xyz.swapee.wc.IExchangeBroker_
 * - `DealBroker` _xyz.swapee.wc.IDealBroker_
 * - `OfferExchange` _xyz.swapee.wc.IOfferExchange_
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntent_
 * - `ExchangeIdRow` _xyz.swapee.wc.IExchangeIdRow_
 * - `TransactionInfo` _xyz.swapee.wc.ITransactionInfo_
 * - `TransactionInfoHead` _xyz.swapee.wc.ITransactionInfoHead_
 * - `TransactionInfoHeadMob` _xyz.swapee.wc.ITransactionInfoHead_
 * @return {void}
 */
xyz.swapee.wc.back.IExchangePageCircuitDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IExchangePageCircuitDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/41-ExchangePageCircuitClasses.xml}  106b1b05316981ef2fc59c1cc11b397c */
/**
 * The classes of the _IExchangePageCircuitDisplay_.
 * @record xyz.swapee.wc.ExchangePageCircuitClasses
 */
xyz.swapee.wc.ExchangePageCircuitClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.ExchangePageCircuitClasses.prototype.Class = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ExchangePageCircuitClasses.prototype.props = /** @type {xyz.swapee.wc.ExchangePageCircuitClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/50-IExchangePageCircuitController.xml}  d6fe0be220c2813eb101ae9b7dd5b661 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IExchangePageCircuitController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IExchangePageCircuitController.Inputs, !xyz.swapee.wc.IExchangePageCircuitOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel>} xyz.swapee.wc.IExchangePageCircuitController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitController)} xyz.swapee.wc.AbstractExchangePageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitController} xyz.swapee.wc.ExchangePageCircuitController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitController` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitController
 */
xyz.swapee.wc.AbstractExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitController.constructor&xyz.swapee.wc.ExchangePageCircuitController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitController.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitController.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitController}
 */
xyz.swapee.wc.AbstractExchangePageCircuitController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitController}
 */
xyz.swapee.wc.AbstractExchangePageCircuitController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitController}
 */
xyz.swapee.wc.AbstractExchangePageCircuitController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitController}
 */
xyz.swapee.wc.AbstractExchangePageCircuitController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitController.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitController} xyz.swapee.wc.ExchangePageCircuitControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IExchangePageCircuitController.Inputs, !xyz.swapee.wc.IExchangePageCircuitOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IExchangePageCircuitOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IExchangePageCircuitController.Inputs, !xyz.swapee.wc.IExchangePageCircuitController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IExchangePageCircuitController.Inputs, !xyz.swapee.wc.ExchangePageCircuitMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangePageCircuitController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IExchangePageCircuitController.Inputs>)} xyz.swapee.wc.IExchangePageCircuitController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IExchangePageCircuitController
 */
xyz.swapee.wc.IExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.resetPort} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.setId} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.setId = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.unsetId} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.unsetId = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.setAmountFrom} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.setAmountFrom = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.unsetAmountFrom} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.unsetAmountFrom = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.setCryptoIn} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.setCryptoIn = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.unsetCryptoIn} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.unsetCryptoIn = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.setCryptoOut} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.setCryptoOut = function() {}
/** @type {xyz.swapee.wc.IExchangePageCircuitController.unsetCryptoOut} */
xyz.swapee.wc.IExchangePageCircuitController.prototype.unsetCryptoOut = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitController&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitController.Initialese>)} xyz.swapee.wc.ExchangePageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController} xyz.swapee.wc.IExchangePageCircuitController.typeof */
/**
 * A concrete class of _IExchangePageCircuitController_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitController
 * @implements {xyz.swapee.wc.IExchangePageCircuitController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitController.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitController.constructor&xyz.swapee.wc.IExchangePageCircuitController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitController}
 */
xyz.swapee.wc.ExchangePageCircuitController.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitController.
 * @interface xyz.swapee.wc.IExchangePageCircuitControllerFields
 */
xyz.swapee.wc.IExchangePageCircuitControllerFields = class { }
/**
 * The inputs to the _IExchangePageCircuit_'s controller.
 */
xyz.swapee.wc.IExchangePageCircuitControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangePageCircuitController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IExchangePageCircuitControllerFields.prototype.props = /** @type {xyz.swapee.wc.IExchangePageCircuitController} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitController} */
xyz.swapee.wc.RecordIExchangePageCircuitController

/** @typedef {xyz.swapee.wc.IExchangePageCircuitController} xyz.swapee.wc.BoundIExchangePageCircuitController */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitController} xyz.swapee.wc.BoundExchangePageCircuitController */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitPort.Inputs} xyz.swapee.wc.IExchangePageCircuitController.Inputs The inputs to the _IExchangePageCircuit_'s controller. */

/** @typedef {xyz.swapee.wc.IExchangePageCircuitPort.WeakInputs} xyz.swapee.wc.IExchangePageCircuitController.WeakInputs The inputs to the _IExchangePageCircuit_'s controller. */

/**
 * Contains getters to cast the _IExchangePageCircuitController_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitControllerCaster
 */
xyz.swapee.wc.IExchangePageCircuitControllerCaster = class { }
/**
 * Cast the _IExchangePageCircuitController_ instance into the _BoundIExchangePageCircuitController_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitController}
 */
xyz.swapee.wc.IExchangePageCircuitControllerCaster.prototype.asIExchangePageCircuitController
/**
 * Cast the _IExchangePageCircuitController_ instance into the _BoundIExchangePageCircuitProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitProcessor}
 */
xyz.swapee.wc.IExchangePageCircuitControllerCaster.prototype.asIExchangePageCircuitProcessor
/**
 * Access the _ExchangePageCircuitController_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitController}
 */
xyz.swapee.wc.IExchangePageCircuitControllerCaster.prototype.superExchangePageCircuitController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__resetPort<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.resetPort = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IExchangePageCircuitController.__setId
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__setId<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._setId */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.setId} */
/**
 * Sets the `id` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.setId = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitController.__unsetId
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__unsetId<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._unsetId */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.unsetId} */
/**
 * Clears the `id` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.unsetId = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IExchangePageCircuitController.__setAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__setAmountFrom<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._setAmountFrom */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.setAmountFrom} */
/**
 * Sets the `amountFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.setAmountFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitController.__unsetAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__unsetAmountFrom<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._unsetAmountFrom */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.unsetAmountFrom} */
/**
 * Clears the `amountFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.unsetAmountFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IExchangePageCircuitController.__setCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__setCryptoIn<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._setCryptoIn */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.setCryptoIn} */
/**
 * Sets the `cryptoIn` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.setCryptoIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitController.__unsetCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__unsetCryptoIn<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._unsetCryptoIn */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.unsetCryptoIn} */
/**
 * Clears the `cryptoIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.unsetCryptoIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IExchangePageCircuitController.__setCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__setCryptoOut<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._setCryptoOut */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.setCryptoOut} */
/**
 * Sets the `cryptoOut` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.setCryptoOut = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangePageCircuitController.__unsetCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangePageCircuitController.__unsetCryptoOut<!xyz.swapee.wc.IExchangePageCircuitController>} xyz.swapee.wc.IExchangePageCircuitController._unsetCryptoOut */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController.unsetCryptoOut} */
/**
 * Clears the `cryptoOut` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangePageCircuitController.unsetCryptoOut = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangePageCircuitController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/51-IExchangePageCircuitControllerFront.xml}  3f3be01eda0a29c30e1c9f1a18709df0 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IExchangePageCircuitController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangePageCircuitController)} xyz.swapee.wc.front.AbstractExchangePageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangePageCircuitController} xyz.swapee.wc.front.ExchangePageCircuitController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangePageCircuitController` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangePageCircuitController
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.front.AbstractExchangePageCircuitController.constructor&xyz.swapee.wc.front.ExchangePageCircuitController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangePageCircuitController.prototype.constructor = xyz.swapee.wc.front.AbstractExchangePageCircuitController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitController.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitController|typeof xyz.swapee.wc.front.ExchangePageCircuitController)|(!xyz.swapee.wc.front.IExchangePageCircuitControllerAT|typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitController}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitController}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitController|typeof xyz.swapee.wc.front.ExchangePageCircuitController)|(!xyz.swapee.wc.front.IExchangePageCircuitControllerAT|typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitController}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitController|typeof xyz.swapee.wc.front.ExchangePageCircuitController)|(!xyz.swapee.wc.front.IExchangePageCircuitControllerAT|typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitController}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangePageCircuitController.Initialese[]) => xyz.swapee.wc.front.IExchangePageCircuitController} xyz.swapee.wc.front.ExchangePageCircuitControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangePageCircuitControllerCaster&xyz.swapee.wc.front.IExchangePageCircuitControllerAT)} xyz.swapee.wc.front.IExchangePageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitControllerAT} xyz.swapee.wc.front.IExchangePageCircuitControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IExchangePageCircuitController
 */
xyz.swapee.wc.front.IExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.front.IExchangePageCircuitController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IExchangePageCircuitControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangePageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.setId} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.setId = function() {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.unsetId} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.unsetId = function() {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.setAmountFrom} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.setAmountFrom = function() {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.unsetAmountFrom} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.unsetAmountFrom = function() {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.setCryptoIn} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.setCryptoIn = function() {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.unsetCryptoIn} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.unsetCryptoIn = function() {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.setCryptoOut} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.setCryptoOut = function() {}
/** @type {xyz.swapee.wc.front.IExchangePageCircuitController.unsetCryptoOut} */
xyz.swapee.wc.front.IExchangePageCircuitController.prototype.unsetCryptoOut = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangePageCircuitController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangePageCircuitController.Initialese>)} xyz.swapee.wc.front.ExchangePageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController} xyz.swapee.wc.front.IExchangePageCircuitController.typeof */
/**
 * A concrete class of _IExchangePageCircuitController_ instances.
 * @constructor xyz.swapee.wc.front.ExchangePageCircuitController
 * @implements {xyz.swapee.wc.front.IExchangePageCircuitController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangePageCircuitController.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.front.ExchangePageCircuitController.constructor&xyz.swapee.wc.front.IExchangePageCircuitController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangePageCircuitController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangePageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangePageCircuitController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitController}
 */
xyz.swapee.wc.front.ExchangePageCircuitController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController} */
xyz.swapee.wc.front.RecordIExchangePageCircuitController

/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController} xyz.swapee.wc.front.BoundIExchangePageCircuitController */

/** @typedef {xyz.swapee.wc.front.ExchangePageCircuitController} xyz.swapee.wc.front.BoundExchangePageCircuitController */

/**
 * Contains getters to cast the _IExchangePageCircuitController_ interface.
 * @interface xyz.swapee.wc.front.IExchangePageCircuitControllerCaster
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerCaster = class { }
/**
 * Cast the _IExchangePageCircuitController_ instance into the _BoundIExchangePageCircuitController_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangePageCircuitController}
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerCaster.prototype.asIExchangePageCircuitController
/**
 * Access the _ExchangePageCircuitController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangePageCircuitController}
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerCaster.prototype.superExchangePageCircuitController

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__setId
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__setId<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._setId */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.setId} */
/**
 * Sets the `id` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.setId = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__unsetId
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__unsetId<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._unsetId */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.unsetId} */
/**
 * Clears the `id` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.unsetId = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__setAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__setAmountFrom<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._setAmountFrom */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.setAmountFrom} */
/**
 * Sets the `amountFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.setAmountFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__unsetAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__unsetAmountFrom<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._unsetAmountFrom */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.unsetAmountFrom} */
/**
 * Clears the `amountFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.unsetAmountFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__setCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__setCryptoIn<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._setCryptoIn */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.setCryptoIn} */
/**
 * Sets the `cryptoIn` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.setCryptoIn = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__unsetCryptoIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__unsetCryptoIn<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._unsetCryptoIn */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.unsetCryptoIn} */
/**
 * Clears the `cryptoIn` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.unsetCryptoIn = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__setCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__setCryptoOut<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._setCryptoOut */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.setCryptoOut} */
/**
 * Sets the `cryptoOut` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.setCryptoOut = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangePageCircuitController.__unsetCryptoOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitController.__unsetCryptoOut<!xyz.swapee.wc.front.IExchangePageCircuitController>} xyz.swapee.wc.front.IExchangePageCircuitController._unsetCryptoOut */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitController.unsetCryptoOut} */
/**
 * Clears the `cryptoOut` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangePageCircuitController.unsetCryptoOut = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IExchangePageCircuitController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/52-IExchangePageCircuitControllerBack.xml}  74568f8e6e6de1895768275bf4671590 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IExchangePageCircuitController.Inputs>&xyz.swapee.wc.IExchangePageCircuitController.Initialese} xyz.swapee.wc.back.IExchangePageCircuitController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangePageCircuitController)} xyz.swapee.wc.back.AbstractExchangePageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangePageCircuitController} xyz.swapee.wc.back.ExchangePageCircuitController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangePageCircuitController` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangePageCircuitController
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.back.AbstractExchangePageCircuitController.constructor&xyz.swapee.wc.back.ExchangePageCircuitController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangePageCircuitController.prototype.constructor = xyz.swapee.wc.back.AbstractExchangePageCircuitController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitController.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitController|typeof xyz.swapee.wc.back.ExchangePageCircuitController)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!com.webcircuits.INavigatorBack|typeof com.webcircuits.NavigatorBack)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitController}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitController}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitController|typeof xyz.swapee.wc.back.ExchangePageCircuitController)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!com.webcircuits.INavigatorBack|typeof com.webcircuits.NavigatorBack)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitController}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitController|typeof xyz.swapee.wc.back.ExchangePageCircuitController)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)|(!com.webcircuits.INavigatorBack|typeof com.webcircuits.NavigatorBack)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitController}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangePageCircuitController.Initialese[]) => xyz.swapee.wc.back.IExchangePageCircuitController} xyz.swapee.wc.back.ExchangePageCircuitControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangePageCircuitControllerCaster&xyz.swapee.wc.IExchangePageCircuitController&com.webcircuits.INavigatorBack<!xyz.swapee.wc.IExchangePageCircuitController.Inputs, !xyz.swapee.wc.ExchangePageCircuitMemory>&com.webcircuits.IDriverBack<!xyz.swapee.wc.IExchangePageCircuitController.Inputs>)} xyz.swapee.wc.back.IExchangePageCircuitController.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitController} xyz.swapee.wc.IExchangePageCircuitController.typeof */
/** @typedef {typeof com.webcircuits.INavigatorBack} com.webcircuits.INavigatorBack.typeof */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitController
 */
xyz.swapee.wc.back.IExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.back.IExchangePageCircuitController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangePageCircuitController.typeof&com.webcircuits.INavigatorBack.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangePageCircuitController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangePageCircuitController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitController.Initialese>)} xyz.swapee.wc.back.ExchangePageCircuitController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangePageCircuitController} xyz.swapee.wc.back.IExchangePageCircuitController.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangePageCircuitController_ instances.
 * @constructor xyz.swapee.wc.back.ExchangePageCircuitController
 * @implements {xyz.swapee.wc.back.IExchangePageCircuitController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitController.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangePageCircuitController = class extends /** @type {xyz.swapee.wc.back.ExchangePageCircuitController.constructor&xyz.swapee.wc.back.IExchangePageCircuitController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangePageCircuitController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangePageCircuitController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitController}
 */
xyz.swapee.wc.back.ExchangePageCircuitController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitController} */
xyz.swapee.wc.back.RecordIExchangePageCircuitController

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitController} xyz.swapee.wc.back.BoundIExchangePageCircuitController */

/** @typedef {xyz.swapee.wc.back.ExchangePageCircuitController} xyz.swapee.wc.back.BoundExchangePageCircuitController */

/**
 * Contains getters to cast the _IExchangePageCircuitController_ interface.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitControllerCaster
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerCaster = class { }
/**
 * Cast the _IExchangePageCircuitController_ instance into the _BoundIExchangePageCircuitController_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangePageCircuitController}
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerCaster.prototype.asIExchangePageCircuitController
/**
 * Access the _ExchangePageCircuitController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangePageCircuitController}
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerCaster.prototype.superExchangePageCircuitController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/53-IExchangePageCircuitControllerAR.xml}  93f36e762cff3fa6663e8f332e3658c6 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangePageCircuitController.Initialese} xyz.swapee.wc.back.IExchangePageCircuitControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangePageCircuitControllerAR)} xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR} xyz.swapee.wc.back.ExchangePageCircuitControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangePageCircuitControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.constructor&xyz.swapee.wc.back.ExchangePageCircuitControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitControllerAR|typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitControllerAR|typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitControllerAR|typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangePageCircuitController|typeof xyz.swapee.wc.ExchangePageCircuitController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangePageCircuitControllerAR.Initialese[]) => xyz.swapee.wc.back.IExchangePageCircuitControllerAR} xyz.swapee.wc.back.ExchangePageCircuitControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangePageCircuitControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangePageCircuitController)} xyz.swapee.wc.back.IExchangePageCircuitControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangePageCircuitControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitControllerAR
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerAR = class extends /** @type {xyz.swapee.wc.back.IExchangePageCircuitControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangePageCircuitController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangePageCircuitControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitControllerAR.Initialese>)} xyz.swapee.wc.back.ExchangePageCircuitControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangePageCircuitControllerAR} xyz.swapee.wc.back.IExchangePageCircuitControllerAR.typeof */
/**
 * A concrete class of _IExchangePageCircuitControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.ExchangePageCircuitControllerAR
 * @implements {xyz.swapee.wc.back.IExchangePageCircuitControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IExchangePageCircuitControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangePageCircuitControllerAR = class extends /** @type {xyz.swapee.wc.back.ExchangePageCircuitControllerAR.constructor&xyz.swapee.wc.back.IExchangePageCircuitControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangePageCircuitControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangePageCircuitControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitControllerAR}
 */
xyz.swapee.wc.back.ExchangePageCircuitControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitControllerAR} */
xyz.swapee.wc.back.RecordIExchangePageCircuitControllerAR

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitControllerAR} xyz.swapee.wc.back.BoundIExchangePageCircuitControllerAR */

/** @typedef {xyz.swapee.wc.back.ExchangePageCircuitControllerAR} xyz.swapee.wc.back.BoundExchangePageCircuitControllerAR */

/**
 * Contains getters to cast the _IExchangePageCircuitControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitControllerARCaster
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerARCaster = class { }
/**
 * Cast the _IExchangePageCircuitControllerAR_ instance into the _BoundIExchangePageCircuitControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangePageCircuitControllerAR}
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerARCaster.prototype.asIExchangePageCircuitControllerAR
/**
 * Access the _ExchangePageCircuitControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangePageCircuitControllerAR}
 */
xyz.swapee.wc.back.IExchangePageCircuitControllerARCaster.prototype.superExchangePageCircuitControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/54-IExchangePageCircuitControllerAT.xml}  6a2c333e1ed96dbade454372d3106d0c */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IExchangePageCircuitControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangePageCircuitControllerAT)} xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT} xyz.swapee.wc.front.ExchangePageCircuitControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangePageCircuitControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.constructor&xyz.swapee.wc.front.ExchangePageCircuitControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitControllerAT|typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitControllerAT|typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitControllerAT|typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangePageCircuitControllerAT.Initialese[]) => xyz.swapee.wc.front.IExchangePageCircuitControllerAT} xyz.swapee.wc.front.ExchangePageCircuitControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangePageCircuitControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IExchangePageCircuitControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangePageCircuitControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IExchangePageCircuitControllerAT
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerAT = class extends /** @type {xyz.swapee.wc.front.IExchangePageCircuitControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangePageCircuitControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangePageCircuitControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangePageCircuitControllerAT.Initialese>)} xyz.swapee.wc.front.ExchangePageCircuitControllerAT.constructor */
/**
 * A concrete class of _IExchangePageCircuitControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.ExchangePageCircuitControllerAT
 * @implements {xyz.swapee.wc.front.IExchangePageCircuitControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangePageCircuitControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangePageCircuitControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangePageCircuitControllerAT = class extends /** @type {xyz.swapee.wc.front.ExchangePageCircuitControllerAT.constructor&xyz.swapee.wc.front.IExchangePageCircuitControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangePageCircuitControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangePageCircuitControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangePageCircuitControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitControllerAT}
 */
xyz.swapee.wc.front.ExchangePageCircuitControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitControllerAT} */
xyz.swapee.wc.front.RecordIExchangePageCircuitControllerAT

/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitControllerAT} xyz.swapee.wc.front.BoundIExchangePageCircuitControllerAT */

/** @typedef {xyz.swapee.wc.front.ExchangePageCircuitControllerAT} xyz.swapee.wc.front.BoundExchangePageCircuitControllerAT */

/**
 * Contains getters to cast the _IExchangePageCircuitControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IExchangePageCircuitControllerATCaster
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerATCaster = class { }
/**
 * Cast the _IExchangePageCircuitControllerAT_ instance into the _BoundIExchangePageCircuitControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangePageCircuitControllerAT}
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerATCaster.prototype.asIExchangePageCircuitControllerAT
/**
 * Access the _ExchangePageCircuitControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangePageCircuitControllerAT}
 */
xyz.swapee.wc.front.IExchangePageCircuitControllerATCaster.prototype.superExchangePageCircuitControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/70-IExchangePageCircuitScreen.xml}  b39f680f27c1da122179bd6f39287869 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.front.ExchangePageCircuitInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangePageCircuitDisplay.Settings, !xyz.swapee.wc.IExchangePageCircuitDisplay.Queries, null>&xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese} xyz.swapee.wc.IExchangePageCircuitScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitScreen)} xyz.swapee.wc.AbstractExchangePageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitScreen} xyz.swapee.wc.ExchangePageCircuitScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitScreen` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitScreen
 */
xyz.swapee.wc.AbstractExchangePageCircuitScreen = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitScreen.constructor&xyz.swapee.wc.ExchangePageCircuitScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitScreen.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitScreen.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitScreen|typeof xyz.swapee.wc.ExchangePageCircuitScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangePageCircuitController|typeof xyz.swapee.wc.front.ExchangePageCircuitController)|(!xyz.swapee.wc.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.ExchangePageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitScreen}
 */
xyz.swapee.wc.AbstractExchangePageCircuitScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.AbstractExchangePageCircuitScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitScreen|typeof xyz.swapee.wc.ExchangePageCircuitScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangePageCircuitController|typeof xyz.swapee.wc.front.ExchangePageCircuitController)|(!xyz.swapee.wc.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.ExchangePageCircuitDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.AbstractExchangePageCircuitScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitScreen|typeof xyz.swapee.wc.ExchangePageCircuitScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangePageCircuitController|typeof xyz.swapee.wc.front.ExchangePageCircuitController)|(!xyz.swapee.wc.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.ExchangePageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.AbstractExchangePageCircuitScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitScreen.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitScreen} xyz.swapee.wc.ExchangePageCircuitScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.ExchangePageCircuitMemory, !xyz.swapee.wc.front.ExchangePageCircuitInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangePageCircuitDisplay.Settings, !xyz.swapee.wc.IExchangePageCircuitDisplay.Queries, null, null>&xyz.swapee.wc.front.IExchangePageCircuitController&xyz.swapee.wc.IExchangePageCircuitDisplay)} xyz.swapee.wc.IExchangePageCircuitScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IExchangePageCircuitScreen
 */
xyz.swapee.wc.IExchangePageCircuitScreen = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IExchangePageCircuitController.typeof&xyz.swapee.wc.IExchangePageCircuitDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitScreen.Initialese>)} xyz.swapee.wc.ExchangePageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangePageCircuitScreen} xyz.swapee.wc.IExchangePageCircuitScreen.typeof */
/**
 * A concrete class of _IExchangePageCircuitScreen_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitScreen
 * @implements {xyz.swapee.wc.IExchangePageCircuitScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitScreen.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitScreen = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitScreen.constructor&xyz.swapee.wc.IExchangePageCircuitScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.ExchangePageCircuitScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangePageCircuitScreen} */
xyz.swapee.wc.RecordIExchangePageCircuitScreen

/** @typedef {xyz.swapee.wc.IExchangePageCircuitScreen} xyz.swapee.wc.BoundIExchangePageCircuitScreen */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitScreen} xyz.swapee.wc.BoundExchangePageCircuitScreen */

/**
 * Contains getters to cast the _IExchangePageCircuitScreen_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitScreenCaster
 */
xyz.swapee.wc.IExchangePageCircuitScreenCaster = class { }
/**
 * Cast the _IExchangePageCircuitScreen_ instance into the _BoundIExchangePageCircuitScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitScreen}
 */
xyz.swapee.wc.IExchangePageCircuitScreenCaster.prototype.asIExchangePageCircuitScreen
/**
 * Access the _ExchangePageCircuitScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitScreen}
 */
xyz.swapee.wc.IExchangePageCircuitScreenCaster.prototype.superExchangePageCircuitScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/70-IExchangePageCircuitScreenBack.xml}  9a4be4ab591884a6fb1d2b89c3edc79f */
/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese} xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangePageCircuitScreen)} xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangePageCircuitScreen} xyz.swapee.wc.back.ExchangePageCircuitScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangePageCircuitScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangePageCircuitScreen
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen = class extends /** @type {xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.constructor&xyz.swapee.wc.back.ExchangePageCircuitScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.prototype.constructor = xyz.swapee.wc.back.AbstractExchangePageCircuitScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitScreen|typeof xyz.swapee.wc.back.ExchangePageCircuitScreen)|(!xyz.swapee.wc.back.IExchangePageCircuitScreenAT|typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitScreen|typeof xyz.swapee.wc.back.ExchangePageCircuitScreen)|(!xyz.swapee.wc.back.IExchangePageCircuitScreenAT|typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitScreen|typeof xyz.swapee.wc.back.ExchangePageCircuitScreen)|(!xyz.swapee.wc.back.IExchangePageCircuitScreenAT|typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese[]) => xyz.swapee.wc.back.IExchangePageCircuitScreen} xyz.swapee.wc.back.ExchangePageCircuitScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangePageCircuitScreenCaster&xyz.swapee.wc.back.IExchangePageCircuitScreenAT)} xyz.swapee.wc.back.IExchangePageCircuitScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangePageCircuitScreenAT} xyz.swapee.wc.back.IExchangePageCircuitScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitScreen
 */
xyz.swapee.wc.back.IExchangePageCircuitScreen = class extends /** @type {xyz.swapee.wc.back.IExchangePageCircuitScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangePageCircuitScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangePageCircuitScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangePageCircuitScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese>)} xyz.swapee.wc.back.ExchangePageCircuitScreen.constructor */
/**
 * A concrete class of _IExchangePageCircuitScreen_ instances.
 * @constructor xyz.swapee.wc.back.ExchangePageCircuitScreen
 * @implements {xyz.swapee.wc.back.IExchangePageCircuitScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangePageCircuitScreen = class extends /** @type {xyz.swapee.wc.back.ExchangePageCircuitScreen.constructor&xyz.swapee.wc.back.IExchangePageCircuitScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangePageCircuitScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreen}
 */
xyz.swapee.wc.back.ExchangePageCircuitScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitScreen} */
xyz.swapee.wc.back.RecordIExchangePageCircuitScreen

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitScreen} xyz.swapee.wc.back.BoundIExchangePageCircuitScreen */

/** @typedef {xyz.swapee.wc.back.ExchangePageCircuitScreen} xyz.swapee.wc.back.BoundExchangePageCircuitScreen */

/**
 * Contains getters to cast the _IExchangePageCircuitScreen_ interface.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitScreenCaster
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenCaster = class { }
/**
 * Cast the _IExchangePageCircuitScreen_ instance into the _BoundIExchangePageCircuitScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangePageCircuitScreen}
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenCaster.prototype.asIExchangePageCircuitScreen
/**
 * Access the _ExchangePageCircuitScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangePageCircuitScreen}
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenCaster.prototype.superExchangePageCircuitScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/73-IExchangePageCircuitScreenAR.xml}  d5a28bf7aad592198d820d8b98c5f1a3 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangePageCircuitScreen.Initialese} xyz.swapee.wc.front.IExchangePageCircuitScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangePageCircuitScreenAR)} xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR} xyz.swapee.wc.front.ExchangePageCircuitScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangePageCircuitScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.constructor&xyz.swapee.wc.front.ExchangePageCircuitScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitScreenAR|typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangePageCircuitScreen|typeof xyz.swapee.wc.ExchangePageCircuitScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitScreenAR|typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangePageCircuitScreen|typeof xyz.swapee.wc.ExchangePageCircuitScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangePageCircuitScreenAR|typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangePageCircuitScreen|typeof xyz.swapee.wc.ExchangePageCircuitScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangePageCircuitScreenAR.Initialese[]) => xyz.swapee.wc.front.IExchangePageCircuitScreenAR} xyz.swapee.wc.front.ExchangePageCircuitScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangePageCircuitScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangePageCircuitScreen)} xyz.swapee.wc.front.IExchangePageCircuitScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangePageCircuitScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IExchangePageCircuitScreenAR
 */
xyz.swapee.wc.front.IExchangePageCircuitScreenAR = class extends /** @type {xyz.swapee.wc.front.IExchangePageCircuitScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangePageCircuitScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangePageCircuitScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangePageCircuitScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangePageCircuitScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangePageCircuitScreenAR.Initialese>)} xyz.swapee.wc.front.ExchangePageCircuitScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangePageCircuitScreenAR} xyz.swapee.wc.front.IExchangePageCircuitScreenAR.typeof */
/**
 * A concrete class of _IExchangePageCircuitScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.ExchangePageCircuitScreenAR
 * @implements {xyz.swapee.wc.front.IExchangePageCircuitScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IExchangePageCircuitScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangePageCircuitScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangePageCircuitScreenAR = class extends /** @type {xyz.swapee.wc.front.ExchangePageCircuitScreenAR.constructor&xyz.swapee.wc.front.IExchangePageCircuitScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangePageCircuitScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangePageCircuitScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangePageCircuitScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangePageCircuitScreenAR}
 */
xyz.swapee.wc.front.ExchangePageCircuitScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitScreenAR} */
xyz.swapee.wc.front.RecordIExchangePageCircuitScreenAR

/** @typedef {xyz.swapee.wc.front.IExchangePageCircuitScreenAR} xyz.swapee.wc.front.BoundIExchangePageCircuitScreenAR */

/** @typedef {xyz.swapee.wc.front.ExchangePageCircuitScreenAR} xyz.swapee.wc.front.BoundExchangePageCircuitScreenAR */

/**
 * Contains getters to cast the _IExchangePageCircuitScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IExchangePageCircuitScreenARCaster
 */
xyz.swapee.wc.front.IExchangePageCircuitScreenARCaster = class { }
/**
 * Cast the _IExchangePageCircuitScreenAR_ instance into the _BoundIExchangePageCircuitScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangePageCircuitScreenAR}
 */
xyz.swapee.wc.front.IExchangePageCircuitScreenARCaster.prototype.asIExchangePageCircuitScreenAR
/**
 * Access the _ExchangePageCircuitScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangePageCircuitScreenAR}
 */
xyz.swapee.wc.front.IExchangePageCircuitScreenARCaster.prototype.superExchangePageCircuitScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/74-IExchangePageCircuitScreenAT.xml}  854cdfa46d7adac3c1cceeb07fa61dfa */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangePageCircuitScreenAT)} xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT} xyz.swapee.wc.back.ExchangePageCircuitScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangePageCircuitScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.constructor&xyz.swapee.wc.back.ExchangePageCircuitScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitScreenAT|typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitScreenAT|typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangePageCircuitScreenAT|typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese[]) => xyz.swapee.wc.back.IExchangePageCircuitScreenAT} xyz.swapee.wc.back.ExchangePageCircuitScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangePageCircuitScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IExchangePageCircuitScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangePageCircuitScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitScreenAT
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenAT = class extends /** @type {xyz.swapee.wc.back.IExchangePageCircuitScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangePageCircuitScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese>)} xyz.swapee.wc.back.ExchangePageCircuitScreenAT.constructor */
/**
 * A concrete class of _IExchangePageCircuitScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.ExchangePageCircuitScreenAT
 * @implements {xyz.swapee.wc.back.IExchangePageCircuitScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangePageCircuitScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangePageCircuitScreenAT = class extends /** @type {xyz.swapee.wc.back.ExchangePageCircuitScreenAT.constructor&xyz.swapee.wc.back.IExchangePageCircuitScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangePageCircuitScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangePageCircuitScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangePageCircuitScreenAT}
 */
xyz.swapee.wc.back.ExchangePageCircuitScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitScreenAT} */
xyz.swapee.wc.back.RecordIExchangePageCircuitScreenAT

/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitScreenAT} xyz.swapee.wc.back.BoundIExchangePageCircuitScreenAT */

/** @typedef {xyz.swapee.wc.back.ExchangePageCircuitScreenAT} xyz.swapee.wc.back.BoundExchangePageCircuitScreenAT */

/**
 * Contains getters to cast the _IExchangePageCircuitScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IExchangePageCircuitScreenATCaster
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenATCaster = class { }
/**
 * Cast the _IExchangePageCircuitScreenAT_ instance into the _BoundIExchangePageCircuitScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangePageCircuitScreenAT}
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenATCaster.prototype.asIExchangePageCircuitScreenAT
/**
 * Access the _ExchangePageCircuitScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangePageCircuitScreenAT}
 */
xyz.swapee.wc.back.IExchangePageCircuitScreenATCaster.prototype.superExchangePageCircuitScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/design/80-IExchangePageCircuitGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IExchangePageCircuitDisplay.Initialese} xyz.swapee.wc.IExchangePageCircuitGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangePageCircuitGPU)} xyz.swapee.wc.AbstractExchangePageCircuitGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangePageCircuitGPU} xyz.swapee.wc.ExchangePageCircuitGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitGPU` interface.
 * @constructor xyz.swapee.wc.AbstractExchangePageCircuitGPU
 */
xyz.swapee.wc.AbstractExchangePageCircuitGPU = class extends /** @type {xyz.swapee.wc.AbstractExchangePageCircuitGPU.constructor&xyz.swapee.wc.ExchangePageCircuitGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangePageCircuitGPU.prototype.constructor = xyz.swapee.wc.AbstractExchangePageCircuitGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangePageCircuitGPU.class = /** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitGPU|typeof xyz.swapee.wc.ExchangePageCircuitGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangePageCircuitGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangePageCircuitGPU}
 */
xyz.swapee.wc.AbstractExchangePageCircuitGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitGPU}
 */
xyz.swapee.wc.AbstractExchangePageCircuitGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitGPU|typeof xyz.swapee.wc.ExchangePageCircuitGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitGPU}
 */
xyz.swapee.wc.AbstractExchangePageCircuitGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangePageCircuitGPU|typeof xyz.swapee.wc.ExchangePageCircuitGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangePageCircuitDisplay|typeof xyz.swapee.wc.back.ExchangePageCircuitDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitGPU}
 */
xyz.swapee.wc.AbstractExchangePageCircuitGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangePageCircuitGPU.Initialese[]) => xyz.swapee.wc.IExchangePageCircuitGPU} xyz.swapee.wc.ExchangePageCircuitGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangePageCircuitGPUCaster&com.webcircuits.IBrowserView<.!ExchangePageCircuitMemory,.!ExchangePageCircuitLand>&xyz.swapee.wc.back.IExchangePageCircuitDisplay)} xyz.swapee.wc.IExchangePageCircuitGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!ExchangePageCircuitMemory,.!ExchangePageCircuitLand>} com.webcircuits.IBrowserView<.!ExchangePageCircuitMemory,.!ExchangePageCircuitLand>.typeof */
/**
 * Handles the periphery of the _IExchangePageCircuitDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IExchangePageCircuitGPU
 */
xyz.swapee.wc.IExchangePageCircuitGPU = class extends /** @type {xyz.swapee.wc.IExchangePageCircuitGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!ExchangePageCircuitMemory,.!ExchangePageCircuitLand>.typeof&xyz.swapee.wc.back.IExchangePageCircuitDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangePageCircuitGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangePageCircuitGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangePageCircuitGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitGPU.Initialese>)} xyz.swapee.wc.ExchangePageCircuitGPU.constructor */
/**
 * A concrete class of _IExchangePageCircuitGPU_ instances.
 * @constructor xyz.swapee.wc.ExchangePageCircuitGPU
 * @implements {xyz.swapee.wc.IExchangePageCircuitGPU} Handles the periphery of the _IExchangePageCircuitDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangePageCircuitGPU.Initialese>} ‎
 */
xyz.swapee.wc.ExchangePageCircuitGPU = class extends /** @type {xyz.swapee.wc.ExchangePageCircuitGPU.constructor&xyz.swapee.wc.IExchangePageCircuitGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangePageCircuitGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangePageCircuitGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangePageCircuitGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangePageCircuitGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangePageCircuitGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangePageCircuitGPU}
 */
xyz.swapee.wc.ExchangePageCircuitGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangePageCircuitGPU.
 * @interface xyz.swapee.wc.IExchangePageCircuitGPUFields
 */
xyz.swapee.wc.IExchangePageCircuitGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IExchangePageCircuitGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangePageCircuitGPU} */
xyz.swapee.wc.RecordIExchangePageCircuitGPU

/** @typedef {xyz.swapee.wc.IExchangePageCircuitGPU} xyz.swapee.wc.BoundIExchangePageCircuitGPU */

/** @typedef {xyz.swapee.wc.ExchangePageCircuitGPU} xyz.swapee.wc.BoundExchangePageCircuitGPU */

/**
 * Contains getters to cast the _IExchangePageCircuitGPU_ interface.
 * @interface xyz.swapee.wc.IExchangePageCircuitGPUCaster
 */
xyz.swapee.wc.IExchangePageCircuitGPUCaster = class { }
/**
 * Cast the _IExchangePageCircuitGPU_ instance into the _BoundIExchangePageCircuitGPU_ type.
 * @type {!xyz.swapee.wc.BoundIExchangePageCircuitGPU}
 */
xyz.swapee.wc.IExchangePageCircuitGPUCaster.prototype.asIExchangePageCircuitGPU
/**
 * Access the _ExchangePageCircuitGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangePageCircuitGPU}
 */
xyz.swapee.wc.IExchangePageCircuitGPUCaster.prototype.superExchangePageCircuitGPU

// nss:xyz.swapee.wc
/* @typal-end */