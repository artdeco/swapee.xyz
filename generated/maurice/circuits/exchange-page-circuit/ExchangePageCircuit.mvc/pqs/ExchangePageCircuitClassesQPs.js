import ExchangePageCircuitClassesPQs from './ExchangePageCircuitClassesPQs'
export const ExchangePageCircuitClassesQPs=/**@type {!xyz.swapee.wc.ExchangePageCircuitClassesQPs}*/(Object.keys(ExchangePageCircuitClassesPQs)
 .reduce((a,k)=>{a[ExchangePageCircuitClassesPQs[k]]=k;return a},{}))