import {ExchangePageCircuitMemoryPQs} from './ExchangePageCircuitMemoryPQs'
export const ExchangePageCircuitMemoryQPs=/**@type {!xyz.swapee.wc.ExchangePageCircuitMemoryQPs}*/(Object.keys(ExchangePageCircuitMemoryPQs)
 .reduce((a,k)=>{a[ExchangePageCircuitMemoryPQs[k]]=k;return a},{}))