import {ExchangePageCircuitInputsPQs} from './ExchangePageCircuitInputsPQs'
export const ExchangePageCircuitInputsQPs=/**@type {!xyz.swapee.wc.ExchangePageCircuitInputsQPs}*/(Object.keys(ExchangePageCircuitInputsPQs)
 .reduce((a,k)=>{a[ExchangePageCircuitInputsPQs[k]]=k;return a},{}))