import {ExchangePageCircuitQueriesPQs} from './ExchangePageCircuitQueriesPQs'
export const ExchangePageCircuitQueriesQPs=/**@type {!xyz.swapee.wc.ExchangePageCircuitQueriesQPs}*/(Object.keys(ExchangePageCircuitQueriesPQs)
 .reduce((a,k)=>{a[ExchangePageCircuitQueriesPQs[k]]=k;return a},{}))