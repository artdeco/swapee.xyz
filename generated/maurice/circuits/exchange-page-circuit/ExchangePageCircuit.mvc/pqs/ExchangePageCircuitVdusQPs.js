import {ExchangePageCircuitVdusPQs} from './ExchangePageCircuitVdusPQs'
export const ExchangePageCircuitVdusQPs=/**@type {!xyz.swapee.wc.ExchangePageCircuitVdusQPs}*/(Object.keys(ExchangePageCircuitVdusPQs)
 .reduce((a,k)=>{a[ExchangePageCircuitVdusPQs[k]]=k;return a},{}))