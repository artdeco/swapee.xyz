import {ExchangePageCircuitMemoryPQs} from './ExchangePageCircuitMemoryPQs'
export const ExchangePageCircuitInputsPQs=/**@type {!xyz.swapee.wc.ExchangePageCircuitInputsQPs}*/({
 ...ExchangePageCircuitMemoryPQs,
})