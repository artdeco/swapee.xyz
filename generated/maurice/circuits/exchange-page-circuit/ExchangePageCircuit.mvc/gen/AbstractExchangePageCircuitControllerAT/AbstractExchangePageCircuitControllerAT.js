import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitControllerAT}
 */
function __AbstractExchangePageCircuitControllerAT() {}
__AbstractExchangePageCircuitControllerAT.prototype = /** @type {!_AbstractExchangePageCircuitControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT}
 */
class _AbstractExchangePageCircuitControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangePageCircuitControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT} ‎
 */
class AbstractExchangePageCircuitControllerAT extends newAbstract(
 _AbstractExchangePageCircuitControllerAT,773590659624,null,{
  asIExchangePageCircuitControllerAT:1,
  superExchangePageCircuitControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT} */
AbstractExchangePageCircuitControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitControllerAT} */
function AbstractExchangePageCircuitControllerATClass(){}

export default AbstractExchangePageCircuitControllerAT


AbstractExchangePageCircuitControllerAT[$implementations]=[
 __AbstractExchangePageCircuitControllerAT,
 UartUniversal,
 AbstractExchangePageCircuitControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangePageCircuitControllerAT}*/({
  get asIExchangePageCircuitController(){
   return this
  },
  setId(){
   this.uart.t("inv",{mid:'30fe8',args:[...arguments]})
  },
  unsetId(){
   this.uart.t("inv",{mid:'c8be8'})
  },
  setAmountFrom(){
   this.uart.t("inv",{mid:'5781c',args:[...arguments]})
  },
  unsetAmountFrom(){
   this.uart.t("inv",{mid:'eccc4'})
  },
  setCryptoIn(){
   this.uart.t("inv",{mid:'59e6a',args:[...arguments]})
  },
  unsetCryptoIn(){
   this.uart.t("inv",{mid:'53795'})
  },
  setCryptoOut(){
   this.uart.t("inv",{mid:'30df4',args:[...arguments]})
  },
  unsetCryptoOut(){
   this.uart.t("inv",{mid:'cbc5c'})
  },
 }),
]