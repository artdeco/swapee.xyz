import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitScreenAT}
 */
function __AbstractExchangePageCircuitScreenAT() {}
__AbstractExchangePageCircuitScreenAT.prototype = /** @type {!_AbstractExchangePageCircuitScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT}
 */
class _AbstractExchangePageCircuitScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangePageCircuitScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT} ‎
 */
class AbstractExchangePageCircuitScreenAT extends newAbstract(
 _AbstractExchangePageCircuitScreenAT,773590659628,null,{
  asIExchangePageCircuitScreenAT:1,
  superExchangePageCircuitScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT} */
AbstractExchangePageCircuitScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreenAT} */
function AbstractExchangePageCircuitScreenATClass(){}

export default AbstractExchangePageCircuitScreenAT


AbstractExchangePageCircuitScreenAT[$implementations]=[
 __AbstractExchangePageCircuitScreenAT,
 UartUniversal,
]