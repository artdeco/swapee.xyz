import AbstractExchangePageCircuitGPU from '../AbstractExchangePageCircuitGPU'
import AbstractExchangePageCircuitScreenBack from '../AbstractExchangePageCircuitScreenBack'
import {HtmlComponent,Landed,makeRevealConcealPaints,mvc} from '@webcircuits/webcircuits'
import {ExchangePageCircuitInputsQPs} from '../../pqs/ExchangePageCircuitInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractExchangePageCircuit from '../AbstractExchangePageCircuit'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitHtmlComponent}
 */
function __AbstractExchangePageCircuitHtmlComponent() {}
__AbstractExchangePageCircuitHtmlComponent.prototype = /** @type {!_AbstractExchangePageCircuitHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent}
 */
class _AbstractExchangePageCircuitHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ExchangePageCircuitHtmlComponent} */ (res)
  }
}
/**
 * The _IExchangePageCircuit_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent} ‎
 */
export class AbstractExchangePageCircuitHtmlComponent extends newAbstract(
 _AbstractExchangePageCircuitHtmlComponent,773590659612,null,{
  asIExchangePageCircuitHtmlComponent:1,
  superExchangePageCircuitHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent} */
AbstractExchangePageCircuitHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent} */
function AbstractExchangePageCircuitHtmlComponentClass(){}


AbstractExchangePageCircuitHtmlComponent[$implementations]=[
 __AbstractExchangePageCircuitHtmlComponent,
 HtmlComponent,
 AbstractExchangePageCircuit,
 AbstractExchangePageCircuitGPU,
 AbstractExchangePageCircuitScreenBack,
 Landed,
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  constructor(){
   this.land={
    CryptoSelectIn:null,
    CryptoSelectOut:null,
    OffersFilter:null,
    ExchangeBroker:null,
    DealBroker:null,
    OfferExchange:null,
    ExchangeIntent:null,
    ExchangeIdRow:null,
    TransactionInfo:null,
    TransactionInfoHead:null,
    TransactionInfoHeadMob:null,
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  inputsQPs:ExchangePageCircuitInputsQPs,
 }),

/** @type {!AbstractExchangePageCircuitHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/function paintCryptoInLa() {
   this.CryptoInLa.setText(this.model.cryptoIn)
  },/**@this {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/function paintCryptoOutLa() {
   this.CryptoOutLa.setText(this.model.cryptoOut)
  },/**@this {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/function paintAmountFromLa() {
   this.AmountFromLa.setText(this.model.amountFrom)
  }
 ] }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint:makeRevealConcealPaints({
   RevealWithIds:{id:1},
   ConcealWithIds:{id:0},
  }),
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint:function paint_InImWrs_Content(_,{CryptoSelectIn:CryptoSelectIn}){
   if(!CryptoSelectIn) return
   let{
    '73071':selectedIcon,
   }=CryptoSelectIn.model
   const{asIExchangePageCircuitGPU:{InImWrs:InImWrs}}=this
   for(const InImWr of InImWrs){
    InImWr.setImg(selectedIcon)
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint:function paint_OutImWrs_Content(_,{CryptoSelectOut:CryptoSelectOut}){
   if(!CryptoSelectOut) return
   let{
    '73071':selectedIcon,
   }=CryptoSelectOut.model
   const{asIExchangePageCircuitGPU:{OutImWrs:OutImWrs}}=this
   for(const OutImWr of OutImWrs){
    OutImWr.setImg(selectedIcon)
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint:function interrupt_click_on_RestartButtons(_,{OfferExchange:OfferExchange}){
   if(!OfferExchange) return
   const{asIExchangePageCircuitGPU:{RestartButtons:RestartButtons}}=this
   for(const RestartButton of RestartButtons){
    RestartButton.listen('click',(ev)=>{
     OfferExchange['a4b41']() // reset
    })
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint:function $conceal_ConcealWithTid(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '97bea':tid,
   }=TransactionInfo.model
   const{
    asIExchangePageCircuitGPU:{ConcealWithTids:ConcealWithTids},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(ConcealWithTids,tid)
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint:function $reveal_RevealWithTid(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   let{
    '97bea':tid,
   }=TransactionInfo.model
   const{
    asIExchangePageCircuitGPU:{RevealWithTids:RevealWithTids},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(RevealWithTids,tid)
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIExchangePageCircuitGPU:{
     CryptoSelectIn:CryptoSelectIn,
     CryptoSelectOut:CryptoSelectOut,
     OffersFilter:OffersFilter,
     ExchangeBroker:ExchangeBroker,
     DealBroker:DealBroker,
     OfferExchange:OfferExchange,
     ExchangeIntent:ExchangeIntent,
     ExchangeIdRow:ExchangeIdRow,
     TransactionInfo:TransactionInfo,
     TransactionInfoHead:TransactionInfoHead,
     TransactionInfoHeadMob:TransactionInfoHeadMob,
    },
   }=this
   complete(3545350742,{CryptoSelectIn:CryptoSelectIn})
   complete(3545350742,{CryptoSelectOut:CryptoSelectOut})
   complete(8066530051,{OffersFilter:OffersFilter},{
    /**@this {xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/
    get['c125f'](){ // -> amountOut
     const{land:{DealBroker:_DealBroker}}=this
     if(!_DealBroker) return void 0
     const estimatedAmountTo=_DealBroker.model['c63f7'] // <- estimatedAmountTo
     return estimatedAmountTo
    },
   })
   complete(1683059665,{ExchangeBroker:ExchangeBroker})
   complete(3427226314,{DealBroker:DealBroker})
   complete(6529339651,{OfferExchange:OfferExchange})
   complete(7833868048,{ExchangeIntent:ExchangeIntent},{
    cryptoIn:'96c88', // -> currencyFrom
    cryptoOut:'c23cd', // -> currencyTo
    amountFrom:'748e6', // -> amountFrom
    fixedRate:'cec31', // -> fixed
    floatingRate:'546ad', // -> float
    notId:'b2fda', // -> ready
   })
   complete(1099475227,{ExchangeIdRow:ExchangeIdRow},{
    /**@this {xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/
    get['cec31'](){ // -> fixed
     const{land:{TransactionInfo:_TransactionInfo}}=this
     if(!_TransactionInfo) return void 0
     const fixed=_TransactionInfo.model['cec31'] // <- fixed
     return fixed
    },
    id:'b80bb', // -> id
   })
   complete(1988051819,{TransactionInfo:TransactionInfo},{
    id:'97bea', // -> tid
   })
   complete(4300544218,{TransactionInfoHead:TransactionInfoHead})
   complete(4300544218,{TransactionInfoHeadMob:TransactionInfoHeadMob})
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint(_,{OffersFilter:OffersFilter,OfferExchange:OfferExchange}){
   const land={OffersFilter:OffersFilter,OfferExchange:OfferExchange}
   if(!land.OffersFilter||!land.OfferExchange) return
   OffersFilter['d7159']=OfferExchange['d98fc'] // onSwapDirection=swapAddresses
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint(_,{OfferExchange:OfferExchange}){
   if(!OfferExchange) return
   const{asIExchangePageCircuitController:{
    unsetId:unsetId,
   }}=this
   OfferExchange['df85f']=()=>{unsetId()} // onReset -> unsetId
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint({id:id},{OfferExchange:OfferExchange}){
   if(!OfferExchange) return
   if(!id) {
    OfferExchange['a4b41']() // reset()
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const currencyOut=ExchangeIntent.model['98dbb']
   if(currencyOut) {
    DealBroker['e0519']() // pulseGetOffer()
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const currencyIn=ExchangeIntent.model['f3088']
   if(currencyIn) {
    DealBroker['e0519']() // pulseGetOffer()
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const fixed=ExchangeIntent.model['cec31']
   DealBroker['e0519']() // pulseGetOffer()
   this.void({'value':fixed})
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const amountFrom=ExchangeIntent.model['748e6']
   if(amountFrom) {
    DealBroker['e0519']() // pulseGetOffer()
   }
  },
 }),
 AbstractExchangePageCircuitHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
  void:function voidValue(value){
   value?value['value']:void 0
  },
 }),
]