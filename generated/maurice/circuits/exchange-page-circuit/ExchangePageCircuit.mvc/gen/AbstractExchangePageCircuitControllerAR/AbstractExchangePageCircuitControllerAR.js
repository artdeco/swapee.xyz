import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitControllerAR}
 */
function __AbstractExchangePageCircuitControllerAR() {}
__AbstractExchangePageCircuitControllerAR.prototype = /** @type {!_AbstractExchangePageCircuitControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR}
 */
class _AbstractExchangePageCircuitControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangePageCircuitControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR} ‎
 */
class AbstractExchangePageCircuitControllerAR extends newAbstract(
 _AbstractExchangePageCircuitControllerAR,773590659623,null,{
  asIExchangePageCircuitControllerAR:1,
  superExchangePageCircuitControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR} */
AbstractExchangePageCircuitControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitControllerAR} */
function AbstractExchangePageCircuitControllerARClass(){}

export default AbstractExchangePageCircuitControllerAR


AbstractExchangePageCircuitControllerAR[$implementations]=[
 __AbstractExchangePageCircuitControllerAR,
 AR,
 AbstractExchangePageCircuitControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangePageCircuitControllerAR}*/({
  allocator(){
   this.methods={
    setId:'30fe8',
    unsetId:'c8be8',
    setAmountFrom:'5781c',
    unsetAmountFrom:'eccc4',
    setCryptoIn:'59e6a',
    unsetCryptoIn:'53795',
    setCryptoOut:'30df4',
    unsetCryptoOut:'cbc5c',
   }
  },
 }),
]