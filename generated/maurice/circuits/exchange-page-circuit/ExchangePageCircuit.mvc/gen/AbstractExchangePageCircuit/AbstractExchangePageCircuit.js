import AbstractExchangePageCircuitProcessor from '../AbstractExchangePageCircuitProcessor'
import {ExchangePageCircuitCore} from '../ExchangePageCircuitCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractExchangePageCircuitComputer} from '../AbstractExchangePageCircuitComputer'
import {AbstractExchangePageCircuitController} from '../AbstractExchangePageCircuitController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuit}
 */
function __AbstractExchangePageCircuit() {}
__AbstractExchangePageCircuit.prototype = /** @type {!_AbstractExchangePageCircuit} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuit}
 */
class _AbstractExchangePageCircuit { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuit} ‎
 */
class AbstractExchangePageCircuit extends newAbstract(
 _AbstractExchangePageCircuit,77359065969,null,{
  asIExchangePageCircuit:1,
  superExchangePageCircuit:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuit} */
AbstractExchangePageCircuit.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuit} */
function AbstractExchangePageCircuitClass(){}

export default AbstractExchangePageCircuit


AbstractExchangePageCircuit[$implementations]=[
 __AbstractExchangePageCircuit,
 ExchangePageCircuitCore,
 AbstractExchangePageCircuitProcessor,
 IntegratedComponent,
 AbstractExchangePageCircuitComputer,
 AbstractExchangePageCircuitController,
]


export {AbstractExchangePageCircuit}