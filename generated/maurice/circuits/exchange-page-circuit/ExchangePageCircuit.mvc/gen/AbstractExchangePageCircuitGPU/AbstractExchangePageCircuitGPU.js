import AbstractExchangePageCircuitDisplay from '../AbstractExchangePageCircuitDisplayBack'
import ExchangePageCircuitClassesPQs from '../../pqs/ExchangePageCircuitClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {ExchangePageCircuitClassesQPs} from '../../pqs/ExchangePageCircuitClassesQPs'
import {ExchangePageCircuitVdusPQs} from '../../pqs/ExchangePageCircuitVdusPQs'
import {ExchangePageCircuitVdusQPs} from '../../pqs/ExchangePageCircuitVdusQPs'
import {ExchangePageCircuitMemoryPQs} from '../../pqs/ExchangePageCircuitMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitGPU}
 */
function __AbstractExchangePageCircuitGPU() {}
__AbstractExchangePageCircuitGPU.prototype = /** @type {!_AbstractExchangePageCircuitGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitGPU}
 */
class _AbstractExchangePageCircuitGPU { }
/**
 * Handles the periphery of the _IExchangePageCircuitDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitGPU} ‎
 */
class AbstractExchangePageCircuitGPU extends newAbstract(
 _AbstractExchangePageCircuitGPU,773590659616,null,{
  asIExchangePageCircuitGPU:1,
  superExchangePageCircuitGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitGPU} */
AbstractExchangePageCircuitGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitGPU} */
function AbstractExchangePageCircuitGPUClass(){}

export default AbstractExchangePageCircuitGPU


AbstractExchangePageCircuitGPU[$implementations]=[
 __AbstractExchangePageCircuitGPU,
 AbstractExchangePageCircuitGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitGPU}*/({
  classesQPs:ExchangePageCircuitClassesQPs,
  vdusPQs:ExchangePageCircuitVdusPQs,
  vdusQPs:ExchangePageCircuitVdusQPs,
  memoryPQs:ExchangePageCircuitMemoryPQs,
 }),
 AbstractExchangePageCircuitDisplay,
 BrowserView,
 AbstractExchangePageCircuitGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitGPU}*/({
  allocator(){
   pressFit(this.classes,'',ExchangePageCircuitClassesPQs)
  },
 }),
]