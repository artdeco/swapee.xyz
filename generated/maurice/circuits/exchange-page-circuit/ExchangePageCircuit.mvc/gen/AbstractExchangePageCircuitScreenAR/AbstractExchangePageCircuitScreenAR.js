import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitScreenAR}
 */
function __AbstractExchangePageCircuitScreenAR() {}
__AbstractExchangePageCircuitScreenAR.prototype = /** @type {!_AbstractExchangePageCircuitScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR}
 */
class _AbstractExchangePageCircuitScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangePageCircuitScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR} ‎
 */
class AbstractExchangePageCircuitScreenAR extends newAbstract(
 _AbstractExchangePageCircuitScreenAR,773590659627,null,{
  asIExchangePageCircuitScreenAR:1,
  superExchangePageCircuitScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR} */
AbstractExchangePageCircuitScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangePageCircuitScreenAR} */
function AbstractExchangePageCircuitScreenARClass(){}

export default AbstractExchangePageCircuitScreenAR


AbstractExchangePageCircuitScreenAR[$implementations]=[
 __AbstractExchangePageCircuitScreenAR,
 AR,
 AbstractExchangePageCircuitScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangePageCircuitScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractExchangePageCircuitScreenAR}