import ExchangePageCircuitClassesPQs from '../../pqs/ExchangePageCircuitClassesPQs'
import AbstractExchangePageCircuitScreenAR from '../AbstractExchangePageCircuitScreenAR'
import {CoreCache,Screen,AddressBar} from '@webcircuits/front'
import {ExchangePageCircuitInputsPQs} from '../../pqs/ExchangePageCircuitInputsPQs'
import {ExchangePageCircuitQueriesPQs} from '../../pqs/ExchangePageCircuitQueriesPQs'
import {ExchangePageCircuitMemoryQPs} from '../../pqs/ExchangePageCircuitMemoryQPs'
import {ExchangePageCircuitVdusPQs} from '../../pqs/ExchangePageCircuitVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitScreen}
 */
function __AbstractExchangePageCircuitScreen() {}
__AbstractExchangePageCircuitScreen.prototype = /** @type {!_AbstractExchangePageCircuitScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitScreen}
 */
class _AbstractExchangePageCircuitScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitScreen} ‎
 */
class AbstractExchangePageCircuitScreen extends newAbstract(
 _AbstractExchangePageCircuitScreen,773590659625,null,{
  asIExchangePageCircuitScreen:1,
  superExchangePageCircuitScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitScreen} */
AbstractExchangePageCircuitScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitScreen} */
function AbstractExchangePageCircuitScreenClass(){}

export default AbstractExchangePageCircuitScreen


AbstractExchangePageCircuitScreen[$implementations]=[
 __AbstractExchangePageCircuitScreen,
 AbstractExchangePageCircuitScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitScreen}*/({
  deduceInputs(){
   const{asIExchangePageCircuitDisplay:{
    CryptoInLa:CryptoInLa,
    CryptoOutLa:CryptoOutLa,
    AmountFromLa:AmountFromLa,
   }}=this
   return{
    cryptoIn:CryptoInLa?.innerText,
    cryptoOut:CryptoOutLa?.innerText,
    amountFrom:AmountFromLa?.innerText,
   }
  },
 }),
 AbstractExchangePageCircuitScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitScreen}*/({
  inputsPQs:ExchangePageCircuitInputsPQs,
  classesPQs:ExchangePageCircuitClassesPQs,
  queriesPQs:ExchangePageCircuitQueriesPQs,
  memoryQPs:ExchangePageCircuitMemoryQPs,
 }),
 Screen,
 AbstractExchangePageCircuitScreenAR,
 AddressBar,
 AbstractExchangePageCircuitScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitScreen}*/({
  vdusPQs:ExchangePageCircuitVdusPQs,
 }),
]