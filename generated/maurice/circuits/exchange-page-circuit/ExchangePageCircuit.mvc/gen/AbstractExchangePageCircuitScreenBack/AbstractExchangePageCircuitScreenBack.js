import AbstractExchangePageCircuitScreenAT from '../AbstractExchangePageCircuitScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitScreenBack}
 */
function __AbstractExchangePageCircuitScreenBack() {}
__AbstractExchangePageCircuitScreenBack.prototype = /** @type {!_AbstractExchangePageCircuitScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitScreen}
 */
class _AbstractExchangePageCircuitScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitScreen} ‎
 */
class AbstractExchangePageCircuitScreenBack extends newAbstract(
 _AbstractExchangePageCircuitScreenBack,773590659626,null,{
  asIExchangePageCircuitScreen:1,
  superExchangePageCircuitScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreen} */
AbstractExchangePageCircuitScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitScreen} */
function AbstractExchangePageCircuitScreenBackClass(){}

export default AbstractExchangePageCircuitScreenBack


AbstractExchangePageCircuitScreenBack[$implementations]=[
 __AbstractExchangePageCircuitScreenBack,
 AbstractExchangePageCircuitScreenAT,
]