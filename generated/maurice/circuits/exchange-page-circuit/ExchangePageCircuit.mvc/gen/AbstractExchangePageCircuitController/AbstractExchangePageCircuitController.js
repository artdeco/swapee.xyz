import ExchangePageCircuitBuffer from '../ExchangePageCircuitBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ExchangePageCircuitPortConnector} from '../ExchangePageCircuitPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitController}
 */
function __AbstractExchangePageCircuitController() {}
__AbstractExchangePageCircuitController.prototype = /** @type {!_AbstractExchangePageCircuitController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitController}
 */
class _AbstractExchangePageCircuitController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitController} ‎
 */
export class AbstractExchangePageCircuitController extends newAbstract(
 _AbstractExchangePageCircuitController,773590659620,null,{
  asIExchangePageCircuitController:1,
  superExchangePageCircuitController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitController} */
AbstractExchangePageCircuitController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitController} */
function AbstractExchangePageCircuitControllerClass(){}


AbstractExchangePageCircuitController[$implementations]=[
 AbstractExchangePageCircuitControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IExchangePageCircuitPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractExchangePageCircuitController,
 ExchangePageCircuitBuffer,
 IntegratedController,
 /**@type {!AbstractExchangePageCircuitController}*/(ExchangePageCircuitPortConnector),
 AbstractExchangePageCircuitControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitController}*/({
  setId(val){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({id:val})
  },
  setAmountFrom(val){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({amountFrom:val})
  },
  setCryptoIn(val){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoIn:val})
  },
  setCryptoOut(val){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoOut:val})
  },
  unsetId(){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({id:''})
  },
  unsetAmountFrom(){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({amountFrom:''})
  },
  unsetCryptoIn(){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoIn:''})
  },
  unsetCryptoOut(){
   const{asIExchangePageCircuitController:{setInputs:setInputs}}=this
   setInputs({cryptoOut:''})
  },
 }),
]


export default AbstractExchangePageCircuitController