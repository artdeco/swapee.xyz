
import AbstractExchangePageCircuit from '../AbstractExchangePageCircuit'

/** @abstract {xyz.swapee.wc.IExchangePageCircuitElement} */
export default class AbstractExchangePageCircuitElement { }



AbstractExchangePageCircuitElement[$implementations]=[AbstractExchangePageCircuit,
 /** @type {!AbstractExchangePageCircuitElement} */ ({
  rootId:'ExchangePageCircuit',
  __$id:7735906596,
  fqn:'xyz.swapee.wc.IExchangePageCircuit',
  maurice_element_v3:true,
 }),
]