import ExchangePageCircuitRenderVdus from './methods/render-vdus'
import ExchangePageCircuitElementPort from '../ExchangePageCircuitElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {ExchangePageCircuitInputsPQs} from '../../pqs/ExchangePageCircuitInputsPQs'
import {ExchangePageCircuitQueriesPQs} from '../../pqs/ExchangePageCircuitQueriesPQs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractExchangePageCircuit from '../AbstractExchangePageCircuit'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitElement}
 */
function __AbstractExchangePageCircuitElement() {}
__AbstractExchangePageCircuitElement.prototype = /** @type {!_AbstractExchangePageCircuitElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitElement}
 */
class _AbstractExchangePageCircuitElement { }
/**
 * A component description.
 *
 * The _IExchangePageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitElement} ‎
 */
class AbstractExchangePageCircuitElement extends newAbstract(
 _AbstractExchangePageCircuitElement,773590659613,null,{
  asIExchangePageCircuitElement:1,
  superExchangePageCircuitElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitElement} */
AbstractExchangePageCircuitElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitElement} */
function AbstractExchangePageCircuitElementClass(){}

export default AbstractExchangePageCircuitElement


AbstractExchangePageCircuitElement[$implementations]=[
 __AbstractExchangePageCircuitElement,
 ElementBase,
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':id':idColAttr,
   ':type':typeColAttr,
   ':amount-from':amountFromColAttr,
   ':crypto-in':cryptoInColAttr,
   ':crypto-out':cryptoOutColAttr,
   ':not-id':notIdColAttr,
   ':fixed-rate':fixedRateColAttr,
   ':floating-rate':floatingRateColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'id':idAttr,
    'type':typeAttr,
    'amount-from':amountFromAttr,
    'crypto-in':cryptoInAttr,
    'crypto-out':cryptoOutAttr,
    'not-id':notIdAttr,
    'fixed-rate':fixedRateAttr,
    'floating-rate':floatingRateAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(idAttr===undefined?{'id':idColAttr}:{}),
    ...(typeAttr===undefined?{'type':typeColAttr}:{}),
    ...(amountFromAttr===undefined?{'amount-from':amountFromColAttr}:{}),
    ...(cryptoInAttr===undefined?{'crypto-in':cryptoInColAttr}:{}),
    ...(cryptoOutAttr===undefined?{'crypto-out':cryptoOutColAttr}:{}),
    ...(notIdAttr===undefined?{'not-id':notIdColAttr}:{}),
    ...(fixedRateAttr===undefined?{'fixed-rate':fixedRateColAttr}:{}),
    ...(floatingRateAttr===undefined?{'floating-rate':floatingRateColAttr}:{}),
   }
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'id':idAttr,
   'type':typeAttr,
   'amount-from':amountFromAttr,
   'crypto-in':cryptoInAttr,
   'crypto-out':cryptoOutAttr,
   'not-id':notIdAttr,
   'fixed-rate':fixedRateAttr,
   'floating-rate':floatingRateAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    id:idAttr,
    type:typeAttr,
    amountFrom:amountFromAttr,
    cryptoIn:cryptoInAttr,
    cryptoOut:cryptoOutAttr,
    notId:notIdAttr,
    fixedRate:fixedRateAttr,
    floatingRate:floatingRateAttr,
   }
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 Landed,
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  render:function renderCryptoSelectIn(){
   const{
    asILanded:{
     land:{
      CryptoSelectIn:CryptoSelectIn,
     },
    },
    asIExchangePageCircuitElement:{
     buildCryptoSelectIn:buildCryptoSelectIn,
    },
   }=this
   if(!CryptoSelectIn) return
   const{model:CryptoSelectInModel}=CryptoSelectIn
   const{
    '73071':selectedIcon,
   }=CryptoSelectInModel
   const res=buildCryptoSelectIn({
    selectedIcon:selectedIcon,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  render:function renderCryptoSelectOut(){
   const{
    asILanded:{
     land:{
      CryptoSelectOut:CryptoSelectOut,
     },
    },
    asIExchangePageCircuitElement:{
     buildCryptoSelectOut:buildCryptoSelectOut,
    },
   }=this
   if(!CryptoSelectOut) return
   const{model:CryptoSelectOutModel}=CryptoSelectOut
   const{
    '73071':selectedIcon,
   }=CryptoSelectOutModel
   const res=buildCryptoSelectOut({
    selectedIcon:selectedIcon,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  render:function renderOfferExchange(){
   const{
    asILanded:{
     land:{
      OfferExchange:OfferExchange,
     },
    },
    asIExchangePageCircuitElement:{
     buildOfferExchange:buildOfferExchange,
    },
   }=this
   if(!OfferExchange) return
   const{model:OfferExchangeModel}=OfferExchange
   const{
   }=OfferExchangeModel
   const res=buildOfferExchange({
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  render:function renderTransactionInfo(){
   const{
    asILanded:{
     land:{
      TransactionInfo:TransactionInfo,
     },
    },
    asIExchangePageCircuitElement:{
     buildTransactionInfo:buildTransactionInfo,
    },
   }=this
   if(!TransactionInfo) return
   const{model:TransactionInfoModel}=TransactionInfo
   const{
    '97bea':tid,
   }=TransactionInfoModel
   const res=buildTransactionInfo({
    tid:tid,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.IExchangePageCircuitElement}*/({land:{
    CryptoSelectIn:null,
    CryptoSelectOut:null,
    OfferExchange:null,
    TransactionInfo:null,
   }}))
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  render:ExchangePageCircuitRenderVdus,
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  classes:{
   'Class': '9bd81',
  },
  inputsPQs:ExchangePageCircuitInputsPQs,
  queriesPQs:ExchangePageCircuitQueriesPQs,
  vdus:{
   'CryptoInLa': 'c3c11',
   'CryptoOutLa': 'c3c12',
   'AmountFromLa': 'c3c13',
   'CryptoSelectIn': 'c3c14',
   'CryptoSelectOut': 'c3c15',
   'OffersFilter': 'c3c16',
   'ExchangeBroker': 'c3c17',
   'DealBroker': 'c3c114',
   'ExchangeIntent': 'c3c115',
   'ExchangeIdRow': 'c3c116',
   'TransactionInfo': 'c3c117',
   'TransactionInfoHead': 'c3c123',
   'TransactionInfoHeadMob': 'c3c124',
   'OfferExchange': 'c3c128',
   'ConcealWithTid': 'c3c121',
   'RevealWithTid': 'c3c125',
   'ConcealWithId': 'c3c126',
   'RevealWithId': 'c3c127',
   'RestartButton': 'c3c129',
   'InImWr': 'c3c130',
   'OutImWr': 'c3c131',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','id','type','amountFrom','cryptoIn','cryptoOut','notId','fixedRate','floatingRate','query:conceal-with-tid','query:reveal-with-tid','query:restart-button','query:conceal-with-id','query:reveal-with-id','query:in-im-wr','query:out-im-wr','query:crypto-select-in','query:crypto-select-out','query:offers-filter','query:exchange-broker','query:deal-broker','query:offer-exchange','query:exchange-intent','query:exchange-id-row','query:transaction-info','query:transaction-info-head','query:transaction-info-head-mob','no-solder',':no-solder',':id',':type','amount-from',':amount-from','crypto-in',':crypto-in','crypto-out',':crypto-out','not-id',':not-id','fixed-rate',':fixed-rate','floating-rate',':floating-rate','fe646','3c0bf','61f17','3fb46','83057','3f4c8','48e04','aad68','a8e4e','4e37d','c61f1','5ba9a','700bd','64c41','38bd9','c9e36','09b03','c3b6b','f654f','6b245','047a7','6ee13','b80bb','599dc','748e6','4212d','c60b5','0b951','7d0bd','57342','children']),
   })
  },
  get Port(){
   return ExchangePageCircuitElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:conceal-with-tid':concealWithTidSel,'query:reveal-with-tid':revealWithTidSel,'query:restart-button':restartButtonSel,'query:conceal-with-id':concealWithIdSel,'query:reveal-with-id':revealWithIdSel,'query:in-im-wr':inImWrSel,'query:out-im-wr':outImWrSel,'query:crypto-select-in':cryptoSelectInSel,'query:crypto-select-out':cryptoSelectOutSel,'query:offers-filter':offersFilterSel,'query:exchange-broker':exchangeBrokerSel,'query:deal-broker':dealBrokerSel,'query:offer-exchange':offerExchangeSel,'query:exchange-intent':exchangeIntentSel,'query:exchange-id-row':exchangeIdRowSel,'query:transaction-info':transactionInfoSel,'query:transaction-info-head':transactionInfoHeadSel,'query:transaction-info-head-mob':transactionInfoHeadMobSel}){
   const _ret={}
   if(concealWithTidSel) _ret.concealWithTidSel=concealWithTidSel
   if(revealWithTidSel) _ret.revealWithTidSel=revealWithTidSel
   if(restartButtonSel) _ret.restartButtonSel=restartButtonSel
   if(concealWithIdSel) _ret.concealWithIdSel=concealWithIdSel
   if(revealWithIdSel) _ret.revealWithIdSel=revealWithIdSel
   if(inImWrSel) _ret.inImWrSel=inImWrSel
   if(outImWrSel) _ret.outImWrSel=outImWrSel
   if(cryptoSelectInSel) _ret.cryptoSelectInSel=cryptoSelectInSel
   if(cryptoSelectOutSel) _ret.cryptoSelectOutSel=cryptoSelectOutSel
   if(offersFilterSel) _ret.offersFilterSel=offersFilterSel
   if(exchangeBrokerSel) _ret.exchangeBrokerSel=exchangeBrokerSel
   if(dealBrokerSel) _ret.dealBrokerSel=dealBrokerSel
   if(offerExchangeSel) _ret.offerExchangeSel=offerExchangeSel
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   if(exchangeIdRowSel) _ret.exchangeIdRowSel=exchangeIdRowSel
   if(transactionInfoSel) _ret.transactionInfoSel=transactionInfoSel
   if(transactionInfoHeadSel) _ret.transactionInfoHeadSel=transactionInfoHeadSel
   if(transactionInfoHeadMobSel) _ret.transactionInfoHeadMobSel=transactionInfoHeadMobSel
   return _ret
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  constructor(){
   this.land={
    CryptoSelectIn:null,
    CryptoSelectOut:null,
    OffersFilter:null,
    ExchangeBroker:null,
    DealBroker:null,
    OfferExchange:null,
    ExchangeIntent:null,
    ExchangeIdRow:null,
    TransactionInfo:null,
    TransactionInfoHead:null,
    TransactionInfoHeadMob:null,
   }
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnCryptoSelectIn({cryptoSelectInSel:cryptoSelectInSel}){
   if(!cryptoSelectInSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const CryptoSelectIn=await milleu(cryptoSelectInSel)
   if(!CryptoSelectIn) {
    console.warn('❗️ cryptoSelectInSel %s must be present on the page for %s to work',cryptoSelectInSel,fqn)
    return{}
   }
   land.CryptoSelectIn=CryptoSelectIn
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnCryptoSelectOut({cryptoSelectOutSel:cryptoSelectOutSel}){
   if(!cryptoSelectOutSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const CryptoSelectOut=await milleu(cryptoSelectOutSel)
   if(!CryptoSelectOut) {
    console.warn('❗️ cryptoSelectOutSel %s must be present on the page for %s to work',cryptoSelectOutSel,fqn)
    return{}
   }
   land.CryptoSelectOut=CryptoSelectOut
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnOffersFilter({offersFilterSel:offersFilterSel}){
   if(!offersFilterSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const OffersFilter=await milleu(offersFilterSel)
   if(!OffersFilter) {
    console.warn('❗️ offersFilterSel %s must be present on the page for %s to work',offersFilterSel,fqn)
    return{}
   }
   land.OffersFilter=OffersFilter
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnExchangeBroker({exchangeBrokerSel:exchangeBrokerSel}){
   if(!exchangeBrokerSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeBroker=await milleu(exchangeBrokerSel)
   if(!ExchangeBroker) {
    console.warn('❗️ exchangeBrokerSel %s must be present on the page for %s to work',exchangeBrokerSel,fqn)
    return{}
   }
   land.ExchangeBroker=ExchangeBroker
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnDealBroker({dealBrokerSel:dealBrokerSel}){
   if(!dealBrokerSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const DealBroker=await milleu(dealBrokerSel)
   if(!DealBroker) {
    console.warn('❗️ dealBrokerSel %s must be present on the page for %s to work',dealBrokerSel,fqn)
    return{}
   }
   land.DealBroker=DealBroker
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnOfferExchange({offerExchangeSel:offerExchangeSel}){
   if(!offerExchangeSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const OfferExchange=await milleu(offerExchangeSel)
   if(!OfferExchange) {
    console.warn('❗️ offerExchangeSel %s must be present on the page for %s to work',offerExchangeSel,fqn)
    return{}
   }
   land.OfferExchange=OfferExchange
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnExchangeIdRow({exchangeIdRowSel:exchangeIdRowSel}){
   if(!exchangeIdRowSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIdRow=await milleu(exchangeIdRowSel)
   if(!ExchangeIdRow) {
    console.warn('❗️ exchangeIdRowSel %s must be present on the page for %s to work',exchangeIdRowSel,fqn)
    return{}
   }
   land.ExchangeIdRow=ExchangeIdRow
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnTransactionInfo({transactionInfoSel:transactionInfoSel}){
   if(!transactionInfoSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const TransactionInfo=await milleu(transactionInfoSel)
   if(!TransactionInfo) {
    console.warn('❗️ transactionInfoSel %s must be present on the page for %s to work',transactionInfoSel,fqn)
    return{}
   }
   land.TransactionInfo=TransactionInfo
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnTransactionInfoHead({transactionInfoHeadSel:transactionInfoHeadSel}){
   if(!transactionInfoHeadSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const TransactionInfoHead=await milleu(transactionInfoHeadSel)
   if(!TransactionInfoHead) {
    console.warn('❗️ transactionInfoHeadSel %s must be present on the page for %s to work',transactionInfoHeadSel,fqn)
    return{}
   }
   land.TransactionInfoHead=TransactionInfoHead
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  calibrate:async function awaitOnTransactionInfoHeadMob({transactionInfoHeadMobSel:transactionInfoHeadMobSel}){
   if(!transactionInfoHeadMobSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const TransactionInfoHeadMob=await milleu(transactionInfoHeadMobSel)
   if(!TransactionInfoHeadMob) {
    console.warn('❗️ transactionInfoHeadMobSel %s must be present on the page for %s to work',transactionInfoHeadMobSel,fqn)
    return{}
   }
   land.TransactionInfoHeadMob=TransactionInfoHeadMob
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  solder:(_,{
   concealWithTidSel:concealWithTidSel,
   revealWithTidSel:revealWithTidSel,
   restartButtonSel:restartButtonSel,
   concealWithIdSel:concealWithIdSel,
   revealWithIdSel:revealWithIdSel,
   inImWrSel:inImWrSel,
   outImWrSel:outImWrSel,
   cryptoSelectInSel:cryptoSelectInSel,
   cryptoSelectOutSel:cryptoSelectOutSel,
   offersFilterSel:offersFilterSel,
   exchangeBrokerSel:exchangeBrokerSel,
   dealBrokerSel:dealBrokerSel,
   offerExchangeSel:offerExchangeSel,
   exchangeIntentSel:exchangeIntentSel,
   exchangeIdRowSel:exchangeIdRowSel,
   transactionInfoSel:transactionInfoSel,
   transactionInfoHeadSel:transactionInfoHeadSel,
   transactionInfoHeadMobSel:transactionInfoHeadMobSel,
  })=>{
   return{
    concealWithTidSel:concealWithTidSel,
    revealWithTidSel:revealWithTidSel,
    restartButtonSel:restartButtonSel,
    concealWithIdSel:concealWithIdSel,
    revealWithIdSel:revealWithIdSel,
    inImWrSel:inImWrSel,
    outImWrSel:outImWrSel,
    cryptoSelectInSel:cryptoSelectInSel,
    cryptoSelectOutSel:cryptoSelectOutSel,
    offersFilterSel:offersFilterSel,
    exchangeBrokerSel:exchangeBrokerSel,
    dealBrokerSel:dealBrokerSel,
    offerExchangeSel:offerExchangeSel,
    exchangeIntentSel:exchangeIntentSel,
    exchangeIdRowSel:exchangeIdRowSel,
    transactionInfoSel:transactionInfoSel,
    transactionInfoHeadSel:transactionInfoHeadSel,
    transactionInfoHeadMobSel:transactionInfoHeadMobSel,
   }
  },
 }),
 AbstractExchangePageCircuitElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
  constructor(){
   schedule(this,()=>{
    const{asIGuest:{asIMaurice:{queryVdu:queryVdu}}}=this
    const{revealWithIdSel:revealWithIdSel,concealWithIdSel:concealWithIdSel,concealWithTidSel:concealWithTidSel,revealWithTidSel:revealWithTidSel,restartButtonSel:restartButtonSel,inImWrSel:inImWrSel,outImWrSel:outImWrSel}=this.inputs
    this.RevealWithIds=queryVdu(revealWithIdSel,true)
    this.ConcealWithIds=queryVdu(concealWithIdSel,true)
    this.ConcealWithTids=queryVdu(concealWithTidSel,true)
    this.RevealWithTids=queryVdu(revealWithTidSel,true)
    this.RestartButtons=queryVdu(restartButtonSel,true)
    this.InImWrs=queryVdu(inImWrSel,true)
    this.OutImWrs=queryVdu(outImWrSel,true)
    this.externalVdus={
     'RevealWithId':revealWithIdSel?this.RevealWithId:void 0,
     'ConcealWithId':concealWithIdSel?this.ConcealWithId:void 0,
     'ConcealWithTid':concealWithTidSel?this.ConcealWithTid:void 0,
     'RevealWithTid':revealWithTidSel?this.RevealWithTid:void 0,
     'RestartButton':restartButtonSel?this.RestartButton:void 0,
     'InImWr':inImWrSel?this.InImWr:void 0,
     'OutImWr':outImWrSel?this.OutImWr:void 0,
    }
   })
  },
 }),
]



AbstractExchangePageCircuitElement[$implementations]=[AbstractExchangePageCircuit,
 /** @type {!AbstractExchangePageCircuitElement} */ ({
  rootId:'ExchangePageCircuit',
  __$id:7735906596,
  fqn:'xyz.swapee.wc.IExchangePageCircuit',
  maurice_element_v3:true,
 }),
]