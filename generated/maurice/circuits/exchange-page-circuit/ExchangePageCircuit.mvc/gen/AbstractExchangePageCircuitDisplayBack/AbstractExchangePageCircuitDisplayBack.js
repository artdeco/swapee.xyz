import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitDisplay}
 */
function __AbstractExchangePageCircuitDisplay() {}
__AbstractExchangePageCircuitDisplay.prototype = /** @type {!_AbstractExchangePageCircuitDisplay} */ ({ })
/** @this {xyz.swapee.wc.back.ExchangePageCircuitDisplay} */ function ExchangePageCircuitDisplayConstructor() {
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.ConcealWithTids=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.RevealWithTids=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.RestartButtons=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.ConcealWithIds=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.RevealWithIds=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.InImWrs=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.OutImWrs=[]
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay}
 */
class _AbstractExchangePageCircuitDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay} ‎
 */
class AbstractExchangePageCircuitDisplay extends newAbstract(
 _AbstractExchangePageCircuitDisplay,773590659619,ExchangePageCircuitDisplayConstructor,{
  asIExchangePageCircuitDisplay:1,
  superExchangePageCircuitDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay} */
AbstractExchangePageCircuitDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitDisplay} */
function AbstractExchangePageCircuitDisplayClass(){}

export default AbstractExchangePageCircuitDisplay


AbstractExchangePageCircuitDisplay[$implementations]=[
 __AbstractExchangePageCircuitDisplay,
 GraphicsDriverBack,
 AbstractExchangePageCircuitDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangePageCircuitDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IExchangePageCircuitDisplay}*/({
    CryptoInLa:twinMock,
    CryptoOutLa:twinMock,
    AmountFromLa:twinMock,
    CryptoSelectIn:twinMock,
    CryptoSelectOut:twinMock,
    OffersFilter:twinMock,
    ExchangeBroker:twinMock,
    DealBroker:twinMock,
    OfferExchange:twinMock,
    ExchangeIntent:twinMock,
    ExchangeIdRow:twinMock,
    TransactionInfo:twinMock,
    TransactionInfoHead:twinMock,
    TransactionInfoHeadMob:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.ExchangePageCircuitDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IExchangePageCircuitDisplay.Initialese}*/({
   ConcealWithTids:1,
   RevealWithTids:1,
   RestartButtons:1,
   CryptoInLa:1,
   ConcealWithIds:1,
   RevealWithIds:1,
   CryptoOutLa:1,
   AmountFromLa:1,
   InImWrs:1,
   OutImWrs:1,
   CryptoSelectIn:1,
   CryptoSelectOut:1,
   OffersFilter:1,
   ExchangeBroker:1,
   DealBroker:1,
   OfferExchange:1,
   ExchangeIntent:1,
   ExchangeIdRow:1,
   TransactionInfo:1,
   TransactionInfoHead:1,
   TransactionInfoHeadMob:1,
  }),
  initializer({
   ConcealWithTids:_ConcealWithTids,
   RevealWithTids:_RevealWithTids,
   RestartButtons:_RestartButtons,
   CryptoInLa:_CryptoInLa,
   ConcealWithIds:_ConcealWithIds,
   RevealWithIds:_RevealWithIds,
   CryptoOutLa:_CryptoOutLa,
   AmountFromLa:_AmountFromLa,
   InImWrs:_InImWrs,
   OutImWrs:_OutImWrs,
   CryptoSelectIn:_CryptoSelectIn,
   CryptoSelectOut:_CryptoSelectOut,
   OffersFilter:_OffersFilter,
   ExchangeBroker:_ExchangeBroker,
   DealBroker:_DealBroker,
   OfferExchange:_OfferExchange,
   ExchangeIntent:_ExchangeIntent,
   ExchangeIdRow:_ExchangeIdRow,
   TransactionInfo:_TransactionInfo,
   TransactionInfoHead:_TransactionInfoHead,
   TransactionInfoHeadMob:_TransactionInfoHeadMob,
  }) {
   if(_ConcealWithTids!==undefined) this.ConcealWithTids=_ConcealWithTids
   if(_RevealWithTids!==undefined) this.RevealWithTids=_RevealWithTids
   if(_RestartButtons!==undefined) this.RestartButtons=_RestartButtons
   if(_CryptoInLa!==undefined) this.CryptoInLa=_CryptoInLa
   if(_ConcealWithIds!==undefined) this.ConcealWithIds=_ConcealWithIds
   if(_RevealWithIds!==undefined) this.RevealWithIds=_RevealWithIds
   if(_CryptoOutLa!==undefined) this.CryptoOutLa=_CryptoOutLa
   if(_AmountFromLa!==undefined) this.AmountFromLa=_AmountFromLa
   if(_InImWrs!==undefined) this.InImWrs=_InImWrs
   if(_OutImWrs!==undefined) this.OutImWrs=_OutImWrs
   if(_CryptoSelectIn!==undefined) this.CryptoSelectIn=_CryptoSelectIn
   if(_CryptoSelectOut!==undefined) this.CryptoSelectOut=_CryptoSelectOut
   if(_OffersFilter!==undefined) this.OffersFilter=_OffersFilter
   if(_ExchangeBroker!==undefined) this.ExchangeBroker=_ExchangeBroker
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_OfferExchange!==undefined) this.OfferExchange=_OfferExchange
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_ExchangeIdRow!==undefined) this.ExchangeIdRow=_ExchangeIdRow
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
   if(_TransactionInfoHead!==undefined) this.TransactionInfoHead=_TransactionInfoHead
   if(_TransactionInfoHeadMob!==undefined) this.TransactionInfoHeadMob=_TransactionInfoHeadMob
  },
 }),
]