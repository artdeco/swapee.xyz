import AbstractExchangePageCircuitControllerAR from '../AbstractExchangePageCircuitControllerAR'
import {AbstractExchangePageCircuitController} from '../AbstractExchangePageCircuitController'
import {DriverBack,NavigatorBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitControllerBack}
 */
function __AbstractExchangePageCircuitControllerBack() {}
__AbstractExchangePageCircuitControllerBack.prototype = /** @type {!_AbstractExchangePageCircuitControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitController}
 */
class _AbstractExchangePageCircuitControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangePageCircuitController} ‎
 */
class AbstractExchangePageCircuitControllerBack extends newAbstract(
 _AbstractExchangePageCircuitControllerBack,773590659622,null,{
  asIExchangePageCircuitController:1,
  superExchangePageCircuitController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitController} */
AbstractExchangePageCircuitControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangePageCircuitController} */
function AbstractExchangePageCircuitControllerBackClass(){}

export default AbstractExchangePageCircuitControllerBack


AbstractExchangePageCircuitControllerBack[$implementations]=[
 __AbstractExchangePageCircuitControllerBack,
 AbstractExchangePageCircuitController,
 AbstractExchangePageCircuitControllerAR,
 DriverBack,
 NavigatorBack,
]