import {makeBuffers} from '@webcircuits/webcircuits'

export const ExchangePageCircuitBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  id:String,
  type:String,
  amountFrom:String,
  cryptoIn:String,
  cryptoOut:String,
  notId:Boolean,
  fixedRate:Boolean,
  floatingRate:Boolean,
 }),
})

export default ExchangePageCircuitBuffer