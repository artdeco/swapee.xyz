import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangePageCircuitElementPort}
 */
function __ExchangePageCircuitElementPort() {}
__ExchangePageCircuitElementPort.prototype = /** @type {!_ExchangePageCircuitElementPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangePageCircuitElementPort} */ function ExchangePageCircuitElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IExchangePageCircuitElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    concealWithTidOpts: {},
    revealWithTidOpts: {},
    restartButtonOpts: {},
    cryptoInLaOpts: {},
    concealWithIdOpts: {},
    revealWithIdOpts: {},
    cryptoOutLaOpts: {},
    amountFromLaOpts: {},
    inImWrOpts: {},
    outImWrOpts: {},
    cryptoSelectInOpts: {},
    cryptoSelectOutOpts: {},
    offersFilterOpts: {},
    exchangeBrokerOpts: {},
    dealBrokerOpts: {},
    offerExchangeOpts: {},
    exchangeIntentOpts: {},
    exchangeIdRowOpts: {},
    transactionInfoOpts: {},
    transactionInfoHeadOpts: {},
    transactionInfoHeadMobOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitElementPort}
 */
class _ExchangePageCircuitElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitElementPort} ‎
 */
class ExchangePageCircuitElementPort extends newAbstract(
 _ExchangePageCircuitElementPort,773590659614,ExchangePageCircuitElementPortConstructor,{
  asIExchangePageCircuitElementPort:1,
  superExchangePageCircuitElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitElementPort} */
ExchangePageCircuitElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitElementPort} */
function ExchangePageCircuitElementPortClass(){}

export default ExchangePageCircuitElementPort


ExchangePageCircuitElementPort[$implementations]=[
 __ExchangePageCircuitElementPort,
 ExchangePageCircuitElementPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'conceal-with-tid-opts':undefined,
    'reveal-with-tid-opts':undefined,
    'restart-button-opts':undefined,
    'crypto-in-la-opts':undefined,
    'conceal-with-id-opts':undefined,
    'reveal-with-id-opts':undefined,
    'crypto-out-la-opts':undefined,
    'amount-from-la-opts':undefined,
    'in-im-wr-opts':undefined,
    'out-im-wr-opts':undefined,
    'crypto-select-in-opts':undefined,
    'crypto-select-out-opts':undefined,
    'offers-filter-opts':undefined,
    'exchange-broker-opts':undefined,
    'deal-broker-opts':undefined,
    'offer-exchange-opts':undefined,
    'exchange-intent-opts':undefined,
    'exchange-id-row-opts':undefined,
    'transaction-info-opts':undefined,
    'transaction-info-head-opts':undefined,
    'transaction-info-head-mob-opts':undefined,
    'amount-from':undefined,
    'crypto-in':undefined,
    'crypto-out':undefined,
    'not-id':undefined,
    'fixed-rate':undefined,
    'floating-rate':undefined,
   })
  },
 }),
]