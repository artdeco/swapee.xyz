
/**@this {xyz.swapee.wc.IExchangePageCircuitComputer}*/
export function preadaptId(inputs,changes,mapForm) {
 const{ExchangeBroker:ExchangeBroker}=this.land
 if(!ExchangeBroker) return
 /**@type {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptId.Form}*/
 const _inputs={
  id:ExchangeBroker.model['b80bb'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.id) return
 changes.id=changes['b80bb']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptId(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangePageCircuitComputer}*/
export function preadaptNotId(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptNotId.Form}*/
 const _inputs={
  id:inputs.id,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptNotId(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangePageCircuitComputer}*/
export function preadaptType(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptType.Form}*/
 const _inputs={
  type:inputs.type,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptType(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangePageCircuitComputer}*/
export function preadaptIntent(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangePageCircuitComputer.adaptIntent.Form}*/
 const _inputs={
  currencyFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['96c88']:void 0,
  currencyTo:this.land.ExchangeIntent?this.land.ExchangeIntent.model['c23cd']:void 0,
  amountFrom:this.land.ExchangeIntent?this.land.ExchangeIntent.model['748e6']:void 0,
  fixed:this.land.ExchangeIntent?this.land.ExchangeIntent.model['cec31']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.currencyFrom)) return
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.amountFrom=changes['748e6']
 changes.fixed=changes['cec31']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptIntent(__inputs,__changes)
 return RET
}