import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitComputer}
 */
function __AbstractExchangePageCircuitComputer() {}
__AbstractExchangePageCircuitComputer.prototype = /** @type {!_AbstractExchangePageCircuitComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitComputer}
 */
class _AbstractExchangePageCircuitComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitComputer} ‎
 */
export class AbstractExchangePageCircuitComputer extends newAbstract(
 _AbstractExchangePageCircuitComputer,77359065961,null,{
  asIExchangePageCircuitComputer:1,
  superExchangePageCircuitComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitComputer} */
AbstractExchangePageCircuitComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitComputer} */
function AbstractExchangePageCircuitComputerClass(){}


AbstractExchangePageCircuitComputer[$implementations]=[
 __AbstractExchangePageCircuitComputer,
 Adapter,
]


export default AbstractExchangePageCircuitComputer