import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitProcessor}
 */
function __AbstractExchangePageCircuitProcessor() {}
__AbstractExchangePageCircuitProcessor.prototype = /** @type {!_AbstractExchangePageCircuitProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitProcessor}
 */
class _AbstractExchangePageCircuitProcessor { }
/**
 * The processor to compute changes to the memory for the _IExchangePageCircuit_.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitProcessor} ‎
 */
class AbstractExchangePageCircuitProcessor extends newAbstract(
 _AbstractExchangePageCircuitProcessor,77359065968,null,{
  asIExchangePageCircuitProcessor:1,
  superExchangePageCircuitProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitProcessor} */
AbstractExchangePageCircuitProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitProcessor} */
function AbstractExchangePageCircuitProcessorClass(){}

export default AbstractExchangePageCircuitProcessor


AbstractExchangePageCircuitProcessor[$implementations]=[
 __AbstractExchangePageCircuitProcessor,
]