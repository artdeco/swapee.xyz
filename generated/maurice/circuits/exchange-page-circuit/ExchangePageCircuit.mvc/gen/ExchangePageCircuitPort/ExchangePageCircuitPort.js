import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {ExchangePageCircuitInputsPQs} from '../../pqs/ExchangePageCircuitInputsPQs'
import {ExchangePageCircuitOuterCoreConstructor} from '../ExchangePageCircuitCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangePageCircuitPort}
 */
function __ExchangePageCircuitPort() {}
__ExchangePageCircuitPort.prototype = /** @type {!_ExchangePageCircuitPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangePageCircuitPort} */ function ExchangePageCircuitPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ExchangePageCircuitOuterCore} */ ({model:null})
  ExchangePageCircuitOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitPort}
 */
class _ExchangePageCircuitPort { }
/**
 * The port that serves as an interface to the _IExchangePageCircuit_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitPort} ‎
 */
export class ExchangePageCircuitPort extends newAbstract(
 _ExchangePageCircuitPort,77359065965,ExchangePageCircuitPortConstructor,{
  asIExchangePageCircuitPort:1,
  superExchangePageCircuitPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitPort} */
ExchangePageCircuitPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitPort} */
function ExchangePageCircuitPortClass(){}

export const ExchangePageCircuitPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IExchangePageCircuit.Pinout>}*/({
 get Port() { return ExchangePageCircuitPort },
})

ExchangePageCircuitPort[$implementations]=[
 ExchangePageCircuitPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitPort}*/({
  resetPort(){
   this.resetExchangePageCircuitPort()
  },
  resetExchangePageCircuitPort(){
   ExchangePageCircuitPortConstructor.call(this)
  },
 }),
 __ExchangePageCircuitPort,
 Parametric,
 ExchangePageCircuitPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitPort}*/({
  constructor(){
   mountPins(this.inputs,ExchangePageCircuitInputsPQs)
  },
 }),
]


export default ExchangePageCircuitPort