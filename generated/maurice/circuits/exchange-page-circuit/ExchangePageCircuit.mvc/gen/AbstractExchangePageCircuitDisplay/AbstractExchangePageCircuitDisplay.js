import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangePageCircuitDisplay}
 */
function __AbstractExchangePageCircuitDisplay() {}
__AbstractExchangePageCircuitDisplay.prototype = /** @type {!_AbstractExchangePageCircuitDisplay} */ ({ })
/** @this {xyz.swapee.wc.ExchangePageCircuitDisplay} */ function ExchangePageCircuitDisplayConstructor() {
  /** @type {!Array<!HTMLSpanElement>} */ this.ConcealWithTids=[]
  /** @type {!Array<!HTMLSpanElement>} */ this.RevealWithTids=[]
  /** @type {!Array<!HTMLSpanElement>} */ this.RestartButtons=[]
  /** @type {HTMLSpanElement} */ this.CryptoInLa=null
  /** @type {!Array<!HTMLSpanElement>} */ this.ConcealWithIds=[]
  /** @type {!Array<!HTMLSpanElement>} */ this.RevealWithIds=[]
  /** @type {HTMLSpanElement} */ this.CryptoOutLa=null
  /** @type {HTMLSpanElement} */ this.AmountFromLa=null
  /** @type {!Array<!HTMLElement>} */ this.InImWrs=[]
  /** @type {!Array<!HTMLElement>} */ this.OutImWrs=[]
  /** @type {HTMLElement} */ this.CryptoSelectIn=null
  /** @type {HTMLElement} */ this.CryptoSelectOut=null
  /** @type {HTMLElement} */ this.OffersFilter=null
  /** @type {HTMLElement} */ this.ExchangeBroker=null
  /** @type {HTMLElement} */ this.DealBroker=null
  /** @type {HTMLElement} */ this.OfferExchange=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
  /** @type {HTMLElement} */ this.ExchangeIdRow=null
  /** @type {HTMLElement} */ this.TransactionInfo=null
  /** @type {HTMLElement} */ this.TransactionInfoHead=null
  /** @type {HTMLElement} */ this.TransactionInfoHeadMob=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitDisplay}
 */
class _AbstractExchangePageCircuitDisplay { }
/**
 * Display for presenting information from the _IExchangePageCircuit_.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitDisplay} ‎
 */
class AbstractExchangePageCircuitDisplay extends newAbstract(
 _AbstractExchangePageCircuitDisplay,773590659617,ExchangePageCircuitDisplayConstructor,{
  asIExchangePageCircuitDisplay:1,
  superExchangePageCircuitDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitDisplay} */
AbstractExchangePageCircuitDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitDisplay} */
function AbstractExchangePageCircuitDisplayClass(){}

export default AbstractExchangePageCircuitDisplay


AbstractExchangePageCircuitDisplay[$implementations]=[
 __AbstractExchangePageCircuitDisplay,
 Display,
 AbstractExchangePageCircuitDisplayClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{revealWithIdScopeSel:revealWithIdScopeSel,concealWithIdScopeSel:concealWithIdScopeSel,concealWithTidScopeSel:concealWithTidScopeSel,revealWithTidScopeSel:revealWithTidScopeSel,restartButtonScopeSel:restartButtonScopeSel,inImWrScopeSel:inImWrScopeSel,outImWrScopeSel:outImWrScopeSel,cryptoSelectInScopeSel:cryptoSelectInScopeSel,cryptoSelectOutScopeSel:cryptoSelectOutScopeSel,offersFilterScopeSel:offersFilterScopeSel,exchangeBrokerScopeSel:exchangeBrokerScopeSel,dealBrokerScopeSel:dealBrokerScopeSel,offerExchangeScopeSel:offerExchangeScopeSel,exchangeIntentScopeSel:exchangeIntentScopeSel,exchangeIdRowScopeSel:exchangeIdRowScopeSel,transactionInfoScopeSel:transactionInfoScopeSel,transactionInfoHeadScopeSel:transactionInfoHeadScopeSel,transactionInfoHeadMobScopeSel:transactionInfoHeadMobScopeSel}}=this
    this.scan({
     revealWithIdSel:revealWithIdScopeSel,
     concealWithIdSel:concealWithIdScopeSel,
     concealWithTidSel:concealWithTidScopeSel,
     revealWithTidSel:revealWithTidScopeSel,
     restartButtonSel:restartButtonScopeSel,
     inImWrSel:inImWrScopeSel,
     outImWrSel:outImWrScopeSel,
     cryptoSelectInSel:cryptoSelectInScopeSel,
     cryptoSelectOutSel:cryptoSelectOutScopeSel,
     offersFilterSel:offersFilterScopeSel,
     exchangeBrokerSel:exchangeBrokerScopeSel,
     dealBrokerSel:dealBrokerScopeSel,
     offerExchangeSel:offerExchangeScopeSel,
     exchangeIntentSel:exchangeIntentScopeSel,
     exchangeIdRowSel:exchangeIdRowScopeSel,
     transactionInfoSel:transactionInfoScopeSel,
     transactionInfoHeadSel:transactionInfoHeadScopeSel,
     transactionInfoHeadMobSel:transactionInfoHeadMobScopeSel,
    })
   })
  },
  scan:function vduScan({revealWithIdSel:revealWithIdSelScope,concealWithIdSel:concealWithIdSelScope,concealWithTidSel:concealWithTidSelScope,revealWithTidSel:revealWithTidSelScope,restartButtonSel:restartButtonSelScope,inImWrSel:inImWrSelScope,outImWrSel:outImWrSelScope,cryptoSelectInSel:cryptoSelectInSelScope,cryptoSelectOutSel:cryptoSelectOutSelScope,offersFilterSel:offersFilterSelScope,exchangeBrokerSel:exchangeBrokerSelScope,dealBrokerSel:dealBrokerSelScope,offerExchangeSel:offerExchangeSelScope,exchangeIntentSel:exchangeIntentSelScope,exchangeIdRowSel:exchangeIdRowSelScope,transactionInfoSel:transactionInfoSelScope,transactionInfoHeadSel:transactionInfoHeadSelScope,transactionInfoHeadMobSel:transactionInfoHeadMobSelScope}){
   const{element:element,asIExchangePageCircuitScreen:{vdusPQs:{
    CryptoInLa:CryptoInLa,
    CryptoOutLa:CryptoOutLa,
    AmountFromLa:AmountFromLa,
   }},queries:{revealWithIdSel,concealWithIdSel,concealWithTidSel,revealWithTidSel,restartButtonSel,inImWrSel,outImWrSel,cryptoSelectInSel,cryptoSelectOutSel,offersFilterSel,exchangeBrokerSel,dealBrokerSel,offerExchangeSel,exchangeIntentSel,exchangeIdRowSel,transactionInfoSel,transactionInfoHeadSel,transactionInfoHeadMobSel}}=this
   const children=getDataIds(element)
   /**@type {!Array<!HTMLSpanElement>}*/
   let RevealWithIds=[]
   if(revealWithIdSel) {
    let root
    if(revealWithIdSelScope){
     root=element.closest(revealWithIdSelScope)
    }else root=document
    // what if what to use one scoped and the rest unscoped global handles
    const all=root?root.querySelectorAll(revealWithIdSel):[]
    RevealWithIds.push(...all)
   }
   /**@type {!Array<!HTMLSpanElement>}*/
   let ConcealWithIds=[]
   if(concealWithIdSel) {
    let root
    if(concealWithIdSelScope){
     root=element.closest(concealWithIdSelScope)
    }else root=document
    // what if what to use one scoped and the rest unscoped global handles
    const all=root?root.querySelectorAll(concealWithIdSel):[]
    ConcealWithIds.push(...all)
   }
   /**@type {!Array<!HTMLSpanElement>}*/
   let ConcealWithTids=[]
   if(concealWithTidSel) {
    let root
    if(concealWithTidSelScope){
     root=element.closest(concealWithTidSelScope)
    }else root=document
    // what if what to use one scoped and the rest unscoped global handles
    const all=root?root.querySelectorAll(concealWithTidSel):[]
    ConcealWithTids.push(...all)
   }
   /**@type {!Array<!HTMLSpanElement>}*/
   let RevealWithTids=[]
   if(revealWithTidSel) {
    let root
    if(revealWithTidSelScope){
     root=element.closest(revealWithTidSelScope)
    }else root=document
    // what if what to use one scoped and the rest unscoped global handles
    const all=root?root.querySelectorAll(revealWithTidSel):[]
    RevealWithTids.push(...all)
   }
   /**@type {!Array<!HTMLSpanElement>}*/
   let RestartButtons=[]
   if(restartButtonSel) {
    let root
    if(restartButtonSelScope){
     root=element.closest(restartButtonSelScope)
    }else root=document
    // what if what to use one scoped and the rest unscoped global handles
    const all=root?root.querySelectorAll(restartButtonSel):[]
    RestartButtons.push(...all)
   }
   /**@type {!Array<!HTMLElement>}*/
   let InImWrs=[]
   if(inImWrSel) {
    let root
    if(inImWrSelScope){
     root=element.closest(inImWrSelScope)
    }else root=document
    // what if what to use one scoped and the rest unscoped global handles
    const all=root?root.querySelectorAll(inImWrSel):[]
    InImWrs.push(...all)
   }
   /**@type {!Array<!HTMLElement>}*/
   let OutImWrs=[]
   if(outImWrSel) {
    let root
    if(outImWrSelScope){
     root=element.closest(outImWrSelScope)
    }else root=document
    // what if what to use one scoped and the rest unscoped global handles
    const all=root?root.querySelectorAll(outImWrSel):[]
    OutImWrs.push(...all)
   }
   /**@type {HTMLElement}*/
   const CryptoSelectIn=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _CryptoSelectIn=cryptoSelectInSel?root.querySelector(cryptoSelectInSel):void 0
    return _CryptoSelectIn
   })(cryptoSelectInSelScope)
   /**@type {HTMLElement}*/
   const CryptoSelectOut=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _CryptoSelectOut=cryptoSelectOutSel?root.querySelector(cryptoSelectOutSel):void 0
    return _CryptoSelectOut
   })(cryptoSelectOutSelScope)
   /**@type {HTMLElement}*/
   const OffersFilter=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _OffersFilter=offersFilterSel?root.querySelector(offersFilterSel):void 0
    return _OffersFilter
   })(offersFilterSelScope)
   /**@type {HTMLElement}*/
   const ExchangeBroker=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeBroker=exchangeBrokerSel?root.querySelector(exchangeBrokerSel):void 0
    return _ExchangeBroker
   })(exchangeBrokerSelScope)
   /**@type {HTMLElement}*/
   const DealBroker=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _DealBroker=dealBrokerSel?root.querySelector(dealBrokerSel):void 0
    return _DealBroker
   })(dealBrokerSelScope)
   /**@type {HTMLElement}*/
   const OfferExchange=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _OfferExchange=offerExchangeSel?root.querySelector(offerExchangeSel):void 0
    return _OfferExchange
   })(offerExchangeSelScope)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   /**@type {HTMLElement}*/
   const ExchangeIdRow=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIdRow=exchangeIdRowSel?root.querySelector(exchangeIdRowSel):void 0
    return _ExchangeIdRow
   })(exchangeIdRowSelScope)
   /**@type {HTMLElement}*/
   const TransactionInfo=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _TransactionInfo=transactionInfoSel?root.querySelector(transactionInfoSel):void 0
    return _TransactionInfo
   })(transactionInfoSelScope)
   /**@type {HTMLElement}*/
   const TransactionInfoHead=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _TransactionInfoHead=transactionInfoHeadSel?root.querySelector(transactionInfoHeadSel):void 0
    return _TransactionInfoHead
   })(transactionInfoHeadSelScope)
   /**@type {HTMLElement}*/
   const TransactionInfoHeadMob=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _TransactionInfoHeadMob=transactionInfoHeadMobSel?root.querySelector(transactionInfoHeadMobSel):void 0
    return _TransactionInfoHeadMob
   })(transactionInfoHeadMobSelScope)
   Object.assign(this,{
    CryptoInLa:/**@type {HTMLSpanElement}*/(children[CryptoInLa]),
    CryptoOutLa:/**@type {HTMLSpanElement}*/(children[CryptoOutLa]),
    AmountFromLa:/**@type {HTMLSpanElement}*/(children[AmountFromLa]),
    RevealWithIds:RevealWithIds,
    ConcealWithIds:ConcealWithIds,
    ConcealWithTids:ConcealWithTids,
    RevealWithTids:RevealWithTids,
    RestartButtons:RestartButtons,
    InImWrs:InImWrs,
    OutImWrs:OutImWrs,
    CryptoSelectIn:CryptoSelectIn,
    CryptoSelectOut:CryptoSelectOut,
    OffersFilter:OffersFilter,
    ExchangeBroker:ExchangeBroker,
    DealBroker:DealBroker,
    OfferExchange:OfferExchange,
    ExchangeIntent:ExchangeIntent,
    ExchangeIdRow:ExchangeIdRow,
    TransactionInfo:TransactionInfo,
    TransactionInfoHead:TransactionInfoHead,
    TransactionInfoHeadMob:TransactionInfoHeadMob,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.ExchangePageCircuitDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IExchangePageCircuitDisplay.Initialese}*/({
   ConcealWithTids:1,
   RevealWithTids:1,
   RestartButtons:1,
   CryptoInLa:1,
   ConcealWithIds:1,
   RevealWithIds:1,
   CryptoOutLa:1,
   AmountFromLa:1,
   InImWrs:1,
   OutImWrs:1,
   CryptoSelectIn:1,
   CryptoSelectOut:1,
   OffersFilter:1,
   ExchangeBroker:1,
   DealBroker:1,
   OfferExchange:1,
   ExchangeIntent:1,
   ExchangeIdRow:1,
   TransactionInfo:1,
   TransactionInfoHead:1,
   TransactionInfoHeadMob:1,
  }),
  initializer({
   ConcealWithTids:_ConcealWithTids,
   RevealWithTids:_RevealWithTids,
   RestartButtons:_RestartButtons,
   CryptoInLa:_CryptoInLa,
   ConcealWithIds:_ConcealWithIds,
   RevealWithIds:_RevealWithIds,
   CryptoOutLa:_CryptoOutLa,
   AmountFromLa:_AmountFromLa,
   InImWrs:_InImWrs,
   OutImWrs:_OutImWrs,
   CryptoSelectIn:_CryptoSelectIn,
   CryptoSelectOut:_CryptoSelectOut,
   OffersFilter:_OffersFilter,
   ExchangeBroker:_ExchangeBroker,
   DealBroker:_DealBroker,
   OfferExchange:_OfferExchange,
   ExchangeIntent:_ExchangeIntent,
   ExchangeIdRow:_ExchangeIdRow,
   TransactionInfo:_TransactionInfo,
   TransactionInfoHead:_TransactionInfoHead,
   TransactionInfoHeadMob:_TransactionInfoHeadMob,
  }) {
   if(_ConcealWithTids!==undefined) this.ConcealWithTids=_ConcealWithTids
   if(_RevealWithTids!==undefined) this.RevealWithTids=_RevealWithTids
   if(_RestartButtons!==undefined) this.RestartButtons=_RestartButtons
   if(_CryptoInLa!==undefined) this.CryptoInLa=_CryptoInLa
   if(_ConcealWithIds!==undefined) this.ConcealWithIds=_ConcealWithIds
   if(_RevealWithIds!==undefined) this.RevealWithIds=_RevealWithIds
   if(_CryptoOutLa!==undefined) this.CryptoOutLa=_CryptoOutLa
   if(_AmountFromLa!==undefined) this.AmountFromLa=_AmountFromLa
   if(_InImWrs!==undefined) this.InImWrs=_InImWrs
   if(_OutImWrs!==undefined) this.OutImWrs=_OutImWrs
   if(_CryptoSelectIn!==undefined) this.CryptoSelectIn=_CryptoSelectIn
   if(_CryptoSelectOut!==undefined) this.CryptoSelectOut=_CryptoSelectOut
   if(_OffersFilter!==undefined) this.OffersFilter=_OffersFilter
   if(_ExchangeBroker!==undefined) this.ExchangeBroker=_ExchangeBroker
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_OfferExchange!==undefined) this.OfferExchange=_OfferExchange
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_ExchangeIdRow!==undefined) this.ExchangeIdRow=_ExchangeIdRow
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
   if(_TransactionInfoHead!==undefined) this.TransactionInfoHead=_TransactionInfoHead
   if(_TransactionInfoHeadMob!==undefined) this.TransactionInfoHeadMob=_TransactionInfoHeadMob
  },
 }),
]