import {mountPins} from '@type.engineering/seers'
import {ExchangePageCircuitMemoryPQs} from '../../pqs/ExchangePageCircuitMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangePageCircuitCore}
 */
function __ExchangePageCircuitCore() {}
__ExchangePageCircuitCore.prototype = /** @type {!_ExchangePageCircuitCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitCore}
 */
class _ExchangePageCircuitCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitCore} ‎
 */
class ExchangePageCircuitCore extends newAbstract(
 _ExchangePageCircuitCore,77359065967,null,{
  asIExchangePageCircuitCore:1,
  superExchangePageCircuitCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitCore} */
ExchangePageCircuitCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitCore} */
function ExchangePageCircuitCoreClass(){}

export default ExchangePageCircuitCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangePageCircuitOuterCore}
 */
function __ExchangePageCircuitOuterCore() {}
__ExchangePageCircuitOuterCore.prototype = /** @type {!_ExchangePageCircuitOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ExchangePageCircuitOuterCore} */
export function ExchangePageCircuitOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IExchangePageCircuitOuterCore.Model}*/
  this.model={
    id: '',
    type: '',
    amountFrom: '0.1',
    cryptoIn: 'BTC',
    cryptoOut: 'ETH',
    notId: false,
    fixedRate: false,
    floatingRate: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitOuterCore}
 */
class _ExchangePageCircuitOuterCore { }
/**
 * The _IExchangePageCircuit_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitOuterCore} ‎
 */
export class ExchangePageCircuitOuterCore extends newAbstract(
 _ExchangePageCircuitOuterCore,77359065963,ExchangePageCircuitOuterCoreConstructor,{
  asIExchangePageCircuitOuterCore:1,
  superExchangePageCircuitOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitOuterCore} */
ExchangePageCircuitOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitOuterCore} */
function ExchangePageCircuitOuterCoreClass(){}


ExchangePageCircuitOuterCore[$implementations]=[
 __ExchangePageCircuitOuterCore,
 ExchangePageCircuitOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitOuterCore}*/({
  constructor(){
   mountPins(this.model,ExchangePageCircuitMemoryPQs)

  },
 }),
]

ExchangePageCircuitCore[$implementations]=[
 ExchangePageCircuitCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangePageCircuitCore}*/({
  resetCore(){
   this.resetExchangePageCircuitCore()
  },
  resetExchangePageCircuitCore(){
   ExchangePageCircuitOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ExchangePageCircuitOuterCore}*/(
     /**@type {!xyz.swapee.wc.IExchangePageCircuitOuterCore}*/(this)),
   )
  },
 }),
 __ExchangePageCircuitCore,
 ExchangePageCircuitOuterCore,
]

export {ExchangePageCircuitCore}