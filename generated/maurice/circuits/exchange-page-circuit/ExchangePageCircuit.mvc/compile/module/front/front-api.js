import { ExchangePageCircuitDisplay, ExchangePageCircuitScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.ExchangePageCircuitDisplay} */
export { ExchangePageCircuitDisplay }
/** @lazy @api {xyz.swapee.wc.ExchangePageCircuitScreen} */
export { ExchangePageCircuitScreen }