import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractExchangePageCircuit}*/
export class AbstractExchangePageCircuit extends Module['77359065961'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuit} */
AbstractExchangePageCircuit.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangePageCircuitPort} */
export const ExchangePageCircuitPort=Module['77359065963']
/**@extends {xyz.swapee.wc.AbstractExchangePageCircuitController}*/
export class AbstractExchangePageCircuitController extends Module['77359065964'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitController} */
AbstractExchangePageCircuitController.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangePageCircuitHtmlComponent} */
export const ExchangePageCircuitHtmlComponent=Module['773590659610']
/** @type {typeof xyz.swapee.wc.ExchangePageCircuitBuffer} */
export const ExchangePageCircuitBuffer=Module['773590659611']
/**@extends {xyz.swapee.wc.AbstractExchangePageCircuitComputer}*/
export class AbstractExchangePageCircuitComputer extends Module['773590659630'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangePageCircuitComputer} */
AbstractExchangePageCircuitComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangePageCircuitComputer} */
export const ExchangePageCircuitComputer=Module['773590659631']
/** @type {typeof xyz.swapee.wc.back.ExchangePageCircuitController} */
export const ExchangePageCircuitController=Module['773590659661']