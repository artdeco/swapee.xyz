/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuit` interface.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuit}
 */
class AbstractExchangePageCircuit extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangePageCircuit_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangePageCircuitPort}
 */
class ExchangePageCircuitPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitController}
 */
class AbstractExchangePageCircuitController extends (class {/* lazy-loaded */}) {}
/**
 * The _IExchangePageCircuit_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ExchangePageCircuitHtmlComponent}
 */
class ExchangePageCircuitHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangePageCircuitBuffer}
 */
class ExchangePageCircuitBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitComputer}
 */
class AbstractExchangePageCircuitComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ExchangePageCircuitComputer}
 */
class ExchangePageCircuitComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ExchangePageCircuitController}
 */
class ExchangePageCircuitController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangePageCircuit = AbstractExchangePageCircuit
module.exports.ExchangePageCircuitPort = ExchangePageCircuitPort
module.exports.AbstractExchangePageCircuitController = AbstractExchangePageCircuitController
module.exports.ExchangePageCircuitHtmlComponent = ExchangePageCircuitHtmlComponent
module.exports.ExchangePageCircuitBuffer = ExchangePageCircuitBuffer
module.exports.AbstractExchangePageCircuitComputer = AbstractExchangePageCircuitComputer
module.exports.ExchangePageCircuitComputer = ExchangePageCircuitComputer
module.exports.ExchangePageCircuitController = ExchangePageCircuitController