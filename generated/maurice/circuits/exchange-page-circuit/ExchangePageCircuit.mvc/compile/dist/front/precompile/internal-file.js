/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const B=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const fa=B["37270038985"],ha=B["372700389810"],C=B["372700389811"];function D(c,d,f,g){return B["372700389812"](c,d,f,g,!1,void 0)};
const E=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const ia=E["61893096584"],ja=E["61893096586"],ka=E["618930965811"],la=E["618930965812"],ma=E["618930965815"],na=E["618930965818"];function I(){}I.prototype={};function oa(){this.l=[];this.J=[];this.H=[];this.h=null;this.j=[];this.I=[];this.g=this.i=null;this.C=[];this.G=[];this.M=this.L=this.K=this.v=this.A=this.D=this.s=this.u=this.F=this.o=this.m=null}class pa{}class J extends D(pa,773590659617,oa,{ka:1,Wa:2}){}
J[C]=[I,ia,{constructor(){fa(this,()=>{const {queries:{Ta:c,qa:d,ra:f,Ua:g,Sa:h,Ha:k,Qa:l,xa:n,ya:p,La:q,Aa:r,za:t,Ka:u,Ca:v,Ba:w,bb:x,$a:y,Za:z}}=this;this.scan({fa:c,P:d,R:f,ga:g,ea:h,aa:k,da:l,V:n,W:p,ca:q,Y:r,X:t,ba:u,$:v,Z:w,ja:x,ia:y,ha:z})})},scan:function({fa:c,P:d,R:f,ga:g,ea:h,aa:k,da:l,V:n,W:p,ca:q,Y:r,X:t,ba:u,$:v,Z:w,ja:x,ia:y,ha:z}){const {element:e,la:{vdusPQs:{h:F,i:G,g:xa}},queries:{fa:M,P:N,R:O,ga:P,ea:Q,aa:R,da:S,V:T,W:U,ca:V,Y:W,X,ba:Y,$:Z,Z:aa,ja:ba,ia:ca,
ha:da}}=this,H=ma(e);let ea=[];if(M){let a;c?a=e.closest(c):a=document;ea.push(...(a?a.querySelectorAll(M):[]))}c=[];if(N){let a;d?a=e.closest(d):a=document;c.push(...(a?a.querySelectorAll(N):[]))}d=[];if(O){let a;f?a=e.closest(f):a=document;d.push(...(a?a.querySelectorAll(O):[]))}f=[];if(P){let a;g?a=e.closest(g):a=document;f.push(...(a?a.querySelectorAll(P):[]))}g=[];if(Q){let a;h?a=e.closest(h):a=document;g.push(...(a?a.querySelectorAll(Q):[]))}h=[];if(R){let a;k?a=e.closest(k):a=document;h.push(...(a?
a.querySelectorAll(R):[]))}k=[];if(S){let a;l?a=e.closest(l):a=document;k.push(...(a?a.querySelectorAll(S):[]))}Object.assign(this,{h:H[F],i:H[G],g:H[xa],I:ea,j:c,l:d,J:f,H:g,C:h,G:k,m:(a=>{let b;a?b=e.closest(a):b=document;return T?b.querySelector(T):void 0})(n),o:(a=>{let b;a?b=e.closest(a):b=document;return U?b.querySelector(U):void 0})(p),F:(a=>{let b;a?b=e.closest(a):b=document;return V?b.querySelector(V):void 0})(q),u:(a=>{let b;a?b=e.closest(a):b=document;return W?b.querySelector(W):void 0})(r),
s:(a=>{let b;a?b=e.closest(a):b=document;return X?b.querySelector(X):void 0})(t),D:(a=>{let b;a?b=e.closest(a):b=document;return Y?b.querySelector(Y):void 0})(u),A:(a=>{let b;a?b=e.closest(a):b=document;return Z?b.querySelector(Z):void 0})(v),v:(a=>{let b;a?b=e.closest(a):b=document;return aa?b.querySelector(aa):void 0})(w),K:(a=>{let b;a?b=e.closest(a):b=document;return ba?b.querySelector(ba):void 0})(x),L:(a=>{let b;a?b=e.closest(a):b=document;return ca?b.querySelector(ca):void 0})(y),M:(a=>{let b;
a?b=e.closest(a):b=document;return da?b.querySelector(da):void 0})(z)})}},{[ha]:{l:1,J:1,H:1,h:1,j:1,I:1,i:1,g:1,C:1,G:1,m:1,o:1,F:1,u:1,s:1,D:1,A:1,v:1,K:1,L:1,M:1},initializer({l:c,J:d,H:f,h:g,j:h,I:k,i:l,g:n,C:p,G:q,m:r,o:t,F:u,u:v,s:w,D:x,A:y,v:z,K:e,L:F,M:G}){void 0!==c&&(this.l=c);void 0!==d&&(this.J=d);void 0!==f&&(this.H=f);void 0!==g&&(this.h=g);void 0!==h&&(this.j=h);void 0!==k&&(this.I=k);void 0!==l&&(this.i=l);void 0!==n&&(this.g=n);void 0!==p&&(this.C=p);void 0!==q&&(this.G=q);void 0!==
r&&(this.m=r);void 0!==t&&(this.o=t);void 0!==u&&(this.F=u);void 0!==v&&(this.u=v);void 0!==w&&(this.s=w);void 0!==x&&(this.D=x);void 0!==y&&(this.A=y);void 0!==z&&(this.v=z);void 0!==e&&(this.K=e);void 0!==F&&(this.L=F);void 0!==G&&(this.M=G)}}];var qa=class extends J.implements(){};const ra={O:"amount",T:"from",U:"to",type:"type",id:"id"};function sa(c,d){const f=c.O,g=c.T,h=c.U;return(c=c.id)?["Transaction #"+c,f,g,"to",h,"|",d].join(" "):g&&h?["Exchange",f,g,"to",h,"|",d].join(" "):d};function ta(){}ta.prototype={};class ua{}class va extends D(ua,773590659624,null,{oa:1,Va:2}){}va[C]=[ta,ka,{}];function wa(){}wa.prototype={};class ya{}class za extends D(ya,773590659627,null,{pa:1,Ya:2}){}za[C]=[wa,la,{allocator(){this.methods={}}}];const K={Ma:"89b02",Na:"72c0a",O:"748e6",ma:"9dc9f",T:"4212d",U:"c60b5",Da:"7d0bd",Ea:"57342",type:"599dc",Fa:"7cb0f",Oa:"16cad",Ia:"0ba91",Ra:"6491e",id:"b80bb",Ja:"0b951"};const Aa={...K};const Ba=Object.keys(K).reduce((c,d)=>{c[K[d]]=d;return c},{});function Ca(){}Ca.prototype={};class Da{}class Ea extends D(Da,773590659625,null,{la:1,Xa:2}){}function L(){}
Ea[C]=[Ca,L.prototype={deduceInputs(){const {ka:{h:c,i:d,g:f}}=this;return{T:null==c?void 0:c.innerText,U:null==d?void 0:d.innerText,O:null==f?void 0:f.innerText}}},L.prototype={inputsPQs:Aa,queriesPQs:{ca:"70874",V:"189ed",W:"c3057",Y:"ecfe5",ua:"dea0b",wa:"ff39e",Ga:"752a8",Pa:"08d96",X:"f9c2e",$:"13da4",Z:"02ed0",ja:"63ad3",sa:"3e699",ta:"0a9ee",R:"2406f",ab:"ce7e3",ia:"d7fd3",ha:"67089",ga:"1e665",P:"026fb",fa:"6550e",ba:"15964",ea:"0d48c",aa:"d23f6",da:"7eb8e"},memoryQPs:Ba},ja,za,na,L.prototype=
{vdusPQs:{h:"c3c11",i:"c3c12",g:"c3c13",m:"c3c14",o:"c3c15",F:"c3c16",u:"c3c17",s:"c3c114",A:"c3c115",v:"c3c116",K:"c3c117",l:"c3c121",L:"c3c123",M:"c3c124",J:"c3c125",j:"c3c126",I:"c3c127",D:"c3c128",H:"c3c129",C:"c3c130",G:"c3c131"}}];var Fa=class extends Ea.implements(va,qa,{get queries(){return this.settings}},{buildTitle:sa,__$id:7735906596},{baseTitle:document.title,urlInputs:ra}){};module.exports["773590659641"]=qa;module.exports["773590659671"]=Fa;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['7735906596']=module.exports