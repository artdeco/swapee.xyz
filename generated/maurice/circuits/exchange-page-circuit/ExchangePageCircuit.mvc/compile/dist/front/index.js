/**
 * Display for presenting information from the _IExchangePageCircuit_.
 * @extends {xyz.swapee.wc.ExchangePageCircuitDisplay}
 */
class ExchangePageCircuitDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ExchangePageCircuitScreen}
 */
class ExchangePageCircuitScreen extends (class {/* lazy-loaded */}) {}

module.exports.ExchangePageCircuitDisplay = ExchangePageCircuitDisplay
module.exports.ExchangePageCircuitScreen = ExchangePageCircuitScreen