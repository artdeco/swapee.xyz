/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuit` interface.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuit}
 */
class AbstractExchangePageCircuit extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangePageCircuit_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangePageCircuitPort}
 */
class ExchangePageCircuitPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitController}
 */
class AbstractExchangePageCircuitController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IExchangePageCircuit_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ExchangePageCircuitElement}
 */
class ExchangePageCircuitElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangePageCircuitBuffer}
 */
class ExchangePageCircuitBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangePageCircuitComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangePageCircuitComputer}
 */
class AbstractExchangePageCircuitComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ExchangePageCircuitController}
 */
class ExchangePageCircuitController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangePageCircuit = AbstractExchangePageCircuit
module.exports.ExchangePageCircuitPort = ExchangePageCircuitPort
module.exports.AbstractExchangePageCircuitController = AbstractExchangePageCircuitController
module.exports.ExchangePageCircuitElement = ExchangePageCircuitElement
module.exports.ExchangePageCircuitBuffer = ExchangePageCircuitBuffer
module.exports.AbstractExchangePageCircuitComputer = AbstractExchangePageCircuitComputer
module.exports.ExchangePageCircuitController = ExchangePageCircuitController

Object.defineProperties(module.exports, {
 'AbstractExchangePageCircuit': {get: () => require('./precompile/internal')[77359065961]},
 [77359065961]: {get: () => module.exports['AbstractExchangePageCircuit']},
 'ExchangePageCircuitPort': {get: () => require('./precompile/internal')[77359065963]},
 [77359065963]: {get: () => module.exports['ExchangePageCircuitPort']},
 'AbstractExchangePageCircuitController': {get: () => require('./precompile/internal')[77359065964]},
 [77359065964]: {get: () => module.exports['AbstractExchangePageCircuitController']},
 'ExchangePageCircuitElement': {get: () => require('./precompile/internal')[77359065968]},
 [77359065968]: {get: () => module.exports['ExchangePageCircuitElement']},
 'ExchangePageCircuitBuffer': {get: () => require('./precompile/internal')[773590659611]},
 [773590659611]: {get: () => module.exports['ExchangePageCircuitBuffer']},
 'AbstractExchangePageCircuitComputer': {get: () => require('./precompile/internal')[773590659630]},
 [773590659630]: {get: () => module.exports['AbstractExchangePageCircuitComputer']},
 'ExchangePageCircuitController': {get: () => require('./precompile/internal')[773590659661]},
 [773590659661]: {get: () => module.exports['ExchangePageCircuitController']},
})