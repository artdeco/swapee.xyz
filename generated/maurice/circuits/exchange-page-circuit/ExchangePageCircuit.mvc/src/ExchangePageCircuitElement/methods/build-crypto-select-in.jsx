/** @type {xyz.swapee.wc.IExchangePageCircuitElement._buildCryptoSelectIn} */
export default function buildCryptoSelectIn({selectedIcon:selectedIcon}) {
 return (<div $id="ExchangePageCircuit">
  <imgwr $id="InImWr">{selectedIcon}</imgwr>
 </div>)
}