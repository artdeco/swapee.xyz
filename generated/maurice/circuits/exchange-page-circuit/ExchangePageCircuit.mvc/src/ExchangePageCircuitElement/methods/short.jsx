/** @type {xyz.swapee.wc.IExchangePageCircuitElement._short} */
export default function short({
 amountFrom:amountFrom,cryptoOut:cryptoOut,cryptoIn:cryptoIn,
 fixedRate:fixedRate,id:_id,notId:notId,floatingRate:floatingRate,
},{
 OffersFilter:OffersFilter,ExchangeIntent:ExchangeIntent,
 ExchangeIdRow,TransactionInfo,
},{DealBroker:{estimatedAmountTo:estimatedAmountTo},TransactionInfo:{fixed:fixed}}) {
 // ExchangeBroker:{id:id}
 return <>
  <OffersFilter amountOut={estimatedAmountTo} />
  {/* <_ExchangePageCircuit id={id} /> */}
  <ExchangeIdRow id={_id} fixed={fixed} />
  <TransactionInfo tid={_id} />
  {/* <ExchangeBroker fixedId={fixedId} /> */}
  {/* <DealBroker getOffer={ready} /> */}
  {/* also need to render this however it's not within the component */}
  {/* currently not possible */}
  <ExchangeIntent currencyFrom={cryptoIn} currencyTo={cryptoOut} amountFrom={amountFrom} fixed={fixedRate} float={floatingRate} ready={notId} />
 </>
}