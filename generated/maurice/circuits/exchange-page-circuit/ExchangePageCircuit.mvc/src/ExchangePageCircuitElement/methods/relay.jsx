/** @type {xyz.swapee.wc.IExchangePageCircuitElement._relay} */
export default function relay({
 This:This,DealBroker:DealBroker,ExchangeIntent:ExchangeIntent,
 OfferExchange:OfferExchange,OffersFilter:OffersFilter,
}) {
 return (<>
  <This onIdLow={[
   OfferExchange.reset(),
  ]} />
  <OfferExchange onReset={[
   This.unsetId(),
  ]} />
  <OffersFilter onSwapDirection={OfferExchange.swapAddresses} />
  <ExchangeIntent
   onCurrencyOutHigh={DealBroker.pulseGetOffer()}
   onCurrencyInHigh={DealBroker.pulseGetOffer()}
   onFixedEdge={DealBroker.pulseGetOffer()}
   onAmountFromHigh={DealBroker.pulseGetOffer()}
  />
 </>)
}