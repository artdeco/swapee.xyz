/** @type {xyz.swapee.wc.IExchangePageCircuitElement._render} */
export default function ExchangePageCircuitRender({
 cryptoIn:cryptoIn,cryptoOut:cryptoOut,amountFrom:amountFrom,id:id,
}) {
 return (<div $id="ExchangePageCircuit">
  <span $id="CryptoInLa">{cryptoIn}</span>
  <span $id="CryptoOutLa">{cryptoOut}</span>
  <span $id="AmountFromLa">{amountFrom}</span>

  <div $id="RevealWithId" $reveal={id} />
  <div $id="ConcealWithId" $conceal={id} />
 </div>)
}