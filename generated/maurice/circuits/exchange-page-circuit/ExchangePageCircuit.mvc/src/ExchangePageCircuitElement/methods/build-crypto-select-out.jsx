/** @type {xyz.swapee.wc.IExchangePageCircuitElement._buildCryptoSelectOut} */
export default function buildCryptoSelectOut({selectedIcon:selectedIcon}) {
 return (<div $id="ExchangePageCircuit">
  <imgwr $id="OutImWr">{selectedIcon}</imgwr>
 </div>)
}