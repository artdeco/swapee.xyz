/** @type {xyz.swapee.wc.IExchangePageCircuitElement._buildTransactionInfo} */
export default function buildTransactionInfo({tid:tid}) {
 return <>
  <div $id="ConcealWithTid" $conceal={tid} />
  <div $id="RevealWithTid" $reveal={tid} />
 </>
}