import solder from './methods/solder'
import server from './methods/server'
import buildTransactionInfo from './methods/build-transaction-info'
import buildOfferExchange from './methods/build-offer-exchange'
import buildCryptoSelectIn from './methods/build-crypto-select-in'
import buildCryptoSelectOut from './methods/build-crypto-select-out'
import render from './methods/render'
import ExchangePageCircuitServerController from '../ExchangePageCircuitServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractExchangePageCircuitElement from '../../gen/AbstractExchangePageCircuitElement'

/** @extends {xyz.swapee.wc.ExchangePageCircuitElement} */
export default class ExchangePageCircuitElement extends AbstractExchangePageCircuitElement.implements(
 ExchangePageCircuitServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IExchangePageCircuitElement} */ ({
  solder:solder,
  server:server,
  buildTransactionInfo:buildTransactionInfo,
  buildOfferExchange:buildOfferExchange,
  buildCryptoSelectIn:buildCryptoSelectIn,
  buildCryptoSelectOut:buildCryptoSelectOut,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IExchangePageCircuitElement}*/({
   classesMap: true,
   rootSelector:     `.ExchangePageCircuit`,
   stylesheet:       'html/styles/ExchangePageCircuit.css',
   blockName:        'html/ExchangePageCircuitBlock.html',
   // buildOffersFilter(){},
  }),
  /**@type {xyz.swapee.wc.IExchangePageCircuitDesigner}*/({
  }),
){}

// thank you for using web circuits
