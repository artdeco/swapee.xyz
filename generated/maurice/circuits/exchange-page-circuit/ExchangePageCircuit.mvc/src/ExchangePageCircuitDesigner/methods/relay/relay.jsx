
/**@type {xyz.swapee.wc.IExchangePageCircuitDesigner._relay} */
export default function relay({
 This:This,DealBroker:DealBroker,ExchangeIntent:ExchangeIntent,
 OfferExchange:OfferExchange,OffersFilter:OffersFilter,
}) {
 return (h(Fragment,{},
  h(This,{ onIdLow:[
   OfferExchange.reset(),
  ] }),
  h(OfferExchange,{ onReset:[
   This.unsetId(),
  ] }),
  h(OffersFilter,{ onSwapDirection:OfferExchange.swapAddresses }),
  h(ExchangeIntent,{
   onCurrencyOutHigh:DealBroker.pulseGetOffer(),
   onCurrencyInHigh:DealBroker.pulseGetOffer(),
   onFixedEdge:DealBroker.pulseGetOffer(),
   onAmountFromHigh:DealBroker.pulseGetOffer()
  })
 ))
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZXhjaGFuZ2UtcGFnZS1jaXJjdWl0L2V4Y2hhbmdlLXBhZ2UtY2lyY3VpdC53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBZ1NHLFNBQVMsS0FBSztDQUNiLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyw2QkFBNkI7Q0FDN0QsMkJBQTJCLENBQUMseUJBQXlCO0FBQ3RELENBQUM7Q0FDQSxPQUFPLENBQUMsWUFBQztFQUNSLFNBQU0sUUFBUztHQUNkLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztFQUN0QixDQUFFO0VBQ0Ysa0JBQWUsUUFBUztHQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7RUFDZixDQUFFO0VBQ0YsaUJBQWMsZ0JBQWlCLGFBQWEsQ0FBQyxhQUFjO0VBQzNEO0dBQ0Msa0JBQW1CLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBRTtHQUM5QyxpQkFBa0IsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFFO0dBQzdDLFlBQWEsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFFO0dBQ3hDLGlCQUFrQixVQUFVLENBQUMsYUFBYSxDQUFDO0FBQzlDO0NBQ0MsQ0FBa0M7QUFDbkMsQ0FBRiJ9