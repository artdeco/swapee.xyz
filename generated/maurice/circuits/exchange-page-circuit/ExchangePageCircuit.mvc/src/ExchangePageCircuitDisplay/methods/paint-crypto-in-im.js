/** @type {xyz.swapee.wc.IExchangePageCircuitDisplay._paintCryptoInIm} */
export default function paintCryptoInIm() {
 setTimeout(()=>{
  const img=this.CryptoInImWr.querySelector('img').src
  this.setInputs({inImg:img})
 },10)
}