/** @type {xyz.swapee.wc.IExchangePageCircuitDisplay._paintCryptoOutIm} */
export default function paintCryptoOutIm() {
 setTimeout(()=>{
  const img=this.CryptoOutImWr.querySelector('img').src
  this.setInputs({outImg:img})
 },10)
}