import {urlInputs} from '../../../../../../../maurice/circuits/exchange-page-circuit/url'
import ExchangePageCircuitHtmlController from '../ExchangePageCircuitHtmlController'
import ExchangePageCircuitHtmlComputer from '../ExchangePageCircuitHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractExchangePageCircuitHtmlComponent} from '../../gen/AbstractExchangePageCircuitHtmlComponent'

/** @extends {xyz.swapee.wc.ExchangePageCircuitHtmlComponent} */
export default class extends AbstractExchangePageCircuitHtmlComponent.implements(
 ExchangePageCircuitHtmlController,
 ExchangePageCircuitHtmlComputer,
 IntegratedComponentInitialiser,
/**@type {!xyz.swapee.wc.IExchangePageCircuitHtmlComponent}*/({
   urlInputs:urlInputs,
  }),
){}