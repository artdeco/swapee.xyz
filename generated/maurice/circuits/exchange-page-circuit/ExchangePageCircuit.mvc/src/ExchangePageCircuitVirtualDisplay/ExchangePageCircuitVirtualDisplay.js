import {Painter} from '@webcircuits/webcircuits'

const ExchangePageCircuitVirtualDisplay=Painter.__trait(
 /**@type {!xyz.swapee.wc.IExchangePageCircuitVirtualDisplay}*/({
  paint:[
   function paintCryptoInIm({},{ExchangeIntent:ExchangeIntent}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _lan={}
    if(ExchangeIntent) {
     const{'96c88':currencyFrom}=ExchangeIntent.model
     _lan['3427b']={'96c88':currencyFrom}
    }else _lan['3427b']={}
    if(!_lan['3427b']['96c88']) return
    const _mem=serMemory({})
    t_pa({
     pid:'e6ff635',
     mem:_mem,
     lan:_lan,
    })
   },
   function paintCryptoOutIm({},{ExchangeIntent:ExchangeIntent}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _lan={}
    if(ExchangeIntent) {
     const{'c23cd':currencyTo}=ExchangeIntent.model
     _lan['3427b']={'c23cd':currencyTo}
    }else _lan['3427b']={}
    if(!_lan['3427b']['c23cd']) return
    const _mem=serMemory({})
    t_pa({
     pid:'bb45a77',
     mem:_mem,
     lan:_lan,
    })
   },
  ],
 }),
)
export default ExchangePageCircuitVirtualDisplay