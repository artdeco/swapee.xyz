/** @type {!engineering.type.mvc.ICalibrator.calibrate} */
export default function calibrate({exchangeIntentSel:exchangeIntentSel}) {
 this.insulate(7833868048,exchangeIntentSel,{
  cryptoIn:'96c88', // -> currencyFrom
  cryptoOut:'c23cd', // -> currencyTo
  amountFrom:'748e6', // -> amountFrom
  fixedRate:'cec31', // -> fixed
 })
}