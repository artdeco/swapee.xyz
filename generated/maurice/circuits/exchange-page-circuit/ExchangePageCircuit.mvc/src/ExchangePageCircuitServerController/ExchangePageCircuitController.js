import calibrate from './methods/calibrate'
import AbstractExchangePageCircuitController from '../../gen/AbstractExchangePageCircuitController'

/** @extends {xyz.swapee.wc.ExchangePageCircuitController} */
export default class extends AbstractExchangePageCircuitController.implements(
 /** @type {!xyz.swapee.wc.IExchangePageCircuitController} */ ({
  calibrate:calibrate,
 }),
){}