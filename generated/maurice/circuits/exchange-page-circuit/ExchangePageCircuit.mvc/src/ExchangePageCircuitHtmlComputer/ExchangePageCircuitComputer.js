import adaptNotId from './methods/adapt-not-id'
import adaptId from './methods/adapt-id'
import adaptIntent from './methods/adapt-intent'
import adaptType from './methods/adapt-type'
import {preadaptNotId,preadaptId,preadaptIntent,preadaptType} from '../../gen/AbstractExchangePageCircuitComputer/preadapters'
import AbstractExchangePageCircuitComputer from '../../gen/AbstractExchangePageCircuitComputer'

/** @extends {xyz.swapee.wc.ExchangePageCircuitComputer} */
export default class ExchangePageCircuitHtmlComputer extends AbstractExchangePageCircuitComputer.implements(
 /** @type {!xyz.swapee.wc.IExchangePageCircuitComputer} */ ({
  adaptNotId:adaptNotId,
  adaptId:adaptId,
  adaptIntent:adaptIntent,
  adaptType:adaptType,
  adapt:[preadaptNotId,preadaptId,preadaptIntent,preadaptType],
 }),
/**@type {!xyz.swapee.wc.IExchangePageCircuitComputer}*/({
   // adaptGetTransaction({exchangeBrokerId:exchangeBrokerId,id:id}) {
   //  return{
   //   getTransaction:!!(!exchangeBrokerId&&id),
   //  }
   // },
   // adaptClearMeta() {
   //  return{
   //   amountFrom:'',
   //   cryptoIn:'',
   //   cryptoOut:'',
   //   type:'',
   //  }
   // },
  }),
){}