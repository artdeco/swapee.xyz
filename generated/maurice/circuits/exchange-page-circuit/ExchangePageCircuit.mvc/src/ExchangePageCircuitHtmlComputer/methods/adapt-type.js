/** @type {xyz.swapee.wc.IExchangePageCircuitComputer._adaptType} */
export default function adaptType({type:type}) {
 if(type=='fixed') {
  return{fixedRate:true,floatingRate:false}
 }
 return{fixedRate:false,floatingRate:true}
}