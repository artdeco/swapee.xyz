/** @type {xyz.swapee.wc.IExchangePageCircuitComputer._adaptNotId} */
export default function adaptNotId({id:id}) {
 return{
  notId:!id,
 }
}