/** @type {xyz.swapee.wc.IExchangePageCircuitComputer._adaptIntent} */
export default function adaptIntent({
 fixed:fixed,currencyFrom:currencyFrom,currencyTo:currencyTo,
 amountFrom:amountFrom,
}) {
 return{
  cryptoIn:currencyFrom,cryptoOut:currencyTo,
  type:fixed?'fixed':'floating',
  amountFrom:amountFrom,
 }
}