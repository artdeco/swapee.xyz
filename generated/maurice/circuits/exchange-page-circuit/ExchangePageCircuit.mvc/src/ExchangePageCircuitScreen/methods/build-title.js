/** @type {xyz.swapee.wc.IExchangePageCircuitScreen._buildTitle} */
export default function buildTitle(state,baseTitle){
 const{
  amountFrom:amount,
  cryptoIn:from,
  cryptoOut:to,
  id,
 }=state
 if(id) return ['Transaction #'+id,amount,from,'to',to,'|',baseTitle].join(' ')
 if(!from||!to) return baseTitle
 return['Exchange',amount,from,'to',to,'|',baseTitle].join(' ')
}