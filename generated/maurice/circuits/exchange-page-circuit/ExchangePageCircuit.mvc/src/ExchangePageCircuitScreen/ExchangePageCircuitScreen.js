import {urlInputs} from '../../../../../../../maurice/circuits/exchange-page-circuit/url'
import buildTitle from './methods/build-title'
import AbstractExchangePageCircuitControllerAT from '../../gen/AbstractExchangePageCircuitControllerAT'
import ExchangePageCircuitDisplay from '../ExchangePageCircuitDisplay'
import AbstractExchangePageCircuitScreen from '../../gen/AbstractExchangePageCircuitScreen'

/** @extends {xyz.swapee.wc.ExchangePageCircuitScreen} */
export default class extends AbstractExchangePageCircuitScreen.implements(
 AbstractExchangePageCircuitControllerAT,
 ExchangePageCircuitDisplay,
 /**@type {!xyz.swapee.wc.IExchangePageCircuitScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IExchangePageCircuitScreen} */ ({
  buildTitle:buildTitle,
  __$id:7735906596,
 }),
/**@type {!xyz.swapee.wc.IExchangePageCircuitScreen}*/({
   baseTitle:document.title,
   urlInputs:urlInputs,
  }),
){}