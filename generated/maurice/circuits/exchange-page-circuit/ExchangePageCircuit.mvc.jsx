/** @extends {xyz.swapee.wc.AbstractExchangePageCircuit} */
export default class AbstractExchangePageCircuit extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractExchangePageCircuitComputer} */
export class AbstractExchangePageCircuitComputer extends (<computer>
   <adapter name="adaptId">
    <xyz.swapee.wc.IExchangeBrokerCore id="required" />
    <outputs>
     <xyz.swapee.wc.IExchangePageCircuitCore id />
    </outputs>
   </adapter>

   <adapter name="adaptNotId">
    <xyz.swapee.wc.IExchangePageCircuitCore id />
    <outputs>
     <xyz.swapee.wc.IExchangePageCircuitCore notId />
    </outputs>
   </adapter>

   <adapter name="adaptType">
    <xyz.swapee.wc.IExchangePageCircuitCore type />
    <outputs>
     <xyz.swapee.wc.IExchangePageCircuitCore fixedRate floatingRate />
    </outputs>
   </adapter>

   <adapter name="adaptIntent">
    <xyz.swapee.wc.IExchangeIntentCore currencyFrom="real" currencyTo amountFrom fixed />
    <outputs>
     <xyz.swapee.wc.IExchangePageCircuitCore type cryptoIn cryptoOut amountFrom />
    </outputs>
    From the intent, reads the data.
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangePageCircuitController} */
export class AbstractExchangePageCircuitController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangePageCircuitPort} */
export class ExchangePageCircuitPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangePageCircuitView} */
export class AbstractExchangePageCircuitView extends (<view>
  <classes>
   <string opt name="Class">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangePageCircuitElement} */
export class AbstractExchangePageCircuitElement extends (<element v3 html mv>
 <block src="./ExchangePageCircuit.mvc/src/ExchangePageCircuitElement/methods/render.jsx" />
 <inducer src="./ExchangePageCircuit.mvc/src/ExchangePageCircuitElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangePageCircuitHtmlComponent} */
export class AbstractExchangePageCircuitHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.ICryptoSelect via="CryptoSelectIn" />
   <xyz.swapee.wc.ICryptoSelect via="CryptoSelectOut" />
   <xyz.swapee.wc.IOffersFilter via="OffersFilter" />
   <xyz.swapee.wc.IExchangeBroker via="ExchangeBroker" />
   <xyz.swapee.wc.IDealBroker via="DealBroker" />
   <xyz.swapee.wc.IOfferExchange via="OfferExchange" />
   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
   <xyz.swapee.wc.IExchangeIdRow via="ExchangeIdRow" />
   <xyz.swapee.wc.ITransactionInfo via="TransactionInfo" />
   <xyz.swapee.wc.ITransactionInfoHead via="TransactionInfoHead" />
   <xyz.swapee.wc.ITransactionInfoHead via="TransactionInfoHeadMob" />
  </connectors>

</html-ic>) { }
// </class-end>