/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IRegionPingComputer={}
xyz.swapee.wc.IRegionPingComputer.adaptPing={}
xyz.swapee.wc.IRegionPingComputer.adaptDifference={}
xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired={}
xyz.swapee.wc.IRegionPingOuterCore={}
xyz.swapee.wc.IRegionPingOuterCore.Model={}
xyz.swapee.wc.IRegionPingOuterCore.Model.Host={}
xyz.swapee.wc.IRegionPingOuterCore.Model.Region={}
xyz.swapee.wc.IRegionPingOuterCore.Model.ServerRegion={}
xyz.swapee.wc.IRegionPingOuterCore.Model.Start={}
xyz.swapee.wc.IRegionPingOuterCore.Model.Received={}
xyz.swapee.wc.IRegionPingOuterCore.Model.Diff={}
xyz.swapee.wc.IRegionPingOuterCore.Model.Diff2={}
xyz.swapee.wc.IRegionPingOuterCore.WeakModel={}
xyz.swapee.wc.IRegionPingPort={}
xyz.swapee.wc.IRegionPingPort.Inputs={}
xyz.swapee.wc.IRegionPingPort.WeakInputs={}
xyz.swapee.wc.IRegionPingCore={}
xyz.swapee.wc.IRegionPingCore.Model={}
xyz.swapee.wc.IRegionPingCore.Model.LoadingPing={}
xyz.swapee.wc.IRegionPingCore.Model.HasMorePing={}
xyz.swapee.wc.IRegionPingCore.Model.LoadPingError={}
xyz.swapee.wc.IRegionPingPortInterface={}
xyz.swapee.wc.IRegionPingProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IRegionPingController={}
xyz.swapee.wc.front.IRegionPingControllerAT={}
xyz.swapee.wc.front.IRegionPingScreenAR={}
xyz.swapee.wc.IRegionPing={}
xyz.swapee.wc.IRegionPingHtmlComponent={}
xyz.swapee.wc.IRegionPingElement={}
xyz.swapee.wc.IRegionPingElementPort={}
xyz.swapee.wc.IRegionPingElementPort.Inputs={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts={}
xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts={}
xyz.swapee.wc.IRegionPingElementPort.WeakInputs={}
xyz.swapee.wc.IRegionPingRadio={}
xyz.swapee.wc.IRegionPingRadio.adaptLoadPing={}
xyz.swapee.wc.IRegionPingDesigner={}
xyz.swapee.wc.IRegionPingDesigner.communicator={}
xyz.swapee.wc.IRegionPingDesigner.relay={}
xyz.swapee.wc.IRegionPingService={}
xyz.swapee.wc.IRegionPingService.filterPing={}
xyz.swapee.wc.IRegionPingDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IRegionPingDisplay={}
xyz.swapee.wc.back.IRegionPingController={}
xyz.swapee.wc.back.IRegionPingControllerAR={}
xyz.swapee.wc.back.IRegionPingScreen={}
xyz.swapee.wc.back.IRegionPingScreenAT={}
xyz.swapee.wc.IRegionPingController={}
xyz.swapee.wc.IRegionPingScreen={}
xyz.swapee.wc.IRegionPingGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/02-IRegionPingComputer.xml}  d57421f2448373b03c0ab718e9446283 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IRegionPingComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingComputer)} xyz.swapee.wc.AbstractRegionPingComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingComputer} xyz.swapee.wc.RegionPingComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingComputer` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingComputer
 */
xyz.swapee.wc.AbstractRegionPingComputer = class extends /** @type {xyz.swapee.wc.AbstractRegionPingComputer.constructor&xyz.swapee.wc.RegionPingComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingComputer.prototype.constructor = xyz.swapee.wc.AbstractRegionPingComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingComputer.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingComputer}
 */
xyz.swapee.wc.AbstractRegionPingComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingComputer}
 */
xyz.swapee.wc.AbstractRegionPingComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingComputer}
 */
xyz.swapee.wc.AbstractRegionPingComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingComputer}
 */
xyz.swapee.wc.AbstractRegionPingComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingComputer.Initialese[]) => xyz.swapee.wc.IRegionPingComputer} xyz.swapee.wc.RegionPingComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IRegionPingComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.RegionPingMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IRegionPingComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IRegionPingComputer
 */
xyz.swapee.wc.IRegionPingComputer = class extends /** @type {xyz.swapee.wc.IRegionPingComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IRegionPingComputer.adaptPing} */
xyz.swapee.wc.IRegionPingComputer.prototype.adaptPing = function() {}
/** @type {xyz.swapee.wc.IRegionPingComputer.adaptDifference} */
xyz.swapee.wc.IRegionPingComputer.prototype.adaptDifference = function() {}
/** @type {xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired} */
xyz.swapee.wc.IRegionPingComputer.prototype.adaptDifferenceRequired = function() {}
/** @type {xyz.swapee.wc.IRegionPingComputer.compute} */
xyz.swapee.wc.IRegionPingComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingComputer.Initialese>)} xyz.swapee.wc.RegionPingComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingComputer} xyz.swapee.wc.IRegionPingComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IRegionPingComputer_ instances.
 * @constructor xyz.swapee.wc.RegionPingComputer
 * @implements {xyz.swapee.wc.IRegionPingComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingComputer.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingComputer = class extends /** @type {xyz.swapee.wc.RegionPingComputer.constructor&xyz.swapee.wc.IRegionPingComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingComputer}
 */
xyz.swapee.wc.RegionPingComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IRegionPingComputer} */
xyz.swapee.wc.RecordIRegionPingComputer

/** @typedef {xyz.swapee.wc.IRegionPingComputer} xyz.swapee.wc.BoundIRegionPingComputer */

/** @typedef {xyz.swapee.wc.RegionPingComputer} xyz.swapee.wc.BoundRegionPingComputer */

/**
 * Contains getters to cast the _IRegionPingComputer_ interface.
 * @interface xyz.swapee.wc.IRegionPingComputerCaster
 */
xyz.swapee.wc.IRegionPingComputerCaster = class { }
/**
 * Cast the _IRegionPingComputer_ instance into the _BoundIRegionPingComputer_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingComputer}
 */
xyz.swapee.wc.IRegionPingComputerCaster.prototype.asIRegionPingComputer
/**
 * Access the _RegionPingComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingComputer}
 */
xyz.swapee.wc.IRegionPingComputerCaster.prototype.superRegionPingComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IRegionPingComputer.adaptPing.Form, changes: xyz.swapee.wc.IRegionPingComputer.adaptPing.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IRegionPingComputer.adaptPing.Return|void)>)} xyz.swapee.wc.IRegionPingComputer.__adaptPing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingComputer.__adaptPing<!xyz.swapee.wc.IRegionPingComputer>} xyz.swapee.wc.IRegionPingComputer._adaptPing */
/** @typedef {typeof xyz.swapee.wc.IRegionPingComputer.adaptPing} */
/**
 * Filters ping.
 * @param {!xyz.swapee.wc.IRegionPingComputer.adaptPing.Form} form The form with inputs.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * @param {xyz.swapee.wc.IRegionPingComputer.adaptPing.Form} changes The previous values of the form.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IRegionPingComputer.adaptPing.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IRegionPingComputer.adaptPing = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.Start_Safe} xyz.swapee.wc.IRegionPingComputer.adaptPing.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.ServerRegion&xyz.swapee.wc.IRegionPingCore.Model.Received} xyz.swapee.wc.IRegionPingComputer.adaptPing.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IRegionPingComputer.adaptDifference.Form, changes: xyz.swapee.wc.IRegionPingComputer.adaptDifference.Form) => (void|xyz.swapee.wc.IRegionPingComputer.adaptDifference.Return)} xyz.swapee.wc.IRegionPingComputer.__adaptDifference
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingComputer.__adaptDifference<!xyz.swapee.wc.IRegionPingComputer>} xyz.swapee.wc.IRegionPingComputer._adaptDifference */
/** @typedef {typeof xyz.swapee.wc.IRegionPingComputer.adaptDifference} */
/**
 * @param {!xyz.swapee.wc.IRegionPingComputer.adaptDifference.Form} form The form with inputs.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * - `received` _?number_ The date when the request was received by the server. ⤴ *IRegionPingOuterCore.Model.Received_Safe*
 * @param {xyz.swapee.wc.IRegionPingComputer.adaptDifference.Form} changes The previous values of the form.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * - `received` _?number_ The date when the request was received by the server. ⤴ *IRegionPingOuterCore.Model.Received_Safe*
 * @return {void|xyz.swapee.wc.IRegionPingComputer.adaptDifference.Return} The form with outputs.
 */
xyz.swapee.wc.IRegionPingComputer.adaptDifference = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.Start_Safe&xyz.swapee.wc.IRegionPingCore.Model.Received_Safe} xyz.swapee.wc.IRegionPingComputer.adaptDifference.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.Diff&xyz.swapee.wc.IRegionPingCore.Model.Diff2} xyz.swapee.wc.IRegionPingComputer.adaptDifference.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Form, changes: xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Form) => (void|xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Return)} xyz.swapee.wc.IRegionPingComputer.__adaptDifferenceRequired
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingComputer.__adaptDifferenceRequired<!xyz.swapee.wc.IRegionPingComputer>} xyz.swapee.wc.IRegionPingComputer._adaptDifferenceRequired */
/** @typedef {typeof xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired} */
/**
 * @param {!xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Form} form The form with inputs.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * - `received` _?number_ The date when the request was received by the server. ⤴ *IRegionPingOuterCore.Model.Received_Safe*
 * @param {xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Form} changes The previous values of the form.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * - `received` _?number_ The date when the request was received by the server. ⤴ *IRegionPingOuterCore.Model.Received_Safe*
 * @return {void|xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Return} The form with outputs.
 */
xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.Start_Safe&xyz.swapee.wc.IRegionPingCore.Model.Received_Safe} xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.Diff&xyz.swapee.wc.IRegionPingCore.Model.Diff2} xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.RegionPingMemory) => void} xyz.swapee.wc.IRegionPingComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingComputer.__compute<!xyz.swapee.wc.IRegionPingComputer>} xyz.swapee.wc.IRegionPingComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IRegionPingComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.RegionPingMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/03-IRegionPingOuterCore.xml}  fee6bcc5158001ead342fade1ae7577b */
/**
 * The date when the request was received by the server.
 * @typedef {number}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Sent.sent

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Sent The date when the request was received by the server (optional overlay).
 * @prop {?number} [sent=null] The date when the request was received by the server. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Sent_Safe The date when the request was received by the server (required overlay).
 * @prop {?number} sent The date when the request was received by the server.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Sent The date when the request was received by the server (optional overlay).
 * @prop {*} [sent=null] The date when the request was received by the server. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Sent_Safe The date when the request was received by the server (required overlay).
 * @prop {*} sent The date when the request was received by the server.
 */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Sent} xyz.swapee.wc.IRegionPingPort.Inputs.Sent The date when the request was received by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Sent_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Sent_Safe The date when the request was received by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Sent} xyz.swapee.wc.IRegionPingPort.WeakInputs.Sent The date when the request was received by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Sent_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Sent_Safe The date when the request was received by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Sent} xyz.swapee.wc.IRegionPingCore.Model.Sent The date when the request was received by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Sent_Safe} xyz.swapee.wc.IRegionPingCore.Model.Sent_Safe The date when the request was received by the server (required overlay). */

/**
 * .
 * @typedef {boolean}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Ping.ping

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Ping  (optional overlay).
 * @prop {boolean} [ping=false] . Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Ping_Safe  (required overlay).
 * @prop {boolean} ping .
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Ping  (optional overlay).
 * @prop {*} [ping=null] . Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Ping_Safe  (required overlay).
 * @prop {*} ping .
 */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Ping} xyz.swapee.wc.IRegionPingPort.Inputs.Ping  (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Ping_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Ping_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Ping} xyz.swapee.wc.IRegionPingPort.WeakInputs.Ping  (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Ping_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Ping_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Ping} xyz.swapee.wc.IRegionPingCore.Model.Ping  (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Ping_Safe} xyz.swapee.wc.IRegionPingCore.Model.Ping_Safe  (required overlay). */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IRegionPingOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingOuterCore)} xyz.swapee.wc.AbstractRegionPingOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingOuterCore} xyz.swapee.wc.RegionPingOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingOuterCore
 */
xyz.swapee.wc.AbstractRegionPingOuterCore = class extends /** @type {xyz.swapee.wc.AbstractRegionPingOuterCore.constructor&xyz.swapee.wc.RegionPingOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingOuterCore.prototype.constructor = xyz.swapee.wc.AbstractRegionPingOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IRegionPingOuterCore|typeof xyz.swapee.wc.RegionPingOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingOuterCore}
 */
xyz.swapee.wc.AbstractRegionPingOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingOuterCore}
 */
xyz.swapee.wc.AbstractRegionPingOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IRegionPingOuterCore|typeof xyz.swapee.wc.RegionPingOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingOuterCore}
 */
xyz.swapee.wc.AbstractRegionPingOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IRegionPingOuterCore|typeof xyz.swapee.wc.RegionPingOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingOuterCore}
 */
xyz.swapee.wc.AbstractRegionPingOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingOuterCoreCaster)} xyz.swapee.wc.IRegionPingOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IRegionPing_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IRegionPingOuterCore
 */
xyz.swapee.wc.IRegionPingOuterCore = class extends /** @type {xyz.swapee.wc.IRegionPingOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IRegionPingOuterCore.prototype.constructor = xyz.swapee.wc.IRegionPingOuterCore

/** @typedef {function(new: xyz.swapee.wc.IRegionPingOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingOuterCore.Initialese>)} xyz.swapee.wc.RegionPingOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingOuterCore} xyz.swapee.wc.IRegionPingOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IRegionPingOuterCore_ instances.
 * @constructor xyz.swapee.wc.RegionPingOuterCore
 * @implements {xyz.swapee.wc.IRegionPingOuterCore} The _IRegionPing_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingOuterCore = class extends /** @type {xyz.swapee.wc.RegionPingOuterCore.constructor&xyz.swapee.wc.IRegionPingOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.RegionPingOuterCore.prototype.constructor = xyz.swapee.wc.RegionPingOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingOuterCore}
 */
xyz.swapee.wc.RegionPingOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingOuterCore.
 * @interface xyz.swapee.wc.IRegionPingOuterCoreFields
 */
xyz.swapee.wc.IRegionPingOuterCoreFields = class { }
/**
 * The _IRegionPing_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IRegionPingOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IRegionPingOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore} */
xyz.swapee.wc.RecordIRegionPingOuterCore

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore} xyz.swapee.wc.BoundIRegionPingOuterCore */

/** @typedef {xyz.swapee.wc.RegionPingOuterCore} xyz.swapee.wc.BoundRegionPingOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Host.host

/**
 * The region as deducted by the routed.
 * @typedef {string}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Region.region

/**
 * The region as confirmed by the server.
 * @typedef {string}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.ServerRegion.serverRegion

/**
 * The date when the request was started.
 * @typedef {number}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Start.start

/**
 * The date when the request was received by the server.
 * @typedef {number}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Received.received

/**
 * The ping as when request was received by the server from the client.
 * @typedef {number}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Diff.diff

/**
 * The ping as when request was received by the server from the client.
 * @typedef {number}
 */
xyz.swapee.wc.IRegionPingOuterCore.Model.Diff2.diff2

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Host&xyz.swapee.wc.IRegionPingOuterCore.Model.Region&xyz.swapee.wc.IRegionPingOuterCore.Model.ServerRegion&xyz.swapee.wc.IRegionPingOuterCore.Model.Start&xyz.swapee.wc.IRegionPingOuterCore.Model.Received&xyz.swapee.wc.IRegionPingOuterCore.Model.Diff&xyz.swapee.wc.IRegionPingOuterCore.Model.Diff2} xyz.swapee.wc.IRegionPingOuterCore.Model The _IRegionPing_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Host&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Region&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.ServerRegion&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Start&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Received&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff2} xyz.swapee.wc.IRegionPingOuterCore.WeakModel The _IRegionPing_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IRegionPingOuterCore_ interface.
 * @interface xyz.swapee.wc.IRegionPingOuterCoreCaster
 */
xyz.swapee.wc.IRegionPingOuterCoreCaster = class { }
/**
 * Cast the _IRegionPingOuterCore_ instance into the _BoundIRegionPingOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingOuterCore}
 */
xyz.swapee.wc.IRegionPingOuterCoreCaster.prototype.asIRegionPingOuterCore
/**
 * Access the _RegionPingOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingOuterCore}
 */
xyz.swapee.wc.IRegionPingOuterCoreCaster.prototype.superRegionPingOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Host The core property (optional overlay).
 * @prop {string} [host=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Host_Safe The core property (required overlay).
 * @prop {string} host The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Region The region as deducted by the routed (optional overlay).
 * @prop {string} [region=""] The region as deducted by the routed. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Region_Safe The region as deducted by the routed (required overlay).
 * @prop {string} region The region as deducted by the routed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.ServerRegion The region as confirmed by the server (optional overlay).
 * @prop {string} [serverRegion=""] The region as confirmed by the server. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.ServerRegion_Safe The region as confirmed by the server (required overlay).
 * @prop {string} serverRegion The region as confirmed by the server.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Start The date when the request was started (optional overlay).
 * @prop {?number} [start=null] The date when the request was started. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Start_Safe The date when the request was started (required overlay).
 * @prop {?number} start The date when the request was started.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Received The date when the request was received by the server (optional overlay).
 * @prop {?number} [received=null] The date when the request was received by the server. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Received_Safe The date when the request was received by the server (required overlay).
 * @prop {?number} received The date when the request was received by the server.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Diff The ping as when request was received by the server from the client (optional overlay).
 * @prop {number} [diff=0] The ping as when request was received by the server from the client. Default `0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Diff_Safe The ping as when request was received by the server from the client (required overlay).
 * @prop {number} diff The ping as when request was received by the server from the client.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Diff2 The ping as when request was received by the server from the client (optional overlay).
 * @prop {number} [diff2=0] The ping as when request was received by the server from the client. Default `0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.Model.Diff2_Safe The ping as when request was received by the server from the client (required overlay).
 * @prop {number} diff2 The ping as when request was received by the server from the client.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Host The core property (optional overlay).
 * @prop {*} [host=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Host_Safe The core property (required overlay).
 * @prop {*} host The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Region The region as deducted by the routed (optional overlay).
 * @prop {*} [region=null] The region as deducted by the routed. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Region_Safe The region as deducted by the routed (required overlay).
 * @prop {*} region The region as deducted by the routed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.ServerRegion The region as confirmed by the server (optional overlay).
 * @prop {*} [serverRegion=null] The region as confirmed by the server. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.ServerRegion_Safe The region as confirmed by the server (required overlay).
 * @prop {*} serverRegion The region as confirmed by the server.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Start The date when the request was started (optional overlay).
 * @prop {*} [start=null] The date when the request was started. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Start_Safe The date when the request was started (required overlay).
 * @prop {*} start The date when the request was started.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Received The date when the request was received by the server (optional overlay).
 * @prop {*} [received=null] The date when the request was received by the server. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Received_Safe The date when the request was received by the server (required overlay).
 * @prop {*} received The date when the request was received by the server.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff The ping as when request was received by the server from the client (optional overlay).
 * @prop {*} [diff=null] The ping as when request was received by the server from the client. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff_Safe The ping as when request was received by the server from the client (required overlay).
 * @prop {*} diff The ping as when request was received by the server from the client.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff2 The ping as when request was received by the server from the client (optional overlay).
 * @prop {*} [diff2=null] The ping as when request was received by the server from the client. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff2_Safe The ping as when request was received by the server from the client (required overlay).
 * @prop {*} diff2 The ping as when request was received by the server from the client.
 */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Host} xyz.swapee.wc.IRegionPingPort.Inputs.Host The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Host_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Region} xyz.swapee.wc.IRegionPingPort.Inputs.Region The region as deducted by the routed (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Region_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Region_Safe The region as deducted by the routed (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.ServerRegion} xyz.swapee.wc.IRegionPingPort.Inputs.ServerRegion The region as confirmed by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.ServerRegion_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.ServerRegion_Safe The region as confirmed by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Start} xyz.swapee.wc.IRegionPingPort.Inputs.Start The date when the request was started (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Start_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Start_Safe The date when the request was started (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Received} xyz.swapee.wc.IRegionPingPort.Inputs.Received The date when the request was received by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Received_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Received_Safe The date when the request was received by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff} xyz.swapee.wc.IRegionPingPort.Inputs.Diff The ping as when request was received by the server from the client (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Diff_Safe The ping as when request was received by the server from the client (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff2} xyz.swapee.wc.IRegionPingPort.Inputs.Diff2 The ping as when request was received by the server from the client (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff2_Safe} xyz.swapee.wc.IRegionPingPort.Inputs.Diff2_Safe The ping as when request was received by the server from the client (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Host} xyz.swapee.wc.IRegionPingPort.WeakInputs.Host The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Host_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Region} xyz.swapee.wc.IRegionPingPort.WeakInputs.Region The region as deducted by the routed (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Region_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Region_Safe The region as deducted by the routed (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.ServerRegion} xyz.swapee.wc.IRegionPingPort.WeakInputs.ServerRegion The region as confirmed by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.ServerRegion_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.ServerRegion_Safe The region as confirmed by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Start} xyz.swapee.wc.IRegionPingPort.WeakInputs.Start The date when the request was started (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Start_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Start_Safe The date when the request was started (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Received} xyz.swapee.wc.IRegionPingPort.WeakInputs.Received The date when the request was received by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Received_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Received_Safe The date when the request was received by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff} xyz.swapee.wc.IRegionPingPort.WeakInputs.Diff The ping as when request was received by the server from the client (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Diff_Safe The ping as when request was received by the server from the client (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff2} xyz.swapee.wc.IRegionPingPort.WeakInputs.Diff2 The ping as when request was received by the server from the client (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.WeakModel.Diff2_Safe} xyz.swapee.wc.IRegionPingPort.WeakInputs.Diff2_Safe The ping as when request was received by the server from the client (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Host} xyz.swapee.wc.IRegionPingCore.Model.Host The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Host_Safe} xyz.swapee.wc.IRegionPingCore.Model.Host_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Region} xyz.swapee.wc.IRegionPingCore.Model.Region The region as deducted by the routed (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Region_Safe} xyz.swapee.wc.IRegionPingCore.Model.Region_Safe The region as deducted by the routed (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.ServerRegion} xyz.swapee.wc.IRegionPingCore.Model.ServerRegion The region as confirmed by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.ServerRegion_Safe} xyz.swapee.wc.IRegionPingCore.Model.ServerRegion_Safe The region as confirmed by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Start} xyz.swapee.wc.IRegionPingCore.Model.Start The date when the request was started (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Start_Safe} xyz.swapee.wc.IRegionPingCore.Model.Start_Safe The date when the request was started (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Received} xyz.swapee.wc.IRegionPingCore.Model.Received The date when the request was received by the server (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Received_Safe} xyz.swapee.wc.IRegionPingCore.Model.Received_Safe The date when the request was received by the server (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Diff} xyz.swapee.wc.IRegionPingCore.Model.Diff The ping as when request was received by the server from the client (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Diff_Safe} xyz.swapee.wc.IRegionPingCore.Model.Diff_Safe The ping as when request was received by the server from the client (required overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Diff2} xyz.swapee.wc.IRegionPingCore.Model.Diff2 The ping as when request was received by the server from the client (optional overlay). */

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model.Diff2_Safe} xyz.swapee.wc.IRegionPingCore.Model.Diff2_Safe The ping as when request was received by the server from the client (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/04-IRegionPingPort.xml}  722ba6fdd9acc1601c2fca8666b0690b */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IRegionPingPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingPort)} xyz.swapee.wc.AbstractRegionPingPort.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingPort} xyz.swapee.wc.RegionPingPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingPort` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingPort
 */
xyz.swapee.wc.AbstractRegionPingPort = class extends /** @type {xyz.swapee.wc.AbstractRegionPingPort.constructor&xyz.swapee.wc.RegionPingPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingPort.prototype.constructor = xyz.swapee.wc.AbstractRegionPingPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingPort.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingPort|typeof xyz.swapee.wc.RegionPingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingPort}
 */
xyz.swapee.wc.AbstractRegionPingPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingPort}
 */
xyz.swapee.wc.AbstractRegionPingPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingPort|typeof xyz.swapee.wc.RegionPingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingPort}
 */
xyz.swapee.wc.AbstractRegionPingPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingPort|typeof xyz.swapee.wc.RegionPingPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingPort}
 */
xyz.swapee.wc.AbstractRegionPingPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingPort.Initialese[]) => xyz.swapee.wc.IRegionPingPort} xyz.swapee.wc.RegionPingPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingPortFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IRegionPingPort.Inputs>)} xyz.swapee.wc.IRegionPingPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IRegionPing_, providing input
 * pins.
 * @interface xyz.swapee.wc.IRegionPingPort
 */
xyz.swapee.wc.IRegionPingPort = class extends /** @type {xyz.swapee.wc.IRegionPingPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IRegionPingPort.resetPort} */
xyz.swapee.wc.IRegionPingPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IRegionPingPort.resetRegionPingPort} */
xyz.swapee.wc.IRegionPingPort.prototype.resetRegionPingPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingPort&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingPort.Initialese>)} xyz.swapee.wc.RegionPingPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingPort} xyz.swapee.wc.IRegionPingPort.typeof */
/**
 * A concrete class of _IRegionPingPort_ instances.
 * @constructor xyz.swapee.wc.RegionPingPort
 * @implements {xyz.swapee.wc.IRegionPingPort} The port that serves as an interface to the _IRegionPing_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingPort.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingPort = class extends /** @type {xyz.swapee.wc.RegionPingPort.constructor&xyz.swapee.wc.IRegionPingPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingPort}
 */
xyz.swapee.wc.RegionPingPort.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingPort.
 * @interface xyz.swapee.wc.IRegionPingPortFields
 */
xyz.swapee.wc.IRegionPingPortFields = class { }
/**
 * The inputs to the _IRegionPing_'s controller via its port.
 */
xyz.swapee.wc.IRegionPingPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IRegionPingPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IRegionPingPortFields.prototype.props = /** @type {!xyz.swapee.wc.IRegionPingPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingPort} */
xyz.swapee.wc.RecordIRegionPingPort

/** @typedef {xyz.swapee.wc.IRegionPingPort} xyz.swapee.wc.BoundIRegionPingPort */

/** @typedef {xyz.swapee.wc.RegionPingPort} xyz.swapee.wc.BoundRegionPingPort */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingOuterCore.WeakModel)} xyz.swapee.wc.IRegionPingPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingOuterCore.WeakModel} xyz.swapee.wc.IRegionPingOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IRegionPing_'s controller via its port.
 * @record xyz.swapee.wc.IRegionPingPort.Inputs
 */
xyz.swapee.wc.IRegionPingPort.Inputs = class extends /** @type {xyz.swapee.wc.IRegionPingPort.Inputs.constructor&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IRegionPingPort.Inputs.prototype.constructor = xyz.swapee.wc.IRegionPingPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IRegionPingOuterCore.WeakModel)} xyz.swapee.wc.IRegionPingPort.WeakInputs.constructor */
/**
 * The inputs to the _IRegionPing_'s controller via its port.
 * @record xyz.swapee.wc.IRegionPingPort.WeakInputs
 */
xyz.swapee.wc.IRegionPingPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IRegionPingPort.WeakInputs.constructor&xyz.swapee.wc.IRegionPingOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IRegionPingPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IRegionPingPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IRegionPingPortInterface
 */
xyz.swapee.wc.IRegionPingPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IRegionPingPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IRegionPingPortInterface.prototype.constructor = xyz.swapee.wc.IRegionPingPortInterface

/**
 * A concrete class of _IRegionPingPortInterface_ instances.
 * @constructor xyz.swapee.wc.RegionPingPortInterface
 * @implements {xyz.swapee.wc.IRegionPingPortInterface} The port interface.
 */
xyz.swapee.wc.RegionPingPortInterface = class extends xyz.swapee.wc.IRegionPingPortInterface { }
xyz.swapee.wc.RegionPingPortInterface.prototype.constructor = xyz.swapee.wc.RegionPingPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingPortInterface.Props
 * @prop {string} host The core property.
 * @prop {string} region The region as deducted by the routed.
 * @prop {string} serverRegion The region as confirmed by the server.
 * @prop {?number} start The date when the request was started.
 * @prop {?number} received The date when the request was received by the server.
 * @prop {number} diff The ping as when request was received by the server from the client.
 * @prop {number} diff2 The ping as when request was received by the server from the client.
 */

/**
 * Contains getters to cast the _IRegionPingPort_ interface.
 * @interface xyz.swapee.wc.IRegionPingPortCaster
 */
xyz.swapee.wc.IRegionPingPortCaster = class { }
/**
 * Cast the _IRegionPingPort_ instance into the _BoundIRegionPingPort_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingPort}
 */
xyz.swapee.wc.IRegionPingPortCaster.prototype.asIRegionPingPort
/**
 * Access the _RegionPingPort_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingPort}
 */
xyz.swapee.wc.IRegionPingPortCaster.prototype.superRegionPingPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingPort.__resetPort<!xyz.swapee.wc.IRegionPingPort>} xyz.swapee.wc.IRegionPingPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IRegionPingPort.resetPort} */
/**
 * Resets the _IRegionPing_ port.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingPort.__resetRegionPingPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingPort.__resetRegionPingPort<!xyz.swapee.wc.IRegionPingPort>} xyz.swapee.wc.IRegionPingPort._resetRegionPingPort */
/** @typedef {typeof xyz.swapee.wc.IRegionPingPort.resetRegionPingPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingPort.resetRegionPingPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/09-IRegionPingCore.xml}  2d207ff9eeba58e969035ace10b1bddf */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IRegionPingCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingCore)} xyz.swapee.wc.AbstractRegionPingCore.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingCore} xyz.swapee.wc.RegionPingCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingCore` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingCore
 */
xyz.swapee.wc.AbstractRegionPingCore = class extends /** @type {xyz.swapee.wc.AbstractRegionPingCore.constructor&xyz.swapee.wc.RegionPingCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingCore.prototype.constructor = xyz.swapee.wc.AbstractRegionPingCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingCore.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingCore|typeof xyz.swapee.wc.RegionPingCore)|(!xyz.swapee.wc.IRegionPingOuterCore|typeof xyz.swapee.wc.RegionPingOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingCore}
 */
xyz.swapee.wc.AbstractRegionPingCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingCore}
 */
xyz.swapee.wc.AbstractRegionPingCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingCore|typeof xyz.swapee.wc.RegionPingCore)|(!xyz.swapee.wc.IRegionPingOuterCore|typeof xyz.swapee.wc.RegionPingOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingCore}
 */
xyz.swapee.wc.AbstractRegionPingCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingCore|typeof xyz.swapee.wc.RegionPingCore)|(!xyz.swapee.wc.IRegionPingOuterCore|typeof xyz.swapee.wc.RegionPingOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingCore}
 */
xyz.swapee.wc.AbstractRegionPingCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingCoreCaster&xyz.swapee.wc.IRegionPingOuterCore)} xyz.swapee.wc.IRegionPingCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IRegionPingCore
 */
xyz.swapee.wc.IRegionPingCore = class extends /** @type {xyz.swapee.wc.IRegionPingCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IRegionPingOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IRegionPingCore.resetCore} */
xyz.swapee.wc.IRegionPingCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IRegionPingCore.resetRegionPingCore} */
xyz.swapee.wc.IRegionPingCore.prototype.resetRegionPingCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingCore&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingCore.Initialese>)} xyz.swapee.wc.RegionPingCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingCore} xyz.swapee.wc.IRegionPingCore.typeof */
/**
 * A concrete class of _IRegionPingCore_ instances.
 * @constructor xyz.swapee.wc.RegionPingCore
 * @implements {xyz.swapee.wc.IRegionPingCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingCore.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingCore = class extends /** @type {xyz.swapee.wc.RegionPingCore.constructor&xyz.swapee.wc.IRegionPingCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.RegionPingCore.prototype.constructor = xyz.swapee.wc.RegionPingCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingCore}
 */
xyz.swapee.wc.RegionPingCore.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingCore.
 * @interface xyz.swapee.wc.IRegionPingCoreFields
 */
xyz.swapee.wc.IRegionPingCoreFields = class { }
/**
 * The _IRegionPing_'s memory.
 */
xyz.swapee.wc.IRegionPingCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IRegionPingCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IRegionPingCoreFields.prototype.props = /** @type {xyz.swapee.wc.IRegionPingCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingCore} */
xyz.swapee.wc.RecordIRegionPingCore

/** @typedef {xyz.swapee.wc.IRegionPingCore} xyz.swapee.wc.BoundIRegionPingCore */

/** @typedef {xyz.swapee.wc.RegionPingCore} xyz.swapee.wc.BoundRegionPingCore */

/**
 * Whether the items are being loaded from the remote.
 * @typedef {boolean}
 */
xyz.swapee.wc.IRegionPingCore.Model.LoadingPing.loadingPing

/**
 * Whether there are more items to load.
 * @typedef {boolean}
 */
xyz.swapee.wc.IRegionPingCore.Model.HasMorePing.hasMorePing

/**
 * An error during loading of items from the remote.
 * @typedef {Error}
 */
xyz.swapee.wc.IRegionPingCore.Model.LoadPingError.loadPingError

/** @typedef {xyz.swapee.wc.IRegionPingOuterCore.Model&xyz.swapee.wc.IRegionPingCore.Model.LoadingPing&xyz.swapee.wc.IRegionPingCore.Model.HasMorePing&xyz.swapee.wc.IRegionPingCore.Model.LoadPingError} xyz.swapee.wc.IRegionPingCore.Model The _IRegionPing_'s memory. */

/**
 * Contains getters to cast the _IRegionPingCore_ interface.
 * @interface xyz.swapee.wc.IRegionPingCoreCaster
 */
xyz.swapee.wc.IRegionPingCoreCaster = class { }
/**
 * Cast the _IRegionPingCore_ instance into the _BoundIRegionPingCore_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingCore}
 */
xyz.swapee.wc.IRegionPingCoreCaster.prototype.asIRegionPingCore
/**
 * Access the _RegionPingCore_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingCore}
 */
xyz.swapee.wc.IRegionPingCoreCaster.prototype.superRegionPingCore

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingCore.Model.LoadingPing Whether the items are being loaded from the remote (optional overlay).
 * @prop {boolean} [loadingPing=false] Whether the items are being loaded from the remote. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingCore.Model.LoadingPing_Safe Whether the items are being loaded from the remote (required overlay).
 * @prop {boolean} loadingPing Whether the items are being loaded from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingCore.Model.HasMorePing Whether there are more items to load (optional overlay).
 * @prop {?boolean} [hasMorePing=null] Whether there are more items to load. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingCore.Model.HasMorePing_Safe Whether there are more items to load (required overlay).
 * @prop {?boolean} hasMorePing Whether there are more items to load.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingCore.Model.LoadPingError An error during loading of items from the remote (optional overlay).
 * @prop {?Error} [loadPingError=null] An error during loading of items from the remote. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingCore.Model.LoadPingError_Safe An error during loading of items from the remote (required overlay).
 * @prop {?Error} loadPingError An error during loading of items from the remote.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingCore.__resetCore<!xyz.swapee.wc.IRegionPingCore>} xyz.swapee.wc.IRegionPingCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IRegionPingCore.resetCore} */
/**
 * Resets the _IRegionPing_ core.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingCore.__resetRegionPingCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingCore.__resetRegionPingCore<!xyz.swapee.wc.IRegionPingCore>} xyz.swapee.wc.IRegionPingCore._resetRegionPingCore */
/** @typedef {typeof xyz.swapee.wc.IRegionPingCore.resetRegionPingCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingCore.resetRegionPingCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/10-IRegionPingProcessor.xml}  c73deeebe37e59133298363d7f792150 */
/** @typedef {xyz.swapee.wc.IRegionPingComputer.Initialese&xyz.swapee.wc.IRegionPingController.Initialese} xyz.swapee.wc.IRegionPingProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingProcessor)} xyz.swapee.wc.AbstractRegionPingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingProcessor} xyz.swapee.wc.RegionPingProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingProcessor
 */
xyz.swapee.wc.AbstractRegionPingProcessor = class extends /** @type {xyz.swapee.wc.AbstractRegionPingProcessor.constructor&xyz.swapee.wc.RegionPingProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingProcessor.prototype.constructor = xyz.swapee.wc.AbstractRegionPingProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!xyz.swapee.wc.IRegionPingCore|typeof xyz.swapee.wc.RegionPingCore)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingProcessor}
 */
xyz.swapee.wc.AbstractRegionPingProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingProcessor}
 */
xyz.swapee.wc.AbstractRegionPingProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!xyz.swapee.wc.IRegionPingCore|typeof xyz.swapee.wc.RegionPingCore)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingProcessor}
 */
xyz.swapee.wc.AbstractRegionPingProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!xyz.swapee.wc.IRegionPingCore|typeof xyz.swapee.wc.RegionPingCore)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingProcessor}
 */
xyz.swapee.wc.AbstractRegionPingProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingProcessor.Initialese[]) => xyz.swapee.wc.IRegionPingProcessor} xyz.swapee.wc.RegionPingProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IRegionPingProcessorCaster&xyz.swapee.wc.IRegionPingComputer&xyz.swapee.wc.IRegionPingCore&xyz.swapee.wc.IRegionPingController)} xyz.swapee.wc.IRegionPingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingCore} xyz.swapee.wc.IRegionPingCore.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController} xyz.swapee.wc.IRegionPingController.typeof */
/**
 * The processor to compute changes to the memory for the _IRegionPing_.
 * @interface xyz.swapee.wc.IRegionPingProcessor
 */
xyz.swapee.wc.IRegionPingProcessor = class extends /** @type {xyz.swapee.wc.IRegionPingProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IRegionPingComputer.typeof&xyz.swapee.wc.IRegionPingCore.typeof&xyz.swapee.wc.IRegionPingController.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingProcessor.Initialese>)} xyz.swapee.wc.RegionPingProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingProcessor} xyz.swapee.wc.IRegionPingProcessor.typeof */
/**
 * A concrete class of _IRegionPingProcessor_ instances.
 * @constructor xyz.swapee.wc.RegionPingProcessor
 * @implements {xyz.swapee.wc.IRegionPingProcessor} The processor to compute changes to the memory for the _IRegionPing_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingProcessor.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingProcessor = class extends /** @type {xyz.swapee.wc.RegionPingProcessor.constructor&xyz.swapee.wc.IRegionPingProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingProcessor}
 */
xyz.swapee.wc.RegionPingProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IRegionPingProcessor} */
xyz.swapee.wc.RecordIRegionPingProcessor

/** @typedef {xyz.swapee.wc.IRegionPingProcessor} xyz.swapee.wc.BoundIRegionPingProcessor */

/** @typedef {xyz.swapee.wc.RegionPingProcessor} xyz.swapee.wc.BoundRegionPingProcessor */

/**
 * Contains getters to cast the _IRegionPingProcessor_ interface.
 * @interface xyz.swapee.wc.IRegionPingProcessorCaster
 */
xyz.swapee.wc.IRegionPingProcessorCaster = class { }
/**
 * Cast the _IRegionPingProcessor_ instance into the _BoundIRegionPingProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingProcessor}
 */
xyz.swapee.wc.IRegionPingProcessorCaster.prototype.asIRegionPingProcessor
/**
 * Access the _RegionPingProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingProcessor}
 */
xyz.swapee.wc.IRegionPingProcessorCaster.prototype.superRegionPingProcessor

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingProcessor.__pulsePing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingProcessor.__pulsePing<!xyz.swapee.wc.IRegionPingProcessor>} xyz.swapee.wc.IRegionPingProcessor._pulsePing */
/** @typedef {typeof xyz.swapee.wc.IRegionPingProcessor.pulsePing} */
/**
 * A method called to set the `ping` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingProcessor.pulsePing = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingProcessor
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/100-RegionPingMemory.xml}  178966e9f20d703314fd0ad77f6a7e9f */
/**
 * The memory of the _IRegionPing_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.RegionPingMemory
 */
xyz.swapee.wc.RegionPingMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.RegionPingMemory.prototype.host = /** @type {string} */ (void 0)
/**
 * The region as deducted by the routed. Default empty string.
 */
xyz.swapee.wc.RegionPingMemory.prototype.region = /** @type {string} */ (void 0)
/**
 * The region as confirmed by the server. Default empty string.
 */
xyz.swapee.wc.RegionPingMemory.prototype.serverRegion = /** @type {string} */ (void 0)
/**
 * The date when the request was started. Default `null`.
 */
xyz.swapee.wc.RegionPingMemory.prototype.start = /** @type {?number} */ (void 0)
/**
 * The date when the request was received by the server. Default `null`.
 */
xyz.swapee.wc.RegionPingMemory.prototype.received = /** @type {?number} */ (void 0)
/**
 * The ping as when request was received by the server from the client. Default `0`.
 */
xyz.swapee.wc.RegionPingMemory.prototype.diff = /** @type {number} */ (void 0)
/**
 * The ping as when request was received by the server from the client. Default `0`.
 */
xyz.swapee.wc.RegionPingMemory.prototype.diff2 = /** @type {number} */ (void 0)
/**
 * Whether the items are being loaded from the remote. Default `false`.
 */
xyz.swapee.wc.RegionPingMemory.prototype.loadingPing = /** @type {boolean} */ (void 0)
/**
 * Whether there are more items to load. Default `null`.
 */
xyz.swapee.wc.RegionPingMemory.prototype.hasMorePing = /** @type {?boolean} */ (void 0)
/**
 * An error during loading of items from the remote. Default `null`.
 */
xyz.swapee.wc.RegionPingMemory.prototype.loadPingError = /** @type {?Error} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/102-RegionPingInputs.xml}  b3d6e2a067b9238d377a6d2dabcc5503 */
/**
 * The inputs of the _IRegionPing_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.RegionPingInputs
 */
xyz.swapee.wc.front.RegionPingInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.RegionPingInputs.prototype.host = /** @type {string|undefined} */ (void 0)
/**
 * The region as deducted by the routed. Default empty string.
 */
xyz.swapee.wc.front.RegionPingInputs.prototype.region = /** @type {string|undefined} */ (void 0)
/**
 * The region as confirmed by the server. Default empty string.
 */
xyz.swapee.wc.front.RegionPingInputs.prototype.serverRegion = /** @type {string|undefined} */ (void 0)
/**
 * The date when the request was started. Default `null`.
 */
xyz.swapee.wc.front.RegionPingInputs.prototype.start = /** @type {(?number)|undefined} */ (void 0)
/**
 * The date when the request was received by the server. Default `null`.
 */
xyz.swapee.wc.front.RegionPingInputs.prototype.received = /** @type {(?number)|undefined} */ (void 0)
/**
 * The ping as when request was received by the server from the client. Default `0`.
 */
xyz.swapee.wc.front.RegionPingInputs.prototype.diff = /** @type {number|undefined} */ (void 0)
/**
 * The ping as when request was received by the server from the client. Default `0`.
 */
xyz.swapee.wc.front.RegionPingInputs.prototype.diff2 = /** @type {number|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/11-IRegionPing.xml}  4f3d46276fd9c026bcf7cd89d0704d2a */
/**
 * An atomic wrapper for the _IRegionPing_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.RegionPingEnv
 */
xyz.swapee.wc.RegionPingEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.RegionPingEnv.prototype.regionPing = /** @type {xyz.swapee.wc.IRegionPing} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.IRegionPingController.Inputs>&xyz.swapee.wc.IRegionPingProcessor.Initialese&xyz.swapee.wc.IRegionPingComputer.Initialese&xyz.swapee.wc.IRegionPingController.Initialese} xyz.swapee.wc.IRegionPing.Initialese */

/** @typedef {function(new: xyz.swapee.wc.RegionPing)} xyz.swapee.wc.AbstractRegionPing.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPing} xyz.swapee.wc.RegionPing.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPing` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPing
 */
xyz.swapee.wc.AbstractRegionPing = class extends /** @type {xyz.swapee.wc.AbstractRegionPing.constructor&xyz.swapee.wc.RegionPing.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPing.prototype.constructor = xyz.swapee.wc.AbstractRegionPing
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPing.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPing} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPing|typeof xyz.swapee.wc.RegionPing)|(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPing}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPing.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPing}
 */
xyz.swapee.wc.AbstractRegionPing.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPing}
 */
xyz.swapee.wc.AbstractRegionPing.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPing|typeof xyz.swapee.wc.RegionPing)|(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPing}
 */
xyz.swapee.wc.AbstractRegionPing.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPing|typeof xyz.swapee.wc.RegionPing)|(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPing}
 */
xyz.swapee.wc.AbstractRegionPing.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPing.Initialese[]) => xyz.swapee.wc.IRegionPing} xyz.swapee.wc.RegionPingConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPing.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IRegionPing.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IRegionPing.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IRegionPing.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.RegionPingMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.RegionPingClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingCaster&xyz.swapee.wc.IRegionPingProcessor&xyz.swapee.wc.IRegionPingComputer&xyz.swapee.wc.IRegionPingController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.IRegionPingController.Inputs, null>)} xyz.swapee.wc.IRegionPing.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IRegionPing
 */
xyz.swapee.wc.IRegionPing = class extends /** @type {xyz.swapee.wc.IRegionPing.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IRegionPingProcessor.typeof&xyz.swapee.wc.IRegionPingComputer.typeof&xyz.swapee.wc.IRegionPingController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPing* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPing.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPing.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPing&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPing.Initialese>)} xyz.swapee.wc.RegionPing.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPing} xyz.swapee.wc.IRegionPing.typeof */
/**
 * A concrete class of _IRegionPing_ instances.
 * @constructor xyz.swapee.wc.RegionPing
 * @implements {xyz.swapee.wc.IRegionPing} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPing.Initialese>} ‎
 */
xyz.swapee.wc.RegionPing = class extends /** @type {xyz.swapee.wc.RegionPing.constructor&xyz.swapee.wc.IRegionPing.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPing* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPing.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPing* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPing.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPing.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPing}
 */
xyz.swapee.wc.RegionPing.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPing.
 * @interface xyz.swapee.wc.IRegionPingFields
 */
xyz.swapee.wc.IRegionPingFields = class { }
/**
 * The input pins of the _IRegionPing_ port.
 */
xyz.swapee.wc.IRegionPingFields.prototype.pinout = /** @type {!xyz.swapee.wc.IRegionPing.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPing} */
xyz.swapee.wc.RecordIRegionPing

/** @typedef {xyz.swapee.wc.IRegionPing} xyz.swapee.wc.BoundIRegionPing */

/** @typedef {xyz.swapee.wc.RegionPing} xyz.swapee.wc.BoundRegionPing */

/** @typedef {xyz.swapee.wc.IRegionPingController.Inputs} xyz.swapee.wc.IRegionPing.Pinout The input pins of the _IRegionPing_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IRegionPingController.Inputs>)} xyz.swapee.wc.IRegionPingBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IRegionPingBuffer
 */
xyz.swapee.wc.IRegionPingBuffer = class extends /** @type {xyz.swapee.wc.IRegionPingBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IRegionPingBuffer.prototype.constructor = xyz.swapee.wc.IRegionPingBuffer

/**
 * A concrete class of _IRegionPingBuffer_ instances.
 * @constructor xyz.swapee.wc.RegionPingBuffer
 * @implements {xyz.swapee.wc.IRegionPingBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.RegionPingBuffer = class extends xyz.swapee.wc.IRegionPingBuffer { }
xyz.swapee.wc.RegionPingBuffer.prototype.constructor = xyz.swapee.wc.RegionPingBuffer

/**
 * Contains getters to cast the _IRegionPing_ interface.
 * @interface xyz.swapee.wc.IRegionPingCaster
 */
xyz.swapee.wc.IRegionPingCaster = class { }
/**
 * Cast the _IRegionPing_ instance into the _BoundIRegionPing_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPing}
 */
xyz.swapee.wc.IRegionPingCaster.prototype.asIRegionPing
/**
 * Access the _RegionPing_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPing}
 */
xyz.swapee.wc.IRegionPingCaster.prototype.superRegionPing

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/110-RegionPingSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.RegionPingCachePQs
 */
xyz.swapee.wc.RegionPingCachePQs = class {
  constructor() {
    /**
     * `f6ff7`
     */
    this.loadingPing=/** @type {string} */ (void 0)
    /**
     * `eb2da`
     */
    this.hasMorePing=/** @type {string} */ (void 0)
    /**
     * `i589d`
     */
    this.loadPingError=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.RegionPingCachePQs.prototype.constructor = xyz.swapee.wc.RegionPingCachePQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.RegionPingCacheQPs
 * @dict
 */
xyz.swapee.wc.RegionPingCacheQPs = class { }
/**
 * `loadingPing`
 */
xyz.swapee.wc.RegionPingCacheQPs.prototype.f6ff7 = /** @type {string} */ (void 0)
/**
 * `hasMorePing`
 */
xyz.swapee.wc.RegionPingCacheQPs.prototype.eb2da = /** @type {string} */ (void 0)
/**
 * `loadPingError`
 */
xyz.swapee.wc.RegionPingCacheQPs.prototype.i589d = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.RegionPingVdusPQs
 */
xyz.swapee.wc.RegionPingVdusPQs = class { }
xyz.swapee.wc.RegionPingVdusPQs.prototype.constructor = xyz.swapee.wc.RegionPingVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.RegionPingVdusQPs
 * @dict
 */
xyz.swapee.wc.RegionPingVdusQPs = class { }
xyz.swapee.wc.RegionPingVdusQPs.prototype.constructor = xyz.swapee.wc.RegionPingVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/12-IRegionPingHtmlComponent.xml}  bb701b66acd8513364c949d9568a4a45 */
/** @typedef {xyz.swapee.wc.back.IRegionPingController.Initialese&xyz.swapee.wc.back.IRegionPingScreen.Initialese&xyz.swapee.wc.IRegionPing.Initialese&xyz.swapee.wc.IRegionPingGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IRegionPingProcessor.Initialese&xyz.swapee.wc.IRegionPingComputer.Initialese} xyz.swapee.wc.IRegionPingHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingHtmlComponent)} xyz.swapee.wc.AbstractRegionPingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingHtmlComponent} xyz.swapee.wc.RegionPingHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingHtmlComponent
 */
xyz.swapee.wc.AbstractRegionPingHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractRegionPingHtmlComponent.constructor&xyz.swapee.wc.RegionPingHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractRegionPingHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingHtmlComponent|typeof xyz.swapee.wc.RegionPingHtmlComponent)|(!xyz.swapee.wc.back.IRegionPingController|typeof xyz.swapee.wc.back.RegionPingController)|(!xyz.swapee.wc.back.IRegionPingScreen|typeof xyz.swapee.wc.back.RegionPingScreen)|(!xyz.swapee.wc.IRegionPing|typeof xyz.swapee.wc.RegionPing)|(!xyz.swapee.wc.IRegionPingGPU|typeof xyz.swapee.wc.RegionPingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingHtmlComponent}
 */
xyz.swapee.wc.AbstractRegionPingHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingHtmlComponent}
 */
xyz.swapee.wc.AbstractRegionPingHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingHtmlComponent|typeof xyz.swapee.wc.RegionPingHtmlComponent)|(!xyz.swapee.wc.back.IRegionPingController|typeof xyz.swapee.wc.back.RegionPingController)|(!xyz.swapee.wc.back.IRegionPingScreen|typeof xyz.swapee.wc.back.RegionPingScreen)|(!xyz.swapee.wc.IRegionPing|typeof xyz.swapee.wc.RegionPing)|(!xyz.swapee.wc.IRegionPingGPU|typeof xyz.swapee.wc.RegionPingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingHtmlComponent}
 */
xyz.swapee.wc.AbstractRegionPingHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingHtmlComponent|typeof xyz.swapee.wc.RegionPingHtmlComponent)|(!xyz.swapee.wc.back.IRegionPingController|typeof xyz.swapee.wc.back.RegionPingController)|(!xyz.swapee.wc.back.IRegionPingScreen|typeof xyz.swapee.wc.back.RegionPingScreen)|(!xyz.swapee.wc.IRegionPing|typeof xyz.swapee.wc.RegionPing)|(!xyz.swapee.wc.IRegionPingGPU|typeof xyz.swapee.wc.RegionPingGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IRegionPingProcessor|typeof xyz.swapee.wc.RegionPingProcessor)|(!xyz.swapee.wc.IRegionPingComputer|typeof xyz.swapee.wc.RegionPingComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingHtmlComponent}
 */
xyz.swapee.wc.AbstractRegionPingHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingHtmlComponent.Initialese[]) => xyz.swapee.wc.IRegionPingHtmlComponent} xyz.swapee.wc.RegionPingHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IRegionPingHtmlComponentCaster&xyz.swapee.wc.back.IRegionPingController&xyz.swapee.wc.back.IRegionPingScreen&xyz.swapee.wc.IRegionPing&xyz.swapee.wc.IRegionPingGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.IRegionPingController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IRegionPingProcessor&xyz.swapee.wc.IRegionPingComputer)} xyz.swapee.wc.IRegionPingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IRegionPingController} xyz.swapee.wc.back.IRegionPingController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IRegionPingScreen} xyz.swapee.wc.back.IRegionPingScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingGPU} xyz.swapee.wc.IRegionPingGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IRegionPing_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IRegionPingHtmlComponent
 */
xyz.swapee.wc.IRegionPingHtmlComponent = class extends /** @type {xyz.swapee.wc.IRegionPingHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IRegionPingController.typeof&xyz.swapee.wc.back.IRegionPingScreen.typeof&xyz.swapee.wc.IRegionPing.typeof&xyz.swapee.wc.IRegionPingGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IRegionPingProcessor.typeof&xyz.swapee.wc.IRegionPingComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingHtmlComponent.Initialese>)} xyz.swapee.wc.RegionPingHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingHtmlComponent} xyz.swapee.wc.IRegionPingHtmlComponent.typeof */
/**
 * A concrete class of _IRegionPingHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.RegionPingHtmlComponent
 * @implements {xyz.swapee.wc.IRegionPingHtmlComponent} The _IRegionPing_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingHtmlComponent = class extends /** @type {xyz.swapee.wc.RegionPingHtmlComponent.constructor&xyz.swapee.wc.IRegionPingHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingHtmlComponent}
 */
xyz.swapee.wc.RegionPingHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IRegionPingHtmlComponent} */
xyz.swapee.wc.RecordIRegionPingHtmlComponent

/** @typedef {xyz.swapee.wc.IRegionPingHtmlComponent} xyz.swapee.wc.BoundIRegionPingHtmlComponent */

/** @typedef {xyz.swapee.wc.RegionPingHtmlComponent} xyz.swapee.wc.BoundRegionPingHtmlComponent */

/**
 * Contains getters to cast the _IRegionPingHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IRegionPingHtmlComponentCaster
 */
xyz.swapee.wc.IRegionPingHtmlComponentCaster = class { }
/**
 * Cast the _IRegionPingHtmlComponent_ instance into the _BoundIRegionPingHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingHtmlComponent}
 */
xyz.swapee.wc.IRegionPingHtmlComponentCaster.prototype.asIRegionPingHtmlComponent
/**
 * Access the _RegionPingHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingHtmlComponent}
 */
xyz.swapee.wc.IRegionPingHtmlComponentCaster.prototype.superRegionPingHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/130-IRegionPingElement.xml}  1f8818f756e583716aede6767781ce4b */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.IRegionPingElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IRegionPingElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingElement)} xyz.swapee.wc.AbstractRegionPingElement.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingElement} xyz.swapee.wc.RegionPingElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingElement` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingElement
 */
xyz.swapee.wc.AbstractRegionPingElement = class extends /** @type {xyz.swapee.wc.AbstractRegionPingElement.constructor&xyz.swapee.wc.RegionPingElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingElement.prototype.constructor = xyz.swapee.wc.AbstractRegionPingElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingElement.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingElement|typeof xyz.swapee.wc.RegionPingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingElement}
 */
xyz.swapee.wc.AbstractRegionPingElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElement}
 */
xyz.swapee.wc.AbstractRegionPingElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingElement|typeof xyz.swapee.wc.RegionPingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElement}
 */
xyz.swapee.wc.AbstractRegionPingElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingElement|typeof xyz.swapee.wc.RegionPingElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElement}
 */
xyz.swapee.wc.AbstractRegionPingElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingElement.Initialese[]) => xyz.swapee.wc.IRegionPingElement} xyz.swapee.wc.RegionPingElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingElementFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.IRegionPingElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.IRegionPingElement.Inputs, null>)} xyz.swapee.wc.IRegionPingElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IRegionPing_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IRegionPingElement
 */
xyz.swapee.wc.IRegionPingElement = class extends /** @type {xyz.swapee.wc.IRegionPingElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IRegionPingElement.solder} */
xyz.swapee.wc.IRegionPingElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IRegionPingElement.render} */
xyz.swapee.wc.IRegionPingElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IRegionPingElement.server} */
xyz.swapee.wc.IRegionPingElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IRegionPingElement.inducer} */
xyz.swapee.wc.IRegionPingElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingElement&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingElement.Initialese>)} xyz.swapee.wc.RegionPingElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElement} xyz.swapee.wc.IRegionPingElement.typeof */
/**
 * A concrete class of _IRegionPingElement_ instances.
 * @constructor xyz.swapee.wc.RegionPingElement
 * @implements {xyz.swapee.wc.IRegionPingElement} A component description.
 *
 * The _IRegionPing_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingElement.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingElement = class extends /** @type {xyz.swapee.wc.RegionPingElement.constructor&xyz.swapee.wc.IRegionPingElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingElement}
 */
xyz.swapee.wc.RegionPingElement.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingElement.
 * @interface xyz.swapee.wc.IRegionPingElementFields
 */
xyz.swapee.wc.IRegionPingElementFields = class { }
/**
 * The element-specific inputs to the _IRegionPing_ component.
 */
xyz.swapee.wc.IRegionPingElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IRegionPingElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingElement} */
xyz.swapee.wc.RecordIRegionPingElement

/** @typedef {xyz.swapee.wc.IRegionPingElement} xyz.swapee.wc.BoundIRegionPingElement */

/** @typedef {xyz.swapee.wc.RegionPingElement} xyz.swapee.wc.BoundRegionPingElement */

/** @typedef {xyz.swapee.wc.IRegionPingPort.Inputs&xyz.swapee.wc.IRegionPingDisplay.Queries&xyz.swapee.wc.IRegionPingController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IRegionPingElementPort.Inputs} xyz.swapee.wc.IRegionPingElement.Inputs The element-specific inputs to the _IRegionPing_ component. */

/**
 * Contains getters to cast the _IRegionPingElement_ interface.
 * @interface xyz.swapee.wc.IRegionPingElementCaster
 */
xyz.swapee.wc.IRegionPingElementCaster = class { }
/**
 * Cast the _IRegionPingElement_ instance into the _BoundIRegionPingElement_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingElement}
 */
xyz.swapee.wc.IRegionPingElementCaster.prototype.asIRegionPingElement
/**
 * Access the _RegionPingElement_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingElement}
 */
xyz.swapee.wc.IRegionPingElementCaster.prototype.superRegionPingElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.RegionPingMemory, props: !xyz.swapee.wc.IRegionPingElement.Inputs) => Object<string, *>} xyz.swapee.wc.IRegionPingElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingElement.__solder<!xyz.swapee.wc.IRegionPingElement>} xyz.swapee.wc.IRegionPingElement._solder */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.RegionPingMemory} model The model.
 * - `loadingPing` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMorePing` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadPingError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IRegionPingElement.Inputs} props The element props.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IRegionPingElementPort.Inputs.NoSolder* Default `false`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IRegionPingElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.RegionPingMemory, instance?: !xyz.swapee.wc.IRegionPingScreen&xyz.swapee.wc.IRegionPingController) => !engineering.type.VNode} xyz.swapee.wc.IRegionPingElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingElement.__render<!xyz.swapee.wc.IRegionPingElement>} xyz.swapee.wc.IRegionPingElement._render */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.RegionPingMemory} [model] The model for the view.
 * - `loadingPing` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMorePing` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadPingError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IRegionPingScreen&xyz.swapee.wc.IRegionPingController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IRegionPingElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.RegionPingMemory, inputs: !xyz.swapee.wc.IRegionPingElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IRegionPingElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingElement.__server<!xyz.swapee.wc.IRegionPingElement>} xyz.swapee.wc.IRegionPingElement._server */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.RegionPingMemory} memory The memory registers.
 * - `loadingPing` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMorePing` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadPingError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IRegionPingElement.Inputs} inputs The inputs to the port.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IRegionPingElementPort.Inputs.NoSolder* Default `false`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IRegionPingElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.RegionPingMemory, port?: !xyz.swapee.wc.IRegionPingElement.Inputs) => ?} xyz.swapee.wc.IRegionPingElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingElement.__inducer<!xyz.swapee.wc.IRegionPingElement>} xyz.swapee.wc.IRegionPingElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.RegionPingMemory} [model] The model of the component into which to induce the state.
 * - `loadingPing` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMorePing` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadPingError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IRegionPingElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IRegionPingElementPort.Inputs.NoSolder* Default `false`.
 * @return {?}
 */
xyz.swapee.wc.IRegionPingElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/140-IRegionPingElementPort.xml}  8b2275ac40e5e2bb93c6b85a4e533fc9 */
/**
 * The options to pass to the _Regionwr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionwrOpts.regionwrOpts

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionwrOpts The options to pass to the _Regionwr_ vdu (optional overlay).
 * @prop {!Object} [regionwrOpts] The options to pass to the _Regionwr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionwrOpts_Safe The options to pass to the _Regionwr_ vdu (required overlay).
 * @prop {!Object} regionwrOpts The options to pass to the _Regionwr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionwrOpts The options to pass to the _Regionwr_ vdu (optional overlay).
 * @prop {*} [regionwrOpts=null] The options to pass to the _Regionwr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionwrOpts_Safe The options to pass to the _Regionwr_ vdu (required overlay).
 * @prop {*} regionwrOpts The options to pass to the _Regionwr_ vdu.
 */

/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IRegionPingElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingElementPort)} xyz.swapee.wc.AbstractRegionPingElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingElementPort} xyz.swapee.wc.RegionPingElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingElementPort
 */
xyz.swapee.wc.AbstractRegionPingElementPort = class extends /** @type {xyz.swapee.wc.AbstractRegionPingElementPort.constructor&xyz.swapee.wc.RegionPingElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingElementPort.prototype.constructor = xyz.swapee.wc.AbstractRegionPingElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingElementPort|typeof xyz.swapee.wc.RegionPingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingElementPort}
 */
xyz.swapee.wc.AbstractRegionPingElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElementPort}
 */
xyz.swapee.wc.AbstractRegionPingElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingElementPort|typeof xyz.swapee.wc.RegionPingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElementPort}
 */
xyz.swapee.wc.AbstractRegionPingElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingElementPort|typeof xyz.swapee.wc.RegionPingElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingElementPort}
 */
xyz.swapee.wc.AbstractRegionPingElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingElementPort.Initialese[]) => xyz.swapee.wc.IRegionPingElementPort} xyz.swapee.wc.RegionPingElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IRegionPingElementPort.Inputs>)} xyz.swapee.wc.IRegionPingElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IRegionPingElementPort
 */
xyz.swapee.wc.IRegionPingElementPort = class extends /** @type {xyz.swapee.wc.IRegionPingElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingElementPort.Initialese>)} xyz.swapee.wc.RegionPingElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort} xyz.swapee.wc.IRegionPingElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IRegionPingElementPort_ instances.
 * @constructor xyz.swapee.wc.RegionPingElementPort
 * @implements {xyz.swapee.wc.IRegionPingElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingElementPort.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingElementPort = class extends /** @type {xyz.swapee.wc.RegionPingElementPort.constructor&xyz.swapee.wc.IRegionPingElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingElementPort}
 */
xyz.swapee.wc.RegionPingElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingElementPort.
 * @interface xyz.swapee.wc.IRegionPingElementPortFields
 */
xyz.swapee.wc.IRegionPingElementPortFields = class { }
/**
 * The inputs to the _IRegionPingElement_'s controller via its element port.
 */
xyz.swapee.wc.IRegionPingElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IRegionPingElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IRegionPingElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IRegionPingElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingElementPort} */
xyz.swapee.wc.RecordIRegionPingElementPort

/** @typedef {xyz.swapee.wc.IRegionPingElementPort} xyz.swapee.wc.BoundIRegionPingElementPort */

/** @typedef {xyz.swapee.wc.RegionPingElementPort} xyz.swapee.wc.BoundRegionPingElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _RegionFlagLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts.regionFlagLaOpts

/**
 * The options to pass to the _LoWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts.loWrOpts

/**
 * The options to pass to the _PingWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts.pingWrOpts

/**
 * The options to pass to the _RegionPingLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts.regionPingLaOpts

/**
 * The options to pass to the _RegionPingLa2_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts.regionPingLa2Opts

/**
 * The options to pass to the _RegionLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts.regionLaOpts

/**
 * The options to pass to the _RegionWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts.regionWrOpts

/**
 * The options to pass to the _ServerRegionLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts.serverRegionLaOpts

/**
 * The options to pass to the _ServerRegionWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts.serverRegionWrOpts

/** @typedef {function(new: xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts&xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts&xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts&xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts&xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts)} xyz.swapee.wc.IRegionPingElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder} xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts} xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts.typeof */
/**
 * The inputs to the _IRegionPingElement_'s controller via its element port.
 * @record xyz.swapee.wc.IRegionPingElementPort.Inputs
 */
xyz.swapee.wc.IRegionPingElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IRegionPingElementPort.Inputs.constructor&xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IRegionPingElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IRegionPingElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IRegionPingElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionFlagLaOpts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.LoWrOpts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.PingWrOpts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLaOpts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLa2Opts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionLaOpts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionWrOpts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionLaOpts&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionWrOpts)} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionFlagLaOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionFlagLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.LoWrOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.LoWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.PingWrOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.PingWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLaOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLa2Opts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLa2Opts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionLaOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionWrOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionLaOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionWrOpts} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionWrOpts.typeof */
/**
 * The inputs to the _IRegionPingElement_'s controller via its element port.
 * @record xyz.swapee.wc.IRegionPingElementPort.WeakInputs
 */
xyz.swapee.wc.IRegionPingElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IRegionPingElementPort.WeakInputs.constructor&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionFlagLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.LoWrOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.PingWrOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLa2Opts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionWrOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionLaOpts.typeof&xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionWrOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IRegionPingElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IRegionPingElementPort.WeakInputs

/**
 * Contains getters to cast the _IRegionPingElementPort_ interface.
 * @interface xyz.swapee.wc.IRegionPingElementPortCaster
 */
xyz.swapee.wc.IRegionPingElementPortCaster = class { }
/**
 * Cast the _IRegionPingElementPort_ instance into the _BoundIRegionPingElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingElementPort}
 */
xyz.swapee.wc.IRegionPingElementPortCaster.prototype.asIRegionPingElementPort
/**
 * Access the _RegionPingElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingElementPort}
 */
xyz.swapee.wc.IRegionPingElementPortCaster.prototype.superRegionPingElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts The options to pass to the _RegionFlagLa_ vdu (optional overlay).
 * @prop {!Object} [regionFlagLaOpts] The options to pass to the _RegionFlagLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionFlagLaOpts_Safe The options to pass to the _RegionFlagLa_ vdu (required overlay).
 * @prop {!Object} regionFlagLaOpts The options to pass to the _RegionFlagLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts The options to pass to the _LoWr_ vdu (optional overlay).
 * @prop {!Object} [loWrOpts] The options to pass to the _LoWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.LoWrOpts_Safe The options to pass to the _LoWr_ vdu (required overlay).
 * @prop {!Object} loWrOpts The options to pass to the _LoWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts The options to pass to the _PingWr_ vdu (optional overlay).
 * @prop {!Object} [pingWrOpts] The options to pass to the _PingWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.PingWrOpts_Safe The options to pass to the _PingWr_ vdu (required overlay).
 * @prop {!Object} pingWrOpts The options to pass to the _PingWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts The options to pass to the _RegionPingLa_ vdu (optional overlay).
 * @prop {!Object} [regionPingLaOpts] The options to pass to the _RegionPingLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLaOpts_Safe The options to pass to the _RegionPingLa_ vdu (required overlay).
 * @prop {!Object} regionPingLaOpts The options to pass to the _RegionPingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts The options to pass to the _RegionPingLa2_ vdu (optional overlay).
 * @prop {!Object} [regionPingLa2Opts] The options to pass to the _RegionPingLa2_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionPingLa2Opts_Safe The options to pass to the _RegionPingLa2_ vdu (required overlay).
 * @prop {!Object} regionPingLa2Opts The options to pass to the _RegionPingLa2_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts The options to pass to the _RegionLa_ vdu (optional overlay).
 * @prop {!Object} [regionLaOpts] The options to pass to the _RegionLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionLaOpts_Safe The options to pass to the _RegionLa_ vdu (required overlay).
 * @prop {!Object} regionLaOpts The options to pass to the _RegionLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts The options to pass to the _RegionWr_ vdu (optional overlay).
 * @prop {!Object} [regionWrOpts] The options to pass to the _RegionWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.RegionWrOpts_Safe The options to pass to the _RegionWr_ vdu (required overlay).
 * @prop {!Object} regionWrOpts The options to pass to the _RegionWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts The options to pass to the _ServerRegionLa_ vdu (optional overlay).
 * @prop {!Object} [serverRegionLaOpts] The options to pass to the _ServerRegionLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionLaOpts_Safe The options to pass to the _ServerRegionLa_ vdu (required overlay).
 * @prop {!Object} serverRegionLaOpts The options to pass to the _ServerRegionLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts The options to pass to the _ServerRegionWr_ vdu (optional overlay).
 * @prop {!Object} [serverRegionWrOpts] The options to pass to the _ServerRegionWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.Inputs.ServerRegionWrOpts_Safe The options to pass to the _ServerRegionWr_ vdu (required overlay).
 * @prop {!Object} serverRegionWrOpts The options to pass to the _ServerRegionWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionFlagLaOpts The options to pass to the _RegionFlagLa_ vdu (optional overlay).
 * @prop {*} [regionFlagLaOpts=null] The options to pass to the _RegionFlagLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionFlagLaOpts_Safe The options to pass to the _RegionFlagLa_ vdu (required overlay).
 * @prop {*} regionFlagLaOpts The options to pass to the _RegionFlagLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.LoWrOpts The options to pass to the _LoWr_ vdu (optional overlay).
 * @prop {*} [loWrOpts=null] The options to pass to the _LoWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.LoWrOpts_Safe The options to pass to the _LoWr_ vdu (required overlay).
 * @prop {*} loWrOpts The options to pass to the _LoWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.PingWrOpts The options to pass to the _PingWr_ vdu (optional overlay).
 * @prop {*} [pingWrOpts=null] The options to pass to the _PingWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.PingWrOpts_Safe The options to pass to the _PingWr_ vdu (required overlay).
 * @prop {*} pingWrOpts The options to pass to the _PingWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLaOpts The options to pass to the _RegionPingLa_ vdu (optional overlay).
 * @prop {*} [regionPingLaOpts=null] The options to pass to the _RegionPingLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLaOpts_Safe The options to pass to the _RegionPingLa_ vdu (required overlay).
 * @prop {*} regionPingLaOpts The options to pass to the _RegionPingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLa2Opts The options to pass to the _RegionPingLa2_ vdu (optional overlay).
 * @prop {*} [regionPingLa2Opts=null] The options to pass to the _RegionPingLa2_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionPingLa2Opts_Safe The options to pass to the _RegionPingLa2_ vdu (required overlay).
 * @prop {*} regionPingLa2Opts The options to pass to the _RegionPingLa2_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionLaOpts The options to pass to the _RegionLa_ vdu (optional overlay).
 * @prop {*} [regionLaOpts=null] The options to pass to the _RegionLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionLaOpts_Safe The options to pass to the _RegionLa_ vdu (required overlay).
 * @prop {*} regionLaOpts The options to pass to the _RegionLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionWrOpts The options to pass to the _RegionWr_ vdu (optional overlay).
 * @prop {*} [regionWrOpts=null] The options to pass to the _RegionWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.RegionWrOpts_Safe The options to pass to the _RegionWr_ vdu (required overlay).
 * @prop {*} regionWrOpts The options to pass to the _RegionWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionLaOpts The options to pass to the _ServerRegionLa_ vdu (optional overlay).
 * @prop {*} [serverRegionLaOpts=null] The options to pass to the _ServerRegionLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionLaOpts_Safe The options to pass to the _ServerRegionLa_ vdu (required overlay).
 * @prop {*} serverRegionLaOpts The options to pass to the _ServerRegionLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionWrOpts The options to pass to the _ServerRegionWr_ vdu (optional overlay).
 * @prop {*} [serverRegionWrOpts=null] The options to pass to the _ServerRegionWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingElementPort.WeakInputs.ServerRegionWrOpts_Safe The options to pass to the _ServerRegionWr_ vdu (required overlay).
 * @prop {*} serverRegionWrOpts The options to pass to the _ServerRegionWr_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/160-IRegionPingRadio.xml}  6e47cede79dae69c9c8d6dfd85db38cf */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IRegionPingRadio.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingRadio)} xyz.swapee.wc.AbstractRegionPingRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingRadio} xyz.swapee.wc.RegionPingRadio.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingRadio` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingRadio
 */
xyz.swapee.wc.AbstractRegionPingRadio = class extends /** @type {xyz.swapee.wc.AbstractRegionPingRadio.constructor&xyz.swapee.wc.RegionPingRadio.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingRadio.prototype.constructor = xyz.swapee.wc.AbstractRegionPingRadio
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingRadio.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingRadio} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IRegionPingRadio|typeof xyz.swapee.wc.RegionPingRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingRadio.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingRadio}
 */
xyz.swapee.wc.AbstractRegionPingRadio.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingRadio}
 */
xyz.swapee.wc.AbstractRegionPingRadio.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IRegionPingRadio|typeof xyz.swapee.wc.RegionPingRadio} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingRadio}
 */
xyz.swapee.wc.AbstractRegionPingRadio.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IRegionPingRadio|typeof xyz.swapee.wc.RegionPingRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingRadio}
 */
xyz.swapee.wc.AbstractRegionPingRadio.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IRegionPingRadioCaster)} xyz.swapee.wc.IRegionPingRadio.constructor */
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @interface xyz.swapee.wc.IRegionPingRadio
 */
xyz.swapee.wc.IRegionPingRadio = class extends /** @type {xyz.swapee.wc.IRegionPingRadio.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IRegionPingRadio.adaptLoadPing} */
xyz.swapee.wc.IRegionPingRadio.prototype.adaptLoadPing = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingRadio&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingRadio.Initialese>)} xyz.swapee.wc.RegionPingRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingRadio} xyz.swapee.wc.IRegionPingRadio.typeof */
/**
 * A concrete class of _IRegionPingRadio_ instances.
 * @constructor xyz.swapee.wc.RegionPingRadio
 * @implements {xyz.swapee.wc.IRegionPingRadio} Transmits data _to-_ and _fro-_ off-chip system.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingRadio.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingRadio = class extends /** @type {xyz.swapee.wc.RegionPingRadio.constructor&xyz.swapee.wc.IRegionPingRadio.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.RegionPingRadio.prototype.constructor = xyz.swapee.wc.RegionPingRadio
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingRadio}
 */
xyz.swapee.wc.RegionPingRadio.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IRegionPingRadio} */
xyz.swapee.wc.RecordIRegionPingRadio

/** @typedef {xyz.swapee.wc.IRegionPingRadio} xyz.swapee.wc.BoundIRegionPingRadio */

/** @typedef {xyz.swapee.wc.RegionPingRadio} xyz.swapee.wc.BoundRegionPingRadio */

/**
 * Contains getters to cast the _IRegionPingRadio_ interface.
 * @interface xyz.swapee.wc.IRegionPingRadioCaster
 */
xyz.swapee.wc.IRegionPingRadioCaster = class { }
/**
 * Cast the _IRegionPingRadio_ instance into the _BoundIRegionPingRadio_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingRadio}
 */
xyz.swapee.wc.IRegionPingRadioCaster.prototype.asIRegionPingRadio
/**
 * Cast the _IRegionPingRadio_ instance into the _BoundIRegionPingComputer_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingComputer}
 */
xyz.swapee.wc.IRegionPingRadioCaster.prototype.asIRegionPingComputer
/**
 * Access the _RegionPingRadio_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingRadio}
 */
xyz.swapee.wc.IRegionPingRadioCaster.prototype.superRegionPingRadio

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IRegionPingRadio.adaptLoadPing.Form, changes: IRegionPingComputer.adaptLoadPing.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IRegionPingRadio.adaptLoadPing.Return|void)>)} xyz.swapee.wc.IRegionPingRadio.__adaptLoadPing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingRadio.__adaptLoadPing<!xyz.swapee.wc.IRegionPingRadio>} xyz.swapee.wc.IRegionPingRadio._adaptLoadPing */
/** @typedef {typeof xyz.swapee.wc.IRegionPingRadio.adaptLoadPing} */
/**
 * Filters ping.
 * @param {!xyz.swapee.wc.IRegionPingRadio.adaptLoadPing.Form} form The form with inputs.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * @param {IRegionPingComputer.adaptLoadPing.Form} changes The previous values of the form.
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IRegionPingRadio.adaptLoadPing.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IRegionPingRadio.adaptLoadPing = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.Start_Safe} xyz.swapee.wc.IRegionPingRadio.adaptLoadPing.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.ServerRegion&xyz.swapee.wc.IRegionPingCore.Model.Received} xyz.swapee.wc.IRegionPingRadio.adaptLoadPing.Return The form with outputs. */

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingRadio
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/170-IRegionPingDesigner.xml}  1af72c776fcaf124c990ae076a1233d8 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IRegionPingDesigner
 */
xyz.swapee.wc.IRegionPingDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.RegionPingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IRegionPing />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.RegionPingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IRegionPing />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.RegionPingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IRegionPingDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `RegionPing` _typeof IRegionPingController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IRegionPingDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `RegionPing` _typeof IRegionPingController_
   * - `This` _typeof IRegionPingController_
   * @param {!xyz.swapee.wc.IRegionPingDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `RegionPing` _!RegionPingMemory_
   * - `This` _!RegionPingMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.RegionPingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.RegionPingClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IRegionPingDesigner.prototype.constructor = xyz.swapee.wc.IRegionPingDesigner

/**
 * A concrete class of _IRegionPingDesigner_ instances.
 * @constructor xyz.swapee.wc.RegionPingDesigner
 * @implements {xyz.swapee.wc.IRegionPingDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.RegionPingDesigner = class extends xyz.swapee.wc.IRegionPingDesigner { }
xyz.swapee.wc.RegionPingDesigner.prototype.constructor = xyz.swapee.wc.RegionPingDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IRegionPingController} RegionPing
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IRegionPingController} RegionPing
 * @prop {typeof xyz.swapee.wc.IRegionPingController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IRegionPingDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.RegionPingMemory} RegionPing
 * @prop {!xyz.swapee.wc.RegionPingMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/190-IRegionPingService.xml}  2a206e59c8c316ccef5ecea7617a644d */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IRegionPingService.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingService)} xyz.swapee.wc.AbstractRegionPingService.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingService} xyz.swapee.wc.RegionPingService.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingService` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingService
 */
xyz.swapee.wc.AbstractRegionPingService = class extends /** @type {xyz.swapee.wc.AbstractRegionPingService.constructor&xyz.swapee.wc.RegionPingService.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingService.prototype.constructor = xyz.swapee.wc.AbstractRegionPingService
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingService.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingService} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IRegionPingService|typeof xyz.swapee.wc.RegionPingService} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingService.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingService}
 */
xyz.swapee.wc.AbstractRegionPingService.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingService}
 */
xyz.swapee.wc.AbstractRegionPingService.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IRegionPingService|typeof xyz.swapee.wc.RegionPingService} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingService}
 */
xyz.swapee.wc.AbstractRegionPingService.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IRegionPingService|typeof xyz.swapee.wc.RegionPingService} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingService}
 */
xyz.swapee.wc.AbstractRegionPingService.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IRegionPingServiceCaster)} xyz.swapee.wc.IRegionPingService.constructor */
/**
 * A service for the IRegionPing.
 * @interface xyz.swapee.wc.IRegionPingService
 */
xyz.swapee.wc.IRegionPingService = class extends /** @type {xyz.swapee.wc.IRegionPingService.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IRegionPingService.filterPing} */
xyz.swapee.wc.IRegionPingService.prototype.filterPing = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingService&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingService.Initialese>)} xyz.swapee.wc.RegionPingService.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingService} xyz.swapee.wc.IRegionPingService.typeof */
/**
 * A concrete class of _IRegionPingService_ instances.
 * @constructor xyz.swapee.wc.RegionPingService
 * @implements {xyz.swapee.wc.IRegionPingService} A service for the IRegionPing.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingService.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingService = class extends /** @type {xyz.swapee.wc.RegionPingService.constructor&xyz.swapee.wc.IRegionPingService.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.RegionPingService.prototype.constructor = xyz.swapee.wc.RegionPingService
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingService}
 */
xyz.swapee.wc.RegionPingService.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IRegionPingService} */
xyz.swapee.wc.RecordIRegionPingService

/** @typedef {xyz.swapee.wc.IRegionPingService} xyz.swapee.wc.BoundIRegionPingService */

/** @typedef {xyz.swapee.wc.RegionPingService} xyz.swapee.wc.BoundRegionPingService */

/**
 * Contains getters to cast the _IRegionPingService_ interface.
 * @interface xyz.swapee.wc.IRegionPingServiceCaster
 */
xyz.swapee.wc.IRegionPingServiceCaster = class { }
/**
 * Cast the _IRegionPingService_ instance into the _BoundIRegionPingService_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingService}
 */
xyz.swapee.wc.IRegionPingServiceCaster.prototype.asIRegionPingService
/**
 * Access the _RegionPingService_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingService}
 */
xyz.swapee.wc.IRegionPingServiceCaster.prototype.superRegionPingService

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IRegionPingService.filterPing.Form) => !Promise<xyz.swapee.wc.IRegionPingService.filterPing.Return>} xyz.swapee.wc.IRegionPingService.__filterPing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingService.__filterPing<!xyz.swapee.wc.IRegionPingService>} xyz.swapee.wc.IRegionPingService._filterPing */
/** @typedef {typeof xyz.swapee.wc.IRegionPingService.filterPing} */
/**
 * Filters ping.
 * @param {!xyz.swapee.wc.IRegionPingService.filterPing.Form} form The form.
 * - `start` _?number_ The date when the request was started. ⤴ *IRegionPingOuterCore.Model.Start_Safe*
 * @return {!Promise<xyz.swapee.wc.IRegionPingService.filterPing.Return>}
 */
xyz.swapee.wc.IRegionPingService.filterPing = function(form) {}

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.Start_Safe} xyz.swapee.wc.IRegionPingService.filterPing.Form The form. */

/** @typedef {xyz.swapee.wc.IRegionPingCore.Model.ServerRegion&xyz.swapee.wc.IRegionPingCore.Model.Received} xyz.swapee.wc.IRegionPingService.filterPing.Return */

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingService
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/40-IRegionPingDisplay.xml}  479d8d85ab35d85882774e4a9cb7f280 */
/**
 * @typedef {Object} $xyz.swapee.wc.IRegionPingDisplay.Initialese
 * @prop {HTMLSpanElement} [RegionFlagLa]
 * @prop {HTMLSpanElement} [LoWr]
 * @prop {HTMLDivElement} [PingWr]
 * @prop {HTMLSpanElement} [RegionPingLa]
 * @prop {HTMLSpanElement} [RegionPingLa2]
 * @prop {HTMLSpanElement} [RegionLa]
 * @prop {HTMLSpanElement} [RegionWr]
 * @prop {HTMLSpanElement} [ServerRegionLa]
 * @prop {HTMLSpanElement} [ServerRegionWr]
 */
/** @typedef {$xyz.swapee.wc.IRegionPingDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IRegionPingDisplay.Settings>} xyz.swapee.wc.IRegionPingDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.RegionPingDisplay)} xyz.swapee.wc.AbstractRegionPingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingDisplay} xyz.swapee.wc.RegionPingDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingDisplay
 */
xyz.swapee.wc.AbstractRegionPingDisplay = class extends /** @type {xyz.swapee.wc.AbstractRegionPingDisplay.constructor&xyz.swapee.wc.RegionPingDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingDisplay.prototype.constructor = xyz.swapee.wc.AbstractRegionPingDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingDisplay|typeof xyz.swapee.wc.RegionPingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingDisplay}
 */
xyz.swapee.wc.AbstractRegionPingDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingDisplay}
 */
xyz.swapee.wc.AbstractRegionPingDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingDisplay|typeof xyz.swapee.wc.RegionPingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingDisplay}
 */
xyz.swapee.wc.AbstractRegionPingDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingDisplay|typeof xyz.swapee.wc.RegionPingDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingDisplay}
 */
xyz.swapee.wc.AbstractRegionPingDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingDisplay.Initialese[]) => xyz.swapee.wc.IRegionPingDisplay} xyz.swapee.wc.RegionPingDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.RegionPingMemory, !HTMLDivElement, !xyz.swapee.wc.IRegionPingDisplay.Settings, xyz.swapee.wc.IRegionPingDisplay.Queries, null>)} xyz.swapee.wc.IRegionPingDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IRegionPing_.
 * @interface xyz.swapee.wc.IRegionPingDisplay
 */
xyz.swapee.wc.IRegionPingDisplay = class extends /** @type {xyz.swapee.wc.IRegionPingDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IRegionPingDisplay.paint} */
xyz.swapee.wc.IRegionPingDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingDisplay.Initialese>)} xyz.swapee.wc.RegionPingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingDisplay} xyz.swapee.wc.IRegionPingDisplay.typeof */
/**
 * A concrete class of _IRegionPingDisplay_ instances.
 * @constructor xyz.swapee.wc.RegionPingDisplay
 * @implements {xyz.swapee.wc.IRegionPingDisplay} Display for presenting information from the _IRegionPing_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingDisplay.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingDisplay = class extends /** @type {xyz.swapee.wc.RegionPingDisplay.constructor&xyz.swapee.wc.IRegionPingDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingDisplay}
 */
xyz.swapee.wc.RegionPingDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingDisplay.
 * @interface xyz.swapee.wc.IRegionPingDisplayFields
 */
xyz.swapee.wc.IRegionPingDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IRegionPingDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IRegionPingDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.RegionFlagLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.LoWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.PingWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.RegionPingLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.RegionPingLa2 = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.RegionLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.RegionWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.ServerRegionLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IRegionPingDisplayFields.prototype.ServerRegionWr = /** @type {HTMLSpanElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingDisplay} */
xyz.swapee.wc.RecordIRegionPingDisplay

/** @typedef {xyz.swapee.wc.IRegionPingDisplay} xyz.swapee.wc.BoundIRegionPingDisplay */

/** @typedef {xyz.swapee.wc.RegionPingDisplay} xyz.swapee.wc.BoundRegionPingDisplay */

/** @typedef {xyz.swapee.wc.IRegionPingDisplay.Queries} xyz.swapee.wc.IRegionPingDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IRegionPingDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IRegionPingDisplay_ interface.
 * @interface xyz.swapee.wc.IRegionPingDisplayCaster
 */
xyz.swapee.wc.IRegionPingDisplayCaster = class { }
/**
 * Cast the _IRegionPingDisplay_ instance into the _BoundIRegionPingDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingDisplay}
 */
xyz.swapee.wc.IRegionPingDisplayCaster.prototype.asIRegionPingDisplay
/**
 * Cast the _IRegionPingDisplay_ instance into the _BoundIRegionPingScreen_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingScreen}
 */
xyz.swapee.wc.IRegionPingDisplayCaster.prototype.asIRegionPingScreen
/**
 * Access the _RegionPingDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingDisplay}
 */
xyz.swapee.wc.IRegionPingDisplayCaster.prototype.superRegionPingDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.RegionPingMemory, land: null) => void} xyz.swapee.wc.IRegionPingDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingDisplay.__paint<!xyz.swapee.wc.IRegionPingDisplay>} xyz.swapee.wc.IRegionPingDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IRegionPingDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.RegionPingMemory} memory The display data.
 * - `host` _string_ The core property. Default empty string.
 * - `region` _string_ The region as deducted by the routed. Default empty string.
 * - `serverRegion` _string_ The region as confirmed by the server. Default empty string.
 * - `start` _?number_ The date when the request was started. Default `null`.
 * - `received` _?number_ The date when the request was received by the server. Default `null`.
 * - `diff` _number_ The ping as when request was received by the server from the client. Default `0`.
 * - `diff2` _number_ The ping as when request was received by the server from the client. Default `0`.
 * - `loadingPing` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMorePing` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadPingError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/40-IRegionPingDisplayBack.xml}  94e917b2361e345e2b90f8635db2685d */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IRegionPingDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [RegionFlagLa]
 * @prop {!com.webcircuits.IHtmlTwin} [LoWr]
 * @prop {!com.webcircuits.IHtmlTwin} [PingWr]
 * @prop {!com.webcircuits.IHtmlTwin} [RegionPingLa]
 * @prop {!com.webcircuits.IHtmlTwin} [RegionPingLa2]
 * @prop {!com.webcircuits.IHtmlTwin} [RegionLa]
 * @prop {!com.webcircuits.IHtmlTwin} [RegionWr]
 * @prop {!com.webcircuits.IHtmlTwin} [ServerRegionLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ServerRegionWr]
 */
/** @typedef {$xyz.swapee.wc.back.IRegionPingDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.RegionPingClasses>} xyz.swapee.wc.back.IRegionPingDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.RegionPingDisplay)} xyz.swapee.wc.back.AbstractRegionPingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.RegionPingDisplay} xyz.swapee.wc.back.RegionPingDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IRegionPingDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractRegionPingDisplay
 */
xyz.swapee.wc.back.AbstractRegionPingDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractRegionPingDisplay.constructor&xyz.swapee.wc.back.RegionPingDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractRegionPingDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractRegionPingDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractRegionPingDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractRegionPingDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IRegionPingDisplay|typeof xyz.swapee.wc.back.RegionPingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractRegionPingDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractRegionPingDisplay}
 */
xyz.swapee.wc.back.AbstractRegionPingDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingDisplay}
 */
xyz.swapee.wc.back.AbstractRegionPingDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IRegionPingDisplay|typeof xyz.swapee.wc.back.RegionPingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingDisplay}
 */
xyz.swapee.wc.back.AbstractRegionPingDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IRegionPingDisplay|typeof xyz.swapee.wc.back.RegionPingDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingDisplay}
 */
xyz.swapee.wc.back.AbstractRegionPingDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IRegionPingDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IRegionPingDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.RegionPingClasses, null>)} xyz.swapee.wc.back.IRegionPingDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IRegionPingDisplay
 */
xyz.swapee.wc.back.IRegionPingDisplay = class extends /** @type {xyz.swapee.wc.back.IRegionPingDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IRegionPingDisplay.paint} */
xyz.swapee.wc.back.IRegionPingDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IRegionPingDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingDisplay.Initialese>)} xyz.swapee.wc.back.RegionPingDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IRegionPingDisplay} xyz.swapee.wc.back.IRegionPingDisplay.typeof */
/**
 * A concrete class of _IRegionPingDisplay_ instances.
 * @constructor xyz.swapee.wc.back.RegionPingDisplay
 * @implements {xyz.swapee.wc.back.IRegionPingDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.RegionPingDisplay = class extends /** @type {xyz.swapee.wc.back.RegionPingDisplay.constructor&xyz.swapee.wc.back.IRegionPingDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.RegionPingDisplay.prototype.constructor = xyz.swapee.wc.back.RegionPingDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.RegionPingDisplay}
 */
xyz.swapee.wc.back.RegionPingDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingDisplay.
 * @interface xyz.swapee.wc.back.IRegionPingDisplayFields
 */
xyz.swapee.wc.back.IRegionPingDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.RegionFlagLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.LoWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.PingWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.RegionPingLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.RegionPingLa2 = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.RegionLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.RegionWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.ServerRegionLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IRegionPingDisplayFields.prototype.ServerRegionWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IRegionPingDisplay} */
xyz.swapee.wc.back.RecordIRegionPingDisplay

/** @typedef {xyz.swapee.wc.back.IRegionPingDisplay} xyz.swapee.wc.back.BoundIRegionPingDisplay */

/** @typedef {xyz.swapee.wc.back.RegionPingDisplay} xyz.swapee.wc.back.BoundRegionPingDisplay */

/**
 * Contains getters to cast the _IRegionPingDisplay_ interface.
 * @interface xyz.swapee.wc.back.IRegionPingDisplayCaster
 */
xyz.swapee.wc.back.IRegionPingDisplayCaster = class { }
/**
 * Cast the _IRegionPingDisplay_ instance into the _BoundIRegionPingDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIRegionPingDisplay}
 */
xyz.swapee.wc.back.IRegionPingDisplayCaster.prototype.asIRegionPingDisplay
/**
 * Access the _RegionPingDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundRegionPingDisplay}
 */
xyz.swapee.wc.back.IRegionPingDisplayCaster.prototype.superRegionPingDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.RegionPingMemory, land?: null) => void} xyz.swapee.wc.back.IRegionPingDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IRegionPingDisplay.__paint<!xyz.swapee.wc.back.IRegionPingDisplay>} xyz.swapee.wc.back.IRegionPingDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IRegionPingDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.RegionPingMemory} [memory] The display data.
 * - `host` _string_ The core property. Default empty string.
 * - `region` _string_ The region as deducted by the routed. Default empty string.
 * - `serverRegion` _string_ The region as confirmed by the server. Default empty string.
 * - `start` _?number_ The date when the request was started. Default `null`.
 * - `received` _?number_ The date when the request was received by the server. Default `null`.
 * - `diff` _number_ The ping as when request was received by the server from the client. Default `0`.
 * - `diff2` _number_ The ping as when request was received by the server from the client. Default `0`.
 * - `loadingPing` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMorePing` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadPingError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IRegionPingDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IRegionPingDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/41-RegionPingClasses.xml}  24b4e8883c216486e300bd05c32aa953 */
/**
 * The classes of the _IRegionPingDisplay_.
 * @record xyz.swapee.wc.RegionPingClasses
 */
xyz.swapee.wc.RegionPingClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.RegionPingClasses.prototype.Reg = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.RegionPingClasses.prototype.props = /** @type {xyz.swapee.wc.RegionPingClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/50-IRegionPingController.xml}  6e2f83e9875bd0c60688e3902ebc56dc */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IRegionPingController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IRegionPingController.Inputs, !xyz.swapee.wc.IRegionPingOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IRegionPingOuterCore.WeakModel>} xyz.swapee.wc.IRegionPingController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.RegionPingController)} xyz.swapee.wc.AbstractRegionPingController.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingController} xyz.swapee.wc.RegionPingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingController` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingController
 */
xyz.swapee.wc.AbstractRegionPingController = class extends /** @type {xyz.swapee.wc.AbstractRegionPingController.constructor&xyz.swapee.wc.RegionPingController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingController.prototype.constructor = xyz.swapee.wc.AbstractRegionPingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingController.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingController}
 */
xyz.swapee.wc.AbstractRegionPingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingController}
 */
xyz.swapee.wc.AbstractRegionPingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingController}
 */
xyz.swapee.wc.AbstractRegionPingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingController}
 */
xyz.swapee.wc.AbstractRegionPingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingController.Initialese[]) => xyz.swapee.wc.IRegionPingController} xyz.swapee.wc.RegionPingControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IRegionPingController.Inputs, !xyz.swapee.wc.IRegionPingOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IRegionPingOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IRegionPingController.Inputs, !xyz.swapee.wc.IRegionPingController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IRegionPingController.Inputs, !xyz.swapee.wc.RegionPingMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IRegionPingController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IRegionPingController.Inputs>)} xyz.swapee.wc.IRegionPingController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IRegionPingController
 */
xyz.swapee.wc.IRegionPingController = class extends /** @type {xyz.swapee.wc.IRegionPingController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IRegionPingController.resetPort} */
xyz.swapee.wc.IRegionPingController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.setStart} */
xyz.swapee.wc.IRegionPingController.prototype.setStart = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.unsetStart} */
xyz.swapee.wc.IRegionPingController.prototype.unsetStart = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.setReceived} */
xyz.swapee.wc.IRegionPingController.prototype.setReceived = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.unsetReceived} */
xyz.swapee.wc.IRegionPingController.prototype.unsetReceived = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.setDiff} */
xyz.swapee.wc.IRegionPingController.prototype.setDiff = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.unsetDiff} */
xyz.swapee.wc.IRegionPingController.prototype.unsetDiff = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.setDiff2} */
xyz.swapee.wc.IRegionPingController.prototype.setDiff2 = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.unsetDiff2} */
xyz.swapee.wc.IRegionPingController.prototype.unsetDiff2 = function() {}
/** @type {xyz.swapee.wc.IRegionPingController.loadPing} */
xyz.swapee.wc.IRegionPingController.prototype.loadPing = function() {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingController&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingController.Initialese>)} xyz.swapee.wc.RegionPingController.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController} xyz.swapee.wc.IRegionPingController.typeof */
/**
 * A concrete class of _IRegionPingController_ instances.
 * @constructor xyz.swapee.wc.RegionPingController
 * @implements {xyz.swapee.wc.IRegionPingController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingController.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingController = class extends /** @type {xyz.swapee.wc.RegionPingController.constructor&xyz.swapee.wc.IRegionPingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingController}
 */
xyz.swapee.wc.RegionPingController.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingController.
 * @interface xyz.swapee.wc.IRegionPingControllerFields
 */
xyz.swapee.wc.IRegionPingControllerFields = class { }
/**
 * The inputs to the _IRegionPing_'s controller.
 */
xyz.swapee.wc.IRegionPingControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IRegionPingController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IRegionPingControllerFields.prototype.props = /** @type {xyz.swapee.wc.IRegionPingController} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingController} */
xyz.swapee.wc.RecordIRegionPingController

/** @typedef {xyz.swapee.wc.IRegionPingController} xyz.swapee.wc.BoundIRegionPingController */

/** @typedef {xyz.swapee.wc.RegionPingController} xyz.swapee.wc.BoundRegionPingController */

/** @typedef {xyz.swapee.wc.IRegionPingPort.Inputs} xyz.swapee.wc.IRegionPingController.Inputs The inputs to the _IRegionPing_'s controller. */

/** @typedef {xyz.swapee.wc.IRegionPingPort.WeakInputs} xyz.swapee.wc.IRegionPingController.WeakInputs The inputs to the _IRegionPing_'s controller. */

/**
 * Contains getters to cast the _IRegionPingController_ interface.
 * @interface xyz.swapee.wc.IRegionPingControllerCaster
 */
xyz.swapee.wc.IRegionPingControllerCaster = class { }
/**
 * Cast the _IRegionPingController_ instance into the _BoundIRegionPingController_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingController}
 */
xyz.swapee.wc.IRegionPingControllerCaster.prototype.asIRegionPingController
/**
 * Cast the _IRegionPingController_ instance into the _BoundIRegionPingProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingProcessor}
 */
xyz.swapee.wc.IRegionPingControllerCaster.prototype.asIRegionPingProcessor
/**
 * Access the _RegionPingController_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingController}
 */
xyz.swapee.wc.IRegionPingControllerCaster.prototype.superRegionPingController

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.IRegionPingController.__setSent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__setSent<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._setSent */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.setSent} */
/**
 * Sets the `sent` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.setSent = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__unsetSent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__unsetSent<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._unsetSent */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.unsetSent} */
/**
 * Clears the `sent` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.unsetSent = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__pulsePing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__pulsePing<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._pulsePing */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.pulsePing} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.pulsePing = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__resetPort<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.resetPort = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.IRegionPingController.__setStart
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__setStart<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._setStart */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.setStart} */
/**
 * Sets the `start` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.setStart = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__unsetStart
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__unsetStart<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._unsetStart */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.unsetStart} */
/**
 * Clears the `start` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.unsetStart = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.IRegionPingController.__setReceived
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__setReceived<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._setReceived */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.setReceived} */
/**
 * Sets the `received` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.setReceived = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__unsetReceived
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__unsetReceived<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._unsetReceived */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.unsetReceived} */
/**
 * Clears the `received` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.unsetReceived = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.IRegionPingController.__setDiff
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__setDiff<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._setDiff */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.setDiff} */
/**
 * Sets the `diff` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.setDiff = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__unsetDiff
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__unsetDiff<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._unsetDiff */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.unsetDiff} */
/**
 * Clears the `diff` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.unsetDiff = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.IRegionPingController.__setDiff2
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__setDiff2<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._setDiff2 */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.setDiff2} */
/**
 * Sets the `diff2` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.setDiff2 = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__unsetDiff2
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__unsetDiff2<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._unsetDiff2 */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.unsetDiff2} */
/**
 * Clears the `diff2` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.unsetDiff2 = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IRegionPingController.__loadPing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IRegionPingController.__loadPing<!xyz.swapee.wc.IRegionPingController>} xyz.swapee.wc.IRegionPingController._loadPing */
/** @typedef {typeof xyz.swapee.wc.IRegionPingController.loadPing} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.IRegionPingController.loadPing = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IRegionPingController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/51-IRegionPingControllerFront.xml}  d5287900de16acb88c73190d55074f50 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IRegionPingController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.RegionPingController)} xyz.swapee.wc.front.AbstractRegionPingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.RegionPingController} xyz.swapee.wc.front.RegionPingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IRegionPingController` interface.
 * @constructor xyz.swapee.wc.front.AbstractRegionPingController
 */
xyz.swapee.wc.front.AbstractRegionPingController = class extends /** @type {xyz.swapee.wc.front.AbstractRegionPingController.constructor&xyz.swapee.wc.front.RegionPingController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractRegionPingController.prototype.constructor = xyz.swapee.wc.front.AbstractRegionPingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractRegionPingController.class = /** @type {typeof xyz.swapee.wc.front.AbstractRegionPingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IRegionPingController|typeof xyz.swapee.wc.front.RegionPingController)|(!xyz.swapee.wc.front.IRegionPingControllerAT|typeof xyz.swapee.wc.front.RegionPingControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractRegionPingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractRegionPingController}
 */
xyz.swapee.wc.front.AbstractRegionPingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingController}
 */
xyz.swapee.wc.front.AbstractRegionPingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IRegionPingController|typeof xyz.swapee.wc.front.RegionPingController)|(!xyz.swapee.wc.front.IRegionPingControllerAT|typeof xyz.swapee.wc.front.RegionPingControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingController}
 */
xyz.swapee.wc.front.AbstractRegionPingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IRegionPingController|typeof xyz.swapee.wc.front.RegionPingController)|(!xyz.swapee.wc.front.IRegionPingControllerAT|typeof xyz.swapee.wc.front.RegionPingControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingController}
 */
xyz.swapee.wc.front.AbstractRegionPingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IRegionPingController.Initialese[]) => xyz.swapee.wc.front.IRegionPingController} xyz.swapee.wc.front.RegionPingControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IRegionPingControllerCaster&xyz.swapee.wc.front.IRegionPingControllerAT)} xyz.swapee.wc.front.IRegionPingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingControllerAT} xyz.swapee.wc.front.IRegionPingControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IRegionPingController
 */
xyz.swapee.wc.front.IRegionPingController = class extends /** @type {xyz.swapee.wc.front.IRegionPingController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IRegionPingControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IRegionPingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IRegionPingController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IRegionPingController.setStart} */
xyz.swapee.wc.front.IRegionPingController.prototype.setStart = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.unsetStart} */
xyz.swapee.wc.front.IRegionPingController.prototype.unsetStart = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.setReceived} */
xyz.swapee.wc.front.IRegionPingController.prototype.setReceived = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.unsetReceived} */
xyz.swapee.wc.front.IRegionPingController.prototype.unsetReceived = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.setDiff} */
xyz.swapee.wc.front.IRegionPingController.prototype.setDiff = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.unsetDiff} */
xyz.swapee.wc.front.IRegionPingController.prototype.unsetDiff = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.setDiff2} */
xyz.swapee.wc.front.IRegionPingController.prototype.setDiff2 = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.unsetDiff2} */
xyz.swapee.wc.front.IRegionPingController.prototype.unsetDiff2 = function() {}
/** @type {xyz.swapee.wc.front.IRegionPingController.loadPing} */
xyz.swapee.wc.front.IRegionPingController.prototype.loadPing = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IRegionPingController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IRegionPingController.Initialese>)} xyz.swapee.wc.front.RegionPingController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController} xyz.swapee.wc.front.IRegionPingController.typeof */
/**
 * A concrete class of _IRegionPingController_ instances.
 * @constructor xyz.swapee.wc.front.RegionPingController
 * @implements {xyz.swapee.wc.front.IRegionPingController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IRegionPingController.Initialese>} ‎
 */
xyz.swapee.wc.front.RegionPingController = class extends /** @type {xyz.swapee.wc.front.RegionPingController.constructor&xyz.swapee.wc.front.IRegionPingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IRegionPingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IRegionPingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.RegionPingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.RegionPingController}
 */
xyz.swapee.wc.front.RegionPingController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IRegionPingController} */
xyz.swapee.wc.front.RecordIRegionPingController

/** @typedef {xyz.swapee.wc.front.IRegionPingController} xyz.swapee.wc.front.BoundIRegionPingController */

/** @typedef {xyz.swapee.wc.front.RegionPingController} xyz.swapee.wc.front.BoundRegionPingController */

/**
 * Contains getters to cast the _IRegionPingController_ interface.
 * @interface xyz.swapee.wc.front.IRegionPingControllerCaster
 */
xyz.swapee.wc.front.IRegionPingControllerCaster = class { }
/**
 * Cast the _IRegionPingController_ instance into the _BoundIRegionPingController_ type.
 * @type {!xyz.swapee.wc.front.BoundIRegionPingController}
 */
xyz.swapee.wc.front.IRegionPingControllerCaster.prototype.asIRegionPingController
/**
 * Access the _RegionPingController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundRegionPingController}
 */
xyz.swapee.wc.front.IRegionPingControllerCaster.prototype.superRegionPingController

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.front.IRegionPingController.__setSent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__setSent<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._setSent */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.setSent} */
/**
 * Sets the `sent` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.setSent = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IRegionPingController.__unsetSent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__unsetSent<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._unsetSent */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.unsetSent} */
/**
 * Clears the `sent` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.unsetSent = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IRegionPingController.__pulsePing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__pulsePing<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._pulsePing */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.pulsePing} */
/** @return {void} */
xyz.swapee.wc.front.IRegionPingController.pulsePing = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.front.IRegionPingController.__setStart
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__setStart<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._setStart */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.setStart} */
/**
 * Sets the `start` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.setStart = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IRegionPingController.__unsetStart
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__unsetStart<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._unsetStart */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.unsetStart} */
/**
 * Clears the `start` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.unsetStart = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.front.IRegionPingController.__setReceived
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__setReceived<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._setReceived */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.setReceived} */
/**
 * Sets the `received` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.setReceived = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IRegionPingController.__unsetReceived
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__unsetReceived<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._unsetReceived */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.unsetReceived} */
/**
 * Clears the `received` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.unsetReceived = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.front.IRegionPingController.__setDiff
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__setDiff<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._setDiff */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.setDiff} */
/**
 * Sets the `diff` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.setDiff = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IRegionPingController.__unsetDiff
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__unsetDiff<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._unsetDiff */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.unsetDiff} */
/**
 * Clears the `diff` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.unsetDiff = function() {}

/**
 * @typedef {(this: THIS, val: number) => void} xyz.swapee.wc.front.IRegionPingController.__setDiff2
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__setDiff2<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._setDiff2 */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.setDiff2} */
/**
 * Sets the `diff2` in the core via port.
 * @param {number} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.setDiff2 = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IRegionPingController.__unsetDiff2
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__unsetDiff2<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._unsetDiff2 */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.unsetDiff2} */
/**
 * Clears the `diff2` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.unsetDiff2 = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IRegionPingController.__loadPing
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IRegionPingController.__loadPing<!xyz.swapee.wc.front.IRegionPingController>} xyz.swapee.wc.front.IRegionPingController._loadPing */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingController.loadPing} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.front.IRegionPingController.loadPing = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IRegionPingController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/52-IRegionPingControllerBack.xml}  db2902aa2e906221b67fd6af948406fb */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IRegionPingController.Inputs>&xyz.swapee.wc.IRegionPingController.Initialese} xyz.swapee.wc.back.IRegionPingController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.RegionPingController)} xyz.swapee.wc.back.AbstractRegionPingController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.RegionPingController} xyz.swapee.wc.back.RegionPingController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IRegionPingController` interface.
 * @constructor xyz.swapee.wc.back.AbstractRegionPingController
 */
xyz.swapee.wc.back.AbstractRegionPingController = class extends /** @type {xyz.swapee.wc.back.AbstractRegionPingController.constructor&xyz.swapee.wc.back.RegionPingController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractRegionPingController.prototype.constructor = xyz.swapee.wc.back.AbstractRegionPingController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractRegionPingController.class = /** @type {typeof xyz.swapee.wc.back.AbstractRegionPingController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IRegionPingController|typeof xyz.swapee.wc.back.RegionPingController)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractRegionPingController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractRegionPingController}
 */
xyz.swapee.wc.back.AbstractRegionPingController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingController}
 */
xyz.swapee.wc.back.AbstractRegionPingController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IRegionPingController|typeof xyz.swapee.wc.back.RegionPingController)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingController}
 */
xyz.swapee.wc.back.AbstractRegionPingController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IRegionPingController|typeof xyz.swapee.wc.back.RegionPingController)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingController}
 */
xyz.swapee.wc.back.AbstractRegionPingController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IRegionPingController.Initialese[]) => xyz.swapee.wc.back.IRegionPingController} xyz.swapee.wc.back.RegionPingControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IRegionPingControllerCaster&xyz.swapee.wc.IRegionPingController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IRegionPingController.Inputs>)} xyz.swapee.wc.back.IRegionPingController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IRegionPingController
 */
xyz.swapee.wc.back.IRegionPingController = class extends /** @type {xyz.swapee.wc.back.IRegionPingController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IRegionPingController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IRegionPingController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IRegionPingController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingController.Initialese>)} xyz.swapee.wc.back.RegionPingController.constructor */
/**
 * A concrete class of _IRegionPingController_ instances.
 * @constructor xyz.swapee.wc.back.RegionPingController
 * @implements {xyz.swapee.wc.back.IRegionPingController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingController.Initialese>} ‎
 */
xyz.swapee.wc.back.RegionPingController = class extends /** @type {xyz.swapee.wc.back.RegionPingController.constructor&xyz.swapee.wc.back.IRegionPingController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IRegionPingController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.RegionPingController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.RegionPingController}
 */
xyz.swapee.wc.back.RegionPingController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IRegionPingController} */
xyz.swapee.wc.back.RecordIRegionPingController

/** @typedef {xyz.swapee.wc.back.IRegionPingController} xyz.swapee.wc.back.BoundIRegionPingController */

/** @typedef {xyz.swapee.wc.back.RegionPingController} xyz.swapee.wc.back.BoundRegionPingController */

/**
 * Contains getters to cast the _IRegionPingController_ interface.
 * @interface xyz.swapee.wc.back.IRegionPingControllerCaster
 */
xyz.swapee.wc.back.IRegionPingControllerCaster = class { }
/**
 * Cast the _IRegionPingController_ instance into the _BoundIRegionPingController_ type.
 * @type {!xyz.swapee.wc.back.BoundIRegionPingController}
 */
xyz.swapee.wc.back.IRegionPingControllerCaster.prototype.asIRegionPingController
/**
 * Access the _RegionPingController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundRegionPingController}
 */
xyz.swapee.wc.back.IRegionPingControllerCaster.prototype.superRegionPingController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/53-IRegionPingControllerAR.xml}  c6092b3b0163f2016ea7be6daea7f69d */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IRegionPingController.Initialese} xyz.swapee.wc.back.IRegionPingControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.RegionPingControllerAR)} xyz.swapee.wc.back.AbstractRegionPingControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.RegionPingControllerAR} xyz.swapee.wc.back.RegionPingControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IRegionPingControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractRegionPingControllerAR
 */
xyz.swapee.wc.back.AbstractRegionPingControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractRegionPingControllerAR.constructor&xyz.swapee.wc.back.RegionPingControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractRegionPingControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractRegionPingControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractRegionPingControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractRegionPingControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IRegionPingControllerAR|typeof xyz.swapee.wc.back.RegionPingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractRegionPingControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractRegionPingControllerAR}
 */
xyz.swapee.wc.back.AbstractRegionPingControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingControllerAR}
 */
xyz.swapee.wc.back.AbstractRegionPingControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IRegionPingControllerAR|typeof xyz.swapee.wc.back.RegionPingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingControllerAR}
 */
xyz.swapee.wc.back.AbstractRegionPingControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IRegionPingControllerAR|typeof xyz.swapee.wc.back.RegionPingControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IRegionPingController|typeof xyz.swapee.wc.RegionPingController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingControllerAR}
 */
xyz.swapee.wc.back.AbstractRegionPingControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IRegionPingControllerAR.Initialese[]) => xyz.swapee.wc.back.IRegionPingControllerAR} xyz.swapee.wc.back.RegionPingControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IRegionPingControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IRegionPingController)} xyz.swapee.wc.back.IRegionPingControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IRegionPingControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IRegionPingControllerAR
 */
xyz.swapee.wc.back.IRegionPingControllerAR = class extends /** @type {xyz.swapee.wc.back.IRegionPingControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IRegionPingController.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IRegionPingControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IRegionPingControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingControllerAR.Initialese>)} xyz.swapee.wc.back.RegionPingControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IRegionPingControllerAR} xyz.swapee.wc.back.IRegionPingControllerAR.typeof */
/**
 * A concrete class of _IRegionPingControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.RegionPingControllerAR
 * @implements {xyz.swapee.wc.back.IRegionPingControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IRegionPingControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.RegionPingControllerAR = class extends /** @type {xyz.swapee.wc.back.RegionPingControllerAR.constructor&xyz.swapee.wc.back.IRegionPingControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IRegionPingControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.RegionPingControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.RegionPingControllerAR}
 */
xyz.swapee.wc.back.RegionPingControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IRegionPingControllerAR} */
xyz.swapee.wc.back.RecordIRegionPingControllerAR

/** @typedef {xyz.swapee.wc.back.IRegionPingControllerAR} xyz.swapee.wc.back.BoundIRegionPingControllerAR */

/** @typedef {xyz.swapee.wc.back.RegionPingControllerAR} xyz.swapee.wc.back.BoundRegionPingControllerAR */

/**
 * Contains getters to cast the _IRegionPingControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IRegionPingControllerARCaster
 */
xyz.swapee.wc.back.IRegionPingControllerARCaster = class { }
/**
 * Cast the _IRegionPingControllerAR_ instance into the _BoundIRegionPingControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIRegionPingControllerAR}
 */
xyz.swapee.wc.back.IRegionPingControllerARCaster.prototype.asIRegionPingControllerAR
/**
 * Access the _RegionPingControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundRegionPingControllerAR}
 */
xyz.swapee.wc.back.IRegionPingControllerARCaster.prototype.superRegionPingControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/54-IRegionPingControllerAT.xml}  14191337a51ceaaced12b75c2f9b467b */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IRegionPingControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.RegionPingControllerAT)} xyz.swapee.wc.front.AbstractRegionPingControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.RegionPingControllerAT} xyz.swapee.wc.front.RegionPingControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IRegionPingControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractRegionPingControllerAT
 */
xyz.swapee.wc.front.AbstractRegionPingControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractRegionPingControllerAT.constructor&xyz.swapee.wc.front.RegionPingControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractRegionPingControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractRegionPingControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractRegionPingControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractRegionPingControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IRegionPingControllerAT|typeof xyz.swapee.wc.front.RegionPingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractRegionPingControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractRegionPingControllerAT}
 */
xyz.swapee.wc.front.AbstractRegionPingControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingControllerAT}
 */
xyz.swapee.wc.front.AbstractRegionPingControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IRegionPingControllerAT|typeof xyz.swapee.wc.front.RegionPingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingControllerAT}
 */
xyz.swapee.wc.front.AbstractRegionPingControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IRegionPingControllerAT|typeof xyz.swapee.wc.front.RegionPingControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingControllerAT}
 */
xyz.swapee.wc.front.AbstractRegionPingControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IRegionPingControllerAT.Initialese[]) => xyz.swapee.wc.front.IRegionPingControllerAT} xyz.swapee.wc.front.RegionPingControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IRegionPingControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IRegionPingControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IRegionPingControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IRegionPingControllerAT
 */
xyz.swapee.wc.front.IRegionPingControllerAT = class extends /** @type {xyz.swapee.wc.front.IRegionPingControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IRegionPingControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IRegionPingControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IRegionPingControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IRegionPingControllerAT.Initialese>)} xyz.swapee.wc.front.RegionPingControllerAT.constructor */
/**
 * A concrete class of _IRegionPingControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.RegionPingControllerAT
 * @implements {xyz.swapee.wc.front.IRegionPingControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IRegionPingControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IRegionPingControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.RegionPingControllerAT = class extends /** @type {xyz.swapee.wc.front.RegionPingControllerAT.constructor&xyz.swapee.wc.front.IRegionPingControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IRegionPingControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IRegionPingControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.RegionPingControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.RegionPingControllerAT}
 */
xyz.swapee.wc.front.RegionPingControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IRegionPingControllerAT} */
xyz.swapee.wc.front.RecordIRegionPingControllerAT

/** @typedef {xyz.swapee.wc.front.IRegionPingControllerAT} xyz.swapee.wc.front.BoundIRegionPingControllerAT */

/** @typedef {xyz.swapee.wc.front.RegionPingControllerAT} xyz.swapee.wc.front.BoundRegionPingControllerAT */

/**
 * Contains getters to cast the _IRegionPingControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IRegionPingControllerATCaster
 */
xyz.swapee.wc.front.IRegionPingControllerATCaster = class { }
/**
 * Cast the _IRegionPingControllerAT_ instance into the _BoundIRegionPingControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIRegionPingControllerAT}
 */
xyz.swapee.wc.front.IRegionPingControllerATCaster.prototype.asIRegionPingControllerAT
/**
 * Access the _RegionPingControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundRegionPingControllerAT}
 */
xyz.swapee.wc.front.IRegionPingControllerATCaster.prototype.superRegionPingControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/70-IRegionPingScreen.xml}  f4de73a5678c4970a89c41fc1dbb7484 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.front.RegionPingInputs, !HTMLDivElement, !xyz.swapee.wc.IRegionPingDisplay.Settings, !xyz.swapee.wc.IRegionPingDisplay.Queries, null>&xyz.swapee.wc.IRegionPingDisplay.Initialese} xyz.swapee.wc.IRegionPingScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.RegionPingScreen)} xyz.swapee.wc.AbstractRegionPingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingScreen} xyz.swapee.wc.RegionPingScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingScreen` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingScreen
 */
xyz.swapee.wc.AbstractRegionPingScreen = class extends /** @type {xyz.swapee.wc.AbstractRegionPingScreen.constructor&xyz.swapee.wc.RegionPingScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingScreen.prototype.constructor = xyz.swapee.wc.AbstractRegionPingScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingScreen.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingScreen|typeof xyz.swapee.wc.RegionPingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IRegionPingController|typeof xyz.swapee.wc.front.RegionPingController)|(!xyz.swapee.wc.IRegionPingDisplay|typeof xyz.swapee.wc.RegionPingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingScreen}
 */
xyz.swapee.wc.AbstractRegionPingScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingScreen}
 */
xyz.swapee.wc.AbstractRegionPingScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingScreen|typeof xyz.swapee.wc.RegionPingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IRegionPingController|typeof xyz.swapee.wc.front.RegionPingController)|(!xyz.swapee.wc.IRegionPingDisplay|typeof xyz.swapee.wc.RegionPingDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingScreen}
 */
xyz.swapee.wc.AbstractRegionPingScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingScreen|typeof xyz.swapee.wc.RegionPingScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IRegionPingController|typeof xyz.swapee.wc.front.RegionPingController)|(!xyz.swapee.wc.IRegionPingDisplay|typeof xyz.swapee.wc.RegionPingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingScreen}
 */
xyz.swapee.wc.AbstractRegionPingScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingScreen.Initialese[]) => xyz.swapee.wc.IRegionPingScreen} xyz.swapee.wc.RegionPingScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IRegionPingScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.RegionPingMemory, !xyz.swapee.wc.front.RegionPingInputs, !HTMLDivElement, !xyz.swapee.wc.IRegionPingDisplay.Settings, !xyz.swapee.wc.IRegionPingDisplay.Queries, null, null>&xyz.swapee.wc.front.IRegionPingController&xyz.swapee.wc.IRegionPingDisplay)} xyz.swapee.wc.IRegionPingScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IRegionPingScreen
 */
xyz.swapee.wc.IRegionPingScreen = class extends /** @type {xyz.swapee.wc.IRegionPingScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IRegionPingController.typeof&xyz.swapee.wc.IRegionPingDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingScreen.Initialese>)} xyz.swapee.wc.RegionPingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IRegionPingScreen} xyz.swapee.wc.IRegionPingScreen.typeof */
/**
 * A concrete class of _IRegionPingScreen_ instances.
 * @constructor xyz.swapee.wc.RegionPingScreen
 * @implements {xyz.swapee.wc.IRegionPingScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingScreen.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingScreen = class extends /** @type {xyz.swapee.wc.RegionPingScreen.constructor&xyz.swapee.wc.IRegionPingScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingScreen}
 */
xyz.swapee.wc.RegionPingScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IRegionPingScreen} */
xyz.swapee.wc.RecordIRegionPingScreen

/** @typedef {xyz.swapee.wc.IRegionPingScreen} xyz.swapee.wc.BoundIRegionPingScreen */

/** @typedef {xyz.swapee.wc.RegionPingScreen} xyz.swapee.wc.BoundRegionPingScreen */

/**
 * Contains getters to cast the _IRegionPingScreen_ interface.
 * @interface xyz.swapee.wc.IRegionPingScreenCaster
 */
xyz.swapee.wc.IRegionPingScreenCaster = class { }
/**
 * Cast the _IRegionPingScreen_ instance into the _BoundIRegionPingScreen_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingScreen}
 */
xyz.swapee.wc.IRegionPingScreenCaster.prototype.asIRegionPingScreen
/**
 * Access the _RegionPingScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingScreen}
 */
xyz.swapee.wc.IRegionPingScreenCaster.prototype.superRegionPingScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/70-IRegionPingScreenBack.xml}  612d5f704aa2baa7dea8e3206116554d */
/** @typedef {xyz.swapee.wc.back.IRegionPingScreenAT.Initialese} xyz.swapee.wc.back.IRegionPingScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.RegionPingScreen)} xyz.swapee.wc.back.AbstractRegionPingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.RegionPingScreen} xyz.swapee.wc.back.RegionPingScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IRegionPingScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractRegionPingScreen
 */
xyz.swapee.wc.back.AbstractRegionPingScreen = class extends /** @type {xyz.swapee.wc.back.AbstractRegionPingScreen.constructor&xyz.swapee.wc.back.RegionPingScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractRegionPingScreen.prototype.constructor = xyz.swapee.wc.back.AbstractRegionPingScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractRegionPingScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractRegionPingScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IRegionPingScreen|typeof xyz.swapee.wc.back.RegionPingScreen)|(!xyz.swapee.wc.back.IRegionPingScreenAT|typeof xyz.swapee.wc.back.RegionPingScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractRegionPingScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractRegionPingScreen}
 */
xyz.swapee.wc.back.AbstractRegionPingScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreen}
 */
xyz.swapee.wc.back.AbstractRegionPingScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IRegionPingScreen|typeof xyz.swapee.wc.back.RegionPingScreen)|(!xyz.swapee.wc.back.IRegionPingScreenAT|typeof xyz.swapee.wc.back.RegionPingScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreen}
 */
xyz.swapee.wc.back.AbstractRegionPingScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IRegionPingScreen|typeof xyz.swapee.wc.back.RegionPingScreen)|(!xyz.swapee.wc.back.IRegionPingScreenAT|typeof xyz.swapee.wc.back.RegionPingScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreen}
 */
xyz.swapee.wc.back.AbstractRegionPingScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IRegionPingScreen.Initialese[]) => xyz.swapee.wc.back.IRegionPingScreen} xyz.swapee.wc.back.RegionPingScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IRegionPingScreenCaster&xyz.swapee.wc.back.IRegionPingScreenAT)} xyz.swapee.wc.back.IRegionPingScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IRegionPingScreenAT} xyz.swapee.wc.back.IRegionPingScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IRegionPingScreen
 */
xyz.swapee.wc.back.IRegionPingScreen = class extends /** @type {xyz.swapee.wc.back.IRegionPingScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IRegionPingScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IRegionPingScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IRegionPingScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingScreen.Initialese>)} xyz.swapee.wc.back.RegionPingScreen.constructor */
/**
 * A concrete class of _IRegionPingScreen_ instances.
 * @constructor xyz.swapee.wc.back.RegionPingScreen
 * @implements {xyz.swapee.wc.back.IRegionPingScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.RegionPingScreen = class extends /** @type {xyz.swapee.wc.back.RegionPingScreen.constructor&xyz.swapee.wc.back.IRegionPingScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IRegionPingScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.RegionPingScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.RegionPingScreen}
 */
xyz.swapee.wc.back.RegionPingScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IRegionPingScreen} */
xyz.swapee.wc.back.RecordIRegionPingScreen

/** @typedef {xyz.swapee.wc.back.IRegionPingScreen} xyz.swapee.wc.back.BoundIRegionPingScreen */

/** @typedef {xyz.swapee.wc.back.RegionPingScreen} xyz.swapee.wc.back.BoundRegionPingScreen */

/**
 * Contains getters to cast the _IRegionPingScreen_ interface.
 * @interface xyz.swapee.wc.back.IRegionPingScreenCaster
 */
xyz.swapee.wc.back.IRegionPingScreenCaster = class { }
/**
 * Cast the _IRegionPingScreen_ instance into the _BoundIRegionPingScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIRegionPingScreen}
 */
xyz.swapee.wc.back.IRegionPingScreenCaster.prototype.asIRegionPingScreen
/**
 * Access the _RegionPingScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundRegionPingScreen}
 */
xyz.swapee.wc.back.IRegionPingScreenCaster.prototype.superRegionPingScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/73-IRegionPingScreenAR.xml}  739cbba8d717a89b305268888c972aea */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IRegionPingScreen.Initialese} xyz.swapee.wc.front.IRegionPingScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.RegionPingScreenAR)} xyz.swapee.wc.front.AbstractRegionPingScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.RegionPingScreenAR} xyz.swapee.wc.front.RegionPingScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IRegionPingScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractRegionPingScreenAR
 */
xyz.swapee.wc.front.AbstractRegionPingScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractRegionPingScreenAR.constructor&xyz.swapee.wc.front.RegionPingScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractRegionPingScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractRegionPingScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractRegionPingScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractRegionPingScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IRegionPingScreenAR|typeof xyz.swapee.wc.front.RegionPingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IRegionPingScreen|typeof xyz.swapee.wc.RegionPingScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractRegionPingScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractRegionPingScreenAR}
 */
xyz.swapee.wc.front.AbstractRegionPingScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingScreenAR}
 */
xyz.swapee.wc.front.AbstractRegionPingScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IRegionPingScreenAR|typeof xyz.swapee.wc.front.RegionPingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IRegionPingScreen|typeof xyz.swapee.wc.RegionPingScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingScreenAR}
 */
xyz.swapee.wc.front.AbstractRegionPingScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IRegionPingScreenAR|typeof xyz.swapee.wc.front.RegionPingScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IRegionPingScreen|typeof xyz.swapee.wc.RegionPingScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.RegionPingScreenAR}
 */
xyz.swapee.wc.front.AbstractRegionPingScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IRegionPingScreenAR.Initialese[]) => xyz.swapee.wc.front.IRegionPingScreenAR} xyz.swapee.wc.front.RegionPingScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IRegionPingScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IRegionPingScreen)} xyz.swapee.wc.front.IRegionPingScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IRegionPingScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IRegionPingScreenAR
 */
xyz.swapee.wc.front.IRegionPingScreenAR = class extends /** @type {xyz.swapee.wc.front.IRegionPingScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IRegionPingScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IRegionPingScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IRegionPingScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IRegionPingScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IRegionPingScreenAR.Initialese>)} xyz.swapee.wc.front.RegionPingScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IRegionPingScreenAR} xyz.swapee.wc.front.IRegionPingScreenAR.typeof */
/**
 * A concrete class of _IRegionPingScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.RegionPingScreenAR
 * @implements {xyz.swapee.wc.front.IRegionPingScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IRegionPingScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IRegionPingScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.RegionPingScreenAR = class extends /** @type {xyz.swapee.wc.front.RegionPingScreenAR.constructor&xyz.swapee.wc.front.IRegionPingScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IRegionPingScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IRegionPingScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.RegionPingScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.RegionPingScreenAR}
 */
xyz.swapee.wc.front.RegionPingScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IRegionPingScreenAR} */
xyz.swapee.wc.front.RecordIRegionPingScreenAR

/** @typedef {xyz.swapee.wc.front.IRegionPingScreenAR} xyz.swapee.wc.front.BoundIRegionPingScreenAR */

/** @typedef {xyz.swapee.wc.front.RegionPingScreenAR} xyz.swapee.wc.front.BoundRegionPingScreenAR */

/**
 * Contains getters to cast the _IRegionPingScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IRegionPingScreenARCaster
 */
xyz.swapee.wc.front.IRegionPingScreenARCaster = class { }
/**
 * Cast the _IRegionPingScreenAR_ instance into the _BoundIRegionPingScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIRegionPingScreenAR}
 */
xyz.swapee.wc.front.IRegionPingScreenARCaster.prototype.asIRegionPingScreenAR
/**
 * Access the _RegionPingScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundRegionPingScreenAR}
 */
xyz.swapee.wc.front.IRegionPingScreenARCaster.prototype.superRegionPingScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/74-IRegionPingScreenAT.xml}  64210fd39eec2eeded7b76ae8685478f */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IRegionPingScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.RegionPingScreenAT)} xyz.swapee.wc.back.AbstractRegionPingScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.RegionPingScreenAT} xyz.swapee.wc.back.RegionPingScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IRegionPingScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractRegionPingScreenAT
 */
xyz.swapee.wc.back.AbstractRegionPingScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractRegionPingScreenAT.constructor&xyz.swapee.wc.back.RegionPingScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractRegionPingScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractRegionPingScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractRegionPingScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractRegionPingScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IRegionPingScreenAT|typeof xyz.swapee.wc.back.RegionPingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractRegionPingScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractRegionPingScreenAT}
 */
xyz.swapee.wc.back.AbstractRegionPingScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreenAT}
 */
xyz.swapee.wc.back.AbstractRegionPingScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IRegionPingScreenAT|typeof xyz.swapee.wc.back.RegionPingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreenAT}
 */
xyz.swapee.wc.back.AbstractRegionPingScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IRegionPingScreenAT|typeof xyz.swapee.wc.back.RegionPingScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.RegionPingScreenAT}
 */
xyz.swapee.wc.back.AbstractRegionPingScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IRegionPingScreenAT.Initialese[]) => xyz.swapee.wc.back.IRegionPingScreenAT} xyz.swapee.wc.back.RegionPingScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IRegionPingScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IRegionPingScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IRegionPingScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IRegionPingScreenAT
 */
xyz.swapee.wc.back.IRegionPingScreenAT = class extends /** @type {xyz.swapee.wc.back.IRegionPingScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IRegionPingScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IRegionPingScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingScreenAT.Initialese>)} xyz.swapee.wc.back.RegionPingScreenAT.constructor */
/**
 * A concrete class of _IRegionPingScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.RegionPingScreenAT
 * @implements {xyz.swapee.wc.back.IRegionPingScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IRegionPingScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IRegionPingScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.RegionPingScreenAT = class extends /** @type {xyz.swapee.wc.back.RegionPingScreenAT.constructor&xyz.swapee.wc.back.IRegionPingScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IRegionPingScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IRegionPingScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.RegionPingScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.RegionPingScreenAT}
 */
xyz.swapee.wc.back.RegionPingScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IRegionPingScreenAT} */
xyz.swapee.wc.back.RecordIRegionPingScreenAT

/** @typedef {xyz.swapee.wc.back.IRegionPingScreenAT} xyz.swapee.wc.back.BoundIRegionPingScreenAT */

/** @typedef {xyz.swapee.wc.back.RegionPingScreenAT} xyz.swapee.wc.back.BoundRegionPingScreenAT */

/**
 * Contains getters to cast the _IRegionPingScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IRegionPingScreenATCaster
 */
xyz.swapee.wc.back.IRegionPingScreenATCaster = class { }
/**
 * Cast the _IRegionPingScreenAT_ instance into the _BoundIRegionPingScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIRegionPingScreenAT}
 */
xyz.swapee.wc.back.IRegionPingScreenATCaster.prototype.asIRegionPingScreenAT
/**
 * Access the _RegionPingScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundRegionPingScreenAT}
 */
xyz.swapee.wc.back.IRegionPingScreenATCaster.prototype.superRegionPingScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/region-ping/RegionPing.mvc/design/80-IRegionPingGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IRegionPingDisplay.Initialese} xyz.swapee.wc.IRegionPingGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.RegionPingGPU)} xyz.swapee.wc.AbstractRegionPingGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.RegionPingGPU} xyz.swapee.wc.RegionPingGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingGPU` interface.
 * @constructor xyz.swapee.wc.AbstractRegionPingGPU
 */
xyz.swapee.wc.AbstractRegionPingGPU = class extends /** @type {xyz.swapee.wc.AbstractRegionPingGPU.constructor&xyz.swapee.wc.RegionPingGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractRegionPingGPU.prototype.constructor = xyz.swapee.wc.AbstractRegionPingGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractRegionPingGPU.class = /** @type {typeof xyz.swapee.wc.AbstractRegionPingGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IRegionPingGPU|typeof xyz.swapee.wc.RegionPingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IRegionPingDisplay|typeof xyz.swapee.wc.back.RegionPingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractRegionPingGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractRegionPingGPU}
 */
xyz.swapee.wc.AbstractRegionPingGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingGPU}
 */
xyz.swapee.wc.AbstractRegionPingGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IRegionPingGPU|typeof xyz.swapee.wc.RegionPingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IRegionPingDisplay|typeof xyz.swapee.wc.back.RegionPingDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingGPU}
 */
xyz.swapee.wc.AbstractRegionPingGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IRegionPingGPU|typeof xyz.swapee.wc.RegionPingGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IRegionPingDisplay|typeof xyz.swapee.wc.back.RegionPingDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.RegionPingGPU}
 */
xyz.swapee.wc.AbstractRegionPingGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IRegionPingGPU.Initialese[]) => xyz.swapee.wc.IRegionPingGPU} xyz.swapee.wc.RegionPingGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IRegionPingGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IRegionPingGPUCaster&com.webcircuits.IBrowserView<.!RegionPingMemory,>&xyz.swapee.wc.back.IRegionPingDisplay)} xyz.swapee.wc.IRegionPingGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!RegionPingMemory,>} com.webcircuits.IBrowserView<.!RegionPingMemory,>.typeof */
/**
 * Handles the periphery of the _IRegionPingDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IRegionPingGPU
 */
xyz.swapee.wc.IRegionPingGPU = class extends /** @type {xyz.swapee.wc.IRegionPingGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!RegionPingMemory,>.typeof&xyz.swapee.wc.back.IRegionPingDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IRegionPingGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IRegionPingGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IRegionPingGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingGPU.Initialese>)} xyz.swapee.wc.RegionPingGPU.constructor */
/**
 * A concrete class of _IRegionPingGPU_ instances.
 * @constructor xyz.swapee.wc.RegionPingGPU
 * @implements {xyz.swapee.wc.IRegionPingGPU} Handles the periphery of the _IRegionPingDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IRegionPingGPU.Initialese>} ‎
 */
xyz.swapee.wc.RegionPingGPU = class extends /** @type {xyz.swapee.wc.RegionPingGPU.constructor&xyz.swapee.wc.IRegionPingGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IRegionPingGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IRegionPingGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IRegionPingGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IRegionPingGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.RegionPingGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.RegionPingGPU}
 */
xyz.swapee.wc.RegionPingGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IRegionPingGPU.
 * @interface xyz.swapee.wc.IRegionPingGPUFields
 */
xyz.swapee.wc.IRegionPingGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IRegionPingGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IRegionPingGPU} */
xyz.swapee.wc.RecordIRegionPingGPU

/** @typedef {xyz.swapee.wc.IRegionPingGPU} xyz.swapee.wc.BoundIRegionPingGPU */

/** @typedef {xyz.swapee.wc.RegionPingGPU} xyz.swapee.wc.BoundRegionPingGPU */

/**
 * Contains getters to cast the _IRegionPingGPU_ interface.
 * @interface xyz.swapee.wc.IRegionPingGPUCaster
 */
xyz.swapee.wc.IRegionPingGPUCaster = class { }
/**
 * Cast the _IRegionPingGPU_ instance into the _BoundIRegionPingGPU_ type.
 * @type {!xyz.swapee.wc.BoundIRegionPingGPU}
 */
xyz.swapee.wc.IRegionPingGPUCaster.prototype.asIRegionPingGPU
/**
 * Access the _RegionPingGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundRegionPingGPU}
 */
xyz.swapee.wc.IRegionPingGPUCaster.prototype.superRegionPingGPU

// nss:xyz.swapee.wc
/* @typal-end */