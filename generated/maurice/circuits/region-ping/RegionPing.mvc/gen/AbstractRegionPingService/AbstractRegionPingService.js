import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingService}
 */
function __AbstractRegionPingService() {}
__AbstractRegionPingService.prototype = /** @type {!_AbstractRegionPingService} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingService}
 */
class _AbstractRegionPingService { }
/**
 * A service for the IRegionPing.
 * @extends {xyz.swapee.wc.AbstractRegionPingService} ‎
 */
class AbstractRegionPingService extends newAbstract(
 _AbstractRegionPingService,163122484517,null,{
  asIRegionPingService:1,
  superRegionPingService:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingService} */
AbstractRegionPingService.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingService} */
function AbstractRegionPingServiceClass(){}

export default AbstractRegionPingService


AbstractRegionPingService[$implementations]=[
 __AbstractRegionPingService,
]

export {AbstractRegionPingService}