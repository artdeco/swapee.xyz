import AbstractRegionPingGPU from '../AbstractRegionPingGPU'
import AbstractRegionPingScreenBack from '../AbstractRegionPingScreenBack'
import AbstractRegionPingRadio from '../AbstractRegionPingRadio'
import {HtmlComponent,makeRevealConcealPaints,mvc} from '@webcircuits/webcircuits'
import {access} from '@mauriceguest/guest2'
import {RegionPingInputsQPs} from '../../pqs/RegionPingInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractRegionPing from '../AbstractRegionPing'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingHtmlComponent}
 */
function __AbstractRegionPingHtmlComponent() {}
__AbstractRegionPingHtmlComponent.prototype = /** @type {!_AbstractRegionPingHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingHtmlComponent}
 */
class _AbstractRegionPingHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.RegionPingHtmlComponent} */ (res)
  }
}
/**
 * The _IRegionPing_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractRegionPingHtmlComponent} ‎
 */
export class AbstractRegionPingHtmlComponent extends newAbstract(
 _AbstractRegionPingHtmlComponent,163122484512,null,{
  asIRegionPingHtmlComponent:1,
  superRegionPingHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingHtmlComponent} */
AbstractRegionPingHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingHtmlComponent} */
function AbstractRegionPingHtmlComponentClass(){}


AbstractRegionPingHtmlComponent[$implementations]=[
 __AbstractRegionPingHtmlComponent,
 HtmlComponent,
 AbstractRegionPing,
 AbstractRegionPingGPU,
 AbstractRegionPingScreenBack,
 AbstractRegionPingRadio,
 AbstractRegionPingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingHtmlComponent}*/({
  inputsQPs:RegionPingInputsQPs,
 }),

/** @type {!AbstractRegionPingHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IRegionPingHtmlComponent}*/function paintRegionLa() {
     this.RegionLa.setText(this.model.region)
   },/**@this {!xyz.swapee.wc.IRegionPingHtmlComponent}*/function paintServerRegionLa() {
     this.ServerRegionLa.setText(this.model.serverRegion)
   },/**@this {!xyz.swapee.wc.IRegionPingHtmlComponent}*/function paintRegionPingLa() {
     this.RegionPingLa.setText(this.model.diff)
   },/**@this {!xyz.swapee.wc.IRegionPingHtmlComponent}*/function paintRegionPingLa2() {
     this.RegionPingLa2.setText(this.model.diff2)
   }
 ] }),
 AbstractRegionPingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingHtmlComponent}*/({
  paint:function $reveal_ifNot_$conceal_RegionWr({serverRegion:serverRegion,region:region}){
   const{
    asIRegionPingGPU:{RegionWr:RegionWr},
    asIBrowserView:{conceal:conceal,reveal:reveal},
   }=this
   const state={serverRegion:serverRegion,region:region}
   ;({serverRegion:serverRegion,region:region}=state)
   if(serverRegion) conceal(RegionWr,true)
   else reveal(RegionWr,region)
  },
 }),
 AbstractRegionPingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingHtmlComponent}*/({
  paint:makeRevealConcealPaints({
   LoWr:{loadingPing:1},
   ServerRegionWr:{serverRegion:1},
   PingWr:{diff2:1},
  }),
 }),
 AbstractRegionPingHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingHtmlComponent}*/({
  paint({start:start}){
   const{asIRegionPingController:{
    unsetReceived:unsetReceived,
   }}=this
   if(start) {
    unsetReceived()
   }
  },
 }),
]