import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingDisplay}
 */
function __AbstractRegionPingDisplay() {}
__AbstractRegionPingDisplay.prototype = /** @type {!_AbstractRegionPingDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionPingDisplay}
 */
class _AbstractRegionPingDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractRegionPingDisplay} ‎
 */
class AbstractRegionPingDisplay extends newAbstract(
 _AbstractRegionPingDisplay,163122484521,null,{
  asIRegionPingDisplay:1,
  superRegionPingDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingDisplay} */
AbstractRegionPingDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingDisplay} */
function AbstractRegionPingDisplayClass(){}

export default AbstractRegionPingDisplay


AbstractRegionPingDisplay[$implementations]=[
 __AbstractRegionPingDisplay,
 GraphicsDriverBack,
 AbstractRegionPingDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IRegionPingDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IRegionPingDisplay}*/({
    LoWr:twinMock,
    RegionWr:twinMock,
    RegionLa:twinMock,
    ServerRegionWr:twinMock,
    ServerRegionLa:twinMock,
    PingWr:twinMock,
    RegionPingLa:twinMock,
    RegionPingLa2:twinMock,
    RegionFlagLa:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.RegionPingDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IRegionPingDisplay.Initialese}*/({
   RegionFlagLa:1,
   LoWr:1,
   PingWr:1,
   RegionPingLa:1,
   RegionPingLa2:1,
   RegionLa:1,
   RegionWr:1,
   ServerRegionLa:1,
   ServerRegionWr:1,
  }),
  initializer({
   RegionFlagLa:_RegionFlagLa,
   LoWr:_LoWr,
   PingWr:_PingWr,
   RegionPingLa:_RegionPingLa,
   RegionPingLa2:_RegionPingLa2,
   RegionLa:_RegionLa,
   RegionWr:_RegionWr,
   ServerRegionLa:_ServerRegionLa,
   ServerRegionWr:_ServerRegionWr,
  }) {
   if(_RegionFlagLa!==undefined) this.RegionFlagLa=_RegionFlagLa
   if(_LoWr!==undefined) this.LoWr=_LoWr
   if(_PingWr!==undefined) this.PingWr=_PingWr
   if(_RegionPingLa!==undefined) this.RegionPingLa=_RegionPingLa
   if(_RegionPingLa2!==undefined) this.RegionPingLa2=_RegionPingLa2
   if(_RegionLa!==undefined) this.RegionLa=_RegionLa
   if(_RegionWr!==undefined) this.RegionWr=_RegionWr
   if(_ServerRegionLa!==undefined) this.ServerRegionLa=_ServerRegionLa
   if(_ServerRegionWr!==undefined) this.ServerRegionWr=_ServerRegionWr
  },
 }),
]