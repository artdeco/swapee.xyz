import AbstractRegionPingProcessor from '../AbstractRegionPingProcessor'
import {RegionPingCore} from '../RegionPingCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractRegionPingComputer} from '../AbstractRegionPingComputer'
import {AbstractRegionPingController} from '../AbstractRegionPingController'
import {regulateRegionPingCache} from './methods/regulateRegionPingCache'
import {RegionPingCacheQPs} from '../../pqs/RegionPingCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPing}
 */
function __AbstractRegionPing() {}
__AbstractRegionPing.prototype = /** @type {!_AbstractRegionPing} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPing}
 */
class _AbstractRegionPing { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractRegionPing} ‎
 */
class AbstractRegionPing extends newAbstract(
 _AbstractRegionPing,163122484510,null,{
  asIRegionPing:1,
  superRegionPing:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPing} */
AbstractRegionPing.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPing} */
function AbstractRegionPingClass(){}

export default AbstractRegionPing


AbstractRegionPing[$implementations]=[
 __AbstractRegionPing,
 RegionPingCore,
 AbstractRegionPingProcessor,
 IntegratedComponent,
 AbstractRegionPingComputer,
 AbstractRegionPingController,
 AbstractRegionPingClass.prototype=/**@type {!xyz.swapee.wc.IRegionPing}*/({
  regulateState:regulateRegionPingCache,
  stateQPs:RegionPingCacheQPs,
 }),
]


export {AbstractRegionPing}