import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateRegionPingCache=makeBuffers({
 loadingPing:Boolean,
 hasMorePing:Boolean,
 loadPingError:5,
},{silent:true})