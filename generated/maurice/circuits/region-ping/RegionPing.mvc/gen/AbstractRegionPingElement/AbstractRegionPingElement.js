import RegionPingRenderVdus from './methods/render-vdus'
import RegionPingElementPort from '../RegionPingElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {RegionPingInputsPQs} from '../../pqs/RegionPingInputsPQs'
import {RegionPingCachePQs} from '../../pqs/RegionPingCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractRegionPing from '../AbstractRegionPing'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingElement}
 */
function __AbstractRegionPingElement() {}
__AbstractRegionPingElement.prototype = /** @type {!_AbstractRegionPingElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingElement}
 */
class _AbstractRegionPingElement { }
/**
 * A component description.
 *
 * The _IRegionPing_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractRegionPingElement} ‎
 */
class AbstractRegionPingElement extends newAbstract(
 _AbstractRegionPingElement,163122484513,null,{
  asIRegionPingElement:1,
  superRegionPingElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingElement} */
AbstractRegionPingElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingElement} */
function AbstractRegionPingElementClass(){}

export default AbstractRegionPingElement


AbstractRegionPingElement[$implementations]=[
 __AbstractRegionPingElement,
 ElementBase,
 AbstractRegionPingElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':host':hostColAttr,
   ':region':regionColAttr,
   ':server-region':serverRegionColAttr,
   ':start':startColAttr,
   ':received':receivedColAttr,
   ':diff':diffColAttr,
   ':diff2':diff2ColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'host':hostAttr,
    'region':regionAttr,
    'server-region':serverRegionAttr,
    'start':startAttr,
    'received':receivedAttr,
    'diff':diffAttr,
    'diff2':diff2Attr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(hostAttr===undefined?{'host':hostColAttr}:{}),
    ...(regionAttr===undefined?{'region':regionColAttr}:{}),
    ...(serverRegionAttr===undefined?{'server-region':serverRegionColAttr}:{}),
    ...(startAttr===undefined?{'start':startColAttr}:{}),
    ...(receivedAttr===undefined?{'received':receivedColAttr}:{}),
    ...(diffAttr===undefined?{'diff':diffColAttr}:{}),
    ...(diff2Attr===undefined?{'diff2':diff2ColAttr}:{}),
   }
  },
 }),
 AbstractRegionPingElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'host':hostAttr,
   'region':regionAttr,
   'server-region':serverRegionAttr,
   'start':startAttr,
   'received':receivedAttr,
   'diff':diffAttr,
   'diff2':diff2Attr,
  })=>{
   return{
    noSolder:noSolderAttr,
    host:hostAttr,
    region:regionAttr,
    serverRegion:serverRegionAttr,
    start:startAttr,
    received:receivedAttr,
    diff:diffAttr,
    diff2:diff2Attr,
   }
  },
 }),
 AbstractRegionPingElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractRegionPingElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingElement}*/({
  render:RegionPingRenderVdus,
 }),
 AbstractRegionPingElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingElement}*/({
  classes:{
   'Reg': '3791f',
  },
  inputsPQs:RegionPingInputsPQs,
  cachePQs:RegionPingCachePQs,
  vdus:{
   'RegionFlagLa': 'g7372',
   'RegionLa': 'g7373',
   'LoWr': 'g7375',
   'RegionWr': 'g7376',
   'RegionPingLa': 'g7377',
   'RegionPingLa2': 'g7378',
   'PingWr': 'g7379',
   'ServerRegionWr': 'g73710',
   'ServerRegionLa': 'g73711',
  },
 }),
 IntegratedComponent,
 AbstractRegionPingElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','host','region','serverRegion','start','received','diff','diff2','no-solder',':no-solder',':host',':region','server-region',':server-region',':start',':received',':diff',':diff2','fe646','83a1c','16fec','00d47','66bf6','4553d','787e3','5886b','a649c','b5aad','67b3d','960db','40a20','ea2b2','c5946','2d2de','e559c','children']),
   })
  },
  get Port(){
   return RegionPingElementPort
  },
 }),
]



AbstractRegionPingElement[$implementations]=[AbstractRegionPing,
 /** @type {!AbstractRegionPingElement} */ ({
  rootId:'RegionPing',
  __$id:1631224845,
  fqn:'xyz.swapee.wc.IRegionPing',
  maurice_element_v3:true,
 }),
]