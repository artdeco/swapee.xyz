
import AbstractRegionPing from '../AbstractRegionPing'

/** @abstract {xyz.swapee.wc.IRegionPingElement} */
export default class AbstractRegionPingElement { }



AbstractRegionPingElement[$implementations]=[AbstractRegionPing,
 /** @type {!AbstractRegionPingElement} */ ({
  rootId:'RegionPing',
  __$id:1631224845,
  fqn:'xyz.swapee.wc.IRegionPing',
  maurice_element_v3:true,
 }),
]