export default function RegionPingRenderVdus(){
 return (<div $id="RegionPing">
  <vdu $id="LoWr" />
  <vdu $id="RegionWr" />
  <vdu $id="RegionLa" />
  <vdu $id="ServerRegionWr" />
  <vdu $id="ServerRegionLa" />
  <vdu $id="PingWr" />
  <vdu $id="RegionPingLa" />
  <vdu $id="RegionPingLa2" />
  <vdu $id="RegionFlagLa" />
 </div>)
}