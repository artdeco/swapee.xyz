import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingDisplay}
 */
function __AbstractRegionPingDisplay() {}
__AbstractRegionPingDisplay.prototype = /** @type {!_AbstractRegionPingDisplay} */ ({ })
/** @this {xyz.swapee.wc.RegionPingDisplay} */ function RegionPingDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.RegionFlagLa=null
  /** @type {HTMLSpanElement} */ this.LoWr=null
  /** @type {HTMLDivElement} */ this.PingWr=null
  /** @type {HTMLSpanElement} */ this.RegionPingLa=null
  /** @type {HTMLSpanElement} */ this.RegionPingLa2=null
  /** @type {HTMLSpanElement} */ this.RegionLa=null
  /** @type {HTMLSpanElement} */ this.RegionWr=null
  /** @type {HTMLSpanElement} */ this.ServerRegionLa=null
  /** @type {HTMLSpanElement} */ this.ServerRegionWr=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingDisplay}
 */
class _AbstractRegionPingDisplay { }
/**
 * Display for presenting information from the _IRegionPing_.
 * @extends {xyz.swapee.wc.AbstractRegionPingDisplay} ‎
 */
class AbstractRegionPingDisplay extends newAbstract(
 _AbstractRegionPingDisplay,163122484519,RegionPingDisplayConstructor,{
  asIRegionPingDisplay:1,
  superRegionPingDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingDisplay} */
AbstractRegionPingDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingDisplay} */
function AbstractRegionPingDisplayClass(){}

export default AbstractRegionPingDisplay


AbstractRegionPingDisplay[$implementations]=[
 __AbstractRegionPingDisplay,
 Display,
 AbstractRegionPingDisplayClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIRegionPingScreen:{vdusPQs:{
    LoWr:LoWr,
    RegionWr:RegionWr,
    RegionLa:RegionLa,
    ServerRegionWr:ServerRegionWr,
    ServerRegionLa:ServerRegionLa,
    PingWr:PingWr,
    RegionPingLa:RegionPingLa,
    RegionPingLa2:RegionPingLa2,
    RegionFlagLa:RegionFlagLa,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    LoWr:/**@type {HTMLSpanElement}*/(children[LoWr]),
    RegionWr:/**@type {HTMLSpanElement}*/(children[RegionWr]),
    RegionLa:/**@type {HTMLSpanElement}*/(children[RegionLa]),
    ServerRegionWr:/**@type {HTMLSpanElement}*/(children[ServerRegionWr]),
    ServerRegionLa:/**@type {HTMLSpanElement}*/(children[ServerRegionLa]),
    PingWr:/**@type {HTMLDivElement}*/(children[PingWr]),
    RegionPingLa:/**@type {HTMLSpanElement}*/(children[RegionPingLa]),
    RegionPingLa2:/**@type {HTMLSpanElement}*/(children[RegionPingLa2]),
    RegionFlagLa:/**@type {HTMLSpanElement}*/(children[RegionFlagLa]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.RegionPingDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IRegionPingDisplay.Initialese}*/({
   RegionFlagLa:1,
   LoWr:1,
   PingWr:1,
   RegionPingLa:1,
   RegionPingLa2:1,
   RegionLa:1,
   RegionWr:1,
   ServerRegionLa:1,
   ServerRegionWr:1,
  }),
  initializer({
   RegionFlagLa:_RegionFlagLa,
   LoWr:_LoWr,
   PingWr:_PingWr,
   RegionPingLa:_RegionPingLa,
   RegionPingLa2:_RegionPingLa2,
   RegionLa:_RegionLa,
   RegionWr:_RegionWr,
   ServerRegionLa:_ServerRegionLa,
   ServerRegionWr:_ServerRegionWr,
  }) {
   if(_RegionFlagLa!==undefined) this.RegionFlagLa=_RegionFlagLa
   if(_LoWr!==undefined) this.LoWr=_LoWr
   if(_PingWr!==undefined) this.PingWr=_PingWr
   if(_RegionPingLa!==undefined) this.RegionPingLa=_RegionPingLa
   if(_RegionPingLa2!==undefined) this.RegionPingLa2=_RegionPingLa2
   if(_RegionLa!==undefined) this.RegionLa=_RegionLa
   if(_RegionWr!==undefined) this.RegionWr=_RegionWr
   if(_ServerRegionLa!==undefined) this.ServerRegionLa=_ServerRegionLa
   if(_ServerRegionWr!==undefined) this.ServerRegionWr=_ServerRegionWr
  },
 }),
]