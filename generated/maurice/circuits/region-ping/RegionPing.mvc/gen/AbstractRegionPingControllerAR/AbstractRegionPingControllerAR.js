import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingControllerAR}
 */
function __AbstractRegionPingControllerAR() {}
__AbstractRegionPingControllerAR.prototype = /** @type {!_AbstractRegionPingControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionPingControllerAR}
 */
class _AbstractRegionPingControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IRegionPingControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractRegionPingControllerAR} ‎
 */
class AbstractRegionPingControllerAR extends newAbstract(
 _AbstractRegionPingControllerAR,163122484525,null,{
  asIRegionPingControllerAR:1,
  superRegionPingControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingControllerAR} */
AbstractRegionPingControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingControllerAR} */
function AbstractRegionPingControllerARClass(){}

export default AbstractRegionPingControllerAR


AbstractRegionPingControllerAR[$implementations]=[
 __AbstractRegionPingControllerAR,
 AR,
 AbstractRegionPingControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IRegionPingControllerAR}*/({
  allocator(){
   this.methods={
    setStart:'2e46d',
    unsetStart:'54258',
    setReceived:'20e48',
    unsetReceived:'0c67e',
    setDiff:'a6969',
    unsetDiff:'71fec',
    setDiff2:'8c620',
    unsetDiff2:'003f8',
    loadPing:'3e088',
   }
  },
 }),
]