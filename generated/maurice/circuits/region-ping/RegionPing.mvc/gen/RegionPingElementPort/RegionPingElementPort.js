import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionPingElementPort}
 */
function __RegionPingElementPort() {}
__RegionPingElementPort.prototype = /** @type {!_RegionPingElementPort} */ ({ })
/** @this {xyz.swapee.wc.RegionPingElementPort} */ function RegionPingElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IRegionPingElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    regionFlagLaOpts: {},
    loWrOpts: {},
    pingWrOpts: {},
    regionPingLaOpts: {},
    regionPingLa2Opts: {},
    regionLaOpts: {},
    regionWrOpts: {},
    serverRegionLaOpts: {},
    serverRegionWrOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingElementPort}
 */
class _RegionPingElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractRegionPingElementPort} ‎
 */
class RegionPingElementPort extends newAbstract(
 _RegionPingElementPort,163122484514,RegionPingElementPortConstructor,{
  asIRegionPingElementPort:1,
  superRegionPingElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingElementPort} */
RegionPingElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingElementPort} */
function RegionPingElementPortClass(){}

export default RegionPingElementPort


RegionPingElementPort[$implementations]=[
 __RegionPingElementPort,
 RegionPingElementPortClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'region-flag-la-opts':undefined,
    'lo-wr-opts':undefined,
    'ping-wr-opts':undefined,
    'region-ping-la-opts':undefined,
    'region-ping-la2-opts':undefined,
    'region-la-opts':undefined,
    'region-wr-opts':undefined,
    'server-region-la-opts':undefined,
    'server-region-wr-opts':undefined,
    'server-region':undefined,
   })
  },
 }),
]