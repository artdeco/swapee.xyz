import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingComputer}
 */
function __AbstractRegionPingComputer() {}
__AbstractRegionPingComputer.prototype = /** @type {!_AbstractRegionPingComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingComputer}
 */
class _AbstractRegionPingComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractRegionPingComputer} ‎
 */
export class AbstractRegionPingComputer extends newAbstract(
 _AbstractRegionPingComputer,16312248451,null,{
  asIRegionPingComputer:1,
  superRegionPingComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingComputer} */
AbstractRegionPingComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingComputer} */
function AbstractRegionPingComputerClass(){}


AbstractRegionPingComputer[$implementations]=[
 __AbstractRegionPingComputer,
 Adapter,
]


export default AbstractRegionPingComputer