
/**@this {xyz.swapee.wc.IRegionPingComputer}*/
export function preadaptPing(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IRegionPingComputer.adaptPing.Form}*/
 const _inputs={
  start:inputs.start,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.start) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPing(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IRegionPingComputer}*/
export function preadaptDifference(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IRegionPingComputer.adaptDifference.Form}*/
 const _inputs={
  start:inputs.start,
  received:inputs.received,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptDifference(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IRegionPingComputer}*/
export function preadaptDifferenceRequired(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IRegionPingComputer.adaptDifferenceRequired.Form}*/
 const _inputs={
  start:inputs.start,
  received:inputs.received,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.start)||(!__inputs.received)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptDifferenceRequired(__inputs,__changes)
 return RET
}