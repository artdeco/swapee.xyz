import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingScreenAT}
 */
function __AbstractRegionPingScreenAT() {}
__AbstractRegionPingScreenAT.prototype = /** @type {!_AbstractRegionPingScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionPingScreenAT}
 */
class _AbstractRegionPingScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IRegionPingScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractRegionPingScreenAT} ‎
 */
class AbstractRegionPingScreenAT extends newAbstract(
 _AbstractRegionPingScreenAT,163122484530,null,{
  asIRegionPingScreenAT:1,
  superRegionPingScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingScreenAT} */
AbstractRegionPingScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingScreenAT} */
function AbstractRegionPingScreenATClass(){}

export default AbstractRegionPingScreenAT


AbstractRegionPingScreenAT[$implementations]=[
 __AbstractRegionPingScreenAT,
 UartUniversal,
]