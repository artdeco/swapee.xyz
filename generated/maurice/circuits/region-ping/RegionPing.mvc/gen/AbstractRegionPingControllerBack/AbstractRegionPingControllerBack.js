import AbstractRegionPingControllerAR from '../AbstractRegionPingControllerAR'
import {AbstractRegionPingController} from '../AbstractRegionPingController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingControllerBack}
 */
function __AbstractRegionPingControllerBack() {}
__AbstractRegionPingControllerBack.prototype = /** @type {!_AbstractRegionPingControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionPingController}
 */
class _AbstractRegionPingControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractRegionPingController} ‎
 */
class AbstractRegionPingControllerBack extends newAbstract(
 _AbstractRegionPingControllerBack,163122484524,null,{
  asIRegionPingController:1,
  superRegionPingController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingController} */
AbstractRegionPingControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingController} */
function AbstractRegionPingControllerBackClass(){}

export default AbstractRegionPingControllerBack


AbstractRegionPingControllerBack[$implementations]=[
 __AbstractRegionPingControllerBack,
 AbstractRegionPingController,
 AbstractRegionPingControllerAR,
 DriverBack,
]