import AbstractRegionPingScreenAT from '../AbstractRegionPingScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingScreenBack}
 */
function __AbstractRegionPingScreenBack() {}
__AbstractRegionPingScreenBack.prototype = /** @type {!_AbstractRegionPingScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionPingScreen}
 */
class _AbstractRegionPingScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractRegionPingScreen} ‎
 */
class AbstractRegionPingScreenBack extends newAbstract(
 _AbstractRegionPingScreenBack,163122484528,null,{
  asIRegionPingScreen:1,
  superRegionPingScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingScreen} */
AbstractRegionPingScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionPingScreen} */
function AbstractRegionPingScreenBackClass(){}

export default AbstractRegionPingScreenBack


AbstractRegionPingScreenBack[$implementations]=[
 __AbstractRegionPingScreenBack,
 AbstractRegionPingScreenAT,
]