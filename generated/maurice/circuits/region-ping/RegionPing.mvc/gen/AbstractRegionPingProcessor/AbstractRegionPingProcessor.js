import {preadaptLoadPing} from '../AbstractRegionPingRadio/preadapters'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingProcessor}
 */
function __AbstractRegionPingProcessor() {}
__AbstractRegionPingProcessor.prototype = /** @type {!_AbstractRegionPingProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingProcessor}
 */
class _AbstractRegionPingProcessor { }
/**
 * The processor to compute changes to the memory for the _IRegionPing_.
 * @extends {xyz.swapee.wc.AbstractRegionPingProcessor} ‎
 */
class AbstractRegionPingProcessor extends newAbstract(
 _AbstractRegionPingProcessor,16312248459,null,{
  asIRegionPingProcessor:1,
  superRegionPingProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingProcessor} */
AbstractRegionPingProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingProcessor} */
function AbstractRegionPingProcessorClass(){}

export default AbstractRegionPingProcessor


AbstractRegionPingProcessor[$implementations]=[
 __AbstractRegionPingProcessor,
 AbstractRegionPingProcessorClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingProcessor}*/({
  loadPing(){
   return preadaptLoadPing.call(this,this.model).then(this.setInfo)
  },
 }),
]