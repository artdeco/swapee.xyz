import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingScreenAR}
 */
function __AbstractRegionPingScreenAR() {}
__AbstractRegionPingScreenAR.prototype = /** @type {!_AbstractRegionPingScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractRegionPingScreenAR}
 */
class _AbstractRegionPingScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IRegionPingScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractRegionPingScreenAR} ‎
 */
class AbstractRegionPingScreenAR extends newAbstract(
 _AbstractRegionPingScreenAR,163122484529,null,{
  asIRegionPingScreenAR:1,
  superRegionPingScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractRegionPingScreenAR} */
AbstractRegionPingScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractRegionPingScreenAR} */
function AbstractRegionPingScreenARClass(){}

export default AbstractRegionPingScreenAR


AbstractRegionPingScreenAR[$implementations]=[
 __AbstractRegionPingScreenAR,
 AR,
 AbstractRegionPingScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IRegionPingScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractRegionPingScreenAR}