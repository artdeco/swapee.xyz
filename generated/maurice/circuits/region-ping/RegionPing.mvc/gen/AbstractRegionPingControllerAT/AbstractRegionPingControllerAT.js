import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingControllerAT}
 */
function __AbstractRegionPingControllerAT() {}
__AbstractRegionPingControllerAT.prototype = /** @type {!_AbstractRegionPingControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractRegionPingControllerAT}
 */
class _AbstractRegionPingControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IRegionPingControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractRegionPingControllerAT} ‎
 */
class AbstractRegionPingControllerAT extends newAbstract(
 _AbstractRegionPingControllerAT,163122484526,null,{
  asIRegionPingControllerAT:1,
  superRegionPingControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractRegionPingControllerAT} */
AbstractRegionPingControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractRegionPingControllerAT} */
function AbstractRegionPingControllerATClass(){}

export default AbstractRegionPingControllerAT


AbstractRegionPingControllerAT[$implementations]=[
 __AbstractRegionPingControllerAT,
 UartUniversal,
 AbstractRegionPingControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IRegionPingControllerAT}*/({
  get asIRegionPingController(){
   return this
  },
  setStart(){
   this.uart.t("inv",{mid:'2e46d',args:[...arguments]})
  },
  unsetStart(){
   this.uart.t("inv",{mid:'54258'})
  },
  setReceived(){
   this.uart.t("inv",{mid:'20e48',args:[...arguments]})
  },
  unsetReceived(){
   this.uart.t("inv",{mid:'0c67e'})
  },
  setDiff(){
   this.uart.t("inv",{mid:'a6969',args:[...arguments]})
  },
  unsetDiff(){
   this.uart.t("inv",{mid:'71fec'})
  },
  setDiff2(){
   this.uart.t("inv",{mid:'8c620',args:[...arguments]})
  },
  unsetDiff2(){
   this.uart.t("inv",{mid:'003f8'})
  },
  loadPing(){
   this.uart.t("inv",{mid:'3e088'})
  },
 }),
]