import {mountPins} from '@webcircuits/webcircuits'
import {RegionPingMemoryPQs} from '../../pqs/RegionPingMemoryPQs'
import {RegionPingCachePQs} from '../../pqs/RegionPingCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionPingCore}
 */
function __RegionPingCore() {}
__RegionPingCore.prototype = /** @type {!_RegionPingCore} */ ({ })
/** @this {xyz.swapee.wc.RegionPingCore} */ function RegionPingCoreConstructor() {
  /**@type {!xyz.swapee.wc.IRegionPingCore.Model}*/
  this.model={
    loadingPing: false,
    hasMorePing: null,
    loadPingError: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingCore}
 */
class _RegionPingCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractRegionPingCore} ‎
 */
class RegionPingCore extends newAbstract(
 _RegionPingCore,16312248458,RegionPingCoreConstructor,{
  asIRegionPingCore:1,
  superRegionPingCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingCore} */
RegionPingCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingCore} */
function RegionPingCoreClass(){}

export default RegionPingCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionPingOuterCore}
 */
function __RegionPingOuterCore() {}
__RegionPingOuterCore.prototype = /** @type {!_RegionPingOuterCore} */ ({ })
/** @this {xyz.swapee.wc.RegionPingOuterCore} */
export function RegionPingOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IRegionPingOuterCore.Model}*/
  this.model={
    host: '',
    region: '',
    serverRegion: '',
    start: null,
    received: null,
    diff: 0,
    diff2: 0,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingOuterCore}
 */
class _RegionPingOuterCore { }
/**
 * The _IRegionPing_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractRegionPingOuterCore} ‎
 */
export class RegionPingOuterCore extends newAbstract(
 _RegionPingOuterCore,16312248453,RegionPingOuterCoreConstructor,{
  asIRegionPingOuterCore:1,
  superRegionPingOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingOuterCore} */
RegionPingOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingOuterCore} */
function RegionPingOuterCoreClass(){}


RegionPingOuterCore[$implementations]=[
 __RegionPingOuterCore,
 RegionPingOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingOuterCore}*/({
  constructor(){
   mountPins(this.model,'',RegionPingMemoryPQs)
   mountPins(this.model,'',RegionPingCachePQs)
  },
 }),
]

RegionPingCore[$implementations]=[
 RegionPingCoreClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingCore}*/({
  resetCore(){
   this.resetRegionPingCore()
  },
  resetRegionPingCore(){
   RegionPingCoreConstructor.call(
    /**@type {xyz.swapee.wc.RegionPingCore}*/(this),
   )
   RegionPingOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.RegionPingOuterCore}*/(
     /**@type {!xyz.swapee.wc.IRegionPingOuterCore}*/(this)),
   )
  },
 }),
 __RegionPingCore,
 RegionPingOuterCore,
]

export {RegionPingCore}