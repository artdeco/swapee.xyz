import AbstractRegionPingDisplay from '../AbstractRegionPingDisplayBack'
import RegionPingClassesPQs from '../../pqs/RegionPingClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {RegionPingClassesQPs} from '../../pqs/RegionPingClassesQPs'
import {RegionPingVdusPQs} from '../../pqs/RegionPingVdusPQs'
import {RegionPingVdusQPs} from '../../pqs/RegionPingVdusQPs'
import {RegionPingMemoryPQs} from '../../pqs/RegionPingMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingGPU}
 */
function __AbstractRegionPingGPU() {}
__AbstractRegionPingGPU.prototype = /** @type {!_AbstractRegionPingGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingGPU}
 */
class _AbstractRegionPingGPU { }
/**
 * Handles the periphery of the _IRegionPingDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractRegionPingGPU} ‎
 */
class AbstractRegionPingGPU extends newAbstract(
 _AbstractRegionPingGPU,163122484518,null,{
  asIRegionPingGPU:1,
  superRegionPingGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingGPU} */
AbstractRegionPingGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingGPU} */
function AbstractRegionPingGPUClass(){}

export default AbstractRegionPingGPU


AbstractRegionPingGPU[$implementations]=[
 __AbstractRegionPingGPU,
 AbstractRegionPingGPUClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingGPU}*/({
  classesQPs:RegionPingClassesQPs,
  vdusPQs:RegionPingVdusPQs,
  vdusQPs:RegionPingVdusQPs,
  memoryPQs:RegionPingMemoryPQs,
 }),
 AbstractRegionPingDisplay,
 BrowserView,
 AbstractRegionPingGPUClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingGPU}*/({
  allocator(){
   pressFit(this.classes,'',RegionPingClassesPQs)
  },
 }),
]