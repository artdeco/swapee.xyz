import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {RegionPingInputsPQs} from '../../pqs/RegionPingInputsPQs'
import {RegionPingOuterCoreConstructor} from '../RegionPingCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionPingPort}
 */
function __RegionPingPort() {}
__RegionPingPort.prototype = /** @type {!_RegionPingPort} */ ({ })
/** @this {xyz.swapee.wc.RegionPingPort} */ function RegionPingPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.RegionPingOuterCore} */ ({model:null})
  RegionPingOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingPort}
 */
class _RegionPingPort { }
/**
 * The port that serves as an interface to the _IRegionPing_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractRegionPingPort} ‎
 */
export class RegionPingPort extends newAbstract(
 _RegionPingPort,16312248455,RegionPingPortConstructor,{
  asIRegionPingPort:1,
  superRegionPingPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingPort} */
RegionPingPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingPort} */
function RegionPingPortClass(){}

export const RegionPingPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IRegionPing.Pinout>}*/({
 get Port() { return RegionPingPort },
})

RegionPingPort[$implementations]=[
 RegionPingPortClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingPort}*/({
  resetPort(){
   this.resetRegionPingPort()
  },
  resetRegionPingPort(){
   RegionPingPortConstructor.call(this)
  },
 }),
 __RegionPingPort,
 Parametric,
 RegionPingPortClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingPort}*/({
  constructor(){
   mountPins(this.inputs,'',RegionPingInputsPQs)
  },
 }),
]


export default RegionPingPort