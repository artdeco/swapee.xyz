import RegionPingBuffer from '../RegionPingBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {RegionPingPortConnector} from '../RegionPingPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingController}
 */
function __AbstractRegionPingController() {}
__AbstractRegionPingController.prototype = /** @type {!_AbstractRegionPingController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingController}
 */
class _AbstractRegionPingController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractRegionPingController} ‎
 */
export class AbstractRegionPingController extends newAbstract(
 _AbstractRegionPingController,163122484522,null,{
  asIRegionPingController:1,
  superRegionPingController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingController} */
AbstractRegionPingController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingController} */
function AbstractRegionPingControllerClass(){}


AbstractRegionPingController[$implementations]=[
 AbstractRegionPingControllerClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IRegionPingPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractRegionPingController,
 RegionPingBuffer,
 IntegratedController,
 /**@type {!AbstractRegionPingController}*/(RegionPingPortConnector),
 AbstractRegionPingControllerClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingController}*/({
  setStart(val){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({start:val})
  },
  setReceived(val){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({received:val})
  },
  setDiff(val){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({diff:val})
  },
  setDiff2(val){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({diff2:val})
  },
  unsetStart(){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({start:null})
  },
  unsetReceived(){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({received:null})
  },
  unsetDiff(){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({diff:null})
  },
  unsetDiff2(){
   const{asIRegionPingController:{setInputs:setInputs}}=this
   setInputs({diff2:null})
  },
 }),
]


export default AbstractRegionPingController