import RegionPingClassesPQs from '../../pqs/RegionPingClassesPQs'
import AbstractRegionPingScreenAR from '../AbstractRegionPingScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {RegionPingInputsPQs} from '../../pqs/RegionPingInputsPQs'
import {RegionPingMemoryQPs} from '../../pqs/RegionPingMemoryQPs'
import {RegionPingCacheQPs} from '../../pqs/RegionPingCacheQPs'
import {RegionPingVdusPQs} from '../../pqs/RegionPingVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingScreen}
 */
function __AbstractRegionPingScreen() {}
__AbstractRegionPingScreen.prototype = /** @type {!_AbstractRegionPingScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingScreen}
 */
class _AbstractRegionPingScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractRegionPingScreen} ‎
 */
class AbstractRegionPingScreen extends newAbstract(
 _AbstractRegionPingScreen,163122484527,null,{
  asIRegionPingScreen:1,
  superRegionPingScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingScreen} */
AbstractRegionPingScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingScreen} */
function AbstractRegionPingScreenClass(){}

export default AbstractRegionPingScreen


AbstractRegionPingScreen[$implementations]=[
 __AbstractRegionPingScreen,
 AbstractRegionPingScreenClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingScreen}*/({
  inputsPQs:RegionPingInputsPQs,
  classesPQs:RegionPingClassesPQs,
  memoryQPs:RegionPingMemoryQPs,
  cacheQPs:RegionPingCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractRegionPingScreenAR,
 AbstractRegionPingScreenClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingScreen}*/({
  vdusPQs:RegionPingVdusPQs,
 }),
 AbstractRegionPingScreenClass.prototype=/**@type {!xyz.swapee.wc.IRegionPingScreen}*/({
  deduceInputs(){
   const{asIRegionPingDisplay:{
    RegionLa:RegionLa,
    ServerRegionLa:ServerRegionLa,
    RegionPingLa:RegionPingLa,
    RegionPingLa2:RegionPingLa2,
   }}=this
   return{
    region:RegionLa?.innerText,
    serverRegion:ServerRegionLa?.innerText,
    diff:RegionPingLa?.innerText,
    diff2:RegionPingLa2?.innerText,
   }
  },
 }),
]