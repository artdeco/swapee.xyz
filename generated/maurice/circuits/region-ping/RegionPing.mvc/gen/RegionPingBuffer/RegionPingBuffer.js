import {makeBuffers} from '@webcircuits/webcircuits'

export const RegionPingBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  host:String,
  region:String,
  serverRegion:String,
  start:Number,
  received:Number,
  diff:Number,
  diff2:Number,
 }),
})

export default RegionPingBuffer