import {preadaptLoadPing} from './preadapters'
import {makeLoaders} from '@webcircuits/webcircuits'
import {StatefulLoader} from '@mauriceguest/guest2'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionPingRadio}
 */
function __AbstractRegionPingRadio() {}
__AbstractRegionPingRadio.prototype = /** @type {!_AbstractRegionPingRadio} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionPingRadio}
 */
class _AbstractRegionPingRadio { }
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @extends {xyz.swapee.wc.AbstractRegionPingRadio} ‎
 */
class AbstractRegionPingRadio extends newAbstract(
 _AbstractRegionPingRadio,163122484515,null,{
  asIRegionPingRadio:1,
  superRegionPingRadio:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionPingRadio} */
AbstractRegionPingRadio.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingRadio} */
function AbstractRegionPingRadioClass(){}

export default AbstractRegionPingRadio


AbstractRegionPingRadio[$implementations]=[
 __AbstractRegionPingRadio,
 makeLoaders(1631224845,{
  adaptLoadPing:[
   '29a6e',
   {
    region:'960db',
    start:'ea2b2',
   },
   {
    serverRegion:'40a20',
    received:'c5946',
   },
   {
    loadingPing:1,
    loadPingError:2,
   },
  ],
 }),
 StatefulLoader,
 {adapt:[preadaptLoadPing]},
]