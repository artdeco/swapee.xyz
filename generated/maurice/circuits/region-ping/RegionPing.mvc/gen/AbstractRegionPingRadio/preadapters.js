
/**@this {xyz.swapee.wc.IRegionPingRadio}*/
export function preadaptLoadPing(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IRegionPingRadio.adaptLoadPing.Form}*/
 const _inputs={
  host:inputs.host,
  region:inputs.region,
  start:inputs.start,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.start) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadPing(__inputs,__changes)
 return RET
}