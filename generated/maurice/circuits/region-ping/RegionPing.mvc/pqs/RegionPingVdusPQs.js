export const RegionPingVdusPQs=/**@type {!xyz.swapee.wc.RegionPingVdusPQs}*/({
 RegionFlagLa:'g7372',
 RegionLa:'g7373',
 LoWr:'g7375',
 RegionWr:'g7376',
 RegionPingLa:'g7377',
 RegionPingLa2:'g7378',
 PingWr:'g7379',
 ServerRegionWr:'g73710',
 ServerRegionLa:'g73711',
})