import RegionPingClassesPQs from './RegionPingClassesPQs'
export const RegionPingClassesQPs=/**@type {!xyz.swapee.wc.RegionPingClassesQPs}*/(Object.keys(RegionPingClassesPQs)
 .reduce((a,k)=>{a[RegionPingClassesPQs[k]]=k;return a},{}))