export const RegionPingCachePQs=/**@type {!xyz.swapee.wc.RegionPingCachePQs}*/({
 loadingPing:'f6ff7',
 hasMorePing:'4b2da',
 loadPingError:'8589d',
})