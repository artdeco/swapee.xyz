import {RegionPingCachePQs} from './RegionPingCachePQs'
export const RegionPingCacheQPs=/**@type {!xyz.swapee.wc.RegionPingCacheQPs}*/(Object.keys(RegionPingCachePQs)
 .reduce((a,k)=>{a[RegionPingCachePQs[k]]=k;return a},{}))