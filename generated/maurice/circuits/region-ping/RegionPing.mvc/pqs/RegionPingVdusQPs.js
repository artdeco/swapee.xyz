import {RegionPingVdusPQs} from './RegionPingVdusPQs'
export const RegionPingVdusQPs=/**@type {!xyz.swapee.wc.RegionPingVdusQPs}*/(Object.keys(RegionPingVdusPQs)
 .reduce((a,k)=>{a[RegionPingVdusPQs[k]]=k;return a},{}))