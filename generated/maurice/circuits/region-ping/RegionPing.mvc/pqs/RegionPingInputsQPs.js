import {RegionPingInputsPQs} from './RegionPingInputsPQs'
export const RegionPingInputsQPs=/**@type {!xyz.swapee.wc.RegionPingInputsQPs}*/(Object.keys(RegionPingInputsPQs)
 .reduce((a,k)=>{a[RegionPingInputsPQs[k]]=k;return a},{}))