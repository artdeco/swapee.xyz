import {RegionPingMemoryPQs} from './RegionPingMemoryPQs'
export const RegionPingMemoryQPs=/**@type {!xyz.swapee.wc.RegionPingMemoryQPs}*/(Object.keys(RegionPingMemoryPQs)
 .reduce((a,k)=>{a[RegionPingMemoryPQs[k]]=k;return a},{}))