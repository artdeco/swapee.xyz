/** @type {xyz.swapee.wc.IRegionPingElement._render} */
export default function RegionPingRender({
 loadingPing:loadingPing,region:region,
 diff:diff,diff2:diff2,serverRegion:serverRegion,
},{}) {
 return (<div $id="RegionPing">
  <span $id="LoWr" $reveal={loadingPing} />
  <span $id="RegionWr" $reveal={region} $conceal={serverRegion}>
   <span $id="RegionLa">{region}</span>
  </span>
  <span $id="ServerRegionWr" $reveal={serverRegion}>
   <span $id="ServerRegionLa">{serverRegion}</span>
  </span>
  <span $id="PingWr" $reveal={diff2} />
  <span $id="RegionPingLa">{diff}</span>
  <span $id="RegionPingLa2">{diff2}</span>
 </div>)
}