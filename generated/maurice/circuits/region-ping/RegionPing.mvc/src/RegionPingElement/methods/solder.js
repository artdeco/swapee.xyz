/** @type {xyz.swapee.wc.IRegionPingElement._solder} */
export default function solder({region:region}) {
 return{
  region:region,
  'host':this.REMOTE_HOST,
 }
}