import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import RegionPingServerController from '../RegionPingServerController'
import RegionPingService from '../../src/RegionPingService'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractRegionPingElement from '../../gen/AbstractRegionPingElement'

/** @extends {xyz.swapee.wc.RegionPingElement} */
export default class RegionPingElement extends AbstractRegionPingElement.implements(
 RegionPingServerController,
 RegionPingService,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IRegionPingElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IRegionPingElement}*/({
   classesMap: true,
   rootSelector:     `.RegionPing`,
   stylesheet:       'html/styles/RegionPing.css',
   blockName:        'html/RegionPingBlock.html',
  }),
){}

// thank you for using web circuits
