import RegionPingHtmlController from '../RegionPingHtmlController'
import RegionPingHtmlComputer from '../RegionPingHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractRegionPingHtmlComponent} from '../../gen/AbstractRegionPingHtmlComponent'

/** @extends {xyz.swapee.wc.RegionPingHtmlComponent} */
export default class extends AbstractRegionPingHtmlComponent.implements(
 RegionPingHtmlController,
 RegionPingHtmlComputer,
 IntegratedComponentInitialiser,
){}