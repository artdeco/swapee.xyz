import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IRegionPingService._filterPing} */
export default function filterPing(){
 const received=(new Date).getTime()
 const{'req':{'region':region}}=this
 // need to get the server region from this.request if the request was
 // added to the stack which it is.
 return{
  serverRegion:region,
  received:received,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvcmVnaW9uLXBpbmcvcmVnaW9uLXBpbmcud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQWdMRyxTQUFTLFVBQVUsQ0FBQztDQUNuQixNQUFNLFFBQVEsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQztDQUNsQyxNQUFNLE9BQU8sZUFBZSxHQUFHO0NBQy9CLEdBQUcsS0FBSyxHQUFHLElBQUksSUFBSSxPQUFPLE9BQU8sS0FBSyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksUUFBUTtDQUNsRSxHQUFHLE1BQU0sR0FBRyxJQUFJLE1BQU0sTUFBTSxHQUFHLEVBQUU7Q0FDakM7RUFDQyxtQkFBbUI7RUFDbkIsaUJBQWlCO0FBQ25CO0FBQ0EsQ0FBRiJ9