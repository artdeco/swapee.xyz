import AbstractRegionPingService from '../../gen/AbstractRegionPingService'
import filterPing from './methods/filterPing/filter-ping'

/**@extends {xyz.swapee.wc.RegionPingService} */
export default class extends AbstractRegionPingService.implements(
 AbstractRegionPingService.class.prototype=/**@type {!xyz.swapee.wc.RegionPingService}*/({
  filterPing:filterPing,
 }),
){}