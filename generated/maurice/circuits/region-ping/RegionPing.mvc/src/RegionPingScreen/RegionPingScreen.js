import deduceInputs from './methods/deduce-inputs'
import constructor from './methods/constructor'
import AbstractRegionPingControllerAT from '../../gen/AbstractRegionPingControllerAT'
import RegionPingDisplay from '../RegionPingDisplay'
import AbstractRegionPingScreen from '../../gen/AbstractRegionPingScreen'

/** @extends {xyz.swapee.wc.RegionPingScreen} */
export default class extends AbstractRegionPingScreen.implements(
 AbstractRegionPingControllerAT,
 RegionPingDisplay,
 /**@type {!xyz.swapee.wc.IRegionPingScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IRegionPingScreen} */ ({
  deduceInputs:deduceInputs,
  constructor:constructor,
  __$id:1631224845,
 }),
){}