import adaptDifference from './methods/adapt-difference'
import adaptDifferenceRequired from './methods/adapt-difference-required'
import {preadaptDifference,preadaptDifferenceRequired} from '../../gen/AbstractRegionPingComputer/preadapters'
import AbstractRegionPingComputer from '../../gen/AbstractRegionPingComputer'

/** @extends {xyz.swapee.wc.RegionPingComputer} */
export default class RegionPingHtmlComputer extends AbstractRegionPingComputer.implements(
 /** @type {!xyz.swapee.wc.IRegionPingComputer} */ ({
  adaptDifference:adaptDifference,
  adaptDifferenceRequired:adaptDifferenceRequired,
  adapt:[preadaptDifference,preadaptDifferenceRequired],
 }),
){}