/** @type {xyz.swapee.wc.IRegionPingComputer._adaptDifferenceRequired} */
export default function adaptDifferenceRequired({received,start}){
 return{
  diff:received-start,
  diff2:Date.now()-received,
 }
}