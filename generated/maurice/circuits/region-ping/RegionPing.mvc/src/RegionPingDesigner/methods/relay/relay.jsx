
/**@type {xyz.swapee.wc.IRegionPingDesigner._relay} */
export default function relay({RegionPing:_RegionPing,This}){
 return h(Fragment,{},
  h(This,{ onStartHigh:[
   _RegionPing.unsetReceived(),
  ]})
 )
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvcmVnaW9uLXBpbmcvcmVnaW9uLXBpbmcud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQTRMRyxTQUFTLEtBQUssRUFBRSxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7Q0FDM0MsT0FBTyxZQUFDO0VBQ1AsU0FBTSxZQUFhO0dBQ2xCLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztFQUM1QixDQUFFO0NBQ0g7QUFDRCxDQUFGIn0=