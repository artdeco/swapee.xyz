/**
 * An abstract class of `xyz.swapee.wc.IRegionPing` interface.
 * @extends {xyz.swapee.wc.AbstractRegionPing}
 */
class AbstractRegionPing extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IRegionPing_, providing input
 * pins.
 * @extends {xyz.swapee.wc.RegionPingPort}
 */
class RegionPingPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingController` interface.
 * @extends {xyz.swapee.wc.AbstractRegionPingController}
 */
class AbstractRegionPingController extends (class {/* lazy-loaded */}) {}
/**
 * The _IRegionPing_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.RegionPingHtmlComponent}
 */
class RegionPingHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.RegionPingBuffer}
 */
class RegionPingBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingComputer` interface.
 * @extends {xyz.swapee.wc.AbstractRegionPingComputer}
 */
class AbstractRegionPingComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.RegionPingComputer}
 */
class RegionPingComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.RegionPingController}
 */
class RegionPingController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractRegionPing = AbstractRegionPing
module.exports.RegionPingPort = RegionPingPort
module.exports.AbstractRegionPingController = AbstractRegionPingController
module.exports.RegionPingHtmlComponent = RegionPingHtmlComponent
module.exports.RegionPingBuffer = RegionPingBuffer
module.exports.AbstractRegionPingComputer = AbstractRegionPingComputer
module.exports.RegionPingComputer = RegionPingComputer
module.exports.RegionPingController = RegionPingController