import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1631224845} */
function d(a,b,c){a={host:a.host,region:a.region,start:a.start};a=c?c(a):a;if(a.start)return b=c?c(b):b,this.M(a,b)};/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const ca=e["372700389810"],f=e["372700389811"];function h(a,b,c,g){return e["372700389812"](a,b,c,g,!1,void 0)};function k(){}k.prototype={};class da{}class l extends h(da,16312248459,null,{ha:1,xa:2}){}l[f]=[k,{P(){return d.call(this,this.model).then(this.setInfo)}}];
const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ea=m["61505580523"],n=m["615055805212"],fa=m["615055805218"],ha=m["615055805221"],ia=m["615055805223"],q=m["615055805233"],ja=m["615055805238"],ka=m["615055805239"];const r={host:"67b3d",region:"960db",ping:"df911",j:"40a20",start:"ea2b2",g:"c5946",l:"2d2de",la:"78918",i:"e559c"};const t={G:"f6ff7",I:"4b2da",H:"8589d"};function u(){}u.prototype={};function la(){this.model={G:!1,I:null,H:null}}class ma{}class v extends h(ma,16312248458,la,{ca:1,qa:2}){}function w(){}w.prototype={};function x(){this.model={host:"",region:"",j:"",start:null,g:null,l:0,i:0}}class na{}class y extends h(na,16312248453,x,{fa:1,ua:2}){}y[f]=[w,{constructor(){q(this.model,"",r);q(this.model,"",t)}}];v[f]=[{},u,y];

const z=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const oa=z.IntegratedController,pa=z.Parametric;
const A=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const qa=A.StatefulLoader,ra=A.IntegratedComponentInitialiser,sa=A.IntegratedComponent,ta=A["38"];function B(){}B.prototype={};class ua{}class C extends h(ua,16312248451,null,{aa:1,oa:2}){}C[f]=[B,fa];const D={regulate:n({host:String,region:String,j:String,start:Number,g:Number,l:Number,i:Number})};const E={...r};function F(){}F.prototype={};function va(){const a={model:null};x.call(a);this.inputs=a.model}class wa{}class G extends h(wa,16312248455,va,{ga:1,wa:2}){}function H(){}G[f]=[H.prototype={},F,pa,H.prototype={constructor(){q(this.inputs,"",E)}}];function I(){}I.prototype={};class xa{}class J extends h(xa,163122484522,null,{h:1,V:2}){}function K(){}
J[f]=[K.prototype={},I,D,oa,{get Port(){return G}},K.prototype={setStart(a){const {h:{setInputs:b}}=this;b({start:a})},U(a){const {h:{setInputs:b}}=this;b({g:a})},R(a){const {h:{setInputs:b}}=this;b({l:a})},T(a){const {h:{setInputs:b}}=this;b({i:a})},Y(){const {h:{setInputs:a}}=this;a({start:null})},J(){const {h:{setInputs:a}}=this;a({g:null})},W(){const {h:{setInputs:a}}=this;a({l:null})},X(){const {h:{setInputs:a}}=this;a({i:null})}}];const ya=n({G:Boolean,I:Boolean,H:5},{silent:!0});const za=Object.keys(t).reduce((a,b)=>{a[t[b]]=b;return a},{});function L(){}L.prototype={};class Aa{}class M extends h(Aa,163122484510,null,{$:1,ma:2}){}M[f]=[L,v,l,sa,C,J,{regulateState:ya,stateQPs:za}];
const N=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const Ba=N["12817393923"],Ca=N["12817393924"],Da=N["12817393925"],Ea=N["12817393926"];function O(){}O.prototype={};class Fa{}class P extends h(Fa,163122484525,null,{ba:1,pa:2}){}P[f]=[O,Ea,{allocator(){this.methods={setStart:"2e46d",Y:"54258",U:"20e48",J:"0c67e",R:"a6969",W:"71fec",T:"8c620",X:"003f8",P:"3e088"}}}];function Q(){}Q.prototype={};class Ga{}class R extends h(Ga,163122484524,null,{h:1,V:2}){}R[f]=[Q,J,P,Ba];var S=class extends R.implements(){};function Ha(){return{l:null,i:null}};function Ia({g:a,start:b}){return{l:a-b,i:Date.now()-a}};function Ja(a,b,c){a={start:a.start,g:a.g};a=c?c(a):a;b=c?c(b):b;return this.K(a,b)}function Ka(a,b,c){a={start:a.start,g:a.g};a=c?c(a):a;if(a.start&&a.g)return b=c?c(b):b,this.L(a,b)};class T extends C.implements({K:Ha,L:Ia,adapt:[Ja,Ka]}){};function U(){}U.prototype={};class La{}class V extends h(La,163122484521,null,{da:1,ra:2}){}
V[f]=[U,Ca,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{m:a,A:a,s:a,D:a,C:a,o:a,u:a,v:a,F:a})}},{[ca]:{F:1,m:1,o:1,u:1,v:1,s:1,A:1,C:1,D:1},initializer({F:a,m:b,o:c,u:g,v:p,s:Y,A:Z,C:aa,D:ba}){void 0!==a&&(this.F=a);void 0!==b&&(this.m=b);void 0!==c&&(this.o=c);void 0!==g&&(this.u=g);void 0!==p&&(this.v=p);void 0!==Y&&(this.s=Y);void 0!==Z&&(this.A=Z);void 0!==aa&&(this.C=aa);void 0!==ba&&(this.D=ba)}}];const W={Z:"3791f"};const Ma=Object.keys(W).reduce((a,b)=>{a[W[b]]=b;return a},{});const Na={F:"g7372",s:"g7373",m:"g7375",A:"g7376",u:"g7377",v:"g7378",o:"g7379",D:"g73710",C:"g73711"};const Oa=Object.keys(Na).reduce((a,b)=>{a[Na[b]]=b;return a},{});function Pa(){}Pa.prototype={};class Qa{}class Ra extends h(Qa,163122484518,null,{O:1,sa:2}){}function Sa(){}Ra[f]=[Pa,Sa.prototype={classesQPs:Ma,vdusQPs:Oa,memoryPQs:r},V,ea,Sa.prototype={allocator(){ta(this.classes,"",W)}}];function Ta(){}Ta.prototype={};class Ua{}class Va extends h(Ua,163122484530,null,{ka:1,Aa:2}){}Va[f]=[Ta,Da];function Wa(){}Wa.prototype={};class Xa{}class Ya extends h(Xa,163122484528,null,{ja:1,za:2}){}Ya[f]=[Wa,Va];function Za(){}Za.prototype={};class $a{}class ab extends h($a,163122484515,null,{ia:1,ya:2}){}ab[f]=[Za,ka(1631224845,{M:["29a6e",{region:"960db",start:"ea2b2"},{j:"40a20",g:"c5946"},{G:1,H:2}]}),qa,{adapt:[d]}];const bb=Object.keys(E).reduce((a,b)=>{a[E[b]]=b;return a},{});function cb(){}cb.prototype={};class db{static mvc(a,b,c){return ia(this,a,b,null,c)}}class eb extends h(db,163122484512,null,{ea:1,ta:2}){}function X(){}
eb[f]=[cb,ha,M,Ra,Ya,ab,X.prototype={inputsQPs:bb},{paint:[function(){this.s.setText(this.model.region)},function(){this.C.setText(this.model.j)},function(){this.u.setText(this.model.l)},function(){this.v.setText(this.model.i)}]},X.prototype={paint:function({j:a,region:b}){const {O:{A:c},asIBrowserView:{conceal:g,reveal:p}}=this;({j:a,region:b}={j:a,region:b});a?g(c,!0):p(c,b)}},X.prototype={paint:ja({m:{G:1},D:{j:1},o:{i:1}})},X.prototype={paint({start:a}){const {h:{J:b}}=this;a&&b()}}];var fb=class extends eb.implements(S,T,ra){};module.exports["16312248450"]=M;module.exports["16312248451"]=M;module.exports["16312248453"]=G;module.exports["16312248454"]=J;module.exports["163122484510"]=fb;module.exports["163122484511"]=D;module.exports["163122484530"]=C;module.exports["163122484531"]=T;module.exports["163122484561"]=S;
/*! @embed-object-end {1631224845} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule