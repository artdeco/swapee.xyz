import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1631224845} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function e(a,b,h,g){return c["372700389812"](a,b,h,g,!1,void 0)};function f(){}f.prototype={};class aa{}class l extends e(aa,16312248459,null,{J:1,ia:2}){}l[d]=[f,{}];

const m=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const n=m["615055805212"],ba=m["615055805218"],p=m["615055805233"];const q={host:"67b3d",region:"960db",ping:"df911",j:"40a20",start:"ea2b2",m:"c5946",g:"2d2de",X:"78918",i:"e559c"};const r={l:"f6ff7",o:"4b2da",s:"8589d"};function t(){}t.prototype={};function ca(){this.model={l:!1,o:null,s:null}}class da{}class u extends e(da,16312248458,ca,{D:1,da:2}){}function v(){}v.prototype={};function w(){this.model={host:"",region:"",j:"",start:null,m:null,g:0,i:0}}class ea{}class A extends e(ea,16312248453,w,{H:1,ga:2}){}A[d]=[v,{constructor(){p(this.model,"",q);p(this.model,"",r)}}];u[d]=[{},t,A];
const B=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const fa=B.IntegratedComponentInitialiser,C=B.IntegratedComponent,ha=B["95173443851"];function D(){}D.prototype={};class ia{}class E extends e(ia,16312248451,null,{A:1,aa:2}){}E[d]=[D,ba];const F={regulate:n({host:String,region:String,j:String,start:Number,m:Number,g:Number,i:Number})};
const G=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ja=G.IntegratedController,ka=G.Parametric;const H={...q};function I(){}I.prototype={};function la(){const a={model:null};w.call(a);this.inputs=a.model}class ma{}class J extends e(ma,16312248455,la,{I:1,ha:2}){}function K(){}J[d]=[K.prototype={},I,ka,K.prototype={constructor(){p(this.inputs,"",H)}}];function L(){}L.prototype={};class na{}class M extends e(na,163122484522,null,{u:1,ba:2}){}function N(){}M[d]=[N.prototype={},L,F,ja,{get Port(){return J}},N.prototype={setStart(a){const {u:{setInputs:b}}=this;b({start:a})}}];const oa=n({l:Boolean,o:Boolean,s:5},{silent:!0});const pa=Object.keys(r).reduce((a,b)=>{a[r[b]]=b;return a},{});function O(){}O.prototype={};class qa{}class P extends e(qa,163122484510,null,{v:1,$:2}){}P[d]=[O,u,l,C,E,M,{regulateState:oa,stateQPs:pa}];function ra({region:a}){return{region:a,host:this.REMOTE_HOST}};const sa=require(eval('"@type.engineering/web-computing"')).h;function ta(){return sa("div",{$id:"RegionPing"})};const Q=require(eval('"@type.engineering/web-computing"')).h;function ua({l:a,region:b,g:h,i:g,j:k}){return Q("div",{$id:"RegionPing"},Q("span",{$id:"LoWr",$reveal:a}),Q("span",{$id:"RegionWr",$reveal:b,$conceal:k},Q("span",{$id:"RegionLa"},b)),Q("span",{$id:"ServerRegionWr",$reveal:k},Q("span",{$id:"ServerRegionLa"},k)),Q("span",{$id:"PingWr",$reveal:g}),Q("span",{$id:"RegionPingLa"},h),Q("span",{$id:"RegionPingLa2"},g))};var R=class extends M.implements(){};function S(){}S.prototype={};class va{}class T extends e(va,163122484517,null,{K:1,ja:2}){}function wa(){}T[d]=[S];var xa=class extends T.implements(wa.prototype={}){};require("https");require("http");const ya=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}ya("aqt");require("fs");require("child_process");
const U=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const za=U.ElementBase,Aa=U.HTMLBlocker;const V=require(eval('"@type.engineering/web-computing"')).h;function W(){}W.prototype={};function Ba(){this.inputs={noSolder:!1,R:{},M:{},P:{},V:{},U:{},T:{},W:{},Y:{},Z:{}}}class Ca{}class X extends e(Ca,163122484514,Ba,{G:1,fa:2}){}
X[d]=[W,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"region-flag-la-opts":void 0,"lo-wr-opts":void 0,"ping-wr-opts":void 0,"region-ping-la-opts":void 0,"region-ping-la2-opts":void 0,"region-la-opts":void 0,"region-wr-opts":void 0,"server-region-la-opts":void 0,"server-region-wr-opts":void 0,"server-region":void 0})}}];function Da(){}Da.prototype={};class Ea{}class Y extends e(Ea,163122484513,null,{F:1,ea:2}){}function Z(){}
Y[d]=[Da,za,Z.prototype={calibrate:function({":no-solder":a,":host":b,":region":h,":server-region":g,":start":k,":received":x,":diff":y,":diff2":z}){const {attributes:{"no-solder":Fa,host:Ga,region:Ha,"server-region":Ia,start:Ja,received:Ka,diff:La,diff2:Ma}}=this;return{...(void 0===Fa?{"no-solder":a}:{}),...(void 0===Ga?{host:b}:{}),...(void 0===Ha?{region:h}:{}),...(void 0===Ia?{"server-region":g}:{}),...(void 0===Ja?{start:k}:{}),...(void 0===Ka?{received:x}:{}),...(void 0===La?{diff:y}:{}),...(void 0===
Ma?{diff2:z}:{})}}},Z.prototype={calibrate:({"no-solder":a,host:b,region:h,"server-region":g,start:k,received:x,diff:y,diff2:z})=>({noSolder:a,host:b,region:h,j:g,start:k,m:x,g:y,i:z})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return V("div",{$id:"RegionPing"},V("vdu",{$id:"LoWr"}),V("vdu",{$id:"RegionWr"}),V("vdu",{$id:"RegionLa"}),V("vdu",{$id:"ServerRegionWr"}),V("vdu",{$id:"ServerRegionLa"}),V("vdu",{$id:"PingWr"}),V("vdu",{$id:"RegionPingLa"}),V("vdu",
{$id:"RegionPingLa2"}),V("vdu",{$id:"RegionFlagLa"}))}},Z.prototype={classes:{Reg:"3791f"},inputsPQs:H,cachePQs:r,vdus:{RegionFlagLa:"g7372",RegionLa:"g7373",LoWr:"g7375",RegionWr:"g7376",RegionPingLa:"g7377",RegionPingLa2:"g7378",PingWr:"g7379",ServerRegionWr:"g73710",ServerRegionLa:"g73711"}},C,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder host region serverRegion start received diff diff2 no-solder :no-solder :host :region server-region :server-region :start :received :diff :diff2 fe646 83a1c 16fec 00d47 66bf6 4553d 787e3 5886b a649c b5aad 67b3d 960db 40a20 ea2b2 c5946 2d2de e559c children".split(" "))})},
get Port(){return X}}];Y[d]=[P,{rootId:"RegionPing",__$id:1631224845,fqn:"xyz.swapee.wc.IRegionPing",maurice_element_v3:!0}];class Na extends Y.implements(R,xa,ha,Aa,fa,{solder:ra,server:ta,render:ua},{classesMap:!0,rootSelector:".RegionPing",stylesheet:"html/styles/RegionPing.css",blockName:"html/RegionPingBlock.html"}){};module.exports["16312248450"]=P;module.exports["16312248451"]=P;module.exports["16312248453"]=J;module.exports["16312248454"]=M;module.exports["16312248458"]=Na;module.exports["163122484511"]=F;module.exports["163122484530"]=E;module.exports["163122484561"]=R;
/*! @embed-object-end {1631224845} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule