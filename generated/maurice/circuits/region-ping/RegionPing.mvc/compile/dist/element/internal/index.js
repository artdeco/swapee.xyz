import Module from './element'

/**@extends {xyz.swapee.wc.AbstractRegionPing}*/
export class AbstractRegionPing extends Module['16312248451'] {}
/** @type {typeof xyz.swapee.wc.AbstractRegionPing} */
AbstractRegionPing.class=function(){}
/** @type {typeof xyz.swapee.wc.RegionPingPort} */
export const RegionPingPort=Module['16312248453']
/**@extends {xyz.swapee.wc.AbstractRegionPingController}*/
export class AbstractRegionPingController extends Module['16312248454'] {}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingController} */
AbstractRegionPingController.class=function(){}
/** @type {typeof xyz.swapee.wc.RegionPingElement} */
export const RegionPingElement=Module['16312248458']
/** @type {typeof xyz.swapee.wc.RegionPingBuffer} */
export const RegionPingBuffer=Module['163122484511']
/**@extends {xyz.swapee.wc.AbstractRegionPingComputer}*/
export class AbstractRegionPingComputer extends Module['163122484530'] {}
/** @type {typeof xyz.swapee.wc.AbstractRegionPingComputer} */
AbstractRegionPingComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.RegionPingController} */
export const RegionPingController=Module['163122484561']