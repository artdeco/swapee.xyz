/**
 * An abstract class of `xyz.swapee.wc.IRegionPing` interface.
 * @extends {xyz.swapee.wc.AbstractRegionPing}
 */
class AbstractRegionPing extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IRegionPing_, providing input
 * pins.
 * @extends {xyz.swapee.wc.RegionPingPort}
 */
class RegionPingPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingController` interface.
 * @extends {xyz.swapee.wc.AbstractRegionPingController}
 */
class AbstractRegionPingController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IRegionPing_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.RegionPingElement}
 */
class RegionPingElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.RegionPingBuffer}
 */
class RegionPingBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionPingComputer` interface.
 * @extends {xyz.swapee.wc.AbstractRegionPingComputer}
 */
class AbstractRegionPingComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.RegionPingController}
 */
class RegionPingController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractRegionPing = AbstractRegionPing
module.exports.RegionPingPort = RegionPingPort
module.exports.AbstractRegionPingController = AbstractRegionPingController
module.exports.RegionPingElement = RegionPingElement
module.exports.RegionPingBuffer = RegionPingBuffer
module.exports.AbstractRegionPingComputer = AbstractRegionPingComputer
module.exports.RegionPingController = RegionPingController

Object.defineProperties(module.exports, {
 'AbstractRegionPing': {get: () => require('./precompile/internal')[16312248451]},
 [16312248451]: {get: () => module.exports['AbstractRegionPing']},
 'RegionPingPort': {get: () => require('./precompile/internal')[16312248453]},
 [16312248453]: {get: () => module.exports['RegionPingPort']},
 'AbstractRegionPingController': {get: () => require('./precompile/internal')[16312248454]},
 [16312248454]: {get: () => module.exports['AbstractRegionPingController']},
 'RegionPingElement': {get: () => require('./precompile/internal')[16312248458]},
 [16312248458]: {get: () => module.exports['RegionPingElement']},
 'RegionPingBuffer': {get: () => require('./precompile/internal')[163122484511]},
 [163122484511]: {get: () => module.exports['RegionPingBuffer']},
 'AbstractRegionPingComputer': {get: () => require('./precompile/internal')[163122484530]},
 [163122484530]: {get: () => module.exports['AbstractRegionPingComputer']},
 'RegionPingController': {get: () => require('./precompile/internal')[163122484561]},
 [163122484561]: {get: () => module.exports['RegionPingController']},
})