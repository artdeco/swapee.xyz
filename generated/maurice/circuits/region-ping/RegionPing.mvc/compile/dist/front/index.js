/**
 * Display for presenting information from the _IRegionPing_.
 * @extends {xyz.swapee.wc.RegionPingDisplay}
 */
class RegionPingDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.RegionPingScreen}
 */
class RegionPingScreen extends (class {/* lazy-loaded */}) {}

module.exports.RegionPingDisplay = RegionPingDisplay
module.exports.RegionPingScreen = RegionPingScreen