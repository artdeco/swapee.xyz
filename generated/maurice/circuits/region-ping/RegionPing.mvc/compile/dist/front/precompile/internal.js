var f="depack-remove-start",g=f;try{if(f)throw Error();Object.setPrototypeOf(g,g);g.U=new WeakMap;g.map=new Map;g.set=new Set;Object.getOwnPropertySymbols({});Object.getOwnPropertyDescriptors({});f.includes("");[].keys();Object.values({});Object.assign({},{})}catch(a){}f="depack-remove-end";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const k=h["37270038985"],l=h["372700389810"],m=h["372700389811"];function u(a,b,c,d){return h["372700389812"](a,b,c,d,!1,void 0)};/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();/*

@LICENSE @webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const w=v["61893096584"],x=v["61893096586"],y=v["618930965811"],z=v["618930965812"],A=v["618930965815"],B=v["618930965819"];function C(){}C.prototype={};function D(){this.u=this.j=this.s=this.g=this.i=this.h=this.m=this.l=this.o=null}class E{}class F extends u(E,163122484519,D,{A:1,P:2}){}
F[m]=[C,w,function(){}.prototype={constructor(){k(this,()=>{this.scan({})})},scan:function(){const {element:a,v:{vdusPQs:{l:b,s:c,g:d,u:n,j:p,m:q,h:r,i:t,o:Q}}}=this,e=A(a);Object.assign(this,{l:e[b],s:e[c],g:e[d],u:e[n],j:e[p],m:e[q],h:e[r],i:e[t],o:e[Q]})}},{[l]:{o:1,l:1,m:1,h:1,i:1,g:1,s:1,j:1,u:1},initializer({o:a,l:b,m:c,h:d,i:n,g:p,s:q,j:r,u:t}){void 0!==a&&(this.o=a);void 0!==b&&(this.l=b);void 0!==c&&(this.m=c);void 0!==d&&(this.h=d);void 0!==n&&(this.i=n);void 0!==p&&(this.g=p);void 0!==
q&&(this.s=q);void 0!==r&&(this.j=r);void 0!==t&&(this.u=t)}}];var G=class extends F.implements(){};function H(){};function I(){this.v.setInputs({start:Date.now()})};function J(){}J.prototype={};class K{}class L extends u(K,163122484526,null,{G:1,O:2}){}L[m]=[J,y,function(){}.prototype={setStart(){this.uart.t("inv",{mid:"2e46d",args:[...arguments]})}}];function M(){}M.prototype={};class N{}class O extends u(N,163122484529,null,{H:1,T:2}){}O[m]=[M,z,function(){}.prototype={allocator(){this.methods={}}}];const P={host:"67b3d",region:"960db",ping:"df911",F:"40a20",start:"ea2b2",L:"c5946",C:"2d2de",M:"78918",D:"e559c"};const R={...P};const S=Object.keys(P).reduce((a,b)=>{a[P[b]]=b;return a},{});const T={K:"f6ff7",I:"4b2da",J:"8589d"};const U=Object.keys(T).reduce((a,b)=>{a[T[b]]=b;return a},{});function V(){}V.prototype={};class W{}class X extends u(W,163122484527,null,{v:1,R:2}){}function Y(){}X[m]=[V,Y.prototype={inputsPQs:R,memoryQPs:S,cacheQPs:U},x,B,O,Y.prototype={vdusPQs:{o:"g7372",g:"g7373",l:"g7375",s:"g7376",h:"g7377",i:"g7378",m:"g7379",u:"g73710",j:"g73711"}},Y.prototype={deduceInputs(){const {A:{g:a,j:b,h:c,i:d}}=this;return{region:null==a?void 0:a.innerText,F:null==b?void 0:b.innerText,C:null==c?void 0:c.innerText,D:null==d?void 0:d.innerText}}}];var Z=class extends X.implements(L,G,{get queries(){return this.settings}},{deduceInputs:H,constructor:I,__$id:1631224845}){};module.exports["163122484541"]=G;module.exports["163122484571"]=Z;

//# sourceMappingURL=internal.js.map