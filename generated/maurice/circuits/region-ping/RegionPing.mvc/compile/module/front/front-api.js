import { RegionPingDisplay, RegionPingScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.RegionPingDisplay} */
export { RegionPingDisplay }
/** @lazy @api {xyz.swapee.wc.RegionPingScreen} */
export { RegionPingScreen }