import AbstractHyperRegionPingInterruptLine from '../../../../gen/AbstractRegionPingInterruptLine/hyper/AbstractHyperRegionPingInterruptLine'
import RegionPingInterruptLine from '../../RegionPingInterruptLine'
import RegionPingInterruptLineGeneralAspects from '../RegionPingInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperRegionPingInterruptLine} */
export default class extends AbstractHyperRegionPingInterruptLine
 .consults(
  RegionPingInterruptLineGeneralAspects,
 )
 .implements(
  RegionPingInterruptLine,
 )
{}