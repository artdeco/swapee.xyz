const d=new Date
let _RegionPingPage,_regionPingArcsIds,_regionPingMethodsIds
// _getRegionPing

// const PROD=true
const PROD=false

;({
 RegionPingPage:_RegionPingPage,
 // getRegionPing:_getRegionPing,
 regionPingArcsIds:_regionPingArcsIds,
 regionPingMethodsIds:_regionPingMethodsIds,
}=require(PROD?'xyz/swapee/rc/region-ping/prod':'./region-ping.page'))

const dd=new Date
console.log(`${PROD?`📦`:`📝`} Loaded %s in %sms`,'region-ping',dd.getTime()-d.getTime())

import 'xyz/swapee/rc/region-ping'

export const RegionPingPage=/**@type {typeof xyz.swapee.rc.RegionPingPage}*/(_RegionPingPage)
export const regionPingArcsIds=/**@type {typeof xyz.swapee.rc.regionPingArcsIds}*/(_regionPingArcsIds)
export const regionPingMethodsIds=/**@type {typeof xyz.swapee.rc.regionPingMethodsIds}*/(_regionPingMethodsIds)
// export const getRegionPing=/**@type {xyz.swapee.rc.getRegionPing}*/(_getRegionPing)