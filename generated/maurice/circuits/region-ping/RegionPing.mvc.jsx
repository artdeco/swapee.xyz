/** @extends {xyz.swapee.wc.AbstractRegionPing} */
export default class AbstractRegionPing extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractRegionPingComputer} */
export class AbstractRegionPingComputer extends (<computer>
   <adapter name="adaptDifference">
    <xyz.swapee.wc.IRegionPingCore start received />
    <outputs>
     <xyz.swapee.wc.IRegionPingCore diff diff2 />
    </outputs>
   </adapter>
   <adapter name="adaptDifferenceRequired">
    <xyz.swapee.wc.IRegionPingCore start="required" received="required" />
    <outputs>
     <xyz.swapee.wc.IRegionPingCore diff diff2 />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractRegionPingController} */
export class AbstractRegionPingController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractRegionPingPort} */
export class RegionPingPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractRegionPingView} */
export class AbstractRegionPingView extends (<view>
  <classes>
   <string opt name="Reg">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractRegionPingElement} */
export class AbstractRegionPingElement extends (<element v3 html mv>
 <block src="./RegionPing.mvc/src/RegionPingElement/methods/render.jsx" />
 <inducer src="./RegionPing.mvc/src/RegionPingElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractRegionPingHtmlComponent} */
export class AbstractRegionPingHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>