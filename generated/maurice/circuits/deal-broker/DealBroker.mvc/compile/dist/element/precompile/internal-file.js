/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const b=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=b["372700389811"];function g(a,c,e,f){return b["372700389812"](a,c,e,f,!1,void 0)};function h(){}h.prototype={};class aa{}class k extends g(aa,"IDealBrokerProcessor",null,{U:1,ia:2}){}k[d]=[h,{}];

const l=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const m=l["615055805212"],ba=l["615055805218"],n=l["615055805233"];const p={g:"b370a",l:"2568a",v:"5fd9c",A:"96f44",X:"86cc4",Y:"565af",Z:"10a9a",D:"4685c",m:"5f4a3",o:"341da",j:"8ca05",i:"ea0a0",V:"96c88",H:"748e6",W:"c23cd",u:"ae0d3",s:"d0b0c",rate:"67942"};const q={F:"c63f7"};function r(){}r.prototype={};function ca(){this.model={F:""}}class da{}class t extends g(da,"IDealBrokerCore",ca,{K:1,da:2}){}function D(){}D.prototype={};function E(){this.model={g:!1,m:"",o:!1,l:"",v:"",A:"",rate:"",j:"",i:"",D:"",u:"",s:""}}class ea{}class F extends g(ea,"IDealBrokerOuterCore",E,{R:1,ga:2}){}F[d]=[D,{constructor(){n(this.model,"",p);n(this.model,"",q)}}];t[d]=[{},r,F];
const G=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const fa=G.IntegratedComponentInitialiser,H=G.IntegratedComponent,ha=G["95173443851"];function I(){}I.prototype={};class ia{}class J extends g(ia,"IDealBrokerComputer",null,{J:1,aa:2}){}J[d]=[I,ba];const K={regulate:m({g:Boolean,m:String,o:Boolean,l:String,v:String,A:String,rate:String,j:String,i:String,D:String,u:String,s:String})};
const L=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ja=L.IntegratedController,ka=L.Parametric;const M={...p};function N(){}N.prototype={};function la(){const a={model:null};E.call(a);this.inputs=a.model}class ma{}class O extends g(ma,"IDealBrokerPort",la,{T:1,ha:2}){}function P(){}O[d]=[P.prototype={},N,ka,P.prototype={constructor(){n(this.inputs,"",M)}}];function Q(){}Q.prototype={};class na{}class R extends g(na,"IDealBrokerController",null,{G:1,ba:2}){}function S(){}R[d]=[S.prototype={},Q,{calibrate:function({g:a}){a&&setTimeout(()=>{const {G:{setInputs:c}}=this;c({g:!1})},1)}},K,ja,{get Port(){return O}},S.prototype={}];const oa=m({F:String},{silent:!0});const pa=Object.keys(q).reduce((a,c)=>{a[q[c]]=c;return a},{});function T(){}T.prototype={};class qa{}class U extends g(qa,"IDealBroker",null,{I:1,$:2}){}U[d]=[T,t,k,H,J,R,{regulateState:oa,stateQPs:pa}];function ra(){return{}};const sa=require(eval('"@type.engineering/web-computing"')).h;function ta(){return sa("div",{$id:"DealBroker"})};const ua=require(eval('"@type.engineering/web-computing"')).h;function va(){return ua("div",{$id:"DealBroker"})};var V=class extends R.implements(){};require("https");require("http");const wa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}wa("aqt");require("fs");require("child_process");
const W=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const xa=W.ElementBase,ya=W.HTMLBlocker;function X(){}X.prototype={};function za(){this.inputs={noSolder:!1}}class Aa{}class Ba extends g(Aa,"IDealBrokerElementPort",za,{P:1,fa:2}){}Ba[d]=[X,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"get-offer":void 0,"get-offer-error":void 0,"getting-offer":void 0,"fixed-id":void 0,"network-fee":void 0,"partner-fee":void 0,"estimated-float-amount-to":void 0,"estimated-fixed-amount-to":void 0,"visible-amount":void 0,"min-amount":void 0,"max-amount":void 0})}}];function Ca(){}Ca.prototype={};class Da{}class Y extends g(Da,"IDealBrokerElement",null,{M:1,ea:2}){}function Z(){}
Y[d]=[Ca,xa,Z.prototype={calibrate:function({":no-solder":a,":get-offer":c,":get-offer-error":e,":getting-offer":f,":fixed-id":u,":network-fee":v,":partner-fee":w,":rate":x,":estimated-float-amount-to":y,":estimated-fixed-amount-to":z,":visible-amount":A,":min-amount":B,":max-amount":C}){const {attributes:{"no-solder":Ea,"get-offer":Fa,"get-offer-error":Ga,"getting-offer":Ha,"fixed-id":Ia,"network-fee":Ja,"partner-fee":Ka,rate:La,"estimated-float-amount-to":Ma,"estimated-fixed-amount-to":Na,"visible-amount":Oa,
"min-amount":Pa,"max-amount":Qa}}=this;return{...(void 0===Ea?{"no-solder":a}:{}),...(void 0===Fa?{"get-offer":c}:{}),...(void 0===Ga?{"get-offer-error":e}:{}),...(void 0===Ha?{"getting-offer":f}:{}),...(void 0===Ia?{"fixed-id":u}:{}),...(void 0===Ja?{"network-fee":v}:{}),...(void 0===Ka?{"partner-fee":w}:{}),...(void 0===La?{rate:x}:{}),...(void 0===Ma?{"estimated-float-amount-to":y}:{}),...(void 0===Na?{"estimated-fixed-amount-to":z}:{}),...(void 0===Oa?{"visible-amount":A}:{}),...(void 0===Pa?
{"min-amount":B}:{}),...(void 0===Qa?{"max-amount":C}:{})}}},Z.prototype={calibrate:({"no-solder":a,"get-offer":c,"get-offer-error":e,"getting-offer":f,"fixed-id":u,"network-fee":v,"partner-fee":w,rate:x,"estimated-float-amount-to":y,"estimated-fixed-amount-to":z,"visible-amount":A,"min-amount":B,"max-amount":C})=>({noSolder:a,g:c,m:e,o:f,l:u,v,A:w,rate:x,j:y,i:z,D:A,u:B,s:C})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={inputsPQs:M,cachePQs:q,vdus:{}},H,Z.prototype={constructor(){Object.assign(this,
{knownInputs:new Set("noSolder getOffer getOfferError gettingOffer fixedId networkFee partnerFee rate estimatedFloatAmountTo estimatedFixedAmountTo visibleAmount minAmount maxAmount no-solder :no-solder get-offer :get-offer get-offer-error :get-offer-error getting-offer :getting-offer fixed-id :fixed-id network-fee :network-fee partner-fee :partner-fee :rate estimated-float-amount-to :estimated-float-amount-to estimated-fixed-amount-to :estimated-fixed-amount-to visible-amount :visible-amount min-amount :min-amount max-amount :max-amount fe646 b370a 5f4a3 341da 2568a 5fd9c 96f44 67942 8ca05 ea0a0 4685c ae0d3 d0b0c children".split(" "))})},
get Port(){return Ba}}];Y[d]=[U,{rootId:"DealBroker",__$id:3427226314,fqn:"xyz.swapee.wc.IDealBroker",maurice_element_v3:!0}];class Ra extends Y.implements(V,ha,ya,fa,{solder:ra,server:ta,render:va},{classesMap:!0,rootSelector:".DealBroker",stylesheet:"html/styles/DealBroker.css",blockName:"html/DealBrokerBlock.html"},{}){};module.exports["34272263140"]=U;module.exports["34272263141"]=U;module.exports["34272263143"]=O;module.exports["34272263144"]=R;module.exports["34272263148"]=Ra;module.exports["342722631411"]=K;module.exports["342722631430"]=J;module.exports["342722631461"]=V;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['3427226314']=module.exports