/**
 * An abstract class of `xyz.swapee.wc.IDealBroker` interface.
 * @extends {xyz.swapee.wc.AbstractDealBroker}
 */
class AbstractDealBroker extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IDealBroker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.DealBrokerPort}
 */
class DealBrokerPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerController` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerController}
 */
class AbstractDealBrokerController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IDealBroker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.DealBrokerElement}
 */
class DealBrokerElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.DealBrokerBuffer}
 */
class DealBrokerBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerComputer` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerComputer}
 */
class AbstractDealBrokerComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.DealBrokerController}
 */
class DealBrokerController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractDealBroker = AbstractDealBroker
module.exports.DealBrokerPort = DealBrokerPort
module.exports.AbstractDealBrokerController = AbstractDealBrokerController
module.exports.DealBrokerElement = DealBrokerElement
module.exports.DealBrokerBuffer = DealBrokerBuffer
module.exports.AbstractDealBrokerComputer = AbstractDealBrokerComputer
module.exports.DealBrokerController = DealBrokerController

Object.defineProperties(module.exports, {
 'AbstractDealBroker': {get: () => require('./precompile/internal')[34272263141]},
 [34272263141]: {get: () => module.exports['AbstractDealBroker']},
 'DealBrokerPort': {get: () => require('./precompile/internal')[34272263143]},
 [34272263143]: {get: () => module.exports['DealBrokerPort']},
 'AbstractDealBrokerController': {get: () => require('./precompile/internal')[34272263144]},
 [34272263144]: {get: () => module.exports['AbstractDealBrokerController']},
 'DealBrokerElement': {get: () => require('./precompile/internal')[34272263148]},
 [34272263148]: {get: () => module.exports['DealBrokerElement']},
 'DealBrokerBuffer': {get: () => require('./precompile/internal')[342722631411]},
 [342722631411]: {get: () => module.exports['DealBrokerBuffer']},
 'AbstractDealBrokerComputer': {get: () => require('./precompile/internal')[342722631430]},
 [342722631430]: {get: () => module.exports['AbstractDealBrokerComputer']},
 'DealBrokerController': {get: () => require('./precompile/internal')[342722631461]},
 [342722631461]: {get: () => module.exports['DealBrokerController']},
})