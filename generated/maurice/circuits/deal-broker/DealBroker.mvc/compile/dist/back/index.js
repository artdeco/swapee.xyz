/**
 * An abstract class of `xyz.swapee.wc.IDealBroker` interface.
 * @extends {xyz.swapee.wc.AbstractDealBroker}
 */
class AbstractDealBroker extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IDealBroker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.DealBrokerPort}
 */
class DealBrokerPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerController` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerController}
 */
class AbstractDealBrokerController extends (class {/* lazy-loaded */}) {}
/**
 * The _IDealBroker_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.DealBrokerHtmlComponent}
 */
class DealBrokerHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.DealBrokerBuffer}
 */
class DealBrokerBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerComputer` interface.
 * @extends {xyz.swapee.wc.AbstractDealBrokerComputer}
 */
class AbstractDealBrokerComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.DealBrokerComputer}
 */
class DealBrokerComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.DealBrokerController}
 */
class DealBrokerController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractDealBroker = AbstractDealBroker
module.exports.DealBrokerPort = DealBrokerPort
module.exports.AbstractDealBrokerController = AbstractDealBrokerController
module.exports.DealBrokerHtmlComponent = DealBrokerHtmlComponent
module.exports.DealBrokerBuffer = DealBrokerBuffer
module.exports.AbstractDealBrokerComputer = AbstractDealBrokerComputer
module.exports.DealBrokerComputer = DealBrokerComputer
module.exports.DealBrokerController = DealBrokerController