import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractDealBroker}*/
export class AbstractDealBroker extends Module['34272263141'] {}
/** @type {typeof xyz.swapee.wc.AbstractDealBroker} */
AbstractDealBroker.class=function(){}
/** @type {typeof xyz.swapee.wc.DealBrokerPort} */
export const DealBrokerPort=Module['34272263143']
/**@extends {xyz.swapee.wc.AbstractDealBrokerController}*/
export class AbstractDealBrokerController extends Module['34272263144'] {}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerController} */
AbstractDealBrokerController.class=function(){}
/** @type {typeof xyz.swapee.wc.DealBrokerHtmlComponent} */
export const DealBrokerHtmlComponent=Module['342722631410']
/** @type {typeof xyz.swapee.wc.DealBrokerBuffer} */
export const DealBrokerBuffer=Module['342722631411']
/**@extends {xyz.swapee.wc.AbstractDealBrokerComputer}*/
export class AbstractDealBrokerComputer extends Module['342722631430'] {}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerComputer} */
AbstractDealBrokerComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.DealBrokerComputer} */
export const DealBrokerComputer=Module['342722631431']
/** @type {typeof xyz.swapee.wc.back.DealBrokerController} */
export const DealBrokerController=Module['342722631461']