/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const c=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const d=c["372700389811"];function f(a,b,e,ea){return c["372700389812"](a,b,e,ea,!1,void 0)};function g(){}g.prototype={J(){const {asIIntegratedController:{setInputs:a}}=this;a({j:!0})}};class aa{}class h extends f(aa,"IDealBrokerProcessor",null,{ma:1,Fa:2}){}h[d]=[g,function(){}.prototype={I(){const {H:{model:{l:a},setInfo:b}}=this;b({l:!a})}}];/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ba=k["61505580523"],l=k["615055805212"],ca=k["615055805218"],da=k["615055805221"],fa=k["615055805223"],m=k["615055805233"];const n={j:"b370a",C:"2568a",o:"5fd9c",s:"96f44",sa:"86cc4",ta:"565af",ua:"10a9a",u:"4685c",m:"5f4a3",l:"341da",i:"8ca05",h:"ea0a0",qa:"96c88",da:"748e6",ra:"c23cd",F:"ae0d3",D:"d0b0c",rate:"67942"};const p={v:"c63f7"};function q(){}q.prototype={};function ha(){this.model={v:""}}class ia{}class r extends f(ia,"IDealBrokerCore",ha,{ga:1,za:2}){}function t(){}t.prototype={};function u(){this.model={j:!1,m:"",l:!1,C:"",o:"",s:"",rate:"",i:"",h:"",u:"",F:"",D:""}}class ja{}class v extends f(ja,"IDealBrokerOuterCore",u,{ka:1,Da:2}){}v[d]=[t,function(){}.prototype={constructor(){m(this.model,"",n);m(this.model,"",p)}}];r[d]=[function(){}.prototype={},q,v];/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you'll see boundary comments like
@object-code-[start|end] around chunks), proprietary software from the package:
*/
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();/*

@LICENSE @type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
*/
const ka=w.IntegratedController,la=w.Parametric;/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const x=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ma=x.IntegratedComponentInitialiser,na=x.IntegratedComponent;function y(){}y.prototype={};class oa{}class z extends f(oa,"IDealBrokerComputer",null,{H:1,xa:2}){}z[d]=[y,ca];const A={regulate:l({j:Boolean,m:String,l:Boolean,C:String,o:String,s:String,rate:String,i:String,h:String,u:String,F:String,D:String})};const B={...n};function C(){}C.prototype={};function D(){const a={model:null};u.call(a);this.inputs=a.model}class pa{}class E extends f(pa,"IDealBrokerPort",D,{la:1,Ea:2}){}function F(){}E[d]=[F.prototype={A(){D.call(this)}},C,la,F.prototype={constructor(){m(this.inputs,"",B)}}];function G(){}G.prototype={};class qa{}class H extends f(qa,"IDealBrokerController",null,{g:1,V:2}){}function I(){}
H[d]=[I.prototype={A(){this.port.A()}},G,{calibrate:function({j:a}){a&&setTimeout(()=>{const {g:{setInputs:b}}=this;b({j:!1})},1)}},A,ka,{get Port(){return E}},I.prototype={M(a){const {g:{setInputs:b}}=this;b({m:a})},O(){const {g:{setInputs:a}}=this;a({l:!0})},P(a){const {g:{setInputs:b}}=this;b({o:a})},R(a){const {g:{setInputs:b}}=this;b({s:a})},T(a){const {g:{setInputs:b}}=this;b({rate:a})},L(a){const {g:{setInputs:b}}=this;b({i:a})},K(a){const {g:{setInputs:b}}=this;b({h:a})},U(a){const {g:{setInputs:b}}=
this;b({u:a})},Y(){const {g:{setInputs:a}}=this;a({m:""})},Z(){const {g:{setInputs:a}}=this;a({l:!1})},$(){const {g:{setInputs:a}}=this;a({o:""})},aa(){const {g:{setInputs:a}}=this;a({s:""})},ba(){const {g:{setInputs:a}}=this;a({rate:""})},X(){const {g:{setInputs:a}}=this;a({i:""})},W(){const {g:{setInputs:a}}=this;a({h:""})},ca(){const {g:{setInputs:a}}=this;a({u:""})}}];const ra=l({v:String},{silent:!0});const sa=Object.keys(p).reduce((a,b)=>{a[p[b]]=b;return a},{});function J(){}J.prototype={};class ta{}class K extends f(ta,"IDealBroker",null,{ea:1,wa:2}){}K[d]=[J,r,h,na,z,H,function(){}.prototype={regulateState:ra,stateQPs:sa}];/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const L=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();/*

@LICENSE @webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const ua=L["12817393923"],va=L["12817393924"],wa=L["12817393925"],xa=L["12817393926"];function M(){}M.prototype={};class ya{}class N extends f(ya,"IDealBrokerControllerAR",null,{fa:1,ya:2}){}N[d]=[M,xa,function(){}.prototype={allocator(){this.methods={M:"642e3",Y:"a602e",I:"ae3ff",O:"4c6a6",Z:"884f8",P:"ebcab",$:"691ce",R:"38c56",aa:"09ede",T:"7b58a",ba:"acaf8",L:"922ab",X:"be6e2",K:"2738f",W:"9854b",U:"8f918",ca:"27889",J:"e0519"}}}];function O(){}O.prototype={};class za{}class P extends f(za,"IDealBrokerController",null,{g:1,V:2}){}P[d]=[O,H,N,ua];var Q=class extends P.implements(){};function Aa({h:a,i:b}){return{v:a||b}};function Ba(a,b,e){a={i:a.i,h:a.h};a=e?e(a):a;b=e?e(b):b;return this.G(a,b)};class R extends z.implements({G:Aa,adapt:[Ba]}){};function S(){}S.prototype={};class Ca{}class T extends f(Ca,"IDealBrokerDisplay",null,{ha:1,Aa:2}){}T[d]=[S,va];const U={};const Da=Object.keys(U).reduce((a,b)=>{a[U[b]]=b;return a},{});function V(){}V.prototype={};class Ea{}class W extends f(Ea,"IDealBrokerGPU",null,{ia:1,Ba:2}){}W[d]=[V,function(){}.prototype={vdusQPs:Da,memoryPQs:n},T,ba];function X(){}X.prototype={};class Fa{}class Y extends f(Fa,"IDealBrokerScreenAT",null,{pa:1,Ha:2}){}Y[d]=[X,wa];function Z(){}Z.prototype={};class Ga{}class Ha extends f(Ga,"IDealBrokerScreen",null,{oa:1,Ga:2}){}Ha[d]=[Z,Y];const Ia=Object.keys(B).reduce((a,b)=>{a[B[b]]=b;return a},{});function Ja(){}Ja.prototype={};class Ka{static mvc(a,b,e){return fa(this,a,b,null,e)}}class La extends f(Ka,"IDealBrokerHtmlComponent",null,{ja:1,Ca:2}){}function Ma(){}La[d]=[Ja,da,K,W,Ha,Ma.prototype={inputsQPs:Ia},Ma.prototype={paint({j:a}){const {g:{A:b}}=this;a&&b()}}];var Na=class extends La.implements(Q,R,ma){};module.exports["34272263140"]=K;module.exports["34272263141"]=K;module.exports["34272263143"]=E;module.exports["34272263144"]=H;module.exports["342722631410"]=Na;module.exports["342722631411"]=A;module.exports["342722631430"]=z;module.exports["342722631431"]=R;module.exports["342722631461"]=Q;

//# sourceMappingURL=internal.js.map