/**
 * Display for presenting information from the _IDealBroker_.
 * @extends {xyz.swapee.wc.DealBrokerDisplay}
 */
class DealBrokerDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.DealBrokerScreen}
 */
class DealBrokerScreen extends (class {/* lazy-loaded */}) {}

module.exports.DealBrokerDisplay = DealBrokerDisplay
module.exports.DealBrokerScreen = DealBrokerScreen