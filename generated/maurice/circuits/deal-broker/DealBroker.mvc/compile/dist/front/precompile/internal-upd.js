import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {3427226314} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const f=e["372700389811"];function g(a,b,y){return e["372700389812"](a,b,null,y,!1,void 0)};
const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const k=h["61893096584"],l=h["61893096586"],m=h["618930965811"],n=h["618930965812"],p=h["618930965819"];function q(){}q.prototype={};class r{}class t extends g(r,"IDealBrokerDisplay",{i:1,P:2}){}t[f]=[q,k];var u=class extends t.implements(){};function v(){};function w(){}w.prototype={};class x{}class z extends g(x,"IDealBrokerControllerAT",{h:1,O:2}){}z[f]=[w,m,{}];function A(){}A.prototype={};class B{}class C extends g(B,"IDealBrokerScreenAR",{l:1,T:2}){}C[f]=[A,n,{allocator(){this.methods={}}}];const D={G:"b370a",F:"2568a",L:"5fd9c",M:"96f44",A:"86cc4",C:"565af",D:"10a9a",U:"4685c",H:"5f4a3",I:"341da",v:"8ca05",u:"ea0a0",m:"96c88",g:"748e6",o:"c23cd",K:"ae0d3",J:"d0b0c",rate:"67942"};const E={...D};const F=Object.keys(D).reduce((a,b)=>{a[D[b]]=b;return a},{});const G={s:"c63f7"};const H=Object.keys(G).reduce((a,b)=>{a[G[b]]=b;return a},{});function I(){}I.prototype={};class J{}class K extends g(J,"IDealBrokerScreen",{j:1,R:2}){}function L(){}K[f]=[I,L.prototype={inputsPQs:E,memoryQPs:F,cacheQPs:H},l,p,C,L.prototype={vdusPQs:{}}];var M=class extends K.implements(z,u,{get queries(){return this.settings}},{deduceInputs:v,__$id:3427226314}){};module.exports["342722631441"]=u;module.exports["342722631471"]=M;
/*! @embed-object-end {3427226314} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule