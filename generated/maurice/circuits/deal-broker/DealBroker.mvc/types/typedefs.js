/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IDealBrokerComputer={}
xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo={}
xyz.swapee.wc.IDealBrokerOuterCore={}
xyz.swapee.wc.IDealBrokerOuterCore.Model={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOffer={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOfferError={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.GettingOffer={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.FixedId={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.NetworkFee={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.PartnerFee={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.Rate={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFloatAmountTo={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFixedAmountTo={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.VisibleAmount={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.MinAmount={}
xyz.swapee.wc.IDealBrokerOuterCore.Model.MaxAmount={}
xyz.swapee.wc.IDealBrokerOuterCore.WeakModel={}
xyz.swapee.wc.IDealBrokerPort={}
xyz.swapee.wc.IDealBrokerPort.Inputs={}
xyz.swapee.wc.IDealBrokerPort.WeakInputs={}
xyz.swapee.wc.IDealBrokerCore={}
xyz.swapee.wc.IDealBrokerCore.Model={}
xyz.swapee.wc.IDealBrokerCore.Model.EstimatedAmountTo={}
xyz.swapee.wc.IDealBrokerPortInterface={}
xyz.swapee.wc.IDealBrokerProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IDealBrokerController={}
xyz.swapee.wc.front.IDealBrokerControllerAT={}
xyz.swapee.wc.front.IDealBrokerScreenAR={}
xyz.swapee.wc.IDealBroker={}
xyz.swapee.wc.IDealBrokerHtmlComponent={}
xyz.swapee.wc.IDealBrokerElement={}
xyz.swapee.wc.IDealBrokerElementPort={}
xyz.swapee.wc.IDealBrokerElementPort.Inputs={}
xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IDealBrokerElementPort.WeakInputs={}
xyz.swapee.wc.IDealBrokerDesigner={}
xyz.swapee.wc.IDealBrokerDesigner.communicator={}
xyz.swapee.wc.IDealBrokerDesigner.relay={}
xyz.swapee.wc.IDealBrokerDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IDealBrokerDisplay={}
xyz.swapee.wc.back.IDealBrokerController={}
xyz.swapee.wc.back.IDealBrokerControllerAR={}
xyz.swapee.wc.back.IDealBrokerScreen={}
xyz.swapee.wc.back.IDealBrokerScreenAT={}
xyz.swapee.wc.IDealBrokerController={}
xyz.swapee.wc.IDealBrokerScreen={}
xyz.swapee.wc.IDealBrokerGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/02-IDealBrokerComputer.xml}  e9a10f8d7ead9cedec78d6ef1ecffe29 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IDealBrokerComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerComputer)} xyz.swapee.wc.AbstractDealBrokerComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerComputer} xyz.swapee.wc.DealBrokerComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerComputer` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerComputer
 */
xyz.swapee.wc.AbstractDealBrokerComputer = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerComputer.constructor&xyz.swapee.wc.DealBrokerComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerComputer.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerComputer.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerComputer}
 */
xyz.swapee.wc.AbstractDealBrokerComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerComputer}
 */
xyz.swapee.wc.AbstractDealBrokerComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerComputer}
 */
xyz.swapee.wc.AbstractDealBrokerComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerComputer}
 */
xyz.swapee.wc.AbstractDealBrokerComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerComputer.Initialese[]) => xyz.swapee.wc.IDealBrokerComputer} xyz.swapee.wc.DealBrokerComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.DealBrokerMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IDealBrokerComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IDealBrokerComputer
 */
xyz.swapee.wc.IDealBrokerComputer = class extends /** @type {xyz.swapee.wc.IDealBrokerComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo} */
xyz.swapee.wc.IDealBrokerComputer.prototype.adaptEstimatedAmountTo = function() {}
/** @type {xyz.swapee.wc.IDealBrokerComputer.compute} */
xyz.swapee.wc.IDealBrokerComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerComputer.Initialese>)} xyz.swapee.wc.DealBrokerComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerComputer} xyz.swapee.wc.IDealBrokerComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IDealBrokerComputer_ instances.
 * @constructor xyz.swapee.wc.DealBrokerComputer
 * @implements {xyz.swapee.wc.IDealBrokerComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerComputer.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerComputer = class extends /** @type {xyz.swapee.wc.DealBrokerComputer.constructor&xyz.swapee.wc.IDealBrokerComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerComputer}
 */
xyz.swapee.wc.DealBrokerComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerComputer} */
xyz.swapee.wc.RecordIDealBrokerComputer

/** @typedef {xyz.swapee.wc.IDealBrokerComputer} xyz.swapee.wc.BoundIDealBrokerComputer */

/** @typedef {xyz.swapee.wc.DealBrokerComputer} xyz.swapee.wc.BoundDealBrokerComputer */

/**
 * Contains getters to cast the _IDealBrokerComputer_ interface.
 * @interface xyz.swapee.wc.IDealBrokerComputerCaster
 */
xyz.swapee.wc.IDealBrokerComputerCaster = class { }
/**
 * Cast the _IDealBrokerComputer_ instance into the _BoundIDealBrokerComputer_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerComputer}
 */
xyz.swapee.wc.IDealBrokerComputerCaster.prototype.asIDealBrokerComputer
/**
 * Access the _DealBrokerComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerComputer}
 */
xyz.swapee.wc.IDealBrokerComputerCaster.prototype.superDealBrokerComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Form, changes: xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Form) => (void|xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Return)} xyz.swapee.wc.IDealBrokerComputer.__adaptEstimatedAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerComputer.__adaptEstimatedAmountTo<!xyz.swapee.wc.IDealBrokerComputer>} xyz.swapee.wc.IDealBrokerComputer._adaptEstimatedAmountTo */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo} */
/**
 * @param {!xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Form} form The form with inputs.
 * - `estimatedFloatAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). ⤴ *IDealBrokerOuterCore.Model.EstimatedFloatAmountTo_Safe*
 * - `estimatedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. ⤴ *IDealBrokerOuterCore.Model.EstimatedFixedAmountTo_Safe*
 * @param {xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Form} changes The previous values of the form.
 * - `estimatedFloatAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). ⤴ *IDealBrokerOuterCore.Model.EstimatedFloatAmountTo_Safe*
 * - `estimatedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. ⤴ *IDealBrokerOuterCore.Model.EstimatedFixedAmountTo_Safe*
 * @return {void|xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Return} The form with outputs.
 */
xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IDealBrokerCore.Model.EstimatedFloatAmountTo_Safe&xyz.swapee.wc.IDealBrokerCore.Model.EstimatedFixedAmountTo_Safe} xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IDealBrokerCore.Model.EstimatedAmountTo} xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.DealBrokerMemory) => void} xyz.swapee.wc.IDealBrokerComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerComputer.__compute<!xyz.swapee.wc.IDealBrokerComputer>} xyz.swapee.wc.IDealBrokerComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.DealBrokerMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/03-IDealBrokerOuterCore.xml}  fab5ebe57a8016957e25a545d43faf08 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IDealBrokerOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerOuterCore)} xyz.swapee.wc.AbstractDealBrokerOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerOuterCore} xyz.swapee.wc.DealBrokerOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerOuterCore
 */
xyz.swapee.wc.AbstractDealBrokerOuterCore = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerOuterCore.constructor&xyz.swapee.wc.DealBrokerOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerOuterCore.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IDealBrokerOuterCore|typeof xyz.swapee.wc.DealBrokerOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IDealBrokerOuterCore|typeof xyz.swapee.wc.DealBrokerOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IDealBrokerOuterCore|typeof xyz.swapee.wc.DealBrokerOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerOuterCore}
 */
xyz.swapee.wc.AbstractDealBrokerOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerOuterCoreCaster)} xyz.swapee.wc.IDealBrokerOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IDealBroker_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IDealBrokerOuterCore
 */
xyz.swapee.wc.IDealBrokerOuterCore = class extends /** @type {xyz.swapee.wc.IDealBrokerOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerOuterCore.prototype.constructor = xyz.swapee.wc.IDealBrokerOuterCore

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerOuterCore.Initialese>)} xyz.swapee.wc.DealBrokerOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerOuterCore} xyz.swapee.wc.IDealBrokerOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IDealBrokerOuterCore_ instances.
 * @constructor xyz.swapee.wc.DealBrokerOuterCore
 * @implements {xyz.swapee.wc.IDealBrokerOuterCore} The _IDealBroker_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerOuterCore = class extends /** @type {xyz.swapee.wc.DealBrokerOuterCore.constructor&xyz.swapee.wc.IDealBrokerOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerOuterCore.prototype.constructor = xyz.swapee.wc.DealBrokerOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerOuterCore}
 */
xyz.swapee.wc.DealBrokerOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerOuterCore.
 * @interface xyz.swapee.wc.IDealBrokerOuterCoreFields
 */
xyz.swapee.wc.IDealBrokerOuterCoreFields = class { }
/**
 * The _IDealBroker_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IDealBrokerOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IDealBrokerOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore} */
xyz.swapee.wc.RecordIDealBrokerOuterCore

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore} xyz.swapee.wc.BoundIDealBrokerOuterCore */

/** @typedef {xyz.swapee.wc.DealBrokerOuterCore} xyz.swapee.wc.BoundDealBrokerOuterCore */

/**
 * Start loading offer.
 * @typedef {boolean}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOffer.getOffer

/**
 * An error while getting either fixed or floating-rate offer.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOfferError.getOfferError

/**
 * Whether loading either fixed or floating rate offer.
 * @typedef {boolean}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.GettingOffer.gettingOffer

/**
 * The ID of a fixed-rate transaction for which this rate applies.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.FixedId.fixedId

/**
 *   The fee charged by the miners when crypto money to the user, set by the
 * exchange.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.NetworkFee.networkFee

/**
 * The fee that the partner is going to keep for themselves.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.PartnerFee.partnerFee

/**
 * The rate.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.Rate.rate

/**
 *   The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFloatAmountTo.estimatedFloatAmountTo

/**
 *   The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFixedAmountTo.estimatedFixedAmountTo

/**
 * The amount before all fees.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.VisibleAmount.visibleAmount

/**
 * The minimum required amount extracted from the error.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.MinAmount.minAmount

/**
 * The maximum amount that can be exchanged extracted from the error.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerOuterCore.Model.MaxAmount.maxAmount

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOffer&xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOfferError&xyz.swapee.wc.IDealBrokerOuterCore.Model.GettingOffer&xyz.swapee.wc.IDealBrokerOuterCore.Model.FixedId&xyz.swapee.wc.IDealBrokerOuterCore.Model.NetworkFee&xyz.swapee.wc.IDealBrokerOuterCore.Model.PartnerFee&xyz.swapee.wc.IDealBrokerOuterCore.Model.Rate&xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFloatAmountTo&xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFixedAmountTo&xyz.swapee.wc.IDealBrokerOuterCore.Model.VisibleAmount&xyz.swapee.wc.IDealBrokerOuterCore.Model.MinAmount&xyz.swapee.wc.IDealBrokerOuterCore.Model.MaxAmount} xyz.swapee.wc.IDealBrokerOuterCore.Model The _IDealBroker_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOffer&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOfferError&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GettingOffer&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.FixedId&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.NetworkFee&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.PartnerFee&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.Rate&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFloatAmountTo&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFixedAmountTo&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.VisibleAmount&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MinAmount&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MaxAmount} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel The _IDealBroker_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IDealBrokerOuterCore_ interface.
 * @interface xyz.swapee.wc.IDealBrokerOuterCoreCaster
 */
xyz.swapee.wc.IDealBrokerOuterCoreCaster = class { }
/**
 * Cast the _IDealBrokerOuterCore_ instance into the _BoundIDealBrokerOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerOuterCore}
 */
xyz.swapee.wc.IDealBrokerOuterCoreCaster.prototype.asIDealBrokerOuterCore
/**
 * Access the _DealBrokerOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerOuterCore}
 */
xyz.swapee.wc.IDealBrokerOuterCoreCaster.prototype.superDealBrokerOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOffer Start loading offer (optional overlay).
 * @prop {boolean} [getOffer=false] Start loading offer. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOffer_Safe Start loading offer (required overlay).
 * @prop {boolean} getOffer Start loading offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOfferError An error while getting either fixed or floating-rate offer (optional overlay).
 * @prop {string} [getOfferError=""] An error while getting either fixed or floating-rate offer. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOfferError_Safe An error while getting either fixed or floating-rate offer (required overlay).
 * @prop {string} getOfferError An error while getting either fixed or floating-rate offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.GettingOffer Whether loading either fixed or floating rate offer (optional overlay).
 * @prop {boolean} [gettingOffer=false] Whether loading either fixed or floating rate offer. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay).
 * @prop {boolean} gettingOffer Whether loading either fixed or floating rate offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.FixedId The ID of a fixed-rate transaction for which this rate applies (optional overlay).
 * @prop {string} [fixedId=""] The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.FixedId_Safe The ID of a fixed-rate transaction for which this rate applies (required overlay).
 * @prop {string} fixedId The ID of a fixed-rate transaction for which this rate applies.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.NetworkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange (optional overlay).
 * @prop {string} [networkFee=""] The fee charged by the miners when crypto money to the user, set by the
 * exchange. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.NetworkFee_Safe The fee charged by the miners when crypto money to the user, set by the
 * exchange (required overlay).
 * @prop {string} networkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.PartnerFee The fee that the partner is going to keep for themselves (optional overlay).
 * @prop {string} [partnerFee=""] The fee that the partner is going to keep for themselves. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.PartnerFee_Safe The fee that the partner is going to keep for themselves (required overlay).
 * @prop {string} partnerFee The fee that the partner is going to keep for themselves.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.Rate The rate (optional overlay).
 * @prop {string} [rate=""] The rate. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.Rate_Safe The rate (required overlay).
 * @prop {string} rate The rate.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (optional overlay).
 * @prop {string} [estimatedFloatAmountTo=""] The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFloatAmountTo_Safe The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (required overlay).
 * @prop {string} estimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (optional overlay).
 * @prop {string} [estimatedFixedAmountTo=""] The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFixedAmountTo_Safe The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (required overlay).
 * @prop {string} estimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.VisibleAmount The amount before all fees (optional overlay).
 * @prop {string} [visibleAmount=""] The amount before all fees. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.VisibleAmount_Safe The amount before all fees (required overlay).
 * @prop {string} visibleAmount The amount before all fees.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.MinAmount The minimum required amount extracted from the error (optional overlay).
 * @prop {string} [minAmount=""] The minimum required amount extracted from the error. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.MinAmount_Safe The minimum required amount extracted from the error (required overlay).
 * @prop {string} minAmount The minimum required amount extracted from the error.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay).
 * @prop {string} [maxAmount=""] The maximum amount that can be exchanged extracted from the error. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.Model.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay).
 * @prop {string} maxAmount The maximum amount that can be exchanged extracted from the error.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOffer Start loading offer (optional overlay).
 * @prop {*} [getOffer=null] Start loading offer. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOffer_Safe Start loading offer (required overlay).
 * @prop {*} getOffer Start loading offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOfferError An error while getting either fixed or floating-rate offer (optional overlay).
 * @prop {*} [getOfferError=null] An error while getting either fixed or floating-rate offer. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOfferError_Safe An error while getting either fixed or floating-rate offer (required overlay).
 * @prop {*} getOfferError An error while getting either fixed or floating-rate offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GettingOffer Whether loading either fixed or floating rate offer (optional overlay).
 * @prop {*} [gettingOffer=null] Whether loading either fixed or floating rate offer. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay).
 * @prop {*} gettingOffer Whether loading either fixed or floating rate offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.FixedId The ID of a fixed-rate transaction for which this rate applies (optional overlay).
 * @prop {*} [fixedId=null] The ID of a fixed-rate transaction for which this rate applies. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.FixedId_Safe The ID of a fixed-rate transaction for which this rate applies (required overlay).
 * @prop {*} fixedId The ID of a fixed-rate transaction for which this rate applies.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.NetworkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange (optional overlay).
 * @prop {*} [networkFee=null] The fee charged by the miners when crypto money to the user, set by the
 * exchange. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.NetworkFee_Safe The fee charged by the miners when crypto money to the user, set by the
 * exchange (required overlay).
 * @prop {*} networkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.PartnerFee The fee that the partner is going to keep for themselves (optional overlay).
 * @prop {*} [partnerFee=null] The fee that the partner is going to keep for themselves. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.PartnerFee_Safe The fee that the partner is going to keep for themselves (required overlay).
 * @prop {*} partnerFee The fee that the partner is going to keep for themselves.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.Rate The rate (optional overlay).
 * @prop {*} [rate=null] The rate. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.Rate_Safe The rate (required overlay).
 * @prop {*} rate The rate.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (optional overlay).
 * @prop {*} [estimatedFloatAmountTo=null] The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFloatAmountTo_Safe The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (required overlay).
 * @prop {*} estimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (optional overlay).
 * @prop {*} [estimatedFixedAmountTo=null] The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFixedAmountTo_Safe The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (required overlay).
 * @prop {*} estimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.VisibleAmount The amount before all fees (optional overlay).
 * @prop {*} [visibleAmount=null] The amount before all fees. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.VisibleAmount_Safe The amount before all fees (required overlay).
 * @prop {*} visibleAmount The amount before all fees.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MinAmount The minimum required amount extracted from the error (optional overlay).
 * @prop {*} [minAmount=null] The minimum required amount extracted from the error. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MinAmount_Safe The minimum required amount extracted from the error (required overlay).
 * @prop {*} minAmount The minimum required amount extracted from the error.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay).
 * @prop {*} [maxAmount=null] The maximum amount that can be exchanged extracted from the error. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay).
 * @prop {*} maxAmount The maximum amount that can be exchanged extracted from the error.
 */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOffer} xyz.swapee.wc.IDealBrokerPort.Inputs.GetOffer Start loading offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOffer_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.GetOffer_Safe Start loading offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOfferError} xyz.swapee.wc.IDealBrokerPort.Inputs.GetOfferError An error while getting either fixed or floating-rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOfferError_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.GetOfferError_Safe An error while getting either fixed or floating-rate offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GettingOffer} xyz.swapee.wc.IDealBrokerPort.Inputs.GettingOffer Whether loading either fixed or floating rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GettingOffer_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.FixedId} xyz.swapee.wc.IDealBrokerPort.Inputs.FixedId The ID of a fixed-rate transaction for which this rate applies (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.FixedId_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.FixedId_Safe The ID of a fixed-rate transaction for which this rate applies (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.NetworkFee} xyz.swapee.wc.IDealBrokerPort.Inputs.NetworkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.NetworkFee_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.NetworkFee_Safe The fee charged by the miners when crypto money to the user, set by the
 * exchange (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.PartnerFee} xyz.swapee.wc.IDealBrokerPort.Inputs.PartnerFee The fee that the partner is going to keep for themselves (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.PartnerFee_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.PartnerFee_Safe The fee that the partner is going to keep for themselves (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.Rate} xyz.swapee.wc.IDealBrokerPort.Inputs.Rate The rate (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.Rate_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.Rate_Safe The rate (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFloatAmountTo} xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFloatAmountTo_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFloatAmountTo_Safe The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (required overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFixedAmountTo} xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFixedAmountTo_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFixedAmountTo_Safe The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.VisibleAmount} xyz.swapee.wc.IDealBrokerPort.Inputs.VisibleAmount The amount before all fees (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.VisibleAmount_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.VisibleAmount_Safe The amount before all fees (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MinAmount} xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount The minimum required amount extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MinAmount_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount_Safe The minimum required amount extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MaxAmount} xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MaxAmount_Safe} xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOffer} xyz.swapee.wc.IDealBrokerPort.WeakInputs.GetOffer Start loading offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOffer_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.GetOffer_Safe Start loading offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOfferError} xyz.swapee.wc.IDealBrokerPort.WeakInputs.GetOfferError An error while getting either fixed or floating-rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GetOfferError_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.GetOfferError_Safe An error while getting either fixed or floating-rate offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GettingOffer} xyz.swapee.wc.IDealBrokerPort.WeakInputs.GettingOffer Whether loading either fixed or floating rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.GettingOffer_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.FixedId} xyz.swapee.wc.IDealBrokerPort.WeakInputs.FixedId The ID of a fixed-rate transaction for which this rate applies (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.FixedId_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.FixedId_Safe The ID of a fixed-rate transaction for which this rate applies (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.NetworkFee} xyz.swapee.wc.IDealBrokerPort.WeakInputs.NetworkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.NetworkFee_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.NetworkFee_Safe The fee charged by the miners when crypto money to the user, set by the
 * exchange (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.PartnerFee} xyz.swapee.wc.IDealBrokerPort.WeakInputs.PartnerFee The fee that the partner is going to keep for themselves (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.PartnerFee_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.PartnerFee_Safe The fee that the partner is going to keep for themselves (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.Rate} xyz.swapee.wc.IDealBrokerPort.WeakInputs.Rate The rate (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.Rate_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.Rate_Safe The rate (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFloatAmountTo} xyz.swapee.wc.IDealBrokerPort.WeakInputs.EstimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFloatAmountTo_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.EstimatedFloatAmountTo_Safe The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (required overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFixedAmountTo} xyz.swapee.wc.IDealBrokerPort.WeakInputs.EstimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.EstimatedFixedAmountTo_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.EstimatedFixedAmountTo_Safe The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.VisibleAmount} xyz.swapee.wc.IDealBrokerPort.WeakInputs.VisibleAmount The amount before all fees (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.VisibleAmount_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.VisibleAmount_Safe The amount before all fees (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MinAmount} xyz.swapee.wc.IDealBrokerPort.WeakInputs.MinAmount The minimum required amount extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MinAmount_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.MinAmount_Safe The minimum required amount extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MaxAmount} xyz.swapee.wc.IDealBrokerPort.WeakInputs.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.MaxAmount_Safe} xyz.swapee.wc.IDealBrokerPort.WeakInputs.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOffer} xyz.swapee.wc.IDealBrokerCore.Model.GetOffer Start loading offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOffer_Safe} xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe Start loading offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOfferError} xyz.swapee.wc.IDealBrokerCore.Model.GetOfferError An error while getting either fixed or floating-rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.GetOfferError_Safe} xyz.swapee.wc.IDealBrokerCore.Model.GetOfferError_Safe An error while getting either fixed or floating-rate offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.GettingOffer} xyz.swapee.wc.IDealBrokerCore.Model.GettingOffer Whether loading either fixed or floating rate offer (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.GettingOffer_Safe} xyz.swapee.wc.IDealBrokerCore.Model.GettingOffer_Safe Whether loading either fixed or floating rate offer (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.FixedId} xyz.swapee.wc.IDealBrokerCore.Model.FixedId The ID of a fixed-rate transaction for which this rate applies (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.FixedId_Safe} xyz.swapee.wc.IDealBrokerCore.Model.FixedId_Safe The ID of a fixed-rate transaction for which this rate applies (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.NetworkFee} xyz.swapee.wc.IDealBrokerCore.Model.NetworkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.NetworkFee_Safe} xyz.swapee.wc.IDealBrokerCore.Model.NetworkFee_Safe The fee charged by the miners when crypto money to the user, set by the
 * exchange (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.PartnerFee} xyz.swapee.wc.IDealBrokerCore.Model.PartnerFee The fee that the partner is going to keep for themselves (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.PartnerFee_Safe} xyz.swapee.wc.IDealBrokerCore.Model.PartnerFee_Safe The fee that the partner is going to keep for themselves (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.Rate} xyz.swapee.wc.IDealBrokerCore.Model.Rate The rate (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.Rate_Safe} xyz.swapee.wc.IDealBrokerCore.Model.Rate_Safe The rate (required overlay). */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFloatAmountTo} xyz.swapee.wc.IDealBrokerCore.Model.EstimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFloatAmountTo_Safe} xyz.swapee.wc.IDealBrokerCore.Model.EstimatedFloatAmountTo_Safe The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs (required overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFixedAmountTo} xyz.swapee.wc.IDealBrokerCore.Model.EstimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (optional overlay).
 */

/**
 * @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.EstimatedFixedAmountTo_Safe} xyz.swapee.wc.IDealBrokerCore.Model.EstimatedFixedAmountTo_Safe The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change (required overlay).
 */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.VisibleAmount} xyz.swapee.wc.IDealBrokerCore.Model.VisibleAmount The amount before all fees (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.VisibleAmount_Safe} xyz.swapee.wc.IDealBrokerCore.Model.VisibleAmount_Safe The amount before all fees (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.MinAmount} xyz.swapee.wc.IDealBrokerCore.Model.MinAmount The minimum required amount extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.MinAmount_Safe} xyz.swapee.wc.IDealBrokerCore.Model.MinAmount_Safe The minimum required amount extracted from the error (required overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.MaxAmount} xyz.swapee.wc.IDealBrokerCore.Model.MaxAmount The maximum amount that can be exchanged extracted from the error (optional overlay). */

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model.MaxAmount_Safe} xyz.swapee.wc.IDealBrokerCore.Model.MaxAmount_Safe The maximum amount that can be exchanged extracted from the error (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/04-IDealBrokerPort.xml}  f07678246ca8202b12b074af724fc3c6 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IDealBrokerPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerPort)} xyz.swapee.wc.AbstractDealBrokerPort.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerPort} xyz.swapee.wc.DealBrokerPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerPort` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerPort
 */
xyz.swapee.wc.AbstractDealBrokerPort = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerPort.constructor&xyz.swapee.wc.DealBrokerPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerPort.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerPort.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerPort|typeof xyz.swapee.wc.DealBrokerPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerPort}
 */
xyz.swapee.wc.AbstractDealBrokerPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerPort}
 */
xyz.swapee.wc.AbstractDealBrokerPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerPort|typeof xyz.swapee.wc.DealBrokerPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerPort}
 */
xyz.swapee.wc.AbstractDealBrokerPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerPort|typeof xyz.swapee.wc.DealBrokerPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerPort}
 */
xyz.swapee.wc.AbstractDealBrokerPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerPort.Initialese[]) => xyz.swapee.wc.IDealBrokerPort} xyz.swapee.wc.DealBrokerPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerPortFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IDealBrokerPort.Inputs>)} xyz.swapee.wc.IDealBrokerPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IDealBroker_, providing input
 * pins.
 * @interface xyz.swapee.wc.IDealBrokerPort
 */
xyz.swapee.wc.IDealBrokerPort = class extends /** @type {xyz.swapee.wc.IDealBrokerPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerPort.resetPort} */
xyz.swapee.wc.IDealBrokerPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IDealBrokerPort.resetDealBrokerPort} */
xyz.swapee.wc.IDealBrokerPort.prototype.resetDealBrokerPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerPort&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerPort.Initialese>)} xyz.swapee.wc.DealBrokerPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerPort} xyz.swapee.wc.IDealBrokerPort.typeof */
/**
 * A concrete class of _IDealBrokerPort_ instances.
 * @constructor xyz.swapee.wc.DealBrokerPort
 * @implements {xyz.swapee.wc.IDealBrokerPort} The port that serves as an interface to the _IDealBroker_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerPort.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerPort = class extends /** @type {xyz.swapee.wc.DealBrokerPort.constructor&xyz.swapee.wc.IDealBrokerPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerPort}
 */
xyz.swapee.wc.DealBrokerPort.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerPort.
 * @interface xyz.swapee.wc.IDealBrokerPortFields
 */
xyz.swapee.wc.IDealBrokerPortFields = class { }
/**
 * The inputs to the _IDealBroker_'s controller via its port.
 */
xyz.swapee.wc.IDealBrokerPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IDealBrokerPortFields.prototype.props = /** @type {!xyz.swapee.wc.IDealBrokerPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerPort} */
xyz.swapee.wc.RecordIDealBrokerPort

/** @typedef {xyz.swapee.wc.IDealBrokerPort} xyz.swapee.wc.BoundIDealBrokerPort */

/** @typedef {xyz.swapee.wc.DealBrokerPort} xyz.swapee.wc.BoundDealBrokerPort */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerOuterCore.WeakModel)} xyz.swapee.wc.IDealBrokerPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerOuterCore.WeakModel} xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IDealBroker_'s controller via its port.
 * @record xyz.swapee.wc.IDealBrokerPort.Inputs
 */
xyz.swapee.wc.IDealBrokerPort.Inputs = class extends /** @type {xyz.swapee.wc.IDealBrokerPort.Inputs.constructor&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerPort.Inputs.prototype.constructor = xyz.swapee.wc.IDealBrokerPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerOuterCore.WeakModel)} xyz.swapee.wc.IDealBrokerPort.WeakInputs.constructor */
/**
 * The inputs to the _IDealBroker_'s controller via its port.
 * @record xyz.swapee.wc.IDealBrokerPort.WeakInputs
 */
xyz.swapee.wc.IDealBrokerPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IDealBrokerPort.WeakInputs.constructor&xyz.swapee.wc.IDealBrokerOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IDealBrokerPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IDealBrokerPortInterface
 */
xyz.swapee.wc.IDealBrokerPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IDealBrokerPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IDealBrokerPortInterface.prototype.constructor = xyz.swapee.wc.IDealBrokerPortInterface

/**
 * A concrete class of _IDealBrokerPortInterface_ instances.
 * @constructor xyz.swapee.wc.DealBrokerPortInterface
 * @implements {xyz.swapee.wc.IDealBrokerPortInterface} The port interface.
 */
xyz.swapee.wc.DealBrokerPortInterface = class extends xyz.swapee.wc.IDealBrokerPortInterface { }
xyz.swapee.wc.DealBrokerPortInterface.prototype.constructor = xyz.swapee.wc.DealBrokerPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerPortInterface.Props
 * @prop {boolean} getOffer Start loading offer.
 * @prop {string} getOfferError An error while getting either fixed or floating-rate offer.
 * @prop {boolean} gettingOffer Whether loading either fixed or floating rate offer.
 * @prop {string} fixedId The ID of a fixed-rate transaction for which this rate applies.
 * @prop {string} networkFee The fee charged by the miners when crypto money to the user, set by the
 * exchange.
 * @prop {string} partnerFee The fee that the partner is going to keep for themselves.
 * @prop {string} rate The rate.
 * @prop {string} estimatedFloatAmountTo The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs.
 * @prop {string} estimatedFixedAmountTo The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change.
 * @prop {string} visibleAmount The amount before all fees.
 * @prop {string} minAmount The minimum required amount extracted from the error.
 * @prop {string} maxAmount The maximum amount that can be exchanged extracted from the error.
 */

/**
 * Contains getters to cast the _IDealBrokerPort_ interface.
 * @interface xyz.swapee.wc.IDealBrokerPortCaster
 */
xyz.swapee.wc.IDealBrokerPortCaster = class { }
/**
 * Cast the _IDealBrokerPort_ instance into the _BoundIDealBrokerPort_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerPort}
 */
xyz.swapee.wc.IDealBrokerPortCaster.prototype.asIDealBrokerPort
/**
 * Access the _DealBrokerPort_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerPort}
 */
xyz.swapee.wc.IDealBrokerPortCaster.prototype.superDealBrokerPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerPort.__resetPort<!xyz.swapee.wc.IDealBrokerPort>} xyz.swapee.wc.IDealBrokerPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerPort.resetPort} */
/**
 * Resets the _IDealBroker_ port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerPort.__resetDealBrokerPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerPort.__resetDealBrokerPort<!xyz.swapee.wc.IDealBrokerPort>} xyz.swapee.wc.IDealBrokerPort._resetDealBrokerPort */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerPort.resetDealBrokerPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerPort.resetDealBrokerPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/09-IDealBrokerCore.xml}  fa1da7cd92f6ebee7f99034d27c5740b */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IDealBrokerCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerCore)} xyz.swapee.wc.AbstractDealBrokerCore.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerCore} xyz.swapee.wc.DealBrokerCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerCore` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerCore
 */
xyz.swapee.wc.AbstractDealBrokerCore = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerCore.constructor&xyz.swapee.wc.DealBrokerCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerCore.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerCore.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerCore|typeof xyz.swapee.wc.DealBrokerCore)|(!xyz.swapee.wc.IDealBrokerOuterCore|typeof xyz.swapee.wc.DealBrokerOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerCore}
 */
xyz.swapee.wc.AbstractDealBrokerCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerCore}
 */
xyz.swapee.wc.AbstractDealBrokerCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerCore|typeof xyz.swapee.wc.DealBrokerCore)|(!xyz.swapee.wc.IDealBrokerOuterCore|typeof xyz.swapee.wc.DealBrokerOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerCore}
 */
xyz.swapee.wc.AbstractDealBrokerCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerCore|typeof xyz.swapee.wc.DealBrokerCore)|(!xyz.swapee.wc.IDealBrokerOuterCore|typeof xyz.swapee.wc.DealBrokerOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerCore}
 */
xyz.swapee.wc.AbstractDealBrokerCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerCoreCaster&xyz.swapee.wc.IDealBrokerOuterCore)} xyz.swapee.wc.IDealBrokerCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IDealBrokerCore
 */
xyz.swapee.wc.IDealBrokerCore = class extends /** @type {xyz.swapee.wc.IDealBrokerCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IDealBrokerCore.resetCore} */
xyz.swapee.wc.IDealBrokerCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IDealBrokerCore.resetDealBrokerCore} */
xyz.swapee.wc.IDealBrokerCore.prototype.resetDealBrokerCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerCore&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerCore.Initialese>)} xyz.swapee.wc.DealBrokerCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerCore} xyz.swapee.wc.IDealBrokerCore.typeof */
/**
 * A concrete class of _IDealBrokerCore_ instances.
 * @constructor xyz.swapee.wc.DealBrokerCore
 * @implements {xyz.swapee.wc.IDealBrokerCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerCore.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerCore = class extends /** @type {xyz.swapee.wc.DealBrokerCore.constructor&xyz.swapee.wc.IDealBrokerCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerCore.prototype.constructor = xyz.swapee.wc.DealBrokerCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerCore}
 */
xyz.swapee.wc.DealBrokerCore.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerCore.
 * @interface xyz.swapee.wc.IDealBrokerCoreFields
 */
xyz.swapee.wc.IDealBrokerCoreFields = class { }
/**
 * The _IDealBroker_'s memory.
 */
xyz.swapee.wc.IDealBrokerCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IDealBrokerCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IDealBrokerCoreFields.prototype.props = /** @type {xyz.swapee.wc.IDealBrokerCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerCore} */
xyz.swapee.wc.RecordIDealBrokerCore

/** @typedef {xyz.swapee.wc.IDealBrokerCore} xyz.swapee.wc.BoundIDealBrokerCore */

/** @typedef {xyz.swapee.wc.DealBrokerCore} xyz.swapee.wc.BoundDealBrokerCore */

/**
 *   Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change.
 * @typedef {string}
 */
xyz.swapee.wc.IDealBrokerCore.Model.EstimatedAmountTo.estimatedAmountTo

/** @typedef {xyz.swapee.wc.IDealBrokerOuterCore.Model&xyz.swapee.wc.IDealBrokerCore.Model.EstimatedAmountTo} xyz.swapee.wc.IDealBrokerCore.Model The _IDealBroker_'s memory. */

/**
 * Contains getters to cast the _IDealBrokerCore_ interface.
 * @interface xyz.swapee.wc.IDealBrokerCoreCaster
 */
xyz.swapee.wc.IDealBrokerCoreCaster = class { }
/**
 * Cast the _IDealBrokerCore_ instance into the _BoundIDealBrokerCore_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerCore}
 */
xyz.swapee.wc.IDealBrokerCoreCaster.prototype.asIDealBrokerCore
/**
 * Access the _DealBrokerCore_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerCore}
 */
xyz.swapee.wc.IDealBrokerCoreCaster.prototype.superDealBrokerCore

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerCore.Model.EstimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change (optional overlay).
 * @prop {string} [estimatedAmountTo=""] Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerCore.Model.EstimatedAmountTo_Safe Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change (required overlay).
 * @prop {string} estimatedAmountTo Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerCore.__resetCore<!xyz.swapee.wc.IDealBrokerCore>} xyz.swapee.wc.IDealBrokerCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerCore.resetCore} */
/**
 * Resets the _IDealBroker_ core.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerCore.__resetDealBrokerCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerCore.__resetDealBrokerCore<!xyz.swapee.wc.IDealBrokerCore>} xyz.swapee.wc.IDealBrokerCore._resetDealBrokerCore */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerCore.resetDealBrokerCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerCore.resetDealBrokerCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/10-IDealBrokerProcessor.xml}  70108aec62b5a3eb5439d80fe8ed62bb */
/** @typedef {xyz.swapee.wc.IDealBrokerComputer.Initialese&xyz.swapee.wc.IDealBrokerController.Initialese} xyz.swapee.wc.IDealBrokerProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerProcessor)} xyz.swapee.wc.AbstractDealBrokerProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerProcessor} xyz.swapee.wc.DealBrokerProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerProcessor
 */
xyz.swapee.wc.AbstractDealBrokerProcessor = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerProcessor.constructor&xyz.swapee.wc.DealBrokerProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerProcessor.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!xyz.swapee.wc.IDealBrokerCore|typeof xyz.swapee.wc.DealBrokerCore)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!xyz.swapee.wc.IDealBrokerCore|typeof xyz.swapee.wc.DealBrokerCore)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!xyz.swapee.wc.IDealBrokerCore|typeof xyz.swapee.wc.DealBrokerCore)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerProcessor}
 */
xyz.swapee.wc.AbstractDealBrokerProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerProcessor.Initialese[]) => xyz.swapee.wc.IDealBrokerProcessor} xyz.swapee.wc.DealBrokerProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerProcessorCaster&xyz.swapee.wc.IDealBrokerComputer&xyz.swapee.wc.IDealBrokerCore&xyz.swapee.wc.IDealBrokerController)} xyz.swapee.wc.IDealBrokerProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController} xyz.swapee.wc.IDealBrokerController.typeof */
/**
 * The processor to compute changes to the memory for the _IDealBroker_.
 * @interface xyz.swapee.wc.IDealBrokerProcessor
 */
xyz.swapee.wc.IDealBrokerProcessor = class extends /** @type {xyz.swapee.wc.IDealBrokerProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerComputer.typeof&xyz.swapee.wc.IDealBrokerCore.typeof&xyz.swapee.wc.IDealBrokerController.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerProcessor.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerProcessor.pulseGetOffer} */
xyz.swapee.wc.IDealBrokerProcessor.prototype.pulseGetOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerProcessor.Initialese>)} xyz.swapee.wc.DealBrokerProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerProcessor} xyz.swapee.wc.IDealBrokerProcessor.typeof */
/**
 * A concrete class of _IDealBrokerProcessor_ instances.
 * @constructor xyz.swapee.wc.DealBrokerProcessor
 * @implements {xyz.swapee.wc.IDealBrokerProcessor} The processor to compute changes to the memory for the _IDealBroker_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerProcessor.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerProcessor = class extends /** @type {xyz.swapee.wc.DealBrokerProcessor.constructor&xyz.swapee.wc.IDealBrokerProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerProcessor}
 */
xyz.swapee.wc.DealBrokerProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerProcessor} */
xyz.swapee.wc.RecordIDealBrokerProcessor

/** @typedef {xyz.swapee.wc.IDealBrokerProcessor} xyz.swapee.wc.BoundIDealBrokerProcessor */

/** @typedef {xyz.swapee.wc.DealBrokerProcessor} xyz.swapee.wc.BoundDealBrokerProcessor */

/**
 * Contains getters to cast the _IDealBrokerProcessor_ interface.
 * @interface xyz.swapee.wc.IDealBrokerProcessorCaster
 */
xyz.swapee.wc.IDealBrokerProcessorCaster = class { }
/**
 * Cast the _IDealBrokerProcessor_ instance into the _BoundIDealBrokerProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerProcessor}
 */
xyz.swapee.wc.IDealBrokerProcessorCaster.prototype.asIDealBrokerProcessor
/**
 * Access the _DealBrokerProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerProcessor}
 */
xyz.swapee.wc.IDealBrokerProcessorCaster.prototype.superDealBrokerProcessor

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerProcessor.__pulseGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerProcessor.__pulseGetOffer<!xyz.swapee.wc.IDealBrokerProcessor>} xyz.swapee.wc.IDealBrokerProcessor._pulseGetOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerProcessor.pulseGetOffer} */
/**
 * A method called to set the `getOffer` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerProcessor.pulseGetOffer = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerProcessor
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/100-DealBrokerMemory.xml}  64c8c097af459ba586e7d1615f8a662a */
/**
 * The memory of the _IDealBroker_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.DealBrokerMemory
 */
xyz.swapee.wc.DealBrokerMemory = class { }
/**
 * Start loading offer. Default `false`.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.getOffer = /** @type {boolean} */ (void 0)
/**
 * An error while getting either fixed or floating-rate offer. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.getOfferError = /** @type {string} */ (void 0)
/**
 * Whether loading either fixed or floating rate offer. Default `false`.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.gettingOffer = /** @type {boolean} */ (void 0)
/**
 * The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.fixedId = /** @type {string} */ (void 0)
/**
 * The fee charged by the miners when crypto money to the user, set by the
 * exchange. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.networkFee = /** @type {string} */ (void 0)
/**
 * The fee that the partner is going to keep for themselves. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.partnerFee = /** @type {string} */ (void 0)
/**
 * The rate. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.rate = /** @type {string} */ (void 0)
/**
 * The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.estimatedFloatAmountTo = /** @type {string} */ (void 0)
/**
 * The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.estimatedFixedAmountTo = /** @type {string} */ (void 0)
/**
 * The amount before all fees. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.visibleAmount = /** @type {string} */ (void 0)
/**
 * The minimum required amount extracted from the error. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.minAmount = /** @type {string} */ (void 0)
/**
 * The maximum amount that can be exchanged extracted from the error. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.maxAmount = /** @type {string} */ (void 0)
/**
 * Either fixed-rate or floating-rate amount that the user can get. After
 * the actual transaction is created, this value may change. Default empty string.
 */
xyz.swapee.wc.DealBrokerMemory.prototype.estimatedAmountTo = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/102-DealBrokerInputs.xml}  68d9abe8bf12b398170f82b1def559e8 */
/**
 * The inputs of the _IDealBroker_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.DealBrokerInputs
 */
xyz.swapee.wc.front.DealBrokerInputs = class { }
/**
 * Start loading offer. Default `false`.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.getOffer = /** @type {boolean|undefined} */ (void 0)
/**
 * An error while getting either fixed or floating-rate offer. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.getOfferError = /** @type {string|undefined} */ (void 0)
/**
 * Whether loading either fixed or floating rate offer. Default `false`.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.gettingOffer = /** @type {boolean|undefined} */ (void 0)
/**
 * The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.fixedId = /** @type {string|undefined} */ (void 0)
/**
 * The fee charged by the miners when crypto money to the user, set by the
 * exchange. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.networkFee = /** @type {string|undefined} */ (void 0)
/**
 * The fee that the partner is going to keep for themselves. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.partnerFee = /** @type {string|undefined} */ (void 0)
/**
 * The rate. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.rate = /** @type {string|undefined} */ (void 0)
/**
 * The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). When set to null, this means the deal
 * is not available for selected amount / pairs. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.estimatedFloatAmountTo = /** @type {string|undefined} */ (void 0)
/**
 * The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.estimatedFixedAmountTo = /** @type {string|undefined} */ (void 0)
/**
 * The amount before all fees. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.visibleAmount = /** @type {string|undefined} */ (void 0)
/**
 * The minimum required amount extracted from the error. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.minAmount = /** @type {string|undefined} */ (void 0)
/**
 * The maximum amount that can be exchanged extracted from the error. Default empty string.
 */
xyz.swapee.wc.front.DealBrokerInputs.prototype.maxAmount = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/11-IDealBroker.xml}  bf7e939555fb12dde22c8fd8a53542a7 */
/**
 * An atomic wrapper for the _IDealBroker_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.DealBrokerEnv
 */
xyz.swapee.wc.DealBrokerEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.DealBrokerEnv.prototype.dealBroker = /** @type {xyz.swapee.wc.IDealBroker} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.IDealBrokerController.Inputs>&xyz.swapee.wc.IDealBrokerProcessor.Initialese&xyz.swapee.wc.IDealBrokerComputer.Initialese&xyz.swapee.wc.IDealBrokerController.Initialese} xyz.swapee.wc.IDealBroker.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBroker)} xyz.swapee.wc.AbstractDealBroker.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBroker} xyz.swapee.wc.DealBroker.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBroker` interface.
 * @constructor xyz.swapee.wc.AbstractDealBroker
 */
xyz.swapee.wc.AbstractDealBroker = class extends /** @type {xyz.swapee.wc.AbstractDealBroker.constructor&xyz.swapee.wc.DealBroker.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBroker.prototype.constructor = xyz.swapee.wc.AbstractDealBroker
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBroker.class = /** @type {typeof xyz.swapee.wc.AbstractDealBroker} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBroker|typeof xyz.swapee.wc.DealBroker)|(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBroker}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBroker.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBroker}
 */
xyz.swapee.wc.AbstractDealBroker.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBroker}
 */
xyz.swapee.wc.AbstractDealBroker.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBroker|typeof xyz.swapee.wc.DealBroker)|(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBroker}
 */
xyz.swapee.wc.AbstractDealBroker.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBroker|typeof xyz.swapee.wc.DealBroker)|(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBroker}
 */
xyz.swapee.wc.AbstractDealBroker.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBroker.Initialese[]) => xyz.swapee.wc.IDealBroker} xyz.swapee.wc.DealBrokerConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBroker.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IDealBroker.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IDealBroker.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IDealBroker.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.DealBrokerMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.DealBrokerClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerCaster&xyz.swapee.wc.IDealBrokerProcessor&xyz.swapee.wc.IDealBrokerComputer&xyz.swapee.wc.IDealBrokerController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.IDealBrokerController.Inputs, null>)} xyz.swapee.wc.IDealBroker.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IDealBroker
 */
xyz.swapee.wc.IDealBroker = class extends /** @type {xyz.swapee.wc.IDealBroker.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerProcessor.typeof&xyz.swapee.wc.IDealBrokerComputer.typeof&xyz.swapee.wc.IDealBrokerController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBroker* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBroker.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBroker.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBroker&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBroker.Initialese>)} xyz.swapee.wc.DealBroker.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBroker} xyz.swapee.wc.IDealBroker.typeof */
/**
 * A concrete class of _IDealBroker_ instances.
 * @constructor xyz.swapee.wc.DealBroker
 * @implements {xyz.swapee.wc.IDealBroker} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBroker.Initialese>} ‎
 */
xyz.swapee.wc.DealBroker = class extends /** @type {xyz.swapee.wc.DealBroker.constructor&xyz.swapee.wc.IDealBroker.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBroker* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBroker.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBroker* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBroker.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBroker.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBroker}
 */
xyz.swapee.wc.DealBroker.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBroker.
 * @interface xyz.swapee.wc.IDealBrokerFields
 */
xyz.swapee.wc.IDealBrokerFields = class { }
/**
 * The input pins of the _IDealBroker_ port.
 */
xyz.swapee.wc.IDealBrokerFields.prototype.pinout = /** @type {!xyz.swapee.wc.IDealBroker.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBroker} */
xyz.swapee.wc.RecordIDealBroker

/** @typedef {xyz.swapee.wc.IDealBroker} xyz.swapee.wc.BoundIDealBroker */

/** @typedef {xyz.swapee.wc.DealBroker} xyz.swapee.wc.BoundDealBroker */

/** @typedef {xyz.swapee.wc.IDealBrokerController.Inputs} xyz.swapee.wc.IDealBroker.Pinout The input pins of the _IDealBroker_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IDealBrokerController.Inputs>)} xyz.swapee.wc.IDealBrokerBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IDealBrokerBuffer
 */
xyz.swapee.wc.IDealBrokerBuffer = class extends /** @type {xyz.swapee.wc.IDealBrokerBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerBuffer.prototype.constructor = xyz.swapee.wc.IDealBrokerBuffer

/**
 * A concrete class of _IDealBrokerBuffer_ instances.
 * @constructor xyz.swapee.wc.DealBrokerBuffer
 * @implements {xyz.swapee.wc.IDealBrokerBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.DealBrokerBuffer = class extends xyz.swapee.wc.IDealBrokerBuffer { }
xyz.swapee.wc.DealBrokerBuffer.prototype.constructor = xyz.swapee.wc.DealBrokerBuffer

/**
 * Contains getters to cast the _IDealBroker_ interface.
 * @interface xyz.swapee.wc.IDealBrokerCaster
 */
xyz.swapee.wc.IDealBrokerCaster = class { }
/**
 * Cast the _IDealBroker_ instance into the _BoundIDealBroker_ type.
 * @type {!xyz.swapee.wc.BoundIDealBroker}
 */
xyz.swapee.wc.IDealBrokerCaster.prototype.asIDealBroker
/**
 * Access the _DealBroker_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBroker}
 */
xyz.swapee.wc.IDealBrokerCaster.prototype.superDealBroker

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/110-DealBrokerSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.DealBrokerMemoryPQs
 */
xyz.swapee.wc.DealBrokerMemoryPQs = class {
  constructor() {
    /**
     * `b370a`
     */
    this.getOffer=/** @type {string} */ (void 0)
    /**
     * `c568a`
     */
    this.fixedId=/** @type {string} */ (void 0)
    /**
     * `ffd9c`
     */
    this.networkFee=/** @type {string} */ (void 0)
    /**
     * `j6f44`
     */
    this.partnerFee=/** @type {string} */ (void 0)
    /**
     * `i6cc4`
     */
    this.expectedAmountTo=/** @type {string} */ (void 0)
    /**
     * `f65af`
     */
    this.expectedFixedAmountTo=/** @type {string} */ (void 0)
    /**
     * `b0a9a`
     */
    this.fixedAmountTo=/** @type {string} */ (void 0)
    /**
     * `e685c`
     */
    this.visibleAmount=/** @type {string} */ (void 0)
    /**
     * `ff4a3`
     */
    this.getOfferError=/** @type {string} */ (void 0)
    /**
     * `d41da`
     */
    this.gettingOffer=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.DealBrokerMemoryPQs.prototype.constructor = xyz.swapee.wc.DealBrokerMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.DealBrokerMemoryQPs
 * @dict
 */
xyz.swapee.wc.DealBrokerMemoryQPs = class { }
/**
 * `getOffer`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.b370a = /** @type {string} */ (void 0)
/**
 * `fixedId`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.c568a = /** @type {string} */ (void 0)
/**
 * `networkFee`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.ffd9c = /** @type {string} */ (void 0)
/**
 * `partnerFee`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.j6f44 = /** @type {string} */ (void 0)
/**
 * `expectedAmountTo`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.i6cc4 = /** @type {string} */ (void 0)
/**
 * `expectedFixedAmountTo`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.f65af = /** @type {string} */ (void 0)
/**
 * `fixedAmountTo`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.b0a9a = /** @type {string} */ (void 0)
/**
 * `visibleAmount`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.e685c = /** @type {string} */ (void 0)
/**
 * `getOfferError`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.ff4a3 = /** @type {string} */ (void 0)
/**
 * `gettingOffer`
 */
xyz.swapee.wc.DealBrokerMemoryQPs.prototype.d41da = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.DealBrokerMemoryPQs)} xyz.swapee.wc.DealBrokerInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerMemoryPQs} xyz.swapee.wc.DealBrokerMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.DealBrokerInputsPQs
 */
xyz.swapee.wc.DealBrokerInputsPQs = class extends /** @type {xyz.swapee.wc.DealBrokerInputsPQs.constructor&xyz.swapee.wc.DealBrokerMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerInputsPQs.prototype.constructor = xyz.swapee.wc.DealBrokerInputsPQs

/** @typedef {function(new: xyz.swapee.wc.DealBrokerMemoryPQs)} xyz.swapee.wc.DealBrokerInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.DealBrokerInputsQPs
 * @dict
 */
xyz.swapee.wc.DealBrokerInputsQPs = class extends /** @type {xyz.swapee.wc.DealBrokerInputsQPs.constructor&xyz.swapee.wc.DealBrokerMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.DealBrokerInputsQPs.prototype.constructor = xyz.swapee.wc.DealBrokerInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.DealBrokerVdusPQs
 */
xyz.swapee.wc.DealBrokerVdusPQs = class { }
xyz.swapee.wc.DealBrokerVdusPQs.prototype.constructor = xyz.swapee.wc.DealBrokerVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.DealBrokerVdusQPs
 * @dict
 */
xyz.swapee.wc.DealBrokerVdusQPs = class { }
xyz.swapee.wc.DealBrokerVdusQPs.prototype.constructor = xyz.swapee.wc.DealBrokerVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/12-IDealBrokerHtmlComponent.xml}  3fa4d25fd0a32e53971153451feb82c2 */
/** @typedef {xyz.swapee.wc.back.IDealBrokerController.Initialese&xyz.swapee.wc.back.IDealBrokerScreen.Initialese&xyz.swapee.wc.IDealBroker.Initialese&xyz.swapee.wc.IDealBrokerGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IDealBrokerProcessor.Initialese&xyz.swapee.wc.IDealBrokerComputer.Initialese} xyz.swapee.wc.IDealBrokerHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerHtmlComponent)} xyz.swapee.wc.AbstractDealBrokerHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerHtmlComponent} xyz.swapee.wc.DealBrokerHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerHtmlComponent
 */
xyz.swapee.wc.AbstractDealBrokerHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerHtmlComponent.constructor&xyz.swapee.wc.DealBrokerHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerHtmlComponent|typeof xyz.swapee.wc.DealBrokerHtmlComponent)|(!xyz.swapee.wc.back.IDealBrokerController|typeof xyz.swapee.wc.back.DealBrokerController)|(!xyz.swapee.wc.back.IDealBrokerScreen|typeof xyz.swapee.wc.back.DealBrokerScreen)|(!xyz.swapee.wc.IDealBroker|typeof xyz.swapee.wc.DealBroker)|(!xyz.swapee.wc.IDealBrokerGPU|typeof xyz.swapee.wc.DealBrokerGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerHtmlComponent|typeof xyz.swapee.wc.DealBrokerHtmlComponent)|(!xyz.swapee.wc.back.IDealBrokerController|typeof xyz.swapee.wc.back.DealBrokerController)|(!xyz.swapee.wc.back.IDealBrokerScreen|typeof xyz.swapee.wc.back.DealBrokerScreen)|(!xyz.swapee.wc.IDealBroker|typeof xyz.swapee.wc.DealBroker)|(!xyz.swapee.wc.IDealBrokerGPU|typeof xyz.swapee.wc.DealBrokerGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerHtmlComponent|typeof xyz.swapee.wc.DealBrokerHtmlComponent)|(!xyz.swapee.wc.back.IDealBrokerController|typeof xyz.swapee.wc.back.DealBrokerController)|(!xyz.swapee.wc.back.IDealBrokerScreen|typeof xyz.swapee.wc.back.DealBrokerScreen)|(!xyz.swapee.wc.IDealBroker|typeof xyz.swapee.wc.DealBroker)|(!xyz.swapee.wc.IDealBrokerGPU|typeof xyz.swapee.wc.DealBrokerGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IDealBrokerProcessor|typeof xyz.swapee.wc.DealBrokerProcessor)|(!xyz.swapee.wc.IDealBrokerComputer|typeof xyz.swapee.wc.DealBrokerComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerHtmlComponent}
 */
xyz.swapee.wc.AbstractDealBrokerHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerHtmlComponent.Initialese[]) => xyz.swapee.wc.IDealBrokerHtmlComponent} xyz.swapee.wc.DealBrokerHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerHtmlComponentCaster&xyz.swapee.wc.back.IDealBrokerController&xyz.swapee.wc.back.IDealBrokerScreen&xyz.swapee.wc.IDealBroker&xyz.swapee.wc.IDealBrokerGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.IDealBrokerController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IDealBrokerProcessor&xyz.swapee.wc.IDealBrokerComputer)} xyz.swapee.wc.IDealBrokerHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerController} xyz.swapee.wc.back.IDealBrokerController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerScreen} xyz.swapee.wc.back.IDealBrokerScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerGPU} xyz.swapee.wc.IDealBrokerGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IDealBroker_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IDealBrokerHtmlComponent
 */
xyz.swapee.wc.IDealBrokerHtmlComponent = class extends /** @type {xyz.swapee.wc.IDealBrokerHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IDealBrokerController.typeof&xyz.swapee.wc.back.IDealBrokerScreen.typeof&xyz.swapee.wc.IDealBroker.typeof&xyz.swapee.wc.IDealBrokerGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IDealBrokerProcessor.typeof&xyz.swapee.wc.IDealBrokerComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerHtmlComponent.Initialese>)} xyz.swapee.wc.DealBrokerHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerHtmlComponent} xyz.swapee.wc.IDealBrokerHtmlComponent.typeof */
/**
 * A concrete class of _IDealBrokerHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.DealBrokerHtmlComponent
 * @implements {xyz.swapee.wc.IDealBrokerHtmlComponent} The _IDealBroker_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerHtmlComponent = class extends /** @type {xyz.swapee.wc.DealBrokerHtmlComponent.constructor&xyz.swapee.wc.IDealBrokerHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerHtmlComponent}
 */
xyz.swapee.wc.DealBrokerHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerHtmlComponent} */
xyz.swapee.wc.RecordIDealBrokerHtmlComponent

/** @typedef {xyz.swapee.wc.IDealBrokerHtmlComponent} xyz.swapee.wc.BoundIDealBrokerHtmlComponent */

/** @typedef {xyz.swapee.wc.DealBrokerHtmlComponent} xyz.swapee.wc.BoundDealBrokerHtmlComponent */

/**
 * Contains getters to cast the _IDealBrokerHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IDealBrokerHtmlComponentCaster
 */
xyz.swapee.wc.IDealBrokerHtmlComponentCaster = class { }
/**
 * Cast the _IDealBrokerHtmlComponent_ instance into the _BoundIDealBrokerHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerHtmlComponent}
 */
xyz.swapee.wc.IDealBrokerHtmlComponentCaster.prototype.asIDealBrokerHtmlComponent
/**
 * Access the _DealBrokerHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerHtmlComponent}
 */
xyz.swapee.wc.IDealBrokerHtmlComponentCaster.prototype.superDealBrokerHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/130-IDealBrokerElement.xml}  20c318be1a9b071c7ad0bb281d97a78f */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.IDealBrokerElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IDealBrokerElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerElement)} xyz.swapee.wc.AbstractDealBrokerElement.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerElement} xyz.swapee.wc.DealBrokerElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerElement` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerElement
 */
xyz.swapee.wc.AbstractDealBrokerElement = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerElement.constructor&xyz.swapee.wc.DealBrokerElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerElement.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerElement.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerElement|typeof xyz.swapee.wc.DealBrokerElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerElement}
 */
xyz.swapee.wc.AbstractDealBrokerElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElement}
 */
xyz.swapee.wc.AbstractDealBrokerElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerElement|typeof xyz.swapee.wc.DealBrokerElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElement}
 */
xyz.swapee.wc.AbstractDealBrokerElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerElement|typeof xyz.swapee.wc.DealBrokerElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElement}
 */
xyz.swapee.wc.AbstractDealBrokerElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerElement.Initialese[]) => xyz.swapee.wc.IDealBrokerElement} xyz.swapee.wc.DealBrokerElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerElementFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.IDealBrokerElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.IDealBrokerElement.Inputs, null>)} xyz.swapee.wc.IDealBrokerElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IDealBroker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IDealBrokerElement
 */
xyz.swapee.wc.IDealBrokerElement = class extends /** @type {xyz.swapee.wc.IDealBrokerElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerElement.solder} */
xyz.swapee.wc.IDealBrokerElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IDealBrokerElement.render} */
xyz.swapee.wc.IDealBrokerElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IDealBrokerElement.server} */
xyz.swapee.wc.IDealBrokerElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IDealBrokerElement.inducer} */
xyz.swapee.wc.IDealBrokerElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerElement&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerElement.Initialese>)} xyz.swapee.wc.DealBrokerElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElement} xyz.swapee.wc.IDealBrokerElement.typeof */
/**
 * A concrete class of _IDealBrokerElement_ instances.
 * @constructor xyz.swapee.wc.DealBrokerElement
 * @implements {xyz.swapee.wc.IDealBrokerElement} A component description.
 *
 * The _IDealBroker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerElement.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerElement = class extends /** @type {xyz.swapee.wc.DealBrokerElement.constructor&xyz.swapee.wc.IDealBrokerElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerElement}
 */
xyz.swapee.wc.DealBrokerElement.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerElement.
 * @interface xyz.swapee.wc.IDealBrokerElementFields
 */
xyz.swapee.wc.IDealBrokerElementFields = class { }
/**
 * The element-specific inputs to the _IDealBroker_ component.
 */
xyz.swapee.wc.IDealBrokerElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerElement} */
xyz.swapee.wc.RecordIDealBrokerElement

/** @typedef {xyz.swapee.wc.IDealBrokerElement} xyz.swapee.wc.BoundIDealBrokerElement */

/** @typedef {xyz.swapee.wc.DealBrokerElement} xyz.swapee.wc.BoundDealBrokerElement */

/** @typedef {xyz.swapee.wc.IDealBrokerPort.Inputs&xyz.swapee.wc.IDealBrokerDisplay.Queries&xyz.swapee.wc.IDealBrokerController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IDealBrokerElementPort.Inputs} xyz.swapee.wc.IDealBrokerElement.Inputs The element-specific inputs to the _IDealBroker_ component. */

/**
 * Contains getters to cast the _IDealBrokerElement_ interface.
 * @interface xyz.swapee.wc.IDealBrokerElementCaster
 */
xyz.swapee.wc.IDealBrokerElementCaster = class { }
/**
 * Cast the _IDealBrokerElement_ instance into the _BoundIDealBrokerElement_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerElement}
 */
xyz.swapee.wc.IDealBrokerElementCaster.prototype.asIDealBrokerElement
/**
 * Access the _DealBrokerElement_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerElement}
 */
xyz.swapee.wc.IDealBrokerElementCaster.prototype.superDealBrokerElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.DealBrokerMemory, props: !xyz.swapee.wc.IDealBrokerElement.Inputs) => Object<string, *>} xyz.swapee.wc.IDealBrokerElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerElement.__solder<!xyz.swapee.wc.IDealBrokerElement>} xyz.swapee.wc.IDealBrokerElement._solder */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.DealBrokerMemory} model The model.
 * - `getOffer` _boolean_ Start loading offer. Default `false`.
 * - `fixedId` _string_ The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 * - `networkFee` _string_ The fee charged by the miners, set by the exchange. Default empty string.
 * - `partnerFee` _string_ The fee that the partner is going to keep for themselves. Default empty string.
 * - `expectedAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). Default empty string.
 * - `expectedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 * - `fixedAmountTo` _string_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. Default empty string.
 * - `visibleAmount` _string_ The amount before all fees. Default empty string.
 * - `getOfferError` _string_ Default empty string.
 * - `gettingOffer` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerElement.Inputs} props The element props.
 * - `[getOffer=null]` _&#42;?_ Start loading offer. ⤴ *IDealBrokerOuterCore.WeakModel.GetOffer* ⤴ *IDealBrokerOuterCore.WeakModel.GetOffer* Default `null`.
 * - `[fixedId=null]` _&#42;?_ The ID of a fixed-rate transaction for which this rate applies. ⤴ *IDealBrokerOuterCore.WeakModel.FixedId* ⤴ *IDealBrokerOuterCore.WeakModel.FixedId* Default `null`.
 * - `[networkFee=null]` _&#42;?_ The fee charged by the miners, set by the exchange. ⤴ *IDealBrokerOuterCore.WeakModel.NetworkFee* ⤴ *IDealBrokerOuterCore.WeakModel.NetworkFee* Default `null`.
 * - `[partnerFee=null]` _&#42;?_ The fee that the partner is going to keep for themselves. ⤴ *IDealBrokerOuterCore.WeakModel.PartnerFee* ⤴ *IDealBrokerOuterCore.WeakModel.PartnerFee* Default `null`.
 * - `[expectedAmountTo=null]` _&#42;?_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedAmountTo* Default `null`.
 * - `[expectedFixedAmountTo=null]` _&#42;?_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedFixedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedFixedAmountTo* Default `null`.
 * - `[fixedAmountTo=null]` _&#42;?_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. ⤴ *IDealBrokerOuterCore.WeakModel.FixedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.FixedAmountTo* Default `null`.
 * - `[visibleAmount=null]` _&#42;?_ The amount before all fees. ⤴ *IDealBrokerOuterCore.WeakModel.VisibleAmount* ⤴ *IDealBrokerOuterCore.WeakModel.VisibleAmount* Default `null`.
 * - `[getOfferError=null]` _&#42;?_ ⤴ *IDealBrokerOuterCore.WeakModel.GetOfferError* ⤴ *IDealBrokerOuterCore.WeakModel.GetOfferError* Default `null`.
 * - `[gettingOffer=null]` _&#42;?_ ⤴ *IDealBrokerOuterCore.WeakModel.GettingOffer* ⤴ *IDealBrokerOuterCore.WeakModel.GettingOffer* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IDealBrokerElementPort.Inputs.NoSolder* Default `false`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IDealBrokerElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.DealBrokerMemory, instance?: !xyz.swapee.wc.IDealBrokerScreen&xyz.swapee.wc.IDealBrokerController) => !engineering.type.VNode} xyz.swapee.wc.IDealBrokerElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerElement.__render<!xyz.swapee.wc.IDealBrokerElement>} xyz.swapee.wc.IDealBrokerElement._render */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.DealBrokerMemory} [model] The model for the view.
 * - `getOffer` _boolean_ Start loading offer. Default `false`.
 * - `fixedId` _string_ The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 * - `networkFee` _string_ The fee charged by the miners, set by the exchange. Default empty string.
 * - `partnerFee` _string_ The fee that the partner is going to keep for themselves. Default empty string.
 * - `expectedAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). Default empty string.
 * - `expectedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 * - `fixedAmountTo` _string_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. Default empty string.
 * - `visibleAmount` _string_ The amount before all fees. Default empty string.
 * - `getOfferError` _string_ Default empty string.
 * - `gettingOffer` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerScreen&xyz.swapee.wc.IDealBrokerController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IDealBrokerElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.DealBrokerMemory, inputs: !xyz.swapee.wc.IDealBrokerElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IDealBrokerElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerElement.__server<!xyz.swapee.wc.IDealBrokerElement>} xyz.swapee.wc.IDealBrokerElement._server */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.DealBrokerMemory} memory The memory registers.
 * - `getOffer` _boolean_ Start loading offer. Default `false`.
 * - `fixedId` _string_ The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 * - `networkFee` _string_ The fee charged by the miners, set by the exchange. Default empty string.
 * - `partnerFee` _string_ The fee that the partner is going to keep for themselves. Default empty string.
 * - `expectedAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). Default empty string.
 * - `expectedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 * - `fixedAmountTo` _string_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. Default empty string.
 * - `visibleAmount` _string_ The amount before all fees. Default empty string.
 * - `getOfferError` _string_ Default empty string.
 * - `gettingOffer` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerElement.Inputs} inputs The inputs to the port.
 * - `[getOffer=null]` _&#42;?_ Start loading offer. ⤴ *IDealBrokerOuterCore.WeakModel.GetOffer* ⤴ *IDealBrokerOuterCore.WeakModel.GetOffer* Default `null`.
 * - `[fixedId=null]` _&#42;?_ The ID of a fixed-rate transaction for which this rate applies. ⤴ *IDealBrokerOuterCore.WeakModel.FixedId* ⤴ *IDealBrokerOuterCore.WeakModel.FixedId* Default `null`.
 * - `[networkFee=null]` _&#42;?_ The fee charged by the miners, set by the exchange. ⤴ *IDealBrokerOuterCore.WeakModel.NetworkFee* ⤴ *IDealBrokerOuterCore.WeakModel.NetworkFee* Default `null`.
 * - `[partnerFee=null]` _&#42;?_ The fee that the partner is going to keep for themselves. ⤴ *IDealBrokerOuterCore.WeakModel.PartnerFee* ⤴ *IDealBrokerOuterCore.WeakModel.PartnerFee* Default `null`.
 * - `[expectedAmountTo=null]` _&#42;?_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedAmountTo* Default `null`.
 * - `[expectedFixedAmountTo=null]` _&#42;?_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedFixedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedFixedAmountTo* Default `null`.
 * - `[fixedAmountTo=null]` _&#42;?_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. ⤴ *IDealBrokerOuterCore.WeakModel.FixedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.FixedAmountTo* Default `null`.
 * - `[visibleAmount=null]` _&#42;?_ The amount before all fees. ⤴ *IDealBrokerOuterCore.WeakModel.VisibleAmount* ⤴ *IDealBrokerOuterCore.WeakModel.VisibleAmount* Default `null`.
 * - `[getOfferError=null]` _&#42;?_ ⤴ *IDealBrokerOuterCore.WeakModel.GetOfferError* ⤴ *IDealBrokerOuterCore.WeakModel.GetOfferError* Default `null`.
 * - `[gettingOffer=null]` _&#42;?_ ⤴ *IDealBrokerOuterCore.WeakModel.GettingOffer* ⤴ *IDealBrokerOuterCore.WeakModel.GettingOffer* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IDealBrokerElementPort.Inputs.NoSolder* Default `false`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IDealBrokerElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.DealBrokerMemory, port?: !xyz.swapee.wc.IDealBrokerElement.Inputs) => ?} xyz.swapee.wc.IDealBrokerElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerElement.__inducer<!xyz.swapee.wc.IDealBrokerElement>} xyz.swapee.wc.IDealBrokerElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.DealBrokerMemory} [model] The model of the component into which to induce the state.
 * - `getOffer` _boolean_ Start loading offer. Default `false`.
 * - `fixedId` _string_ The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 * - `networkFee` _string_ The fee charged by the miners, set by the exchange. Default empty string.
 * - `partnerFee` _string_ The fee that the partner is going to keep for themselves. Default empty string.
 * - `expectedAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). Default empty string.
 * - `expectedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 * - `fixedAmountTo` _string_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. Default empty string.
 * - `visibleAmount` _string_ The amount before all fees. Default empty string.
 * - `getOfferError` _string_ Default empty string.
 * - `gettingOffer` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.IDealBrokerElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[getOffer=null]` _&#42;?_ Start loading offer. ⤴ *IDealBrokerOuterCore.WeakModel.GetOffer* ⤴ *IDealBrokerOuterCore.WeakModel.GetOffer* Default `null`.
 * - `[fixedId=null]` _&#42;?_ The ID of a fixed-rate transaction for which this rate applies. ⤴ *IDealBrokerOuterCore.WeakModel.FixedId* ⤴ *IDealBrokerOuterCore.WeakModel.FixedId* Default `null`.
 * - `[networkFee=null]` _&#42;?_ The fee charged by the miners, set by the exchange. ⤴ *IDealBrokerOuterCore.WeakModel.NetworkFee* ⤴ *IDealBrokerOuterCore.WeakModel.NetworkFee* Default `null`.
 * - `[partnerFee=null]` _&#42;?_ The fee that the partner is going to keep for themselves. ⤴ *IDealBrokerOuterCore.WeakModel.PartnerFee* ⤴ *IDealBrokerOuterCore.WeakModel.PartnerFee* Default `null`.
 * - `[expectedAmountTo=null]` _&#42;?_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedAmountTo* Default `null`.
 * - `[expectedFixedAmountTo=null]` _&#42;?_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedFixedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.ExpectedFixedAmountTo* Default `null`.
 * - `[fixedAmountTo=null]` _&#42;?_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. ⤴ *IDealBrokerOuterCore.WeakModel.FixedAmountTo* ⤴ *IDealBrokerOuterCore.WeakModel.FixedAmountTo* Default `null`.
 * - `[visibleAmount=null]` _&#42;?_ The amount before all fees. ⤴ *IDealBrokerOuterCore.WeakModel.VisibleAmount* ⤴ *IDealBrokerOuterCore.WeakModel.VisibleAmount* Default `null`.
 * - `[getOfferError=null]` _&#42;?_ ⤴ *IDealBrokerOuterCore.WeakModel.GetOfferError* ⤴ *IDealBrokerOuterCore.WeakModel.GetOfferError* Default `null`.
 * - `[gettingOffer=null]` _&#42;?_ ⤴ *IDealBrokerOuterCore.WeakModel.GettingOffer* ⤴ *IDealBrokerOuterCore.WeakModel.GettingOffer* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IDealBrokerElementPort.Inputs.NoSolder* Default `false`.
 * @return {?}
 */
xyz.swapee.wc.IDealBrokerElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/140-IDealBrokerElementPort.xml}  c853ef93ae465df7e7026f26c11af790 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IDealBrokerElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerElementPort)} xyz.swapee.wc.AbstractDealBrokerElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerElementPort} xyz.swapee.wc.DealBrokerElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerElementPort
 */
xyz.swapee.wc.AbstractDealBrokerElementPort = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerElementPort.constructor&xyz.swapee.wc.DealBrokerElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerElementPort.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerElementPort|typeof xyz.swapee.wc.DealBrokerElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerElementPort|typeof xyz.swapee.wc.DealBrokerElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerElementPort|typeof xyz.swapee.wc.DealBrokerElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerElementPort}
 */
xyz.swapee.wc.AbstractDealBrokerElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerElementPort.Initialese[]) => xyz.swapee.wc.IDealBrokerElementPort} xyz.swapee.wc.DealBrokerElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IDealBrokerElementPort.Inputs>)} xyz.swapee.wc.IDealBrokerElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IDealBrokerElementPort
 */
xyz.swapee.wc.IDealBrokerElementPort = class extends /** @type {xyz.swapee.wc.IDealBrokerElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerElementPort.Initialese>)} xyz.swapee.wc.DealBrokerElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElementPort} xyz.swapee.wc.IDealBrokerElementPort.typeof */
/**
 * A concrete class of _IDealBrokerElementPort_ instances.
 * @constructor xyz.swapee.wc.DealBrokerElementPort
 * @implements {xyz.swapee.wc.IDealBrokerElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerElementPort.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerElementPort = class extends /** @type {xyz.swapee.wc.DealBrokerElementPort.constructor&xyz.swapee.wc.IDealBrokerElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerElementPort}
 */
xyz.swapee.wc.DealBrokerElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerElementPort.
 * @interface xyz.swapee.wc.IDealBrokerElementPortFields
 */
xyz.swapee.wc.IDealBrokerElementPortFields = class { }
/**
 * The inputs to the _IDealBrokerElement_'s controller via its element port.
 */
xyz.swapee.wc.IDealBrokerElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IDealBrokerElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IDealBrokerElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerElementPort} */
xyz.swapee.wc.RecordIDealBrokerElementPort

/** @typedef {xyz.swapee.wc.IDealBrokerElementPort} xyz.swapee.wc.BoundIDealBrokerElementPort */

/** @typedef {xyz.swapee.wc.DealBrokerElementPort} xyz.swapee.wc.BoundDealBrokerElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder)} xyz.swapee.wc.IDealBrokerElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder} xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _IDealBrokerElement_'s controller via its element port.
 * @record xyz.swapee.wc.IDealBrokerElementPort.Inputs
 */
xyz.swapee.wc.IDealBrokerElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IDealBrokerElementPort.Inputs.constructor&xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IDealBrokerElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _IDealBrokerElement_'s controller via its element port.
 * @record xyz.swapee.wc.IDealBrokerElementPort.WeakInputs
 */
xyz.swapee.wc.IDealBrokerElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.constructor&xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IDealBrokerElementPort.WeakInputs

/**
 * Contains getters to cast the _IDealBrokerElementPort_ interface.
 * @interface xyz.swapee.wc.IDealBrokerElementPortCaster
 */
xyz.swapee.wc.IDealBrokerElementPortCaster = class { }
/**
 * Cast the _IDealBrokerElementPort_ instance into the _BoundIDealBrokerElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerElementPort}
 */
xyz.swapee.wc.IDealBrokerElementPortCaster.prototype.asIDealBrokerElementPort
/**
 * Access the _DealBrokerElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerElementPort}
 */
xyz.swapee.wc.IDealBrokerElementPortCaster.prototype.superDealBrokerElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/170-IDealBrokerDesigner.xml}  41300c6e9d376c2afde05760272b2f02 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IDealBrokerDesigner
 */
xyz.swapee.wc.IDealBrokerDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.DealBrokerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IDealBroker />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.DealBrokerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IDealBroker />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.DealBrokerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IDealBrokerDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `DealBroker` _typeof IDealBrokerController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IDealBrokerDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `DealBroker` _typeof IDealBrokerController_
   * - `This` _typeof IDealBrokerController_
   * @param {!xyz.swapee.wc.IDealBrokerDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `DealBroker` _!DealBrokerMemory_
   * - `This` _!DealBrokerMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.DealBrokerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.DealBrokerClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IDealBrokerDesigner.prototype.constructor = xyz.swapee.wc.IDealBrokerDesigner

/**
 * A concrete class of _IDealBrokerDesigner_ instances.
 * @constructor xyz.swapee.wc.DealBrokerDesigner
 * @implements {xyz.swapee.wc.IDealBrokerDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.DealBrokerDesigner = class extends xyz.swapee.wc.IDealBrokerDesigner { }
xyz.swapee.wc.DealBrokerDesigner.prototype.constructor = xyz.swapee.wc.DealBrokerDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IDealBrokerDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker
 * @prop {!xyz.swapee.wc.DealBrokerMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/40-IDealBrokerDisplay.xml}  5db19c4d155be64bdc997c420496caee */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IDealBrokerDisplay.Settings>} xyz.swapee.wc.IDealBrokerDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerDisplay)} xyz.swapee.wc.AbstractDealBrokerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerDisplay} xyz.swapee.wc.DealBrokerDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerDisplay
 */
xyz.swapee.wc.AbstractDealBrokerDisplay = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerDisplay.constructor&xyz.swapee.wc.DealBrokerDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerDisplay.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerDisplay|typeof xyz.swapee.wc.DealBrokerDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerDisplay|typeof xyz.swapee.wc.DealBrokerDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerDisplay|typeof xyz.swapee.wc.DealBrokerDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerDisplay}
 */
xyz.swapee.wc.AbstractDealBrokerDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerDisplay.Initialese[]) => xyz.swapee.wc.IDealBrokerDisplay} xyz.swapee.wc.DealBrokerDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.DealBrokerMemory, !HTMLDivElement, !xyz.swapee.wc.IDealBrokerDisplay.Settings, xyz.swapee.wc.IDealBrokerDisplay.Queries, null>)} xyz.swapee.wc.IDealBrokerDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IDealBroker_.
 * @interface xyz.swapee.wc.IDealBrokerDisplay
 */
xyz.swapee.wc.IDealBrokerDisplay = class extends /** @type {xyz.swapee.wc.IDealBrokerDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerDisplay.paint} */
xyz.swapee.wc.IDealBrokerDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerDisplay.Initialese>)} xyz.swapee.wc.DealBrokerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerDisplay} xyz.swapee.wc.IDealBrokerDisplay.typeof */
/**
 * A concrete class of _IDealBrokerDisplay_ instances.
 * @constructor xyz.swapee.wc.DealBrokerDisplay
 * @implements {xyz.swapee.wc.IDealBrokerDisplay} Display for presenting information from the _IDealBroker_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerDisplay.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerDisplay = class extends /** @type {xyz.swapee.wc.DealBrokerDisplay.constructor&xyz.swapee.wc.IDealBrokerDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerDisplay}
 */
xyz.swapee.wc.DealBrokerDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerDisplay.
 * @interface xyz.swapee.wc.IDealBrokerDisplayFields
 */
xyz.swapee.wc.IDealBrokerDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IDealBrokerDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IDealBrokerDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IDealBrokerDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IDealBrokerDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerDisplay} */
xyz.swapee.wc.RecordIDealBrokerDisplay

/** @typedef {xyz.swapee.wc.IDealBrokerDisplay} xyz.swapee.wc.BoundIDealBrokerDisplay */

/** @typedef {xyz.swapee.wc.DealBrokerDisplay} xyz.swapee.wc.BoundDealBrokerDisplay */

/** @typedef {xyz.swapee.wc.IDealBrokerDisplay.Queries} xyz.swapee.wc.IDealBrokerDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IDealBrokerDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IDealBrokerDisplay_ interface.
 * @interface xyz.swapee.wc.IDealBrokerDisplayCaster
 */
xyz.swapee.wc.IDealBrokerDisplayCaster = class { }
/**
 * Cast the _IDealBrokerDisplay_ instance into the _BoundIDealBrokerDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerDisplay}
 */
xyz.swapee.wc.IDealBrokerDisplayCaster.prototype.asIDealBrokerDisplay
/**
 * Cast the _IDealBrokerDisplay_ instance into the _BoundIDealBrokerScreen_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerScreen}
 */
xyz.swapee.wc.IDealBrokerDisplayCaster.prototype.asIDealBrokerScreen
/**
 * Access the _DealBrokerDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerDisplay}
 */
xyz.swapee.wc.IDealBrokerDisplayCaster.prototype.superDealBrokerDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.DealBrokerMemory, land: null) => void} xyz.swapee.wc.IDealBrokerDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerDisplay.__paint<!xyz.swapee.wc.IDealBrokerDisplay>} xyz.swapee.wc.IDealBrokerDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.DealBrokerMemory} memory The display data.
 * - `getOffer` _boolean_ Start loading offer. Default `false`.
 * - `fixedId` _string_ The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 * - `networkFee` _string_ The fee charged by the miners, set by the exchange. Default empty string.
 * - `partnerFee` _string_ The fee that the partner is going to keep for themselves. Default empty string.
 * - `expectedAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). Default empty string.
 * - `expectedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 * - `fixedAmountTo` _string_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. Default empty string.
 * - `visibleAmount` _string_ The amount before all fees. Default empty string.
 * - `getOfferError` _string_ Default empty string.
 * - `gettingOffer` _boolean_ Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/40-IDealBrokerDisplayBack.xml}  77d738469e4ed16afeccb636068cd5f8 */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.DealBrokerClasses>} xyz.swapee.wc.back.IDealBrokerDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerDisplay)} xyz.swapee.wc.back.AbstractDealBrokerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerDisplay} xyz.swapee.wc.back.DealBrokerDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerDisplay
 */
xyz.swapee.wc.back.AbstractDealBrokerDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerDisplay.constructor&xyz.swapee.wc.back.DealBrokerDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerDisplay|typeof xyz.swapee.wc.back.DealBrokerDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerDisplay|typeof xyz.swapee.wc.back.DealBrokerDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerDisplay|typeof xyz.swapee.wc.back.DealBrokerDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerDisplay}
 */
xyz.swapee.wc.back.AbstractDealBrokerDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.DealBrokerClasses, null>)} xyz.swapee.wc.back.IDealBrokerDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IDealBrokerDisplay
 */
xyz.swapee.wc.back.IDealBrokerDisplay = class extends /** @type {xyz.swapee.wc.back.IDealBrokerDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IDealBrokerDisplay.paint} */
xyz.swapee.wc.back.IDealBrokerDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerDisplay.Initialese>)} xyz.swapee.wc.back.DealBrokerDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerDisplay} xyz.swapee.wc.back.IDealBrokerDisplay.typeof */
/**
 * A concrete class of _IDealBrokerDisplay_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerDisplay
 * @implements {xyz.swapee.wc.back.IDealBrokerDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerDisplay = class extends /** @type {xyz.swapee.wc.back.DealBrokerDisplay.constructor&xyz.swapee.wc.back.IDealBrokerDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.DealBrokerDisplay.prototype.constructor = xyz.swapee.wc.back.DealBrokerDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerDisplay}
 */
xyz.swapee.wc.back.DealBrokerDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerDisplay} */
xyz.swapee.wc.back.RecordIDealBrokerDisplay

/** @typedef {xyz.swapee.wc.back.IDealBrokerDisplay} xyz.swapee.wc.back.BoundIDealBrokerDisplay */

/** @typedef {xyz.swapee.wc.back.DealBrokerDisplay} xyz.swapee.wc.back.BoundDealBrokerDisplay */

/**
 * Contains getters to cast the _IDealBrokerDisplay_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerDisplayCaster
 */
xyz.swapee.wc.back.IDealBrokerDisplayCaster = class { }
/**
 * Cast the _IDealBrokerDisplay_ instance into the _BoundIDealBrokerDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerDisplay}
 */
xyz.swapee.wc.back.IDealBrokerDisplayCaster.prototype.asIDealBrokerDisplay
/**
 * Access the _DealBrokerDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerDisplay}
 */
xyz.swapee.wc.back.IDealBrokerDisplayCaster.prototype.superDealBrokerDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.DealBrokerMemory, land?: null) => void} xyz.swapee.wc.back.IDealBrokerDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IDealBrokerDisplay.__paint<!xyz.swapee.wc.back.IDealBrokerDisplay>} xyz.swapee.wc.back.IDealBrokerDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.DealBrokerMemory} [memory] The display data.
 * - `getOffer` _boolean_ Start loading offer. Default `false`.
 * - `fixedId` _string_ The ID of a fixed-rate transaction for which this rate applies. Default empty string.
 * - `networkFee` _string_ The fee charged by the miners, set by the exchange. Default empty string.
 * - `partnerFee` _string_ The fee that the partner is going to keep for themselves. Default empty string.
 * - `expectedAmountTo` _string_ The floating amount that the user is expected to receive (visible amount
 * minus parner fee minus network fee). Default empty string.
 * - `expectedFixedAmountTo` _string_ The amount that the user is expected to get with fixed-rate offers, obtained
 * _before_ creating an actual transaction, after which this value might change. Default empty string.
 * - `fixedAmountTo` _string_ The amount that the user is guaranteed to receive from the system according
 * to the generated fixed rate. Default empty string.
 * - `visibleAmount` _string_ The amount before all fees. Default empty string.
 * - `getOfferError` _string_ Default empty string.
 * - `gettingOffer` _boolean_ Default `false`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IDealBrokerDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IDealBrokerDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/41-DealBrokerClasses.xml}  1ff72887fa45d8da8f716085a96ac103 */
/**
 * The classes of the _IDealBrokerDisplay_.
 * @record xyz.swapee.wc.DealBrokerClasses
 */
xyz.swapee.wc.DealBrokerClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.DealBrokerClasses.prototype.props = /** @type {xyz.swapee.wc.DealBrokerClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/50-IDealBrokerController.xml}  ecbbfaf309ca3330671e3be831ca2517 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IDealBrokerController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IDealBrokerController.Inputs, !xyz.swapee.wc.IDealBrokerOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IDealBrokerOuterCore.WeakModel>} xyz.swapee.wc.IDealBrokerController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerController)} xyz.swapee.wc.AbstractDealBrokerController.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerController} xyz.swapee.wc.DealBrokerController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerController` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerController
 */
xyz.swapee.wc.AbstractDealBrokerController = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerController.constructor&xyz.swapee.wc.DealBrokerController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerController.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerController.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerController}
 */
xyz.swapee.wc.AbstractDealBrokerController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerController}
 */
xyz.swapee.wc.AbstractDealBrokerController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerController}
 */
xyz.swapee.wc.AbstractDealBrokerController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerController}
 */
xyz.swapee.wc.AbstractDealBrokerController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerController.Initialese[]) => xyz.swapee.wc.IDealBrokerController} xyz.swapee.wc.DealBrokerControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IDealBrokerController.Inputs, !xyz.swapee.wc.IDealBrokerOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IDealBrokerOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IDealBrokerController.Inputs, !xyz.swapee.wc.IDealBrokerController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IDealBrokerController.Inputs, !xyz.swapee.wc.DealBrokerMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IDealBrokerController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IDealBrokerController.Inputs>)} xyz.swapee.wc.IDealBrokerController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IDealBrokerController
 */
xyz.swapee.wc.IDealBrokerController = class extends /** @type {xyz.swapee.wc.IDealBrokerController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IDealBrokerController.resetPort} */
xyz.swapee.wc.IDealBrokerController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setGetOfferError} */
xyz.swapee.wc.IDealBrokerController.prototype.setGetOfferError = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetGetOfferError} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetGetOfferError = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.flipGettingOffer} */
xyz.swapee.wc.IDealBrokerController.prototype.flipGettingOffer = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setGettingOffer} */
xyz.swapee.wc.IDealBrokerController.prototype.setGettingOffer = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetGettingOffer} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetGettingOffer = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setNetworkFee} */
xyz.swapee.wc.IDealBrokerController.prototype.setNetworkFee = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetNetworkFee} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetNetworkFee = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setPartnerFee} */
xyz.swapee.wc.IDealBrokerController.prototype.setPartnerFee = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetPartnerFee} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetPartnerFee = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setRate} */
xyz.swapee.wc.IDealBrokerController.prototype.setRate = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetRate} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetRate = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setEstimatedFloatAmountTo} */
xyz.swapee.wc.IDealBrokerController.prototype.setEstimatedFloatAmountTo = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetEstimatedFloatAmountTo} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetEstimatedFloatAmountTo = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setEstimatedFixedAmountTo} */
xyz.swapee.wc.IDealBrokerController.prototype.setEstimatedFixedAmountTo = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetEstimatedFixedAmountTo} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetEstimatedFixedAmountTo = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.setVisibleAmount} */
xyz.swapee.wc.IDealBrokerController.prototype.setVisibleAmount = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.unsetVisibleAmount} */
xyz.swapee.wc.IDealBrokerController.prototype.unsetVisibleAmount = function() {}
/** @type {xyz.swapee.wc.IDealBrokerController.pulseGetOffer} */
xyz.swapee.wc.IDealBrokerController.prototype.pulseGetOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerController&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerController.Initialese>)} xyz.swapee.wc.DealBrokerController.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController} xyz.swapee.wc.IDealBrokerController.typeof */
/**
 * A concrete class of _IDealBrokerController_ instances.
 * @constructor xyz.swapee.wc.DealBrokerController
 * @implements {xyz.swapee.wc.IDealBrokerController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerController.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerController = class extends /** @type {xyz.swapee.wc.DealBrokerController.constructor&xyz.swapee.wc.IDealBrokerController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerController}
 */
xyz.swapee.wc.DealBrokerController.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerController.
 * @interface xyz.swapee.wc.IDealBrokerControllerFields
 */
xyz.swapee.wc.IDealBrokerControllerFields = class { }
/**
 * The inputs to the _IDealBroker_'s controller.
 */
xyz.swapee.wc.IDealBrokerControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IDealBrokerController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IDealBrokerControllerFields.prototype.props = /** @type {xyz.swapee.wc.IDealBrokerController} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerController} */
xyz.swapee.wc.RecordIDealBrokerController

/** @typedef {xyz.swapee.wc.IDealBrokerController} xyz.swapee.wc.BoundIDealBrokerController */

/** @typedef {xyz.swapee.wc.DealBrokerController} xyz.swapee.wc.BoundDealBrokerController */

/** @typedef {xyz.swapee.wc.IDealBrokerPort.Inputs} xyz.swapee.wc.IDealBrokerController.Inputs The inputs to the _IDealBroker_'s controller. */

/** @typedef {xyz.swapee.wc.IDealBrokerPort.WeakInputs} xyz.swapee.wc.IDealBrokerController.WeakInputs The inputs to the _IDealBroker_'s controller. */

/**
 * Contains getters to cast the _IDealBrokerController_ interface.
 * @interface xyz.swapee.wc.IDealBrokerControllerCaster
 */
xyz.swapee.wc.IDealBrokerControllerCaster = class { }
/**
 * Cast the _IDealBrokerController_ instance into the _BoundIDealBrokerController_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerController}
 */
xyz.swapee.wc.IDealBrokerControllerCaster.prototype.asIDealBrokerController
/**
 * Cast the _IDealBrokerController_ instance into the _BoundIDealBrokerProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerProcessor}
 */
xyz.swapee.wc.IDealBrokerControllerCaster.prototype.asIDealBrokerProcessor
/**
 * Access the _DealBrokerController_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerController}
 */
xyz.swapee.wc.IDealBrokerControllerCaster.prototype.superDealBrokerController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__resetPort<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.resetPort = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IDealBrokerController.__setGetOfferError
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setGetOfferError<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setGetOfferError */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setGetOfferError} */
/**
 * Sets the `getOfferError` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setGetOfferError = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetGetOfferError
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetGetOfferError<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetGetOfferError */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetGetOfferError} */
/**
 * Clears the `getOfferError` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetGetOfferError = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__flipGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__flipGettingOffer<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._flipGettingOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.flipGettingOffer} */
/**
 * Flips between the positive and negative values of the `gettingOffer`.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.flipGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__setGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setGettingOffer<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setGettingOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setGettingOffer} */
/**
 * Sets the `gettingOffer` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetGettingOffer<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetGettingOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetGettingOffer} */
/**
 * Clears the `gettingOffer` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetGettingOffer = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IDealBrokerController.__setNetworkFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setNetworkFee<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setNetworkFee */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setNetworkFee} */
/**
 * Sets the `networkFee` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setNetworkFee = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetNetworkFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetNetworkFee<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetNetworkFee */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetNetworkFee} */
/**
 * Clears the `networkFee` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetNetworkFee = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IDealBrokerController.__setPartnerFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setPartnerFee<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setPartnerFee */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setPartnerFee} */
/**
 * Sets the `partnerFee` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setPartnerFee = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetPartnerFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetPartnerFee<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetPartnerFee */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetPartnerFee} */
/**
 * Clears the `partnerFee` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetPartnerFee = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IDealBrokerController.__setRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setRate<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setRate */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setRate} */
/**
 * Sets the `rate` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setRate = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetRate<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetRate */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetRate} */
/**
 * Clears the `rate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetRate = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IDealBrokerController.__setEstimatedFloatAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setEstimatedFloatAmountTo<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setEstimatedFloatAmountTo */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setEstimatedFloatAmountTo} */
/**
 * Sets the `estimatedFloatAmountTo` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setEstimatedFloatAmountTo = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetEstimatedFloatAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetEstimatedFloatAmountTo<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetEstimatedFloatAmountTo */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetEstimatedFloatAmountTo} */
/**
 * Clears the `estimatedFloatAmountTo` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetEstimatedFloatAmountTo = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IDealBrokerController.__setEstimatedFixedAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setEstimatedFixedAmountTo<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setEstimatedFixedAmountTo */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setEstimatedFixedAmountTo} */
/**
 * Sets the `estimatedFixedAmountTo` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setEstimatedFixedAmountTo = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetEstimatedFixedAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetEstimatedFixedAmountTo<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetEstimatedFixedAmountTo */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetEstimatedFixedAmountTo} */
/**
 * Clears the `estimatedFixedAmountTo` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetEstimatedFixedAmountTo = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IDealBrokerController.__setVisibleAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__setVisibleAmount<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._setVisibleAmount */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.setVisibleAmount} */
/**
 * Sets the `visibleAmount` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.setVisibleAmount = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__unsetVisibleAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__unsetVisibleAmount<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._unsetVisibleAmount */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.unsetVisibleAmount} */
/**
 * Clears the `visibleAmount` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.unsetVisibleAmount = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IDealBrokerController.__pulseGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IDealBrokerController.__pulseGetOffer<!xyz.swapee.wc.IDealBrokerController>} xyz.swapee.wc.IDealBrokerController._pulseGetOffer */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerController.pulseGetOffer} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.IDealBrokerController.pulseGetOffer = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IDealBrokerController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/51-IDealBrokerControllerFront.xml}  cfd00e05217e8eeb363d5065f429c709 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IDealBrokerController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.DealBrokerController)} xyz.swapee.wc.front.AbstractDealBrokerController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.DealBrokerController} xyz.swapee.wc.front.DealBrokerController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IDealBrokerController` interface.
 * @constructor xyz.swapee.wc.front.AbstractDealBrokerController
 */
xyz.swapee.wc.front.AbstractDealBrokerController = class extends /** @type {xyz.swapee.wc.front.AbstractDealBrokerController.constructor&xyz.swapee.wc.front.DealBrokerController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractDealBrokerController.prototype.constructor = xyz.swapee.wc.front.AbstractDealBrokerController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractDealBrokerController.class = /** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerController|typeof xyz.swapee.wc.front.DealBrokerController)|(!xyz.swapee.wc.front.IDealBrokerControllerAT|typeof xyz.swapee.wc.front.DealBrokerControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractDealBrokerController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractDealBrokerController}
 */
xyz.swapee.wc.front.AbstractDealBrokerController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerController}
 */
xyz.swapee.wc.front.AbstractDealBrokerController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerController|typeof xyz.swapee.wc.front.DealBrokerController)|(!xyz.swapee.wc.front.IDealBrokerControllerAT|typeof xyz.swapee.wc.front.DealBrokerControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerController}
 */
xyz.swapee.wc.front.AbstractDealBrokerController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerController|typeof xyz.swapee.wc.front.DealBrokerController)|(!xyz.swapee.wc.front.IDealBrokerControllerAT|typeof xyz.swapee.wc.front.DealBrokerControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerController}
 */
xyz.swapee.wc.front.AbstractDealBrokerController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IDealBrokerController.Initialese[]) => xyz.swapee.wc.front.IDealBrokerController} xyz.swapee.wc.front.DealBrokerControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IDealBrokerControllerCaster&xyz.swapee.wc.front.IDealBrokerControllerAT)} xyz.swapee.wc.front.IDealBrokerController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerControllerAT} xyz.swapee.wc.front.IDealBrokerControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IDealBrokerController
 */
xyz.swapee.wc.front.IDealBrokerController = class extends /** @type {xyz.swapee.wc.front.IDealBrokerController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IDealBrokerControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IDealBrokerController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setGetOfferError} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setGetOfferError = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetGetOfferError} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetGetOfferError = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.flipGettingOffer} */
xyz.swapee.wc.front.IDealBrokerController.prototype.flipGettingOffer = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setGettingOffer} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setGettingOffer = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetGettingOffer} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetGettingOffer = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setNetworkFee} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setNetworkFee = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetNetworkFee} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetNetworkFee = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setPartnerFee} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setPartnerFee = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetPartnerFee} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetPartnerFee = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setRate} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setRate = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetRate} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetRate = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setEstimatedFloatAmountTo} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setEstimatedFloatAmountTo = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetEstimatedFloatAmountTo} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetEstimatedFloatAmountTo = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setEstimatedFixedAmountTo} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setEstimatedFixedAmountTo = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetEstimatedFixedAmountTo} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetEstimatedFixedAmountTo = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.setVisibleAmount} */
xyz.swapee.wc.front.IDealBrokerController.prototype.setVisibleAmount = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.unsetVisibleAmount} */
xyz.swapee.wc.front.IDealBrokerController.prototype.unsetVisibleAmount = function() {}
/** @type {xyz.swapee.wc.front.IDealBrokerController.pulseGetOffer} */
xyz.swapee.wc.front.IDealBrokerController.prototype.pulseGetOffer = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IDealBrokerController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerController.Initialese>)} xyz.swapee.wc.front.DealBrokerController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController} xyz.swapee.wc.front.IDealBrokerController.typeof */
/**
 * A concrete class of _IDealBrokerController_ instances.
 * @constructor xyz.swapee.wc.front.DealBrokerController
 * @implements {xyz.swapee.wc.front.IDealBrokerController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerController.Initialese>} ‎
 */
xyz.swapee.wc.front.DealBrokerController = class extends /** @type {xyz.swapee.wc.front.DealBrokerController.constructor&xyz.swapee.wc.front.IDealBrokerController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IDealBrokerController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.DealBrokerController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.DealBrokerController}
 */
xyz.swapee.wc.front.DealBrokerController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IDealBrokerController} */
xyz.swapee.wc.front.RecordIDealBrokerController

/** @typedef {xyz.swapee.wc.front.IDealBrokerController} xyz.swapee.wc.front.BoundIDealBrokerController */

/** @typedef {xyz.swapee.wc.front.DealBrokerController} xyz.swapee.wc.front.BoundDealBrokerController */

/**
 * Contains getters to cast the _IDealBrokerController_ interface.
 * @interface xyz.swapee.wc.front.IDealBrokerControllerCaster
 */
xyz.swapee.wc.front.IDealBrokerControllerCaster = class { }
/**
 * Cast the _IDealBrokerController_ instance into the _BoundIDealBrokerController_ type.
 * @type {!xyz.swapee.wc.front.BoundIDealBrokerController}
 */
xyz.swapee.wc.front.IDealBrokerControllerCaster.prototype.asIDealBrokerController
/**
 * Access the _DealBrokerController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundDealBrokerController}
 */
xyz.swapee.wc.front.IDealBrokerControllerCaster.prototype.superDealBrokerController

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IDealBrokerController.__setGetOfferError
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setGetOfferError<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setGetOfferError */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setGetOfferError} */
/**
 * Sets the `getOfferError` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setGetOfferError = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetGetOfferError
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetGetOfferError<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetGetOfferError */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetGetOfferError} */
/**
 * Clears the `getOfferError` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetGetOfferError = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__flipGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__flipGettingOffer<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._flipGettingOffer */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.flipGettingOffer} */
/**
 * Flips between the positive and negative values of the `gettingOffer`.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.flipGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__setGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setGettingOffer<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setGettingOffer */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setGettingOffer} */
/**
 * Sets the `gettingOffer` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setGettingOffer = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetGettingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetGettingOffer<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetGettingOffer */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetGettingOffer} */
/**
 * Clears the `gettingOffer` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetGettingOffer = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IDealBrokerController.__setNetworkFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setNetworkFee<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setNetworkFee */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setNetworkFee} */
/**
 * Sets the `networkFee` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setNetworkFee = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetNetworkFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetNetworkFee<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetNetworkFee */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetNetworkFee} */
/**
 * Clears the `networkFee` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetNetworkFee = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IDealBrokerController.__setPartnerFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setPartnerFee<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setPartnerFee */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setPartnerFee} */
/**
 * Sets the `partnerFee` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setPartnerFee = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetPartnerFee
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetPartnerFee<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetPartnerFee */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetPartnerFee} */
/**
 * Clears the `partnerFee` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetPartnerFee = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IDealBrokerController.__setRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setRate<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setRate */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setRate} */
/**
 * Sets the `rate` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setRate = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetRate
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetRate<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetRate */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetRate} */
/**
 * Clears the `rate` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetRate = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IDealBrokerController.__setEstimatedFloatAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setEstimatedFloatAmountTo<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setEstimatedFloatAmountTo */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setEstimatedFloatAmountTo} */
/**
 * Sets the `estimatedFloatAmountTo` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setEstimatedFloatAmountTo = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetEstimatedFloatAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetEstimatedFloatAmountTo<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetEstimatedFloatAmountTo */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetEstimatedFloatAmountTo} */
/**
 * Clears the `estimatedFloatAmountTo` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetEstimatedFloatAmountTo = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IDealBrokerController.__setEstimatedFixedAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setEstimatedFixedAmountTo<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setEstimatedFixedAmountTo */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setEstimatedFixedAmountTo} */
/**
 * Sets the `estimatedFixedAmountTo` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setEstimatedFixedAmountTo = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetEstimatedFixedAmountTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetEstimatedFixedAmountTo<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetEstimatedFixedAmountTo */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetEstimatedFixedAmountTo} */
/**
 * Clears the `estimatedFixedAmountTo` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetEstimatedFixedAmountTo = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IDealBrokerController.__setVisibleAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__setVisibleAmount<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._setVisibleAmount */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.setVisibleAmount} */
/**
 * Sets the `visibleAmount` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.setVisibleAmount = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__unsetVisibleAmount
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__unsetVisibleAmount<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._unsetVisibleAmount */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.unsetVisibleAmount} */
/**
 * Clears the `visibleAmount` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IDealBrokerController.unsetVisibleAmount = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IDealBrokerController.__pulseGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IDealBrokerController.__pulseGetOffer<!xyz.swapee.wc.front.IDealBrokerController>} xyz.swapee.wc.front.IDealBrokerController._pulseGetOffer */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerController.pulseGetOffer} */
/** @return {void} */
xyz.swapee.wc.front.IDealBrokerController.pulseGetOffer = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IDealBrokerController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/52-IDealBrokerControllerBack.xml}  b05ff9eeabb8590353ba6b92cd8e0431 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IDealBrokerController.Inputs>&xyz.swapee.wc.IDealBrokerController.Initialese} xyz.swapee.wc.back.IDealBrokerController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerController)} xyz.swapee.wc.back.AbstractDealBrokerController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerController} xyz.swapee.wc.back.DealBrokerController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerController` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerController
 */
xyz.swapee.wc.back.AbstractDealBrokerController = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerController.constructor&xyz.swapee.wc.back.DealBrokerController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerController.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerController.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerController|typeof xyz.swapee.wc.back.DealBrokerController)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerController}
 */
xyz.swapee.wc.back.AbstractDealBrokerController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerController}
 */
xyz.swapee.wc.back.AbstractDealBrokerController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerController|typeof xyz.swapee.wc.back.DealBrokerController)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerController}
 */
xyz.swapee.wc.back.AbstractDealBrokerController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerController|typeof xyz.swapee.wc.back.DealBrokerController)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerController}
 */
xyz.swapee.wc.back.AbstractDealBrokerController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerController.Initialese[]) => xyz.swapee.wc.back.IDealBrokerController} xyz.swapee.wc.back.DealBrokerControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerControllerCaster&xyz.swapee.wc.IDealBrokerController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IDealBrokerController.Inputs>)} xyz.swapee.wc.back.IDealBrokerController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IDealBrokerController
 */
xyz.swapee.wc.back.IDealBrokerController = class extends /** @type {xyz.swapee.wc.back.IDealBrokerController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IDealBrokerController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerController.Initialese>)} xyz.swapee.wc.back.DealBrokerController.constructor */
/**
 * A concrete class of _IDealBrokerController_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerController
 * @implements {xyz.swapee.wc.back.IDealBrokerController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerController.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerController = class extends /** @type {xyz.swapee.wc.back.DealBrokerController.constructor&xyz.swapee.wc.back.IDealBrokerController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerController}
 */
xyz.swapee.wc.back.DealBrokerController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerController} */
xyz.swapee.wc.back.RecordIDealBrokerController

/** @typedef {xyz.swapee.wc.back.IDealBrokerController} xyz.swapee.wc.back.BoundIDealBrokerController */

/** @typedef {xyz.swapee.wc.back.DealBrokerController} xyz.swapee.wc.back.BoundDealBrokerController */

/**
 * Contains getters to cast the _IDealBrokerController_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerControllerCaster
 */
xyz.swapee.wc.back.IDealBrokerControllerCaster = class { }
/**
 * Cast the _IDealBrokerController_ instance into the _BoundIDealBrokerController_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerController}
 */
xyz.swapee.wc.back.IDealBrokerControllerCaster.prototype.asIDealBrokerController
/**
 * Access the _DealBrokerController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerController}
 */
xyz.swapee.wc.back.IDealBrokerControllerCaster.prototype.superDealBrokerController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/53-IDealBrokerControllerAR.xml}  22c331357d852431c3bcf145845e6782 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IDealBrokerController.Initialese} xyz.swapee.wc.back.IDealBrokerControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerControllerAR)} xyz.swapee.wc.back.AbstractDealBrokerControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerControllerAR} xyz.swapee.wc.back.DealBrokerControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerControllerAR
 */
xyz.swapee.wc.back.AbstractDealBrokerControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerControllerAR.constructor&xyz.swapee.wc.back.DealBrokerControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerControllerAR|typeof xyz.swapee.wc.back.DealBrokerControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerControllerAR|typeof xyz.swapee.wc.back.DealBrokerControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerControllerAR|typeof xyz.swapee.wc.back.DealBrokerControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerController|typeof xyz.swapee.wc.DealBrokerController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerControllerAR}
 */
xyz.swapee.wc.back.AbstractDealBrokerControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerControllerAR.Initialese[]) => xyz.swapee.wc.back.IDealBrokerControllerAR} xyz.swapee.wc.back.DealBrokerControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IDealBrokerController)} xyz.swapee.wc.back.IDealBrokerControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IDealBrokerControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IDealBrokerControllerAR
 */
xyz.swapee.wc.back.IDealBrokerControllerAR = class extends /** @type {xyz.swapee.wc.back.IDealBrokerControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IDealBrokerController.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerControllerAR.Initialese>)} xyz.swapee.wc.back.DealBrokerControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerControllerAR} xyz.swapee.wc.back.IDealBrokerControllerAR.typeof */
/**
 * A concrete class of _IDealBrokerControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerControllerAR
 * @implements {xyz.swapee.wc.back.IDealBrokerControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IDealBrokerControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerControllerAR = class extends /** @type {xyz.swapee.wc.back.DealBrokerControllerAR.constructor&xyz.swapee.wc.back.IDealBrokerControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerControllerAR}
 */
xyz.swapee.wc.back.DealBrokerControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerControllerAR} */
xyz.swapee.wc.back.RecordIDealBrokerControllerAR

/** @typedef {xyz.swapee.wc.back.IDealBrokerControllerAR} xyz.swapee.wc.back.BoundIDealBrokerControllerAR */

/** @typedef {xyz.swapee.wc.back.DealBrokerControllerAR} xyz.swapee.wc.back.BoundDealBrokerControllerAR */

/**
 * Contains getters to cast the _IDealBrokerControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerControllerARCaster
 */
xyz.swapee.wc.back.IDealBrokerControllerARCaster = class { }
/**
 * Cast the _IDealBrokerControllerAR_ instance into the _BoundIDealBrokerControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerControllerAR}
 */
xyz.swapee.wc.back.IDealBrokerControllerARCaster.prototype.asIDealBrokerControllerAR
/**
 * Access the _DealBrokerControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerControllerAR}
 */
xyz.swapee.wc.back.IDealBrokerControllerARCaster.prototype.superDealBrokerControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/54-IDealBrokerControllerAT.xml}  2042e950948c639e5d5a81218d23a45d */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IDealBrokerControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.DealBrokerControllerAT)} xyz.swapee.wc.front.AbstractDealBrokerControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.DealBrokerControllerAT} xyz.swapee.wc.front.DealBrokerControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IDealBrokerControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractDealBrokerControllerAT
 */
xyz.swapee.wc.front.AbstractDealBrokerControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractDealBrokerControllerAT.constructor&xyz.swapee.wc.front.DealBrokerControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractDealBrokerControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractDealBrokerControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractDealBrokerControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerControllerAT|typeof xyz.swapee.wc.front.DealBrokerControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractDealBrokerControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractDealBrokerControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerControllerAT|typeof xyz.swapee.wc.front.DealBrokerControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerControllerAT|typeof xyz.swapee.wc.front.DealBrokerControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerControllerAT}
 */
xyz.swapee.wc.front.AbstractDealBrokerControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IDealBrokerControllerAT.Initialese[]) => xyz.swapee.wc.front.IDealBrokerControllerAT} xyz.swapee.wc.front.DealBrokerControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IDealBrokerControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IDealBrokerControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IDealBrokerControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IDealBrokerControllerAT
 */
xyz.swapee.wc.front.IDealBrokerControllerAT = class extends /** @type {xyz.swapee.wc.front.IDealBrokerControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IDealBrokerControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IDealBrokerControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerControllerAT.Initialese>)} xyz.swapee.wc.front.DealBrokerControllerAT.constructor */
/**
 * A concrete class of _IDealBrokerControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.DealBrokerControllerAT
 * @implements {xyz.swapee.wc.front.IDealBrokerControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IDealBrokerControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.DealBrokerControllerAT = class extends /** @type {xyz.swapee.wc.front.DealBrokerControllerAT.constructor&xyz.swapee.wc.front.IDealBrokerControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IDealBrokerControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.DealBrokerControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.DealBrokerControllerAT}
 */
xyz.swapee.wc.front.DealBrokerControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IDealBrokerControllerAT} */
xyz.swapee.wc.front.RecordIDealBrokerControllerAT

/** @typedef {xyz.swapee.wc.front.IDealBrokerControllerAT} xyz.swapee.wc.front.BoundIDealBrokerControllerAT */

/** @typedef {xyz.swapee.wc.front.DealBrokerControllerAT} xyz.swapee.wc.front.BoundDealBrokerControllerAT */

/**
 * Contains getters to cast the _IDealBrokerControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IDealBrokerControllerATCaster
 */
xyz.swapee.wc.front.IDealBrokerControllerATCaster = class { }
/**
 * Cast the _IDealBrokerControllerAT_ instance into the _BoundIDealBrokerControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIDealBrokerControllerAT}
 */
xyz.swapee.wc.front.IDealBrokerControllerATCaster.prototype.asIDealBrokerControllerAT
/**
 * Access the _DealBrokerControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundDealBrokerControllerAT}
 */
xyz.swapee.wc.front.IDealBrokerControllerATCaster.prototype.superDealBrokerControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/70-IDealBrokerScreen.xml}  1ddc89af1d88e036ab73f78966778a37 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.front.DealBrokerInputs, !HTMLDivElement, !xyz.swapee.wc.IDealBrokerDisplay.Settings, !xyz.swapee.wc.IDealBrokerDisplay.Queries, null>&xyz.swapee.wc.IDealBrokerDisplay.Initialese} xyz.swapee.wc.IDealBrokerScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerScreen)} xyz.swapee.wc.AbstractDealBrokerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerScreen} xyz.swapee.wc.DealBrokerScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerScreen` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerScreen
 */
xyz.swapee.wc.AbstractDealBrokerScreen = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerScreen.constructor&xyz.swapee.wc.DealBrokerScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerScreen.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerScreen.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerScreen|typeof xyz.swapee.wc.DealBrokerScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IDealBrokerController|typeof xyz.swapee.wc.front.DealBrokerController)|(!xyz.swapee.wc.IDealBrokerDisplay|typeof xyz.swapee.wc.DealBrokerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerScreen}
 */
xyz.swapee.wc.AbstractDealBrokerScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerScreen}
 */
xyz.swapee.wc.AbstractDealBrokerScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerScreen|typeof xyz.swapee.wc.DealBrokerScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IDealBrokerController|typeof xyz.swapee.wc.front.DealBrokerController)|(!xyz.swapee.wc.IDealBrokerDisplay|typeof xyz.swapee.wc.DealBrokerDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerScreen}
 */
xyz.swapee.wc.AbstractDealBrokerScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerScreen|typeof xyz.swapee.wc.DealBrokerScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IDealBrokerController|typeof xyz.swapee.wc.front.DealBrokerController)|(!xyz.swapee.wc.IDealBrokerDisplay|typeof xyz.swapee.wc.DealBrokerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerScreen}
 */
xyz.swapee.wc.AbstractDealBrokerScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerScreen.Initialese[]) => xyz.swapee.wc.IDealBrokerScreen} xyz.swapee.wc.DealBrokerScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.DealBrokerMemory, !xyz.swapee.wc.front.DealBrokerInputs, !HTMLDivElement, !xyz.swapee.wc.IDealBrokerDisplay.Settings, !xyz.swapee.wc.IDealBrokerDisplay.Queries, null, null>&xyz.swapee.wc.front.IDealBrokerController&xyz.swapee.wc.IDealBrokerDisplay)} xyz.swapee.wc.IDealBrokerScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IDealBrokerScreen
 */
xyz.swapee.wc.IDealBrokerScreen = class extends /** @type {xyz.swapee.wc.IDealBrokerScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IDealBrokerController.typeof&xyz.swapee.wc.IDealBrokerDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerScreen.Initialese>)} xyz.swapee.wc.DealBrokerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IDealBrokerScreen} xyz.swapee.wc.IDealBrokerScreen.typeof */
/**
 * A concrete class of _IDealBrokerScreen_ instances.
 * @constructor xyz.swapee.wc.DealBrokerScreen
 * @implements {xyz.swapee.wc.IDealBrokerScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerScreen.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerScreen = class extends /** @type {xyz.swapee.wc.DealBrokerScreen.constructor&xyz.swapee.wc.IDealBrokerScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerScreen}
 */
xyz.swapee.wc.DealBrokerScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IDealBrokerScreen} */
xyz.swapee.wc.RecordIDealBrokerScreen

/** @typedef {xyz.swapee.wc.IDealBrokerScreen} xyz.swapee.wc.BoundIDealBrokerScreen */

/** @typedef {xyz.swapee.wc.DealBrokerScreen} xyz.swapee.wc.BoundDealBrokerScreen */

/**
 * Contains getters to cast the _IDealBrokerScreen_ interface.
 * @interface xyz.swapee.wc.IDealBrokerScreenCaster
 */
xyz.swapee.wc.IDealBrokerScreenCaster = class { }
/**
 * Cast the _IDealBrokerScreen_ instance into the _BoundIDealBrokerScreen_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerScreen}
 */
xyz.swapee.wc.IDealBrokerScreenCaster.prototype.asIDealBrokerScreen
/**
 * Access the _DealBrokerScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerScreen}
 */
xyz.swapee.wc.IDealBrokerScreenCaster.prototype.superDealBrokerScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/70-IDealBrokerScreenBack.xml}  94101afdec5777cdbf0dd234c6809f80 */
/** @typedef {xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese} xyz.swapee.wc.back.IDealBrokerScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerScreen)} xyz.swapee.wc.back.AbstractDealBrokerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerScreen} xyz.swapee.wc.back.DealBrokerScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerScreen
 */
xyz.swapee.wc.back.AbstractDealBrokerScreen = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerScreen.constructor&xyz.swapee.wc.back.DealBrokerScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerScreen.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerScreen|typeof xyz.swapee.wc.back.DealBrokerScreen)|(!xyz.swapee.wc.back.IDealBrokerScreenAT|typeof xyz.swapee.wc.back.DealBrokerScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerScreen|typeof xyz.swapee.wc.back.DealBrokerScreen)|(!xyz.swapee.wc.back.IDealBrokerScreenAT|typeof xyz.swapee.wc.back.DealBrokerScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerScreen|typeof xyz.swapee.wc.back.DealBrokerScreen)|(!xyz.swapee.wc.back.IDealBrokerScreenAT|typeof xyz.swapee.wc.back.DealBrokerScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreen}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerScreen.Initialese[]) => xyz.swapee.wc.back.IDealBrokerScreen} xyz.swapee.wc.back.DealBrokerScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerScreenCaster&xyz.swapee.wc.back.IDealBrokerScreenAT)} xyz.swapee.wc.back.IDealBrokerScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IDealBrokerScreenAT} xyz.swapee.wc.back.IDealBrokerScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IDealBrokerScreen
 */
xyz.swapee.wc.back.IDealBrokerScreen = class extends /** @type {xyz.swapee.wc.back.IDealBrokerScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IDealBrokerScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerScreen.Initialese>)} xyz.swapee.wc.back.DealBrokerScreen.constructor */
/**
 * A concrete class of _IDealBrokerScreen_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerScreen
 * @implements {xyz.swapee.wc.back.IDealBrokerScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerScreen = class extends /** @type {xyz.swapee.wc.back.DealBrokerScreen.constructor&xyz.swapee.wc.back.IDealBrokerScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreen}
 */
xyz.swapee.wc.back.DealBrokerScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerScreen} */
xyz.swapee.wc.back.RecordIDealBrokerScreen

/** @typedef {xyz.swapee.wc.back.IDealBrokerScreen} xyz.swapee.wc.back.BoundIDealBrokerScreen */

/** @typedef {xyz.swapee.wc.back.DealBrokerScreen} xyz.swapee.wc.back.BoundDealBrokerScreen */

/**
 * Contains getters to cast the _IDealBrokerScreen_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerScreenCaster
 */
xyz.swapee.wc.back.IDealBrokerScreenCaster = class { }
/**
 * Cast the _IDealBrokerScreen_ instance into the _BoundIDealBrokerScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerScreen}
 */
xyz.swapee.wc.back.IDealBrokerScreenCaster.prototype.asIDealBrokerScreen
/**
 * Access the _DealBrokerScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerScreen}
 */
xyz.swapee.wc.back.IDealBrokerScreenCaster.prototype.superDealBrokerScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/73-IDealBrokerScreenAR.xml}  92c270e65738b6e8f2539be98984badd */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IDealBrokerScreen.Initialese} xyz.swapee.wc.front.IDealBrokerScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.DealBrokerScreenAR)} xyz.swapee.wc.front.AbstractDealBrokerScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.DealBrokerScreenAR} xyz.swapee.wc.front.DealBrokerScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IDealBrokerScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractDealBrokerScreenAR
 */
xyz.swapee.wc.front.AbstractDealBrokerScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractDealBrokerScreenAR.constructor&xyz.swapee.wc.front.DealBrokerScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractDealBrokerScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractDealBrokerScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractDealBrokerScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerScreenAR|typeof xyz.swapee.wc.front.DealBrokerScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerScreen|typeof xyz.swapee.wc.DealBrokerScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractDealBrokerScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractDealBrokerScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerScreenAR|typeof xyz.swapee.wc.front.DealBrokerScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerScreen|typeof xyz.swapee.wc.DealBrokerScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IDealBrokerScreenAR|typeof xyz.swapee.wc.front.DealBrokerScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IDealBrokerScreen|typeof xyz.swapee.wc.DealBrokerScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.DealBrokerScreenAR}
 */
xyz.swapee.wc.front.AbstractDealBrokerScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IDealBrokerScreenAR.Initialese[]) => xyz.swapee.wc.front.IDealBrokerScreenAR} xyz.swapee.wc.front.DealBrokerScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IDealBrokerScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IDealBrokerScreen)} xyz.swapee.wc.front.IDealBrokerScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IDealBrokerScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IDealBrokerScreenAR
 */
xyz.swapee.wc.front.IDealBrokerScreenAR = class extends /** @type {xyz.swapee.wc.front.IDealBrokerScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IDealBrokerScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IDealBrokerScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IDealBrokerScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerScreenAR.Initialese>)} xyz.swapee.wc.front.DealBrokerScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IDealBrokerScreenAR} xyz.swapee.wc.front.IDealBrokerScreenAR.typeof */
/**
 * A concrete class of _IDealBrokerScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.DealBrokerScreenAR
 * @implements {xyz.swapee.wc.front.IDealBrokerScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IDealBrokerScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IDealBrokerScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.DealBrokerScreenAR = class extends /** @type {xyz.swapee.wc.front.DealBrokerScreenAR.constructor&xyz.swapee.wc.front.IDealBrokerScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IDealBrokerScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IDealBrokerScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.DealBrokerScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.DealBrokerScreenAR}
 */
xyz.swapee.wc.front.DealBrokerScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IDealBrokerScreenAR} */
xyz.swapee.wc.front.RecordIDealBrokerScreenAR

/** @typedef {xyz.swapee.wc.front.IDealBrokerScreenAR} xyz.swapee.wc.front.BoundIDealBrokerScreenAR */

/** @typedef {xyz.swapee.wc.front.DealBrokerScreenAR} xyz.swapee.wc.front.BoundDealBrokerScreenAR */

/**
 * Contains getters to cast the _IDealBrokerScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IDealBrokerScreenARCaster
 */
xyz.swapee.wc.front.IDealBrokerScreenARCaster = class { }
/**
 * Cast the _IDealBrokerScreenAR_ instance into the _BoundIDealBrokerScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIDealBrokerScreenAR}
 */
xyz.swapee.wc.front.IDealBrokerScreenARCaster.prototype.asIDealBrokerScreenAR
/**
 * Access the _DealBrokerScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundDealBrokerScreenAR}
 */
xyz.swapee.wc.front.IDealBrokerScreenARCaster.prototype.superDealBrokerScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/74-IDealBrokerScreenAT.xml}  8a4797a6f85cab513ba849b654b01fb4 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.DealBrokerScreenAT)} xyz.swapee.wc.back.AbstractDealBrokerScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.DealBrokerScreenAT} xyz.swapee.wc.back.DealBrokerScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IDealBrokerScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractDealBrokerScreenAT
 */
xyz.swapee.wc.back.AbstractDealBrokerScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractDealBrokerScreenAT.constructor&xyz.swapee.wc.back.DealBrokerScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractDealBrokerScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractDealBrokerScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractDealBrokerScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerScreenAT|typeof xyz.swapee.wc.back.DealBrokerScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractDealBrokerScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractDealBrokerScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerScreenAT|typeof xyz.swapee.wc.back.DealBrokerScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IDealBrokerScreenAT|typeof xyz.swapee.wc.back.DealBrokerScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreenAT}
 */
xyz.swapee.wc.back.AbstractDealBrokerScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese[]) => xyz.swapee.wc.back.IDealBrokerScreenAT} xyz.swapee.wc.back.DealBrokerScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IDealBrokerScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IDealBrokerScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IDealBrokerScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IDealBrokerScreenAT
 */
xyz.swapee.wc.back.IDealBrokerScreenAT = class extends /** @type {xyz.swapee.wc.back.IDealBrokerScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IDealBrokerScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IDealBrokerScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese>)} xyz.swapee.wc.back.DealBrokerScreenAT.constructor */
/**
 * A concrete class of _IDealBrokerScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.DealBrokerScreenAT
 * @implements {xyz.swapee.wc.back.IDealBrokerScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IDealBrokerScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.DealBrokerScreenAT = class extends /** @type {xyz.swapee.wc.back.DealBrokerScreenAT.constructor&xyz.swapee.wc.back.IDealBrokerScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IDealBrokerScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.DealBrokerScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.DealBrokerScreenAT}
 */
xyz.swapee.wc.back.DealBrokerScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IDealBrokerScreenAT} */
xyz.swapee.wc.back.RecordIDealBrokerScreenAT

/** @typedef {xyz.swapee.wc.back.IDealBrokerScreenAT} xyz.swapee.wc.back.BoundIDealBrokerScreenAT */

/** @typedef {xyz.swapee.wc.back.DealBrokerScreenAT} xyz.swapee.wc.back.BoundDealBrokerScreenAT */

/**
 * Contains getters to cast the _IDealBrokerScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IDealBrokerScreenATCaster
 */
xyz.swapee.wc.back.IDealBrokerScreenATCaster = class { }
/**
 * Cast the _IDealBrokerScreenAT_ instance into the _BoundIDealBrokerScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIDealBrokerScreenAT}
 */
xyz.swapee.wc.back.IDealBrokerScreenATCaster.prototype.asIDealBrokerScreenAT
/**
 * Access the _DealBrokerScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundDealBrokerScreenAT}
 */
xyz.swapee.wc.back.IDealBrokerScreenATCaster.prototype.superDealBrokerScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/deal-broker/DealBroker.mvc/design/80-IDealBrokerGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IDealBrokerDisplay.Initialese} xyz.swapee.wc.IDealBrokerGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.DealBrokerGPU)} xyz.swapee.wc.AbstractDealBrokerGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.DealBrokerGPU} xyz.swapee.wc.DealBrokerGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IDealBrokerGPU` interface.
 * @constructor xyz.swapee.wc.AbstractDealBrokerGPU
 */
xyz.swapee.wc.AbstractDealBrokerGPU = class extends /** @type {xyz.swapee.wc.AbstractDealBrokerGPU.constructor&xyz.swapee.wc.DealBrokerGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractDealBrokerGPU.prototype.constructor = xyz.swapee.wc.AbstractDealBrokerGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractDealBrokerGPU.class = /** @type {typeof xyz.swapee.wc.AbstractDealBrokerGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IDealBrokerGPU|typeof xyz.swapee.wc.DealBrokerGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IDealBrokerDisplay|typeof xyz.swapee.wc.back.DealBrokerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractDealBrokerGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractDealBrokerGPU}
 */
xyz.swapee.wc.AbstractDealBrokerGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerGPU}
 */
xyz.swapee.wc.AbstractDealBrokerGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IDealBrokerGPU|typeof xyz.swapee.wc.DealBrokerGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IDealBrokerDisplay|typeof xyz.swapee.wc.back.DealBrokerDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerGPU}
 */
xyz.swapee.wc.AbstractDealBrokerGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IDealBrokerGPU|typeof xyz.swapee.wc.DealBrokerGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IDealBrokerDisplay|typeof xyz.swapee.wc.back.DealBrokerDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.DealBrokerGPU}
 */
xyz.swapee.wc.AbstractDealBrokerGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IDealBrokerGPU.Initialese[]) => xyz.swapee.wc.IDealBrokerGPU} xyz.swapee.wc.DealBrokerGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IDealBrokerGPUCaster&com.webcircuits.IBrowserView<.!DealBrokerMemory,>&xyz.swapee.wc.back.IDealBrokerDisplay)} xyz.swapee.wc.IDealBrokerGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!DealBrokerMemory,>} com.webcircuits.IBrowserView<.!DealBrokerMemory,>.typeof */
/**
 * Handles the periphery of the _IDealBrokerDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IDealBrokerGPU
 */
xyz.swapee.wc.IDealBrokerGPU = class extends /** @type {xyz.swapee.wc.IDealBrokerGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!DealBrokerMemory,>.typeof&xyz.swapee.wc.back.IDealBrokerDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IDealBrokerGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IDealBrokerGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IDealBrokerGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerGPU.Initialese>)} xyz.swapee.wc.DealBrokerGPU.constructor */
/**
 * A concrete class of _IDealBrokerGPU_ instances.
 * @constructor xyz.swapee.wc.DealBrokerGPU
 * @implements {xyz.swapee.wc.IDealBrokerGPU} Handles the periphery of the _IDealBrokerDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IDealBrokerGPU.Initialese>} ‎
 */
xyz.swapee.wc.DealBrokerGPU = class extends /** @type {xyz.swapee.wc.DealBrokerGPU.constructor&xyz.swapee.wc.IDealBrokerGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IDealBrokerGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IDealBrokerGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IDealBrokerGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IDealBrokerGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.DealBrokerGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.DealBrokerGPU}
 */
xyz.swapee.wc.DealBrokerGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IDealBrokerGPU.
 * @interface xyz.swapee.wc.IDealBrokerGPUFields
 */
xyz.swapee.wc.IDealBrokerGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IDealBrokerGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IDealBrokerGPU} */
xyz.swapee.wc.RecordIDealBrokerGPU

/** @typedef {xyz.swapee.wc.IDealBrokerGPU} xyz.swapee.wc.BoundIDealBrokerGPU */

/** @typedef {xyz.swapee.wc.DealBrokerGPU} xyz.swapee.wc.BoundDealBrokerGPU */

/**
 * Contains getters to cast the _IDealBrokerGPU_ interface.
 * @interface xyz.swapee.wc.IDealBrokerGPUCaster
 */
xyz.swapee.wc.IDealBrokerGPUCaster = class { }
/**
 * Cast the _IDealBrokerGPU_ instance into the _BoundIDealBrokerGPU_ type.
 * @type {!xyz.swapee.wc.BoundIDealBrokerGPU}
 */
xyz.swapee.wc.IDealBrokerGPUCaster.prototype.asIDealBrokerGPU
/**
 * Access the _DealBrokerGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundDealBrokerGPU}
 */
xyz.swapee.wc.IDealBrokerGPUCaster.prototype.superDealBrokerGPU

// nss:xyz.swapee.wc
/* @typal-end */