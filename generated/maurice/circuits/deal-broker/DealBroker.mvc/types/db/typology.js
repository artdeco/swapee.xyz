/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IDealBrokerComputer': {
  'id': 34272263141,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptEstimatedAmountTo': 2
  }
 },
 'xyz.swapee.wc.DealBrokerMemoryPQs': {
  'id': 34272263142,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerOuterCore': {
  'id': 34272263143,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.DealBrokerInputsPQs': {
  'id': 34272263144,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerPort': {
  'id': 34272263145,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetDealBrokerPort': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerPortInterface': {
  'id': 34272263146,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerCore': {
  'id': 34272263147,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetDealBrokerCore': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerProcessor': {
  'id': 34272263148,
  'symbols': {},
  'methods': {
   'pulseGetOffer': 1
  }
 },
 'xyz.swapee.wc.IDealBroker': {
  'id': 34272263149,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerBuffer': {
  'id': 342722631410,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerHtmlComponent': {
  'id': 342722631411,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerElement': {
  'id': 342722631412,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IDealBrokerElementPort': {
  'id': 342722631413,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerDesigner': {
  'id': 342722631414,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IDealBrokerGPU': {
  'id': 342722631415,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerDisplay': {
  'id': 342722631416,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.DealBrokerVdusPQs': {
  'id': 342722631417,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IDealBrokerDisplay': {
  'id': 342722631418,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerController': {
  'id': 342722631419,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setNetworkFee': 2,
   'unsetNetworkFee': 3,
   'setPartnerFee': 4,
   'unsetPartnerFee': 5,
   'setVisibleAmount': 12,
   'unsetVisibleAmount': 13,
   'setGetOfferError': 14,
   'unsetGetOfferError': 15,
   'flipGettingOffer': 16,
   'setGettingOffer': 17,
   'unsetGettingOffer': 18,
   'pulseGetOffer': 19,
   'setEstimatedFloatAmountTo': 20,
   'unsetEstimatedFloatAmountTo': 21,
   'setEstimatedFixedAmountTo': 22,
   'unsetEstimatedFixedAmountTo': 23
  }
 },
 'xyz.swapee.wc.front.IDealBrokerController': {
  'id': 342722631420,
  'symbols': {},
  'methods': {
   'setNetworkFee': 1,
   'unsetNetworkFee': 2,
   'setPartnerFee': 3,
   'unsetPartnerFee': 4,
   'setVisibleAmount': 11,
   'unsetVisibleAmount': 12,
   'setGetOfferError': 13,
   'unsetGetOfferError': 14,
   'flipGettingOffer': 15,
   'setGettingOffer': 16,
   'unsetGettingOffer': 17,
   'pulseGetOffer': 18,
   'setEstimatedFloatAmountTo': 19,
   'unsetEstimatedFloatAmountTo': 20,
   'setEstimatedFixedAmountTo': 21,
   'unsetEstimatedFixedAmountTo': 22
  }
 },
 'xyz.swapee.wc.back.IDealBrokerController': {
  'id': 342722631421,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerControllerAR': {
  'id': 342722631422,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerControllerAT': {
  'id': 342722631423,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerScreen': {
  'id': 342722631424,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerScreen': {
  'id': 342722631425,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerScreenAR': {
  'id': 342722631426,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerScreenAT': {
  'id': 342722631427,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.DealBrokerCachePQs': {
  'id': 342722631428,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})