import {DealBrokerVdusPQs} from './DealBrokerVdusPQs'
export const DealBrokerVdusQPs=/**@type {!xyz.swapee.wc.DealBrokerVdusQPs}*/(Object.keys(DealBrokerVdusPQs)
 .reduce((a,k)=>{a[DealBrokerVdusPQs[k]]=k;return a},{}))