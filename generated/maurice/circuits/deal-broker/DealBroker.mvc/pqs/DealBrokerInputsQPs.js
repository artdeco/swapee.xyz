import {DealBrokerInputsPQs} from './DealBrokerInputsPQs'
export const DealBrokerInputsQPs=/**@type {!xyz.swapee.wc.DealBrokerInputsQPs}*/(Object.keys(DealBrokerInputsPQs)
 .reduce((a,k)=>{a[DealBrokerInputsPQs[k]]=k;return a},{}))