import {DealBrokerCachePQs} from './DealBrokerCachePQs'
export const DealBrokerCacheQPs=/**@type {!xyz.swapee.wc.DealBrokerCacheQPs}*/(Object.keys(DealBrokerCachePQs)
 .reduce((a,k)=>{a[DealBrokerCachePQs[k]]=k;return a},{}))