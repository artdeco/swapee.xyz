import {DealBrokerMemoryPQs} from './DealBrokerMemoryPQs'
export const DealBrokerInputsPQs=/**@type {!xyz.swapee.wc.DealBrokerInputsQPs}*/({
 ...DealBrokerMemoryPQs,
})