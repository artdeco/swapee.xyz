import {DealBrokerMemoryPQs} from './DealBrokerMemoryPQs'
export const DealBrokerMemoryQPs=/**@type {!xyz.swapee.wc.DealBrokerMemoryQPs}*/(Object.keys(DealBrokerMemoryPQs)
 .reduce((a,k)=>{a[DealBrokerMemoryPQs[k]]=k;return a},{}))