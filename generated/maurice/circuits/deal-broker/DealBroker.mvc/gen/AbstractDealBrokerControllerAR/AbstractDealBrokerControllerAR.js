import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerControllerAR}
 */
function __AbstractDealBrokerControllerAR() {}
__AbstractDealBrokerControllerAR.prototype = /** @type {!_AbstractDealBrokerControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerControllerAR}
 */
class _AbstractDealBrokerControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IDealBrokerControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerControllerAR} ‎
 */
class AbstractDealBrokerControllerAR extends newAbstract(
 _AbstractDealBrokerControllerAR,'IDealBrokerControllerAR',null,{
  asIDealBrokerControllerAR:1,
  superDealBrokerControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerControllerAR} */
AbstractDealBrokerControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerControllerAR} */
function AbstractDealBrokerControllerARClass(){}

export default AbstractDealBrokerControllerAR


AbstractDealBrokerControllerAR[$implementations]=[
 __AbstractDealBrokerControllerAR,
 AR,
 AbstractDealBrokerControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IDealBrokerControllerAR}*/({
  allocator(){
   this.methods={
    setGetOfferError:'642e3',
    unsetGetOfferError:'a602e',
    flipGettingOffer:'ae3ff',
    setGettingOffer:'4c6a6',
    unsetGettingOffer:'884f8',
    setNetworkFee:'ebcab',
    unsetNetworkFee:'691ce',
    setPartnerFee:'38c56',
    unsetPartnerFee:'09ede',
    setRate:'7b58a',
    unsetRate:'acaf8',
    setEstimatedFloatAmountTo:'922ab',
    unsetEstimatedFloatAmountTo:'be6e2',
    setEstimatedFixedAmountTo:'2738f',
    unsetEstimatedFixedAmountTo:'9854b',
    setVisibleAmount:'8f918',
    unsetVisibleAmount:'27889',
    pulseGetOffer:'e0519',
   }
  },
 }),
]