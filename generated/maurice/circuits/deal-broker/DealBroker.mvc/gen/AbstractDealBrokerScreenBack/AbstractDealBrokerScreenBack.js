import AbstractDealBrokerScreenAT from '../AbstractDealBrokerScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerScreenBack}
 */
function __AbstractDealBrokerScreenBack() {}
__AbstractDealBrokerScreenBack.prototype = /** @type {!_AbstractDealBrokerScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerScreen}
 */
class _AbstractDealBrokerScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerScreen} ‎
 */
class AbstractDealBrokerScreenBack extends newAbstract(
 _AbstractDealBrokerScreenBack,'IDealBrokerScreen',null,{
  asIDealBrokerScreen:1,
  superDealBrokerScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerScreen} */
AbstractDealBrokerScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerScreen} */
function AbstractDealBrokerScreenBackClass(){}

export default AbstractDealBrokerScreenBack


AbstractDealBrokerScreenBack[$implementations]=[
 __AbstractDealBrokerScreenBack,
 AbstractDealBrokerScreenAT,
]