import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerScreenAT}
 */
function __AbstractDealBrokerScreenAT() {}
__AbstractDealBrokerScreenAT.prototype = /** @type {!_AbstractDealBrokerScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerScreenAT}
 */
class _AbstractDealBrokerScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IDealBrokerScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerScreenAT} ‎
 */
class AbstractDealBrokerScreenAT extends newAbstract(
 _AbstractDealBrokerScreenAT,'IDealBrokerScreenAT',null,{
  asIDealBrokerScreenAT:1,
  superDealBrokerScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerScreenAT} */
AbstractDealBrokerScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerScreenAT} */
function AbstractDealBrokerScreenATClass(){}

export default AbstractDealBrokerScreenAT


AbstractDealBrokerScreenAT[$implementations]=[
 __AbstractDealBrokerScreenAT,
 UartUniversal,
]