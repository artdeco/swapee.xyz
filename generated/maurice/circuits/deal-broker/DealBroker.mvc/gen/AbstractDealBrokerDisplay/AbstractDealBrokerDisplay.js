import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerDisplay}
 */
function __AbstractDealBrokerDisplay() {}
__AbstractDealBrokerDisplay.prototype = /** @type {!_AbstractDealBrokerDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerDisplay}
 */
class _AbstractDealBrokerDisplay { }
/**
 * Display for presenting information from the _IDealBroker_.
 * @extends {xyz.swapee.wc.AbstractDealBrokerDisplay} ‎
 */
class AbstractDealBrokerDisplay extends newAbstract(
 _AbstractDealBrokerDisplay,'IDealBrokerDisplay',null,{
  asIDealBrokerDisplay:1,
  superDealBrokerDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerDisplay} */
AbstractDealBrokerDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerDisplay} */
function AbstractDealBrokerDisplayClass(){}

export default AbstractDealBrokerDisplay


AbstractDealBrokerDisplay[$implementations]=[
 __AbstractDealBrokerDisplay,
 Display,
]