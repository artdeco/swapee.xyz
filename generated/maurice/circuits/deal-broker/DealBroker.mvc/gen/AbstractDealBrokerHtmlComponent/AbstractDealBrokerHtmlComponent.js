import AbstractDealBrokerGPU from '../AbstractDealBrokerGPU'
import AbstractDealBrokerScreenBack from '../AbstractDealBrokerScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {DealBrokerInputsQPs} from '../../pqs/DealBrokerInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractDealBroker from '../AbstractDealBroker'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerHtmlComponent}
 */
function __AbstractDealBrokerHtmlComponent() {}
__AbstractDealBrokerHtmlComponent.prototype = /** @type {!_AbstractDealBrokerHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerHtmlComponent}
 */
class _AbstractDealBrokerHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.DealBrokerHtmlComponent} */ (res)
  }
}
/**
 * The _IDealBroker_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractDealBrokerHtmlComponent} ‎
 */
export class AbstractDealBrokerHtmlComponent extends newAbstract(
 _AbstractDealBrokerHtmlComponent,'IDealBrokerHtmlComponent',null,{
  asIDealBrokerHtmlComponent:1,
  superDealBrokerHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerHtmlComponent} */
AbstractDealBrokerHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerHtmlComponent} */
function AbstractDealBrokerHtmlComponentClass(){}


AbstractDealBrokerHtmlComponent[$implementations]=[
 __AbstractDealBrokerHtmlComponent,
 HtmlComponent,
 AbstractDealBroker,
 AbstractDealBrokerGPU,
 AbstractDealBrokerScreenBack,
 AbstractDealBrokerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerHtmlComponent}*/({
  inputsQPs:DealBrokerInputsQPs,
 }),
 AbstractDealBrokerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerHtmlComponent}*/({
  paint({getOffer:getOffer}){
   const{asIDealBrokerController:{
    resetPort:resetPort,
   }}=this
   if(getOffer) {
    resetPort()
   }
  },
 }),
]