
import AbstractDealBroker from '../AbstractDealBroker'

/** @abstract {xyz.swapee.wc.IDealBrokerElement} */
export default class AbstractDealBrokerElement { }



AbstractDealBrokerElement[$implementations]=[AbstractDealBroker,
 /** @type {!AbstractDealBrokerElement} */ ({
  rootId:'DealBroker',
  __$id:3427226314,
  fqn:'xyz.swapee.wc.IDealBroker',
  maurice_element_v3:true,
 }),
]