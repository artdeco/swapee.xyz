import DealBrokerElementPort from '../DealBrokerElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {DealBrokerInputsPQs} from '../../pqs/DealBrokerInputsPQs'
import {DealBrokerCachePQs} from '../../pqs/DealBrokerCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractDealBroker from '../AbstractDealBroker'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerElement}
 */
function __AbstractDealBrokerElement() {}
__AbstractDealBrokerElement.prototype = /** @type {!_AbstractDealBrokerElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerElement}
 */
class _AbstractDealBrokerElement { }
/**
 * A component description.
 *
 * The _IDealBroker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractDealBrokerElement} ‎
 */
class AbstractDealBrokerElement extends newAbstract(
 _AbstractDealBrokerElement,'IDealBrokerElement',null,{
  asIDealBrokerElement:1,
  superDealBrokerElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerElement} */
AbstractDealBrokerElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerElement} */
function AbstractDealBrokerElementClass(){}

export default AbstractDealBrokerElement


AbstractDealBrokerElement[$implementations]=[
 __AbstractDealBrokerElement,
 ElementBase,
 AbstractDealBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':get-offer':getOfferColAttr,
   ':get-offer-error':getOfferErrorColAttr,
   ':getting-offer':gettingOfferColAttr,
   ':fixed-id':fixedIdColAttr,
   ':network-fee':networkFeeColAttr,
   ':partner-fee':partnerFeeColAttr,
   ':rate':rateColAttr,
   ':estimated-float-amount-to':estimatedFloatAmountToColAttr,
   ':estimated-fixed-amount-to':estimatedFixedAmountToColAttr,
   ':visible-amount':visibleAmountColAttr,
   ':min-amount':minAmountColAttr,
   ':max-amount':maxAmountColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'get-offer':getOfferAttr,
    'get-offer-error':getOfferErrorAttr,
    'getting-offer':gettingOfferAttr,
    'fixed-id':fixedIdAttr,
    'network-fee':networkFeeAttr,
    'partner-fee':partnerFeeAttr,
    'rate':rateAttr,
    'estimated-float-amount-to':estimatedFloatAmountToAttr,
    'estimated-fixed-amount-to':estimatedFixedAmountToAttr,
    'visible-amount':visibleAmountAttr,
    'min-amount':minAmountAttr,
    'max-amount':maxAmountAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(getOfferAttr===undefined?{'get-offer':getOfferColAttr}:{}),
    ...(getOfferErrorAttr===undefined?{'get-offer-error':getOfferErrorColAttr}:{}),
    ...(gettingOfferAttr===undefined?{'getting-offer':gettingOfferColAttr}:{}),
    ...(fixedIdAttr===undefined?{'fixed-id':fixedIdColAttr}:{}),
    ...(networkFeeAttr===undefined?{'network-fee':networkFeeColAttr}:{}),
    ...(partnerFeeAttr===undefined?{'partner-fee':partnerFeeColAttr}:{}),
    ...(rateAttr===undefined?{'rate':rateColAttr}:{}),
    ...(estimatedFloatAmountToAttr===undefined?{'estimated-float-amount-to':estimatedFloatAmountToColAttr}:{}),
    ...(estimatedFixedAmountToAttr===undefined?{'estimated-fixed-amount-to':estimatedFixedAmountToColAttr}:{}),
    ...(visibleAmountAttr===undefined?{'visible-amount':visibleAmountColAttr}:{}),
    ...(minAmountAttr===undefined?{'min-amount':minAmountColAttr}:{}),
    ...(maxAmountAttr===undefined?{'max-amount':maxAmountColAttr}:{}),
   }
  },
 }),
 AbstractDealBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'get-offer':getOfferAttr,
   'get-offer-error':getOfferErrorAttr,
   'getting-offer':gettingOfferAttr,
   'fixed-id':fixedIdAttr,
   'network-fee':networkFeeAttr,
   'partner-fee':partnerFeeAttr,
   'rate':rateAttr,
   'estimated-float-amount-to':estimatedFloatAmountToAttr,
   'estimated-fixed-amount-to':estimatedFixedAmountToAttr,
   'visible-amount':visibleAmountAttr,
   'min-amount':minAmountAttr,
   'max-amount':maxAmountAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    getOffer:getOfferAttr,
    getOfferError:getOfferErrorAttr,
    gettingOffer:gettingOfferAttr,
    fixedId:fixedIdAttr,
    networkFee:networkFeeAttr,
    partnerFee:partnerFeeAttr,
    rate:rateAttr,
    estimatedFloatAmountTo:estimatedFloatAmountToAttr,
    estimatedFixedAmountTo:estimatedFixedAmountToAttr,
    visibleAmount:visibleAmountAttr,
    minAmount:minAmountAttr,
    maxAmount:maxAmountAttr,
   }
  },
 }),
 AbstractDealBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractDealBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerElement}*/({
  inputsPQs:DealBrokerInputsPQs,
  cachePQs:DealBrokerCachePQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractDealBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','getOffer','getOfferError','gettingOffer','fixedId','networkFee','partnerFee','rate','estimatedFloatAmountTo','estimatedFixedAmountTo','visibleAmount','minAmount','maxAmount','no-solder',':no-solder','get-offer',':get-offer','get-offer-error',':get-offer-error','getting-offer',':getting-offer','fixed-id',':fixed-id','network-fee',':network-fee','partner-fee',':partner-fee',':rate','estimated-float-amount-to',':estimated-float-amount-to','estimated-fixed-amount-to',':estimated-fixed-amount-to','visible-amount',':visible-amount','min-amount',':min-amount','max-amount',':max-amount','fe646','b370a','5f4a3','341da','2568a','5fd9c','96f44','67942','8ca05','ea0a0','4685c','ae0d3','d0b0c','children']),
   })
  },
  get Port(){
   return DealBrokerElementPort
  },
 }),
]



AbstractDealBrokerElement[$implementations]=[AbstractDealBroker,
 /** @type {!AbstractDealBrokerElement} */ ({
  rootId:'DealBroker',
  __$id:3427226314,
  fqn:'xyz.swapee.wc.IDealBroker',
  maurice_element_v3:true,
 }),
]