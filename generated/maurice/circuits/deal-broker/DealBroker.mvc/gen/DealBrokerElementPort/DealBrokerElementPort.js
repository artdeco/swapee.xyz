import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerElementPort}
 */
function __DealBrokerElementPort() {}
__DealBrokerElementPort.prototype = /** @type {!_DealBrokerElementPort} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerElementPort} */ function DealBrokerElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IDealBrokerElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerElementPort}
 */
class _DealBrokerElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractDealBrokerElementPort} ‎
 */
class DealBrokerElementPort extends newAbstract(
 _DealBrokerElementPort,'IDealBrokerElementPort',DealBrokerElementPortConstructor,{
  asIDealBrokerElementPort:1,
  superDealBrokerElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerElementPort} */
DealBrokerElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerElementPort} */
function DealBrokerElementPortClass(){}

export default DealBrokerElementPort


DealBrokerElementPort[$implementations]=[
 __DealBrokerElementPort,
 DealBrokerElementPortClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'get-offer':undefined,
    'get-offer-error':undefined,
    'getting-offer':undefined,
    'fixed-id':undefined,
    'network-fee':undefined,
    'partner-fee':undefined,
    'estimated-float-amount-to':undefined,
    'estimated-fixed-amount-to':undefined,
    'visible-amount':undefined,
    'min-amount':undefined,
    'max-amount':undefined,
   })
  },
 }),
]