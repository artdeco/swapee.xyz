import DealBrokerBuffer from '../DealBrokerBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {DealBrokerPortConnector} from '../DealBrokerPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerController}
 */
function __AbstractDealBrokerController() {}
__AbstractDealBrokerController.prototype = /** @type {!_AbstractDealBrokerController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerController}
 */
class _AbstractDealBrokerController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractDealBrokerController} ‎
 */
export class AbstractDealBrokerController extends newAbstract(
 _AbstractDealBrokerController,'IDealBrokerController',null,{
  asIDealBrokerController:1,
  superDealBrokerController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerController} */
AbstractDealBrokerController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerController} */
function AbstractDealBrokerControllerClass(){}


AbstractDealBrokerController[$implementations]=[
 AbstractDealBrokerControllerClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IDealBrokerPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractDealBrokerController,
 /**@type {!xyz.swapee.wc.IDealBrokerController}*/({
  calibrate: /**@this {!xyz.swapee.wc.IDealBrokerController}*/ function calibrateGetOffer({getOffer:getOffer}){
   if(!getOffer) return
   setTimeout(()=>{
    const{asIDealBrokerController:{setInputs:setInputs}}=this
    setInputs({
     getOffer:false,
    })
   },1)
  },
 }),
 DealBrokerBuffer,
 IntegratedController,
 /**@type {!AbstractDealBrokerController}*/(DealBrokerPortConnector),
 AbstractDealBrokerControllerClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerController}*/({
  setGetOfferError(val){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({getOfferError:val})
  },
  setGettingOffer(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({gettingOffer:true})
  },
  setNetworkFee(val){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({networkFee:val})
  },
  setPartnerFee(val){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({partnerFee:val})
  },
  setRate(val){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({rate:val})
  },
  setEstimatedFloatAmountTo(val){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({estimatedFloatAmountTo:val})
  },
  setEstimatedFixedAmountTo(val){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({estimatedFixedAmountTo:val})
  },
  setVisibleAmount(val){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({visibleAmount:val})
  },
  unsetGetOfferError(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({getOfferError:''})
  },
  unsetGettingOffer(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({gettingOffer:false})
  },
  unsetNetworkFee(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({networkFee:''})
  },
  unsetPartnerFee(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({partnerFee:''})
  },
  unsetRate(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({rate:''})
  },
  unsetEstimatedFloatAmountTo(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({estimatedFloatAmountTo:''})
  },
  unsetEstimatedFixedAmountTo(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({estimatedFixedAmountTo:''})
  },
  unsetVisibleAmount(){
   const{asIDealBrokerController:{setInputs:setInputs}}=this
   setInputs({visibleAmount:''})
  },
 }),
]


export default AbstractDealBrokerController