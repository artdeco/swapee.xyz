import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerProcessor}
 */
function __AbstractDealBrokerProcessor() {}
__AbstractDealBrokerProcessor.prototype = /** @type {!_AbstractDealBrokerProcessor} */ ({
  /** @return {void} */
  pulseGetOffer() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     getOffer:true,
    })
  },
})
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerProcessor}
 */
class _AbstractDealBrokerProcessor { }
/**
 * The processor to compute changes to the memory for the _IDealBroker_.
 * @extends {xyz.swapee.wc.AbstractDealBrokerProcessor} ‎
 */
class AbstractDealBrokerProcessor extends newAbstract(
 _AbstractDealBrokerProcessor,'IDealBrokerProcessor',null,{
  asIDealBrokerProcessor:1,
  superDealBrokerProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerProcessor} */
AbstractDealBrokerProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerProcessor} */
function AbstractDealBrokerProcessorClass(){}

export default AbstractDealBrokerProcessor


AbstractDealBrokerProcessor[$implementations]=[
 __AbstractDealBrokerProcessor,
 AbstractDealBrokerProcessorClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerProcessor}*/({
  flipGettingOffer(){
   const{
    asIDealBrokerComputer:{
     model:{gettingOffer:gettingOffer},
     setInfo:setInfo,
    },
   }=this
   setInfo({gettingOffer:!gettingOffer})
  },
 }),
]