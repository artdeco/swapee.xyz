import AbstractDealBrokerProcessor from '../AbstractDealBrokerProcessor'
import {DealBrokerCore} from '../DealBrokerCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractDealBrokerComputer} from '../AbstractDealBrokerComputer'
import {AbstractDealBrokerController} from '../AbstractDealBrokerController'
import {regulateDealBrokerCache} from './methods/regulateDealBrokerCache'
import {DealBrokerCacheQPs} from '../../pqs/DealBrokerCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBroker}
 */
function __AbstractDealBroker() {}
__AbstractDealBroker.prototype = /** @type {!_AbstractDealBroker} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBroker}
 */
class _AbstractDealBroker { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractDealBroker} ‎
 */
class AbstractDealBroker extends newAbstract(
 _AbstractDealBroker,'IDealBroker',null,{
  asIDealBroker:1,
  superDealBroker:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBroker} */
AbstractDealBroker.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBroker} */
function AbstractDealBrokerClass(){}

export default AbstractDealBroker


AbstractDealBroker[$implementations]=[
 __AbstractDealBroker,
 DealBrokerCore,
 AbstractDealBrokerProcessor,
 IntegratedComponent,
 AbstractDealBrokerComputer,
 AbstractDealBrokerController,
 AbstractDealBrokerClass.prototype=/**@type {!xyz.swapee.wc.IDealBroker}*/({
  regulateState:regulateDealBrokerCache,
  stateQPs:DealBrokerCacheQPs,
 }),
]


export {AbstractDealBroker}