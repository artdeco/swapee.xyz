import {makeBuffers} from '@webcircuits/webcircuits'

export const DealBrokerBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  getOffer:Boolean,
  getOfferError:String,
  gettingOffer:Boolean,
  fixedId:String,
  networkFee:String,
  partnerFee:String,
  rate:String,
  estimatedFloatAmountTo:String,
  estimatedFixedAmountTo:String,
  visibleAmount:String,
  minAmount:String,
  maxAmount:String,
 }),
})

export default DealBrokerBuffer