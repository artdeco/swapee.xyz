import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {DealBrokerInputsPQs} from '../../pqs/DealBrokerInputsPQs'
import {DealBrokerOuterCoreConstructor} from '../DealBrokerCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerPort}
 */
function __DealBrokerPort() {}
__DealBrokerPort.prototype = /** @type {!_DealBrokerPort} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerPort} */ function DealBrokerPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.DealBrokerOuterCore} */ ({model:null})
  DealBrokerOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerPort}
 */
class _DealBrokerPort { }
/**
 * The port that serves as an interface to the _IDealBroker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractDealBrokerPort} ‎
 */
export class DealBrokerPort extends newAbstract(
 _DealBrokerPort,'IDealBrokerPort',DealBrokerPortConstructor,{
  asIDealBrokerPort:1,
  superDealBrokerPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerPort} */
DealBrokerPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerPort} */
function DealBrokerPortClass(){}

export const DealBrokerPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IDealBroker.Pinout>}*/({
 get Port() { return DealBrokerPort },
})

DealBrokerPort[$implementations]=[
 DealBrokerPortClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerPort}*/({
  resetPort(){
   this.resetDealBrokerPort()
  },
  resetDealBrokerPort(){
   DealBrokerPortConstructor.call(this)
  },
 }),
 __DealBrokerPort,
 Parametric,
 DealBrokerPortClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerPort}*/({
  constructor(){
   mountPins(this.inputs,'',DealBrokerInputsPQs)
  },
 }),
]


export default DealBrokerPort