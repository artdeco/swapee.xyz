import {mountPins} from '@webcircuits/webcircuits'
import {DealBrokerMemoryPQs} from '../../pqs/DealBrokerMemoryPQs'
import {DealBrokerCachePQs} from '../../pqs/DealBrokerCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerCore}
 */
function __DealBrokerCore() {}
__DealBrokerCore.prototype = /** @type {!_DealBrokerCore} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerCore} */ function DealBrokerCoreConstructor() {
  /**@type {!xyz.swapee.wc.IDealBrokerCore.Model}*/
  this.model={
    estimatedAmountTo: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerCore}
 */
class _DealBrokerCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractDealBrokerCore} ‎
 */
class DealBrokerCore extends newAbstract(
 _DealBrokerCore,'IDealBrokerCore',DealBrokerCoreConstructor,{
  asIDealBrokerCore:1,
  superDealBrokerCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerCore} */
DealBrokerCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerCore} */
function DealBrokerCoreClass(){}

export default DealBrokerCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_DealBrokerOuterCore}
 */
function __DealBrokerOuterCore() {}
__DealBrokerOuterCore.prototype = /** @type {!_DealBrokerOuterCore} */ ({ })
/** @this {xyz.swapee.wc.DealBrokerOuterCore} */
export function DealBrokerOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IDealBrokerOuterCore.Model}*/
  this.model={
    getOffer: false,
    getOfferError: '',
    gettingOffer: false,
    fixedId: '',
    networkFee: '',
    partnerFee: '',
    rate: '',
    estimatedFloatAmountTo: '',
    estimatedFixedAmountTo: '',
    visibleAmount: '',
    minAmount: '',
    maxAmount: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerOuterCore}
 */
class _DealBrokerOuterCore { }
/**
 * The _IDealBroker_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractDealBrokerOuterCore} ‎
 */
export class DealBrokerOuterCore extends newAbstract(
 _DealBrokerOuterCore,'IDealBrokerOuterCore',DealBrokerOuterCoreConstructor,{
  asIDealBrokerOuterCore:1,
  superDealBrokerOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerOuterCore} */
DealBrokerOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerOuterCore} */
function DealBrokerOuterCoreClass(){}


DealBrokerOuterCore[$implementations]=[
 __DealBrokerOuterCore,
 DealBrokerOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerOuterCore}*/({
  constructor(){
   mountPins(this.model,'',DealBrokerMemoryPQs)
   mountPins(this.model,'',DealBrokerCachePQs)
  },
 }),
]

DealBrokerCore[$implementations]=[
 DealBrokerCoreClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerCore}*/({
  resetCore(){
   this.resetDealBrokerCore()
  },
  resetDealBrokerCore(){
   DealBrokerCoreConstructor.call(
    /**@type {xyz.swapee.wc.DealBrokerCore}*/(this),
   )
   DealBrokerOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.DealBrokerOuterCore}*/(
     /**@type {!xyz.swapee.wc.IDealBrokerOuterCore}*/(this)),
   )
  },
 }),
 __DealBrokerCore,
 DealBrokerOuterCore,
]

export {DealBrokerCore}