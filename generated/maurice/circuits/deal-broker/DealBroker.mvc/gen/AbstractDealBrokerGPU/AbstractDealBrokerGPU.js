import AbstractDealBrokerDisplay from '../AbstractDealBrokerDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {DealBrokerVdusPQs} from '../../pqs/DealBrokerVdusPQs'
import {DealBrokerVdusQPs} from '../../pqs/DealBrokerVdusQPs'
import {DealBrokerMemoryPQs} from '../../pqs/DealBrokerMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerGPU}
 */
function __AbstractDealBrokerGPU() {}
__AbstractDealBrokerGPU.prototype = /** @type {!_AbstractDealBrokerGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerGPU}
 */
class _AbstractDealBrokerGPU { }
/**
 * Handles the periphery of the _IDealBrokerDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractDealBrokerGPU} ‎
 */
class AbstractDealBrokerGPU extends newAbstract(
 _AbstractDealBrokerGPU,'IDealBrokerGPU',null,{
  asIDealBrokerGPU:1,
  superDealBrokerGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerGPU} */
AbstractDealBrokerGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerGPU} */
function AbstractDealBrokerGPUClass(){}

export default AbstractDealBrokerGPU


AbstractDealBrokerGPU[$implementations]=[
 __AbstractDealBrokerGPU,
 AbstractDealBrokerGPUClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerGPU}*/({
  vdusPQs:DealBrokerVdusPQs,
  vdusQPs:DealBrokerVdusQPs,
  memoryPQs:DealBrokerMemoryPQs,
 }),
 AbstractDealBrokerDisplay,
 BrowserView,
]