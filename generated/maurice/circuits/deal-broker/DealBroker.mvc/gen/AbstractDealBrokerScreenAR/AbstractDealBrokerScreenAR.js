import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerScreenAR}
 */
function __AbstractDealBrokerScreenAR() {}
__AbstractDealBrokerScreenAR.prototype = /** @type {!_AbstractDealBrokerScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerScreenAR}
 */
class _AbstractDealBrokerScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IDealBrokerScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerScreenAR} ‎
 */
class AbstractDealBrokerScreenAR extends newAbstract(
 _AbstractDealBrokerScreenAR,'IDealBrokerScreenAR',null,{
  asIDealBrokerScreenAR:1,
  superDealBrokerScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerScreenAR} */
AbstractDealBrokerScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerScreenAR} */
function AbstractDealBrokerScreenARClass(){}

export default AbstractDealBrokerScreenAR


AbstractDealBrokerScreenAR[$implementations]=[
 __AbstractDealBrokerScreenAR,
 AR,
 AbstractDealBrokerScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IDealBrokerScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractDealBrokerScreenAR}