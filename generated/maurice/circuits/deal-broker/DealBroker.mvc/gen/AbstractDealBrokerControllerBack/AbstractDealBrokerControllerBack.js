import AbstractDealBrokerControllerAR from '../AbstractDealBrokerControllerAR'
import {AbstractDealBrokerController} from '../AbstractDealBrokerController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerControllerBack}
 */
function __AbstractDealBrokerControllerBack() {}
__AbstractDealBrokerControllerBack.prototype = /** @type {!_AbstractDealBrokerControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerController}
 */
class _AbstractDealBrokerControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerController} ‎
 */
class AbstractDealBrokerControllerBack extends newAbstract(
 _AbstractDealBrokerControllerBack,'IDealBrokerController',null,{
  asIDealBrokerController:1,
  superDealBrokerController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerController} */
AbstractDealBrokerControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerController} */
function AbstractDealBrokerControllerBackClass(){}

export default AbstractDealBrokerControllerBack


AbstractDealBrokerControllerBack[$implementations]=[
 __AbstractDealBrokerControllerBack,
 AbstractDealBrokerController,
 AbstractDealBrokerControllerAR,
 DriverBack,
]