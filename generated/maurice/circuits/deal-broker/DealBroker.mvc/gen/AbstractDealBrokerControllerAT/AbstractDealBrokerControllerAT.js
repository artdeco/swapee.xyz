import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerControllerAT}
 */
function __AbstractDealBrokerControllerAT() {}
__AbstractDealBrokerControllerAT.prototype = /** @type {!_AbstractDealBrokerControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerControllerAT}
 */
class _AbstractDealBrokerControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IDealBrokerControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractDealBrokerControllerAT} ‎
 */
class AbstractDealBrokerControllerAT extends newAbstract(
 _AbstractDealBrokerControllerAT,'IDealBrokerControllerAT',null,{
  asIDealBrokerControllerAT:1,
  superDealBrokerControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerControllerAT} */
AbstractDealBrokerControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractDealBrokerControllerAT} */
function AbstractDealBrokerControllerATClass(){}

export default AbstractDealBrokerControllerAT


AbstractDealBrokerControllerAT[$implementations]=[
 __AbstractDealBrokerControllerAT,
 UartUniversal,
 AbstractDealBrokerControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IDealBrokerControllerAT}*/({
  get asIDealBrokerController(){
   return this
  },
  setGetOfferError(){
   this.uart.t("inv",{mid:'642e3',args:[...arguments]})
  },
  unsetGetOfferError(){
   this.uart.t("inv",{mid:'a602e'})
  },
  flipGettingOffer(){
   this.uart.t("inv",{mid:'ae3ff'})
  },
  setGettingOffer(){
   this.uart.t("inv",{mid:'4c6a6'})
  },
  unsetGettingOffer(){
   this.uart.t("inv",{mid:'884f8'})
  },
  setNetworkFee(){
   this.uart.t("inv",{mid:'ebcab',args:[...arguments]})
  },
  unsetNetworkFee(){
   this.uart.t("inv",{mid:'691ce'})
  },
  setPartnerFee(){
   this.uart.t("inv",{mid:'38c56',args:[...arguments]})
  },
  unsetPartnerFee(){
   this.uart.t("inv",{mid:'09ede'})
  },
  setRate(){
   this.uart.t("inv",{mid:'7b58a',args:[...arguments]})
  },
  unsetRate(){
   this.uart.t("inv",{mid:'acaf8'})
  },
  setEstimatedFloatAmountTo(){
   this.uart.t("inv",{mid:'922ab',args:[...arguments]})
  },
  unsetEstimatedFloatAmountTo(){
   this.uart.t("inv",{mid:'be6e2'})
  },
  setEstimatedFixedAmountTo(){
   this.uart.t("inv",{mid:'2738f',args:[...arguments]})
  },
  unsetEstimatedFixedAmountTo(){
   this.uart.t("inv",{mid:'9854b'})
  },
  setVisibleAmount(){
   this.uart.t("inv",{mid:'8f918',args:[...arguments]})
  },
  unsetVisibleAmount(){
   this.uart.t("inv",{mid:'27889'})
  },
  pulseGetOffer(){
   this.uart.t("inv",{mid:'e0519'})
  },
 }),
]