import AbstractDealBrokerScreenAR from '../AbstractDealBrokerScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {DealBrokerInputsPQs} from '../../pqs/DealBrokerInputsPQs'
import {DealBrokerMemoryQPs} from '../../pqs/DealBrokerMemoryQPs'
import {DealBrokerCacheQPs} from '../../pqs/DealBrokerCacheQPs'
import {DealBrokerVdusPQs} from '../../pqs/DealBrokerVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerScreen}
 */
function __AbstractDealBrokerScreen() {}
__AbstractDealBrokerScreen.prototype = /** @type {!_AbstractDealBrokerScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerScreen}
 */
class _AbstractDealBrokerScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractDealBrokerScreen} ‎
 */
class AbstractDealBrokerScreen extends newAbstract(
 _AbstractDealBrokerScreen,'IDealBrokerScreen',null,{
  asIDealBrokerScreen:1,
  superDealBrokerScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerScreen} */
AbstractDealBrokerScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerScreen} */
function AbstractDealBrokerScreenClass(){}

export default AbstractDealBrokerScreen


AbstractDealBrokerScreen[$implementations]=[
 __AbstractDealBrokerScreen,
 AbstractDealBrokerScreenClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerScreen}*/({
  inputsPQs:DealBrokerInputsPQs,
  memoryQPs:DealBrokerMemoryQPs,
  cacheQPs:DealBrokerCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractDealBrokerScreenAR,
 AbstractDealBrokerScreenClass.prototype=/**@type {!xyz.swapee.wc.IDealBrokerScreen}*/({
  vdusPQs:DealBrokerVdusPQs,
 }),
]