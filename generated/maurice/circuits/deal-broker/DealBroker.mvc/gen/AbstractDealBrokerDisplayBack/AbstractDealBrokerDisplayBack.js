import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerDisplay}
 */
function __AbstractDealBrokerDisplay() {}
__AbstractDealBrokerDisplay.prototype = /** @type {!_AbstractDealBrokerDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerDisplay}
 */
class _AbstractDealBrokerDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractDealBrokerDisplay} ‎
 */
class AbstractDealBrokerDisplay extends newAbstract(
 _AbstractDealBrokerDisplay,'IDealBrokerDisplay',null,{
  asIDealBrokerDisplay:1,
  superDealBrokerDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerDisplay} */
AbstractDealBrokerDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractDealBrokerDisplay} */
function AbstractDealBrokerDisplayClass(){}

export default AbstractDealBrokerDisplay


AbstractDealBrokerDisplay[$implementations]=[
 __AbstractDealBrokerDisplay,
 GraphicsDriverBack,
]