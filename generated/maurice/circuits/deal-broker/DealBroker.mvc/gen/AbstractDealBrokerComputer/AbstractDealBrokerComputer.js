import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractDealBrokerComputer}
 */
function __AbstractDealBrokerComputer() {}
__AbstractDealBrokerComputer.prototype = /** @type {!_AbstractDealBrokerComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractDealBrokerComputer}
 */
class _AbstractDealBrokerComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractDealBrokerComputer} ‎
 */
export class AbstractDealBrokerComputer extends newAbstract(
 _AbstractDealBrokerComputer,'IDealBrokerComputer',null,{
  asIDealBrokerComputer:1,
  superDealBrokerComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractDealBrokerComputer} */
AbstractDealBrokerComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractDealBrokerComputer} */
function AbstractDealBrokerComputerClass(){}


AbstractDealBrokerComputer[$implementations]=[
 __AbstractDealBrokerComputer,
 Adapter,
]


export default AbstractDealBrokerComputer