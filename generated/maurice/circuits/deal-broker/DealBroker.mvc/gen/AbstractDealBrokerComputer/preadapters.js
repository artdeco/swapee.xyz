
/**@this {xyz.swapee.wc.IDealBrokerComputer}*/
export function preadaptEstimatedAmountTo(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IDealBrokerComputer.adaptEstimatedAmountTo.Form}*/
 const _inputs={
  estimatedFloatAmountTo:inputs.estimatedFloatAmountTo,
  estimatedFixedAmountTo:inputs.estimatedFixedAmountTo,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptEstimatedAmountTo(__inputs,__changes)
 return RET
}