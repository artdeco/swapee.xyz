import deduceInputs from './methods/deduce-inputs'
import AbstractDealBrokerControllerAT from '../../gen/AbstractDealBrokerControllerAT'
import DealBrokerDisplay from '../DealBrokerDisplay'
import AbstractDealBrokerScreen from '../../gen/AbstractDealBrokerScreen'

/** @extends {xyz.swapee.wc.DealBrokerScreen} */
export default class extends AbstractDealBrokerScreen.implements(
 AbstractDealBrokerControllerAT,
 DealBrokerDisplay,
 /**@type {!xyz.swapee.wc.IDealBrokerScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IDealBrokerScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:3427226314,
 }),
){}