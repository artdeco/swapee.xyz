import DealBrokerHtmlController from '../DealBrokerHtmlController'
import DealBrokerHtmlComputer from '../DealBrokerHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractDealBrokerHtmlComponent} from '../../gen/AbstractDealBrokerHtmlComponent'

/** @extends {xyz.swapee.wc.DealBrokerHtmlComponent} */
export default class extends AbstractDealBrokerHtmlComponent.implements(
 DealBrokerHtmlController,
 DealBrokerHtmlComputer,
 IntegratedComponentInitialiser,
){}