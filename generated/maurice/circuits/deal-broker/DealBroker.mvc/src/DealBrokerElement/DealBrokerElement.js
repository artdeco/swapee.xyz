import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import DealBrokerServerController from '../DealBrokerServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractDealBrokerElement from '../../gen/AbstractDealBrokerElement'

/** @extends {xyz.swapee.wc.DealBrokerElement} */
export default class DealBrokerElement extends AbstractDealBrokerElement.implements(
 DealBrokerServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IDealBrokerElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IDealBrokerElement}*/({
   classesMap: true,
   rootSelector:     `.DealBroker`,
   stylesheet:       'html/styles/DealBroker.css',
   blockName:        'html/DealBrokerBlock.html',
  }),
  /**@type {xyz.swapee.wc.IDealBrokerDesigner}*/({
  }),
){}

// thank you for using web circuits
