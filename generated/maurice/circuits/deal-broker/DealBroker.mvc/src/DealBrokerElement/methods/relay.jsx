/** @type {xyz.swapee.wc.IDealBrokerElement._relay} */
export default function relay({This:This,DealBroker:_DealBroker}) {
 return (<>
  <This onGetOfferHigh={[
   _DealBroker.resetPort(), // todo: port pulling
  ]} />
 </>)
}