/** @type {xyz.swapee.wc.IDealBrokerComputer._adaptEstimatedAmountTo} */
export default function adaptEstimatedAmountTo({
 estimatedFixedAmountTo:estimatedFixedAmountTo,
 estimatedFloatAmountTo:estimatedFloatAmountTo,
}) {
 return{
  estimatedAmountTo:estimatedFixedAmountTo||estimatedFloatAmountTo,
 }
}