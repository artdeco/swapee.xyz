import adaptEstimatedAmountTo from './methods/adapt-estimated-amount-to'
import {preadaptEstimatedAmountTo} from '../../gen/AbstractDealBrokerComputer/preadapters'
import AbstractDealBrokerComputer from '../../gen/AbstractDealBrokerComputer'

/** @extends {xyz.swapee.wc.DealBrokerComputer} */
export default class DealBrokerHtmlComputer extends AbstractDealBrokerComputer.implements(
 /** @type {!xyz.swapee.wc.IDealBrokerComputer} */ ({
  adaptEstimatedAmountTo:adaptEstimatedAmountTo,
  adapt:[preadaptEstimatedAmountTo],
 }),
){}