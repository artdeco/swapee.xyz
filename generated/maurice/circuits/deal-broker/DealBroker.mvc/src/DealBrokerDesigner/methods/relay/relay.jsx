
/**@type {xyz.swapee.wc.IDealBrokerDesigner._relay} */
export default function relay({This:This,DealBroker:_DealBroker}) {
 return (h(Fragment,{},
  h(This,{ onGetOfferHigh:[
   _DealBroker.resetPort(), // todo: port pulling
  ] })
 ))
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZGVhbC1icm9rZXIvZGVhbC1icm9rZXIud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQXdJRyxTQUFTLEtBQUssRUFBRSxTQUFTLENBQUMsc0JBQXNCLENBQUM7Q0FDaEQsT0FBTyxDQUFDLFlBQUM7RUFDUixTQUFNLGVBQWdCO0dBQ3JCLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEdBQUcsTUFBTSxLQUFLO0VBQ3hDLENBQUU7Q0FDSCxDQUFrQztBQUNuQyxDQUFGIn0=