import AbstractHyperDealBrokerInterruptLine from '../../../../gen/AbstractDealBrokerInterruptLine/hyper/AbstractHyperDealBrokerInterruptLine'
import DealBrokerInterruptLine from '../../DealBrokerInterruptLine'
import DealBrokerInterruptLineGeneralAspects from '../DealBrokerInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperDealBrokerInterruptLine} */
export default class extends AbstractHyperDealBrokerInterruptLine
 .consults(
  DealBrokerInterruptLineGeneralAspects,
 )
 .implements(
  DealBrokerInterruptLine,
 )
{}