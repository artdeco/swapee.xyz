/** @extends {xyz.swapee.wc.AbstractDealBroker} */
export default class AbstractDealBroker extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractDealBrokerComputer} */
export class AbstractDealBrokerComputer extends (<computer>
   <adapter name="adaptEstimatedAmountTo">
    <xyz.swapee.wc.IDealBrokerCore estimatedFloatAmountTo estimatedFixedAmountTo />
    <outputs>
     <xyz.swapee.wc.IDealBrokerCore estimatedAmountTo />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractDealBrokerController} */
export class AbstractDealBrokerController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractDealBrokerPort} */
export class DealBrokerPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractDealBrokerView} */
export class AbstractDealBrokerView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractDealBrokerElement} */
export class AbstractDealBrokerElement extends (<element v3 html mv>
 <block src="./DealBroker.mvc/src/DealBrokerElement/methods/render.jsx" />
 <inducer src="./DealBroker.mvc/src/DealBrokerElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractDealBrokerHtmlComponent} */
export class AbstractDealBrokerHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>