import AbstractHyperTransactionInfoInterruptLine from '../../../../gen/AbstractTransactionInfoInterruptLine/hyper/AbstractHyperTransactionInfoInterruptLine'
import TransactionInfoInterruptLine from '../../TransactionInfoInterruptLine'
import TransactionInfoInterruptLineGeneralAspects from '../TransactionInfoInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperTransactionInfoInterruptLine} */
export default class extends AbstractHyperTransactionInfoInterruptLine
 .consults(
  TransactionInfoInterruptLineGeneralAspects,
 )
 .implements(
  TransactionInfoInterruptLine,
 )
{}