import AbstractTransactionInfoControllerAT from '../../gen/AbstractTransactionInfoControllerAT'
import TransactionInfoDisplay from '../TransactionInfoDisplay'
import AbstractTransactionInfoScreen from '../../gen/AbstractTransactionInfoScreen'

/** @extends {xyz.swapee.wc.TransactionInfoScreen} */
export default class extends AbstractTransactionInfoScreen.implements(
 AbstractTransactionInfoControllerAT,
 TransactionInfoDisplay,
 /**@type {!xyz.swapee.wc.ITransactionInfoScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ITransactionInfoScreen} */ ({
  __$id:1988051819,
 }),
/**@type {!xyz.swapee.wc.ITransactionInfoScreen}*/({
   // deduceInputs(el) {},
  }),
){}