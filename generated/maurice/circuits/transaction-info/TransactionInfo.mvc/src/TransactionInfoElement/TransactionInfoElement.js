import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import TransactionInfoServerController from '../TransactionInfoServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractTransactionInfoElement from '../../gen/AbstractTransactionInfoElement'

/** @extends {xyz.swapee.wc.TransactionInfoElement} */
export default class TransactionInfoElement extends AbstractTransactionInfoElement.implements(
 TransactionInfoServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ITransactionInfoElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ITransactionInfoElement}*/({
   classesMap: true,
   rootSelector:     `.TransactionInfo`,
   stylesheet:       'html/styles/TransactionInfo.css',
   blockName:        'html/TransactionInfoBlock.html',
  }),
){}

// thank you for using web circuits
