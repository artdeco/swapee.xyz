import TransactionInfoHtmlController from '../TransactionInfoHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractTransactionInfoHtmlComponent} from '../../gen/AbstractTransactionInfoHtmlComponent'

/** @extends {xyz.swapee.wc.TransactionInfoHtmlComponent} */
export default class extends AbstractTransactionInfoHtmlComponent.implements(
 TransactionInfoHtmlController,
 IntegratedComponentInitialiser,
){}