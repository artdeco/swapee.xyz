import {TransactionInfoInputsPQs} from './TransactionInfoInputsPQs'
export const TransactionInfoInputsQPs=/**@type {!xyz.swapee.wc.TransactionInfoInputsQPs}*/(Object.keys(TransactionInfoInputsPQs)
 .reduce((a,k)=>{a[TransactionInfoInputsPQs[k]]=k;return a},{}))