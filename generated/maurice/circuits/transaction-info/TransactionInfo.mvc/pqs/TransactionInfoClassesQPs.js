import TransactionInfoClassesPQs from './TransactionInfoClassesPQs'
export const TransactionInfoClassesQPs=/**@type {!xyz.swapee.wc.TransactionInfoClassesQPs}*/(Object.keys(TransactionInfoClassesPQs)
 .reduce((a,k)=>{a[TransactionInfoClassesPQs[k]]=k;return a},{}))