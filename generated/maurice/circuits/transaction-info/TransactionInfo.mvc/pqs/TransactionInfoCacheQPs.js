import {TransactionInfoCachePQs} from './TransactionInfoCachePQs'
export const TransactionInfoCacheQPs=/**@type {!xyz.swapee.wc.TransactionInfoCacheQPs}*/(Object.keys(TransactionInfoCachePQs)
 .reduce((a,k)=>{a[TransactionInfoCachePQs[k]]=k;return a},{}))