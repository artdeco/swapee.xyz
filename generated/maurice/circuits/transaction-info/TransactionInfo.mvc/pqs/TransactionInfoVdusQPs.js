import {TransactionInfoVdusPQs} from './TransactionInfoVdusPQs'
export const TransactionInfoVdusQPs=/**@type {!xyz.swapee.wc.TransactionInfoVdusQPs}*/(Object.keys(TransactionInfoVdusPQs)
 .reduce((a,k)=>{a[TransactionInfoVdusPQs[k]]=k;return a},{}))