import {TransactionInfoMemoryPQs} from './TransactionInfoMemoryPQs'
export const TransactionInfoInputsPQs=/**@type {!xyz.swapee.wc.TransactionInfoInputsQPs}*/({
 ...TransactionInfoMemoryPQs,
})