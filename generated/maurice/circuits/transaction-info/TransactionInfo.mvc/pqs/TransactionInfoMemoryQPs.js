import {TransactionInfoMemoryPQs} from './TransactionInfoMemoryPQs'
export const TransactionInfoMemoryQPs=/**@type {!xyz.swapee.wc.TransactionInfoMemoryQPs}*/(Object.keys(TransactionInfoMemoryPQs)
 .reduce((a,k)=>{a[TransactionInfoMemoryPQs[k]]=k;return a},{}))