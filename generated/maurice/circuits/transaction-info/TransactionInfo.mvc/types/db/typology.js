/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ITransactionInfoComputer': {
  'id': 19880518191,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoMemoryPQs': {
  'id': 19880518192,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoOuterCore': {
  'id': 19880518193,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TransactionInfoInputsPQs': {
  'id': 19880518194,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoPort': {
  'id': 19880518195,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTransactionInfoPort': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoPortInterface': {
  'id': 19880518196,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoCore': {
  'id': 19880518197,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTransactionInfoCore': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoProcessor': {
  'id': 19880518198,
  'symbols': {},
  'methods': {
   'pulseGetTransaction': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfo': {
  'id': 19880518199,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoBuffer': {
  'id': 198805181910,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHtmlComponent': {
  'id': 198805181911,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoElement': {
  'id': 198805181912,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ITransactionInfoElementPort': {
  'id': 198805181913,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoDesigner': {
  'id': 198805181914,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITransactionInfoGPU': {
  'id': 198805181915,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoDisplay': {
  'id': 198805181916,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoVdusPQs': {
  'id': 198805181917,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITransactionInfoDisplay': {
  'id': 198805181918,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoClassesPQs': {
  'id': 198805181919,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoController': {
  'id': 198805181920,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'pulseGetTransaction': 2
  }
 },
 'xyz.swapee.wc.front.ITransactionInfoController': {
  'id': 198805181921,
  'symbols': {},
  'methods': {
   'pulseGetTransaction': 1
  }
 },
 'xyz.swapee.wc.back.ITransactionInfoController': {
  'id': 198805181922,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoControllerAR': {
  'id': 198805181923,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoControllerAT': {
  'id': 198805181924,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoScreen': {
  'id': 198805181925,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoScreen': {
  'id': 198805181926,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoScreenAR': {
  'id': 198805181927,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoScreenAT': {
  'id': 198805181928,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TransactionInfoCachePQs': {
  'id': 198805181929,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})