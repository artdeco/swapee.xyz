/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ITransactionInfoComputer={}
xyz.swapee.wc.ITransactionInfoOuterCore={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.Tid={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransaction={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyFrom={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyTo={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountFrom={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountTo={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.NetworkFee={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.PartnerFee={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.Rate={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.VisibleAmount={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.NotFound={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.Fixed={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.GettingTransaction={}
xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransactionError={}
xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel={}
xyz.swapee.wc.ITransactionInfoPort={}
xyz.swapee.wc.ITransactionInfoPort.Inputs={}
xyz.swapee.wc.ITransactionInfoPort.WeakInputs={}
xyz.swapee.wc.ITransactionInfoCore={}
xyz.swapee.wc.ITransactionInfoCore.Model={}
xyz.swapee.wc.ITransactionInfoCore.Model.IconsFolder={}
xyz.swapee.wc.ITransactionInfoPortInterface={}
xyz.swapee.wc.ITransactionInfoProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ITransactionInfoController={}
xyz.swapee.wc.front.ITransactionInfoControllerAT={}
xyz.swapee.wc.front.ITransactionInfoScreenAR={}
xyz.swapee.wc.ITransactionInfo={}
xyz.swapee.wc.ITransactionInfoHtmlComponent={}
xyz.swapee.wc.ITransactionInfoElement={}
xyz.swapee.wc.ITransactionInfoElementPort={}
xyz.swapee.wc.ITransactionInfoElementPort.Inputs={}
xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs={}
xyz.swapee.wc.ITransactionInfoDesigner={}
xyz.swapee.wc.ITransactionInfoDesigner.communicator={}
xyz.swapee.wc.ITransactionInfoDesigner.relay={}
xyz.swapee.wc.ITransactionInfoDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ITransactionInfoDisplay={}
xyz.swapee.wc.back.ITransactionInfoController={}
xyz.swapee.wc.back.ITransactionInfoControllerAR={}
xyz.swapee.wc.back.ITransactionInfoScreen={}
xyz.swapee.wc.back.ITransactionInfoScreenAT={}
xyz.swapee.wc.ITransactionInfoController={}
xyz.swapee.wc.ITransactionInfoScreen={}
xyz.swapee.wc.ITransactionInfoGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/02-ITransactionInfoComputer.xml}  ddb7b2dbedf9b79b34c5e27297c3a5f7 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ITransactionInfoComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoComputer)} xyz.swapee.wc.AbstractTransactionInfoComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoComputer} xyz.swapee.wc.TransactionInfoComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoComputer` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoComputer
 */
xyz.swapee.wc.AbstractTransactionInfoComputer = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoComputer.constructor&xyz.swapee.wc.TransactionInfoComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoComputer.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoComputer.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoComputer}
 */
xyz.swapee.wc.AbstractTransactionInfoComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoComputer.Initialese[]) => xyz.swapee.wc.ITransactionInfoComputer} xyz.swapee.wc.TransactionInfoComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.TransactionInfoMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.ITransactionInfoComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ITransactionInfoComputer
 */
xyz.swapee.wc.ITransactionInfoComputer = class extends /** @type {xyz.swapee.wc.ITransactionInfoComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoComputer.compute} */
xyz.swapee.wc.ITransactionInfoComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoComputer.Initialese>)} xyz.swapee.wc.TransactionInfoComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoComputer} xyz.swapee.wc.ITransactionInfoComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITransactionInfoComputer_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoComputer
 * @implements {xyz.swapee.wc.ITransactionInfoComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoComputer.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoComputer = class extends /** @type {xyz.swapee.wc.TransactionInfoComputer.constructor&xyz.swapee.wc.ITransactionInfoComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoComputer}
 */
xyz.swapee.wc.TransactionInfoComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoComputer} */
xyz.swapee.wc.RecordITransactionInfoComputer

/** @typedef {xyz.swapee.wc.ITransactionInfoComputer} xyz.swapee.wc.BoundITransactionInfoComputer */

/** @typedef {xyz.swapee.wc.TransactionInfoComputer} xyz.swapee.wc.BoundTransactionInfoComputer */

/**
 * Contains getters to cast the _ITransactionInfoComputer_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoComputerCaster
 */
xyz.swapee.wc.ITransactionInfoComputerCaster = class { }
/**
 * Cast the _ITransactionInfoComputer_ instance into the _BoundITransactionInfoComputer_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoComputer}
 */
xyz.swapee.wc.ITransactionInfoComputerCaster.prototype.asITransactionInfoComputer
/**
 * Access the _TransactionInfoComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoComputer}
 */
xyz.swapee.wc.ITransactionInfoComputerCaster.prototype.superTransactionInfoComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.TransactionInfoMemory) => void} xyz.swapee.wc.ITransactionInfoComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoComputer.__compute<!xyz.swapee.wc.ITransactionInfoComputer>} xyz.swapee.wc.ITransactionInfoComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.TransactionInfoMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/03-ITransactionInfoOuterCore.xml}  8652321af2a86d79018214da6da1443f */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITransactionInfoOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoOuterCore)} xyz.swapee.wc.AbstractTransactionInfoOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoOuterCore} xyz.swapee.wc.TransactionInfoOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoOuterCore
 */
xyz.swapee.wc.AbstractTransactionInfoOuterCore = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoOuterCore.constructor&xyz.swapee.wc.TransactionInfoOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoOuterCore.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ITransactionInfoOuterCore|typeof xyz.swapee.wc.TransactionInfoOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ITransactionInfoOuterCore|typeof xyz.swapee.wc.TransactionInfoOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ITransactionInfoOuterCore|typeof xyz.swapee.wc.TransactionInfoOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoOuterCore}
 */
xyz.swapee.wc.AbstractTransactionInfoOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoOuterCoreCaster)} xyz.swapee.wc.ITransactionInfoOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _ITransactionInfo_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ITransactionInfoOuterCore
 */
xyz.swapee.wc.ITransactionInfoOuterCore = class extends /** @type {xyz.swapee.wc.ITransactionInfoOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoOuterCore.prototype.constructor = xyz.swapee.wc.ITransactionInfoOuterCore

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoOuterCore.Initialese>)} xyz.swapee.wc.TransactionInfoOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoOuterCore} xyz.swapee.wc.ITransactionInfoOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITransactionInfoOuterCore_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoOuterCore
 * @implements {xyz.swapee.wc.ITransactionInfoOuterCore} The _ITransactionInfo_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoOuterCore = class extends /** @type {xyz.swapee.wc.TransactionInfoOuterCore.constructor&xyz.swapee.wc.ITransactionInfoOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoOuterCore.prototype.constructor = xyz.swapee.wc.TransactionInfoOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoOuterCore}
 */
xyz.swapee.wc.TransactionInfoOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoOuterCore.
 * @interface xyz.swapee.wc.ITransactionInfoOuterCoreFields
 */
xyz.swapee.wc.ITransactionInfoOuterCoreFields = class { }
/**
 * The _ITransactionInfo_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ITransactionInfoOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITransactionInfoOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore} */
xyz.swapee.wc.RecordITransactionInfoOuterCore

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore} xyz.swapee.wc.BoundITransactionInfoOuterCore */

/** @typedef {xyz.swapee.wc.TransactionInfoOuterCore} xyz.swapee.wc.BoundTransactionInfoOuterCore */

/**
 * The transaction ID.
 * @typedef {string}
 */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.Tid.tid

/**
 * Forces to load transaction.
 * @typedef {boolean}
 */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransaction.getTransaction

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyFrom.currencyFrom

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyTo.currencyTo

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountFrom.amountFrom

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountTo.amountTo

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.NetworkFee.networkFee

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.PartnerFee.partnerFee

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.Rate.rate

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.VisibleAmount.visibleAmount

/** @typedef {boolean} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.NotFound.notFound

/** @typedef {boolean} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.Fixed.fixed

/** @typedef {boolean} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.GettingTransaction.gettingTransaction

/** @typedef {string} */
xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransactionError.getTransactionError

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.Tid&xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransaction&xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyFrom&xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyTo&xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountFrom&xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountTo&xyz.swapee.wc.ITransactionInfoOuterCore.Model.NetworkFee&xyz.swapee.wc.ITransactionInfoOuterCore.Model.PartnerFee&xyz.swapee.wc.ITransactionInfoOuterCore.Model.Rate&xyz.swapee.wc.ITransactionInfoOuterCore.Model.VisibleAmount&xyz.swapee.wc.ITransactionInfoOuterCore.Model.NotFound&xyz.swapee.wc.ITransactionInfoOuterCore.Model.Fixed&xyz.swapee.wc.ITransactionInfoOuterCore.Model.GettingTransaction&xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransactionError} xyz.swapee.wc.ITransactionInfoOuterCore.Model The _ITransactionInfo_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Tid&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransaction&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyFrom&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyTo&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountFrom&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountTo&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NetworkFee&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.PartnerFee&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Rate&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.VisibleAmount&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NotFound&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Fixed&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GettingTransaction&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransactionError} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel The _ITransactionInfo_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ITransactionInfoOuterCore_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoOuterCoreCaster
 */
xyz.swapee.wc.ITransactionInfoOuterCoreCaster = class { }
/**
 * Cast the _ITransactionInfoOuterCore_ instance into the _BoundITransactionInfoOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoOuterCore}
 */
xyz.swapee.wc.ITransactionInfoOuterCoreCaster.prototype.asITransactionInfoOuterCore
/**
 * Access the _TransactionInfoOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoOuterCore}
 */
xyz.swapee.wc.ITransactionInfoOuterCoreCaster.prototype.superTransactionInfoOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.Tid The transaction ID (optional overlay).
 * @prop {string} [tid=""] The transaction ID. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.Tid_Safe The transaction ID (required overlay).
 * @prop {string} tid The transaction ID.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransaction Forces to load transaction (optional overlay).
 * @prop {boolean} [getTransaction=false] Forces to load transaction. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransaction_Safe Forces to load transaction (required overlay).
 * @prop {boolean} getTransaction Forces to load transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyFrom  (optional overlay).
 * @prop {string} [currencyFrom=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyFrom_Safe  (required overlay).
 * @prop {string} currencyFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyTo  (optional overlay).
 * @prop {string} [currencyTo=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyTo_Safe  (required overlay).
 * @prop {string} currencyTo
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountFrom  (optional overlay).
 * @prop {string} [amountFrom=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountFrom_Safe  (required overlay).
 * @prop {string} amountFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountTo  (optional overlay).
 * @prop {string} [amountTo=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountTo_Safe  (required overlay).
 * @prop {string} amountTo
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.NetworkFee  (optional overlay).
 * @prop {string} [networkFee=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.NetworkFee_Safe  (required overlay).
 * @prop {string} networkFee
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.PartnerFee  (optional overlay).
 * @prop {string} [partnerFee=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.PartnerFee_Safe  (required overlay).
 * @prop {string} partnerFee
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.Rate  (optional overlay).
 * @prop {string} [rate=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.Rate_Safe  (required overlay).
 * @prop {string} rate
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.VisibleAmount  (optional overlay).
 * @prop {string} [visibleAmount=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.VisibleAmount_Safe  (required overlay).
 * @prop {string} visibleAmount
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.NotFound  (optional overlay).
 * @prop {boolean} [notFound=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.NotFound_Safe  (required overlay).
 * @prop {boolean} notFound
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.Fixed  (optional overlay).
 * @prop {?boolean} [fixed=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.Fixed_Safe  (required overlay).
 * @prop {?boolean} fixed
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.GettingTransaction  (optional overlay).
 * @prop {boolean} [gettingTransaction=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.GettingTransaction_Safe  (required overlay).
 * @prop {boolean} gettingTransaction
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransactionError  (optional overlay).
 * @prop {string} [getTransactionError=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransactionError_Safe  (required overlay).
 * @prop {string} getTransactionError
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Tid The transaction ID (optional overlay).
 * @prop {*} [tid=null] The transaction ID. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Tid_Safe The transaction ID (required overlay).
 * @prop {*} tid The transaction ID.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransaction Forces to load transaction (optional overlay).
 * @prop {*} [getTransaction=null] Forces to load transaction. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransaction_Safe Forces to load transaction (required overlay).
 * @prop {*} getTransaction Forces to load transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyFrom  (optional overlay).
 * @prop {*} [currencyFrom=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyFrom_Safe  (required overlay).
 * @prop {*} currencyFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyTo  (optional overlay).
 * @prop {*} [currencyTo=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyTo_Safe  (required overlay).
 * @prop {*} currencyTo
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountFrom  (optional overlay).
 * @prop {*} [amountFrom=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountFrom_Safe  (required overlay).
 * @prop {*} amountFrom
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountTo  (optional overlay).
 * @prop {*} [amountTo=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountTo_Safe  (required overlay).
 * @prop {*} amountTo
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NetworkFee  (optional overlay).
 * @prop {*} [networkFee=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NetworkFee_Safe  (required overlay).
 * @prop {*} networkFee
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.PartnerFee  (optional overlay).
 * @prop {*} [partnerFee=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.PartnerFee_Safe  (required overlay).
 * @prop {*} partnerFee
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Rate  (optional overlay).
 * @prop {*} [rate=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Rate_Safe  (required overlay).
 * @prop {*} rate
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.VisibleAmount  (optional overlay).
 * @prop {*} [visibleAmount=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.VisibleAmount_Safe  (required overlay).
 * @prop {*} visibleAmount
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NotFound  (optional overlay).
 * @prop {*} [notFound=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NotFound_Safe  (required overlay).
 * @prop {*} notFound
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Fixed  (optional overlay).
 * @prop {*} [fixed=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Fixed_Safe  (required overlay).
 * @prop {*} fixed
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GettingTransaction  (optional overlay).
 * @prop {*} [gettingTransaction=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GettingTransaction_Safe  (required overlay).
 * @prop {*} gettingTransaction
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransactionError  (optional overlay).
 * @prop {*} [getTransactionError=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransactionError_Safe  (required overlay).
 * @prop {*} getTransactionError
 */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Tid} xyz.swapee.wc.ITransactionInfoPort.Inputs.Tid The transaction ID (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Tid_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.Tid_Safe The transaction ID (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransaction} xyz.swapee.wc.ITransactionInfoPort.Inputs.GetTransaction Forces to load transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransaction_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.GetTransaction_Safe Forces to load transaction (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyFrom} xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyFrom_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyTo} xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyTo  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyTo_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyTo_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountTo} xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountTo  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountTo_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountTo_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NetworkFee} xyz.swapee.wc.ITransactionInfoPort.Inputs.NetworkFee  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NetworkFee_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.NetworkFee_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.PartnerFee} xyz.swapee.wc.ITransactionInfoPort.Inputs.PartnerFee  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.PartnerFee_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.PartnerFee_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Rate} xyz.swapee.wc.ITransactionInfoPort.Inputs.Rate  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Rate_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.Rate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.VisibleAmount} xyz.swapee.wc.ITransactionInfoPort.Inputs.VisibleAmount  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.VisibleAmount_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.VisibleAmount_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NotFound} xyz.swapee.wc.ITransactionInfoPort.Inputs.NotFound  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NotFound_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.NotFound_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Fixed} xyz.swapee.wc.ITransactionInfoPort.Inputs.Fixed  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Fixed_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.Fixed_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GettingTransaction} xyz.swapee.wc.ITransactionInfoPort.Inputs.GettingTransaction  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GettingTransaction_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.GettingTransaction_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransactionError} xyz.swapee.wc.ITransactionInfoPort.Inputs.GetTransactionError  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransactionError_Safe} xyz.swapee.wc.ITransactionInfoPort.Inputs.GetTransactionError_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Tid} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.Tid The transaction ID (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Tid_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.Tid_Safe The transaction ID (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransaction} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.GetTransaction Forces to load transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransaction_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.GetTransaction_Safe Forces to load transaction (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyFrom} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.CurrencyFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyFrom_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.CurrencyFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyTo} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.CurrencyTo  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.CurrencyTo_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.CurrencyTo_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountTo} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.AmountTo  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.AmountTo_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.AmountTo_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NetworkFee} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.NetworkFee  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NetworkFee_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.NetworkFee_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.PartnerFee} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.PartnerFee  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.PartnerFee_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.PartnerFee_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Rate} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.Rate  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Rate_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.Rate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.VisibleAmount} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.VisibleAmount  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.VisibleAmount_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.VisibleAmount_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NotFound} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.NotFound  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.NotFound_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.NotFound_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Fixed} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.Fixed  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.Fixed_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.Fixed_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GettingTransaction} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.GettingTransaction  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GettingTransaction_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.GettingTransaction_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransactionError} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.GetTransactionError  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.GetTransactionError_Safe} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.GetTransactionError_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.Tid} xyz.swapee.wc.ITransactionInfoCore.Model.Tid The transaction ID (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.Tid_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.Tid_Safe The transaction ID (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransaction} xyz.swapee.wc.ITransactionInfoCore.Model.GetTransaction Forces to load transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransaction_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.GetTransaction_Safe Forces to load transaction (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyFrom} xyz.swapee.wc.ITransactionInfoCore.Model.CurrencyFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyFrom_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.CurrencyFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyTo} xyz.swapee.wc.ITransactionInfoCore.Model.CurrencyTo  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.CurrencyTo_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.CurrencyTo_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountFrom} xyz.swapee.wc.ITransactionInfoCore.Model.AmountFrom  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountFrom_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.AmountFrom_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountTo} xyz.swapee.wc.ITransactionInfoCore.Model.AmountTo  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.AmountTo_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.AmountTo_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.NetworkFee} xyz.swapee.wc.ITransactionInfoCore.Model.NetworkFee  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.NetworkFee_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.NetworkFee_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.PartnerFee} xyz.swapee.wc.ITransactionInfoCore.Model.PartnerFee  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.PartnerFee_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.PartnerFee_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.Rate} xyz.swapee.wc.ITransactionInfoCore.Model.Rate  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.Rate_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.Rate_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.VisibleAmount} xyz.swapee.wc.ITransactionInfoCore.Model.VisibleAmount  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.VisibleAmount_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.VisibleAmount_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.NotFound} xyz.swapee.wc.ITransactionInfoCore.Model.NotFound  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.NotFound_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.NotFound_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.Fixed} xyz.swapee.wc.ITransactionInfoCore.Model.Fixed  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.Fixed_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.Fixed_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.GettingTransaction} xyz.swapee.wc.ITransactionInfoCore.Model.GettingTransaction  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.GettingTransaction_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.GettingTransaction_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransactionError} xyz.swapee.wc.ITransactionInfoCore.Model.GetTransactionError  (optional overlay). */

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model.GetTransactionError_Safe} xyz.swapee.wc.ITransactionInfoCore.Model.GetTransactionError_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/04-ITransactionInfoPort.xml}  e7081dddccc9390a7b1c5213c02d8fd2 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITransactionInfoPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoPort)} xyz.swapee.wc.AbstractTransactionInfoPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoPort} xyz.swapee.wc.TransactionInfoPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoPort` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoPort
 */
xyz.swapee.wc.AbstractTransactionInfoPort = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoPort.constructor&xyz.swapee.wc.TransactionInfoPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoPort.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoPort.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoPort|typeof xyz.swapee.wc.TransactionInfoPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoPort}
 */
xyz.swapee.wc.AbstractTransactionInfoPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoPort}
 */
xyz.swapee.wc.AbstractTransactionInfoPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoPort|typeof xyz.swapee.wc.TransactionInfoPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoPort}
 */
xyz.swapee.wc.AbstractTransactionInfoPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoPort|typeof xyz.swapee.wc.TransactionInfoPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoPort}
 */
xyz.swapee.wc.AbstractTransactionInfoPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoPort.Initialese[]) => xyz.swapee.wc.ITransactionInfoPort} xyz.swapee.wc.TransactionInfoPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITransactionInfoPort.Inputs>)} xyz.swapee.wc.ITransactionInfoPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ITransactionInfo_, providing input
 * pins.
 * @interface xyz.swapee.wc.ITransactionInfoPort
 */
xyz.swapee.wc.ITransactionInfoPort = class extends /** @type {xyz.swapee.wc.ITransactionInfoPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoPort.resetPort} */
xyz.swapee.wc.ITransactionInfoPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoPort.resetTransactionInfoPort} */
xyz.swapee.wc.ITransactionInfoPort.prototype.resetTransactionInfoPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoPort.Initialese>)} xyz.swapee.wc.TransactionInfoPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoPort} xyz.swapee.wc.ITransactionInfoPort.typeof */
/**
 * A concrete class of _ITransactionInfoPort_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoPort
 * @implements {xyz.swapee.wc.ITransactionInfoPort} The port that serves as an interface to the _ITransactionInfo_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoPort.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoPort = class extends /** @type {xyz.swapee.wc.TransactionInfoPort.constructor&xyz.swapee.wc.ITransactionInfoPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoPort}
 */
xyz.swapee.wc.TransactionInfoPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoPort.
 * @interface xyz.swapee.wc.ITransactionInfoPortFields
 */
xyz.swapee.wc.ITransactionInfoPortFields = class { }
/**
 * The inputs to the _ITransactionInfo_'s controller via its port.
 */
xyz.swapee.wc.ITransactionInfoPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITransactionInfoPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITransactionInfoPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoPort} */
xyz.swapee.wc.RecordITransactionInfoPort

/** @typedef {xyz.swapee.wc.ITransactionInfoPort} xyz.swapee.wc.BoundITransactionInfoPort */

/** @typedef {xyz.swapee.wc.TransactionInfoPort} xyz.swapee.wc.BoundTransactionInfoPort */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel)} xyz.swapee.wc.ITransactionInfoPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel} xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ITransactionInfo_'s controller via its port.
 * @record xyz.swapee.wc.ITransactionInfoPort.Inputs
 */
xyz.swapee.wc.ITransactionInfoPort.Inputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoPort.Inputs.constructor&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoPort.Inputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel)} xyz.swapee.wc.ITransactionInfoPort.WeakInputs.constructor */
/**
 * The inputs to the _ITransactionInfo_'s controller via its port.
 * @record xyz.swapee.wc.ITransactionInfoPort.WeakInputs
 */
xyz.swapee.wc.ITransactionInfoPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoPort.WeakInputs.constructor&xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ITransactionInfoPortInterface
 */
xyz.swapee.wc.ITransactionInfoPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ITransactionInfoPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ITransactionInfoPortInterface.prototype.constructor = xyz.swapee.wc.ITransactionInfoPortInterface

/**
 * A concrete class of _ITransactionInfoPortInterface_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoPortInterface
 * @implements {xyz.swapee.wc.ITransactionInfoPortInterface} The port interface.
 */
xyz.swapee.wc.TransactionInfoPortInterface = class extends xyz.swapee.wc.ITransactionInfoPortInterface { }
xyz.swapee.wc.TransactionInfoPortInterface.prototype.constructor = xyz.swapee.wc.TransactionInfoPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoPortInterface.Props
 * @prop {string} tid The transaction ID.
 * @prop {boolean} getTransaction Forces to load transaction.
 * @prop {string} currencyFrom
 * @prop {string} currencyTo
 * @prop {string} amountFrom
 * @prop {string} amountTo
 * @prop {string} networkFee
 * @prop {string} partnerFee
 * @prop {string} rate
 * @prop {string} visibleAmount
 * @prop {boolean} notFound
 * @prop {?boolean} fixed
 * @prop {boolean} gettingTransaction
 * @prop {string} getTransactionError
 */

/**
 * Contains getters to cast the _ITransactionInfoPort_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoPortCaster
 */
xyz.swapee.wc.ITransactionInfoPortCaster = class { }
/**
 * Cast the _ITransactionInfoPort_ instance into the _BoundITransactionInfoPort_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoPort}
 */
xyz.swapee.wc.ITransactionInfoPortCaster.prototype.asITransactionInfoPort
/**
 * Access the _TransactionInfoPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoPort}
 */
xyz.swapee.wc.ITransactionInfoPortCaster.prototype.superTransactionInfoPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoPort.__resetPort<!xyz.swapee.wc.ITransactionInfoPort>} xyz.swapee.wc.ITransactionInfoPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoPort.resetPort} */
/**
 * Resets the _ITransactionInfo_ port.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoPort.__resetTransactionInfoPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoPort.__resetTransactionInfoPort<!xyz.swapee.wc.ITransactionInfoPort>} xyz.swapee.wc.ITransactionInfoPort._resetTransactionInfoPort */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoPort.resetTransactionInfoPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoPort.resetTransactionInfoPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/09-ITransactionInfoCore.xml}  cdb79e371932d5c3bfe58ae8ff859b17 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITransactionInfoCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoCore)} xyz.swapee.wc.AbstractTransactionInfoCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoCore} xyz.swapee.wc.TransactionInfoCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoCore` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoCore
 */
xyz.swapee.wc.AbstractTransactionInfoCore = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoCore.constructor&xyz.swapee.wc.TransactionInfoCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoCore.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoCore.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoCore|typeof xyz.swapee.wc.TransactionInfoCore)|(!xyz.swapee.wc.ITransactionInfoOuterCore|typeof xyz.swapee.wc.TransactionInfoOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoCore}
 */
xyz.swapee.wc.AbstractTransactionInfoCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoCore}
 */
xyz.swapee.wc.AbstractTransactionInfoCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoCore|typeof xyz.swapee.wc.TransactionInfoCore)|(!xyz.swapee.wc.ITransactionInfoOuterCore|typeof xyz.swapee.wc.TransactionInfoOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoCore}
 */
xyz.swapee.wc.AbstractTransactionInfoCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoCore|typeof xyz.swapee.wc.TransactionInfoCore)|(!xyz.swapee.wc.ITransactionInfoOuterCore|typeof xyz.swapee.wc.TransactionInfoOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoCore}
 */
xyz.swapee.wc.AbstractTransactionInfoCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoCoreCaster&xyz.swapee.wc.ITransactionInfoOuterCore)} xyz.swapee.wc.ITransactionInfoCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ITransactionInfoCore
 */
xyz.swapee.wc.ITransactionInfoCore = class extends /** @type {xyz.swapee.wc.ITransactionInfoCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ITransactionInfoCore.resetCore} */
xyz.swapee.wc.ITransactionInfoCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoCore.resetTransactionInfoCore} */
xyz.swapee.wc.ITransactionInfoCore.prototype.resetTransactionInfoCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoCore.Initialese>)} xyz.swapee.wc.TransactionInfoCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoCore} xyz.swapee.wc.ITransactionInfoCore.typeof */
/**
 * A concrete class of _ITransactionInfoCore_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoCore
 * @implements {xyz.swapee.wc.ITransactionInfoCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoCore.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoCore = class extends /** @type {xyz.swapee.wc.TransactionInfoCore.constructor&xyz.swapee.wc.ITransactionInfoCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoCore.prototype.constructor = xyz.swapee.wc.TransactionInfoCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoCore}
 */
xyz.swapee.wc.TransactionInfoCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoCore.
 * @interface xyz.swapee.wc.ITransactionInfoCoreFields
 */
xyz.swapee.wc.ITransactionInfoCoreFields = class { }
/**
 * The _ITransactionInfo_'s memory.
 */
xyz.swapee.wc.ITransactionInfoCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITransactionInfoCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ITransactionInfoCoreFields.prototype.props = /** @type {xyz.swapee.wc.ITransactionInfoCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoCore} */
xyz.swapee.wc.RecordITransactionInfoCore

/** @typedef {xyz.swapee.wc.ITransactionInfoCore} xyz.swapee.wc.BoundITransactionInfoCore */

/** @typedef {xyz.swapee.wc.TransactionInfoCore} xyz.swapee.wc.BoundTransactionInfoCore */

/**
 * The folder in the docs directory from which icons can be loaded.
 * @typedef {string}
 */
xyz.swapee.wc.ITransactionInfoCore.Model.IconsFolder.iconsFolder

/** @typedef {xyz.swapee.wc.ITransactionInfoOuterCore.Model&xyz.swapee.wc.ITransactionInfoCore.Model.IconsFolder} xyz.swapee.wc.ITransactionInfoCore.Model The _ITransactionInfo_'s memory. */

/**
 * Contains getters to cast the _ITransactionInfoCore_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoCoreCaster
 */
xyz.swapee.wc.ITransactionInfoCoreCaster = class { }
/**
 * Cast the _ITransactionInfoCore_ instance into the _BoundITransactionInfoCore_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoCore}
 */
xyz.swapee.wc.ITransactionInfoCoreCaster.prototype.asITransactionInfoCore
/**
 * Access the _TransactionInfoCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoCore}
 */
xyz.swapee.wc.ITransactionInfoCoreCaster.prototype.superTransactionInfoCore

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoCore.Model.IconsFolder The folder in the docs directory from which icons can be loaded (optional overlay).
 * @prop {string} [iconsFolder=""] The folder in the docs directory from which icons can be loaded. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoCore.Model.IconsFolder_Safe The folder in the docs directory from which icons can be loaded (required overlay).
 * @prop {string} iconsFolder The folder in the docs directory from which icons can be loaded.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoCore.__resetCore<!xyz.swapee.wc.ITransactionInfoCore>} xyz.swapee.wc.ITransactionInfoCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoCore.resetCore} */
/**
 * Resets the _ITransactionInfo_ core.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoCore.__resetTransactionInfoCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoCore.__resetTransactionInfoCore<!xyz.swapee.wc.ITransactionInfoCore>} xyz.swapee.wc.ITransactionInfoCore._resetTransactionInfoCore */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoCore.resetTransactionInfoCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoCore.resetTransactionInfoCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/10-ITransactionInfoProcessor.xml}  7753c1a5ae5a96e89b54e83f06748d8c */
/** @typedef {xyz.swapee.wc.ITransactionInfoComputer.Initialese&xyz.swapee.wc.ITransactionInfoController.Initialese} xyz.swapee.wc.ITransactionInfoProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoProcessor)} xyz.swapee.wc.AbstractTransactionInfoProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoProcessor} xyz.swapee.wc.TransactionInfoProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoProcessor
 */
xyz.swapee.wc.AbstractTransactionInfoProcessor = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoProcessor.constructor&xyz.swapee.wc.TransactionInfoProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoProcessor.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!xyz.swapee.wc.ITransactionInfoCore|typeof xyz.swapee.wc.TransactionInfoCore)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!xyz.swapee.wc.ITransactionInfoCore|typeof xyz.swapee.wc.TransactionInfoCore)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!xyz.swapee.wc.ITransactionInfoCore|typeof xyz.swapee.wc.TransactionInfoCore)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoProcessor}
 */
xyz.swapee.wc.AbstractTransactionInfoProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoProcessor.Initialese[]) => xyz.swapee.wc.ITransactionInfoProcessor} xyz.swapee.wc.TransactionInfoProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoProcessorCaster&xyz.swapee.wc.ITransactionInfoComputer&xyz.swapee.wc.ITransactionInfoCore&xyz.swapee.wc.ITransactionInfoController)} xyz.swapee.wc.ITransactionInfoProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoComputer} xyz.swapee.wc.ITransactionInfoComputer.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoCore} xyz.swapee.wc.ITransactionInfoCore.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoController} xyz.swapee.wc.ITransactionInfoController.typeof */
/**
 * The processor to compute changes to the memory for the _ITransactionInfo_.
 * @interface xyz.swapee.wc.ITransactionInfoProcessor
 */
xyz.swapee.wc.ITransactionInfoProcessor = class extends /** @type {xyz.swapee.wc.ITransactionInfoProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoComputer.typeof&xyz.swapee.wc.ITransactionInfoCore.typeof&xyz.swapee.wc.ITransactionInfoController.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoProcessor.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoProcessor.pulseGetTransaction} */
xyz.swapee.wc.ITransactionInfoProcessor.prototype.pulseGetTransaction = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoProcessor.Initialese>)} xyz.swapee.wc.TransactionInfoProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoProcessor} xyz.swapee.wc.ITransactionInfoProcessor.typeof */
/**
 * A concrete class of _ITransactionInfoProcessor_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoProcessor
 * @implements {xyz.swapee.wc.ITransactionInfoProcessor} The processor to compute changes to the memory for the _ITransactionInfo_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoProcessor.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoProcessor = class extends /** @type {xyz.swapee.wc.TransactionInfoProcessor.constructor&xyz.swapee.wc.ITransactionInfoProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoProcessor}
 */
xyz.swapee.wc.TransactionInfoProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoProcessor} */
xyz.swapee.wc.RecordITransactionInfoProcessor

/** @typedef {xyz.swapee.wc.ITransactionInfoProcessor} xyz.swapee.wc.BoundITransactionInfoProcessor */

/** @typedef {xyz.swapee.wc.TransactionInfoProcessor} xyz.swapee.wc.BoundTransactionInfoProcessor */

/**
 * Contains getters to cast the _ITransactionInfoProcessor_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoProcessorCaster
 */
xyz.swapee.wc.ITransactionInfoProcessorCaster = class { }
/**
 * Cast the _ITransactionInfoProcessor_ instance into the _BoundITransactionInfoProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoProcessor}
 */
xyz.swapee.wc.ITransactionInfoProcessorCaster.prototype.asITransactionInfoProcessor
/**
 * Access the _TransactionInfoProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoProcessor}
 */
xyz.swapee.wc.ITransactionInfoProcessorCaster.prototype.superTransactionInfoProcessor

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoProcessor.__pulseGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoProcessor.__pulseGetTransaction<!xyz.swapee.wc.ITransactionInfoProcessor>} xyz.swapee.wc.ITransactionInfoProcessor._pulseGetTransaction */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoProcessor.pulseGetTransaction} */
/**
 * A method called to set the `getTransaction` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoProcessor.pulseGetTransaction = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoProcessor
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/100-TransactionInfoMemory.xml}  6520f3ae435b4b8141848966acb5c748 */
/**
 * The memory of the _ITransactionInfo_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.TransactionInfoMemory
 */
xyz.swapee.wc.TransactionInfoMemory = class { }
/**
 * The transaction ID. Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.tid = /** @type {string} */ (void 0)
/**
 * Forces to load transaction. Default `false`.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.getTransaction = /** @type {boolean} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.currencyFrom = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.currencyTo = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.amountFrom = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.amountTo = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.networkFee = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.partnerFee = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.rate = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.visibleAmount = /** @type {string} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.notFound = /** @type {boolean} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.fixed = /** @type {?boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.gettingTransaction = /** @type {boolean} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.getTransactionError = /** @type {string} */ (void 0)
/**
 * The folder in the docs directory from which icons can be loaded. Default empty string.
 */
xyz.swapee.wc.TransactionInfoMemory.prototype.iconsFolder = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/102-TransactionInfoInputs.xml}  e85f5c03599a11aa890a18291c740001 */
/**
 * The inputs of the _ITransactionInfo_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.TransactionInfoInputs
 */
xyz.swapee.wc.front.TransactionInfoInputs = class { }
/**
 * The transaction ID. Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.tid = /** @type {string|undefined} */ (void 0)
/**
 * Forces to load transaction. Default `false`.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.getTransaction = /** @type {boolean|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.currencyFrom = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.currencyTo = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.amountFrom = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.amountTo = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.networkFee = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.partnerFee = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.rate = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.visibleAmount = /** @type {string|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.notFound = /** @type {boolean|undefined} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.fixed = /** @type {(?boolean)|undefined} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.gettingTransaction = /** @type {boolean|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.TransactionInfoInputs.prototype.getTransactionError = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/11-ITransactionInfo.xml}  593052002ab82b13deeb3e0d16db7378 */
/**
 * An atomic wrapper for the _ITransactionInfo_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.TransactionInfoEnv
 */
xyz.swapee.wc.TransactionInfoEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.TransactionInfoEnv.prototype.transactionInfo = /** @type {xyz.swapee.wc.ITransactionInfo} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.ITransactionInfoController.Inputs>&xyz.swapee.wc.ITransactionInfoProcessor.Initialese&xyz.swapee.wc.ITransactionInfoComputer.Initialese&xyz.swapee.wc.ITransactionInfoController.Initialese} xyz.swapee.wc.ITransactionInfo.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfo)} xyz.swapee.wc.AbstractTransactionInfo.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfo} xyz.swapee.wc.TransactionInfo.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfo` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfo
 */
xyz.swapee.wc.AbstractTransactionInfo = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfo.constructor&xyz.swapee.wc.TransactionInfo.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfo.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfo
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfo.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfo} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfo|typeof xyz.swapee.wc.TransactionInfo)|(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfo}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfo.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfo}
 */
xyz.swapee.wc.AbstractTransactionInfo.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfo}
 */
xyz.swapee.wc.AbstractTransactionInfo.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfo|typeof xyz.swapee.wc.TransactionInfo)|(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfo}
 */
xyz.swapee.wc.AbstractTransactionInfo.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfo|typeof xyz.swapee.wc.TransactionInfo)|(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfo}
 */
xyz.swapee.wc.AbstractTransactionInfo.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfo.Initialese[]) => xyz.swapee.wc.ITransactionInfo} xyz.swapee.wc.TransactionInfoConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfo.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ITransactionInfo.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ITransactionInfo.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ITransactionInfo.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.TransactionInfoClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoCaster&xyz.swapee.wc.ITransactionInfoProcessor&xyz.swapee.wc.ITransactionInfoComputer&xyz.swapee.wc.ITransactionInfoController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.ITransactionInfoController.Inputs, null>)} xyz.swapee.wc.ITransactionInfo.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ITransactionInfo
 */
xyz.swapee.wc.ITransactionInfo = class extends /** @type {xyz.swapee.wc.ITransactionInfo.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoProcessor.typeof&xyz.swapee.wc.ITransactionInfoComputer.typeof&xyz.swapee.wc.ITransactionInfoController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfo* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfo.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfo.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfo&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfo.Initialese>)} xyz.swapee.wc.TransactionInfo.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfo} xyz.swapee.wc.ITransactionInfo.typeof */
/**
 * A concrete class of _ITransactionInfo_ instances.
 * @constructor xyz.swapee.wc.TransactionInfo
 * @implements {xyz.swapee.wc.ITransactionInfo} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfo.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfo = class extends /** @type {xyz.swapee.wc.TransactionInfo.constructor&xyz.swapee.wc.ITransactionInfo.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfo* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfo.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfo* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfo.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfo.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfo}
 */
xyz.swapee.wc.TransactionInfo.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfo.
 * @interface xyz.swapee.wc.ITransactionInfoFields
 */
xyz.swapee.wc.ITransactionInfoFields = class { }
/**
 * The input pins of the _ITransactionInfo_ port.
 */
xyz.swapee.wc.ITransactionInfoFields.prototype.pinout = /** @type {!xyz.swapee.wc.ITransactionInfo.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfo} */
xyz.swapee.wc.RecordITransactionInfo

/** @typedef {xyz.swapee.wc.ITransactionInfo} xyz.swapee.wc.BoundITransactionInfo */

/** @typedef {xyz.swapee.wc.TransactionInfo} xyz.swapee.wc.BoundTransactionInfo */

/** @typedef {xyz.swapee.wc.ITransactionInfoController.Inputs} xyz.swapee.wc.ITransactionInfo.Pinout The input pins of the _ITransactionInfo_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITransactionInfoController.Inputs>)} xyz.swapee.wc.ITransactionInfoBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ITransactionInfoBuffer
 */
xyz.swapee.wc.ITransactionInfoBuffer = class extends /** @type {xyz.swapee.wc.ITransactionInfoBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoBuffer.prototype.constructor = xyz.swapee.wc.ITransactionInfoBuffer

/**
 * A concrete class of _ITransactionInfoBuffer_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoBuffer
 * @implements {xyz.swapee.wc.ITransactionInfoBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.TransactionInfoBuffer = class extends xyz.swapee.wc.ITransactionInfoBuffer { }
xyz.swapee.wc.TransactionInfoBuffer.prototype.constructor = xyz.swapee.wc.TransactionInfoBuffer

/**
 * Contains getters to cast the _ITransactionInfo_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoCaster
 */
xyz.swapee.wc.ITransactionInfoCaster = class { }
/**
 * Cast the _ITransactionInfo_ instance into the _BoundITransactionInfo_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfo}
 */
xyz.swapee.wc.ITransactionInfoCaster.prototype.asITransactionInfo
/**
 * Access the _TransactionInfo_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfo}
 */
xyz.swapee.wc.ITransactionInfoCaster.prototype.superTransactionInfo

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/110-TransactionInfoSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoMemoryPQs
 */
xyz.swapee.wc.TransactionInfoMemoryPQs = class {
  constructor() {
    /**
     * `j7bea`
     */
    this.tid=/** @type {string} */ (void 0)
    /**
     * `j6c88`
     */
    this.currencyFrom=/** @type {string} */ (void 0)
    /**
     * `c23cd`
     */
    this.currencyTo=/** @type {string} */ (void 0)
    /**
     * `h48e6`
     */
    this.amountFrom=/** @type {string} */ (void 0)
    /**
     * `jdc9f`
     */
    this.amountTo=/** @type {string} */ (void 0)
    /**
     * `ffd9c`
     */
    this.networkFee=/** @type {string} */ (void 0)
    /**
     * `j6f44`
     */
    this.partnerFee=/** @type {string} */ (void 0)
    /**
     * `e685c`
     */
    this.visibleAmount=/** @type {string} */ (void 0)
    /**
     * `cec31`
     */
    this.fixed=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TransactionInfoMemoryPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoMemoryQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoMemoryQPs = class { }
/**
 * `tid`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.j7bea = /** @type {string} */ (void 0)
/**
 * `currencyFrom`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.j6c88 = /** @type {string} */ (void 0)
/**
 * `currencyTo`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.c23cd = /** @type {string} */ (void 0)
/**
 * `amountFrom`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.h48e6 = /** @type {string} */ (void 0)
/**
 * `amountTo`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.jdc9f = /** @type {string} */ (void 0)
/**
 * `networkFee`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.ffd9c = /** @type {string} */ (void 0)
/**
 * `partnerFee`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.j6f44 = /** @type {string} */ (void 0)
/**
 * `visibleAmount`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.e685c = /** @type {string} */ (void 0)
/**
 * `fixed`
 */
xyz.swapee.wc.TransactionInfoMemoryQPs.prototype.cec31 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoMemoryPQs)} xyz.swapee.wc.TransactionInfoInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoMemoryPQs} xyz.swapee.wc.TransactionInfoMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoInputsPQs
 */
xyz.swapee.wc.TransactionInfoInputsPQs = class extends /** @type {xyz.swapee.wc.TransactionInfoInputsPQs.constructor&xyz.swapee.wc.TransactionInfoMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoInputsPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoInputsPQs

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoMemoryPQs)} xyz.swapee.wc.TransactionInfoInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoInputsQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoInputsQPs = class extends /** @type {xyz.swapee.wc.TransactionInfoInputsQPs.constructor&xyz.swapee.wc.TransactionInfoMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TransactionInfoInputsQPs.prototype.constructor = xyz.swapee.wc.TransactionInfoInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoVdusPQs
 */
xyz.swapee.wc.TransactionInfoVdusPQs = class {
  constructor() {
    /**
     * `j0b21`
     */
    this.CryptoIn=/** @type {string} */ (void 0)
    /**
     * `j0b22`
     */
    this.CryptoOut=/** @type {string} */ (void 0)
    /**
     * `j0b23`
     */
    this.AmountIn=/** @type {string} */ (void 0)
    /**
     * `j0b24`
     */
    this.AmountOut=/** @type {string} */ (void 0)
    /**
     * `j0b25`
     */
    this.FixedLock=/** @type {string} */ (void 0)
    /**
     * `j0b26`
     */
    this.NetworkFeeWr=/** @type {string} */ (void 0)
    /**
     * `j0b27`
     */
    this.NetworkFeeLa=/** @type {string} */ (void 0)
    /**
     * `j0b28`
     */
    this.PartnerFeeWr=/** @type {string} */ (void 0)
    /**
     * `j0b29`
     */
    this.PartnerFeeLa=/** @type {string} */ (void 0)
    /**
     * `j0b210`
     */
    this.VisibleAmountWr=/** @type {string} */ (void 0)
    /**
     * `j0b211`
     */
    this.VisibleAmounLa=/** @type {string} */ (void 0)
    /**
     * `j0b212`
     */
    this.BreakdownBu=/** @type {string} */ (void 0)
    /**
     * `j0b213`
     */
    this.InIm=/** @type {string} */ (void 0)
    /**
     * `j0b214`
     */
    this.OutIm=/** @type {string} */ (void 0)
    /**
     * `j0b215`
     */
    this.AmountOutLoIn=/** @type {string} */ (void 0)
    /**
     * `j0b216`
     */
    this.ReloadGetTransactionBu=/** @type {string} */ (void 0)
    /**
     * `j0b217`
     */
    this.GetTransactionErrorWr=/** @type {string} */ (void 0)
    /**
     * `j0b218`
     */
    this.GetTransactionErrorLa=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TransactionInfoVdusPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoVdusQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoVdusQPs = class { }
/**
 * `CryptoIn`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b21 = /** @type {string} */ (void 0)
/**
 * `CryptoOut`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b22 = /** @type {string} */ (void 0)
/**
 * `AmountIn`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b23 = /** @type {string} */ (void 0)
/**
 * `AmountOut`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b24 = /** @type {string} */ (void 0)
/**
 * `FixedLock`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b25 = /** @type {string} */ (void 0)
/**
 * `NetworkFeeWr`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b26 = /** @type {string} */ (void 0)
/**
 * `NetworkFeeLa`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b27 = /** @type {string} */ (void 0)
/**
 * `PartnerFeeWr`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b28 = /** @type {string} */ (void 0)
/**
 * `PartnerFeeLa`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b29 = /** @type {string} */ (void 0)
/**
 * `VisibleAmountWr`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b210 = /** @type {string} */ (void 0)
/**
 * `VisibleAmounLa`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b211 = /** @type {string} */ (void 0)
/**
 * `BreakdownBu`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b212 = /** @type {string} */ (void 0)
/**
 * `InIm`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b213 = /** @type {string} */ (void 0)
/**
 * `OutIm`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b214 = /** @type {string} */ (void 0)
/**
 * `AmountOutLoIn`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b215 = /** @type {string} */ (void 0)
/**
 * `ReloadGetTransactionBu`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b216 = /** @type {string} */ (void 0)
/**
 * `GetTransactionErrorWr`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b217 = /** @type {string} */ (void 0)
/**
 * `GetTransactionErrorLa`
 */
xyz.swapee.wc.TransactionInfoVdusQPs.prototype.j0b218 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TransactionInfoClassesPQs
 */
xyz.swapee.wc.TransactionInfoClassesPQs = class {
  constructor() {
    /**
     * `jbd81`
     */
    this.Class=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TransactionInfoClassesPQs.prototype.constructor = xyz.swapee.wc.TransactionInfoClassesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TransactionInfoClassesQPs
 * @dict
 */
xyz.swapee.wc.TransactionInfoClassesQPs = class { }
/**
 * `Class`
 */
xyz.swapee.wc.TransactionInfoClassesQPs.prototype.jbd81 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/12-ITransactionInfoHtmlComponent.xml}  50f114f833a8844b2aab66d2a42cc0c1 */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoController.Initialese&xyz.swapee.wc.back.ITransactionInfoScreen.Initialese&xyz.swapee.wc.ITransactionInfo.Initialese&xyz.swapee.wc.ITransactionInfoGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ITransactionInfoProcessor.Initialese&xyz.swapee.wc.ITransactionInfoComputer.Initialese} xyz.swapee.wc.ITransactionInfoHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoHtmlComponent)} xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoHtmlComponent} xyz.swapee.wc.TransactionInfoHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoHtmlComponent
 */
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.constructor&xyz.swapee.wc.TransactionInfoHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHtmlComponent|typeof xyz.swapee.wc.TransactionInfoHtmlComponent)|(!xyz.swapee.wc.back.ITransactionInfoController|typeof xyz.swapee.wc.back.TransactionInfoController)|(!xyz.swapee.wc.back.ITransactionInfoScreen|typeof xyz.swapee.wc.back.TransactionInfoScreen)|(!xyz.swapee.wc.ITransactionInfo|typeof xyz.swapee.wc.TransactionInfo)|(!xyz.swapee.wc.ITransactionInfoGPU|typeof xyz.swapee.wc.TransactionInfoGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHtmlComponent|typeof xyz.swapee.wc.TransactionInfoHtmlComponent)|(!xyz.swapee.wc.back.ITransactionInfoController|typeof xyz.swapee.wc.back.TransactionInfoController)|(!xyz.swapee.wc.back.ITransactionInfoScreen|typeof xyz.swapee.wc.back.TransactionInfoScreen)|(!xyz.swapee.wc.ITransactionInfo|typeof xyz.swapee.wc.TransactionInfo)|(!xyz.swapee.wc.ITransactionInfoGPU|typeof xyz.swapee.wc.TransactionInfoGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoHtmlComponent|typeof xyz.swapee.wc.TransactionInfoHtmlComponent)|(!xyz.swapee.wc.back.ITransactionInfoController|typeof xyz.swapee.wc.back.TransactionInfoController)|(!xyz.swapee.wc.back.ITransactionInfoScreen|typeof xyz.swapee.wc.back.TransactionInfoScreen)|(!xyz.swapee.wc.ITransactionInfo|typeof xyz.swapee.wc.TransactionInfo)|(!xyz.swapee.wc.ITransactionInfoGPU|typeof xyz.swapee.wc.TransactionInfoGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITransactionInfoProcessor|typeof xyz.swapee.wc.TransactionInfoProcessor)|(!xyz.swapee.wc.ITransactionInfoComputer|typeof xyz.swapee.wc.TransactionInfoComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoHtmlComponent}
 */
xyz.swapee.wc.AbstractTransactionInfoHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoHtmlComponent.Initialese[]) => xyz.swapee.wc.ITransactionInfoHtmlComponent} xyz.swapee.wc.TransactionInfoHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoHtmlComponentCaster&xyz.swapee.wc.back.ITransactionInfoController&xyz.swapee.wc.back.ITransactionInfoScreen&xyz.swapee.wc.ITransactionInfo&xyz.swapee.wc.ITransactionInfoGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.ITransactionInfoController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.ITransactionInfoProcessor&xyz.swapee.wc.ITransactionInfoComputer)} xyz.swapee.wc.ITransactionInfoHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoController} xyz.swapee.wc.back.ITransactionInfoController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoScreen} xyz.swapee.wc.back.ITransactionInfoScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoGPU} xyz.swapee.wc.ITransactionInfoGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _ITransactionInfo_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ITransactionInfoHtmlComponent
 */
xyz.swapee.wc.ITransactionInfoHtmlComponent = class extends /** @type {xyz.swapee.wc.ITransactionInfoHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITransactionInfoController.typeof&xyz.swapee.wc.back.ITransactionInfoScreen.typeof&xyz.swapee.wc.ITransactionInfo.typeof&xyz.swapee.wc.ITransactionInfoGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ITransactionInfoProcessor.typeof&xyz.swapee.wc.ITransactionInfoComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHtmlComponent.Initialese>)} xyz.swapee.wc.TransactionInfoHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoHtmlComponent} xyz.swapee.wc.ITransactionInfoHtmlComponent.typeof */
/**
 * A concrete class of _ITransactionInfoHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoHtmlComponent
 * @implements {xyz.swapee.wc.ITransactionInfoHtmlComponent} The _ITransactionInfo_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoHtmlComponent = class extends /** @type {xyz.swapee.wc.TransactionInfoHtmlComponent.constructor&xyz.swapee.wc.ITransactionInfoHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoHtmlComponent}
 */
xyz.swapee.wc.TransactionInfoHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoHtmlComponent} */
xyz.swapee.wc.RecordITransactionInfoHtmlComponent

/** @typedef {xyz.swapee.wc.ITransactionInfoHtmlComponent} xyz.swapee.wc.BoundITransactionInfoHtmlComponent */

/** @typedef {xyz.swapee.wc.TransactionInfoHtmlComponent} xyz.swapee.wc.BoundTransactionInfoHtmlComponent */

/**
 * Contains getters to cast the _ITransactionInfoHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoHtmlComponentCaster
 */
xyz.swapee.wc.ITransactionInfoHtmlComponentCaster = class { }
/**
 * Cast the _ITransactionInfoHtmlComponent_ instance into the _BoundITransactionInfoHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoHtmlComponent}
 */
xyz.swapee.wc.ITransactionInfoHtmlComponentCaster.prototype.asITransactionInfoHtmlComponent
/**
 * Access the _TransactionInfoHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoHtmlComponent}
 */
xyz.swapee.wc.ITransactionInfoHtmlComponentCaster.prototype.superTransactionInfoHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/130-ITransactionInfoElement.xml}  28f06e9ef7c5e85950240f8ed7907c72 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.ITransactionInfoElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ITransactionInfoElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoElement)} xyz.swapee.wc.AbstractTransactionInfoElement.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoElement} xyz.swapee.wc.TransactionInfoElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoElement` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoElement
 */
xyz.swapee.wc.AbstractTransactionInfoElement = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoElement.constructor&xyz.swapee.wc.TransactionInfoElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoElement.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoElement.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoElement|typeof xyz.swapee.wc.TransactionInfoElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoElement}
 */
xyz.swapee.wc.AbstractTransactionInfoElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElement}
 */
xyz.swapee.wc.AbstractTransactionInfoElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoElement|typeof xyz.swapee.wc.TransactionInfoElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElement}
 */
xyz.swapee.wc.AbstractTransactionInfoElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoElement|typeof xyz.swapee.wc.TransactionInfoElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElement}
 */
xyz.swapee.wc.AbstractTransactionInfoElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoElement.Initialese[]) => xyz.swapee.wc.ITransactionInfoElement} xyz.swapee.wc.TransactionInfoElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoElementFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.ITransactionInfoElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.ITransactionInfoElement.Inputs, null>)} xyz.swapee.wc.ITransactionInfoElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _ITransactionInfo_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ITransactionInfoElement
 */
xyz.swapee.wc.ITransactionInfoElement = class extends /** @type {xyz.swapee.wc.ITransactionInfoElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoElement.solder} */
xyz.swapee.wc.ITransactionInfoElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoElement.render} */
xyz.swapee.wc.ITransactionInfoElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoElement.server} */
xyz.swapee.wc.ITransactionInfoElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoElement.inducer} */
xyz.swapee.wc.ITransactionInfoElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoElement&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoElement.Initialese>)} xyz.swapee.wc.TransactionInfoElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElement} xyz.swapee.wc.ITransactionInfoElement.typeof */
/**
 * A concrete class of _ITransactionInfoElement_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoElement
 * @implements {xyz.swapee.wc.ITransactionInfoElement} A component description.
 *
 * The _ITransactionInfo_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoElement.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoElement = class extends /** @type {xyz.swapee.wc.TransactionInfoElement.constructor&xyz.swapee.wc.ITransactionInfoElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoElement}
 */
xyz.swapee.wc.TransactionInfoElement.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoElement.
 * @interface xyz.swapee.wc.ITransactionInfoElementFields
 */
xyz.swapee.wc.ITransactionInfoElementFields = class { }
/**
 * The element-specific inputs to the _ITransactionInfo_ component.
 */
xyz.swapee.wc.ITransactionInfoElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoElement} */
xyz.swapee.wc.RecordITransactionInfoElement

/** @typedef {xyz.swapee.wc.ITransactionInfoElement} xyz.swapee.wc.BoundITransactionInfoElement */

/** @typedef {xyz.swapee.wc.TransactionInfoElement} xyz.swapee.wc.BoundTransactionInfoElement */

/** @typedef {xyz.swapee.wc.ITransactionInfoPort.Inputs&xyz.swapee.wc.ITransactionInfoDisplay.Queries&xyz.swapee.wc.ITransactionInfoController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ITransactionInfoElementPort.Inputs} xyz.swapee.wc.ITransactionInfoElement.Inputs The element-specific inputs to the _ITransactionInfo_ component. */

/**
 * Contains getters to cast the _ITransactionInfoElement_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoElementCaster
 */
xyz.swapee.wc.ITransactionInfoElementCaster = class { }
/**
 * Cast the _ITransactionInfoElement_ instance into the _BoundITransactionInfoElement_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoElement}
 */
xyz.swapee.wc.ITransactionInfoElementCaster.prototype.asITransactionInfoElement
/**
 * Access the _TransactionInfoElement_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoElement}
 */
xyz.swapee.wc.ITransactionInfoElementCaster.prototype.superTransactionInfoElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.TransactionInfoMemory, props: !xyz.swapee.wc.ITransactionInfoElement.Inputs) => Object<string, *>} xyz.swapee.wc.ITransactionInfoElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoElement.__solder<!xyz.swapee.wc.ITransactionInfoElement>} xyz.swapee.wc.ITransactionInfoElement._solder */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.TransactionInfoMemory} model The model.
 * - `tid` _string_ The transaction ID. Default empty string.
 * - `currencyFrom` _string_ Default empty string.
 * - `currencyTo` _string_ Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `amountTo` _string_ Default empty string.
 * - `networkFee` _string_ Default empty string.
 * - `partnerFee` _string_ Default empty string.
 * - `visibleAmount` _string_ Default empty string.
 * - `fixed` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.ITransactionInfoElement.Inputs} props The element props.
 * - `[tid=null]` _&#42;?_ The transaction ID. ⤴ *ITransactionInfoOuterCore.WeakModel.Tid* ⤴ *ITransactionInfoOuterCore.WeakModel.Tid* Default `null`.
 * - `[currencyFrom=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyFrom* ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyFrom* Default `null`.
 * - `[currencyTo=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyTo* ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyTo* Default `null`.
 * - `[amountFrom=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.AmountFrom* ⤴ *ITransactionInfoOuterCore.WeakModel.AmountFrom* Default `null`.
 * - `[amountTo=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.AmountTo* ⤴ *ITransactionInfoOuterCore.WeakModel.AmountTo* Default `null`.
 * - `[networkFee=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.NetworkFee* ⤴ *ITransactionInfoOuterCore.WeakModel.NetworkFee* Default `null`.
 * - `[partnerFee=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.PartnerFee* ⤴ *ITransactionInfoOuterCore.WeakModel.PartnerFee* Default `null`.
 * - `[visibleAmount=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.VisibleAmount* ⤴ *ITransactionInfoOuterCore.WeakModel.VisibleAmount* Default `null`.
 * - `[fixed=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.Fixed* ⤴ *ITransactionInfoOuterCore.WeakModel.Fixed* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITransactionInfoElementPort.Inputs.NoSolder* Default `false`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[reloadGetTransactionBuOpts]` _!Object?_ The options to pass to the _ReloadGetTransactionBu_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.ReloadGetTransactionBuOpts* Default `{}`.
 * - `[getTransactionErrorWrOpts]` _!Object?_ The options to pass to the _GetTransactionErrorWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.GetTransactionErrorWrOpts* Default `{}`.
 * - `[getTransactionErrorLaOpts]` _!Object?_ The options to pass to the _GetTransactionErrorLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.GetTransactionErrorLaOpts* Default `{}`.
 * - `[fixedLockOpts]` _!Object?_ The options to pass to the _FixedLock_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.FixedLockOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ITransactionInfoElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TransactionInfoMemory, instance?: !xyz.swapee.wc.ITransactionInfoScreen&xyz.swapee.wc.ITransactionInfoController) => !engineering.type.VNode} xyz.swapee.wc.ITransactionInfoElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoElement.__render<!xyz.swapee.wc.ITransactionInfoElement>} xyz.swapee.wc.ITransactionInfoElement._render */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.TransactionInfoMemory} [model] The model for the view.
 * - `tid` _string_ The transaction ID. Default empty string.
 * - `currencyFrom` _string_ Default empty string.
 * - `currencyTo` _string_ Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `amountTo` _string_ Default empty string.
 * - `networkFee` _string_ Default empty string.
 * - `partnerFee` _string_ Default empty string.
 * - `visibleAmount` _string_ Default empty string.
 * - `fixed` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.ITransactionInfoScreen&xyz.swapee.wc.ITransactionInfoController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITransactionInfoElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TransactionInfoMemory, inputs: !xyz.swapee.wc.ITransactionInfoElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ITransactionInfoElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoElement.__server<!xyz.swapee.wc.ITransactionInfoElement>} xyz.swapee.wc.ITransactionInfoElement._server */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.TransactionInfoMemory} memory The memory registers.
 * - `tid` _string_ The transaction ID. Default empty string.
 * - `currencyFrom` _string_ Default empty string.
 * - `currencyTo` _string_ Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `amountTo` _string_ Default empty string.
 * - `networkFee` _string_ Default empty string.
 * - `partnerFee` _string_ Default empty string.
 * - `visibleAmount` _string_ Default empty string.
 * - `fixed` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.ITransactionInfoElement.Inputs} inputs The inputs to the port.
 * - `[tid=null]` _&#42;?_ The transaction ID. ⤴ *ITransactionInfoOuterCore.WeakModel.Tid* ⤴ *ITransactionInfoOuterCore.WeakModel.Tid* Default `null`.
 * - `[currencyFrom=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyFrom* ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyFrom* Default `null`.
 * - `[currencyTo=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyTo* ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyTo* Default `null`.
 * - `[amountFrom=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.AmountFrom* ⤴ *ITransactionInfoOuterCore.WeakModel.AmountFrom* Default `null`.
 * - `[amountTo=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.AmountTo* ⤴ *ITransactionInfoOuterCore.WeakModel.AmountTo* Default `null`.
 * - `[networkFee=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.NetworkFee* ⤴ *ITransactionInfoOuterCore.WeakModel.NetworkFee* Default `null`.
 * - `[partnerFee=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.PartnerFee* ⤴ *ITransactionInfoOuterCore.WeakModel.PartnerFee* Default `null`.
 * - `[visibleAmount=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.VisibleAmount* ⤴ *ITransactionInfoOuterCore.WeakModel.VisibleAmount* Default `null`.
 * - `[fixed=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.Fixed* ⤴ *ITransactionInfoOuterCore.WeakModel.Fixed* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITransactionInfoElementPort.Inputs.NoSolder* Default `false`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[reloadGetTransactionBuOpts]` _!Object?_ The options to pass to the _ReloadGetTransactionBu_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.ReloadGetTransactionBuOpts* Default `{}`.
 * - `[getTransactionErrorWrOpts]` _!Object?_ The options to pass to the _GetTransactionErrorWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.GetTransactionErrorWrOpts* Default `{}`.
 * - `[getTransactionErrorLaOpts]` _!Object?_ The options to pass to the _GetTransactionErrorLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.GetTransactionErrorLaOpts* Default `{}`.
 * - `[fixedLockOpts]` _!Object?_ The options to pass to the _FixedLock_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.FixedLockOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITransactionInfoElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TransactionInfoMemory, port?: !xyz.swapee.wc.ITransactionInfoElement.Inputs) => ?} xyz.swapee.wc.ITransactionInfoElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoElement.__inducer<!xyz.swapee.wc.ITransactionInfoElement>} xyz.swapee.wc.ITransactionInfoElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.TransactionInfoMemory} [model] The model of the component into which to induce the state.
 * - `tid` _string_ The transaction ID. Default empty string.
 * - `currencyFrom` _string_ Default empty string.
 * - `currencyTo` _string_ Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `amountTo` _string_ Default empty string.
 * - `networkFee` _string_ Default empty string.
 * - `partnerFee` _string_ Default empty string.
 * - `visibleAmount` _string_ Default empty string.
 * - `fixed` _boolean_ Default `false`.
 * @param {!xyz.swapee.wc.ITransactionInfoElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[tid=null]` _&#42;?_ The transaction ID. ⤴ *ITransactionInfoOuterCore.WeakModel.Tid* ⤴ *ITransactionInfoOuterCore.WeakModel.Tid* Default `null`.
 * - `[currencyFrom=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyFrom* ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyFrom* Default `null`.
 * - `[currencyTo=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyTo* ⤴ *ITransactionInfoOuterCore.WeakModel.CurrencyTo* Default `null`.
 * - `[amountFrom=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.AmountFrom* ⤴ *ITransactionInfoOuterCore.WeakModel.AmountFrom* Default `null`.
 * - `[amountTo=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.AmountTo* ⤴ *ITransactionInfoOuterCore.WeakModel.AmountTo* Default `null`.
 * - `[networkFee=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.NetworkFee* ⤴ *ITransactionInfoOuterCore.WeakModel.NetworkFee* Default `null`.
 * - `[partnerFee=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.PartnerFee* ⤴ *ITransactionInfoOuterCore.WeakModel.PartnerFee* Default `null`.
 * - `[visibleAmount=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.VisibleAmount* ⤴ *ITransactionInfoOuterCore.WeakModel.VisibleAmount* Default `null`.
 * - `[fixed=null]` _&#42;?_ ⤴ *ITransactionInfoOuterCore.WeakModel.Fixed* ⤴ *ITransactionInfoOuterCore.WeakModel.Fixed* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITransactionInfoElementPort.Inputs.NoSolder* Default `false`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[reloadGetTransactionBuOpts]` _!Object?_ The options to pass to the _ReloadGetTransactionBu_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.ReloadGetTransactionBuOpts* Default `{}`.
 * - `[getTransactionErrorWrOpts]` _!Object?_ The options to pass to the _GetTransactionErrorWr_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.GetTransactionErrorWrOpts* Default `{}`.
 * - `[getTransactionErrorLaOpts]` _!Object?_ The options to pass to the _GetTransactionErrorLa_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.GetTransactionErrorLaOpts* Default `{}`.
 * - `[fixedLockOpts]` _!Object?_ The options to pass to the _FixedLock_ vdu. ⤴ *ITransactionInfoElementPort.Inputs.FixedLockOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ITransactionInfoElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/140-ITransactionInfoElementPort.xml}  854a3d51d51b97b60464b9028b092267 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITransactionInfoElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoElementPort)} xyz.swapee.wc.AbstractTransactionInfoElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoElementPort} xyz.swapee.wc.TransactionInfoElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoElementPort
 */
xyz.swapee.wc.AbstractTransactionInfoElementPort = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoElementPort.constructor&xyz.swapee.wc.TransactionInfoElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoElementPort.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoElementPort|typeof xyz.swapee.wc.TransactionInfoElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoElementPort|typeof xyz.swapee.wc.TransactionInfoElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoElementPort|typeof xyz.swapee.wc.TransactionInfoElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoElementPort}
 */
xyz.swapee.wc.AbstractTransactionInfoElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoElementPort.Initialese[]) => xyz.swapee.wc.ITransactionInfoElementPort} xyz.swapee.wc.TransactionInfoElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITransactionInfoElementPort.Inputs>)} xyz.swapee.wc.ITransactionInfoElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ITransactionInfoElementPort
 */
xyz.swapee.wc.ITransactionInfoElementPort = class extends /** @type {xyz.swapee.wc.ITransactionInfoElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoElementPort.Initialese>)} xyz.swapee.wc.TransactionInfoElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElementPort} xyz.swapee.wc.ITransactionInfoElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITransactionInfoElementPort_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoElementPort
 * @implements {xyz.swapee.wc.ITransactionInfoElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoElementPort.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoElementPort = class extends /** @type {xyz.swapee.wc.TransactionInfoElementPort.constructor&xyz.swapee.wc.ITransactionInfoElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoElementPort}
 */
xyz.swapee.wc.TransactionInfoElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoElementPort.
 * @interface xyz.swapee.wc.ITransactionInfoElementPortFields
 */
xyz.swapee.wc.ITransactionInfoElementPortFields = class { }
/**
 * The inputs to the _ITransactionInfoElement_'s controller via its element port.
 */
xyz.swapee.wc.ITransactionInfoElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITransactionInfoElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITransactionInfoElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoElementPort} */
xyz.swapee.wc.RecordITransactionInfoElementPort

/** @typedef {xyz.swapee.wc.ITransactionInfoElementPort} xyz.swapee.wc.BoundITransactionInfoElementPort */

/** @typedef {xyz.swapee.wc.TransactionInfoElementPort} xyz.swapee.wc.BoundTransactionInfoElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder)} xyz.swapee.wc.ITransactionInfoElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder} xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _ITransactionInfoElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITransactionInfoElementPort.Inputs
 */
xyz.swapee.wc.ITransactionInfoElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoElementPort.Inputs.constructor&xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _ITransactionInfoElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs
 */
xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.constructor&xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs

/**
 * Contains getters to cast the _ITransactionInfoElementPort_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoElementPortCaster
 */
xyz.swapee.wc.ITransactionInfoElementPortCaster = class { }
/**
 * Cast the _ITransactionInfoElementPort_ instance into the _BoundITransactionInfoElementPort_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoElementPort}
 */
xyz.swapee.wc.ITransactionInfoElementPortCaster.prototype.asITransactionInfoElementPort
/**
 * Access the _TransactionInfoElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoElementPort}
 */
xyz.swapee.wc.ITransactionInfoElementPortCaster.prototype.superTransactionInfoElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/170-ITransactionInfoDesigner.xml}  8373958c88155b01a752988ee7847a2c */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ITransactionInfoDesigner
 */
xyz.swapee.wc.ITransactionInfoDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.TransactionInfoClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITransactionInfo />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TransactionInfoClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITransactionInfo />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TransactionInfoClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITransactionInfoDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `TransactionInfo` _typeof ITransactionInfoController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITransactionInfoDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `TransactionInfo` _typeof ITransactionInfoController_
   * - `This` _typeof ITransactionInfoController_
   * @param {!xyz.swapee.wc.ITransactionInfoDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `TransactionInfo` _!TransactionInfoMemory_
   * - `This` _!TransactionInfoMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TransactionInfoClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TransactionInfoClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ITransactionInfoDesigner.prototype.constructor = xyz.swapee.wc.ITransactionInfoDesigner

/**
 * A concrete class of _ITransactionInfoDesigner_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoDesigner
 * @implements {xyz.swapee.wc.ITransactionInfoDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.TransactionInfoDesigner = class extends xyz.swapee.wc.ITransactionInfoDesigner { }
xyz.swapee.wc.TransactionInfoDesigner.prototype.constructor = xyz.swapee.wc.TransactionInfoDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITransactionInfoController} TransactionInfo
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITransactionInfoController} TransactionInfo
 * @prop {typeof xyz.swapee.wc.ITransactionInfoController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITransactionInfoDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} TransactionInfo
 * @prop {!xyz.swapee.wc.TransactionInfoMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/40-ITransactionInfoDisplay.xml}  da7c0781fec021133d0c9817f4b354be */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITransactionInfoDisplay.Settings>} xyz.swapee.wc.ITransactionInfoDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoDisplay)} xyz.swapee.wc.AbstractTransactionInfoDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoDisplay} xyz.swapee.wc.TransactionInfoDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoDisplay
 */
xyz.swapee.wc.AbstractTransactionInfoDisplay = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoDisplay.constructor&xyz.swapee.wc.TransactionInfoDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoDisplay.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoDisplay|typeof xyz.swapee.wc.TransactionInfoDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoDisplay|typeof xyz.swapee.wc.TransactionInfoDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoDisplay|typeof xyz.swapee.wc.TransactionInfoDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoDisplay}
 */
xyz.swapee.wc.AbstractTransactionInfoDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoDisplay.Initialese[]) => xyz.swapee.wc.ITransactionInfoDisplay} xyz.swapee.wc.TransactionInfoDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.TransactionInfoMemory, !HTMLDivElement, !xyz.swapee.wc.ITransactionInfoDisplay.Settings, xyz.swapee.wc.ITransactionInfoDisplay.Queries, null>)} xyz.swapee.wc.ITransactionInfoDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ITransactionInfo_.
 * @interface xyz.swapee.wc.ITransactionInfoDisplay
 */
xyz.swapee.wc.ITransactionInfoDisplay = class extends /** @type {xyz.swapee.wc.ITransactionInfoDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoDisplay.paint} */
xyz.swapee.wc.ITransactionInfoDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoDisplay.Initialese>)} xyz.swapee.wc.TransactionInfoDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoDisplay} xyz.swapee.wc.ITransactionInfoDisplay.typeof */
/**
 * A concrete class of _ITransactionInfoDisplay_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoDisplay
 * @implements {xyz.swapee.wc.ITransactionInfoDisplay} Display for presenting information from the _ITransactionInfo_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoDisplay.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoDisplay = class extends /** @type {xyz.swapee.wc.TransactionInfoDisplay.constructor&xyz.swapee.wc.ITransactionInfoDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoDisplay}
 */
xyz.swapee.wc.TransactionInfoDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoDisplay.
 * @interface xyz.swapee.wc.ITransactionInfoDisplayFields
 */
xyz.swapee.wc.ITransactionInfoDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ITransactionInfoDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ITransactionInfoDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ITransactionInfoDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ITransactionInfoDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoDisplay} */
xyz.swapee.wc.RecordITransactionInfoDisplay

/** @typedef {xyz.swapee.wc.ITransactionInfoDisplay} xyz.swapee.wc.BoundITransactionInfoDisplay */

/** @typedef {xyz.swapee.wc.TransactionInfoDisplay} xyz.swapee.wc.BoundTransactionInfoDisplay */

/** @typedef {xyz.swapee.wc.ITransactionInfoDisplay.Queries} xyz.swapee.wc.ITransactionInfoDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITransactionInfoDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _ITransactionInfoDisplay_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoDisplayCaster
 */
xyz.swapee.wc.ITransactionInfoDisplayCaster = class { }
/**
 * Cast the _ITransactionInfoDisplay_ instance into the _BoundITransactionInfoDisplay_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoDisplay}
 */
xyz.swapee.wc.ITransactionInfoDisplayCaster.prototype.asITransactionInfoDisplay
/**
 * Cast the _ITransactionInfoDisplay_ instance into the _BoundITransactionInfoScreen_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoScreen}
 */
xyz.swapee.wc.ITransactionInfoDisplayCaster.prototype.asITransactionInfoScreen
/**
 * Access the _TransactionInfoDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoDisplay}
 */
xyz.swapee.wc.ITransactionInfoDisplayCaster.prototype.superTransactionInfoDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TransactionInfoMemory, land: null) => void} xyz.swapee.wc.ITransactionInfoDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoDisplay.__paint<!xyz.swapee.wc.ITransactionInfoDisplay>} xyz.swapee.wc.ITransactionInfoDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TransactionInfoMemory} memory The display data.
 * - `tid` _string_ The transaction ID. Default empty string.
 * - `iconsFolder` _string_ The folder in the docs directory from which icons can be loaded. Default empty string.
 * - `getTransaction` _boolean_ Forces to load transaction. Default `false`.
 * - `currencyFrom` _string_ Default empty string.
 * - `currencyTo` _string_ Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `amountTo` _string_ Default empty string.
 * - `networkFee` _string_ Default empty string.
 * - `partnerFee` _string_ Default empty string.
 * - `rate` _string_ Default empty string.
 * - `visibleAmount` _string_ Default empty string.
 * - `notFound` _boolean_ Default `false`.
 * - `fixed` _?boolean_ Default `null`.
 * - `gettingTransaction` _boolean_ Default `false`.
 * - `getTransactionError` _string_ Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/40-ITransactionInfoDisplayBack.xml}  08b3daf996049c9443fc170e035f297f */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TransactionInfoClasses>} xyz.swapee.wc.back.ITransactionInfoDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoDisplay)} xyz.swapee.wc.back.AbstractTransactionInfoDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoDisplay} xyz.swapee.wc.back.TransactionInfoDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoDisplay
 */
xyz.swapee.wc.back.AbstractTransactionInfoDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoDisplay.constructor&xyz.swapee.wc.back.TransactionInfoDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoDisplay|typeof xyz.swapee.wc.back.TransactionInfoDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoDisplay|typeof xyz.swapee.wc.back.TransactionInfoDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoDisplay|typeof xyz.swapee.wc.back.TransactionInfoDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoDisplay}
 */
xyz.swapee.wc.back.AbstractTransactionInfoDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.TransactionInfoClasses, null>)} xyz.swapee.wc.back.ITransactionInfoDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ITransactionInfoDisplay
 */
xyz.swapee.wc.back.ITransactionInfoDisplay = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ITransactionInfoDisplay.paint} */
xyz.swapee.wc.back.ITransactionInfoDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoDisplay.Initialese>)} xyz.swapee.wc.back.TransactionInfoDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoDisplay} xyz.swapee.wc.back.ITransactionInfoDisplay.typeof */
/**
 * A concrete class of _ITransactionInfoDisplay_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoDisplay
 * @implements {xyz.swapee.wc.back.ITransactionInfoDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoDisplay = class extends /** @type {xyz.swapee.wc.back.TransactionInfoDisplay.constructor&xyz.swapee.wc.back.ITransactionInfoDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.TransactionInfoDisplay.prototype.constructor = xyz.swapee.wc.back.TransactionInfoDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoDisplay}
 */
xyz.swapee.wc.back.TransactionInfoDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoDisplay} */
xyz.swapee.wc.back.RecordITransactionInfoDisplay

/** @typedef {xyz.swapee.wc.back.ITransactionInfoDisplay} xyz.swapee.wc.back.BoundITransactionInfoDisplay */

/** @typedef {xyz.swapee.wc.back.TransactionInfoDisplay} xyz.swapee.wc.back.BoundTransactionInfoDisplay */

/**
 * Contains getters to cast the _ITransactionInfoDisplay_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoDisplayCaster
 */
xyz.swapee.wc.back.ITransactionInfoDisplayCaster = class { }
/**
 * Cast the _ITransactionInfoDisplay_ instance into the _BoundITransactionInfoDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoDisplay}
 */
xyz.swapee.wc.back.ITransactionInfoDisplayCaster.prototype.asITransactionInfoDisplay
/**
 * Access the _TransactionInfoDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoDisplay}
 */
xyz.swapee.wc.back.ITransactionInfoDisplayCaster.prototype.superTransactionInfoDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.TransactionInfoMemory, land?: null) => void} xyz.swapee.wc.back.ITransactionInfoDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoDisplay.__paint<!xyz.swapee.wc.back.ITransactionInfoDisplay>} xyz.swapee.wc.back.ITransactionInfoDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TransactionInfoMemory} [memory] The display data.
 * - `tid` _string_ The transaction ID. Default empty string.
 * - `iconsFolder` _string_ The folder in the docs directory from which icons can be loaded. Default empty string.
 * - `getTransaction` _boolean_ Forces to load transaction. Default `false`.
 * - `currencyFrom` _string_ Default empty string.
 * - `currencyTo` _string_ Default empty string.
 * - `amountFrom` _string_ Default empty string.
 * - `amountTo` _string_ Default empty string.
 * - `networkFee` _string_ Default empty string.
 * - `partnerFee` _string_ Default empty string.
 * - `rate` _string_ Default empty string.
 * - `visibleAmount` _string_ Default empty string.
 * - `notFound` _boolean_ Default `false`.
 * - `fixed` _?boolean_ Default `null`.
 * - `gettingTransaction` _boolean_ Default `false`.
 * - `getTransactionError` _string_ Default empty string.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.ITransactionInfoDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ITransactionInfoDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/41-TransactionInfoClasses.xml}  99a0c0c2dbd7998789413f1b4d0d4def */
/**
 * The classes of the _ITransactionInfoDisplay_.
 * @record xyz.swapee.wc.TransactionInfoClasses
 */
xyz.swapee.wc.TransactionInfoClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.TransactionInfoClasses.prototype.Class = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.TransactionInfoClasses.prototype.props = /** @type {xyz.swapee.wc.TransactionInfoClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/50-ITransactionInfoController.xml}  54ca749458c7ce66d97cc9d5dfab915f */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITransactionInfoController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITransactionInfoController.Inputs, !xyz.swapee.wc.ITransactionInfoOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel>} xyz.swapee.wc.ITransactionInfoController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoController)} xyz.swapee.wc.AbstractTransactionInfoController.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoController} xyz.swapee.wc.TransactionInfoController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoController` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoController
 */
xyz.swapee.wc.AbstractTransactionInfoController = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoController.constructor&xyz.swapee.wc.TransactionInfoController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoController.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoController.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoController}
 */
xyz.swapee.wc.AbstractTransactionInfoController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoController}
 */
xyz.swapee.wc.AbstractTransactionInfoController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoController}
 */
xyz.swapee.wc.AbstractTransactionInfoController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoController}
 */
xyz.swapee.wc.AbstractTransactionInfoController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoController.Initialese[]) => xyz.swapee.wc.ITransactionInfoController} xyz.swapee.wc.TransactionInfoControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ITransactionInfoController.Inputs, !xyz.swapee.wc.ITransactionInfoOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ITransactionInfoOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITransactionInfoController.Inputs, !xyz.swapee.wc.ITransactionInfoController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITransactionInfoController.Inputs, !xyz.swapee.wc.TransactionInfoMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITransactionInfoController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITransactionInfoController.Inputs>)} xyz.swapee.wc.ITransactionInfoController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ITransactionInfoController
 */
xyz.swapee.wc.ITransactionInfoController = class extends /** @type {xyz.swapee.wc.ITransactionInfoController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITransactionInfoController.resetPort} */
xyz.swapee.wc.ITransactionInfoController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ITransactionInfoController.pulseGetTransaction} */
xyz.swapee.wc.ITransactionInfoController.prototype.pulseGetTransaction = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoController&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoController.Initialese>)} xyz.swapee.wc.TransactionInfoController.constructor */
/**
 * A concrete class of _ITransactionInfoController_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoController
 * @implements {xyz.swapee.wc.ITransactionInfoController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoController.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoController = class extends /** @type {xyz.swapee.wc.TransactionInfoController.constructor&xyz.swapee.wc.ITransactionInfoController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoController}
 */
xyz.swapee.wc.TransactionInfoController.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoController.
 * @interface xyz.swapee.wc.ITransactionInfoControllerFields
 */
xyz.swapee.wc.ITransactionInfoControllerFields = class { }
/**
 * The inputs to the _ITransactionInfo_'s controller.
 */
xyz.swapee.wc.ITransactionInfoControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITransactionInfoController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ITransactionInfoControllerFields.prototype.props = /** @type {xyz.swapee.wc.ITransactionInfoController} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoController} */
xyz.swapee.wc.RecordITransactionInfoController

/** @typedef {xyz.swapee.wc.ITransactionInfoController} xyz.swapee.wc.BoundITransactionInfoController */

/** @typedef {xyz.swapee.wc.TransactionInfoController} xyz.swapee.wc.BoundTransactionInfoController */

/** @typedef {xyz.swapee.wc.ITransactionInfoPort.Inputs} xyz.swapee.wc.ITransactionInfoController.Inputs The inputs to the _ITransactionInfo_'s controller. */

/** @typedef {xyz.swapee.wc.ITransactionInfoPort.WeakInputs} xyz.swapee.wc.ITransactionInfoController.WeakInputs The inputs to the _ITransactionInfo_'s controller. */

/**
 * Contains getters to cast the _ITransactionInfoController_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoControllerCaster
 */
xyz.swapee.wc.ITransactionInfoControllerCaster = class { }
/**
 * Cast the _ITransactionInfoController_ instance into the _BoundITransactionInfoController_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoController}
 */
xyz.swapee.wc.ITransactionInfoControllerCaster.prototype.asITransactionInfoController
/**
 * Cast the _ITransactionInfoController_ instance into the _BoundITransactionInfoProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoProcessor}
 */
xyz.swapee.wc.ITransactionInfoControllerCaster.prototype.asITransactionInfoProcessor
/**
 * Access the _TransactionInfoController_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoController}
 */
xyz.swapee.wc.ITransactionInfoControllerCaster.prototype.superTransactionInfoController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoController.__resetPort<!xyz.swapee.wc.ITransactionInfoController>} xyz.swapee.wc.ITransactionInfoController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITransactionInfoController.__pulseGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITransactionInfoController.__pulseGetTransaction<!xyz.swapee.wc.ITransactionInfoController>} xyz.swapee.wc.ITransactionInfoController._pulseGetTransaction */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoController.pulseGetTransaction} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.ITransactionInfoController.pulseGetTransaction = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITransactionInfoController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/51-ITransactionInfoControllerFront.xml}  46fede38cde9d87c358d4e0d374bcd64 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ITransactionInfoController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TransactionInfoController)} xyz.swapee.wc.front.AbstractTransactionInfoController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TransactionInfoController} xyz.swapee.wc.front.TransactionInfoController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITransactionInfoController` interface.
 * @constructor xyz.swapee.wc.front.AbstractTransactionInfoController
 */
xyz.swapee.wc.front.AbstractTransactionInfoController = class extends /** @type {xyz.swapee.wc.front.AbstractTransactionInfoController.constructor&xyz.swapee.wc.front.TransactionInfoController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTransactionInfoController.prototype.constructor = xyz.swapee.wc.front.AbstractTransactionInfoController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTransactionInfoController.class = /** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoController|typeof xyz.swapee.wc.front.TransactionInfoController)|(!xyz.swapee.wc.front.ITransactionInfoControllerAT|typeof xyz.swapee.wc.front.TransactionInfoControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTransactionInfoController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTransactionInfoController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoController|typeof xyz.swapee.wc.front.TransactionInfoController)|(!xyz.swapee.wc.front.ITransactionInfoControllerAT|typeof xyz.swapee.wc.front.TransactionInfoControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoController|typeof xyz.swapee.wc.front.TransactionInfoController)|(!xyz.swapee.wc.front.ITransactionInfoControllerAT|typeof xyz.swapee.wc.front.TransactionInfoControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoController}
 */
xyz.swapee.wc.front.AbstractTransactionInfoController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITransactionInfoController.Initialese[]) => xyz.swapee.wc.front.ITransactionInfoController} xyz.swapee.wc.front.TransactionInfoControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITransactionInfoControllerCaster&xyz.swapee.wc.front.ITransactionInfoControllerAT)} xyz.swapee.wc.front.ITransactionInfoController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITransactionInfoControllerAT} xyz.swapee.wc.front.ITransactionInfoControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ITransactionInfoController
 */
xyz.swapee.wc.front.ITransactionInfoController = class extends /** @type {xyz.swapee.wc.front.ITransactionInfoController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ITransactionInfoControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITransactionInfoController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.ITransactionInfoController.pulseGetTransaction} */
xyz.swapee.wc.front.ITransactionInfoController.prototype.pulseGetTransaction = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.ITransactionInfoController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoController.Initialese>)} xyz.swapee.wc.front.TransactionInfoController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITransactionInfoController} xyz.swapee.wc.front.ITransactionInfoController.typeof */
/**
 * A concrete class of _ITransactionInfoController_ instances.
 * @constructor xyz.swapee.wc.front.TransactionInfoController
 * @implements {xyz.swapee.wc.front.ITransactionInfoController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoController.Initialese>} ‎
 */
xyz.swapee.wc.front.TransactionInfoController = class extends /** @type {xyz.swapee.wc.front.TransactionInfoController.constructor&xyz.swapee.wc.front.ITransactionInfoController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITransactionInfoController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TransactionInfoController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TransactionInfoController}
 */
xyz.swapee.wc.front.TransactionInfoController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITransactionInfoController} */
xyz.swapee.wc.front.RecordITransactionInfoController

/** @typedef {xyz.swapee.wc.front.ITransactionInfoController} xyz.swapee.wc.front.BoundITransactionInfoController */

/** @typedef {xyz.swapee.wc.front.TransactionInfoController} xyz.swapee.wc.front.BoundTransactionInfoController */

/**
 * Contains getters to cast the _ITransactionInfoController_ interface.
 * @interface xyz.swapee.wc.front.ITransactionInfoControllerCaster
 */
xyz.swapee.wc.front.ITransactionInfoControllerCaster = class { }
/**
 * Cast the _ITransactionInfoController_ instance into the _BoundITransactionInfoController_ type.
 * @type {!xyz.swapee.wc.front.BoundITransactionInfoController}
 */
xyz.swapee.wc.front.ITransactionInfoControllerCaster.prototype.asITransactionInfoController
/**
 * Access the _TransactionInfoController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTransactionInfoController}
 */
xyz.swapee.wc.front.ITransactionInfoControllerCaster.prototype.superTransactionInfoController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ITransactionInfoController.__pulseGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITransactionInfoController.__pulseGetTransaction<!xyz.swapee.wc.front.ITransactionInfoController>} xyz.swapee.wc.front.ITransactionInfoController._pulseGetTransaction */
/** @typedef {typeof xyz.swapee.wc.front.ITransactionInfoController.pulseGetTransaction} */
/** @return {void} */
xyz.swapee.wc.front.ITransactionInfoController.pulseGetTransaction = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.ITransactionInfoController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/52-ITransactionInfoControllerBack.xml}  36e5cd0413de06ab0eacb2045c71d606 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITransactionInfoController.Inputs>&xyz.swapee.wc.ITransactionInfoController.Initialese} xyz.swapee.wc.back.ITransactionInfoController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoController)} xyz.swapee.wc.back.AbstractTransactionInfoController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoController} xyz.swapee.wc.back.TransactionInfoController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoController` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoController
 */
xyz.swapee.wc.back.AbstractTransactionInfoController = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoController.constructor&xyz.swapee.wc.back.TransactionInfoController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoController.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoController.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoController|typeof xyz.swapee.wc.back.TransactionInfoController)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoController|typeof xyz.swapee.wc.back.TransactionInfoController)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoController|typeof xyz.swapee.wc.back.TransactionInfoController)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoController}
 */
xyz.swapee.wc.back.AbstractTransactionInfoController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoController.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoController} xyz.swapee.wc.back.TransactionInfoControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoControllerCaster&xyz.swapee.wc.ITransactionInfoController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ITransactionInfoController.Inputs>)} xyz.swapee.wc.back.ITransactionInfoController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ITransactionInfoController
 */
xyz.swapee.wc.back.ITransactionInfoController = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITransactionInfoController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoController.Initialese>)} xyz.swapee.wc.back.TransactionInfoController.constructor */
/**
 * A concrete class of _ITransactionInfoController_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoController
 * @implements {xyz.swapee.wc.back.ITransactionInfoController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoController.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoController = class extends /** @type {xyz.swapee.wc.back.TransactionInfoController.constructor&xyz.swapee.wc.back.ITransactionInfoController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoController}
 */
xyz.swapee.wc.back.TransactionInfoController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoController} */
xyz.swapee.wc.back.RecordITransactionInfoController

/** @typedef {xyz.swapee.wc.back.ITransactionInfoController} xyz.swapee.wc.back.BoundITransactionInfoController */

/** @typedef {xyz.swapee.wc.back.TransactionInfoController} xyz.swapee.wc.back.BoundTransactionInfoController */

/**
 * Contains getters to cast the _ITransactionInfoController_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoControllerCaster
 */
xyz.swapee.wc.back.ITransactionInfoControllerCaster = class { }
/**
 * Cast the _ITransactionInfoController_ instance into the _BoundITransactionInfoController_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoController}
 */
xyz.swapee.wc.back.ITransactionInfoControllerCaster.prototype.asITransactionInfoController
/**
 * Access the _TransactionInfoController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoController}
 */
xyz.swapee.wc.back.ITransactionInfoControllerCaster.prototype.superTransactionInfoController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/53-ITransactionInfoControllerAR.xml}  65f4071de05376c499eccabe011ac6d8 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITransactionInfoController.Initialese} xyz.swapee.wc.back.ITransactionInfoControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoControllerAR)} xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoControllerAR} xyz.swapee.wc.back.TransactionInfoControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoControllerAR
 */
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.constructor&xyz.swapee.wc.back.TransactionInfoControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoControllerAR|typeof xyz.swapee.wc.back.TransactionInfoControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoControllerAR|typeof xyz.swapee.wc.back.TransactionInfoControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoControllerAR|typeof xyz.swapee.wc.back.TransactionInfoControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoController|typeof xyz.swapee.wc.TransactionInfoController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoControllerAR}
 */
xyz.swapee.wc.back.AbstractTransactionInfoControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoControllerAR.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoControllerAR} xyz.swapee.wc.back.TransactionInfoControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITransactionInfoController)} xyz.swapee.wc.back.ITransactionInfoControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITransactionInfoControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ITransactionInfoControllerAR
 */
xyz.swapee.wc.back.ITransactionInfoControllerAR = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITransactionInfoController.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoControllerAR.Initialese>)} xyz.swapee.wc.back.TransactionInfoControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoControllerAR} xyz.swapee.wc.back.ITransactionInfoControllerAR.typeof */
/**
 * A concrete class of _ITransactionInfoControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoControllerAR
 * @implements {xyz.swapee.wc.back.ITransactionInfoControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ITransactionInfoControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoControllerAR = class extends /** @type {xyz.swapee.wc.back.TransactionInfoControllerAR.constructor&xyz.swapee.wc.back.ITransactionInfoControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoControllerAR}
 */
xyz.swapee.wc.back.TransactionInfoControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoControllerAR} */
xyz.swapee.wc.back.RecordITransactionInfoControllerAR

/** @typedef {xyz.swapee.wc.back.ITransactionInfoControllerAR} xyz.swapee.wc.back.BoundITransactionInfoControllerAR */

/** @typedef {xyz.swapee.wc.back.TransactionInfoControllerAR} xyz.swapee.wc.back.BoundTransactionInfoControllerAR */

/**
 * Contains getters to cast the _ITransactionInfoControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoControllerARCaster
 */
xyz.swapee.wc.back.ITransactionInfoControllerARCaster = class { }
/**
 * Cast the _ITransactionInfoControllerAR_ instance into the _BoundITransactionInfoControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoControllerAR}
 */
xyz.swapee.wc.back.ITransactionInfoControllerARCaster.prototype.asITransactionInfoControllerAR
/**
 * Access the _TransactionInfoControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoControllerAR}
 */
xyz.swapee.wc.back.ITransactionInfoControllerARCaster.prototype.superTransactionInfoControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/54-ITransactionInfoControllerAT.xml}  9fe1a1fb48a0595444d61e5483a831fb */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ITransactionInfoControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TransactionInfoControllerAT)} xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TransactionInfoControllerAT} xyz.swapee.wc.front.TransactionInfoControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITransactionInfoControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractTransactionInfoControllerAT
 */
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.constructor&xyz.swapee.wc.front.TransactionInfoControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractTransactionInfoControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoControllerAT|typeof xyz.swapee.wc.front.TransactionInfoControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTransactionInfoControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoControllerAT|typeof xyz.swapee.wc.front.TransactionInfoControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoControllerAT|typeof xyz.swapee.wc.front.TransactionInfoControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoControllerAT}
 */
xyz.swapee.wc.front.AbstractTransactionInfoControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITransactionInfoControllerAT.Initialese[]) => xyz.swapee.wc.front.ITransactionInfoControllerAT} xyz.swapee.wc.front.TransactionInfoControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITransactionInfoControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ITransactionInfoControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITransactionInfoControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ITransactionInfoControllerAT
 */
xyz.swapee.wc.front.ITransactionInfoControllerAT = class extends /** @type {xyz.swapee.wc.front.ITransactionInfoControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITransactionInfoControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITransactionInfoControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoControllerAT.Initialese>)} xyz.swapee.wc.front.TransactionInfoControllerAT.constructor */
/**
 * A concrete class of _ITransactionInfoControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.TransactionInfoControllerAT
 * @implements {xyz.swapee.wc.front.ITransactionInfoControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITransactionInfoControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.TransactionInfoControllerAT = class extends /** @type {xyz.swapee.wc.front.TransactionInfoControllerAT.constructor&xyz.swapee.wc.front.ITransactionInfoControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITransactionInfoControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TransactionInfoControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TransactionInfoControllerAT}
 */
xyz.swapee.wc.front.TransactionInfoControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITransactionInfoControllerAT} */
xyz.swapee.wc.front.RecordITransactionInfoControllerAT

/** @typedef {xyz.swapee.wc.front.ITransactionInfoControllerAT} xyz.swapee.wc.front.BoundITransactionInfoControllerAT */

/** @typedef {xyz.swapee.wc.front.TransactionInfoControllerAT} xyz.swapee.wc.front.BoundTransactionInfoControllerAT */

/**
 * Contains getters to cast the _ITransactionInfoControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ITransactionInfoControllerATCaster
 */
xyz.swapee.wc.front.ITransactionInfoControllerATCaster = class { }
/**
 * Cast the _ITransactionInfoControllerAT_ instance into the _BoundITransactionInfoControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundITransactionInfoControllerAT}
 */
xyz.swapee.wc.front.ITransactionInfoControllerATCaster.prototype.asITransactionInfoControllerAT
/**
 * Access the _TransactionInfoControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTransactionInfoControllerAT}
 */
xyz.swapee.wc.front.ITransactionInfoControllerATCaster.prototype.superTransactionInfoControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/70-ITransactionInfoScreen.xml}  52295e93e76395af1995f7301b0e936a */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.front.TransactionInfoInputs, !HTMLDivElement, !xyz.swapee.wc.ITransactionInfoDisplay.Settings, !xyz.swapee.wc.ITransactionInfoDisplay.Queries, null>&xyz.swapee.wc.ITransactionInfoDisplay.Initialese} xyz.swapee.wc.ITransactionInfoScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoScreen)} xyz.swapee.wc.AbstractTransactionInfoScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoScreen} xyz.swapee.wc.TransactionInfoScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoScreen` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoScreen
 */
xyz.swapee.wc.AbstractTransactionInfoScreen = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoScreen.constructor&xyz.swapee.wc.TransactionInfoScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoScreen.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoScreen.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoScreen|typeof xyz.swapee.wc.TransactionInfoScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITransactionInfoController|typeof xyz.swapee.wc.front.TransactionInfoController)|(!xyz.swapee.wc.ITransactionInfoDisplay|typeof xyz.swapee.wc.TransactionInfoDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoScreen|typeof xyz.swapee.wc.TransactionInfoScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITransactionInfoController|typeof xyz.swapee.wc.front.TransactionInfoController)|(!xyz.swapee.wc.ITransactionInfoDisplay|typeof xyz.swapee.wc.TransactionInfoDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoScreen|typeof xyz.swapee.wc.TransactionInfoScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITransactionInfoController|typeof xyz.swapee.wc.front.TransactionInfoController)|(!xyz.swapee.wc.ITransactionInfoDisplay|typeof xyz.swapee.wc.TransactionInfoDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoScreen}
 */
xyz.swapee.wc.AbstractTransactionInfoScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoScreen.Initialese[]) => xyz.swapee.wc.ITransactionInfoScreen} xyz.swapee.wc.TransactionInfoScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.TransactionInfoMemory, !xyz.swapee.wc.front.TransactionInfoInputs, !HTMLDivElement, !xyz.swapee.wc.ITransactionInfoDisplay.Settings, !xyz.swapee.wc.ITransactionInfoDisplay.Queries, null, null>&xyz.swapee.wc.front.ITransactionInfoController&xyz.swapee.wc.ITransactionInfoDisplay)} xyz.swapee.wc.ITransactionInfoScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ITransactionInfoScreen
 */
xyz.swapee.wc.ITransactionInfoScreen = class extends /** @type {xyz.swapee.wc.ITransactionInfoScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ITransactionInfoController.typeof&xyz.swapee.wc.ITransactionInfoDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoScreen.Initialese>)} xyz.swapee.wc.TransactionInfoScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ITransactionInfoScreen} xyz.swapee.wc.ITransactionInfoScreen.typeof */
/**
 * A concrete class of _ITransactionInfoScreen_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoScreen
 * @implements {xyz.swapee.wc.ITransactionInfoScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoScreen.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoScreen = class extends /** @type {xyz.swapee.wc.TransactionInfoScreen.constructor&xyz.swapee.wc.ITransactionInfoScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoScreen}
 */
xyz.swapee.wc.TransactionInfoScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITransactionInfoScreen} */
xyz.swapee.wc.RecordITransactionInfoScreen

/** @typedef {xyz.swapee.wc.ITransactionInfoScreen} xyz.swapee.wc.BoundITransactionInfoScreen */

/** @typedef {xyz.swapee.wc.TransactionInfoScreen} xyz.swapee.wc.BoundTransactionInfoScreen */

/**
 * Contains getters to cast the _ITransactionInfoScreen_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoScreenCaster
 */
xyz.swapee.wc.ITransactionInfoScreenCaster = class { }
/**
 * Cast the _ITransactionInfoScreen_ instance into the _BoundITransactionInfoScreen_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoScreen}
 */
xyz.swapee.wc.ITransactionInfoScreenCaster.prototype.asITransactionInfoScreen
/**
 * Access the _TransactionInfoScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoScreen}
 */
xyz.swapee.wc.ITransactionInfoScreenCaster.prototype.superTransactionInfoScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/70-ITransactionInfoScreenBack.xml}  60370dc750c59bf67e6445b9e4a20f12 */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese} xyz.swapee.wc.back.ITransactionInfoScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoScreen)} xyz.swapee.wc.back.AbstractTransactionInfoScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoScreen} xyz.swapee.wc.back.TransactionInfoScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoScreen
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreen = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoScreen.constructor&xyz.swapee.wc.back.TransactionInfoScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoScreen.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoScreen|typeof xyz.swapee.wc.back.TransactionInfoScreen)|(!xyz.swapee.wc.back.ITransactionInfoScreenAT|typeof xyz.swapee.wc.back.TransactionInfoScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoScreen|typeof xyz.swapee.wc.back.TransactionInfoScreen)|(!xyz.swapee.wc.back.ITransactionInfoScreenAT|typeof xyz.swapee.wc.back.TransactionInfoScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoScreen|typeof xyz.swapee.wc.back.TransactionInfoScreen)|(!xyz.swapee.wc.back.ITransactionInfoScreenAT|typeof xyz.swapee.wc.back.TransactionInfoScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreen}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoScreen.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoScreen} xyz.swapee.wc.back.TransactionInfoScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoScreenCaster&xyz.swapee.wc.back.ITransactionInfoScreenAT)} xyz.swapee.wc.back.ITransactionInfoScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITransactionInfoScreenAT} xyz.swapee.wc.back.ITransactionInfoScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ITransactionInfoScreen
 */
xyz.swapee.wc.back.ITransactionInfoScreen = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITransactionInfoScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoScreen.Initialese>)} xyz.swapee.wc.back.TransactionInfoScreen.constructor */
/**
 * A concrete class of _ITransactionInfoScreen_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoScreen
 * @implements {xyz.swapee.wc.back.ITransactionInfoScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoScreen = class extends /** @type {xyz.swapee.wc.back.TransactionInfoScreen.constructor&xyz.swapee.wc.back.ITransactionInfoScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreen}
 */
xyz.swapee.wc.back.TransactionInfoScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoScreen} */
xyz.swapee.wc.back.RecordITransactionInfoScreen

/** @typedef {xyz.swapee.wc.back.ITransactionInfoScreen} xyz.swapee.wc.back.BoundITransactionInfoScreen */

/** @typedef {xyz.swapee.wc.back.TransactionInfoScreen} xyz.swapee.wc.back.BoundTransactionInfoScreen */

/**
 * Contains getters to cast the _ITransactionInfoScreen_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoScreenCaster
 */
xyz.swapee.wc.back.ITransactionInfoScreenCaster = class { }
/**
 * Cast the _ITransactionInfoScreen_ instance into the _BoundITransactionInfoScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoScreen}
 */
xyz.swapee.wc.back.ITransactionInfoScreenCaster.prototype.asITransactionInfoScreen
/**
 * Access the _TransactionInfoScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoScreen}
 */
xyz.swapee.wc.back.ITransactionInfoScreenCaster.prototype.superTransactionInfoScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/73-ITransactionInfoScreenAR.xml}  c08c21a24eaae5fb782b176ad4cf5318 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITransactionInfoScreen.Initialese} xyz.swapee.wc.front.ITransactionInfoScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TransactionInfoScreenAR)} xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TransactionInfoScreenAR} xyz.swapee.wc.front.TransactionInfoScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITransactionInfoScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractTransactionInfoScreenAR
 */
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.constructor&xyz.swapee.wc.front.TransactionInfoScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractTransactionInfoScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoScreenAR|typeof xyz.swapee.wc.front.TransactionInfoScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoScreen|typeof xyz.swapee.wc.TransactionInfoScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTransactionInfoScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoScreenAR|typeof xyz.swapee.wc.front.TransactionInfoScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoScreen|typeof xyz.swapee.wc.TransactionInfoScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITransactionInfoScreenAR|typeof xyz.swapee.wc.front.TransactionInfoScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITransactionInfoScreen|typeof xyz.swapee.wc.TransactionInfoScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TransactionInfoScreenAR}
 */
xyz.swapee.wc.front.AbstractTransactionInfoScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITransactionInfoScreenAR.Initialese[]) => xyz.swapee.wc.front.ITransactionInfoScreenAR} xyz.swapee.wc.front.TransactionInfoScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITransactionInfoScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITransactionInfoScreen)} xyz.swapee.wc.front.ITransactionInfoScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITransactionInfoScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ITransactionInfoScreenAR
 */
xyz.swapee.wc.front.ITransactionInfoScreenAR = class extends /** @type {xyz.swapee.wc.front.ITransactionInfoScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITransactionInfoScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITransactionInfoScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITransactionInfoScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoScreenAR.Initialese>)} xyz.swapee.wc.front.TransactionInfoScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITransactionInfoScreenAR} xyz.swapee.wc.front.ITransactionInfoScreenAR.typeof */
/**
 * A concrete class of _ITransactionInfoScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.TransactionInfoScreenAR
 * @implements {xyz.swapee.wc.front.ITransactionInfoScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ITransactionInfoScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITransactionInfoScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.TransactionInfoScreenAR = class extends /** @type {xyz.swapee.wc.front.TransactionInfoScreenAR.constructor&xyz.swapee.wc.front.ITransactionInfoScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITransactionInfoScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITransactionInfoScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TransactionInfoScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TransactionInfoScreenAR}
 */
xyz.swapee.wc.front.TransactionInfoScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITransactionInfoScreenAR} */
xyz.swapee.wc.front.RecordITransactionInfoScreenAR

/** @typedef {xyz.swapee.wc.front.ITransactionInfoScreenAR} xyz.swapee.wc.front.BoundITransactionInfoScreenAR */

/** @typedef {xyz.swapee.wc.front.TransactionInfoScreenAR} xyz.swapee.wc.front.BoundTransactionInfoScreenAR */

/**
 * Contains getters to cast the _ITransactionInfoScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ITransactionInfoScreenARCaster
 */
xyz.swapee.wc.front.ITransactionInfoScreenARCaster = class { }
/**
 * Cast the _ITransactionInfoScreenAR_ instance into the _BoundITransactionInfoScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundITransactionInfoScreenAR}
 */
xyz.swapee.wc.front.ITransactionInfoScreenARCaster.prototype.asITransactionInfoScreenAR
/**
 * Access the _TransactionInfoScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTransactionInfoScreenAR}
 */
xyz.swapee.wc.front.ITransactionInfoScreenARCaster.prototype.superTransactionInfoScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/74-ITransactionInfoScreenAT.xml}  fa890f2edee43dcd695fde64b2856bea */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TransactionInfoScreenAT)} xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TransactionInfoScreenAT} xyz.swapee.wc.back.TransactionInfoScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITransactionInfoScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractTransactionInfoScreenAT
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.constructor&xyz.swapee.wc.back.TransactionInfoScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractTransactionInfoScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoScreenAT|typeof xyz.swapee.wc.back.TransactionInfoScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoScreenAT|typeof xyz.swapee.wc.back.TransactionInfoScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITransactionInfoScreenAT|typeof xyz.swapee.wc.back.TransactionInfoScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreenAT}
 */
xyz.swapee.wc.back.AbstractTransactionInfoScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese[]) => xyz.swapee.wc.back.ITransactionInfoScreenAT} xyz.swapee.wc.back.TransactionInfoScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITransactionInfoScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ITransactionInfoScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITransactionInfoScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ITransactionInfoScreenAT
 */
xyz.swapee.wc.back.ITransactionInfoScreenAT = class extends /** @type {xyz.swapee.wc.back.ITransactionInfoScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITransactionInfoScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITransactionInfoScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese>)} xyz.swapee.wc.back.TransactionInfoScreenAT.constructor */
/**
 * A concrete class of _ITransactionInfoScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.TransactionInfoScreenAT
 * @implements {xyz.swapee.wc.back.ITransactionInfoScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITransactionInfoScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.TransactionInfoScreenAT = class extends /** @type {xyz.swapee.wc.back.TransactionInfoScreenAT.constructor&xyz.swapee.wc.back.ITransactionInfoScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITransactionInfoScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TransactionInfoScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TransactionInfoScreenAT}
 */
xyz.swapee.wc.back.TransactionInfoScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITransactionInfoScreenAT} */
xyz.swapee.wc.back.RecordITransactionInfoScreenAT

/** @typedef {xyz.swapee.wc.back.ITransactionInfoScreenAT} xyz.swapee.wc.back.BoundITransactionInfoScreenAT */

/** @typedef {xyz.swapee.wc.back.TransactionInfoScreenAT} xyz.swapee.wc.back.BoundTransactionInfoScreenAT */

/**
 * Contains getters to cast the _ITransactionInfoScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ITransactionInfoScreenATCaster
 */
xyz.swapee.wc.back.ITransactionInfoScreenATCaster = class { }
/**
 * Cast the _ITransactionInfoScreenAT_ instance into the _BoundITransactionInfoScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundITransactionInfoScreenAT}
 */
xyz.swapee.wc.back.ITransactionInfoScreenATCaster.prototype.asITransactionInfoScreenAT
/**
 * Access the _TransactionInfoScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTransactionInfoScreenAT}
 */
xyz.swapee.wc.back.ITransactionInfoScreenATCaster.prototype.superTransactionInfoScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/transaction-info/TransactionInfo.mvc/design/80-ITransactionInfoGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ITransactionInfoDisplay.Initialese} xyz.swapee.wc.ITransactionInfoGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TransactionInfoGPU)} xyz.swapee.wc.AbstractTransactionInfoGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.TransactionInfoGPU} xyz.swapee.wc.TransactionInfoGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoGPU` interface.
 * @constructor xyz.swapee.wc.AbstractTransactionInfoGPU
 */
xyz.swapee.wc.AbstractTransactionInfoGPU = class extends /** @type {xyz.swapee.wc.AbstractTransactionInfoGPU.constructor&xyz.swapee.wc.TransactionInfoGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTransactionInfoGPU.prototype.constructor = xyz.swapee.wc.AbstractTransactionInfoGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTransactionInfoGPU.class = /** @type {typeof xyz.swapee.wc.AbstractTransactionInfoGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITransactionInfoGPU|typeof xyz.swapee.wc.TransactionInfoGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITransactionInfoDisplay|typeof xyz.swapee.wc.back.TransactionInfoDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTransactionInfoGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTransactionInfoGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITransactionInfoGPU|typeof xyz.swapee.wc.TransactionInfoGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITransactionInfoDisplay|typeof xyz.swapee.wc.back.TransactionInfoDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITransactionInfoGPU|typeof xyz.swapee.wc.TransactionInfoGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITransactionInfoDisplay|typeof xyz.swapee.wc.back.TransactionInfoDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TransactionInfoGPU}
 */
xyz.swapee.wc.AbstractTransactionInfoGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITransactionInfoGPU.Initialese[]) => xyz.swapee.wc.ITransactionInfoGPU} xyz.swapee.wc.TransactionInfoGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ITransactionInfoGPUCaster&com.webcircuits.IBrowserView<.!TransactionInfoMemory,>&xyz.swapee.wc.back.ITransactionInfoDisplay)} xyz.swapee.wc.ITransactionInfoGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!TransactionInfoMemory,>} com.webcircuits.IBrowserView<.!TransactionInfoMemory,>.typeof */
/**
 * Handles the periphery of the _ITransactionInfoDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ITransactionInfoGPU
 */
xyz.swapee.wc.ITransactionInfoGPU = class extends /** @type {xyz.swapee.wc.ITransactionInfoGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!TransactionInfoMemory,>.typeof&xyz.swapee.wc.back.ITransactionInfoDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITransactionInfoGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITransactionInfoGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITransactionInfoGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoGPU.Initialese>)} xyz.swapee.wc.TransactionInfoGPU.constructor */
/**
 * A concrete class of _ITransactionInfoGPU_ instances.
 * @constructor xyz.swapee.wc.TransactionInfoGPU
 * @implements {xyz.swapee.wc.ITransactionInfoGPU} Handles the periphery of the _ITransactionInfoDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITransactionInfoGPU.Initialese>} ‎
 */
xyz.swapee.wc.TransactionInfoGPU = class extends /** @type {xyz.swapee.wc.TransactionInfoGPU.constructor&xyz.swapee.wc.ITransactionInfoGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITransactionInfoGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITransactionInfoGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITransactionInfoGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITransactionInfoGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TransactionInfoGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TransactionInfoGPU}
 */
xyz.swapee.wc.TransactionInfoGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ITransactionInfoGPU.
 * @interface xyz.swapee.wc.ITransactionInfoGPUFields
 */
xyz.swapee.wc.ITransactionInfoGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ITransactionInfoGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ITransactionInfoGPU} */
xyz.swapee.wc.RecordITransactionInfoGPU

/** @typedef {xyz.swapee.wc.ITransactionInfoGPU} xyz.swapee.wc.BoundITransactionInfoGPU */

/** @typedef {xyz.swapee.wc.TransactionInfoGPU} xyz.swapee.wc.BoundTransactionInfoGPU */

/**
 * Contains getters to cast the _ITransactionInfoGPU_ interface.
 * @interface xyz.swapee.wc.ITransactionInfoGPUCaster
 */
xyz.swapee.wc.ITransactionInfoGPUCaster = class { }
/**
 * Cast the _ITransactionInfoGPU_ instance into the _BoundITransactionInfoGPU_ type.
 * @type {!xyz.swapee.wc.BoundITransactionInfoGPU}
 */
xyz.swapee.wc.ITransactionInfoGPUCaster.prototype.asITransactionInfoGPU
/**
 * Access the _TransactionInfoGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundTransactionInfoGPU}
 */
xyz.swapee.wc.ITransactionInfoGPUCaster.prototype.superTransactionInfoGPU

// nss:xyz.swapee.wc
/* @typal-end */