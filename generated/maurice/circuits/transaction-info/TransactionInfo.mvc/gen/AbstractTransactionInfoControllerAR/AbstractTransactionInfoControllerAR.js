import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoControllerAR}
 */
function __AbstractTransactionInfoControllerAR() {}
__AbstractTransactionInfoControllerAR.prototype = /** @type {!_AbstractTransactionInfoControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoControllerAR}
 */
class _AbstractTransactionInfoControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITransactionInfoControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoControllerAR} ‎
 */
class AbstractTransactionInfoControllerAR extends newAbstract(
 _AbstractTransactionInfoControllerAR,198805181923,null,{
  asITransactionInfoControllerAR:1,
  superTransactionInfoControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoControllerAR} */
AbstractTransactionInfoControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoControllerAR} */
function AbstractTransactionInfoControllerARClass(){}

export default AbstractTransactionInfoControllerAR


AbstractTransactionInfoControllerAR[$implementations]=[
 __AbstractTransactionInfoControllerAR,
 AR,
 AbstractTransactionInfoControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ITransactionInfoControllerAR}*/({
  allocator(){
   this.methods={
    pulseGetTransaction:'f303b',
   }
  },
 }),
]