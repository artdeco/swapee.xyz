import TransactionInfoBuffer from '../TransactionInfoBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {TransactionInfoPortConnector} from '../TransactionInfoPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoController}
 */
function __AbstractTransactionInfoController() {}
__AbstractTransactionInfoController.prototype = /** @type {!_AbstractTransactionInfoController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoController}
 */
class _AbstractTransactionInfoController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoController} ‎
 */
export class AbstractTransactionInfoController extends newAbstract(
 _AbstractTransactionInfoController,198805181920,null,{
  asITransactionInfoController:1,
  superTransactionInfoController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoController} */
AbstractTransactionInfoController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoController} */
function AbstractTransactionInfoControllerClass(){}


AbstractTransactionInfoController[$implementations]=[
 AbstractTransactionInfoControllerClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.ITransactionInfoPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractTransactionInfoController,
 /**@type {!xyz.swapee.wc.ITransactionInfoController}*/({
  calibrate: /**@this {!xyz.swapee.wc.ITransactionInfoController}*/ function calibrateGetTransaction({getTransaction:getTransaction}){
   if(!getTransaction) return
   setTimeout(()=>{
    const{asITransactionInfoController:{setInputs:setInputs}}=this
    setInputs({
     getTransaction:false,
    })
   },1)
  },
 }),
 TransactionInfoBuffer,
 IntegratedController,
 /**@type {!AbstractTransactionInfoController}*/(TransactionInfoPortConnector),
]


export default AbstractTransactionInfoController