import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoControllerAT}
 */
function __AbstractTransactionInfoControllerAT() {}
__AbstractTransactionInfoControllerAT.prototype = /** @type {!_AbstractTransactionInfoControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoControllerAT}
 */
class _AbstractTransactionInfoControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITransactionInfoControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoControllerAT} ‎
 */
class AbstractTransactionInfoControllerAT extends newAbstract(
 _AbstractTransactionInfoControllerAT,198805181924,null,{
  asITransactionInfoControllerAT:1,
  superTransactionInfoControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoControllerAT} */
AbstractTransactionInfoControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoControllerAT} */
function AbstractTransactionInfoControllerATClass(){}

export default AbstractTransactionInfoControllerAT


AbstractTransactionInfoControllerAT[$implementations]=[
 __AbstractTransactionInfoControllerAT,
 UartUniversal,
 AbstractTransactionInfoControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ITransactionInfoControllerAT}*/({
  get asITransactionInfoController(){
   return this
  },
  pulseGetTransaction(){
   this.uart.t("inv",{mid:'f303b'})
  },
 }),
]