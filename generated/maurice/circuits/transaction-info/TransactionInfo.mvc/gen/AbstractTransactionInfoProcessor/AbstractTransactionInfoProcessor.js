import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoProcessor}
 */
function __AbstractTransactionInfoProcessor() {}
__AbstractTransactionInfoProcessor.prototype = /** @type {!_AbstractTransactionInfoProcessor} */ ({
  /** @return {void} */
  pulseGetTransaction() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     getTransaction:true,
    })
  },
})
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoProcessor}
 */
class _AbstractTransactionInfoProcessor { }
/**
 * The processor to compute changes to the memory for the _ITransactionInfo_.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoProcessor} ‎
 */
class AbstractTransactionInfoProcessor extends newAbstract(
 _AbstractTransactionInfoProcessor,19880518198,null,{
  asITransactionInfoProcessor:1,
  superTransactionInfoProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoProcessor} */
AbstractTransactionInfoProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoProcessor} */
function AbstractTransactionInfoProcessorClass(){}

export default AbstractTransactionInfoProcessor


AbstractTransactionInfoProcessor[$implementations]=[
 __AbstractTransactionInfoProcessor,
]