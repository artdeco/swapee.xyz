import AbstractTransactionInfoControllerAR from '../AbstractTransactionInfoControllerAR'
import {AbstractTransactionInfoController} from '../AbstractTransactionInfoController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoControllerBack}
 */
function __AbstractTransactionInfoControllerBack() {}
__AbstractTransactionInfoControllerBack.prototype = /** @type {!_AbstractTransactionInfoControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoController}
 */
class _AbstractTransactionInfoControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoController} ‎
 */
class AbstractTransactionInfoControllerBack extends newAbstract(
 _AbstractTransactionInfoControllerBack,198805181922,null,{
  asITransactionInfoController:1,
  superTransactionInfoController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoController} */
AbstractTransactionInfoControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoController} */
function AbstractTransactionInfoControllerBackClass(){}

export default AbstractTransactionInfoControllerBack


AbstractTransactionInfoControllerBack[$implementations]=[
 __AbstractTransactionInfoControllerBack,
 AbstractTransactionInfoController,
 AbstractTransactionInfoControllerAR,
 DriverBack,
]