import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoComputer}
 */
function __AbstractTransactionInfoComputer() {}
__AbstractTransactionInfoComputer.prototype = /** @type {!_AbstractTransactionInfoComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoComputer}
 */
class _AbstractTransactionInfoComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoComputer} ‎
 */
export class AbstractTransactionInfoComputer extends newAbstract(
 _AbstractTransactionInfoComputer,19880518191,null,{
  asITransactionInfoComputer:1,
  superTransactionInfoComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoComputer} */
AbstractTransactionInfoComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoComputer} */
function AbstractTransactionInfoComputerClass(){}


AbstractTransactionInfoComputer[$implementations]=[
 __AbstractTransactionInfoComputer,
 Adapter,
]


export default AbstractTransactionInfoComputer