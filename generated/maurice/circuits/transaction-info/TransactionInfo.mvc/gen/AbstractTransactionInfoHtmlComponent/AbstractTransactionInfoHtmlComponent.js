import AbstractTransactionInfoGPU from '../AbstractTransactionInfoGPU'
import AbstractTransactionInfoScreenBack from '../AbstractTransactionInfoScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {TransactionInfoInputsQPs} from '../../pqs/TransactionInfoInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractTransactionInfo from '../AbstractTransactionInfo'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoHtmlComponent}
 */
function __AbstractTransactionInfoHtmlComponent() {}
__AbstractTransactionInfoHtmlComponent.prototype = /** @type {!_AbstractTransactionInfoHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHtmlComponent}
 */
class _AbstractTransactionInfoHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.TransactionInfoHtmlComponent} */ (res)
  }
}
/**
 * The _ITransactionInfo_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoHtmlComponent} ‎
 */
export class AbstractTransactionInfoHtmlComponent extends newAbstract(
 _AbstractTransactionInfoHtmlComponent,198805181911,null,{
  asITransactionInfoHtmlComponent:1,
  superTransactionInfoHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHtmlComponent} */
AbstractTransactionInfoHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoHtmlComponent} */
function AbstractTransactionInfoHtmlComponentClass(){}


AbstractTransactionInfoHtmlComponent[$implementations]=[
 __AbstractTransactionInfoHtmlComponent,
 HtmlComponent,
 AbstractTransactionInfo,
 AbstractTransactionInfoGPU,
 AbstractTransactionInfoScreenBack,
 AbstractTransactionInfoHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoHtmlComponent}*/({
  inputsQPs:TransactionInfoInputsQPs,
 }),
]