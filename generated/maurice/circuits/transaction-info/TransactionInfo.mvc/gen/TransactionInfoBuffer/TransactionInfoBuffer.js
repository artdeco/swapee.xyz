import {makeBuffers} from '@webcircuits/webcircuits'

export const TransactionInfoBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  tid:String,
  getTransaction:Boolean,
  currencyFrom:String,
  currencyTo:String,
  amountFrom:String,
  amountTo:String,
  networkFee:String,
  partnerFee:String,
  rate:String,
  visibleAmount:String,
  notFound:Boolean,
  fixed:Boolean,
  gettingTransaction:Boolean,
  getTransactionError:String,
 }),
})

export default TransactionInfoBuffer