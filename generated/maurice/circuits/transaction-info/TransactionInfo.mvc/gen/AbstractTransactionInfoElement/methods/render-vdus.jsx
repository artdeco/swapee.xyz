export default function TransactionInfoRenderVdus(){
 return (<div $id="TransactionInfo">
  <vdu $id="TransactionLoaderWr" />
  <vdu $id="TransactionLoInWr" />
  <vdu $id="TransactionLoIn" />
  <vdu $id="GetTransactionErrorWr" />
  <vdu $id="GetTransactionErrorLa" />
  <vdu $id="NotFoundWr" />
  <vdu $id="TransactionFoundWr" />
  <vdu $id="CryptoIn" />
  <vdu $id="CryptoOut" />
  <vdu $id="RateLa" />
  <vdu $id="AmountIn" />
  <vdu $id="AmountOut" />
  <vdu $id="InIm" />
  <vdu $id="OutIm" />
  <vdu $id="FixedLock" />
  <vdu $id="NetworkFeeWr" />
  <vdu $id="NetworkFeeLa" />
  <vdu $id="PartnerFeeWr" />
  <vdu $id="PartnerFeeLa" />
  <vdu $id="VisibleAmountWr" />
  <vdu $id="VisibleAmounLa" />
  <vdu $id="BreakdownBu" />
  <vdu $id="ReloadGetTransactionBu" />
 </div>)
}