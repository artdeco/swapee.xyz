import TransactionInfoElementPort from '../TransactionInfoElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {TransactionInfoInputsPQs} from '../../pqs/TransactionInfoInputsPQs'
import {TransactionInfoCachePQs} from '../../pqs/TransactionInfoCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractTransactionInfo from '../AbstractTransactionInfo'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoElement}
 */
function __AbstractTransactionInfoElement() {}
__AbstractTransactionInfoElement.prototype = /** @type {!_AbstractTransactionInfoElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoElement}
 */
class _AbstractTransactionInfoElement { }
/**
 * A component description.
 *
 * The _ITransactionInfo_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoElement} ‎
 */
class AbstractTransactionInfoElement extends newAbstract(
 _AbstractTransactionInfoElement,198805181912,null,{
  asITransactionInfoElement:1,
  superTransactionInfoElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoElement} */
AbstractTransactionInfoElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoElement} */
function AbstractTransactionInfoElementClass(){}

export default AbstractTransactionInfoElement


AbstractTransactionInfoElement[$implementations]=[
 __AbstractTransactionInfoElement,
 ElementBase,
 AbstractTransactionInfoElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':tid':tidColAttr,
   ':get-transaction':getTransactionColAttr,
   ':currency-from':currencyFromColAttr,
   ':currency-to':currencyToColAttr,
   ':amount-from':amountFromColAttr,
   ':amount-to':amountToColAttr,
   ':network-fee':networkFeeColAttr,
   ':partner-fee':partnerFeeColAttr,
   ':rate':rateColAttr,
   ':visible-amount':visibleAmountColAttr,
   ':not-found':notFoundColAttr,
   ':fixed':fixedColAttr,
   ':getting-transaction':gettingTransactionColAttr,
   ':get-transaction-error':getTransactionErrorColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'tid':tidAttr,
    'get-transaction':getTransactionAttr,
    'currency-from':currencyFromAttr,
    'currency-to':currencyToAttr,
    'amount-from':amountFromAttr,
    'amount-to':amountToAttr,
    'network-fee':networkFeeAttr,
    'partner-fee':partnerFeeAttr,
    'rate':rateAttr,
    'visible-amount':visibleAmountAttr,
    'not-found':notFoundAttr,
    'fixed':fixedAttr,
    'getting-transaction':gettingTransactionAttr,
    'get-transaction-error':getTransactionErrorAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(tidAttr===undefined?{'tid':tidColAttr}:{}),
    ...(getTransactionAttr===undefined?{'get-transaction':getTransactionColAttr}:{}),
    ...(currencyFromAttr===undefined?{'currency-from':currencyFromColAttr}:{}),
    ...(currencyToAttr===undefined?{'currency-to':currencyToColAttr}:{}),
    ...(amountFromAttr===undefined?{'amount-from':amountFromColAttr}:{}),
    ...(amountToAttr===undefined?{'amount-to':amountToColAttr}:{}),
    ...(networkFeeAttr===undefined?{'network-fee':networkFeeColAttr}:{}),
    ...(partnerFeeAttr===undefined?{'partner-fee':partnerFeeColAttr}:{}),
    ...(rateAttr===undefined?{'rate':rateColAttr}:{}),
    ...(visibleAmountAttr===undefined?{'visible-amount':visibleAmountColAttr}:{}),
    ...(notFoundAttr===undefined?{'not-found':notFoundColAttr}:{}),
    ...(fixedAttr===undefined?{'fixed':fixedColAttr}:{}),
    ...(gettingTransactionAttr===undefined?{'getting-transaction':gettingTransactionColAttr}:{}),
    ...(getTransactionErrorAttr===undefined?{'get-transaction-error':getTransactionErrorColAttr}:{}),
   }
  },
 }),
 AbstractTransactionInfoElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'tid':tidAttr,
   'get-transaction':getTransactionAttr,
   'currency-from':currencyFromAttr,
   'currency-to':currencyToAttr,
   'amount-from':amountFromAttr,
   'amount-to':amountToAttr,
   'network-fee':networkFeeAttr,
   'partner-fee':partnerFeeAttr,
   'rate':rateAttr,
   'visible-amount':visibleAmountAttr,
   'not-found':notFoundAttr,
   'fixed':fixedAttr,
   'getting-transaction':gettingTransactionAttr,
   'get-transaction-error':getTransactionErrorAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    tid:tidAttr,
    getTransaction:getTransactionAttr,
    currencyFrom:currencyFromAttr,
    currencyTo:currencyToAttr,
    amountFrom:amountFromAttr,
    amountTo:amountToAttr,
    networkFee:networkFeeAttr,
    partnerFee:partnerFeeAttr,
    rate:rateAttr,
    visibleAmount:visibleAmountAttr,
    notFound:notFoundAttr,
    fixed:fixedAttr,
    gettingTransaction:gettingTransactionAttr,
    getTransactionError:getTransactionErrorAttr,
   }
  },
 }),
 AbstractTransactionInfoElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractTransactionInfoElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoElement}*/({
  classes:{
   'Class': '9bd81',
  },
  inputsPQs:TransactionInfoInputsPQs,
  cachePQs:TransactionInfoCachePQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractTransactionInfoElementClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','tid','getTransaction','currencyFrom','currencyTo','amountFrom','amountTo','networkFee','partnerFee','rate','visibleAmount','notFound','fixed','gettingTransaction','getTransactionError','no-solder',':no-solder',':tid','get-transaction',':get-transaction','currency-from',':currency-from','currency-to',':currency-to','amount-from',':amount-from','amount-to',':amount-to','network-fee',':network-fee','partner-fee',':partner-fee',':rate','visible-amount',':visible-amount','not-found',':not-found',':fixed','getting-transaction',':getting-transaction','get-transaction-error',':get-transaction-error','fe646','97bea','5a67c','96c88','c23cd','748e6','9dc9f','5fd9c','96f44','67942','4685c','5ccf4','cec31','7c389','43e5c','children']),
   })
  },
  get Port(){
   return TransactionInfoElementPort
  },
 }),
]



AbstractTransactionInfoElement[$implementations]=[AbstractTransactionInfo,
 /** @type {!AbstractTransactionInfoElement} */ ({
  rootId:'TransactionInfo',
  __$id:1988051819,
  fqn:'xyz.swapee.wc.ITransactionInfo',
  maurice_element_v3:true,
 }),
]