
import AbstractTransactionInfo from '../AbstractTransactionInfo'

/** @abstract {xyz.swapee.wc.ITransactionInfoElement} */
export default class AbstractTransactionInfoElement { }



AbstractTransactionInfoElement[$implementations]=[AbstractTransactionInfo,
 /** @type {!AbstractTransactionInfoElement} */ ({
  rootId:'TransactionInfo',
  __$id:1988051819,
  fqn:'xyz.swapee.wc.ITransactionInfo',
  maurice_element_v3:true,
 }),
]