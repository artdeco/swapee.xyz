import AbstractTransactionInfoDisplay from '../AbstractTransactionInfoDisplayBack'
import TransactionInfoClassesPQs from '../../pqs/TransactionInfoClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {TransactionInfoClassesQPs} from '../../pqs/TransactionInfoClassesQPs'
import {TransactionInfoVdusPQs} from '../../pqs/TransactionInfoVdusPQs'
import {TransactionInfoVdusQPs} from '../../pqs/TransactionInfoVdusQPs'
import {TransactionInfoMemoryPQs} from '../../pqs/TransactionInfoMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoGPU}
 */
function __AbstractTransactionInfoGPU() {}
__AbstractTransactionInfoGPU.prototype = /** @type {!_AbstractTransactionInfoGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoGPU}
 */
class _AbstractTransactionInfoGPU { }
/**
 * Handles the periphery of the _ITransactionInfoDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoGPU} ‎
 */
class AbstractTransactionInfoGPU extends newAbstract(
 _AbstractTransactionInfoGPU,198805181915,null,{
  asITransactionInfoGPU:1,
  superTransactionInfoGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoGPU} */
AbstractTransactionInfoGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoGPU} */
function AbstractTransactionInfoGPUClass(){}

export default AbstractTransactionInfoGPU


AbstractTransactionInfoGPU[$implementations]=[
 __AbstractTransactionInfoGPU,
 AbstractTransactionInfoGPUClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoGPU}*/({
  classesQPs:TransactionInfoClassesQPs,
  vdusPQs:TransactionInfoVdusPQs,
  vdusQPs:TransactionInfoVdusQPs,
  memoryPQs:TransactionInfoMemoryPQs,
 }),
 AbstractTransactionInfoDisplay,
 BrowserView,
 AbstractTransactionInfoGPUClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoGPU}*/({
  allocator(){
   pressFit(this.classes,'',TransactionInfoClassesPQs)
  },
 }),
]