import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoScreenAT}
 */
function __AbstractTransactionInfoScreenAT() {}
__AbstractTransactionInfoScreenAT.prototype = /** @type {!_AbstractTransactionInfoScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoScreenAT}
 */
class _AbstractTransactionInfoScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITransactionInfoScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoScreenAT} ‎
 */
class AbstractTransactionInfoScreenAT extends newAbstract(
 _AbstractTransactionInfoScreenAT,198805181928,null,{
  asITransactionInfoScreenAT:1,
  superTransactionInfoScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreenAT} */
AbstractTransactionInfoScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreenAT} */
function AbstractTransactionInfoScreenATClass(){}

export default AbstractTransactionInfoScreenAT


AbstractTransactionInfoScreenAT[$implementations]=[
 __AbstractTransactionInfoScreenAT,
 UartUniversal,
]