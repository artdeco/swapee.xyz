import TransactionInfoClassesPQs from '../../pqs/TransactionInfoClassesPQs'
import AbstractTransactionInfoScreenAR from '../AbstractTransactionInfoScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {TransactionInfoInputsPQs} from '../../pqs/TransactionInfoInputsPQs'
import {TransactionInfoMemoryQPs} from '../../pqs/TransactionInfoMemoryQPs'
import {TransactionInfoCacheQPs} from '../../pqs/TransactionInfoCacheQPs'
import {TransactionInfoVdusPQs} from '../../pqs/TransactionInfoVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoScreen}
 */
function __AbstractTransactionInfoScreen() {}
__AbstractTransactionInfoScreen.prototype = /** @type {!_AbstractTransactionInfoScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoScreen}
 */
class _AbstractTransactionInfoScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoScreen} ‎
 */
class AbstractTransactionInfoScreen extends newAbstract(
 _AbstractTransactionInfoScreen,198805181925,null,{
  asITransactionInfoScreen:1,
  superTransactionInfoScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoScreen} */
AbstractTransactionInfoScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoScreen} */
function AbstractTransactionInfoScreenClass(){}

export default AbstractTransactionInfoScreen


AbstractTransactionInfoScreen[$implementations]=[
 __AbstractTransactionInfoScreen,
 AbstractTransactionInfoScreenClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoScreen}*/({
  inputsPQs:TransactionInfoInputsPQs,
  classesPQs:TransactionInfoClassesPQs,
  memoryQPs:TransactionInfoMemoryQPs,
  cacheQPs:TransactionInfoCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractTransactionInfoScreenAR,
 AbstractTransactionInfoScreenClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoScreen}*/({
  vdusPQs:TransactionInfoVdusPQs,
 }),
]