import AbstractTransactionInfoScreenAT from '../AbstractTransactionInfoScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoScreenBack}
 */
function __AbstractTransactionInfoScreenBack() {}
__AbstractTransactionInfoScreenBack.prototype = /** @type {!_AbstractTransactionInfoScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoScreen}
 */
class _AbstractTransactionInfoScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoScreen} ‎
 */
class AbstractTransactionInfoScreenBack extends newAbstract(
 _AbstractTransactionInfoScreenBack,198805181926,null,{
  asITransactionInfoScreen:1,
  superTransactionInfoScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreen} */
AbstractTransactionInfoScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoScreen} */
function AbstractTransactionInfoScreenBackClass(){}

export default AbstractTransactionInfoScreenBack


AbstractTransactionInfoScreenBack[$implementations]=[
 __AbstractTransactionInfoScreenBack,
 AbstractTransactionInfoScreenAT,
]