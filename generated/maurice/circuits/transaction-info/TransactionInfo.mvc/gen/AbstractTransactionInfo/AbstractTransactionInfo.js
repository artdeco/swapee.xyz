import AbstractTransactionInfoProcessor from '../AbstractTransactionInfoProcessor'
import {TransactionInfoCore} from '../TransactionInfoCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractTransactionInfoComputer} from '../AbstractTransactionInfoComputer'
import {AbstractTransactionInfoController} from '../AbstractTransactionInfoController'
import {regulateTransactionInfoCache} from './methods/regulateTransactionInfoCache'
import {TransactionInfoCacheQPs} from '../../pqs/TransactionInfoCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfo}
 */
function __AbstractTransactionInfo() {}
__AbstractTransactionInfo.prototype = /** @type {!_AbstractTransactionInfo} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfo}
 */
class _AbstractTransactionInfo { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractTransactionInfo} ‎
 */
class AbstractTransactionInfo extends newAbstract(
 _AbstractTransactionInfo,19880518199,null,{
  asITransactionInfo:1,
  superTransactionInfo:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfo} */
AbstractTransactionInfo.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfo} */
function AbstractTransactionInfoClass(){}

export default AbstractTransactionInfo


AbstractTransactionInfo[$implementations]=[
 __AbstractTransactionInfo,
 TransactionInfoCore,
 AbstractTransactionInfoProcessor,
 IntegratedComponent,
 AbstractTransactionInfoComputer,
 AbstractTransactionInfoController,
 AbstractTransactionInfoClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfo}*/({
  regulateState:regulateTransactionInfoCache,
  stateQPs:TransactionInfoCacheQPs,
 }),
]


export {AbstractTransactionInfo}