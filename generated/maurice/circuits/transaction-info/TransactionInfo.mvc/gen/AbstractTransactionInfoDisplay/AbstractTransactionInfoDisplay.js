import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoDisplay}
 */
function __AbstractTransactionInfoDisplay() {}
__AbstractTransactionInfoDisplay.prototype = /** @type {!_AbstractTransactionInfoDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoDisplay}
 */
class _AbstractTransactionInfoDisplay { }
/**
 * Display for presenting information from the _ITransactionInfo_.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoDisplay} ‎
 */
class AbstractTransactionInfoDisplay extends newAbstract(
 _AbstractTransactionInfoDisplay,198805181916,null,{
  asITransactionInfoDisplay:1,
  superTransactionInfoDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoDisplay} */
AbstractTransactionInfoDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoDisplay} */
function AbstractTransactionInfoDisplayClass(){}

export default AbstractTransactionInfoDisplay


AbstractTransactionInfoDisplay[$implementations]=[
 __AbstractTransactionInfoDisplay,
 Display,
]