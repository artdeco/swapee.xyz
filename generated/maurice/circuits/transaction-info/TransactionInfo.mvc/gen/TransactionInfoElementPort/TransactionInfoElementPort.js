import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoElementPort}
 */
function __TransactionInfoElementPort() {}
__TransactionInfoElementPort.prototype = /** @type {!_TransactionInfoElementPort} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoElementPort} */ function TransactionInfoElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ITransactionInfoElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoElementPort}
 */
class _TransactionInfoElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoElementPort} ‎
 */
class TransactionInfoElementPort extends newAbstract(
 _TransactionInfoElementPort,198805181913,TransactionInfoElementPortConstructor,{
  asITransactionInfoElementPort:1,
  superTransactionInfoElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoElementPort} */
TransactionInfoElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoElementPort} */
function TransactionInfoElementPortClass(){}

export default TransactionInfoElementPort


TransactionInfoElementPort[$implementations]=[
 __TransactionInfoElementPort,
 TransactionInfoElementPortClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'get-transaction':undefined,
    'currency-from':undefined,
    'currency-to':undefined,
    'amount-from':undefined,
    'amount-to':undefined,
    'network-fee':undefined,
    'partner-fee':undefined,
    'visible-amount':undefined,
    'not-found':undefined,
    'getting-transaction':undefined,
    'get-transaction-error':undefined,
   })
  },
 }),
]