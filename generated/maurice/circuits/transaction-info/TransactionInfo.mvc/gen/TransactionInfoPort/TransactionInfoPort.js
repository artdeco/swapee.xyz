import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {TransactionInfoInputsPQs} from '../../pqs/TransactionInfoInputsPQs'
import {TransactionInfoOuterCoreConstructor} from '../TransactionInfoCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoPort}
 */
function __TransactionInfoPort() {}
__TransactionInfoPort.prototype = /** @type {!_TransactionInfoPort} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoPort} */ function TransactionInfoPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.TransactionInfoOuterCore} */ ({model:null})
  TransactionInfoOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoPort}
 */
class _TransactionInfoPort { }
/**
 * The port that serves as an interface to the _ITransactionInfo_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoPort} ‎
 */
export class TransactionInfoPort extends newAbstract(
 _TransactionInfoPort,19880518195,TransactionInfoPortConstructor,{
  asITransactionInfoPort:1,
  superTransactionInfoPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoPort} */
TransactionInfoPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoPort} */
function TransactionInfoPortClass(){}

export const TransactionInfoPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ITransactionInfo.Pinout>}*/({
 get Port() { return TransactionInfoPort },
})

TransactionInfoPort[$implementations]=[
 TransactionInfoPortClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoPort}*/({
  resetPort(){
   this.resetTransactionInfoPort()
  },
  resetTransactionInfoPort(){
   TransactionInfoPortConstructor.call(this)
  },
 }),
 __TransactionInfoPort,
 Parametric,
 TransactionInfoPortClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoPort}*/({
  constructor(){
   mountPins(this.inputs,'',TransactionInfoInputsPQs)
  },
 }),
]


export default TransactionInfoPort