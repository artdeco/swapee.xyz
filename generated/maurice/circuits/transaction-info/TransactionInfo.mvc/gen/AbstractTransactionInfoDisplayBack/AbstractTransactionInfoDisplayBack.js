import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoDisplay}
 */
function __AbstractTransactionInfoDisplay() {}
__AbstractTransactionInfoDisplay.prototype = /** @type {!_AbstractTransactionInfoDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoDisplay}
 */
class _AbstractTransactionInfoDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractTransactionInfoDisplay} ‎
 */
class AbstractTransactionInfoDisplay extends newAbstract(
 _AbstractTransactionInfoDisplay,198805181918,null,{
  asITransactionInfoDisplay:1,
  superTransactionInfoDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoDisplay} */
AbstractTransactionInfoDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTransactionInfoDisplay} */
function AbstractTransactionInfoDisplayClass(){}

export default AbstractTransactionInfoDisplay


AbstractTransactionInfoDisplay[$implementations]=[
 __AbstractTransactionInfoDisplay,
 GraphicsDriverBack,
]