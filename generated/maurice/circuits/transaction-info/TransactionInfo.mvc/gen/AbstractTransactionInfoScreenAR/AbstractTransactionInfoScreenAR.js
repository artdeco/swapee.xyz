import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTransactionInfoScreenAR}
 */
function __AbstractTransactionInfoScreenAR() {}
__AbstractTransactionInfoScreenAR.prototype = /** @type {!_AbstractTransactionInfoScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoScreenAR}
 */
class _AbstractTransactionInfoScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITransactionInfoScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractTransactionInfoScreenAR} ‎
 */
class AbstractTransactionInfoScreenAR extends newAbstract(
 _AbstractTransactionInfoScreenAR,198805181927,null,{
  asITransactionInfoScreenAR:1,
  superTransactionInfoScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoScreenAR} */
AbstractTransactionInfoScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTransactionInfoScreenAR} */
function AbstractTransactionInfoScreenARClass(){}

export default AbstractTransactionInfoScreenAR


AbstractTransactionInfoScreenAR[$implementations]=[
 __AbstractTransactionInfoScreenAR,
 AR,
 AbstractTransactionInfoScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ITransactionInfoScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractTransactionInfoScreenAR}