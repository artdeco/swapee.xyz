import {mountPins} from '@webcircuits/webcircuits'
import {TransactionInfoMemoryPQs} from '../../pqs/TransactionInfoMemoryPQs'
import {TransactionInfoCachePQs} from '../../pqs/TransactionInfoCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoCore}
 */
function __TransactionInfoCore() {}
__TransactionInfoCore.prototype = /** @type {!_TransactionInfoCore} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoCore} */ function TransactionInfoCoreConstructor() {
  /**@type {!xyz.swapee.wc.ITransactionInfoCore.Model}*/
  this.model={
    iconsFolder: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoCore}
 */
class _TransactionInfoCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoCore} ‎
 */
class TransactionInfoCore extends newAbstract(
 _TransactionInfoCore,19880518197,TransactionInfoCoreConstructor,{
  asITransactionInfoCore:1,
  superTransactionInfoCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoCore} */
TransactionInfoCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoCore} */
function TransactionInfoCoreClass(){}

export default TransactionInfoCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TransactionInfoOuterCore}
 */
function __TransactionInfoOuterCore() {}
__TransactionInfoOuterCore.prototype = /** @type {!_TransactionInfoOuterCore} */ ({ })
/** @this {xyz.swapee.wc.TransactionInfoOuterCore} */
export function TransactionInfoOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ITransactionInfoOuterCore.Model}*/
  this.model={
    tid: '',
    getTransaction: false,
    currencyFrom: '',
    currencyTo: '',
    amountFrom: '',
    amountTo: '',
    networkFee: '',
    partnerFee: '',
    rate: '',
    visibleAmount: '',
    notFound: false,
    fixed: null,
    gettingTransaction: false,
    getTransactionError: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTransactionInfoOuterCore}
 */
class _TransactionInfoOuterCore { }
/**
 * The _ITransactionInfo_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoOuterCore} ‎
 */
export class TransactionInfoOuterCore extends newAbstract(
 _TransactionInfoOuterCore,19880518193,TransactionInfoOuterCoreConstructor,{
  asITransactionInfoOuterCore:1,
  superTransactionInfoOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoOuterCore} */
TransactionInfoOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoOuterCore} */
function TransactionInfoOuterCoreClass(){}


TransactionInfoOuterCore[$implementations]=[
 __TransactionInfoOuterCore,
 TransactionInfoOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoOuterCore}*/({
  constructor(){
   mountPins(this.model,'',TransactionInfoMemoryPQs)
   mountPins(this.model,'',TransactionInfoCachePQs)
  },
 }),
]

TransactionInfoCore[$implementations]=[
 TransactionInfoCoreClass.prototype=/**@type {!xyz.swapee.wc.ITransactionInfoCore}*/({
  resetCore(){
   this.resetTransactionInfoCore()
  },
  resetTransactionInfoCore(){
   TransactionInfoCoreConstructor.call(
    /**@type {xyz.swapee.wc.TransactionInfoCore}*/(this),
   )
   TransactionInfoOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.TransactionInfoOuterCore}*/(
     /**@type {!xyz.swapee.wc.ITransactionInfoOuterCore}*/(this)),
   )
  },
 }),
 __TransactionInfoCore,
 TransactionInfoOuterCore,
]

export {TransactionInfoCore}