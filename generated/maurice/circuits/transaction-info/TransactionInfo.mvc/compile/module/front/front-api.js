import { TransactionInfoDisplay, TransactionInfoScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.TransactionInfoDisplay} */
export { TransactionInfoDisplay }
/** @lazy @api {xyz.swapee.wc.TransactionInfoScreen} */
export { TransactionInfoScreen }