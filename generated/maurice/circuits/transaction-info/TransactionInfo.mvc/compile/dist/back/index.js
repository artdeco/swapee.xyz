/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfo` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfo}
 */
class AbstractTransactionInfo extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITransactionInfo_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TransactionInfoPort}
 */
class TransactionInfoPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoController` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoController}
 */
class AbstractTransactionInfoController extends (class {/* lazy-loaded */}) {}
/**
 * The _ITransactionInfo_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.TransactionInfoHtmlComponent}
 */
class TransactionInfoHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TransactionInfoBuffer}
 */
class TransactionInfoBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoComputer}
 */
class AbstractTransactionInfoComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.TransactionInfoController}
 */
class TransactionInfoController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractTransactionInfo = AbstractTransactionInfo
module.exports.TransactionInfoPort = TransactionInfoPort
module.exports.AbstractTransactionInfoController = AbstractTransactionInfoController
module.exports.TransactionInfoHtmlComponent = TransactionInfoHtmlComponent
module.exports.TransactionInfoBuffer = TransactionInfoBuffer
module.exports.AbstractTransactionInfoComputer = AbstractTransactionInfoComputer
module.exports.TransactionInfoController = TransactionInfoController