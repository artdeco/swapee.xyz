import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractTransactionInfo}*/
export class AbstractTransactionInfo extends Module['19880518191'] {}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfo} */
AbstractTransactionInfo.class=function(){}
/** @type {typeof xyz.swapee.wc.TransactionInfoPort} */
export const TransactionInfoPort=Module['19880518193']
/**@extends {xyz.swapee.wc.AbstractTransactionInfoController}*/
export class AbstractTransactionInfoController extends Module['19880518194'] {}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoController} */
AbstractTransactionInfoController.class=function(){}
/** @type {typeof xyz.swapee.wc.TransactionInfoHtmlComponent} */
export const TransactionInfoHtmlComponent=Module['198805181910']
/** @type {typeof xyz.swapee.wc.TransactionInfoBuffer} */
export const TransactionInfoBuffer=Module['198805181911']
/**@extends {xyz.swapee.wc.AbstractTransactionInfoComputer}*/
export class AbstractTransactionInfoComputer extends Module['198805181930'] {}
/** @type {typeof xyz.swapee.wc.AbstractTransactionInfoComputer} */
AbstractTransactionInfoComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.back.TransactionInfoController} */
export const TransactionInfoController=Module['198805181961']