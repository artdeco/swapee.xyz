/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function e(a,b,k,da){return c["372700389812"](a,b,k,da,!1,void 0)};function f(){}f.prototype={G(){const {asIIntegratedController:{setInputs:a}}=this;a({g:!0})}};class aa{}class g extends e(aa,19880518198,null,{V:1,ga:2}){}g[d]=[f];
const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ba=h["61505580523"],l=h["615055805212"],ca=h["615055805218"],ea=h["615055805221"],fa=h["615055805223"],m=h["615055805233"];const n={D:"97bea",l:"96c88",m:"c23cd",h:"748e6",i:"9dc9f",v:"5fd9c",C:"96f44",F:"4685c",fixed:"cec31",s:"7c389",g:"5a67c",o:"43e5c",A:"5ccf4",rate:"67942"};const p={u:"ffba8"};function q(){}q.prototype={};function ha(){this.model={u:""}}class ia{}class r extends e(ia,19880518197,ha,{M:1,aa:2}){}function t(){}t.prototype={};function u(){this.model={D:"",g:!1,l:"",m:"",h:"",i:"",v:"",C:"",rate:"",F:"",A:!1,fixed:null,s:!1,o:""}}class ja{}class v extends e(ja,19880518193,u,{T:1,ea:2}){}v[d]=[t,{constructor(){m(this.model,"",n);m(this.model,"",p)}}];r[d]=[{},q,v];

const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ka=w.IntegratedController,la=w.Parametric;
const x=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ma=x.IntegratedComponentInitialiser,na=x.IntegratedComponent,oa=x["38"];function y(){}y.prototype={};class pa{}class z extends e(pa,19880518191,null,{K:1,Z:2}){}z[d]=[y,ca];const A={regulate:l({D:String,g:Boolean,l:String,m:String,h:String,i:String,v:String,C:String,rate:String,F:String,A:Boolean,fixed:Boolean,s:Boolean,o:String})};const B={...n};function C(){}C.prototype={};function D(){const a={model:null};u.call(a);this.inputs=a.model}class qa{}class E extends e(qa,19880518195,D,{U:1,fa:2}){}function F(){}E[d]=[F.prototype={resetPort(){D.call(this)}},C,la,F.prototype={constructor(){m(this.inputs,"",B)}}];function G(){}G.prototype={};class ra{}class H extends e(ra,198805181920,null,{j:1,H:2}){}H[d]=[{resetPort(){this.port.resetPort()}},G,{calibrate:function({g:a}){a&&setTimeout(()=>{const {j:{setInputs:b}}=this;b({g:!1})},1)}},A,ka,{get Port(){return E}}];const sa=l({u:String},{silent:!0});const ta=Object.keys(p).reduce((a,b)=>{a[p[b]]=b;return a},{});function I(){}I.prototype={};class ua{}class J extends e(ua,19880518199,null,{J:1,Y:2}){}J[d]=[I,r,g,na,z,H,{regulateState:sa,stateQPs:ta}];
const K=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const va=K["12817393923"],wa=K["12817393924"],xa=K["12817393925"],ya=K["12817393926"];function L(){}L.prototype={};class za{}class M extends e(za,198805181923,null,{L:1,$:2}){}M[d]=[L,ya,{allocator(){this.methods={G:"f303b"}}}];function N(){}N.prototype={};class Aa{}class O extends e(Aa,198805181922,null,{j:1,H:2}){}O[d]=[N,H,M,va];var P=class extends O.implements(){};function Q(){}Q.prototype={};class Ba{}class R extends e(Ba,198805181918,null,{O:1,ba:2}){}R[d]=[Q,wa];const S={I:"9bd81"};const Ca=Object.keys(S).reduce((a,b)=>{a[S[b]]=b;return a},{});const T={};const Da=Object.keys(T).reduce((a,b)=>{a[T[b]]=b;return a},{});function U(){}U.prototype={};class Ea{}class V extends e(Ea,198805181915,null,{P:1,ca:2}){}function W(){}V[d]=[U,W.prototype={classesQPs:Ca,vdusQPs:Da,memoryPQs:n},R,ba,W.prototype={allocator(){oa(this.classes,"",S)}}];function X(){}X.prototype={};class Fa{}class Y extends e(Fa,198805181928,null,{X:1,ia:2}){}Y[d]=[X,xa];function Z(){}Z.prototype={};class Ga{}class Ha extends e(Ga,198805181926,null,{W:1,ha:2}){}Ha[d]=[Z,Y];const Ia=Object.keys(B).reduce((a,b)=>{a[B[b]]=b;return a},{});function Ja(){}Ja.prototype={};class Ka{static mvc(a,b,k){return fa(this,a,b,null,k)}}class La extends e(Ka,198805181911,null,{R:1,da:2}){}La[d]=[Ja,ea,J,V,Ha,{inputsQPs:Ia}];var Ma=class extends La.implements(P,ma){};module.exports["19880518190"]=J;module.exports["19880518191"]=J;module.exports["19880518193"]=E;module.exports["19880518194"]=H;module.exports["198805181910"]=Ma;module.exports["198805181911"]=A;module.exports["198805181930"]=z;module.exports["198805181961"]=P;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1988051819']=module.exports