/**
 * Display for presenting information from the _ITransactionInfo_.
 * @extends {xyz.swapee.wc.TransactionInfoDisplay}
 */
class TransactionInfoDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.TransactionInfoScreen}
 */
class TransactionInfoScreen extends (class {/* lazy-loaded */}) {}

module.exports.TransactionInfoDisplay = TransactionInfoDisplay
module.exports.TransactionInfoScreen = TransactionInfoScreen