import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1988051819} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const b=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=b["372700389811"];function g(a,d,e,f){return b["372700389812"](a,d,e,f,!1,void 0)};function h(){}h.prototype={};class aa{}class k extends g(aa,"ITransactionInfoProcessor",null,{U:1,da:2}){}k[c]=[h];

const l=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ba=l["615055805212"],ca=l["615055805218"],m=l["615055805233"];const n={F:"97bea",m:"96c88",o:"c23cd",j:"748e6",l:"9dc9f",v:"5fd9c",D:"96f44",G:"4685c",fixed:"cec31",u:"7c389",g:"5a67c",s:"43e5c",i:"ffba8",A:"5ccf4",rate:"67942"};function p(){}p.prototype={};class da{}class q extends g(da,"ITransactionInfoCore",null,{K:1,Y:2}){}function E(){}E.prototype={};function F(){this.model={F:"",i:"",g:!1,m:"",o:"",j:"",l:"",v:"",D:"",rate:"",G:"",A:!1,fixed:null,u:!1,s:""}}class ea{}class G extends g(ea,"ITransactionInfoOuterCore",F,{R:1,aa:2}){}G[c]=[E,{constructor(){m(this.model,"",n)}}];q[c]=[{},p,G];
const H=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const fa=H.IntegratedComponentInitialiser,I=H.IntegratedComponent,ha=H["95173443851"];function J(){}J.prototype={};class ia{}class K extends g(ia,"ITransactionInfoComputer",null,{J:1,W:2}){}K[c]=[J,ca];const L={regulate:ba({F:String,i:String,g:Boolean,m:String,o:String,j:String,l:String,v:String,D:String,rate:String,G:String,A:Boolean,fixed:Boolean,u:Boolean,s:String})};
const M=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ja=M.IntegratedController,ka=M.Parametric;const N={...n};function O(){}O.prototype={};function la(){const a={model:null};F.call(a);this.inputs=a.model}class ma{}class P extends g(ma,"ITransactionInfoPort",la,{T:1,ba:2}){}function Q(){}P[c]=[Q.prototype={},O,ka,Q.prototype={constructor(){m(this.inputs,"",N)}}];function R(){}R.prototype={};class na{}class S extends g(na,"ITransactionInfoController",null,{H:1,X:2}){}S[c]=[{},R,{calibrate:function({g:a}){a&&setTimeout(()=>{const {H:{setInputs:d}}=this;d({g:!1})},1)}},L,ja,{get Port(){return P}}];function T(){}T.prototype={};class oa{}class U extends g(oa,"ITransactionInfo",null,{I:1,V:2}){}U[c]=[T,q,k,I,K,S];function pa({i:a}){return{i:a}};const qa=require(eval('"@type.engineering/web-computing"')).h;function ra(){return qa("div",{$id:"TransactionInfo"})};const sa=require(eval('"@type.engineering/web-computing"')).h;function ta(){return sa("div",{$id:"TransactionInfo"})};var V=class extends S.implements(){};require("https");require("http");const ua=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}ua("aqt");require("fs");require("child_process");
const W=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const va=W.ElementBase,wa=W.HTMLBlocker;function X(){}X.prototype={};function xa(){this.inputs={noSolder:!1}}class ya{}class za extends g(ya,"ITransactionInfoElementPort",xa,{P:1,$:2}){}za[c]=[X,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"icons-folder":void 0,"get-transaction":void 0,"currency-from":void 0,"currency-to":void 0,"amount-from":void 0,"amount-to":void 0,"network-fee":void 0,"partner-fee":void 0,"visible-amount":void 0,"not-found":void 0,"getting-transaction":void 0,"get-transaction-error":void 0})}}];function Aa(){}Aa.prototype={};class Ba{}class Y extends g(Ba,"ITransactionInfoElement",null,{M:1,Z:2}){}function Z(){}
Y[c]=[Aa,va,Z.prototype={calibrate:function({":no-solder":a,":tid":d,":icons-folder":e,":get-transaction":f,":currency-from":r,":currency-to":t,":amount-from":u,":amount-to":v,":network-fee":w,":partner-fee":x,":rate":y,":visible-amount":z,":not-found":A,":fixed":B,":getting-transaction":C,":get-transaction-error":D}){const {attributes:{"no-solder":Ca,tid:Da,"icons-folder":Ea,"get-transaction":Fa,"currency-from":Ga,"currency-to":Ha,"amount-from":Ia,"amount-to":Ja,"network-fee":Ka,"partner-fee":La,
rate:Ma,"visible-amount":Na,"not-found":Oa,fixed:Pa,"getting-transaction":Qa,"get-transaction-error":Ra}}=this;return{...(void 0===Ca?{"no-solder":a}:{}),...(void 0===Da?{tid:d}:{}),...(void 0===Ea?{"icons-folder":e}:{}),...(void 0===Fa?{"get-transaction":f}:{}),...(void 0===Ga?{"currency-from":r}:{}),...(void 0===Ha?{"currency-to":t}:{}),...(void 0===Ia?{"amount-from":u}:{}),...(void 0===Ja?{"amount-to":v}:{}),...(void 0===Ka?{"network-fee":w}:{}),...(void 0===La?{"partner-fee":x}:{}),...(void 0===
Ma?{rate:y}:{}),...(void 0===Na?{"visible-amount":z}:{}),...(void 0===Oa?{"not-found":A}:{}),...(void 0===Pa?{fixed:B}:{}),...(void 0===Qa?{"getting-transaction":C}:{}),...(void 0===Ra?{"get-transaction-error":D}:{})}}},Z.prototype={calibrate:({"no-solder":a,tid:d,"icons-folder":e,"get-transaction":f,"currency-from":r,"currency-to":t,"amount-from":u,"amount-to":v,"network-fee":w,"partner-fee":x,rate:y,"visible-amount":z,"not-found":A,fixed:B,"getting-transaction":C,"get-transaction-error":D})=>({noSolder:a,
F:d,i:e,g:f,m:r,o:t,j:u,l:v,v:w,D:x,rate:y,G:z,A,fixed:B,u:C,s:D})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={classes:{Class:"9bd81"},inputsPQs:N,vdus:{}},I,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder tid iconsFolder getTransaction currencyFrom currencyTo amountFrom amountTo networkFee partnerFee rate visibleAmount notFound fixed gettingTransaction getTransactionError no-solder :no-solder :tid icons-folder :icons-folder get-transaction :get-transaction currency-from :currency-from currency-to :currency-to amount-from :amount-from amount-to :amount-to network-fee :network-fee partner-fee :partner-fee :rate visible-amount :visible-amount not-found :not-found :fixed getting-transaction :getting-transaction get-transaction-error :get-transaction-error fe646 97bea ffba8 5a67c 96c88 c23cd 748e6 9dc9f 5fd9c 96f44 67942 4685c 5ccf4 cec31 7c389 43e5c children".split(" "))})},
get Port(){return za}}];Y[c]=[U,{rootId:"TransactionInfo",__$id:1988051819,fqn:"xyz.swapee.wc.ITransactionInfo",maurice_element_v3:!0}];class Sa extends Y.implements(V,ha,wa,fa,{solder:pa,server:ra,render:ta},{classesMap:!0,rootSelector:".TransactionInfo",stylesheet:"html/styles/TransactionInfo.css",blockName:"html/TransactionInfoBlock.html"}){};module.exports["19880518190"]=U;module.exports["19880518191"]=U;module.exports["19880518193"]=P;module.exports["19880518194"]=S;module.exports["19880518198"]=Sa;module.exports["198805181911"]=L;module.exports["198805181930"]=K;module.exports["198805181961"]=V;
/*! @embed-object-end {1988051819} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule