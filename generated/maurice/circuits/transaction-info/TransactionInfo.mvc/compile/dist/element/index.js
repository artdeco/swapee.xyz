/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfo` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfo}
 */
class AbstractTransactionInfo extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITransactionInfo_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TransactionInfoPort}
 */
class TransactionInfoPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoController` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoController}
 */
class AbstractTransactionInfoController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ITransactionInfo_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.TransactionInfoElement}
 */
class TransactionInfoElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TransactionInfoBuffer}
 */
class TransactionInfoBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITransactionInfoComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTransactionInfoComputer}
 */
class AbstractTransactionInfoComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.TransactionInfoController}
 */
class TransactionInfoController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractTransactionInfo = AbstractTransactionInfo
module.exports.TransactionInfoPort = TransactionInfoPort
module.exports.AbstractTransactionInfoController = AbstractTransactionInfoController
module.exports.TransactionInfoElement = TransactionInfoElement
module.exports.TransactionInfoBuffer = TransactionInfoBuffer
module.exports.AbstractTransactionInfoComputer = AbstractTransactionInfoComputer
module.exports.TransactionInfoController = TransactionInfoController

Object.defineProperties(module.exports, {
 'AbstractTransactionInfo': {get: () => require('./precompile/internal')[19880518191]},
 [19880518191]: {get: () => module.exports['AbstractTransactionInfo']},
 'TransactionInfoPort': {get: () => require('./precompile/internal')[19880518193]},
 [19880518193]: {get: () => module.exports['TransactionInfoPort']},
 'AbstractTransactionInfoController': {get: () => require('./precompile/internal')[19880518194]},
 [19880518194]: {get: () => module.exports['AbstractTransactionInfoController']},
 'TransactionInfoElement': {get: () => require('./precompile/internal')[19880518198]},
 [19880518198]: {get: () => module.exports['TransactionInfoElement']},
 'TransactionInfoBuffer': {get: () => require('./precompile/internal')[198805181911]},
 [198805181911]: {get: () => module.exports['TransactionInfoBuffer']},
 'AbstractTransactionInfoComputer': {get: () => require('./precompile/internal')[198805181930]},
 [198805181930]: {get: () => module.exports['AbstractTransactionInfoComputer']},
 'TransactionInfoController': {get: () => require('./precompile/internal')[198805181961]},
 [198805181961]: {get: () => module.exports['TransactionInfoController']},
})