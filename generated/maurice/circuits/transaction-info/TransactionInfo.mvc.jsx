/** @extends {xyz.swapee.wc.AbstractTransactionInfo} */
export default class AbstractTransactionInfo extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractTransactionInfoComputer} */
export class AbstractTransactionInfoComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTransactionInfoController} */
export class AbstractTransactionInfoController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTransactionInfoPort} */
export class TransactionInfoPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTransactionInfoView} */
export class AbstractTransactionInfoView extends (<view>
  <classes>
   <string opt name="Class">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTransactionInfoElement} */
export class AbstractTransactionInfoElement extends (<element v3 html mv>
 <block src="./TransactionInfo.mvc/src/TransactionInfoElement/methods/render.jsx" />
 <inducer src="./TransactionInfo.mvc/src/TransactionInfoElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTransactionInfoHtmlComponent} */
export class AbstractTransactionInfoHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>