import AbstractHyperOfferRowInterruptLine from '../../../../gen/AbstractOfferRowInterruptLine/hyper/AbstractHyperOfferRowInterruptLine'
import OfferRowInterruptLine from '../../OfferRowInterruptLine'
import OfferRowInterruptLineGeneralAspects from '../OfferRowInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperOfferRowInterruptLine} */
export default class extends AbstractHyperOfferRowInterruptLine
 .consults(
  OfferRowInterruptLineGeneralAspects,
 )
 .implements(
  OfferRowInterruptLine,
 )
{}