/** @extends {xyz.swapee.wc.AbstractOfferRow} */
export default class AbstractOfferRow extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractOfferRowComputer} */
export class AbstractOfferRowComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferRowController} */
export class AbstractOfferRowController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOfferRowPort} */
export class OfferRowPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOfferRowView} */
export class AbstractOfferRowView extends (<view>
  <classes>
   <string opt name="BestOffer">The best offer.</string>
   <string opt name="Recommended">The recommended offer.</string>
   <string opt name="ProgressVertLine">The vertical line for progress.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferRowElement} */
export class AbstractOfferRowElement extends (<element v3 html mv>
 <block src="./OfferRow.mvc/src/OfferRowElement/methods/render.jsx" />
 <inducer src="./OfferRow.mvc/src/OfferRowElement/methods/inducer.jsx" />
 <element-port name="HTMLTags">
   <bool name="fixed">Whether this is a fixed-rate offer.</bool>
    <bool name="recommended">Whether this is a recommended offer that will be highlighted.</bool>
  </element-port>
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferRowHtmlComponent} */
export class AbstractOfferRowHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>